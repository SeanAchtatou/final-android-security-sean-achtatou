package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1", f = "Builders.kt", i = {}, l = {223}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Builders.kt */
final class FlowKt__BuildersKt$flowViaChannel$1 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Function2 $block;
    final /* synthetic */ int $bufferSize;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__BuildersKt$flowViaChannel$1(int i, Function2 function2, Continuation continuation) {
        super(2, continuation);
        this.$bufferSize = i;
        this.$block = function2;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__BuildersKt$flowViaChannel$1 flowKt__BuildersKt$flowViaChannel$1 = new FlowKt__BuildersKt$flowViaChannel$1(this.$bufferSize, this.$block, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__BuildersKt$flowViaChannel$1.p$ = (FlowCollector) obj;
        return flowKt__BuildersKt$flowViaChannel$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__BuildersKt$flowViaChannel$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1$1", f = "Builders.kt", i = {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2}, l = {240, 240, 230}, m = "invokeSuspend", n = {"channel", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "channel", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "channel", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "value"}, s = {"L$0", "L$1", "L$3", "L$4", "L$5", "L$0", "L$1", "L$3", "L$4", "L$5", "L$0", "L$1", "L$3", "L$4", "L$5", "L$7", "L$8"})
    /* renamed from: kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1$1  reason: invalid class name */
    /* compiled from: Builders.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        Object L$0;
        Object L$1;
        Object L$2;
        Object L$3;
        Object L$4;
        Object L$5;
        Object L$6;
        Object L$7;
        Object L$8;
        int label;
        private CoroutineScope p$;
        final /* synthetic */ FlowKt__BuildersKt$flowViaChannel$1 this$0;

        {
            this.this$0 = r1;
        }

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(this.this$0, flowCollector, continuation);
            CoroutineScope coroutineScope = (CoroutineScope) obj;
            r0.p$ = (CoroutineScope) obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        /* Debug info: failed to restart local var, previous not found, register: 25 */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v9, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v12, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v9, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v12, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v14, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v16, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v11, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0139 A[Catch:{ Throwable -> 0x0198 }] */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x017c A[Catch:{ Throwable -> 0x0198 }] */
        @org.jetbrains.annotations.Nullable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r26) {
            /*
                r25 = this;
                r1 = r25
                java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
                int r2 = r1.label
                r3 = 3
                r4 = 2
                r5 = 1
                r6 = 0
                if (r2 == 0) goto L_0x00cf
                r7 = 0
                if (r2 == r5) goto L_0x008e
                if (r2 == r4) goto L_0x0058
                if (r2 != r3) goto L_0x0050
                r2 = r6
                r8 = r7
                r9 = r6
                r10 = r6
                r11 = r7
                r12 = r6
                r13 = r6
                r14 = r7
                r15 = r6
                java.lang.Object r2 = r1.L$8
                java.lang.Object r9 = r1.L$7
                java.lang.Object r3 = r1.L$6
                kotlinx.coroutines.channels.ChannelIterator r3 = (kotlinx.coroutines.channels.ChannelIterator) r3
                java.lang.Object r4 = r1.L$5
                kotlinx.coroutines.channels.ReceiveChannel r4 = (kotlinx.coroutines.channels.ReceiveChannel) r4
                java.lang.Object r10 = r1.L$4
                java.lang.Throwable r10 = (java.lang.Throwable) r10
                java.lang.Object r12 = r1.L$3
                kotlinx.coroutines.channels.ReceiveChannel r12 = (kotlinx.coroutines.channels.ReceiveChannel) r12
                java.lang.Object r13 = r1.L$2
                kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1$1 r13 = (kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1.AnonymousClass1) r13
                java.lang.Object r5 = r1.L$1
                kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
                java.lang.Object r15 = r1.L$0
                r6 = r15
                kotlinx.coroutines.channels.Channel r6 = (kotlinx.coroutines.channels.Channel) r6
                kotlin.ResultKt.throwOnFailure(r26)     // Catch:{ Throwable -> 0x00c7, all -> 0x00bf }
                r16 = r8
                r15 = r11
                r11 = r26
                r8 = r0
                r0 = r13
                r13 = r7
                r7 = r5
                r5 = r1
                r1 = r2
                r2 = 3
                goto L_0x0183
            L_0x0050:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r2)
                throw r0
            L_0x0058:
                r2 = r6
                r3 = r7
                r4 = r6
                r5 = r6
                r14 = r7
                r8 = r6
                java.lang.Object r9 = r1.L$6
                kotlinx.coroutines.channels.ChannelIterator r9 = (kotlinx.coroutines.channels.ChannelIterator) r9
                java.lang.Object r10 = r1.L$5
                r2 = r10
                kotlinx.coroutines.channels.ReceiveChannel r2 = (kotlinx.coroutines.channels.ReceiveChannel) r2
                java.lang.Object r10 = r1.L$4
                java.lang.Throwable r10 = (java.lang.Throwable) r10
                java.lang.Object r4 = r1.L$3
                r12 = r4
                kotlinx.coroutines.channels.ReceiveChannel r12 = (kotlinx.coroutines.channels.ReceiveChannel) r12
                java.lang.Object r4 = r1.L$2
                kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1$1 r4 = (kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1.AnonymousClass1) r4
                java.lang.Object r5 = r1.L$1
                kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
                java.lang.Object r8 = r1.L$0
                r6 = r8
                kotlinx.coroutines.channels.Channel r6 = (kotlinx.coroutines.channels.Channel) r6
                kotlin.ResultKt.throwOnFailure(r26)     // Catch:{ Throwable -> 0x00c7, all -> 0x00bf }
                r11 = r26
                r13 = r11
                r8 = r7
                r15 = 2
                r7 = r5
                r5 = r1
                r23 = r9
                r9 = r3
                r3 = r23
                goto L_0x0156
            L_0x008e:
                r2 = r6
                r3 = r7
                r4 = r6
                r5 = r6
                r14 = r7
                r8 = r6
                java.lang.Object r9 = r1.L$6
                kotlinx.coroutines.channels.ChannelIterator r9 = (kotlinx.coroutines.channels.ChannelIterator) r9
                java.lang.Object r10 = r1.L$5
                r2 = r10
                kotlinx.coroutines.channels.ReceiveChannel r2 = (kotlinx.coroutines.channels.ReceiveChannel) r2
                java.lang.Object r10 = r1.L$4
                java.lang.Throwable r10 = (java.lang.Throwable) r10
                java.lang.Object r4 = r1.L$3
                r12 = r4
                kotlinx.coroutines.channels.ReceiveChannel r12 = (kotlinx.coroutines.channels.ReceiveChannel) r12
                java.lang.Object r4 = r1.L$2
                kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1$1 r4 = (kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1.AnonymousClass1) r4
                java.lang.Object r5 = r1.L$1
                kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
                java.lang.Object r8 = r1.L$0
                r6 = r8
                kotlinx.coroutines.channels.Channel r6 = (kotlinx.coroutines.channels.Channel) r6
                kotlin.ResultKt.throwOnFailure(r26)     // Catch:{ Throwable -> 0x00c7, all -> 0x00bf }
                r11 = r26
                r15 = r11
                r8 = r7
                r13 = 1
                r7 = r5
                r5 = r1
                goto L_0x0131
            L_0x00bf:
                r0 = move-exception
                r11 = r26
                r8 = r7
                r7 = r5
                r5 = r1
                goto L_0x01b9
            L_0x00c7:
                r0 = move-exception
                r11 = r26
                r8 = r7
                r7 = r5
                r5 = r1
                goto L_0x01b5
            L_0x00cf:
                kotlin.ResultKt.throwOnFailure(r26)
                kotlinx.coroutines.CoroutineScope r2 = r1.p$
                kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1 r3 = r1.this$0
                int r3 = r3.$bufferSize
                kotlinx.coroutines.channels.Channel r3 = kotlinx.coroutines.channels.ChannelKt.Channel(r3)
                r18 = 0
                r19 = 0
                kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1$1$1 r4 = new kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1$1$1
                r4.<init>(r1, r3, r6)
                r20 = r4
                kotlin.jvm.functions.Function2 r20 = (kotlin.jvm.functions.Function2) r20
                r21 = 3
                r22 = 0
                r17 = r2
                kotlinx.coroutines.Job unused = kotlinx.coroutines.BuildersKt__Builders_commonKt.launch$default(r17, r18, r19, r20, r21, r22)
                r5 = r3
                kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
                r7 = 0
                r12 = r5
                r14 = 0
                r10 = r6
                java.lang.Throwable r10 = (java.lang.Throwable) r10
                r2 = r12
                r4 = 0
                kotlinx.coroutines.channels.ChannelIterator r6 = r2.iterator()     // Catch:{ Throwable -> 0x01ad, all -> 0x01a4 }
                r11 = r4
                r9 = r6
                r8 = r7
                r7 = r26
                r4 = r0
                r0 = r1
                r6 = r3
                r3 = r2
                r2 = r0
            L_0x010c:
                r2.L$0 = r6     // Catch:{ Throwable -> 0x019f, all -> 0x019a }
                r2.L$1 = r5     // Catch:{ Throwable -> 0x019f, all -> 0x019a }
                r2.L$2 = r0     // Catch:{ Throwable -> 0x019f, all -> 0x019a }
                r2.L$3 = r12     // Catch:{ Throwable -> 0x019f, all -> 0x019a }
                r2.L$4 = r10     // Catch:{ Throwable -> 0x019f, all -> 0x019a }
                r2.L$5 = r3     // Catch:{ Throwable -> 0x019f, all -> 0x019a }
                r2.L$6 = r9     // Catch:{ Throwable -> 0x019f, all -> 0x019a }
                r13 = 1
                r2.label = r13     // Catch:{ Throwable -> 0x019f, all -> 0x019a }
                java.lang.Object r15 = r9.hasNext(r0)     // Catch:{ Throwable -> 0x019f, all -> 0x019a }
                if (r15 != r4) goto L_0x0124
                return r4
            L_0x0124:
                r23 = r4
                r4 = r0
                r0 = r23
                r24 = r5
                r5 = r2
                r2 = r3
                r3 = r11
                r11 = r7
                r7 = r24
            L_0x0131:
                java.lang.Boolean r15 = (java.lang.Boolean) r15     // Catch:{ Throwable -> 0x0198 }
                boolean r15 = r15.booleanValue()     // Catch:{ Throwable -> 0x0198 }
                if (r15 == 0) goto L_0x018f
                r5.L$0 = r6     // Catch:{ Throwable -> 0x0198 }
                r5.L$1 = r7     // Catch:{ Throwable -> 0x0198 }
                r5.L$2 = r4     // Catch:{ Throwable -> 0x0198 }
                r5.L$3 = r12     // Catch:{ Throwable -> 0x0198 }
                r5.L$4 = r10     // Catch:{ Throwable -> 0x0198 }
                r5.L$5 = r2     // Catch:{ Throwable -> 0x0198 }
                r5.L$6 = r9     // Catch:{ Throwable -> 0x0198 }
                r15 = 2
                r5.label = r15     // Catch:{ Throwable -> 0x0198 }
                java.lang.Object r13 = r9.next(r4)     // Catch:{ Throwable -> 0x0198 }
                if (r13 != r0) goto L_0x0151
                return r0
            L_0x0151:
                r23 = r9
                r9 = r3
                r3 = r23
            L_0x0156:
                r26 = r13
                r16 = 0
                kotlinx.coroutines.flow.FlowCollector r15 = r1     // Catch:{ Throwable -> 0x0198 }
                r5.L$0 = r6     // Catch:{ Throwable -> 0x0198 }
                r5.L$1 = r7     // Catch:{ Throwable -> 0x0198 }
                r5.L$2 = r4     // Catch:{ Throwable -> 0x0198 }
                r5.L$3 = r12     // Catch:{ Throwable -> 0x0198 }
                r5.L$4 = r10     // Catch:{ Throwable -> 0x0198 }
                r5.L$5 = r2     // Catch:{ Throwable -> 0x0198 }
                r5.L$6 = r3     // Catch:{ Throwable -> 0x0198 }
                r5.L$7 = r13     // Catch:{ Throwable -> 0x0198 }
                r1 = r26
                r5.L$8 = r1     // Catch:{ Throwable -> 0x0198 }
                r18 = r2
                r2 = 3
                r5.label = r2     // Catch:{ Throwable -> 0x0198 }
                java.lang.Object r15 = r15.emit(r1, r5)     // Catch:{ Throwable -> 0x0198 }
                if (r15 != r0) goto L_0x017c
                return r0
            L_0x017c:
                r15 = r9
                r9 = r13
                r13 = r8
                r8 = r0
                r0 = r4
                r4 = r18
            L_0x0183:
                r1 = r25
                r9 = r3
                r3 = r4
                r2 = r5
                r5 = r7
                r4 = r8
                r7 = r11
                r8 = r13
                r11 = r15
                goto L_0x010c
            L_0x018f:
                kotlin.Unit r0 = kotlin.Unit.INSTANCE     // Catch:{ Throwable -> 0x0198 }
                kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r12, r10)
                kotlin.Unit r0 = kotlin.Unit.INSTANCE
                return r0
            L_0x0198:
                r0 = move-exception
                goto L_0x01b5
            L_0x019a:
                r0 = move-exception
                r11 = r7
                r7 = r5
                r5 = r2
                goto L_0x01b9
            L_0x019f:
                r0 = move-exception
                r11 = r7
                r7 = r5
                r5 = r2
                goto L_0x01b5
            L_0x01a4:
                r0 = move-exception
                r11 = r26
                r6 = r3
                r8 = r7
                r7 = r5
                r5 = r25
                goto L_0x01b9
            L_0x01ad:
                r0 = move-exception
                r11 = r26
                r6 = r3
                r8 = r7
                r7 = r5
                r5 = r25
            L_0x01b5:
                r10 = r0
                throw r0     // Catch:{ all -> 0x01b8 }
            L_0x01b8:
                r0 = move-exception
            L_0x01b9:
                kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r12, r10)
                throw r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__BuildersKt$flowViaChannel$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            final FlowCollector flowCollector = this.p$;
            this.label = 1;
            if (CoroutineScopeKt.coroutineScope(new AnonymousClass1(this, null), this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            ResultKt.throwOnFailure(result);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }
}
