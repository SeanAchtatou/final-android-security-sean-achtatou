package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$3", f = "Builders.kt", i = {0, 0, 0}, l = {91}, m = "invokeSuspend", n = {"$this$forEach$iv", "element$iv", "value"}, s = {"L$1", "L$3", "L$4"})
/* compiled from: Builders.kt */
final class FlowKt__BuildersKt$asFlow$3 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Iterable $this_asFlow;
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    Object L$4;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__BuildersKt$asFlow$3(Iterable iterable, Continuation continuation) {
        super(2, continuation);
        this.$this_asFlow = iterable;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__BuildersKt$asFlow$3 flowKt__BuildersKt$asFlow$3 = new FlowKt__BuildersKt$asFlow$3(this.$this_asFlow, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__BuildersKt$asFlow$3.p$ = (FlowCollector) obj;
        return flowKt__BuildersKt$asFlow$3;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__BuildersKt$asFlow$3) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0048  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r13) {
        /*
            r12 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r12.label
            r2 = 1
            if (r1 == 0) goto L_0x0030
            if (r1 != r2) goto L_0x0028
            r1 = 0
            r3 = r1
            r4 = 0
            r5 = r4
            r6 = r1
            java.lang.Object r3 = r12.L$4
            java.lang.Object r6 = r12.L$3
            java.lang.Object r7 = r12.L$2
            java.util.Iterator r7 = (java.util.Iterator) r7
            java.lang.Object r8 = r12.L$1
            r1 = r8
            java.lang.Iterable r1 = (java.lang.Iterable) r1
            java.lang.Object r8 = r12.L$0
            kotlinx.coroutines.flow.FlowCollector r8 = (kotlinx.coroutines.flow.FlowCollector) r8
            kotlin.ResultKt.throwOnFailure(r13)
            r9 = r0
            r0 = r13
            r13 = r12
            goto L_0x0065
        L_0x0028:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0030:
            kotlin.ResultKt.throwOnFailure(r13)
            kotlinx.coroutines.flow.FlowCollector r1 = r12.p$
            java.lang.Iterable r3 = r12.$this_asFlow
            r4 = 0
            java.util.Iterator r5 = r3.iterator()
            r8 = r1
            r1 = r3
            r7 = r5
            r3 = r0
            r0 = r13
            r13 = r12
        L_0x0042:
            boolean r5 = r7.hasNext()
            if (r5 == 0) goto L_0x0067
            java.lang.Object r6 = r7.next()
            r5 = r6
            r9 = 0
            r13.L$0 = r8
            r13.L$1 = r1
            r13.L$2 = r7
            r13.L$3 = r6
            r13.L$4 = r5
            r13.label = r2
            java.lang.Object r10 = r8.emit(r5, r13)
            if (r10 != r3) goto L_0x0061
            return r3
        L_0x0061:
            r11 = r9
            r9 = r3
            r3 = r5
            r5 = r11
        L_0x0065:
            r3 = r9
            goto L_0x0042
        L_0x0067:
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$3.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
