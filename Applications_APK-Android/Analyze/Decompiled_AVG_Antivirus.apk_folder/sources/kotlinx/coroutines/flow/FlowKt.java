package kotlinx.coroutines.flow;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.BuilderInference;
import kotlin.Metadata;
import kotlin.PublishedApi;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.ranges.IntRange;
import kotlin.ranges.LongRange;
import kotlin.sequences.Sequence;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.FlowPreview;
import kotlinx.coroutines.channels.BroadcastChannel;
import kotlinx.coroutines.channels.ReceiveChannel;
import kotlinx.coroutines.channels.SendChannel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"kotlinx/coroutines/flow/FlowKt__BuildersKt", "kotlinx/coroutines/flow/FlowKt__ChannelFlowKt", "kotlinx/coroutines/flow/FlowKt__CollectKt", "kotlinx/coroutines/flow/FlowKt__CollectionKt", "kotlinx/coroutines/flow/FlowKt__ContextKt", "kotlinx/coroutines/flow/FlowKt__CountKt", "kotlinx/coroutines/flow/FlowKt__DelayKt", "kotlinx/coroutines/flow/FlowKt__DistinctKt", "kotlinx/coroutines/flow/FlowKt__ErrorsKt", "kotlinx/coroutines/flow/FlowKt__LimitKt", "kotlinx/coroutines/flow/FlowKt__MergeKt", "kotlinx/coroutines/flow/FlowKt__ReduceKt", "kotlinx/coroutines/flow/FlowKt__TransformKt", "kotlinx/coroutines/flow/FlowKt__ZipKt"}, k = 4, mv = {1, 1, 15})
public final class FlowKt {
    @NotNull
    @FlowPreview
    public static final <T> Flow<T> asFlow(@NotNull Iterable<? extends T> $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> asFlow(@NotNull Iterator<? extends T> $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> asFlow(@NotNull Function0<? extends T> $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> asFlow(@NotNull Function1<? super Continuation<? super T>, ? extends Object> $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final Flow<Integer> asFlow(@NotNull IntRange $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final Flow<Long> asFlow(@NotNull LongRange $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> asFlow(@NotNull Sequence<? extends T> $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> asFlow(@NotNull BroadcastChannel<T> $this$asFlow) {
        return FlowKt__ChannelFlowKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final Flow<Integer> asFlow(@NotNull int[] $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final Flow<Long> asFlow(@NotNull long[] $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> asFlow(@NotNull T[] $this$asFlow) {
        return FlowKt__BuildersKt.asFlow($this$asFlow);
    }

    @NotNull
    @FlowPreview
    public static final <T> BroadcastChannel<T> broadcastIn(@NotNull Flow<? extends T> $this$broadcastIn, @NotNull CoroutineScope scope, int capacity, @NotNull CoroutineStart start) {
        return FlowKt__ChannelFlowKt.broadcastIn($this$broadcastIn, scope, capacity, start);
    }

    @Nullable
    @FlowPreview
    public static final <T> Object collect(@NotNull Flow<? extends T> $this$collect, @NotNull Function2<? super T, ? super Continuation<? super Unit>, ? extends Object> action, @NotNull Continuation<? super Unit> $completion) {
        return FlowKt__CollectKt.collect($this$collect, action, $completion);
    }

    @NotNull
    public static final <T1, T2, R> Flow<R> combineLatest(@NotNull Flow<? extends T1> $this$combineLatest, @NotNull Flow<? extends T2> other, @NotNull Function3<? super T1, ? super T2, ? super Continuation<? super R>, ? extends Object> transform) {
        return FlowKt__ZipKt.combineLatest($this$combineLatest, other, transform);
    }

    @Nullable
    @FlowPreview
    public static final <T> Object count(@NotNull Flow<? extends T> $this$count, @NotNull Continuation<? super Integer> $completion) {
        return FlowKt__CountKt.count($this$count, $completion);
    }

    @Nullable
    @FlowPreview
    public static final <T> Object count(@NotNull Flow<? extends T> $this$count, @NotNull Function2<? super T, ? super Continuation<? super Boolean>, ? extends Object> predicate, @NotNull Continuation<? super Integer> $completion) {
        return FlowKt__CountKt.count($this$count, predicate, $completion);
    }

    @NotNull
    public static final <T> Flow<T> debounce(@NotNull Flow<? extends T> $this$debounce, long timeoutMillis) {
        return FlowKt__DelayKt.debounce($this$debounce, timeoutMillis);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> delayEach(@NotNull Flow<? extends T> $this$delayEach, long timeMillis) {
        return FlowKt__DelayKt.delayEach($this$delayEach, timeMillis);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> delayFlow(@NotNull Flow<? extends T> $this$delayFlow, long timeMillis) {
        return FlowKt__DelayKt.delayFlow($this$delayFlow, timeMillis);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> distinctUntilChanged(@NotNull Flow<? extends T> $this$distinctUntilChanged) {
        return FlowKt__DistinctKt.distinctUntilChanged($this$distinctUntilChanged);
    }

    @NotNull
    @FlowPreview
    public static final <T, K> Flow<T> distinctUntilChangedBy(@NotNull Flow<? extends T> $this$distinctUntilChangedBy, @NotNull Function1<? super T, ? extends K> keySelector) {
        return FlowKt__DistinctKt.distinctUntilChangedBy($this$distinctUntilChangedBy, keySelector);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> drop(@NotNull Flow<? extends T> $this$drop, int count) {
        return FlowKt__LimitKt.drop($this$drop, count);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> dropWhile(@NotNull Flow<? extends T> $this$dropWhile, @NotNull Function2<? super T, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        return FlowKt__LimitKt.dropWhile($this$dropWhile, predicate);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> emptyFlow() {
        return FlowKt__BuildersKt.emptyFlow();
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> filter(@NotNull Flow<? extends T> $this$filter, @NotNull Function2<? super T, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        return FlowKt__TransformKt.filter($this$filter, predicate);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> filterNot(@NotNull Flow<? extends T> $this$filterNot, @NotNull Function2<? super T, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        return FlowKt__TransformKt.filterNot($this$filterNot, predicate);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> filterNotNull(@NotNull Flow<? extends T> $this$filterNotNull) {
        return FlowKt__TransformKt.filterNotNull($this$filterNotNull);
    }

    @NotNull
    public static final ReceiveChannel<Unit> fixedPeriodTicker(@NotNull CoroutineScope $this$fixedPeriodTicker, long delayMillis, long initialDelayMillis) {
        return FlowKt__DelayKt.fixedPeriodTicker($this$fixedPeriodTicker, delayMillis, initialDelayMillis);
    }

    @NotNull
    @FlowPreview
    public static final <T, R> Flow<R> flatMapConcat(@NotNull Flow<? extends T> $this$flatMapConcat, @NotNull Function2<? super T, ? super Continuation<? super Flow<? extends R>>, ? extends Object> transform) {
        return FlowKt__MergeKt.flatMapConcat($this$flatMapConcat, transform);
    }

    @NotNull
    @FlowPreview
    public static final <T, R> Flow<R> flatMapMerge(@NotNull Flow<? extends T> $this$flatMapMerge, int concurrency, int bufferSize, @NotNull Function2<? super T, ? super Continuation<? super Flow<? extends R>>, ? extends Object> transform) {
        return FlowKt__MergeKt.flatMapMerge($this$flatMapMerge, concurrency, bufferSize, transform);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> flattenConcat(@NotNull Flow<? extends Flow<? extends T>> $this$flattenConcat) {
        return FlowKt__MergeKt.flattenConcat($this$flattenConcat);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> flattenMerge(@NotNull Flow<? extends Flow<? extends T>> $this$flattenMerge, int concurrency, int bufferSize) {
        return FlowKt__MergeKt.flattenMerge($this$flattenMerge, concurrency, bufferSize);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> flow(@NotNull @BuilderInference Function2<? super FlowCollector<? super T>, ? super Continuation<? super Unit>, ? extends Object> block) {
        return FlowKt__BuildersKt.flow(block);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> flowOf(@NotNull T... elements) {
        return FlowKt__BuildersKt.flowOf(elements);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> flowOn(@NotNull Flow<? extends T> $this$flowOn, @NotNull CoroutineContext flowContext, int bufferSize) {
        return FlowKt__ContextKt.flowOn($this$flowOn, flowContext, bufferSize);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> flowViaChannel(int bufferSize, @NotNull @BuilderInference Function2<? super CoroutineScope, ? super SendChannel<? super T>, Unit> block) {
        return FlowKt__BuildersKt.flowViaChannel(bufferSize, block);
    }

    @NotNull
    @FlowPreview
    public static final <T, R> Flow<R> flowWith(@NotNull Flow<? extends T> $this$flowWith, @NotNull CoroutineContext flowContext, int bufferSize, @NotNull Function1<? super Flow<? extends T>, ? extends Flow<? extends R>> builder) {
        return FlowKt__ContextKt.flowWith($this$flowWith, flowContext, bufferSize, builder);
    }

    @Nullable
    @FlowPreview
    public static final <T, R> Object fold(@NotNull Flow<? extends T> $this$fold, R initial, @NotNull Function3<? super R, ? super T, ? super Continuation<? super R>, ? extends Object> operation, @NotNull Continuation<? super R> $completion) {
        return FlowKt__ReduceKt.fold($this$fold, initial, operation, $completion);
    }

    @NotNull
    @FlowPreview
    public static final <T, R> Flow<R> map(@NotNull Flow<? extends T> $this$map, @NotNull Function2<? super T, ? super Continuation<? super R>, ? extends Object> transformer) {
        return FlowKt__TransformKt.map($this$map, transformer);
    }

    @NotNull
    @FlowPreview
    public static final <T, R> Flow<R> mapNotNull(@NotNull Flow<? extends T> $this$mapNotNull, @NotNull Function2<? super T, ? super Continuation<? super R>, ? extends Object> transformer) {
        return FlowKt__TransformKt.mapNotNull($this$mapNotNull, transformer);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> onEach(@NotNull Flow<? extends T> $this$onEach, @NotNull Function2<? super T, ? super Continuation<? super Unit>, ? extends Object> action) {
        return FlowKt__TransformKt.onEach($this$onEach, action);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> onErrorCollect(@NotNull Flow<? extends T> $this$onErrorCollect, @NotNull Flow<? extends T> fallback, @NotNull Function1<? super Throwable, Boolean> predicate) {
        return FlowKt__ErrorsKt.onErrorCollect($this$onErrorCollect, fallback, predicate);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> onErrorReturn(@NotNull Flow<? extends T> $this$onErrorReturn, T fallback, @NotNull Function1<? super Throwable, Boolean> predicate) {
        return FlowKt__ErrorsKt.onErrorReturn($this$onErrorReturn, fallback, predicate);
    }

    @NotNull
    @FlowPreview
    public static final <T> ReceiveChannel<T> produceIn(@NotNull Flow<? extends T> $this$produceIn, @NotNull CoroutineScope scope, int capacity) {
        return FlowKt__ChannelFlowKt.produceIn($this$produceIn, scope, capacity);
    }

    @Nullable
    @FlowPreview
    public static final <S, T extends S> Object reduce(@NotNull Flow<? extends T> $this$reduce, @NotNull Function3<? super S, ? super T, ? super Continuation<? super S>, ? extends Object> operation, @NotNull Continuation<? super S> $completion) {
        return FlowKt__ReduceKt.reduce($this$reduce, operation, $completion);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> retry(@NotNull Flow<? extends T> $this$retry, int retries, @NotNull Function1<? super Throwable, Boolean> predicate) {
        return FlowKt__ErrorsKt.retry($this$retry, retries, predicate);
    }

    @NotNull
    public static final <T> Flow<T> sample(@NotNull Flow<? extends T> $this$sample, long periodMillis) {
        return FlowKt__DelayKt.sample($this$sample, periodMillis);
    }

    @Nullable
    @FlowPreview
    public static final <T> Object single(@NotNull Flow<? extends T> $this$single, @NotNull Continuation<? super T> $completion) {
        return FlowKt__ReduceKt.single($this$single, $completion);
    }

    @Nullable
    @FlowPreview
    public static final <T> Object singleOrNull(@NotNull Flow<? extends T> $this$singleOrNull, @NotNull Continuation<? super T> $completion) {
        return FlowKt__ReduceKt.singleOrNull($this$singleOrNull, $completion);
    }

    @NotNull
    @FlowPreview
    public static final <T, R> Flow<R> switchMap(@NotNull Flow<? extends T> $this$switchMap, @NotNull Function2<? super T, ? super Continuation<? super Flow<? extends R>>, ? extends Object> transform) {
        return FlowKt__MergeKt.switchMap($this$switchMap, transform);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> take(@NotNull Flow<? extends T> $this$take, int count) {
        return FlowKt__LimitKt.take($this$take, count);
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> takeWhile(@NotNull Flow<? extends T> $this$takeWhile, @NotNull Function2<? super T, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        return FlowKt__LimitKt.takeWhile($this$takeWhile, predicate);
    }

    @Nullable
    @FlowPreview
    public static final <T, C extends Collection<? super T>> Object toCollection(@NotNull Flow<? extends T> $this$toCollection, @NotNull C destination, @NotNull Continuation<? super C> $completion) {
        return FlowKt__CollectionKt.toCollection($this$toCollection, destination, $completion);
    }

    @Nullable
    @FlowPreview
    public static final <T> Object toList(@NotNull Flow<? extends T> $this$toList, @NotNull List<T> destination, @NotNull Continuation<? super List<? extends T>> $completion) {
        return FlowKt__CollectionKt.toList($this$toList, destination, $completion);
    }

    @Nullable
    @FlowPreview
    public static final <T> Object toSet(@NotNull Flow<? extends T> $this$toSet, @NotNull Set<T> destination, @NotNull Continuation<? super Set<? extends T>> $completion) {
        return FlowKt__CollectionKt.toSet($this$toSet, destination, $completion);
    }

    @NotNull
    @FlowPreview
    public static final <T, R> Flow<R> transform(@NotNull Flow<? extends T> $this$transform, @NotNull @BuilderInference Function3<? super FlowCollector<? super R>, ? super T, ? super Continuation<? super Unit>, ? extends Object> transformer) {
        return FlowKt__TransformKt.transform($this$transform, transformer);
    }

    @PublishedApi
    @FlowPreview
    @NotNull
    public static final <T> Flow<T> unsafeFlow(@NotNull @BuilderInference Function2<? super FlowCollector<? super T>, ? super Continuation<? super Unit>, ? extends Object> block) {
        return FlowKt__BuildersKt.unsafeFlow(block);
    }

    @NotNull
    public static final <T1, T2, R> Flow<R> zip(@NotNull Flow<? extends T1> $this$zip, @NotNull Flow<? extends T2> other, @NotNull Function3<? super T1, ? super T2, ? super Continuation<? super R>, ? extends Object> transform) {
        return FlowKt__ZipKt.zip($this$zip, other, transform);
    }
}
