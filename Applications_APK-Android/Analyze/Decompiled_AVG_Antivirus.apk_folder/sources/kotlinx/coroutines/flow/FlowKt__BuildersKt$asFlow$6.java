package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$6", f = "Builders.kt", i = {0, 0, 0}, l = {141}, m = "invokeSuspend", n = {"$this$forEach$iv", "element$iv", "value"}, s = {"L$1", "L$3", "L$4"})
/* compiled from: Builders.kt */
final class FlowKt__BuildersKt$asFlow$6 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Object[] $this_asFlow;
    int I$0;
    int I$1;
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    Object L$4;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__BuildersKt$asFlow$6(Object[] objArr, Continuation continuation) {
        super(2, continuation);
        this.$this_asFlow = objArr;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__BuildersKt$asFlow$6 flowKt__BuildersKt$asFlow$6 = new FlowKt__BuildersKt$asFlow$6(this.$this_asFlow, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__BuildersKt$asFlow$6.p$ = (FlowCollector) obj;
        return flowKt__BuildersKt$asFlow$6;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__BuildersKt$asFlow$6) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0048  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r15) {
        /*
            r14 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r14.label
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0034
            if (r1 != r2) goto L_0x002c
            r1 = 0
            r4 = r1
            r5 = r3
            r6 = r1
            java.lang.Object r4 = r14.L$4
            java.lang.Object r6 = r14.L$3
            int r7 = r14.I$1
            int r8 = r14.I$0
            java.lang.Object r9 = r14.L$2
            java.lang.Object[] r9 = (java.lang.Object[]) r9
            java.lang.Object r10 = r14.L$1
            r1 = r10
            java.lang.Object[] r1 = (java.lang.Object[]) r1
            java.lang.Object r10 = r14.L$0
            kotlinx.coroutines.flow.FlowCollector r10 = (kotlinx.coroutines.flow.FlowCollector) r10
            kotlin.ResultKt.throwOnFailure(r15)
            r11 = r0
            r0 = r15
            r15 = r14
            goto L_0x0067
        L_0x002c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0034:
            kotlin.ResultKt.throwOnFailure(r15)
            kotlinx.coroutines.flow.FlowCollector r1 = r14.p$
            java.lang.Object[] r4 = r14.$this_asFlow
            r5 = 0
            int r6 = r4.length
            r10 = r1
            r1 = r4
            r9 = r1
            r3 = r5
            r8 = r6
            r7 = 0
            r4 = r0
            r0 = r15
            r15 = r14
        L_0x0046:
            if (r7 >= r8) goto L_0x006b
            r6 = r9[r7]
            r5 = r6
            r11 = 0
            r15.L$0 = r10
            r15.L$1 = r1
            r15.L$2 = r9
            r15.I$0 = r8
            r15.I$1 = r7
            r15.L$3 = r6
            r15.L$4 = r5
            r15.label = r2
            java.lang.Object r12 = r10.emit(r5, r15)
            if (r12 != r4) goto L_0x0063
            return r4
        L_0x0063:
            r13 = r11
            r11 = r4
            r4 = r5
            r5 = r13
        L_0x0067:
            int r7 = r7 + r2
            r4 = r11
            goto L_0x0046
        L_0x006b:
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$6.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
