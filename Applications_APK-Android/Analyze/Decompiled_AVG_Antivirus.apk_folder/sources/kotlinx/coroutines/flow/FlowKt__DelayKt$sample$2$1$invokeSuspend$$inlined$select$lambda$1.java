package kotlinx.coroutines.flow;

import java.util.concurrent.CancellationException;
import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;
import kotlinx.coroutines.channels.ReceiveChannel;
import kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"<anonymous>", "", "T", "it", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx/coroutines/flow/FlowKt__DelayKt$sample$2$1$1$1"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1$1$1", f = "Delay.kt", i = {}, l = {}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Delay.kt */
final class FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1 extends SuspendLambda implements Function2<Object, Continuation<? super Unit>, Object> {
    final /* synthetic */ Ref.BooleanRef $isDone$inlined;
    final /* synthetic */ Ref.ObjectRef $lastValue$inlined;
    final /* synthetic */ ReceiveChannel $ticker$inlined;
    final /* synthetic */ ReceiveChannel $values$inlined;
    int label;
    private Object p$0;
    final /* synthetic */ FlowKt__DelayKt$sample$2.AnonymousClass1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1(Continuation continuation, FlowKt__DelayKt$sample$2.AnonymousClass1 r2, ReceiveChannel receiveChannel, ReceiveChannel receiveChannel2, Ref.BooleanRef booleanRef, Ref.ObjectRef objectRef) {
        super(2, continuation);
        this.this$0 = r2;
        this.$values$inlined = receiveChannel;
        this.$ticker$inlined = receiveChannel2;
        this.$isDone$inlined = booleanRef;
        this.$lastValue$inlined = objectRef;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1 flowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1 = new FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1(continuation, this.this$0, this.$values$inlined, this.$ticker$inlined, this.$isDone$inlined, this.$lastValue$inlined);
        flowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1.p$0 = obj;
        return flowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        IntrinsicsKt.getCOROUTINE_SUSPENDED();
        if (this.label == 0) {
            ResultKt.throwOnFailure(result);
            T t = this.p$0;
            if (t == null) {
                ReceiveChannel.DefaultImpls.cancel$default(this.$ticker$inlined, (CancellationException) null, 1, (Object) null);
                this.$isDone$inlined.element = true;
            } else {
                this.$lastValue$inlined.element = t;
            }
            return Unit.INSTANCE;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
