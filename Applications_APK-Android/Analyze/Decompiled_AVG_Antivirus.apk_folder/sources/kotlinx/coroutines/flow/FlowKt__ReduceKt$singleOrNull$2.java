package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u0006\u0010\u0004\u001a\u0002H\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "T", "", "value", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ReduceKt$singleOrNull$2", f = "Reduce.kt", i = {}, l = {}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Reduce.kt */
final class FlowKt__ReduceKt$singleOrNull$2 extends SuspendLambda implements Function2<T, Continuation<? super Unit>, Object> {
    final /* synthetic */ Ref.ObjectRef $result;
    int label;
    private Object p$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__ReduceKt$singleOrNull$2(Ref.ObjectRef objectRef, Continuation continuation) {
        super(2, continuation);
        this.$result = objectRef;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__ReduceKt$singleOrNull$2 flowKt__ReduceKt$singleOrNull$2 = new FlowKt__ReduceKt$singleOrNull$2(this.$result, continuation);
        flowKt__ReduceKt$singleOrNull$2.p$0 = obj;
        return flowKt__ReduceKt$singleOrNull$2;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__ReduceKt$singleOrNull$2) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        IntrinsicsKt.getCOROUTINE_SUSPENDED();
        if (this.label == 0) {
            ResultKt.throwOnFailure(result);
            T t = this.p$0;
            if (this.$result.element == null) {
                this.$result.element = t;
                return Unit.INSTANCE;
            }
            throw new IllegalStateException("Expected only one element".toString());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
