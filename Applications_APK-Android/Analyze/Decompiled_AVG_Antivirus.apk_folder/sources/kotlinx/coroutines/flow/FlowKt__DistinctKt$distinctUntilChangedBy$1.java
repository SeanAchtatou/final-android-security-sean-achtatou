package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;
import kotlinx.coroutines.flow.internal.NullSurrogate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00020\u0004H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "T", "K", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__DistinctKt$distinctUntilChangedBy$1", f = "Distinct.kt", i = {0}, l = {29}, m = "invokeSuspend", n = {"previousKey"}, s = {"L$0"})
/* compiled from: Distinct.kt */
final class FlowKt__DistinctKt$distinctUntilChangedBy$1 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Function1 $keySelector;
    final /* synthetic */ Flow $this_distinctUntilChangedBy;
    Object L$0;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__DistinctKt$distinctUntilChangedBy$1(Flow flow, Function1 function1, Continuation continuation) {
        super(2, continuation);
        this.$this_distinctUntilChangedBy = flow;
        this.$keySelector = function1;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__DistinctKt$distinctUntilChangedBy$1 flowKt__DistinctKt$distinctUntilChangedBy$1 = new FlowKt__DistinctKt$distinctUntilChangedBy$1(this.$this_distinctUntilChangedBy, this.$keySelector, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__DistinctKt$distinctUntilChangedBy$1.p$ = (FlowCollector) obj;
        return flowKt__DistinctKt$distinctUntilChangedBy$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__DistinctKt$distinctUntilChangedBy$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            final FlowCollector flowCollector = this.p$;
            final Ref.ObjectRef previousKey = new Ref.ObjectRef();
            previousKey.element = NullSurrogate.INSTANCE;
            this.L$0 = previousKey;
            this.label = 1;
            if (FlowKt.collect(this.$this_distinctUntilChangedBy, new AnonymousClass1(this, null), this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            Ref.ObjectRef previousKey2 = (Ref.ObjectRef) this.L$0;
            ResultKt.throwOnFailure(result);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u00032\u0006\u0010\u0004\u001a\u0002H\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "T", "K", "value", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__DistinctKt$distinctUntilChangedBy$1$1", f = "Distinct.kt", i = {0}, l = {33}, m = "invokeSuspend", n = {"key"}, s = {"L$0"})
    /* renamed from: kotlinx.coroutines.flow.FlowKt__DistinctKt$distinctUntilChangedBy$1$1  reason: invalid class name */
    /* compiled from: Distinct.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<T, Continuation<? super Unit>, Object> {
        Object L$0;
        int label;
        private Object p$0;
        final /* synthetic */ FlowKt__DistinctKt$distinctUntilChangedBy$1 this$0;

        {
            this.this$0 = r1;
        }

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(this.this$0, flowCollector, previousKey, continuation);
            r0.p$0 = obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.jvm.internal.Intrinsics.areEqual(java.lang.Object, java.lang.Object):boolean
         arg types: [T, T]
         candidates:
          kotlin.jvm.internal.Intrinsics.areEqual(double, java.lang.Double):boolean
          kotlin.jvm.internal.Intrinsics.areEqual(float, java.lang.Float):boolean
          kotlin.jvm.internal.Intrinsics.areEqual(java.lang.Double, double):boolean
          kotlin.jvm.internal.Intrinsics.areEqual(java.lang.Double, java.lang.Double):boolean
          kotlin.jvm.internal.Intrinsics.areEqual(java.lang.Float, float):boolean
          kotlin.jvm.internal.Intrinsics.areEqual(java.lang.Float, java.lang.Float):boolean
          kotlin.jvm.internal.Intrinsics.areEqual(java.lang.Object, java.lang.Object):boolean */
        @Nullable
        public final Object invokeSuspend(@NotNull Object result) {
            Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                ResultKt.throwOnFailure(result);
                Object obj = this.p$0;
                T invoke = this.this$0.$keySelector.invoke(obj);
                if (!Intrinsics.areEqual((Object) previousKey.element, (Object) invoke)) {
                    previousKey.element = invoke;
                    FlowCollector flowCollector = flowCollector;
                    this.L$0 = invoke;
                    this.label = 1;
                    if (flowCollector.emit(obj, this) == coroutine_suspended) {
                        return coroutine_suspended;
                    }
                }
            } else if (i == 1) {
                Object key = this.L$0;
                ResultKt.throwOnFailure(result);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Unit.INSTANCE;
        }
    }
}
