package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005H@ø\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"<anonymous>", "", "T1", "T2", "R", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1", f = "Zip.kt", i = {}, l = {32}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Zip.kt */
final class FlowKt__ZipKt$combineLatest$1 extends SuspendLambda implements Function2<FlowCollector<? super R>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Flow $other;
    final /* synthetic */ Flow $this_combineLatest;
    final /* synthetic */ Function3 $transform;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__ZipKt$combineLatest$1(Flow flow, Flow flow2, Function3 function3, Continuation continuation) {
        super(2, continuation);
        this.$this_combineLatest = flow;
        this.$other = flow2;
        this.$transform = function3;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__ZipKt$combineLatest$1 flowKt__ZipKt$combineLatest$1 = new FlowKt__ZipKt$combineLatest$1(this.$this_combineLatest, this.$other, this.$transform, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__ZipKt$combineLatest$1.p$ = (FlowCollector) obj;
        return flowKt__ZipKt$combineLatest$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__ZipKt$combineLatest$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0004*\u00020\u0005H@ø\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"<anonymous>", "", "T1", "T2", "R", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1", f = "Zip.kt", i = {0, 0, 0, 0, 0, 0}, l = {160}, m = "invokeSuspend", n = {"firstChannel", "secondChannel", "firstValue", "secondValue", "firstIsClosed", "secondIsClosed"}, s = {"L$0", "L$1", "L$2", "L$3", "L$4", "L$5"})
    /* renamed from: kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1  reason: invalid class name */
    /* compiled from: Zip.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        Object L$0;
        Object L$1;
        Object L$2;
        Object L$3;
        Object L$4;
        Object L$5;
        Object L$6;
        int label;
        private CoroutineScope p$;
        final /* synthetic */ FlowKt__ZipKt$combineLatest$1 this$0;

        {
            this.this$0 = r1;
        }

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(this.this$0, flowCollector, continuation);
            CoroutineScope coroutineScope = (CoroutineScope) obj;
            r0.p$ = (CoroutineScope) obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v6, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: kotlin.jvm.internal.Ref$BooleanRef} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v7, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v6, resolved type: kotlin.jvm.internal.Ref$BooleanRef} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v9, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v5, resolved type: kotlin.jvm.internal.Ref$ObjectRef} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v10, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v10, resolved type: kotlinx.coroutines.channels.Channel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v11, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: kotlinx.coroutines.channels.Channel} */
        /* JADX WARNING: Can't wrap try/catch for region: R(19:0|(1:(2:3|37)(2:4|5))(1:6)|7|(14:13|14|15|16|17|18|(1:20)(1:21)|22|(1:24)(1:25)|30|(1:32)|33|(1:35)(17:36|37|7|(1:9)|13|14|15|16|17|18|(0)(0)|22|(0)(0)|30|(0)|33|(0))|35)(2:11|12)|9|13|14|15|16|17|18|(0)(0)|22|(0)(0)|30|(0)|33|(0)(0)|35) */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x014f, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0151, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0152, code lost:
            r3 = r12;
            r25 = r14;
            r26 = r15;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0159, code lost:
            r3.handleBuilderException(r0);
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00dd A[Catch:{ Throwable -> 0x014f }] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00de A[Catch:{ Throwable -> 0x014f }] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0126 A[Catch:{ Throwable -> 0x014f }] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0127 A[Catch:{ Throwable -> 0x014f }] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0167  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0172 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0173  */
        @org.jetbrains.annotations.Nullable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r36) {
            /*
                r35 = this;
                r1 = r35
                java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
                int r2 = r1.label
                r3 = 1
                r4 = 0
                r5 = 0
                if (r2 == 0) goto L_0x0050
                if (r2 != r3) goto L_0x0048
                r2 = r4
                r4 = r5
                r6 = r5
                r7 = r5
                r8 = r5
                r9 = r5
                java.lang.Object r10 = r1.L$6
                kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1 r10 = (kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1.AnonymousClass1) r10
                java.lang.Object r10 = r1.L$5
                r4 = r10
                kotlin.jvm.internal.Ref$BooleanRef r4 = (kotlin.jvm.internal.Ref.BooleanRef) r4
                java.lang.Object r10 = r1.L$4
                r6 = r10
                kotlin.jvm.internal.Ref$BooleanRef r6 = (kotlin.jvm.internal.Ref.BooleanRef) r6
                java.lang.Object r10 = r1.L$3
                r7 = r10
                kotlin.jvm.internal.Ref$ObjectRef r7 = (kotlin.jvm.internal.Ref.ObjectRef) r7
                java.lang.Object r10 = r1.L$2
                r8 = r10
                kotlin.jvm.internal.Ref$ObjectRef r8 = (kotlin.jvm.internal.Ref.ObjectRef) r8
                java.lang.Object r10 = r1.L$1
                r9 = r10
                kotlinx.coroutines.channels.Channel r9 = (kotlinx.coroutines.channels.Channel) r9
                java.lang.Object r10 = r1.L$0
                r5 = r10
                kotlinx.coroutines.channels.Channel r5 = (kotlinx.coroutines.channels.Channel) r5
                kotlin.ResultKt.throwOnFailure(r36)
                r19 = r36
                r14 = r0
                r15 = r1
                r2 = r9
                r34 = r6
                r6 = r5
                r5 = r7
                r7 = r8
                r8 = r34
                goto L_0x0176
            L_0x0048:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r2)
                throw r0
            L_0x0050:
                kotlin.ResultKt.throwOnFailure(r36)
                kotlinx.coroutines.CoroutineScope r2 = r1.p$
                kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1 r6 = r1.this$0
                kotlinx.coroutines.flow.Flow r6 = r6.$this_combineLatest
                kotlinx.coroutines.channels.Channel r6 = kotlinx.coroutines.flow.FlowKt__ZipKt.asFairChannel$FlowKt__ZipKt(r2, r6)
                kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1 r7 = r1.this$0
                kotlinx.coroutines.flow.Flow r7 = r7.$other
                kotlinx.coroutines.channels.Channel r2 = kotlinx.coroutines.flow.FlowKt__ZipKt.asFairChannel$FlowKt__ZipKt(r2, r7)
                kotlin.jvm.internal.Ref$ObjectRef r7 = new kotlin.jvm.internal.Ref$ObjectRef
                r7.<init>()
                r7.element = r5
                kotlin.jvm.internal.Ref$ObjectRef r8 = new kotlin.jvm.internal.Ref$ObjectRef
                r8.<init>()
                r8.element = r5
                r5 = r8
                kotlin.jvm.internal.Ref$BooleanRef r8 = new kotlin.jvm.internal.Ref$BooleanRef
                r8.<init>()
                r8.element = r4
                kotlin.jvm.internal.Ref$BooleanRef r9 = new kotlin.jvm.internal.Ref$BooleanRef
                r9.<init>()
                r9.element = r4
                r4 = r9
                r19 = r36
                r14 = r0
                r15 = r1
            L_0x0087:
                boolean r0 = r8.element
                if (r0 == 0) goto L_0x0093
                boolean r0 = r4.element
                if (r0 != 0) goto L_0x0090
                goto L_0x0093
            L_0x0090:
                kotlin.Unit r0 = kotlin.Unit.INSTANCE
                return r0
            L_0x0093:
                r20 = 0
                r15.L$0 = r6
                r15.L$1 = r2
                r15.L$2 = r7
                r15.L$3 = r5
                r15.L$4 = r8
                r15.L$5 = r4
                r15.L$6 = r15
                r15.label = r3
                r13 = r15
                kotlin.coroutines.Continuation r13 = (kotlin.coroutines.Continuation) r13
                r21 = 0
                kotlinx.coroutines.selects.SelectBuilderImpl r0 = new kotlinx.coroutines.selects.SelectBuilderImpl
                r0.<init>(r13)
                r12 = r0
                r0 = r12
                kotlinx.coroutines.selects.SelectBuilder r0 = (kotlinx.coroutines.selects.SelectBuilder) r0     // Catch:{ Throwable -> 0x0151 }
                r22 = 0
                boolean r11 = r8.element     // Catch:{ Throwable -> 0x0151 }
                kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1$invokeSuspend$$inlined$select$lambda$1 r18 = new kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1$invokeSuspend$$inlined$select$lambda$1     // Catch:{ Throwable -> 0x0151 }
                r10 = 0
                r9 = r18
                r23 = r11
                r11 = r15
                r3 = r12
                r12 = r8
                r24 = r13
                r13 = r6
                r25 = r14
                r14 = r7
                r26 = r15
                r15 = r5
                r16 = r4
                r17 = r2
                r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)     // Catch:{ Throwable -> 0x014f }
                kotlin.jvm.functions.Function2 r18 = (kotlin.jvm.functions.Function2) r18     // Catch:{ Throwable -> 0x014f }
                r10 = r18
                r15 = r0
                r27 = r6
                r28 = 0
                if (r23 == 0) goto L_0x00de
                goto L_0x0105
            L_0x00de:
                kotlinx.coroutines.selects.SelectClause1 r14 = r27.getOnReceiveOrNull()     // Catch:{ Throwable -> 0x014f }
                kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1$invokeSuspend$$inlined$select$lambda$2 r29 = new kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1$invokeSuspend$$inlined$select$lambda$2     // Catch:{ Throwable -> 0x014f }
                r11 = 0
                r9 = r29
                r12 = r26
                r13 = r8
                r30 = r14
                r14 = r6
                r31 = r15
                r15 = r7
                r16 = r5
                r17 = r4
                r18 = r2
                r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ Throwable -> 0x014f }
                r9 = r29
                kotlin.jvm.functions.Function2 r9 = (kotlin.jvm.functions.Function2) r9     // Catch:{ Throwable -> 0x014f }
                r12 = r30
                r11 = r31
                r11.invoke(r12, r9)     // Catch:{ Throwable -> 0x014f }
            L_0x0105:
                boolean r15 = r4.element     // Catch:{ Throwable -> 0x014f }
                kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1$invokeSuspend$$inlined$select$lambda$3 r18 = new kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1$invokeSuspend$$inlined$select$lambda$3     // Catch:{ Throwable -> 0x014f }
                r10 = 0
                r9 = r18
                r11 = r26
                r12 = r8
                r13 = r6
                r14 = r7
                r23 = r15
                r15 = r5
                r16 = r4
                r17 = r2
                r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)     // Catch:{ Throwable -> 0x014f }
                kotlin.jvm.functions.Function2 r18 = (kotlin.jvm.functions.Function2) r18     // Catch:{ Throwable -> 0x014f }
                r10 = r18
                r15 = r0
                r27 = r2
                r28 = 0
                if (r23 == 0) goto L_0x0127
                goto L_0x014e
            L_0x0127:
                kotlinx.coroutines.selects.SelectClause1 r14 = r27.getOnReceiveOrNull()     // Catch:{ Throwable -> 0x014f }
                kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1$invokeSuspend$$inlined$select$lambda$4 r29 = new kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1$1$invokeSuspend$$inlined$select$lambda$4     // Catch:{ Throwable -> 0x014f }
                r11 = 0
                r9 = r29
                r12 = r26
                r13 = r8
                r32 = r14
                r14 = r6
                r33 = r15
                r15 = r7
                r16 = r5
                r17 = r4
                r18 = r2
                r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ Throwable -> 0x014f }
                r9 = r29
                kotlin.jvm.functions.Function2 r9 = (kotlin.jvm.functions.Function2) r9     // Catch:{ Throwable -> 0x014f }
                r12 = r32
                r11 = r33
                r11.invoke(r12, r9)     // Catch:{ Throwable -> 0x014f }
            L_0x014e:
                goto L_0x015c
            L_0x014f:
                r0 = move-exception
                goto L_0x0159
            L_0x0151:
                r0 = move-exception
                r3 = r12
                r24 = r13
                r25 = r14
                r26 = r15
            L_0x0159:
                r3.handleBuilderException(r0)
            L_0x015c:
                java.lang.Object r0 = r3.getResult()
                java.lang.Object r3 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
                if (r0 != r3) goto L_0x016e
                r3 = r26
                kotlin.coroutines.Continuation r3 = (kotlin.coroutines.Continuation) r3
                kotlin.coroutines.jvm.internal.DebugProbesKt.probeCoroutineSuspended(r3)
            L_0x016e:
                r3 = r25
                if (r0 != r3) goto L_0x0173
                return r3
            L_0x0173:
                r14 = r3
                r15 = r26
            L_0x0176:
                r3 = 1
                goto L_0x0087
            */
            throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__ZipKt$combineLatest$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            final FlowCollector flowCollector = this.p$;
            this.label = 1;
            if (CoroutineScopeKt.coroutineScope(new AnonymousClass1(this, null), this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            ResultKt.throwOnFailure(result);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }
}
