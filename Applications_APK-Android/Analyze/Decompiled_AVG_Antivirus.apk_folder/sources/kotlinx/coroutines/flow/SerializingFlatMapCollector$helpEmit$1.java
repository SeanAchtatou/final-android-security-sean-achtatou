package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.ContinuationImpl;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0004\b\u0000\u0010\u00022\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H@ø\u0001\u0000"}, d2 = {"helpEmit", "", "T", "continuation", "Lkotlin/coroutines/Continuation;", ""}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.SerializingFlatMapCollector", f = "Merge.kt", i = {0, 0}, l = {157}, m = "helpEmit", n = {"this", "element"}, s = {"L$0", "L$1"})
/* compiled from: Merge.kt */
final class SerializingFlatMapCollector$helpEmit$1 extends ContinuationImpl {
    Object L$0;
    Object L$1;
    int label;
    /* synthetic */ Object result;
    final /* synthetic */ SerializingFlatMapCollector this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    SerializingFlatMapCollector$helpEmit$1(SerializingFlatMapCollector serializingFlatMapCollector, Continuation continuation) {
        super(continuation);
        this.this$0 = serializingFlatMapCollector;
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.helpEmit(this);
    }
}
