package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__LimitKt$take$2", f = "Limit.kt", i = {0}, l = {57}, m = "invokeSuspend", n = {"consumed"}, s = {"L$0"})
/* compiled from: Limit.kt */
final class FlowKt__LimitKt$take$2 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ int $count;
    final /* synthetic */ Flow $this_take;
    Object L$0;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__LimitKt$take$2(Flow flow, int i, Continuation continuation) {
        super(2, continuation);
        this.$this_take = flow;
        this.$count = i;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__LimitKt$take$2 flowKt__LimitKt$take$2 = new FlowKt__LimitKt$take$2(this.$this_take, this.$count, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__LimitKt$take$2.p$ = (FlowCollector) obj;
        return flowKt__LimitKt$take$2;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__LimitKt$take$2) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            final FlowCollector flowCollector = this.p$;
            final Ref.IntRef consumed = new Ref.IntRef();
            consumed.element = 0;
            try {
                this.L$0 = consumed;
                this.label = 1;
                if (FlowKt.collect(this.$this_take, new AnonymousClass1(this, null), this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } catch (TakeLimitException e) {
            }
        } else if (i == 1) {
            Ref.IntRef consumed2 = (Ref.IntRef) this.L$0;
            try {
                ResultKt.throwOnFailure(result);
            } catch (TakeLimitException e2) {
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0003\u001a\u0002H\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "value", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__LimitKt$take$2$1", f = "Limit.kt", i = {}, l = {58}, m = "invokeSuspend", n = {}, s = {})
    /* renamed from: kotlinx.coroutines.flow.FlowKt__LimitKt$take$2$1  reason: invalid class name */
    /* compiled from: Limit.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<T, Continuation<? super Unit>, Object> {
        int label;
        private Object p$0;
        final /* synthetic */ FlowKt__LimitKt$take$2 this$0;

        {
            this.this$0 = r1;
        }

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(this.this$0, flowCollector, consumed, continuation);
            r0.p$0 = obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        @Nullable
        public final Object invokeSuspend(@NotNull Object result) {
            Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                ResultKt.throwOnFailure(result);
                Object obj = this.p$0;
                FlowCollector flowCollector = flowCollector;
                this.label = 1;
                if (flowCollector.emit(obj, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                ResultKt.throwOnFailure(result);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ref.IntRef intRef = consumed;
            intRef.element++;
            if (intRef.element != this.this$0.$count) {
                return Unit.INSTANCE;
            }
            throw new TakeLimitException();
        }
    }
}
