package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;
import kotlinx.coroutines.channels.ReceiveChannel;
import kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2;
import kotlinx.coroutines.flow.internal.NullSurrogate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\b\u0004\n\u0002\b\u0004\n\u0002\b\u0005\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0003\u001a\u00020\u0001H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"<anonymous>", "", "T", "it", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx/coroutines/flow/FlowKt__DelayKt$sample$2$1$1$2"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__DelayKt$sample$2$1$1$2", f = "Delay.kt", i = {0}, l = {144}, m = "invokeSuspend", n = {"value"}, s = {"L$0"})
/* compiled from: Delay.kt */
final class FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2 extends SuspendLambda implements Function2<Unit, Continuation<? super Unit>, Object> {
    final /* synthetic */ Ref.BooleanRef $isDone$inlined;
    final /* synthetic */ Ref.ObjectRef $lastValue$inlined;
    final /* synthetic */ ReceiveChannel $ticker$inlined;
    final /* synthetic */ ReceiveChannel $values$inlined;
    Object L$0;
    int label;
    private Unit p$0;
    final /* synthetic */ FlowKt__DelayKt$sample$2.AnonymousClass1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2(Continuation continuation, FlowKt__DelayKt$sample$2.AnonymousClass1 r2, ReceiveChannel receiveChannel, ReceiveChannel receiveChannel2, Ref.BooleanRef booleanRef, Ref.ObjectRef objectRef) {
        super(2, continuation);
        this.this$0 = r2;
        this.$values$inlined = receiveChannel;
        this.$ticker$inlined = receiveChannel2;
        this.$isDone$inlined = booleanRef;
        this.$lastValue$inlined = objectRef;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2 flowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2 = new FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2(continuation, this.this$0, this.$values$inlined, this.$ticker$inlined, this.$isDone$inlined, this.$lastValue$inlined);
        Unit unit = (Unit) obj;
        flowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2.p$0 = (Unit) obj;
        return flowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__DelayKt$sample$2$1$invokeSuspend$$inlined$select$lambda$2) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            Unit unit = this.p$0;
            T t = this.$lastValue$inlined.element;
            if (t == null) {
                return Unit.INSTANCE;
            }
            this.$lastValue$inlined.element = null;
            FlowCollector flowCollector = r1;
            Object unbox$kotlinx_coroutines_core = NullSurrogate.unbox$kotlinx_coroutines_core(t);
            this.L$0 = t;
            this.label = 1;
            if (flowCollector.emit(unbox$kotlinx_coroutines_core, this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            Object value = this.L$0;
            ResultKt.throwOnFailure(result);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }
}
