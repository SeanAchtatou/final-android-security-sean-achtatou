package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.channels.Channel;
import kotlinx.coroutines.channels.ChannelKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00030\u0004H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "T", "R", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__MergeKt$flatMapMerge$3", f = "Merge.kt", i = {}, l = {48}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Merge.kt */
final class FlowKt__MergeKt$flatMapMerge$3 extends SuspendLambda implements Function2<FlowCollector<? super R>, Continuation<? super Unit>, Object> {
    final /* synthetic */ int $bufferSize;
    final /* synthetic */ int $concurrency;
    final /* synthetic */ Flow $this_flatMapMerge;
    final /* synthetic */ Function2 $transform;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__MergeKt$flatMapMerge$3(Flow flow, int i, int i2, Function2 function2, Continuation continuation) {
        super(2, continuation);
        this.$this_flatMapMerge = flow;
        this.$concurrency = i;
        this.$bufferSize = i2;
        this.$transform = function2;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__MergeKt$flatMapMerge$3 flowKt__MergeKt$flatMapMerge$3 = new FlowKt__MergeKt$flatMapMerge$3(this.$this_flatMapMerge, this.$concurrency, this.$bufferSize, this.$transform, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__MergeKt$flatMapMerge$3.p$ = (FlowCollector) obj;
        return flowKt__MergeKt$flatMapMerge$3;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__MergeKt$flatMapMerge$3) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\u00020\u0004H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "T", "R", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__MergeKt$flatMapMerge$3$1", f = "Merge.kt", i = {0, 0}, l = {51}, m = "invokeSuspend", n = {"semaphore", "flatMap"}, s = {"L$0", "L$1"})
    /* renamed from: kotlinx.coroutines.flow.FlowKt__MergeKt$flatMapMerge$3$1  reason: invalid class name */
    /* compiled from: Merge.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        Object L$0;
        Object L$1;
        int label;
        private CoroutineScope p$;
        final /* synthetic */ FlowKt__MergeKt$flatMapMerge$3 this$0;

        {
            this.this$0 = r1;
        }

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(this.this$0, flowCollector, continuation);
            CoroutineScope coroutineScope = (CoroutineScope) obj;
            r0.p$ = (CoroutineScope) obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        @Nullable
        public final Object invokeSuspend(@NotNull Object result) {
            Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                ResultKt.throwOnFailure(result);
                final CoroutineScope coroutineScope = this.p$;
                Channel semaphore = ChannelKt.Channel(this.this$0.$concurrency);
                SerializingFlatMapCollector flatMap = new SerializingFlatMapCollector(flowCollector, this.this$0.$bufferSize);
                final Channel channel = semaphore;
                final SerializingFlatMapCollector serializingFlatMapCollector = flatMap;
                this.L$0 = semaphore;
                this.L$1 = flatMap;
                this.label = 1;
                if (FlowKt.collect(this.this$0.$this_flatMapMerge, new Function2<T, Continuation<? super Unit>, Object>(this, null) {
                    Object L$0;
                    int label;
                    private Object p$0;
                    final /* synthetic */ AnonymousClass1 this$0;

                    {
                        this.this$0 = r1;
                    }

                    @NotNull
                    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
                        Intrinsics.checkParameterIsNotNull(continuation, "completion");
                        AnonymousClass1 r1 = 

                        @Nullable
                        public final Object invokeSuspend(@NotNull Object result) {
                            Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
                            int i = this.label;
                            if (i == 0) {
                                ResultKt.throwOnFailure(result);
                                final FlowCollector flowCollector = this.p$;
                                this.label = 1;
                                if (CoroutineScopeKt.coroutineScope(new AnonymousClass1(this, null), this) == coroutine_suspended) {
                                    return coroutine_suspended;
                                }
                            } else if (i == 1) {
                                ResultKt.throwOnFailure(result);
                            } else {
                                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                            }
                            return Unit.INSTANCE;
                        }
                    }
