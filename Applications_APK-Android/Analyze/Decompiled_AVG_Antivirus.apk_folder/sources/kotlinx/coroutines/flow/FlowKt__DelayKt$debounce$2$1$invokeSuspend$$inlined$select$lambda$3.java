package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.channels.Channel;
import kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2;
import kotlinx.coroutines.flow.internal.NullSurrogate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\b\u0004\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"<anonymous>", "", "T", "invoke", "(Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx/coroutines/flow/FlowKt__DelayKt$debounce$2$1$1$3"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__DelayKt$debounce$2$1$1$3", f = "Delay.kt", i = {}, l = {93}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Delay.kt */
final class FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$3 extends SuspendLambda implements Function1<Continuation<? super Unit>, Object> {
    final /* synthetic */ Job $collector$inlined;
    final /* synthetic */ Ref.BooleanRef $isDone$inlined;
    final /* synthetic */ Ref.ObjectRef $lastValue$inlined;
    final /* synthetic */ Channel $values$inlined;
    int label;
    final /* synthetic */ FlowKt__DelayKt$debounce$2.AnonymousClass1 this$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$3(Continuation continuation, FlowKt__DelayKt$debounce$2.AnonymousClass1 r2, Channel channel, Ref.ObjectRef objectRef, Job job, Ref.BooleanRef booleanRef) {
        super(1, continuation);
        this.this$0 = r2;
        this.$values$inlined = channel;
        this.$lastValue$inlined = objectRef;
        this.$collector$inlined = job;
        this.$isDone$inlined = booleanRef;
    }

    @NotNull
    public final Continuation<Unit> create(@NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        return new FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$3(continuation, this.this$0, this.$values$inlined, this.$lastValue$inlined, this.$collector$inlined, this.$isDone$inlined);
    }

    public final Object invoke(Object obj) {
        return ((FlowKt__DelayKt$debounce$2$1$invokeSuspend$$inlined$select$lambda$3) create((Continuation) obj)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            if (this.$lastValue$inlined.element != null) {
                FlowCollector flowCollector = r1;
                Object unbox$kotlinx_coroutines_core = NullSurrogate.unbox$kotlinx_coroutines_core(this.$lastValue$inlined.element);
                this.label = 1;
                if (flowCollector.emit(unbox$kotlinx_coroutines_core, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            }
        } else if (i == 1) {
            ResultKt.throwOnFailure(result);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.$isDone$inlined.element = true;
        return Unit.INSTANCE;
    }
}
