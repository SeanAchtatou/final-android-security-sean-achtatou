package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\b\b\u0001\u0010\u0003*\u00020\u0004*\b\u0012\u0004\u0012\u0002H\u00030\u00052\u0006\u0010\u0006\u001a\u0002H\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"<anonymous>", "", "T", "R", "", "Lkotlinx/coroutines/flow/FlowCollector;", "value", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__TransformKt$mapNotNull$1", f = "Transform.kt", i = {1}, l = {83, 84}, m = "invokeSuspend", n = {"transformed"}, s = {"L$0"})
/* compiled from: Transform.kt */
final class FlowKt__TransformKt$mapNotNull$1 extends SuspendLambda implements Function3<FlowCollector<? super R>, T, Continuation<? super Unit>, Object> {
    final /* synthetic */ Function2 $transformer;
    Object L$0;
    int label;
    private FlowCollector p$;
    private Object p$0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__TransformKt$mapNotNull$1(Function2 function2, Continuation continuation) {
        super(3, continuation);
        this.$transformer = function2;
    }

    @NotNull
    public final Continuation<Unit> create(@NotNull FlowCollector<? super R> flowCollector, T t, @NotNull Continuation<? super Unit> continuation) {
        Intrinsics.checkParameterIsNotNull(flowCollector, "$this$create");
        Intrinsics.checkParameterIsNotNull(continuation, "continuation");
        FlowKt__TransformKt$mapNotNull$1 flowKt__TransformKt$mapNotNull$1 = new FlowKt__TransformKt$mapNotNull$1(this.$transformer, continuation);
        flowKt__TransformKt$mapNotNull$1.p$ = flowCollector;
        flowKt__TransformKt$mapNotNull$1.p$0 = t;
        return flowKt__TransformKt$mapNotNull$1;
    }

    public final Object invoke(Object obj, Object obj2, Object obj3) {
        return ((FlowKt__TransformKt$mapNotNull$1) create((FlowCollector) obj, obj2, (Continuation) obj3)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object transformed;
        FlowCollector flowCollector;
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            flowCollector = this.p$;
            Object obj = this.p$0;
            Function2 function2 = this.$transformer;
            this.L$0 = flowCollector;
            this.label = 1;
            transformed = function2.invoke(obj, this);
            if (transformed == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            flowCollector = (FlowCollector) this.L$0;
            ResultKt.throwOnFailure(result);
            transformed = result;
        } else if (i == 2) {
            Object transformed2 = this.L$0;
            ResultKt.throwOnFailure(result);
            return Unit.INSTANCE;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (transformed == null) {
            return Unit.INSTANCE;
        }
        this.L$0 = transformed;
        this.label = 2;
        if (flowCollector.emit(transformed, this) == coroutine_suspended) {
            return coroutine_suspended;
        }
        return Unit.INSTANCE;
    }
}
