package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ErrorsKt$retry$2", f = "Errors.kt", i = {0, 0}, l = {60}, m = "invokeSuspend", n = {"retries", "fromDownstream"}, s = {"I$0", "L$1"})
/* compiled from: Errors.kt */
final class FlowKt__ErrorsKt$retry$2 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Function1 $predicate;
    final /* synthetic */ int $retries;
    final /* synthetic */ Flow $this_retry;
    int I$0;
    Object L$0;
    Object L$1;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__ErrorsKt$retry$2(Flow flow, int i, Function1 function1, Continuation continuation) {
        super(2, continuation);
        this.$this_retry = flow;
        this.$retries = i;
        this.$predicate = function1;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__ErrorsKt$retry$2 flowKt__ErrorsKt$retry$2 = new FlowKt__ErrorsKt$retry$2(this.$this_retry, this.$retries, this.$predicate, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__ErrorsKt$retry$2.p$ = (FlowCollector) obj;
        return flowKt__ErrorsKt$retry$2;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__ErrorsKt$retry$2) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: kotlin.jvm.internal.Ref$BooleanRef} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007c  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r11) {
        /*
            r10 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r10.label
            r2 = 1
            r3 = 0
            r4 = 0
            if (r1 == 0) goto L_0x002f
            if (r1 != r2) goto L_0x0027
            r1 = r4
            r5 = r3
            java.lang.Object r6 = r10.L$1
            r1 = r6
            kotlin.jvm.internal.Ref$BooleanRef r1 = (kotlin.jvm.internal.Ref.BooleanRef) r1
            int r5 = r10.I$0
            java.lang.Object r6 = r10.L$0
            kotlinx.coroutines.flow.FlowCollector r6 = (kotlinx.coroutines.flow.FlowCollector) r6
            kotlin.ResultKt.throwOnFailure(r11)     // Catch:{ Throwable -> 0x0020 }
            r0 = r11
            r11 = r10
            goto L_0x005c
        L_0x0020:
            r7 = move-exception
            r8 = r7
            r7 = r1
            r1 = r0
            r0 = r11
            r11 = r10
            goto L_0x0061
        L_0x0027:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x002f:
            kotlin.ResultKt.throwOnFailure(r11)
            kotlinx.coroutines.flow.FlowCollector r1 = r10.p$
            int r5 = r10.$retries
            r6 = r1
            r1 = r0
            r0 = r11
            r11 = r10
        L_0x003a:
            kotlin.jvm.internal.Ref$BooleanRef r7 = new kotlin.jvm.internal.Ref$BooleanRef
            r7.<init>()
            r7.element = r3
            kotlinx.coroutines.flow.Flow r8 = r11.$this_retry     // Catch:{ Throwable -> 0x0060 }
            kotlinx.coroutines.flow.FlowKt__ErrorsKt$retry$2$1 r9 = new kotlinx.coroutines.flow.FlowKt__ErrorsKt$retry$2$1     // Catch:{ Throwable -> 0x0060 }
            r9.<init>(r6, r7, r4)     // Catch:{ Throwable -> 0x0060 }
            kotlin.jvm.functions.Function2 r9 = (kotlin.jvm.functions.Function2) r9     // Catch:{ Throwable -> 0x0060 }
            r11.L$0 = r6     // Catch:{ Throwable -> 0x0060 }
            r11.I$0 = r5     // Catch:{ Throwable -> 0x0060 }
            r11.L$1 = r7     // Catch:{ Throwable -> 0x0060 }
            r11.label = r2     // Catch:{ Throwable -> 0x0060 }
            java.lang.Object r2 = kotlinx.coroutines.flow.FlowKt.collect(r8, r9, r11)     // Catch:{ Throwable -> 0x0060 }
            if (r2 != r1) goto L_0x005b
            return r1
        L_0x005b:
            r1 = r7
        L_0x005c:
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        L_0x0060:
            r8 = move-exception
        L_0x0061:
            boolean r9 = r7.element
            if (r9 != 0) goto L_0x007c
            kotlin.jvm.functions.Function1 r9 = r11.$predicate
            java.lang.Object r9 = r9.invoke(r8)
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x007b
            int r9 = r5 + -1
            if (r5 == 0) goto L_0x007a
            r5 = r9
            goto L_0x003a
        L_0x007a:
            r5 = r9
        L_0x007b:
            throw r8
        L_0x007c:
            throw r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__ErrorsKt$retry$2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
