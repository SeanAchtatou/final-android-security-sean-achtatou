package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.channels.BroadcastChannel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ChannelFlowKt$asFlow$1", f = "ChannelFlow.kt", i = {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2}, l = {68, 68, 27}, m = "invokeSuspend", n = {"subscription", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "subscription", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "subscription", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "value"}, s = {"L$1", "L$2", "L$4", "L$5", "L$6", "L$1", "L$2", "L$4", "L$5", "L$6", "L$1", "L$2", "L$4", "L$5", "L$6", "L$8", "L$9"})
/* compiled from: ChannelFlow.kt */
final class FlowKt__ChannelFlowKt$asFlow$1 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ BroadcastChannel $this_asFlow;
    Object L$0;
    Object L$1;
    Object L$2;
    Object L$3;
    Object L$4;
    Object L$5;
    Object L$6;
    Object L$7;
    Object L$8;
    Object L$9;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__ChannelFlowKt$asFlow$1(BroadcastChannel broadcastChannel, Continuation continuation) {
        super(2, continuation);
        this.$this_asFlow = broadcastChannel;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__ChannelFlowKt$asFlow$1 flowKt__ChannelFlowKt$asFlow$1 = new FlowKt__ChannelFlowKt$asFlow$1(this.$this_asFlow, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__ChannelFlowKt$asFlow$1.p$ = (FlowCollector) obj;
        return flowKt__ChannelFlowKt$asFlow$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__ChannelFlowKt$asFlow$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* Debug info: failed to restart local var, previous not found, register: 18 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v12, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v9, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v9, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v12, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v15, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v11, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v11, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v13, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x012c A[Catch:{ Throwable -> 0x0186 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0171 A[Catch:{ Throwable -> 0x0186 }] */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r19) {
        /*
            r18 = this;
            r1 = r18
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r2 = r1.label
            r3 = 3
            r4 = 2
            r5 = 1
            r6 = 0
            if (r2 == 0) goto L_0x00dd
            r7 = 0
            if (r2 == r5) goto L_0x0099
            if (r2 == r4) goto L_0x0060
            if (r2 != r3) goto L_0x0058
            r2 = r6
            r8 = r7
            r9 = r6
            r10 = r6
            r11 = r7
            r12 = r6
            r13 = r6
            r14 = r7
            r15 = r6
            java.lang.Object r2 = r1.L$9
            java.lang.Object r9 = r1.L$8
            java.lang.Object r3 = r1.L$7
            kotlinx.coroutines.channels.ChannelIterator r3 = (kotlinx.coroutines.channels.ChannelIterator) r3
            java.lang.Object r4 = r1.L$6
            kotlinx.coroutines.channels.ReceiveChannel r4 = (kotlinx.coroutines.channels.ReceiveChannel) r4
            java.lang.Object r10 = r1.L$5
            java.lang.Throwable r10 = (java.lang.Throwable) r10
            java.lang.Object r12 = r1.L$4
            kotlinx.coroutines.channels.ReceiveChannel r12 = (kotlinx.coroutines.channels.ReceiveChannel) r12
            java.lang.Object r13 = r1.L$3
            kotlinx.coroutines.flow.FlowKt__ChannelFlowKt$asFlow$1 r13 = (kotlinx.coroutines.flow.FlowKt__ChannelFlowKt$asFlow$1) r13
            java.lang.Object r5 = r1.L$2
            kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
            java.lang.Object r15 = r1.L$1
            r6 = r15
            kotlinx.coroutines.channels.ReceiveChannel r6 = (kotlinx.coroutines.channels.ReceiveChannel) r6
            java.lang.Object r15 = r1.L$0
            kotlinx.coroutines.flow.FlowCollector r15 = (kotlinx.coroutines.flow.FlowCollector) r15
            kotlin.ResultKt.throwOnFailure(r19)     // Catch:{ Throwable -> 0x00d5, all -> 0x00cd }
            r16 = r8
            r8 = r15
            r15 = r9
            r9 = r3
            r3 = r0
            r0 = r13
            r13 = r11
            r11 = r7
            r7 = r19
            r17 = r2
            r2 = r1
            r1 = r17
            goto L_0x017a
        L_0x0058:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0060:
            r2 = r6
            r3 = r7
            r4 = r6
            r5 = r6
            r14 = r7
            r8 = r6
            java.lang.Object r9 = r1.L$7
            kotlinx.coroutines.channels.ChannelIterator r9 = (kotlinx.coroutines.channels.ChannelIterator) r9
            java.lang.Object r10 = r1.L$6
            r2 = r10
            kotlinx.coroutines.channels.ReceiveChannel r2 = (kotlinx.coroutines.channels.ReceiveChannel) r2
            java.lang.Object r10 = r1.L$5
            java.lang.Throwable r10 = (java.lang.Throwable) r10
            java.lang.Object r4 = r1.L$4
            r12 = r4
            kotlinx.coroutines.channels.ReceiveChannel r12 = (kotlinx.coroutines.channels.ReceiveChannel) r12
            java.lang.Object r4 = r1.L$3
            kotlinx.coroutines.flow.FlowKt__ChannelFlowKt$asFlow$1 r4 = (kotlinx.coroutines.flow.FlowKt__ChannelFlowKt$asFlow$1) r4
            java.lang.Object r5 = r1.L$2
            kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
            java.lang.Object r8 = r1.L$1
            r6 = r8
            kotlinx.coroutines.channels.ReceiveChannel r6 = (kotlinx.coroutines.channels.ReceiveChannel) r6
            java.lang.Object r8 = r1.L$0
            kotlinx.coroutines.flow.FlowCollector r8 = (kotlinx.coroutines.flow.FlowCollector) r8
            kotlin.ResultKt.throwOnFailure(r19)     // Catch:{ Throwable -> 0x00d5, all -> 0x00cd }
            r13 = r19
            r15 = r13
            r11 = r7
            r7 = r5
            r5 = r1
            r17 = r9
            r9 = r3
            r3 = r17
            goto L_0x014b
        L_0x0099:
            r2 = r6
            r3 = r7
            r4 = r6
            r5 = r6
            r14 = r7
            r8 = r6
            java.lang.Object r9 = r1.L$7
            kotlinx.coroutines.channels.ChannelIterator r9 = (kotlinx.coroutines.channels.ChannelIterator) r9
            java.lang.Object r10 = r1.L$6
            r2 = r10
            kotlinx.coroutines.channels.ReceiveChannel r2 = (kotlinx.coroutines.channels.ReceiveChannel) r2
            java.lang.Object r10 = r1.L$5
            java.lang.Throwable r10 = (java.lang.Throwable) r10
            java.lang.Object r4 = r1.L$4
            r12 = r4
            kotlinx.coroutines.channels.ReceiveChannel r12 = (kotlinx.coroutines.channels.ReceiveChannel) r12
            java.lang.Object r4 = r1.L$3
            kotlinx.coroutines.flow.FlowKt__ChannelFlowKt$asFlow$1 r4 = (kotlinx.coroutines.flow.FlowKt__ChannelFlowKt$asFlow$1) r4
            java.lang.Object r5 = r1.L$2
            kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
            java.lang.Object r8 = r1.L$1
            r6 = r8
            kotlinx.coroutines.channels.ReceiveChannel r6 = (kotlinx.coroutines.channels.ReceiveChannel) r6
            java.lang.Object r8 = r1.L$0
            kotlinx.coroutines.flow.FlowCollector r8 = (kotlinx.coroutines.flow.FlowCollector) r8
            kotlin.ResultKt.throwOnFailure(r19)     // Catch:{ Throwable -> 0x00d5, all -> 0x00cd }
            r13 = r19
            r15 = r13
            r11 = r7
            r7 = r5
            r5 = r1
            goto L_0x0124
        L_0x00cd:
            r0 = move-exception
            r13 = r19
            r11 = r7
            r7 = r5
            r5 = r1
            goto L_0x01a7
        L_0x00d5:
            r0 = move-exception
            r13 = r19
            r11 = r7
            r7 = r5
            r5 = r1
            goto L_0x01a3
        L_0x00dd:
            kotlin.ResultKt.throwOnFailure(r19)
            kotlinx.coroutines.flow.FlowCollector r2 = r1.p$
            kotlinx.coroutines.channels.BroadcastChannel r3 = r1.$this_asFlow
            kotlinx.coroutines.channels.ReceiveChannel r3 = r3.openSubscription()
            r5 = r3
            r7 = 0
            r12 = r5
            r14 = 0
            r10 = r6
            java.lang.Throwable r10 = (java.lang.Throwable) r10
            r4 = r12
            r6 = 0
            kotlinx.coroutines.channels.ChannelIterator r8 = r4.iterator()     // Catch:{ Throwable -> 0x019b, all -> 0x0192 }
            r13 = r6
            r11 = r7
            r9 = r8
            r7 = r19
            r8 = r2
            r6 = r3
            r3 = r0
            r0 = r1
            r2 = r0
        L_0x0100:
            r2.L$0 = r8     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            r2.L$1 = r6     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            r2.L$2 = r5     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            r2.L$3 = r0     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            r2.L$4 = r12     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            r2.L$5 = r10     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            r2.L$6 = r4     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            r2.L$7 = r9     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            r15 = 1
            r2.label = r15     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            java.lang.Object r15 = r9.hasNext(r0)     // Catch:{ Throwable -> 0x018d, all -> 0x0188 }
            if (r15 != r3) goto L_0x011a
            return r3
        L_0x011a:
            r17 = r4
            r4 = r0
            r0 = r3
            r3 = r13
            r13 = r7
            r7 = r5
            r5 = r2
            r2 = r17
        L_0x0124:
            java.lang.Boolean r15 = (java.lang.Boolean) r15     // Catch:{ Throwable -> 0x0186 }
            boolean r15 = r15.booleanValue()     // Catch:{ Throwable -> 0x0186 }
            if (r15 == 0) goto L_0x017d
            r5.L$0 = r8     // Catch:{ Throwable -> 0x0186 }
            r5.L$1 = r6     // Catch:{ Throwable -> 0x0186 }
            r5.L$2 = r7     // Catch:{ Throwable -> 0x0186 }
            r5.L$3 = r4     // Catch:{ Throwable -> 0x0186 }
            r5.L$4 = r12     // Catch:{ Throwable -> 0x0186 }
            r5.L$5 = r10     // Catch:{ Throwable -> 0x0186 }
            r5.L$6 = r2     // Catch:{ Throwable -> 0x0186 }
            r5.L$7 = r9     // Catch:{ Throwable -> 0x0186 }
            r15 = 2
            r5.label = r15     // Catch:{ Throwable -> 0x0186 }
            java.lang.Object r15 = r9.next(r4)     // Catch:{ Throwable -> 0x0186 }
            if (r15 != r0) goto L_0x0146
            return r0
        L_0x0146:
            r17 = r9
            r9 = r3
            r3 = r17
        L_0x014b:
            r19 = r15
            r16 = 0
            r5.L$0 = r8     // Catch:{ Throwable -> 0x0186 }
            r5.L$1 = r6     // Catch:{ Throwable -> 0x0186 }
            r5.L$2 = r7     // Catch:{ Throwable -> 0x0186 }
            r5.L$3 = r4     // Catch:{ Throwable -> 0x0186 }
            r5.L$4 = r12     // Catch:{ Throwable -> 0x0186 }
            r5.L$5 = r10     // Catch:{ Throwable -> 0x0186 }
            r5.L$6 = r2     // Catch:{ Throwable -> 0x0186 }
            r5.L$7 = r3     // Catch:{ Throwable -> 0x0186 }
            r5.L$8 = r15     // Catch:{ Throwable -> 0x0186 }
            r1 = r19
            r5.L$9 = r1     // Catch:{ Throwable -> 0x0186 }
            r19 = r2
            r2 = 3
            r5.label = r2     // Catch:{ Throwable -> 0x0186 }
            java.lang.Object r2 = r8.emit(r1, r5)     // Catch:{ Throwable -> 0x0186 }
            if (r2 != r0) goto L_0x0171
            return r0
        L_0x0171:
            r2 = r5
            r5 = r7
            r7 = r13
            r13 = r9
            r9 = r3
            r3 = r0
            r0 = r4
            r4 = r19
        L_0x017a:
            r1 = r18
            goto L_0x0100
        L_0x017d:
            kotlin.Unit r0 = kotlin.Unit.INSTANCE     // Catch:{ Throwable -> 0x0186 }
            kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r12, r10)
            kotlin.Unit r0 = kotlin.Unit.INSTANCE
            return r0
        L_0x0186:
            r0 = move-exception
            goto L_0x01a3
        L_0x0188:
            r0 = move-exception
            r13 = r7
            r7 = r5
            r5 = r2
            goto L_0x01a7
        L_0x018d:
            r0 = move-exception
            r13 = r7
            r7 = r5
            r5 = r2
            goto L_0x01a3
        L_0x0192:
            r0 = move-exception
            r13 = r19
            r6 = r3
            r11 = r7
            r7 = r5
            r5 = r18
            goto L_0x01a7
        L_0x019b:
            r0 = move-exception
            r13 = r19
            r6 = r3
            r11 = r7
            r7 = r5
            r5 = r18
        L_0x01a3:
            r10 = r0
            throw r0     // Catch:{ all -> 0x01a6 }
        L_0x01a6:
            r0 = move-exception
        L_0x01a7:
            kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r12, r10)
            throw r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__ChannelFlowKt$asFlow$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
