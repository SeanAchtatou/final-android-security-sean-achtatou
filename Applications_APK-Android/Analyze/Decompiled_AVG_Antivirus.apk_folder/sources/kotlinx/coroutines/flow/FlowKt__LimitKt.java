package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.FlowPreview;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0000\n\u0002\b\u0004\u001a&\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u001aJ\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\"\u0010\u0006\u001a\u001e\b\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0012\u0004\u0018\u00010\n0\u0007H\u0007ø\u0001\u0000¢\u0006\u0002\u0010\u000b\u001a&\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u001aJ\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\"\u0010\u0006\u001a\u001e\b\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0012\u0004\u0018\u00010\n0\u0007H\u0007ø\u0001\u0000¢\u0006\u0002\u0010\u000b\u0002\u0004\n\u0002\b\u0019¨\u0006\u000e"}, d2 = {"drop", "Lkotlinx/coroutines/flow/Flow;", "T", "count", "", "dropWhile", "predicate", "Lkotlin/Function2;", "Lkotlin/coroutines/Continuation;", "", "", "(Lkotlinx/coroutines/flow/Flow;Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;", "take", "takeWhile", "kotlinx-coroutines-core"}, k = 5, mv = {1, 1, 15}, xs = "kotlinx/coroutines/flow/FlowKt")
/* compiled from: Limit.kt */
final /* synthetic */ class FlowKt__LimitKt {
    @NotNull
    @FlowPreview
    public static final <T> Flow<T> drop(@NotNull Flow<? extends T> $this$drop, int count) {
        Intrinsics.checkParameterIsNotNull($this$drop, "$this$drop");
        if (count >= 0) {
            return FlowKt.unsafeFlow(new FlowKt__LimitKt$drop$2($this$drop, count, null));
        }
        throw new IllegalArgumentException(("Drop count should be non-negative, but had " + count).toString());
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> dropWhile(@NotNull Flow<? extends T> $this$dropWhile, @NotNull Function2<? super T, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        Intrinsics.checkParameterIsNotNull($this$dropWhile, "$this$dropWhile");
        Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        return FlowKt.unsafeFlow(new FlowKt__LimitKt$dropWhile$1($this$dropWhile, predicate, null));
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> take(@NotNull Flow<? extends T> $this$take, int count) {
        Intrinsics.checkParameterIsNotNull($this$take, "$this$take");
        if (count > 0) {
            return FlowKt.unsafeFlow(new FlowKt__LimitKt$take$2($this$take, count, null));
        }
        throw new IllegalArgumentException(("Requested element count " + count + " should be positive").toString());
    }

    @NotNull
    @FlowPreview
    public static final <T> Flow<T> takeWhile(@NotNull Flow<? extends T> $this$takeWhile, @NotNull Function2<? super T, ? super Continuation<? super Boolean>, ? extends Object> predicate) {
        Intrinsics.checkParameterIsNotNull($this$takeWhile, "$this$takeWhile");
        Intrinsics.checkParameterIsNotNull(predicate, "predicate");
        return FlowKt.unsafeFlow(new FlowKt__LimitKt$takeWhile$1($this$takeWhile, predicate, null));
    }
}
