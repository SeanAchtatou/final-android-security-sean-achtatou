package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.Job;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003*\b\u0012\u0004\u0012\u0002H\u00030\u0004H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "T", "R", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ContextKt$flowWith$1", f = "Context.kt", i = {0, 0}, l = {109}, m = "invokeSuspend", n = {"originalContext", "prepared"}, s = {"L$0", "L$1"})
/* compiled from: Context.kt */
final class FlowKt__ContextKt$flowWith$1 extends SuspendLambda implements Function2<FlowCollector<? super R>, Continuation<? super Unit>, Object> {
    final /* synthetic */ int $bufferSize;
    final /* synthetic */ Function1 $builder;
    final /* synthetic */ CoroutineContext $flowContext;
    final /* synthetic */ Flow $source;
    Object L$0;
    Object L$1;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__ContextKt$flowWith$1(Flow flow, int i, Function1 function1, CoroutineContext coroutineContext, Continuation continuation) {
        super(2, continuation);
        this.$source = flow;
        this.$bufferSize = i;
        this.$builder = function1;
        this.$flowContext = coroutineContext;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__ContextKt$flowWith$1 flowKt__ContextKt$flowWith$1 = new FlowKt__ContextKt$flowWith$1(this.$source, this.$bufferSize, this.$builder, this.$flowContext, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__ContextKt$flowWith$1.p$ = (FlowCollector) obj;
        return flowKt__ContextKt$flowWith$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__ContextKt$flowWith$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            final FlowCollector flowCollector = this.p$;
            CoroutineContext originalContext = getContext().minusKey(Job.Key);
            Flow prepared = FlowKt.flowOn(this.$source, originalContext, this.$bufferSize);
            this.L$0 = originalContext;
            this.L$1 = prepared;
            this.label = 1;
            if (FlowKt.collect(FlowKt.flowOn((Flow) this.$builder.invoke(prepared), this.$flowContext, this.$bufferSize), new AnonymousClass1(null), this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            Flow prepared2 = (Flow) this.L$1;
            CoroutineContext originalContext2 = (CoroutineContext) this.L$0;
            ResultKt.throwOnFailure(result);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u00032\u0006\u0010\u0004\u001a\u0002H\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", "T", "R", "value", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ContextKt$flowWith$1$1", f = "Context.kt", i = {}, l = {110}, m = "invokeSuspend", n = {}, s = {})
    /* renamed from: kotlinx.coroutines.flow.FlowKt__ContextKt$flowWith$1$1  reason: invalid class name */
    /* compiled from: Context.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<R, Continuation<? super Unit>, Object> {
        int label;
        private Object p$0;

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(flowCollector, continuation);
            r0.p$0 = obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        @Nullable
        public final Object invokeSuspend(@NotNull Object result) {
            Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                ResultKt.throwOnFailure(result);
                Object obj = this.p$0;
                FlowCollector flowCollector = flowCollector;
                this.label = 1;
                if (flowCollector.emit(obj, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                ResultKt.throwOnFailure(result);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Unit.INSTANCE;
        }
    }
}
