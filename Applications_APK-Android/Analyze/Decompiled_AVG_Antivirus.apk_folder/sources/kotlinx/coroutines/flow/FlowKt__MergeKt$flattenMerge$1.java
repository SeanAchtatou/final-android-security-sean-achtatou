package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "Lkotlinx/coroutines/flow/Flow;", "T", "it", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__MergeKt$flattenMerge$1", f = "Merge.kt", i = {}, l = {}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Merge.kt */
final class FlowKt__MergeKt$flattenMerge$1 extends SuspendLambda implements Function2<Flow<? extends T>, Continuation<? super Flow<? extends T>>, Object> {
    int label;
    private Flow p$0;

    FlowKt__MergeKt$flattenMerge$1(Continuation continuation) {
        super(2, continuation);
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__MergeKt$flattenMerge$1 flowKt__MergeKt$flattenMerge$1 = new FlowKt__MergeKt$flattenMerge$1(continuation);
        Flow flow = (Flow) obj;
        flowKt__MergeKt$flattenMerge$1.p$0 = (Flow) obj;
        return flowKt__MergeKt$flattenMerge$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__MergeKt$flattenMerge$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        IntrinsicsKt.getCOROUTINE_SUSPENDED();
        if (this.label == 0) {
            ResultKt.throwOnFailure(result);
            return this.p$0;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
