package kotlinx.coroutines.flow.internal;

import kotlin.Metadata;
import kotlin.PublishedApi;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineId;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.flow.FlowCollector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@PublishedApi
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0001\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\u00060\u0003j\u0002`\u0004B\u001b\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0019\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00028\u0000H@ø\u0001\u0000¢\u0006\u0002\u0010\fR\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006\r"}, d2 = {"Lkotlinx/coroutines/flow/internal/SafeCollector;", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "", "Lkotlinx/coroutines/internal/SynchronizedObject;", "collector", "collectContext", "Lkotlin/coroutines/CoroutineContext;", "(Lkotlinx/coroutines/flow/FlowCollector;Lkotlin/coroutines/CoroutineContext;)V", "emit", "", "value", "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
/* compiled from: SafeCollector.kt */
public final class SafeCollector<T> implements FlowCollector<T> {
    private final CoroutineContext collectContext;
    private final FlowCollector<T> collector;

    public SafeCollector(@NotNull FlowCollector<? super T> collector2, @NotNull CoroutineContext collectContext2) {
        Intrinsics.checkParameterIsNotNull(collector2, "collector");
        Intrinsics.checkParameterIsNotNull(collectContext2, "collectContext");
        this.collector = collector2;
        this.collectContext = collectContext2.minusKey(Job.Key).minusKey(CoroutineId.Key);
    }

    /* JADX INFO: Multiple debug info for r0v2 kotlin.coroutines.CoroutineContext: [D('emitContext' kotlin.coroutines.CoroutineContext), D('$this$minusId$iv' kotlin.coroutines.CoroutineContext)] */
    @Nullable
    public Object emit(T value, @NotNull Continuation<? super Unit> $completion) {
        CoroutineContext $this$minusId$iv = $completion.getContext().minusKey(Job.Key).minusKey(CoroutineId.Key);
        if (!(!Intrinsics.areEqual($this$minusId$iv, this.collectContext))) {
            return this.collector.emit(value, $completion);
        }
        throw new IllegalStateException(("Flow invariant is violated: flow was collected in " + this.collectContext + ", but emission happened in " + $this$minusId$iv + ". " + "Please refer to 'flow' documentation or use 'flowOn' instead").toString());
    }
}
