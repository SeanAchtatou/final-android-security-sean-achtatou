package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "T", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ErrorsKt$collectSafely$1", f = "Errors.kt", i = {0, 1, 1}, l = {82, 92}, m = "invokeSuspend", n = {"fromDownstream", "fromDownstream", "e"}, s = {"L$1", "L$0", "L$1"})
/* compiled from: Errors.kt */
final class FlowKt__ErrorsKt$collectSafely$1 extends SuspendLambda implements Function2<FlowCollector<? super T>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Function3 $onException;
    final /* synthetic */ Flow $this_collectSafely;
    Object L$0;
    Object L$1;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__ErrorsKt$collectSafely$1(Flow flow, Function3 function3, Continuation continuation) {
        super(2, continuation);
        this.$this_collectSafely = flow;
        this.$onException = function3;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__ErrorsKt$collectSafely$1 flowKt__ErrorsKt$collectSafely$1 = new FlowKt__ErrorsKt$collectSafely$1(this.$this_collectSafely, this.$onException, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__ErrorsKt$collectSafely$1.p$ = (FlowCollector) obj;
        return flowKt__ErrorsKt$collectSafely$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__ErrorsKt$collectSafely$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: kotlin.jvm.internal.Ref$BooleanRef} */
    /* JADX WARNING: Multi-variable type inference failed */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r8.label
            r2 = 2
            r3 = 1
            r4 = 0
            if (r1 == 0) goto L_0x0037
            if (r1 == r3) goto L_0x0027
            if (r1 != r2) goto L_0x001f
            r0 = r4
            r1 = r4
            java.lang.Object r2 = r8.L$1
            r0 = r2
            java.lang.Throwable r0 = (java.lang.Throwable) r0
            java.lang.Object r2 = r8.L$0
            r1 = r2
            kotlin.jvm.internal.Ref$BooleanRef r1 = (kotlin.jvm.internal.Ref.BooleanRef) r1
            kotlin.ResultKt.throwOnFailure(r9)
            goto L_0x0074
        L_0x001f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0027:
            r1 = r4
            java.lang.Object r3 = r8.L$1
            r1 = r3
            kotlin.jvm.internal.Ref$BooleanRef r1 = (kotlin.jvm.internal.Ref.BooleanRef) r1
            java.lang.Object r3 = r8.L$0
            kotlinx.coroutines.flow.FlowCollector r3 = (kotlinx.coroutines.flow.FlowCollector) r3
            kotlin.ResultKt.throwOnFailure(r9)     // Catch:{ Throwable -> 0x0035 }
            goto L_0x005c
        L_0x0035:
            r4 = move-exception
            goto L_0x0060
        L_0x0037:
            kotlin.ResultKt.throwOnFailure(r9)
            kotlinx.coroutines.flow.FlowCollector r1 = r8.p$
            kotlin.jvm.internal.Ref$BooleanRef r5 = new kotlin.jvm.internal.Ref$BooleanRef
            r5.<init>()
            r6 = 0
            r5.element = r6
            kotlinx.coroutines.flow.Flow r6 = r8.$this_collectSafely     // Catch:{ Throwable -> 0x005d }
            kotlinx.coroutines.flow.FlowKt__ErrorsKt$collectSafely$1$1 r7 = new kotlinx.coroutines.flow.FlowKt__ErrorsKt$collectSafely$1$1     // Catch:{ Throwable -> 0x005d }
            r7.<init>(r1, r5, r4)     // Catch:{ Throwable -> 0x005d }
            kotlin.jvm.functions.Function2 r7 = (kotlin.jvm.functions.Function2) r7     // Catch:{ Throwable -> 0x005d }
            r8.L$0 = r1     // Catch:{ Throwable -> 0x005d }
            r8.L$1 = r5     // Catch:{ Throwable -> 0x005d }
            r8.label = r3     // Catch:{ Throwable -> 0x005d }
            java.lang.Object r1 = kotlinx.coroutines.flow.FlowKt.collect(r6, r7, r8)     // Catch:{ Throwable -> 0x005d }
            if (r1 != r0) goto L_0x005b
            return r0
        L_0x005b:
            r1 = r5
        L_0x005c:
            goto L_0x0075
        L_0x005d:
            r4 = move-exception
            r3 = r1
            r1 = r5
        L_0x0060:
            boolean r5 = r1.element
            if (r5 != 0) goto L_0x0078
            kotlin.jvm.functions.Function3 r5 = r8.$onException
            r8.L$0 = r1
            r8.L$1 = r4
            r8.label = r2
            java.lang.Object r2 = r5.invoke(r3, r4, r8)
            if (r2 != r0) goto L_0x0073
            return r0
        L_0x0073:
            r0 = r4
        L_0x0074:
        L_0x0075:
            kotlin.Unit r0 = kotlin.Unit.INSTANCE
            return r0
        L_0x0078:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__ErrorsKt$collectSafely$1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
