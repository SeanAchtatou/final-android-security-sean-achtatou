package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineScopeKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00040\u0005H@ø\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"<anonymous>", "", "T1", "T2", "R", "Lkotlinx/coroutines/flow/FlowCollector;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1", f = "Zip.kt", i = {}, l = {122}, m = "invokeSuspend", n = {}, s = {})
/* compiled from: Zip.kt */
final class FlowKt__ZipKt$zip$1 extends SuspendLambda implements Function2<FlowCollector<? super R>, Continuation<? super Unit>, Object> {
    final /* synthetic */ Flow $other;
    final /* synthetic */ Flow $this_zip;
    final /* synthetic */ Function3 $transform;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__ZipKt$zip$1(Flow flow, Flow flow2, Function3 function3, Continuation continuation) {
        super(2, continuation);
        this.$this_zip = flow;
        this.$other = flow2;
        this.$transform = function3;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__ZipKt$zip$1 flowKt__ZipKt$zip$1 = new FlowKt__ZipKt$zip$1(this.$this_zip, this.$other, this.$transform, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__ZipKt$zip$1.p$ = (FlowCollector) obj;
        return flowKt__ZipKt$zip$1;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__ZipKt$zip$1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0003\"\u0004\b\u0002\u0010\u0004*\u00020\u0005H@ø\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"<anonymous>", "", "T1", "T2", "R", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
    @DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1", f = "Zip.kt", i = {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5}, l = {164, 164, 141, 144, 145, 145}, m = "invokeSuspend", n = {"first", "second", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "first", "second", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "first", "second", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "value", "first", "second", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "value", "first", "second", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "value", "secondValue", "first", "second", "otherIterator", "$this$consumeEach$iv", "$this$consume$iv$iv", "cause$iv$iv", "$this$consume$iv", "e$iv", "value", "secondValue"}, s = {"L$0", "L$1", "L$2", "L$3", "L$5", "L$6", "L$7", "L$0", "L$1", "L$2", "L$3", "L$5", "L$6", "L$7", "L$0", "L$1", "L$2", "L$3", "L$5", "L$6", "L$7", "L$9", "L$10", "L$0", "L$1", "L$2", "L$3", "L$5", "L$6", "L$7", "L$9", "L$10", "L$0", "L$1", "L$2", "L$3", "L$5", "L$6", "L$7", "L$9", "L$10", "L$11", "L$0", "L$1", "L$2", "L$3", "L$5", "L$6", "L$7", "L$9", "L$10", "L$11"})
    /* renamed from: kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1  reason: invalid class name */
    /* compiled from: Zip.kt */
    static final class AnonymousClass1 extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        Object L$0;
        Object L$1;
        Object L$10;
        Object L$11;
        Object L$12;
        Object L$2;
        Object L$3;
        Object L$4;
        Object L$5;
        Object L$6;
        Object L$7;
        Object L$8;
        Object L$9;
        int label;
        private CoroutineScope p$;
        final /* synthetic */ FlowKt__ZipKt$zip$1 this$0;

        {
            this.this$0 = r1;
        }

        @NotNull
        public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
            Intrinsics.checkParameterIsNotNull(continuation, "completion");
            AnonymousClass1 r0 = new AnonymousClass1(this.this$0, flowCollector, continuation);
            CoroutineScope coroutineScope = (CoroutineScope) obj;
            r0.p$ = (CoroutineScope) obj;
            return r0;
        }

        public final Object invoke(Object obj, Object obj2) {
            return ((AnonymousClass1) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
        }

        /* Debug info: failed to restart local var, previous not found, register: 25 */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v17, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v57, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v18, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v19, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v26, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v13, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v24, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v13, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v25, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v19, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v26, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v17, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v21, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v60, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v22, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v23, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v28, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v15, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v29, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v15, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v30, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v21, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v31, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v19, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v19, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v19, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v20, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v19, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v21, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v22, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v66, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v22, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v67, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v30, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v68, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v34, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v69, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v23, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v25, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v23, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v26, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v23, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v27, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v26, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v73, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v26, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v74, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v34, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v75, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v37, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v76, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v26, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v39, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v30, resolved type: java.lang.Throwable} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v84, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v38, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v85, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v40, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v86, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v24, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v87, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v32, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v91, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v29, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v92, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v37, resolved type: kotlinx.coroutines.channels.ChannelIterator} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v93, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v34, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v94, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v33, resolved type: kotlinx.coroutines.channels.ReceiveChannel} */
        /* JADX INFO: Multiple debug info for r0v32 java.lang.Object: [D('$this$consume$iv' kotlinx.coroutines.channels.ReceiveChannel), D('value' java.lang.Object)] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x02de A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x02df  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x02f5 A[Catch:{ Throwable -> 0x048a, all -> 0x0484 }] */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x033b  */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x0383 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:82:0x03c7 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x03c8  */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x0403  */
        @org.jetbrains.annotations.Nullable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r26) {
            /*
                r25 = this;
                r1 = r25
                java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
                int r2 = r1.label
                r4 = 0
                r5 = 0
                switch(r2) {
                    case 0: goto L_0x027b;
                    case 1: goto L_0x021f;
                    case 2: goto L_0x01d6;
                    case 3: goto L_0x0161;
                    case 4: goto L_0x0102;
                    case 5: goto L_0x0082;
                    case 6: goto L_0x0015;
                    default: goto L_0x000d;
                }
            L_0x000d:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0015:
                r2 = r5
                r6 = r5
                r7 = r4
                r8 = r5
                r9 = r5
                r10 = r4
                r11 = r5
                r12 = r5
                r13 = r4
                r14 = r5
                r15 = r5
                r16 = r5
                r17 = r5
                java.lang.Object r2 = r1.L$11
                java.lang.Object r6 = r1.L$10
                java.lang.Object r8 = r1.L$9
                java.lang.Object r3 = r1.L$8
                kotlinx.coroutines.channels.ChannelIterator r3 = (kotlinx.coroutines.channels.ChannelIterator) r3
                java.lang.Object r5 = r1.L$7
                kotlinx.coroutines.channels.ReceiveChannel r5 = (kotlinx.coroutines.channels.ReceiveChannel) r5
                java.lang.Object r9 = r1.L$6
                java.lang.Throwable r9 = (java.lang.Throwable) r9
                java.lang.Object r11 = r1.L$5
                kotlinx.coroutines.channels.ReceiveChannel r11 = (kotlinx.coroutines.channels.ReceiveChannel) r11
                java.lang.Object r12 = r1.L$4
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1 r12 = (kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1.AnonymousClass1) r12
                r18 = r0
                java.lang.Object r0 = r1.L$3
                r14 = r0
                kotlinx.coroutines.channels.ReceiveChannel r14 = (kotlinx.coroutines.channels.ReceiveChannel) r14
                java.lang.Object r0 = r1.L$2
                r15 = r0
                kotlinx.coroutines.channels.ChannelIterator r15 = (kotlinx.coroutines.channels.ChannelIterator) r15
                java.lang.Object r0 = r1.L$1
                r16 = r0
                kotlinx.coroutines.channels.ReceiveChannel r16 = (kotlinx.coroutines.channels.ReceiveChannel) r16
                java.lang.Object r0 = r1.L$0
                r17 = r0
                kotlinx.coroutines.channels.ReceiveChannel r17 = (kotlinx.coroutines.channels.ReceiveChannel) r17
                kotlin.ResultKt.throwOnFailure(r26)     // Catch:{ Throwable -> 0x007b, all -> 0x0071 }
                r20 = r4
                r4 = r6
                r0 = r11
                r6 = r12
                r19 = r13
                r11 = r14
                r12 = r15
                r14 = r16
                r15 = r17
                r13 = r18
                r16 = r26
                r18 = r7
                r7 = r8
                r17 = r10
                goto L_0x0408
            L_0x0071:
                r0 = move-exception
                r12 = r4
                r7 = r16
                r2 = r17
                r16 = r26
                goto L_0x04c0
            L_0x007b:
                r0 = move-exception
                r12 = r16
                r16 = r26
                goto L_0x04b8
            L_0x0082:
                r18 = r0
                r2 = 0
                r0 = r2
                r3 = r2
                r5 = r4
                r6 = r2
                r7 = r2
                r8 = r4
                r9 = r2
                r10 = r2
                r13 = r4
                r11 = r2
                r12 = r2
                r14 = r2
                r15 = r2
                java.lang.Object r2 = r1.L$12
                kotlinx.coroutines.flow.FlowCollector r2 = (kotlinx.coroutines.flow.FlowCollector) r2
                java.lang.Object r0 = r1.L$11
                java.lang.Object r3 = r1.L$10
                java.lang.Object r6 = r1.L$9
                r16 = r0
                java.lang.Object r0 = r1.L$8
                kotlinx.coroutines.channels.ChannelIterator r0 = (kotlinx.coroutines.channels.ChannelIterator) r0
                r17 = r0
                java.lang.Object r0 = r1.L$7
                kotlinx.coroutines.channels.ReceiveChannel r0 = (kotlinx.coroutines.channels.ReceiveChannel) r0
                java.lang.Object r7 = r1.L$6
                r9 = r7
                java.lang.Throwable r9 = (java.lang.Throwable) r9
                java.lang.Object r7 = r1.L$5
                kotlinx.coroutines.channels.ReceiveChannel r7 = (kotlinx.coroutines.channels.ReceiveChannel) r7
                java.lang.Object r10 = r1.L$4
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1 r10 = (kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1.AnonymousClass1) r10
                r19 = r0
                java.lang.Object r0 = r1.L$3
                r11 = r0
                kotlinx.coroutines.channels.ReceiveChannel r11 = (kotlinx.coroutines.channels.ReceiveChannel) r11
                java.lang.Object r0 = r1.L$2
                r12 = r0
                kotlinx.coroutines.channels.ChannelIterator r12 = (kotlinx.coroutines.channels.ChannelIterator) r12
                java.lang.Object r0 = r1.L$1
                r14 = r0
                kotlinx.coroutines.channels.ReceiveChannel r14 = (kotlinx.coroutines.channels.ReceiveChannel) r14
                java.lang.Object r0 = r1.L$0
                r15 = r0
                kotlinx.coroutines.channels.ReceiveChannel r15 = (kotlinx.coroutines.channels.ReceiveChannel) r15
                kotlin.ResultKt.throwOnFailure(r26)     // Catch:{ Throwable -> 0x00f7, all -> 0x00e9 }
                r0 = r26
                r20 = r4
                r4 = r3
                r3 = r17
                r17 = r8
                r8 = r6
                r6 = r10
                r10 = r16
                r16 = r0
                r23 = r18
                r18 = r5
                r5 = r19
                r19 = r13
                r13 = r23
                goto L_0x03df
            L_0x00e9:
                r0 = move-exception
                r16 = r26
                r2 = r15
                r15 = r12
                r12 = r4
                r23 = r11
                r11 = r7
                r7 = r14
                r14 = r23
                goto L_0x04c0
            L_0x00f7:
                r0 = move-exception
                r16 = r26
                r17 = r15
                r15 = r12
                r12 = r14
                r14 = r11
                r11 = r7
                goto L_0x04b8
            L_0x0102:
                r18 = r0
                r2 = 0
                r0 = r2
                r3 = r4
                r5 = r2
                r6 = r2
                r7 = r4
                r8 = r2
                r9 = r2
                r13 = r4
                r10 = r2
                r11 = r2
                r12 = r2
                r14 = r2
                java.lang.Object r0 = r1.L$10
                java.lang.Object r2 = r1.L$9
                java.lang.Object r5 = r1.L$8
                kotlinx.coroutines.channels.ChannelIterator r5 = (kotlinx.coroutines.channels.ChannelIterator) r5
                java.lang.Object r15 = r1.L$7
                r6 = r15
                kotlinx.coroutines.channels.ReceiveChannel r6 = (kotlinx.coroutines.channels.ReceiveChannel) r6
                java.lang.Object r15 = r1.L$6
                r8 = r15
                java.lang.Throwable r8 = (java.lang.Throwable) r8
                java.lang.Object r15 = r1.L$5
                r9 = r15
                kotlinx.coroutines.channels.ReceiveChannel r9 = (kotlinx.coroutines.channels.ReceiveChannel) r9
                java.lang.Object r15 = r1.L$4
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1 r15 = (kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1.AnonymousClass1) r15
                r16 = r0
                java.lang.Object r0 = r1.L$3
                r10 = r0
                kotlinx.coroutines.channels.ReceiveChannel r10 = (kotlinx.coroutines.channels.ReceiveChannel) r10
                java.lang.Object r0 = r1.L$2
                r11 = r0
                kotlinx.coroutines.channels.ChannelIterator r11 = (kotlinx.coroutines.channels.ChannelIterator) r11
                java.lang.Object r0 = r1.L$1
                r12 = r0
                kotlinx.coroutines.channels.ReceiveChannel r12 = (kotlinx.coroutines.channels.ReceiveChannel) r12
                java.lang.Object r0 = r1.L$0
                r17 = r0
                kotlinx.coroutines.channels.ReceiveChannel r17 = (kotlinx.coroutines.channels.ReceiveChannel) r17
                kotlin.ResultKt.throwOnFailure(r26)     // Catch:{ Throwable -> 0x01cd, all -> 0x01c0 }
                r0 = r26
                r14 = r10
                r10 = r18
                r18 = r4
                r4 = r5
                r5 = r17
                r17 = r7
                r7 = r2
                r2 = r16
                r16 = r0
                r23 = r8
                r8 = r6
                r6 = r15
                r15 = r11
                r11 = r9
                r9 = r23
                goto L_0x0384
            L_0x0161:
                r18 = r0
                r2 = 0
                r0 = r2
                r3 = r4
                r5 = r2
                r6 = r2
                r7 = r4
                r8 = r2
                r9 = r2
                r13 = r4
                r10 = r2
                r11 = r2
                r12 = r2
                r14 = r2
                java.lang.Object r0 = r1.L$10
                java.lang.Object r2 = r1.L$9
                java.lang.Object r5 = r1.L$8
                kotlinx.coroutines.channels.ChannelIterator r5 = (kotlinx.coroutines.channels.ChannelIterator) r5
                java.lang.Object r15 = r1.L$7
                r6 = r15
                kotlinx.coroutines.channels.ReceiveChannel r6 = (kotlinx.coroutines.channels.ReceiveChannel) r6
                java.lang.Object r15 = r1.L$6
                r8 = r15
                java.lang.Throwable r8 = (java.lang.Throwable) r8
                java.lang.Object r15 = r1.L$5
                r9 = r15
                kotlinx.coroutines.channels.ReceiveChannel r9 = (kotlinx.coroutines.channels.ReceiveChannel) r9
                java.lang.Object r15 = r1.L$4
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1 r15 = (kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1.AnonymousClass1) r15
                r16 = r0
                java.lang.Object r0 = r1.L$3
                r10 = r0
                kotlinx.coroutines.channels.ReceiveChannel r10 = (kotlinx.coroutines.channels.ReceiveChannel) r10
                java.lang.Object r0 = r1.L$2
                r11 = r0
                kotlinx.coroutines.channels.ChannelIterator r11 = (kotlinx.coroutines.channels.ChannelIterator) r11
                java.lang.Object r0 = r1.L$1
                r12 = r0
                kotlinx.coroutines.channels.ReceiveChannel r12 = (kotlinx.coroutines.channels.ReceiveChannel) r12
                java.lang.Object r0 = r1.L$0
                r17 = r0
                kotlinx.coroutines.channels.ReceiveChannel r17 = (kotlinx.coroutines.channels.ReceiveChannel) r17
                kotlin.ResultKt.throwOnFailure(r26)     // Catch:{ Throwable -> 0x01cd, all -> 0x01c0 }
                r0 = r26
                r14 = r10
                r10 = r18
                r18 = r4
                r4 = r5
                r5 = r17
                r17 = r7
                r7 = r2
                r2 = r16
                r16 = r0
                r23 = r8
                r8 = r6
                r6 = r15
                r15 = r11
                r11 = r9
                r9 = r23
                goto L_0x0350
            L_0x01c0:
                r0 = move-exception
                r16 = r26
                r14 = r10
                r15 = r11
                r7 = r12
                r2 = r17
                r12 = r4
                r11 = r9
                r9 = r8
                goto L_0x04c0
            L_0x01cd:
                r0 = move-exception
                r16 = r26
                r14 = r10
                r15 = r11
                r11 = r9
                r9 = r8
                goto L_0x04b8
            L_0x01d6:
                r18 = r0
                r2 = 0
                r0 = r2
                r3 = r4
                r5 = r2
                r6 = r2
                r13 = r4
                r7 = r2
                r8 = r2
                r9 = r2
                r10 = r2
                java.lang.Object r2 = r1.L$8
                kotlinx.coroutines.channels.ChannelIterator r2 = (kotlinx.coroutines.channels.ChannelIterator) r2
                java.lang.Object r11 = r1.L$7
                r0 = r11
                kotlinx.coroutines.channels.ReceiveChannel r0 = (kotlinx.coroutines.channels.ReceiveChannel) r0
                java.lang.Object r11 = r1.L$6
                r5 = r11
                java.lang.Throwable r5 = (java.lang.Throwable) r5
                java.lang.Object r11 = r1.L$5
                kotlinx.coroutines.channels.ReceiveChannel r11 = (kotlinx.coroutines.channels.ReceiveChannel) r11
                java.lang.Object r6 = r1.L$4
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1 r6 = (kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1.AnonymousClass1) r6
                java.lang.Object r12 = r1.L$3
                r14 = r12
                kotlinx.coroutines.channels.ReceiveChannel r14 = (kotlinx.coroutines.channels.ReceiveChannel) r14
                java.lang.Object r7 = r1.L$2
                r15 = r7
                kotlinx.coroutines.channels.ChannelIterator r15 = (kotlinx.coroutines.channels.ChannelIterator) r15
                java.lang.Object r7 = r1.L$1
                r16 = r7
                kotlinx.coroutines.channels.ReceiveChannel r16 = (kotlinx.coroutines.channels.ReceiveChannel) r16
                java.lang.Object r7 = r1.L$0
                r17 = r7
                kotlinx.coroutines.channels.ReceiveChannel r17 = (kotlinx.coroutines.channels.ReceiveChannel) r17
                kotlin.ResultKt.throwOnFailure(r26)     // Catch:{ Throwable -> 0x0273, all -> 0x0268 }
                r8 = r26
                r12 = r4
                r9 = r5
                r7 = r16
                r5 = r17
                r10 = r18
                r4 = r1
                r1 = r8
                goto L_0x0311
            L_0x021f:
                r18 = r0
                r2 = 0
                r0 = r2
                r3 = r4
                r5 = r2
                r6 = r2
                r13 = r4
                r7 = r2
                r8 = r2
                r9 = r2
                r10 = r2
                java.lang.Object r2 = r1.L$8
                kotlinx.coroutines.channels.ChannelIterator r2 = (kotlinx.coroutines.channels.ChannelIterator) r2
                java.lang.Object r11 = r1.L$7
                r0 = r11
                kotlinx.coroutines.channels.ReceiveChannel r0 = (kotlinx.coroutines.channels.ReceiveChannel) r0
                java.lang.Object r11 = r1.L$6
                r5 = r11
                java.lang.Throwable r5 = (java.lang.Throwable) r5
                java.lang.Object r11 = r1.L$5
                kotlinx.coroutines.channels.ReceiveChannel r11 = (kotlinx.coroutines.channels.ReceiveChannel) r11
                java.lang.Object r6 = r1.L$4
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1 r6 = (kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1.AnonymousClass1) r6
                java.lang.Object r12 = r1.L$3
                r14 = r12
                kotlinx.coroutines.channels.ReceiveChannel r14 = (kotlinx.coroutines.channels.ReceiveChannel) r14
                java.lang.Object r7 = r1.L$2
                r15 = r7
                kotlinx.coroutines.channels.ChannelIterator r15 = (kotlinx.coroutines.channels.ChannelIterator) r15
                java.lang.Object r7 = r1.L$1
                r16 = r7
                kotlinx.coroutines.channels.ReceiveChannel r16 = (kotlinx.coroutines.channels.ReceiveChannel) r16
                java.lang.Object r7 = r1.L$0
                r17 = r7
                kotlinx.coroutines.channels.ReceiveChannel r17 = (kotlinx.coroutines.channels.ReceiveChannel) r17
                kotlin.ResultKt.throwOnFailure(r26)     // Catch:{ Throwable -> 0x0273, all -> 0x0268 }
                r8 = r26
                r12 = r4
                r9 = r5
                r7 = r16
                r5 = r17
                r10 = r18
                r4 = r1
                r1 = r8
                goto L_0x02ed
            L_0x0268:
                r0 = move-exception
                r12 = r4
                r9 = r5
                r7 = r16
                r2 = r17
                r16 = r26
                goto L_0x04c0
            L_0x0273:
                r0 = move-exception
                r9 = r5
                r12 = r16
                r16 = r26
                goto L_0x04b8
            L_0x027b:
                r18 = r0
                kotlin.ResultKt.throwOnFailure(r26)
                kotlinx.coroutines.CoroutineScope r0 = r1.p$
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1 r2 = r1.this$0
                kotlinx.coroutines.flow.Flow r2 = r2.$this_zip
                kotlinx.coroutines.channels.ReceiveChannel r2 = kotlinx.coroutines.flow.FlowKt__ZipKt.asChannel$FlowKt__ZipKt(r0, r2)
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1 r3 = r1.this$0
                kotlinx.coroutines.flow.Flow r3 = r3.$other
                kotlinx.coroutines.channels.ReceiveChannel r16 = kotlinx.coroutines.flow.FlowKt__ZipKt.asChannel$FlowKt__ZipKt(r0, r3)
                if (r16 == 0) goto L_0x04d3
                r0 = r16
                kotlinx.coroutines.channels.SendChannel r0 = (kotlinx.coroutines.channels.SendChannel) r0
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1$1 r3 = new kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1$1$1
                r3.<init>(r2)
                kotlin.jvm.functions.Function1 r3 = (kotlin.jvm.functions.Function1) r3
                r0.invokeOnClose(r3)
                kotlinx.coroutines.channels.ChannelIterator r15 = r16.iterator()
                r14 = r2
                r4 = 0
                r11 = r14
                r13 = 0
                r3 = 0
                r5 = r3
                java.lang.Throwable r5 = (java.lang.Throwable) r5     // Catch:{ all -> 0x04c6 }
                r9 = r5
                r0 = r11
                r3 = 0
                kotlinx.coroutines.channels.ChannelIterator r5 = r0.iterator()     // Catch:{ Throwable -> 0x04af, all -> 0x04a6 }
                r8 = r26
                r6 = r0
                r0 = r1
                r10 = r3
                r12 = r4
                r4 = r16
                r7 = r18
                r3 = r2
                r2 = r0
            L_0x02c3:
                r2.L$0 = r3     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                r2.L$1 = r4     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                r2.L$2 = r15     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                r2.L$3 = r14     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                r2.L$4 = r0     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                r2.L$5 = r11     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                r2.L$6 = r9     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                r2.L$7 = r6     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                r2.L$8 = r5     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                r1 = 1
                r2.label = r1     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                java.lang.Object r1 = r5.hasNext(r0)     // Catch:{ Throwable -> 0x049a, all -> 0x0493 }
                if (r1 != r7) goto L_0x02df
                return r7
            L_0x02df:
                r23 = r6
                r6 = r0
                r0 = r23
                r24 = r4
                r4 = r2
                r2 = r5
                r5 = r3
                r3 = r10
                r10 = r7
                r7 = r24
            L_0x02ed:
                java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                boolean r1 = r1.booleanValue()     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                if (r1 == 0) goto L_0x046d
                r4.L$0 = r5     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$1 = r7     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$2 = r15     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$3 = r14     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$4 = r6     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$5 = r11     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$6 = r9     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$7 = r0     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$8 = r2     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r1 = 2
                r4.label = r1     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                java.lang.Object r1 = r2.next(r6)     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                if (r1 != r10) goto L_0x0311
                return r10
            L_0x0311:
                r26 = r1
                r16 = 0
                r4.L$0 = r5     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$1 = r7     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$2 = r15     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$3 = r14     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$4 = r6     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$5 = r11     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$6 = r9     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$7 = r0     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$8 = r2     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r4.L$9 = r1     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r17 = r0
                r0 = r26
                r4.L$10 = r0     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                r18 = r0
                r0 = 3
                r4.label = r0     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                java.lang.Object r0 = r15.hasNext(r4)     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                if (r0 != r10) goto L_0x033b
                return r10
            L_0x033b:
                r23 = r7
                r7 = r1
                r1 = r4
                r4 = r2
                r2 = r18
                r18 = r12
                r12 = r23
                r24 = r17
                r17 = r3
                r3 = r16
                r16 = r8
                r8 = r24
            L_0x0350:
                java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                boolean r0 = r0.booleanValue()     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                if (r0 != 0) goto L_0x0364
                r2 = r1
                r3 = r5
                r0 = r6
                r6 = r8
                r8 = r16
                r5 = r4
                r4 = r12
                r12 = r18
                goto L_0x0418
            L_0x0364:
                r1.L$0 = r5     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$1 = r12     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$2 = r15     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$3 = r14     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$4 = r6     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$5 = r11     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$6 = r9     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$7 = r8     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$8 = r4     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$9 = r7     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r1.L$10 = r2     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                r0 = 4
                r1.label = r0     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                java.lang.Object r0 = r15.next(r1)     // Catch:{ Throwable -> 0x0466, all -> 0x045f }
                if (r0 != r10) goto L_0x0384
                return r10
            L_0x0384:
                java.lang.Object r0 = kotlinx.coroutines.flow.internal.NullSurrogate.unbox$kotlinx_coroutines_core(r0)     // Catch:{ Throwable -> 0x0456, all -> 0x044d }
                r26 = r3
                kotlinx.coroutines.flow.FlowCollector r3 = r1     // Catch:{ Throwable -> 0x0456, all -> 0x044d }
                r19 = r13
                kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1 r13 = r1.this$0     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                kotlin.jvm.functions.Function3 r13 = r13.$transform     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r20 = r10
                java.lang.Object r10 = kotlinx.coroutines.flow.internal.NullSurrogate.unbox$kotlinx_coroutines_core(r2)     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r21 = r10
                java.lang.Object r10 = kotlinx.coroutines.flow.internal.NullSurrogate.unbox$kotlinx_coroutines_core(r0)     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$0 = r5     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$1 = r12     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$2 = r15     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$3 = r14     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$4 = r6     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$5 = r11     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$6 = r9     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$7 = r8     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$8 = r4     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$9 = r7     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$10 = r2     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$11 = r0     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r1.L$12 = r3     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r22 = r0
                r0 = 5
                r1.label = r0     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r0 = r21
                java.lang.Object r0 = r13.invoke(r0, r10, r1)     // Catch:{ Throwable -> 0x0444, all -> 0x043b }
                r10 = r20
                if (r0 != r10) goto L_0x03c8
                return r10
            L_0x03c8:
                r13 = r10
                r20 = r18
                r10 = r22
                r18 = r26
                r23 = r4
                r4 = r2
                r2 = r3
                r3 = r23
                r24 = r15
                r15 = r5
                r5 = r8
                r8 = r7
                r7 = r11
                r11 = r14
                r14 = r12
                r12 = r24
            L_0x03df:
                r1.L$0 = r15     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$1 = r14     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$2 = r12     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$3 = r11     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$4 = r6     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$5 = r7     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$6 = r9     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$7 = r5     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$8 = r3     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$9 = r8     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$10 = r4     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r1.L$11 = r10     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                r21 = r3
                r3 = 6
                r1.label = r3     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                java.lang.Object r0 = r2.emit(r0, r1)     // Catch:{ Throwable -> 0x042e, all -> 0x041f }
                if (r0 != r13) goto L_0x0403
                return r13
            L_0x0403:
                r0 = r7
                r7 = r8
                r2 = r10
                r3 = r21
            L_0x0408:
                r2 = r1
                r10 = r13
                r4 = r14
                r8 = r16
                r13 = r19
                r14 = r11
                r11 = r0
                r0 = r6
                r6 = r5
                r5 = r3
                r3 = r15
                r15 = r12
                r12 = r20
            L_0x0418:
                r1 = r25
                r7 = r10
                r10 = r17
                goto L_0x02c3
            L_0x041f:
                r0 = move-exception
                r2 = r15
                r13 = r19
                r15 = r12
                r12 = r20
                r23 = r11
                r11 = r7
                r7 = r14
                r14 = r23
                goto L_0x04c0
            L_0x042e:
                r0 = move-exception
                r17 = r15
                r13 = r19
                r4 = r20
                r15 = r12
                r12 = r14
                r14 = r11
                r11 = r7
                goto L_0x04b8
            L_0x043b:
                r0 = move-exception
                r2 = r5
                r7 = r12
                r12 = r18
                r13 = r19
                goto L_0x04c0
            L_0x0444:
                r0 = move-exception
                r17 = r5
                r4 = r18
                r13 = r19
                goto L_0x04b8
            L_0x044d:
                r0 = move-exception
                r19 = r13
                r2 = r5
                r7 = r12
                r12 = r18
                goto L_0x04c0
            L_0x0456:
                r0 = move-exception
                r19 = r13
                r17 = r5
                r4 = r18
                goto L_0x04b8
            L_0x045f:
                r0 = move-exception
                r2 = r5
                r7 = r12
                r12 = r18
                goto L_0x04c0
            L_0x0466:
                r0 = move-exception
                r17 = r5
                r4 = r18
                goto L_0x04b8
            L_0x046d:
                kotlin.Unit r0 = kotlin.Unit.INSTANCE     // Catch:{ Throwable -> 0x048a, all -> 0x0484 }
                kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r11, r9)     // Catch:{ all -> 0x047d }
                r1 = 1
                r2 = 0
                kotlinx.coroutines.channels.ReceiveChannel.DefaultImpls.cancel$default(r7, r2, r1, r2)
                kotlin.Unit r0 = kotlin.Unit.INSTANCE
                return r0
            L_0x047d:
                r0 = move-exception
                r1 = r4
                r2 = r5
                r16 = r8
                goto L_0x04cd
            L_0x0484:
                r0 = move-exception
                r1 = r4
                r2 = r5
                r16 = r8
                goto L_0x04c0
            L_0x048a:
                r0 = move-exception
                r1 = r4
                r17 = r5
                r16 = r8
                r4 = r12
                r12 = r7
                goto L_0x04b8
            L_0x0493:
                r0 = move-exception
                r1 = r2
                r2 = r3
                r7 = r4
                r16 = r8
                goto L_0x04c0
            L_0x049a:
                r0 = move-exception
                r1 = r2
                r17 = r3
                r16 = r8
                r23 = r12
                r12 = r4
                r4 = r23
                goto L_0x04b8
            L_0x04a6:
                r0 = move-exception
                r1 = r25
                r12 = r4
                r7 = r16
                r16 = r26
                goto L_0x04c0
            L_0x04af:
                r0 = move-exception
                r1 = r25
                r17 = r2
                r12 = r16
                r16 = r26
            L_0x04b8:
                r9 = r0
                throw r0     // Catch:{ all -> 0x04bb }
            L_0x04bb:
                r0 = move-exception
                r7 = r12
                r2 = r17
                r12 = r4
            L_0x04c0:
                kotlinx.coroutines.channels.ChannelsKt.cancelConsumed(r11, r9)     // Catch:{ all -> 0x04c4 }
                throw r0     // Catch:{ all -> 0x04c4 }
            L_0x04c4:
                r0 = move-exception
                goto L_0x04cd
            L_0x04c6:
                r0 = move-exception
                r1 = r25
                r7 = r16
                r16 = r26
            L_0x04cd:
                r3 = 1
                r4 = 0
                kotlinx.coroutines.channels.ReceiveChannel.DefaultImpls.cancel$default(r7, r4, r3, r4)
                throw r0
            L_0x04d3:
                kotlin.TypeCastException r0 = new kotlin.TypeCastException
                java.lang.String r1 = "null cannot be cast to non-null type kotlinx.coroutines.channels.SendChannel<*>"
                r0.<init>(r1)
                throw r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__ZipKt$zip$1.AnonymousClass1.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @Nullable
    public final Object invokeSuspend(@NotNull Object result) {
        Object coroutine_suspended = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(result);
            final FlowCollector flowCollector = this.p$;
            this.label = 1;
            if (CoroutineScopeKt.coroutineScope(new AnonymousClass1(this, null), this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            ResultKt.throwOnFailure(result);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }
}
