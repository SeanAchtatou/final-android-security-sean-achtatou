package kotlinx.coroutines.flow;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u0002H@ø\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "Lkotlinx/coroutines/flow/FlowCollector;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"}, k = 3, mv = {1, 1, 15})
@DebugMetadata(c = "kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$7", f = "Builders.kt", i = {0, 0, 0}, l = {151}, m = "invokeSuspend", n = {"$this$forEach$iv", "element$iv", "value"}, s = {"L$1", "I$2", "I$3"})
/* compiled from: Builders.kt */
final class FlowKt__BuildersKt$asFlow$7 extends SuspendLambda implements Function2<FlowCollector<? super Integer>, Continuation<? super Unit>, Object> {
    final /* synthetic */ int[] $this_asFlow;
    int I$0;
    int I$1;
    int I$2;
    int I$3;
    Object L$0;
    Object L$1;
    Object L$2;
    int label;
    private FlowCollector p$;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    FlowKt__BuildersKt$asFlow$7(int[] iArr, Continuation continuation) {
        super(2, continuation);
        this.$this_asFlow = iArr;
    }

    @NotNull
    public final Continuation<Unit> create(@Nullable Object obj, @NotNull Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "completion");
        FlowKt__BuildersKt$asFlow$7 flowKt__BuildersKt$asFlow$7 = new FlowKt__BuildersKt$asFlow$7(this.$this_asFlow, continuation);
        FlowCollector flowCollector = (FlowCollector) obj;
        flowKt__BuildersKt$asFlow$7.p$ = (FlowCollector) obj;
        return flowKt__BuildersKt$asFlow$7;
    }

    public final Object invoke(Object obj, Object obj2) {
        return ((FlowKt__BuildersKt$asFlow$7) create(obj, (Continuation) obj2)).invokeSuspend(Unit.INSTANCE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0048  */
    @org.jetbrains.annotations.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(@org.jetbrains.annotations.NotNull java.lang.Object r15) {
        /*
            r14 = this;
            java.lang.Object r0 = kotlin.coroutines.intrinsics.IntrinsicsKt.getCOROUTINE_SUSPENDED()
            int r1 = r14.label
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0034
            if (r1 != r2) goto L_0x002c
            r1 = r3
            r4 = r3
            r5 = r3
            r6 = 0
            int r1 = r14.I$3
            int r5 = r14.I$2
            int r7 = r14.I$1
            int r8 = r14.I$0
            java.lang.Object r9 = r14.L$2
            int[] r9 = (int[]) r9
            java.lang.Object r10 = r14.L$1
            r6 = r10
            int[] r6 = (int[]) r6
            java.lang.Object r10 = r14.L$0
            kotlinx.coroutines.flow.FlowCollector r10 = (kotlinx.coroutines.flow.FlowCollector) r10
            kotlin.ResultKt.throwOnFailure(r15)
            r11 = r0
            r0 = r15
            r15 = r14
            goto L_0x0074
        L_0x002c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0034:
            kotlin.ResultKt.throwOnFailure(r15)
            kotlinx.coroutines.flow.FlowCollector r1 = r14.p$
            int[] r4 = r14.$this_asFlow
            r5 = 0
            int r6 = r4.length
            r10 = r1
            r9 = r4
            r3 = r5
            r8 = r6
            r7 = 0
            r1 = r0
            r6 = r9
            r0 = r15
            r15 = r14
        L_0x0046:
            if (r7 >= r8) goto L_0x0078
            r5 = r9[r7]
            java.lang.Integer r4 = kotlin.coroutines.jvm.internal.Boxing.boxInt(r5)
            java.lang.Number r4 = (java.lang.Number) r4
            int r4 = r4.intValue()
            r11 = 0
            java.lang.Integer r12 = kotlin.coroutines.jvm.internal.Boxing.boxInt(r4)
            r15.L$0 = r10
            r15.L$1 = r6
            r15.L$2 = r9
            r15.I$0 = r8
            r15.I$1 = r7
            r15.I$2 = r5
            r15.I$3 = r4
            r15.label = r2
            java.lang.Object r12 = r10.emit(r12, r15)
            if (r12 != r1) goto L_0x0070
            return r1
        L_0x0070:
            r13 = r11
            r11 = r1
            r1 = r4
            r4 = r13
        L_0x0074:
            int r7 = r7 + r2
            r1 = r11
            goto L_0x0046
        L_0x0078:
            kotlin.Unit r1 = kotlin.Unit.INSTANCE
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.flow.FlowKt__BuildersKt$asFlow$7.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
