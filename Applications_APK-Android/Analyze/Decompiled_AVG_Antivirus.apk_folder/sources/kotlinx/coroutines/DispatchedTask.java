package kotlinx.coroutines;

import kotlin.ExceptionsKt;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.jvm.JvmField;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.scheduling.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u000e\b \u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\u00060\u0002j\u0002`\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0010¢\u0006\u0002\b\u0011J\u0019\u0010\u0012\u001a\u0004\u0018\u00010\u00102\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0000¢\u0006\u0002\b\u0013J\u001f\u0010\u0014\u001a\u0002H\u0001\"\u0004\b\u0001\u0010\u00012\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0010¢\u0006\u0004\b\u0015\u0010\u0016J!\u0010\u0017\u001a\u00020\f2\b\u0010\u0018\u001a\u0004\u0018\u00010\u00102\b\u0010\u0019\u001a\u0004\u0018\u00010\u0010H\u0000¢\u0006\u0002\b\u001aJ\u0006\u0010\u001b\u001a\u00020\fJ\u000f\u0010\u001c\u001a\u0004\u0018\u00010\u000eH ¢\u0006\u0002\b\u001dR\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\bX \u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0012\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u000e¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lkotlinx/coroutines/DispatchedTask;", "T", "Lkotlinx/coroutines/scheduling/Task;", "Lkotlinx/coroutines/SchedulerTask;", "resumeMode", "", "(I)V", "delegate", "Lkotlin/coroutines/Continuation;", "getDelegate$kotlinx_coroutines_core", "()Lkotlin/coroutines/Continuation;", "cancelResult", "", "state", "", "cause", "", "cancelResult$kotlinx_coroutines_core", "getExceptionalResult", "getExceptionalResult$kotlinx_coroutines_core", "getSuccessfulResult", "getSuccessfulResult$kotlinx_coroutines_core", "(Ljava/lang/Object;)Ljava/lang/Object;", "handleFatalException", "exception", "finallyException", "handleFatalException$kotlinx_coroutines_core", "run", "takeState", "takeState$kotlinx_coroutines_core", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
/* compiled from: Dispatched.kt */
public abstract class DispatchedTask<T> extends Task {
    @JvmField
    public int resumeMode;

    @NotNull
    public abstract Continuation<T> getDelegate$kotlinx_coroutines_core();

    @Nullable
    public abstract Object takeState$kotlinx_coroutines_core();

    public DispatchedTask(int resumeMode2) {
        this.resumeMode = resumeMode2;
    }

    public void cancelResult$kotlinx_coroutines_core(@Nullable Object state, @NotNull Throwable cause) {
        Intrinsics.checkParameterIsNotNull(cause, "cause");
    }

    public <T> T getSuccessfulResult$kotlinx_coroutines_core(@Nullable Object state) {
        return state;
    }

    @Nullable
    public final Throwable getExceptionalResult$kotlinx_coroutines_core(@Nullable Object state) {
        CompletedExceptionally completedExceptionally = (CompletedExceptionally) (!(state instanceof CompletedExceptionally) ? null : state);
        if (completedExceptionally != null) {
            return completedExceptionally.cause;
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00bc, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00bd, code lost:
        r4 = kotlin.Result.Companion;
        r3 = kotlin.Result.m3constructorimpl(kotlin.ResultKt.createFailure(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e1, code lost:
        r1 = th;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:31:0x00ac, B:39:0x00d1] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r15 = this;
            kotlinx.coroutines.scheduling.TaskContext r0 = r15.taskContext
            r1 = 0
            r2 = r1
            java.lang.Throwable r2 = (java.lang.Throwable) r2
            kotlin.coroutines.Continuation r3 = r15.getDelegate$kotlinx_coroutines_core()     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            if (r3 == 0) goto L_0x00a3
            kotlinx.coroutines.DispatchedContinuation r3 = (kotlinx.coroutines.DispatchedContinuation) r3     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            kotlin.coroutines.Continuation<T> r4 = r3.continuation     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            kotlin.coroutines.CoroutineContext r5 = r4.getContext()     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            int r6 = r15.resumeMode     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            boolean r6 = kotlinx.coroutines.ResumeModeKt.isCancellableMode(r6)     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            if (r6 == 0) goto L_0x0027
            kotlinx.coroutines.Job$Key r1 = kotlinx.coroutines.Job.Key     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            kotlin.coroutines.CoroutineContext$Key r1 = (kotlin.coroutines.CoroutineContext.Key) r1     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            kotlin.coroutines.CoroutineContext$Element r1 = r5.get(r1)     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            kotlinx.coroutines.Job r1 = (kotlinx.coroutines.Job) r1     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
        L_0x0027:
            java.lang.Object r6 = r15.takeState$kotlinx_coroutines_core()     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            java.lang.Object r7 = r3.countOrElement     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            r8 = 0
            java.lang.Object r9 = kotlinx.coroutines.internal.ThreadContextKt.updateThreadContext(r5, r7)     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            r10 = 0
            if (r1 == 0) goto L_0x005d
            boolean r11 = r1.isActive()     // Catch:{ all -> 0x009e }
            if (r11 != 0) goto L_0x005d
            java.util.concurrent.CancellationException r11 = r1.getCancellationException()     // Catch:{ all -> 0x009e }
            r12 = r11
            java.lang.Throwable r12 = (java.lang.Throwable) r12     // Catch:{ all -> 0x009e }
            r15.cancelResult$kotlinx_coroutines_core(r6, r12)     // Catch:{ all -> 0x009e }
            r12 = r4
            r13 = 0
            kotlin.Result$Companion r14 = kotlin.Result.Companion     // Catch:{ all -> 0x009e }
            r14 = r11
            java.lang.Throwable r14 = (java.lang.Throwable) r14     // Catch:{ all -> 0x009e }
            java.lang.Throwable r14 = kotlinx.coroutines.internal.StackTraceRecoveryKt.recoverStackTrace(r14, r12)     // Catch:{ all -> 0x009e }
            java.lang.Object r14 = kotlin.ResultKt.createFailure(r14)     // Catch:{ all -> 0x009e }
            java.lang.Object r14 = kotlin.Result.m3constructorimpl(r14)     // Catch:{ all -> 0x009e }
            r12.resumeWith(r14)     // Catch:{ all -> 0x009e }
            goto L_0x0084
        L_0x005d:
            java.lang.Throwable r11 = r15.getExceptionalResult$kotlinx_coroutines_core(r6)     // Catch:{ all -> 0x009e }
            if (r11 == 0) goto L_0x0077
            r12 = r4
            r13 = 0
            kotlin.Result$Companion r14 = kotlin.Result.Companion     // Catch:{ all -> 0x009e }
            java.lang.Throwable r14 = kotlinx.coroutines.internal.StackTraceRecoveryKt.recoverStackTrace(r11, r12)     // Catch:{ all -> 0x009e }
            java.lang.Object r14 = kotlin.ResultKt.createFailure(r14)     // Catch:{ all -> 0x009e }
            java.lang.Object r14 = kotlin.Result.m3constructorimpl(r14)     // Catch:{ all -> 0x009e }
            r12.resumeWith(r14)     // Catch:{ all -> 0x009e }
            goto L_0x0084
        L_0x0077:
            java.lang.Object r12 = r15.getSuccessfulResult$kotlinx_coroutines_core(r6)     // Catch:{ all -> 0x009e }
            kotlin.Result$Companion r13 = kotlin.Result.Companion     // Catch:{ all -> 0x009e }
            java.lang.Object r12 = kotlin.Result.m3constructorimpl(r12)     // Catch:{ all -> 0x009e }
            r4.resumeWith(r12)     // Catch:{ all -> 0x009e }
        L_0x0084:
            kotlin.Unit r10 = kotlin.Unit.INSTANCE     // Catch:{ all -> 0x009e }
            kotlinx.coroutines.internal.ThreadContextKt.restoreThreadContext(r5, r9)     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            kotlin.Result$Companion r1 = kotlin.Result.Companion     // Catch:{ Throwable -> 0x009c }
            r1 = r15
            kotlinx.coroutines.DispatchedTask r1 = (kotlinx.coroutines.DispatchedTask) r1     // Catch:{ Throwable -> 0x009c }
            r3 = 0
            r0.afterTask()     // Catch:{ Throwable -> 0x009c }
            kotlin.Unit r1 = kotlin.Unit.INSTANCE     // Catch:{ Throwable -> 0x009c }
            java.lang.Object r1 = kotlin.Result.m3constructorimpl(r1)     // Catch:{ Throwable -> 0x009c }
            goto L_0x00ec
        L_0x009c:
            r1 = move-exception
            goto L_0x00e2
        L_0x009e:
            r10 = move-exception
            kotlinx.coroutines.internal.ThreadContextKt.restoreThreadContext(r5, r9)     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            throw r10     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
        L_0x00a3:
            kotlin.TypeCastException r1 = new kotlin.TypeCastException     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            java.lang.String r3 = "null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>"
            r1.<init>(r3)     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
            throw r1     // Catch:{ Throwable -> 0x00cf, all -> 0x00ab }
        L_0x00ab:
            r1 = move-exception
            kotlin.Result$Companion r3 = kotlin.Result.Companion     // Catch:{ Throwable -> 0x00bc }
            r3 = r15
            kotlinx.coroutines.DispatchedTask r3 = (kotlinx.coroutines.DispatchedTask) r3     // Catch:{ Throwable -> 0x00bc }
            r4 = 0
            r0.afterTask()     // Catch:{ Throwable -> 0x00bc }
            kotlin.Unit r3 = kotlin.Unit.INSTANCE     // Catch:{ Throwable -> 0x00bc }
            java.lang.Object r3 = kotlin.Result.m3constructorimpl(r3)     // Catch:{ Throwable -> 0x00bc }
            goto L_0x00c7
        L_0x00bc:
            r3 = move-exception
            kotlin.Result$Companion r4 = kotlin.Result.Companion
            java.lang.Object r3 = kotlin.ResultKt.createFailure(r3)
            java.lang.Object r3 = kotlin.Result.m3constructorimpl(r3)
        L_0x00c7:
            java.lang.Throwable r4 = kotlin.Result.m6exceptionOrNullimpl(r3)
            r15.handleFatalException$kotlinx_coroutines_core(r2, r4)
            throw r1
        L_0x00cf:
            r1 = move-exception
            r2 = r1
            kotlin.Result$Companion r1 = kotlin.Result.Companion     // Catch:{ Throwable -> 0x00e1 }
            r1 = r15
            kotlinx.coroutines.DispatchedTask r1 = (kotlinx.coroutines.DispatchedTask) r1     // Catch:{ Throwable -> 0x00e1 }
            r3 = 0
            r0.afterTask()     // Catch:{ Throwable -> 0x00e1 }
            kotlin.Unit r1 = kotlin.Unit.INSTANCE     // Catch:{ Throwable -> 0x00e1 }
            java.lang.Object r1 = kotlin.Result.m3constructorimpl(r1)     // Catch:{ Throwable -> 0x00e1 }
            goto L_0x00ec
        L_0x00e1:
            r1 = move-exception
        L_0x00e2:
            kotlin.Result$Companion r3 = kotlin.Result.Companion
            java.lang.Object r1 = kotlin.ResultKt.createFailure(r1)
            java.lang.Object r1 = kotlin.Result.m3constructorimpl(r1)
        L_0x00ec:
            java.lang.Throwable r3 = kotlin.Result.m6exceptionOrNullimpl(r1)
            r15.handleFatalException$kotlinx_coroutines_core(r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.DispatchedTask.run():void");
    }

    public final void handleFatalException$kotlinx_coroutines_core(@Nullable Throwable exception, @Nullable Throwable finallyException) {
        if (exception != null || finallyException != null) {
            if (!(exception == null || finallyException == null)) {
                ExceptionsKt.addSuppressed(exception, finallyException);
            }
            Throwable cause = exception != null ? exception : finallyException;
            String str = "Fatal exception in coroutines machinery for " + this + ". " + "Please read KDoc to 'handleFatalException' method and report this incident to maintainers";
            if (cause == null) {
                Intrinsics.throwNpe();
            }
            CoroutineExceptionHandlerKt.handleCoroutineException(getDelegate$kotlinx_coroutines_core().getContext(), new CoroutinesInternalError(str, cause));
        }
    }
}
