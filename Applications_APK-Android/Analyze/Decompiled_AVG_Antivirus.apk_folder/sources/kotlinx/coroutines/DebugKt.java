package kotlinx.coroutines;

import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.JvmField;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u000f\u001a\u00020\u0003*\u0006\u0012\u0002\b\u00030\u0010H\u0000\"\u0010\u0010\u0000\u001a\u00020\u00018\u0000X\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0003XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0003XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0003XT¢\u0006\u0002\n\u0000\"\u0010\u0010\u0007\u001a\u00020\u00018\u0000X\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0003XT¢\u0006\u0002\n\u0000\"\u0018\u0010\t\u001a\u00020\u0003*\u00020\n8@X\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f\"\u0018\u0010\r\u001a\u00020\u0003*\u00020\n8@X\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\f¨\u0006\u0011"}, d2 = {"DEBUG", "", "DEBUG_PROPERTY_NAME", "", "DEBUG_PROPERTY_VALUE_AUTO", "DEBUG_PROPERTY_VALUE_OFF", "DEBUG_PROPERTY_VALUE_ON", "RECOVER_STACK_TRACES", "STACKTRACE_RECOVERY_PROPERTY_NAME", "classSimpleName", "", "getClassSimpleName", "(Ljava/lang/Object;)Ljava/lang/String;", "hexAddress", "getHexAddress", "toDebugString", "Lkotlin/coroutines/Continuation;", "kotlinx-coroutines-core"}, k = 2, mv = {1, 1, 15})
/* compiled from: Debug.kt */
public final class DebugKt {
    @JvmField
    public static final boolean DEBUG;
    @NotNull
    public static final String DEBUG_PROPERTY_NAME = "kotlinx.coroutines.debug";
    @NotNull
    public static final String DEBUG_PROPERTY_VALUE_AUTO = "auto";
    @NotNull
    public static final String DEBUG_PROPERTY_VALUE_OFF = "off";
    @NotNull
    public static final String DEBUG_PROPERTY_VALUE_ON = "on";
    @JvmField
    public static final boolean RECOVER_STACK_TRACES;
    @NotNull
    public static final String STACKTRACE_RECOVERY_PROPERTY_NAME = "kotlinx.coroutines.stacktrace.recovery";

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        if (r0.equals(kotlinx.coroutines.DebugKt.DEBUG_PROPERTY_VALUE_AUTO) != false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        if (r0.equals(kotlinx.coroutines.DebugKt.DEBUG_PROPERTY_VALUE_ON) != false) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0049, code lost:
        if (r0.equals("") != false) goto L_0x004b;
     */
    static {
        /*
            java.lang.String r0 = "kotlinx.coroutines.debug"
            java.lang.String r0 = kotlinx.coroutines.internal.SystemPropsKt.systemProp(r0)
            r1 = 0
            r2 = 0
            r3 = 1
            if (r0 != 0) goto L_0x000c
        L_0x000b:
            goto L_0x0029
        L_0x000c:
            int r4 = r0.hashCode()
            if (r4 == 0) goto L_0x0043
            r5 = 3551(0xddf, float:4.976E-42)
            if (r4 == r5) goto L_0x003a
            r5 = 109935(0x1ad6f, float:1.54052E-40)
            if (r4 == r5) goto L_0x0030
            r5 = 3005871(0x2dddaf, float:4.212122E-39)
            if (r4 != r5) goto L_0x005f
            java.lang.String r4 = "auto"
            boolean r4 = r0.equals(r4)
            if (r4 == 0) goto L_0x005f
            goto L_0x000b
        L_0x0029:
            java.lang.Class<kotlinx.coroutines.CoroutineId> r4 = kotlinx.coroutines.CoroutineId.class
            boolean r4 = r4.desiredAssertionStatus()
            goto L_0x004c
        L_0x0030:
            java.lang.String r4 = "off"
            boolean r4 = r0.equals(r4)
            if (r4 == 0) goto L_0x005f
            r4 = 0
            goto L_0x004c
        L_0x003a:
            java.lang.String r4 = "on"
            boolean r4 = r0.equals(r4)
            if (r4 == 0) goto L_0x005f
            goto L_0x004b
        L_0x0043:
            java.lang.String r4 = ""
            boolean r4 = r0.equals(r4)
            if (r4 == 0) goto L_0x005f
        L_0x004b:
            r4 = 1
        L_0x004c:
            kotlinx.coroutines.DebugKt.DEBUG = r4
            boolean r0 = kotlinx.coroutines.DebugKt.DEBUG
            if (r0 == 0) goto L_0x005c
            java.lang.String r0 = "kotlinx.coroutines.stacktrace.recovery"
            boolean r0 = kotlinx.coroutines.internal.SystemPropsKt.systemProp(r0, r3)
            if (r0 == 0) goto L_0x005c
            r2 = 1
        L_0x005c:
            kotlinx.coroutines.DebugKt.RECOVER_STACK_TRACES = r2
            return
        L_0x005f:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "System property 'kotlinx.coroutines.debug' has unrecognized value '"
            r2.append(r3)
            r2.append(r0)
            r3 = 39
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            java.lang.Throwable r3 = (java.lang.Throwable) r3
            throw r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.DebugKt.<clinit>():void");
    }

    @NotNull
    public static final String getHexAddress(@NotNull Object $this$hexAddress) {
        Intrinsics.checkParameterIsNotNull($this$hexAddress, "$this$hexAddress");
        String hexString = Integer.toHexString(System.identityHashCode($this$hexAddress));
        Intrinsics.checkExpressionValueIsNotNull(hexString, "Integer.toHexString(System.identityHashCode(this))");
        return hexString;
    }

    @NotNull
    public static final String toDebugString(@NotNull Continuation<?> $this$toDebugString) {
        Object obj;
        String str;
        Intrinsics.checkParameterIsNotNull($this$toDebugString, "$this$toDebugString");
        if ($this$toDebugString instanceof DispatchedContinuation) {
            return $this$toDebugString.toString();
        }
        try {
            Result.Companion companion = Result.Companion;
            obj = Result.m3constructorimpl($this$toDebugString + '@' + getHexAddress($this$toDebugString));
        } catch (Throwable th) {
            Result.Companion companion2 = Result.Companion;
            obj = Result.m3constructorimpl(ResultKt.createFailure(th));
        }
        if (Result.m6exceptionOrNullimpl(obj) == null) {
            str = obj;
        } else {
            str = $this$toDebugString.getClass().getName() + '@' + getHexAddress($this$toDebugString);
        }
        return str;
    }

    @NotNull
    public static final String getClassSimpleName(@NotNull Object $this$classSimpleName) {
        Intrinsics.checkParameterIsNotNull($this$classSimpleName, "$this$classSimpleName");
        String simpleName = $this$classSimpleName.getClass().getSimpleName();
        Intrinsics.checkExpressionValueIsNotNull(simpleName, "this::class.java.simpleName");
        return simpleName;
    }
}
