package kotlinx.coroutines.selects;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;
import kotlin.PublishedApi;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlin.jvm.JvmField;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CompletedExceptionally;
import kotlinx.coroutines.CompletedExceptionallyKt;
import kotlinx.coroutines.CoroutineExceptionHandlerKt;
import kotlinx.coroutines.DelayKt;
import kotlinx.coroutines.DispatchedKt;
import kotlinx.coroutines.DisposableHandle;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.JobCancellingNode;
import kotlinx.coroutines.internal.AtomicDesc;
import kotlinx.coroutines.internal.AtomicOp;
import kotlinx.coroutines.internal.LockFreeLinkedListHead;
import kotlinx.coroutines.internal.LockFreeLinkedListNode;
import kotlinx.coroutines.internal.OpDescriptor;
import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlinx.coroutines.selects.SelectBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@PublishedApi
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¢\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0001\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u00032\b\u0012\u0004\u0012\u0002H\u00010\u00042\b\u0012\u0004\u0012\u0002H\u00010\u00052\u00060\u0006j\u0002`\u0007:\u0003LMNB\u0013\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005¢\u0006\u0002\u0010\tJ\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u001cH\u0016J\b\u0010#\u001a\u00020!H\u0002J'\u0010$\u001a\u00020!2\u000e\u0010%\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0&2\f\u0010'\u001a\b\u0012\u0004\u0012\u00020!0&H\bJ\n\u0010(\u001a\u0004\u0018\u00010\fH\u0001J\u0010\u0010)\u001a\n\u0018\u00010*j\u0004\u0018\u0001`+H\u0016J\u0010\u0010,\u001a\u00020!2\u0006\u0010-\u001a\u00020.H\u0001J\b\u0010/\u001a\u00020!H\u0002J6\u00100\u001a\u00020!2\u0006\u00101\u001a\u0002022\u001c\u0010'\u001a\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0012\u0004\u0018\u00010\f03H\u0016ø\u0001\u0000¢\u0006\u0002\u00104J\u0012\u00105\u001a\u0004\u0018\u00010\f2\u0006\u00106\u001a\u000207H\u0016J\u0012\u00108\u001a\u0004\u0018\u00010\f2\u0006\u00106\u001a\u000207H\u0016J\u0010\u00109\u001a\u00020!2\u0006\u0010:\u001a\u00020.H\u0016J\u001e\u0010;\u001a\u00020!2\f\u0010<\u001a\b\u0012\u0004\u0012\u00028\u00000=H\u0016ø\u0001\u0000¢\u0006\u0002\u0010>J\u0012\u0010?\u001a\u00020\u00192\b\u0010@\u001a\u0004\u0018\u00010\fH\u0016J3\u0010A\u001a\u00020!*\u00020B2\u001c\u0010'\u001a\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0012\u0004\u0018\u00010\f03H\u0002ø\u0001\u0000¢\u0006\u0002\u0010CJE\u0010A\u001a\u00020!\"\u0004\b\u0001\u0010D*\b\u0012\u0004\u0012\u0002HD0E2\"\u0010'\u001a\u001e\b\u0001\u0012\u0004\u0012\u0002HD\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0012\u0004\u0018\u00010\f0FH\u0002ø\u0001\u0000¢\u0006\u0002\u0010GJY\u0010A\u001a\u00020!\"\u0004\b\u0001\u0010H\"\u0004\b\u0002\u0010D*\u000e\u0012\u0004\u0012\u0002HH\u0012\u0004\u0012\u0002HD0I2\u0006\u0010J\u001a\u0002HH2\"\u0010'\u001a\u001e\b\u0001\u0012\u0004\u0012\u0002HD\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0012\u0004\u0018\u00010\f0FH\u0002ø\u0001\u0000¢\u0006\u0002\u0010KR\u0016\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\u000bX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\u000bX\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u000e\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078VX\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u00058VX\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u00158VX\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u00198VX\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u001aR\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u001d\u001a\u0004\u0018\u00010\f8BX\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u001fR\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0004¢\u0006\u0002\n\u0000\u0002\u0004\n\u0002\b\u0019¨\u0006O"}, d2 = {"Lkotlinx/coroutines/selects/SelectBuilderImpl;", "R", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "Lkotlinx/coroutines/selects/SelectBuilder;", "Lkotlinx/coroutines/selects/SelectInstance;", "Lkotlin/coroutines/Continuation;", "Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "Lkotlinx/coroutines/internal/CoroutineStackFrame;", "uCont", "(Lkotlin/coroutines/Continuation;)V", "_result", "Lkotlinx/atomicfu/AtomicRef;", "", "_state", "callerFrame", "getCallerFrame", "()Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "completion", "getCompletion", "()Lkotlin/coroutines/Continuation;", "context", "Lkotlin/coroutines/CoroutineContext;", "getContext", "()Lkotlin/coroutines/CoroutineContext;", "isSelected", "", "()Z", "parentHandle", "Lkotlinx/coroutines/DisposableHandle;", "state", "getState", "()Ljava/lang/Object;", "disposeOnSelect", "", "handle", "doAfterSelect", "doResume", "value", "Lkotlin/Function0;", "block", "getResult", "getStackTraceElement", "Ljava/lang/StackTraceElement;", "Lkotlinx/coroutines/internal/StackTraceElement;", "handleBuilderException", "e", "", "initCancellability", "onTimeout", "timeMillis", "", "Lkotlin/Function1;", "(JLkotlin/jvm/functions/Function1;)V", "performAtomicIfNotSelected", "desc", "Lkotlinx/coroutines/internal/AtomicDesc;", "performAtomicTrySelect", "resumeSelectCancellableWithException", "exception", "resumeWith", "result", "Lkotlin/Result;", "(Ljava/lang/Object;)V", "trySelect", "idempotent", "invoke", "Lkotlinx/coroutines/selects/SelectClause0;", "(Lkotlinx/coroutines/selects/SelectClause0;Lkotlin/jvm/functions/Function1;)V", "Q", "Lkotlinx/coroutines/selects/SelectClause1;", "Lkotlin/Function2;", "(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V", "P", "Lkotlinx/coroutines/selects/SelectClause2;", "param", "(Lkotlinx/coroutines/selects/SelectClause2;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V", "AtomicSelectOp", "DisposeNode", "SelectOnCancelling", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
/* compiled from: Select.kt */
public final class SelectBuilderImpl<R> extends LockFreeLinkedListHead implements SelectBuilder<R>, SelectInstance<R>, Continuation<R>, CoroutineStackFrame {
    static final AtomicReferenceFieldUpdater _result$FU = AtomicReferenceFieldUpdater.newUpdater(SelectBuilderImpl.class, Object.class, "_result");
    static final AtomicReferenceFieldUpdater _state$FU = AtomicReferenceFieldUpdater.newUpdater(SelectBuilderImpl.class, Object.class, "_state");
    volatile Object _result = SelectKt.UNDECIDED;
    volatile Object _state = this;
    private volatile DisposableHandle parentHandle;
    private final Continuation<R> uCont;

    public SelectBuilderImpl(@NotNull Continuation<? super R> uCont2) {
        Intrinsics.checkParameterIsNotNull(uCont2, "uCont");
        this.uCont = uCont2;
    }

    public <P, Q> void invoke(@NotNull SelectClause2<? super P, ? extends Q> $this$invoke, @NotNull Function2<? super Q, ? super Continuation<? super R>, ? extends Object> block) {
        Intrinsics.checkParameterIsNotNull($this$invoke, "$this$invoke");
        Intrinsics.checkParameterIsNotNull(block, "block");
        SelectBuilder.DefaultImpls.invoke(this, $this$invoke, block);
    }

    @Nullable
    public CoroutineStackFrame getCallerFrame() {
        Continuation<R> continuation = this.uCont;
        if (!(continuation instanceof CoroutineStackFrame)) {
            continuation = null;
        }
        return (CoroutineStackFrame) continuation;
    }

    @Nullable
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @NotNull
    public CoroutineContext getContext() {
        return this.uCont.getContext();
    }

    @NotNull
    public Continuation<R> getCompletion() {
        return this;
    }

    private final void doResume(Function0<? extends Object> value, Function0<Unit> block) {
        if (isSelected()) {
            while (true) {
                Object result = this._result;
                if (result == SelectKt.UNDECIDED) {
                    if (_result$FU.compareAndSet(this, SelectKt.UNDECIDED, value.invoke())) {
                        return;
                    }
                } else if (result != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    throw new IllegalStateException("Already resumed");
                } else if (_result$FU.compareAndSet(this, IntrinsicsKt.getCOROUTINE_SUSPENDED(), SelectKt.RESUMED)) {
                    block.invoke();
                    return;
                }
            }
        } else {
            throw new IllegalStateException("Must be selected first".toString());
        }
    }

    public void resumeWith(@NotNull Object result) {
        if (isSelected()) {
            while (true) {
                Object result$iv = this._result;
                if (result$iv == SelectKt.UNDECIDED) {
                    if (_result$FU.compareAndSet(this, SelectKt.UNDECIDED, CompletedExceptionallyKt.toState(result))) {
                        return;
                    }
                } else if (result$iv != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    throw new IllegalStateException("Already resumed");
                } else if (_result$FU.compareAndSet(this, IntrinsicsKt.getCOROUTINE_SUSPENDED(), SelectKt.RESUMED)) {
                    if (Result.m9isFailureimpl(result)) {
                        Continuation $this$resumeWithStackTrace$iv = this.uCont;
                        Throwable exception$iv = Result.m6exceptionOrNullimpl(result);
                        if (exception$iv == null) {
                            Intrinsics.throwNpe();
                        }
                        Result.Companion companion = Result.Companion;
                        $this$resumeWithStackTrace$iv.resumeWith(Result.m3constructorimpl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(exception$iv, $this$resumeWithStackTrace$iv))));
                        return;
                    }
                    this.uCont.resumeWith(result);
                    return;
                }
            }
        } else {
            throw new IllegalStateException("Must be selected first".toString());
        }
    }

    public void resumeSelectCancellableWithException(@NotNull Throwable exception) {
        Intrinsics.checkParameterIsNotNull(exception, "exception");
        if (isSelected()) {
            while (true) {
                Object result$iv = this._result;
                if (result$iv == SelectKt.UNDECIDED) {
                    if (_result$FU.compareAndSet(this, SelectKt.UNDECIDED, new CompletedExceptionally(exception, false, 2, null))) {
                        return;
                    }
                } else if (result$iv != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    throw new IllegalStateException("Already resumed");
                } else if (_result$FU.compareAndSet(this, IntrinsicsKt.getCOROUTINE_SUSPENDED(), SelectKt.RESUMED)) {
                    DispatchedKt.resumeCancellableWithException(IntrinsicsKt.intercepted(this.uCont), exception);
                    return;
                }
            }
        } else {
            throw new IllegalStateException("Must be selected first".toString());
        }
    }

    @Nullable
    @PublishedApi
    public final Object getResult() {
        if (!isSelected()) {
            initCancellability();
        }
        Object result = this._result;
        if (result == SelectKt.UNDECIDED) {
            if (_result$FU.compareAndSet(this, SelectKt.UNDECIDED, IntrinsicsKt.getCOROUTINE_SUSPENDED())) {
                return IntrinsicsKt.getCOROUTINE_SUSPENDED();
            }
            result = this._result;
        }
        if (result == SelectKt.RESUMED) {
            throw new IllegalStateException("Already resumed");
        } else if (!(result instanceof CompletedExceptionally)) {
            return result;
        } else {
            throw ((CompletedExceptionally) result).cause;
        }
    }

    private final void initCancellability() {
        Job parent = (Job) getContext().get(Job.Key);
        if (parent != null) {
            DisposableHandle newRegistration = Job.DefaultImpls.invokeOnCompletion$default(parent, true, false, new SelectOnCancelling(this, parent), 2, null);
            this.parentHandle = newRegistration;
            if (isSelected()) {
                newRegistration.dispose();
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0002\u0010\u0004J\u0013\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0002J\b\u0010\t\u001a\u00020\nH\u0016¨\u0006\u000b"}, d2 = {"Lkotlinx/coroutines/selects/SelectBuilderImpl$SelectOnCancelling;", "Lkotlinx/coroutines/JobCancellingNode;", "Lkotlinx/coroutines/Job;", "job", "(Lkotlinx/coroutines/selects/SelectBuilderImpl;Lkotlinx/coroutines/Job;)V", "invoke", "", "cause", "", "toString", "", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Select.kt */
    private final class SelectOnCancelling extends JobCancellingNode<Job> {
        final /* synthetic */ SelectBuilderImpl this$0;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public SelectOnCancelling(@NotNull SelectBuilderImpl $outer, Job job) {
            super(job);
            Intrinsics.checkParameterIsNotNull(job, "job");
            this.this$0 = $outer;
        }

        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Throwable) obj);
            return Unit.INSTANCE;
        }

        public void invoke(@Nullable Throwable cause) {
            if (this.this$0.trySelect(null)) {
                this.this$0.resumeSelectCancellableWithException(this.job.getCancellationException());
            }
        }

        @NotNull
        public String toString() {
            return "SelectOnCancelling[" + this.this$0 + ']';
        }
    }

    /* access modifiers changed from: private */
    public final Object getState() {
        while (true) {
            Object state = this._state;
            if (!(state instanceof OpDescriptor)) {
                return state;
            }
            ((OpDescriptor) state).perform(this);
        }
    }

    @PublishedApi
    public final void handleBuilderException(@NotNull Throwable e) {
        Intrinsics.checkParameterIsNotNull(e, "e");
        if (trySelect(null)) {
            Result.Companion companion = Result.Companion;
            resumeWith(Result.m3constructorimpl(ResultKt.createFailure(e)));
            return;
        }
        CoroutineExceptionHandlerKt.handleCoroutineException(getContext(), e);
    }

    public boolean isSelected() {
        return getState() != this;
    }

    /* JADX INFO: Multiple debug info for r4v1 kotlinx.coroutines.internal.LockFreeLinkedListNode$CondAddOp: [D('this_$iv$iv' kotlinx.coroutines.internal.LockFreeLinkedListNode), D('condAdd$iv' kotlinx.coroutines.internal.LockFreeLinkedListNode$CondAddOp)] */
    public void disposeOnSelect(@NotNull DisposableHandle handle) {
        boolean z;
        Intrinsics.checkParameterIsNotNull(handle, "handle");
        DisposeNode node = new DisposeNode(handle);
        while (getState() == this) {
            LockFreeLinkedListNode.CondAddOp condAdd$iv = new SelectBuilderImpl$disposeOnSelect$$inlined$addLastIf$1(node, node, this);
            while (true) {
                Object prev = getPrev();
                if (prev != null) {
                    int tryCondAddNext = ((LockFreeLinkedListNode) prev).tryCondAddNext(node, this, condAdd$iv);
                    z = true;
                    if (tryCondAddNext != 1) {
                        if (tryCondAddNext == 2) {
                            z = false;
                            continue;
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                }
            }
            if (z) {
                return;
            }
        }
        handle.dispose();
    }

    /* access modifiers changed from: private */
    public final void doAfterSelect() {
        DisposableHandle disposableHandle = this.parentHandle;
        if (disposableHandle != null) {
            disposableHandle.dispose();
        }
        Object next = getNext();
        if (next != null) {
            for (LockFreeLinkedListNode cur$iv = (LockFreeLinkedListNode) next; !Intrinsics.areEqual(cur$iv, this); cur$iv = cur$iv.getNextNode()) {
                if (cur$iv instanceof DisposeNode) {
                    ((DisposeNode) cur$iv).handle.dispose();
                }
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    public boolean trySelect(@Nullable Object idempotent) {
        if (!(idempotent instanceof OpDescriptor)) {
            do {
                Object state = getState();
                if (state != this) {
                    return idempotent != null && state == idempotent;
                }
            } while (!_state$FU.compareAndSet(this, this, idempotent));
            doAfterSelect();
            return true;
        }
        throw new IllegalStateException("cannot use OpDescriptor as idempotent marker".toString());
    }

    @Nullable
    public Object performAtomicTrySelect(@NotNull AtomicDesc desc) {
        Intrinsics.checkParameterIsNotNull(desc, "desc");
        return new AtomicSelectOp(this, desc, true).perform(null);
    }

    @Nullable
    public Object performAtomicIfNotSelected(@NotNull AtomicDesc desc) {
        Intrinsics.checkParameterIsNotNull(desc, "desc");
        return new AtomicSelectOp(this, desc, false).perform(null);
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0004\u0018\u00002\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u001c\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u00022\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u0016J\u0012\u0010\f\u001a\u00020\t2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u0002J\u0014\u0010\r\u001a\u0004\u0018\u00010\u00022\b\u0010\n\u001a\u0004\u0018\u00010\u0002H\u0016J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lkotlinx/coroutines/selects/SelectBuilderImpl$AtomicSelectOp;", "Lkotlinx/coroutines/internal/AtomicOp;", "", "desc", "Lkotlinx/coroutines/internal/AtomicDesc;", "select", "", "(Lkotlinx/coroutines/selects/SelectBuilderImpl;Lkotlinx/coroutines/internal/AtomicDesc;Z)V", "complete", "", "affected", "failure", "completeSelect", "prepare", "prepareIfNotSelected", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Select.kt */
    private final class AtomicSelectOp extends AtomicOp<Object> {
        @NotNull
        @JvmField
        public final AtomicDesc desc;
        @JvmField
        public final boolean select;
        final /* synthetic */ SelectBuilderImpl this$0;

        public AtomicSelectOp(@NotNull SelectBuilderImpl $outer, AtomicDesc desc2, boolean select2) {
            Intrinsics.checkParameterIsNotNull(desc2, "desc");
            this.this$0 = $outer;
            this.desc = desc2;
            this.select = select2;
        }

        @Nullable
        public Object prepare(@Nullable Object affected) {
            Object it;
            if (affected != null || (it = prepareIfNotSelected()) == null) {
                return this.desc.prepare(this);
            }
            return it;
        }

        public void complete(@Nullable Object affected, @Nullable Object failure) {
            completeSelect(failure);
            this.desc.complete(this, failure);
        }

        @Nullable
        public final Object prepareIfNotSelected() {
            SelectBuilderImpl $receiver$iv = this.this$0;
            while (true) {
                Object state = $receiver$iv._state;
                if (state == this) {
                    return null;
                }
                if (state instanceof OpDescriptor) {
                    ((OpDescriptor) state).perform(this.this$0);
                } else {
                    SelectBuilderImpl selectBuilderImpl = this.this$0;
                    if (state != selectBuilderImpl) {
                        return SelectKt.getALREADY_SELECTED();
                    }
                    if (SelectBuilderImpl._state$FU.compareAndSet(selectBuilderImpl, this.this$0, this)) {
                        return null;
                    }
                }
            }
        }

        private final void completeSelect(Object failure) {
            boolean selectSuccess = this.select && failure == null;
            if (SelectBuilderImpl._state$FU.compareAndSet(this.this$0, this, selectSuccess ? null : this.this$0) && selectSuccess) {
                this.this$0.doAfterSelect();
            }
        }
    }

    public void invoke(@NotNull SelectClause0 $this$invoke, @NotNull Function1<? super Continuation<? super R>, ? extends Object> block) {
        Intrinsics.checkParameterIsNotNull($this$invoke, "$this$invoke");
        Intrinsics.checkParameterIsNotNull(block, "block");
        $this$invoke.registerSelectClause0(this, block);
    }

    public <Q> void invoke(@NotNull SelectClause1<? extends Q> $this$invoke, @NotNull Function2<? super Q, ? super Continuation<? super R>, ? extends Object> block) {
        Intrinsics.checkParameterIsNotNull($this$invoke, "$this$invoke");
        Intrinsics.checkParameterIsNotNull(block, "block");
        $this$invoke.registerSelectClause1(this, block);
    }

    public <P, Q> void invoke(@NotNull SelectClause2<? super P, ? extends Q> $this$invoke, P param, @NotNull Function2<? super Q, ? super Continuation<? super R>, ? extends Object> block) {
        Intrinsics.checkParameterIsNotNull($this$invoke, "$this$invoke");
        Intrinsics.checkParameterIsNotNull(block, "block");
        $this$invoke.registerSelectClause2(this, param, block);
    }

    /* JADX INFO: Multiple debug info for r0v3 java.lang.Runnable: [D('$i$f$Runnable' int), D('action' java.lang.Runnable)] */
    public void onTimeout(long timeMillis, @NotNull Function1<? super Continuation<? super R>, ? extends Object> block) {
        Intrinsics.checkParameterIsNotNull(block, "block");
        if (timeMillis > 0) {
            disposeOnSelect(DelayKt.getDelay(getContext()).invokeOnTimeout(timeMillis, new SelectBuilderImpl$onTimeout$$inlined$Runnable$1(this, block)));
        } else if (trySelect(null)) {
            UndispatchedKt.startCoroutineUnintercepted(block, getCompletion());
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lkotlinx/coroutines/selects/SelectBuilderImpl$DisposeNode;", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "handle", "Lkotlinx/coroutines/DisposableHandle;", "(Lkotlinx/coroutines/DisposableHandle;)V", "kotlinx-coroutines-core"}, k = 1, mv = {1, 1, 15})
    /* compiled from: Select.kt */
    private static final class DisposeNode extends LockFreeLinkedListNode {
        @NotNull
        @JvmField
        public final DisposableHandle handle;

        public DisposeNode(@NotNull DisposableHandle handle2) {
            Intrinsics.checkParameterIsNotNull(handle2, "handle");
            this.handle = handle2;
        }
    }
}
