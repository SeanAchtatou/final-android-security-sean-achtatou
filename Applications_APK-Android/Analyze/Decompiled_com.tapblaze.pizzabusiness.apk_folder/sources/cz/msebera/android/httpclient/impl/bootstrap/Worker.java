package cz.msebera.android.httpclient.impl.bootstrap;

import cz.msebera.android.httpclient.ExceptionLogger;
import cz.msebera.android.httpclient.HttpServerConnection;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpCoreContext;
import cz.msebera.android.httpclient.protocol.HttpService;
import java.io.IOException;

class Worker implements Runnable {
    private final HttpServerConnection conn;
    private final ExceptionLogger exceptionLogger;
    private final HttpService httpservice;

    Worker(HttpService httpService, HttpServerConnection httpServerConnection, ExceptionLogger exceptionLogger2) {
        this.httpservice = httpService;
        this.conn = httpServerConnection;
        this.exceptionLogger = exceptionLogger2;
    }

    public HttpServerConnection getConnection() {
        return this.conn;
    }

    public void run() {
        try {
            BasicHttpContext basicHttpContext = new BasicHttpContext();
            HttpCoreContext adapt = HttpCoreContext.adapt(basicHttpContext);
            while (!Thread.interrupted() && this.conn.isOpen()) {
                this.httpservice.handleRequest(this.conn, adapt);
                basicHttpContext.clear();
            }
            this.conn.close();
            try {
                this.conn.shutdown();
            } catch (IOException e) {
                this.exceptionLogger.log(e);
            }
        } catch (Exception e2) {
            this.exceptionLogger.log(e2);
            this.conn.shutdown();
        } catch (Throwable th) {
            try {
                this.conn.shutdown();
            } catch (IOException e3) {
                this.exceptionLogger.log(e3);
            }
            throw th;
        }
    }
}
