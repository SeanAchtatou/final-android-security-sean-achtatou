package cz.msebera.android.httpclient.entity.mime;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.mime.content.ByteArrayBody;
import cz.msebera.android.httpclient.entity.mime.content.ContentBody;
import cz.msebera.android.httpclient.entity.mime.content.FileBody;
import cz.msebera.android.httpclient.entity.mime.content.InputStreamBody;
import cz.msebera.android.httpclient.entity.mime.content.StringBody;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.Args;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MultipartEntityBuilder {
    private static final String DEFAULT_SUBTYPE = "form-data";
    private static final char[] MULTIPART_CHARS = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private List<FormBodyPart> bodyParts = null;
    private String boundary = null;
    private Charset charset = null;
    private ContentType contentType;
    private HttpMultipartMode mode = HttpMultipartMode.STRICT;

    public static MultipartEntityBuilder create() {
        return new MultipartEntityBuilder();
    }

    MultipartEntityBuilder() {
    }

    public MultipartEntityBuilder setMode(HttpMultipartMode httpMultipartMode) {
        this.mode = httpMultipartMode;
        return this;
    }

    public MultipartEntityBuilder setLaxMode() {
        this.mode = HttpMultipartMode.BROWSER_COMPATIBLE;
        return this;
    }

    public MultipartEntityBuilder setStrictMode() {
        this.mode = HttpMultipartMode.STRICT;
        return this;
    }

    public MultipartEntityBuilder setBoundary(String str) {
        this.boundary = str;
        return this;
    }

    public MultipartEntityBuilder setMimeSubtype(String str) {
        Args.notBlank(str, "MIME subtype");
        this.contentType = ContentType.create("multipart/" + str);
        return this;
    }

    public MultipartEntityBuilder seContentType(ContentType contentType2) {
        Args.notNull(contentType2, "Content type");
        this.contentType = contentType2;
        return this;
    }

    public MultipartEntityBuilder setCharset(Charset charset2) {
        this.charset = charset2;
        return this;
    }

    public MultipartEntityBuilder addPart(FormBodyPart formBodyPart) {
        if (formBodyPart == null) {
            return this;
        }
        if (this.bodyParts == null) {
            this.bodyParts = new ArrayList();
        }
        this.bodyParts.add(formBodyPart);
        return this;
    }

    public MultipartEntityBuilder addPart(String str, ContentBody contentBody) {
        Args.notNull(str, "Name");
        Args.notNull(contentBody, "Content body");
        return addPart(FormBodyPartBuilder.create(str, contentBody).build());
    }

    public MultipartEntityBuilder addTextBody(String str, String str2, ContentType contentType2) {
        return addPart(str, new StringBody(str2, contentType2));
    }

    public MultipartEntityBuilder addTextBody(String str, String str2) {
        return addTextBody(str, str2, ContentType.DEFAULT_TEXT);
    }

    public MultipartEntityBuilder addBinaryBody(String str, byte[] bArr, ContentType contentType2, String str2) {
        return addPart(str, new ByteArrayBody(bArr, contentType2, str2));
    }

    public MultipartEntityBuilder addBinaryBody(String str, byte[] bArr) {
        return addBinaryBody(str, bArr, ContentType.DEFAULT_BINARY, (String) null);
    }

    public MultipartEntityBuilder addBinaryBody(String str, File file, ContentType contentType2, String str2) {
        return addPart(str, new FileBody(file, contentType2, str2));
    }

    public MultipartEntityBuilder addBinaryBody(String str, File file) {
        return addBinaryBody(str, file, ContentType.DEFAULT_BINARY, file != null ? file.getName() : null);
    }

    public MultipartEntityBuilder addBinaryBody(String str, InputStream inputStream, ContentType contentType2, String str2) {
        return addPart(str, new InputStreamBody(inputStream, contentType2, str2));
    }

    public MultipartEntityBuilder addBinaryBody(String str, InputStream inputStream) {
        return addBinaryBody(str, inputStream, ContentType.DEFAULT_BINARY, (String) null);
    }

    private String generateBoundary() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int nextInt = random.nextInt(11) + 30;
        for (int i = 0; i < nextInt; i++) {
            char[] cArr = MULTIPART_CHARS;
            sb.append(cArr[random.nextInt(cArr.length)]);
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public MultipartFormEntity buildEntity() {
        ContentType contentType2;
        List list;
        AbstractMultipartForm abstractMultipartForm;
        ContentType contentType3;
        ContentType contentType4;
        String str = this.boundary;
        if (str == null && (contentType4 = this.contentType) != null) {
            str = contentType4.getParameter("boundary");
        }
        if (str == null) {
            str = generateBoundary();
        }
        Charset charset2 = this.charset;
        if (charset2 == null && (contentType3 = this.contentType) != null) {
            charset2 = contentType3.getCharset();
        }
        ArrayList arrayList = new ArrayList(2);
        arrayList.add(new BasicNameValuePair("boundary", str));
        if (charset2 != null) {
            arrayList.add(new BasicNameValuePair(HttpRequest.PARAM_CHARSET, charset2.name()));
        }
        NameValuePair[] nameValuePairArr = (NameValuePair[]) arrayList.toArray(new NameValuePair[arrayList.size()]);
        ContentType contentType5 = this.contentType;
        if (contentType5 != null) {
            contentType2 = contentType5.withParameters(nameValuePairArr);
        } else {
            contentType2 = ContentType.create("multipart/form-data", nameValuePairArr);
        }
        List<FormBodyPart> list2 = this.bodyParts;
        if (list2 != null) {
            list = new ArrayList(list2);
        } else {
            list = Collections.emptyList();
        }
        HttpMultipartMode httpMultipartMode = this.mode;
        if (httpMultipartMode == null) {
            httpMultipartMode = HttpMultipartMode.STRICT;
        }
        int i = AnonymousClass1.$SwitchMap$cz$msebera$android$httpclient$entity$mime$HttpMultipartMode[httpMultipartMode.ordinal()];
        if (i == 1) {
            abstractMultipartForm = new HttpBrowserCompatibleMultipart(charset2, str, list);
        } else if (i != 2) {
            abstractMultipartForm = new HttpStrictMultipart(charset2, str, list);
        } else {
            abstractMultipartForm = new HttpRFC6532Multipart(charset2, str, list);
        }
        return new MultipartFormEntity(abstractMultipartForm, contentType2, abstractMultipartForm.getTotalLength());
    }

    /* renamed from: cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$cz$msebera$android$httpclient$entity$mime$HttpMultipartMode = new int[HttpMultipartMode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                cz.msebera.android.httpclient.entity.mime.HttpMultipartMode[] r0 = cz.msebera.android.httpclient.entity.mime.HttpMultipartMode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder.AnonymousClass1.$SwitchMap$cz$msebera$android$httpclient$entity$mime$HttpMultipartMode = r0
                int[] r0 = cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder.AnonymousClass1.$SwitchMap$cz$msebera$android$httpclient$entity$mime$HttpMultipartMode     // Catch:{ NoSuchFieldError -> 0x0014 }
                cz.msebera.android.httpclient.entity.mime.HttpMultipartMode r1 = cz.msebera.android.httpclient.entity.mime.HttpMultipartMode.BROWSER_COMPATIBLE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder.AnonymousClass1.$SwitchMap$cz$msebera$android$httpclient$entity$mime$HttpMultipartMode     // Catch:{ NoSuchFieldError -> 0x001f }
                cz.msebera.android.httpclient.entity.mime.HttpMultipartMode r1 = cz.msebera.android.httpclient.entity.mime.HttpMultipartMode.RFC6532     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder.AnonymousClass1.<clinit>():void");
        }
    }

    public HttpEntity build() {
        return buildEntity();
    }
}
