package cz.msebera.android.httpclient.pool;

import com.ironsource.sdk.constants.Constants;
import cz.msebera.android.httpclient.concurrent.FutureCallback;
import cz.msebera.android.httpclient.pool.PoolEntry;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.Asserts;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class AbstractConnPool<T, C, E extends PoolEntry<T, C>> implements ConnPool<T, E>, ConnPoolControl<T> {
    private final LinkedList<E> available = new LinkedList<>();
    private final ConnFactory<T, C> connFactory;
    private volatile int defaultMaxPerRoute;
    private volatile boolean isShutDown;
    private final Set<E> leased = new HashSet();
    private final Lock lock = new ReentrantLock();
    private final Map<T, Integer> maxPerRoute = new HashMap();
    private volatile int maxTotal;
    private final LinkedList<PoolEntryFuture<E>> pending = new LinkedList<>();
    private final Map<T, RouteSpecificPool<T, C, E>> routeToPool = new HashMap();
    private volatile int validateAfterInactivity;

    /* access modifiers changed from: protected */
    public abstract E createEntry(Object obj, Object obj2);

    /* access modifiers changed from: protected */
    public void onLease(E e) {
    }

    /* access modifiers changed from: protected */
    public void onRelease(E e) {
    }

    /* access modifiers changed from: protected */
    public void onReuse(E e) {
    }

    /* access modifiers changed from: protected */
    public boolean validate(PoolEntry poolEntry) {
        return true;
    }

    public AbstractConnPool(ConnFactory<T, C> connFactory2, int i, int i2) {
        this.connFactory = (ConnFactory) Args.notNull(connFactory2, "Connection factory");
        this.defaultMaxPerRoute = Args.positive(i, "Max per route value");
        this.maxTotal = Args.positive(i2, "Max total value");
    }

    public boolean isShutdown() {
        return this.isShutDown;
    }

    public void shutdown() throws IOException {
        if (!this.isShutDown) {
            this.isShutDown = true;
            this.lock.lock();
            try {
                Iterator<E> it = this.available.iterator();
                while (it.hasNext()) {
                    ((PoolEntry) it.next()).close();
                }
                for (E close : this.leased) {
                    close.close();
                }
                for (RouteSpecificPool<T, C, E> shutdown : this.routeToPool.values()) {
                    shutdown.shutdown();
                }
                this.routeToPool.clear();
                this.leased.clear();
                this.available.clear();
            } finally {
                this.lock.unlock();
            }
        }
    }

    private RouteSpecificPool<T, C, E> getPool(final T t) {
        RouteSpecificPool<T, C, E> routeSpecificPool = this.routeToPool.get(t);
        if (routeSpecificPool != null) {
            return routeSpecificPool;
        }
        AnonymousClass1 r0 = new RouteSpecificPool<T, C, E>(t) {
            /* access modifiers changed from: protected */
            public E createEntry(C c) {
                return AbstractConnPool.this.createEntry(t, c);
            }
        };
        this.routeToPool.put(t, r0);
        return r0;
    }

    public Future<E> lease(T t, Object obj, FutureCallback<E> futureCallback) {
        Args.notNull(t, "Route");
        Asserts.check(!this.isShutDown, "Connection pool shut down");
        final T t2 = t;
        final Object obj2 = obj;
        return new PoolEntryFuture<E>(this.lock, futureCallback) {
            public E getPoolEntry(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException, IOException {
                E access$000 = AbstractConnPool.this.getPoolEntryBlocking(t2, obj2, j, timeUnit, this);
                AbstractConnPool.this.onLease(access$000);
                return access$000;
            }
        };
    }

    public Future<E> lease(T t, Object obj) {
        return lease(t, obj, null);
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public E getPoolEntryBlocking(T r8, java.lang.Object r9, long r10, java.util.concurrent.TimeUnit r12, cz.msebera.android.httpclient.pool.PoolEntryFuture<E> r13) throws java.io.IOException, java.lang.InterruptedException, java.util.concurrent.TimeoutException {
        /*
            r7 = this;
            r0 = 0
            r1 = 0
            int r3 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r3 <= 0) goto L_0x0016
            java.util.Date r1 = new java.util.Date
            long r2 = java.lang.System.currentTimeMillis()
            long r10 = r12.toMillis(r10)
            long r2 = r2 + r10
            r1.<init>(r2)
            goto L_0x0017
        L_0x0016:
            r1 = r0
        L_0x0017:
            java.util.concurrent.locks.Lock r10 = r7.lock
            r10.lock()
            cz.msebera.android.httpclient.pool.RouteSpecificPool r10 = r7.getPool(r8)     // Catch:{ all -> 0x0138 }
        L_0x0020:
            if (r0 != 0) goto L_0x0130
            boolean r11 = r7.isShutDown     // Catch:{ all -> 0x0138 }
            r12 = 1
            r0 = 0
            if (r11 != 0) goto L_0x002a
            r11 = 1
            goto L_0x002b
        L_0x002a:
            r11 = 0
        L_0x002b:
            java.lang.String r2 = "Connection pool shut down"
            cz.msebera.android.httpclient.util.Asserts.check(r11, r2)     // Catch:{ all -> 0x0138 }
        L_0x0030:
            cz.msebera.android.httpclient.pool.PoolEntry r11 = r10.getFree(r9)     // Catch:{ all -> 0x0138 }
            if (r11 != 0) goto L_0x0037
            goto L_0x0071
        L_0x0037:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0138 }
            boolean r2 = r11.isExpired(r2)     // Catch:{ all -> 0x0138 }
            if (r2 == 0) goto L_0x0045
            r11.close()     // Catch:{ all -> 0x0138 }
            goto L_0x0062
        L_0x0045:
            int r2 = r7.validateAfterInactivity     // Catch:{ all -> 0x0138 }
            if (r2 <= 0) goto L_0x0062
            long r2 = r11.getUpdated()     // Catch:{ all -> 0x0138 }
            int r4 = r7.validateAfterInactivity     // Catch:{ all -> 0x0138 }
            long r4 = (long) r4     // Catch:{ all -> 0x0138 }
            long r2 = r2 + r4
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0138 }
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 > 0) goto L_0x0062
            boolean r2 = r7.validate(r11)     // Catch:{ all -> 0x0138 }
            if (r2 != 0) goto L_0x0062
            r11.close()     // Catch:{ all -> 0x0138 }
        L_0x0062:
            boolean r2 = r11.isClosed()     // Catch:{ all -> 0x0138 }
            if (r2 == 0) goto L_0x0071
            java.util.LinkedList<E> r2 = r7.available     // Catch:{ all -> 0x0138 }
            r2.remove(r11)     // Catch:{ all -> 0x0138 }
            r10.free(r11, r0)     // Catch:{ all -> 0x0138 }
            goto L_0x0030
        L_0x0071:
            if (r11 == 0) goto L_0x0086
            java.util.LinkedList<E> r8 = r7.available     // Catch:{ all -> 0x0138 }
            r8.remove(r11)     // Catch:{ all -> 0x0138 }
            java.util.Set<E> r8 = r7.leased     // Catch:{ all -> 0x0138 }
            r8.add(r11)     // Catch:{ all -> 0x0138 }
            r7.onReuse(r11)     // Catch:{ all -> 0x0138 }
            java.util.concurrent.locks.Lock r8 = r7.lock
            r8.unlock()
            return r11
        L_0x0086:
            int r2 = r7.getMax(r8)     // Catch:{ all -> 0x0138 }
            int r3 = r10.getAllocatedCount()     // Catch:{ all -> 0x0138 }
            int r3 = r3 + r12
            int r3 = r3 - r2
            int r3 = java.lang.Math.max(r0, r3)     // Catch:{ all -> 0x0138 }
            if (r3 <= 0) goto L_0x00ae
            r4 = 0
        L_0x0097:
            if (r4 >= r3) goto L_0x00ae
            cz.msebera.android.httpclient.pool.PoolEntry r5 = r10.getLastUsed()     // Catch:{ all -> 0x0138 }
            if (r5 != 0) goto L_0x00a0
            goto L_0x00ae
        L_0x00a0:
            r5.close()     // Catch:{ all -> 0x0138 }
            java.util.LinkedList<E> r6 = r7.available     // Catch:{ all -> 0x0138 }
            r6.remove(r5)     // Catch:{ all -> 0x0138 }
            r10.remove(r5)     // Catch:{ all -> 0x0138 }
            int r4 = r4 + 1
            goto L_0x0097
        L_0x00ae:
            int r3 = r10.getAllocatedCount()     // Catch:{ all -> 0x0138 }
            if (r3 >= r2) goto L_0x00ff
            java.util.Set<E> r2 = r7.leased     // Catch:{ all -> 0x0138 }
            int r2 = r2.size()     // Catch:{ all -> 0x0138 }
            int r3 = r7.maxTotal     // Catch:{ all -> 0x0138 }
            int r3 = r3 - r2
            int r0 = java.lang.Math.max(r3, r0)     // Catch:{ all -> 0x0138 }
            if (r0 <= 0) goto L_0x00ff
            java.util.LinkedList<E> r9 = r7.available     // Catch:{ all -> 0x0138 }
            int r9 = r9.size()     // Catch:{ all -> 0x0138 }
            int r0 = r0 - r12
            if (r9 <= r0) goto L_0x00ea
            java.util.LinkedList<E> r9 = r7.available     // Catch:{ all -> 0x0138 }
            boolean r9 = r9.isEmpty()     // Catch:{ all -> 0x0138 }
            if (r9 != 0) goto L_0x00ea
            java.util.LinkedList<E> r9 = r7.available     // Catch:{ all -> 0x0138 }
            java.lang.Object r9 = r9.removeLast()     // Catch:{ all -> 0x0138 }
            cz.msebera.android.httpclient.pool.PoolEntry r9 = (cz.msebera.android.httpclient.pool.PoolEntry) r9     // Catch:{ all -> 0x0138 }
            r9.close()     // Catch:{ all -> 0x0138 }
            java.lang.Object r11 = r9.getRoute()     // Catch:{ all -> 0x0138 }
            cz.msebera.android.httpclient.pool.RouteSpecificPool r11 = r7.getPool(r11)     // Catch:{ all -> 0x0138 }
            r11.remove(r9)     // Catch:{ all -> 0x0138 }
        L_0x00ea:
            cz.msebera.android.httpclient.pool.ConnFactory<T, C> r9 = r7.connFactory     // Catch:{ all -> 0x0138 }
            java.lang.Object r8 = r9.create(r8)     // Catch:{ all -> 0x0138 }
            cz.msebera.android.httpclient.pool.PoolEntry r8 = r10.add(r8)     // Catch:{ all -> 0x0138 }
            java.util.Set<E> r9 = r7.leased     // Catch:{ all -> 0x0138 }
            r9.add(r8)     // Catch:{ all -> 0x0138 }
            java.util.concurrent.locks.Lock r9 = r7.lock
            r9.unlock()
            return r8
        L_0x00ff:
            r10.queue(r13)     // Catch:{ all -> 0x0126 }
            java.util.LinkedList<cz.msebera.android.httpclient.pool.PoolEntryFuture<E>> r12 = r7.pending     // Catch:{ all -> 0x0126 }
            r12.add(r13)     // Catch:{ all -> 0x0126 }
            boolean r12 = r13.await(r1)     // Catch:{ all -> 0x0126 }
            r10.unqueue(r13)     // Catch:{ all -> 0x0138 }
            java.util.LinkedList<cz.msebera.android.httpclient.pool.PoolEntryFuture<E>> r0 = r7.pending     // Catch:{ all -> 0x0138 }
            r0.remove(r13)     // Catch:{ all -> 0x0138 }
            if (r12 != 0) goto L_0x0123
            if (r1 == 0) goto L_0x0123
            long r2 = r1.getTime()     // Catch:{ all -> 0x0138 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0138 }
            int r12 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r12 <= 0) goto L_0x0130
        L_0x0123:
            r0 = r11
            goto L_0x0020
        L_0x0126:
            r8 = move-exception
            r10.unqueue(r13)     // Catch:{ all -> 0x0138 }
            java.util.LinkedList<cz.msebera.android.httpclient.pool.PoolEntryFuture<E>> r9 = r7.pending     // Catch:{ all -> 0x0138 }
            r9.remove(r13)     // Catch:{ all -> 0x0138 }
            throw r8     // Catch:{ all -> 0x0138 }
        L_0x0130:
            java.util.concurrent.TimeoutException r8 = new java.util.concurrent.TimeoutException     // Catch:{ all -> 0x0138 }
            java.lang.String r9 = "Timeout waiting for connection"
            r8.<init>(r9)     // Catch:{ all -> 0x0138 }
            throw r8     // Catch:{ all -> 0x0138 }
        L_0x0138:
            r8 = move-exception
            java.util.concurrent.locks.Lock r9 = r7.lock
            r9.unlock()
            goto L_0x0140
        L_0x013f:
            throw r8
        L_0x0140:
            goto L_0x013f
        */
        throw new UnsupportedOperationException("Method not decompiled: cz.msebera.android.httpclient.pool.AbstractConnPool.getPoolEntryBlocking(java.lang.Object, java.lang.Object, long, java.util.concurrent.TimeUnit, cz.msebera.android.httpclient.pool.PoolEntryFuture):cz.msebera.android.httpclient.pool.PoolEntry");
    }

    public void release(PoolEntry poolEntry, boolean z) {
        this.lock.lock();
        try {
            if (this.leased.remove(poolEntry)) {
                RouteSpecificPool pool = getPool(poolEntry.getRoute());
                pool.free(poolEntry, z);
                if (!z || this.isShutDown) {
                    poolEntry.close();
                } else {
                    this.available.addFirst(poolEntry);
                    onRelease(poolEntry);
                }
                PoolEntryFuture nextPending = pool.nextPending();
                if (nextPending != null) {
                    this.pending.remove(nextPending);
                } else {
                    nextPending = this.pending.poll();
                }
                if (nextPending != null) {
                    nextPending.wakeup();
                }
            }
        } finally {
            this.lock.unlock();
        }
    }

    private int getMax(T t) {
        Integer num = this.maxPerRoute.get(t);
        if (num != null) {
            return num.intValue();
        }
        return this.defaultMaxPerRoute;
    }

    public void setMaxTotal(int i) {
        Args.positive(i, "Max value");
        this.lock.lock();
        try {
            this.maxTotal = i;
        } finally {
            this.lock.unlock();
        }
    }

    public int getMaxTotal() {
        this.lock.lock();
        try {
            return this.maxTotal;
        } finally {
            this.lock.unlock();
        }
    }

    public void setDefaultMaxPerRoute(int i) {
        Args.positive(i, "Max per route value");
        this.lock.lock();
        try {
            this.defaultMaxPerRoute = i;
        } finally {
            this.lock.unlock();
        }
    }

    public int getDefaultMaxPerRoute() {
        this.lock.lock();
        try {
            return this.defaultMaxPerRoute;
        } finally {
            this.lock.unlock();
        }
    }

    public void setMaxPerRoute(T t, int i) {
        Args.notNull(t, "Route");
        Args.positive(i, "Max per route value");
        this.lock.lock();
        try {
            this.maxPerRoute.put(t, Integer.valueOf(i));
        } finally {
            this.lock.unlock();
        }
    }

    public int getMaxPerRoute(T t) {
        Args.notNull(t, "Route");
        this.lock.lock();
        try {
            return getMax(t);
        } finally {
            this.lock.unlock();
        }
    }

    public PoolStats getTotalStats() {
        this.lock.lock();
        try {
            return new PoolStats(this.leased.size(), this.pending.size(), this.available.size(), this.maxTotal);
        } finally {
            this.lock.unlock();
        }
    }

    public PoolStats getStats(T t) {
        Args.notNull(t, "Route");
        this.lock.lock();
        try {
            RouteSpecificPool pool = getPool(t);
            return new PoolStats(pool.getLeasedCount(), pool.getPendingCount(), pool.getAvailableCount(), getMax(t));
        } finally {
            this.lock.unlock();
        }
    }

    public Set<T> getRoutes() {
        this.lock.lock();
        try {
            return new HashSet(this.routeToPool.keySet());
        } finally {
            this.lock.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public void enumAvailable(PoolEntryCallback<T, C> poolEntryCallback) {
        this.lock.lock();
        try {
            Iterator<E> it = this.available.iterator();
            while (it.hasNext()) {
                PoolEntry poolEntry = (PoolEntry) it.next();
                poolEntryCallback.process(poolEntry);
                if (poolEntry.isClosed()) {
                    getPool(poolEntry.getRoute()).remove(poolEntry);
                    it.remove();
                }
            }
            purgePoolMap();
        } finally {
            this.lock.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public void enumLeased(PoolEntryCallback<T, C> poolEntryCallback) {
        this.lock.lock();
        try {
            for (E process : this.leased) {
                poolEntryCallback.process(process);
            }
        } finally {
            this.lock.unlock();
        }
    }

    private void purgePoolMap() {
        Iterator<Map.Entry<T, RouteSpecificPool<T, C, E>>> it = this.routeToPool.entrySet().iterator();
        while (it.hasNext()) {
            RouteSpecificPool routeSpecificPool = (RouteSpecificPool) it.next().getValue();
            if (routeSpecificPool.getPendingCount() + routeSpecificPool.getAllocatedCount() == 0) {
                it.remove();
            }
        }
    }

    public void closeIdle(long j, TimeUnit timeUnit) {
        Args.notNull(timeUnit, "Time unit");
        long millis = timeUnit.toMillis(j);
        if (millis < 0) {
            millis = 0;
        }
        final long currentTimeMillis = System.currentTimeMillis() - millis;
        enumAvailable(new PoolEntryCallback<T, C>() {
            public void process(PoolEntry<T, C> poolEntry) {
                if (poolEntry.getUpdated() <= currentTimeMillis) {
                    poolEntry.close();
                }
            }
        });
    }

    public void closeExpired() {
        final long currentTimeMillis = System.currentTimeMillis();
        enumAvailable(new PoolEntryCallback<T, C>() {
            public void process(PoolEntry<T, C> poolEntry) {
                if (poolEntry.isExpired(currentTimeMillis)) {
                    poolEntry.close();
                }
            }
        });
    }

    public int getValidateAfterInactivity() {
        return this.validateAfterInactivity;
    }

    public void setValidateAfterInactivity(int i) {
        this.validateAfterInactivity = i;
    }

    public String toString() {
        return "[leased: " + this.leased + "][available: " + this.available + "][pending: " + this.pending + Constants.RequestParameters.RIGHT_BRACKETS;
    }
}
