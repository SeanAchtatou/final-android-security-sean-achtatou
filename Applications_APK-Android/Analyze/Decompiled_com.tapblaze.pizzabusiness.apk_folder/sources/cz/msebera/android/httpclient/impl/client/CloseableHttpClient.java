package cz.msebera.android.httpclient.impl.client;

import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.utils.URIUtils;
import cz.msebera.android.httpclient.extras.HttpClientAndroidLog;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.EntityUtils;
import java.io.Closeable;
import java.io.IOException;
import java.net.URI;

public abstract class CloseableHttpClient implements HttpClient, Closeable {
    public HttpClientAndroidLog log = new HttpClientAndroidLog(getClass());

    /* access modifiers changed from: protected */
    public abstract CloseableHttpResponse doExecute(HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext) throws IOException, ClientProtocolException;

    public CloseableHttpResponse execute(HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext) throws IOException, ClientProtocolException {
        return doExecute(httpHost, httpRequest, httpContext);
    }

    public CloseableHttpResponse execute(HttpUriRequest httpUriRequest, HttpContext httpContext) throws IOException, ClientProtocolException {
        Args.notNull(httpUriRequest, "HTTP request");
        return doExecute(determineTarget(httpUriRequest), httpUriRequest, httpContext);
    }

    private static HttpHost determineTarget(HttpUriRequest httpUriRequest) throws ClientProtocolException {
        URI uri = httpUriRequest.getURI();
        if (!uri.isAbsolute()) {
            return null;
        }
        HttpHost extractHost = URIUtils.extractHost(uri);
        if (extractHost != null) {
            return extractHost;
        }
        throw new ClientProtocolException("URI does not specify a valid host name: " + uri);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.client.methods.HttpUriRequest, cz.msebera.android.httpclient.protocol.HttpContext):cz.msebera.android.httpclient.client.methods.CloseableHttpResponse
     arg types: [cz.msebera.android.httpclient.client.methods.HttpUriRequest, ?[OBJECT, ARRAY]]
     candidates:
      cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest):cz.msebera.android.httpclient.HttpResponse
      cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.client.methods.HttpUriRequest, cz.msebera.android.httpclient.protocol.HttpContext):cz.msebera.android.httpclient.HttpResponse
      cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest):cz.msebera.android.httpclient.client.methods.CloseableHttpResponse
      cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.client.methods.HttpUriRequest, cz.msebera.android.httpclient.client.ResponseHandler):T
      cz.msebera.android.httpclient.client.HttpClient.execute(cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest):cz.msebera.android.httpclient.HttpResponse
      cz.msebera.android.httpclient.client.HttpClient.execute(cz.msebera.android.httpclient.client.methods.HttpUriRequest, cz.msebera.android.httpclient.protocol.HttpContext):cz.msebera.android.httpclient.HttpResponse
      cz.msebera.android.httpclient.client.HttpClient.execute(cz.msebera.android.httpclient.client.methods.HttpUriRequest, cz.msebera.android.httpclient.client.ResponseHandler):T
      cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.client.methods.HttpUriRequest, cz.msebera.android.httpclient.protocol.HttpContext):cz.msebera.android.httpclient.client.methods.CloseableHttpResponse */
    public CloseableHttpResponse execute(HttpUriRequest httpUriRequest) throws IOException, ClientProtocolException {
        return execute(httpUriRequest, (HttpContext) null);
    }

    public CloseableHttpResponse execute(HttpHost httpHost, HttpRequest httpRequest) throws IOException, ClientProtocolException {
        return doExecute(httpHost, httpRequest, null);
    }

    public <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler responseHandler) throws IOException, ClientProtocolException {
        return execute(httpUriRequest, responseHandler, (HttpContext) null);
    }

    public <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) throws IOException, ClientProtocolException {
        return execute(determineTarget(httpUriRequest), httpUriRequest, responseHandler, httpContext);
    }

    public <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler) throws IOException, ClientProtocolException {
        return execute(httpHost, httpRequest, responseHandler, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest, cz.msebera.android.httpclient.protocol.HttpContext):cz.msebera.android.httpclient.client.methods.CloseableHttpResponse
     arg types: [cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest, cz.msebera.android.httpclient.protocol.HttpContext]
     candidates:
      cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest, cz.msebera.android.httpclient.protocol.HttpContext):cz.msebera.android.httpclient.HttpResponse
      cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest, cz.msebera.android.httpclient.client.ResponseHandler):T
      cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.client.methods.HttpUriRequest, cz.msebera.android.httpclient.client.ResponseHandler, cz.msebera.android.httpclient.protocol.HttpContext):T
      cz.msebera.android.httpclient.client.HttpClient.execute(cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest, cz.msebera.android.httpclient.protocol.HttpContext):cz.msebera.android.httpclient.HttpResponse
      cz.msebera.android.httpclient.client.HttpClient.execute(cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest, cz.msebera.android.httpclient.client.ResponseHandler):T
      cz.msebera.android.httpclient.client.HttpClient.execute(cz.msebera.android.httpclient.client.methods.HttpUriRequest, cz.msebera.android.httpclient.client.ResponseHandler, cz.msebera.android.httpclient.protocol.HttpContext):T
      cz.msebera.android.httpclient.impl.client.CloseableHttpClient.execute(cz.msebera.android.httpclient.HttpHost, cz.msebera.android.httpclient.HttpRequest, cz.msebera.android.httpclient.protocol.HttpContext):cz.msebera.android.httpclient.client.methods.CloseableHttpResponse */
    public <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) throws IOException, ClientProtocolException {
        Args.notNull(responseHandler, "Response handler");
        CloseableHttpResponse execute = execute(httpHost, httpRequest, httpContext);
        try {
            T handleResponse = responseHandler.handleResponse(execute);
            EntityUtils.consume(execute.getEntity());
            execute.close();
            return handleResponse;
        } catch (ClientProtocolException e) {
            try {
                EntityUtils.consume(execute.getEntity());
            } catch (Exception e2) {
                this.log.warn("Error consuming content after an exception.", e2);
            }
            throw e;
        } catch (Throwable th) {
            execute.close();
            throw th;
        }
    }
}
