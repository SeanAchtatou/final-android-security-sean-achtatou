package cz.msebera.android.httpclient.impl.bootstrap;

import cz.msebera.android.httpclient.ConnectionReuseStrategy;
import cz.msebera.android.httpclient.ExceptionLogger;
import cz.msebera.android.httpclient.HttpConnectionFactory;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponseFactory;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.config.ConnectionConfig;
import cz.msebera.android.httpclient.config.SocketConfig;
import cz.msebera.android.httpclient.impl.DefaultBHttpServerConnection;
import cz.msebera.android.httpclient.protocol.HttpExpectationVerifier;
import cz.msebera.android.httpclient.protocol.HttpProcessor;
import cz.msebera.android.httpclient.protocol.HttpRequestHandler;
import cz.msebera.android.httpclient.protocol.HttpRequestHandlerMapper;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLContext;

public class ServerBootstrap {
    private ConnectionReuseStrategy connStrategy;
    private ConnectionConfig connectionConfig;
    private HttpConnectionFactory<? extends DefaultBHttpServerConnection> connectionFactory;
    private ExceptionLogger exceptionLogger;
    private HttpExpectationVerifier expectationVerifier;
    private Map<String, HttpRequestHandler> handlerMap;
    private HttpRequestHandlerMapper handlerMapper;
    private HttpProcessor httpProcessor;
    private int listenerPort;
    private InetAddress localAddress;
    private LinkedList<HttpRequestInterceptor> requestFirst;
    private LinkedList<HttpRequestInterceptor> requestLast;
    private HttpResponseFactory responseFactory;
    private LinkedList<HttpResponseInterceptor> responseFirst;
    private LinkedList<HttpResponseInterceptor> responseLast;
    private String serverInfo;
    private ServerSocketFactory serverSocketFactory;
    private SocketConfig socketConfig;
    private SSLContext sslContext;
    private SSLServerSetupHandler sslSetupHandler;

    private ServerBootstrap() {
    }

    public static ServerBootstrap bootstrap() {
        return new ServerBootstrap();
    }

    public final ServerBootstrap setListenerPort(int i) {
        this.listenerPort = i;
        return this;
    }

    public final ServerBootstrap setLocalAddress(InetAddress inetAddress) {
        this.localAddress = inetAddress;
        return this;
    }

    public final ServerBootstrap setSocketConfig(SocketConfig socketConfig2) {
        this.socketConfig = socketConfig2;
        return this;
    }

    public final ServerBootstrap setConnectionConfig(ConnectionConfig connectionConfig2) {
        this.connectionConfig = connectionConfig2;
        return this;
    }

    public final ServerBootstrap setHttpProcessor(HttpProcessor httpProcessor2) {
        this.httpProcessor = httpProcessor2;
        return this;
    }

    public final ServerBootstrap addInterceptorFirst(HttpResponseInterceptor httpResponseInterceptor) {
        if (httpResponseInterceptor == null) {
            return this;
        }
        if (this.responseFirst == null) {
            this.responseFirst = new LinkedList<>();
        }
        this.responseFirst.addFirst(httpResponseInterceptor);
        return this;
    }

    public final ServerBootstrap addInterceptorLast(HttpResponseInterceptor httpResponseInterceptor) {
        if (httpResponseInterceptor == null) {
            return this;
        }
        if (this.responseLast == null) {
            this.responseLast = new LinkedList<>();
        }
        this.responseLast.addLast(httpResponseInterceptor);
        return this;
    }

    public final ServerBootstrap addInterceptorFirst(HttpRequestInterceptor httpRequestInterceptor) {
        if (httpRequestInterceptor == null) {
            return this;
        }
        if (this.requestFirst == null) {
            this.requestFirst = new LinkedList<>();
        }
        this.requestFirst.addFirst(httpRequestInterceptor);
        return this;
    }

    public final ServerBootstrap addInterceptorLast(HttpRequestInterceptor httpRequestInterceptor) {
        if (httpRequestInterceptor == null) {
            return this;
        }
        if (this.requestLast == null) {
            this.requestLast = new LinkedList<>();
        }
        this.requestLast.addLast(httpRequestInterceptor);
        return this;
    }

    public final ServerBootstrap setServerInfo(String str) {
        this.serverInfo = str;
        return this;
    }

    public final ServerBootstrap setConnectionReuseStrategy(ConnectionReuseStrategy connectionReuseStrategy) {
        this.connStrategy = connectionReuseStrategy;
        return this;
    }

    public final ServerBootstrap setResponseFactory(HttpResponseFactory httpResponseFactory) {
        this.responseFactory = httpResponseFactory;
        return this;
    }

    public final ServerBootstrap setHandlerMapper(HttpRequestHandlerMapper httpRequestHandlerMapper) {
        this.handlerMapper = httpRequestHandlerMapper;
        return this;
    }

    public final ServerBootstrap registerHandler(String str, HttpRequestHandler httpRequestHandler) {
        if (!(str == null || httpRequestHandler == null)) {
            if (this.handlerMap == null) {
                this.handlerMap = new HashMap();
            }
            this.handlerMap.put(str, httpRequestHandler);
        }
        return this;
    }

    public final ServerBootstrap setExpectationVerifier(HttpExpectationVerifier httpExpectationVerifier) {
        this.expectationVerifier = httpExpectationVerifier;
        return this;
    }

    public final ServerBootstrap setConnectionFactory(HttpConnectionFactory<? extends DefaultBHttpServerConnection> httpConnectionFactory) {
        this.connectionFactory = httpConnectionFactory;
        return this;
    }

    public final ServerBootstrap setSslSetupHandler(SSLServerSetupHandler sSLServerSetupHandler) {
        this.sslSetupHandler = sSLServerSetupHandler;
        return this;
    }

    public final ServerBootstrap setServerSocketFactory(ServerSocketFactory serverSocketFactory2) {
        this.serverSocketFactory = serverSocketFactory2;
        return this;
    }

    public final ServerBootstrap setSslContext(SSLContext sSLContext) {
        this.sslContext = sSLContext;
        return this;
    }

    public final ServerBootstrap setExceptionLogger(ExceptionLogger exceptionLogger2) {
        this.exceptionLogger = exceptionLogger2;
        return this;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:73:0x00cc */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: cz.msebera.android.httpclient.protocol.HttpRequestHandlerMapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: cz.msebera.android.httpclient.protocol.UriHttpRequestHandlerMapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v2, resolved type: cz.msebera.android.httpclient.impl.DefaultBHttpServerConnectionFactory} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v23, resolved type: cz.msebera.android.httpclient.protocol.UriHttpRequestHandlerMapper} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v26, resolved type: cz.msebera.android.httpclient.protocol.UriHttpRequestHandlerMapper} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x011f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public cz.msebera.android.httpclient.impl.bootstrap.HttpServer create() {
        /*
            r17 = this;
            r0 = r17
            cz.msebera.android.httpclient.protocol.HttpProcessor r1 = r0.httpProcessor
            r2 = 0
            if (r1 != 0) goto L_0x009a
            cz.msebera.android.httpclient.protocol.HttpProcessorBuilder r1 = cz.msebera.android.httpclient.protocol.HttpProcessorBuilder.create()
            java.util.LinkedList<cz.msebera.android.httpclient.HttpRequestInterceptor> r3 = r0.requestFirst
            if (r3 == 0) goto L_0x0023
            java.util.Iterator r3 = r3.iterator()
        L_0x0013:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0023
            java.lang.Object r4 = r3.next()
            cz.msebera.android.httpclient.HttpRequestInterceptor r4 = (cz.msebera.android.httpclient.HttpRequestInterceptor) r4
            r1.addFirst(r4)
            goto L_0x0013
        L_0x0023:
            java.util.LinkedList<cz.msebera.android.httpclient.HttpResponseInterceptor> r3 = r0.responseFirst
            if (r3 == 0) goto L_0x003b
            java.util.Iterator r3 = r3.iterator()
        L_0x002b:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x003b
            java.lang.Object r4 = r3.next()
            cz.msebera.android.httpclient.HttpResponseInterceptor r4 = (cz.msebera.android.httpclient.HttpResponseInterceptor) r4
            r1.addFirst(r4)
            goto L_0x002b
        L_0x003b:
            java.lang.String r3 = r0.serverInfo
            if (r3 != 0) goto L_0x0041
            java.lang.String r3 = "Apache-HttpCore/1.1"
        L_0x0041:
            r4 = 4
            cz.msebera.android.httpclient.HttpResponseInterceptor[] r4 = new cz.msebera.android.httpclient.HttpResponseInterceptor[r4]
            cz.msebera.android.httpclient.protocol.ResponseDate r5 = new cz.msebera.android.httpclient.protocol.ResponseDate
            r5.<init>()
            r4[r2] = r5
            r5 = 1
            cz.msebera.android.httpclient.protocol.ResponseServer r6 = new cz.msebera.android.httpclient.protocol.ResponseServer
            r6.<init>(r3)
            r4[r5] = r6
            r3 = 2
            cz.msebera.android.httpclient.protocol.ResponseContent r5 = new cz.msebera.android.httpclient.protocol.ResponseContent
            r5.<init>()
            r4[r3] = r5
            r3 = 3
            cz.msebera.android.httpclient.protocol.ResponseConnControl r5 = new cz.msebera.android.httpclient.protocol.ResponseConnControl
            r5.<init>()
            r4[r3] = r5
            r1.addAll(r4)
            java.util.LinkedList<cz.msebera.android.httpclient.HttpRequestInterceptor> r3 = r0.requestLast
            if (r3 == 0) goto L_0x007e
            java.util.Iterator r3 = r3.iterator()
        L_0x006e:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x007e
            java.lang.Object r4 = r3.next()
            cz.msebera.android.httpclient.HttpRequestInterceptor r4 = (cz.msebera.android.httpclient.HttpRequestInterceptor) r4
            r1.addLast(r4)
            goto L_0x006e
        L_0x007e:
            java.util.LinkedList<cz.msebera.android.httpclient.HttpResponseInterceptor> r3 = r0.responseLast
            if (r3 == 0) goto L_0x0096
            java.util.Iterator r3 = r3.iterator()
        L_0x0086:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0096
            java.lang.Object r4 = r3.next()
            cz.msebera.android.httpclient.HttpResponseInterceptor r4 = (cz.msebera.android.httpclient.HttpResponseInterceptor) r4
            r1.addLast(r4)
            goto L_0x0086
        L_0x0096:
            cz.msebera.android.httpclient.protocol.HttpProcessor r1 = r1.build()
        L_0x009a:
            r4 = r1
            cz.msebera.android.httpclient.protocol.HttpRequestHandlerMapper r1 = r0.handlerMapper
            if (r1 != 0) goto L_0x00cc
            cz.msebera.android.httpclient.protocol.UriHttpRequestHandlerMapper r1 = new cz.msebera.android.httpclient.protocol.UriHttpRequestHandlerMapper
            r1.<init>()
            java.util.Map<java.lang.String, cz.msebera.android.httpclient.protocol.HttpRequestHandler> r3 = r0.handlerMap
            if (r3 == 0) goto L_0x00cc
            java.util.Set r3 = r3.entrySet()
            java.util.Iterator r3 = r3.iterator()
        L_0x00b0:
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L_0x00cc
            java.lang.Object r5 = r3.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            java.lang.Object r6 = r5.getKey()
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r5 = r5.getValue()
            cz.msebera.android.httpclient.protocol.HttpRequestHandler r5 = (cz.msebera.android.httpclient.protocol.HttpRequestHandler) r5
            r1.register(r6, r5)
            goto L_0x00b0
        L_0x00cc:
            r7 = r1
            cz.msebera.android.httpclient.ConnectionReuseStrategy r1 = r0.connStrategy
            if (r1 != 0) goto L_0x00d3
            cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy r1 = cz.msebera.android.httpclient.impl.DefaultConnectionReuseStrategy.INSTANCE
        L_0x00d3:
            r5 = r1
            cz.msebera.android.httpclient.HttpResponseFactory r1 = r0.responseFactory
            if (r1 != 0) goto L_0x00da
            cz.msebera.android.httpclient.impl.DefaultHttpResponseFactory r1 = cz.msebera.android.httpclient.impl.DefaultHttpResponseFactory.INSTANCE
        L_0x00da:
            r6 = r1
            cz.msebera.android.httpclient.protocol.HttpService r13 = new cz.msebera.android.httpclient.protocol.HttpService
            cz.msebera.android.httpclient.protocol.HttpExpectationVerifier r8 = r0.expectationVerifier
            r3 = r13
            r3.<init>(r4, r5, r6, r7, r8)
            javax.net.ServerSocketFactory r1 = r0.serverSocketFactory
            if (r1 != 0) goto L_0x00f4
            javax.net.ssl.SSLContext r1 = r0.sslContext
            if (r1 == 0) goto L_0x00f0
            javax.net.ssl.SSLServerSocketFactory r1 = r1.getServerSocketFactory()
            goto L_0x00f4
        L_0x00f0:
            javax.net.ServerSocketFactory r1 = javax.net.ServerSocketFactory.getDefault()
        L_0x00f4:
            r12 = r1
            cz.msebera.android.httpclient.HttpConnectionFactory<? extends cz.msebera.android.httpclient.impl.DefaultBHttpServerConnection> r1 = r0.connectionFactory
            if (r1 != 0) goto L_0x0106
            cz.msebera.android.httpclient.config.ConnectionConfig r1 = r0.connectionConfig
            if (r1 == 0) goto L_0x0104
            cz.msebera.android.httpclient.impl.DefaultBHttpServerConnectionFactory r3 = new cz.msebera.android.httpclient.impl.DefaultBHttpServerConnectionFactory
            r3.<init>(r1)
            r14 = r3
            goto L_0x0107
        L_0x0104:
            cz.msebera.android.httpclient.impl.DefaultBHttpServerConnectionFactory r1 = cz.msebera.android.httpclient.impl.DefaultBHttpServerConnectionFactory.INSTANCE
        L_0x0106:
            r14 = r1
        L_0x0107:
            cz.msebera.android.httpclient.ExceptionLogger r1 = r0.exceptionLogger
            if (r1 != 0) goto L_0x010d
            cz.msebera.android.httpclient.ExceptionLogger r1 = cz.msebera.android.httpclient.ExceptionLogger.NO_OP
        L_0x010d:
            r16 = r1
            cz.msebera.android.httpclient.impl.bootstrap.HttpServer r1 = new cz.msebera.android.httpclient.impl.bootstrap.HttpServer
            int r3 = r0.listenerPort
            if (r3 <= 0) goto L_0x0117
            r9 = r3
            goto L_0x0118
        L_0x0117:
            r9 = 0
        L_0x0118:
            java.net.InetAddress r10 = r0.localAddress
            cz.msebera.android.httpclient.config.SocketConfig r2 = r0.socketConfig
            if (r2 == 0) goto L_0x011f
            goto L_0x0121
        L_0x011f:
            cz.msebera.android.httpclient.config.SocketConfig r2 = cz.msebera.android.httpclient.config.SocketConfig.DEFAULT
        L_0x0121:
            r11 = r2
            cz.msebera.android.httpclient.impl.bootstrap.SSLServerSetupHandler r15 = r0.sslSetupHandler
            r8 = r1
            r8.<init>(r9, r10, r11, r12, r13, r14, r15, r16)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: cz.msebera.android.httpclient.impl.bootstrap.ServerBootstrap.create():cz.msebera.android.httpclient.impl.bootstrap.HttpServer");
    }
}
