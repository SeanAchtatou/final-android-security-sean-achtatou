package cz.msebera.android.httpclient.client.entity;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.utils.URLEncodedUtils;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.protocol.HTTP;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.List;

public class UrlEncodedFormEntity extends StringEntity {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public UrlEncodedFormEntity(java.util.List<? extends cz.msebera.android.httpclient.NameValuePair> r2, java.lang.String r3) throws java.io.UnsupportedEncodingException {
        /*
            r1 = this;
            if (r3 == 0) goto L_0x0004
            r0 = r3
            goto L_0x000a
        L_0x0004:
            java.nio.charset.Charset r0 = cz.msebera.android.httpclient.protocol.HTTP.DEF_CONTENT_CHARSET
            java.lang.String r0 = r0.name()
        L_0x000a:
            java.lang.String r2 = cz.msebera.android.httpclient.client.utils.URLEncodedUtils.format(r2, r0)
            java.lang.String r0 = "application/x-www-form-urlencoded"
            cz.msebera.android.httpclient.entity.ContentType r3 = cz.msebera.android.httpclient.entity.ContentType.create(r0, r3)
            r1.<init>(r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity.<init>(java.util.List, java.lang.String):void");
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UrlEncodedFormEntity(Iterable<? extends NameValuePair> iterable, Charset charset) {
        super(URLEncodedUtils.format(iterable, charset != null ? charset : HTTP.DEF_CONTENT_CHARSET), ContentType.create("application/x-www-form-urlencoded", charset));
    }

    public UrlEncodedFormEntity(List<? extends NameValuePair> list) throws UnsupportedEncodingException {
        this(list, (Charset) null);
    }

    public UrlEncodedFormEntity(Iterable<? extends NameValuePair> iterable) {
        this(iterable, (Charset) null);
    }
}
