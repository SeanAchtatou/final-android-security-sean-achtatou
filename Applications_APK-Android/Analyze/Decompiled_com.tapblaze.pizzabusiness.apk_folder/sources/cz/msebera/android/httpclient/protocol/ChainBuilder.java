package cz.msebera.android.httpclient.protocol;

import cz.msebera.android.httpclient.HttpRequestInterceptor;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

final class ChainBuilder<E> {
    private final LinkedList<E> list = new LinkedList<>();
    private final Map<Class<?>, E> uniqueClasses = new HashMap();

    private void ensureUnique(E e) {
        E remove = this.uniqueClasses.remove(e.getClass());
        if (remove != null) {
            this.list.remove(remove);
        }
        this.uniqueClasses.put(e.getClass(), e);
    }

    public ChainBuilder<E> addFirst(E e) {
        if (e == null) {
            return this;
        }
        ensureUnique(e);
        this.list.addFirst(e);
        return this;
    }

    public ChainBuilder<E> addLast(E e) {
        if (e == null) {
            return this;
        }
        ensureUnique(e);
        this.list.addLast(e);
        return this;
    }

    public ChainBuilder<E> addAllFirst(Collection<HttpRequestInterceptor> collection) {
        if (collection == null) {
            return this;
        }
        for (HttpRequestInterceptor addFirst : collection) {
            addFirst(addFirst);
        }
        return this;
    }

    public ChainBuilder<E> addAllFirst(Object... objArr) {
        if (objArr == null) {
            return this;
        }
        for (Object addFirst : objArr) {
            addFirst(addFirst);
        }
        return this;
    }

    public ChainBuilder<E> addAllLast(Collection<HttpRequestInterceptor> collection) {
        if (collection == null) {
            return this;
        }
        for (HttpRequestInterceptor addLast : collection) {
            addLast(addLast);
        }
        return this;
    }

    public ChainBuilder<E> addAllLast(Object... objArr) {
        if (objArr == null) {
            return this;
        }
        for (Object addLast : objArr) {
            addLast(addLast);
        }
        return this;
    }

    public LinkedList<E> build() {
        return new LinkedList<>(this.list);
    }
}
