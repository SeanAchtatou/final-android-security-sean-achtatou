package cz.msebera.android.httpclient.impl.conn;

public class ConnectionShutdownException extends IllegalStateException {
    private static final long serialVersionUID = 5868657401162844497L;
}
