package cz.msebera.android.httpclient.auth;

import com.ironsource.sdk.constants.Constants;
import cz.msebera.android.httpclient.util.Args;
import cz.msebera.android.httpclient.util.LangUtils;
import java.io.Serializable;
import java.security.Principal;

public final class BasicUserPrincipal implements Principal, Serializable {
    private static final long serialVersionUID = -2266305184969850467L;
    private final String username;

    public BasicUserPrincipal(String str) {
        Args.notNull(str, "User name");
        this.username = str;
    }

    public String getName() {
        return this.username;
    }

    public int hashCode() {
        return LangUtils.hashCode(17, this.username);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BasicUserPrincipal) || !LangUtils.equals(this.username, ((BasicUserPrincipal) obj).username)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "[principal: " + this.username + Constants.RequestParameters.RIGHT_BRACKETS;
    }
}
