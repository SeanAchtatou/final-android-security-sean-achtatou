package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.Collections;
import java.util.Map;
import org.json.JSONObject;

public class ad extends ae {
    private final f a;
    private final AppLovinAdRewardListener c;

    public ad(f fVar, AppLovinAdRewardListener appLovinAdRewardListener, i iVar) {
        super("TaskValidateAppLovinReward", iVar);
        this.a = fVar;
        this.c = appLovinAdRewardListener;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.z;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        String str;
        if (!c()) {
            if (i < 400 || i >= 500) {
                this.c.validationRequestFailed(this.a, i);
                str = "network_timeout";
            } else {
                this.c.userRewardRejected(this.a, Collections.emptyMap());
                str = "rejected";
            }
            this.a.a(c.a(str));
        }
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        if (!c()) {
            this.a.a(cVar);
            String b = cVar.b();
            Map<String, String> a2 = cVar.a();
            if (b.equals("accepted")) {
                this.c.userRewardVerified(this.a, a2);
            } else if (b.equals("quota_exceeded")) {
                this.c.userOverQuota(this.a, a2);
            } else if (b.equals("rejected")) {
                this.c.userRewardRejected(this.a, a2);
            } else {
                this.c.validationRequestFailed(this.a, AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "zone_id", this.a.getAdZone().a(), this.b);
        String clCode = this.a.getClCode();
        if (!m.b(clCode)) {
            clCode = "NO_CLCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, "clcode", clCode, this.b);
    }

    public String b() {
        return "2.0/vr";
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.a.aA();
    }
}
