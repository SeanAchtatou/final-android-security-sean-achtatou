package com.applovin.impl.sdk;

import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.utils.i;
import org.json.JSONObject;

public class s {
    private final i a;
    private final JSONObject b;
    private final Object c = new Object();

    public s(i iVar) {
        this.a = iVar;
        this.b = i.a((String) iVar.b(e.q, "{}"), new JSONObject(), iVar);
    }

    public Integer a(String str) {
        Integer valueOf;
        synchronized (this.c) {
            if (this.b.has(str)) {
                i.a(this.b, str, i.b(this.b, str, 0, this.a) + 1, this.a);
            } else {
                i.a(this.b, str, 1, this.a);
            }
            this.a.a(e.q, this.b.toString());
            valueOf = Integer.valueOf(i.b(this.b, str, 0, this.a));
        }
        return valueOf;
    }
}
