package com.applovin.impl.sdk;

import android.util.Log;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.utils.m;
import com.ironsource.sdk.constants.Constants;

public class o {
    private final i a;

    o(i iVar) {
        this.a = iVar;
    }

    private boolean a() {
        return this.a.C().d();
    }

    public static void c(String str, String str2, Throwable th) {
        Log.e("AppLovinSdk", Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2, th);
    }

    public static void f(String str, String str2) {
        Log.d("AppLovinSdk", Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2);
    }

    public static void g(String str, String str2) {
        Log.i("AppLovinSdk", Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2);
    }

    public static void h(String str, String str2) {
        Log.w("AppLovinSdk", Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2);
    }

    public static void i(String str, String str2) {
        c(str, str2, null);
    }

    private void j(String str, String str2) {
    }

    public void a(String str, Boolean bool, String str2) {
        a(str, bool, str2, null);
    }

    public void a(String str, Boolean bool, String str2, Throwable th) {
        if (a()) {
            String str3 = Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2;
            Log.e("AppLovinSdk", str3, th);
            j("ERROR", str3 + " : " + th);
        }
        if (bool.booleanValue() && ((Boolean) this.a.a(c.ew)).booleanValue() && this.a.P() != null) {
            this.a.P().a(str2, th);
        }
    }

    public void a(String str, String str2) {
        int intValue;
        if (a() && m.b(str2) && (intValue = ((Integer) this.a.a(c.ag)).intValue()) > 0) {
            int length = str2.length();
            int i = ((length + intValue) - 1) / intValue;
            for (int i2 = 0; i2 < i; i2++) {
                int i3 = i2 * intValue;
                b(str, str2.substring(i3, Math.min(length, i3 + intValue)));
            }
        }
    }

    public void a(String str, String str2, Throwable th) {
        if (a()) {
            String str3 = Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2;
            Log.w("AppLovinSdk", str3, th);
            j("WARN", str3);
        }
    }

    public void b(String str, String str2) {
        if (a()) {
            String str3 = Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2;
            Log.d("AppLovinSdk", str3);
            j("DEBUG", str3);
        }
    }

    public void b(String str, String str2, Throwable th) {
        a(str, true, str2, th);
    }

    public void c(String str, String str2) {
        if (a()) {
            String str3 = Constants.RequestParameters.LEFT_BRACKETS + str + "] " + str2;
            Log.i("AppLovinSdk", str3);
            j("INFO", str3);
        }
    }

    public void d(String str, String str2) {
        a(str, str2, (Throwable) null);
    }

    public void e(String str, String str2) {
        b(str, str2, null);
    }
}
