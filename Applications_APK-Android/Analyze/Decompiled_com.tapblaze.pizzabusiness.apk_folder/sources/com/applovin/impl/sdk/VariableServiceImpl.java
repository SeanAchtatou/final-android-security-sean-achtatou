package com.applovin.impl.sdk;

import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.d.p;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinVariableService;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class VariableServiceImpl implements AppLovinVariableService {
    private final i a;
    /* access modifiers changed from: private */
    public final AtomicBoolean b = new AtomicBoolean();
    private final AtomicBoolean c = new AtomicBoolean();
    /* access modifiers changed from: private */
    public AppLovinVariableService.OnVariablesUpdateListener d;
    private Bundle e;
    private final Object f = new Object();

    VariableServiceImpl(i iVar) {
        this.a = iVar;
        String str = (String) iVar.a(e.h);
        if (m.b(str)) {
            updateVariables(i.a(str, iVar));
        }
    }

    private Object a(String str, Object obj, Class<?> cls) {
        if (TextUtils.isEmpty(str)) {
            o.i("AppLovinVariableService", "Unable to retrieve variable value for empty name");
            return obj;
        }
        if (!this.a.d()) {
            o.h("AppLovinSdk", "Attempted to retrieve variable before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
        }
        synchronized (this.f) {
            if (this.e == null) {
                o.i("AppLovinVariableService", "Unable to retrieve variable value for name \"" + str + "\", none retrieved from server yet. Please set a listener to be notified when values are retrieved from the server.");
                return obj;
            } else if (cls.equals(String.class)) {
                String string = this.e.getString(str, (String) obj);
                return string;
            } else if (cls.equals(Boolean.class)) {
                Boolean valueOf = Boolean.valueOf(this.e.getBoolean(str, ((Boolean) obj).booleanValue()));
                return valueOf;
            } else {
                throw new IllegalStateException("Unable to retrieve variable value for " + str);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r4 = this;
            java.lang.Object r0 = r4.f
            monitor-enter(r0)
            com.applovin.sdk.AppLovinVariableService$OnVariablesUpdateListener r1 = r4.d     // Catch:{ all -> 0x0021 }
            if (r1 == 0) goto L_0x001f
            android.os.Bundle r1 = r4.e     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x000c
            goto L_0x001f
        L_0x000c:
            android.os.Bundle r1 = r4.e     // Catch:{ all -> 0x0021 }
            java.lang.Object r1 = r1.clone()     // Catch:{ all -> 0x0021 }
            android.os.Bundle r1 = (android.os.Bundle) r1     // Catch:{ all -> 0x0021 }
            r2 = 1
            com.applovin.impl.sdk.VariableServiceImpl$2 r3 = new com.applovin.impl.sdk.VariableServiceImpl$2     // Catch:{ all -> 0x0021 }
            r3.<init>(r1)     // Catch:{ all -> 0x0021 }
            com.applovin.sdk.AppLovinSdkUtils.runOnUiThread(r2, r3)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            return
        L_0x001f:
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            return
        L_0x0021:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.VariableServiceImpl.a():void");
    }

    public boolean getBoolean(String str) {
        return getBoolean(str, false);
    }

    public boolean getBoolean(String str, boolean z) {
        return ((Boolean) a(str, Boolean.valueOf(z), Boolean.class)).booleanValue();
    }

    public String getString(String str) {
        return getString(str, null);
    }

    public String getString(String str, String str2) {
        return (String) a(str, str2, String.class);
    }

    public void loadVariables() {
        String str;
        if (!this.a.d()) {
            str = "The AppLovin SDK is waiting for the initial variables to be returned upon completing initialization.";
        } else if (this.b.compareAndSet(false, true)) {
            this.a.K().a(new p(this.a, new p.a() {
                public void a() {
                    VariableServiceImpl.this.b.set(false);
                }
            }), r.a.BACKGROUND);
            return;
        } else {
            str = "Ignored explicit variables load. Service is already in the process of retrieving the latest set of variables.";
        }
        o.i("AppLovinVariableService", str);
    }

    public void setOnVariablesUpdateListener(AppLovinVariableService.OnVariablesUpdateListener onVariablesUpdateListener) {
        this.d = onVariablesUpdateListener;
        synchronized (this.f) {
            if (onVariablesUpdateListener != null) {
                if (this.e != null && this.c.compareAndSet(false, true)) {
                    this.a.v().b("AppLovinVariableService", "Setting initial listener");
                    a();
                }
            }
        }
    }

    public String toString() {
        return "VariableService{variables=" + this.e + ", listener=" + this.d + '}';
    }

    public void updateVariables(JSONObject jSONObject) {
        o v = this.a.v();
        v.b("AppLovinVariableService", "Updating variables: " + jSONObject + "...");
        synchronized (this.f) {
            this.e = i.c(jSONObject);
            a();
            this.a.a(e.h, jSONObject.toString());
        }
    }
}
