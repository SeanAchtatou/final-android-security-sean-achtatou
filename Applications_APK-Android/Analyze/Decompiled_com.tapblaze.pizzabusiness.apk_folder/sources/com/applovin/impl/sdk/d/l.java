package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class l extends m {
    private final List<String> a;

    public l(List<String> list, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super(d.a(a(list), iVar), appLovinAdLoadListener, "TaskFetchMultizoneAd", iVar);
        this.a = Collections.unmodifiableList(list);
    }

    private static String a(List<String> list) {
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        throw new IllegalArgumentException("No zone identifiers specified");
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.m;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap(1);
        List<String> list = this.a;
        hashMap.put("zone_ids", m.d(e.a(list, list.size())));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public b c() {
        return b.APPLOVIN_MULTIZONE;
    }
}
