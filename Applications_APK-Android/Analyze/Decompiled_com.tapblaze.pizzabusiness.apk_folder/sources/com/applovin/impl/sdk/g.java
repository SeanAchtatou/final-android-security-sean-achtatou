package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicBoolean;

class g extends BroadcastReceiver {
    /* access modifiers changed from: private */
    public static AlertDialog b;
    /* access modifiers changed from: private */
    public static final AtomicBoolean c = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final h a;
    private n d;

    public interface a {
        void a();

        void b();
    }

    g(h hVar, i iVar) {
        this.a = hVar;
        iVar.ad().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        iVar.ad().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public void a(long j, final i iVar, final a aVar) {
        if (j > 0) {
            AlertDialog alertDialog = b;
            if (alertDialog == null || !alertDialog.isShowing()) {
                if (c.getAndSet(true)) {
                    if (j < this.d.a()) {
                        o v = iVar.v();
                        v.b("ConsentAlertManager", "Scheduling consent alert earlier (" + j + "ms) than remaining scheduled time (" + this.d.a() + "ms)");
                        this.d.d();
                    } else {
                        o v2 = iVar.v();
                        v2.d("ConsentAlertManager", "Skip scheduling consent alert - one scheduled already with remaining time of " + this.d.a() + " milliseconds");
                        return;
                    }
                }
                o v3 = iVar.v();
                v3.b("ConsentAlertManager", "Scheduling consent alert for " + j + " milliseconds");
                this.d = n.a(j, iVar, new Runnable() {
                    public void run() {
                        String str;
                        o oVar;
                        if (g.this.a.c()) {
                            iVar.v().e("ConsentAlertManager", "Consent dialog already showing, skip showing of consent alert");
                            return;
                        }
                        Activity a2 = iVar.aa().a();
                        if (a2 == null || !h.a(iVar.D())) {
                            if (a2 == null) {
                                oVar = iVar.v();
                                str = "No parent Activity found - rescheduling consent alert...";
                            } else {
                                oVar = iVar.v();
                                str = "No internet available - rescheduling consent alert...";
                            }
                            oVar.e("ConsentAlertManager", str);
                            g.c.set(false);
                            g.this.a(((Long) iVar.a(c.aw)).longValue(), iVar, aVar);
                            return;
                        }
                        AppLovinSdkUtils.runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog unused = g.b = new AlertDialog.Builder(iVar.aa().a()).setTitle((CharSequence) iVar.a(c.ax)).setMessage((CharSequence) iVar.a(c.ay)).setCancelable(false).setPositiveButton((CharSequence) iVar.a(c.az), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.a();
                                        dialogInterface.dismiss();
                                        g.c.set(false);
                                    }
                                }).setNegativeButton((CharSequence) iVar.a(c.aA), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        aVar.b();
                                        dialogInterface.dismiss();
                                        g.c.set(false);
                                        g.this.a(((Long) iVar.a(c.av)).longValue(), iVar, aVar);
                                    }
                                }).create();
                                g.b.show();
                            }
                        });
                    }
                });
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (this.d != null) {
            String action = intent.getAction();
            if ("com.applovin.application_paused".equals(action)) {
                this.d.b();
            } else if ("com.applovin.application_resumed".equals(action)) {
                this.d.c();
            }
        }
    }
}
