package com.applovin.impl.sdk.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.c.a;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.o;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdk;
import com.google.android.gms.drive.DriveFile;
import com.ironsource.sdk.constants.Constants;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public abstract class p {
    private static ApplicationInfo a;
    private static Boolean b;

    public static double a(long j) {
        double d = (double) j;
        Double.isNaN(d);
        return d / 1000.0d;
    }

    public static float a(float f) {
        return f * 1000.0f;
    }

    public static int a(JSONObject jSONObject) {
        int b2 = i.b(jSONObject, "video_completion_percent", -1, (i) null);
        if (b2 < 0 || b2 > 100) {
            return 95;
        }
        return b2;
    }

    public static long a(i iVar) {
        long longValue = ((Long) iVar.a(c.eL)).longValue();
        long longValue2 = ((Long) iVar.a(c.eM)).longValue();
        long currentTimeMillis = System.currentTimeMillis();
        return (longValue <= 0 || longValue2 <= 0) ? currentTimeMillis : currentTimeMillis + (longValue - longValue2);
    }

    public static Activity a(View view, i iVar) {
        if (view == null) {
            return null;
        }
        int i = 0;
        while (i < 1000) {
            i++;
            try {
                Context context = view.getContext();
                if (context instanceof Activity) {
                    return (Activity) context;
                }
                ViewParent parent = view.getParent();
                if (!(parent instanceof View)) {
                    return null;
                }
                view = (View) parent;
            } catch (Throwable th) {
                iVar.v().b("Utils", "Encountered error while retrieving activity from view", th);
            }
        }
        return null;
    }

    public static Bitmap a(Context context, int i, int i2) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            int i3 = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(context.getResources(), i);
            if (options.outHeight > i2 || options.outWidth > i2) {
                double d = (double) i2;
                double max = (double) Math.max(options.outHeight, options.outWidth);
                Double.isNaN(d);
                Double.isNaN(max);
                i3 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log(d / max) / Math.log(0.5d))));
            }
            new BitmapFactory.Options().inSampleSize = i3;
            return BitmapFactory.decodeResource(context.getResources(), i);
        } catch (Exception unused) {
            return null;
        } finally {
            a((Closeable) null, (i) null);
            a((Closeable) null, (i) null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.i):void
     arg types: [java.io.FileInputStream, ?[OBJECT, ARRAY]]
     candidates:
      com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.i):android.app.Activity
      com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.i):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.i):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.i):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.p.a(long, long):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.i):void */
    public static Bitmap a(File file, int i) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        BitmapFactory.Options options;
        try {
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            int i2 = 1;
            options2.inJustDecodeBounds = true;
            fileInputStream = new FileInputStream(file);
            try {
                BitmapFactory.decodeStream(fileInputStream, null, options2);
                fileInputStream.close();
                if (options2.outHeight > i || options2.outWidth > i) {
                    double d = (double) i;
                    double max = (double) Math.max(options2.outHeight, options2.outWidth);
                    Double.isNaN(d);
                    Double.isNaN(max);
                    i2 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log(d / max) / Math.log(0.5d))));
                }
                options = new BitmapFactory.Options();
                options.inSampleSize = i2;
                fileInputStream2 = new FileInputStream(file);
            } catch (Exception unused) {
                fileInputStream2 = null;
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                return null;
            } catch (Throwable th) {
                th = th;
                fileInputStream2 = null;
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                throw th;
            }
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(fileInputStream2, null, options);
                fileInputStream2.close();
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                return decodeStream;
            } catch (Exception unused2) {
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                return null;
            } catch (Throwable th2) {
                th = th2;
                a((Closeable) fileInputStream, (i) null);
                a((Closeable) fileInputStream2, (i) null);
                throw th;
            }
        } catch (Exception unused3) {
            fileInputStream2 = null;
            fileInputStream = null;
            a((Closeable) fileInputStream, (i) null);
            a((Closeable) fileInputStream2, (i) null);
            return null;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream2 = null;
            fileInputStream = null;
            a((Closeable) fileInputStream, (i) null);
            a((Closeable) fileInputStream2, (i) null);
            throw th;
        }
    }

    public static View a(Context context, View view) {
        View e = e(context);
        return e != null ? e : a(view);
    }

    public static View a(View view) {
        View rootView;
        if (view == null || (rootView = view.getRootView()) == null) {
            return null;
        }
        View findViewById = rootView.findViewById(16908290);
        return findViewById != null ? findViewById : rootView;
    }

    public static d a(JSONObject jSONObject, i iVar) {
        return d.a(AppLovinAdSize.fromString(i.b(jSONObject, "ad_size", (String) null, iVar)), AppLovinAdType.fromString(i.b(jSONObject, "ad_type", (String) null, iVar)), i.b(jSONObject, "zone_id", (String) null, iVar), iVar);
    }

    public static i a(AppLovinSdk appLovinSdk) {
        try {
            Field declaredField = appLovinSdk.getClass().getDeclaredField("mSdkImpl");
            declaredField.setAccessible(true);
            return (i) declaredField.get(appLovinSdk);
        } catch (Throwable th) {
            throw new IllegalStateException("Internal error - unable to retrieve SDK implementation: " + th);
        }
    }

    public static AppLovinAd a(AppLovinAd appLovinAd, i iVar) {
        if (!(appLovinAd instanceof g)) {
            return appLovinAd;
        }
        g gVar = (g) appLovinAd;
        AppLovinAd dequeueAd = iVar.o().dequeueAd(gVar.getAdZone());
        o v = iVar.v();
        v.b("Utils", "Dequeued ad for dummy ad: " + dequeueAd);
        if (dequeueAd == null) {
            return gVar.a();
        }
        gVar.a(dequeueAd);
        ((AppLovinAdBase) dequeueAd).setDummyAd(gVar);
        return dequeueAd;
    }

    public static Object a(Object obj, i iVar) {
        int i;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            HashMap hashMap = new HashMap(map.size());
            for (Map.Entry entry : map.entrySet()) {
                Object key = entry.getKey();
                hashMap.put(key instanceof String ? (String) key : String.valueOf(key), a(entry.getValue(), iVar));
            }
            return hashMap;
        } else if (obj instanceof List) {
            List<Object> list = (List) obj;
            ArrayList arrayList = new ArrayList(list.size());
            for (Object a2 : list) {
                arrayList.add(a(a2, iVar));
            }
            return arrayList;
        } else if (obj instanceof Date) {
            return String.valueOf(((Date) obj).getTime());
        } else {
            String valueOf = String.valueOf(obj);
            if (obj instanceof String) {
                i = ((Integer) iVar.a(c.aV)).intValue();
                if (i <= 0 || valueOf.length() <= i) {
                    return valueOf;
                }
            } else if (!(obj instanceof Uri) || (i = ((Integer) iVar.a(c.aW)).intValue()) <= 0 || valueOf.length() <= i) {
                return valueOf;
            }
            return valueOf.substring(0, i);
        }
    }

    public static String a(Context context) {
        Bundle h = h(context);
        if (h == null) {
            return null;
        }
        String string = h.getString("applovin.sdk.key");
        return string != null ? string : "";
    }

    public static String a(String str) {
        return (str == null || str.length() <= 4) ? "NOKEY" : str.substring(str.length() - 4);
    }

    public static String a(Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append(Constants.RequestParameters.AMPERSAND);
            }
            sb.append(next.getKey());
            sb.append('=');
            sb.append(next.getValue());
        }
        return sb.toString();
    }

    public static Field a(Class cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Class superclass = cls.getSuperclass();
            if (superclass == null) {
                return null;
            }
            return a(superclass, str);
        }
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, String str3, i iVar) {
        return a(str, jSONObject, str2, null, str3, iVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, String str3, String str4, i iVar) {
        HashMap hashMap = new HashMap(2);
        hashMap.put("{CLCODE}", str2);
        if (str3 == null) {
            str3 = "";
        }
        hashMap.put("{EVENT_ID}", str3);
        return a(str, jSONObject, hashMap, str4, iVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, Map<String, String> map, String str2, i iVar) {
        JSONObject b2 = i.b(jSONObject, str, new JSONObject(), iVar);
        ArrayList arrayList = new ArrayList(b2.length() + 1);
        if (m.b(str2)) {
            arrayList.add(new a(str2, null));
        }
        if (b2.length() > 0) {
            Iterator<String> keys = b2.keys();
            while (keys.hasNext()) {
                try {
                    String next = keys.next();
                    if (!TextUtils.isEmpty(next)) {
                        String optString = b2.optString(next);
                        String a2 = m.a(next, map);
                        if (!TextUtils.isEmpty(optString)) {
                            optString = m.a(optString, map);
                        }
                        arrayList.add(new a(a2, optString));
                    }
                } catch (Throwable th) {
                    iVar.v().b("Utils", "Failed to create and add postback url.", th);
                }
            }
        }
        return arrayList;
    }

    private static List<Class> a(List<String> list, i iVar) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (String next : list) {
            try {
                arrayList.add(Class.forName(next));
            } catch (ClassNotFoundException unused) {
                o v = iVar.v();
                v.e("Utils", "Failed to create class for name: " + next);
            }
        }
        return arrayList;
    }

    public static void a(AppLovinAdLoadListener appLovinAdLoadListener, d dVar, int i, i iVar) {
        if (appLovinAdLoadListener != null) {
            try {
                if (appLovinAdLoadListener instanceof l) {
                    ((l) appLovinAdLoadListener).a(dVar, i);
                } else {
                    appLovinAdLoadListener.failedToReceiveAd(i);
                }
            } catch (Throwable th) {
                iVar.v().b("Utils", "Unable process a failure to receive an ad", th);
            }
        }
    }

    public static void a(Closeable closeable, i iVar) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
                if (iVar != null) {
                    o v = iVar.v();
                    v.b("Utils", "Unable to close stream: " + closeable, th);
                }
            }
        }
    }

    public static void a(String str, Boolean bool, Map<String, String> map) {
        if (bool.booleanValue()) {
            map.put(str, Boolean.toString(true));
        }
    }

    public static void a(String str, String str2, Map<String, String> map) {
        if (!TextUtils.isEmpty(str2)) {
            map.put(str, str2);
        }
    }

    public static void a(String str, JSONObject jSONObject, i iVar) {
        if (jSONObject.has("no_fill_reason")) {
            Object a2 = i.a(jSONObject, "no_fill_reason", new Object(), iVar);
            o.i("AppLovinSdk", "\n**************************************************\nNO FILL received:\n..ID: \"" + str + "\"\n..SDK KEY: \"" + iVar.t() + "\"\n..Reason: " + a2 + "\n**************************************************\n");
        }
    }

    public static void a(HttpURLConnection httpURLConnection, i iVar) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Throwable th) {
                if (iVar != null) {
                    o v = iVar.v();
                    v.b("Utils", "Unable to disconnect connection: " + httpURLConnection, th);
                }
            }
        }
    }

    public static boolean a() {
        Bundle h;
        Context E = i.E();
        return (E == null || (h = h(E)) == null || !h.containsKey("applovin.sdk.verbose_logging")) ? false : true;
    }

    public static boolean a(long j, long j2) {
        return (j & j2) != 0;
    }

    public static boolean a(Context context, Uri uri, i iVar) {
        boolean z;
        try {
            Intent intent = new Intent("android.intent.action.VIEW", uri);
            if (!(context instanceof Activity)) {
                intent.setFlags(DriveFile.MODE_READ_ONLY);
            }
            iVar.Y().b();
            context.startActivity(intent);
            z = true;
        } catch (Throwable th) {
            o v = iVar.v();
            v.b("Utils", "Unable to open \"" + uri + "\".", th);
            z = false;
        }
        if (!z) {
            iVar.Y().c();
        }
        return z;
    }

    public static boolean a(View view, Activity activity) {
        View rootView;
        if (!(activity == null || view == null)) {
            Window window = activity.getWindow();
            if (window != null) {
                rootView = window.getDecorView();
            } else {
                View findViewById = activity.findViewById(16908290);
                if (findViewById != null) {
                    rootView = findViewById.getRootView();
                }
            }
            return a(view, rootView);
        }
        return false;
    }

    public static boolean a(View view, View view2) {
        if (view == view2) {
            return true;
        }
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                if (a(view, viewGroup.getChildAt(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean a(Object obj, List<String> list, i iVar) {
        if (list == null) {
            return false;
        }
        for (Class isInstance : a(list, iVar)) {
            if (isInstance.isInstance(obj)) {
                if (obj instanceof Map) {
                    for (Map.Entry entry : ((Map) obj).entrySet()) {
                        if (!(entry.getKey() instanceof String)) {
                            iVar.v().b("Utils", "Invalid key type used. Map keys should be of type String.");
                            return false;
                        } else if (!a(entry.getValue(), list, iVar)) {
                            return false;
                        }
                    }
                    return true;
                } else if (!(obj instanceof List)) {
                    return true;
                } else {
                    for (Object a2 : (List) obj) {
                        if (!a(a2, list, iVar)) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        o v = iVar.v();
        v.b("Utils", "Object '" + obj + "' does not match any of the required types '" + list + "'.");
        return false;
    }

    public static boolean a(String str, List<String> list) {
        for (String startsWith : list) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    public static long b(float f) {
        return c(a(f));
    }

    public static String b(Class cls, String str) {
        try {
            Field a2 = a(cls, str);
            a2.setAccessible(true);
            return (String) a2.get(null);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static String b(String str) {
        return str.replace("{PLACEMENT}", "");
    }

    public static Map<String, String> b(Map<String, String> map) {
        HashMap hashMap = new HashMap(map);
        for (String str : hashMap.keySet()) {
            String str2 = (String) hashMap.get(str);
            if (str2 != null) {
                hashMap.put(str, m.d(str2));
            }
        }
        return hashMap;
    }

    public static void b(AppLovinAd appLovinAd, i iVar) {
        if (appLovinAd instanceof AppLovinAdBase) {
            String t = iVar.t();
            String t2 = ((AppLovinAdBase) appLovinAd).getSdk().t();
            if (!t.equals(t2)) {
                o.i("AppLovinAd", "Ad was loaded from sdk with key: " + t2 + ", but is being rendered from sdk with key: " + t);
                iVar.L().a(com.applovin.impl.sdk.c.g.l);
            }
        }
    }

    public static boolean b() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static boolean b(Context context) {
        Bundle h;
        if (context == null) {
            context = i.E();
        }
        return (context == null || (h = h(context)) == null || !h.getBoolean("applovin.sdk.test_ads", false)) ? false : true;
    }

    public static boolean b(i iVar) {
        if (b == null) {
            try {
                Context E = i.E();
                b = Boolean.valueOf(Class.forName(E.getPackageName() + ".BuildConfig").getField("DEBUG").getBoolean(null));
            } catch (Throwable th) {
                iVar.v().b("Utils", "Failed to retrieve BuildConfig.DEBUG", th);
                b = false;
            }
        }
        return b.booleanValue();
    }

    private static long c(float f) {
        return (long) Math.round(f);
    }

    public static MaxAdFormat c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.equalsIgnoreCase("banner")) {
            return MaxAdFormat.BANNER;
        }
        if (str.equalsIgnoreCase("mrec")) {
            return MaxAdFormat.MREC;
        }
        if (str.equalsIgnoreCase("leaderboard") || str.equalsIgnoreCase("leader")) {
            return MaxAdFormat.LEADER;
        }
        if (str.equalsIgnoreCase("interstitial") || str.equalsIgnoreCase("inter")) {
            return MaxAdFormat.INTERSTITIAL;
        }
        if (str.equalsIgnoreCase(Constants.CONVERT_REWARDED) || str.equalsIgnoreCase("reward")) {
            return MaxAdFormat.REWARDED;
        }
        throw new IllegalArgumentException("Unknown format: " + str);
    }

    public static boolean c() {
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(runningAppProcessInfo);
        return runningAppProcessInfo.importance == 100 || runningAppProcessInfo.importance == 200;
    }

    public static boolean c(Context context) {
        Bundle h;
        if (context == null) {
            context = i.E();
        }
        return (context == null || (h = h(context)) == null || !h.getBoolean("applovin.sdk.verbose_logging", false)) ? false : true;
    }

    public static int d(Context context) {
        Resources resources;
        Configuration configuration;
        if (context == null || (resources = context.getResources()) == null || (configuration = resources.getConfiguration()) == null) {
            return 0;
        }
        return configuration.orientation;
    }

    public static String d(String str) {
        Uri parse = Uri.parse(str);
        return new Uri.Builder().scheme(parse.getScheme()).authority(parse.getAuthority()).path(parse.getPath()).build().toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000a A[Catch:{ all -> 0x002e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean d() {
        /*
            java.util.Enumeration r0 = java.net.NetworkInterface.getNetworkInterfaces()     // Catch:{ all -> 0x002e }
        L_0x0004:
            boolean r1 = r0.hasMoreElements()     // Catch:{ all -> 0x002e }
            if (r1 == 0) goto L_0x0036
            java.lang.Object r1 = r0.nextElement()     // Catch:{ all -> 0x002e }
            java.net.NetworkInterface r1 = (java.net.NetworkInterface) r1     // Catch:{ all -> 0x002e }
            java.lang.String r1 = r1.getDisplayName()     // Catch:{ all -> 0x002e }
            java.lang.String r2 = "tun"
            boolean r2 = r1.contains(r2)     // Catch:{ all -> 0x002e }
            if (r2 != 0) goto L_0x002c
            java.lang.String r2 = "ppp"
            boolean r2 = r1.contains(r2)     // Catch:{ all -> 0x002e }
            if (r2 != 0) goto L_0x002c
            java.lang.String r2 = "ipsec"
            boolean r1 = r1.contains(r2)     // Catch:{ all -> 0x002e }
            if (r1 == 0) goto L_0x0004
        L_0x002c:
            r0 = 1
            return r0
        L_0x002e:
            r0 = move-exception
            java.lang.String r1 = "Utils"
            java.lang.String r2 = "Unable to check Network Interfaces"
            com.applovin.impl.sdk.o.c(r1, r2, r0)
        L_0x0036:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.utils.p.d():boolean");
    }

    public static View e(Context context) {
        if (!(context instanceof Activity)) {
            return null;
        }
        return ((Activity) context).getWindow().getDecorView().findViewById(16908290);
    }

    public static boolean e(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            Class.forName(str);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    public static long f(String str) {
        if (!m.b(str)) {
            return Long.MAX_VALUE;
        }
        try {
            return (long) Color.parseColor(str);
        } catch (Throwable unused) {
            return Long.MAX_VALUE;
        }
    }

    public static String f(Context context) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setPackage(context.getPackageName());
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        if (!queryIntentActivities.isEmpty()) {
            return queryIntentActivities.get(0).activityInfo.name;
        }
        return null;
    }

    public static int g(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager == null) {
            return 0;
        }
        return windowManager.getDefaultDisplay().getRotation();
    }

    public static int g(String str) {
        int i = 0;
        for (String str2 : str.split("\\.")) {
            if (str2.length() > 2) {
                o.i("Utils", "Version number components cannot be longer than two digits -> " + str);
                return i;
            }
            i = (i * 100) + Integer.valueOf(str2).intValue();
        }
        return i;
    }

    private static Bundle h(Context context) {
        ApplicationInfo applicationInfo = a;
        if (applicationInfo != null) {
            return applicationInfo.metaData;
        }
        try {
            a = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            return a.metaData;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }
}
