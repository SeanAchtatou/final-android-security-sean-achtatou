package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import org.json.JSONObject;

public class y extends z {
    private final f a;

    public y(f fVar, i iVar) {
        super("TaskReportAppLovinReward", iVar);
        this.a = fVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.x;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        d("Failed to report reward for ad: " + this.a + " - error code: " + i);
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "zone_id", this.a.getAdZone().a(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, "fire_percent", this.a.ah(), this.b);
        String clCode = this.a.getClCode();
        if (!m.b(clCode)) {
            clCode = "NO_CLCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, "clcode", clCode, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/cr";
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        a("Reported reward successfully for ad: " + this.a);
    }

    /* access modifiers changed from: protected */
    public c c() {
        return this.a.aC();
    }

    /* access modifiers changed from: protected */
    public void d() {
        d("No reward result was found for ad: " + this.a);
    }
}
