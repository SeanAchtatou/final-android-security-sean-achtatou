package com.applovin.impl.sdk;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class k {
    private final String a = UUID.randomUUID().toString();
    private final String b;
    private final Map<String, Object> c;
    private final long d;

    public k(String str, Map<String, String> map, Map<String, Object> map2) {
        this.b = str;
        this.c = new HashMap();
        this.c.putAll(map);
        this.c.put("applovin_sdk_super_properties", map2);
        this.d = System.currentTimeMillis();
    }

    public String a() {
        return this.b;
    }

    public Map<String, Object> b() {
        return this.c;
    }

    public long c() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        k kVar = (k) obj;
        if (this.d != kVar.d) {
            return false;
        }
        String str = this.b;
        if (str == null ? kVar.b != null : !str.equals(kVar.b)) {
            return false;
        }
        Map<String, Object> map = this.c;
        if (map == null ? kVar.c != null : !map.equals(kVar.c)) {
            return false;
        }
        String str2 = this.a;
        String str3 = kVar.a;
        if (str2 != null) {
            return str2.equals(str3);
        }
        if (str3 == null) {
            return true;
        }
    }

    public int hashCode() {
        String str = this.b;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, Object> map = this.c;
        int hashCode2 = map != null ? map.hashCode() : 0;
        long j = this.d;
        int i2 = (((hashCode + hashCode2) * 31) + ((int) (j ^ (j >>> 32)))) * 31;
        String str2 = this.a;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        return "Event{name='" + this.b + '\'' + ", id='" + this.a + '\'' + ", creationTimestampMillis=" + this.d + ", parameters=" + this.c + '}';
    }
}
