package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.concurrent.TimeUnit;

public abstract class x<T> extends a implements a.c<T> {
    /* access modifiers changed from: private */
    public final b<T> a;
    private final a.c<T> c;
    protected a.C0006a d;
    /* access modifiers changed from: private */
    public r.a e;
    /* access modifiers changed from: private */
    public c<String> f;
    /* access modifiers changed from: private */
    public c<String> g;

    public x(b<T> bVar, i iVar) {
        this(bVar, iVar, false);
    }

    public x(b<T> bVar, final i iVar, boolean z) {
        super("TaskRepeatRequest", iVar, z);
        this.e = r.a.BACKGROUND;
        this.f = null;
        this.g = null;
        if (bVar != null) {
            this.a = bVar;
            this.d = new a.C0006a();
            this.c = new a.c<T>() {
                public void a(int i) {
                    c cVar;
                    x xVar;
                    boolean z = false;
                    boolean z2 = i < 200 || i >= 500;
                    boolean z3 = i == 429;
                    if (i != -103) {
                        z = true;
                    }
                    if (z && (z2 || z3)) {
                        String f = x.this.a.f();
                        if (x.this.a.j() > 0) {
                            x xVar2 = x.this;
                            xVar2.c("Unable to send request due to server failure (code " + i + "). " + x.this.a.j() + " attempts left, retrying in " + TimeUnit.MILLISECONDS.toSeconds((long) x.this.a.l()) + " seconds...");
                            int j = x.this.a.j() - 1;
                            x.this.a.a(j);
                            if (j == 0) {
                                x xVar3 = x.this;
                                xVar3.c(xVar3.f);
                                if (m.b(f) && f.length() >= 4) {
                                    x.this.a.a(f);
                                    x xVar4 = x.this;
                                    xVar4.b("Switching to backup endpoint " + f);
                                }
                            }
                            r K = iVar.K();
                            x xVar5 = x.this;
                            K.a(xVar5, xVar5.e, (long) x.this.a.l());
                            return;
                        }
                        if (f == null || !f.equals(x.this.a.a())) {
                            xVar = x.this;
                            cVar = xVar.f;
                        } else {
                            xVar = x.this;
                            cVar = xVar.g;
                        }
                        xVar.c(cVar);
                    }
                    x.this.a(i);
                }

                public void a(T t, int i) {
                    x.this.a.a(0);
                    x.this.a(t, i);
                }
            };
            return;
        }
        throw new IllegalArgumentException("No request specified");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.b.d.a(com.applovin.impl.sdk.b.c<?>, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.c<ST>, ST]
     candidates:
      com.applovin.impl.sdk.b.d.a(java.lang.String, com.applovin.impl.sdk.b.c):com.applovin.impl.sdk.b.c<ST>
      com.applovin.impl.sdk.b.d.a(com.applovin.impl.sdk.b.c<?>, java.lang.Object):void */
    /* access modifiers changed from: private */
    public <ST> void c(c<ST> cVar) {
        if (cVar != null) {
            d C = e().C();
            C.a((c<?>) cVar, (Object) cVar.b());
            C.a();
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.e;
    }

    public abstract void a(int i);

    public void a(c<String> cVar) {
        this.f = cVar;
    }

    public void a(r.a aVar) {
        this.e = aVar;
    }

    public abstract void a(Object obj, int i);

    public void b(c<String> cVar) {
        this.g = cVar;
    }

    public void run() {
        int i;
        a J = e().J();
        if (!e().c() && !e().d()) {
            d("AppLovin SDK is disabled: please check your connection");
            o.i("AppLovinSdk", "AppLovin SDK is disabled: please check your connection");
            i = -22;
        } else if (!m.b(this.a.a()) || this.a.a().length() < 4) {
            d("Task has an invalid or null request endpoint.");
            i = AppLovinErrorCodes.INVALID_URL;
        } else {
            if (TextUtils.isEmpty(this.a.b())) {
                this.a.b(this.a.e() != null ? "POST" : "GET");
            }
            J.a(this.a, this.d, this.c);
            return;
        }
        a(i);
    }
}
