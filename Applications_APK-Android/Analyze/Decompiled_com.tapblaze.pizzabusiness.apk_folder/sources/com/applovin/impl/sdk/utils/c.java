package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.content.res.XmlResourceParser;
import com.applovin.impl.sdk.o;
import com.ironsource.sdk.constants.Constants;

public class c {
    private static c a;
    private static final Object b = new Object();
    private final int c;

    private c() {
        this.c = 0;
    }

    private c(Context context) {
        int i;
        try {
            XmlResourceParser openXmlResourceParser = context.getAssets().openXmlResourceParser("AndroidManifest.xml");
            int eventType = openXmlResourceParser.getEventType();
            i = 0;
            do {
                if (2 == eventType) {
                    try {
                        if (openXmlResourceParser.getName().equals(Constants.ParametersKeys.ORIENTATION_APPLICATION)) {
                            int i2 = 0;
                            while (true) {
                                if (i2 >= openXmlResourceParser.getAttributeCount()) {
                                    break;
                                }
                                String attributeName = openXmlResourceParser.getAttributeName(i2);
                                String attributeValue = openXmlResourceParser.getAttributeValue(i2);
                                if (attributeName.equals("networkSecurityConfig")) {
                                    i = Integer.valueOf(attributeValue).intValue();
                                    break;
                                }
                                i2++;
                            }
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            o.c("AndroidManifest", "Failed to parse AndroidManifest.xml.", th);
                            this.c = i;
                        } catch (Throwable th2) {
                            this.c = i;
                            throw th2;
                        }
                    }
                }
                eventType = openXmlResourceParser.next();
            } while (eventType != 1);
        } catch (Throwable th3) {
            th = th3;
            i = 0;
            o.c("AndroidManifest", "Failed to parse AndroidManifest.xml.", th);
            this.c = i;
        }
        this.c = i;
    }

    public static c a(Context context) {
        c cVar;
        synchronized (b) {
            if (a == null) {
                a = new c(context);
            }
            cVar = a;
        }
        return cVar;
    }

    public boolean a() {
        return this.c != 0;
    }
}
