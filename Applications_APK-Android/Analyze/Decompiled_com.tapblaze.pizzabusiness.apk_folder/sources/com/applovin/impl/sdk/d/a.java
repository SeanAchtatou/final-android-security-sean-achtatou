package com.applovin.impl.sdk.d;

import android.content.Context;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;

public abstract class a implements Runnable {
    private final String a;
    /* access modifiers changed from: protected */
    public final i b;
    private final o c;
    private final Context d;
    private final boolean e;

    public a(String str, i iVar) {
        this(str, iVar, false);
    }

    public a(String str, i iVar, boolean z) {
        this.a = str;
        this.b = iVar;
        this.c = iVar.v();
        this.d = iVar.D();
        this.e = z;
    }

    public abstract com.applovin.impl.sdk.c.i a();

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.c.b(this.a, str);
    }

    /* access modifiers changed from: protected */
    public void a(String str, Throwable th) {
        this.c.b(this.a, str, th);
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.c.c(this.a, str);
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        this.c.d(this.a, str);
    }

    /* access modifiers changed from: protected */
    public void d(String str) {
        this.c.e(this.a, str);
    }

    /* access modifiers changed from: protected */
    public i e() {
        return this.b;
    }

    public String f() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public Context g() {
        return this.d;
    }

    public boolean h() {
        return this.e;
    }
}
