package com.applovin.impl.sdk.d;

import com.applovin.impl.a.c;
import com.applovin.impl.a.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAdLoadListener;

class aa extends a {
    /* access modifiers changed from: private */
    public c a;
    /* access modifiers changed from: private */
    public final AppLovinAdLoadListener c;

    aa(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super("TaskResolveVastWrapper", iVar);
        this.c = appLovinAdLoadListener;
        this.a = cVar;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        d("Failed to resolve VAST wrapper due to error code " + i);
        if (i == -103) {
            p.a(this.c, this.a.g(), i, this.b);
        } else {
            com.applovin.impl.a.i.a(this.a, this.c, i == -102 ? d.TIMED_OUT : d.GENERAL_WRAPPER_ERROR, i, this.b);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.y;
    }

    public void run() {
        String a2 = com.applovin.impl.a.i.a(this.a);
        if (m.b(a2)) {
            a("Resolving VAST ad with depth " + this.a.a() + " at " + a2);
            try {
                this.b.K().a(new x<r>(b.a(this.b).a(a2).b("GET").a(r.a).a(((Integer) this.b.a(com.applovin.impl.sdk.b.c.eH)).intValue()).b(((Integer) this.b.a(com.applovin.impl.sdk.b.c.eI)).intValue()).a(false).a(), this.b) {
                    public void a(int i) {
                        d("Unable to resolve VAST wrapper. Server returned " + i);
                        aa.this.a(i);
                    }

                    public void a(r rVar, int i) {
                        this.b.K().a(t.a(rVar, aa.this.a, aa.this.c, aa.this.b));
                    }
                });
            } catch (Throwable th) {
                a("Unable to resolve VAST wrapper", th);
                a(-1);
                this.b.M().a(a());
            }
        } else {
            d("Resolving VAST failed. Could not find resolution URL");
            a(-1);
        }
    }
}
