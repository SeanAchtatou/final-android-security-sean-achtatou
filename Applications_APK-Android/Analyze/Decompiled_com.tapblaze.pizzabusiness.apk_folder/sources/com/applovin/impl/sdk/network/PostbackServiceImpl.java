package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.d.j;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.sdk.AppLovinPostbackListener;
import com.applovin.sdk.AppLovinPostbackService;

public class PostbackServiceImpl implements AppLovinPostbackService {
    private final i a;

    public PostbackServiceImpl(i iVar) {
        this.a = iVar;
    }

    public void dispatchPostbackAsync(String str, AppLovinPostbackListener appLovinPostbackListener) {
        dispatchPostbackRequest(g.b(this.a).a(str).a(false).a(), appLovinPostbackListener);
    }

    public void dispatchPostbackRequest(g gVar, r.a aVar, AppLovinPostbackListener appLovinPostbackListener) {
        this.a.K().a(new j(gVar, aVar, this.a, appLovinPostbackListener), aVar);
    }

    public void dispatchPostbackRequest(g gVar, AppLovinPostbackListener appLovinPostbackListener) {
        dispatchPostbackRequest(gVar, r.a.POSTBACKS, appLovinPostbackListener);
    }

    public String toString() {
        return "PostbackService{}";
    }
}
