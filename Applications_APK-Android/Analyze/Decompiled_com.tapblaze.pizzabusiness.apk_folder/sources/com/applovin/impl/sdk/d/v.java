package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.c.a;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.p;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.vungle.warren.analytics.AnalyticsEvent;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class v extends a {
    private final AppLovinNativeAdLoadListener a;
    private final JSONObject c;

    v(JSONObject jSONObject, i iVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super("TaskRenderNativeAd", iVar);
        this.a = appLovinNativeAdLoadListener;
        this.c = jSONObject;
    }

    private String a(String str, Map<String, String> map, String str2) {
        String str3 = map.get(str);
        if (str3 != null) {
            return str3.replace("{CLCODE}", str2);
        }
        return null;
    }

    private String a(Map<String, String> map, String str, String str2) {
        String str3 = map.get(AnalyticsEvent.Ad.clickUrl);
        if (str2 == null) {
            str2 = "";
        }
        return str3.replace("{CLCODE}", str).replace("{EVENT_ID}", str2);
    }

    private void a(JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = jSONObject;
        JSONArray optJSONArray = jSONObject2.optJSONArray("native_ads");
        JSONObject optJSONObject = jSONObject2.optJSONObject("native_settings");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            c("No ads were returned from the server");
            this.a.onNativeAdsFailedToLoad(204);
            return;
        }
        List b = com.applovin.impl.sdk.utils.i.b(optJSONArray);
        ArrayList arrayList = new ArrayList(b.size());
        Map a2 = optJSONObject != null ? com.applovin.impl.sdk.utils.i.a(optJSONObject) : new HashMap(0);
        Iterator it = b.iterator();
        while (it.hasNext()) {
            Map map = (Map) it.next();
            String str = (String) map.get("clcode");
            String b2 = com.applovin.impl.sdk.utils.i.b(jSONObject2, "zone_id", (String) null, this.b);
            String str2 = (String) map.get("event_id");
            d b3 = d.b(b2, this.b);
            String a3 = a("simp_url", a2, str);
            String a4 = a(a2, str, str2);
            List<a> a5 = p.a("simp_urls", optJSONObject, str, a3, this.b);
            Iterator it2 = it;
            String str3 = a4;
            String str4 = com.applovin.impl.sdk.utils.i.a(optJSONObject, "should_post_click_url", true, this.b).booleanValue() ? a4 : null;
            JSONObject jSONObject3 = optJSONObject;
            String str5 = a3;
            List<a> a6 = p.a("click_tracking_urls", optJSONObject, str, str2, str4, this.b);
            if (a5.size() == 0) {
                throw new IllegalArgumentException("No impression URL available");
            } else if (a6.size() != 0) {
                String str6 = (String) map.get("resource_cache_prefix");
                NativeAdImpl a7 = new NativeAdImpl.a().a(b3).e(b2).f((String) map.get("title")).g((String) map.get("description")).h((String) map.get(ShareConstants.FEED_CAPTION_PARAM)).q((String) map.get("cta")).a((String) map.get("icon_url")).b((String) map.get(MessengerShareContentUtility.IMAGE_URL)).d((String) map.get(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_VIDEO_URL)).c((String) map.get("star_rating_url")).i((String) map.get("icon_url")).j((String) map.get(MessengerShareContentUtility.IMAGE_URL)).k((String) map.get(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_VIDEO_URL)).a(Float.parseFloat((String) map.get("star_rating"))).p(str).l(str3).m(str5).n(a("video_start_url", a2, str)).o(a("video_end_url", a2, str)).a(a5).b(a6).a(Long.parseLong((String) map.get("ad_id"))).c(str6 != null ? e.a(str6) : this.b.b(c.bJ)).a(this.b).a();
                arrayList.add(a7);
                a("Prepared native ad: " + a7.getAdId());
                jSONObject2 = jSONObject;
                optJSONObject = jSONObject3;
                it = it2;
            } else {
                throw new IllegalArgumentException("No click tracking URL available");
            }
        }
        AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.a;
        if (appLovinNativeAdLoadListener != null) {
            appLovinNativeAdLoadListener.onNativeAdsLoaded(arrayList);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.v;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        try {
            if (this.a != null) {
                this.a.onNativeAdsFailedToLoad(i);
            }
        } catch (Exception e) {
            a("Unable to notify listener about failure.", e);
        }
    }

    public void run() {
        try {
            if (this.c != null) {
                if (this.c.length() != 0) {
                    a(this.c);
                    return;
                }
            }
            a((int) AppLovinErrorCodes.UNABLE_TO_PREPARE_NATIVE_AD);
        } catch (Exception e) {
            a("Unable to render native ad.", e);
            a((int) AppLovinErrorCodes.UNABLE_TO_PREPARE_NATIVE_AD);
            this.b.M().a(a());
        }
    }
}
