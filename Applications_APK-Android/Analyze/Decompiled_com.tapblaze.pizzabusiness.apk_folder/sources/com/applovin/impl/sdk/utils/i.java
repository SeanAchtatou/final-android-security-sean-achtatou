package com.applovin.impl.sdk.utils;

import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.impl.sdk.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class i {
    public static float a(JSONObject jSONObject, String str, float f, com.applovin.impl.sdk.i iVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return f;
        }
        try {
            double d = jSONObject.getDouble(str);
            return (-3.4028234663852886E38d >= d || d >= 3.4028234663852886E38d) ? f : (float) d;
        } catch (JSONException e) {
            if (iVar == null) {
                return f;
            }
            o v = iVar.v();
            v.b("JsonUtils", "Failed to retrieve float property for key = " + str, e);
            return f;
        }
    }

    public static long a(JSONObject jSONObject, String str, long j, com.applovin.impl.sdk.i iVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return j;
        }
        try {
            return jSONObject.getLong(str);
        } catch (JSONException e) {
            if (iVar == null) {
                return j;
            }
            o v = iVar.v();
            v.b("JsonUtils", "Failed to retrieve int property for key = " + str, e);
            return j;
        }
    }

    public static Boolean a(JSONObject jSONObject, String str, Boolean bool, com.applovin.impl.sdk.i iVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return bool;
        }
        try {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        } catch (JSONException unused) {
            boolean z = true;
            if (b(jSONObject, str, (bool == null || !bool.booleanValue()) ? 0 : 1, iVar) <= 0) {
                z = false;
            }
            return Boolean.valueOf(z);
        }
    }

    private static Object a(Object obj) throws JSONException {
        if (obj == JSONObject.NULL) {
            return null;
        }
        return obj instanceof JSONObject ? b((JSONObject) obj) : obj instanceof JSONArray ? b((JSONArray) obj) : obj;
    }

    public static Object a(JSONArray jSONArray, int i, Object obj, com.applovin.impl.sdk.i iVar) {
        if (jSONArray == null || jSONArray.length() <= i) {
            return obj;
        }
        try {
            return jSONArray.get(i);
        } catch (JSONException e) {
            if (iVar == null) {
                return obj;
            }
            o v = iVar.v();
            v.b("JsonUtils", "Failed to retrieve object at index " + i + " for JSON array", e);
            return obj;
        }
    }

    public static Object a(JSONObject jSONObject, String str, Object obj, com.applovin.impl.sdk.i iVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return obj;
        }
        try {
            Object obj2 = jSONObject.get(str);
            return obj2 != null ? obj2 : obj;
        } catch (JSONException e) {
            if (iVar == null) {
                return obj;
            }
            o v = iVar.v();
            v.b("JsonUtils", "Failed to retrieve Object for key = " + str, e);
            return obj;
        }
    }

    public static String a(Map<String, Object> map, String str, com.applovin.impl.sdk.i iVar) {
        try {
            return a((Map<String, ?>) map).toString();
        } catch (JSONException e) {
            o v = iVar.v();
            v.b("JsonUtils", "Failed to convert map '" + map + "' to JSON string.", e);
            return str;
        }
    }

    public static ArrayList<Bundle> a(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return new ArrayList<>();
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(c(jSONArray.optJSONObject(i)));
        }
        return arrayList;
    }

    public static List a(JSONObject jSONObject, String str, List list, com.applovin.impl.sdk.i iVar) {
        try {
            JSONArray b = b(jSONObject, str, (JSONArray) null, iVar);
            return b != null ? b(b) : list;
        } catch (JSONException unused) {
            return list;
        }
    }

    public static Map<String, String> a(Bundle bundle) {
        if (bundle == null) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap(bundle.size());
        for (String next : bundle.keySet()) {
            hashMap.put(next, String.valueOf(bundle.get(next)));
        }
        return hashMap;
    }

    public static Map<String, Object> a(String str, Map<String, Object> map, com.applovin.impl.sdk.i iVar) {
        try {
            return b(new JSONObject(str));
        } catch (JSONException e) {
            o v = iVar.v();
            v.b("JsonUtils", "Failed to convert json string '" + str + "' to map", e);
            return map;
        }
    }

    public static Map<String, String> a(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, a(jSONObject.get(next)).toString());
        }
        return hashMap;
    }

    public static JSONObject a(String str, com.applovin.impl.sdk.i iVar) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return new JSONObject(str);
        } catch (Throwable unused) {
            o v = iVar.v();
            v.e("JsonUtils", "Failed to deserialize into JSON: " + str);
            return null;
        }
    }

    public static JSONObject a(String str, JSONObject jSONObject, com.applovin.impl.sdk.i iVar) {
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            if (iVar != null) {
                o v = iVar.v();
                v.b("JsonUtils", "Failed to convert JSON string '" + str + "' to JSONObject", e);
            }
            return jSONObject;
        }
    }

    public static JSONObject a(Map<String, ?> map) throws JSONException {
        if (map == null) {
            return new JSONObject();
        }
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry next : map.entrySet()) {
            jSONObject.put((String) next.getKey(), next.getValue());
        }
        return jSONObject;
    }

    public static JSONObject a(JSONArray jSONArray, int i, JSONObject jSONObject, com.applovin.impl.sdk.i iVar) {
        if (jSONArray == null || i >= jSONArray.length()) {
            return jSONObject;
        }
        try {
            return jSONArray.getJSONObject(i);
        } catch (JSONException e) {
            if (iVar == null) {
                return jSONObject;
            }
            o v = iVar.v();
            v.b("JsonUtils", "Failed to retrieve JSON object from array for index = " + i, e);
            return jSONObject;
        }
    }

    public static void a(JSONObject jSONObject, String str, int i, com.applovin.impl.sdk.i iVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, i);
            } catch (JSONException e) {
                if (iVar != null) {
                    o v = iVar.v();
                    v.b("JsonUtils", "Failed to put int property for key = " + str, e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, String str2, com.applovin.impl.sdk.i iVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, str2);
            } catch (JSONException e) {
                if (iVar != null) {
                    o v = iVar.v();
                    v.b("JsonUtils", "Failed to put String property for key = " + str, e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, JSONArray jSONArray, com.applovin.impl.sdk.i iVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, jSONArray);
            } catch (JSONException e) {
                if (iVar != null) {
                    o v = iVar.v();
                    v.b("JsonUtils", "Failed to put JSONArray property for key = " + str, e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, JSONObject jSONObject2, com.applovin.impl.sdk.i iVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, jSONObject2);
            } catch (JSONException e) {
                if (iVar != null) {
                    o v = iVar.v();
                    v.b("JsonUtils", "Failed to put JSON property for key = " + str, e);
                }
            }
        }
    }

    public static boolean a(String str, JSONArray jSONArray) {
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                Object obj = jSONArray.get(i);
                if ((obj instanceof String) && ((String) obj).equalsIgnoreCase(str)) {
                    return true;
                }
                i++;
            } catch (JSONException unused) {
            }
        }
        return false;
    }

    public static boolean a(JSONObject jSONObject, String str) {
        return jSONObject != null && jSONObject.has(str);
    }

    public static int b(JSONObject jSONObject, String str, int i, com.applovin.impl.sdk.i iVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return i;
        }
        try {
            return jSONObject.getInt(str);
        } catch (JSONException e) {
            if (iVar == null) {
                return i;
            }
            o v = iVar.v();
            v.b("JsonUtils", "Failed to retrieve int property for key = " + str, e);
            return i;
        }
    }

    public static String b(JSONObject jSONObject, String str, String str2, com.applovin.impl.sdk.i iVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return str2;
        }
        try {
            return jSONObject.getString(str);
        } catch (JSONException e) {
            if (iVar == null) {
                return str2;
            }
            o v = iVar.v();
            v.b("JsonUtils", "Failed to retrieve string property for key = " + str, e);
            return str2;
        }
    }

    public static List b(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(a(jSONArray.get(i)));
        }
        return arrayList;
    }

    public static Map<String, Object> b(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, a(jSONObject.get(next)));
        }
        return hashMap;
    }

    public static JSONArray b(JSONObject jSONObject, String str, JSONArray jSONArray, com.applovin.impl.sdk.i iVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return jSONArray;
        }
        try {
            return jSONObject.getJSONArray(str);
        } catch (JSONException e) {
            if (iVar == null) {
                return jSONArray;
            }
            o v = iVar.v();
            v.b("JsonUtils", "Failed to retrieve JSON array for key = " + str, e);
            return jSONArray;
        }
    }

    public static JSONObject b(JSONObject jSONObject, String str, JSONObject jSONObject2, com.applovin.impl.sdk.i iVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return jSONObject2;
        }
        try {
            return jSONObject.getJSONObject(str);
        } catch (JSONException e) {
            if (iVar == null) {
                return jSONObject2;
            }
            o v = iVar.v();
            v.b("JsonUtils", "Failed to retrieve JSON property for key = " + str, e);
            return jSONObject2;
        }
    }

    public static void b(JSONObject jSONObject, String str, long j, com.applovin.impl.sdk.i iVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, j);
            } catch (JSONException e) {
                if (iVar != null) {
                    o v = iVar.v();
                    v.b("JsonUtils", "Failed to put long property for key = " + str, e);
                }
            }
        }
    }

    public static Bundle c(JSONObject jSONObject) {
        if (jSONObject == null || jSONObject.length() == 0) {
            return Bundle.EMPTY;
        }
        Bundle bundle = new Bundle();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (jSONObject.isNull(next)) {
                bundle.putString(next, null);
            } else {
                Object opt = jSONObject.opt(next);
                if (opt instanceof JSONObject) {
                    bundle.putBundle(next, c((JSONObject) opt));
                } else if (opt instanceof JSONArray) {
                    JSONArray jSONArray = (JSONArray) opt;
                    if (jSONArray.length() == 0) {
                        bundle.putStringArrayList(next, new ArrayList(0));
                    } else if (a(jSONArray, 0, (Object) null, (com.applovin.impl.sdk.i) null) instanceof String) {
                        ArrayList arrayList = new ArrayList(jSONArray.length());
                        for (int i = 0; i < jSONArray.length(); i++) {
                            arrayList.add((String) a(jSONArray, i, (Object) null, (com.applovin.impl.sdk.i) null));
                        }
                        bundle.putStringArrayList(next, arrayList);
                    } else {
                        bundle.putParcelableArrayList(next, a(jSONArray));
                    }
                } else if (opt instanceof Boolean) {
                    bundle.putBoolean(next, ((Boolean) opt).booleanValue());
                } else if (opt instanceof String) {
                    bundle.putString(next, (String) opt);
                } else if (opt instanceof Integer) {
                    bundle.putInt(next, ((Integer) opt).intValue());
                } else if (opt instanceof Long) {
                    bundle.putLong(next, ((Long) opt).longValue());
                } else if (opt instanceof Double) {
                    bundle.putDouble(next, ((Double) opt).doubleValue());
                }
            }
        }
        return bundle;
    }

    public static String d(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        try {
            return jSONObject.toString(4);
        } catch (JSONException unused) {
            return jSONObject.toString();
        }
    }
}
