package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.security.NetworkSecurityPolicy;
import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.d;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.sdk.precache.DownloadManager;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class h {
    private static final int[] a = {7, 4, 2, 1, 11};
    private static final int[] b = {5, 6, 10, 3, 9, 8, 14};
    private static final int[] c = {15, 12, 13};

    public static String a(InputStream inputStream, i iVar) {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[((Integer) iVar.a(c.dB)).intValue()];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    return byteArrayOutputStream.toString("UTF-8");
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (Throwable th) {
            iVar.v().b("ConnectionUtils", "Encountered error while reading stream", th);
            return null;
        }
    }

    public static String a(String str, i iVar) {
        return a((String) iVar.a(c.aI), str, iVar);
    }

    public static String a(String str, String str2, i iVar) {
        if (str == null || str.length() < 4) {
            throw new IllegalArgumentException("Invalid domain specified");
        } else if (str2 == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (iVar != null) {
            return str + str2;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static JSONObject a(JSONObject jSONObject) throws JSONException {
        return (JSONObject) jSONObject.getJSONArray("results").get(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.b.d.a(com.applovin.impl.sdk.b.c<?>, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.c<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.b.d.a(java.lang.String, com.applovin.impl.sdk.b.c):com.applovin.impl.sdk.b.c<ST>
      com.applovin.impl.sdk.b.d.a(com.applovin.impl.sdk.b.c<?>, java.lang.Object):void */
    public static void a(int i, i iVar) {
        StringBuilder sb;
        String str;
        d C = iVar.C();
        if (i == 401) {
            C.a(c.S, "");
            C.a(c.T, "");
            C.a();
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(iVar.t());
            str = "\" is rejected by AppLovin. Please make sure the SDK key is correct.";
        } else if (i == 418) {
            C.a((c<?>) c.R, (Object) true);
            C.a();
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(iVar.t());
            str = "\" has been blocked. Please contact AppLovin support at support@applovin.com.";
        } else if ((i >= 400 && i < 500) || i == -1) {
            iVar.f();
            return;
        } else {
            return;
        }
        sb.append(str);
        o.i("AppLovinSdk", sb.toString());
    }

    public static void a(JSONObject jSONObject, boolean z, i iVar) {
        iVar.ac().a(jSONObject, z);
    }

    public static boolean a() {
        return a((String) null);
    }

    private static boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(Context context) {
        if (context.getSystemService("connectivity") == null) {
            return true;
        }
        NetworkInfo b2 = b(context);
        if (b2 != null) {
            return b2.isConnected();
        }
        return false;
    }

    public static boolean a(String str) {
        if (g.g()) {
            return (!g.h() || TextUtils.isEmpty(str)) ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(str);
        }
        return true;
    }

    private static NetworkInfo b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            return connectivityManager.getActiveNetworkInfo();
        }
        return null;
    }

    public static String b(String str, i iVar) {
        return a((String) iVar.a(c.aJ), str, iVar);
    }

    public static void c(JSONObject jSONObject, i iVar) {
        String b2 = i.b(jSONObject, "persisted_data", (String) null, iVar);
        if (m.b(b2)) {
            iVar.a(e.x, b2);
            iVar.v().c("ConnectionUtils", "Updated persisted data");
        }
    }

    public static void d(JSONObject jSONObject, i iVar) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (iVar != null) {
            try {
                if (jSONObject.has(DownloadManager.SETTINGS)) {
                    d C = iVar.C();
                    if (!jSONObject.isNull(DownloadManager.SETTINGS)) {
                        C.a(jSONObject.getJSONObject(DownloadManager.SETTINGS));
                        C.a();
                        iVar.v().b("ConnectionUtils", "New settings processed");
                    }
                }
            } catch (JSONException e) {
                iVar.v().b("ConnectionUtils", "Unable to parse settings out of API response", e);
            }
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static Map<String, String> e(i iVar) {
        String str;
        HashMap hashMap = new HashMap();
        String str2 = (String) iVar.a(c.T);
        if (m.b(str2)) {
            str = "device_token";
        } else {
            if (!((Boolean) iVar.a(c.eJ)).booleanValue()) {
                str2 = iVar.t();
                str = "api_key";
            }
            hashMap.put("sc", m.d((String) iVar.a(c.V)));
            hashMap.put("sc2", m.d((String) iVar.a(c.W)));
            hashMap.put("server_installed_at", m.d((String) iVar.a(c.X)));
            p.a("persisted_data", m.d((String) iVar.a(e.x)), hashMap);
            return hashMap;
        }
        hashMap.put(str, str2);
        hashMap.put("sc", m.d((String) iVar.a(c.V)));
        hashMap.put("sc2", m.d((String) iVar.a(c.W)));
        hashMap.put("server_installed_at", m.d((String) iVar.a(c.X)));
        p.a("persisted_data", m.d((String) iVar.a(e.x)), hashMap);
        return hashMap;
    }

    public static void e(JSONObject jSONObject, i iVar) {
        JSONArray b2 = i.b(jSONObject, "zones", (JSONArray) null, iVar);
        if (b2 != null) {
            Iterator<com.applovin.impl.sdk.ad.d> it = iVar.W().a(b2).iterator();
            while (it.hasNext()) {
                com.applovin.impl.sdk.ad.d next = it.next();
                if (next.d()) {
                    iVar.p().preloadAds(next);
                } else {
                    iVar.o().preloadAds(next);
                }
            }
            iVar.T().a(iVar.W().a());
            iVar.U().a(iVar.W().a());
        }
    }

    public static String f(i iVar) {
        NetworkInfo b2 = b(iVar.D());
        if (b2 == null) {
            return "unknown";
        }
        int type = b2.getType();
        int subtype = b2.getSubtype();
        return type == 1 ? ConnectivityService.NETWORK_TYPE_WIFI : type == 0 ? a(subtype, a) ? "2g" : a(subtype, b) ? ConnectivityService.NETWORK_TYPE_3G : a(subtype, c) ? "4g" : "mobile" : "unknown";
    }

    public static void f(JSONObject jSONObject, i iVar) {
        JSONObject b2 = i.b(jSONObject, "variables", (JSONObject) null, iVar);
        if (b2 != null) {
            iVar.s().updateVariables(b2);
        }
    }

    public static String g(i iVar) {
        return a((String) iVar.a(c.aG), "4.0/ad", iVar);
    }

    public static String h(i iVar) {
        return a((String) iVar.a(c.aH), "4.0/ad", iVar);
    }

    public static String i(i iVar) {
        return a((String) iVar.a(c.aM), "1.0/variable_config", iVar);
    }

    public static String j(i iVar) {
        return a((String) iVar.a(c.aN), "1.0/variable_config", iVar);
    }
}
