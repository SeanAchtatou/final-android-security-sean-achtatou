package com.applovin.impl.sdk;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.SensorManager;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.applovin.impl.sdk.d.i;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.l;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinSdkUtils;
import com.facebook.places.model.PlaceFields;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.ironsource.sdk.constants.Constants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class j {
    private static String e;
    private static String f;
    private static int g;
    private final i a;
    /* access modifiers changed from: private */
    public final o b;
    /* access modifiers changed from: private */
    public final Context c;
    private final Map<Class, Object> d;
    /* access modifiers changed from: private */
    public final AtomicReference<a> h = new AtomicReference<>();

    public static class a {
        public boolean a;
        public String b = "";
    }

    public static class b {
        public String a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public long g;
    }

    public static class c {
        public int a = -1;
        public int b = -1;
    }

    public static class d {
        public boolean A;
        public boolean B;
        public int C = -1;
        public String D;
        public long E;
        public e F = new e();
        public Boolean G;
        public Boolean H;
        public boolean I;
        public String a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public int h;
        public String i;
        public String j;
        public Locale k;
        public String l;
        public float m;
        public int n;
        public float o;
        public float p;
        public double q;
        public double r;
        public int s;
        public boolean t;
        public c u;
        public int v;
        public String w;
        public boolean x;
        public boolean y;
        public boolean z;
    }

    public static class e {
        public long a = -1;
        public long b = -1;
        public long c = -1;
        public boolean d = false;
    }

    protected j(i iVar) {
        if (iVar != null) {
            this.a = iVar;
            this.b = iVar.v();
            this.c = iVar.D();
            this.d = Collections.synchronizedMap(new HashMap());
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private d a(d dVar) {
        if (dVar == null) {
            dVar = new d();
        }
        dVar.G = f.a(this.c);
        dVar.H = f.b(this.c);
        dVar.u = ((Boolean) this.a.a(com.applovin.impl.sdk.b.c.dZ)).booleanValue() ? j() : null;
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.ek)).booleanValue()) {
            dVar.t = n();
        }
        try {
            AudioManager audioManager = (AudioManager) this.c.getSystemService("audio");
            if (audioManager != null) {
                dVar.v = (int) (((float) audioManager.getStreamVolume(3)) * ((Float) this.a.a(com.applovin.impl.sdk.b.c.el)).floatValue());
            }
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect volume", th);
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.em)).booleanValue()) {
            if (e == null) {
                String r = r();
                if (!m.b(r)) {
                    r = "";
                }
                e = r;
            }
            if (m.b(e)) {
                dVar.w = e;
            }
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.ed)).booleanValue()) {
            try {
                dVar.E = Environment.getDataDirectory().getFreeSpace();
            } catch (Throwable th2) {
                dVar.E = -1;
                this.b.b("DataCollector", "Unable to collect free space.", th2);
            }
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.ee)).booleanValue()) {
            try {
                ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                ((ActivityManager) this.c.getSystemService("activity")).getMemoryInfo(memoryInfo);
                dVar.F.b = memoryInfo.availMem;
                dVar.F.d = memoryInfo.lowMemory;
                dVar.F.c = memoryInfo.threshold;
                dVar.F.a = memoryInfo.totalMem;
            } catch (Throwable th3) {
                this.b.b("DataCollector", "Unable to collect memory info.", th3);
            }
        }
        String str = (String) this.a.C().a(com.applovin.impl.sdk.b.c.eo);
        int i = 0;
        if (!str.equalsIgnoreCase(f)) {
            try {
                f = str;
                PackageInfo packageInfo = this.c.getPackageManager().getPackageInfo(str, 0);
                dVar.s = packageInfo.versionCode;
                g = packageInfo.versionCode;
            } catch (Throwable unused) {
                g = 0;
            }
        } else {
            dVar.s = g;
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.ea)).booleanValue()) {
            dVar.z = AppLovinSdkUtils.isTablet(this.c);
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eb)).booleanValue()) {
            dVar.A = m();
        }
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.ec)).booleanValue()) {
            String k = k();
            if (!TextUtils.isEmpty(k)) {
                dVar.D = k;
            }
        }
        dVar.l = g();
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.ef)).booleanValue()) {
            dVar.B = p.d();
        }
        if (g.f()) {
            try {
                if (((PowerManager) this.c.getSystemService("power")).isPowerSaveMode()) {
                    i = 1;
                }
                dVar.C = i;
            } catch (Throwable th4) {
                this.b.b("DataCollector", "Unable to collect power saving mode", th4);
            }
        }
        return dVar;
    }

    private String a(int i) {
        if (i == 1) {
            return "receiver";
        }
        if (i == 2) {
            return "speaker";
        }
        if (i == 4 || i == 3) {
            return "headphones";
        }
        if (i == 8) {
            return "bluetootha2dpoutput";
        }
        if (i == 13 || i == 19 || i == 5 || i == 6 || i == 12 || i == 11) {
            return "lineout";
        }
        if (i == 9 || i == 10) {
            return "hdmioutput";
        }
        return null;
    }

    private boolean a(String str) {
        if (str != null) {
            Context context = this.c;
            if (context != null) {
                return k.a(str, context.getPackageName(), this.c.getPackageManager()) == 0;
            }
            throw new IllegalArgumentException("No context specified");
        }
        throw new IllegalArgumentException("No permission name specified");
    }

    private boolean a(String str, com.applovin.impl.sdk.b.c<String> cVar) {
        for (String startsWith : com.applovin.impl.sdk.utils.e.a((String) this.a.a(cVar))) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private String b(String str) {
        int length = str.length();
        int[] iArr = {11, 12, 10, 3, 2, 1, 15, 10, 15, 14};
        int length2 = iArr.length;
        char[] cArr = new char[length];
        for (int i = 0; i < length; i++) {
            cArr[i] = str.charAt(i);
            for (int i2 = length2 - 1; i2 >= 0; i2--) {
                cArr[i] = (char) (cArr[i] ^ iArr[i2]);
            }
        }
        return new String(cArr);
    }

    private Map<String, String> f() {
        return a(null, false, true);
    }

    private String g() {
        try {
            int d2 = p.d(this.c);
            return d2 == 1 ? Constants.ParametersKeys.ORIENTATION_PORTRAIT : d2 == 2 ? Constants.ParametersKeys.ORIENTATION_LANDSCAPE : Constants.ParametersKeys.ORIENTATION_NONE;
        } catch (Throwable th) {
            this.a.v().b("DataCollector", "Encountered error while attempting to collect application orientation", th);
            return Constants.ParametersKeys.ORIENTATION_NONE;
        }
    }

    private a h() {
        if (i()) {
            try {
                a aVar = new a();
                AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.c);
                aVar.a = advertisingIdInfo.isLimitAdTrackingEnabled();
                aVar.b = advertisingIdInfo.getId();
                return aVar;
            } catch (Throwable th) {
                this.b.b("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }", th);
            }
        } else {
            o.i("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }");
            return new a();
        }
    }

    private boolean i() {
        return p.e("com.google.android.gms.ads.identifier.AdvertisingIdClient");
    }

    private c j() {
        try {
            c cVar = new c();
            Intent registerReceiver = this.c.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = -1;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra("level", -1) : -1;
            int intExtra2 = registerReceiver != null ? registerReceiver.getIntExtra("scale", -1) : -1;
            if (intExtra <= 0 || intExtra2 <= 0) {
                cVar.b = -1;
            } else {
                cVar.b = (int) ((((float) intExtra) / ((float) intExtra2)) * 100.0f);
            }
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("status", -1);
            }
            cVar.a = i;
            return cVar;
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect battery info", th);
            return null;
        }
    }

    private String k() {
        try {
            AudioManager audioManager = (AudioManager) this.c.getSystemService("audio");
            StringBuilder sb = new StringBuilder();
            if (g.g()) {
                for (AudioDeviceInfo type : audioManager.getDevices(2)) {
                    String a2 = a(type.getType());
                    if (!TextUtils.isEmpty(a2)) {
                        sb.append(a2);
                        sb.append(",");
                    }
                }
            } else {
                if (audioManager.isWiredHeadsetOn()) {
                    sb.append("headphones");
                    sb.append(",");
                }
                if (audioManager.isBluetoothA2dpOn()) {
                    sb.append("bluetootha2dpoutput");
                }
            }
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) == ',') {
                sb.deleteCharAt(sb.length() - 1);
            }
            String sb2 = sb.toString();
            if (TextUtils.isEmpty(sb2)) {
                this.b.b("DataCollector", "No sound outputs detected");
            }
            return sb2;
        } catch (Throwable th) {
            this.b.b("DataCollector", "Unable to collect sound outputs", th);
            return null;
        }
    }

    private double l() {
        double offset = (double) TimeZone.getDefault().getOffset(new Date().getTime());
        Double.isNaN(offset);
        double round = (double) Math.round((offset * 10.0d) / 3600000.0d);
        Double.isNaN(round);
        return round / 10.0d;
    }

    private boolean m() {
        try {
            PackageManager packageManager = this.c.getPackageManager();
            return g.f() ? packageManager.hasSystemFeature("android.software.leanback") : g.c() ? packageManager.hasSystemFeature("android.hardware.type.television") : !packageManager.hasSystemFeature("android.hardware.touchscreen");
        } catch (Throwable th) {
            this.b.b("DataCollector", "Failed to determine if device is TV.", th);
            return false;
        }
    }

    private boolean n() {
        try {
            return o() || p();
        } catch (Throwable unused) {
            return false;
        }
    }

    private boolean o() {
        String str = Build.TAGS;
        return str != null && str.contains(b("lz}$blpz"));
    }

    private boolean p() {
        for (String b2 : new String[]{"&zpz}ld&hyy&Z|yl{|zl{'hyb", "&zk`g&z|", "&zpz}ld&k`g&z|", "&zpz}ld&qk`g&z|", "&mh}h&efjhe&qk`g&z|", "&mh}h&efjhe&k`g&z|", "&zpz}ld&zm&qk`g&z|", "&zpz}ld&k`g&oh`ezhol&z|", "&mh}h&efjhe&z|"}) {
            if (new File(b(b2)).exists()) {
                return true;
            }
        }
        return false;
    }

    private boolean q() {
        return a(Build.DEVICE, com.applovin.impl.sdk.b.c.eh) || a(Build.HARDWARE, com.applovin.impl.sdk.b.c.eg) || a(Build.MANUFACTURER, com.applovin.impl.sdk.b.c.ei) || a(Build.MODEL, com.applovin.impl.sdk.b.c.ej);
    }

    private String r() {
        final AtomicReference atomicReference = new AtomicReference();
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        new Handler(this.c.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    atomicReference.set(new WebView(j.this.c).getSettings().getUserAgentString());
                } catch (Throwable th) {
                    countDownLatch.countDown();
                    throw th;
                }
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await(((Long) this.a.a(com.applovin.impl.sdk.b.c.en)).longValue(), TimeUnit.MILLISECONDS);
        } catch (Throwable unused) {
        }
        return (String) atomicReference.get();
    }

    /* access modifiers changed from: package-private */
    public String a() {
        String encodeToString = Base64.encodeToString(new JSONObject(f()).toString().getBytes(Charset.defaultCharset()), 2);
        if (!((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eK)).booleanValue()) {
            return encodeToString;
        }
        return l.a(encodeToString, this.a.t(), p.a(this.a));
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x03df  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x03ff  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x041a  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0435  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0442  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x045e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map<java.lang.String, java.lang.String> a(java.util.Map<java.lang.String, java.lang.String> r6, boolean r7, boolean r8) {
        /*
            r5 = this;
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            com.applovin.impl.sdk.j$d r1 = r5.b()
            java.lang.String r2 = r1.d
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "brand"
            r0.put(r3, r2)
            java.lang.String r2 = r1.e
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "brand_name"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "hardware"
            r0.put(r3, r2)
            int r2 = r1.h
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "api_level"
            r0.put(r3, r2)
            java.lang.String r2 = r1.j
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "carrier"
            r0.put(r3, r2)
            java.lang.String r2 = r1.i
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "country_code"
            r0.put(r3, r2)
            java.util.Locale r2 = r1.k
            java.lang.String r2 = r2.toString()
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "locale"
            r0.put(r3, r2)
            java.lang.String r2 = r1.a
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "model"
            r0.put(r3, r2)
            java.lang.String r2 = r1.b
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "os"
            r0.put(r3, r2)
            java.lang.String r2 = r1.c
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "platform"
            r0.put(r3, r2)
            java.lang.String r2 = r1.g
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "revision"
            r0.put(r3, r2)
            java.lang.String r2 = r1.l
            java.lang.String r3 = "orientation_lock"
            r0.put(r3, r2)
            double r2 = r1.r
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "tz_offset"
            r0.put(r3, r2)
            boolean r2 = r1.I
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "aida"
            r0.put(r3, r2)
            int r2 = r1.s
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "wvvc"
            r0.put(r3, r2)
            float r2 = r1.m
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "adns"
            r0.put(r3, r2)
            int r2 = r1.n
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "adnsd"
            r0.put(r3, r2)
            float r2 = r1.o
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "xdpi"
            r0.put(r3, r2)
            float r2 = r1.p
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "ydpi"
            r0.put(r3, r2)
            double r2 = r1.q
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "screen_size_in"
            r0.put(r3, r2)
            boolean r2 = r1.x
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "sim"
            r0.put(r3, r2)
            boolean r2 = r1.y
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "gy"
            r0.put(r3, r2)
            boolean r2 = r1.z
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "is_tablet"
            r0.put(r3, r2)
            boolean r2 = r1.A
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "tv"
            r0.put(r3, r2)
            boolean r2 = r1.B
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "vs"
            r0.put(r3, r2)
            int r2 = r1.C
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "lpm"
            r0.put(r3, r2)
            long r2 = r1.E
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "fs"
            r0.put(r3, r2)
            com.applovin.impl.sdk.j$e r2 = r1.F
            long r2 = r2.b
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "fm"
            r0.put(r3, r2)
            com.applovin.impl.sdk.j$e r2 = r1.F
            long r2 = r2.a
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "tm"
            r0.put(r3, r2)
            com.applovin.impl.sdk.j$e r2 = r1.F
            long r2 = r2.c
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "lmt"
            r0.put(r3, r2)
            com.applovin.impl.sdk.j$e r2 = r1.F
            boolean r2 = r2.d
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "lm"
            r0.put(r3, r2)
            boolean r2 = r1.t
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "adr"
            r0.put(r3, r2)
            int r2 = r1.v
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "volume"
            r0.put(r3, r2)
            java.lang.String r2 = r1.w
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "ua"
            com.applovin.impl.sdk.utils.p.a(r3, r2, r0)
            java.lang.String r2 = r1.D
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "so"
            com.applovin.impl.sdk.utils.p.a(r3, r2, r0)
            com.applovin.impl.sdk.j$c r2 = r1.u
            if (r2 == 0) goto L_0x01ac
            int r3 = r2.a
            java.lang.String r3 = java.lang.String.valueOf(r3)
            java.lang.String r4 = "act"
            r0.put(r4, r3)
            int r2 = r2.b
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "acm"
            r0.put(r3, r2)
        L_0x01ac:
            java.lang.Boolean r2 = r1.G
            if (r2 == 0) goto L_0x01b9
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = "huc"
            r0.put(r3, r2)
        L_0x01b9:
            java.lang.Boolean r1 = r1.H
            if (r1 == 0) goto L_0x01c6
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "aru"
            r0.put(r2, r1)
        L_0x01c6:
            android.content.Context r1 = r5.c
            android.graphics.Point r1 = com.applovin.impl.sdk.utils.g.a(r1)
            int r2 = r1.x
            java.lang.String r2 = java.lang.Integer.toString(r2)
            java.lang.String r3 = "dx"
            r0.put(r3, r2)
            int r1 = r1.y
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.String r2 = "dy"
            r0.put(r2, r1)
            java.lang.String r1 = "accept"
            java.lang.String r2 = "custom_size,launch_app,video"
            r0.put(r1, r2)
            com.applovin.impl.sdk.i r1 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.String> r2 = com.applovin.impl.sdk.b.c.S
            java.lang.Object r1 = r1.a(r2)
            java.lang.String r2 = "api_did"
            r0.put(r2, r1)
            java.lang.String r1 = com.applovin.sdk.AppLovinSdk.VERSION
            java.lang.String r2 = "sdk_version"
            r0.put(r2, r1)
            r1 = 131(0x83, float:1.84E-43)
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.String r2 = "build"
            r0.put(r2, r1)
            java.lang.String r1 = "format"
            java.lang.String r2 = "json"
            r0.put(r1, r2)
            com.applovin.impl.sdk.j$b r1 = r5.c()
            java.lang.String r2 = r1.b
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "app_version"
            r0.put(r3, r2)
            long r2 = r1.g
            java.lang.String r2 = java.lang.Long.toString(r2)
            java.lang.String r3 = "ia"
            r0.put(r3, r2)
            java.lang.String r2 = r1.e
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "tg"
            r0.put(r3, r2)
            java.lang.String r2 = r1.d
            java.lang.String r3 = "installer_name"
            r0.put(r3, r2)
            java.lang.String r1 = r1.f
            java.lang.String r2 = "debug"
            r0.put(r2, r1)
            com.applovin.impl.sdk.i r1 = r5.a
            java.lang.String r1 = r1.n()
            java.lang.String r1 = com.applovin.impl.sdk.utils.m.d(r1)
            java.lang.String r2 = "mediation_provider"
            com.applovin.impl.sdk.utils.p.a(r2, r1, r0)
            com.applovin.impl.sdk.i r1 = r5.a
            java.lang.String r1 = com.applovin.impl.sdk.utils.h.f(r1)
            java.lang.String r2 = "network"
            r0.put(r2, r1)
            com.applovin.impl.sdk.i r1 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.String> r2 = com.applovin.impl.sdk.b.c.dY
            java.lang.Object r1 = r1.a(r2)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "plugin_version"
            com.applovin.impl.sdk.utils.p.a(r2, r1, r0)
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r1 = "preloading"
            r0.put(r1, r7)
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.sdk.AppLovinSdkSettings r7 = r7.l()
            boolean r7 = r7.isTestAdsEnabled()
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            java.lang.String r1 = "test_ads"
            com.applovin.impl.sdk.utils.p.a(r1, r7, r0)
            com.applovin.impl.sdk.i r7 = r5.a
            boolean r7 = r7.H()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r1 = "first_install"
            r0.put(r1, r7)
            com.applovin.impl.sdk.i r7 = r5.a
            boolean r7 = r7.I()
            r1 = 1
            r7 = r7 ^ r1
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r2 = "first_install_v2"
            r0.put(r2, r7)
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r2 = com.applovin.impl.sdk.b.c.eJ
            java.lang.Object r7 = r7.a(r2)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 != 0) goto L_0x02c2
            com.applovin.impl.sdk.i r7 = r5.a
            java.lang.String r7 = r7.t()
            java.lang.String r2 = "sdk_key"
            r0.put(r2, r7)
        L_0x02c2:
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.String> r2 = com.applovin.impl.sdk.b.c.V
            java.lang.Object r7 = r7.a(r2)
            java.lang.String r2 = "sc"
            r0.put(r2, r7)
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.String> r2 = com.applovin.impl.sdk.b.c.W
            java.lang.Object r7 = r7.a(r2)
            java.lang.String r2 = "sc2"
            r0.put(r2, r7)
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.String> r2 = com.applovin.impl.sdk.b.c.X
            java.lang.Object r7 = r7.a(r2)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r7 = com.applovin.impl.sdk.utils.m.d(r7)
            java.lang.String r2 = "server_installed_at"
            r0.put(r2, r7)
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.b.e<java.lang.String> r2 = com.applovin.impl.sdk.b.e.x
            java.lang.Object r7 = r7.a(r2)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r7 = com.applovin.impl.sdk.utils.m.d(r7)
            java.lang.String r2 = "persisted_data"
            com.applovin.impl.sdk.utils.p.a(r2, r7, r0)
            android.content.Context r7 = r5.c
            java.lang.String r2 = "android.permission.WRITE_EXTERNAL_STORAGE"
            boolean r7 = com.applovin.impl.sdk.utils.g.a(r2, r7)
            java.lang.String r7 = java.lang.Boolean.toString(r7)
            java.lang.String r2 = "v1"
            r0.put(r2, r7)
            java.lang.String r7 = "true"
            java.lang.String r2 = "v2"
            r0.put(r2, r7)
            java.lang.String r2 = "v3"
            r0.put(r2, r7)
            java.lang.String r2 = "v4"
            r0.put(r2, r7)
            java.lang.String r2 = "v5"
            r0.put(r2, r7)
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r2 = com.applovin.impl.sdk.b.c.ep
            java.lang.Object r7 = r7.a(r2)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x038a
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.c.h r7 = r7.L()
            com.applovin.impl.sdk.c.g r2 = com.applovin.impl.sdk.c.g.b
            long r2 = r7.b(r2)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "li"
            r0.put(r3, r2)
            com.applovin.impl.sdk.c.g r2 = com.applovin.impl.sdk.c.g.d
            long r2 = r7.b(r2)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "si"
            r0.put(r3, r2)
            com.applovin.impl.sdk.c.g r2 = com.applovin.impl.sdk.c.g.h
            long r2 = r7.b(r2)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "pf"
            r0.put(r3, r2)
            com.applovin.impl.sdk.c.g r2 = com.applovin.impl.sdk.c.g.n
            long r2 = r7.b(r2)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "mpf"
            r0.put(r3, r2)
            com.applovin.impl.sdk.c.g r2 = com.applovin.impl.sdk.c.g.i
            long r2 = r7.b(r2)
            java.lang.String r7 = java.lang.String.valueOf(r2)
            java.lang.String r2 = "gpf"
            r0.put(r2, r7)
        L_0x038a:
            android.content.Context r7 = r5.c
            java.lang.String r7 = r7.getPackageName()
            java.lang.String r7 = com.applovin.impl.sdk.utils.m.e(r7)
            java.lang.String r2 = "vz"
            r0.put(r2, r7)
            com.applovin.impl.sdk.i r7 = r5.a
            boolean r7 = r7.h()
            java.lang.String r7 = java.lang.Boolean.toString(r7)
            java.lang.String r2 = "pnr"
            r0.put(r2, r7)
            if (r8 == 0) goto L_0x03cd
            java.util.concurrent.atomic.AtomicReference<com.applovin.impl.sdk.j$a> r7 = r5.h
            java.lang.Object r7 = r7.get()
            com.applovin.impl.sdk.j$a r7 = (com.applovin.impl.sdk.j.a) r7
            if (r7 == 0) goto L_0x03b8
            r5.e()
            goto L_0x03d7
        L_0x03b8:
            boolean r7 = com.applovin.impl.sdk.utils.p.b()
            if (r7 == 0) goto L_0x03cd
            com.applovin.impl.sdk.j$a r7 = new com.applovin.impl.sdk.j$a
            r7.<init>()
            java.lang.String r8 = java.lang.Boolean.toString(r1)
            java.lang.String r1 = "inc"
            r0.put(r1, r8)
            goto L_0x03d7
        L_0x03cd:
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.j r7 = r7.O()
            com.applovin.impl.sdk.j$a r7 = r7.d()
        L_0x03d7:
            java.lang.String r8 = r7.b
            boolean r1 = com.applovin.impl.sdk.utils.m.b(r8)
            if (r1 == 0) goto L_0x03e4
            java.lang.String r1 = "idfa"
            r0.put(r1, r8)
        L_0x03e4:
            boolean r7 = r7.a
            java.lang.String r7 = java.lang.Boolean.toString(r7)
            java.lang.String r8 = "dnt"
            r0.put(r8, r7)
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r8 = com.applovin.impl.sdk.b.c.dR
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x040a
            com.applovin.impl.sdk.i r7 = r5.a
            java.lang.String r7 = r7.i()
            java.lang.String r8 = "cuid"
            com.applovin.impl.sdk.utils.p.a(r8, r7, r0)
        L_0x040a:
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r8 = com.applovin.impl.sdk.b.c.dU
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0425
            com.applovin.impl.sdk.i r7 = r5.a
            java.lang.String r7 = r7.j()
            java.lang.String r8 = "compass_random_token"
            r0.put(r8, r7)
        L_0x0425:
            com.applovin.impl.sdk.i r7 = r5.a
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r8 = com.applovin.impl.sdk.b.c.dW
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0440
            com.applovin.impl.sdk.i r7 = r5.a
            java.lang.String r7 = r7.k()
            java.lang.String r8 = "applovin_random_token"
            r0.put(r8, r7)
        L_0x0440:
            if (r6 == 0) goto L_0x0445
            r0.putAll(r6)
        L_0x0445:
            java.util.UUID r6 = java.util.UUID.randomUUID()
            java.lang.String r6 = r6.toString()
            java.lang.String r7 = "rid"
            r0.put(r7, r6)
            com.applovin.impl.sdk.i r6 = r5.a
            com.applovin.impl.sdk.network.a r6 = r6.J()
            com.applovin.impl.sdk.network.a$b r6 = r6.a()
            if (r6 == 0) goto L_0x048e
            long r7 = r6.a()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r8 = "lrm_ts_ms"
            r0.put(r8, r7)
            java.lang.String r7 = r6.b()
            java.lang.String r8 = "lrm_url"
            r0.put(r8, r7)
            long r7 = r6.d()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r8 = "lrm_ct_ms"
            r0.put(r8, r7)
            long r6 = r6.c()
            java.lang.String r6 = java.lang.String.valueOf(r6)
            java.lang.String r7 = "lrm_rs"
            r0.put(r7, r6)
        L_0x048e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.j.a(java.util.Map, boolean, boolean):java.util.Map");
    }

    public d b() {
        d dVar;
        TelephonyManager telephonyManager;
        Object obj = this.d.get(d.class);
        if (obj != null) {
            dVar = (d) obj;
        } else {
            dVar = new d();
            dVar.k = Locale.getDefault();
            dVar.a = Build.MODEL;
            dVar.b = Build.VERSION.RELEASE;
            dVar.c = "android";
            dVar.d = Build.MANUFACTURER;
            dVar.e = Build.BRAND;
            dVar.f = Build.HARDWARE;
            dVar.h = Build.VERSION.SDK_INT;
            dVar.g = Build.DEVICE;
            dVar.r = l();
            dVar.x = q();
            dVar.I = i();
            try {
                dVar.y = ((SensorManager) this.c.getSystemService("sensor")).getDefaultSensor(4) != null;
            } catch (Throwable th) {
                this.b.b("DataCollector", "Unable to retrieve gyroscope availability", th);
            }
            if (a("android.permission.READ_PHONE_STATE") && (telephonyManager = (TelephonyManager) this.c.getSystemService((String) PlaceFields.PHONE)) != null) {
                dVar.i = telephonyManager.getSimCountryIso().toUpperCase(Locale.ENGLISH);
                String networkOperatorName = telephonyManager.getNetworkOperatorName();
                try {
                    dVar.j = URLEncoder.encode(networkOperatorName, "UTF-8");
                } catch (UnsupportedEncodingException unused) {
                    dVar.j = networkOperatorName;
                }
            }
            try {
                DisplayMetrics displayMetrics = this.c.getResources().getDisplayMetrics();
                dVar.m = displayMetrics.density;
                dVar.n = displayMetrics.densityDpi;
                dVar.o = displayMetrics.xdpi;
                dVar.p = displayMetrics.ydpi;
                Point a2 = g.a(this.c);
                double sqrt = Math.sqrt(Math.pow((double) a2.x, 2.0d) + Math.pow((double) a2.y, 2.0d));
                double d2 = (double) dVar.o;
                Double.isNaN(d2);
                dVar.q = sqrt / d2;
            } catch (Throwable unused2) {
            }
            this.d.put(d.class, dVar);
        }
        return a(dVar);
    }

    public b c() {
        PackageInfo packageInfo;
        Object obj = this.d.get(b.class);
        if (obj != null) {
            return (b) obj;
        }
        ApplicationInfo applicationInfo = this.c.getApplicationInfo();
        long lastModified = new File(applicationInfo.sourceDir).lastModified();
        PackageManager packageManager = this.c.getPackageManager();
        String str = null;
        try {
            packageInfo = packageManager.getPackageInfo(this.c.getPackageName(), 0);
            try {
                str = packageManager.getInstallerPackageName(applicationInfo.packageName);
            } catch (Throwable unused) {
            }
        } catch (Throwable unused2) {
            packageInfo = null;
        }
        b bVar = new b();
        bVar.c = applicationInfo.packageName;
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        bVar.d = str;
        bVar.g = lastModified;
        bVar.a = String.valueOf(packageManager.getApplicationLabel(applicationInfo));
        if (packageInfo != null) {
            str2 = packageInfo.versionName;
        }
        bVar.b = str2;
        bVar.e = (String) this.a.a(com.applovin.impl.sdk.b.e.g);
        bVar.f = Boolean.toString(p.b(this.a));
        this.d.put(b.class, bVar);
        return bVar;
    }

    public a d() {
        a h2 = h();
        if (!((Boolean) this.a.a(com.applovin.impl.sdk.b.c.dQ)).booleanValue()) {
            return new a();
        }
        if (!h2.a || ((Boolean) this.a.a(com.applovin.impl.sdk.b.c.dP)).booleanValue()) {
            return h2;
        }
        h2.b = "";
        return h2;
    }

    public void e() {
        this.a.K().a(new i(this.a, new i.a() {
            public void a(a aVar) {
                j.this.h.set(aVar);
            }
        }), r.a.ADVERTISING_INFO_COLLECTION);
    }
}
