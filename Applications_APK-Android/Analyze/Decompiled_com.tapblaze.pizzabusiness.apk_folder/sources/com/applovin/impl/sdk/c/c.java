package com.applovin.impl.sdk.c;

import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.d.x;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.m;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    /* access modifiers changed from: private */
    public final i a;
    /* access modifiers changed from: private */
    public final o b;
    private final Object c = new Object();
    private final C0005c d = new C0005c();

    private static class a {
        private final i a;
        private final JSONObject b;

        private a(String str, String str2, String str3, i iVar) {
            this.b = new JSONObject();
            this.a = iVar;
            com.applovin.impl.sdk.utils.i.a(this.b, "pk", str, iVar);
            com.applovin.impl.sdk.utils.i.b(this.b, "ts", System.currentTimeMillis(), iVar);
            if (m.b(str2)) {
                com.applovin.impl.sdk.utils.i.a(this.b, "sk1", str2, iVar);
            }
            if (m.b(str3)) {
                com.applovin.impl.sdk.utils.i.a(this.b, "sk2", str3, iVar);
            }
        }

        /* access modifiers changed from: private */
        public String a() throws OutOfMemoryError {
            return this.b.toString();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.i):long
         arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i]
         candidates:
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.i):float
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.i):java.util.List
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.i):org.json.JSONObject
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.i):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.i):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.i):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.i):long */
        /* access modifiers changed from: package-private */
        public void a(String str, long j) {
            b(str, com.applovin.impl.sdk.utils.i.a(this.b, str, 0L, this.a) + j);
        }

        /* access modifiers changed from: package-private */
        public void a(String str, String str2) {
            JSONArray b2 = com.applovin.impl.sdk.utils.i.b(this.b, str, new JSONArray(), this.a);
            b2.put(str2);
            com.applovin.impl.sdk.utils.i.a(this.b, str, b2, this.a);
        }

        /* access modifiers changed from: package-private */
        public void b(String str, long j) {
            com.applovin.impl.sdk.utils.i.b(this.b, str, j, this.a);
        }

        public String toString() {
            return "AdEventStats{stats='" + this.b + '\'' + '}';
        }
    }

    public class b {
        private final AppLovinAdBase b;
        private final c c;

        public b(AppLovinAdBase appLovinAdBase, c cVar) {
            this.b = appLovinAdBase;
            this.c = cVar;
        }

        public b a(b bVar) {
            this.c.a(bVar, 1, this.b);
            return this;
        }

        public b a(b bVar, long j) {
            this.c.b(bVar, j, this.b);
            return this;
        }

        public b a(b bVar, String str) {
            this.c.a(bVar, str, this.b);
            return this;
        }

        public void a() {
            this.c.e();
        }
    }

    /* renamed from: com.applovin.impl.sdk.c.c$c  reason: collision with other inner class name */
    private class C0005c extends LinkedHashMap<String, a> {
        private C0005c() {
        }

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, a> entry) {
            return size() > ((Integer) c.this.a.a(com.applovin.impl.sdk.b.c.et)).intValue();
        }
    }

    public c(i iVar) {
        if (iVar != null) {
            this.a = iVar;
            this.b = iVar.v();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    /* access modifiers changed from: private */
    public void a(b bVar, long j, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (bVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            synchronized (this.c) {
                b(appLovinAdBase).a(((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eu)).booleanValue() ? bVar.b() : bVar.a(), j);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(b bVar, String str, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (bVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            synchronized (this.d) {
                b(appLovinAdBase).a(((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eu)).booleanValue() ? bVar.b() : bVar.a(), str);
            }
        }
    }

    private void a(JSONObject jSONObject) {
        AnonymousClass1 r0 = new x<Object>(com.applovin.impl.sdk.network.b.a(this.a).a(c()).c(d()).a(h.e(this.a)).b("POST").a(jSONObject).b(((Integer) this.a.a(com.applovin.impl.sdk.b.c.er)).intValue()).a(((Integer) this.a.a(com.applovin.impl.sdk.b.c.es)).intValue()).a(), this.a) {
            public void a(int i) {
                o a2 = c.this.b;
                a2.e("AdEventStatsManager", "Failed to submitted ad stats: " + i);
            }

            public void a(Object obj, int i) {
                o a2 = c.this.b;
                a2.b("AdEventStatsManager", "Ad stats submitted: " + i);
            }
        };
        r0.a(com.applovin.impl.sdk.b.c.aI);
        r0.b(com.applovin.impl.sdk.b.c.aJ);
        this.a.K().a(r0, r.a.BACKGROUND);
    }

    private a b(AppLovinAdBase appLovinAdBase) {
        a aVar;
        synchronized (this.c) {
            String primaryKey = appLovinAdBase.getPrimaryKey();
            aVar = (a) this.d.get(primaryKey);
            if (aVar == null) {
                a aVar2 = new a(primaryKey, appLovinAdBase.getSecondaryKey1(), appLovinAdBase.getSecondaryKey2(), this.a);
                this.d.put(primaryKey, aVar2);
                aVar = aVar2;
            }
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    public void b(b bVar, long j, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (bVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            synchronized (this.c) {
                b(appLovinAdBase).b(((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eu)).booleanValue() ? bVar.b() : bVar.a(), j);
            }
        }
    }

    private String c() {
        return h.a("2.0/s", this.a);
    }

    private String d() {
        return h.b("2.0/s", this.a);
    }

    /* access modifiers changed from: private */
    public void e() {
        HashSet hashSet;
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            synchronized (this.c) {
                hashSet = new HashSet(this.d.size());
                for (a aVar : this.d.values()) {
                    try {
                        hashSet.add(aVar.a());
                    } catch (OutOfMemoryError e) {
                        o oVar = this.b;
                        oVar.b("AdEventStatsManager", "Failed to serialize " + aVar + " due to OOM error", e);
                        b();
                    }
                }
            }
            this.a.a(e.s, hashSet);
        }
    }

    public b a(AppLovinAdBase appLovinAdBase) {
        return new b(appLovinAdBase, this);
    }

    public void a() {
        if (((Boolean) this.a.a(com.applovin.impl.sdk.b.c.eq)).booleanValue()) {
            Set<String> set = (Set) this.a.b(e.s, new HashSet(0));
            this.a.b(e.s);
            if (set == null || set.isEmpty()) {
                this.b.b("AdEventStatsManager", "No serialized ad events found");
                return;
            }
            o oVar = this.b;
            oVar.b("AdEventStatsManager", "De-serializing " + set.size() + " stat ad events");
            JSONArray jSONArray = new JSONArray();
            for (String str : set) {
                try {
                    jSONArray.put(new JSONObject(str));
                } catch (JSONException e) {
                    o oVar2 = this.b;
                    oVar2.b("AdEventStatsManager", "Failed to parse: " + str, e);
                }
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("stats", jSONArray);
                a(jSONObject);
            } catch (JSONException e2) {
                this.b.b("AdEventStatsManager", "Failed to create stats to submit", e2);
            }
        }
    }

    public void b() {
        synchronized (this.c) {
            this.b.b("AdEventStatsManager", "Clearing ad stats...");
            this.d.clear();
        }
    }
}
