package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.d;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinSdk;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.internal.NativeProtocol;
import com.ironsource.sdk.constants.Constants;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class b extends a {
    b(i iVar) {
        super("TaskApiSubmitData", iVar);
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        try {
            this.b.P().c();
            JSONObject a = h.a(jSONObject);
            this.b.C().a(c.S, a.getString("device_id"));
            this.b.C().a(c.T, a.getString("device_token"));
            this.b.C().a();
            h.d(a, this.b);
            h.e(a, this.b);
            String b = com.applovin.impl.sdk.utils.i.b(a, "latest_version", "", this.b);
            if (e(b)) {
                String str = "Current SDK version (" + AppLovinSdk.VERSION + ") is outdated. Please integrate the latest version of the AppLovin SDK (" + b + "). Doing so will improve your CPMs and ensure you have access to the latest revenue earning features.";
                if (com.applovin.impl.sdk.utils.i.a(a, "sdk_update_message")) {
                    str = com.applovin.impl.sdk.utils.i.b(a, "sdk_update_message", str, this.b);
                }
                o.h("AppLovinSdk", str);
            }
            this.b.L().b();
            this.b.M().b();
        } catch (Throwable th) {
            a("Unable to parse API response", th);
        }
    }

    private void b(JSONObject jSONObject) throws JSONException {
        j O = this.b.O();
        j.b c = O.c();
        j.d b = O.b();
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("model", b.a);
        jSONObject2.put("os", b.b);
        jSONObject2.put("brand", b.d);
        jSONObject2.put("brand_name", b.e);
        jSONObject2.put("hardware", b.f);
        jSONObject2.put("sdk_version", b.h);
        jSONObject2.put("revision", b.g);
        jSONObject2.put("adns", (double) b.m);
        jSONObject2.put("adnsd", b.n);
        jSONObject2.put("xdpi", String.valueOf(b.o));
        jSONObject2.put("ydpi", String.valueOf(b.p));
        jSONObject2.put("screen_size_in", String.valueOf(b.q));
        jSONObject2.put("gy", m.a(b.y));
        jSONObject2.put("country_code", b.i);
        jSONObject2.put("carrier", b.j);
        jSONObject2.put("orientation_lock", b.l);
        jSONObject2.put("tz_offset", b.r);
        jSONObject2.put("aida", String.valueOf(b.I));
        jSONObject2.put("adr", m.a(b.t));
        jSONObject2.put("wvvc", b.s);
        jSONObject2.put("volume", b.v);
        jSONObject2.put("type", "android");
        jSONObject2.put("sim", m.a(b.x));
        jSONObject2.put("is_tablet", m.a(b.z));
        jSONObject2.put("lpm", b.C);
        jSONObject2.put("tv", m.a(b.A));
        jSONObject2.put("vs", m.a(b.B));
        jSONObject2.put("fs", b.E);
        jSONObject2.put("fm", String.valueOf(b.F.b));
        jSONObject2.put("tm", String.valueOf(b.F.a));
        jSONObject2.put("lmt", String.valueOf(b.F.c));
        jSONObject2.put("lm", String.valueOf(b.F.d));
        g(jSONObject2);
        Boolean bool = b.G;
        if (bool != null) {
            jSONObject2.put("huc", bool.toString());
        }
        Boolean bool2 = b.H;
        if (bool2 != null) {
            jSONObject2.put("aru", bool2.toString());
        }
        j.c cVar = b.u;
        if (cVar != null) {
            jSONObject2.put("act", cVar.a);
            jSONObject2.put("acm", cVar.b);
        }
        String str = b.w;
        if (m.b(str)) {
            jSONObject2.put("ua", m.d(str));
        }
        String str2 = b.D;
        if (!TextUtils.isEmpty(str2)) {
            jSONObject2.put("so", m.d(str2));
        }
        Locale locale = b.k;
        if (locale != null) {
            jSONObject2.put("locale", m.d(locale.toString()));
        }
        jSONObject.put(DeviceRequestsHelper.DEVICE_INFO_PARAM, jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("package_name", c.c);
        jSONObject3.put("installer_name", c.d);
        jSONObject3.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, c.a);
        jSONObject3.put("app_version", c.b);
        jSONObject3.put("installed_at", c.g);
        jSONObject3.put("tg", c.e);
        jSONObject3.put("applovin_sdk_version", AppLovinSdk.VERSION);
        jSONObject3.put("first_install", String.valueOf(this.b.H()));
        jSONObject3.put("first_install_v2", String.valueOf(!this.b.I()));
        jSONObject3.put(Constants.RequestParameters.DEBUG, Boolean.toString(p.b(this.b)));
        String str3 = (String) this.b.a(c.dY);
        if (m.b(str3)) {
            jSONObject3.put("plugin_version", str3);
        }
        if (((Boolean) this.b.a(c.dR)).booleanValue() && m.b(this.b.i())) {
            jSONObject3.put("cuid", this.b.i());
        }
        if (((Boolean) this.b.a(c.dU)).booleanValue()) {
            jSONObject3.put("compass_random_token", this.b.j());
        }
        if (((Boolean) this.b.a(c.dW)).booleanValue()) {
            jSONObject3.put("applovin_random_token", this.b.k());
        }
        jSONObject.put("app_info", jSONObject3);
    }

    private void c(JSONObject jSONObject) throws JSONException {
        if (((Boolean) this.b.a(c.ep)).booleanValue()) {
            jSONObject.put("stats", this.b.L().c());
        }
        if (((Boolean) this.b.a(c.ab)).booleanValue()) {
            JSONObject b = d.b(g());
            if (b.length() > 0) {
                jSONObject.put("network_response_codes", b);
            }
            if (((Boolean) this.b.a(c.ac)).booleanValue()) {
                d.a(g());
            }
        }
    }

    private void d(JSONObject jSONObject) throws JSONException {
        JSONArray a;
        if (((Boolean) this.b.a(c.ew)).booleanValue() && (a = this.b.P().a()) != null && a.length() > 0) {
            jSONObject.put(ReportDBAdapter.ReportColumns.COLUMN_ERRORS, a);
        }
    }

    private void e(JSONObject jSONObject) throws JSONException {
        JSONArray a;
        if (((Boolean) this.b.a(c.ev)).booleanValue() && (a = this.b.M().a()) != null && a.length() > 0) {
            jSONObject.put("tasks", a);
        }
    }

    private boolean e(String str) {
        return m.b(str) && !AppLovinSdk.VERSION.equals(str) && p.g(str) > AppLovinSdk.VERSION_CODE;
    }

    private void f(JSONObject jSONObject) {
        AnonymousClass1 r0 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(h.a("2.0/device", this.b)).c(h.b("2.0/device", this.b)).a(h.e(this.b)).b("POST").a(jSONObject).a((Object) new JSONObject()).a(((Integer) this.b.a(c.dA)).intValue()).a(), this.b) {
            public void a(int i) {
                h.a(i, this.b);
            }

            public void a(JSONObject jSONObject, int i) {
                b.this.a(jSONObject);
            }
        };
        r0.a(c.aI);
        r0.b(c.aJ);
        this.b.K().a(r0);
    }

    private void g(JSONObject jSONObject) {
        try {
            j.a d = this.b.O().d();
            String str = d.b;
            if (m.b(str)) {
                jSONObject.put("idfa", str);
            }
            jSONObject.put("dnt", Boolean.toString(d.a));
        } catch (Throwable th) {
            a("Failed to populate advertising info", th);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.h;
    }

    public void run() {
        try {
            b("Submitting user data...");
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            c(jSONObject);
            d(jSONObject);
            e(jSONObject);
            f(jSONObject);
        } catch (JSONException e) {
            a("Unable to build JSON message with collected data", e);
            this.b.M().a(a());
        }
    }
}
