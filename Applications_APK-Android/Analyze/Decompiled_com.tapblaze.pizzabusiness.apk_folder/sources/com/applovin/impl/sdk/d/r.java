package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.d;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.p;
import com.ironsource.sdk.constants.Constants;
import com.vungle.warren.AdLoader;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class r {
    private final String a = "TaskManager";
    /* access modifiers changed from: private */
    public final i b;
    /* access modifiers changed from: private */
    public final o c;
    private final ScheduledThreadPoolExecutor d;
    private final ScheduledThreadPoolExecutor e;
    private final ScheduledThreadPoolExecutor f;
    private final ScheduledThreadPoolExecutor g;
    private final ScheduledThreadPoolExecutor h;
    private final ScheduledThreadPoolExecutor i;
    private final ScheduledThreadPoolExecutor j;
    private final ScheduledThreadPoolExecutor k;
    private final ScheduledThreadPoolExecutor l;
    private final ScheduledThreadPoolExecutor m;
    private final ScheduledThreadPoolExecutor n;
    private final ScheduledThreadPoolExecutor o;
    private final ScheduledThreadPoolExecutor p;
    private final ScheduledThreadPoolExecutor q;
    private final ScheduledThreadPoolExecutor r;
    private final ScheduledThreadPoolExecutor s;
    private final ScheduledThreadPoolExecutor t;
    private final ScheduledThreadPoolExecutor u;
    private final ScheduledThreadPoolExecutor v;
    private final ScheduledThreadPoolExecutor w;
    private final List<c> x = new ArrayList(5);
    private final Object y = new Object();
    private boolean z;

    public enum a {
        MAIN,
        TIMEOUT,
        BACKGROUND,
        ADVERTISING_INFO_COLLECTION,
        POSTBACKS,
        CACHING_INTERSTITIAL,
        CACHING_INCENTIVIZED,
        CACHING_OTHER,
        REWARD,
        MEDIATION_MAIN,
        MEDIATION_TIMEOUT,
        MEDIATION_BACKGROUND,
        MEDIATION_POSTBACKS,
        MEDIATION_BANNER,
        MEDIATION_INTERSTITIAL,
        MEDIATION_INCENTIVIZED,
        MEDIATION_REWARD
    }

    private class b implements ThreadFactory {
        private final String b;

        b(String str) {
            this.b = str;
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "AppLovinSdk:" + this.b + ":" + p.a(r.this.b.t()));
            thread.setDaemon(true);
            thread.setPriority(10);
            thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable th) {
                    r.this.c.b("TaskManager", "Caught unhandled exception", th);
                }
            });
            return thread;
        }
    }

    private class c implements Runnable {
        private final String b;
        /* access modifiers changed from: private */
        public final a c;
        /* access modifiers changed from: private */
        public final a d;

        c(a aVar, a aVar2) {
            this.b = aVar.f();
            this.c = aVar;
            this.d = aVar2;
        }

        public void run() {
            long a2;
            o b2;
            StringBuilder sb;
            long currentTimeMillis = System.currentTimeMillis();
            try {
                g.a();
                if (r.this.b.c()) {
                    if (!this.c.h()) {
                        r.this.c.c(this.b, "Task re-scheduled...");
                        r.this.a(this.c, this.d, AdLoader.RETRY_DELAY);
                        a2 = r.this.a(this.d) - 1;
                        b2 = r.this.c;
                        sb = new StringBuilder();
                        sb.append(this.d);
                        sb.append(" queue finished task ");
                        sb.append(this.c.f());
                        sb.append(" with queue size ");
                        sb.append(a2);
                        b2.c("TaskManager", sb.toString());
                    }
                }
                r.this.c.c(this.b, "Task started execution...");
                this.c.run();
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                r.this.b.M().a(this.c.a(), currentTimeMillis2);
                o b3 = r.this.c;
                String str = this.b;
                b3.c(str, "Task executed successfully in " + currentTimeMillis2 + "ms.");
                a2 = r.this.a(this.d) - 1;
                b2 = r.this.c;
                sb = new StringBuilder();
            } catch (Throwable th) {
                o b4 = r.this.c;
                b4.c("TaskManager", this.d + " queue finished task " + this.c.f() + " with queue size " + (r.this.a(this.d) - 1));
                throw th;
            }
            sb.append(this.d);
            sb.append(" queue finished task ");
            sb.append(this.c.f());
            sb.append(" with queue size ");
            sb.append(a2);
            b2.c("TaskManager", sb.toString());
        }
    }

    public r(i iVar) {
        this.b = iVar;
        this.c = iVar.v();
        this.d = a(Constants.ParametersKeys.MAIN);
        this.e = a("timeout");
        this.f = a("back");
        this.g = a("advertising_info_collection");
        this.h = a("postbacks");
        this.i = a("caching_interstitial");
        this.j = a("caching_incentivized");
        this.k = a("caching_other");
        this.l = a("reward");
        this.m = a("mediation_main");
        this.n = a("mediation_timeout");
        this.o = a("mediation_background");
        this.p = a("mediation_postbacks");
        this.q = a("mediation_banner");
        this.r = a("mediation_interstitial");
        this.s = a("mediation_incentivized");
        this.t = a("mediation_reward");
        this.u = a("auxiliary_operations", ((Integer) iVar.a(com.applovin.impl.sdk.b.c.cx)).intValue());
        this.v = a("caching_operations", ((Integer) iVar.a(com.applovin.impl.sdk.b.c.cy)).intValue());
        this.w = a("shared_thread_pool", ((Integer) iVar.a(com.applovin.impl.sdk.b.c.ah)).intValue());
    }

    /* access modifiers changed from: private */
    public long a(a aVar) {
        long taskCount;
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
        if (aVar == a.MAIN) {
            taskCount = this.d.getTaskCount();
            scheduledThreadPoolExecutor = this.d;
        } else if (aVar == a.TIMEOUT) {
            taskCount = this.e.getTaskCount();
            scheduledThreadPoolExecutor = this.e;
        } else if (aVar == a.BACKGROUND) {
            taskCount = this.f.getTaskCount();
            scheduledThreadPoolExecutor = this.f;
        } else if (aVar == a.ADVERTISING_INFO_COLLECTION) {
            taskCount = this.g.getTaskCount();
            scheduledThreadPoolExecutor = this.g;
        } else if (aVar == a.POSTBACKS) {
            taskCount = this.h.getTaskCount();
            scheduledThreadPoolExecutor = this.h;
        } else if (aVar == a.CACHING_INTERSTITIAL) {
            taskCount = this.i.getTaskCount();
            scheduledThreadPoolExecutor = this.i;
        } else if (aVar == a.CACHING_INCENTIVIZED) {
            taskCount = this.j.getTaskCount();
            scheduledThreadPoolExecutor = this.j;
        } else if (aVar == a.CACHING_OTHER) {
            taskCount = this.k.getTaskCount();
            scheduledThreadPoolExecutor = this.k;
        } else if (aVar == a.REWARD) {
            taskCount = this.l.getTaskCount();
            scheduledThreadPoolExecutor = this.l;
        } else if (aVar == a.MEDIATION_MAIN) {
            taskCount = this.m.getTaskCount();
            scheduledThreadPoolExecutor = this.m;
        } else if (aVar == a.MEDIATION_TIMEOUT) {
            taskCount = this.n.getTaskCount();
            scheduledThreadPoolExecutor = this.n;
        } else if (aVar == a.MEDIATION_BACKGROUND) {
            taskCount = this.o.getTaskCount();
            scheduledThreadPoolExecutor = this.o;
        } else if (aVar == a.MEDIATION_POSTBACKS) {
            taskCount = this.p.getTaskCount();
            scheduledThreadPoolExecutor = this.p;
        } else if (aVar == a.MEDIATION_BANNER) {
            taskCount = this.q.getTaskCount();
            scheduledThreadPoolExecutor = this.q;
        } else if (aVar == a.MEDIATION_INTERSTITIAL) {
            taskCount = this.r.getTaskCount();
            scheduledThreadPoolExecutor = this.r;
        } else if (aVar == a.MEDIATION_INCENTIVIZED) {
            taskCount = this.s.getTaskCount();
            scheduledThreadPoolExecutor = this.s;
        } else if (aVar != a.MEDIATION_REWARD) {
            return 0;
        } else {
            taskCount = this.t.getTaskCount();
            scheduledThreadPoolExecutor = this.t;
        }
        return taskCount - scheduledThreadPoolExecutor.getCompletedTaskCount();
    }

    private ScheduledThreadPoolExecutor a(String str) {
        return a(str, 1);
    }

    private ScheduledThreadPoolExecutor a(String str, int i2) {
        return new ScheduledThreadPoolExecutor(i2, new b(str));
    }

    private void a(final Runnable runnable, long j2, final ScheduledExecutorService scheduledExecutorService, boolean z2) {
        if (j2 <= 0) {
            scheduledExecutorService.submit(runnable);
        } else if (z2) {
            d.a(j2, this.b, new Runnable() {
                public void run() {
                    scheduledExecutorService.execute(runnable);
                }
            });
        } else {
            scheduledExecutorService.schedule(runnable, j2, TimeUnit.MILLISECONDS);
        }
    }

    private boolean a(c cVar) {
        if (cVar.c.h()) {
            return false;
        }
        synchronized (this.y) {
            if (this.z) {
                return false;
            }
            this.x.add(cVar);
            return true;
        }
    }

    public void a(a aVar) {
        if (aVar != null) {
            long currentTimeMillis = System.currentTimeMillis();
            try {
                o oVar = this.c;
                oVar.c("TaskManager", "Executing " + aVar.f() + " immediately...");
                aVar.run();
                this.b.M().a(aVar.a(), System.currentTimeMillis() - currentTimeMillis);
                o oVar2 = this.c;
                oVar2.c("TaskManager", aVar.f() + " finished executing...");
            } catch (Throwable th) {
                this.c.b(aVar.f(), "Task failed execution", th);
                this.b.M().a(aVar.a(), true, System.currentTimeMillis() - currentTimeMillis);
            }
        } else {
            this.c.e("TaskManager", "Attempted to execute null task immediately");
        }
    }

    public void a(a aVar, a aVar2) {
        a(aVar, aVar2, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.d.r.a(com.applovin.impl.sdk.d.a, com.applovin.impl.sdk.d.r$a, long, boolean):void
     arg types: [com.applovin.impl.sdk.d.a, com.applovin.impl.sdk.d.r$a, long, int]
     candidates:
      com.applovin.impl.sdk.d.r.a(java.lang.Runnable, long, java.util.concurrent.ScheduledExecutorService, boolean):void
      com.applovin.impl.sdk.d.r.a(com.applovin.impl.sdk.d.a, com.applovin.impl.sdk.d.r$a, long, boolean):void */
    public void a(a aVar, a aVar2, long j2) {
        a(aVar, aVar2, j2, false);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: com.applovin.impl.sdk.d.r$c} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v29, resolved type: com.applovin.impl.sdk.d.a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v30, resolved type: com.applovin.impl.sdk.d.a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v31, resolved type: com.applovin.impl.sdk.d.a} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.applovin.impl.sdk.d.a r12, com.applovin.impl.sdk.d.r.a r13, long r14, boolean r16) {
        /*
            r11 = this;
            r6 = r11
            r1 = r12
            r0 = r13
            r2 = r14
            if (r1 == 0) goto L_0x0124
            r4 = 0
            int r7 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r7 < 0) goto L_0x010d
            com.applovin.impl.sdk.d.r$c r4 = new com.applovin.impl.sdk.d.r$c
            r4.<init>(r12, r13)
            boolean r5 = r11.a(r4)
            if (r5 != 0) goto L_0x00e9
            com.applovin.impl.sdk.i r5 = r6.b
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r7 = com.applovin.impl.sdk.b.c.ai
            java.lang.Object r5 = r5.a(r7)
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            boolean r5 = r5.booleanValue()
            if (r5 == 0) goto L_0x0033
            java.util.concurrent.ScheduledThreadPoolExecutor r4 = r6.w
            r0 = r11
            r1 = r12
            r2 = r14
        L_0x002c:
            r5 = r16
            r0.a(r1, r2, r4, r5)
            goto L_0x010c
        L_0x0033:
            long r7 = r11.a(r13)
            r9 = 1
            long r7 = r7 + r9
            com.applovin.impl.sdk.o r5 = r6.c
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Scheduling "
            r9.append(r10)
            java.lang.String r1 = r12.f()
            r9.append(r1)
            java.lang.String r1 = " on "
            r9.append(r1)
            r9.append(r13)
            java.lang.String r1 = " queue in "
            r9.append(r1)
            r9.append(r14)
            java.lang.String r1 = "ms with new queue size "
            r9.append(r1)
            r9.append(r7)
            java.lang.String r1 = r9.toString()
            java.lang.String r7 = "TaskManager"
            r5.b(r7, r1)
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.MAIN
            if (r0 != r1) goto L_0x0079
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.d
        L_0x0074:
            r0 = r11
            r1 = r4
            r2 = r14
            r4 = r5
            goto L_0x002c
        L_0x0079:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.TIMEOUT
            if (r0 != r1) goto L_0x0080
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.e
            goto L_0x0074
        L_0x0080:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.BACKGROUND
            if (r0 != r1) goto L_0x0087
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f
            goto L_0x0074
        L_0x0087:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.ADVERTISING_INFO_COLLECTION
            if (r0 != r1) goto L_0x008e
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.g
            goto L_0x0074
        L_0x008e:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.POSTBACKS
            if (r0 != r1) goto L_0x0095
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.h
            goto L_0x0074
        L_0x0095:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.CACHING_INTERSTITIAL
            if (r0 != r1) goto L_0x009c
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.i
            goto L_0x0074
        L_0x009c:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.CACHING_INCENTIVIZED
            if (r0 != r1) goto L_0x00a3
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.j
            goto L_0x0074
        L_0x00a3:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.CACHING_OTHER
            if (r0 != r1) goto L_0x00aa
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.k
            goto L_0x0074
        L_0x00aa:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.REWARD
            if (r0 != r1) goto L_0x00b1
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.l
            goto L_0x0074
        L_0x00b1:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.MEDIATION_MAIN
            if (r0 != r1) goto L_0x00b8
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.m
            goto L_0x0074
        L_0x00b8:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.MEDIATION_TIMEOUT
            if (r0 != r1) goto L_0x00bf
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.n
            goto L_0x0074
        L_0x00bf:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.MEDIATION_BACKGROUND
            if (r0 != r1) goto L_0x00c6
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.o
            goto L_0x0074
        L_0x00c6:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.MEDIATION_POSTBACKS
            if (r0 != r1) goto L_0x00cd
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.p
            goto L_0x0074
        L_0x00cd:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.MEDIATION_BANNER
            if (r0 != r1) goto L_0x00d4
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.q
            goto L_0x0074
        L_0x00d4:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.MEDIATION_INTERSTITIAL
            if (r0 != r1) goto L_0x00db
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.r
            goto L_0x0074
        L_0x00db:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.MEDIATION_INCENTIVIZED
            if (r0 != r1) goto L_0x00e2
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.s
            goto L_0x0074
        L_0x00e2:
            com.applovin.impl.sdk.d.r$a r1 = com.applovin.impl.sdk.d.r.a.MEDIATION_REWARD
            if (r0 != r1) goto L_0x010c
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.t
            goto L_0x0074
        L_0x00e9:
            com.applovin.impl.sdk.o r0 = r6.c
            java.lang.String r2 = r12.f()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Task "
            r3.append(r4)
            java.lang.String r1 = r12.f()
            r3.append(r1)
            java.lang.String r1 = " execution delayed until after init"
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r0.c(r2, r1)
        L_0x010c:
            return
        L_0x010d:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "Invalid delay specified: "
            r1.append(r4)
            r1.append(r14)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0124:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No task specified"
            r0.<init>(r1)
            goto L_0x012d
        L_0x012c:
            throw r0
        L_0x012d:
            goto L_0x012c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.d.r.a(com.applovin.impl.sdk.d.a, com.applovin.impl.sdk.d.r$a, long, boolean):void");
    }

    public boolean a() {
        return this.z;
    }

    public ScheduledExecutorService b() {
        return this.u;
    }

    public ScheduledExecutorService c() {
        return this.v;
    }

    public void d() {
        synchronized (this.y) {
            this.z = false;
        }
    }

    public void e() {
        synchronized (this.y) {
            this.z = true;
            for (c next : this.x) {
                a(next.c, next.d);
            }
            this.x.clear();
        }
    }
}
