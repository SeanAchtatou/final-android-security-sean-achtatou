package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class j extends a {
    /* access modifiers changed from: private */
    public final g a;
    /* access modifiers changed from: private */
    public final AppLovinPostbackListener c;
    private final r.a d;

    public j(g gVar, r.a aVar, i iVar, AppLovinPostbackListener appLovinPostbackListener) {
        super("TaskDispatchPostback", iVar);
        if (gVar != null) {
            this.a = gVar;
            this.c = appLovinPostbackListener;
            this.d = aVar;
            return;
        }
        throw new IllegalArgumentException("No request specified");
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.c;
    }

    public void run() {
        final String a2 = this.a.a();
        if (!m.b(a2)) {
            b("Requested URL is not valid; nothing to do...");
            AppLovinPostbackListener appLovinPostbackListener = this.c;
            if (appLovinPostbackListener != null) {
                appLovinPostbackListener.onPostbackFailure(a2, AppLovinErrorCodes.INVALID_URL);
                return;
            }
            return;
        }
        AnonymousClass1 r1 = new x<Object>(this.a, e()) {
            public void a(int i) {
                d("Failed to dispatch postback. Error code: " + i + " URL: " + a2);
                if (j.this.c != null) {
                    j.this.c.onPostbackFailure(a2, i);
                }
            }

            public void a(Object obj, int i) {
                a("Successfully dispatched postback to URL: " + a2);
                if (((Boolean) this.b.a(c.eU)).booleanValue()) {
                    if (obj != null && (obj instanceof JSONObject)) {
                        JSONObject jSONObject = (JSONObject) obj;
                        Iterator<String> it = this.b.b(c.aD).iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            if (j.this.a.a().startsWith(it.next())) {
                                a("Updating settings from: " + j.this.a.a());
                                h.d(jSONObject, this.b);
                                h.c(jSONObject, this.b);
                                break;
                            }
                        }
                    }
                } else if (obj != null && (obj instanceof String)) {
                    for (String startsWith : this.b.b(c.aD)) {
                        if (j.this.a.a().startsWith(startsWith)) {
                            String str = (String) obj;
                            if (!TextUtils.isEmpty(str)) {
                                try {
                                    a("Updating settings from: " + j.this.a.a());
                                    JSONObject jSONObject2 = new JSONObject(str);
                                    h.d(jSONObject2, this.b);
                                    h.c(jSONObject2, this.b);
                                    break;
                                } catch (JSONException unused) {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                }
                if (j.this.c != null) {
                    j.this.c.onPostbackSuccess(a2);
                }
            }
        };
        r1.a(this.d);
        e().K().a(r1);
    }
}
