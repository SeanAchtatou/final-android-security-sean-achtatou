package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.h;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class m extends a {
    private final d a;
    private final AppLovinAdLoadListener c;
    private boolean d;

    public m(d dVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        this(dVar, appLovinAdLoadListener, "TaskFetchNextAd", iVar);
    }

    m(d dVar, AppLovinAdLoadListener appLovinAdLoadListener, String str, i iVar) {
        super(str, iVar);
        this.d = false;
        this.a = dVar;
        this.c = appLovinAdLoadListener;
    }

    private void a(h hVar) {
        long b = hVar.b(g.c);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - b > TimeUnit.MINUTES.toMillis((long) ((Integer) this.b.a(c.dK)).intValue())) {
            hVar.b(g.c, currentTimeMillis);
            hVar.c(g.d);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        boolean z = i != 204;
        o v = e().v();
        String f = f();
        Boolean valueOf = Boolean.valueOf(z);
        v.a(f, valueOf, "Unable to fetch " + this.a + " ad: server returned " + i);
        if (i == -800) {
            this.b.L().a(g.h);
        }
        try {
            a(i);
        } catch (Throwable th) {
            o.c(f(), "Unable process a failure to recieve an ad", th);
        }
    }

    /* access modifiers changed from: private */
    public void b(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.h.d(jSONObject, this.b);
        com.applovin.impl.sdk.utils.h.c(jSONObject, this.b);
        com.applovin.impl.sdk.utils.h.e(jSONObject, this.b);
        a a2 = a(jSONObject);
        if (((Boolean) this.b.a(c.eN)).booleanValue()) {
            this.b.K().a(a2);
        } else {
            this.b.K().a(a2, r.a.MAIN);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.n;
    }

    /* access modifiers changed from: protected */
    public a a(JSONObject jSONObject) {
        return new s(jSONObject, this.a, c(), this.c, this.b);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        AppLovinAdLoadListener appLovinAdLoadListener = this.c;
        if (appLovinAdLoadListener == null) {
            return;
        }
        if (appLovinAdLoadListener instanceof l) {
            ((l) appLovinAdLoadListener).a(this.a, i);
        } else {
            appLovinAdLoadListener.failedToReceiveAd(i);
        }
    }

    public void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap(4);
        hashMap.put("zone_id", com.applovin.impl.sdk.utils.m.d(this.a.a()));
        if (this.a.b() != null) {
            hashMap.put("size", this.a.b().getLabel());
        }
        if (this.a.c() != null) {
            hashMap.put("require", this.a.c().getLabel());
        }
        if (((Boolean) this.b.a(c.Y)).booleanValue()) {
            hashMap.put("n", String.valueOf(this.b.ab().a(this.a.a())));
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public b c() {
        return this.a.j() ? b.APPLOVIN_PRIMARY_ZONE : b.APPLOVIN_CUSTOM_ZONE;
    }

    /* access modifiers changed from: protected */
    public String d() {
        return com.applovin.impl.sdk.utils.h.g(this.b);
    }

    /* access modifiers changed from: protected */
    public String i() {
        return com.applovin.impl.sdk.utils.h.h(this.b);
    }

    public void run() {
        String str;
        StringBuilder sb;
        if (this.d) {
            sb = new StringBuilder();
            str = "Preloading next ad of zone: ";
        } else {
            sb = new StringBuilder();
            str = "Fetching next ad of zone: ";
        }
        sb.append(str);
        sb.append(this.a);
        a(sb.toString());
        if (((Boolean) this.b.a(c.ef)).booleanValue() && p.d()) {
            a("User is connected to a VPN");
        }
        h L = this.b.L();
        L.a(g.a);
        if (L.b(g.c) == 0) {
            L.b(g.c, System.currentTimeMillis());
        }
        try {
            Map<String, String> a2 = this.b.O().a(b(), this.d, false);
            a(L);
            AnonymousClass1 r2 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).a(d()).a(a2).c(i()).b("GET").a((Object) new JSONObject()).a(((Integer) this.b.a(c.dz)).intValue()).b(((Integer) this.b.a(c.dy)).intValue()).b(true).a(), this.b) {
                public void a(int i) {
                    m.this.b(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    if (i == 200) {
                        com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_latency_millis", this.d.a(), this.b);
                        com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_response_size", this.d.b(), this.b);
                        m.this.b(jSONObject);
                        return;
                    }
                    m.this.b(i);
                }
            };
            r2.a(c.aG);
            r2.b(c.aH);
            this.b.K().a(r2);
        } catch (Throwable th) {
            a("Unable to fetch ad " + this.a, th);
            b(0);
            this.b.M().a(a());
        }
    }
}
