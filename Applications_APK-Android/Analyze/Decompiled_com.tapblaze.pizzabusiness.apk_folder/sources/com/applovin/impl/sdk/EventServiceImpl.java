package com.applovin.impl.sdk;

import android.content.Intent;
import android.text.TextUtils;
import androidx.core.app.NotificationCompat;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.d.i;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinEventParameters;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinEventTypes;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.appevents.AppEventsConstants;
import com.ironsource.sdk.constants.Constants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class EventServiceImpl implements AppLovinEventService {
    /* access modifiers changed from: private */
    public final i a;
    /* access modifiers changed from: private */
    public final Map<String, Object> b;
    private final AtomicBoolean c = new AtomicBoolean();

    public EventServiceImpl(i iVar) {
        this.a = iVar;
        if (((Boolean) iVar.a(c.aU)).booleanValue()) {
            this.b = i.a((String) this.a.b(e.p, "{}"), new HashMap(), this.a);
            return;
        }
        this.b = new HashMap();
        iVar.a(e.p, "{}");
    }

    /* access modifiers changed from: private */
    public String a() {
        return ((String) this.a.a(c.aK)) + "4.0/pix";
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> a(k kVar, j.a aVar) {
        j O = this.a.O();
        j.d b2 = O.b();
        j.b c2 = O.c();
        boolean contains = this.a.b(c.aR).contains(kVar.a());
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(NotificationCompat.CATEGORY_EVENT, contains ? m.d(kVar.a()) : "postinstall");
        hashMap.put("ts", Long.toString(kVar.c()));
        hashMap.put("platform", m.d(b2.c));
        hashMap.put("model", m.d(b2.a));
        hashMap.put("package_name", m.d(c2.c));
        hashMap.put("installer_name", m.d(c2.d));
        hashMap.put("ia", Long.toString(c2.g));
        hashMap.put("api_did", this.a.a(c.S));
        hashMap.put("brand", m.d(b2.d));
        hashMap.put("brand_name", m.d(b2.e));
        hashMap.put("hardware", m.d(b2.f));
        hashMap.put("revision", m.d(b2.g));
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("os", m.d(b2.b));
        hashMap.put("orientation_lock", b2.l);
        hashMap.put("app_version", m.d(c2.b));
        hashMap.put("country_code", m.d(b2.i));
        hashMap.put("carrier", m.d(b2.j));
        hashMap.put("tz_offset", String.valueOf(b2.r));
        hashMap.put("aida", String.valueOf(b2.I));
        String str = "1";
        hashMap.put("adr", b2.t ? str : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        hashMap.put("volume", String.valueOf(b2.v));
        if (!b2.x) {
            str = AppEventsConstants.EVENT_PARAM_VALUE_NO;
        }
        hashMap.put("sim", str);
        hashMap.put("gy", String.valueOf(b2.y));
        hashMap.put("is_tablet", String.valueOf(b2.z));
        hashMap.put("tv", String.valueOf(b2.A));
        hashMap.put("vs", String.valueOf(b2.B));
        hashMap.put("lpm", String.valueOf(b2.C));
        hashMap.put("tg", c2.e);
        hashMap.put("fs", String.valueOf(b2.E));
        hashMap.put("fm", String.valueOf(b2.F.b));
        hashMap.put("tm", String.valueOf(b2.F.a));
        hashMap.put("lmt", String.valueOf(b2.F.c));
        hashMap.put("lm", String.valueOf(b2.F.d));
        hashMap.put("adns", String.valueOf(b2.m));
        hashMap.put("adnsd", String.valueOf(b2.n));
        hashMap.put("xdpi", String.valueOf(b2.o));
        hashMap.put("ydpi", String.valueOf(b2.p));
        hashMap.put("screen_size_in", String.valueOf(b2.q));
        hashMap.put(Constants.RequestParameters.DEBUG, Boolean.toString(p.b(this.a)));
        if (!((Boolean) this.a.a(c.eJ)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.a.t());
        }
        a(aVar, hashMap);
        if (((Boolean) this.a.a(c.dR)).booleanValue()) {
            p.a("cuid", this.a.i(), hashMap);
        }
        if (((Boolean) this.a.a(c.dU)).booleanValue()) {
            hashMap.put("compass_random_token", this.a.j());
        }
        if (((Boolean) this.a.a(c.dW)).booleanValue()) {
            hashMap.put("applovin_random_token", this.a.k());
        }
        Boolean bool = b2.G;
        if (bool != null) {
            hashMap.put("huc", bool.toString());
        }
        Boolean bool2 = b2.H;
        if (bool2 != null) {
            hashMap.put("aru", bool2.toString());
        }
        j.c cVar = b2.u;
        if (cVar != null) {
            hashMap.put("act", String.valueOf(cVar.a));
            hashMap.put("acm", String.valueOf(cVar.b));
        }
        String str2 = b2.w;
        if (m.b(str2)) {
            hashMap.put("ua", m.d(str2));
        }
        String str3 = b2.D;
        if (!TextUtils.isEmpty(str3)) {
            hashMap.put("so", m.d(str3));
        }
        if (!contains) {
            hashMap.put("sub_event", m.d(kVar.a()));
        }
        hashMap.put("sc", m.d((String) this.a.a(c.V)));
        hashMap.put("sc2", m.d((String) this.a.a(c.W)));
        hashMap.put("server_installed_at", m.d((String) this.a.a(c.X)));
        p.a("persisted_data", m.d((String) this.a.a(e.x)), hashMap);
        p.a("plugin_version", m.d((String) this.a.a(c.dY)), hashMap);
        p.a("mediation_provider", m.d(this.a.n()), hashMap);
        return hashMap;
    }

    private void a(i.a aVar) {
        this.a.K().a(new com.applovin.impl.sdk.d.i(this.a, aVar), r.a.ADVERTISING_INFO_COLLECTION);
    }

    private void a(j.a aVar, Map<String, String> map) {
        String str = aVar.b;
        if (m.b(str)) {
            map.put("idfa", str);
        }
        map.put("dnt", Boolean.toString(aVar.a));
    }

    /* access modifiers changed from: private */
    public String b() {
        return ((String) this.a.a(c.aL)) + "4.0/pix";
    }

    private void c() {
        if (((Boolean) this.a.a(c.aU)).booleanValue()) {
            this.a.a(e.p, com.applovin.impl.sdk.utils.i.a(this.b, "{}", this.a));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, boolean z) {
        trackEvent(str, new HashMap(), null, z);
    }

    public Map<String, Object> getSuperProperties() {
        return new HashMap(this.b);
    }

    public void maybeTrackAppOpenEvent() {
        if (this.c.compareAndSet(false, true)) {
            this.a.q().trackEvent("landing");
        }
    }

    public void setSuperProperty(Object obj, String str) {
        if (TextUtils.isEmpty(str)) {
            o.i("AppLovinEventService", "Super property key cannot be null or empty");
        } else if (obj == null) {
            this.b.remove(str);
            c();
        } else {
            List<String> b2 = this.a.b(c.aT);
            if (!p.a(obj, b2, this.a)) {
                o.i("AppLovinEventService", "Failed to set super property '" + obj + "' for key '" + str + "' - valid super property types include: " + b2);
                return;
            }
            this.b.put(str, p.a(obj, this.a));
            c();
        }
    }

    public String toString() {
        return "EventService{}";
    }

    public void trackCheckout(String str, Map<String, String> map) {
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap(1);
        }
        hashMap.put("transaction_id", str);
        trackEvent(AppLovinEventTypes.USER_COMPLETED_CHECKOUT, hashMap);
    }

    public void trackEvent(String str) {
        trackEvent(str, new HashMap());
    }

    public void trackEvent(String str, Map<String, String> map) {
        trackEvent(str, map, null, true);
    }

    public void trackEvent(String str, Map<String, String> map, Map<String, String> map2, boolean z) {
        if (((Boolean) this.a.a(c.aS)).booleanValue()) {
            o v = this.a.v();
            v.b("AppLovinEventService", "Tracking event: \"" + str + "\" with parameters: " + map);
            final String str2 = str;
            final Map<String, String> map3 = map;
            final boolean z2 = z;
            final Map<String, String> map4 = map2;
            a(new i.a() {
                public void a(j.a aVar) {
                    k kVar = new k(str2, map3, EventServiceImpl.this.b);
                    try {
                        if (z2) {
                            EventServiceImpl.this.a.N().a(f.k().a(EventServiceImpl.this.a()).b(EventServiceImpl.this.b()).a(EventServiceImpl.this.a(kVar, aVar)).b(map4).c(kVar.b()).a(((Boolean) EventServiceImpl.this.a.a(c.eJ)).booleanValue()).a());
                            return;
                        }
                        EventServiceImpl.this.a.R().dispatchPostbackRequest(g.b(EventServiceImpl.this.a).a(EventServiceImpl.this.a()).c(EventServiceImpl.this.b()).b((Map<String, String>) EventServiceImpl.this.a(kVar, aVar)).c(map4).a(com.applovin.impl.sdk.utils.i.a((Map<String, ?>) kVar.b())).a(((Boolean) EventServiceImpl.this.a.a(c.eJ)).booleanValue()).a(), null);
                    } catch (Throwable th) {
                        o v = EventServiceImpl.this.a.v();
                        v.b("AppLovinEventService", "Unable to track event: " + kVar, th);
                    }
                }
            });
        }
    }

    public void trackInAppPurchase(Intent intent, Map<String, String> map) {
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap();
        }
        try {
            hashMap.put(AppLovinEventParameters.IN_APP_PURCHASE_DATA, intent.getStringExtra("INAPP_PURCHASE_DATA"));
            hashMap.put(AppLovinEventParameters.IN_APP_DATA_SIGNATURE, intent.getStringExtra("INAPP_DATA_SIGNATURE"));
        } catch (Throwable th) {
            o.c("AppLovinEventService", "Unable to track in app purchase - invalid purchase intent", th);
        }
        trackEvent("iap", hashMap);
    }
}
