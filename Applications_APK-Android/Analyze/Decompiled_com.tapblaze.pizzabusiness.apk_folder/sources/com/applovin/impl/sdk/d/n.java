package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import java.util.Map;
import org.json.JSONObject;

public class n extends m {
    private final int a;
    private final AppLovinNativeAdLoadListener c;

    public n(String str, int i, i iVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super(d.b(str, iVar), null, "TaskFetchNextNativeAd", iVar);
        this.a = i;
        this.c = appLovinNativeAdLoadListener;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.o;
    }

    /* access modifiers changed from: protected */
    public a a(JSONObject jSONObject) {
        return new v(jSONObject, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.c;
        if (appLovinNativeAdLoadListener != null) {
            appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i);
        }
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> b() {
        Map<String, String> b = super.b();
        b.put("slot_count", Integer.toString(this.a));
        return b;
    }

    /* access modifiers changed from: protected */
    public String d() {
        return ((String) this.b.a(c.aG)) + "4.0/nad";
    }

    /* access modifiers changed from: protected */
    public String i() {
        return ((String) this.b.a(c.aH)) + "4.0/nad";
    }
}
