package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.c;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.HashMap;
import java.util.Map;

public class o extends m {
    private final c a;

    public o(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super(d.a("adtoken_zone", iVar), appLovinAdLoadListener, "TaskFetchTokenAd", iVar);
        this.a = cVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.p;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap(2);
        hashMap.put("adtoken", m.d(this.a.a()));
        hashMap.put("adtoken_prefix", m.d(this.a.c()));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public b c() {
        return b.REGULAR_AD_TOKEN;
    }
}
