package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.m;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.p;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import java.util.List;

abstract class f extends a {
    protected final AppLovinNativeAdPrecacheListener a;
    private final List<NativeAdImpl> c;
    private final AppLovinNativeAdLoadListener d;
    private int e;

    f(String str, List<NativeAdImpl> list, i iVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        super(str, iVar);
        this.c = list;
        this.d = appLovinNativeAdLoadListener;
        this.a = null;
    }

    f(String str, List<NativeAdImpl> list, i iVar, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        super(str, iVar);
        if (list != null) {
            this.c = list;
            this.d = null;
            this.a = appLovinNativeAdPrecacheListener;
            return;
        }
        throw new IllegalArgumentException("Native ads cannot be null");
    }

    private void a(int i) {
        AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.d;
        if (appLovinNativeAdLoadListener != null) {
            appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i);
        }
    }

    private void a(List<AppLovinNativeAd> list) {
        AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.d;
        if (appLovinNativeAdLoadListener != null) {
            appLovinNativeAdLoadListener.onNativeAdsLoaded(list);
        }
    }

    /* access modifiers changed from: protected */
    public String a(String str, m mVar, List<String> list) {
        if (!com.applovin.impl.sdk.utils.m.b(str)) {
            a("Asked to cache file with null/empty URL, nothing to do.");
            return null;
        } else if (!p.a(str, list)) {
            a("Domain is not whitelisted, skipping precache for URL " + str);
            return null;
        } else {
            try {
                String a2 = mVar.a(g(), str, null, list, true, true, null);
                if (a2 != null) {
                    return a2;
                }
                c("Unable to cache icon resource " + str);
                return null;
            } catch (Exception e2) {
                a("Unable to cache icon resource " + str, e2);
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(NativeAdImpl nativeAdImpl);

    /* access modifiers changed from: protected */
    public abstract void a(NativeAdImpl nativeAdImpl, int i);

    /* access modifiers changed from: protected */
    public abstract boolean a(NativeAdImpl nativeAdImpl, m mVar);

    public void run() {
        List list;
        for (NativeAdImpl next : this.c) {
            a("Beginning resource caching phase...");
            if (a(next, this.b.V())) {
                this.e++;
                a(next);
            } else {
                d("Unable to cache resources");
            }
        }
        try {
            if (this.e == this.c.size()) {
                list = this.c;
            } else if (((Boolean) this.b.a(c.f0do)).booleanValue()) {
                d("Mismatch between successful populations and requested size");
                a(-6);
                return;
            } else {
                list = this.c;
            }
            a(list);
        } catch (Throwable th) {
            o.c(f(), "Encountered exception while notifying publisher code", th);
        }
    }
}
