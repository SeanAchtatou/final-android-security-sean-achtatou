package com.applovin.impl.sdk.b;

import android.content.Context;
import android.content.SharedPreferences;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkSettings;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class d {
    protected final i a;
    protected final o b;
    protected final Context c;
    protected final SharedPreferences d;
    private final Map<String, Object> e = new HashMap();
    private Map<String, Object> f;

    /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0035 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public d(com.applovin.impl.sdk.i r4) {
        /*
            r3 = this;
            r3.<init>()
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.e = r0
            r3.a = r4
            com.applovin.impl.sdk.o r0 = r4.v()
            r3.b = r0
            android.content.Context r0 = r4.D()
            r3.c = r0
            android.content.Context r0 = r3.c
            java.lang.String r1 = "com.applovin.sdk.1"
            r2 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)
            r3.d = r0
            java.lang.Class<com.applovin.impl.sdk.b.c> r0 = com.applovin.impl.sdk.b.c.class
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x0035 }
            java.lang.Class.forName(r0)     // Catch:{ all -> 0x0035 }
            java.lang.Class<com.applovin.impl.sdk.b.b> r0 = com.applovin.impl.sdk.b.b.class
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x0035 }
            java.lang.Class.forName(r0)     // Catch:{ all -> 0x0035 }
        L_0x0035:
            com.applovin.sdk.AppLovinSdkSettings r0 = r4.l()     // Catch:{ all -> 0x0053 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = "localSettings"
            java.lang.reflect.Field r0 = com.applovin.impl.sdk.utils.p.a(r0, r1)     // Catch:{ all -> 0x0053 }
            r1 = 1
            r0.setAccessible(r1)     // Catch:{ all -> 0x0053 }
            com.applovin.sdk.AppLovinSdkSettings r4 = r4.l()     // Catch:{ all -> 0x0053 }
            java.lang.Object r4 = r0.get(r4)     // Catch:{ all -> 0x0053 }
            java.util.HashMap r4 = (java.util.HashMap) r4     // Catch:{ all -> 0x0053 }
            r3.f = r4     // Catch:{ all -> 0x0053 }
        L_0x0053:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.b.d.<init>(com.applovin.impl.sdk.i):void");
    }

    private static Object a(String str, JSONObject jSONObject, Object obj) throws JSONException {
        if (obj instanceof Boolean) {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        }
        if (obj instanceof Float) {
            return Float.valueOf((float) jSONObject.getDouble(str));
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(jSONObject.getInt(str));
        }
        if (obj instanceof Long) {
            return Long.valueOf(jSONObject.getLong(str));
        }
        if (obj instanceof String) {
            return jSONObject.getString(str);
        }
        throw new RuntimeException("SDK Error: unknown value type: " + obj.getClass());
    }

    private <T> T c(c<T> cVar) {
        try {
            return cVar.a(this.f.get(cVar.a()));
        } catch (Throwable unused) {
            return null;
        }
    }

    private String e() {
        return "com.applovin.sdk." + p.a(this.a.t()) + ".";
    }

    public <ST> c<ST> a(String str, c cVar) {
        for (c<ST> next : c.c()) {
            if (next.a().equals(str)) {
                return next;
            }
        }
        return cVar;
    }

    public <T> T a(c cVar) {
        if (cVar != null) {
            synchronized (this.e) {
                try {
                    T c2 = c(cVar);
                    if (c2 != null) {
                        return c2;
                    }
                    Object obj = this.e.get(cVar.a());
                    if (obj != null) {
                        T a2 = cVar.a(obj);
                        return a2;
                    }
                    T b2 = cVar.b();
                    return b2;
                } catch (Throwable unused) {
                    o v = this.a.v();
                    v.e("SettingsManager", "Unable to retrieve value for setting " + cVar.a() + "; using default...");
                    return cVar.b();
                }
            }
        } else {
            throw new IllegalArgumentException("No setting type specified");
        }
    }

    public void a() {
        if (this.c != null) {
            String e2 = e();
            synchronized (this.e) {
                SharedPreferences.Editor edit = this.d.edit();
                for (c next : c.c()) {
                    Object obj = this.e.get(next.a());
                    if (obj != null) {
                        this.a.a(e2 + next.a(), obj, edit);
                    }
                }
                edit.apply();
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public <T> void a(c<?> cVar, Object obj) {
        if (cVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        } else if (obj != null) {
            synchronized (this.e) {
                this.e.put(cVar.a(), obj);
            }
        } else {
            throw new IllegalArgumentException("No new value specified");
        }
    }

    public void a(AppLovinSdkSettings appLovinSdkSettings) {
        boolean z;
        boolean z2;
        if (appLovinSdkSettings != null) {
            synchronized (this.e) {
                if (((Boolean) this.a.a(c.U)).booleanValue()) {
                    this.e.put(c.U.a(), Boolean.valueOf(appLovinSdkSettings.isVerboseLoggingEnabled()));
                }
                if (((Boolean) this.a.a(c.bz)).booleanValue()) {
                    String autoPreloadSizes = appLovinSdkSettings.getAutoPreloadSizes();
                    if (!m.b(autoPreloadSizes)) {
                        autoPreloadSizes = "NONE";
                    }
                    if (autoPreloadSizes.equals("NONE")) {
                        this.e.put(c.aZ.a(), "");
                    } else {
                        this.e.put(c.aZ.a(), autoPreloadSizes);
                    }
                }
                if (((Boolean) this.a.a(c.bA)).booleanValue()) {
                    String autoPreloadTypes = appLovinSdkSettings.getAutoPreloadTypes();
                    if (!m.b(autoPreloadTypes)) {
                        autoPreloadTypes = "NONE";
                    }
                    boolean z3 = false;
                    if (!"NONE".equals(autoPreloadTypes)) {
                        z2 = false;
                        z = false;
                        for (String next : e.a(autoPreloadTypes)) {
                            if (next.equals(AppLovinAdType.REGULAR.getLabel())) {
                                z3 = true;
                            } else {
                                if (!next.equals(AppLovinAdType.INCENTIVIZED.getLabel()) && !next.contains("INCENT")) {
                                    if (!next.contains("REWARD")) {
                                        if (next.equals(AppLovinAdType.NATIVE.getLabel())) {
                                            z = true;
                                        }
                                    }
                                }
                                z2 = true;
                            }
                        }
                    } else {
                        z2 = false;
                        z = false;
                    }
                    if (!z3) {
                        this.e.put(c.aZ.a(), "");
                    }
                    this.e.put(c.ba.a(), Boolean.valueOf(z2));
                    this.e.put(c.bb.a(), Boolean.valueOf(z));
                }
            }
        }
    }

    public void a(JSONObject jSONObject) {
        o oVar;
        String str;
        String str2;
        synchronized (this.e) {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next != null && next.length() > 0) {
                    try {
                        c<Long> a2 = a(next, (c) null);
                        if (a2 != null) {
                            this.e.put(a2.a(), a(next, jSONObject, a2.b()));
                            if (a2 == c.eL) {
                                this.e.put(c.eM.a(), Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                    } catch (JSONException e2) {
                        th = e2;
                        oVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to parse JSON settingsValues array";
                        oVar.b(str, str2, th);
                    } catch (Throwable th) {
                        th = th;
                        oVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to convert setting object ";
                        oVar.b(str, str2, th);
                    }
                }
            }
        }
    }

    public List<String> b(c<String> cVar) {
        return e.a((String) a(cVar));
    }

    public void b() {
        if (this.c != null) {
            String e2 = e();
            synchronized (this.e) {
                for (c next : c.c()) {
                    try {
                        Object a2 = this.a.a(e2 + next.a(), null, next.b().getClass(), this.d);
                        if (a2 != null) {
                            this.e.put(next.a(), a2);
                        }
                    } catch (Exception e3) {
                        o oVar = this.b;
                        oVar.b("SettingsManager", "Unable to load \"" + next.a() + "\"", e3);
                    }
                }
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public void c() {
        synchronized (this.e) {
            this.e.clear();
        }
        this.a.a(this.d);
    }

    public boolean d() {
        return this.a.l().isVerboseLoggingEnabled() || ((Boolean) a(c.U)).booleanValue();
    }
}
