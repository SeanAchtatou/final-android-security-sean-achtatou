package com.applovin.impl.sdk.a;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.applovin.impl.adview.m;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;

public class b {
    /* access modifiers changed from: private */
    public final i a;
    /* access modifiers changed from: private */
    public final m b;
    /* access modifiers changed from: private */
    public AlertDialog c;

    public b(m mVar, i iVar) {
        this.a = iVar;
        this.b = mVar;
    }

    public void a() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                if (b.this.c != null) {
                    b.this.c.dismiss();
                }
            }
        });
    }

    public void b() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(b.this.b);
                builder.setTitle((CharSequence) b.this.a.a(c.bN));
                builder.setMessage((CharSequence) b.this.a.a(c.bO));
                builder.setCancelable(false);
                builder.setPositiveButton((CharSequence) b.this.a.a(c.bQ), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.b.continueVideo();
                        b.this.b.resumeReportRewardTask();
                    }
                });
                builder.setNegativeButton((CharSequence) b.this.a.a(c.bP), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.b.skipVideo();
                        b.this.b.resumeReportRewardTask();
                    }
                });
                AlertDialog unused = b.this.c = builder.show();
            }
        });
    }

    public void c() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(b.this.b);
                builder.setTitle((CharSequence) b.this.a.a(c.bS));
                builder.setMessage((CharSequence) b.this.a.a(c.bT));
                builder.setCancelable(false);
                builder.setPositiveButton((CharSequence) b.this.a.a(c.bV), (DialogInterface.OnClickListener) null);
                builder.setNegativeButton((CharSequence) b.this.a.a(c.bU), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.b.dismiss();
                    }
                });
                AlertDialog unused = b.this.c = builder.show();
            }
        });
    }

    public boolean d() {
        AlertDialog alertDialog = this.c;
        if (alertDialog != null) {
            return alertDialog.isShowing();
        }
        return false;
    }
}
