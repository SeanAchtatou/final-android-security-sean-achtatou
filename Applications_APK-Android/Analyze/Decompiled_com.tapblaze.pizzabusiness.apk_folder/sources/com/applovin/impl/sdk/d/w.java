package com.applovin.impl.sdk.d;

import com.applovin.impl.a.a;
import com.applovin.impl.a.b;
import com.applovin.impl.a.c;
import com.applovin.impl.a.d;
import com.applovin.impl.a.f;
import com.applovin.impl.a.j;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdType;
import java.util.HashSet;

class w extends a {
    private c a;
    private final AppLovinAdLoadListener c;

    w(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super("TaskRenderVastAd", iVar);
        this.c = appLovinAdLoadListener;
        this.a = cVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.w;
    }

    public void run() {
        a("Rendering VAST ad...");
        int size = this.a.b().size();
        HashSet hashSet = new HashSet(size);
        HashSet hashSet2 = new HashSet(size);
        String str = "";
        f fVar = null;
        j jVar = null;
        b bVar = null;
        String str2 = str;
        for (r next : this.a.b()) {
            r c2 = next.c(com.applovin.impl.a.i.a(next) ? "Wrapper" : "InLine");
            if (c2 != null) {
                r c3 = c2.c("AdSystem");
                if (c3 != null) {
                    fVar = f.a(c3, fVar, this.b);
                }
                str = com.applovin.impl.a.i.a(c2, "AdTitle", str);
                str2 = com.applovin.impl.a.i.a(c2, "Description", str2);
                com.applovin.impl.a.i.a(c2.a("Impression"), hashSet, this.a, this.b);
                com.applovin.impl.a.i.a(c2.a("Error"), hashSet2, this.a, this.b);
                r b = c2.b("Creatives");
                if (b != null) {
                    for (r next2 : b.d()) {
                        r b2 = next2.b("Linear");
                        if (b2 != null) {
                            jVar = j.a(b2, jVar, this.a, this.b);
                        } else {
                            r c4 = next2.c("CompanionAds");
                            if (c4 != null) {
                                r c5 = c4.c("Companion");
                                if (c5 != null) {
                                    bVar = b.a(c5, bVar, this.a, this.b);
                                }
                            } else {
                                d("Received and will skip rendering for an unidentified creative: " + next2);
                            }
                        }
                    }
                }
            } else {
                d("Did not find wrapper or inline response for node: " + next);
            }
        }
        a a2 = a.aH().a(this.b).a(this.a.c()).b(this.a.d()).a(this.a.e()).a(this.a.f()).a(str).b(str2).a(fVar).a(jVar).a(bVar).a(hashSet).b(hashSet2).a();
        d a3 = com.applovin.impl.a.i.a(a2);
        if (a3 == null) {
            h hVar = new h(a2, this.b, this.c);
            r.a aVar = r.a.CACHING_OTHER;
            if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.bi)).booleanValue()) {
                if (a2.getType() == AppLovinAdType.REGULAR) {
                    aVar = r.a.CACHING_INTERSTITIAL;
                } else if (a2.getType() == AppLovinAdType.INCENTIVIZED) {
                    aVar = r.a.CACHING_INCENTIVIZED;
                }
            }
            this.b.K().a(hVar, aVar);
            return;
        }
        com.applovin.impl.a.i.a(this.a, this.c, a3, -6, this.b);
    }
}
