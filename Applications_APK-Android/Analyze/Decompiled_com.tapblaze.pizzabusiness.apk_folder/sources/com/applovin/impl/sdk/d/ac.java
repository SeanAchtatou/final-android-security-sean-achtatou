package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.i;

public class ac extends a {
    private final Runnable a;

    public ac(i iVar, Runnable runnable) {
        this(iVar, false, runnable);
    }

    public ac(i iVar, boolean z, Runnable runnable) {
        super("TaskRunnable", iVar, z);
        this.a = runnable;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.f;
    }

    public void run() {
        this.a.run();
    }
}
