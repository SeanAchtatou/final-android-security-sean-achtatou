package com.applovin.impl.sdk.c;

public class a {
    private final String a;
    private final String b;

    public a(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String toString() {
        return "AdEventPostback{url='" + this.a + '\'' + ", backupUrl='" + this.b + '\'' + '}';
    }
}
