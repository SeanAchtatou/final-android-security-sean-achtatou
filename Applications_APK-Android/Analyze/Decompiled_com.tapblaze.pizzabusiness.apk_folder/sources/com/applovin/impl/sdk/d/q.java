package com.applovin.impl.sdk.d;

import android.app.Activity;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.e;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.concurrent.TimeUnit;

public class q extends a {
    /* access modifiers changed from: private */
    public final i a;

    public q(i iVar) {
        super("TaskInitializeSdk", iVar);
        this.a = iVar;
    }

    private void a(c<Boolean> cVar) {
        if (((Boolean) this.a.a(cVar)).booleanValue()) {
            this.a.T().f(d.a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, this.a));
        }
    }

    private void b() {
        if (!this.a.x().a()) {
            Activity ae = this.a.ae();
            if (ae != null) {
                this.a.x().a(ae);
            } else {
                this.a.K().a(new ac(this.a, true, new Runnable() {
                    public void run() {
                        q.this.a.x().a(q.this.a.aa().a());
                    }
                }), r.a.MAIN, TimeUnit.SECONDS.toMillis(1));
            }
        }
    }

    private void c() {
        this.a.K().a(new b(this.a), r.a.MAIN);
    }

    private void d() {
        this.a.T().a();
        this.a.U().a();
    }

    private void i() {
        j();
        k();
        l();
    }

    private void j() {
        LinkedHashSet<d> a2 = this.a.W().a();
        if (!a2.isEmpty()) {
            a("Scheduling preload(s) for " + a2.size() + " zone(s)");
            Iterator<d> it = a2.iterator();
            while (it.hasNext()) {
                d next = it.next();
                if (next.d()) {
                    this.a.p().preloadAds(next);
                } else {
                    this.a.o().preloadAds(next);
                }
            }
        }
    }

    private void k() {
        c<Boolean> cVar = c.ba;
        String str = (String) this.a.a(c.aZ);
        boolean z = false;
        if (str.length() > 0) {
            for (String fromString : e.a(str)) {
                AppLovinAdSize fromString2 = AppLovinAdSize.fromString(fromString);
                if (fromString2 != null) {
                    this.a.T().f(d.a(fromString2, AppLovinAdType.REGULAR, this.a));
                    if (AppLovinAdSize.INTERSTITIAL.getLabel().equals(fromString2.getLabel())) {
                        a(cVar);
                        z = true;
                    }
                }
            }
        }
        if (!z) {
            a(cVar);
        }
    }

    private void l() {
        if (((Boolean) this.a.a(c.bb)).booleanValue()) {
            this.a.U().f(d.h(this.a));
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x013d, code lost:
        if (r12.a.d() == false) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0140, code lost:
        r2 = com.ironsource.sdk.constants.Constants.ParametersKeys.FAILED;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0141, code lost:
        r8.append(r2);
        r8.append(" in ");
        r8.append(java.lang.System.currentTimeMillis() - r6);
        r8.append("ms");
        a(r8.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0159, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00e8, code lost:
        if (r12.a.d() != false) goto L_0x0141;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            java.lang.String r0 = "ms"
            java.lang.String r1 = " in "
            java.lang.String r2 = "succeeded"
            java.lang.String r3 = "failed"
            java.lang.String r4 = " initialization "
            java.lang.String r5 = "AppLovin SDK "
            long r6 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Initializing AppLovin SDK "
            r8.append(r9)
            java.lang.String r9 = com.applovin.sdk.AppLovinSdk.VERSION
            r8.append(r9)
            java.lang.String r9 = "..."
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r12.a(r8)
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.c.h r8 = r8.L()     // Catch:{ all -> 0x00eb }
            r8.d()     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.c.h r8 = r8.L()     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.c.g r9 = com.applovin.impl.sdk.c.g.b     // Catch:{ all -> 0x00eb }
            r8.c(r9)     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.m r8 = r8.V()     // Catch:{ all -> 0x00eb }
            android.content.Context r9 = r12.g()     // Catch:{ all -> 0x00eb }
            r8.a(r9)     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.m r8 = r8.V()     // Catch:{ all -> 0x00eb }
            android.content.Context r9 = r12.g()     // Catch:{ all -> 0x00eb }
            r8.b(r9)     // Catch:{ all -> 0x00eb }
            r12.d()     // Catch:{ all -> 0x00eb }
            r12.i()     // Catch:{ all -> 0x00eb }
            r12.b()     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.c.c r8 = r8.X()     // Catch:{ all -> 0x00eb }
            r8.a()     // Catch:{ all -> 0x00eb }
            r12.c()     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.j r8 = r8.O()     // Catch:{ all -> 0x00eb }
            r8.e()     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            r9 = 1
            r8.a(r9)     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.network.e r8 = r8.N()     // Catch:{ all -> 0x00eb }
            r8.a()     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.sdk.AppLovinEventService r8 = r8.q()     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.EventServiceImpl r8 = (com.applovin.impl.sdk.EventServiceImpl) r8     // Catch:{ all -> 0x00eb }
            r8.maybeTrackAppOpenEvent()     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.mediation.j r8 = r8.B()     // Catch:{ all -> 0x00eb }
            r8.a()     // Catch:{ all -> 0x00eb }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.mediation.a.a r8 = r8.z()     // Catch:{ all -> 0x00eb }
            boolean r8 = r8.a()     // Catch:{ all -> 0x00eb }
            if (r8 == 0) goto L_0x00af
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x00eb }
            com.applovin.impl.mediation.a.a r8 = r8.z()     // Catch:{ all -> 0x00eb }
            r8.b()     // Catch:{ all -> 0x00eb }
        L_0x00af:
            com.applovin.impl.sdk.i r8 = r12.a
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r9 = com.applovin.impl.sdk.b.c.aq
            java.lang.Object r8 = r8.a(r9)
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            boolean r8 = r8.booleanValue()
            if (r8 == 0) goto L_0x00d2
            com.applovin.impl.sdk.i r8 = r12.a
            com.applovin.impl.sdk.b.c<java.lang.Long> r9 = com.applovin.impl.sdk.b.c.ar
            java.lang.Object r8 = r8.a(r9)
            java.lang.Long r8 = (java.lang.Long) r8
            long r8 = r8.longValue()
            com.applovin.impl.sdk.i r10 = r12.a
            r10.a(r8)
        L_0x00d2:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r5)
            java.lang.String r5 = com.applovin.sdk.AppLovinSdk.VERSION
            r8.append(r5)
            r8.append(r4)
            com.applovin.impl.sdk.i r4 = r12.a
            boolean r4 = r4.d()
            if (r4 == 0) goto L_0x0140
            goto L_0x0141
        L_0x00eb:
            r8 = move-exception
            java.lang.String r9 = "Unable to initialize SDK."
            r12.a(r9, r8)     // Catch:{ all -> 0x015a }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x015a }
            r9 = 0
            r8.a(r9)     // Catch:{ all -> 0x015a }
            com.applovin.impl.sdk.i r8 = r12.a     // Catch:{ all -> 0x015a }
            com.applovin.impl.sdk.c.j r8 = r8.M()     // Catch:{ all -> 0x015a }
            com.applovin.impl.sdk.c.i r9 = r12.a()     // Catch:{ all -> 0x015a }
            r8.a(r9)     // Catch:{ all -> 0x015a }
            com.applovin.impl.sdk.i r8 = r12.a
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r9 = com.applovin.impl.sdk.b.c.aq
            java.lang.Object r8 = r8.a(r9)
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            boolean r8 = r8.booleanValue()
            if (r8 == 0) goto L_0x0127
            com.applovin.impl.sdk.i r8 = r12.a
            com.applovin.impl.sdk.b.c<java.lang.Long> r9 = com.applovin.impl.sdk.b.c.ar
            java.lang.Object r8 = r8.a(r9)
            java.lang.Long r8 = (java.lang.Long) r8
            long r8 = r8.longValue()
            com.applovin.impl.sdk.i r10 = r12.a
            r10.a(r8)
        L_0x0127:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r5)
            java.lang.String r5 = com.applovin.sdk.AppLovinSdk.VERSION
            r8.append(r5)
            r8.append(r4)
            com.applovin.impl.sdk.i r4 = r12.a
            boolean r4 = r4.d()
            if (r4 == 0) goto L_0x0140
            goto L_0x0141
        L_0x0140:
            r2 = r3
        L_0x0141:
            r8.append(r2)
            r8.append(r1)
            long r1 = java.lang.System.currentTimeMillis()
            long r1 = r1 - r6
            r8.append(r1)
            r8.append(r0)
            java.lang.String r0 = r8.toString()
            r12.a(r0)
            return
        L_0x015a:
            r8 = move-exception
            com.applovin.impl.sdk.i r9 = r12.a
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r10 = com.applovin.impl.sdk.b.c.aq
            java.lang.Object r9 = r9.a(r10)
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x017e
            com.applovin.impl.sdk.i r9 = r12.a
            com.applovin.impl.sdk.b.c<java.lang.Long> r10 = com.applovin.impl.sdk.b.c.ar
            java.lang.Object r9 = r9.a(r10)
            java.lang.Long r9 = (java.lang.Long) r9
            long r9 = r9.longValue()
            com.applovin.impl.sdk.i r11 = r12.a
            r11.a(r9)
        L_0x017e:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r5)
            java.lang.String r5 = com.applovin.sdk.AppLovinSdk.VERSION
            r9.append(r5)
            r9.append(r4)
            com.applovin.impl.sdk.i r4 = r12.a
            boolean r4 = r4.d()
            if (r4 == 0) goto L_0x0197
            goto L_0x0198
        L_0x0197:
            r2 = r3
        L_0x0198:
            r9.append(r2)
            r9.append(r1)
            long r1 = java.lang.System.currentTimeMillis()
            long r1 = r1 - r6
            r9.append(r1)
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            r12.a(r0)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.d.q.run():void");
    }
}
