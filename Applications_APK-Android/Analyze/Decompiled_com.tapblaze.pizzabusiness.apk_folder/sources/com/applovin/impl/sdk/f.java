package com.applovin.impl.sdk;

import android.content.Context;
import com.applovin.impl.sdk.b.e;

public class f {
    public static Boolean a(Context context) {
        return a(e.i, context);
    }

    private static Boolean a(e<Boolean> eVar, Context context) {
        return (Boolean) com.applovin.impl.sdk.b.f.b(eVar, (Object) null, context);
    }

    private static boolean a(e<Boolean> eVar, Boolean bool, Context context) {
        Boolean a = a(eVar, context);
        com.applovin.impl.sdk.b.f.a(eVar, bool, context);
        return a == null || a != bool;
    }

    public static boolean a(boolean z, Context context) {
        return a(e.i, Boolean.valueOf(z), context);
    }

    public static Boolean b(Context context) {
        return a(e.j, context);
    }

    public static boolean b(boolean z, Context context) {
        return a(e.j, Boolean.valueOf(z), context);
    }
}
