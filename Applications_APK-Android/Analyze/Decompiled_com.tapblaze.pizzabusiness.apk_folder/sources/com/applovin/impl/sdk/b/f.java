package com.applovin.impl.sdk.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import java.util.Set;

public final class f {
    private static SharedPreferences a;
    private final SharedPreferences b;

    public f(i iVar) {
        this.b = iVar.D().getSharedPreferences("com.applovin.sdk.preferences." + iVar.t(), 0);
    }

    private static SharedPreferences a(Context context) {
        if (a == null) {
            a = context.getSharedPreferences("com.applovin.sdk.shared", 0);
        }
        return a;
    }

    public static <T> T a(String str, Object obj, Class cls, SharedPreferences sharedPreferences) {
        Object obj2;
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            if (sharedPreferences.contains(str)) {
                if (Boolean.class.equals(cls)) {
                    obj2 = Boolean.valueOf(obj != null ? sharedPreferences.getBoolean(str, ((Boolean) obj).booleanValue()) : sharedPreferences.getBoolean(str, false));
                } else if (Float.class.equals(cls)) {
                    obj2 = Float.valueOf(obj != null ? sharedPreferences.getFloat(str, ((Float) obj).floatValue()) : sharedPreferences.getFloat(str, 0.0f));
                } else if (Integer.class.equals(cls)) {
                    obj2 = Integer.valueOf(obj != null ? sharedPreferences.getInt(str, ((Integer) obj).intValue()) : sharedPreferences.getInt(str, 0));
                } else if (Long.class.equals(cls)) {
                    obj2 = Long.valueOf(obj != null ? sharedPreferences.getLong(str, ((Long) obj).longValue()) : sharedPreferences.getLong(str, 0));
                } else {
                    obj2 = String.class.equals(cls) ? sharedPreferences.getString(str, (String) obj) : Set.class.isAssignableFrom(cls) ? sharedPreferences.getStringSet(str, (Set) obj) : obj;
                }
                if (obj2 != null) {
                    return cls.cast(obj2);
                }
                StrictMode.setThreadPolicy(allowThreadDiskReads);
                return obj;
            }
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return obj;
        } catch (Throwable th) {
            o.c("SharedPreferencesManager", "Error getting value for key: " + str, th);
            return obj;
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    public static <T> void a(e eVar, Context context) {
        a(context).edit().remove(eVar.a()).apply();
    }

    public static <T> void a(e eVar, Object obj, Context context) {
        a(eVar.a(), obj, a(context), (SharedPreferences.Editor) null);
    }

    private static <T> void a(String str, Object obj, SharedPreferences sharedPreferences, SharedPreferences.Editor editor) {
        boolean z = true;
        boolean z2 = editor != null;
        if (!z2) {
            editor = sharedPreferences.edit();
        }
        if (obj instanceof Boolean) {
            editor.putBoolean(str, ((Boolean) obj).booleanValue());
        } else if (obj instanceof Float) {
            editor.putFloat(str, ((Float) obj).floatValue());
        } else if (obj instanceof Integer) {
            editor.putInt(str, ((Integer) obj).intValue());
        } else if (obj instanceof Long) {
            editor.putLong(str, ((Long) obj).longValue());
        } else if (obj instanceof String) {
            editor.putString(str, (String) obj);
        } else if (obj instanceof Set) {
            editor.putStringSet(str, (Set) obj);
        } else {
            o.i("SharedPreferencesManager", "Unable to put default value of invalid type: " + obj);
            z = false;
        }
        if (z && !z2) {
            editor.apply();
        }
    }

    public static <T> T b(e eVar, Object obj, Context context) {
        return a(eVar.a(), obj, eVar.b(), a(context));
    }

    public void a(SharedPreferences sharedPreferences) {
        sharedPreferences.edit().clear().apply();
    }

    public <T> void a(e eVar) {
        this.b.edit().remove(eVar.a()).apply();
    }

    public <T> void a(e eVar, Object obj) {
        a(eVar, obj, this.b);
    }

    public <T> void a(e eVar, Object obj, SharedPreferences sharedPreferences) {
        a(eVar.a(), obj, sharedPreferences);
    }

    public <T> void a(String str, Object obj, SharedPreferences.Editor editor) {
        a(str, obj, (SharedPreferences) null, editor);
    }

    public <T> void a(String str, Object obj, SharedPreferences sharedPreferences) {
        a(str, obj, sharedPreferences, (SharedPreferences.Editor) null);
    }

    public <T> T b(e<T> eVar, T t) {
        return b(eVar, t, this.b);
    }

    public <T> T b(e eVar, Object obj, SharedPreferences sharedPreferences) {
        return a(eVar.a(), obj, eVar.b(), sharedPreferences);
    }
}
