package com.applovin.impl.sdk.ad;

import android.text.TextUtils;
import android.util.Base64;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.m;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    private final i a;
    private final String b;

    public enum a {
        UNSPECIFIED("UNSPECIFIED"),
        REGULAR("REGULAR"),
        AD_RESPONSE_JSON("AD_RESPONSE_JSON");
        
        private final String d;

        private a(String str) {
            this.d = str;
        }

        public String toString() {
            return this.d;
        }
    }

    public c(String str, i iVar) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Identifier is empty");
        } else if (iVar != null) {
            this.b = str;
            this.a = iVar;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    private String a(com.applovin.impl.sdk.b.c<String> cVar) {
        for (String next : this.a.b(cVar)) {
            if (this.b.startsWith(next)) {
                return next;
            }
        }
        return null;
    }

    public String a() {
        return this.b;
    }

    public a b() {
        return a(com.applovin.impl.sdk.b.c.aP) != null ? a.REGULAR : a(com.applovin.impl.sdk.b.c.aQ) != null ? a.AD_RESPONSE_JSON : a.UNSPECIFIED;
    }

    public String c() {
        String a2 = a(com.applovin.impl.sdk.b.c.aP);
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        String a3 = a(com.applovin.impl.sdk.b.c.aQ);
        if (!TextUtils.isEmpty(a3)) {
            return a3;
        }
        return null;
    }

    public JSONObject d() {
        if (b() != a.AD_RESPONSE_JSON) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(new String(Base64.decode(this.b.substring(c().length()), 0), "UTF-8"));
            o v = this.a.v();
            v.b("AdToken", "Decoded token into ad response: " + jSONObject);
            return jSONObject;
        } catch (JSONException e) {
            o v2 = this.a.v();
            v2.b("AdToken", "Unable to decode token '" + this.b + "' into JSON", e);
            return null;
        } catch (Throwable th) {
            o v3 = this.a.v();
            v3.b("AdToken", "Unable to process ad response from token '" + this.b + "'", th);
            return null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        String str = this.b;
        String str2 = ((c) obj).b;
        return str != null ? str.equals(str2) : str2 == null;
    }

    public int hashCode() {
        String str = this.b;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        String a2 = m.a(32, this.b);
        return "AdToken{id=" + a2 + ", type=" + b() + '}';
    }
}
