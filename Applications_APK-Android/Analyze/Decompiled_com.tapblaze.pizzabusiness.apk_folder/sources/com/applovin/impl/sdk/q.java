package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.r;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

abstract class q implements l, AppLovinNativeAdLoadListener {
    protected final i a;
    protected final o b;
    /* access modifiers changed from: private */
    public final Object c = new Object();
    private final Map<d, r> d = new HashMap();
    private final Map<d, r> e = new HashMap();
    /* access modifiers changed from: private */
    public final Map<d, Object> f = new HashMap();
    private final Set<d> g = new HashSet();

    q(i iVar) {
        this.a = iVar;
        this.b = iVar.v();
    }

    private void b(final d dVar, Object obj) {
        synchronized (this.c) {
            if (this.f.containsKey(dVar)) {
                this.b.d("PreloadManager", "Possibly missing prior registered preload callback.");
            }
            this.f.put(dVar, obj);
        }
        final int intValue = ((Integer) this.a.a(c.aX)).intValue();
        if (intValue > 0) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    synchronized (q.this.c) {
                        Object obj = q.this.f.get(dVar);
                        if (obj != null) {
                            q.this.f.remove(dVar);
                            o oVar = q.this.b;
                            oVar.e("PreloadManager", "Load callback for zone " + dVar + " timed out after " + intValue + " seconds");
                            q.this.a(obj, dVar, AppLovinErrorCodes.FETCH_AD_TIMEOUT);
                        }
                    }
                }
            }, TimeUnit.SECONDS.toMillis((long) intValue));
        }
    }

    private void c(j jVar) {
        i(a(jVar));
    }

    private r j(d dVar) {
        return this.d.get(dVar);
    }

    private r k(d dVar) {
        return this.e.get(dVar);
    }

    private boolean l(d dVar) {
        boolean z;
        synchronized (this.c) {
            r j = j(dVar);
            z = j != null && j.c();
        }
        return z;
    }

    private r m(d dVar) {
        synchronized (this.c) {
            r k = k(dVar);
            if (k != null && k.a() > 0) {
                return k;
            }
            r j = j(dVar);
            return j;
        }
    }

    private boolean n(d dVar) {
        boolean contains;
        synchronized (this.c) {
            contains = this.g.contains(dVar);
        }
        return contains;
    }

    /* access modifiers changed from: package-private */
    public abstract d a(j jVar);

    /* access modifiers changed from: package-private */
    public abstract a a(d dVar);

    /* access modifiers changed from: package-private */
    public abstract void a(Object obj, d dVar, int i);

    /* access modifiers changed from: package-private */
    public abstract void a(Object obj, j jVar);

    public void a(LinkedHashSet<d> linkedHashSet) {
        Map<d, Object> map = this.f;
        if (map != null && !map.isEmpty()) {
            synchronized (this.c) {
                Iterator<d> it = this.f.keySet().iterator();
                while (it.hasNext()) {
                    d next = it.next();
                    if (!next.j() && !linkedHashSet.contains(next)) {
                        Object obj = this.f.get(next);
                        it.remove();
                        o.i("AppLovinAdService", "Failed to load ad for zone (" + next.a() + "). Please check that the zone has been added to your AppLovin account and given at least 30 minutes to fully propagate.");
                        a(obj, next, -7);
                    }
                }
            }
        }
    }

    public boolean a(d dVar, Object obj) {
        boolean z;
        synchronized (this.c) {
            if (!n(dVar)) {
                b(dVar, obj);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public void b(d dVar, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            i(dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(j jVar) {
        Object obj;
        o oVar;
        String str;
        String str2;
        d a2 = a(jVar);
        boolean i = a2.i();
        synchronized (this.c) {
            obj = this.f.get(a2);
            this.f.remove(a2);
            this.g.add(a2);
            if (obj != null) {
                if (!i) {
                    oVar = this.b;
                    str = "PreloadManager";
                    str2 = "Additional callback found or dummy ads are enabled; skipping enqueue...";
                    oVar.b(str, str2);
                }
            }
            j(a2).a(jVar);
            oVar = this.b;
            str = "PreloadManager";
            str2 = "Ad enqueued: " + jVar;
            oVar.b(str, str2);
        }
        if (obj != null) {
            this.b.b("PreloadManager", "Called additional callback regarding " + jVar);
            if (i) {
                try {
                    a(obj, new g(a2, this.a));
                } catch (Throwable th) {
                    o.c("PreloadManager", "Encountered throwable while notifying user callback", th);
                }
            } else {
                a(obj, jVar);
                c(jVar);
            }
        }
        this.b.b("PreloadManager", "Pulled ad from network and saved to preload cache: " + jVar);
    }

    public boolean b(d dVar) {
        return this.f.containsKey(dVar);
    }

    public j c(d dVar) {
        j f2;
        synchronized (this.c) {
            r m = m(dVar);
            f2 = m != null ? m.f() : null;
        }
        return f2;
    }

    /* access modifiers changed from: package-private */
    public void c(d dVar, int i) {
        Object remove;
        o oVar = this.b;
        oVar.b("PreloadManager", "Failed to pre-load an ad of zone " + dVar + ", error code " + i);
        synchronized (this.c) {
            remove = this.f.remove(dVar);
            this.g.add(dVar);
        }
        if (remove != null) {
            try {
                a(remove, dVar, i);
            } catch (Throwable th) {
                o.c("PreloadManager", "Encountered exception while invoking user callback", th);
            }
        }
    }

    public j d(d dVar) {
        j e2;
        synchronized (this.c) {
            r m = m(dVar);
            e2 = m != null ? m.e() : null;
        }
        return e2;
    }

    public j e(d dVar) {
        j jVar;
        StringBuilder sb;
        String str;
        g gVar;
        synchronized (this.c) {
            r j = j(dVar);
            jVar = null;
            if (j != null) {
                if (dVar.i()) {
                    r k = k(dVar);
                    if (k.c()) {
                        gVar = new g(dVar, this.a);
                    } else if (j.a() > 0) {
                        k.a(j.e());
                        gVar = new g(dVar, this.a);
                    } else if (k.a() > 0 && ((Boolean) this.a.a(c.bZ)).booleanValue()) {
                        gVar = new g(dVar, this.a);
                    }
                    jVar = gVar;
                } else {
                    jVar = j.e();
                }
            }
        }
        o oVar = this.b;
        if (jVar != null) {
            str = "Retrieved ad of zone ";
        } else {
            sb = new StringBuilder();
            str = "Unable to retrieve ad of zone ";
        }
        sb.append(str);
        sb.append(dVar);
        sb.append("...");
        oVar.b("PreloadManager", sb.toString());
        return jVar;
    }

    public void f(d dVar) {
        if (dVar != null) {
            int i = 0;
            synchronized (this.c) {
                r j = j(dVar);
                if (j != null) {
                    i = j.b() - j.a();
                }
            }
            b(dVar, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g(com.applovin.impl.sdk.ad.d r5) {
        /*
            r4 = this;
            java.lang.Object r0 = r4.c
            monitor-enter(r0)
            com.applovin.impl.sdk.r r1 = r4.k(r5)     // Catch:{ all -> 0x0032 }
            com.applovin.impl.sdk.i r2 = r4.a     // Catch:{ all -> 0x0032 }
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r3 = com.applovin.impl.sdk.b.c.ca     // Catch:{ all -> 0x0032 }
            java.lang.Object r2 = r2.a(r3)     // Catch:{ all -> 0x0032 }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ all -> 0x0032 }
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x0032 }
            r3 = 1
            if (r2 == 0) goto L_0x0022
            if (r1 == 0) goto L_0x0022
            int r1 = r1.a()     // Catch:{ all -> 0x0032 }
            if (r1 <= 0) goto L_0x0022
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            return r3
        L_0x0022:
            com.applovin.impl.sdk.r r5 = r4.j(r5)     // Catch:{ all -> 0x0032 }
            if (r5 == 0) goto L_0x002f
            boolean r5 = r5.d()     // Catch:{ all -> 0x0032 }
            if (r5 != 0) goto L_0x002f
            goto L_0x0030
        L_0x002f:
            r3 = 0
        L_0x0030:
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            return r3
        L_0x0032:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.q.g(com.applovin.impl.sdk.ad.d):boolean");
    }

    public void h(d dVar) {
        synchronized (this.c) {
            r j = j(dVar);
            if (j != null) {
                j.a(dVar.e());
            } else {
                this.d.put(dVar, new r(dVar.e()));
            }
            r k = k(dVar);
            if (k != null) {
                k.a(dVar.f());
            } else {
                this.e.put(dVar, new r(dVar.f()));
            }
        }
    }

    public void i(d dVar) {
        if (((Boolean) this.a.a(c.aY)).booleanValue() && !l(dVar)) {
            o oVar = this.b;
            oVar.b("PreloadManager", "Preloading ad for zone " + dVar + "...");
            this.a.K().a(a(dVar), r.a.MAIN, 500);
        }
    }
}
