package com.applovin.impl.sdk.network;

import android.content.Context;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.b.f;
import org.json.JSONException;
import org.json.JSONObject;

public class d {
    private static final Object a = new Object();

    private static JSONObject a(String str, Context context) {
        JSONObject b = b(context);
        if (b == null) {
            b = new JSONObject();
        }
        if (!b.has(str)) {
            try {
                b.put(str, new JSONObject());
            } catch (JSONException unused) {
            }
        }
        return b;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:5|6|7|8|9|10|11|12|13|14) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0023 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0026 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(int r4, java.lang.String r5, android.content.Context r6) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 == 0) goto L_0x0007
            return
        L_0x0007:
            java.lang.Object r0 = com.applovin.impl.sdk.network.d.a
            monitor-enter(r0)
            java.lang.String r5 = com.applovin.impl.sdk.utils.p.d(r5)     // Catch:{ all -> 0x002b }
            org.json.JSONObject r1 = a(r5, r6)     // Catch:{ all -> 0x002b }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ all -> 0x002b }
            org.json.JSONObject r2 = r1.optJSONObject(r5)     // Catch:{ all -> 0x002b }
            int r3 = r2.optInt(r4)     // Catch:{ all -> 0x002b }
            int r3 = r3 + 1
            r2.put(r4, r3)     // Catch:{ JSONException -> 0x0023 }
        L_0x0023:
            r1.put(r5, r2)     // Catch:{ JSONException -> 0x0026 }
        L_0x0026:
            a(r1, r6)     // Catch:{ all -> 0x002b }
            monitor-exit(r0)     // Catch:{ all -> 0x002b }
            return
        L_0x002b:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002b }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.d.a(int, java.lang.String, android.content.Context):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.b.f.a(com.applovin.impl.sdk.b.e, android.content.Context):void
     arg types: [com.applovin.impl.sdk.b.e<java.lang.String>, android.content.Context]
     candidates:
      com.applovin.impl.sdk.b.f.a(com.applovin.impl.sdk.b.e, java.lang.Object):void
      com.applovin.impl.sdk.b.f.a(com.applovin.impl.sdk.b.e, android.content.Context):void */
    public static void a(Context context) {
        synchronized (a) {
            f.a((e) e.o, context);
        }
    }

    private static void a(JSONObject jSONObject, Context context) {
        f.a(e.o, jSONObject.toString(), context);
    }

    public static JSONObject b(Context context) {
        JSONObject jSONObject;
        synchronized (a) {
            try {
                jSONObject = new JSONObject((String) f.b(e.o, "{}", context));
            } catch (JSONException unused) {
                return new JSONObject();
            } catch (Throwable th) {
                throw th;
            }
        }
        return jSONObject;
    }
}
