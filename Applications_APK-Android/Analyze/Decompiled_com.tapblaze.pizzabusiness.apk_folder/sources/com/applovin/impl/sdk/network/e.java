package com.applovin.impl.sdk.network;

import android.content.SharedPreferences;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONObject;

public class e {
    private final i a;
    /* access modifiers changed from: private */
    public final o b;
    private final SharedPreferences c;
    private final Object d = new Object();
    private final ArrayList<f> e;
    private final ArrayList<f> f = new ArrayList<>();
    private final Set<f> g = new HashSet();

    public e(i iVar) {
        if (iVar != null) {
            this.a = iVar;
            this.b = iVar.v();
            this.c = iVar.D().getSharedPreferences("com.applovin.sdk.impl.postbackQueue.domain", 0);
            this.e = b();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005a, code lost:
        r0 = ((java.lang.Integer) r4.a.a(com.applovin.impl.sdk.b.c.ds)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006c, code lost:
        if (r5.g() <= r0) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
        r6 = r4.b;
        r6.d("PersistentPostbackManager", "Exceeded maximum persisted attempt count of " + r0 + ". Dequeuing postback: " + r5);
        d(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0092, code lost:
        r1 = r4.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0094, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.g.add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x009a, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x009f, code lost:
        if (r5.e() == null) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a1, code lost:
        r0 = new org.json.JSONObject(r5.e());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ab, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ac, code lost:
        r4.a.R().dispatchPostbackRequest(com.applovin.impl.sdk.network.g.b(r4.a).d(r5.a()).e(r5.b()).b(r5.c()).c(r5.d()).b(r0).c(r5.f()).b(), new com.applovin.impl.sdk.network.e.AnonymousClass1(r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(final com.applovin.impl.sdk.network.f r5, final com.applovin.sdk.AppLovinPostbackListener r6) {
        /*
            r4 = this;
            com.applovin.impl.sdk.o r0 = r4.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Preparing to submit postback..."
            r1.append(r2)
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "PersistentPostbackManager"
            r0.b(r2, r1)
            com.applovin.impl.sdk.i r0 = r4.a
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x002a
            com.applovin.impl.sdk.o r5 = r4.b
            java.lang.String r6 = "PersistentPostbackManager"
            java.lang.String r0 = "Skipping postback dispatch because SDK is still initializing - postback will be dispatched afterwards"
            r5.b(r6, r0)
            return
        L_0x002a:
            java.lang.Object r0 = r4.d
            monitor-enter(r0)
            java.util.Set<com.applovin.impl.sdk.network.f> r1 = r4.g     // Catch:{ all -> 0x00f4 }
            boolean r1 = r1.contains(r5)     // Catch:{ all -> 0x00f4 }
            if (r1 == 0) goto L_0x0053
            com.applovin.impl.sdk.o r6 = r4.b     // Catch:{ all -> 0x00f4 }
            java.lang.String r1 = "PersistentPostbackManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f4 }
            r2.<init>()     // Catch:{ all -> 0x00f4 }
            java.lang.String r3 = "Skip pending postback: "
            r2.append(r3)     // Catch:{ all -> 0x00f4 }
            java.lang.String r5 = r5.a()     // Catch:{ all -> 0x00f4 }
            r2.append(r5)     // Catch:{ all -> 0x00f4 }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x00f4 }
            r6.b(r1, r5)     // Catch:{ all -> 0x00f4 }
            monitor-exit(r0)     // Catch:{ all -> 0x00f4 }
            return
        L_0x0053:
            r5.h()     // Catch:{ all -> 0x00f4 }
            r4.c()     // Catch:{ all -> 0x00f4 }
            monitor-exit(r0)     // Catch:{ all -> 0x00f4 }
            com.applovin.impl.sdk.i r0 = r4.a
            com.applovin.impl.sdk.b.c<java.lang.Integer> r1 = com.applovin.impl.sdk.b.c.ds
            java.lang.Object r0 = r0.a(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            int r1 = r5.g()
            if (r1 <= r0) goto L_0x0092
            com.applovin.impl.sdk.o r6 = r4.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Exceeded maximum persisted attempt count of "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = ". Dequeuing postback: "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = r1.toString()
            java.lang.String r1 = "PersistentPostbackManager"
            r6.d(r1, r0)
            r4.d(r5)
            goto L_0x00f0
        L_0x0092:
            java.lang.Object r1 = r4.d
            monitor-enter(r1)
            java.util.Set<com.applovin.impl.sdk.network.f> r0 = r4.g     // Catch:{ all -> 0x00f1 }
            r0.add(r5)     // Catch:{ all -> 0x00f1 }
            monitor-exit(r1)     // Catch:{ all -> 0x00f1 }
            java.util.Map r0 = r5.e()
            if (r0 == 0) goto L_0x00ab
            org.json.JSONObject r0 = new org.json.JSONObject
            java.util.Map r1 = r5.e()
            r0.<init>(r1)
            goto L_0x00ac
        L_0x00ab:
            r0 = 0
        L_0x00ac:
            com.applovin.impl.sdk.i r1 = r4.a
            com.applovin.impl.sdk.network.g$a r1 = com.applovin.impl.sdk.network.g.b(r1)
            java.lang.String r2 = r5.a()
            com.applovin.impl.sdk.network.g$a r1 = r1.a(r2)
            java.lang.String r2 = r5.b()
            com.applovin.impl.sdk.network.g$a r1 = r1.c(r2)
            java.util.Map r2 = r5.c()
            com.applovin.impl.sdk.network.g$a r1 = r1.b(r2)
            java.util.Map r2 = r5.d()
            com.applovin.impl.sdk.network.g$a r1 = r1.c(r2)
            com.applovin.impl.sdk.network.g$a r0 = r1.a(r0)
            boolean r1 = r5.f()
            com.applovin.impl.sdk.network.g$a r0 = r0.a(r1)
            com.applovin.impl.sdk.network.g r0 = r0.a()
            com.applovin.impl.sdk.i r1 = r4.a
            com.applovin.impl.sdk.network.PostbackServiceImpl r1 = r1.R()
            com.applovin.impl.sdk.network.e$1 r2 = new com.applovin.impl.sdk.network.e$1
            r2.<init>(r5, r6)
            r1.dispatchPostbackRequest(r0, r2)
        L_0x00f0:
            return
        L_0x00f1:
            r5 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00f1 }
            throw r5
        L_0x00f4:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00f4 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, com.applovin.sdk.AppLovinPostbackListener):void");
    }

    private ArrayList<f> b() {
        Set<String> set = (Set) this.a.b(com.applovin.impl.sdk.b.e.k, new LinkedHashSet(0), this.c);
        ArrayList<f> arrayList = new ArrayList<>(Math.max(1, set.size()));
        int intValue = ((Integer) this.a.a(c.ds)).intValue();
        o oVar = this.b;
        oVar.b("PersistentPostbackManager", "Deserializing " + set.size() + " postback(s).");
        for (String str : set) {
            try {
                f fVar = new f(new JSONObject(str), this.a);
                if (fVar.g() < intValue) {
                    arrayList.add(fVar);
                } else {
                    o oVar2 = this.b;
                    oVar2.b("PersistentPostbackManager", "Skipping deserialization because maximum attempt count exceeded for postback: " + fVar);
                }
            } catch (Throwable th) {
                o oVar3 = this.b;
                oVar3.b("PersistentPostbackManager", "Unable to deserialize postback request from json: " + str, th);
            }
        }
        o oVar4 = this.b;
        oVar4.b("PersistentPostbackManager", "Successfully loaded postback queue with " + arrayList.size() + " postback(s).");
        return arrayList;
    }

    private void b(f fVar) {
        synchronized (this.d) {
            this.e.add(fVar);
            c();
            o oVar = this.b;
            oVar.b("PersistentPostbackManager", "Enqueued postback: " + fVar);
        }
    }

    private void c() {
        String str;
        o oVar;
        if (g.b()) {
            LinkedHashSet linkedHashSet = new LinkedHashSet(this.e.size());
            Iterator<f> it = this.e.iterator();
            while (it.hasNext()) {
                try {
                    linkedHashSet.add(it.next().j().toString());
                } catch (Throwable th) {
                    this.b.b("PersistentPostbackManager", "Unable to serialize postback request to JSON.", th);
                }
            }
            this.a.a(com.applovin.impl.sdk.b.e.k, linkedHashSet, this.c);
            oVar = this.b;
            str = "Wrote updated postback queue to disk.";
        } else {
            oVar = this.b;
            str = "Skipping writing postback queue to disk due to old Android version...";
        }
        oVar.b("PersistentPostbackManager", str);
    }

    private void c(f fVar) {
        a(fVar, (AppLovinPostbackListener) null);
    }

    /* access modifiers changed from: private */
    public void d() {
        synchronized (this.d) {
            Iterator<f> it = this.f.iterator();
            while (it.hasNext()) {
                c(it.next());
            }
            this.f.clear();
        }
    }

    /* access modifiers changed from: private */
    public void d(f fVar) {
        synchronized (this.d) {
            this.g.remove(fVar);
            this.e.remove(fVar);
            c();
        }
        o oVar = this.b;
        oVar.b("PersistentPostbackManager", "Dequeued successfully transmitted postback: " + fVar);
    }

    /* access modifiers changed from: private */
    public void e(f fVar) {
        synchronized (this.d) {
            this.g.remove(fVar);
            this.f.add(fVar);
        }
    }

    public void a() {
        synchronized (this.d) {
            if (this.e != null) {
                for (f c2 : new ArrayList(this.e)) {
                    c(c2);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, boolean):void
     arg types: [com.applovin.impl.sdk.network.f, int]
     candidates:
      com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.e, com.applovin.impl.sdk.network.f):void
      com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, com.applovin.sdk.AppLovinPostbackListener):void
      com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, boolean):void */
    public void a(f fVar) {
        a(fVar, true);
    }

    public void a(f fVar, boolean z) {
        a(fVar, z, null);
    }

    public void a(f fVar, boolean z, AppLovinPostbackListener appLovinPostbackListener) {
        if (m.b(fVar.a())) {
            if (z) {
                fVar.i();
            }
            synchronized (this.d) {
                b(fVar);
                a(fVar, appLovinPostbackListener);
            }
        }
    }
}
