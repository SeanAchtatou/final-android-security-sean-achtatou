package com.applovin.impl.sdk.utils;

import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.b.f;
import com.applovin.impl.sdk.i;
import java.util.Locale;
import java.util.UUID;

public final class o {
    private final i a;
    private String b = d();
    private final String c;
    private final String d;

    public o(i iVar) {
        this.a = iVar;
        this.c = a(e.e, (String) f.b(e.d, (Object) null, iVar.D()));
        this.d = a(e.f, (String) iVar.a(c.S));
    }

    private String a(e<String> eVar, String str) {
        String str2 = (String) f.b(eVar, (Object) null, this.a.D());
        if (m.b(str2)) {
            return str2;
        }
        if (!m.b(str)) {
            str = UUID.randomUUID().toString().toLowerCase(Locale.US);
        }
        f.a(eVar, str, this.a.D());
        return str;
    }

    private String d() {
        if (!((Boolean) this.a.a(c.dX)).booleanValue()) {
            this.a.b(e.c);
        }
        String str = (String) this.a.a(e.c);
        if (!m.b(str)) {
            return null;
        }
        com.applovin.impl.sdk.o v = this.a.v();
        v.b("AppLovinSdk", "Using identifier (" + str + ") from previous session");
        this.b = str;
        return null;
    }

    public String a() {
        return this.b;
    }

    public void a(String str) {
        if (((Boolean) this.a.a(c.dX)).booleanValue()) {
            this.a.a(e.c, str);
        }
        this.b = str;
    }

    public String b() {
        return this.c;
    }

    public String c() {
        return this.d;
    }
}
