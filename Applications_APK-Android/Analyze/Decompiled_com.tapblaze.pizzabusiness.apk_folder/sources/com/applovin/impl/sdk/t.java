package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.utils.a;
import com.applovin.impl.sdk.utils.p;
import com.ironsource.sdk.constants.Constants;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class t {
    final i a;
    private final AtomicBoolean b = new AtomicBoolean();
    private final AtomicBoolean c = new AtomicBoolean();
    private final AtomicBoolean d = new AtomicBoolean();
    private Date e;
    private Date f;

    t(i iVar) {
        this.a = iVar;
        Application application = (Application) iVar.D();
        application.registerActivityLifecycleCallbacks(new a() {
            public void onActivityResumed(Activity activity) {
                super.onActivityResumed(activity);
                t.this.e();
            }
        });
        application.registerComponentCallbacks(new ComponentCallbacks2() {
            public void onConfigurationChanged(Configuration configuration) {
            }

            public void onLowMemory() {
            }

            public void onTrimMemory(int i) {
                if (i == 20) {
                    t.this.f();
                }
            }
        });
        IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        application.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if ("android.intent.action.USER_PRESENT".equals(action)) {
                    if (p.c()) {
                        t.this.e();
                    }
                } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                    t.this.f();
                }
            }
        }, intentFilter);
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.d.compareAndSet(true, false)) {
            h();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.d.compareAndSet(false, true)) {
            g();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.k, com.applovin.impl.sdk.j$a):java.util.HashMap<java.lang.String, java.lang.String>
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.j$a, java.util.Map<java.lang.String, java.lang.String>):void
      com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void */
    private void g() {
        this.a.v().b("SessionTracker", "Application Paused");
        this.a.ad().sendBroadcastSync(new Intent("com.applovin.application_paused"));
        if (!this.c.get() && ((Boolean) this.a.a(c.dO)).booleanValue()) {
            boolean booleanValue = ((Boolean) this.a.a(c.dL)).booleanValue();
            long millis = TimeUnit.MINUTES.toMillis(((Long) this.a.a(c.dN)).longValue());
            if (this.e == null || System.currentTimeMillis() - this.e.getTime() >= millis) {
                ((EventServiceImpl) this.a.q()).a(Constants.ParametersKeys.VIDEO_STATUS_PAUSED, false);
                if (booleanValue) {
                    this.e = new Date();
                }
            }
            if (!booleanValue) {
                this.e = new Date();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.k, com.applovin.impl.sdk.j$a):java.util.HashMap<java.lang.String, java.lang.String>
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.j$a, java.util.Map<java.lang.String, java.lang.String>):void
      com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void */
    private void h() {
        this.a.v().b("SessionTracker", "Application Resumed");
        boolean booleanValue = ((Boolean) this.a.a(c.dL)).booleanValue();
        long longValue = ((Long) this.a.a(c.dM)).longValue();
        this.a.ad().sendBroadcastSync(new Intent("com.applovin.application_resumed"));
        if (!this.c.getAndSet(false)) {
            long millis = TimeUnit.MINUTES.toMillis(longValue);
            if (this.f == null || System.currentTimeMillis() - this.f.getTime() >= millis) {
                ((EventServiceImpl) this.a.q()).a("resumed", false);
                if (booleanValue) {
                    this.f = new Date();
                }
            }
            if (!booleanValue) {
                this.f = new Date();
            }
            this.a.L().a(g.k);
            this.b.set(true);
        }
    }

    public boolean a() {
        return this.d.get();
    }

    public void b() {
        this.c.set(true);
    }

    public void c() {
        this.c.set(false);
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.b.getAndSet(false);
    }
}
