package com.applovin.impl.sdk.d;

import com.applovin.impl.a.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinAdLoadListener;
import org.json.JSONObject;

abstract class t extends a {
    private final AppLovinAdLoadListener a;
    private final a c;

    private static final class a extends com.applovin.impl.a.c {
        a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.ad.b bVar, i iVar) {
            super(jSONObject, jSONObject2, bVar, iVar);
        }

        /* access modifiers changed from: package-private */
        public void a(r rVar) {
            if (rVar != null) {
                this.a.add(rVar);
                return;
            }
            throw new IllegalArgumentException("No aggregated vast response specified");
        }
    }

    private static final class b extends t {
        private final JSONObject a;

        b(com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
            super(cVar, appLovinAdLoadListener, iVar);
            if (appLovinAdLoadListener != null) {
                this.a = cVar.c();
                return;
            }
            throw new IllegalArgumentException("No callback specified.");
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.s;
        }

        public void run() {
            d dVar;
            a("Processing SDK JSON response...");
            String b = com.applovin.impl.sdk.utils.i.b(this.a, "xml", (String) null, this.b);
            if (!m.b(b)) {
                d("No VAST response received.");
                dVar = d.NO_WRAPPER_RESPONSE;
            } else if (b.length() < ((Integer) this.b.a(com.applovin.impl.sdk.b.c.eA)).intValue()) {
                try {
                    a(s.a(b, this.b));
                    return;
                } catch (Throwable th) {
                    a("Unable to parse VAST response", th);
                    a(d.XML_PARSING);
                    this.b.M().a(a());
                    return;
                }
            } else {
                d("VAST response is over max length");
                dVar = d.XML_PARSING;
            }
            a(dVar);
        }
    }

    private static final class c extends t {
        private final r a;

        c(r rVar, com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
            super(cVar, appLovinAdLoadListener, iVar);
            if (rVar == null) {
                throw new IllegalArgumentException("No response specified.");
            } else if (cVar == null) {
                throw new IllegalArgumentException("No context specified.");
            } else if (appLovinAdLoadListener != null) {
                this.a = rVar;
            } else {
                throw new IllegalArgumentException("No callback specified.");
            }
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.t;
        }

        public void run() {
            a("Processing VAST Wrapper response...");
            a(this.a);
        }
    }

    t(com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        super("TaskProcessVastResponse", iVar);
        if (cVar != null) {
            this.a = appLovinAdLoadListener;
            this.c = (a) cVar;
            return;
        }
        throw new IllegalArgumentException("No context specified.");
    }

    public static t a(r rVar, com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        return new c(rVar, cVar, appLovinAdLoadListener, iVar);
    }

    public static t a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.ad.b bVar, AppLovinAdLoadListener appLovinAdLoadListener, i iVar) {
        return new b(new a(jSONObject, jSONObject2, bVar, iVar), appLovinAdLoadListener, iVar);
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        d("Failed to process VAST response due to VAST error code " + dVar);
        com.applovin.impl.a.i.a(this.c, this.a, dVar, -6, this.b);
    }

    /* access modifiers changed from: package-private */
    public void a(r rVar) {
        d dVar;
        a aVar;
        int a2 = this.c.a();
        a("Finished parsing XML at depth " + a2);
        this.c.a(rVar);
        if (com.applovin.impl.a.i.a(rVar)) {
            int intValue = ((Integer) this.b.a(com.applovin.impl.sdk.b.c.eB)).intValue();
            if (a2 < intValue) {
                a("VAST response is wrapper. Resolving...");
                aVar = new aa(this.c, this.a, this.b);
            } else {
                d("Reached beyond max wrapper depth of " + intValue);
                dVar = d.WRAPPER_LIMIT_REACHED;
                a(dVar);
                return;
            }
        } else if (com.applovin.impl.a.i.b(rVar)) {
            a("VAST response is inline. Rendering ad...");
            aVar = new w(this.c, this.a, this.b);
        } else {
            d("VAST response is an error");
            dVar = d.NO_WRAPPER_RESPONSE;
            a(dVar);
            return;
        }
        this.b.K().a(aVar);
    }
}
