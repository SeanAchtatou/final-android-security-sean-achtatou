package com.applovin.impl.sdk.d;

import android.net.Uri;
import com.applovin.impl.mediation.b.a;
import com.applovin.impl.mediation.k;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.c.e;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.m;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.vungle.warren.analytics.AnalyticsEvent;
import com.vungle.warren.model.Advertisement;
import cz.msebera.android.httpclient.message.TokenParser;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

abstract class c extends a implements k.a {
    protected final f a;
    private AppLovinAdLoadListener c;
    private final m d;
    private final Collection<Character> e;
    private final e f;
    private boolean g;

    c(String str, f fVar, i iVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        super(str, iVar);
        if (fVar != null) {
            this.a = fVar;
            this.c = appLovinAdLoadListener;
            this.d = iVar.V();
            this.e = j();
            this.f = new e();
            return;
        }
        throw new IllegalArgumentException("No ad specified.");
    }

    private Uri a(Uri uri, String str) {
        String str2;
        StringBuilder sb;
        if (uri != null) {
            String uri2 = uri.toString();
            if (com.applovin.impl.sdk.utils.m.b(uri2)) {
                a("Caching " + str + " image...");
                return g(uri2);
            }
            sb = new StringBuilder();
            sb.append("Failed to cache ");
            sb.append(str);
            str2 = " image";
        } else {
            sb = new StringBuilder();
            sb.append("No ");
            sb.append(str);
            str2 = " image to cache";
        }
        sb.append(str2);
        a(sb.toString());
        return null;
    }

    private String a(String str, String str2) {
        StringBuilder sb;
        String replace = str2.replace("/", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        String G = this.a.G();
        if (com.applovin.impl.sdk.utils.m.b(G)) {
            replace = G + replace;
        }
        File a2 = this.d.a(replace, this.b.D());
        if (a2 == null) {
            return null;
        }
        if (a2.exists()) {
            this.f.b(a2.length());
            sb = new StringBuilder();
        } else {
            if (!this.d.a(a2, str + str2, Arrays.asList(str), this.f)) {
                return null;
            }
            sb = new StringBuilder();
        }
        sb.append(Advertisement.FILE_SCHEME);
        sb.append(a2.getAbsolutePath());
        return sb.toString();
    }

    private Uri g(String str) {
        return b(str, this.a.F(), true);
    }

    private Collection<Character> j() {
        HashSet hashSet = new HashSet();
        for (char valueOf : ((String) this.b.a(com.applovin.impl.sdk.b.c.bF)).toCharArray()) {
            hashSet.add(Character.valueOf(valueOf));
        }
        hashSet.add(Character.valueOf(TokenParser.DQUOTE));
        return hashSet;
    }

    /* access modifiers changed from: package-private */
    public Uri a(String str, List<String> list, boolean z) {
        String str2;
        try {
            if (com.applovin.impl.sdk.utils.m.b(str)) {
                a("Caching video " + str + "...");
                String a2 = this.d.a(g(), str, this.a.G(), list, z, this.f);
                if (com.applovin.impl.sdk.utils.m.b(a2)) {
                    File a3 = this.d.a(a2, g());
                    if (a3 != null) {
                        Uri fromFile = Uri.fromFile(a3);
                        if (fromFile != null) {
                            a("Finish caching video for ad #" + this.a.getAdIdNumber() + ". Updating ad with cachedVideoFilename = " + a2);
                            return fromFile;
                        }
                        str2 = "Unable to create URI from cached video file = " + a3;
                    } else {
                        str2 = "Unable to cache video = " + str + "Video file was missing or null - please make sure your app has the WRITE_EXTERNAL_STORAGE permission!";
                    }
                } else if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.bI)).booleanValue()) {
                    d("Failed to cache video");
                    p.a(this.c, this.a.getAdZone(), AppLovinErrorCodes.UNABLE_TO_PRECACHE_VIDEO_RESOURCES, this.b);
                    this.c = null;
                } else {
                    str2 = "Failed to cache video, but not failing ad load";
                }
                d(str2);
            }
        } catch (Exception e2) {
            a("Encountered exception while attempting to cache video.", e2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String a(String str, List<String> list) {
        if (com.applovin.impl.sdk.utils.m.b(str)) {
            Uri parse = Uri.parse(str);
            if (parse == null) {
                a("Nothing to cache, skipping...");
                return null;
            }
            String lastPathSegment = parse.getLastPathSegment();
            if (com.applovin.impl.sdk.utils.m.b(this.a.G())) {
                lastPathSegment = this.a.G() + lastPathSegment;
            }
            File a2 = this.d.a(lastPathSegment, g());
            ByteArrayOutputStream a3 = (a2 == null || !a2.exists()) ? null : this.d.a(a2);
            if (a3 == null) {
                a3 = this.d.a(str, list, true);
                if (a3 != null) {
                    this.d.a(a3, a2);
                    this.f.a((long) a3.size());
                }
            } else {
                this.f.b((long) a3.size());
            }
            try {
                return a3.toString("UTF-8");
            } catch (UnsupportedEncodingException e2) {
                a("UTF-8 encoding not supported.", e2);
            } catch (Throwable th) {
                a("String resource at " + str + " failed to load.", th);
                return null;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String a(String str, List<String> list, f fVar) {
        if (!com.applovin.impl.sdk.utils.m.b(str)) {
            return str;
        }
        if (!((Boolean) this.b.a(com.applovin.impl.sdk.b.c.bH)).booleanValue()) {
            a("Resource caching is disabled, skipping cache...");
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        boolean shouldCancelHtmlCachingIfShown = fVar.shouldCancelHtmlCachingIfShown();
        for (String next : list) {
            int i = 0;
            int i2 = 0;
            while (i < sb.length()) {
                if (c()) {
                    return str;
                }
                i = sb.indexOf(next, i2);
                if (i == -1) {
                    continue;
                    break;
                }
                int length = sb.length();
                int i3 = i;
                while (!this.e.contains(Character.valueOf(sb.charAt(i3))) && i3 < length) {
                    i3++;
                }
                if (i3 <= i || i3 == length) {
                    d("Unable to cache resource; ad HTML is invalid.");
                    return str;
                }
                String substring = sb.substring(next.length() + i, i3);
                if (!com.applovin.impl.sdk.utils.m.b(substring)) {
                    a("Skip caching of non-resource " + substring);
                } else if (!shouldCancelHtmlCachingIfShown || !fVar.hasShown()) {
                    String a2 = a(next, substring);
                    if (a2 != null) {
                        sb.replace(i, i3, a2);
                        this.f.e();
                    } else {
                        this.f.f();
                    }
                } else {
                    a("Cancelling HTML caching due to ad being shown already");
                    this.f.a();
                    return str;
                }
                i2 = i3;
            }
        }
        return sb.toString();
    }

    public void a(a aVar) {
        if (aVar.b().equalsIgnoreCase(this.a.K())) {
            d("Updating flag for timeout...");
            this.g = true;
        }
        this.b.A().b(this);
    }

    /* access modifiers changed from: package-private */
    public void a(AppLovinAdBase appLovinAdBase) {
        d.a(this.f, appLovinAdBase, this.b);
    }

    /* access modifiers changed from: package-private */
    public Uri b(String str, List<String> list, boolean z) {
        String str2;
        try {
            String a2 = this.d.a(g(), str, this.a.G(), list, z, this.f);
            if (!com.applovin.impl.sdk.utils.m.b(a2)) {
                return null;
            }
            File a3 = this.d.a(a2, g());
            if (a3 != null) {
                Uri fromFile = Uri.fromFile(a3);
                if (fromFile != null) {
                    return fromFile;
                }
                str2 = "Unable to extract Uri from image file";
            } else {
                str2 = "Unable to retrieve File from cached image filename = " + a2;
            }
            d(str2);
            return null;
        } catch (Throwable th) {
            a("Failed to cache image at url = " + str, th);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.b.A().b(this);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        a("Caching mute images...");
        Uri a2 = a(this.a.ay(), AnalyticsEvent.Ad.mute);
        if (a2 != null) {
            this.a.b(a2);
        }
        Uri a3 = a(this.a.az(), AnalyticsEvent.Ad.unmute);
        if (a3 != null) {
            this.a.c(a3);
        }
        a("Ad updated with muteImageFilename = " + this.a.ay() + ", unmuteImageFilename = " + this.a.az());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.d.c.a(java.lang.String, java.util.List<java.lang.String>, boolean):android.net.Uri
     arg types: [java.lang.String, java.util.List<java.lang.String>, int]
     candidates:
      com.applovin.impl.sdk.d.c.a(java.lang.String, java.util.List<java.lang.String>, com.applovin.impl.sdk.ad.f):java.lang.String
      com.applovin.impl.sdk.d.c.a(java.lang.String, java.util.List<java.lang.String>, boolean):android.net.Uri */
    /* access modifiers changed from: package-private */
    public Uri e(String str) {
        return a(str, this.a.F(), true);
    }

    /* access modifiers changed from: package-private */
    public String f(final String str) {
        if (!com.applovin.impl.sdk.utils.m.b(str)) {
            return null;
        }
        b a2 = b.a(this.b).a(str).b("GET").a((Object) "").a(0).a();
        final AtomicReference atomicReference = new AtomicReference(null);
        this.b.J().a(a2, new a.C0006a(), new a.c<String>() {
            public void a(int i) {
                c cVar = c.this;
                cVar.d("Failed to load resource from '" + str + "'");
            }

            public void a(String str, int i) {
                atomicReference.set(str);
            }
        });
        String str2 = (String) atomicReference.get();
        if (str2 != null) {
            this.f.a((long) str2.length());
        }
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.c != null) {
            a("Rendered new ad:" + this.a);
            this.c.adReceived(this.a);
            this.c = null;
        }
    }

    public void run() {
        if (this.a.J()) {
            a("Subscribing to timeout events...");
            this.b.A().a(this);
        }
    }
}
