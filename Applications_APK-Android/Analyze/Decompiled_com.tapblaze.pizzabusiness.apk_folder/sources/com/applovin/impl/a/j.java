package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.MimeTypeMap;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class j {
    private List<k> a = Collections.EMPTY_LIST;
    private List<String> b = Collections.EMPTY_LIST;
    private int c;
    private Uri d;
    private final Set<g> e = new HashSet();
    private final Map<String, Set<g>> f = new HashMap();

    public enum a {
        UNSPECIFIED,
        LOW,
        MEDIUM,
        HIGH
    }

    private j() {
    }

    private j(c cVar) {
        this.b = cVar.h();
    }

    private static int a(String str, i iVar) {
        try {
            List<String> a2 = e.a(str, ":");
            if (a2.size() == 3) {
                return (int) (TimeUnit.HOURS.toSeconds((long) m.a(a2.get(0))) + TimeUnit.MINUTES.toSeconds((long) m.a(a2.get(1))) + ((long) m.a(a2.get(2))));
            }
        } catch (Throwable unused) {
            o v = iVar.v();
            v.e("VastVideoCreative", "Unable to parse duration from \"" + str + "\"");
        }
        return 0;
    }

    public static j a(r rVar, j jVar, c cVar, i iVar) {
        r b2;
        List<k> a2;
        r b3;
        int a3;
        if (rVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (cVar == null) {
            throw new IllegalArgumentException("No context specified.");
        } else if (iVar != null) {
            if (jVar == null) {
                try {
                    jVar = new j(cVar);
                } catch (Throwable th) {
                    iVar.v().b("VastVideoCreative", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (jVar.c == 0 && (b3 = rVar.b("Duration")) != null && (a3 = a(b3.c(), iVar)) > 0) {
                jVar.c = a3;
            }
            r b4 = rVar.b("MediaFiles");
            if (!(b4 == null || (a2 = a(b4, iVar)) == null || a2.size() <= 0)) {
                if (jVar.a != null) {
                    a2.addAll(jVar.a);
                }
                jVar.a = a2;
            }
            r b5 = rVar.b("VideoClicks");
            if (b5 != null) {
                if (jVar.d == null && (b2 = b5.b("ClickThrough")) != null) {
                    String c2 = b2.c();
                    if (m.b(c2)) {
                        jVar.d = Uri.parse(c2);
                    }
                }
                i.a(b5.a("ClickTracking"), jVar.e, cVar, iVar);
            }
            i.a(rVar, jVar.f, cVar, iVar);
            return jVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    private static List<k> a(r rVar, i iVar) {
        List<r> a2 = rVar.a("MediaFile");
        ArrayList arrayList = new ArrayList(a2.size());
        List<String> a3 = e.a((String) iVar.a(c.eE));
        List<String> a4 = e.a((String) iVar.a(c.eD));
        for (r a5 : a2) {
            k a6 = k.a(a5, iVar);
            if (a6 != null) {
                try {
                    String d2 = a6.d();
                    if (!m.b(d2) || a3.contains(d2)) {
                        if (((Boolean) iVar.a(c.eF)).booleanValue()) {
                            String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(a6.b().toString());
                            if (m.b(fileExtensionFromUrl) && !a4.contains(fileExtensionFromUrl)) {
                            }
                        }
                        o v = iVar.v();
                        v.d("VastVideoCreative", "Video file not supported: " + a6);
                    }
                    arrayList.add(a6);
                } catch (Throwable th) {
                    o v2 = iVar.v();
                    v2.b("VastVideoCreative", "Failed to validate vidoe file: " + a6, th);
                }
            }
        }
        return arrayList;
    }

    public k a(a aVar) {
        List<k> list = this.a;
        if (list == null || list.size() == 0) {
            return null;
        }
        List arrayList = new ArrayList(3);
        for (String next : this.b) {
            for (k next2 : this.a) {
                String d2 = next2.d();
                if (m.b(d2) && next.equalsIgnoreCase(d2)) {
                    arrayList.add(next2);
                }
            }
            if (!arrayList.isEmpty()) {
                break;
            }
        }
        if (arrayList.isEmpty()) {
            arrayList = this.a;
        }
        if (g.e()) {
            Collections.sort(arrayList, new Comparator<k>() {
                /* renamed from: a */
                public int compare(k kVar, k kVar2) {
                    return Integer.compare(kVar.e(), kVar2.e());
                }
            });
        }
        return (k) arrayList.get(aVar == a.LOW ? 0 : aVar == a.MEDIUM ? arrayList.size() / 2 : arrayList.size() - 1);
    }

    public List<k> a() {
        return this.a;
    }

    public int b() {
        return this.c;
    }

    public Uri c() {
        return this.d;
    }

    public Set<g> d() {
        return this.e;
    }

    public Map<String, Set<g>> e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        if (this.c != jVar.c) {
            return false;
        }
        List<k> list = this.a;
        if (list == null ? jVar.a != null : !list.equals(jVar.a)) {
            return false;
        }
        Uri uri = this.d;
        if (uri == null ? jVar.d != null : !uri.equals(jVar.d)) {
            return false;
        }
        Set<g> set = this.e;
        if (set == null ? jVar.e != null : !set.equals(jVar.e)) {
            return false;
        }
        Map<String, Set<g>> map = this.f;
        Map<String, Set<g>> map2 = jVar.f;
        return map != null ? map.equals(map2) : map2 == null;
    }

    public int hashCode() {
        List<k> list = this.a;
        int i = 0;
        int hashCode = (((list != null ? list.hashCode() : 0) * 31) + this.c) * 31;
        Uri uri = this.d;
        int hashCode2 = (hashCode + (uri != null ? uri.hashCode() : 0)) * 31;
        Set<g> set = this.e;
        int hashCode3 = (hashCode2 + (set != null ? set.hashCode() : 0)) * 31;
        Map<String, Set<g>> map = this.f;
        if (map != null) {
            i = map.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        return "VastVideoCreative{videoFiles=" + this.a + ", durationSeconds=" + this.c + ", destinationUri=" + this.d + ", clickTrackers=" + this.e + ", eventTrackers=" + this.f + '}';
    }
}
