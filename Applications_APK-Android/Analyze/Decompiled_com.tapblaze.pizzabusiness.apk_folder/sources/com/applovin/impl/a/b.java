package com.applovin.impl.a;

import android.net.Uri;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class b {
    private int a;
    private int b;
    private Uri c;
    private e d;
    private Set<g> e = new HashSet();
    private Map<String, Set<g>> f = new HashMap();

    private b() {
    }

    public static b a(r rVar, b bVar, c cVar, i iVar) {
        r b2;
        if (rVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (iVar != null) {
            if (bVar == null) {
                try {
                    bVar = new b();
                } catch (Throwable th) {
                    iVar.v().b("VastCompanionAd", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (bVar.a == 0 && bVar.b == 0) {
                int a2 = m.a(rVar.b().get("width"));
                int a3 = m.a(rVar.b().get("height"));
                if (a2 > 0 && a3 > 0) {
                    bVar.a = a2;
                    bVar.b = a3;
                }
            }
            bVar.d = e.a(rVar, bVar.d, iVar);
            if (bVar.c == null && (b2 = rVar.b("CompanionClickThrough")) != null) {
                String c2 = b2.c();
                if (m.b(c2)) {
                    bVar.c = Uri.parse(c2);
                }
            }
            i.a(rVar.a("CompanionClickTracking"), bVar.e, cVar, iVar);
            i.a(rVar, bVar.f, cVar, iVar);
            return bVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public Uri a() {
        return this.c;
    }

    public e b() {
        return this.d;
    }

    public Set<g> c() {
        return this.e;
    }

    public Map<String, Set<g>> d() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (this.a != bVar.a || this.b != bVar.b) {
            return false;
        }
        Uri uri = this.c;
        if (uri == null ? bVar.c != null : !uri.equals(bVar.c)) {
            return false;
        }
        e eVar = this.d;
        if (eVar == null ? bVar.d != null : !eVar.equals(bVar.d)) {
            return false;
        }
        Set<g> set = this.e;
        if (set == null ? bVar.e != null : !set.equals(bVar.e)) {
            return false;
        }
        Map<String, Set<g>> map = this.f;
        Map<String, Set<g>> map2 = bVar.f;
        return map != null ? map.equals(map2) : map2 == null;
    }

    public int hashCode() {
        int i = ((this.a * 31) + this.b) * 31;
        Uri uri = this.c;
        int i2 = 0;
        int hashCode = (i + (uri != null ? uri.hashCode() : 0)) * 31;
        e eVar = this.d;
        int hashCode2 = (hashCode + (eVar != null ? eVar.hashCode() : 0)) * 31;
        Set<g> set = this.e;
        int hashCode3 = (hashCode2 + (set != null ? set.hashCode() : 0)) * 31;
        Map<String, Set<g>> map = this.f;
        if (map != null) {
            i2 = map.hashCode();
        }
        return hashCode3 + i2;
    }

    public String toString() {
        return "VastCompanionAd{width=" + this.a + ", height=" + this.b + ", destinationUri=" + this.c + ", nonVideoResource=" + this.d + ", clickTrackers=" + this.e + ", eventTrackers=" + this.f + '}';
    }
}
