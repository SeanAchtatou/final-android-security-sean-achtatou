package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.URLUtil;
import androidx.core.app.NotificationCompat;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class i {
    private static DateFormat a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
    private static Random b = new Random(System.currentTimeMillis());

    public static Uri a(String str, long j, Uri uri, d dVar, com.applovin.impl.sdk.i iVar) {
        if (URLUtil.isValidUrl(str)) {
            try {
                String replace = str.replace("[ERRORCODE]", Integer.toString(dVar.a()));
                if (j >= 0) {
                    replace = replace.replace("[CONTENTPLAYHEAD]", a(j));
                }
                if (uri != null) {
                    replace = replace.replace("[ASSETURI]", uri.toString());
                }
                return Uri.parse(replace.replace("[CACHEBUSTING]", a()).replace("[TIMESTAMP]", b()));
            } catch (Throwable th) {
                o v = iVar.v();
                v.b("VastUtils", "Unable to replace macros in URL string " + str, th);
                return null;
            }
        } else {
            iVar.v().e("VastUtils", "Unable to replace macros in invalid URL string.");
            return null;
        }
    }

    public static d a(a aVar) {
        if (b(aVar) || c(aVar)) {
            return null;
        }
        return d.GENERAL_WRAPPER_ERROR;
    }

    private static String a() {
        return Integer.toString(b.nextInt(89999999) + 10000000);
    }

    private static String a(long j) {
        if (j <= 0) {
            return "00:00:00.000";
        }
        long hours = TimeUnit.SECONDS.toHours(j);
        long seconds = j % TimeUnit.MINUTES.toSeconds(1);
        return String.format(Locale.US, "%02d:%02d:%02d.000", Long.valueOf(hours), Long.valueOf(TimeUnit.SECONDS.toMinutes(j) % TimeUnit.MINUTES.toSeconds(1)), Long.valueOf(seconds));
    }

    public static String a(c cVar) {
        r c;
        if (cVar != null) {
            List<r> b2 = cVar.b();
            int size = cVar.b().size();
            if (size <= 0 || (c = b2.get(size - 1).c("VASTAdTagURI")) == null) {
                return null;
            }
            return c.c();
        }
        throw new IllegalArgumentException("Unable to get resolution uri string for fetching the next wrapper or inline response in the chain");
    }

    public static String a(r rVar, String str, String str2) {
        r b2 = rVar.b(str);
        if (b2 != null) {
            String c = b2.c();
            if (m.b(c)) {
                return c;
            }
        }
        return str2;
    }

    private static Set<g> a(c cVar, com.applovin.impl.sdk.i iVar) {
        if (cVar == null) {
            return null;
        }
        List<r> b2 = cVar.b();
        Set<g> hashSet = new HashSet<>(b2.size());
        for (r next : b2) {
            r c = next.c("Wrapper");
            if (c == null) {
                c = next.c("InLine");
            }
            hashSet = a(hashSet, c != null ? c.a("Error") : next.a("Error"), cVar, iVar);
        }
        o v = iVar.v();
        v.b("VastUtils", "Retrieved " + hashSet.size() + " top level error trackers: " + hashSet);
        return hashSet;
    }

    private static Set<g> a(Set<g> set, List<r> list, c cVar, com.applovin.impl.sdk.i iVar) {
        if (list != null) {
            for (r a2 : list) {
                g a3 = g.a(a2, cVar, iVar);
                if (a3 != null) {
                    set.add(a3);
                }
            }
        }
        return set;
    }

    public static void a(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, d dVar, int i, com.applovin.impl.sdk.i iVar) {
        if (iVar != null) {
            p.a(appLovinAdLoadListener, cVar.g(), i, iVar);
            a(a(cVar, iVar), dVar, iVar);
            return;
        }
        throw new IllegalArgumentException("Unable to handle failure. No sdk specified.");
    }

    public static void a(r rVar, Map<String, Set<g>> map, c cVar, com.applovin.impl.sdk.i iVar) {
        List<r> a2;
        o v;
        String str;
        if (iVar != null) {
            if (rVar == null) {
                v = iVar.v();
                str = "Unable to render event trackers; null node provided";
            } else if (map == null) {
                v = iVar.v();
                str = "Unable to render event trackers; null event trackers provided";
            } else {
                r b2 = rVar.b("TrackingEvents");
                if (b2 != null && (a2 = b2.a("Tracking")) != null) {
                    for (r next : a2) {
                        String str2 = next.b().get(NotificationCompat.CATEGORY_EVENT);
                        if (m.b(str2)) {
                            g a3 = g.a(next, cVar, iVar);
                            if (a3 != null) {
                                Set set = map.get(str2);
                                if (set != null) {
                                    set.add(a3);
                                } else {
                                    HashSet hashSet = new HashSet();
                                    hashSet.add(a3);
                                    map.put(str2, hashSet);
                                }
                            }
                        } else {
                            o v2 = iVar.v();
                            v2.e("VastUtils", "Could not find event for tracking node = " + next);
                        }
                    }
                    return;
                }
                return;
            }
            v.e("VastUtils", str);
            return;
        }
        throw new IllegalArgumentException("Unable to render event trackers. No sdk specified.");
    }

    public static void a(List<r> list, Set<g> set, c cVar, com.applovin.impl.sdk.i iVar) {
        o v;
        String str;
        if (iVar != null) {
            if (list == null) {
                v = iVar.v();
                str = "Unable to render trackers; null nodes provided";
            } else if (set == null) {
                v = iVar.v();
                str = "Unable to render trackers; null trackers provided";
            } else {
                for (r a2 : list) {
                    g a3 = g.a(a2, cVar, iVar);
                    if (a3 != null) {
                        set.add(a3);
                    }
                }
                return;
            }
            v.e("VastUtils", str);
            return;
        }
        throw new IllegalArgumentException("Unable to render trackers. No sdk specified.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, boolean):void
     arg types: [com.applovin.impl.sdk.network.f, int]
     candidates:
      com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.e, com.applovin.impl.sdk.network.f):void
      com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, com.applovin.sdk.AppLovinPostbackListener):void
      com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, boolean):void */
    public static void a(Set<g> set, long j, Uri uri, d dVar, com.applovin.impl.sdk.i iVar) {
        if (iVar == null) {
            throw new IllegalArgumentException("Unable to fire trackers. No sdk specified.");
        } else if (set != null && !set.isEmpty()) {
            for (g b2 : set) {
                Uri a2 = a(b2.b(), j, uri, dVar, iVar);
                if (a2 != null) {
                    iVar.N().a(f.k().a(a2.toString()).a(false).a(), false);
                }
            }
        }
    }

    public static void a(Set<g> set, d dVar, com.applovin.impl.sdk.i iVar) {
        a(set, -1, (Uri) null, dVar, iVar);
    }

    public static void a(Set<g> set, com.applovin.impl.sdk.i iVar) {
        a(set, -1, (Uri) null, d.UNSPECIFIED, iVar);
    }

    public static boolean a(r rVar) {
        if (rVar != null) {
            return rVar.c("Wrapper") != null;
        }
        throw new IllegalArgumentException("Unable to check if a given XmlNode contains a wrapper response");
    }

    private static String b() {
        a.setTimeZone(TimeZone.getDefault());
        return a.format(new Date());
    }

    public static boolean b(a aVar) {
        j h;
        List<k> a2;
        return (aVar == null || (h = aVar.h()) == null || (a2 = h.a()) == null || a2.isEmpty()) ? false : true;
    }

    public static boolean b(r rVar) {
        if (rVar != null) {
            return rVar.c("InLine") != null;
        }
        throw new IllegalArgumentException("Unable to check if a given XmlNode contains an inline response");
    }

    public static boolean c(a aVar) {
        b j;
        e b2;
        if (aVar == null || (j = aVar.j()) == null || (b2 = j.b()) == null) {
            return false;
        }
        return b2.b() != null || m.b(b2.c());
    }
}
