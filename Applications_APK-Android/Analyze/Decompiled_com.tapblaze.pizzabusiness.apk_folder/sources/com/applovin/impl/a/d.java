package com.applovin.impl.a;

import com.ironsource.mediationsdk.logger.IronSourceError;
import cz.msebera.android.httpclient.HttpStatus;

public enum d {
    UNSPECIFIED(-1),
    XML_PARSING(100),
    GENERAL_WRAPPER_ERROR(HttpStatus.SC_MULTIPLE_CHOICES),
    TIMED_OUT(HttpStatus.SC_MOVED_PERMANENTLY),
    WRAPPER_LIMIT_REACHED(302),
    NO_WRAPPER_RESPONSE(HttpStatus.SC_SEE_OTHER),
    GENERAL_LINEAR_ERROR(HttpStatus.SC_BAD_REQUEST),
    NO_MEDIA_FILE_PROVIDED(HttpStatus.SC_UNAUTHORIZED),
    MEDIA_FILE_TIMEOUT(HttpStatus.SC_PAYMENT_REQUIRED),
    MEDIA_FILE_ERROR(HttpStatus.SC_METHOD_NOT_ALLOWED),
    GENERAL_COMPANION_AD_ERROR(600),
    UNABLE_TO_FETCH_COMPANION_AD_RESOURCE(IronSourceError.ERROR_BN_LOAD_WHILE_LONG_INITIATION),
    CAN_NOT_FIND_COMPANION_AD_RESOURCE(IronSourceError.ERROR_BN_LOAD_PLACEMENT_CAPPED);
    
    private final int n;

    private d(int i) {
        this.n = i;
    }

    public int a() {
        return this.n;
    }
}
