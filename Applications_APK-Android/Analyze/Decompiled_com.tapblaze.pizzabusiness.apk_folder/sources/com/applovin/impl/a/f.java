package com.applovin.impl.a;

import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;

public class f {
    private String a;
    private String b;

    private f() {
    }

    public static f a(r rVar, f fVar, i iVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (iVar != null) {
            if (fVar == null) {
                try {
                    fVar = new f();
                } catch (Throwable th) {
                    iVar.v().b("VastSystemInfo", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (!m.b(fVar.a)) {
                String c = rVar.c();
                if (m.b(c)) {
                    fVar.a = c;
                }
            }
            if (!m.b(fVar.b)) {
                String str = rVar.b().get("version");
                if (m.b(str)) {
                    fVar.b = str;
                }
            }
            return fVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        String str = this.a;
        if (str == null ? fVar.a != null : !str.equals(fVar.a)) {
            return false;
        }
        String str2 = this.b;
        String str3 = fVar.b;
        return str2 != null ? str2.equals(str3) : str3 == null;
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "VastSystemInfo{name='" + this.a + '\'' + ", version='" + this.b + '\'' + '}';
    }
}
