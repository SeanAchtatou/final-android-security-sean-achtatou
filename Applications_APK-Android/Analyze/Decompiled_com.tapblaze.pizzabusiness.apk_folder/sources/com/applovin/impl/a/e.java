package com.applovin.impl.a;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;

public class e {
    private a a;
    private Uri b;
    private String c;

    public enum a {
        UNSPECIFIED,
        STATIC,
        IFRAME,
        HTML
    }

    private e() {
    }

    static e a(r rVar, e eVar, i iVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("No node specified.");
        } else if (iVar != null) {
            if (eVar == null) {
                try {
                    eVar = new e();
                } catch (Throwable th) {
                    iVar.v().b("VastNonVideoResource", "Error occurred while initializing", th);
                    return null;
                }
            }
            if (eVar.b == null && !m.b(eVar.c)) {
                String a2 = a(rVar, "StaticResource");
                if (URLUtil.isValidUrl(a2)) {
                    eVar.b = Uri.parse(a2);
                    eVar.a = a.STATIC;
                    return eVar;
                }
                String a3 = a(rVar, "IFrameResource");
                if (m.b(a3)) {
                    eVar.a = a.IFRAME;
                    if (URLUtil.isValidUrl(a3)) {
                        eVar.b = Uri.parse(a3);
                    } else {
                        eVar.c = a3;
                    }
                    return eVar;
                }
                String a4 = a(rVar, "HTMLResource");
                if (m.b(a4)) {
                    eVar.a = a.HTML;
                    if (URLUtil.isValidUrl(a4)) {
                        eVar.b = Uri.parse(a4);
                    } else {
                        eVar.c = a4;
                    }
                }
            }
            return eVar;
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    private static String a(r rVar, String str) {
        r b2 = rVar.b(str);
        if (b2 != null) {
            return b2.c();
        }
        return null;
    }

    public a a() {
        return this.a;
    }

    public void a(Uri uri) {
        this.b = uri;
    }

    public void a(String str) {
        this.c = str;
    }

    public Uri b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        if (this.a != eVar.a) {
            return false;
        }
        Uri uri = this.b;
        if (uri == null ? eVar.b != null : !uri.equals(eVar.b)) {
            return false;
        }
        String str = this.c;
        String str2 = eVar.c;
        return str != null ? str.equals(str2) : str2 == null;
    }

    public int hashCode() {
        a aVar = this.a;
        int i = 0;
        int hashCode = (aVar != null ? aVar.hashCode() : 0) * 31;
        Uri uri = this.b;
        int hashCode2 = (hashCode + (uri != null ? uri.hashCode() : 0)) * 31;
        String str = this.c;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "VastNonVideoResource{type=" + this.a + ", resourceUri=" + this.b + ", resourceContents='" + this.c + '\'' + '}';
    }
}
