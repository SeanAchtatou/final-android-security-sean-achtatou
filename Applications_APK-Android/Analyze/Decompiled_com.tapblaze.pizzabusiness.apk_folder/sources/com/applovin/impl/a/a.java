package com.applovin.impl.a;

import android.net.Uri;
import com.applovin.impl.a.j;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public class a extends f {
    private final String a;
    private final String b;
    private final f c;
    private final long d;
    private final j e;
    private final b f;
    private final Set<g> g;
    private final Set<g> h;

    /* renamed from: com.applovin.impl.a.a$a  reason: collision with other inner class name */
    public static class C0001a {
        /* access modifiers changed from: private */
        public JSONObject a;
        /* access modifiers changed from: private */
        public JSONObject b;
        /* access modifiers changed from: private */
        public com.applovin.impl.sdk.ad.b c;
        /* access modifiers changed from: private */
        public i d;
        /* access modifiers changed from: private */
        public long e;
        /* access modifiers changed from: private */
        public String f;
        /* access modifiers changed from: private */
        public String g;
        /* access modifiers changed from: private */
        public f h;
        /* access modifiers changed from: private */
        public j i;
        /* access modifiers changed from: private */
        public b j;
        /* access modifiers changed from: private */
        public Set<g> k;
        /* access modifiers changed from: private */
        public Set<g> l;

        private C0001a() {
        }

        public C0001a a(long j2) {
            this.e = j2;
            return this;
        }

        public C0001a a(b bVar) {
            this.j = bVar;
            return this;
        }

        public C0001a a(f fVar) {
            this.h = fVar;
            return this;
        }

        public C0001a a(j jVar) {
            this.i = jVar;
            return this;
        }

        public C0001a a(com.applovin.impl.sdk.ad.b bVar) {
            this.c = bVar;
            return this;
        }

        public C0001a a(i iVar) {
            if (iVar != null) {
                this.d = iVar;
                return this;
            }
            throw new IllegalArgumentException("No sdk specified.");
        }

        public C0001a a(String str) {
            this.f = str;
            return this;
        }

        public C0001a a(Set<g> set) {
            this.k = set;
            return this;
        }

        public C0001a a(JSONObject jSONObject) {
            if (jSONObject != null) {
                this.a = jSONObject;
                return this;
            }
            throw new IllegalArgumentException("No ad object specified.");
        }

        public a a() {
            return new a(this);
        }

        public C0001a b(String str) {
            this.g = str;
            return this;
        }

        public C0001a b(Set<g> set) {
            this.l = set;
            return this;
        }

        public C0001a b(JSONObject jSONObject) {
            if (jSONObject != null) {
                this.b = jSONObject;
                return this;
            }
            throw new IllegalArgumentException("No full ad response specified.");
        }
    }

    public enum b {
        COMPANION_AD,
        VIDEO
    }

    public enum c {
        IMPRESSION,
        VIDEO_CLICK,
        COMPANION_CLICK,
        VIDEO,
        COMPANION,
        ERROR
    }

    private a(C0001a aVar) {
        super(aVar.a, aVar.b, aVar.c, aVar.d);
        this.a = aVar.f;
        this.c = aVar.h;
        this.b = aVar.g;
        this.e = aVar.i;
        this.f = aVar.j;
        this.g = aVar.k;
        this.h = aVar.l;
        this.d = aVar.e;
    }

    private Set<g> a(b bVar, String[] strArr) {
        b bVar2;
        j jVar;
        if (strArr == null || strArr.length <= 0) {
            return Collections.emptySet();
        }
        Map<String, Set<g>> map = null;
        if (bVar == b.VIDEO && (jVar = this.e) != null) {
            map = jVar.e();
        } else if (bVar == b.COMPANION_AD && (bVar2 = this.f) != null) {
            map = bVar2.d();
        }
        HashSet hashSet = new HashSet();
        if (map != null && !map.isEmpty()) {
            for (String str : strArr) {
                if (map.containsKey(str)) {
                    hashSet.addAll(map.get(str));
                }
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public static C0001a aH() {
        return new C0001a();
    }

    private String aI() {
        String stringFromAdObject = getStringFromAdObject("vimp_url", null);
        if (stringFromAdObject != null) {
            return stringFromAdObject.replace("{CLCODE}", getClCode());
        }
        return null;
    }

    private j.a aJ() {
        j.a[] values = j.a.values();
        int intValue = ((Integer) this.sdk.a(com.applovin.impl.sdk.b.c.eG)).intValue();
        return (intValue < 0 || intValue >= values.length) ? j.a.UNSPECIFIED : values[intValue];
    }

    private Set<g> aK() {
        j jVar = this.e;
        return jVar != null ? jVar.d() : Collections.emptySet();
    }

    private Set<g> aL() {
        b bVar = this.f;
        return bVar != null ? bVar.c() : Collections.emptySet();
    }

    public Set<g> a(c cVar, String str) {
        return a(cVar, new String[]{str});
    }

    public Set<g> a(c cVar, String[] strArr) {
        o v = this.sdk.v();
        v.b("VastAd", "Retrieving trackers of type '" + cVar + "' and events '" + strArr + "'...");
        if (cVar == c.IMPRESSION) {
            return this.g;
        }
        if (cVar == c.VIDEO_CLICK) {
            return aK();
        }
        if (cVar == c.COMPANION_CLICK) {
            return aL();
        }
        if (cVar == c.VIDEO) {
            return a(b.VIDEO, strArr);
        }
        if (cVar == c.COMPANION) {
            return a(b.COMPANION_AD, strArr);
        }
        if (cVar == c.ERROR) {
            return this.h;
        }
        o v2 = this.sdk.v();
        v2.e("VastAd", "Failed to retrieve trackers of invalid type '" + cVar + "' and events '" + strArr + "'");
        return Collections.emptySet();
    }

    public void a(String str) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("html_template", str);
            }
        } catch (Throwable unused) {
        }
    }

    public boolean a() {
        return b();
    }

    public String aD() {
        return getStringFromAdObject("html_template", "");
    }

    public Uri aE() {
        String stringFromAdObject = getStringFromAdObject("html_template_url", null);
        if (m.b(stringFromAdObject)) {
            return Uri.parse(stringFromAdObject);
        }
        return null;
    }

    public boolean aF() {
        return getBooleanFromAdObject("cache_companion_ad", true);
    }

    public boolean aG() {
        return getBooleanFromAdObject("cache_video", true);
    }

    public List<com.applovin.impl.sdk.c.a> an() {
        List<com.applovin.impl.sdk.c.a> a2;
        synchronized (this.adObjectLock) {
            a2 = p.a("vimp_urls", this.adObject, getClCode(), aI(), this.sdk);
        }
        return a2;
    }

    public boolean b() {
        if (containsKeyForAdObject("vast_is_streaming")) {
            return getBooleanFromAdObject("vast_is_streaming", false);
        }
        k i = i();
        return i != null && i.c();
    }

    public b c() {
        return "companion_ad".equalsIgnoreCase(getStringFromAdObject("vast_first_caching_operation", "companion_ad")) ? b.COMPANION_AD : b.VIDEO;
    }

    public Uri d() {
        k i = i();
        if (i != null) {
            return i.b();
        }
        return null;
    }

    public boolean e() {
        return getBooleanFromAdObject("vast_immediate_ad_load", true);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a) || !super.equals(obj)) {
            return false;
        }
        a aVar = (a) obj;
        String str = this.a;
        if (str == null ? aVar.a != null : !str.equals(aVar.a)) {
            return false;
        }
        String str2 = this.b;
        if (str2 == null ? aVar.b != null : !str2.equals(aVar.b)) {
            return false;
        }
        f fVar = this.c;
        if (fVar == null ? aVar.c != null : !fVar.equals(aVar.c)) {
            return false;
        }
        j jVar = this.e;
        if (jVar == null ? aVar.e != null : !jVar.equals(aVar.e)) {
            return false;
        }
        b bVar = this.f;
        if (bVar == null ? aVar.f != null : !bVar.equals(aVar.f)) {
            return false;
        }
        Set<g> set = this.g;
        if (set == null ? aVar.g != null : !set.equals(aVar.g)) {
            return false;
        }
        Set<g> set2 = this.h;
        Set<g> set3 = aVar.h;
        return set2 != null ? set2.equals(set3) : set3 == null;
    }

    public Uri f() {
        j jVar = this.e;
        if (jVar != null) {
            return jVar.c();
        }
        return null;
    }

    public Uri g() {
        return f();
    }

    public long getCreatedAtMillis() {
        return this.d;
    }

    public j h() {
        return this.e;
    }

    public boolean hasVideoUrl() {
        List<k> a2;
        j jVar = this.e;
        return (jVar == null || (a2 = jVar.a()) == null || a2.size() <= 0) ? false : true;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.a;
        int i = 0;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.b;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        f fVar = this.c;
        int hashCode4 = (hashCode3 + (fVar != null ? fVar.hashCode() : 0)) * 31;
        j jVar = this.e;
        int hashCode5 = (hashCode4 + (jVar != null ? jVar.hashCode() : 0)) * 31;
        b bVar = this.f;
        int hashCode6 = (hashCode5 + (bVar != null ? bVar.hashCode() : 0)) * 31;
        Set<g> set = this.g;
        int hashCode7 = (hashCode6 + (set != null ? set.hashCode() : 0)) * 31;
        Set<g> set2 = this.h;
        if (set2 != null) {
            i = set2.hashCode();
        }
        return hashCode7 + i;
    }

    public k i() {
        j jVar = this.e;
        if (jVar != null) {
            return jVar.a(aJ());
        }
        return null;
    }

    public b j() {
        return this.f;
    }

    public boolean k() {
        return getBooleanFromAdObject("vast_fire_click_trackers_on_html_clicks", false);
    }

    public String toString() {
        return "VastAd{title='" + this.a + '\'' + ", adDescription='" + this.b + '\'' + ", systemInfo=" + this.c + ", videoCreative=" + this.e + ", companionAd=" + this.f + ", impressionTrackers=" + this.g + ", errorTrackers=" + this.h + '}';
    }

    public boolean u() {
        return f() != null;
    }
}
