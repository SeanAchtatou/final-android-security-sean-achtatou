package com.applovin.impl.mediation.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.sdk.AppLovinSdk;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class c {
    private static final List<String> a = new ArrayList();
    private static a b;

    public static class a {
        private final JSONArray a;
        private final JSONArray b;

        private a(JSONArray jSONArray, JSONArray jSONArray2) {
            this.a = jSONArray;
            this.b = jSONArray2;
        }

        public JSONArray a() {
            return this.a;
        }

        public JSONArray b() {
            return this.b;
        }
    }

    static {
        a.add("com.applovin.mediation.adapters.AdColonyMediationAdapter");
        a.add("com.applovin.mediation.adapters.AmazonMediationAdapter");
        a.add("com.applovin.mediation.adapters.AmazonBiddingMediationAdapter");
        a.add("com.applovin.mediation.adapters.AppLovinMediationAdapter");
        a.add("com.applovin.mediation.adapters.ChartboostMediationAdapter");
        a.add("com.applovin.mediation.adapters.FacebookMediationAdapter");
        a.add("com.applovin.mediation.adapters.GoogleMediationAdapter");
        a.add("com.applovin.mediation.adapters.HyperMXMediationAdapter");
        a.add("com.applovin.mediation.adapters.IMobileMediationAdapter");
        a.add("com.applovin.mediation.adapters.InMobiMediationAdapter");
        a.add("com.applovin.mediation.adapters.InneractiveMediationAdapter");
        a.add("com.applovin.mediation.adapters.IronSourceMediationAdapter");
        a.add("com.applovin.mediation.adapters.LeadboltMediationAdapter");
        a.add("com.applovin.mediation.adapters.MadvertiseMediationAdapter");
        a.add("com.applovin.mediation.adapters.MaioMediationAdapter");
        a.add("com.applovin.mediation.adapters.MintegralMediationAdapter");
        a.add("com.applovin.mediation.adapters.MoPubMediationAdapter");
        a.add("com.applovin.mediation.adapters.MyTargetMediationAdapter");
        a.add("com.applovin.mediation.adapters.NendMediationAdapter");
        a.add("com.applovin.mediation.adapters.OguryMediationAdapter");
        a.add("com.applovin.mediation.adapters.SmaatoMediationAdapter");
        a.add("com.applovin.mediation.adapters.TapjoyMediationAdapter");
        a.add("com.applovin.mediation.adapters.TencentMediationAdapter");
        a.add("com.applovin.mediation.adapters.UnityAdsMediationAdapter");
        a.add("com.applovin.mediation.adapters.VerizonAdsMediationAdapter");
        a.add("com.applovin.mediation.adapters.VungleMediationAdapter");
        a.add("com.applovin.mediation.adapters.YandexMediationAdapter");
    }

    public static a a(i iVar) {
        a aVar = b;
        if (aVar != null) {
            return aVar;
        }
        JSONArray jSONArray = new JSONArray();
        JSONArray jSONArray2 = new JSONArray();
        for (String next : a) {
            MaxAdapter a2 = a(next, iVar);
            if (a2 != null) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("class", next);
                    jSONObject.put("sdk_version", a2.getSdkVersion());
                    jSONObject.put("version", a2.getAdapterVersion());
                } catch (Throwable unused) {
                }
                jSONArray.put(jSONObject);
            } else {
                jSONArray2.put(next);
            }
        }
        b = new a(jSONArray, jSONArray2);
        return b;
    }

    public static r.a a(MaxAdFormat maxAdFormat) {
        return maxAdFormat == MaxAdFormat.INTERSTITIAL ? r.a.MEDIATION_INTERSTITIAL : maxAdFormat == MaxAdFormat.REWARDED ? r.a.MEDIATION_INCENTIVIZED : r.a.MEDIATION_BANNER;
    }

    public static MaxAdapter a(String str, i iVar) {
        if (TextUtils.isEmpty(str)) {
            iVar.v().e("AppLovinSdk", "Failed to create adapter instance. No class name provided");
            return null;
        }
        try {
            Class<?> cls = Class.forName(str);
            if (MaxAdapter.class.isAssignableFrom(cls)) {
                return (MaxAdapter) cls.getConstructor(AppLovinSdk.class).newInstance(iVar.S());
            }
            o v = iVar.v();
            v.e("AppLovinSdk", str + " error: not an instance of '" + MaxAdapter.class.getName() + "'.");
            return null;
        } catch (ClassNotFoundException unused) {
        } catch (Throwable th) {
            o v2 = iVar.v();
            v2.b("AppLovinSdk", "Failed to load: " + str, th);
        }
    }

    public static String b(MaxAdFormat maxAdFormat) {
        return maxAdFormat.getLabel();
    }
}
