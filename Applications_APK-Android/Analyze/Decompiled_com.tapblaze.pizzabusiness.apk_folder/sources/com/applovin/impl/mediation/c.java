package com.applovin.impl.mediation;

import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.d;

public class c {
    private final i a;
    /* access modifiers changed from: private */
    public final o b;
    /* access modifiers changed from: private */
    public final a c;
    private d d;

    public interface a {
        void c(com.applovin.impl.mediation.b.c cVar);
    }

    c(i iVar, a aVar) {
        this.a = iVar;
        this.b = iVar.v();
        this.c = aVar;
    }

    public void a() {
        this.b.b("AdHiddenCallbackTimeoutManager", "Cancelling timeout");
        d dVar = this.d;
        if (dVar != null) {
            dVar.a();
            this.d = null;
        }
    }

    public void a(final com.applovin.impl.mediation.b.c cVar, long j) {
        o oVar = this.b;
        oVar.b("AdHiddenCallbackTimeoutManager", "Scheduling in " + j + "ms...");
        this.d = d.a(j, this.a, new Runnable() {
            public void run() {
                c.this.b.b("AdHiddenCallbackTimeoutManager", "Timing out...");
                c.this.c.c(cVar);
            }
        });
    }
}
