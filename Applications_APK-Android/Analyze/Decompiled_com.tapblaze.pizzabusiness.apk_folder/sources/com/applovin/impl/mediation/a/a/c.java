package com.applovin.impl.mediation.a.a;

import android.content.Context;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.facebook.internal.NativeProtocol;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class c implements Comparable<c> {
    private static String a = "MediatedNetwork";
    private final a b;
    private final boolean c;
    private final boolean d;
    private final String e;
    private final String f;
    private final String g;
    private final String h;
    private final List<MaxAdFormat> i;
    private final List<e> j;
    private final d k;

    public enum a {
        MISSING,
        INCOMPLETE_INTEGRATION,
        INVALID_INTEGRATION,
        COMPLETE
    }

    public c(JSONObject jSONObject, i iVar) {
        String str;
        String str2 = "";
        this.e = com.applovin.impl.sdk.utils.i.b(jSONObject, "display_name", str2, iVar);
        this.h = com.applovin.impl.sdk.utils.i.b(jSONObject, "name", str2, iVar);
        JSONObject b2 = com.applovin.impl.sdk.utils.i.b(jSONObject, "configuration", new JSONObject(), iVar);
        this.j = a(b2, iVar, iVar.D());
        this.k = new d(b2, iVar);
        this.c = p.e(com.applovin.impl.sdk.utils.i.b(jSONObject, "existence_class", str2, iVar));
        List<MaxAdFormat> emptyList = Collections.emptyList();
        MaxAdapter a2 = com.applovin.impl.mediation.d.c.a(com.applovin.impl.sdk.utils.i.b(jSONObject, "adapter_class", str2, iVar), iVar);
        if (a2 != null) {
            this.d = true;
            try {
                String adapterVersion = a2.getAdapterVersion();
                try {
                    String sdkVersion = a2.getSdkVersion();
                    emptyList = a(a2);
                    String str3 = sdkVersion;
                    str2 = adapterVersion;
                    str = str3;
                } catch (Throwable th) {
                    th = th;
                    String str4 = str2;
                    str2 = adapterVersion;
                    str = str4;
                    String str5 = a;
                    o.i(str5, "Failed to load adapter for network " + this.e + ". Please check that you have a compatible network SDK integrated. Error: " + th);
                    this.g = str2;
                    this.f = str;
                    this.i = emptyList;
                    this.b = i();
                }
            } catch (Throwable th2) {
                th = th2;
                str = str2;
                String str52 = a;
                o.i(str52, "Failed to load adapter for network " + this.e + ". Please check that you have a compatible network SDK integrated. Error: " + th);
                this.g = str2;
                this.f = str;
                this.i = emptyList;
                this.b = i();
            }
        } else {
            this.d = false;
            str = str2;
        }
        this.g = str2;
        this.f = str;
        this.i = emptyList;
        this.b = i();
    }

    private List<MaxAdFormat> a(MaxAdapter maxAdapter) {
        ArrayList arrayList = new ArrayList(5);
        if (maxAdapter instanceof MaxInterstitialAdapter) {
            arrayList.add(MaxAdFormat.INTERSTITIAL);
        }
        if (maxAdapter instanceof MaxRewardedAdapter) {
            arrayList.add(MaxAdFormat.REWARDED);
        }
        if (maxAdapter instanceof MaxAdViewAdapter) {
            arrayList.add(MaxAdFormat.BANNER);
            arrayList.add(MaxAdFormat.LEADER);
            arrayList.add(MaxAdFormat.MREC);
        }
        return arrayList;
    }

    private List<e> a(JSONObject jSONObject, i iVar, Context context) {
        ArrayList arrayList = new ArrayList();
        JSONObject b2 = com.applovin.impl.sdk.utils.i.b(jSONObject, NativeProtocol.RESULT_ARGS_PERMISSIONS, new JSONObject(), iVar);
        Iterator<String> keys = b2.keys();
        while (keys.hasNext()) {
            try {
                String next = keys.next();
                arrayList.add(new e(next, b2.getString(next), context));
            } catch (JSONException unused) {
            }
        }
        return arrayList;
    }

    private a i() {
        if (!this.c && !this.d) {
            return a.MISSING;
        }
        for (e c2 : this.j) {
            if (!c2.c()) {
                return a.INVALID_INTEGRATION;
            }
        }
        return (!this.k.a() || this.k.b()) ? (!this.c || !this.d) ? a.INCOMPLETE_INTEGRATION : a.COMPLETE : a.INVALID_INTEGRATION;
    }

    /* renamed from: a */
    public int compareTo(c cVar) {
        return this.e.compareToIgnoreCase(cVar.e);
    }

    public a a() {
        return this.b;
    }

    public boolean b() {
        return this.c;
    }

    public boolean c() {
        return this.d;
    }

    public String d() {
        return this.e;
    }

    public String e() {
        return this.f;
    }

    public String f() {
        return this.g;
    }

    public List<e> g() {
        return this.j;
    }

    public final d h() {
        return this.k;
    }

    public String toString() {
        return "MediatedNetwork{name=" + this.e + ", sdkAvailable=" + this.c + ", sdkVersion=" + this.f + ", adapterAvailable=" + this.d + ", adapterVersion=" + this.g + "}";
    }
}
