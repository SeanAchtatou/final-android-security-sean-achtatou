package com.applovin.impl.mediation.a.c.b;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.applovin.impl.mediation.a.a.c;
import com.applovin.impl.mediation.a.c.b.b;
import com.applovin.sdk.R;

public class a extends Activity {
    private ListView a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.mediation_debugger_detail_activity);
        this.a = (ListView) findViewById(R.id.listView);
    }

    public void setNetwork(c cVar) {
        setTitle(cVar.d());
        b bVar = new b(cVar, this);
        bVar.a(new b.a() {
            public void a(String str) {
                new AlertDialog.Builder(a.this, 16974130).setTitle(R.string.applovin_instructions_dialog_title).setMessage(str).setNegativeButton(17039370, (DialogInterface.OnClickListener) null).create().show();
            }
        });
        this.a.setAdapter((ListAdapter) bVar);
    }
}
