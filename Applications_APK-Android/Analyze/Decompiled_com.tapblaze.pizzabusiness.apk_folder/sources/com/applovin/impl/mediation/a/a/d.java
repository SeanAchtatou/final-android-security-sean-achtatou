package com.applovin.impl.mediation.a.a;

import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.c;
import com.applovin.impl.sdk.utils.h;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public class d {
    private final boolean a;
    private final boolean b;
    private final boolean c;
    private final String d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.i):java.util.List
     arg types: [org.json.JSONObject, java.lang.String, java.util.ArrayList, com.applovin.impl.sdk.i]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.i):float
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.i):long
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.i):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.i):java.util.List */
    public d(JSONObject jSONObject, i iVar) {
        this.a = c.a(iVar.D()).a();
        JSONObject b2 = com.applovin.impl.sdk.utils.i.b(jSONObject, "cleartext_traffic", (JSONObject) null, iVar);
        boolean z = false;
        if (b2 != null) {
            this.b = true;
            this.d = com.applovin.impl.sdk.utils.i.b(b2, "description", "", iVar);
            if (h.a()) {
                this.c = true;
                return;
            }
            List a2 = com.applovin.impl.sdk.utils.i.a(b2, "domains", (List) new ArrayList(), iVar);
            if (a2.size() > 0) {
                Iterator it = a2.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (!h.a((String) it.next())) {
                            break;
                        }
                    } else {
                        z = true;
                        break;
                    }
                }
            }
            this.c = z;
            return;
        }
        this.b = false;
        this.d = "";
        this.c = h.a();
    }

    public boolean a() {
        return this.b;
    }

    public boolean b() {
        return this.c;
    }

    public String c() {
        return this.a ? this.d : "You must include an entry in your AndroidManifest.xml to point to your network_security_config.xml.\n\nFor more information, visit: https://developer.android.com/training/articles/security-config";
    }
}
