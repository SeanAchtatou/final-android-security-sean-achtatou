package com.applovin.impl.mediation;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.b.c;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.mediation.b.f;
import com.applovin.impl.mediation.c.b;
import com.applovin.impl.mediation.c.d;
import com.applovin.impl.mediation.c.g;
import com.applovin.impl.mediation.c.h;
import com.applovin.impl.mediation.f;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;

public class MediationServiceImpl {
    /* access modifiers changed from: private */
    public final i a;
    /* access modifiers changed from: private */
    public final o b;

    private class a implements d, MaxAdViewAdListener, MaxRewardedAdListener {
        private final com.applovin.impl.mediation.b.a b;
        /* access modifiers changed from: private */
        public final MaxAdListener c;

        private a(com.applovin.impl.mediation.b.a aVar, MaxAdListener maxAdListener) {
            this.b = aVar;
            this.c = maxAdListener;
        }

        public void a(MaxAd maxAd, e eVar) {
            MediationServiceImpl.this.b(this.b, eVar, this.c);
            if (maxAd.getFormat() == MaxAdFormat.REWARDED && (maxAd instanceof c)) {
                ((c) maxAd).u();
            }
        }

        public void a(String str, e eVar) {
            MediationServiceImpl.this.a(this.b, eVar, this.c);
        }

        public void onAdClicked(MaxAd maxAd) {
            MediationServiceImpl.this.a.ac().a((com.applovin.impl.mediation.b.a) maxAd, "DID_CLICKED");
            MediationServiceImpl.this.c(this.b);
            j.d(this.c, maxAd);
        }

        public void onAdCollapsed(MaxAd maxAd) {
            j.h(this.c, maxAd);
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i) {
            MediationServiceImpl.this.b(this.b, new e(i), this.c);
        }

        public void onAdDisplayed(MaxAd maxAd) {
            MediationServiceImpl.this.b.b("MediationService", "Scheduling impression for ad via callback...");
            MediationServiceImpl.this.maybeScheduleCallbackAdImpressionPostback(this.b);
            if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
                MediationServiceImpl.this.a.Z().c();
            }
            j.b(this.c, maxAd);
        }

        public void onAdExpanded(MaxAd maxAd) {
            j.g(this.c, maxAd);
        }

        public void onAdHidden(final MaxAd maxAd) {
            MediationServiceImpl.this.a.ac().a((com.applovin.impl.mediation.b.a) maxAd, "DID_HIDE");
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
                        MediationServiceImpl.this.a.Z().d();
                    }
                    j.c(a.this.c, maxAd);
                }
            }, maxAd instanceof e ? ((e) maxAd).I() : 0);
        }

        public void onAdLoadFailed(String str, int i) {
            MediationServiceImpl.this.a(this.b, new e(i), this.c);
        }

        public void onAdLoaded(MaxAd maxAd) {
            MediationServiceImpl.this.b(this.b);
            j.a(this.c, maxAd);
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            j.f(this.c, maxAd);
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            j.e(this.c, maxAd);
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            j.a(this.c, maxAd, maxReward);
            MediationServiceImpl.this.a.K().a(new g((c) maxAd, MediationServiceImpl.this.a), r.a.MEDIATION_REWARD);
        }
    }

    public MediationServiceImpl(i iVar) {
        this.a = iVar;
        this.b = iVar.v();
    }

    private void a(com.applovin.impl.mediation.b.a aVar) {
        o oVar = this.b;
        oVar.b("MediationService", "Firing ad preload postback for " + aVar.z());
        a("mpreload", aVar);
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.b.a aVar, e eVar, MaxAdListener maxAdListener) {
        a(eVar, aVar);
        destroyAd(aVar);
        j.a(maxAdListener, aVar.getAdUnitId(), eVar.getErrorCode());
    }

    private void a(e eVar, com.applovin.impl.mediation.b.a aVar) {
        long f = aVar.f();
        o oVar = this.b;
        oVar.b("MediationService", "Firing ad load failure postback with load time: " + f);
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(f));
        a("mlerr", hashMap, eVar, aVar);
    }

    private void a(String str, e eVar) {
        a(str, Collections.EMPTY_MAP, (e) null, eVar);
    }

    /* access modifiers changed from: private */
    public void a(String str, com.applovin.impl.mediation.b.g gVar) {
        a("serr", Collections.EMPTY_MAP, new e(str), gVar);
    }

    private void a(String str, Map<String, String> map, e eVar) {
        a(str, map, (e) null, eVar);
    }

    private void a(String str, Map<String, String> map, e eVar, e eVar2) {
        HashMap hashMap = new HashMap(map);
        String str2 = "";
        hashMap.put("{PLACEMENT}", eVar2.J() != null ? eVar2.J() : str2);
        if (eVar2 instanceof c) {
            c cVar = (c) eVar2;
            if (cVar.m() != null) {
                str2 = cVar.m();
            }
            hashMap.put("{PUBLISHER_AD_UNIT_ID}", str2);
        }
        this.a.K().a(new d(str, hashMap, eVar, eVar2, this.a), r.a.MEDIATION_POSTBACKS);
    }

    /* access modifiers changed from: private */
    public void b(com.applovin.impl.mediation.b.a aVar) {
        long f = aVar.f();
        o oVar = this.b;
        oVar.b("MediationService", "Firing ad load success postback with load time: " + f);
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(f));
        a("load", hashMap, aVar);
    }

    /* access modifiers changed from: private */
    public void b(com.applovin.impl.mediation.b.a aVar, e eVar, MaxAdListener maxAdListener) {
        this.a.ac().a(aVar, "DID_FAIL_DISPLAY");
        maybeScheduleAdDisplayErrorPostback(eVar, aVar);
        if (aVar.h().compareAndSet(false, true)) {
            j.a(maxAdListener, aVar, eVar.getErrorCode());
        }
    }

    /* access modifiers changed from: private */
    public void c(com.applovin.impl.mediation.b.a aVar) {
        a("mclick", aVar);
    }

    public void collectSignal(MaxAdFormat maxAdFormat, final com.applovin.impl.mediation.b.g gVar, Activity activity, final f.a aVar) {
        String str;
        o oVar;
        String str2;
        StringBuilder sb;
        if (gVar == null) {
            throw new IllegalArgumentException("No spec specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (aVar != null) {
            final i a2 = this.a.w().a(gVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(gVar, maxAdFormat, activity.getApplicationContext());
                a2.a(a3, activity);
                AnonymousClass4 r1 = new MaxSignalCollectionListener() {
                    public void onSignalCollected(String str) {
                        aVar.a(f.a(gVar, a2, str));
                    }

                    public void onSignalCollectionFailed(String str) {
                        MediationServiceImpl.this.a(str, gVar);
                        aVar.a(f.b(gVar, a2, str));
                    }
                };
                if (!gVar.b()) {
                    oVar = this.b;
                    sb = new StringBuilder();
                    str2 = "Collecting signal for adapter: ";
                } else if (this.a.x().a(gVar)) {
                    oVar = this.b;
                    sb = new StringBuilder();
                    str2 = "Collecting signal for now-initialized adapter: ";
                } else {
                    o oVar2 = this.b;
                    oVar2.e("MediationService", "Skip collecting signal for not-initialized adapter: " + a2.b());
                    str = "Adapter not initialized yet";
                }
                sb.append(str2);
                sb.append(a2.b());
                oVar.b("MediationService", sb.toString());
                a2.a(a3, gVar, activity, r1);
                return;
            }
            str = "Could not load adapter";
            aVar.a(f.a(gVar, str));
        } else {
            throw new IllegalArgumentException("No callback specified");
        }
    }

    public void destroyAd(MaxAd maxAd) {
        if (maxAd instanceof com.applovin.impl.mediation.b.a) {
            o oVar = this.b;
            oVar.c("MediationService", "Destroying " + maxAd);
            com.applovin.impl.mediation.b.a aVar = (com.applovin.impl.mediation.b.a) maxAd;
            i c = aVar.c();
            if (c != null) {
                c.g();
                aVar.i();
            }
        }
    }

    public void loadAd(String str, MaxAdFormat maxAdFormat, f fVar, boolean z, Activity activity, MaxAdListener maxAdListener) {
        final MaxAdListener maxAdListener2 = maxAdListener;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAdListener2 != null) {
            if (!this.a.d()) {
                o.h("AppLovinSdk", "Attempted to load ad before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
            }
            this.a.a();
            final c a2 = this.a.B().a(maxAdFormat);
            if (a2 != null) {
                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        maxAdListener2.onAdLoaded(a2);
                    }
                }, a2.k());
            }
            final f fVar2 = fVar;
            final String str2 = str;
            final MaxAdFormat maxAdFormat2 = maxAdFormat;
            final Activity activity2 = activity;
            final MaxAdListener maxAdListener3 = maxAdListener;
            this.a.K().a(new b(maxAdFormat, z, activity, this.a, new b.a() {
                public void a(JSONArray jSONArray) {
                    f fVar = fVar2;
                    if (fVar == null) {
                        fVar = new f.a().a();
                    }
                    MediationServiceImpl.this.a.K().a(new com.applovin.impl.mediation.c.c(str2, maxAdFormat2, fVar, jSONArray, activity2, MediationServiceImpl.this.a, maxAdListener3));
                }
            }), com.applovin.impl.mediation.d.c.a(maxAdFormat));
        } else {
            throw new IllegalArgumentException("No listener specified");
        }
    }

    public void loadThirdPartyMediatedAd(String str, com.applovin.impl.mediation.b.a aVar, Activity activity, MaxAdListener maxAdListener) {
        if (aVar != null) {
            o oVar = this.b;
            oVar.b("MediationService", "Loading " + aVar + "...");
            this.a.ac().a(aVar, "WILL_LOAD");
            a(aVar);
            i a2 = this.a.w().a(aVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(aVar, activity.getApplicationContext());
                a2.a(a3, activity);
                com.applovin.impl.mediation.b.a a4 = aVar.a(a2);
                a2.a(str, a4);
                a4.g();
                a2.a(str, a3, a4, activity, new a(a4, maxAdListener));
                return;
            }
            o oVar2 = this.b;
            oVar2.d("MediationService", "Failed to load " + aVar + ": adapter not loaded");
            a(aVar, new e((int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED), maxAdListener);
            return;
        }
        throw new IllegalArgumentException("No mediated ad specified");
    }

    public void maybeScheduleAdDisplayErrorPostback(e eVar, com.applovin.impl.mediation.b.a aVar) {
        a("mierr", Collections.EMPTY_MAP, eVar, aVar);
    }

    public void maybeScheduleAdapterInitializationPostback(e eVar, long j, MaxAdapter.InitializationStatus initializationStatus, String str) {
        HashMap hashMap = new HashMap(3);
        hashMap.put("{INIT_STATUS}", String.valueOf(initializationStatus.getCode()));
        hashMap.put("{INIT_TIME_MS}", String.valueOf(j));
        a("minit", hashMap, new e(str), eVar);
    }

    public void maybeScheduleCallbackAdImpressionPostback(com.applovin.impl.mediation.b.a aVar) {
        a("mcimp", aVar);
    }

    public void maybeScheduleRawAdImpressionPostback(com.applovin.impl.mediation.b.a aVar) {
        this.a.ac().a(aVar, "WILL_DISPLAY");
        a("mimp", aVar);
    }

    public void maybeScheduleViewabilityAdImpressionPostback(com.applovin.impl.mediation.b.b bVar, long j) {
        HashMap hashMap = new HashMap(1);
        hashMap.put("{VIEWABILITY_FLAGS}", String.valueOf(j));
        hashMap.put("{USED_VIEWABILITY_TIMER}", String.valueOf(bVar.r()));
        a("mvimp", hashMap, bVar);
    }

    public void showFullscreenAd(MaxAd maxAd, String str, final Activity activity) {
        if (maxAd == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAd instanceof c) {
            this.a.Z().a(true);
            final c cVar = (c) maxAd;
            final i c = cVar.c();
            if (c != null) {
                cVar.d(str);
                long G = cVar.G();
                o oVar = this.b;
                oVar.c("MediationService", "Showing ad " + maxAd.getAdUnitId() + " with delay of " + G + "ms...");
                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        if (cVar.getFormat() == MaxAdFormat.REWARDED) {
                            MediationServiceImpl.this.a.K().a(new h(cVar, MediationServiceImpl.this.a), r.a.MEDIATION_REWARD);
                        }
                        c.a(cVar, activity);
                        MediationServiceImpl.this.a.Z().a(false);
                        MediationServiceImpl.this.b.b("MediationService", "Scheduling impression for ad manually...");
                        MediationServiceImpl.this.maybeScheduleRawAdImpressionPostback(cVar);
                    }
                }, G);
                return;
            }
            this.a.Z().a(false);
            o oVar2 = this.b;
            oVar2.d("MediationService", "Failed to show " + maxAd + ": adapter not found");
            o.i("MediationService", "There may be an integration problem with the adapter for ad unit id '" + cVar.getAdUnitId() + "'. Please check if you have a supported version of that SDK integrated into your project.");
            throw new IllegalStateException("Could not find adapter for provided ad");
        } else {
            o.i("MediationService", "Unable to show ad for '" + maxAd.getAdUnitId() + "': only REWARDED or INTERSTITIAL ads are eligible for showFullscreenAd(). " + maxAd.getFormat() + " ad was provided.");
            throw new IllegalArgumentException("Provided ad is not a MediatedFullscreenAd");
        }
    }
}
