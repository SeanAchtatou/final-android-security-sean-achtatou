package com.applovin.impl.mediation.c;

import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.d.z;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import org.json.JSONObject;

public class g extends z {
    private final c a;

    public g(c cVar, i iVar) {
        super("TaskReportMaxReward", iVar);
        this.a = cVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.L;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        a("Failed to report reward for mediated ad: " + this.a + " - error code: " + i);
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "ad_unit_id", this.a.getAdUnitId(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, "placement", this.a.J(), this.b);
        String s = this.a.s();
        if (!m.b(s)) {
            s = "NO_MCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, "mcode", s, this.b);
        String r = this.a.r();
        if (!m.b(r)) {
            r = "NO_BCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, "bcode", r, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/mcr";
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        a("Reported reward successfully for mediated ad: " + this.a);
    }

    /* access modifiers changed from: protected */
    public com.applovin.impl.sdk.a.c c() {
        return this.a.v();
    }

    /* access modifiers changed from: protected */
    public void d() {
        d("No reward result was found for mediated ad: " + this.a);
    }
}
