package com.applovin.impl.mediation;

import android.text.TextUtils;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapters.MediationAdapterBase;
import com.applovin.sdk.AppLovinSdk;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class h {
    private final i a;
    private final o b;
    private final Object c = new Object();
    private final Map<String, Class<? extends MaxAdapter>> d = new HashMap();
    private final Set<String> e = new HashSet();

    public h(i iVar) {
        if (iVar != null) {
            this.a = iVar;
            this.b = iVar.v();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private i a(e eVar, Class<? extends MaxAdapter> cls) {
        try {
            i iVar = new i(eVar, (MediationAdapterBase) cls.getConstructor(AppLovinSdk.class).newInstance(this.a.S()), this.a);
            if (iVar.c()) {
                return iVar;
            }
            o.i("MediationAdapterManager", "Adapter is disabled after initialization: " + eVar);
            return null;
        } catch (Throwable th) {
            o.c("MediationAdapterManager", "Failed to load adapter: " + eVar, th);
            return null;
        }
    }

    private Class<? extends MaxAdapter> a(String str) {
        try {
            Class<?> cls = Class.forName(str);
            if (MaxAdapter.class.isAssignableFrom(cls)) {
                return cls.asSubclass(MaxAdapter.class);
            }
            o.i("MediationAdapterManager", str + " error: not an instance of '" + MaxAdapter.class.getName() + "'.");
            return null;
        } catch (Throwable th) {
            o.c("MediationAdapterManager", "Failed to load: " + str, th);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public i a(e eVar) {
        Class<? extends MaxAdapter> cls;
        o oVar;
        String str;
        if (eVar != null) {
            String z = eVar.z();
            String y = eVar.y();
            if (TextUtils.isEmpty(z)) {
                oVar = this.b;
                str = "No adapter name provided for " + y + ", not loading the adapter ";
            } else if (TextUtils.isEmpty(y)) {
                oVar = this.b;
                str = "Unable to find default classname for '" + z + "'";
            } else {
                synchronized (this.c) {
                    if (!this.e.contains(y)) {
                        if (this.d.containsKey(y)) {
                            cls = this.d.get(y);
                        } else {
                            cls = a(y);
                            if (cls == null) {
                                this.e.add(y);
                                return null;
                            }
                        }
                        i a2 = a(eVar, cls);
                        if (a2 != null) {
                            this.b.b("MediationAdapterManager", "Loaded " + z);
                            this.d.put(y, cls);
                            return a2;
                        }
                        this.b.e("MediationAdapterManager", "Failed to load " + z);
                        this.e.add(y);
                        return null;
                    }
                    this.b.b("MediationAdapterManager", "Not attempting to load " + z + " due to prior errors");
                    return null;
                }
            }
            oVar.e("MediationAdapterManager", str);
            return null;
        }
        throw new IllegalArgumentException("No adapter spec specified");
    }

    public Collection<String> a() {
        Set unmodifiableSet;
        synchronized (this.c) {
            HashSet hashSet = new HashSet(this.d.size());
            for (Class<? extends MaxAdapter> name : this.d.values()) {
                hashSet.add(name.getName());
            }
            unmodifiableSet = Collections.unmodifiableSet(hashSet);
        }
        return unmodifiableSet;
    }

    public Collection<String> b() {
        Set unmodifiableSet;
        synchronized (this.c) {
            unmodifiableSet = Collections.unmodifiableSet(this.e);
        }
        return unmodifiableSet;
    }
}
