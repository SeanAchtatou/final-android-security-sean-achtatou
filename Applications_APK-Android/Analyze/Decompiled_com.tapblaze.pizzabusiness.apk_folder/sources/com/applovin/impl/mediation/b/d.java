package com.applovin.impl.mediation.b;

import com.applovin.impl.mediation.i;
import org.json.JSONObject;

public class d extends a {
    private d(d dVar, i iVar) {
        super(dVar.x(), dVar.w(), iVar, dVar.b);
    }

    public d(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.i iVar) {
        super(jSONObject, jSONObject2, null, iVar);
    }

    public a a(i iVar) {
        return new d(this, iVar);
    }

    public String toString() {
        return "MediatedNativeAd{format=" + getFormat() + ", adUnitId=" + getAdUnitId() + ", isReady=" + a() + ", adapterClass='" + y() + "', adapterName='" + z() + "', isTesting=" + A() + ", isRefreshEnabled=" + E() + ", getAdRefreshMillis=" + F() + '}';
    }
}
