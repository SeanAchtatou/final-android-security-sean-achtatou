package com.applovin.impl.mediation.ads;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.applovin.impl.sdk.d;
import com.applovin.impl.sdk.d.ac;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.u;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.q;
import com.applovin.impl.sdk.v;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.TimeUnit;

public class MaxAdViewImpl extends a implements d.a, v.a {
    private static final int[] q = {10, 14};
    /* access modifiers changed from: private */
    public final Activity a;
    /* access modifiers changed from: private */
    public final MaxAdView b;
    private final View c;
    private long d = Long.MAX_VALUE;
    private com.applovin.impl.mediation.b.b e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public final a g;
    /* access modifiers changed from: private */
    public final c h;
    /* access modifiers changed from: private */
    public final d i;
    /* access modifiers changed from: private */
    public final u j;
    /* access modifiers changed from: private */
    public final v k;
    /* access modifiers changed from: private */
    public final Object l = new Object();
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.b m = null;
    private boolean n;
    private boolean o;
    private boolean p = false;

    private class a extends b {
        private a() {
            super();
        }

        public void onAdLoadFailed(String str, int i) {
            j.a(MaxAdViewImpl.this.adListener, str, i);
            MaxAdViewImpl.this.a(i);
        }

        public void onAdLoaded(MaxAd maxAd) {
            if (maxAd instanceof com.applovin.impl.mediation.b.b) {
                com.applovin.impl.mediation.b.b bVar = (com.applovin.impl.mediation.b.b) maxAd;
                bVar.d(MaxAdViewImpl.this.f);
                MaxAdViewImpl.this.a(bVar);
                if (bVar.E()) {
                    long F = bVar.F();
                    o v = MaxAdViewImpl.this.sdk.v();
                    String str = MaxAdViewImpl.this.tag;
                    v.b(str, "Scheduling banner ad refresh " + F + " milliseconds from now for '" + MaxAdViewImpl.this.adUnitId + "'...");
                    MaxAdViewImpl.this.i.a(F);
                }
                j.a(MaxAdViewImpl.this.adListener, maxAd);
                return;
            }
            o oVar = MaxAdViewImpl.this.logger;
            String str2 = MaxAdViewImpl.this.tag;
            oVar.e(str2, "Not a MediatedAdViewAd received: " + maxAd);
            onAdLoadFailed(MaxAdViewImpl.this.adUnitId, -5201);
        }
    }

    private abstract class b implements MaxAdListener, MaxAdViewAdListener {
        private b() {
        }

        public void onAdClicked(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.d(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdCollapsed(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                if (MaxAdViewImpl.this.m.t()) {
                    MaxAdViewImpl.this.startAutoRefresh();
                }
                j.h(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.a(MaxAdViewImpl.this.adListener, maxAd, i);
            }
        }

        public void onAdDisplayed(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.b(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdExpanded(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                if (MaxAdViewImpl.this.m.t()) {
                    MaxAdViewImpl.this.stopAutoRefresh();
                }
                j.g(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdHidden(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.c(MaxAdViewImpl.this.adListener, maxAd);
            }
        }
    }

    private class c extends b {
        private c() {
            super();
        }

        public void onAdLoadFailed(String str, int i) {
            o oVar = MaxAdViewImpl.this.logger;
            String str2 = MaxAdViewImpl.this.tag;
            oVar.b(str2, "Failed to pre-cache ad for refresh with error code " + i);
            MaxAdViewImpl.this.a(i);
        }

        public void onAdLoaded(MaxAd maxAd) {
            MaxAdViewImpl.this.logger.b(MaxAdViewImpl.this.tag, "Successfully pre-cached ad for refresh");
            MaxAdViewImpl.this.a(maxAd);
        }
    }

    public MaxAdViewImpl(String str, MaxAdView maxAdView, View view, i iVar, Activity activity) {
        super(str, MaxAdFormat.BANNER, "MaxAdView", iVar);
        if (activity != null) {
            this.a = activity;
            this.b = maxAdView;
            this.c = view;
            this.g = new a();
            this.h = new c();
            this.i = new d(iVar, this);
            this.j = new u(maxAdView, iVar);
            this.k = new v(maxAdView, iVar, this);
            o oVar = this.logger;
            String str2 = this.tag;
            oVar.b(str2, "Created new MaxAdView (" + this + ")");
            return;
        }
        throw new IllegalArgumentException("No activity specified");
    }

    /* access modifiers changed from: private */
    public void a() {
        com.applovin.impl.mediation.b.b bVar;
        MaxAdView maxAdView = this.b;
        if (maxAdView != null) {
            com.applovin.impl.sdk.utils.b.a(maxAdView, this.c);
        }
        this.k.a();
        synchronized (this.l) {
            bVar = this.m;
        }
        if (bVar != null) {
            this.sdk.y().destroyAd(bVar);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.sdk.b(com.applovin.impl.sdk.b.b.t).contains(String.valueOf(i2))) {
            o v = this.sdk.v();
            String str = this.tag;
            v.b(str, "Ignoring banner ad refresh for error code '" + i2 + "'...");
            return;
        }
        this.n = true;
        long longValue = ((Long) this.sdk.a(com.applovin.impl.sdk.b.b.s)).longValue();
        if (longValue >= 0) {
            o v2 = this.sdk.v();
            String str2 = this.tag;
            v2.b(str2, "Scheduling failed banner ad refresh " + longValue + " milliseconds from now for '" + this.adUnitId + "'...");
            this.i.a(longValue);
        }
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        if (p.a(j2, ((Long) this.sdk.a(com.applovin.impl.sdk.b.b.C)).longValue())) {
            o oVar = this.logger;
            String str = this.tag;
            oVar.b(str, "Undesired flags matched - current: " + Long.toBinaryString(j2) + ", undesired: " + Long.toBinaryString(j2));
            this.logger.b(this.tag, "Waiting for refresh timer to manually fire request");
            this.n = true;
            return;
        }
        this.logger.b(this.tag, "No undesired viewability flags matched - scheduling viewability");
        this.n = false;
        b();
    }

    /* access modifiers changed from: private */
    public void a(AnimatorListenerAdapter animatorListenerAdapter) {
        com.applovin.impl.mediation.b.b bVar = this.m;
        if (bVar == null || bVar.l() == null) {
            animatorListenerAdapter.onAnimationEnd(null);
            return;
        }
        View l2 = this.m.l();
        l2.animate().alpha(0.0f).setDuration(((Long) this.sdk.a(com.applovin.impl.sdk.b.b.z)).longValue()).setListener(animatorListenerAdapter).start();
    }

    private void a(View view, com.applovin.impl.mediation.b.b bVar) {
        int j2 = bVar.j();
        int k2 = bVar.k();
        int i2 = -1;
        int dpToPx = j2 == -1 ? -1 : AppLovinSdkUtils.dpToPx(view.getContext(), j2);
        if (k2 != -1) {
            i2 = AppLovinSdkUtils.dpToPx(view.getContext(), k2);
        }
        int height = this.b.getHeight();
        int width = this.b.getWidth();
        if ((height > 0 && height < i2) || (width > 0 && width < dpToPx)) {
            int pxToDp = AppLovinSdkUtils.pxToDp(view.getContext(), height);
            o.h("AppLovinSdk", "\n**************************************************\n`MaxAdView` size " + AppLovinSdkUtils.pxToDp(view.getContext(), width) + "x" + pxToDp + " dp smaller than required size: " + j2 + "x" + k2 + " dp\n**************************************************\n");
        }
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new RelativeLayout.LayoutParams(dpToPx, i2);
        } else {
            layoutParams.width = dpToPx;
            layoutParams.height = i2;
        }
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            this.logger.b(this.tag, "Pinning ad view to MAX ad view with width: " + dpToPx + " and height: " + i2 + ".");
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
            for (int addRule : g.c() ? q.a(this.b.getGravity(), 10, 14) : q) {
                layoutParams2.addRule(addRule);
            }
        }
        view.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public void a(final com.applovin.impl.mediation.b.b bVar) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                String str;
                String str2;
                o oVar;
                if (bVar.l() != null) {
                    final MaxAdView d = MaxAdViewImpl.this.b;
                    if (d != null) {
                        MaxAdViewImpl.this.a(new AnimatorListenerAdapter() {
                            public void onAnimationEnd(Animator animator) {
                                super.onAnimationEnd(animator);
                                MaxAdViewImpl.this.a();
                                if (bVar.r()) {
                                    MaxAdViewImpl.this.k.a(MaxAdViewImpl.this.a, bVar);
                                }
                                MaxAdViewImpl.this.a(bVar, d);
                                synchronized (MaxAdViewImpl.this.l) {
                                    com.applovin.impl.mediation.b.b unused = MaxAdViewImpl.this.m = bVar;
                                }
                                MaxAdViewImpl.this.logger.b(MaxAdViewImpl.this.tag, "Scheduling impression for ad manually...");
                                MaxAdViewImpl.this.sdk.y().maybeScheduleRawAdImpressionPostback(bVar);
                                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                                    public void run() {
                                        long a2 = MaxAdViewImpl.this.j.a(bVar);
                                        if (!bVar.r()) {
                                            MaxAdViewImpl.this.a(bVar, a2);
                                        }
                                        MaxAdViewImpl.this.a(a2);
                                    }
                                }, bVar.m());
                            }
                        });
                        return;
                    }
                    oVar = MaxAdViewImpl.this.logger;
                    str2 = MaxAdViewImpl.this.tag;
                    str = "Max ad view does not have a parent View";
                } else {
                    oVar = MaxAdViewImpl.this.logger;
                    str2 = MaxAdViewImpl.this.tag;
                    str = "Max ad does not have a loaded ad view";
                }
                oVar.e(str2, str);
                MaxAdViewImpl.this.g.onAdDisplayFailed(bVar, -5201);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.b.b bVar, long j2) {
        this.logger.b(this.tag, "Scheduling viewability impression for ad...");
        this.sdk.y().maybeScheduleViewabilityAdImpressionPostback(bVar, j2);
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.mediation.b.b bVar, MaxAdView maxAdView) {
        View l2 = bVar.l();
        l2.setAlpha(0.0f);
        if (bVar.u() != Long.MAX_VALUE) {
            this.c.setBackgroundColor((int) bVar.u());
        } else {
            long j2 = this.d;
            if (j2 != Long.MAX_VALUE) {
                this.c.setBackgroundColor((int) j2);
            } else {
                this.c.setBackgroundColor(0);
            }
        }
        maxAdView.addView(l2);
        a(l2, bVar);
        l2.animate().alpha(1.0f).setDuration(((Long) this.sdk.a(com.applovin.impl.sdk.b.b.y)).longValue()).start();
    }

    /* access modifiers changed from: private */
    public void a(MaxAd maxAd) {
        if (this.o) {
            this.o = false;
            o oVar = this.logger;
            String str = this.tag;
            oVar.b(str, "Rendering precache request ad: " + maxAd.getAdUnitId() + "...");
            this.g.onAdLoaded(maxAd);
            return;
        }
        this.e = (com.applovin.impl.mediation.b.b) maxAd;
    }

    /* access modifiers changed from: private */
    public void a(final MaxAdListener maxAdListener) {
        if (d()) {
            o.i(this.tag, "Unable to load new ad; ad is already destroyed");
            j.a(this.adListener, this.adUnitId, -1);
            return;
        }
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (MaxAdViewImpl.this.m != null) {
                    MaxAdViewImpl.this.loadRequestBuilder.a("visible_ad_ad_unit_id", MaxAdViewImpl.this.m.getAdUnitId()).a("viewability_flags", String.valueOf(MaxAdViewImpl.this.j.a(MaxAdViewImpl.this.m)));
                } else {
                    MaxAdViewImpl.this.loadRequestBuilder.a("visible_ad_ad_unit_id").a("viewability_flags");
                }
                o oVar = MaxAdViewImpl.this.logger;
                String str = MaxAdViewImpl.this.tag;
                oVar.b(str, "Loading banner ad for '" + MaxAdViewImpl.this.adUnitId + "' and notifying " + maxAdListener + "...");
                MaxAdViewImpl.this.sdk.y().loadAd(MaxAdViewImpl.this.adUnitId, MaxAdViewImpl.this.adFormat, MaxAdViewImpl.this.loadRequestBuilder.a(), false, MaxAdViewImpl.this.a, maxAdListener);
            }
        });
    }

    private void b() {
        if (c()) {
            long longValue = ((Long) this.sdk.a(com.applovin.impl.sdk.b.b.D)).longValue();
            o oVar = this.logger;
            String str = this.tag;
            oVar.b(str, "Scheduling refresh precache request in " + TimeUnit.MICROSECONDS.toSeconds(longValue) + " seconds...");
            this.sdk.K().a(new ac(this.sdk, new Runnable() {
                public void run() {
                    MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                    maxAdViewImpl.a(maxAdViewImpl.h);
                }
            }), com.applovin.impl.mediation.d.c.a(this.adFormat), longValue);
        }
    }

    private boolean c() {
        return ((Long) this.sdk.a(com.applovin.impl.sdk.b.b.D)).longValue() > 0;
    }

    private boolean d() {
        boolean z;
        synchronized (this.l) {
            z = this.p;
        }
        return z;
    }

    public void destroy() {
        a();
        synchronized (this.l) {
            this.p = true;
        }
        this.i.c();
    }

    public String getPlacement() {
        return this.f;
    }

    public void loadAd() {
        o oVar = this.logger;
        String str = this.tag;
        oVar.b(str, "" + this + " Loading ad for " + this.adUnitId + "...");
        if (d()) {
            o.i(this.tag, "Unable to load new ad; ad is already destroyed");
            j.a(this.adListener, this.adUnitId, -1);
        } else if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.b.b.E)).booleanValue() || !this.i.a()) {
            a(this.g);
        } else {
            String str2 = this.tag;
            o.i(str2, "Unable to load a new ad. An ad refresh has already been scheduled in " + TimeUnit.MILLISECONDS.toSeconds(this.i.b()) + " seconds.");
        }
    }

    public void onAdRefresh() {
        String str;
        String str2;
        o oVar;
        this.o = false;
        if (this.e != null) {
            o oVar2 = this.logger;
            String str3 = this.tag;
            oVar2.b(str3, "Refreshing for cached ad: " + this.e.getAdUnitId() + "...");
            this.g.onAdLoaded(this.e);
            this.e = null;
            return;
        }
        if (!c()) {
            oVar = this.logger;
            str2 = this.tag;
            str = "Refreshing ad from network...";
        } else if (this.n) {
            oVar = this.logger;
            str2 = this.tag;
            str = "Refreshing ad from network due to viewability requirements not met for refresh request...";
        } else {
            this.logger.e(this.tag, "Ignoring attempt to refresh ad - either still waiting for precache or did not attempt request due to visibility requirement not met");
            this.o = true;
            return;
        }
        oVar.b(str2, str);
        loadAd();
    }

    public void onLogVisibilityImpression() {
        a(this.m, this.j.a(this.m));
    }

    public void onWindowVisibilityChanged(int i2) {
        if (((Boolean) this.sdk.a(com.applovin.impl.sdk.b.b.x)).booleanValue() && this.i.a()) {
            if (q.a(i2)) {
                this.logger.b(this.tag, "Ad view visible");
                this.i.g();
                return;
            }
            this.logger.b(this.tag, "Ad view hidden");
            this.i.f();
        }
    }

    public void setPlacement(String str) {
        this.f = str;
    }

    public void setPublisherBackgroundColor(int i2) {
        this.d = (long) i2;
    }

    public void startAutoRefresh() {
        this.i.e();
        o oVar = this.logger;
        String str = this.tag;
        oVar.b(str, "Resumed auto-refresh with remaining time: " + this.i.b());
    }

    public void stopAutoRefresh() {
        if (this.m != null) {
            o oVar = this.logger;
            String str = this.tag;
            oVar.b(str, "Pausing auto-refresh with remaining time: " + this.i.b());
            this.i.d();
            return;
        }
        o.h(this.tag, "Stopping auto-refresh has no effect until after the first ad has been loaded.");
    }

    public String toString() {
        return "MaxAdView{adUnitId='" + this.adUnitId + '\'' + ", adListener=" + this.adListener + ", isDestroyed=" + d() + '}';
    }
}
