package com.applovin.impl.mediation;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.mediation.b.g;
import com.applovin.impl.sdk.d.r;
import com.applovin.impl.sdk.o;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.MaxSignalProvider;
import com.applovin.mediation.adapter.listeners.MaxAdViewAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterSignalCollectionParameters;
import com.applovin.sdk.AppLovinSdkUtils;
import com.ironsource.sdk.constants.Constants;
import java.util.concurrent.atomic.AtomicBoolean;

public class i {
    /* access modifiers changed from: private */
    public final Handler a = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final com.applovin.impl.sdk.i b;
    /* access modifiers changed from: private */
    public final o c;
    private final String d;
    /* access modifiers changed from: private */
    public final e e;
    /* access modifiers changed from: private */
    public final String f;
    /* access modifiers changed from: private */
    public MaxAdapter g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public com.applovin.impl.mediation.b.a i;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public final a k = new a();
    /* access modifiers changed from: private */
    public MaxAdapterResponseParameters l;
    private final AtomicBoolean m = new AtomicBoolean(true);
    /* access modifiers changed from: private */
    public final AtomicBoolean n = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public final AtomicBoolean o = new AtomicBoolean(false);

    private class a implements MaxAdViewAdapterListener, MaxInterstitialAdapterListener, MaxRewardedAdapterListener {
        /* access modifiers changed from: private */
        public d b;

        private a() {
        }

        /* access modifiers changed from: private */
        public void a(d dVar) {
            if (dVar != null) {
                this.b = dVar;
                return;
            }
            throw new IllegalArgumentException("No listener specified");
        }

        private void a(String str) {
            i.this.o.set(true);
            a(str, this.b, new Runnable() {
                public void run() {
                    if (i.this.n.compareAndSet(false, true)) {
                        a.this.b.onAdLoaded(i.this.i);
                    }
                }
            });
        }

        /* access modifiers changed from: private */
        public void a(String str, int i) {
            a(str, new MaxAdapterError(i));
        }

        private void a(final String str, final MaxAdListener maxAdListener, final Runnable runnable) {
            i.this.a.post(new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Exception e) {
                        MaxAdListener maxAdListener = maxAdListener;
                        String name = maxAdListener != null ? maxAdListener.getClass().getName() : null;
                        o e2 = i.this.c;
                        e2.b("MediationAdapterWrapper", "Failed to forward call (" + str + ") to " + name, e);
                    }
                }
            });
        }

        private void a(String str, final MaxAdapterError maxAdapterError) {
            a(str, this.b, new Runnable() {
                public void run() {
                    if (i.this.n.compareAndSet(false, true)) {
                        a.this.b.a(i.this.h, maxAdapterError);
                    }
                }
            });
        }

        private void b(String str) {
            if (i.this.i.h().compareAndSet(false, true)) {
                a(str, this.b, new Runnable() {
                    public void run() {
                        a.this.b.onAdDisplayed(i.this.i);
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public void b(String str, int i) {
            b(str, new MaxAdapterError(i));
        }

        private void b(String str, final MaxAdapterError maxAdapterError) {
            a(str, this.b, new Runnable() {
                public void run() {
                    a.this.b.a(i.this.i, maxAdapterError);
                }
            });
        }

        public void onAdViewAdClicked() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": adview ad clicked");
            a("onAdViewAdClicked", this.b, new Runnable() {
                public void run() {
                    a.this.b.onAdClicked(i.this.i);
                }
            });
        }

        public void onAdViewAdCollapsed() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": adview ad collapsed");
            a("onAdViewAdCollapsed", this.b, new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxAdViewAdListener) {
                        ((MaxAdViewAdListener) a.this.b).onAdCollapsed(i.this.i);
                    }
                }
            });
        }

        public void onAdViewAdDisplayFailed(MaxAdapterError maxAdapterError) {
            o e = i.this.c;
            e.d("MediationAdapterWrapper", i.this.f + ": adview ad failed to display with code: " + maxAdapterError);
            b("onAdViewAdDisplayFailed", maxAdapterError);
        }

        public void onAdViewAdDisplayed() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": adview ad displayed");
            b("onAdViewAdDisplayed");
        }

        public void onAdViewAdExpanded() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": adview ad expanded");
            a("onAdViewAdExpanded", this.b, new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxAdViewAdListener) {
                        ((MaxAdViewAdListener) a.this.b).onAdExpanded(i.this.i);
                    }
                }
            });
        }

        public void onAdViewAdHidden() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": adview ad hidden");
            a("onAdViewAdHidden", this.b, new Runnable() {
                public void run() {
                    a.this.b.onAdHidden(i.this.i);
                }
            });
        }

        public void onAdViewAdLoadFailed(MaxAdapterError maxAdapterError) {
            o e = i.this.c;
            e.d("MediationAdapterWrapper", i.this.f + ": adview ad ad failed to load with code: " + maxAdapterError);
            a("onAdViewAdLoadFailed", maxAdapterError);
        }

        public void onAdViewAdLoaded(View view) {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": adview ad loaded");
            View unused = i.this.j = view;
            a("onAdViewAdLoaded");
        }

        public void onInterstitialAdClicked() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": interstitial ad clicked");
            a(Constants.JSMethods.ON_INTERSTITIAL_AD_CLICKED, this.b, new Runnable() {
                public void run() {
                    a.this.b.onAdClicked(i.this.i);
                }
            });
        }

        public void onInterstitialAdDisplayFailed(MaxAdapterError maxAdapterError) {
            o e = i.this.c;
            e.d("MediationAdapterWrapper", i.this.f + ": interstitial ad failed to display with code " + maxAdapterError);
            b("onInterstitialAdDisplayFailed", maxAdapterError);
        }

        public void onInterstitialAdDisplayed() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": interstitial ad displayed");
            b("onInterstitialAdDisplayed");
        }

        public void onInterstitialAdHidden() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": interstitial ad hidden");
            a("onInterstitialAdHidden", this.b, new Runnable() {
                public void run() {
                    a.this.b.onAdHidden(i.this.i);
                }
            });
        }

        public void onInterstitialAdLoadFailed(MaxAdapterError maxAdapterError) {
            o e = i.this.c;
            e.d("MediationAdapterWrapper", i.this.f + ": interstitial ad failed to load with error " + maxAdapterError);
            a("onInterstitialAdLoadFailed", maxAdapterError);
        }

        public void onInterstitialAdLoaded() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": interstitial ad loaded");
            a("onInterstitialAdLoaded");
        }

        public void onRewardedAdClicked() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": rewarded ad clicked");
            a("onRewardedAdClicked", this.b, new Runnable() {
                public void run() {
                    a.this.b.onAdClicked(i.this.i);
                }
            });
        }

        public void onRewardedAdDisplayFailed(MaxAdapterError maxAdapterError) {
            o e = i.this.c;
            e.d("MediationAdapterWrapper", i.this.f + ": rewarded ad display failed with error: " + maxAdapterError);
            b("onRewardedAdDisplayFailed", maxAdapterError);
        }

        public void onRewardedAdDisplayed() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": rewarded ad displayed");
            b("onRewardedAdDisplayed");
        }

        public void onRewardedAdHidden() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": rewarded ad hidden");
            a("onRewardedAdHidden", this.b, new Runnable() {
                public void run() {
                    a.this.b.onAdHidden(i.this.i);
                }
            });
        }

        public void onRewardedAdLoadFailed(MaxAdapterError maxAdapterError) {
            o e = i.this.c;
            e.d("MediationAdapterWrapper", i.this.f + ": rewarded ad failed to load with code: " + maxAdapterError);
            a("onRewardedAdLoadFailed", maxAdapterError);
        }

        public void onRewardedAdLoaded() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": rewarded ad loaded");
            a("onRewardedAdLoaded");
        }

        public void onRewardedAdVideoCompleted() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": rewarded video completed");
            a("onRewardedAdVideoCompleted", this.b, new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxRewardedAdListener) {
                        ((MaxRewardedAdListener) a.this.b).onRewardedVideoCompleted(i.this.i);
                    }
                }
            });
        }

        public void onRewardedAdVideoStarted() {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": rewarded video started");
            a("onRewardedAdVideoStarted", this.b, new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxRewardedAdListener) {
                        ((MaxRewardedAdListener) a.this.b).onRewardedVideoStarted(i.this.i);
                    }
                }
            });
        }

        public void onUserRewarded(final MaxReward maxReward) {
            o e = i.this.c;
            e.c("MediationAdapterWrapper", i.this.f + ": user was rewarded: " + maxReward);
            a("onUserRewarded", this.b, new Runnable() {
                public void run() {
                    if (a.this.b instanceof MaxRewardedAdListener) {
                        ((MaxRewardedAdListener) a.this.b).onUserRewarded(i.this.i, maxReward);
                    }
                }
            });
        }
    }

    private static class b {
        /* access modifiers changed from: private */
        public final g a;
        /* access modifiers changed from: private */
        public final MaxSignalCollectionListener b;
        /* access modifiers changed from: private */
        public final AtomicBoolean c = new AtomicBoolean();

        b(g gVar, MaxSignalCollectionListener maxSignalCollectionListener) {
            this.a = gVar;
            this.b = maxSignalCollectionListener;
        }
    }

    private class c extends com.applovin.impl.sdk.d.a {
        private c() {
            super("TaskTimeoutMediatedAd", i.this.b);
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.G;
        }

        public void run() {
            if (!i.this.o.get()) {
                d(i.this.f + " is timing out " + i.this.i + "...");
                this.b.A().a(i.this.i);
                i.this.k.a(f(), (int) MaxErrorCodes.MEDIATION_ADAPTER_TIMEOUT);
            }
        }
    }

    private class d extends com.applovin.impl.sdk.d.a {
        private final b c;

        private d(b bVar) {
            super("TaskTimeoutSignalCollection", i.this.b);
            this.c = bVar;
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.H;
        }

        public void run() {
            if (!this.c.c.get()) {
                d(i.this.f + " is timing out " + this.c.a + "...");
                i iVar = i.this;
                iVar.b("The adapter (" + i.this.f + ") timed out", this.c);
            }
        }
    }

    i(e eVar, MaxAdapter maxAdapter, com.applovin.impl.sdk.i iVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("No adapter name specified");
        } else if (maxAdapter == null) {
            throw new IllegalArgumentException("No adapter specified");
        } else if (iVar != null) {
            this.d = eVar.z();
            this.g = maxAdapter;
            this.b = iVar;
            this.c = iVar.v();
            this.e = eVar;
            this.f = maxAdapter.getClass().getSimpleName();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        o oVar = this.c;
        oVar.c("MediationAdapterWrapper", "Marking " + this.f + " as disabled due to: " + str);
        this.m.set(false);
    }

    /* access modifiers changed from: private */
    public void a(String str, b bVar) {
        if (bVar.c.compareAndSet(false, true) && bVar.b != null) {
            bVar.b.onSignalCollected(str);
        }
    }

    private void a(final String str, final Runnable runnable) {
        AnonymousClass3 r0 = new Runnable() {
            public void run() {
                try {
                    o e = i.this.c;
                    e.b("MediationAdapterWrapper", i.this.f + ": running " + str + "...");
                    runnable.run();
                    o e2 = i.this.c;
                    e2.b("MediationAdapterWrapper", i.this.f + ": finished " + str + "");
                } catch (Throwable th) {
                    o e3 = i.this.c;
                    e3.b("MediationAdapterWrapper", "Unable to run adapter operation " + str + ", marking " + i.this.f + " as disabled", th);
                    i iVar = i.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append("fail_");
                    sb.append(str);
                    iVar.a(sb.toString());
                }
            }
        };
        if (this.e.B()) {
            this.a.post(r0);
        } else {
            r0.run();
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, b bVar) {
        if (bVar.c.compareAndSet(false, true) && bVar.b != null) {
            bVar.b.onSignalCollectionFailed(str);
        }
    }

    public View a() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void a(final com.applovin.impl.mediation.b.a aVar, final Activity activity) {
        final Runnable runnable;
        if (aVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (aVar.c() == null) {
            this.k.b("ad_show", -5201);
        } else if (aVar.c() != this) {
            throw new IllegalArgumentException("Mediated ad belongs to a different adapter");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (!this.m.get()) {
            o.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is disabled. Showing ads with this adapter is disabled.");
            this.k.b("ad_show", MaxErrorCodes.MEDIATION_ADAPTER_DISABLED);
        } else if (!d()) {
            o.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' does not have an ad loaded. Please load an ad first");
            this.k.b("ad_show", MaxErrorCodes.MEDIATION_ADAPTER_AD_NOT_READY);
        } else {
            if (aVar.getFormat() == MaxAdFormat.INTERSTITIAL) {
                if (this.g instanceof MaxInterstitialAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxInterstitialAdapter) i.this.g).showInterstitialAd(i.this.l, activity, i.this.k);
                        }
                    };
                } else {
                    o.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an interstitial adapter.");
                    this.k.b("showFullscreenAd", MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                    return;
                }
            } else if (aVar.getFormat() != MaxAdFormat.REWARDED) {
                o.i("MediationAdapterWrapper", "Failed to show " + aVar + ": " + aVar.getFormat() + " is not a supported ad format");
                this.k.b("showFullscreenAd", MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                return;
            } else if (this.g instanceof MaxRewardedAdapter) {
                runnable = new Runnable() {
                    public void run() {
                        ((MaxRewardedAdapter) i.this.g).showRewardedAd(i.this.l, activity, i.this.k);
                    }
                };
            } else {
                o.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an incentivized adapter.");
                this.k.b("showFullscreenAd", MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                return;
            }
            a("ad_render", new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Throwable th) {
                        o e = i.this.c;
                        e.b("MediationAdapterWrapper", "Failed to start displaying ad" + aVar, th);
                        i.this.k.b("ad_render", MaxAdapterError.ERROR_CODE_UNSPECIFIED);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final MaxAdapterInitializationParameters maxAdapterInitializationParameters, final Activity activity) {
        a("initialize", new Runnable() {
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                i.this.g.initialize(maxAdapterInitializationParameters, activity, new MaxAdapter.OnCompletionListener() {
                    public void onCompletion() {
                        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                            public void run() {
                                i.this.b.x().a(i.this.e, SystemClock.elapsedRealtime() - elapsedRealtime, MaxAdapter.InitializationStatus.ADAPTER_NOT_SUPPORTED, null);
                            }
                        }, i.this.e.H());
                    }

                    public void onCompletion(final MaxAdapter.InitializationStatus initializationStatus, final String str) {
                        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                            public void run() {
                                i.this.b.x().a(i.this.e, SystemClock.elapsedRealtime() - elapsedRealtime, initializationStatus, str);
                            }
                        }, i.this.e.H());
                    }
                });
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(MaxAdapterSignalCollectionParameters maxAdapterSignalCollectionParameters, g gVar, Activity activity, MaxSignalCollectionListener maxSignalCollectionListener) {
        if (maxSignalCollectionListener == null) {
            throw new IllegalArgumentException("No callback specified");
        } else if (!this.m.get()) {
            o.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is disabled. Signal collection ads with this adapter is disabled.");
            maxSignalCollectionListener.onSignalCollectionFailed("The adapter (" + this.f + ") is disabled");
        } else {
            final b bVar = new b(gVar, maxSignalCollectionListener);
            MaxAdapter maxAdapter = this.g;
            if (maxAdapter instanceof MaxSignalProvider) {
                final MaxSignalProvider maxSignalProvider = (MaxSignalProvider) maxAdapter;
                final MaxAdapterSignalCollectionParameters maxAdapterSignalCollectionParameters2 = maxAdapterSignalCollectionParameters;
                final Activity activity2 = activity;
                final g gVar2 = gVar;
                a("collect_signal", new Runnable() {
                    public void run() {
                        StringBuilder sb;
                        maxSignalProvider.collectSignal(maxAdapterSignalCollectionParameters2, activity2, new MaxSignalCollectionListener() {
                            public void onSignalCollected(String str) {
                                i.this.a(str, bVar);
                            }

                            public void onSignalCollectionFailed(String str) {
                                i.this.b(str, bVar);
                            }
                        });
                        if (bVar.c.get()) {
                            return;
                        }
                        if (gVar2.D() == 0) {
                            o e2 = i.this.c;
                            e2.b("MediationAdapterWrapper", "Failing signal collection " + gVar2 + " since it has 0 timeout");
                            i iVar = i.this;
                            iVar.b("The adapter (" + i.this.f + ") has 0 timeout", bVar);
                            return;
                        }
                        int i = (gVar2.D() > 0 ? 1 : (gVar2.D() == 0 ? 0 : -1));
                        o e3 = i.this.c;
                        if (i > 0) {
                            sb.append("Setting timeout ");
                            sb.append(gVar2.D());
                            sb.append("ms. for ");
                            sb.append(gVar2);
                            e3.b("MediationAdapterWrapper", sb.toString());
                            i.this.b.K().a(new d(bVar), r.a.MEDIATION_TIMEOUT, gVar2.D());
                            return;
                        }
                        sb = new StringBuilder();
                        sb.append("Negative timeout set for ");
                        sb.append(gVar2);
                        sb.append(", not scheduling a timeout");
                        e3.b("MediationAdapterWrapper", sb.toString());
                    }
                });
                return;
            }
            b("The adapter (" + this.f + ") does not support signal collection", bVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, com.applovin.impl.mediation.b.a aVar) {
        this.h = str;
        this.i = aVar;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, final MaxAdapterResponseParameters maxAdapterResponseParameters, final com.applovin.impl.mediation.b.a aVar, final Activity activity, d dVar) {
        final Runnable runnable;
        if (aVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (!this.m.get()) {
            o.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' was disabled due to earlier failures. Loading ads with this adapter is disabled.");
            dVar.onAdLoadFailed(str, MaxErrorCodes.MEDIATION_ADAPTER_DISABLED);
        } else {
            this.l = maxAdapterResponseParameters;
            this.k.a(dVar);
            if (aVar.getFormat() == MaxAdFormat.INTERSTITIAL) {
                if (this.g instanceof MaxInterstitialAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxInterstitialAdapter) i.this.g).loadInterstitialAd(maxAdapterResponseParameters, activity, i.this.k);
                        }
                    };
                } else {
                    o.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an interstitial adapter.");
                    this.k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                    return;
                }
            } else if (aVar.getFormat() == MaxAdFormat.REWARDED) {
                if (this.g instanceof MaxRewardedAdapter) {
                    runnable = new Runnable() {
                        public void run() {
                            ((MaxRewardedAdapter) i.this.g).loadRewardedAd(maxAdapterResponseParameters, activity, i.this.k);
                        }
                    };
                } else {
                    o.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an incentivized adapter.");
                    this.k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                    return;
                }
            } else if (aVar.getFormat() != MaxAdFormat.BANNER && aVar.getFormat() != MaxAdFormat.LEADER && aVar.getFormat() != MaxAdFormat.MREC) {
                o.i("MediationAdapterWrapper", "Failed to load " + aVar + ": " + aVar.getFormat() + " is not a supported ad format");
                this.k.a("loadAd", (int) MaxErrorCodes.FORMAT_TYPE_NOT_SUPPORTED);
                return;
            } else if (this.g instanceof MaxAdViewAdapter) {
                runnable = new Runnable() {
                    public void run() {
                        ((MaxAdViewAdapter) i.this.g).loadAdViewAd(maxAdapterResponseParameters, aVar.getFormat(), activity, i.this.k);
                    }
                };
            } else {
                o.i("MediationAdapterWrapper", "Mediation adapter '" + this.f + "' is not an adview-based adapter.");
                this.k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                return;
            }
            a("ad_load", new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } catch (Throwable th) {
                        o e = i.this.c;
                        e.b("MediationAdapterWrapper", "Failed start loading " + aVar, th);
                        i.this.k.a("loadAd", -1);
                    }
                    if (!i.this.n.get()) {
                        long D = i.this.e.D();
                        if (D == 0) {
                            o e2 = i.this.c;
                            e2.b("MediationAdapterWrapper", "Failing ad " + aVar + " since it has 0 timeout");
                            i.this.k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_IMMEDIATE_TIMEOUT);
                        } else if (D > 0) {
                            o e3 = i.this.c;
                            e3.b("MediationAdapterWrapper", "Setting timeout " + D + "ms. for " + aVar);
                            i.this.b.K().a(new c(), r.a.MEDIATION_TIMEOUT, D);
                        } else {
                            o e4 = i.this.c;
                            e4.b("MediationAdapterWrapper", "Negative timeout set for " + aVar + ", not scheduling a timeout");
                        }
                    }
                }
            });
        }
    }

    public String b() {
        return this.d;
    }

    public boolean c() {
        return this.m.get();
    }

    public boolean d() {
        return this.n.get() && this.o.get();
    }

    public String e() {
        MaxAdapter maxAdapter = this.g;
        if (maxAdapter == null) {
            return null;
        }
        try {
            return maxAdapter.getSdkVersion();
        } catch (Throwable th) {
            o oVar = this.c;
            oVar.b("MediationAdapterWrapper", "Unable to get adapter's SDK version, marking " + this + " as disabled", th);
            a("fail_version");
            return null;
        }
    }

    public String f() {
        MaxAdapter maxAdapter = this.g;
        if (maxAdapter == null) {
            return null;
        }
        try {
            return maxAdapter.getAdapterVersion();
        } catch (Throwable th) {
            o oVar = this.c;
            oVar.b("MediationAdapterWrapper", "Unable to get adapter version, marking " + this + " as disabled", th);
            a("fail_version");
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        a("destroy", new Runnable() {
            public void run() {
                i.this.a("destroy");
                i.this.g.onDestroy();
                MaxAdapter unused = i.this.g = (MaxAdapter) null;
            }
        });
    }

    public String toString() {
        return "MediationAdapterWrapper{adapterTag='" + this.f + "'" + '}';
    }
}
