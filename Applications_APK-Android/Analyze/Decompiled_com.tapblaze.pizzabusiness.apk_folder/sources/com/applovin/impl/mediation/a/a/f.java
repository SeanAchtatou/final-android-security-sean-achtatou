package com.applovin.impl.mediation.a.a;

import android.text.SpannedString;
import com.applovin.impl.mediation.a.a.b;

public class f extends b {
    public f(String str) {
        super(b.a.SECTION);
        this.b = new SpannedString(str);
    }

    public String toString() {
        return "SectionListItemViewModel{text=" + ((Object) this.b) + "}";
    }
}
