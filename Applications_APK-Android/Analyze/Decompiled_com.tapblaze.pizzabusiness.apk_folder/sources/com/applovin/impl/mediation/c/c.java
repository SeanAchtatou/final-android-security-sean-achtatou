package com.applovin.impl.mediation.c;

import android.app.Activity;
import android.graphics.Point;
import com.applovin.impl.mediation.d.b;
import com.applovin.impl.mediation.f;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.h;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.d.x;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.internal.NativeProtocol;
import com.ironsource.sdk.constants.Constants;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c extends a {
    private final String a;
    private final MaxAdFormat c;
    private final f d;
    private final JSONArray e;
    private final Activity f;
    private final MaxAdListener g;

    public c(String str, MaxAdFormat maxAdFormat, f fVar, JSONArray jSONArray, Activity activity, i iVar, MaxAdListener maxAdListener) {
        super("TaskFetchMediatedAd " + str, iVar);
        this.a = str;
        this.c = maxAdFormat;
        this.d = fVar;
        this.e = jSONArray;
        this.f = activity;
        this.g = maxAdListener;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        boolean z = i != 204;
        o v = this.b.v();
        String f2 = f();
        Boolean valueOf = Boolean.valueOf(z);
        v.a(f2, valueOf, "Unable to fetch " + this.a + " ad: server returned " + i);
        if (i == -800) {
            this.b.L().a(g.n);
        }
        b(i);
    }

    private void a(h hVar) {
        long b = hVar.b(g.c);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - b > TimeUnit.MINUTES.toMillis((long) ((Integer) this.b.a(com.applovin.impl.sdk.b.c.dK)).intValue())) {
            hVar.b(g.c, currentTimeMillis);
            hVar.c(g.d);
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        try {
            com.applovin.impl.sdk.utils.h.d(jSONObject, this.b);
            com.applovin.impl.sdk.utils.h.c(jSONObject, this.b);
            com.applovin.impl.sdk.utils.h.f(jSONObject, this.b);
            b.a(jSONObject, this.b);
            b.b(jSONObject, this.b);
            this.b.K().a(b(jSONObject));
        } catch (Throwable th) {
            a("Unable to process mediated ad response", th);
            b(-800);
        }
    }

    private f b(JSONObject jSONObject) {
        return new f(this.a, this.c, jSONObject, this.f, this.b, this.g);
    }

    private String b() {
        return b.a(this.b);
    }

    private void b(int i) {
        j.a(this.g, this.a, i);
    }

    private String c() {
        return b.b(this.b);
    }

    private void c(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(Constants.ParametersKeys.LOADED, new JSONArray((Collection) this.b.w().a()));
            jSONObject2.put(Constants.ParametersKeys.FAILED, new JSONArray((Collection) this.b.w().b()));
            jSONObject.put("classname_info", jSONObject2);
            jSONObject.put("initialized_adapters", this.b.x().c());
            jSONObject.put("initialized_adapter_classnames", new JSONArray((Collection) this.b.x().b()));
            jSONObject.put("installed_mediation_adapters", com.applovin.impl.mediation.d.c.a(this.b).a());
        } catch (Exception e2) {
            a("Failed to populate adapter classnames", e2);
        }
    }

    private JSONObject d() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        e(jSONObject);
        f(jSONObject);
        d(jSONObject);
        c(jSONObject);
        jSONObject.put("sc", m.d((String) this.b.a(com.applovin.impl.sdk.b.c.V)));
        jSONObject.put("sc2", m.d((String) this.b.a(com.applovin.impl.sdk.b.c.W)));
        jSONObject.put("server_installed_at", m.d((String) this.b.a(com.applovin.impl.sdk.b.c.X)));
        String str = (String) this.b.a(e.x);
        if (m.b(str)) {
            jSONObject.put("persisted_data", m.d(str));
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.ep)).booleanValue()) {
            h(jSONObject);
        }
        jSONObject.put("pnr", Boolean.toString(this.b.h()));
        jSONObject.put("mediation_provider", this.b.n());
        return jSONObject;
    }

    private void d(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = this.e;
        if (jSONArray != null) {
            jSONObject.put("signal_data", jSONArray);
        }
    }

    private void e(JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("ad_unit_id", this.a);
        jSONObject2.put("ad_format", com.applovin.impl.mediation.d.c.b(this.c));
        if (this.d != null && ((Boolean) this.b.a(com.applovin.impl.sdk.b.b.h)).booleanValue()) {
            jSONObject2.put("extra_parameters", com.applovin.impl.sdk.utils.i.a((Map<String, ?>) com.applovin.impl.sdk.utils.i.a(this.d.a())));
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.Y)).booleanValue()) {
            jSONObject2.put("n", String.valueOf(this.b.ab().a(this.a)));
        }
        jSONObject.put("ad_info", jSONObject2);
    }

    private void f(JSONObject jSONObject) throws JSONException {
        com.applovin.impl.sdk.j O = this.b.O();
        j.d b = O.b();
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("brand", b.d);
        jSONObject2.put("brand_name", b.e);
        jSONObject2.put("hardware", b.f);
        jSONObject2.put("api_level", b.h);
        jSONObject2.put("carrier", b.j);
        jSONObject2.put("country_code", b.i);
        jSONObject2.put("locale", b.k);
        jSONObject2.put("model", b.a);
        jSONObject2.put("os", b.b);
        jSONObject2.put("platform", b.c);
        jSONObject2.put("revision", b.g);
        jSONObject2.put("orientation_lock", b.l);
        jSONObject2.put("tz_offset", b.r);
        jSONObject2.put("aida", m.a(b.I));
        jSONObject2.put("wvvc", b.s);
        jSONObject2.put("adns", (double) b.m);
        jSONObject2.put("adnsd", b.n);
        jSONObject2.put("xdpi", (double) b.o);
        jSONObject2.put("ydpi", (double) b.p);
        jSONObject2.put("screen_size_in", b.q);
        jSONObject2.put("sim", m.a(b.x));
        jSONObject2.put("gy", m.a(b.y));
        jSONObject2.put("is_tablet", m.a(b.z));
        jSONObject2.put("tv", m.a(b.A));
        jSONObject2.put("vs", m.a(b.B));
        jSONObject2.put("lpm", b.C);
        jSONObject2.put("fs", b.E);
        jSONObject2.put("fm", b.F.b);
        jSONObject2.put("tm", b.F.a);
        jSONObject2.put("lmt", b.F.c);
        jSONObject2.put("lm", b.F.d);
        jSONObject2.put("adr", m.a(b.t));
        jSONObject2.put("volume", b.v);
        jSONObject2.put("network", com.applovin.impl.sdk.utils.h.f(this.b));
        if (m.b(b.w)) {
            jSONObject2.put("ua", b.w);
        }
        if (m.b(b.D)) {
            jSONObject2.put("so", b.D);
        }
        j.c cVar = b.u;
        if (cVar != null) {
            jSONObject2.put("act", cVar.a);
            jSONObject2.put("acm", cVar.b);
        }
        Boolean bool = b.G;
        if (bool != null) {
            jSONObject2.put("huc", bool.toString());
        }
        Boolean bool2 = b.H;
        if (bool2 != null) {
            jSONObject2.put("aru", bool2.toString());
        }
        Point a2 = com.applovin.impl.sdk.utils.g.a(g());
        jSONObject2.put("dx", Integer.toString(a2.x));
        jSONObject2.put("dy", Integer.toString(a2.y));
        g(jSONObject2);
        jSONObject.put(DeviceRequestsHelper.DEVICE_INFO_PARAM, jSONObject2);
        j.b c2 = O.c();
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("package_name", c2.c);
        jSONObject3.put("installer_name", c2.d);
        jSONObject3.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, c2.a);
        jSONObject3.put("app_version", c2.b);
        jSONObject3.put("installed_at", c2.g);
        jSONObject3.put("tg", c2.e);
        jSONObject3.put("api_did", this.b.a(com.applovin.impl.sdk.b.c.S));
        jSONObject3.put("sdk_version", AppLovinSdk.VERSION);
        jSONObject3.put("build", 131);
        jSONObject3.put("test_ads", this.b.l().isTestAdsEnabled());
        jSONObject3.put("first_install", String.valueOf(this.b.H()));
        jSONObject3.put("first_install_v2", String.valueOf(!this.b.I()));
        jSONObject3.put(Constants.RequestParameters.DEBUG, Boolean.toString(p.b(this.b)));
        String i = this.b.i();
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.dR)).booleanValue() && m.b(i)) {
            jSONObject3.put("cuid", i);
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.dU)).booleanValue()) {
            jSONObject3.put("compass_random_token", this.b.j());
        }
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.dW)).booleanValue()) {
            jSONObject3.put("applovin_random_token", this.b.k());
        }
        String str = (String) this.b.a(com.applovin.impl.sdk.b.c.dY);
        if (m.b(str)) {
            jSONObject3.put("plugin_version", str);
        }
        jSONObject.put("app_info", jSONObject3);
        a.b a3 = this.b.J().a();
        if (a3 != null) {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("lrm_ts_ms", String.valueOf(a3.a()));
            jSONObject4.put("lrm_url", a3.b());
            jSONObject4.put("lrm_ct_ms", String.valueOf(a3.d()));
            jSONObject4.put("lrm_rs", String.valueOf(a3.c()));
            jSONObject.put("connection_info", jSONObject4);
        }
    }

    private void g(JSONObject jSONObject) {
        try {
            j.a d2 = this.b.O().d();
            String str = d2.b;
            if (m.b(str)) {
                jSONObject.put("idfa", str);
            }
            jSONObject.put("dnt", d2.a);
        } catch (Throwable th) {
            a("Failed to populate advertising info", th);
        }
    }

    private void h(JSONObject jSONObject) {
        try {
            h L = this.b.L();
            jSONObject.put("li", String.valueOf(L.b(g.b)));
            jSONObject.put("si", String.valueOf(L.b(g.d)));
            jSONObject.put("pf", String.valueOf(L.b(g.h)));
            jSONObject.put("mpf", String.valueOf(L.b(g.n)));
            jSONObject.put("gpf", String.valueOf(L.b(g.i)));
        } catch (Throwable th) {
            a("Failed to populate ad serving info", th);
        }
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.C;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean
     arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.i):float
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.i):long
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.i):java.util.List
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.i):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean */
    public void run() {
        a("Fetching next ad for ad unit id: " + this.a + " and format: " + this.c);
        if (((Boolean) this.b.a(com.applovin.impl.sdk.b.c.ef)).booleanValue() && p.d()) {
            a("User is connected to a VPN");
        }
        h L = this.b.L();
        L.a(g.m);
        if (L.b(g.c) == 0) {
            L.b(g.c, System.currentTimeMillis());
        }
        try {
            JSONObject d2 = d();
            HashMap hashMap = new HashMap();
            hashMap.put("rid", UUID.randomUUID().toString());
            if (d2.has("huc")) {
                hashMap.put("huc", String.valueOf(com.applovin.impl.sdk.utils.i.a(d2, "huc", (Boolean) false, this.b)));
            }
            if (d2.has("aru")) {
                hashMap.put("aru", String.valueOf(com.applovin.impl.sdk.utils.i.a(d2, "aru", (Boolean) false, this.b)));
            }
            if (!((Boolean) this.b.a(com.applovin.impl.sdk.b.c.eJ)).booleanValue()) {
                hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.b.t());
            }
            a(L);
            AnonymousClass1 r1 = new x<JSONObject>(com.applovin.impl.sdk.network.b.a(this.b).b("POST").a(b()).c(c()).a((Map<String, String>) hashMap).a(d2).a((Object) new JSONObject()).b(((Long) this.b.a(com.applovin.impl.sdk.b.b.f)).intValue()).a(((Integer) this.b.a(com.applovin.impl.sdk.b.c.dz)).intValue()).c(((Long) this.b.a(com.applovin.impl.sdk.b.b.e)).intValue()).b(true).a(), this.b) {
                public void a(int i) {
                    c.this.a(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    if (i == 200) {
                        com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_latency_millis", this.d.a(), this.b);
                        com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_response_size", this.d.b(), this.b);
                        c.this.a(jSONObject);
                        return;
                    }
                    c.this.a(i);
                }
            };
            r1.a(com.applovin.impl.sdk.b.b.c);
            r1.b(com.applovin.impl.sdk.b.b.d);
            this.b.K().a(r1);
        } catch (Throwable th) {
            a("Unable to fetch ad " + this.a, th);
            a(0);
            this.b.M().a(a());
        }
    }
}
