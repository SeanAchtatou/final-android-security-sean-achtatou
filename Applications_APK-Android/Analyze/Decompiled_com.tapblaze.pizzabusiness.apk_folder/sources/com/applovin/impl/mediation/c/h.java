package com.applovin.impl.mediation.c;

import com.applovin.impl.mediation.b.c;
import com.applovin.impl.sdk.d.ae;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.m;
import org.json.JSONObject;

public class h extends ae {
    private final c a;

    public h(c cVar, i iVar) {
        super("TaskValidateMaxReward", iVar);
        this.a = cVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.K;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (!c()) {
            this.a.a(com.applovin.impl.sdk.a.c.a((i < 400 || i >= 500) ? "network_timeout" : "rejected"));
        }
    }

    /* access modifiers changed from: protected */
    public void a(com.applovin.impl.sdk.a.c cVar) {
        if (!c()) {
            this.a.a(cVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.i.a(jSONObject, "ad_unit_id", this.a.getAdUnitId(), this.b);
        com.applovin.impl.sdk.utils.i.a(jSONObject, "placement", this.a.J(), this.b);
        String s = this.a.s();
        if (!m.b(s)) {
            s = "NO_MCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, "mcode", s, this.b);
        String r = this.a.r();
        if (!m.b(r)) {
            r = "NO_BCODE";
        }
        com.applovin.impl.sdk.utils.i.a(jSONObject, "bcode", r, this.b);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "2.0/mvr";
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.a.t();
    }
}
