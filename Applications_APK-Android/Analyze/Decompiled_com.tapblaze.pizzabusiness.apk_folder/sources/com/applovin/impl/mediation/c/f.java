package com.applovin.impl.mediation.c;

import android.app.Activity;
import com.applovin.impl.mediation.d.c;
import com.applovin.impl.sdk.c.g;
import com.applovin.impl.sdk.c.h;
import com.applovin.impl.sdk.utils.d;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.sdk.AppLovinSdkUtils;
import com.ironsource.sdk.precache.DownloadManager;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f extends com.applovin.impl.sdk.d.a {
    /* access modifiers changed from: private */
    public final String a;
    /* access modifiers changed from: private */
    public final MaxAdFormat c;
    /* access modifiers changed from: private */
    public final JSONObject d;
    /* access modifiers changed from: private */
    public final MaxAdListener e;
    /* access modifiers changed from: private */
    public final Activity f;

    private class a extends com.applovin.impl.sdk.d.a {
        private final JSONArray c;
        private final int d;

        a(int i, JSONArray jSONArray) {
            super("TaskProcessNextWaterfallAd", f.this.b);
            if (i < 0 || i >= jSONArray.length()) {
                throw new IllegalArgumentException("Invalid ad index specified: " + i);
            }
            this.c = jSONArray;
            this.d = i;
        }

        private String a(int i) {
            return (i < 0 || i >= this.c.length()) ? "undefined" : i.b(i.a(this.c, i, new JSONObject(), this.b), "type", "undefined", this.b);
        }

        private void b() throws JSONException {
            JSONObject jSONObject = this.c.getJSONObject(this.d);
            String a2 = a(this.d);
            if ("adapter".equalsIgnoreCase(a2)) {
                a("Starting task for adapter ad...");
                e("started to load ad");
                this.b.K().a(new e(f.this.a, jSONObject, f.this.d, this.b, f.this.f, new com.applovin.impl.mediation.d.a(f.this.e, this.b) {
                    public void onAdLoadFailed(String str, int i) {
                        a aVar = a.this;
                        aVar.e("failed to load ad: " + i);
                        a.this.c();
                    }

                    public void onAdLoaded(MaxAd maxAd) {
                        a.this.e("loaded ad");
                        f.this.a(maxAd);
                    }
                }));
                return;
            }
            d("Unable to process ad of unknown type: " + a2);
            f.this.a(-800);
        }

        /* access modifiers changed from: private */
        public void c() {
            if (this.d < this.c.length() - 1) {
                b("Attempting to load next ad (" + this.d + ") after failure...");
                this.b.K().a(new a(this.d + 1, this.c), c.a(f.this.c));
                return;
            }
            f.this.b();
        }

        /* access modifiers changed from: private */
        public void e(String str) {
        }

        public com.applovin.impl.sdk.c.i a() {
            return com.applovin.impl.sdk.c.i.F;
        }

        public void run() {
            try {
                b();
            } catch (Throwable th) {
                a("Encountered error while processing ad number " + this.d, th);
                this.b.M().a(a());
                f.this.b();
            }
        }
    }

    f(String str, MaxAdFormat maxAdFormat, JSONObject jSONObject, Activity activity, com.applovin.impl.sdk.i iVar, MaxAdListener maxAdListener) {
        super("TaskProcessMediationWaterfall " + str, iVar);
        this.a = str;
        this.c = maxAdFormat;
        this.d = jSONObject;
        this.e = maxAdListener;
        this.f = activity;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        h L;
        g gVar;
        if (i == 204) {
            L = this.b.L();
            gVar = g.p;
        } else if (i == -5001) {
            L = this.b.L();
            gVar = g.q;
        } else {
            L = this.b.L();
            gVar = g.r;
        }
        L.a(gVar);
        b("Notifying parent of ad load failure for ad unit " + this.a + ": " + i);
        j.a(this.e, this.a, i);
    }

    /* access modifiers changed from: private */
    public void a(MaxAd maxAd) {
        b("Notifying parent of ad load success for ad unit " + this.a);
        j.a(this.e, maxAd);
    }

    /* access modifiers changed from: private */
    public void b() {
        a((int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED);
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.E;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.i):long
     arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.i):float
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.i):java.util.List
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.i):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.i):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean
     arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.i):float
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.i):long
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.i):java.util.List
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.i):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean */
    public void run() {
        JSONArray optJSONArray = this.d.optJSONArray("ads");
        int length = optJSONArray != null ? optJSONArray.length() : 0;
        if (length > 0) {
            a("Loading the first out of " + length + " ads...");
            this.b.K().a(new a(0, optJSONArray));
            return;
        }
        c("No ads were returned from the server");
        p.a(this.a, this.d, this.b);
        JSONObject b = i.b(this.d, DownloadManager.SETTINGS, new JSONObject(), this.b);
        long a2 = i.a(b, "alfdcs", 0L, this.b);
        if (a2 > 0) {
            long millis = TimeUnit.SECONDS.toMillis(a2);
            AnonymousClass1 r4 = new Runnable() {
                public void run() {
                    f.this.a(204);
                }
            };
            if (i.a(b, "alfdcs_iba", (Boolean) false, this.b).booleanValue()) {
                d.a(millis, this.b, r4);
            } else {
                AppLovinSdkUtils.runOnUiThreadDelayed(r4, millis);
            }
        } else {
            a(204);
        }
    }
}
