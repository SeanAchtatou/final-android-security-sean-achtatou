package com.applovin.impl.mediation;

import android.content.Context;
import android.os.Bundle;
import com.applovin.impl.mediation.b.a;
import com.applovin.impl.mediation.b.e;
import com.applovin.impl.mediation.b.g;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterSignalCollectionParameters;

public class MaxAdapterParametersImpl implements MaxAdapterInitializationParameters, MaxAdapterResponseParameters, MaxAdapterSignalCollectionParameters {
    private Bundle a;
    private boolean b;
    private boolean c;
    private boolean d;
    private String e;
    private String f;
    private MaxAdFormat g;

    private MaxAdapterParametersImpl() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.MaxAdapterParametersImpl.a(com.applovin.impl.mediation.b.e, android.content.Context):com.applovin.impl.mediation.MaxAdapterParametersImpl
     arg types: [com.applovin.impl.mediation.b.a, android.content.Context]
     candidates:
      com.applovin.impl.mediation.MaxAdapterParametersImpl.a(com.applovin.impl.mediation.b.a, android.content.Context):com.applovin.impl.mediation.MaxAdapterParametersImpl
      com.applovin.impl.mediation.MaxAdapterParametersImpl.a(com.applovin.impl.mediation.b.e, android.content.Context):com.applovin.impl.mediation.MaxAdapterParametersImpl */
    static MaxAdapterParametersImpl a(a aVar, Context context) {
        MaxAdapterParametersImpl a2 = a((e) aVar, context);
        a2.e = aVar.e();
        a2.f = aVar.d();
        return a2;
    }

    static MaxAdapterParametersImpl a(e eVar, Context context) {
        MaxAdapterParametersImpl maxAdapterParametersImpl = new MaxAdapterParametersImpl();
        maxAdapterParametersImpl.b = eVar.b(context);
        maxAdapterParametersImpl.c = eVar.a(context);
        maxAdapterParametersImpl.a = eVar.C();
        maxAdapterParametersImpl.d = eVar.A();
        return maxAdapterParametersImpl;
    }

    static MaxAdapterParametersImpl a(g gVar, MaxAdFormat maxAdFormat, Context context) {
        MaxAdapterParametersImpl a2 = a(gVar, context);
        a2.g = maxAdFormat;
        return a2;
    }

    public MaxAdFormat getAdFormat() {
        return this.g;
    }

    public String getBidResponse() {
        return this.f;
    }

    public Bundle getServerParameters() {
        return this.a;
    }

    public String getThirdPartyAdPlacementId() {
        return this.e;
    }

    public boolean hasUserConsent() {
        return this.c;
    }

    public boolean isAgeRestrictedUser() {
        return this.b;
    }

    public boolean isTesting() {
        return this.d;
    }
}
