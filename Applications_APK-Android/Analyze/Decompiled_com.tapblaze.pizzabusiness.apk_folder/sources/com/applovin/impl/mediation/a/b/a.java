package com.applovin.impl.mediation.a.b;

import android.os.Build;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.d.x;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class a extends com.applovin.impl.sdk.d.a {
    /* access modifiers changed from: private */
    public final a.c<JSONObject> a;

    public a(a.c<JSONObject> cVar, i iVar) {
        super("TaskFetchMediationDebuggerInfo", iVar, true);
        this.a = cVar;
    }

    public com.applovin.impl.sdk.c.i a() {
        return com.applovin.impl.sdk.c.i.J;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> b() {
        HashMap hashMap = new HashMap();
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("build", String.valueOf(131));
        if (!((Boolean) this.b.a(c.eJ)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.b.t());
        }
        j.b c = this.b.O().c();
        hashMap.put("package_name", m.d(c.c));
        hashMap.put("app_version", m.d(c.b));
        hashMap.put("platform", "android");
        hashMap.put("os", m.d(Build.VERSION.RELEASE));
        return hashMap;
    }

    public void run() {
        AnonymousClass1 r1 = new x<JSONObject>(b.a(this.b).a(com.applovin.impl.mediation.d.b.c(this.b)).c(com.applovin.impl.mediation.d.b.d(this.b)).a(b()).b("GET").a((Object) new JSONObject()).b(((Long) this.b.a(com.applovin.impl.sdk.b.b.g)).intValue()).a(), this.b, h()) {
            public void a(int i) {
                a.this.a.a(i);
            }

            public void a(JSONObject jSONObject, int i) {
                a.this.a.a(jSONObject, i);
            }
        };
        r1.a(com.applovin.impl.sdk.b.b.c);
        r1.b(com.applovin.impl.sdk.b.b.d);
        this.b.K().a(r1);
    }
}
