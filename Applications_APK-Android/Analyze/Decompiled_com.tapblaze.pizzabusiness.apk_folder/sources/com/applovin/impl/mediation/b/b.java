package com.applovin.impl.mediation.b;

import android.view.View;
import com.applovin.impl.mediation.i;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdkUtils;
import org.json.JSONObject;

public class b extends a {
    private static final int c = AppLovinAdSize.BANNER.getHeight();
    private static final int d = AppLovinAdSize.LEADER.getHeight();

    private b(b bVar, i iVar) {
        super(bVar.x(), bVar.w(), iVar, bVar.b);
    }

    public b(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.i iVar) {
        super(jSONObject, jSONObject2, null, iVar);
    }

    public a a(i iVar) {
        return new b(this, iVar);
    }

    public int j() {
        int a = a("ad_view_width", ((Integer) this.b.a(com.applovin.impl.sdk.b.b.p)).intValue());
        return a == -2 ? AppLovinSdkUtils.isTablet(this.b.D()) ? 728 : 320 : a;
    }

    public int k() {
        int a = a("ad_view_height", ((Integer) this.b.a(com.applovin.impl.sdk.b.b.q)).intValue());
        return a == -2 ? AppLovinSdkUtils.isTablet(this.b.D()) ? d : c : a;
    }

    public View l() {
        if (!a() || this.a == null) {
            return null;
        }
        View a = this.a.a();
        if (a != null) {
            return a;
        }
        throw new IllegalStateException("Ad-view based ad is missing an ad view");
    }

    public long m() {
        return b("viewability_imp_delay_ms", ((Long) this.b.a(c.cd)).longValue());
    }

    public int n() {
        return a("viewability_min_width", ((Integer) this.b.a(getFormat() == MaxAdFormat.BANNER ? c.ce : getFormat() == MaxAdFormat.MREC ? c.cg : c.ci)).intValue());
    }

    public int o() {
        return a("viewability_min_height", ((Integer) this.b.a(getFormat() == MaxAdFormat.BANNER ? c.cf : getFormat() == MaxAdFormat.MREC ? c.ch : c.cj)).intValue());
    }

    public float p() {
        return a("viewability_min_alpha", ((Float) this.b.a(com.applovin.impl.sdk.b.b.ck)).floatValue() / 100.0f);
    }

    public int q() {
        return a("viewability_min_pixels", -1);
    }

    public boolean r() {
        return q() >= 0;
    }

    public long s() {
        return b("viewability_timer_min_visible_ms", ((Long) this.b.a(com.applovin.impl.sdk.b.b.cl)).longValue());
    }

    public boolean t() {
        return b("proe", (Boolean) this.b.a(com.applovin.impl.sdk.b.b.M));
    }

    public long u() {
        return p.f(b("bg_color", (String) null));
    }
}
