package com.applovin.impl.mediation.b;

import com.applovin.impl.sdk.i;
import org.json.JSONObject;

public class g extends e {
    public g(JSONObject jSONObject, JSONObject jSONObject2, i iVar) {
        super(jSONObject, jSONObject2, iVar);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return a("max_signal_length", 2048);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.mediation.b.e.b(java.lang.String, long):long
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.String):java.lang.String
      com.applovin.impl.mediation.b.e.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
      com.applovin.impl.mediation.b.e.b(java.lang.String, java.lang.Boolean):boolean */
    public boolean b() {
        return b("only_collect_signal_when_initialized", (Boolean) false);
    }

    public String toString() {
        return "SignalProviderSpec{specObject=" + x() + '}';
    }
}
