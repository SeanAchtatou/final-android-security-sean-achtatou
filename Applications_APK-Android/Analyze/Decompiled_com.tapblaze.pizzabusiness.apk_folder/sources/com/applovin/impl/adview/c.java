package com.applovin.impl.adview;

import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import com.applovin.impl.a.b;
import com.applovin.impl.a.e;
import com.applovin.impl.sdk.ad.a;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.h;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;

class c extends g {
    /* access modifiers changed from: private */
    public final o a;
    private final i b;
    private d c;
    private AppLovinAd d = null;
    private boolean e = false;

    c(d dVar, i iVar, Context context) {
        super(context);
        if (iVar != null) {
            this.b = iVar;
            this.a = iVar.v();
            setBackgroundColor(0);
            WebSettings settings = getSettings();
            settings.setSupportMultipleWindows(false);
            settings.setJavaScriptEnabled(true);
            setWebViewClient(dVar);
            setWebChromeClient(new b(iVar));
            setVerticalScrollBarEnabled(false);
            setHorizontalScrollBarEnabled(false);
            setScrollBarStyle(33554432);
            if (g.i()) {
                setWebViewRenderProcessClient(new e(iVar));
            }
            setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (view.hasFocus()) {
                        return false;
                    }
                    view.requestFocus();
                    return false;
                }
            });
            setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View view) {
                    c.this.a.b("AdWebView", "Received a LongClick event.");
                    return true;
                }
            });
            return;
        }
        throw new IllegalArgumentException("No sdk specified.");
    }

    private String a(String str, String str2) {
        if (m.b(str)) {
            return p.b(str).replace("{SOURCE}", str2);
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00fe, code lost:
        r1 = r4.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0112, code lost:
        r4 = r4.n();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(final com.applovin.impl.sdk.ad.f r4) {
        /*
            r3 = this;
            com.applovin.impl.sdk.i r0 = r3.b     // Catch:{ all -> 0x0121 }
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r1 = com.applovin.impl.sdk.b.c.eP     // Catch:{ all -> 0x0121 }
            java.lang.Object r0 = r0.a(r1)     // Catch:{ all -> 0x0121 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x0121 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0121 }
            if (r0 != 0) goto L_0x0016
            boolean r0 = r4.ap()     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x001e
        L_0x0016:
            com.applovin.impl.adview.c$13 r0 = new com.applovin.impl.adview.c$13     // Catch:{ all -> 0x0121 }
            r0.<init>()     // Catch:{ all -> 0x0121 }
            r3.a(r0)     // Catch:{ all -> 0x0121 }
        L_0x001e:
            boolean r0 = com.applovin.impl.sdk.utils.g.d()     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x002c
            com.applovin.impl.adview.c$14 r0 = new com.applovin.impl.adview.c$14     // Catch:{ all -> 0x0121 }
            r0.<init>(r4)     // Catch:{ all -> 0x0121 }
            r3.a(r0)     // Catch:{ all -> 0x0121 }
        L_0x002c:
            boolean r0 = com.applovin.impl.sdk.utils.g.e()     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x0040
            boolean r0 = r4.ar()     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x0040
            com.applovin.impl.adview.c$15 r0 = new com.applovin.impl.adview.c$15     // Catch:{ all -> 0x0121 }
            r0.<init>()     // Catch:{ all -> 0x0121 }
            r3.a(r0)     // Catch:{ all -> 0x0121 }
        L_0x0040:
            com.applovin.impl.adview.w r4 = r4.as()     // Catch:{ all -> 0x0121 }
            if (r4 == 0) goto L_0x012b
            android.webkit.WebSettings r0 = r3.getSettings()     // Catch:{ all -> 0x0121 }
            android.webkit.WebSettings$PluginState r1 = r4.b()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x0058
            com.applovin.impl.adview.c$16 r2 = new com.applovin.impl.adview.c$16     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x0058:
            java.lang.Boolean r1 = r4.c()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x0066
            com.applovin.impl.adview.c$17 r2 = new com.applovin.impl.adview.c$17     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x0066:
            java.lang.Boolean r1 = r4.d()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x0074
            com.applovin.impl.adview.c$18 r2 = new com.applovin.impl.adview.c$18     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x0074:
            java.lang.Boolean r1 = r4.e()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x0082
            com.applovin.impl.adview.c$19 r2 = new com.applovin.impl.adview.c$19     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x0082:
            java.lang.Boolean r1 = r4.f()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x0090
            com.applovin.impl.adview.c$2 r2 = new com.applovin.impl.adview.c$2     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x0090:
            java.lang.Boolean r1 = r4.g()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x009e
            com.applovin.impl.adview.c$3 r2 = new com.applovin.impl.adview.c$3     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x009e:
            java.lang.Boolean r1 = r4.h()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x00ac
            com.applovin.impl.adview.c$4 r2 = new com.applovin.impl.adview.c$4     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x00ac:
            java.lang.Boolean r1 = r4.i()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x00ba
            com.applovin.impl.adview.c$5 r2 = new com.applovin.impl.adview.c$5     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x00ba:
            java.lang.Boolean r1 = r4.j()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x00c8
            com.applovin.impl.adview.c$6 r2 = new com.applovin.impl.adview.c$6     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x00c8:
            java.lang.Boolean r1 = r4.k()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x00d6
            com.applovin.impl.adview.c$7 r2 = new com.applovin.impl.adview.c$7     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x00d6:
            boolean r1 = com.applovin.impl.sdk.utils.g.c()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x00f8
            java.lang.Boolean r1 = r4.l()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x00ea
            com.applovin.impl.adview.c$8 r2 = new com.applovin.impl.adview.c$8     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x00ea:
            java.lang.Boolean r1 = r4.m()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x00f8
            com.applovin.impl.adview.c$9 r2 = new com.applovin.impl.adview.c$9     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x00f8:
            boolean r1 = com.applovin.impl.sdk.utils.g.f()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x010c
            java.lang.Integer r1 = r4.a()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x010c
            com.applovin.impl.adview.c$10 r2 = new com.applovin.impl.adview.c$10     // Catch:{ all -> 0x0121 }
            r2.<init>(r0, r1)     // Catch:{ all -> 0x0121 }
            r3.a(r2)     // Catch:{ all -> 0x0121 }
        L_0x010c:
            boolean r1 = com.applovin.impl.sdk.utils.g.g()     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x012b
            java.lang.Boolean r4 = r4.n()     // Catch:{ all -> 0x0121 }
            if (r4 == 0) goto L_0x012b
            com.applovin.impl.adview.c$11 r1 = new com.applovin.impl.adview.c$11     // Catch:{ all -> 0x0121 }
            r1.<init>(r0, r4)     // Catch:{ all -> 0x0121 }
            r3.a(r1)     // Catch:{ all -> 0x0121 }
            goto L_0x012b
        L_0x0121:
            r4 = move-exception
            com.applovin.impl.sdk.o r0 = r3.a
            java.lang.String r1 = "AdWebView"
            java.lang.String r2 = "Unable to apply WebView settings"
            r0.b(r1, r2, r4)
        L_0x012b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.c.a(com.applovin.impl.sdk.ad.f):void");
    }

    private void a(Runnable runnable) {
        try {
            runnable.run();
        } catch (Throwable th) {
            this.a.b("AdWebView", "Unable to apply WebView setting", th);
        }
    }

    private void a(String str, String str2, String str3, i iVar) {
        String a2 = a(str3, str);
        if (m.b(a2)) {
            o oVar = this.a;
            oVar.b("AdWebView", "Rendering webview for VAST ad with resourceContents : " + a2);
            loadDataWithBaseURL(str2, a2, "text/html", null, "");
            return;
        }
        String a3 = a((String) iVar.a(com.applovin.impl.sdk.b.c.ez), str);
        if (m.b(a3)) {
            o oVar2 = this.a;
            oVar2.b("AdWebView", "Rendering webview for VAST ad with resourceContents : " + a3);
            loadDataWithBaseURL(str2, a3, "text/html", null, "");
            return;
        }
        o oVar3 = this.a;
        oVar3.b("AdWebView", "Rendering webview for VAST ad with resourceURL : " + str);
        loadUrl(str);
    }

    /* access modifiers changed from: package-private */
    public AppLovinAd a() {
        return this.d;
    }

    public void a(d dVar) {
        this.c = dVar;
    }

    public void a(AppLovinAd appLovinAd) {
        o oVar;
        String str;
        o oVar2;
        String str2;
        String str3;
        String aq;
        String str4;
        String str5;
        String str6;
        String aq2;
        i iVar;
        if (!this.e) {
            this.d = appLovinAd;
            try {
                if (appLovinAd instanceof h) {
                    loadDataWithBaseURL("/", ((h) appLovinAd).a(), "text/html", null, "");
                    oVar = this.a;
                    str = "Empty ad rendered";
                } else {
                    f fVar = (f) appLovinAd;
                    a(fVar);
                    if (fVar.af()) {
                        setVisibility(0);
                    }
                    if (appLovinAd instanceof a) {
                        loadDataWithBaseURL(fVar.aq(), p.b(((a) appLovinAd).a()), "text/html", null, "");
                        oVar = this.a;
                        str = "AppLovinAd rendered";
                    } else if (appLovinAd instanceof com.applovin.impl.a.a) {
                        com.applovin.impl.a.a aVar = (com.applovin.impl.a.a) appLovinAd;
                        b j = aVar.j();
                        if (j != null) {
                            e b2 = j.b();
                            Uri b3 = b2.b();
                            String uri = b3 != null ? b3.toString() : "";
                            String c2 = b2.c();
                            String aD = aVar.aD();
                            if (!m.b(uri)) {
                                if (!m.b(c2)) {
                                    oVar2 = this.a;
                                    str2 = "Unable to load companion ad. No resources provided.";
                                    oVar2.e("AdWebView", str2);
                                    return;
                                }
                            }
                            if (b2.a() == e.a.STATIC) {
                                this.a.b("AdWebView", "Rendering WebView for static VAST ad");
                                loadDataWithBaseURL(fVar.aq(), a((String) this.b.a(com.applovin.impl.sdk.b.c.ey), uri), "text/html", null, "");
                                return;
                            }
                            if (b2.a() == e.a.HTML) {
                                if (m.b(c2)) {
                                    String a2 = a(aD, c2);
                                    str3 = m.b(a2) ? a2 : c2;
                                    o oVar3 = this.a;
                                    oVar3.b("AdWebView", "Rendering WebView for HTML VAST ad with resourceContents: " + str3);
                                    aq = fVar.aq();
                                    str4 = "text/html";
                                    str5 = null;
                                    str6 = "";
                                } else if (m.b(uri)) {
                                    this.a.b("AdWebView", "Preparing to load HTML VAST ad resourceUri");
                                    aq2 = fVar.aq();
                                    iVar = this.b;
                                    a(uri, aq2, aD, iVar);
                                    return;
                                } else {
                                    return;
                                }
                            } else if (b2.a() != e.a.IFRAME) {
                                oVar2 = this.a;
                                str2 = "Failed to render VAST companion ad of invalid type";
                                oVar2.e("AdWebView", str2);
                                return;
                            } else if (m.b(uri)) {
                                this.a.b("AdWebView", "Preparing to load iFrame VAST ad resourceUri");
                                aq2 = fVar.aq();
                                iVar = this.b;
                                a(uri, aq2, aD, iVar);
                                return;
                            } else if (m.b(c2)) {
                                String a3 = a(aD, c2);
                                str3 = m.b(a3) ? a3 : c2;
                                o oVar4 = this.a;
                                oVar4.b("AdWebView", "Rendering WebView for iFrame VAST ad with resourceContents: " + str3);
                                aq = fVar.aq();
                                str4 = "text/html";
                                str5 = null;
                                str6 = "";
                            } else {
                                return;
                            }
                            loadDataWithBaseURL(aq, str3, str4, str5, str6);
                            return;
                        }
                        oVar = this.a;
                        str = "No companion ad provided.";
                    } else {
                        return;
                    }
                }
                oVar.b("AdWebView", str);
            } catch (Throwable th) {
                this.a.b("AdWebView", "Unable to render AppLovinAd", th);
            }
        } else {
            o.i("AdWebView", "Ad can not be loaded in a destroyed webview");
        }
    }

    public void a(String str) {
        a(str, (Runnable) null);
    }

    public void a(String str, Runnable runnable) {
        try {
            o oVar = this.a;
            oVar.b("AdWebView", "Forwarding \"" + str + "\" to ad template");
            loadUrl(str);
        } catch (Throwable th) {
            this.a.b("AdWebView", "Unable to forward to template", th);
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    public d b() {
        return this.c;
    }

    public void computeScroll() {
    }

    public void destroy() {
        this.e = true;
        try {
            super.destroy();
            this.a.b("AdWebView", "Web view destroyed");
        } catch (Throwable th) {
            o oVar = this.a;
            if (oVar != null) {
                oVar.b("AdWebView", "destroy() threw exception", th);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        try {
            super.onFocusChanged(z, i, rect);
        } catch (Exception e2) {
            this.a.b("AdWebView", "onFocusChanged() threw exception", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
    }

    public void onWindowFocusChanged(boolean z) {
        try {
            super.onWindowFocusChanged(z);
        } catch (Exception e2) {
            this.a.b("AdWebView", "onWindowFocusChanged() threw exception", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        try {
            super.onWindowVisibilityChanged(i);
        } catch (Exception e2) {
            this.a.b("AdWebView", "onWindowVisibilityChanged() threw exception", e2);
        }
    }

    public boolean requestFocus(int i, Rect rect) {
        try {
            return super.requestFocus(i, rect);
        } catch (Exception e2) {
            this.a.b("AdWebView", "requestFocus() threw exception", e2);
            return false;
        }
    }

    public void scrollTo(int i, int i2) {
    }
}
