package com.applovin.impl.adview;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.applovin.impl.adview.AppLovinTouchToClickListener;
import com.applovin.impl.adview.i;
import com.applovin.impl.adview.p;
import com.applovin.impl.adview.v;
import com.applovin.impl.sdk.a.b;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.h;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.b.e;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.a;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkUtils;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class m extends Activity implements j {
    public static final String KEY_WRAPPER_ID = "com.applovin.interstitial.wrapper_id";
    public static volatile n lastKnownWrapper;
    /* access modifiers changed from: private */
    public final Handler A = new Handler(Looper.getMainLooper());
    private FrameLayout B;
    /* access modifiers changed from: private */
    public h C;
    /* access modifiers changed from: private */
    public View D;
    /* access modifiers changed from: private */
    public h E;
    /* access modifiers changed from: private */
    public View F;
    /* access modifiers changed from: private */
    public f G;
    private ImageView H;
    /* access modifiers changed from: private */
    public WeakReference<MediaPlayer> I = new WeakReference<>(null);
    private b J;
    /* access modifiers changed from: private */
    public u K;
    /* access modifiers changed from: private */
    public ProgressBar L;
    private v.a M;
    private a N;
    private n O;
    private a P;
    private l a;
    /* access modifiers changed from: private */
    public n b;
    private volatile boolean c = false;
    protected int computedLengthSeconds = 0;
    protected i countdownManager;
    public volatile f currentAd;
    /* access modifiers changed from: private */
    public d d;
    private volatile boolean e = false;
    /* access modifiers changed from: private */
    public volatile boolean f = false;
    /* access modifiers changed from: private */
    public volatile boolean g = false;
    private volatile boolean h = false;
    private volatile boolean i = false;
    /* access modifiers changed from: private */
    public volatile boolean j = false;
    /* access modifiers changed from: private */
    public volatile boolean k = false;
    private boolean l = false;
    public o logger;
    private volatile boolean m = false;
    private boolean n = true;
    /* access modifiers changed from: private */
    public boolean o = false;
    private long p = 0;
    protected volatile boolean poststitialWasDisplayed = false;
    /* access modifiers changed from: private */
    public long q = 0;
    /* access modifiers changed from: private */
    public long r = 0;
    /* access modifiers changed from: private */
    public long s = 0;
    public i sdk;
    /* access modifiers changed from: private */
    public long t = -2;
    private int u = 0;
    private int v = Integer.MIN_VALUE;
    protected volatile boolean videoMuted = false;
    public t videoView;
    private AtomicBoolean w = new AtomicBoolean(false);
    private AtomicBoolean x = new AtomicBoolean(false);
    private AtomicBoolean y = new AtomicBoolean(false);
    private final Handler z = new Handler(Looper.getMainLooper());

    /* access modifiers changed from: private */
    public void A() {
        if (C()) {
            M();
            pauseReportRewardTask();
            this.logger.b("InterActivity", "Prompting incentivized ad close warning");
            this.J.b();
            return;
        }
        skipVideo();
    }

    /* access modifiers changed from: private */
    public void B() {
        c adWebView;
        if (this.currentAd.W() && (adWebView = ((AdViewControllerImpl) this.a.getAdViewController()).getAdWebView()) != null) {
            adWebView.a("javascript:al_onCloseButtonTapped();");
        }
        if (D()) {
            this.logger.b("InterActivity", "Prompting incentivized non-video ad close warning");
            this.J.c();
            return;
        }
        dismiss();
    }

    private boolean C() {
        return G() && !isFullyWatched() && ((Boolean) this.sdk.a(c.bM)).booleanValue() && this.J != null;
    }

    private boolean D() {
        return H() && !F() && ((Boolean) this.sdk.a(c.bR)).booleanValue() && this.J != null;
    }

    private int E() {
        if (!(this.currentAd instanceof com.applovin.impl.sdk.ad.a)) {
            return 0;
        }
        float h2 = ((com.applovin.impl.sdk.ad.a) this.currentAd).h();
        if (h2 <= 0.0f) {
            h2 = this.currentAd.n();
        }
        double a2 = p.a(System.currentTimeMillis() - this.p);
        double d2 = (double) h2;
        Double.isNaN(d2);
        return (int) Math.min((a2 / d2) * 100.0d, 100.0d);
    }

    private boolean F() {
        return E() >= this.currentAd.T();
    }

    private boolean G() {
        return AppLovinAdType.INCENTIVIZED.equals(this.currentAd.getType());
    }

    private boolean H() {
        return !this.currentAd.hasVideoUrl() && G();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0066, code lost:
        if (r0 > 0) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0098, code lost:
        if (r0 > 0) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009a, code lost:
        r0 = java.util.concurrent.TimeUnit.SECONDS.toMillis((long) r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void I() {
        /*
            r7 = this;
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            if (r0 == 0) goto L_0x00e6
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            long r0 = r0.ag()
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x0018
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            int r0 = r0.ah()
            if (r0 < 0) goto L_0x00e6
        L_0x0018:
            com.applovin.impl.sdk.utils.n r0 = r7.O
            if (r0 != 0) goto L_0x00e6
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            long r0 = r0.ag()
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 < 0) goto L_0x002e
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            long r0 = r0.ag()
            goto L_0x00b6
        L_0x002e:
            boolean r0 = r7.isVastAd()
            if (r0 == 0) goto L_0x0069
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            com.applovin.impl.a.a r0 = (com.applovin.impl.a.a) r0
            com.applovin.impl.a.j r1 = r0.h()
            if (r1 == 0) goto L_0x0051
            int r4 = r1.b()
            if (r4 <= 0) goto L_0x0051
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS
            int r1 = r1.b()
            long r5 = (long) r1
            long r4 = r4.toMillis(r5)
            long r2 = r2 + r4
            goto L_0x005b
        L_0x0051:
            com.applovin.impl.adview.t r1 = r7.videoView
            int r1 = r1.getDuration()
            if (r1 <= 0) goto L_0x005b
            long r4 = (long) r1
            long r2 = r2 + r4
        L_0x005b:
            boolean r1 = r0.ai()
            if (r1 == 0) goto L_0x00a2
            float r0 = r0.n()
            int r0 = (int) r0
            if (r0 <= 0) goto L_0x00a2
            goto L_0x009a
        L_0x0069:
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            boolean r0 = r0 instanceof com.applovin.impl.sdk.ad.a
            if (r0 == 0) goto L_0x00a2
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            com.applovin.impl.sdk.ad.a r0 = (com.applovin.impl.sdk.ad.a) r0
            com.applovin.impl.adview.t r1 = r7.videoView
            int r1 = r1.getDuration()
            if (r1 <= 0) goto L_0x007d
            long r4 = (long) r1
            long r2 = r2 + r4
        L_0x007d:
            boolean r1 = r0.ai()
            if (r1 == 0) goto L_0x00a2
            float r1 = r0.h()
            int r1 = (int) r1
            if (r1 <= 0) goto L_0x0093
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS
            long r4 = (long) r1
            long r0 = r0.toMillis(r4)
        L_0x0091:
            long r2 = r2 + r0
            goto L_0x00a2
        L_0x0093:
            float r0 = r0.n()
            int r0 = (int) r0
            if (r0 <= 0) goto L_0x00a2
        L_0x009a:
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS
            long r4 = (long) r0
            long r0 = r1.toMillis(r4)
            goto L_0x0091
        L_0x00a2:
            double r0 = (double) r2
            com.applovin.impl.sdk.ad.f r2 = r7.currentAd
            int r2 = r2.ah()
            double r2 = (double) r2
            r4 = 4636737291354636288(0x4059000000000000, double:100.0)
            java.lang.Double.isNaN(r2)
            double r2 = r2 / r4
            java.lang.Double.isNaN(r0)
            double r0 = r0 * r2
            long r0 = (long) r0
        L_0x00b6:
            com.applovin.impl.sdk.o r2 = r7.logger
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Scheduling report reward in "
            r3.append(r4)
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r4 = r4.toSeconds(r0)
            r3.append(r4)
            java.lang.String r4 = " seconds..."
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "InterActivity"
            r2.b(r4, r3)
            com.applovin.impl.sdk.i r2 = r7.sdk
            com.applovin.impl.adview.m$16 r3 = new com.applovin.impl.adview.m$16
            r3.<init>()
            com.applovin.impl.sdk.utils.n r0 = com.applovin.impl.sdk.utils.n.a(r0, r2, r3)
            r7.O = r0
        L_0x00e6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.I():void");
    }

    private void J() {
        View view;
        String str;
        StringBuilder sb;
        o oVar;
        l lVar = this.a;
        if (lVar != null) {
            lVar.setAdDisplayListener(new AppLovinAdDisplayListener() {
                public void adDisplayed(AppLovinAd appLovinAd) {
                    if (!m.this.f) {
                        m.this.a(appLovinAd);
                    }
                }

                public void adHidden(AppLovinAd appLovinAd) {
                    m.this.b(appLovinAd);
                }
            });
            this.a.setAdClickListener(new AppLovinAdClickListener() {
                public void adClicked(AppLovinAd appLovinAd) {
                    j.a(m.this.b.e(), appLovinAd);
                }
            });
            this.currentAd = (f) this.b.b();
            if (this.x.compareAndSet(false, true)) {
                this.sdk.o().trackImpression(this.currentAd);
                this.currentAd.setHasShown(true);
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            this.B = new FrameLayout(this);
            this.B.setLayoutParams(layoutParams);
            this.B.setBackgroundColor(this.currentAd.C());
            this.countdownManager = new i(this.z, this.sdk);
            j();
            if (this.currentAd.isVideoAd()) {
                this.m = this.currentAd.b();
                if (this.m) {
                    oVar = this.logger;
                    sb = new StringBuilder();
                    str = "Preparing stream for ";
                } else {
                    oVar = this.logger;
                    sb = new StringBuilder();
                    str = "Preparing cached video playback for ";
                }
                sb.append(str);
                sb.append(this.currentAd.d());
                oVar.b("InterActivity", sb.toString());
                d dVar = this.d;
                if (dVar != null) {
                    dVar.b(this.m ? 1 : 0);
                }
            }
            this.videoMuted = i();
            Uri d2 = this.currentAd.d();
            a(d2);
            if (d2 == null) {
                I();
            }
            this.C.bringToFront();
            if (n() && (view = this.D) != null) {
                view.bringToFront();
            }
            h hVar = this.E;
            if (hVar != null) {
                hVar.bringToFront();
            }
            this.a.renderAd(this.currentAd);
            this.b.a(true);
            if (!this.currentAd.hasVideoUrl()) {
                if (H() && ((Boolean) this.sdk.a(c.bW)).booleanValue()) {
                    d(this.currentAd);
                }
                showPoststitial();
                return;
            }
            return;
        }
        exitWithError("AdView was null");
    }

    private void K() {
        if (this.videoView != null) {
            this.u = getVideoPercentViewed();
            this.videoView.stopPlayback();
        }
    }

    private boolean L() {
        return this.videoMuted;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.i.a(com.applovin.impl.sdk.b.e, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.e<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.i.a(java.lang.String, com.applovin.impl.sdk.b.c):com.applovin.impl.sdk.b.c<ST>
      com.applovin.impl.sdk.i.a(com.applovin.impl.sdk.b.e, java.lang.Object):void */
    private void M() {
        t tVar = this.videoView;
        this.sdk.a(e.t, Integer.valueOf(tVar != null ? tVar.getCurrentPosition() : 0));
        this.sdk.a((e) e.u, (Object) true);
        try {
            this.countdownManager.c();
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Unable to pause countdown timers", th);
        }
        this.videoView.pause();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private void N() {
        long max = Math.max(0L, ((Long) this.sdk.a(c.dj)).longValue());
        if (max > 0) {
            o v2 = this.sdk.v();
            v2.b("InterActivity", "Resuming video with delay of " + max);
            this.A.postDelayed(new Runnable() {
                public void run() {
                    m.this.O();
                }
            }, max);
            return;
        }
        this.sdk.v().b("InterActivity", "Resuming video immediately");
        O();
    }

    /* access modifiers changed from: private */
    public void O() {
        t tVar;
        if (!this.poststitialWasDisplayed && (tVar = this.videoView) != null && !tVar.isPlaying()) {
            this.videoView.seekTo(((Integer) this.sdk.b(e.t, Integer.valueOf(this.videoView.getDuration()))).intValue());
            this.videoView.start();
            this.countdownManager.a();
        }
    }

    private void P() {
        if (!this.i) {
            try {
                int videoPercentViewed = getVideoPercentViewed();
                if (this.currentAd.hasVideoUrl()) {
                    a(this.currentAd, (double) videoPercentViewed, isFullyWatched());
                    if (this.d != null) {
                        this.d.c((long) videoPercentViewed);
                    }
                } else if ((this.currentAd instanceof com.applovin.impl.sdk.ad.a) && H() && ((Boolean) this.sdk.a(c.bW)).booleanValue()) {
                    int E2 = E();
                    o oVar = this.logger;
                    oVar.b("InterActivity", "Rewarded playable engaged at " + E2 + " percent");
                    a(this.currentAd, (double) E2, E2 >= this.currentAd.T());
                }
                this.sdk.o().trackVideoEnd(this.currentAd, TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - this.p), videoPercentViewed, this.m);
                this.sdk.o().trackFullScreenAdClosed(this.currentAd, SystemClock.elapsedRealtime() - this.r, this.t);
            } catch (Throwable th) {
                o oVar2 = this.logger;
                if (oVar2 != null) {
                    oVar2.b("InterActivity", "Failed to notify end listener.", th);
                }
            }
        }
    }

    private int a(int i2) {
        return AppLovinSdkUtils.dpToPx(this, i2);
    }

    private int a(int i2, boolean z2) {
        if (z2) {
            if (i2 == 0) {
                return 0;
            }
            if (i2 == 1) {
                return 9;
            }
            if (i2 == 2) {
                return 8;
            }
            return i2 == 3 ? 1 : -1;
        } else if (i2 == 0) {
            return 1;
        } else {
            if (i2 == 1) {
                return 0;
            }
            if (i2 == 2) {
                return 9;
            }
            return i2 == 3 ? 8 : -1;
        }
    }

    private void a(long j2, final h hVar) {
        this.A.postDelayed(new Runnable() {
            public void run() {
                if (hVar.equals(m.this.C)) {
                    m.this.m();
                } else if (hVar.equals(m.this.E)) {
                    m.this.o();
                }
            }
        }, j2);
    }

    /* access modifiers changed from: private */
    public void a(PointF pointF) {
        if (!this.currentAd.u() || this.currentAd.g() == null) {
            e();
            f();
            return;
        }
        this.sdk.v().b("InterActivity", "Clicking through video...");
        clickThroughFromVideo(pointF);
    }

    private void a(Uri uri) {
        this.videoView = this.currentAd.av() ? new p(this.sdk, this, new p.a() {
            public void a(String str) {
                m.this.handleMediaError(str);
            }
        }) : new AppLovinVideoView(this, this.sdk);
        if (uri != null) {
            this.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mediaPlayer) {
                    WeakReference unused = m.this.I = new WeakReference(mediaPlayer);
                    boolean c = m.this.i();
                    float f = c ^ true ? 1.0f : 0.0f;
                    mediaPlayer.setVolume(f, f);
                    if (m.this.d != null) {
                        m.this.d.e(c ? 1 : 0);
                    }
                    int videoWidth = mediaPlayer.getVideoWidth();
                    int videoHeight = mediaPlayer.getVideoHeight();
                    m.this.computedLengthSeconds = (int) TimeUnit.MILLISECONDS.toSeconds((long) mediaPlayer.getDuration());
                    m.this.videoView.setVideoSize(videoWidth, videoHeight);
                    if (m.this.videoView instanceof AppLovinVideoView) {
                        SurfaceHolder holder = ((AppLovinVideoView) m.this.videoView).getHolder();
                        if (holder.getSurface() != null) {
                            mediaPlayer.setDisplay(holder);
                        }
                    }
                    mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        public boolean onError(MediaPlayer mediaPlayer, final int i, final int i2) {
                            m.this.A.post(new Runnable() {
                                public void run() {
                                    m mVar = m.this;
                                    mVar.handleMediaError("Media player error (" + i + "," + i2 + ")");
                                }
                            });
                            return true;
                        }
                    });
                    mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
                            if (i != 3) {
                                if (i == 701) {
                                    m.this.y();
                                    if (m.this.d == null) {
                                        return false;
                                    }
                                    m.this.d.h();
                                    return false;
                                } else if (i != 702) {
                                    return false;
                                }
                            }
                            m.this.z();
                            return false;
                        }
                    });
                    if (m.this.q == 0) {
                        m.this.q();
                        m.this.k();
                        m.this.v();
                        m.this.u();
                        m.this.playVideo();
                        m.this.I();
                    }
                }
            });
            this.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                    m.this.h();
                }
            });
            this.videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mediaPlayer, final int i, final int i2) {
                    m.this.A.post(new Runnable() {
                        public void run() {
                            m mVar = m.this;
                            mVar.handleMediaError("Video view error (" + i + "," + i2);
                        }
                    });
                    return true;
                }
            });
            StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
            this.videoView.setVideoURI(uri);
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
        this.videoView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1, 17));
        this.videoView.setOnTouchListener(new AppLovinTouchToClickListener(this.sdk, this, new AppLovinTouchToClickListener.OnClickListener() {
            public void onClick(View view, PointF pointF) {
                m.this.a(pointF);
            }
        }));
        this.B.addView((View) this.videoView);
        setContentView(this.B);
        p();
        x();
    }

    /* access modifiers changed from: private */
    public void a(final View view, final boolean z2, long j2) {
        float f2 = 0.0f;
        float f3 = z2 ? 0.0f : 1.0f;
        if (z2) {
            f2 = 1.0f;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f2);
        alphaAnimation.setDuration(j2);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (!z2) {
                    view.setVisibility(4);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                view.setVisibility(0);
            }
        });
        view.startAnimation(alphaAnimation);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        j.a(this.b.d(), appLovinAd);
        this.f = true;
        this.sdk.Z().c();
        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
            public void run() {
                m mVar = m.this;
                mVar.b(mVar.videoMuted);
            }
        }, ((Long) this.sdk.a(c.dn)).longValue());
    }

    private void a(AppLovinAd appLovinAd, double d2, boolean z2) {
        this.i = true;
        j.a(this.b.c(), appLovinAd, d2, z2);
    }

    private void a(final String str) {
        n nVar = this.b;
        if (nVar != null) {
            final AppLovinAdDisplayListener d2 = nVar.d();
            if ((d2 instanceof com.applovin.impl.sdk.ad.i) && this.y.compareAndSet(false, true)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        ((com.applovin.impl.sdk.ad.i) d2).onAdDisplayFailed(str);
                    }
                });
            }
        }
    }

    private void a(boolean z2) {
        Uri ay = z2 ? this.currentAd.ay() : this.currentAd.az();
        int a2 = a(((Integer) this.sdk.a(c.dc)).intValue());
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        AppLovinSdkUtils.safePopulateImageView(this.H, ay, a2);
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    private boolean a() {
        int identifier = getResources().getIdentifier((String) this.sdk.a(c.cV), "bool", "android");
        return identifier > 0 && getResources().getBoolean(identifier);
    }

    /* access modifiers changed from: private */
    public void b() {
        getWindow().getDecorView().setSystemUiVisibility(5894);
    }

    private void b(int i2) {
        try {
            setRequestedOrientation(i2);
        } catch (Throwable th) {
            this.sdk.v().b("InterActivity", "Failed to set requested orientation", th);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0052, code lost:
        if (r7 == 2) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0063, code lost:
        if (r7 == 1) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0026, code lost:
        if (r7 == 1) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r7, boolean r8) {
        /*
            r6 = this;
            com.applovin.impl.sdk.i r0 = r6.sdk
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r1 = com.applovin.impl.sdk.b.c.cT
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            com.applovin.impl.adview.n r1 = r6.b
            com.applovin.impl.sdk.ad.f$b r1 = r1.f()
            com.applovin.impl.sdk.ad.f$b r2 = com.applovin.impl.sdk.ad.f.b.ACTIVITY_PORTRAIT
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 != r2) goto L_0x003c
            r1 = 9
            if (r8 == 0) goto L_0x002c
            if (r7 == r5) goto L_0x0024
            if (r7 == r3) goto L_0x0024
            goto L_0x0030
        L_0x0024:
            if (r0 == 0) goto L_0x0066
            if (r7 != r5) goto L_0x0032
        L_0x0028:
            r6.b(r1)
            goto L_0x0066
        L_0x002c:
            if (r7 == 0) goto L_0x0036
            if (r7 == r4) goto L_0x0036
        L_0x0030:
            r6.c = r5
        L_0x0032:
            r6.b(r5)
            goto L_0x0066
        L_0x0036:
            if (r0 == 0) goto L_0x0066
            if (r7 != 0) goto L_0x0028
            r1 = 1
            goto L_0x0028
        L_0x003c:
            com.applovin.impl.adview.n r1 = r6.b
            com.applovin.impl.sdk.ad.f$b r1 = r1.f()
            com.applovin.impl.sdk.ad.f$b r2 = com.applovin.impl.sdk.ad.f.b.ACTIVITY_LANDSCAPE
            if (r1 != r2) goto L_0x0066
            r1 = 8
            r2 = 0
            if (r8 == 0) goto L_0x0057
            if (r7 == 0) goto L_0x0050
            if (r7 == r4) goto L_0x0050
            goto L_0x005b
        L_0x0050:
            if (r0 == 0) goto L_0x0066
            if (r7 != r4) goto L_0x0055
            goto L_0x0028
        L_0x0055:
            r1 = 0
            goto L_0x0028
        L_0x0057:
            if (r7 == r5) goto L_0x0061
            if (r7 == r3) goto L_0x0061
        L_0x005b:
            r6.c = r5
            r6.b(r2)
            goto L_0x0066
        L_0x0061:
            if (r0 == 0) goto L_0x0066
            if (r7 != r5) goto L_0x0028
            goto L_0x0055
        L_0x0066:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.b(int, boolean):void");
    }

    /* access modifiers changed from: private */
    public void b(AppLovinAd appLovinAd) {
        dismiss();
        c(appLovinAd);
    }

    private void b(String str) {
        f fVar = this.currentAd;
        if (fVar != null && fVar.Y()) {
            c(str);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        c adWebView;
        if (this.currentAd.V() && (adWebView = ((AdViewControllerImpl) this.a.getAdViewController()).getAdWebView()) != null) {
            try {
                adWebView.a(z2 ? "javascript:al_mute();" : "javascript:al_unmute();");
            } catch (Throwable th) {
                this.logger.b("InterActivity", "Unable to forward mute setting to template.", th);
            }
        }
    }

    private void c(AppLovinAd appLovinAd) {
        if (!this.g) {
            this.g = true;
            n nVar = this.b;
            if (nVar != null) {
                j.b(nVar.d(), appLovinAd);
            }
            this.sdk.Z().d();
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        c adWebView = ((AdViewControllerImpl) this.a.getAdViewController()).getAdWebView();
        if (adWebView != null && com.applovin.impl.sdk.utils.m.b(str)) {
            adWebView.a(str);
        }
    }

    private void c(boolean z2) {
        this.videoMuted = z2;
        MediaPlayer mediaPlayer = this.I.get();
        if (mediaPlayer != null) {
            float f2 = (float) (z2 ? 0 : 1);
            try {
                mediaPlayer.setVolume(f2, f2);
            } catch (IllegalStateException e2) {
                o oVar = this.logger;
                oVar.b("InterActivity", "Failed to set MediaPlayer muted: " + z2, e2);
            }
        }
    }

    private boolean c() {
        i iVar;
        if (this.b == null || (iVar = this.sdk) == null || ((Boolean) iVar.a(c.cN)).booleanValue()) {
            return true;
        }
        if (!((Boolean) this.sdk.a(c.cO)).booleanValue() || !this.j) {
            return ((Boolean) this.sdk.a(c.cP)).booleanValue() && this.poststitialWasDisplayed;
        }
        return true;
    }

    private void d() {
        int i2;
        if (this.sdk == null || !isFinishing()) {
            if (!(this.currentAd == null || (i2 = this.v) == Integer.MIN_VALUE)) {
                b(i2);
            }
            finish();
        }
    }

    private void d(AppLovinAd appLovinAd) {
        if (!this.h) {
            this.h = true;
            j.a(this.b.c(), appLovinAd);
        }
    }

    private void e() {
        f fVar;
        if (((Boolean) this.sdk.a(c.cW)).booleanValue() && (fVar = this.G) != null && fVar.getVisibility() != 8) {
            a(this.G, this.G.getVisibility() == 4, 750);
        }
    }

    private void f() {
        u uVar;
        s t2 = this.currentAd.t();
        if (t2 != null && t2.e() && !this.poststitialWasDisplayed && (uVar = this.K) != null) {
            a(this.K, uVar.getVisibility() == 4, t2.f());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.i.a(com.applovin.impl.sdk.b.e, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.e<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.i.a(java.lang.String, com.applovin.impl.sdk.b.c):com.applovin.impl.sdk.b.c<ST>
      com.applovin.impl.sdk.i.a(com.applovin.impl.sdk.b.e, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.i.a(com.applovin.impl.sdk.b.e, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.b.e<java.lang.Integer>, int]
     candidates:
      com.applovin.impl.sdk.i.a(java.lang.String, com.applovin.impl.sdk.b.c):com.applovin.impl.sdk.b.c<ST>
      com.applovin.impl.sdk.i.a(com.applovin.impl.sdk.b.e, java.lang.Object):void */
    private void g() {
        i iVar = this.sdk;
        if (iVar != null) {
            iVar.a((e) e.u, (Object) false);
            this.sdk.a((e) e.t, (Object) 0);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        this.e = true;
        showPoststitial();
    }

    /* access modifiers changed from: private */
    public boolean i() {
        return ((Integer) this.sdk.b(e.t, 0)).intValue() > 0 ? this.videoMuted : ((Boolean) this.sdk.a(c.db)).booleanValue() ? this.sdk.l().isMuted() : ((Boolean) this.sdk.a(c.cZ)).booleanValue();
    }

    private void j() {
        this.C = h.a(this.sdk, this, this.currentAd.o());
        this.C.setVisibility(8);
        this.C.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                m.this.B();
            }
        });
        int a2 = a(this.currentAd.Z());
        int i2 = 3;
        int i3 = (this.currentAd.ac() ? 3 : 5) | 48;
        if (!this.currentAd.ad()) {
            i2 = 5;
        }
        int i4 = i2 | 48;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, i3 | 48);
        this.C.a(a2);
        int a3 = a(this.currentAd.aa());
        int a4 = a(this.currentAd.ab());
        layoutParams.setMargins(a4, a3, a4, a3);
        this.B.addView(this.C, layoutParams);
        this.E = h.a(this.sdk, this, this.currentAd.p());
        this.E.setVisibility(8);
        this.E.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                m.this.A();
            }
        });
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(a2, a2, i4);
        layoutParams2.setMargins(a4, a3, a4, a3);
        this.E.a(a2);
        this.B.addView(this.E, layoutParams2);
        this.E.bringToFront();
        if (n()) {
            int a5 = a(((Integer) this.sdk.a(c.cc)).intValue());
            this.D = new View(this);
            this.D.setBackgroundColor(0);
            this.D.setVisibility(8);
            this.F = new View(this);
            this.F.setBackgroundColor(0);
            this.F.setVisibility(8);
            int i5 = a2 + a5;
            int a6 = a3 - a(5);
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(i5, i5, i3);
            layoutParams3.setMargins(a6, a6, a6, a6);
            FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(i5, i5, i4);
            layoutParams4.setMargins(a6, a6, a6, a6);
            this.D.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    m.this.C.performClick();
                }
            });
            this.F.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    m.this.E.performClick();
                }
            });
            this.B.addView(this.D, layoutParams3);
            this.D.bringToFront();
            this.B.addView(this.F, layoutParams4);
            this.F.bringToFront();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.H == null) {
            try {
                this.videoMuted = i();
                this.H = new ImageView(this);
                if (!l()) {
                    int a2 = a(((Integer) this.sdk.a(c.dc)).intValue());
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, ((Integer) this.sdk.a(c.de)).intValue());
                    this.H.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    int a3 = a(((Integer) this.sdk.a(c.dd)).intValue());
                    layoutParams.setMargins(a3, a3, a3, a3);
                    if ((this.videoMuted ? this.currentAd.ay() : this.currentAd.az()) != null) {
                        o v2 = this.sdk.v();
                        v2.b("InterActivity", "Added mute button with params: " + layoutParams);
                        a(this.videoMuted);
                        this.H.setClickable(true);
                        this.H.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                m.this.toggleMute();
                            }
                        });
                        this.B.addView(this.H, layoutParams);
                        this.H.bringToFront();
                        return;
                    }
                    this.sdk.v().e("InterActivity", "Attempting to add mute button but could not find uri");
                    return;
                }
                this.sdk.v().b("InterActivity", "Mute button should be hidden");
            } catch (Exception e2) {
                this.sdk.v().a("InterActivity", "Failed to attach mute button", e2);
            }
        }
    }

    private boolean l() {
        if (!((Boolean) this.sdk.a(c.cX)).booleanValue()) {
            return true;
        }
        if (!((Boolean) this.sdk.a(c.cY)).booleanValue() || i()) {
            return false;
        }
        return !((Boolean) this.sdk.a(c.da)).booleanValue();
    }

    /* access modifiers changed from: private */
    public void m() {
        runOnUiThread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, boolean):boolean
             arg types: [com.applovin.impl.adview.m, int]
             candidates:
              com.applovin.impl.adview.m.a(int, boolean):int
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, long):long
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, java.lang.ref.WeakReference):java.lang.ref.WeakReference
              com.applovin.impl.adview.m.a(long, com.applovin.impl.adview.h):void
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, android.graphics.PointF):void
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, com.applovin.sdk.AppLovinAd):void
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, java.lang.String):void
              com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, boolean):boolean */
            public void run() {
                try {
                    if (m.this.j) {
                        m.this.C.setVisibility(0);
                        return;
                    }
                    long unused = m.this.r = SystemClock.elapsedRealtime();
                    boolean unused2 = m.this.j = true;
                    if (m.this.n() && m.this.D != null) {
                        m.this.D.setVisibility(0);
                        m.this.D.bringToFront();
                    }
                    m.this.C.setVisibility(0);
                    m.this.C.bringToFront();
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                    alphaAnimation.setDuration((long) ((Integer) m.this.sdk.a(c.cF)).intValue());
                    alphaAnimation.setRepeatCount(0);
                    m.this.C.startAnimation(alphaAnimation);
                } catch (Throwable unused3) {
                    m.this.dismiss();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean n() {
        return ((Integer) this.sdk.a(c.cc)).intValue() > 0;
    }

    /* access modifiers changed from: private */
    public void o() {
        runOnUiThread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, boolean):boolean
             arg types: [com.applovin.impl.adview.m, int]
             candidates:
              com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, long):long
              com.applovin.impl.adview.m.b(int, boolean):void
              com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, com.applovin.sdk.AppLovinAd):void
              com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, boolean):boolean */
            public void run() {
                try {
                    if (!m.this.k && m.this.E != null) {
                        long unused = m.this.t = -1;
                        long unused2 = m.this.s = SystemClock.elapsedRealtime();
                        boolean unused3 = m.this.k = true;
                        m.this.E.setVisibility(0);
                        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                        alphaAnimation.setDuration((long) ((Integer) m.this.sdk.a(c.cF)).intValue());
                        alphaAnimation.setRepeatCount(0);
                        m.this.E.startAnimation(alphaAnimation);
                        if (m.this.n() && m.this.F != null) {
                            m.this.F.setVisibility(0);
                            m.this.F.bringToFront();
                        }
                    }
                } catch (Throwable th) {
                    o oVar = m.this.logger;
                    oVar.d("InterActivity", "Unable to show skip button: " + th);
                }
            }
        });
    }

    private void p() {
        h hVar;
        if (this.currentAd.m() >= 0.0f) {
            if (!this.l || (hVar = this.E) == null) {
                hVar = this.C;
            }
            a(com.applovin.impl.sdk.utils.p.b(this.currentAd.m()), hVar);
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        boolean z2 = ((Boolean) this.sdk.a(c.cM)).booleanValue() && t() > 0;
        if (this.G == null && z2) {
            this.G = new f(this);
            int B2 = this.currentAd.B();
            this.G.setTextColor(B2);
            this.G.setTextSize((float) ((Integer) this.sdk.a(c.cL)).intValue());
            this.G.setFinishedStrokeColor(B2);
            this.G.setFinishedStrokeWidth((float) ((Integer) this.sdk.a(c.cK)).intValue());
            this.G.setMax(t());
            this.G.setProgress(t());
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a(((Integer) this.sdk.a(c.cJ)).intValue()), a(((Integer) this.sdk.a(c.cJ)).intValue()), ((Integer) this.sdk.a(c.cI)).intValue());
            int a2 = a(((Integer) this.sdk.a(c.cH)).intValue());
            layoutParams.setMargins(a2, a2, a2, a2);
            this.B.addView(this.G, layoutParams);
            this.G.bringToFront();
            this.G.setVisibility(0);
            final long s2 = s();
            this.countdownManager.a("COUNTDOWN_CLOCK", 1000, new i.a() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.applovin.impl.adview.m.c(com.applovin.impl.adview.m, boolean):boolean
                 arg types: [com.applovin.impl.adview.m, int]
                 candidates:
                  com.applovin.impl.adview.m.c(com.applovin.impl.adview.m, long):long
                  com.applovin.impl.adview.m.c(com.applovin.impl.adview.m, boolean):boolean */
                public void a() {
                    if (m.this.G != null) {
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(s2 - ((long) m.this.videoView.getCurrentPosition()));
                        if (seconds <= 0) {
                            m.this.G.setVisibility(8);
                            boolean unused = m.this.o = true;
                        } else if (m.this.r()) {
                            m.this.G.setProgress((int) seconds);
                        }
                    }
                }

                public boolean b() {
                    return m.this.r();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public boolean r() {
        return !this.o && !this.poststitialWasDisplayed && this.videoView.isPlaying();
    }

    private long s() {
        return TimeUnit.SECONDS.toMillis((long) t());
    }

    private int t() {
        int A2 = this.currentAd.A();
        return (A2 <= 0 && ((Boolean) this.sdk.a(c.dm)).booleanValue()) ? this.computedLengthSeconds + 1 : A2;
    }

    /* access modifiers changed from: private */
    public void u() {
        if (this.L == null && this.currentAd.L()) {
            this.logger.c("InterActivity", "Attaching video progress bar...");
            this.L = new ProgressBar(this, null, 16842872);
            this.L.setMax(((Integer) this.sdk.a(c.dh)).intValue());
            this.L.setPadding(0, 0, 0, 0);
            if (g.f()) {
                try {
                    this.L.setProgressTintList(ColorStateList.valueOf(this.currentAd.M()));
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Unable to update progress bar color.", th);
                }
            }
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.videoView.getWidth(), 20, 80);
            layoutParams.setMargins(0, 0, 0, ((Integer) this.sdk.a(c.di)).intValue());
            this.B.addView(this.L, layoutParams);
            this.L.bringToFront();
            this.countdownManager.a("PROGRESS_BAR", ((Long) this.sdk.a(c.dg)).longValue(), new i.a() {
                public void a() {
                    if (m.this.L == null) {
                        return;
                    }
                    if (m.this.shouldContinueFullLengthVideoCountdown()) {
                        m.this.L.setProgress((int) ((((float) m.this.videoView.getCurrentPosition()) / ((float) m.this.videoView.getDuration())) * ((float) ((Integer) m.this.sdk.a(c.dh)).intValue())));
                        return;
                    }
                    m.this.L.setVisibility(8);
                }

                public boolean b() {
                    return m.this.shouldContinueFullLengthVideoCountdown();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        final s t2 = this.currentAd.t();
        if (com.applovin.impl.sdk.utils.m.b(this.currentAd.s()) && t2 != null && this.K == null) {
            this.logger.c("InterActivity", "Attaching video button...");
            this.K = w();
            double a2 = (double) t2.a();
            Double.isNaN(a2);
            double b2 = (double) t2.b();
            Double.isNaN(b2);
            int width = this.videoView.getWidth();
            int height = this.videoView.getHeight();
            double d2 = (double) width;
            Double.isNaN(d2);
            double d3 = (double) height;
            Double.isNaN(d3);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int) ((a2 / 100.0d) * d2), (int) ((b2 / 100.0d) * d3), t2.d());
            int a3 = a(t2.c());
            layoutParams.setMargins(a3, a3, a3, a3);
            this.B.addView(this.K, layoutParams);
            this.K.bringToFront();
            if (t2.i() > 0.0f) {
                this.K.setVisibility(4);
                this.A.postDelayed(new Runnable() {
                    public void run() {
                        long g = t2.g();
                        m mVar = m.this;
                        mVar.a(mVar.K, true, g);
                        m.this.K.bringToFront();
                    }
                }, com.applovin.impl.sdk.utils.p.b(t2.i()));
            }
            if (t2.j() > 0.0f) {
                this.A.postDelayed(new Runnable() {
                    public void run() {
                        long h = t2.h();
                        m mVar = m.this;
                        mVar.a(mVar.K, false, h);
                    }
                }, com.applovin.impl.sdk.utils.p.b(t2.j()));
            }
        }
    }

    private u w() {
        o oVar = this.logger;
        oVar.b("InterActivity", "Create video button with HTML = " + this.currentAd.s());
        v vVar = new v(this.sdk);
        this.M = new v.a() {
            public void a(u uVar) {
                m.this.logger.b("InterActivity", "Clicking through from video button...");
                m.this.clickThroughFromVideo(uVar.getAndClearLastClickLocation());
            }

            public void b(u uVar) {
                m.this.logger.b("InterActivity", "Closing ad from video button...");
                m.this.dismiss();
            }

            public void c(u uVar) {
                m.this.logger.b("InterActivity", "Skipping video from video button...");
                m.this.skipVideo();
            }
        };
        vVar.a(new WeakReference(this.M));
        u uVar = new u(vVar, getApplicationContext());
        uVar.a(this.currentAd.s());
        return uVar;
    }

    private void x() {
        if (this.m && this.currentAd.N()) {
            this.N = new a(this, ((Integer) this.sdk.a(c.dl)).intValue(), this.currentAd.P());
            this.N.setColor(this.currentAd.Q());
            this.N.setBackgroundColor(this.currentAd.R());
            this.N.setVisibility(8);
            this.B.addView(this.N, new FrameLayout.LayoutParams(-1, -1, 17));
            this.B.bringChildToFront(this.N);
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        a aVar = this.N;
        if (aVar != null) {
            aVar.a();
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        a aVar = this.N;
        if (aVar != null) {
            aVar.b();
        }
    }

    public void clickThroughFromVideo(PointF pointF) {
        try {
            if (this.currentAd.ak() && this.l) {
                o();
            }
            this.sdk.o().trackAndLaunchVideoClick(this.currentAd, this.a, this.currentAd.g(), pointF);
            j.a(this.b.e(), this.currentAd);
            if (this.d != null) {
                this.d.b();
            }
        } catch (Throwable th) {
            this.sdk.v().b("InterActivity", "Encountered error while clicking through video.", th);
        }
    }

    public void continueVideo() {
        O();
    }

    public void dismiss() {
        long currentTimeMillis = System.currentTimeMillis() - this.p;
        o.f("InterActivity", "Dismissing ad after " + currentTimeMillis + " milliseconds elapsed");
        g();
        P();
        if (this.b != null) {
            if (this.currentAd != null) {
                c(this.currentAd);
                d dVar = this.d;
                if (dVar != null) {
                    dVar.c();
                    this.d = null;
                }
            }
            this.b.a(false);
            this.b.g();
        }
        lastKnownWrapper = null;
        d();
    }

    public void exitWithError(String str) {
        a(str);
        try {
            o.c("InterActivity", "Failed to properly render an Interstitial Activity, due to error: " + str, new Throwable("Initialized = " + n.b + "; CleanedUp = " + n.c));
            c(new h());
        } catch (Exception e2) {
            o.c("InterActivity", "Failed to show a video ad due to error:", e2);
        }
        dismiss();
    }

    public boolean getPoststitialWasDisplayed() {
        return this.poststitialWasDisplayed;
    }

    public int getVideoPercentViewed() {
        if (this.e) {
            return 100;
        }
        t tVar = this.videoView;
        if (tVar != null) {
            int duration = tVar.getDuration();
            if (duration <= 0) {
                return this.u;
            }
            double currentPosition = (double) this.videoView.getCurrentPosition();
            double d2 = (double) duration;
            Double.isNaN(currentPosition);
            Double.isNaN(d2);
            return (int) ((currentPosition / d2) * 100.0d);
        }
        this.logger.e("InterActivity", "No video view detected on video end");
        return 0;
    }

    public void handleMediaError(String str) {
        this.logger.e("InterActivity", str);
        if (this.w.compareAndSet(false, true) && this.currentAd.H()) {
            a(str);
            dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isFullyWatched() {
        return getVideoPercentViewed() >= this.currentAd.T();
    }

    /* access modifiers changed from: protected */
    public boolean isVastAd() {
        return this.currentAd instanceof com.applovin.impl.a.a;
    }

    public void onBackPressed() {
        h hVar;
        if (this.currentAd != null) {
            if (this.currentAd.aw() && !this.poststitialWasDisplayed) {
                return;
            }
            if (this.currentAd.ax() && this.poststitialWasDisplayed) {
                return;
            }
        }
        if (c()) {
            this.logger.b("InterActivity", "Back button was pressed; forwarding to Android for handling...");
        } else {
            try {
                if (!this.poststitialWasDisplayed && this.l && this.E != null && this.E.getVisibility() == 0 && this.E.getAlpha() > 0.0f) {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to skip button.");
                    hVar = this.E;
                } else if (this.C == null || this.C.getVisibility() != 0 || this.C.getAlpha() <= 0.0f) {
                    this.logger.b("InterActivity", "Back button was pressed, but was not eligible for dismissal.");
                    b("javascript:al_onBackPressed();");
                    return;
                } else {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to close button.");
                    hVar = this.C;
                }
                hVar.performClick();
                b("javascript:al_onBackPressed();");
                return;
            } catch (Exception unused) {
            }
        }
        super.onBackPressed();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation != 0 && (this.videoView instanceof p) && this.I.get() != null) {
            MediaPlayer mediaPlayer = this.I.get();
            this.videoView.setVideoSize(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0183  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            java.lang.String r0 = "InterActivity"
            super.onCreate(r8)
            if (r8 == 0) goto L_0x0012
            java.lang.String r1 = "instance_impression_tracked"
            boolean r1 = r8.getBoolean(r1)
            java.util.concurrent.atomic.AtomicBoolean r2 = r7.x
            r2.set(r1)
        L_0x0012:
            r1 = 1
            r7.requestWindowFeature(r1)
            android.os.StrictMode$ThreadPolicy r1 = android.os.StrictMode.allowThreadDiskReads()
            android.content.Intent r2 = r7.getIntent()     // Catch:{ all -> 0x0168 }
            java.lang.String r3 = "com.applovin.interstitial.wrapper_id"
            java.lang.String r2 = r2.getStringExtra(r3)     // Catch:{ all -> 0x0168 }
            if (r2 == 0) goto L_0x0165
            boolean r3 = r2.isEmpty()     // Catch:{ all -> 0x0168 }
            if (r3 != 0) goto L_0x0165
            com.applovin.impl.adview.n r2 = com.applovin.impl.adview.n.a(r2)     // Catch:{ all -> 0x0168 }
            r7.b = r2     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.n r2 = r7.b     // Catch:{ all -> 0x0168 }
            if (r2 != 0) goto L_0x003e
            com.applovin.impl.adview.n r2 = com.applovin.impl.adview.m.lastKnownWrapper     // Catch:{ all -> 0x0168 }
            if (r2 == 0) goto L_0x003e
            com.applovin.impl.adview.n r2 = com.applovin.impl.adview.m.lastKnownWrapper     // Catch:{ all -> 0x0168 }
            r7.b = r2     // Catch:{ all -> 0x0168 }
        L_0x003e:
            com.applovin.impl.adview.n r2 = r7.b     // Catch:{ all -> 0x0168 }
            if (r2 == 0) goto L_0x0151
            com.applovin.impl.adview.n r2 = r7.b     // Catch:{ all -> 0x0168 }
            com.applovin.sdk.AppLovinAd r2 = r2.b()     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.n r3 = r7.b     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.i r3 = r3.a()     // Catch:{ all -> 0x0168 }
            r7.sdk = r3     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.n r3 = r7.b     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.i r3 = r3.a()     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.o r3 = r3.v()     // Catch:{ all -> 0x0168 }
            r7.logger = r3     // Catch:{ all -> 0x0168 }
            if (r2 == 0) goto L_0x014b
            com.applovin.impl.sdk.ad.f r2 = (com.applovin.impl.sdk.ad.f) r2     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.c.d r3 = new com.applovin.impl.sdk.c.d     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.i r4 = r7.sdk     // Catch:{ all -> 0x0168 }
            r3.<init>(r2, r4)     // Catch:{ all -> 0x0168 }
            r7.d = r3     // Catch:{ all -> 0x0168 }
            r3 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r3 = r7.findViewById(r3)     // Catch:{ all -> 0x0168 }
            if (r3 == 0) goto L_0x0085
            boolean r4 = r2.hasVideoUrl()     // Catch:{ all -> 0x0168 }
            if (r4 == 0) goto L_0x0080
            int r4 = r2.C()     // Catch:{ all -> 0x0168 }
        L_0x007c:
            r3.setBackgroundColor(r4)     // Catch:{ all -> 0x0168 }
            goto L_0x0085
        L_0x0080:
            int r4 = r2.D()     // Catch:{ all -> 0x0168 }
            goto L_0x007c
        L_0x0085:
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0168 }
            r7.p = r3     // Catch:{ all -> 0x0168 }
            boolean r3 = r2.v()     // Catch:{ all -> 0x0168 }
            if (r3 == 0) goto L_0x009a
            android.view.Window r3 = r7.getWindow()     // Catch:{ all -> 0x0168 }
            r4 = 16777216(0x1000000, float:2.3509887E-38)
            r3.setFlags(r4, r4)     // Catch:{ all -> 0x0168 }
        L_0x009a:
            boolean r3 = r2.w()     // Catch:{ all -> 0x0168 }
            if (r3 == 0) goto L_0x00a9
            android.view.Window r3 = r7.getWindow()     // Catch:{ all -> 0x0168 }
            r4 = 128(0x80, float:1.794E-43)
            r3.addFlags(r4)     // Catch:{ all -> 0x0168 }
        L_0x00a9:
            int r3 = com.applovin.impl.sdk.utils.p.g(r7)     // Catch:{ all -> 0x0168 }
            boolean r4 = com.applovin.sdk.AppLovinSdkUtils.isTablet(r7)     // Catch:{ all -> 0x0168 }
            int r5 = r7.a(r3, r4)     // Catch:{ all -> 0x0168 }
            if (r8 != 0) goto L_0x00ba
            r7.v = r5     // Catch:{ all -> 0x0168 }
            goto L_0x00c2
        L_0x00ba:
            java.lang.String r6 = "original_orientation"
            int r8 = r8.getInt(r6, r5)     // Catch:{ all -> 0x0168 }
            r7.v = r8     // Catch:{ all -> 0x0168 }
        L_0x00c2:
            boolean r8 = r2.z()     // Catch:{ all -> 0x0168 }
            if (r8 == 0) goto L_0x00f0
            r8 = -1
            if (r5 == r8) goto L_0x00e5
            com.applovin.impl.sdk.o r8 = r7.logger     // Catch:{ all -> 0x0168 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0168 }
            r2.<init>()     // Catch:{ all -> 0x0168 }
            java.lang.String r3 = "Locking activity orientation to current orientation: "
            r2.append(r3)     // Catch:{ all -> 0x0168 }
            r2.append(r5)     // Catch:{ all -> 0x0168 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0168 }
            r8.b(r0, r2)     // Catch:{ all -> 0x0168 }
            r7.b(r5)     // Catch:{ all -> 0x0168 }
            goto L_0x00f8
        L_0x00e5:
            com.applovin.impl.sdk.o r8 = r7.logger     // Catch:{ all -> 0x0168 }
            java.lang.String r2 = "Unable to detect current orientation. Locking to targeted orientation..."
            r8.e(r0, r2)     // Catch:{ all -> 0x0168 }
        L_0x00ec:
            r7.b(r3, r4)     // Catch:{ all -> 0x0168 }
            goto L_0x00f8
        L_0x00f0:
            com.applovin.impl.sdk.o r8 = r7.logger     // Catch:{ all -> 0x0168 }
            java.lang.String r2 = "Locking activity orientation to targeted orientation..."
            r8.b(r0, r2)     // Catch:{ all -> 0x0168 }
            goto L_0x00ec
        L_0x00f8:
            com.applovin.impl.adview.l r8 = new com.applovin.impl.adview.l     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.i r2 = r7.sdk     // Catch:{ all -> 0x0168 }
            com.applovin.sdk.AppLovinSdk r2 = r2.S()     // Catch:{ all -> 0x0168 }
            com.applovin.sdk.AppLovinAdSize r3 = com.applovin.sdk.AppLovinAdSize.INTERSTITIAL     // Catch:{ all -> 0x0168 }
            r8.<init>(r2, r3, r7)     // Catch:{ all -> 0x0168 }
            r7.a = r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.l r8 = r7.a     // Catch:{ all -> 0x0168 }
            r2 = 0
            r8.setAutoDestroy(r2)     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.l r8 = r7.a     // Catch:{ all -> 0x0168 }
            com.applovin.adview.AdViewController r8 = r8.getAdViewController()     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.AdViewControllerImpl r8 = (com.applovin.impl.adview.AdViewControllerImpl) r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.c.d r2 = r7.d     // Catch:{ all -> 0x0168 }
            r8.setStatsManagerHelper(r2)     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.n r8 = r7.b     // Catch:{ all -> 0x0168 }
            r8.a(r7)     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.i r8 = r7.sdk     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r2 = com.applovin.impl.sdk.b.c.dk     // Catch:{ all -> 0x0168 }
            java.lang.Object r8 = r8.a(r2)     // Catch:{ all -> 0x0168 }
            java.lang.Boolean r8 = (java.lang.Boolean) r8     // Catch:{ all -> 0x0168 }
            boolean r8 = r8.booleanValue()     // Catch:{ all -> 0x0168 }
            r7.l = r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.a.b r8 = new com.applovin.impl.sdk.a.b     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.i r2 = r7.sdk     // Catch:{ all -> 0x0168 }
            r8.<init>(r7, r2)     // Catch:{ all -> 0x0168 }
            r7.J = r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.m$1 r8 = new com.applovin.impl.adview.m$1     // Catch:{ all -> 0x0168 }
            r8.<init>()     // Catch:{ all -> 0x0168 }
            r7.P = r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.i r8 = r7.sdk     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.a r8 = r8.aa()     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.utils.a r2 = r7.P     // Catch:{ all -> 0x0168 }
            r8.a(r2)     // Catch:{ all -> 0x0168 }
            goto L_0x0179
        L_0x014b:
            java.lang.String r8 = "No current ad found."
        L_0x014d:
            r7.exitWithError(r8)     // Catch:{ all -> 0x0168 }
            goto L_0x0179
        L_0x0151:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0168 }
            r8.<init>()     // Catch:{ all -> 0x0168 }
            java.lang.String r2 = "Wrapper is null; initialized state: "
            r8.append(r2)     // Catch:{ all -> 0x0168 }
            boolean r2 = com.applovin.impl.adview.n.b     // Catch:{ all -> 0x0168 }
            r8.append(r2)     // Catch:{ all -> 0x0168 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0168 }
            goto L_0x014d
        L_0x0165:
            java.lang.String r8 = "Wrapper ID is null"
            goto L_0x014d
        L_0x0168:
            r8 = move-exception
            com.applovin.impl.sdk.o r2 = r7.logger     // Catch:{ all -> 0x018a }
            if (r2 == 0) goto L_0x0174
            com.applovin.impl.sdk.o r2 = r7.logger     // Catch:{ all -> 0x018a }
            java.lang.String r3 = "Encountered error during onCreate."
            r2.b(r0, r3, r8)     // Catch:{ all -> 0x018a }
        L_0x0174:
            java.lang.String r8 = "An error was encountered during interstitial ad creation."
            r7.exitWithError(r8)     // Catch:{ all -> 0x018a }
        L_0x0179:
            android.os.StrictMode.setThreadPolicy(r1)
            r7.g()
            com.applovin.impl.sdk.c.d r8 = r7.d
            if (r8 == 0) goto L_0x0186
            r8.a()
        L_0x0186:
            r7.J()
            return
        L_0x018a:
            r8 = move-exception
            android.os.StrictMode.setThreadPolicy(r1)
            goto L_0x0190
        L_0x018f:
            throw r8
        L_0x0190:
            goto L_0x018f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (r4.currentAd != null) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0077, code lost:
        if (r4.currentAd == null) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0079, code lost:
        P();
        c(r4.currentAd);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0081, code lost:
        super.onDestroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0084, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDestroy() {
        /*
            r4 = this;
            com.applovin.impl.adview.l r0 = r4.a     // Catch:{ all -> 0x0067 }
            r1 = 0
            if (r0 == 0) goto L_0x001d
            com.applovin.impl.adview.l r0 = r4.a     // Catch:{ all -> 0x0067 }
            android.view.ViewParent r0 = r0.getParent()     // Catch:{ all -> 0x0067 }
            boolean r2 = r0 instanceof android.view.ViewGroup     // Catch:{ all -> 0x0067 }
            if (r2 == 0) goto L_0x0016
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0     // Catch:{ all -> 0x0067 }
            com.applovin.impl.adview.l r2 = r4.a     // Catch:{ all -> 0x0067 }
            r0.removeView(r2)     // Catch:{ all -> 0x0067 }
        L_0x0016:
            com.applovin.impl.adview.l r0 = r4.a     // Catch:{ all -> 0x0067 }
            r0.destroy()     // Catch:{ all -> 0x0067 }
            r4.a = r1     // Catch:{ all -> 0x0067 }
        L_0x001d:
            com.applovin.impl.adview.t r0 = r4.videoView     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x002b
            com.applovin.impl.adview.t r0 = r4.videoView     // Catch:{ all -> 0x0067 }
            r0.pause()     // Catch:{ all -> 0x0067 }
            com.applovin.impl.adview.t r0 = r4.videoView     // Catch:{ all -> 0x0067 }
            r0.stopPlayback()     // Catch:{ all -> 0x0067 }
        L_0x002b:
            com.applovin.impl.sdk.i r0 = r4.sdk     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0047
            java.lang.ref.WeakReference<android.media.MediaPlayer> r0 = r4.I     // Catch:{ all -> 0x0067 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0067 }
            android.media.MediaPlayer r0 = (android.media.MediaPlayer) r0     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x003c
            r0.release()     // Catch:{ all -> 0x0067 }
        L_0x003c:
            com.applovin.impl.sdk.i r0 = r4.sdk     // Catch:{ all -> 0x0067 }
            com.applovin.impl.sdk.a r0 = r0.aa()     // Catch:{ all -> 0x0067 }
            com.applovin.impl.sdk.utils.a r2 = r4.P     // Catch:{ all -> 0x0067 }
            r0.b(r2)     // Catch:{ all -> 0x0067 }
        L_0x0047:
            com.applovin.impl.adview.i r0 = r4.countdownManager     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0050
            com.applovin.impl.adview.i r0 = r4.countdownManager     // Catch:{ all -> 0x0067 }
            r0.b()     // Catch:{ all -> 0x0067 }
        L_0x0050:
            android.os.Handler r0 = r4.A     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0059
            android.os.Handler r0 = r4.A     // Catch:{ all -> 0x0067 }
            r0.removeCallbacksAndMessages(r1)     // Catch:{ all -> 0x0067 }
        L_0x0059:
            android.os.Handler r0 = r4.z     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0062
            android.os.Handler r0 = r4.z     // Catch:{ all -> 0x0067 }
            r0.removeCallbacksAndMessages(r1)     // Catch:{ all -> 0x0067 }
        L_0x0062:
            com.applovin.impl.sdk.ad.f r0 = r4.currentAd
            if (r0 == 0) goto L_0x0081
            goto L_0x0079
        L_0x0067:
            r0 = move-exception
            com.applovin.impl.sdk.o r1 = r4.logger     // Catch:{ all -> 0x0085 }
            if (r1 == 0) goto L_0x0075
            com.applovin.impl.sdk.o r1 = r4.logger     // Catch:{ all -> 0x0085 }
            java.lang.String r2 = "InterActivity"
            java.lang.String r3 = "Unable to destroy video view"
            r1.a(r2, r3, r0)     // Catch:{ all -> 0x0085 }
        L_0x0075:
            com.applovin.impl.sdk.ad.f r0 = r4.currentAd
            if (r0 == 0) goto L_0x0081
        L_0x0079:
            r4.P()
            com.applovin.impl.sdk.ad.f r0 = r4.currentAd
            r4.c(r0)
        L_0x0081:
            super.onDestroy()
            return
        L_0x0085:
            r0 = move-exception
            com.applovin.impl.sdk.ad.f r1 = r4.currentAd
            if (r1 == 0) goto L_0x0092
            r4.P()
            com.applovin.impl.sdk.ad.f r1 = r4.currentAd
            r4.c(r1)
        L_0x0092:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.onDestroy():void");
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if ((i2 == 25 || i2 == 24) && this.currentAd.X() && L()) {
            toggleMute();
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.logger.b("InterActivity", "App paused...");
        this.q = System.currentTimeMillis();
        if (this.poststitialWasDisplayed) {
            M();
        }
        this.b.a(false);
        this.J.a();
        pauseReportRewardTask();
        b("javascript:al_onAppPaused();");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0073, code lost:
        if (r0 != null) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ac, code lost:
        if (r1 == false) goto L_0x00ae;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
            r6 = this;
            super.onResume()
            com.applovin.impl.sdk.o r0 = r6.logger
            java.lang.String r1 = "InterActivity"
            java.lang.String r2 = "App resumed..."
            r0.b(r1, r2)
            com.applovin.impl.adview.n r0 = r6.b
            r1 = 1
            r0.a(r1)
            boolean r0 = r6.n
            if (r0 != 0) goto L_0x00b5
            com.applovin.impl.sdk.c.d r0 = r6.d
            if (r0 == 0) goto L_0x0024
            long r2 = java.lang.System.currentTimeMillis()
            long r4 = r6.q
            long r2 = r2 - r4
            r0.d(r2)
        L_0x0024:
            com.applovin.impl.sdk.i r0 = r6.sdk
            com.applovin.impl.sdk.b.e<java.lang.Boolean> r2 = com.applovin.impl.sdk.b.e.u
            r3 = 0
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r3)
            java.lang.Object r0 = r0.b(r2, r4)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r4 = 0
            if (r0 == 0) goto L_0x0076
            com.applovin.impl.sdk.a.b r0 = r6.J
            boolean r0 = r0.d()
            if (r0 != 0) goto L_0x0076
            boolean r0 = r6.poststitialWasDisplayed
            if (r0 != 0) goto L_0x0076
            r6.N()
            r6.y()
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.sdk.i r0 = r6.sdk
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r1 = com.applovin.impl.sdk.b.c.cG
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            boolean r0 = r0.y()
            if (r0 != 0) goto L_0x00b1
            boolean r0 = r6.poststitialWasDisplayed
            if (r0 != 0) goto L_0x00b1
            boolean r0 = r6.l
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.adview.h r0 = r6.E
            if (r0 == 0) goto L_0x00b1
            goto L_0x00ae
        L_0x0076:
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            boolean r0 = r0 instanceof com.applovin.impl.sdk.ad.a
            if (r0 == 0) goto L_0x0087
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            com.applovin.impl.sdk.ad.a r0 = (com.applovin.impl.sdk.ad.a) r0
            boolean r0 = r0.i()
            if (r0 == 0) goto L_0x0087
            goto L_0x0088
        L_0x0087:
            r1 = 0
        L_0x0088:
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.sdk.i r0 = r6.sdk
            com.applovin.impl.sdk.b.c<java.lang.Boolean> r2 = com.applovin.impl.sdk.b.c.cG
            java.lang.Object r0 = r0.a(r2)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            boolean r0 = r0.x()
            if (r0 != 0) goto L_0x00b1
            boolean r0 = r6.poststitialWasDisplayed
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.adview.h r0 = r6.C
            if (r0 == 0) goto L_0x00b1
            if (r1 != 0) goto L_0x00b1
        L_0x00ae:
            r6.a(r4, r0)
        L_0x00b1:
            r6.resumeReportRewardTask()
            goto L_0x00d0
        L_0x00b5:
            com.applovin.impl.sdk.a.b r0 = r6.J
            boolean r0 = r0.d()
            if (r0 != 0) goto L_0x00d0
            boolean r0 = r6.poststitialWasDisplayed
            if (r0 != 0) goto L_0x00d0
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            if (r0 == 0) goto L_0x00d0
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            boolean r0 = r0.O()
            if (r0 == 0) goto L_0x00d0
            r6.y()
        L_0x00d0:
            java.lang.String r0 = "javascript:al_onAppResumed();"
            r6.b(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.onResume():void");
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("instance_impression_tracked", this.x.get());
        bundle.putInt("original_orientation", this.v);
    }

    public void onWindowFocusChanged(boolean z2) {
        String str;
        super.onWindowFocusChanged(z2);
        com.applovin.impl.sdk.i iVar = this.sdk;
        if (z2) {
            if (iVar != null) {
                this.logger.b("InterActivity", "Window gained focus");
                try {
                    if (!g.e() || !((Boolean) this.sdk.a(c.df)).booleanValue() || !a()) {
                        getWindow().setFlags(1024, 1024);
                    } else {
                        b();
                        if (((Long) this.sdk.a(c.cQ)).longValue() > 0) {
                            this.A.postDelayed(new Runnable() {
                                public void run() {
                                    m.this.b();
                                }
                            }, ((Long) this.sdk.a(c.cQ)).longValue());
                        }
                    }
                    if (((Boolean) this.sdk.a(c.cR)).booleanValue() && !this.poststitialWasDisplayed) {
                        N();
                        resumeReportRewardTask();
                    }
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Setting window flags failed.", th);
                }
                this.n = false;
                b("javascript:al_onWindowFocusChanged( " + z2 + " );");
            }
            str = "Window gained focus. SDK is null.";
        } else if (iVar != null) {
            this.logger.b("InterActivity", "Window lost focus");
            if (((Boolean) this.sdk.a(c.cR)).booleanValue() && !this.poststitialWasDisplayed) {
                M();
                pauseReportRewardTask();
            }
            this.n = false;
            b("javascript:al_onWindowFocusChanged( " + z2 + " );");
        } else {
            str = "Window lost focus. SDK is null.";
        }
        o.f("InterActivity", str);
        this.n = false;
        b("javascript:al_onWindowFocusChanged( " + z2 + " );");
    }

    public void pauseReportRewardTask() {
        n nVar = this.O;
        if (nVar != null) {
            nVar.b();
        }
    }

    /* access modifiers changed from: protected */
    public void playVideo() {
        d(this.currentAd);
        this.videoView.start();
        this.countdownManager.a();
    }

    public void resumeReportRewardTask() {
        n nVar = this.O;
        if (nVar != null) {
            nVar.c();
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldContinueFullLengthVideoCountdown() {
        return !this.e && !this.poststitialWasDisplayed;
    }

    public void showPoststitial() {
        long j2;
        h hVar;
        try {
            if (this.d != null) {
                this.d.g();
            }
            if (!this.currentAd.ae()) {
                K();
            }
            if (this.a != null) {
                ViewParent parent = this.a.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(this.a);
                }
                FrameLayout frameLayout = new FrameLayout(this);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
                frameLayout.setBackgroundColor(this.currentAd.D());
                frameLayout.addView(this.a);
                if (this.currentAd.ae()) {
                    K();
                }
                if (this.B != null) {
                    this.B.removeAllViewsInLayout();
                }
                if (n() && this.D != null) {
                    if (this.D.getParent() instanceof ViewGroup) {
                        ((ViewGroup) this.D.getParent()).removeView(this.D);
                    }
                    frameLayout.addView(this.D);
                    this.D.bringToFront();
                }
                if (this.C != null) {
                    ViewParent parent2 = this.C.getParent();
                    if (parent2 instanceof ViewGroup) {
                        ((ViewGroup) parent2).removeView(this.C);
                    }
                    frameLayout.addView(this.C);
                    this.C.bringToFront();
                }
                setContentView(frameLayout);
                if (((Boolean) this.sdk.a(c.eQ)).booleanValue()) {
                    this.a.setVisibility(4);
                    this.a.setVisibility(0);
                }
                int U = this.currentAd.U();
                if (U >= 0) {
                    this.A.postDelayed(new Runnable() {
                        public void run() {
                            m.this.c("javascript:al_onPoststitialShow();");
                        }
                    }, (long) U);
                }
            }
            if (!((this.currentAd instanceof com.applovin.impl.sdk.ad.a) && ((com.applovin.impl.sdk.ad.a) this.currentAd).i())) {
                if (this.currentAd.n() >= 0.0f) {
                    j2 = com.applovin.impl.sdk.utils.p.b(this.currentAd.n());
                    hVar = this.C;
                } else if (this.currentAd.n() == -2.0f) {
                    this.C.setVisibility(0);
                } else {
                    j2 = 0;
                    hVar = this.C;
                }
                a(j2, hVar);
            } else {
                this.logger.b("InterActivity", "Skip showing of close button");
            }
            this.poststitialWasDisplayed = true;
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Encountered error while showing poststitial. Dismissing...", th);
            dismiss();
        }
    }

    public void skipVideo() {
        this.t = SystemClock.elapsedRealtime() - this.s;
        d dVar = this.d;
        if (dVar != null) {
            dVar.f();
        }
        if (this.currentAd.q()) {
            dismiss();
        } else {
            showPoststitial();
        }
    }

    public void toggleMute() {
        boolean z2 = !L();
        d dVar = this.d;
        if (dVar != null) {
            dVar.i();
        }
        try {
            c(z2);
            a(z2);
            b(z2);
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Unable to set volume to " + z2, th);
        }
    }
}
