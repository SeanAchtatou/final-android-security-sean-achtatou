package com.applovin.impl.adview;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;

class b extends WebChromeClient {
    private final o a;

    public b(i iVar) {
        this.a = iVar.v();
    }

    public void onConsoleMessage(String str, int i, String str2) {
        o oVar = this.a;
        oVar.d("AdWebView", "console.log[" + i + "] :" + str);
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        this.a.b("AdWebView", consoleMessage.sourceId() + ": " + consoleMessage.lineNumber() + ": " + consoleMessage.message());
        return true;
    }

    public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        o oVar = this.a;
        oVar.d("AdWebView", "Alert attempted: " + str2);
        return true;
    }

    public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        o oVar = this.a;
        oVar.d("AdWebView", "JS onBeforeUnload attempted: " + str2);
        return true;
    }

    public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        o oVar = this.a;
        oVar.d("AdWebView", "JS confirm attempted: " + str2);
        return true;
    }
}
