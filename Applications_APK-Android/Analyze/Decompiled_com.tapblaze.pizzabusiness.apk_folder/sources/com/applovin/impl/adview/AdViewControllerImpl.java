package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.applovin.adview.AdViewController;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinAdViewEventListener;
import com.applovin.impl.sdk.AppLovinAdServiceImpl;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.h;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicReference;

public class AdViewControllerImpl implements AdViewController {
    private volatile AppLovinAdClickListener A;
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public ViewGroup b;
    /* access modifiers changed from: private */
    public i c;
    /* access modifiers changed from: private */
    public AppLovinAdServiceImpl d;
    /* access modifiers changed from: private */
    public o e;
    /* access modifiers changed from: private */
    public AppLovinAdSize f;
    private String g;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.c.d h;
    private d i;
    private d j;
    /* access modifiers changed from: private */
    public c k;
    private AppLovinAd l;
    private Runnable m;
    private Runnable n;
    /* access modifiers changed from: private */
    public volatile AppLovinAd o = null;
    private volatile AppLovinAd p = null;
    /* access modifiers changed from: private */
    public k q = null;
    /* access modifiers changed from: private */
    public k r = null;
    private final AtomicReference<AppLovinAd> s = new AtomicReference<>();
    private volatile boolean t = false;
    private volatile boolean u = true;
    /* access modifiers changed from: private */
    public volatile boolean v = false;
    private volatile boolean w = false;
    /* access modifiers changed from: private */
    public volatile AppLovinAdLoadListener x;
    private volatile AppLovinAdDisplayListener y;
    /* access modifiers changed from: private */
    public volatile AppLovinAdViewEventListener z;

    private class a implements Runnable {
        private a() {
        }

        public void run() {
            if (AdViewControllerImpl.this.k != null) {
                AdViewControllerImpl.this.k.setVisibility(8);
            }
        }
    }

    private class b implements Runnable {
        private b() {
        }

        public void run() {
            if (AdViewControllerImpl.this.k != null) {
                try {
                    AdViewControllerImpl.this.k.loadDataWithBaseURL("/", "<html></html>", "text/html", null, "");
                } catch (Exception unused) {
                }
            }
        }
    }

    private class c implements Runnable {
        private c() {
        }

        public void run() {
            if (AdViewControllerImpl.this.o == null) {
                return;
            }
            if (AdViewControllerImpl.this.k != null) {
                o b = AdViewControllerImpl.this.e;
                b.b("AppLovinAdView", "Rendering advertisement ad for #" + AdViewControllerImpl.this.o.getAdIdNumber() + "...");
                AdViewControllerImpl.b(AdViewControllerImpl.this.k, AdViewControllerImpl.this.o.getSize());
                AdViewControllerImpl.this.k.a(AdViewControllerImpl.this.o);
                if (AdViewControllerImpl.this.o.getSize() != AppLovinAdSize.INTERSTITIAL && !AdViewControllerImpl.this.v && (AdViewControllerImpl.this.o instanceof f)) {
                    f fVar = (f) AdViewControllerImpl.this.o;
                    AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                    com.applovin.impl.sdk.c.d unused = adViewControllerImpl.h = new com.applovin.impl.sdk.c.d(fVar, adViewControllerImpl.c);
                    AdViewControllerImpl.this.h.a();
                    AdViewControllerImpl.this.k.a(AdViewControllerImpl.this.h);
                    fVar.setHasShown(true);
                }
                if (AdViewControllerImpl.this.k.b() != null && (AdViewControllerImpl.this.o instanceof f)) {
                    AdViewControllerImpl.this.k.b().a(((f) AdViewControllerImpl.this.o).r() ? 0 : 1);
                    return;
                }
                return;
            }
            o.i("AppLovinAdView", "Unable to render advertisement for ad #" + AdViewControllerImpl.this.o.getAdIdNumber() + ". Please make sure you are not calling AppLovinAdView.destroy() prematurely.");
        }
    }

    static class d implements AppLovinAdLoadListener {
        private final AppLovinAdService a;
        private final o b;
        private final AdViewControllerImpl c;

        d(AdViewControllerImpl adViewControllerImpl, i iVar) {
            if (adViewControllerImpl == null) {
                throw new IllegalArgumentException("No view specified");
            } else if (iVar != null) {
                this.b = iVar.v();
                this.a = iVar.o();
                this.c = adViewControllerImpl;
            } else {
                throw new IllegalArgumentException("No sdk specified");
            }
        }

        private AdViewControllerImpl a() {
            return this.c;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            AdViewControllerImpl a2 = a();
            if (a2 != null) {
                a2.a(appLovinAd);
            } else {
                o.i("AppLovinAdView", "Ad view has been garbage collected by the time an ad was received");
            }
        }

        public void failedToReceiveAd(int i) {
            AdViewControllerImpl a2 = a();
            if (a2 != null) {
                a2.a(i);
            }
        }
    }

    private void a(AppLovinAdView appLovinAdView, i iVar, AppLovinAdSize appLovinAdSize, String str, Context context) {
        if (appLovinAdView == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (iVar == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (appLovinAdSize != null) {
            this.c = iVar;
            this.d = iVar.o();
            this.e = iVar.v();
            this.f = appLovinAdSize;
            this.g = str;
            this.a = context;
            this.b = appLovinAdView;
            this.l = new h();
            this.i = new d(this, iVar);
            this.n = new a();
            this.m = new c();
            this.j = new d(this, iVar);
            a(appLovinAdSize);
        } else {
            throw new IllegalArgumentException("No ad size specified");
        }
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAdSize appLovinAdSize) {
        try {
            this.k = new c(this.i, this.c, this.a);
            this.k.setBackgroundColor(0);
            this.k.setWillNotCacheDrawing(false);
            this.b.setBackgroundColor(0);
            this.b.addView(this.k);
            b(this.k, appLovinAdSize);
            if (!this.t) {
                a(this.n);
            }
            if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.eS)).booleanValue()) {
                a(new b());
            }
            this.t = true;
        } catch (Throwable th) {
            o.i("AppLovinAdView", "Failed to create AdView: " + th.getMessage());
        }
    }

    private void a(Runnable runnable) {
        AppLovinSdkUtils.runOnUiThread(runnable);
    }

    private void b() {
        o oVar = this.e;
        if (oVar != null) {
            oVar.b("AppLovinAdView", "Destroying...");
        }
        c cVar = this.k;
        if (cVar != null) {
            try {
                ViewParent parent = cVar.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(this.k);
                }
                this.k.removeAllViews();
                if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.eO)).booleanValue()) {
                    this.k.loadUrl("about:blank");
                    this.k.onPause();
                    this.k.destroyDrawingCache();
                }
            } catch (Throwable th) {
                this.e.a("AppLovinAdView", "Unable to destroy ad view", th);
            }
            this.k.destroy();
            this.k = null;
        }
        this.v = true;
    }

    /* access modifiers changed from: private */
    public static void b(View view, AppLovinAdSize appLovinAdSize) {
        if (view != null) {
            DisplayMetrics displayMetrics = view.getResources().getDisplayMetrics();
            int i2 = -1;
            int applyDimension = appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel()) ? -1 : appLovinAdSize.getWidth() == -1 ? displayMetrics.widthPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getWidth(), displayMetrics);
            if (!appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel())) {
                i2 = appLovinAdSize.getHeight() == -1 ? displayMetrics.heightPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getHeight(), displayMetrics);
            }
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            }
            layoutParams.width = applyDimension;
            layoutParams.height = i2;
            if (layoutParams instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
                layoutParams2.addRule(10);
                layoutParams2.addRule(9);
            }
            view.setLayoutParams(layoutParams);
        }
    }

    private void c() {
        a(new Runnable() {
            public void run() {
                if (AdViewControllerImpl.this.q != null) {
                    o b = AdViewControllerImpl.this.e;
                    b.b("AppLovinAdView", "Detaching expanded ad: " + AdViewControllerImpl.this.q.a());
                    AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                    k unused = adViewControllerImpl.r = adViewControllerImpl.q;
                    k unused2 = AdViewControllerImpl.this.q = (k) null;
                    AdViewControllerImpl adViewControllerImpl2 = AdViewControllerImpl.this;
                    adViewControllerImpl2.a(adViewControllerImpl2.f);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void d() {
        a(new Runnable() {
            public void run() {
                com.applovin.impl.sdk.ad.a aVar;
                if (AdViewControllerImpl.this.r != null || AdViewControllerImpl.this.q != null) {
                    if (AdViewControllerImpl.this.r != null) {
                        aVar = AdViewControllerImpl.this.r.a();
                        AdViewControllerImpl.this.r.dismiss();
                        k unused = AdViewControllerImpl.this.r = (k) null;
                    } else {
                        aVar = AdViewControllerImpl.this.q.a();
                        AdViewControllerImpl.this.q.dismiss();
                        k unused2 = AdViewControllerImpl.this.q = (k) null;
                    }
                    j.b(AdViewControllerImpl.this.z, aVar, (AppLovinAdView) AdViewControllerImpl.this.b);
                }
            }
        });
    }

    private void e() {
        com.applovin.impl.sdk.c.d dVar = this.h;
        if (dVar != null) {
            dVar.c();
            this.h = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.q == null && this.r == null) {
            o oVar = this.e;
            oVar.b("AppLovinAdView", "Ad: " + this.o + " closed.");
            a(this.n);
            j.b(this.y, this.o);
            this.o = null;
        } else if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.cp)).booleanValue()) {
            contractAd();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final int i2) {
        if (!this.v) {
            a(this.n);
        }
        a(new Runnable() {
            public void run() {
                try {
                    if (AdViewControllerImpl.this.x != null) {
                        AdViewControllerImpl.this.x.failedToReceiveAd(i2);
                    }
                } catch (Throwable th) {
                    o.c("AppLovinAdView", "Exception while running app load  callback", th);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(final AppLovinAd appLovinAd) {
        if (appLovinAd != null) {
            this.w = true;
            if (!this.v) {
                renderAd(appLovinAd);
            } else {
                this.s.set(appLovinAd);
                this.e.b("AppLovinAdView", "Ad view has paused when an ad was received, ad saved for later");
            }
            a(new Runnable() {
                public void run() {
                    try {
                        if (AdViewControllerImpl.this.x != null) {
                            AdViewControllerImpl.this.x.adReceived(appLovinAd);
                        }
                    } catch (Throwable th) {
                        o.i("AppLovinAdView", "Exception while running ad load callback: " + th.getMessage());
                    }
                }
            });
            return;
        }
        this.e.e("AppLovinAdView", "No provided when to the view controller");
        a(-1);
    }

    /* access modifiers changed from: package-private */
    public void a(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, Uri uri, PointF pointF) {
        String str;
        o oVar;
        j.a(this.A, appLovinAd);
        if (appLovinAdView == null) {
            oVar = this.e;
            str = "Unable to process ad click - AppLovinAdView destroyed prematurely";
        } else if (appLovinAd instanceof f) {
            this.d.trackAndLaunchClick(appLovinAd, appLovinAdView, this, uri, pointF);
            return;
        } else {
            oVar = this.e;
            str = "Unable to process ad click - EmptyAd is not supported.";
        }
        oVar.e("AppLovinAdView", str);
    }

    public void contractAd() {
        a(new Runnable() {
            public void run() {
                AdViewControllerImpl.this.d();
                if (AdViewControllerImpl.this.b != null && AdViewControllerImpl.this.k != null && AdViewControllerImpl.this.k.getParent() == null) {
                    AdViewControllerImpl.this.b.addView(AdViewControllerImpl.this.k);
                    AdViewControllerImpl.b(AdViewControllerImpl.this.k, AdViewControllerImpl.this.o.getSize());
                }
            }
        });
    }

    public void destroy() {
        if (!(this.k == null || this.q == null)) {
            contractAd();
        }
        b();
    }

    public void dismissInterstitialIfRequired() {
        if ((this.a instanceof m) && (this.o instanceof f)) {
            boolean z2 = ((f) this.o).E() == f.a.DISMISS;
            m mVar = (m) this.a;
            if (z2 && mVar.getPoststitialWasDisplayed()) {
                mVar.dismiss();
            }
        }
    }

    public void expandAd(final PointF pointF) {
        a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.i):android.app.Activity
             arg types: [com.applovin.impl.adview.c, com.applovin.impl.sdk.i]
             candidates:
              com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
              com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
              com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.i):com.applovin.impl.sdk.ad.d
              com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.i):com.applovin.sdk.AppLovinAd
              com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
              com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
              com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.i):java.util.List<java.lang.Class>
              com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.i):void
              com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.i):void
              com.applovin.impl.sdk.utils.p.a(long, long):boolean
              com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
              com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
              com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
              com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.i):android.app.Activity */
            public void run() {
                if (AdViewControllerImpl.this.q == null && (AdViewControllerImpl.this.o instanceof com.applovin.impl.sdk.ad.a) && AdViewControllerImpl.this.k != null) {
                    com.applovin.impl.sdk.ad.a aVar = (com.applovin.impl.sdk.ad.a) AdViewControllerImpl.this.o;
                    Activity a2 = AdViewControllerImpl.this.a instanceof Activity ? (Activity) AdViewControllerImpl.this.a : p.a((View) AdViewControllerImpl.this.k, AdViewControllerImpl.this.c);
                    if (a2 != null) {
                        if (AdViewControllerImpl.this.b != null) {
                            AdViewControllerImpl.this.b.removeView(AdViewControllerImpl.this.k);
                        }
                        AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                        k unused = adViewControllerImpl.q = new k(aVar, adViewControllerImpl.k, a2, AdViewControllerImpl.this.c);
                        AdViewControllerImpl.this.q.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            public void onDismiss(DialogInterface dialogInterface) {
                                AdViewControllerImpl.this.contractAd();
                            }
                        });
                        AdViewControllerImpl.this.q.show();
                        j.a(AdViewControllerImpl.this.z, AdViewControllerImpl.this.o, (AppLovinAdView) AdViewControllerImpl.this.b);
                        if (AdViewControllerImpl.this.h != null) {
                            AdViewControllerImpl.this.h.d();
                            return;
                        }
                        return;
                    }
                    o.i("AppLovinAdView", "Unable to expand ad. No Activity found.");
                    Uri f = aVar.f();
                    if (f != null && ((Boolean) AdViewControllerImpl.this.c.a(com.applovin.impl.sdk.b.c.cw)).booleanValue()) {
                        AdViewControllerImpl.this.d.trackAndLaunchClick(aVar, AdViewControllerImpl.this.getParentView(), AdViewControllerImpl.this, f, pointF);
                        if (AdViewControllerImpl.this.h != null) {
                            AdViewControllerImpl.this.h.b();
                        }
                    }
                    AdViewControllerImpl.this.k.a("javascript:al_onFailedExpand();");
                }
            }
        });
    }

    public AppLovinAdViewEventListener getAdViewEventListener() {
        return this.z;
    }

    public c getAdWebView() {
        return this.k;
    }

    public AppLovinAd getCurrentAd() {
        return this.o;
    }

    public AppLovinAdView getParentView() {
        return (AppLovinAdView) this.b;
    }

    public i getSdk() {
        return this.c;
    }

    public AppLovinAdSize getSize() {
        return this.f;
    }

    public String getZoneId() {
        return this.g;
    }

    public void initializeAdView(AppLovinAdView appLovinAdView, Context context, AppLovinAdSize appLovinAdSize, String str, AppLovinSdk appLovinSdk, AttributeSet attributeSet) {
        if (appLovinAdView == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (context == null) {
            o.i("AppLovinAdView", "Unable to build AppLovinAdView: no context provided. Please use a different constructor for this view.");
        } else {
            if (appLovinAdSize == null && (appLovinAdSize = com.applovin.impl.sdk.utils.b.a(attributeSet)) == null) {
                appLovinAdSize = AppLovinAdSize.BANNER;
            }
            AppLovinAdSize appLovinAdSize2 = appLovinAdSize;
            if (appLovinSdk == null) {
                appLovinSdk = AppLovinSdk.getInstance(context);
            }
            if (appLovinSdk != null && !appLovinSdk.hasCriticalErrors()) {
                a(appLovinAdView, p.a(appLovinSdk), appLovinAdSize2, str, context);
                if (com.applovin.impl.sdk.utils.b.b(attributeSet)) {
                    loadNextAd();
                }
            }
        }
    }

    public boolean isAdReadyToDisplay() {
        return !TextUtils.isEmpty(this.g) ? this.d.hasPreloadedAdForZoneId(this.g) : this.d.hasPreloadedAd(this.f);
    }

    public boolean isAutoDestroy() {
        return this.u;
    }

    public void loadNextAd() {
        if (this.c == null || this.j == null || this.a == null || !this.t) {
            o.g("AppLovinAdView", "Unable to load next ad: AppLovinAdView is not initialized.");
        } else {
            this.d.loadNextAd(this.g, this.f, this.j);
        }
    }

    public void onAdHtmlLoaded(WebView webView) {
        if (this.o instanceof f) {
            webView.setVisibility(0);
            try {
                if (this.o != this.p && this.y != null) {
                    this.p = this.o;
                    j.a(this.y, this.o);
                }
            } catch (Throwable th) {
                o.c("AppLovinAdView", "Exception while notifying ad display listener", th);
            }
        }
    }

    public void onDetachedFromWindow() {
        if (this.t) {
            if (this.o != this.l) {
                j.b(this.y, this.o);
            }
            if (this.k == null || this.q == null) {
                this.e.b("AppLovinAdView", "onDetachedFromWindowCalled without an expanded ad present");
            } else {
                this.e.b("AppLovinAdView", "onDetachedFromWindowCalled with expanded ad present");
                if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.co)).booleanValue()) {
                    contractAd();
                } else {
                    c();
                }
            }
            if (this.u) {
                b();
            }
        }
    }

    public void onVisibilityChanged(int i2) {
        if (!this.t || !this.u) {
            return;
        }
        if (i2 == 8 || i2 == 4) {
            pause();
        } else if (i2 == 0) {
            resume();
        }
    }

    public void pause() {
        if (this.t && !this.v) {
            AppLovinAd appLovinAd = this.o;
            renderAd(this.l);
            if (appLovinAd != null) {
                this.s.set(appLovinAd);
            }
            this.v = true;
        }
    }

    public void renderAd(AppLovinAd appLovinAd) {
        renderAd(appLovinAd, null);
    }

    public void renderAd(AppLovinAd appLovinAd, String str) {
        String str2;
        if (appLovinAd != null) {
            p.b(appLovinAd, this.c);
            if (this.t) {
                AppLovinAd a2 = p.a(appLovinAd, this.c);
                if (a2 == null || a2 == this.o) {
                    o oVar = this.e;
                    if (a2 == null) {
                        str2 = "Unable to render ad. Ad is null. Internal inconsistency error.";
                    } else {
                        str2 = "Ad #" + a2.getAdIdNumber() + " is already showing, ignoring";
                    }
                    oVar.d("AppLovinAdView", str2);
                    return;
                }
                this.e.b("AppLovinAdView", "Rendering ad #" + a2.getAdIdNumber() + " (" + a2.getSize() + ")");
                if (!(this.o instanceof h)) {
                    j.b(this.y, this.o);
                    if (!(a2 instanceof h) && a2.getSize() != AppLovinAdSize.INTERSTITIAL) {
                        e();
                    }
                }
                this.s.set(null);
                this.p = null;
                this.o = a2;
                if ((appLovinAd instanceof f) && !this.v && (this.f == AppLovinAdSize.BANNER || this.f == AppLovinAdSize.MREC || this.f == AppLovinAdSize.LEADER)) {
                    this.c.o().trackImpression((f) appLovinAd);
                }
                boolean z2 = a2 instanceof h;
                if (!z2 && this.q != null) {
                    if (((Boolean) this.c.a(com.applovin.impl.sdk.b.c.cn)).booleanValue()) {
                        d();
                        this.e.b("AppLovinAdView", "Fade out the old ad scheduled");
                    } else {
                        c();
                    }
                }
                if (!z2 || (this.q == null && this.r == null)) {
                    a(this.m);
                } else {
                    this.e.b("AppLovinAdView", "Ignoring empty ad render with expanded ad");
                }
            } else {
                o.g("AppLovinAdView", "Unable to render ad: AppLovinAdView is not initialized.");
            }
        } else {
            throw new IllegalArgumentException("No ad specified");
        }
    }

    public void resume() {
        if (this.t) {
            AppLovinAd andSet = this.s.getAndSet(null);
            if (andSet != null) {
                renderAd(andSet);
            }
            this.v = false;
        }
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.A = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.y = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.x = appLovinAdLoadListener;
    }

    public void setAdVideoPlaybackListener(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
    }

    public void setAdViewEventListener(AppLovinAdViewEventListener appLovinAdViewEventListener) {
        this.z = appLovinAdViewEventListener;
    }

    public void setAutoDestroy(boolean z2) {
        this.u = z2;
    }

    public void setStatsManagerHelper(com.applovin.impl.sdk.c.d dVar) {
        c cVar = this.k;
        if (cVar != null) {
            cVar.a(dVar);
        }
    }
}
