package com.applovin.impl.adview;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import java.lang.ref.WeakReference;

public class v extends WebViewClient {
    private final o a;
    private WeakReference<a> b;

    public interface a {
        void a(u uVar);

        void b(u uVar);

        void c(u uVar);
    }

    public v(i iVar) {
        this.a = iVar.v();
    }

    private void a(WebView webView, String str) {
        o oVar = this.a;
        oVar.c("WebViewButtonClient", "Processing click on ad URL \"" + str + "\"");
        if (str != null && (webView instanceof u)) {
            u uVar = (u) webView;
            Uri parse = Uri.parse(str);
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            a aVar = this.b.get();
            if ("applovin".equalsIgnoreCase(scheme) && "com.applovin.sdk".equalsIgnoreCase(host) && aVar != null) {
                if ("/track_click".equals(path)) {
                    aVar.a(uVar);
                } else if ("/close_ad".equals(path)) {
                    aVar.b(uVar);
                } else if ("/skip_ad".equals(path)) {
                    aVar.c(uVar);
                } else {
                    o oVar2 = this.a;
                    oVar2.d("WebViewButtonClient", "Unknown URL: " + str);
                    o oVar3 = this.a;
                    oVar3.d("WebViewButtonClient", "Path: " + path);
                }
            }
        }
    }

    public void a(WeakReference<a> weakReference) {
        this.b = weakReference;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a(webView, str);
        return true;
    }
}
