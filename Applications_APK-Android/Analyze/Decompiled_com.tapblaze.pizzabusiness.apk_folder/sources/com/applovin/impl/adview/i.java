package com.applovin.impl.adview;

import android.os.Handler;
import com.applovin.impl.sdk.o;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class i {
    /* access modifiers changed from: private */
    public final o a;
    private final Handler b;
    private final Set<b> c = new HashSet();
    /* access modifiers changed from: private */
    public final AtomicInteger d = new AtomicInteger();

    interface a {
        void a();

        boolean b();
    }

    private static class b {
        private final String a;
        private final a b;
        private final long c;

        private b(String str, long j, a aVar) {
            this.a = str;
            this.c = j;
            this.b = aVar;
        }

        /* access modifiers changed from: private */
        public String a() {
            return this.a;
        }

        /* access modifiers changed from: private */
        public long b() {
            return this.c;
        }

        /* access modifiers changed from: private */
        public a c() {
            return this.b;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            String str = this.a;
            String str2 = ((b) obj).a;
            return str != null ? str.equalsIgnoreCase(str2) : str2 == null;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "CountdownProxy{identifier='" + this.a + '\'' + ", countdownStepMillis=" + this.c + '}';
        }
    }

    public i(Handler handler, com.applovin.impl.sdk.i iVar) {
        if (handler == null) {
            throw new IllegalArgumentException("No handler specified.");
        } else if (iVar != null) {
            this.b = handler;
            this.a = iVar.v();
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    /* access modifiers changed from: private */
    public void a(final b bVar, final int i) {
        this.b.postDelayed(new Runnable() {
            public void run() {
                a b2 = bVar.c();
                if (!b2.b()) {
                    o b3 = i.this.a;
                    b3.b("CountdownManager", "Ending countdown for " + bVar.a());
                } else if (i.this.d.get() == i) {
                    try {
                        b2.a();
                    } catch (Throwable th) {
                        o b4 = i.this.a;
                        b4.b("CountdownManager", "Encountered error on countdown step for: " + bVar.a(), th);
                    }
                    i.this.a(bVar, i);
                } else {
                    o b5 = i.this.a;
                    b5.d("CountdownManager", "Killing duplicate countdown from previous generation: " + bVar.a());
                }
            }
        }, bVar.b());
    }

    public void a() {
        HashSet<b> hashSet = new HashSet<>(this.c);
        o oVar = this.a;
        oVar.b("CountdownManager", "Starting " + hashSet.size() + " countdowns...");
        int incrementAndGet = this.d.incrementAndGet();
        for (b bVar : hashSet) {
            o oVar2 = this.a;
            oVar2.b("CountdownManager", "Starting countdown: " + bVar.a() + " for generation " + incrementAndGet + "...");
            a(bVar, incrementAndGet);
        }
    }

    public void a(String str, long j, a aVar) {
        if (j <= 0) {
            throw new IllegalArgumentException("Invalid step specified.");
        } else if (this.b != null) {
            o oVar = this.a;
            oVar.b("CountdownManager", "Adding countdown: " + str);
            this.c.add(new b(str, j, aVar));
        } else {
            throw new IllegalArgumentException("No handler specified.");
        }
    }

    public void b() {
        this.a.b("CountdownManager", "Removing all countdowns...");
        c();
        this.c.clear();
    }

    public void c() {
        this.a.b("CountdownManager", "Stopping countdowns...");
        this.d.incrementAndGet();
        this.b.removeCallbacksAndMessages(null);
    }
}
