package com.applovin.impl.adview;

import android.webkit.WebView;
import android.webkit.WebViewRenderProcess;
import android.webkit.WebViewRenderProcessClient;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import com.applovin.sdk.AppLovinAd;

class e extends WebViewRenderProcessClient {
    private final i a;

    e(i iVar) {
        this.a = iVar;
    }

    public void onRenderProcessResponsive(WebView webView, WebViewRenderProcess webViewRenderProcess) {
    }

    public void onRenderProcessUnresponsive(WebView webView, WebViewRenderProcess webViewRenderProcess) {
        if (webView instanceof c) {
            AppLovinAd a2 = ((c) webView).a();
            if (a2 instanceof AppLovinAdBase) {
                this.a.X().a((AppLovinAdBase) a2).a(b.F).a();
            }
            o v = this.a.v();
            v.e("AdWebViewRenderProcessClient", "WebView render process unresponsive for ad: " + a2);
        }
    }
}
