package com.applovin.impl.adview;

import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import cz.msebera.android.httpclient.HttpStatus;
import org.json.JSONObject;

public class s {
    private final int a;
    private final int b;
    private final int c;
    private final int d;
    private final boolean e;
    private final int f;
    private final int g;
    private final int h;
    private final float i;
    private final float j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean
     arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.i):float
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.i):long
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.i):java.util.List
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.i):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.i):float
     arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.i):long
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.i):java.lang.Boolean
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.i):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.i):java.util.List
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.i):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.i):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.i):float */
    public s(JSONObject jSONObject, i iVar) {
        o v = iVar.v();
        v.c("VideoButtonProperties", "Updating video button properties with JSON = " + com.applovin.impl.sdk.utils.i.d(jSONObject));
        this.a = com.applovin.impl.sdk.utils.i.b(jSONObject, "width", 64, iVar);
        this.b = com.applovin.impl.sdk.utils.i.b(jSONObject, "height", 7, iVar);
        this.c = com.applovin.impl.sdk.utils.i.b(jSONObject, "margin", 20, iVar);
        this.d = com.applovin.impl.sdk.utils.i.b(jSONObject, "gravity", 85, iVar);
        this.e = com.applovin.impl.sdk.utils.i.a(jSONObject, "tap_to_fade", (Boolean) false, iVar).booleanValue();
        this.f = com.applovin.impl.sdk.utils.i.b(jSONObject, "tap_to_fade_duration_milliseconds", (int) HttpStatus.SC_INTERNAL_SERVER_ERROR, iVar);
        this.g = com.applovin.impl.sdk.utils.i.b(jSONObject, "fade_in_duration_milliseconds", (int) HttpStatus.SC_INTERNAL_SERVER_ERROR, iVar);
        this.h = com.applovin.impl.sdk.utils.i.b(jSONObject, "fade_out_duration_milliseconds", (int) HttpStatus.SC_INTERNAL_SERVER_ERROR, iVar);
        this.i = com.applovin.impl.sdk.utils.i.a(jSONObject, "fade_in_delay_seconds", 1.0f, iVar);
        this.j = com.applovin.impl.sdk.utils.i.a(jSONObject, "fade_out_delay_seconds", 6.0f, iVar);
    }

    public int a() {
        return this.a;
    }

    public int b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }

    public int d() {
        return this.d;
    }

    public boolean e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        s sVar = (s) obj;
        return this.a == sVar.a && this.b == sVar.b && this.c == sVar.c && this.d == sVar.d && this.e == sVar.e && this.f == sVar.f && this.g == sVar.g && this.h == sVar.h && Float.compare(sVar.i, this.i) == 0 && Float.compare(sVar.j, this.j) == 0;
    }

    public long f() {
        return (long) this.f;
    }

    public long g() {
        return (long) this.g;
    }

    public long h() {
        return (long) this.h;
    }

    public int hashCode() {
        int i2 = ((((((((((((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + (this.e ? 1 : 0)) * 31) + this.f) * 31) + this.g) * 31) + this.h) * 31;
        float f2 = this.i;
        int i3 = 0;
        int floatToIntBits = (i2 + (f2 != 0.0f ? Float.floatToIntBits(f2) : 0)) * 31;
        float f3 = this.j;
        if (f3 != 0.0f) {
            i3 = Float.floatToIntBits(f3);
        }
        return floatToIntBits + i3;
    }

    public float i() {
        return this.i;
    }

    public float j() {
        return this.j;
    }

    public String toString() {
        return "VideoButtonProperties{widthPercentOfScreen=" + this.a + ", heightPercentOfScreen=" + this.b + ", margin=" + this.c + ", gravity=" + this.d + ", tapToFade=" + this.e + ", tapToFadeDurationMillis=" + this.f + ", fadeInDurationMillis=" + this.g + ", fadeOutDurationMillis=" + this.h + ", fadeInDelay=" + this.i + ", fadeOutDelay=" + this.j + '}';
    }
}
