package com.applovin.impl.adview;

import android.content.Context;
import android.graphics.PointF;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.i;

public class AppLovinTouchToClickListener implements View.OnTouchListener {
    private final long a;
    private final int b;
    private long c;
    private PointF d;
    private final Context e;
    private final OnClickListener f;

    public interface OnClickListener {
        void onClick(View view, PointF pointF);
    }

    public AppLovinTouchToClickListener(i iVar, Context context, OnClickListener onClickListener) {
        this.a = ((Long) iVar.a(c.aB)).longValue();
        this.b = ((Integer) iVar.a(c.aC)).intValue();
        this.e = context;
        this.f = onClickListener;
    }

    private float a(float f2) {
        return f2 / this.e.getResources().getDisplayMetrics().density;
    }

    private float a(PointF pointF, PointF pointF2) {
        float f2 = pointF.x - pointF2.x;
        float f3 = pointF.y - pointF2.y;
        return a((float) Math.sqrt((double) ((f2 * f2) + (f3 * f3))));
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i;
        int action = motionEvent.getAction();
        if (action == 0) {
            this.c = SystemClock.elapsedRealtime();
            this.d = new PointF(motionEvent.getX(), motionEvent.getY());
        } else if (action == 1) {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.c;
            float a2 = a(this.d, new PointF(motionEvent.getX(), motionEvent.getY()));
            long j = this.a;
            if ((j < 0 || elapsedRealtime < j) && ((i = this.b) < 0 || a2 < ((float) i))) {
                this.f.onClick(view, new PointF(motionEvent.getRawX(), motionEvent.getRawY()));
            }
        }
        return true;
    }
}
