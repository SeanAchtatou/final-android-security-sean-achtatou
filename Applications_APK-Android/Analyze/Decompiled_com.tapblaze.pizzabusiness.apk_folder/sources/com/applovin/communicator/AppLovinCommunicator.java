package com.applovin.communicator;

import android.content.Context;
import com.applovin.impl.communicator.MessagingServiceImpl;
import com.applovin.impl.communicator.a;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.o;
import java.util.Collections;
import java.util.List;

public final class AppLovinCommunicator {
    private static AppLovinCommunicator a;
    private static final Object b = new Object();
    private i c;
    private final a d;
    private final MessagingServiceImpl e;

    private AppLovinCommunicator(Context context) {
        this.d = new a(context);
        this.e = new MessagingServiceImpl(context);
    }

    public static AppLovinCommunicator getInstance(Context context) {
        synchronized (b) {
            if (a == null) {
                a = new AppLovinCommunicator(context.getApplicationContext());
            }
        }
        return a;
    }

    public void a(i iVar) {
        o.f("AppLovinCommunicator", "Attaching SDK instance: " + iVar + "...");
        this.c = iVar;
    }

    public AppLovinCommunicatorMessagingService getMessagingService() {
        return this.e;
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        subscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String next : list) {
            o.f("AppLovinCommunicator", "Subscribing " + appLovinCommunicatorSubscriber + " to topic: " + next);
            if (this.d.a(appLovinCommunicatorSubscriber, next)) {
                o.f("AppLovinCommunicator", "Subscribed " + appLovinCommunicatorSubscriber + " to topic: " + next);
                this.e.maybeFlushStickyMessages(next);
            } else {
                o.f("AppLovinCommunicator", "Unable to subscribe " + appLovinCommunicatorSubscriber + " to topic: " + next);
            }
        }
    }

    public String toString() {
        return "AppLovinCommunicator{sdk=" + this.c + '}';
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        unsubscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String next : list) {
            o.f("AppLovinCommunicator", "Unsubscribing " + appLovinCommunicatorSubscriber + " from topic: " + next);
            this.d.b(appLovinCommunicatorSubscriber, next);
        }
    }
}
