package com.ironsource.eventsmodule;

import android.os.AsyncTask;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class EventsSender extends AsyncTask<Object, Void, Boolean> {
    private final String APPLICATION_JSON = "application/json";
    private final String CONTENT_TYPE_FIELD = "Content-Type";
    private final String SERVER_REQUEST_ENCODING = "UTF-8";
    private final String SERVER_REQUEST_METHOD = "POST";
    private final int SERVER_REQUEST_TIMEOUT = 15000;
    private ArrayList extraData;
    private IEventsSenderResultListener mResultListener;

    public EventsSender() {
    }

    public EventsSender(IEventsSenderResultListener iEventsSenderResultListener) {
        this.mResultListener = iEventsSenderResultListener;
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Object... objArr) {
        try {
            boolean z = true;
            URL url = new URL((String) objArr[1]);
            this.extraData = (ArrayList) objArr[2];
            HttpURLConnection httpURLConnection = (HttpURLConnection) ((URLConnection) FirebasePerfUrlConnection.instrument(url.openConnection()));
            httpURLConnection.setReadTimeout(15000);
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            bufferedWriter.write((String) objArr[0]);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            int responseCode = httpURLConnection.getResponseCode();
            httpURLConnection.disconnect();
            if (responseCode != 200) {
                z = false;
            }
            return Boolean.valueOf(z);
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean bool) {
        IEventsSenderResultListener iEventsSenderResultListener = this.mResultListener;
        if (iEventsSenderResultListener != null) {
            iEventsSenderResultListener.onEventsSenderResult(this.extraData, bool.booleanValue());
        }
    }
}
