package com.ironsource.eventsmodule;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import cz.msebera.android.httpclient.HttpStatus;

public class DataBaseEventsStorage extends SQLiteOpenHelper implements IEventsStorageHelper {
    private static final String COMMA_SEP = ",";
    private static final String TYPE_INTEGER = " INTEGER";
    private static final String TYPE_TEXT = " TEXT";
    private static DataBaseEventsStorage mInstance;
    private final int DB_OPEN_BACKOFF_TIME = HttpStatus.SC_BAD_REQUEST;
    private final int DB_RETRY_NUM = 4;
    private final String SQL_CREATE_ENTRIES = "CREATE TABLE events (_id INTEGER PRIMARY KEY,eventid INTEGER,timestamp INTEGER,type TEXT,data TEXT )";
    private final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS events";

    public DataBaseEventsStorage(Context context, String str, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, i);
    }

    public static synchronized DataBaseEventsStorage getInstance(Context context, String str, int i) {
        DataBaseEventsStorage dataBaseEventsStorage;
        synchronized (DataBaseEventsStorage.class) {
            if (mInstance == null) {
                mInstance = new DataBaseEventsStorage(context, str, i);
            }
            dataBaseEventsStorage = mInstance;
        }
        return dataBaseEventsStorage;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0034, code lost:
        if (r0.isOpen() != false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0036, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004b, code lost:
        if (r0.isOpen() != false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0061, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0047 A[SYNTHETIC, Splitter:B:28:0x0047] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void saveEvents(java.util.List<com.ironsource.eventsmodule.EventData> r5, java.lang.String r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            if (r5 == 0) goto L_0x0060
            boolean r0 = r5.isEmpty()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x000a
            goto L_0x0060
        L_0x000a:
            r0 = 1
            r1 = 0
            android.database.sqlite.SQLiteDatabase r0 = r4.getDataBaseWithRetries(r0)     // Catch:{ all -> 0x003c }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ all -> 0x003a }
        L_0x0014:
            boolean r2 = r5.hasNext()     // Catch:{ all -> 0x003a }
            if (r2 == 0) goto L_0x002e
            java.lang.Object r2 = r5.next()     // Catch:{ all -> 0x003a }
            com.ironsource.eventsmodule.EventData r2 = (com.ironsource.eventsmodule.EventData) r2     // Catch:{ all -> 0x003a }
            android.content.ContentValues r2 = r4.getContentValuesForEvent(r2, r6)     // Catch:{ all -> 0x003a }
            if (r0 == 0) goto L_0x0014
            if (r2 == 0) goto L_0x0014
            java.lang.String r3 = "events"
            r0.insert(r3, r1, r2)     // Catch:{ all -> 0x003a }
            goto L_0x0014
        L_0x002e:
            if (r0 == 0) goto L_0x004e
            boolean r5 = r0.isOpen()     // Catch:{ all -> 0x005d }
            if (r5 == 0) goto L_0x004e
        L_0x0036:
            r0.close()     // Catch:{ all -> 0x005d }
            goto L_0x004e
        L_0x003a:
            r5 = move-exception
            goto L_0x003e
        L_0x003c:
            r5 = move-exception
            r0 = r1
        L_0x003e:
            java.lang.String r6 = "IronSource"
            java.lang.String r1 = "Exception while saving events: "
            android.util.Log.e(r6, r1, r5)     // Catch:{ all -> 0x0050 }
            if (r0 == 0) goto L_0x004e
            boolean r5 = r0.isOpen()     // Catch:{ all -> 0x005d }
            if (r5 == 0) goto L_0x004e
            goto L_0x0036
        L_0x004e:
            monitor-exit(r4)
            return
        L_0x0050:
            r5 = move-exception
            if (r0 == 0) goto L_0x005c
            boolean r6 = r0.isOpen()     // Catch:{ all -> 0x005d }
            if (r6 == 0) goto L_0x005c
            r0.close()     // Catch:{ all -> 0x005d }
        L_0x005c:
            throw r5     // Catch:{ all -> 0x005d }
        L_0x005d:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0060:
            monitor-exit(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.eventsmodule.DataBaseEventsStorage.saveEvents(java.util.List, java.lang.String):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v0, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v1, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v2, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v3, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v4, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0071, code lost:
        if (r11.isOpen() != false) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0073, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0093, code lost:
        if (r13 != false) goto L_0x0073;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0084 A[SYNTHETIC, Splitter:B:31:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008f A[Catch:{ all -> 0x0098 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.ironsource.eventsmodule.EventData> loadEvents(java.lang.String r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x00b0 }
            r0.<init>()     // Catch:{ all -> 0x00b0 }
            r1 = 0
            r2 = 0
            android.database.sqlite.SQLiteDatabase r11 = r12.getDataBaseWithRetries(r1)     // Catch:{ all -> 0x0079 }
            java.lang.String r6 = "type = ?"
            r3 = 1
            java.lang.String[] r7 = new java.lang.String[r3]     // Catch:{ all -> 0x0077 }
            r7[r1] = r13     // Catch:{ all -> 0x0077 }
            java.lang.String r10 = "timestamp ASC"
            java.lang.String r4 = "events"
            r5 = 0
            r8 = 0
            r9 = 0
            r3 = r11
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0077 }
            int r13 = r2.getCount()     // Catch:{ all -> 0x0077 }
            if (r13 <= 0) goto L_0x0060
            r2.moveToFirst()     // Catch:{ all -> 0x0077 }
        L_0x0028:
            boolean r13 = r2.isAfterLast()     // Catch:{ all -> 0x0077 }
            if (r13 != 0) goto L_0x005d
            java.lang.String r13 = "eventid"
            int r13 = r2.getColumnIndex(r13)     // Catch:{ all -> 0x0077 }
            int r13 = r2.getInt(r13)     // Catch:{ all -> 0x0077 }
            java.lang.String r1 = "timestamp"
            int r1 = r2.getColumnIndex(r1)     // Catch:{ all -> 0x0077 }
            long r3 = r2.getLong(r1)     // Catch:{ all -> 0x0077 }
            java.lang.String r1 = "data"
            int r1 = r2.getColumnIndex(r1)     // Catch:{ all -> 0x0077 }
            java.lang.String r1 = r2.getString(r1)     // Catch:{ all -> 0x0077 }
            com.ironsource.eventsmodule.EventData r5 = new com.ironsource.eventsmodule.EventData     // Catch:{ all -> 0x0077 }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ all -> 0x0077 }
            r6.<init>(r1)     // Catch:{ all -> 0x0077 }
            r5.<init>(r13, r3, r6)     // Catch:{ all -> 0x0077 }
            r0.add(r5)     // Catch:{ all -> 0x0077 }
            r2.moveToNext()     // Catch:{ all -> 0x0077 }
            goto L_0x0028
        L_0x005d:
            r2.close()     // Catch:{ all -> 0x0077 }
        L_0x0060:
            if (r2 == 0) goto L_0x006b
            boolean r13 = r2.isClosed()     // Catch:{ all -> 0x00b0 }
            if (r13 != 0) goto L_0x006b
            r2.close()     // Catch:{ all -> 0x00b0 }
        L_0x006b:
            if (r11 == 0) goto L_0x0096
            boolean r13 = r11.isOpen()     // Catch:{ all -> 0x00b0 }
            if (r13 == 0) goto L_0x0096
        L_0x0073:
            r11.close()     // Catch:{ all -> 0x00b0 }
            goto L_0x0096
        L_0x0077:
            r13 = move-exception
            goto L_0x007b
        L_0x0079:
            r13 = move-exception
            r11 = r2
        L_0x007b:
            java.lang.String r1 = "IronSource"
            java.lang.String r3 = "Exception while loading events: "
            android.util.Log.e(r1, r3, r13)     // Catch:{ all -> 0x0098 }
            if (r2 == 0) goto L_0x008d
            boolean r13 = r2.isClosed()     // Catch:{ all -> 0x00b0 }
            if (r13 != 0) goto L_0x008d
            r2.close()     // Catch:{ all -> 0x00b0 }
        L_0x008d:
            if (r11 == 0) goto L_0x0096
            boolean r13 = r11.isOpen()     // Catch:{ all -> 0x00b0 }
            if (r13 == 0) goto L_0x0096
            goto L_0x0073
        L_0x0096:
            monitor-exit(r12)
            return r0
        L_0x0098:
            r13 = move-exception
            if (r2 == 0) goto L_0x00a4
            boolean r0 = r2.isClosed()     // Catch:{ all -> 0x00b0 }
            if (r0 != 0) goto L_0x00a4
            r2.close()     // Catch:{ all -> 0x00b0 }
        L_0x00a4:
            if (r11 == 0) goto L_0x00af
            boolean r0 = r11.isOpen()     // Catch:{ all -> 0x00b0 }
            if (r0 == 0) goto L_0x00af
            r11.close()     // Catch:{ all -> 0x00b0 }
        L_0x00af:
            throw r13     // Catch:{ all -> 0x00b0 }
        L_0x00b0:
            r13 = move-exception
            monitor-exit(r12)
            goto L_0x00b4
        L_0x00b3:
            throw r13
        L_0x00b4:
            goto L_0x00b3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.eventsmodule.DataBaseEventsStorage.loadEvents(java.lang.String):java.util.ArrayList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002d, code lost:
        if (r0.isOpen() != false) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        if (r0.isOpen() != false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void clearEvents(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            java.lang.String r1 = "type = ?"
            r2 = 1
            java.lang.String[] r3 = new java.lang.String[r2]     // Catch:{ all -> 0x003f }
            r4 = 0
            r3[r4] = r6     // Catch:{ all -> 0x003f }
            android.database.sqlite.SQLiteDatabase r0 = r5.getDataBaseWithRetries(r2)     // Catch:{ all -> 0x001f }
            java.lang.String r6 = "events"
            r0.delete(r6, r1, r3)     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x0030
            boolean r6 = r0.isOpen()     // Catch:{ all -> 0x003f }
            if (r6 == 0) goto L_0x0030
        L_0x001b:
            r0.close()     // Catch:{ all -> 0x003f }
            goto L_0x0030
        L_0x001f:
            r6 = move-exception
            java.lang.String r1 = "IronSource"
            java.lang.String r2 = "Exception while clearing events: "
            android.util.Log.e(r1, r2, r6)     // Catch:{ all -> 0x0032 }
            if (r0 == 0) goto L_0x0030
            boolean r6 = r0.isOpen()     // Catch:{ all -> 0x003f }
            if (r6 == 0) goto L_0x0030
            goto L_0x001b
        L_0x0030:
            monitor-exit(r5)
            return
        L_0x0032:
            r6 = move-exception
            if (r0 == 0) goto L_0x003e
            boolean r1 = r0.isOpen()     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x003e
            r0.close()     // Catch:{ all -> 0x003f }
        L_0x003e:
            throw r6     // Catch:{ all -> 0x003f }
        L_0x003f:
            r6 = move-exception
            monitor-exit(r5)
            goto L_0x0043
        L_0x0042:
            throw r6
        L_0x0043:
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.eventsmodule.DataBaseEventsStorage.clearEvents(java.lang.String):void");
    }

    private ContentValues getContentValuesForEvent(EventData eventData, String str) {
        if (eventData == null) {
            return null;
        }
        ContentValues contentValues = new ContentValues(4);
        contentValues.put(EventEntry.COLUMN_NAME_EVENT_ID, Integer.valueOf(eventData.getEventId()));
        contentValues.put("timestamp", Long.valueOf(eventData.getTimeStamp()));
        contentValues.put("type", str);
        contentValues.put("data", eventData.getAdditionalData());
        return contentValues;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE events (_id INTEGER PRIMARY KEY,eventid INTEGER,timestamp INTEGER,type TEXT,data TEXT )");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events");
        onCreate(sQLiteDatabase);
    }

    private synchronized SQLiteDatabase getDataBaseWithRetries(boolean z) throws Throwable {
        int i = 0;
        while (true) {
            if (z) {
                try {
                    return getWritableDatabase();
                } catch (Throwable th) {
                    throw th;
                }
            } else {
                return getReadableDatabase();
            }
        }
    }

    static abstract class EventEntry implements BaseColumns {
        public static final String COLUMN_NAME_DATA = "data";
        public static final String COLUMN_NAME_EVENT_ID = "eventid";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final int NUMBER_OF_COLUMNS = 4;
        public static final String TABLE_NAME = "events";

        EventEntry() {
        }
    }
}
