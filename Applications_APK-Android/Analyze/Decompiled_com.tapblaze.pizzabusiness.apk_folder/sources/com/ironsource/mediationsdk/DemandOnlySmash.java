package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class DemandOnlySmash {
    protected JSONObject mAdUnitSettings;
    protected AbstractAdapter mAdapter;
    protected AdapterConfig mAdapterConfig;
    int mLoadTimeoutSecs;
    private Timer mLoadTimer;
    private SMASH_STATE mState;
    private final Object mStateLock = new Object();
    private final Object mTimerLock = new Object();

    protected enum SMASH_STATE {
        NOT_LOADED,
        LOAD_IN_PROGRESS,
        LOADED,
        SHOW_IN_PROGRESS
    }

    public DemandOnlySmash(AdapterConfig adapterConfig, AbstractAdapter abstractAdapter) {
        this.mAdapterConfig = adapterConfig;
        this.mAdapter = abstractAdapter;
        this.mAdUnitSettings = adapterConfig.getAdUnitSetings();
        this.mState = SMASH_STATE.NOT_LOADED;
        this.mLoadTimer = null;
    }

    public void onResume(Activity activity) {
        this.mAdapter.onResume(activity);
    }

    public void onPause(Activity activity) {
        this.mAdapter.onPause(activity);
    }

    public void setConsent(boolean z) {
        this.mAdapter.setConsent(z);
    }

    public String getInstanceName() {
        return this.mAdapterConfig.getProviderName();
    }

    public String getSubProviderId() {
        return this.mAdapterConfig.getSubProviderId();
    }

    public Map<String, Object> getProviderEventData() {
        HashMap hashMap = new HashMap();
        try {
            String str = "";
            hashMap.put("providerAdapterVersion", this.mAdapter != null ? this.mAdapter.getVersion() : str);
            if (this.mAdapter != null) {
                str = this.mAdapter.getCoreSDKVersion();
            }
            hashMap.put("providerSDKVersion", str);
            hashMap.put("spId", this.mAdapterConfig.getSubProviderId());
            hashMap.put("provider", this.mAdapterConfig.getAdSourceNameForEvents());
            hashMap.put(IronSourceConstants.EVENTS_DEMAND_ONLY, 1);
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, "getProviderEventData " + getInstanceName() + ")", e);
        }
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    public boolean compareAndSetState(SMASH_STATE smash_state, SMASH_STATE smash_state2) {
        synchronized (this.mStateLock) {
            if (this.mState != smash_state) {
                return false;
            }
            setState(smash_state2);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public SMASH_STATE compareAndSetState(SMASH_STATE[] smash_stateArr, SMASH_STATE smash_state) {
        SMASH_STATE smash_state2;
        synchronized (this.mStateLock) {
            smash_state2 = this.mState;
            if (Arrays.asList(smash_stateArr).contains(this.mState)) {
                setState(smash_state);
            }
        }
        return smash_state2;
    }

    /* access modifiers changed from: package-private */
    public void setState(SMASH_STATE smash_state) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "DemandOnlySmash " + this.mAdapterConfig.getProviderName() + ": current state=" + this.mState + ", new state=" + smash_state, 0);
        synchronized (this.mStateLock) {
            this.mState = smash_state;
        }
    }

    /* access modifiers changed from: package-private */
    public String getStateString() {
        SMASH_STATE smash_state = this.mState;
        return smash_state == null ? "null" : smash_state.toString();
    }

    /* access modifiers changed from: package-private */
    public void startTimer(TimerTask timerTask) {
        synchronized (this.mTimerLock) {
            stopTimer();
            this.mLoadTimer = new Timer();
            this.mLoadTimer.schedule(timerTask, (long) (this.mLoadTimeoutSecs * 1000));
        }
    }

    /* access modifiers changed from: package-private */
    public void stopTimer() {
        synchronized (this.mTimerLock) {
            if (this.mLoadTimer != null) {
                this.mLoadTimer.cancel();
                this.mLoadTimer = null;
            }
        }
    }
}
