package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.sdk.DemandOnlyRvManagerListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class DemandOnlyRvManager implements DemandOnlyRvManagerListener {
    private String mAppKey;
    private ConcurrentHashMap<String, DemandOnlyRvSmash> mSmashes = new ConcurrentHashMap<>();

    DemandOnlyRvManager(Activity activity, List<ProviderSettings> list, RewardedVideoConfigurations rewardedVideoConfigurations, String str, String str2) {
        this.mAppKey = str;
        for (ProviderSettings next : list) {
            if (next.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME) || next.getProviderTypeForReflection().equalsIgnoreCase(IronSourceConstants.IRONSOURCE_CONFIG_NAME)) {
                AbstractAdapter loadAdapter = loadAdapter(next.getProviderInstanceName());
                if (loadAdapter != null) {
                    this.mSmashes.put(next.getSubProviderId(), new DemandOnlyRvSmash(activity, str, str2, next, this, rewardedVideoConfigurations.getRewardedVideoAdaptersSmartLoadTimeout(), loadAdapter));
                }
            } else {
                logInternal("cannot load " + next.getProviderTypeForReflection());
            }
        }
    }

    public void onResume(Activity activity) {
        if (activity != null) {
            for (DemandOnlyRvSmash onResume : this.mSmashes.values()) {
                onResume.onResume(activity);
            }
        }
    }

    public void onPause(Activity activity) {
        if (activity != null) {
            for (DemandOnlyRvSmash onPause : this.mSmashes.values()) {
                onPause.onPause(activity);
            }
        }
    }

    public void setConsent(boolean z) {
        for (DemandOnlyRvSmash consent : this.mSmashes.values()) {
            consent.setConsent(z);
        }
    }

    public void loadRewardedVideo(String str) {
        try {
            if (!this.mSmashes.containsKey(str)) {
                sendMediationEvent(1500, str);
                RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, ErrorBuilder.buildNonExistentInstanceError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                return;
            }
            DemandOnlyRvSmash demandOnlyRvSmash = this.mSmashes.get(str);
            sendProviderEvent(1001, demandOnlyRvSmash);
            demandOnlyRvSmash.loadRewardedVideo();
        } catch (Exception e) {
            logInternal("loadRewardedVideo exception " + e.getMessage());
            RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(str, ErrorBuilder.buildLoadFailedError("loadRewardedVideo exception"));
        }
    }

    public void showRewardedVideo(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(1500, str);
            RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(str, ErrorBuilder.buildNonExistentInstanceError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            return;
        }
        DemandOnlyRvSmash demandOnlyRvSmash = this.mSmashes.get(str);
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW, demandOnlyRvSmash);
        demandOnlyRvSmash.showRewardedVideo();
    }

    public boolean isRewardedVideoAvailable(String str) {
        if (!this.mSmashes.containsKey(str)) {
            sendMediationEvent(1500, str);
            return false;
        }
        DemandOnlyRvSmash demandOnlyRvSmash = this.mSmashes.get(str);
        if (demandOnlyRvSmash.isRewardedVideoAvailable()) {
            sendProviderEvent(IronSourceConstants.RV_INSTANCE_READY_TRUE, demandOnlyRvSmash);
            return true;
        }
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_READY_FALSE, demandOnlyRvSmash);
        return false;
    }

    public void onRewardedVideoLoadSuccess(DemandOnlyRvSmash demandOnlyRvSmash, long j) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoLoadSuccess");
        sendProviderEvent(1002, demandOnlyRvSmash, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoLoadSuccess(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdLoadFailed(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash, long j) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdLoadFailed error=" + ironSourceError);
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_LOAD_FAILED, demandOnlyRvSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}, new Object[]{"duration", Long.valueOf(j)}});
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdLoadFailed(demandOnlyRvSmash.getSubProviderId(), ironSourceError);
    }

    public void onRewardedVideoAdOpened(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdOpened");
        sendProviderEvent(1005, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdOpened(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdClosed(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdClosed");
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_CLOSED, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdClosed(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdShowFailed error=" + ironSourceError);
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW_FAILED, demandOnlyRvSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}});
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdShowFailed(demandOnlyRvSmash.getSubProviderId(), ironSourceError);
    }

    public void onRewardedVideoAdClicked(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdClicked");
        sendProviderEvent(1006, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdClicked(demandOnlyRvSmash.getSubProviderId());
    }

    public void onRewardedVideoAdVisible(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdVisible");
        sendProviderEvent(IronSourceConstants.RV_INSTANCE_VISIBLE, demandOnlyRvSmash);
    }

    public void onRewardedVideoAdRewarded(DemandOnlyRvSmash demandOnlyRvSmash) {
        logSmashCallback(demandOnlyRvSmash, "onRewardedVideoAdRewarded");
        Map<String, Object> providerEventData = demandOnlyRvSmash.getProviderEventData();
        if (!TextUtils.isEmpty(IronSourceObject.getInstance().getDynamicUserId())) {
            providerEventData.put(IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceObject.getInstance().getDynamicUserId());
        }
        if (IronSourceObject.getInstance().getRvServerParams() != null) {
            for (String next : IronSourceObject.getInstance().getRvServerParams().keySet()) {
                providerEventData.put("custom_" + next, IronSourceObject.getInstance().getRvServerParams().get(next));
            }
        }
        EventData eventData = new EventData(1010, new JSONObject(providerEventData));
        eventData.addToAdditionalData(IronSourceConstants.EVENTS_TRANS_ID, IronSourceUtils.getTransId("" + Long.toString(eventData.getTimeStamp()) + this.mAppKey + demandOnlyRvSmash.getInstanceName()));
        RewardedVideoEventsManager.getInstance().log(eventData);
        RVDemandOnlyListenerWrapper.getInstance().onRewardedVideoAdRewarded(demandOnlyRvSmash.getSubProviderId());
    }

    private void sendProviderEvent(int i, DemandOnlyRvSmash demandOnlyRvSmash) {
        sendProviderEvent(i, demandOnlyRvSmash, null);
    }

    private void sendProviderEvent(int i, DemandOnlyRvSmash demandOnlyRvSmash, Object[][] objArr) {
        Map<String, Object> providerEventData = demandOnlyRvSmash.getProviderEventData();
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "RV sendProviderEvent " + Log.getStackTraceString(e), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }

    private void sendMediationEvent(int i, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_DEMAND_ONLY, 1);
        if (str == null) {
            str = "";
        }
        hashMap.put("spId", str);
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    private AbstractAdapter loadAdapter(String str) {
        try {
            Class<?> cls = Class.forName("com.ironsource.adapters.ironsource.IronSourceAdapter");
            return (AbstractAdapter) cls.getMethod(IronSourceConstants.START_ADAPTER, String.class).invoke(cls, str);
        } catch (Exception unused) {
            return null;
        }
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "DemandOnlyRvManager " + str, 0);
    }

    private void logSmashCallback(DemandOnlyRvSmash demandOnlyRvSmash, String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "DemandOnlyRvManager " + demandOnlyRvSmash.getInstanceName() + " : " + str, 0);
    }
}
