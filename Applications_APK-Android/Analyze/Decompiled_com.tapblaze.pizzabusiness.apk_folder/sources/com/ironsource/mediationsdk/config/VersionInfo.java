package com.ironsource.mediationsdk.config;

public class VersionInfo {
    public static final String BUILD_DATE = "2019-11-25T16:00:37Z";
    public static final long BUILD_UNIX_TIME = 1574697637760L;
    public static final String GIT_DATE = "2019-11-25T15:57:13Z";
    public static final int GIT_REVISION = 5208;
    public static final String GIT_SHA = "ef63532103e25e5ce938410258ecf96b356d23c8";
    public static final String MAVEN_GROUP = "";
    public static final String MAVEN_NAME = "Android-SSP-unit-testing-develop-release-master-branches";
    public static final String VERSION = "6.11.0";
}
