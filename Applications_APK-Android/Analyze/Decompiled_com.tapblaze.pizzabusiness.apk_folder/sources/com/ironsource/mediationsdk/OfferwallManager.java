package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.OfferwallPlacement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.InternalOfferwallApi;
import com.ironsource.mediationsdk.sdk.InternalOfferwallListener;
import com.ironsource.mediationsdk.sdk.OfferwallAdapterApi;
import com.ironsource.mediationsdk.sdk.OfferwallListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

class OfferwallManager implements InternalOfferwallApi, InternalOfferwallListener {
    private final String TAG = getClass().getName();
    private Activity mActivity;
    private OfferwallAdapterApi mAdapter;
    private AtomicBoolean mAtomicShouldPerformInit = new AtomicBoolean(true);
    private String mCurrentPlacementName;
    private AtomicBoolean mIsOfferwallAvailable = new AtomicBoolean(false);
    private InternalOfferwallListener mListenersWrapper;
    private IronSourceLoggerManager mLoggerManager = IronSourceLoggerManager.getLogger();
    private ProviderSettings mProviderSettings;
    private ServerResponseWrapper mServerResponseWrapper;

    public void setOfferwallListener(OfferwallListener offerwallListener) {
    }

    public void showOfferwall() {
    }

    OfferwallManager() {
    }

    public synchronized void initOfferwall(Activity activity, String str, String str2) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
        ironSourceLoggerManager.log(ironSourceTag, this.TAG + ":initOfferwall(appKey: " + str + ", userId: " + str2 + ")", 1);
        this.mActivity = activity;
        this.mServerResponseWrapper = IronSourceObject.getInstance().getCurrentServerResponse();
        if (this.mServerResponseWrapper == null) {
            reportInitFail(ErrorBuilder.buildInitFailedError("Please check configurations for Offerwall adapters", IronSourceConstants.OFFERWALL_AD_UNIT));
            return;
        }
        this.mProviderSettings = this.mServerResponseWrapper.getProviderSettingsHolder().getProviderSettings(IronSourceConstants.SUPERSONIC_CONFIG_NAME);
        if (this.mProviderSettings == null) {
            reportInitFail(ErrorBuilder.buildInitFailedError("Please check configurations for Offerwall adapters", IronSourceConstants.OFFERWALL_AD_UNIT));
            return;
        }
        AbstractAdapter startOfferwallAdapter = startOfferwallAdapter();
        if (startOfferwallAdapter == null) {
            reportInitFail(ErrorBuilder.buildInitFailedError("Please check configurations for Offerwall adapters", IronSourceConstants.OFFERWALL_AD_UNIT));
            return;
        }
        setCustomParams(startOfferwallAdapter);
        startOfferwallAdapter.setLogListener(this.mLoggerManager);
        this.mAdapter = (OfferwallAdapterApi) startOfferwallAdapter;
        this.mAdapter.setInternalOfferwallListener(this);
        this.mAdapter.initOfferwall(activity, str, str2, this.mProviderSettings.getRewardedVideoSettings());
    }

    public void showOfferwall(String str) {
        String str2 = "OWManager:showOfferwall(" + str + ")";
        try {
            if (!IronSourceUtils.isNetworkConnected(this.mActivity)) {
                this.mListenersWrapper.onOfferwallShowFailed(ErrorBuilder.buildNoInternetConnectionShowFailError(IronSourceConstants.OFFERWALL_AD_UNIT));
                return;
            }
            this.mCurrentPlacementName = str;
            OfferwallPlacement offerwallPlacement = this.mServerResponseWrapper.getConfigurations().getOfferwallConfigurations().getOfferwallPlacement(str);
            if (offerwallPlacement == null) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "Placement is not valid, please make sure you are using the right placements, using the default placement.", 3);
                offerwallPlacement = this.mServerResponseWrapper.getConfigurations().getOfferwallConfigurations().getDefaultOfferwallPlacement();
                if (offerwallPlacement == null) {
                    this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "Default placement was not found, please make sure you are using the right placements.", 3);
                    return;
                }
            }
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, str2, 1);
            if (this.mIsOfferwallAvailable != null && this.mIsOfferwallAvailable.get() && this.mAdapter != null) {
                this.mAdapter.showOfferwall(String.valueOf(offerwallPlacement.getPlacementId()), this.mProviderSettings.getRewardedVideoSettings());
            }
        } catch (Exception e) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.INTERNAL, str2, e);
        }
    }

    public synchronized boolean isOfferwallAvailable() {
        boolean z;
        z = false;
        if (this.mIsOfferwallAvailable != null) {
            z = this.mIsOfferwallAvailable.get();
        }
        return z;
    }

    public void getOfferwallCredits() {
        OfferwallAdapterApi offerwallAdapterApi = this.mAdapter;
        if (offerwallAdapterApi != null) {
            offerwallAdapterApi.getOfferwallCredits();
        }
    }

    public void setInternalOfferwallListener(InternalOfferwallListener internalOfferwallListener) {
        this.mListenersWrapper = internalOfferwallListener;
    }

    public void onOfferwallAvailable(boolean z) {
        onOfferwallAvailable(z, null);
    }

    public void onOfferwallAvailable(boolean z, IronSourceError ironSourceError) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, "onOfferwallAvailable(isAvailable: " + z + ")", 1);
        if (z) {
            this.mIsOfferwallAvailable.set(true);
            InternalOfferwallListener internalOfferwallListener = this.mListenersWrapper;
            if (internalOfferwallListener != null) {
                internalOfferwallListener.onOfferwallAvailable(true);
                return;
            }
            return;
        }
        reportInitFail(ironSourceError);
    }

    public void onOfferwallOpened() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "onOfferwallOpened()", 1);
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        try {
            if (!TextUtils.isEmpty(this.mCurrentPlacementName)) {
                mediationAdditionalData.put("placement", this.mCurrentPlacementName);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(305, mediationAdditionalData));
        InternalOfferwallListener internalOfferwallListener = this.mListenersWrapper;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.onOfferwallOpened();
        }
    }

    public void onOfferwallShowFailed(IronSourceError ironSourceError) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, "onOfferwallShowFailed(" + ironSourceError + ")", 1);
        InternalOfferwallListener internalOfferwallListener = this.mListenersWrapper;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.onOfferwallShowFailed(ironSourceError);
        }
    }

    public boolean onOfferwallAdCredited(int i, int i2, boolean z) {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "onOfferwallAdCredited()", 1);
        InternalOfferwallListener internalOfferwallListener = this.mListenersWrapper;
        if (internalOfferwallListener != null) {
            return internalOfferwallListener.onOfferwallAdCredited(i, i2, z);
        }
        return false;
    }

    public void onGetOfferwallCreditsFailed(IronSourceError ironSourceError) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, "onGetOfferwallCreditsFailed(" + ironSourceError + ")", 1);
        InternalOfferwallListener internalOfferwallListener = this.mListenersWrapper;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.onGetOfferwallCreditsFailed(ironSourceError);
        }
    }

    public void onOfferwallClosed() {
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "onOfferwallClosed()", 1);
        InternalOfferwallListener internalOfferwallListener = this.mListenersWrapper;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.onOfferwallClosed();
        }
    }

    private synchronized void reportInitFail(IronSourceError ironSourceError) {
        if (this.mIsOfferwallAvailable != null) {
            this.mIsOfferwallAvailable.set(false);
        }
        if (this.mAtomicShouldPerformInit != null) {
            this.mAtomicShouldPerformInit.set(true);
        }
        if (this.mListenersWrapper != null) {
            this.mListenersWrapper.onOfferwallAvailable(false, ironSourceError);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: com.ironsource.mediationsdk.AbstractAdapter} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.ironsource.mediationsdk.AbstractAdapter startOfferwallAdapter() {
        /*
            r9 = this;
            java.lang.String r0 = "SupersonicAds"
            r1 = 0
            com.ironsource.mediationsdk.IronSourceObject r2 = com.ironsource.mediationsdk.IronSourceObject.getInstance()     // Catch:{ all -> 0x0053 }
            com.ironsource.mediationsdk.AbstractAdapter r3 = r2.getOfferwallAdapter(r0)     // Catch:{ all -> 0x0053 }
            if (r3 != 0) goto L_0x004f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0053 }
            r3.<init>()     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "com.ironsource.adapters."
            r3.append(r4)     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = r0.toLowerCase()     // Catch:{ all -> 0x0053 }
            r3.append(r4)     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "."
            r3.append(r4)     // Catch:{ all -> 0x0053 }
            r3.append(r0)     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "Adapter"
            r3.append(r4)     // Catch:{ all -> 0x0053 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0053 }
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "startAdapter"
            r5 = 1
            java.lang.Class[] r6 = new java.lang.Class[r5]     // Catch:{ all -> 0x0053 }
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r8 = 0
            r6[r8] = r7     // Catch:{ all -> 0x0053 }
            java.lang.reflect.Method r4 = r3.getMethod(r4, r6)     // Catch:{ all -> 0x0053 }
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0053 }
            r5[r8] = r0     // Catch:{ all -> 0x0053 }
            java.lang.Object r0 = r4.invoke(r3, r5)     // Catch:{ all -> 0x0053 }
            r3 = r0
            com.ironsource.mediationsdk.AbstractAdapter r3 = (com.ironsource.mediationsdk.AbstractAdapter) r3     // Catch:{ all -> 0x0053 }
            if (r3 != 0) goto L_0x004f
            return r1
        L_0x004f:
            r2.addOWAdapter(r3)     // Catch:{ all -> 0x0053 }
            return r3
        L_0x0053:
            r0 = move-exception
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r2 = r9.mLoggerManager
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            r4 = 2
            java.lang.String r5 = "SupersonicAds initialization failed - please verify that required dependencies are in you build path."
            r2.log(r3, r5, r4)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r2 = r9.mLoggerManager
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = r9.TAG
            r4.append(r5)
            java.lang.String r5 = ":startOfferwallAdapter"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.logException(r3, r4, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.OfferwallManager.startOfferwallAdapter():com.ironsource.mediationsdk.AbstractAdapter");
    }

    private void setCustomParams(AbstractAdapter abstractAdapter) {
        try {
            Integer age = IronSourceObject.getInstance().getAge();
            if (age != null) {
                abstractAdapter.setAge(age.intValue());
            }
            String gender = IronSourceObject.getInstance().getGender();
            if (gender != null) {
                abstractAdapter.setGender(gender);
            }
            String mediationSegment = IronSourceObject.getInstance().getMediationSegment();
            if (mediationSegment != null) {
                abstractAdapter.setMediationSegment(mediationSegment);
            }
            Boolean consent = IronSourceObject.getInstance().getConsent();
            if (consent != null) {
                IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                ironSourceLoggerManager.log(ironSourceTag, "Offerwall | setConsent(consent:" + consent + ")", 1);
                abstractAdapter.setConsent(consent.booleanValue());
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.INTERNAL;
            ironSourceLoggerManager2.log(ironSourceTag2, ":setCustomParams():" + e.toString(), 3);
        }
    }
}
