package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.DemandOnlySmash;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyIsManagerListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.sdk.constants.Constants;
import java.util.Date;
import java.util.TimerTask;

public class DemandOnlyIsSmash extends DemandOnlySmash implements InterstitialSmashListener {
    /* access modifiers changed from: private */
    public DemandOnlyIsManagerListener mListener;
    /* access modifiers changed from: private */
    public long mLoadStartTime;

    public void onInterstitialAdShowSucceeded() {
    }

    public void onInterstitialInitFailed(IronSourceError ironSourceError) {
    }

    public void onInterstitialInitSuccess() {
    }

    public DemandOnlyIsSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, DemandOnlyIsManagerListener demandOnlyIsManagerListener, int i, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.getInterstitialSettings()), abstractAdapter);
        this.mListener = demandOnlyIsManagerListener;
        this.mLoadTimeoutSecs = i;
        this.mAdapter.initInterstitial(activity, str, str2, this.mAdUnitSettings, this);
    }

    public void loadInterstitial() {
        logInternal("loadInterstitial state=" + getStateString());
        DemandOnlySmash.SMASH_STATE compareAndSetState = compareAndSetState(new DemandOnlySmash.SMASH_STATE[]{DemandOnlySmash.SMASH_STATE.NOT_LOADED, DemandOnlySmash.SMASH_STATE.LOADED}, DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS);
        if (compareAndSetState == DemandOnlySmash.SMASH_STATE.NOT_LOADED || compareAndSetState == DemandOnlySmash.SMASH_STATE.LOADED) {
            startLoadTimer();
            this.mLoadStartTime = new Date().getTime();
            this.mAdapter.loadInterstitial(this.mAdUnitSettings, this);
        } else if (compareAndSetState == DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS) {
            this.mListener.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_LOAD_ALREADY_IN_PROGRESS, "load already in progress"), this, 0);
        } else {
            this.mListener.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_LOAD_ALREADY_IN_PROGRESS, "cannot load because show is in progress"), this, 0);
        }
    }

    public void showInterstitial() {
        logInternal("showInterstitial state=" + getStateString());
        if (compareAndSetState(DemandOnlySmash.SMASH_STATE.LOADED, DemandOnlySmash.SMASH_STATE.SHOW_IN_PROGRESS)) {
            this.mAdapter.showInterstitial(this.mAdUnitSettings, this);
            return;
        }
        this.mListener.onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_CALL_LOAD_BEFORE_SHOW, "load must be called before show"), this);
    }

    public boolean isInterstitialReady() {
        return this.mAdapter.isInterstitialReady(this.mAdUnitSettings);
    }

    private void startLoadTimer() {
        logInternal("start timer");
        startTimer(new TimerTask() {
            public void run() {
                DemandOnlyIsSmash demandOnlyIsSmash = DemandOnlyIsSmash.this;
                demandOnlyIsSmash.logInternal("load timed out state=" + DemandOnlyIsSmash.this.getStateString());
                if (DemandOnlyIsSmash.this.compareAndSetState(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.NOT_LOADED)) {
                    DemandOnlyIsSmash.this.mListener.onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_DO_IS_LOAD_TIMED_OUT, "load timed out"), DemandOnlyIsSmash.this, new Date().getTime() - DemandOnlyIsSmash.this.mLoadStartTime);
                }
            }
        });
    }

    public void onInterstitialAdReady() {
        logAdapterCallback("onInterstitialAdReady state=" + getStateString());
        stopTimer();
        if (compareAndSetState(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.LOADED)) {
            this.mListener.onInterstitialAdReady(this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onInterstitialAdLoadFailed(IronSourceError ironSourceError) {
        logAdapterCallback("onInterstitialAdLoadFailed error=" + ironSourceError.getErrorMessage() + " state=" + getStateString());
        stopTimer();
        if (compareAndSetState(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.NOT_LOADED)) {
            this.mListener.onInterstitialAdLoadFailed(ironSourceError, this, new Date().getTime() - this.mLoadStartTime);
        }
    }

    public void onInterstitialAdOpened() {
        logAdapterCallback("onInterstitialAdOpened");
        this.mListener.onInterstitialAdOpened(this);
    }

    public void onInterstitialAdClosed() {
        setState(DemandOnlySmash.SMASH_STATE.NOT_LOADED);
        logAdapterCallback("onInterstitialAdClosed");
        this.mListener.onInterstitialAdClosed(this);
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError) {
        setState(DemandOnlySmash.SMASH_STATE.NOT_LOADED);
        logAdapterCallback("onInterstitialAdShowFailed error=" + ironSourceError.getErrorMessage());
        this.mListener.onInterstitialAdShowFailed(ironSourceError, this);
    }

    public void onInterstitialAdClicked() {
        logAdapterCallback(Constants.JSMethods.ON_INTERSTITIAL_AD_CLICKED);
        this.mListener.onInterstitialAdClicked(this);
    }

    public void onInterstitialAdVisible() {
        logAdapterCallback("onInterstitialAdVisible");
        this.mListener.onInterstitialAdVisible(this);
    }

    private void logAdapterCallback(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "DemandOnlyInterstitialSmash " + this.mAdapterConfig.getProviderName() + " : " + str, 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "DemandOnlyInterstitialSmash " + this.mAdapterConfig.getProviderName() + " : " + str, 0);
    }
}
