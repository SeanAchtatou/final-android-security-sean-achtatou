package com.ironsource.mediationsdk.model;

import java.util.ArrayList;
import java.util.Iterator;

public class OfferwallConfigurations {
    private static final int DEFAULT_OW_PLACEMENT_ID = 0;
    private OfferwallPlacement mDefaultOWPlacement;
    private ApplicationEvents mEvents;
    private ArrayList<OfferwallPlacement> mOWPlacements = new ArrayList<>();

    public OfferwallConfigurations(ApplicationEvents applicationEvents) {
        this.mEvents = applicationEvents;
    }

    public void addOfferwallPlacement(OfferwallPlacement offerwallPlacement) {
        if (offerwallPlacement != null) {
            this.mOWPlacements.add(offerwallPlacement);
            if (this.mDefaultOWPlacement == null) {
                this.mDefaultOWPlacement = offerwallPlacement;
            } else if (offerwallPlacement.getPlacementId() == 0) {
                this.mDefaultOWPlacement = offerwallPlacement;
            }
        }
    }

    public OfferwallPlacement getOfferwallPlacement(String str) {
        Iterator<OfferwallPlacement> it = this.mOWPlacements.iterator();
        while (it.hasNext()) {
            OfferwallPlacement next = it.next();
            if (next.getPlacementName().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public OfferwallPlacement getDefaultOfferwallPlacement() {
        Iterator<OfferwallPlacement> it = this.mOWPlacements.iterator();
        while (it.hasNext()) {
            OfferwallPlacement next = it.next();
            if (next.isDefault()) {
                return next;
            }
        }
        return this.mDefaultOWPlacement;
    }

    public ApplicationEvents getOfferWallEventsConfigurations() {
        return this.mEvents;
    }
}
