package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionCappingManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

class ProgRvManager implements ProgRvManagerListener, RvLoadTriggerCallback, AuctionEventListener {
    /* access modifiers changed from: private */
    public Context mAppContext;
    private String mAuctionFallback = "";
    /* access modifiers changed from: private */
    public AuctionHandler mAuctionHandler;
    /* access modifiers changed from: private */
    public long mAuctionStartTime;
    private int mAuctionTrial;
    /* access modifiers changed from: private */
    public String mCurrentAuctionId;
    private String mCurrentPlacement;
    private boolean mIsAuctionEnabled;
    private boolean mIsShowingVideo;
    private long mLastChangedAvailabilityTime;
    private Boolean mLastReportedAvailabilityState;
    private int mMaxSmashesToLoad;
    private RvLoadTrigger mRvLoadTrigger;
    /* access modifiers changed from: private */
    public SessionCappingManager mSessionCappingManager;
    /* access modifiers changed from: private */
    public int mSessionDepth = 1;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, ProgRvSmash> mSmashes;
    private RV_MEDIATION_STATE mState;
    private CopyOnWriteArrayList<ProgRvSmash> mWaterfall;
    private ConcurrentHashMap<String, AuctionResponseItem> mWaterfallServerData;

    private enum RV_MEDIATION_STATE {
        RV_STATE_INITIATING,
        RV_STATE_AUCTION_IN_PROGRESS,
        RV_STATE_NOT_LOADED,
        RV_STATE_LOADING_SMASHES,
        RV_STATE_READY_TO_SHOW
    }

    private boolean shouldAddAuctionParams(int i) {
        return i == 1003 || i == 1302 || i == 1301;
    }

    public ProgRvManager(Activity activity, List<ProviderSettings> list, RewardedVideoConfigurations rewardedVideoConfigurations, String str, String str2) {
        long time = new Date().getTime();
        sendMediationEventWithoutAuctionId(IronSourceConstants.RV_MANAGER_INIT_STARTED);
        setState(RV_MEDIATION_STATE.RV_STATE_INITIATING);
        this.mAppContext = activity.getApplicationContext();
        this.mLastReportedAvailabilityState = null;
        this.mMaxSmashesToLoad = rewardedVideoConfigurations.getRewardedVideoAdaptersSmartLoadAmount();
        this.mCurrentPlacement = "";
        AuctionSettings rewardedVideoAuctionSettings = rewardedVideoConfigurations.getRewardedVideoAuctionSettings();
        this.mIsShowingVideo = false;
        this.mWaterfall = new CopyOnWriteArrayList<>();
        this.mWaterfallServerData = new ConcurrentHashMap<>();
        this.mLastChangedAvailabilityTime = new Date().getTime();
        this.mIsAuctionEnabled = rewardedVideoAuctionSettings.getNumOfMaxTrials() > 0;
        if (this.mIsAuctionEnabled) {
            this.mAuctionHandler = new AuctionHandler("rewardedVideo", rewardedVideoAuctionSettings, this);
        }
        this.mRvLoadTrigger = new RvLoadTrigger(rewardedVideoAuctionSettings, this);
        this.mSmashes = new ConcurrentHashMap<>();
        for (ProviderSettings next : list) {
            AbstractAdapter adapter = AdapterRepository.getInstance().getAdapter(next, next.getRewardedVideoSettings(), activity);
            if (adapter != null && AdaptersCompatibilityHandler.getInstance().isAdapterVersionRVCompatible(adapter)) {
                ProgRvSmash progRvSmash = r0;
                ProgRvSmash progRvSmash2 = new ProgRvSmash(activity, str, str2, next, this, rewardedVideoConfigurations.getRewardedVideoAdaptersSmartLoadTimeout(), adapter);
                this.mSmashes.put(progRvSmash.getInstanceName(), progRvSmash);
            }
        }
        this.mSessionCappingManager = new SessionCappingManager(new ArrayList(this.mSmashes.values()));
        for (ProgRvSmash next2 : this.mSmashes.values()) {
            if (next2.isBidder()) {
                next2.initForBidding();
            }
        }
        sendMediationEventWithoutAuctionId(IronSourceConstants.RV_MANAGER_INIT_ENDED, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - time)}});
        loadRewardedVideo(rewardedVideoAuctionSettings.getTimeToWaitBeforeFirstAuctionMs());
    }

    private void loadRewardedVideo(long j) {
        if (this.mSessionCappingManager.areAllSmashesCapped()) {
            sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 80001}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "all smashes are capped"}});
            handleLoadFailure();
        } else if (this.mIsAuctionEnabled) {
            new Timer().schedule(new TimerTask() {
                public void run() {
                    ProgRvManager.this.makeAuction();
                }
            }, j);
        } else {
            updateWaterfallToNonBidding();
            if (this.mWaterfall.isEmpty()) {
                sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 80002}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "waterfall is empty"}});
                handleLoadFailure();
                return;
            }
            sendMediationEventWithoutAuctionId(1000);
            loadSmashes();
        }
    }

    public synchronized void showRewardedVideo(Placement placement) {
        if (placement == null) {
            logAPIError("showRewardedVideo error: empty default placement");
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(1021, "showRewardedVideo error: empty default placement"));
            sendMediationEvent(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1021}}, false, true);
            return;
        }
        this.mCurrentPlacement = placement.getPlacementName();
        logApi("showRewardedVideo() placement=" + this.mCurrentPlacement);
        sendMediationEventWithPlacement(IronSourceConstants.RV_API_SHOW_CALLED);
        if (this.mIsShowingVideo) {
            logAPIError("showRewardedVideo error: can't show ad while an ad is already showing");
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(1022, "showRewardedVideo error: can't show ad while an ad is already showing"));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1022}});
        } else if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
            logAPIError("showRewardedVideo error: show called while no ads are available");
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_RV_SHOW_CALLED_WRONG_STATE, "showRewardedVideo error: show called while no ads are available"));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_RV_SHOW_CALLED_WRONG_STATE)}});
        } else if (CappingManager.isRvPlacementCapped(this.mAppContext, this.mCurrentPlacement)) {
            String str = "showRewardedVideo error: placement " + this.mCurrentPlacement + " is capped";
            logAPIError(str);
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(new IronSourceError(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT, str));
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT)}});
        } else {
            synchronized (this.mSmashes) {
                Iterator<ProgRvSmash> it = this.mWaterfall.iterator();
                while (it.hasNext()) {
                    ProgRvSmash next = it.next();
                    if (next.isReadyToShow()) {
                        this.mIsShowingVideo = true;
                        next.reportShowChance(true, this.mSessionDepth);
                        showVideo(next, placement);
                        setState(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
                        this.mRvLoadTrigger.showStart();
                        return;
                    }
                    next.reportShowChance(false, this.mSessionDepth);
                }
                logApi("showRewardedVideo(): No ads to show ");
                RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
                this.mRvLoadTrigger.showError();
            }
        }
    }

    private void showVideo(ProgRvSmash progRvSmash, Placement placement) {
        logInternal("showVideo()");
        this.mSessionCappingManager.increaseShowCounter(progRvSmash);
        if (this.mSessionCappingManager.isCapped(progRvSmash)) {
            progRvSmash.setCappedPerSession();
            IronSourceUtils.sendAutomationLog(progRvSmash.getInstanceName() + " rewarded video is now session capped");
        }
        CappingManager.incrementRvShowCounter(this.mAppContext, placement.getPlacementName());
        if (CappingManager.isRvPlacementCapped(this.mAppContext, placement.getPlacementName())) {
            sendMediationEventWithPlacement(1400);
        }
        progRvSmash.showVideo(placement, this.mSessionDepth);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0033, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean isRewardedVideoAvailable() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r0 = r4.mState     // Catch:{ all -> 0x0034 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r1 = com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW     // Catch:{ all -> 0x0034 }
            r2 = 0
            if (r0 != r1) goto L_0x0032
            boolean r0 = r4.mIsShowingVideo     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x000d
            goto L_0x0032
        L_0x000d:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.ProgRvSmash> r0 = r4.mSmashes     // Catch:{ all -> 0x0034 }
            monitor-enter(r0)     // Catch:{ all -> 0x0034 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r1 = r4.mWaterfall     // Catch:{ all -> 0x002f }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x002f }
        L_0x0016:
            boolean r3 = r1.hasNext()     // Catch:{ all -> 0x002f }
            if (r3 == 0) goto L_0x002c
            java.lang.Object r3 = r1.next()     // Catch:{ all -> 0x002f }
            com.ironsource.mediationsdk.ProgRvSmash r3 = (com.ironsource.mediationsdk.ProgRvSmash) r3     // Catch:{ all -> 0x002f }
            boolean r3 = r3.isReadyToShow()     // Catch:{ all -> 0x002f }
            if (r3 == 0) goto L_0x0016
            r1 = 1
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            monitor-exit(r4)
            return r1
        L_0x002c:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            monitor-exit(r4)
            return r2
        L_0x002f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            throw r1     // Catch:{ all -> 0x0034 }
        L_0x0032:
            monitor-exit(r4)
            return r2
        L_0x0034:
            r0 = move-exception
            monitor-exit(r4)
            goto L_0x0038
        L_0x0037:
            throw r0
        L_0x0038:
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgRvManager.isRewardedVideoAvailable():boolean");
    }

    public void onResume(Activity activity) {
        for (ProgRvSmash onResume : this.mSmashes.values()) {
            onResume.onResume(activity);
        }
    }

    public void onPause(Activity activity) {
        for (ProgRvSmash onPause : this.mSmashes.values()) {
            onPause.onPause(activity);
        }
    }

    /* access modifiers changed from: private */
    public void makeAuction() {
        AsyncTask.execute(new Runnable() {
            public void run() {
                ProgRvManager.this.logInternal("makeAuction()");
                ProgRvManager.this.setState(RV_MEDIATION_STATE.RV_STATE_AUCTION_IN_PROGRESS);
                String unused = ProgRvManager.this.mCurrentAuctionId = "";
                long unused2 = ProgRvManager.this.mAuctionStartTime = new Date().getTime();
                HashMap hashMap = new HashMap();
                ArrayList arrayList = new ArrayList();
                StringBuilder sb = new StringBuilder();
                synchronized (ProgRvManager.this.mSmashes) {
                    for (ProgRvSmash progRvSmash : ProgRvManager.this.mSmashes.values()) {
                        progRvSmash.unloadVideo();
                        if (!ProgRvManager.this.mSessionCappingManager.isCapped(progRvSmash)) {
                            if (progRvSmash.isBidder() && progRvSmash.isReadyToBid()) {
                                Map<String, Object> biddingData = progRvSmash.getBiddingData();
                                if (biddingData != null) {
                                    hashMap.put(progRvSmash.getInstanceName(), biddingData);
                                    sb.append("2" + progRvSmash.getInstanceName() + ",");
                                }
                            } else if (!progRvSmash.isBidder()) {
                                arrayList.add(progRvSmash.getInstanceName());
                                sb.append("1" + progRvSmash.getInstanceName() + ",");
                            }
                        }
                    }
                }
                if (hashMap.keySet().size() == 0 && arrayList.size() == 0) {
                    ProgRvManager.this.logInternal("makeAuction() failed - request waterfall is empty");
                    ProgRvManager.this.sendMediationEvent(IronSourceConstants.TROUBLESHOOTING_RV_LOAD_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 80003}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "waterfall request is empty"}});
                    ProgRvManager.this.handleLoadFailure();
                    return;
                }
                ProgRvManager progRvManager = ProgRvManager.this;
                progRvManager.logInternal("makeAuction() - request waterfall is: " + ((Object) sb));
                if (sb.length() > 256) {
                    sb.setLength(256);
                } else if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                ProgRvManager.this.sendMediationEventWithoutAuctionId(1000);
                ProgRvManager.this.sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_REQUEST);
                ProgRvManager.this.sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_REQUEST_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
                ProgRvManager.this.mAuctionHandler.executeAuction(ProgRvManager.this.mAppContext, hashMap, arrayList, ProgRvManager.this.mSessionDepth);
            }
        });
    }

    private void updateWaterfallToNonBidding() {
        updateWaterfall(extractNonBidderProvidersFromWaterfall());
    }

    private List<AuctionResponseItem> extractNonBidderProvidersFromWaterfall() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (ProgRvSmash next : this.mSmashes.values()) {
            if (!next.isBidder() && !this.mSessionCappingManager.isCapped(next)) {
                copyOnWriteArrayList.add(new AuctionResponseItem(next.getInstanceName()));
            }
        }
        return copyOnWriteArrayList;
    }

    private String getAsString(AuctionResponseItem auctionResponseItem) {
        String str = TextUtils.isEmpty(auctionResponseItem.getServerData()) ? "1" : "2";
        return str + auctionResponseItem.getInstanceName();
    }

    private void updateWaterfall(List<AuctionResponseItem> list) {
        synchronized (this.mSmashes) {
            this.mWaterfall.clear();
            this.mWaterfallServerData.clear();
            StringBuilder sb = new StringBuilder();
            for (AuctionResponseItem next : list) {
                sb.append(getAsString(next) + ",");
                ProgRvSmash progRvSmash = this.mSmashes.get(next.getInstanceName());
                if (progRvSmash != null) {
                    progRvSmash.setIsLoadCandidate(true);
                    this.mWaterfall.add(progRvSmash);
                    this.mWaterfallServerData.put(progRvSmash.getInstanceName(), next);
                }
            }
            if (sb.length() > 256) {
                sb.setLength(256);
            } else if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
            sendMediationEvent(IronSourceConstants.RV_RESULT_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadSmashes() {
        /*
            r10 = this;
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.ProgRvSmash> r0 = r10.mSmashes
            monitor-enter(r0)
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r1 = r10.mWaterfall     // Catch:{ all -> 0x007b }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x007b }
            r2 = 0
            if (r1 == 0) goto L_0x0038
            r1 = 81001(0x13c69, float:1.13507E-40)
            r3 = 2
            java.lang.Object[][] r4 = new java.lang.Object[r3][]     // Catch:{ all -> 0x007b }
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ all -> 0x007b }
            java.lang.String r6 = "errorCode"
            r5[r2] = r6     // Catch:{ all -> 0x007b }
            r6 = 80004(0x13884, float:1.1211E-40)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x007b }
            r7 = 1
            r5[r7] = r6     // Catch:{ all -> 0x007b }
            r4[r2] = r5     // Catch:{ all -> 0x007b }
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x007b }
            java.lang.String r5 = "reason"
            r3[r2] = r5     // Catch:{ all -> 0x007b }
            java.lang.String r2 = "waterfall is empty"
            r3[r7] = r2     // Catch:{ all -> 0x007b }
            r4[r7] = r3     // Catch:{ all -> 0x007b }
            r10.sendMediationEvent(r1, r4)     // Catch:{ all -> 0x007b }
            r10.handleLoadFailure()     // Catch:{ all -> 0x007b }
            monitor-exit(r0)     // Catch:{ all -> 0x007b }
            return
        L_0x0038:
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r1 = com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE.RV_STATE_LOADING_SMASHES     // Catch:{ all -> 0x007b }
            r10.setState(r1)     // Catch:{ all -> 0x007b }
            r1 = 0
        L_0x003e:
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r3 = r10.mWaterfall     // Catch:{ all -> 0x007b }
            int r3 = r3.size()     // Catch:{ all -> 0x007b }
            if (r2 >= r3) goto L_0x0079
            int r3 = r10.mMaxSmashesToLoad     // Catch:{ all -> 0x007b }
            if (r1 >= r3) goto L_0x0079
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r3 = r10.mWaterfall     // Catch:{ all -> 0x007b }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ all -> 0x007b }
            r4 = r3
            com.ironsource.mediationsdk.ProgRvSmash r4 = (com.ironsource.mediationsdk.ProgRvSmash) r4     // Catch:{ all -> 0x007b }
            boolean r3 = r4.getIsLoadCandidate()     // Catch:{ all -> 0x007b }
            if (r3 == 0) goto L_0x0076
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r3 = r10.mWaterfallServerData     // Catch:{ all -> 0x007b }
            java.lang.String r5 = r4.getInstanceName()     // Catch:{ all -> 0x007b }
            java.lang.Object r3 = r3.get(r5)     // Catch:{ all -> 0x007b }
            com.ironsource.mediationsdk.AuctionResponseItem r3 = (com.ironsource.mediationsdk.AuctionResponseItem) r3     // Catch:{ all -> 0x007b }
            java.lang.String r5 = r3.getServerData()     // Catch:{ all -> 0x007b }
            java.lang.String r6 = r10.mCurrentAuctionId     // Catch:{ all -> 0x007b }
            int r7 = r10.mAuctionTrial     // Catch:{ all -> 0x007b }
            java.lang.String r8 = r10.mAuctionFallback     // Catch:{ all -> 0x007b }
            int r9 = r10.mSessionDepth     // Catch:{ all -> 0x007b }
            r4.loadVideo(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x007b }
            int r1 = r1 + 1
        L_0x0076:
            int r2 = r2 + 1
            goto L_0x003e
        L_0x0079:
            monitor-exit(r0)     // Catch:{ all -> 0x007b }
            return
        L_0x007b:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x007b }
            goto L_0x007f
        L_0x007e:
            throw r1
        L_0x007f:
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgRvManager.loadSmashes():void");
    }

    /* access modifiers changed from: private */
    public void handleLoadFailure() {
        setState(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
        reportAvailabilityIfNeeded(false);
        this.mRvLoadTrigger.loadError();
    }

    /* access modifiers changed from: private */
    public void setState(RV_MEDIATION_STATE rv_mediation_state) {
        logInternal("current state=" + this.mState + ", new state=" + rv_mediation_state);
        this.mState = rv_mediation_state;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00b4, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onLoadSuccess(com.ironsource.mediationsdk.ProgRvSmash r9, java.lang.String r10) {
        /*
            r8 = this;
            monitor-enter(r8)
            java.lang.String r0 = "onLoadSuccess "
            r8.logSmashCallback(r9, r0)     // Catch:{ all -> 0x00b5 }
            java.lang.String r0 = r8.mCurrentAuctionId     // Catch:{ all -> 0x00b5 }
            r1 = 2
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L_0x0068
            java.lang.String r0 = r8.mCurrentAuctionId     // Catch:{ all -> 0x00b5 }
            boolean r0 = r10.equalsIgnoreCase(r0)     // Catch:{ all -> 0x00b5 }
            if (r0 != 0) goto L_0x0068
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b5 }
            r9.<init>()     // Catch:{ all -> 0x00b5 }
            java.lang.String r0 = "onLoadSuccess was invoked with auctionId: "
            r9.append(r0)     // Catch:{ all -> 0x00b5 }
            r9.append(r10)     // Catch:{ all -> 0x00b5 }
            java.lang.String r10 = " and the current id is "
            r9.append(r10)     // Catch:{ all -> 0x00b5 }
            java.lang.String r10 = r8.mCurrentAuctionId     // Catch:{ all -> 0x00b5 }
            r9.append(r10)     // Catch:{ all -> 0x00b5 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x00b5 }
            r8.logInternal(r9)     // Catch:{ all -> 0x00b5 }
            r9 = 81315(0x13da3, float:1.13947E-40)
            java.lang.Object[][] r10 = new java.lang.Object[r1][]     // Catch:{ all -> 0x00b5 }
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00b5 }
            java.lang.String r4 = "errorCode"
            r0[r2] = r4     // Catch:{ all -> 0x00b5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x00b5 }
            r0[r3] = r4     // Catch:{ all -> 0x00b5 }
            r10[r2] = r0     // Catch:{ all -> 0x00b5 }
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00b5 }
            java.lang.String r1 = "reason"
            r0[r2] = r1     // Catch:{ all -> 0x00b5 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b5 }
            r1.<init>()     // Catch:{ all -> 0x00b5 }
            java.lang.String r2 = "onLoadSuccess wrong auction ID "
            r1.append(r2)     // Catch:{ all -> 0x00b5 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r2 = r8.mState     // Catch:{ all -> 0x00b5 }
            r1.append(r2)     // Catch:{ all -> 0x00b5 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00b5 }
            r0[r3] = r1     // Catch:{ all -> 0x00b5 }
            r10[r3] = r0     // Catch:{ all -> 0x00b5 }
            r8.sendMediationEvent(r9, r10)     // Catch:{ all -> 0x00b5 }
            monitor-exit(r8)
            return
        L_0x0068:
            r8.reportAvailabilityIfNeeded(r3)     // Catch:{ all -> 0x00b5 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r10 = r8.mState     // Catch:{ all -> 0x00b5 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r0 = com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW     // Catch:{ all -> 0x00b5 }
            if (r10 == r0) goto L_0x00b3
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r10 = com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW     // Catch:{ all -> 0x00b5 }
            r8.setState(r10)     // Catch:{ all -> 0x00b5 }
            java.util.Date r10 = new java.util.Date     // Catch:{ all -> 0x00b5 }
            r10.<init>()     // Catch:{ all -> 0x00b5 }
            long r4 = r10.getTime()     // Catch:{ all -> 0x00b5 }
            long r6 = r8.mAuctionStartTime     // Catch:{ all -> 0x00b5 }
            long r4 = r4 - r6
            r10 = 1003(0x3eb, float:1.406E-42)
            java.lang.Object[][] r0 = new java.lang.Object[r3][]     // Catch:{ all -> 0x00b5 }
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x00b5 }
            java.lang.String r6 = "duration"
            r1[r2] = r6     // Catch:{ all -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x00b5 }
            r1[r3] = r4     // Catch:{ all -> 0x00b5 }
            r0[r2] = r1     // Catch:{ all -> 0x00b5 }
            r8.sendMediationEvent(r10, r0)     // Catch:{ all -> 0x00b5 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r10 = r8.mWaterfallServerData     // Catch:{ all -> 0x00b5 }
            java.lang.String r9 = r9.getInstanceName()     // Catch:{ all -> 0x00b5 }
            java.lang.Object r9 = r10.get(r9)     // Catch:{ all -> 0x00b5 }
            com.ironsource.mediationsdk.AuctionResponseItem r9 = (com.ironsource.mediationsdk.AuctionResponseItem) r9     // Catch:{ all -> 0x00b5 }
            com.ironsource.mediationsdk.AuctionHandler r10 = r8.mAuctionHandler     // Catch:{ all -> 0x00b5 }
            r10.reportLoadSuccess(r9)     // Catch:{ all -> 0x00b5 }
            com.ironsource.mediationsdk.AuctionHandler r10 = r8.mAuctionHandler     // Catch:{ all -> 0x00b5 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r0 = r8.mWaterfall     // Catch:{ all -> 0x00b5 }
            java.util.concurrent.CopyOnWriteArrayList r0 = (java.util.concurrent.CopyOnWriteArrayList) r0     // Catch:{ all -> 0x00b5 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r1 = r8.mWaterfallServerData     // Catch:{ all -> 0x00b5 }
            r10.reportAuctionLose(r0, r1, r9)     // Catch:{ all -> 0x00b5 }
        L_0x00b3:
            monitor-exit(r8)
            return
        L_0x00b5:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgRvManager.onLoadSuccess(com.ironsource.mediationsdk.ProgRvSmash, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d5, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onLoadError(com.ironsource.mediationsdk.ProgRvSmash r12, java.lang.String r13) {
        /*
            r11 = this;
            monitor-enter(r11)
            java.lang.String r0 = "onLoadError "
            r11.logSmashCallback(r12, r0)     // Catch:{ all -> 0x00d9 }
            java.lang.String r12 = r11.mCurrentAuctionId     // Catch:{ all -> 0x00d9 }
            boolean r12 = r13.equalsIgnoreCase(r12)     // Catch:{ all -> 0x00d9 }
            r0 = 1
            r1 = 0
            if (r12 != 0) goto L_0x0065
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d9 }
            r12.<init>()     // Catch:{ all -> 0x00d9 }
            java.lang.String r2 = "onLoadError was invoked with auctionId:"
            r12.append(r2)     // Catch:{ all -> 0x00d9 }
            r12.append(r13)     // Catch:{ all -> 0x00d9 }
            java.lang.String r13 = " and the current id is "
            r12.append(r13)     // Catch:{ all -> 0x00d9 }
            java.lang.String r13 = r11.mCurrentAuctionId     // Catch:{ all -> 0x00d9 }
            r12.append(r13)     // Catch:{ all -> 0x00d9 }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x00d9 }
            r11.logInternal(r12)     // Catch:{ all -> 0x00d9 }
            r12 = 81315(0x13da3, float:1.13947E-40)
            r13 = 2
            java.lang.Object[][] r2 = new java.lang.Object[r13][]     // Catch:{ all -> 0x00d9 }
            java.lang.Object[] r3 = new java.lang.Object[r13]     // Catch:{ all -> 0x00d9 }
            java.lang.String r4 = "errorCode"
            r3[r1] = r4     // Catch:{ all -> 0x00d9 }
            r4 = 4
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x00d9 }
            r3[r0] = r4     // Catch:{ all -> 0x00d9 }
            r2[r1] = r3     // Catch:{ all -> 0x00d9 }
            java.lang.Object[] r13 = new java.lang.Object[r13]     // Catch:{ all -> 0x00d9 }
            java.lang.String r3 = "reason"
            r13[r1] = r3     // Catch:{ all -> 0x00d9 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d9 }
            r1.<init>()     // Catch:{ all -> 0x00d9 }
            java.lang.String r3 = "loadError wrong auction ID "
            r1.append(r3)     // Catch:{ all -> 0x00d9 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r3 = r11.mState     // Catch:{ all -> 0x00d9 }
            r1.append(r3)     // Catch:{ all -> 0x00d9 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00d9 }
            r13[r0] = r1     // Catch:{ all -> 0x00d9 }
            r2[r0] = r13     // Catch:{ all -> 0x00d9 }
            r11.sendMediationEvent(r12, r2)     // Catch:{ all -> 0x00d9 }
            monitor-exit(r11)
            return
        L_0x0065:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.ProgRvSmash> r12 = r11.mSmashes     // Catch:{ all -> 0x00d9 }
            monitor-enter(r12)     // Catch:{ all -> 0x00d9 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r13 = r11.mWaterfall     // Catch:{ all -> 0x00d6 }
            java.util.Iterator r13 = r13.iterator()     // Catch:{ all -> 0x00d6 }
            r2 = 0
            r3 = 0
        L_0x0070:
            boolean r4 = r13.hasNext()     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x00bd
            java.lang.Object r4 = r13.next()     // Catch:{ all -> 0x00d6 }
            r5 = r4
            com.ironsource.mediationsdk.ProgRvSmash r5 = (com.ironsource.mediationsdk.ProgRvSmash) r5     // Catch:{ all -> 0x00d6 }
            boolean r4 = r5.getIsLoadCandidate()     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x00ad
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r4 = r11.mWaterfallServerData     // Catch:{ all -> 0x00d6 }
            java.lang.String r6 = r5.getInstanceName()     // Catch:{ all -> 0x00d6 }
            java.lang.Object r4 = r4.get(r6)     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x0070
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r13 = r11.mWaterfallServerData     // Catch:{ all -> 0x00d6 }
            java.lang.String r0 = r5.getInstanceName()     // Catch:{ all -> 0x00d6 }
            java.lang.Object r13 = r13.get(r0)     // Catch:{ all -> 0x00d6 }
            com.ironsource.mediationsdk.AuctionResponseItem r13 = (com.ironsource.mediationsdk.AuctionResponseItem) r13     // Catch:{ all -> 0x00d6 }
            java.lang.String r6 = r13.getServerData()     // Catch:{ all -> 0x00d6 }
            java.lang.String r7 = r11.mCurrentAuctionId     // Catch:{ all -> 0x00d6 }
            int r8 = r11.mAuctionTrial     // Catch:{ all -> 0x00d6 }
            java.lang.String r9 = r11.mAuctionFallback     // Catch:{ all -> 0x00d6 }
            int r10 = r11.mSessionDepth     // Catch:{ all -> 0x00d6 }
            r5.loadVideo(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x00d6 }
            monitor-exit(r12)     // Catch:{ all -> 0x00d6 }
            monitor-exit(r11)
            return
        L_0x00ad:
            boolean r4 = r5.isLoadingInProgress()     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x00b5
            r3 = 1
            goto L_0x0070
        L_0x00b5:
            boolean r4 = r5.isReadyToShow()     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x0070
            r2 = 1
            goto L_0x0070
        L_0x00bd:
            if (r2 != 0) goto L_0x00d3
            if (r3 != 0) goto L_0x00d3
            java.lang.String r13 = "onLoadError(): No other available smashes"
            r11.logInternal(r13)     // Catch:{ all -> 0x00d6 }
            r11.reportAvailabilityIfNeeded(r1)     // Catch:{ all -> 0x00d6 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r13 = com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE.RV_STATE_NOT_LOADED     // Catch:{ all -> 0x00d6 }
            r11.setState(r13)     // Catch:{ all -> 0x00d6 }
            com.ironsource.mediationsdk.RvLoadTrigger r13 = r11.mRvLoadTrigger     // Catch:{ all -> 0x00d6 }
            r13.loadError()     // Catch:{ all -> 0x00d6 }
        L_0x00d3:
            monitor-exit(r12)     // Catch:{ all -> 0x00d6 }
            monitor-exit(r11)
            return
        L_0x00d6:
            r13 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x00d6 }
            throw r13     // Catch:{ all -> 0x00d9 }
        L_0x00d9:
            r12 = move-exception
            monitor-exit(r11)
            goto L_0x00dd
        L_0x00dc:
            throw r12
        L_0x00dd:
            goto L_0x00dc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgRvManager.onLoadError(com.ironsource.mediationsdk.ProgRvSmash, java.lang.String):void");
    }

    public void onRewardedVideoAdOpened(ProgRvSmash progRvSmash) {
        synchronized (this) {
            this.mSessionDepth++;
            logSmashCallback(progRvSmash, "onRewardedVideoAdOpened");
            RVListenerWrapper.getInstance().onRewardedVideoAdOpened();
            if (this.mIsAuctionEnabled) {
                this.mAuctionHandler.reportImpression(this.mWaterfallServerData.get(progRvSmash.getInstanceName()));
            }
        }
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError, ProgRvSmash progRvSmash) {
        synchronized (this) {
            logSmashCallback(progRvSmash, "onRewardedVideoAdShowFailed error=" + ironSourceError.getErrorMessage());
            sendMediationEventWithPlacement(IronSourceConstants.RV_CALLBACK_SHOW_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
            RVListenerWrapper.getInstance().onRewardedVideoAdShowFailed(ironSourceError);
            this.mIsShowingVideo = false;
            if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
                reportAvailabilityIfNeeded(false);
            }
            this.mRvLoadTrigger.showError();
        }
    }

    public void onRewardedVideoAdClosed(ProgRvSmash progRvSmash) {
        synchronized (this) {
            logSmashCallback(progRvSmash, "onRewardedVideoAdClosed, mediation state: " + this.mState.name());
            RVListenerWrapper.getInstance().onRewardedVideoAdClosed();
            this.mIsShowingVideo = false;
            if (this.mState != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
                reportAvailabilityIfNeeded(false);
            }
            this.mRvLoadTrigger.showEnd();
        }
    }

    public void onRewardedVideoAdStarted(ProgRvSmash progRvSmash) {
        logSmashCallback(progRvSmash, "onRewardedVideoAdStarted");
        RVListenerWrapper.getInstance().onRewardedVideoAdStarted();
    }

    public void onRewardedVideoAdEnded(ProgRvSmash progRvSmash) {
        logSmashCallback(progRvSmash, "onRewardedVideoAdEnded");
        RVListenerWrapper.getInstance().onRewardedVideoAdEnded();
    }

    public void onRewardedVideoAdRewarded(ProgRvSmash progRvSmash, Placement placement) {
        logSmashCallback(progRvSmash, "onRewardedVideoAdRewarded");
        RVListenerWrapper.getInstance().onRewardedVideoAdRewarded(placement);
    }

    public void onRewardedVideoAdClicked(ProgRvSmash progRvSmash, Placement placement) {
        logSmashCallback(progRvSmash, "onRewardedVideoAdClicked");
        RVListenerWrapper.getInstance().onRewardedVideoAdClicked(placement);
    }

    private void reportAvailabilityIfNeeded(boolean z) {
        Boolean bool = this.mLastReportedAvailabilityState;
        if (bool == null || bool.booleanValue() != z) {
            this.mLastReportedAvailabilityState = Boolean.valueOf(z);
            long time = new Date().getTime() - this.mLastChangedAvailabilityTime;
            this.mLastChangedAvailabilityTime = new Date().getTime();
            if (z) {
                sendMediationEvent(IronSourceConstants.RV_CALLBACK_AVAILABILITY_TRUE, new Object[][]{new Object[]{"duration", Long.valueOf(time)}});
            } else {
                sendMediationEvent(IronSourceConstants.RV_CALLBACK_AVAILABILITY_FALSE, new Object[][]{new Object[]{"duration", Long.valueOf(time)}});
            }
            RVListenerWrapper.getInstance().onRewardedVideoAvailabilityChanged(z);
        }
    }

    private void logSmashCallback(ProgRvSmash progRvSmash, String str) {
        String str2 = progRvSmash.getInstanceName() + " : " + str;
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "ProgRvManager: " + str2, 0);
    }

    /* access modifiers changed from: private */
    public void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "ProgRvManager: " + str, 0);
    }

    private void logErrorInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "ProgRvManager: " + str, 3);
    }

    private void logAPIError(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, str, 3);
    }

    private void logApi(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, str, 1);
    }

    /* access modifiers changed from: private */
    public void sendMediationEventWithoutAuctionId(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, false, false);
    }

    /* access modifiers changed from: private */
    public void sendMediationEventWithoutAuctionId(int i) {
        sendMediationEvent(i, null, false, false);
    }

    /* access modifiers changed from: private */
    public void sendMediationEvent(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, false, true);
    }

    private void sendMediationEventWithPlacement(int i) {
        sendMediationEvent(i, null, true, true);
    }

    private void sendMediationEventWithPlacement(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, true, true);
    }

    private void sendMediationEvent(int i, Object[][] objArr, boolean z, boolean z2) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_PROGRAMMATIC, 1);
        if (z2 && !TextUtils.isEmpty(this.mCurrentAuctionId)) {
            hashMap.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && !TextUtils.isEmpty(this.mCurrentPlacement)) {
            hashMap.put("placement", this.mCurrentPlacement);
        }
        if (shouldAddAuctionParams(i)) {
            RewardedVideoEventsManager.getInstance().setEventAuctionParams(hashMap, this.mAuctionTrial, this.mAuctionFallback);
        }
        hashMap.put("sessionDepth", Integer.valueOf(this.mSessionDepth));
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    hashMap.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "ProgRvManager: RV sendMediationEvent " + Log.getStackTraceString(e), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    public synchronized void onLoadTriggered() {
        logInternal("onLoadTriggered: RV load was triggered in " + this.mState + " state");
        loadRewardedVideo(0);
    }

    public void onAuctionSuccess(List<AuctionResponseItem> list, String str, int i, long j) {
        logInternal("makeAuction(): success");
        this.mCurrentAuctionId = str;
        this.mAuctionTrial = i;
        this.mAuctionFallback = "";
        sendMediationEvent(IronSourceConstants.RV_AUCTION_SUCCESS, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        updateWaterfall(list);
        loadSmashes();
    }

    public void onAuctionFailed(int i, String str, int i2, String str2, long j) {
        logInternal("Auction failed | moving to fallback waterfall");
        this.mAuctionTrial = i2;
        this.mAuctionFallback = str2;
        if (TextUtils.isEmpty(str)) {
            sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{"duration", Long.valueOf(j)}});
        } else {
            sendMediationEventWithoutAuctionId(IronSourceConstants.RV_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str}, new Object[]{"duration", Long.valueOf(j)}});
        }
        updateWaterfallToNonBidding();
        loadSmashes();
    }
}
