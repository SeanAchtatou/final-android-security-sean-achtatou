package com.ironsource.mediationsdk;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;
import com.facebook.internal.ServerProtocol;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AuctionHandler {
    private static final int SERVER_REQUEST_TIMEOUT = 15000;
    private final String AUCTION_INTERNAL_ERROR_LOSS_CODE = "1";
    private final String AUCTION_LOSS_MACRO = "${AUCTION_LOSS}";
    private final String AUCTION_NOT_HIGHEST_RTB_BIDDER_LOSS_CODE = "102";
    private final String AUCTION_PRICE_MACRO = "${AUCTION_PRICE}";
    private String mAdUnit;
    private AuctionEventListener mAuctionListener;
    private String mSessionId;
    private AuctionSettings mSettings;

    public AuctionHandler(String str, AuctionSettings auctionSettings, AuctionEventListener auctionEventListener) {
        this.mAdUnit = str;
        this.mSettings = auctionSettings;
        this.mAuctionListener = auctionEventListener;
        this.mSessionId = IronSourceObject.getInstance().getSessionId();
    }

    public void executeAuction(Context context, Map<String, Object> map, List<String> list, int i) {
        try {
            boolean z = IronSourceUtils.getSerr() == 1;
            new AuctionHttpRequestTask(this.mAuctionListener).execute(this.mSettings.getUrl(), generateRequest(context, map, list, i, z), Boolean.valueOf(z), Integer.valueOf(this.mSettings.getNumOfMaxTrials()), Long.valueOf(this.mSettings.getTrialsInterval()));
        } catch (Exception e) {
            this.mAuctionListener.onAuctionFailed(1000, e.getMessage(), 0, "other", 0);
        }
    }

    public void reportImpression(AuctionResponseItem auctionResponseItem) {
        for (String str : auctionResponseItem.getBurls()) {
            new ImpressionHttpTask().execute(str);
        }
    }

    public void reportLoadSuccess(AuctionResponseItem auctionResponseItem) {
        for (String str : auctionResponseItem.getNurls()) {
            new ImpressionHttpTask().execute(str);
        }
    }

    public void reportAuctionLose(CopyOnWriteArrayList<ProgSmash> copyOnWriteArrayList, ConcurrentHashMap<String, AuctionResponseItem> concurrentHashMap, AuctionResponseItem auctionResponseItem) {
        String price = auctionResponseItem.getPrice();
        Iterator<ProgSmash> it = copyOnWriteArrayList.iterator();
        boolean z = false;
        while (it.hasNext()) {
            String instanceName = it.next().getInstanceName();
            if (instanceName.equals(auctionResponseItem.getInstanceName())) {
                z = true;
            } else {
                for (String replace : concurrentHashMap.get(instanceName).getLurls()) {
                    new ImpressionHttpTask().execute(replace.replace("${AUCTION_PRICE}", price).replace("${AUCTION_LOSS}", z ? "102" : "1"));
                }
            }
        }
    }

    private JSONObject generateRequest(Context context, Map<String, Object> map, List<String> list, int i, boolean z) throws JSONException {
        boolean z2;
        String str = "false";
        JSONObject jSONObject = new JSONObject();
        for (String next : map.keySet()) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, 2);
            jSONObject2.put("biddingAdditionalData", new JSONObject((Map) map.get(next)));
            jSONObject.put(next, jSONObject2);
        }
        for (String put : list) {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(IronSourceConstants.EVENTS_INSTANCE_TYPE, 1);
            jSONObject.put(put, jSONObject3);
        }
        JSONObject jSONObject4 = new JSONObject();
        jSONObject4.put(Constants.RequestParameters.APPLICATION_USER_ID, IronSourceObject.getInstance().getIronSourceUserId());
        String gender = IronSourceObject.getInstance().getGender();
        if (TextUtils.isEmpty(gender)) {
            gender = "unknown";
        }
        jSONObject4.put("applicationUserGender", gender);
        int age = IronSourceObject.getInstance().getAge();
        if (age == null) {
            age = -1;
        }
        jSONObject4.put("applicationUserAge", age);
        Boolean consent = IronSourceObject.getInstance().getConsent();
        if (consent != null) {
            jSONObject4.put(Constants.RequestParameters.CONSENT, consent.booleanValue() ? 1 : 0);
        }
        jSONObject4.put(Constants.RequestParameters.MOBILE_CARRIER, DeviceStatus.getMobileCarrier(context));
        jSONObject4.put(Constants.RequestParameters.CONNECTION_TYPE, IronSourceUtils.getConnectionType(context));
        jSONObject4.put("deviceOS", "android");
        jSONObject4.put("deviceWidth", context.getResources().getConfiguration().screenWidthDp);
        jSONObject4.put("deviceHeight", context.getResources().getConfiguration().screenHeightDp);
        jSONObject4.put(Constants.RequestParameters.DEVICE_OS_VERSION, Build.VERSION.SDK_INT + "(" + Build.VERSION.RELEASE + ")");
        jSONObject4.put(Constants.RequestParameters.DEVICE_MODEL, Build.MODEL);
        jSONObject4.put("deviceMake", Build.MANUFACTURER);
        jSONObject4.put(Constants.RequestParameters.PACKAGE_NAME, context.getPackageName());
        jSONObject4.put("appVersion", ApplicationContext.getPublisherApplicationVersion(context, context.getPackageName()));
        jSONObject4.put("clientTimestamp", new Date().getTime());
        try {
            String orGenerateOnceUniqueIdentifier = DeviceStatus.getOrGenerateOnceUniqueIdentifier(context);
            String str2 = IronSourceConstants.TYPE_UUID;
            String[] advertisingIdInfo = DeviceStatus.getAdvertisingIdInfo(context);
            if (advertisingIdInfo.length == 2) {
                z2 = Boolean.valueOf(advertisingIdInfo[1]).booleanValue();
                if (!TextUtils.isEmpty(advertisingIdInfo[0])) {
                    orGenerateOnceUniqueIdentifier = advertisingIdInfo[0];
                    str2 = IronSourceConstants.TYPE_GAID;
                }
            } else {
                z2 = false;
            }
            jSONObject4.put("advId", orGenerateOnceUniqueIdentifier);
            jSONObject4.put("advIdType", str2);
            jSONObject4.put(Constants.RequestParameters.isLAT, z2 ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : str);
        } catch (Exception unused) {
        }
        JSONObject jSONObject5 = new JSONObject();
        jSONObject5.put("adUnit", this.mAdUnit);
        jSONObject5.put("auctionData", this.mSettings.getAuctionData());
        jSONObject5.put(Constants.RequestParameters.APPLICATION_KEY, IronSourceObject.getInstance().getIronSourceAppKey());
        jSONObject5.put(Constants.RequestParameters.SDK_VERSION, IronSourceUtils.getSDKVersion());
        jSONObject5.put("clientParams", jSONObject4);
        jSONObject5.put("sessionDepth", i);
        jSONObject5.put("sessionId", this.mSessionId);
        if (!z) {
            str = ServerProtocol.DIALOG_RETURN_SCOPES_TRUE;
        }
        jSONObject5.put("doNotEncryptResponse", str);
        jSONObject5.put("instances", jSONObject);
        return jSONObject5;
    }

    static class AuctionHttpRequestTask extends AsyncTask<Object, Void, Boolean> {
        private String mAuctionFallback = "other";
        private String mAuctionId;
        private WeakReference<AuctionEventListener> mAuctionListener;
        private int mCurrentAuctionTrial;
        private int mErrorCode;
        private String mErrorMessage;
        private JSONObject mRequestData;
        private long mRequestStartTime;
        private List<AuctionResponseItem> mWaterfall;

        AuctionHttpRequestTask(AuctionEventListener auctionEventListener) {
            this.mAuctionListener = new WeakReference<>(auctionEventListener);
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Object... objArr) {
            this.mRequestStartTime = new Date().getTime();
            try {
                URL url = new URL((String) objArr[0]);
                this.mRequestData = (JSONObject) objArr[1];
                boolean booleanValue = ((Boolean) objArr[2]).booleanValue();
                int intValue = ((Integer) objArr[3]).intValue();
                long longValue = ((Long) objArr[4]).longValue();
                this.mCurrentAuctionTrial = 0;
                HttpURLConnection httpURLConnection = null;
                while (this.mCurrentAuctionTrial < intValue) {
                    try {
                        long time = new Date().getTime();
                        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "Auction Handler: auction trial " + (this.mCurrentAuctionTrial + 1) + " out of " + intValue + " max trials", 0);
                        httpURLConnection = prepareAuctionRequest(url, longValue);
                        sendAuctionRequest(httpURLConnection, this.mRequestData);
                        int responseCode = httpURLConnection.getResponseCode();
                        if (responseCode != 200) {
                            this.mErrorCode = 1001;
                            this.mErrorMessage = String.valueOf(responseCode);
                            httpURLConnection.disconnect();
                            if (this.mCurrentAuctionTrial < intValue - 1) {
                                waitUntilNextTrial(longValue, time);
                            }
                            this.mCurrentAuctionTrial++;
                        } else {
                            try {
                                handleResponse(readResponse(httpURLConnection), booleanValue);
                                httpURLConnection.disconnect();
                                return true;
                            } catch (JSONException unused) {
                                this.mErrorCode = 1002;
                                this.mErrorMessage = "failed parsing auction response";
                                this.mAuctionFallback = "parsing";
                                httpURLConnection.disconnect();
                                return false;
                            }
                        }
                    } catch (SocketTimeoutException unused2) {
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        this.mErrorCode = 1006;
                        this.mErrorMessage = "Connection timed out";
                    } catch (Exception e) {
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        this.mErrorCode = 1000;
                        this.mErrorMessage = e.getMessage();
                        this.mAuctionFallback = "other";
                        return false;
                    }
                }
                this.mCurrentAuctionTrial = intValue - 1;
                this.mAuctionFallback = "trials_fail";
                return false;
            } catch (Exception e2) {
                this.mErrorCode = 1007;
                this.mErrorMessage = e2.getMessage();
                this.mCurrentAuctionTrial = 0;
                this.mAuctionFallback = "other";
                return false;
            }
        }

        private void waitUntilNextTrial(long j, long j2) {
            long time = j - (new Date().getTime() - j2);
            if (time > 0) {
                SystemClock.sleep(time);
            }
        }

        private void sendAuctionRequest(HttpURLConnection httpURLConnection, JSONObject jSONObject) throws IOException {
            OutputStream outputStream = httpURLConnection.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(String.format("{\"request\" : \"%1$s\"}", IronSourceAES.encode(IronSourceUtils.KEY, jSONObject.toString())));
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStreamWriter.close();
            outputStream.close();
        }

        private HttpURLConnection prepareAuctionRequest(URL url, long j) throws IOException {
            HttpURLConnection httpURLConnection = (HttpURLConnection) ((URLConnection) FirebasePerfUrlConnection.instrument(url.openConnection()));
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            httpURLConnection.setReadTimeout((int) j);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            return httpURLConnection;
        }

        private void handleResponse(String str, boolean z) throws JSONException {
            if (!TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (z) {
                    jSONObject = new JSONObject(IronSourceAES.decode(IronSourceUtils.KEY, jSONObject.getString(ServerResponseWrapper.RESPONSE_FIELD)));
                }
                this.mAuctionId = jSONObject.getString(IronSourceConstants.EVENTS_AUCTION_ID);
                this.mWaterfall = new ArrayList();
                JSONArray jSONArray = jSONObject.getJSONArray("waterfall");
                int i = 0;
                while (i < jSONArray.length()) {
                    AuctionResponseItem auctionResponseItem = new AuctionResponseItem(jSONArray.getJSONObject(i));
                    if (auctionResponseItem.isValid()) {
                        this.mWaterfall.add(auctionResponseItem);
                        i++;
                    } else {
                        this.mErrorCode = 1002;
                        this.mErrorMessage = "waterfall " + i;
                        throw new JSONException("invalid response");
                    }
                }
                return;
            }
            throw new JSONException("empty response");
        }

        private String readResponse(HttpURLConnection httpURLConnection) throws IOException {
            InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    bufferedReader.close();
                    inputStreamReader.close();
                    return sb.toString();
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            AuctionEventListener auctionEventListener = this.mAuctionListener.get();
            if (auctionEventListener != null) {
                long time = new Date().getTime() - this.mRequestStartTime;
                if (bool.booleanValue()) {
                    auctionEventListener.onAuctionSuccess(this.mWaterfall, this.mAuctionId, this.mCurrentAuctionTrial + 1, time);
                } else {
                    auctionEventListener.onAuctionFailed(this.mErrorCode, this.mErrorMessage, this.mCurrentAuctionTrial + 1, this.mAuctionFallback, time);
                }
            }
        }
    }

    static class ImpressionHttpTask extends AsyncTask<String, Void, Boolean> {
        ImpressionHttpTask() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... strArr) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) ((URLConnection) FirebasePerfUrlConnection.instrument(new URL(strArr[0]).openConnection()));
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setReadTimeout(AuctionHandler.SERVER_REQUEST_TIMEOUT);
                httpURLConnection.setConnectTimeout(AuctionHandler.SERVER_REQUEST_TIMEOUT);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                httpURLConnection.disconnect();
                return Boolean.valueOf(responseCode == 200);
            } catch (Exception unused) {
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute((Object) bool);
        }
    }
}
