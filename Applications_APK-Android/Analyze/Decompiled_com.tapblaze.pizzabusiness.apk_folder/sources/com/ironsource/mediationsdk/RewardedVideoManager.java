package com.ironsource.mediationsdk;

import android.content.Context;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import com.ironsource.environment.NetworkStateReceiver;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.ListenersWrapper;
import com.ironsource.mediationsdk.sdk.RewardedVideoManagerListener;
import com.ironsource.mediationsdk.server.Server;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.DailyCappingListener;
import com.ironsource.mediationsdk.utils.DailyCappingManager;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;

class RewardedVideoManager extends AbstractAdUnitManager implements RewardedVideoManagerListener, NetworkStateReceiver.NetworkStateReceiverListener, DailyCappingListener {
    private final String TAG = getClass().getSimpleName();
    private Placement mCurrentPlacement;
    private boolean mIsCurrentlyShowing = false;
    private boolean mIsUltraEventsEnabled = false;
    private ListenersWrapper mListenersWrapper;
    private long mLoadStartTime = new Date().getTime();
    private int mManualLoadInterval;
    private NetworkStateReceiver mNetworkStateReceiver;
    private boolean mPauseSmartLoadDueToNetworkUnavailability = false;
    private boolean mShouldSendMediationLoadSuccessEvent = false;
    private List<AbstractSmash.MEDIATION_STATE> mStatesToIgnore = Arrays.asList(AbstractSmash.MEDIATION_STATE.INIT_FAILED, AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION, AbstractSmash.MEDIATION_STATE.EXHAUSTED, AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY);
    private Timer mTimer = null;

    RewardedVideoManager() {
        this.mDailyCappingManager = new DailyCappingManager("rewarded_video", this);
    }

    public void setRewardedVideoListener(ListenersWrapper listenersWrapper) {
        this.mListenersWrapper = listenersWrapper;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e9, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void initRewardedVideo(android.app.Activity r9, java.lang.String r10, java.lang.String r11) {
        /*
            r8 = this;
            monitor-enter(r8)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r8.mLoggerManager     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x00ea }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ea }
            r2.<init>()     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = r8.TAG     // Catch:{ all -> 0x00ea }
            r2.append(r3)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = ":initRewardedVideo(appKey: "
            r2.append(r3)     // Catch:{ all -> 0x00ea }
            r2.append(r10)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = ", userId: "
            r2.append(r3)     // Catch:{ all -> 0x00ea }
            r2.append(r11)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = ")"
            r2.append(r3)     // Catch:{ all -> 0x00ea }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00ea }
            r3 = 1
            r0.log(r1, r2, r3)     // Catch:{ all -> 0x00ea }
            java.util.Date r0 = new java.util.Date     // Catch:{ all -> 0x00ea }
            r0.<init>()     // Catch:{ all -> 0x00ea }
            long r0 = r0.getTime()     // Catch:{ all -> 0x00ea }
            r2 = 81312(0x13da0, float:1.13942E-40)
            r8.logMediationEvent(r2)     // Catch:{ all -> 0x00ea }
            r8.mAppKey = r10     // Catch:{ all -> 0x00ea }
            r8.mUserId = r11     // Catch:{ all -> 0x00ea }
            r8.mActivity = r9     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.utils.DailyCappingManager r9 = r8.mDailyCappingManager     // Catch:{ all -> 0x00ea }
            android.app.Activity r10 = r8.mActivity     // Catch:{ all -> 0x00ea }
            r9.setContext(r10)     // Catch:{ all -> 0x00ea }
            java.util.concurrent.CopyOnWriteArrayList r9 = r8.mSmashArray     // Catch:{ all -> 0x00ea }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x00ea }
            r10 = 0
            r11 = 0
        L_0x0050:
            boolean r2 = r9.hasNext()     // Catch:{ all -> 0x00ea }
            r4 = 2
            if (r2 == 0) goto L_0x0088
            java.lang.Object r2 = r9.next()     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.AbstractSmash r2 = (com.ironsource.mediationsdk.AbstractSmash) r2     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.utils.DailyCappingManager r5 = r8.mDailyCappingManager     // Catch:{ all -> 0x00ea }
            boolean r5 = r5.shouldSendCapReleasedEvent(r2)     // Catch:{ all -> 0x00ea }
            if (r5 == 0) goto L_0x0078
            r5 = 150(0x96, float:2.1E-43)
            java.lang.Object[][] r6 = new java.lang.Object[r3][]     // Catch:{ all -> 0x00ea }
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00ea }
            java.lang.String r7 = "status"
            r4[r10] = r7     // Catch:{ all -> 0x00ea }
            java.lang.String r7 = "false"
            r4[r3] = r7     // Catch:{ all -> 0x00ea }
            r6[r10] = r4     // Catch:{ all -> 0x00ea }
            r8.logProviderEvent(r5, r2, r6)     // Catch:{ all -> 0x00ea }
        L_0x0078:
            com.ironsource.mediationsdk.utils.DailyCappingManager r4 = r8.mDailyCappingManager     // Catch:{ all -> 0x00ea }
            boolean r4 = r4.isCapped(r2)     // Catch:{ all -> 0x00ea }
            if (r4 == 0) goto L_0x0050
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r4 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY     // Catch:{ all -> 0x00ea }
            r2.setMediationState(r4)     // Catch:{ all -> 0x00ea }
            int r11 = r11 + 1
            goto L_0x0050
        L_0x0088:
            java.util.concurrent.CopyOnWriteArrayList r9 = r8.mSmashArray     // Catch:{ all -> 0x00ea }
            int r9 = r9.size()     // Catch:{ all -> 0x00ea }
            if (r11 != r9) goto L_0x0097
            com.ironsource.mediationsdk.sdk.ListenersWrapper r9 = r8.mListenersWrapper     // Catch:{ all -> 0x00ea }
            r9.onRewardedVideoAvailabilityChanged(r10)     // Catch:{ all -> 0x00ea }
            monitor-exit(r8)
            return
        L_0x0097:
            r9 = 1000(0x3e8, float:1.401E-42)
            r8.logMediationEvent(r9)     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.sdk.ListenersWrapper r9 = r8.mListenersWrapper     // Catch:{ all -> 0x00ea }
            r11 = 0
            r9.setRvPlacement(r11)     // Catch:{ all -> 0x00ea }
            r8.mShouldSendMediationLoadSuccessEvent = r3     // Catch:{ all -> 0x00ea }
            java.util.Date r9 = new java.util.Date     // Catch:{ all -> 0x00ea }
            r9.<init>()     // Catch:{ all -> 0x00ea }
            long r5 = r9.getTime()     // Catch:{ all -> 0x00ea }
            r8.mLoadStartTime = r5     // Catch:{ all -> 0x00ea }
            java.util.Date r9 = new java.util.Date     // Catch:{ all -> 0x00ea }
            r9.<init>()     // Catch:{ all -> 0x00ea }
            long r5 = r9.getTime()     // Catch:{ all -> 0x00ea }
            long r5 = r5 - r0
            r9 = 81313(0x13da1, float:1.13944E-40)
            java.lang.Object[][] r11 = new java.lang.Object[r3][]     // Catch:{ all -> 0x00ea }
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x00ea }
            java.lang.String r1 = "duration"
            r0[r10] = r1     // Catch:{ all -> 0x00ea }
            java.lang.Long r1 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x00ea }
            r0[r3] = r1     // Catch:{ all -> 0x00ea }
            r11[r10] = r0     // Catch:{ all -> 0x00ea }
            r8.logMediationEvent(r9, r11)     // Catch:{ all -> 0x00ea }
            r8.prepareSDK5()     // Catch:{ all -> 0x00ea }
        L_0x00d2:
            int r9 = r8.mSmartLoadAmount     // Catch:{ all -> 0x00ea }
            if (r10 >= r9) goto L_0x00e8
            java.util.concurrent.CopyOnWriteArrayList r9 = r8.mSmashArray     // Catch:{ all -> 0x00ea }
            int r9 = r9.size()     // Catch:{ all -> 0x00ea }
            if (r10 >= r9) goto L_0x00e8
            com.ironsource.mediationsdk.AbstractAdapter r9 = r8.loadNextAdapter()     // Catch:{ all -> 0x00ea }
            if (r9 != 0) goto L_0x00e5
            goto L_0x00e8
        L_0x00e5:
            int r10 = r10 + 1
            goto L_0x00d2
        L_0x00e8:
            monitor-exit(r8)
            return
        L_0x00ea:
            r9 = move-exception
            monitor-exit(r8)
            goto L_0x00ee
        L_0x00ed:
            throw r9
        L_0x00ee:
            goto L_0x00ed
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.initRewardedVideo(android.app.Activity, java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x015c, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01d2, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void showRewardedVideo(java.lang.String r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r10.mLoggerManager     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x01d3 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x01d3 }
            r2.<init>()     // Catch:{ all -> 0x01d3 }
            java.lang.String r3 = r10.TAG     // Catch:{ all -> 0x01d3 }
            r2.append(r3)     // Catch:{ all -> 0x01d3 }
            java.lang.String r3 = ":showRewardedVideo(placementName: "
            r2.append(r3)     // Catch:{ all -> 0x01d3 }
            r2.append(r11)     // Catch:{ all -> 0x01d3 }
            java.lang.String r3 = ")"
            r2.append(r3)     // Catch:{ all -> 0x01d3 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x01d3 }
            r3 = 1
            r0.log(r1, r2, r3)     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.sdk.ListenersWrapper r0 = r10.mListenersWrapper     // Catch:{ all -> 0x01d3 }
            r0.setRvPlacement(r11)     // Catch:{ all -> 0x01d3 }
            r0 = 1100(0x44c, float:1.541E-42)
            java.lang.Object[][] r1 = new java.lang.Object[r3][]     // Catch:{ all -> 0x01d3 }
            r2 = 2
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch:{ all -> 0x01d3 }
            java.lang.String r5 = "placement"
            r6 = 0
            r4[r6] = r5     // Catch:{ all -> 0x01d3 }
            r4[r3] = r11     // Catch:{ all -> 0x01d3 }
            r1[r6] = r4     // Catch:{ all -> 0x01d3 }
            r10.logMediationEvent(r0, r1)     // Catch:{ all -> 0x01d3 }
            boolean r0 = r10.mIsCurrentlyShowing     // Catch:{ all -> 0x01d3 }
            r1 = 1113(0x459, float:1.56E-42)
            r4 = 3
            if (r0 == 0) goto L_0x0077
            java.lang.String r0 = "showRewardedVideo error: can't show ad while an ad is already showing"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.mLoggerManager     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x01d3 }
            r5.log(r7, r0, r4)     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.logger.IronSourceError r4 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x01d3 }
            r5 = 1022(0x3fe, float:1.432E-42)
            r4.<init>(r5, r0)     // Catch:{ all -> 0x01d3 }
            java.lang.Object[][] r0 = new java.lang.Object[r2][]     // Catch:{ all -> 0x01d3 }
            java.lang.Object[] r7 = new java.lang.Object[r2]     // Catch:{ all -> 0x01d3 }
            java.lang.String r8 = "placement"
            r7[r6] = r8     // Catch:{ all -> 0x01d3 }
            r7[r3] = r11     // Catch:{ all -> 0x01d3 }
            r0[r6] = r7     // Catch:{ all -> 0x01d3 }
            java.lang.Object[] r11 = new java.lang.Object[r2]     // Catch:{ all -> 0x01d3 }
            java.lang.String r2 = "errorCode"
            r11[r6] = r2     // Catch:{ all -> 0x01d3 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x01d3 }
            r11[r3] = r2     // Catch:{ all -> 0x01d3 }
            r0[r3] = r11     // Catch:{ all -> 0x01d3 }
            r10.logMediationEvent(r1, r0)     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.sdk.ListenersWrapper r11 = r10.mListenersWrapper     // Catch:{ all -> 0x01d3 }
            r11.onInterstitialAdShowFailed(r4)     // Catch:{ all -> 0x01d3 }
            monitor-exit(r10)
            return
        L_0x0077:
            android.app.Activity r0 = r10.mActivity     // Catch:{ all -> 0x01d3 }
            boolean r0 = com.ironsource.mediationsdk.utils.IronSourceUtils.isNetworkConnected(r0)     // Catch:{ all -> 0x01d3 }
            if (r0 != 0) goto L_0x00b4
            java.lang.String r0 = "showRewardedVideo error: can't show ad when there's no internet connection"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.mLoggerManager     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x01d3 }
            r5.log(r7, r0, r4)     // Catch:{ all -> 0x01d3 }
            java.lang.Object[][] r0 = new java.lang.Object[r2][]     // Catch:{ all -> 0x01d3 }
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch:{ all -> 0x01d3 }
            java.lang.String r5 = "placement"
            r4[r6] = r5     // Catch:{ all -> 0x01d3 }
            r4[r3] = r11     // Catch:{ all -> 0x01d3 }
            r0[r6] = r4     // Catch:{ all -> 0x01d3 }
            java.lang.Object[] r11 = new java.lang.Object[r2]     // Catch:{ all -> 0x01d3 }
            java.lang.String r2 = "errorCode"
            r11[r6] = r2     // Catch:{ all -> 0x01d3 }
            r2 = 520(0x208, float:7.29E-43)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x01d3 }
            r11[r3] = r2     // Catch:{ all -> 0x01d3 }
            r0[r3] = r11     // Catch:{ all -> 0x01d3 }
            r10.logMediationEvent(r1, r0)     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.sdk.ListenersWrapper r11 = r10.mListenersWrapper     // Catch:{ all -> 0x01d3 }
            java.lang.String r0 = "Rewarded Video"
            com.ironsource.mediationsdk.logger.IronSourceError r0 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildNoInternetConnectionShowFailError(r0)     // Catch:{ all -> 0x01d3 }
            r11.onRewardedVideoAdShowFailed(r0)     // Catch:{ all -> 0x01d3 }
            monitor-exit(r10)
            return
        L_0x00b4:
            r11 = 0
            r0 = 0
            r1 = 0
        L_0x00b7:
            java.util.concurrent.CopyOnWriteArrayList r4 = r10.mSmashArray     // Catch:{ all -> 0x01d3 }
            int r4 = r4.size()     // Catch:{ all -> 0x01d3 }
            if (r11 >= r4) goto L_0x01a9
            java.util.concurrent.CopyOnWriteArrayList r4 = r10.mSmashArray     // Catch:{ all -> 0x01d3 }
            java.lang.Object r4 = r4.get(r11)     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.AbstractSmash r4 = (com.ironsource.mediationsdk.AbstractSmash) r4     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.mLoggerManager     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.INTERNAL     // Catch:{ all -> 0x01d3 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x01d3 }
            r8.<init>()     // Catch:{ all -> 0x01d3 }
            java.lang.String r9 = "showRewardedVideo, iterating on: "
            r8.append(r9)     // Catch:{ all -> 0x01d3 }
            java.lang.String r9 = r4.getInstanceName()     // Catch:{ all -> 0x01d3 }
            r8.append(r9)     // Catch:{ all -> 0x01d3 }
            java.lang.String r9 = ", Status: "
            r8.append(r9)     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r9 = r4.getMediationState()     // Catch:{ all -> 0x01d3 }
            r8.append(r9)     // Catch:{ all -> 0x01d3 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x01d3 }
            r5.log(r7, r8, r6)     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r5 = r4.getMediationState()     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r7 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.AVAILABLE     // Catch:{ all -> 0x01d3 }
            if (r5 != r7) goto L_0x0187
            r5 = r4
            com.ironsource.mediationsdk.RewardedVideoSmash r5 = (com.ironsource.mediationsdk.RewardedVideoSmash) r5     // Catch:{ all -> 0x01d3 }
            boolean r5 = r5.isRewardedVideoAvailable()     // Catch:{ all -> 0x01d3 }
            if (r5 == 0) goto L_0x015d
            r10.showAdapter(r4, r11)     // Catch:{ all -> 0x01d3 }
            boolean r11 = r10.mCanShowPremium     // Catch:{ all -> 0x01d3 }
            if (r11 == 0) goto L_0x0114
            com.ironsource.mediationsdk.AbstractSmash r11 = r10.getPremiumSmash()     // Catch:{ all -> 0x01d3 }
            boolean r11 = r4.equals(r11)     // Catch:{ all -> 0x01d3 }
            if (r11 != 0) goto L_0x0114
            r10.disablePremiumForCurrentSession()     // Catch:{ all -> 0x01d3 }
        L_0x0114:
            boolean r11 = r4.isCappedPerSession()     // Catch:{ all -> 0x01d3 }
            if (r11 == 0) goto L_0x012b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r11 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION     // Catch:{ all -> 0x01d3 }
            r4.setMediationState(r11)     // Catch:{ all -> 0x01d3 }
            r11 = 1401(0x579, float:1.963E-42)
            r0 = 0
            java.lang.Object[][] r0 = (java.lang.Object[][]) r0     // Catch:{ all -> 0x01d3 }
            r10.logProviderEvent(r11, r4, r0)     // Catch:{ all -> 0x01d3 }
            r10.completeAdapterCap()     // Catch:{ all -> 0x01d3 }
            goto L_0x015b
        L_0x012b:
            com.ironsource.mediationsdk.utils.DailyCappingManager r11 = r10.mDailyCappingManager     // Catch:{ all -> 0x01d3 }
            boolean r11 = r11.isCapped(r4)     // Catch:{ all -> 0x01d3 }
            if (r11 == 0) goto L_0x014f
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r11 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY     // Catch:{ all -> 0x01d3 }
            r4.setMediationState(r11)     // Catch:{ all -> 0x01d3 }
            r11 = 150(0x96, float:2.1E-43)
            java.lang.Object[][] r0 = new java.lang.Object[r3][]     // Catch:{ all -> 0x01d3 }
            java.lang.Object[] r1 = new java.lang.Object[r2]     // Catch:{ all -> 0x01d3 }
            java.lang.String r2 = "status"
            r1[r6] = r2     // Catch:{ all -> 0x01d3 }
            java.lang.String r2 = "true"
            r1[r3] = r2     // Catch:{ all -> 0x01d3 }
            r0[r6] = r1     // Catch:{ all -> 0x01d3 }
            r10.logProviderEvent(r11, r4, r0)     // Catch:{ all -> 0x01d3 }
            r10.completeAdapterCap()     // Catch:{ all -> 0x01d3 }
            goto L_0x015b
        L_0x014f:
            boolean r11 = r4.isExhausted()     // Catch:{ all -> 0x01d3 }
            if (r11 == 0) goto L_0x015b
            r10.loadNextAdapter()     // Catch:{ all -> 0x01d3 }
            r10.completeIterationRound()     // Catch:{ all -> 0x01d3 }
        L_0x015b:
            monitor-exit(r10)
            return
        L_0x015d:
            r5 = r4
            com.ironsource.mediationsdk.RewardedVideoSmash r5 = (com.ironsource.mediationsdk.RewardedVideoSmash) r5     // Catch:{ all -> 0x01d3 }
            r10.onRewardedVideoAvailabilityChanged(r6, r5)     // Catch:{ all -> 0x01d3 }
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ all -> 0x01d3 }
            java.lang.String r7 = "FailedToShowVideoException"
            r5.<init>(r7)     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r7 = r10.mLoggerManager     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r8 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.INTERNAL     // Catch:{ all -> 0x01d3 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x01d3 }
            r9.<init>()     // Catch:{ all -> 0x01d3 }
            java.lang.String r4 = r4.getInstanceName()     // Catch:{ all -> 0x01d3 }
            r9.append(r4)     // Catch:{ all -> 0x01d3 }
            java.lang.String r4 = " Failed to show video"
            r9.append(r4)     // Catch:{ all -> 0x01d3 }
            java.lang.String r4 = r9.toString()     // Catch:{ all -> 0x01d3 }
            r7.logException(r8, r4, r5)     // Catch:{ all -> 0x01d3 }
            goto L_0x01a5
        L_0x0187:
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r5 = r4.getMediationState()     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r7 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION     // Catch:{ all -> 0x01d3 }
            if (r5 == r7) goto L_0x01a3
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r5 = r4.getMediationState()     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r7 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY     // Catch:{ all -> 0x01d3 }
            if (r5 != r7) goto L_0x0198
            goto L_0x01a3
        L_0x0198:
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r4 = r4.getMediationState()     // Catch:{ all -> 0x01d3 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r5 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE     // Catch:{ all -> 0x01d3 }
            if (r4 != r5) goto L_0x01a5
            int r1 = r1 + 1
            goto L_0x01a5
        L_0x01a3:
            int r0 = r0 + 1
        L_0x01a5:
            int r11 = r11 + 1
            goto L_0x00b7
        L_0x01a9:
            boolean r11 = r10.isBackFillAvailable()     // Catch:{ all -> 0x01d3 }
            if (r11 == 0) goto L_0x01bd
            com.ironsource.mediationsdk.AbstractSmash r11 = r10.getBackfillSmash()     // Catch:{ all -> 0x01d3 }
            java.util.concurrent.CopyOnWriteArrayList r0 = r10.mSmashArray     // Catch:{ all -> 0x01d3 }
            int r0 = r0.size()     // Catch:{ all -> 0x01d3 }
            r10.showAdapter(r11, r0)     // Catch:{ all -> 0x01d3 }
            goto L_0x01d1
        L_0x01bd:
            int r0 = r0 + r1
            java.util.concurrent.CopyOnWriteArrayList r11 = r10.mSmashArray     // Catch:{ all -> 0x01d3 }
            int r11 = r11.size()     // Catch:{ all -> 0x01d3 }
            if (r0 != r11) goto L_0x01d1
            com.ironsource.mediationsdk.sdk.ListenersWrapper r11 = r10.mListenersWrapper     // Catch:{ all -> 0x01d3 }
            java.lang.String r0 = "Rewarded Video"
            com.ironsource.mediationsdk.logger.IronSourceError r0 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildNoAdsToShowError(r0)     // Catch:{ all -> 0x01d3 }
            r11.onRewardedVideoAdShowFailed(r0)     // Catch:{ all -> 0x01d3 }
        L_0x01d1:
            monitor-exit(r10)
            return
        L_0x01d3:
            r11 = move-exception
            monitor-exit(r10)
            goto L_0x01d7
        L_0x01d6:
            throw r11
        L_0x01d7:
            goto L_0x01d6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.showRewardedVideo(java.lang.String):void");
    }

    public synchronized boolean isRewardedVideoAvailable() {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
        ironSourceLoggerManager.log(ironSourceTag, this.TAG + ":isRewardedVideoAvailable()", 1);
        if (this.mPauseSmartLoadDueToNetworkUnavailability) {
            return false;
        }
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.isMediationAvailable() && ((RewardedVideoSmash) abstractSmash).isRewardedVideoAvailable()) {
                return true;
            }
        }
        return false;
    }

    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError, RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, rewardedVideoSmash.getInstanceName() + ":onRewardedVideoAdShowFailed(" + ironSourceError + ")", 1);
        this.mIsCurrentlyShowing = false;
        logProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW_FAILED, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}, new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
        sendMediationLoadEvents();
        this.mListenersWrapper.onRewardedVideoAdShowFailed(ironSourceError);
    }

    public void onRewardedVideoAdOpened(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, rewardedVideoSmash.getInstanceName() + ":onRewardedVideoAdOpened()", 1);
        logProviderEvent(1005, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        this.mListenersWrapper.onRewardedVideoAdOpened();
    }

    public void onRewardedVideoAdClosed(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, rewardedVideoSmash.getInstanceName() + ":onRewardedVideoAdClosed()", 1);
        this.mIsCurrentlyShowing = false;
        verifyOnPauseOnResume();
        logProviderEvent(IronSourceConstants.RV_INSTANCE_CLOSED, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        if (!rewardedVideoSmash.isCappedPerSession() && !this.mDailyCappingManager.isCapped(rewardedVideoSmash)) {
            logProviderEvent(1001, rewardedVideoSmash, null);
        }
        sendMediationLoadEvents();
        this.mListenersWrapper.onRewardedVideoAdClosed();
        Iterator it = this.mSmashArray.iterator();
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.INTERNAL;
            ironSourceLoggerManager2.log(ironSourceTag2, "Fetch on ad closed, iterating on: " + abstractSmash.getInstanceName() + ", Status: " + abstractSmash.getMediationState(), 0);
            if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE) {
                try {
                    if (!abstractSmash.getInstanceName().equals(rewardedVideoSmash.getInstanceName())) {
                        IronSourceLoggerManager ironSourceLoggerManager3 = this.mLoggerManager;
                        IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.INTERNAL;
                        ironSourceLoggerManager3.log(ironSourceTag3, abstractSmash.getInstanceName() + ":reload smash", 1);
                        ((RewardedVideoSmash) abstractSmash).fetchRewardedVideo();
                        logProviderEvent(1001, abstractSmash, null);
                    }
                } catch (Throwable th) {
                    IronSourceLoggerManager ironSourceLoggerManager4 = this.mLoggerManager;
                    IronSourceLogger.IronSourceTag ironSourceTag4 = IronSourceLogger.IronSourceTag.NATIVE;
                    ironSourceLoggerManager4.log(ironSourceTag4, abstractSmash.getInstanceName() + " Failed to call fetchVideo(), " + th.getLocalizedMessage(), 1);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0073, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00be, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onRewardedVideoAvailabilityChanged(boolean r9, com.ironsource.mediationsdk.RewardedVideoSmash r10) {
        /*
            r8 = this;
            monitor-enter(r8)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r8.mLoggerManager     // Catch:{ all -> 0x0122 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK     // Catch:{ all -> 0x0122 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0122 }
            r2.<init>()     // Catch:{ all -> 0x0122 }
            java.lang.String r3 = r10.getInstanceName()     // Catch:{ all -> 0x0122 }
            r2.append(r3)     // Catch:{ all -> 0x0122 }
            java.lang.String r3 = ": onRewardedVideoAvailabilityChanged(available:"
            r2.append(r3)     // Catch:{ all -> 0x0122 }
            r2.append(r9)     // Catch:{ all -> 0x0122 }
            java.lang.String r3 = ")"
            r2.append(r3)     // Catch:{ all -> 0x0122 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0122 }
            r3 = 1
            r0.log(r1, r2, r3)     // Catch:{ all -> 0x0122 }
            boolean r0 = r8.mPauseSmartLoadDueToNetworkUnavailability     // Catch:{ all -> 0x0122 }
            if (r0 == 0) goto L_0x002c
            monitor-exit(r8)
            return
        L_0x002c:
            r0 = 0
            if (r9 == 0) goto L_0x0057
            boolean r1 = r8.mShouldSendMediationLoadSuccessEvent     // Catch:{ all -> 0x0122 }
            if (r1 == 0) goto L_0x0057
            r8.mShouldSendMediationLoadSuccessEvent = r0     // Catch:{ all -> 0x0122 }
            java.util.Date r1 = new java.util.Date     // Catch:{ all -> 0x0122 }
            r1.<init>()     // Catch:{ all -> 0x0122 }
            long r1 = r1.getTime()     // Catch:{ all -> 0x0122 }
            long r4 = r8.mLoadStartTime     // Catch:{ all -> 0x0122 }
            long r1 = r1 - r4
            r4 = 1003(0x3eb, float:1.406E-42)
            java.lang.Object[][] r5 = new java.lang.Object[r3][]     // Catch:{ all -> 0x0122 }
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x0122 }
            java.lang.String r7 = "duration"
            r6[r0] = r7     // Catch:{ all -> 0x0122 }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0122 }
            r6[r3] = r1     // Catch:{ all -> 0x0122 }
            r5[r0] = r6     // Catch:{ all -> 0x0122 }
            r8.logMediationEvent(r4, r5)     // Catch:{ all -> 0x0122 }
        L_0x0057:
            com.ironsource.mediationsdk.AbstractSmash r1 = r8.getBackfillSmash()     // Catch:{ all -> 0x00f1 }
            boolean r1 = r10.equals(r1)     // Catch:{ all -> 0x00f1 }
            if (r1 == 0) goto L_0x0074
            boolean r0 = r8.shouldNotifyAvailabilityChanged(r9)     // Catch:{ all -> 0x00f1 }
            if (r0 == 0) goto L_0x0072
            com.ironsource.mediationsdk.sdk.ListenersWrapper r0 = r8.mListenersWrapper     // Catch:{ all -> 0x00f1 }
            java.lang.Boolean r1 = r8.mLastMediationAvailabilityState     // Catch:{ all -> 0x00f1 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x00f1 }
            r0.onRewardedVideoAvailabilityChanged(r1)     // Catch:{ all -> 0x00f1 }
        L_0x0072:
            monitor-exit(r8)
            return
        L_0x0074:
            com.ironsource.mediationsdk.AbstractSmash r1 = r8.getPremiumSmash()     // Catch:{ all -> 0x00f1 }
            boolean r1 = r10.equals(r1)     // Catch:{ all -> 0x00f1 }
            if (r1 == 0) goto L_0x00bf
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r8.mLoggerManager     // Catch:{ all -> 0x00f1 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK     // Catch:{ all -> 0x00f1 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f1 }
            r4.<init>()     // Catch:{ all -> 0x00f1 }
            java.lang.String r5 = r10.getInstanceName()     // Catch:{ all -> 0x00f1 }
            r4.append(r5)     // Catch:{ all -> 0x00f1 }
            java.lang.String r5 = " is a premium adapter, canShowPremium: "
            r4.append(r5)     // Catch:{ all -> 0x00f1 }
            boolean r5 = r8.canShowPremium()     // Catch:{ all -> 0x00f1 }
            r4.append(r5)     // Catch:{ all -> 0x00f1 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00f1 }
            r1.log(r2, r4, r3)     // Catch:{ all -> 0x00f1 }
            boolean r1 = r8.canShowPremium()     // Catch:{ all -> 0x00f1 }
            if (r1 != 0) goto L_0x00bf
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r1 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION     // Catch:{ all -> 0x00f1 }
            r10.setMediationState(r1)     // Catch:{ all -> 0x00f1 }
            boolean r0 = r8.shouldNotifyAvailabilityChanged(r0)     // Catch:{ all -> 0x00f1 }
            if (r0 == 0) goto L_0x00bd
            com.ironsource.mediationsdk.sdk.ListenersWrapper r0 = r8.mListenersWrapper     // Catch:{ all -> 0x00f1 }
            java.lang.Boolean r1 = r8.mLastMediationAvailabilityState     // Catch:{ all -> 0x00f1 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x00f1 }
            r0.onRewardedVideoAvailabilityChanged(r1)     // Catch:{ all -> 0x00f1 }
        L_0x00bd:
            monitor-exit(r8)
            return
        L_0x00bf:
            boolean r1 = r10.isMediationAvailable()     // Catch:{ all -> 0x00f1 }
            if (r1 == 0) goto L_0x0120
            com.ironsource.mediationsdk.utils.DailyCappingManager r1 = r8.mDailyCappingManager     // Catch:{ all -> 0x00f1 }
            boolean r1 = r1.isCapped(r10)     // Catch:{ all -> 0x00f1 }
            if (r1 != 0) goto L_0x0120
            if (r9 == 0) goto L_0x00e1
            boolean r0 = r8.shouldNotifyAvailabilityChanged(r3)     // Catch:{ all -> 0x00f1 }
            if (r0 == 0) goto L_0x0120
            com.ironsource.mediationsdk.sdk.ListenersWrapper r0 = r8.mListenersWrapper     // Catch:{ all -> 0x00f1 }
            java.lang.Boolean r1 = r8.mLastMediationAvailabilityState     // Catch:{ all -> 0x00f1 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x00f1 }
            r0.onRewardedVideoAvailabilityChanged(r1)     // Catch:{ all -> 0x00f1 }
            goto L_0x0120
        L_0x00e1:
            boolean r0 = r8.shouldNotifyAvailabilityChanged(r0)     // Catch:{ all -> 0x00f1 }
            if (r0 == 0) goto L_0x00ea
            r8.notifyAvailabilityChange()     // Catch:{ all -> 0x00f1 }
        L_0x00ea:
            r8.loadNextAdapter()     // Catch:{ all -> 0x00f1 }
            r8.completeIterationRound()     // Catch:{ all -> 0x00f1 }
            goto L_0x0120
        L_0x00f1:
            r0 = move-exception
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r8.mLoggerManager     // Catch:{ all -> 0x0122 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK     // Catch:{ all -> 0x0122 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0122 }
            r3.<init>()     // Catch:{ all -> 0x0122 }
            java.lang.String r4 = "onRewardedVideoAvailabilityChanged(available:"
            r3.append(r4)     // Catch:{ all -> 0x0122 }
            r3.append(r9)     // Catch:{ all -> 0x0122 }
            java.lang.String r9 = ", "
            r3.append(r9)     // Catch:{ all -> 0x0122 }
            java.lang.String r9 = "provider:"
            r3.append(r9)     // Catch:{ all -> 0x0122 }
            java.lang.String r9 = r10.getName()     // Catch:{ all -> 0x0122 }
            r3.append(r9)     // Catch:{ all -> 0x0122 }
            java.lang.String r9 = ")"
            r3.append(r9)     // Catch:{ all -> 0x0122 }
            java.lang.String r9 = r3.toString()     // Catch:{ all -> 0x0122 }
            r1.logException(r2, r9, r0)     // Catch:{ all -> 0x0122 }
        L_0x0120:
            monitor-exit(r8)
            return
        L_0x0122:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.onRewardedVideoAvailabilityChanged(boolean, com.ironsource.mediationsdk.RewardedVideoSmash):void");
    }

    public void onRewardedVideoAdStarted(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, rewardedVideoSmash.getInstanceName() + ":onRewardedVideoAdStarted()", 1);
        logProviderEvent(IronSourceConstants.RV_INSTANCE_STARTED, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        this.mListenersWrapper.onRewardedVideoAdStarted();
    }

    public void onRewardedVideoAdEnded(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, rewardedVideoSmash.getInstanceName() + ":onRewardedVideoAdEnded()", 1);
        logProviderEvent(IronSourceConstants.RV_INSTANCE_ENDED, rewardedVideoSmash, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        this.mListenersWrapper.onRewardedVideoAdEnded();
    }

    public void onRewardedVideoAdRewarded(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, rewardedVideoSmash.getInstanceName() + ":onRewardedVideoAdRewarded()", 1);
        if (this.mCurrentPlacement == null) {
            this.mCurrentPlacement = IronSourceObject.getInstance().getCurrentServerResponse().getConfigurations().getRewardedVideoConfigurations().getDefaultRewardedVideoPlacement();
        }
        JSONObject providerAdditionalData = IronSourceUtils.getProviderAdditionalData(rewardedVideoSmash);
        try {
            if (this.mCurrentPlacement != null) {
                providerAdditionalData.put("placement", this.mCurrentPlacement.getPlacementName());
                providerAdditionalData.put(IronSourceConstants.EVENTS_REWARD_NAME, this.mCurrentPlacement.getRewardName());
                providerAdditionalData.put(IronSourceConstants.EVENTS_REWARD_AMOUNT, this.mCurrentPlacement.getRewardAmount());
            } else {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        EventData eventData = new EventData(1010, providerAdditionalData);
        if (!TextUtils.isEmpty(this.mAppKey)) {
            eventData.addToAdditionalData(IronSourceConstants.EVENTS_TRANS_ID, IronSourceUtils.getTransId("" + Long.toString(eventData.getTimeStamp()) + this.mAppKey + rewardedVideoSmash.getName()));
            if (!TextUtils.isEmpty(IronSourceObject.getInstance().getDynamicUserId())) {
                eventData.addToAdditionalData(IronSourceConstants.EVENTS_DYNAMIC_USER_ID, IronSourceObject.getInstance().getDynamicUserId());
            }
            Map<String, String> rvServerParams = IronSourceObject.getInstance().getRvServerParams();
            if (rvServerParams != null) {
                for (String next : rvServerParams.keySet()) {
                    eventData.addToAdditionalData("custom_" + next, rvServerParams.get(next));
                }
            }
        }
        RewardedVideoEventsManager.getInstance().log(eventData);
        Placement placement = this.mCurrentPlacement;
        if (placement != null) {
            this.mListenersWrapper.onRewardedVideoAdRewarded(placement);
        } else {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
        }
    }

    public void onRewardedVideoAdClicked(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, rewardedVideoSmash.getInstanceName() + ":onRewardedVideoAdClicked()", 1);
        if (this.mCurrentPlacement == null) {
            this.mCurrentPlacement = IronSourceObject.getInstance().getCurrentServerResponse().getConfigurations().getRewardedVideoConfigurations().getDefaultRewardedVideoPlacement();
        }
        Placement placement = this.mCurrentPlacement;
        if (placement == null) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
            return;
        }
        logProviderEvent(1006, rewardedVideoSmash, new Object[][]{new Object[]{"placement", placement.getPlacementName()}});
        this.mListenersWrapper.onRewardedVideoAdClicked(this.mCurrentPlacement);
    }

    public void onRewardedVideoAdVisible(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.log(ironSourceTag, rewardedVideoSmash.getInstanceName() + ":onRewardedVideoAdVisible()", 1);
        Placement placement = this.mCurrentPlacement;
        if (placement != null) {
            logProviderEvent(IronSourceConstants.RV_INSTANCE_VISIBLE, rewardedVideoSmash, new Object[][]{new Object[]{"placement", placement.getPlacementName()}});
            return;
        }
        this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
    }

    public void onNetworkAvailabilityChanged(boolean z) {
        if (this.mShouldTrackNetworkState) {
            IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            ironSourceLoggerManager.log(ironSourceTag, "Network Availability Changed To: " + z, 0);
            if (shouldNotifyNetworkAvailabilityChanged(z)) {
                this.mPauseSmartLoadDueToNetworkUnavailability = !z;
                this.mListenersWrapper.onRewardedVideoAvailabilityChanged(z);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void shouldTrackNetworkState(Context context, boolean z) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        ironSourceLoggerManager.log(ironSourceTag, this.TAG + " Should Track Network State: " + z, 0);
        this.mShouldTrackNetworkState = z;
        if (this.mShouldTrackNetworkState) {
            if (this.mNetworkStateReceiver == null) {
                this.mNetworkStateReceiver = new NetworkStateReceiver(context, this);
            }
            context.getApplicationContext().registerReceiver(this.mNetworkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } else if (this.mNetworkStateReceiver != null) {
            context.getApplicationContext().unregisterReceiver(this.mNetworkStateReceiver);
        }
    }

    private boolean shouldNotifyNetworkAvailabilityChanged(boolean z) {
        if (this.mLastMediationAvailabilityState == null) {
            return false;
        }
        if (z && !this.mLastMediationAvailabilityState.booleanValue() && hasAvailableSmash()) {
            this.mLastMediationAvailabilityState = true;
            return true;
        } else if (z || !this.mLastMediationAvailabilityState.booleanValue()) {
            return false;
        } else {
            this.mLastMediationAvailabilityState = false;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void setIsUltraEventsEnabled(boolean z) {
        this.mIsUltraEventsEnabled = z;
    }

    private void reportFalseImpressionsOnHigherPriority(int i, int i2) {
        int i3 = 0;
        while (i3 < i && i3 < this.mSmashArray.size()) {
            if (!this.mStatesToIgnore.contains(((AbstractSmash) this.mSmashArray.get(i3)).getMediationState())) {
                reportImpression(((RewardedVideoSmash) this.mSmashArray.get(i3)).getRequestUrl(), false, i2);
            }
            i3++;
        }
    }

    private synchronized void reportImpression(String str, boolean z, int i) {
        String str2 = "";
        try {
            str2 = (str2 + str) + "&sdkVersion=" + IronSourceUtils.getSDKVersion();
            Server.callAsyncRequestURL(str2, z, i);
        } catch (Throwable th) {
            this.mLoggerManager.logException(IronSourceLogger.IronSourceTag.NETWORK, "reportImpression:(providerURL:" + str2 + ", " + "hit:" + z + ")", th);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentPlacement(Placement placement) {
        this.mCurrentPlacement = placement;
        this.mListenersWrapper.setRvPlacement(placement.getPlacementName());
    }

    /* access modifiers changed from: protected */
    public synchronized void disablePremiumForCurrentSession() {
        super.disablePremiumForCurrentSession();
        Iterator it = this.mSmashArray.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.equals(getPremiumSmash())) {
                abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION);
                loadNextAdapter();
                break;
            }
        }
    }

    private synchronized AbstractAdapter startAdapter(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.mLoggerManager;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
        ironSourceLoggerManager.log(ironSourceTag, this.TAG + ":startAdapter(" + rewardedVideoSmash.getInstanceName() + ")", 1);
        AbstractAdapter adapter = AdapterRepository.getInstance().getAdapter(rewardedVideoSmash.mAdapterConfigs, rewardedVideoSmash.mAdapterConfigs.getRewardedVideoSettings(), this.mActivity);
        if (adapter == null) {
            IronSourceLoggerManager ironSourceLoggerManager2 = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager2.log(ironSourceTag2, rewardedVideoSmash.getInstanceName() + " is configured in IronSource's platform, but the adapter is not integrated", 2);
            return null;
        }
        rewardedVideoSmash.setAdapterForSmash(adapter);
        rewardedVideoSmash.setMediationState(AbstractSmash.MEDIATION_STATE.INITIATED);
        setCustomParams(rewardedVideoSmash);
        logProviderEvent(1001, rewardedVideoSmash, null);
        try {
            rewardedVideoSmash.initRewardedVideo(this.mActivity, this.mAppKey, this.mUserId);
            return adapter;
        } catch (Throwable th) {
            IronSourceLoggerManager ironSourceLoggerManager3 = this.mLoggerManager;
            IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager3.logException(ironSourceTag3, this.TAG + "failed to init adapter: " + rewardedVideoSmash.getName() + "v", th);
            rewardedVideoSmash.setMediationState(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
            return null;
        }
    }

    private AbstractAdapter loadNextAdapter() {
        AbstractAdapter abstractAdapter = null;
        int i = 0;
        for (int i2 = 0; i2 < this.mSmashArray.size() && abstractAdapter == null; i2++) {
            if (((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE || ((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == AbstractSmash.MEDIATION_STATE.INITIATED) {
                i++;
                if (i >= this.mSmartLoadAmount) {
                    break;
                }
            } else if (((AbstractSmash) this.mSmashArray.get(i2)).getMediationState() == AbstractSmash.MEDIATION_STATE.NOT_INITIATED && (abstractAdapter = startAdapter((RewardedVideoSmash) this.mSmashArray.get(i2))) == null) {
                ((AbstractSmash) this.mSmashArray.get(i2)).setMediationState(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
            }
        }
        return abstractAdapter;
    }

    private synchronized void showAdapter(AbstractSmash abstractSmash, int i) {
        Object[][] objArr;
        CappingManager.incrementShowCounter(this.mActivity, this.mCurrentPlacement);
        if (CappingManager.isRvPlacementCapped(this.mActivity, this.mCurrentPlacement.getPlacementName())) {
            logMediationEvent(1400, new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}});
        }
        this.mDailyCappingManager.increaseShowCounter(abstractSmash);
        if (this.mCurrentPlacement != null) {
            if (this.mIsUltraEventsEnabled) {
                reportImpression(((RewardedVideoSmash) abstractSmash).getRequestUrl(), true, this.mCurrentPlacement.getPlacementId());
                reportFalseImpressionsOnHigherPriority(i, this.mCurrentPlacement.getPlacementId());
            }
            sendShowChanceEvents(abstractSmash, i, this.mCurrentPlacement.getPlacementName());
        } else {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
        }
        if (this.mCurrentPlacement != null) {
            objArr = new Object[][]{new Object[]{"placement", this.mCurrentPlacement.getPlacementName()}};
        } else {
            objArr = null;
        }
        logProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW, abstractSmash, objArr);
        this.mIsCurrentlyShowing = true;
        ((RewardedVideoSmash) abstractSmash).showRewardedVideo();
    }

    /* access modifiers changed from: package-private */
    public void setManualLoadInterval(int i) {
        this.mManualLoadInterval = i;
    }

    /* access modifiers changed from: private */
    public void scheduleFetchTimer() {
        if (this.mManualLoadInterval <= 0) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "load interval is not set, ignoring", 1);
            return;
        }
        Timer timer = this.mTimer;
        if (timer != null) {
            timer.cancel();
        }
        this.mTimer = new Timer();
        this.mTimer.schedule(new TimerTask() {
            public void run() {
                cancel();
                RewardedVideoManager.this.loadRewardedVideo();
                RewardedVideoManager.this.scheduleFetchTimer();
            }
        }, (long) (this.mManualLoadInterval * 1000));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0094, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0096, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void loadRewardedVideo() {
        /*
            r7 = this;
            monitor-enter(r7)
            android.app.Activity r0 = r7.mActivity     // Catch:{ all -> 0x0097 }
            boolean r0 = com.ironsource.mediationsdk.utils.IronSourceUtils.isNetworkConnected(r0)     // Catch:{ all -> 0x0097 }
            if (r0 == 0) goto L_0x0095
            java.lang.Boolean r0 = r7.mLastMediationAvailabilityState     // Catch:{ all -> 0x0097 }
            if (r0 != 0) goto L_0x000f
            goto L_0x0095
        L_0x000f:
            java.lang.Boolean r0 = r7.mLastMediationAvailabilityState     // Catch:{ all -> 0x0097 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0097 }
            if (r0 != 0) goto L_0x0093
            r0 = 102(0x66, float:1.43E-43)
            r7.logMediationEvent(r0)     // Catch:{ all -> 0x0097 }
            r0 = 1000(0x3e8, float:1.401E-42)
            r7.logMediationEvent(r0)     // Catch:{ all -> 0x0097 }
            r0 = 1
            r7.mShouldSendMediationLoadSuccessEvent = r0     // Catch:{ all -> 0x0097 }
            java.util.concurrent.CopyOnWriteArrayList r1 = r7.mSmashArray     // Catch:{ all -> 0x0097 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x0097 }
        L_0x002a:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x0097 }
            if (r2 == 0) goto L_0x0093
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x0097 }
            com.ironsource.mediationsdk.AbstractSmash r2 = (com.ironsource.mediationsdk.AbstractSmash) r2     // Catch:{ all -> 0x0097 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = r2.getMediationState()     // Catch:{ all -> 0x0097 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r4 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE     // Catch:{ all -> 0x0097 }
            if (r3 != r4) goto L_0x002a
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r3 = r7.mLoggerManager     // Catch:{ all -> 0x006e }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r4 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.INTERNAL     // Catch:{ all -> 0x006e }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x006e }
            r5.<init>()     // Catch:{ all -> 0x006e }
            java.lang.String r6 = "Fetch from timer: "
            r5.append(r6)     // Catch:{ all -> 0x006e }
            java.lang.String r6 = r2.getInstanceName()     // Catch:{ all -> 0x006e }
            r5.append(r6)     // Catch:{ all -> 0x006e }
            java.lang.String r6 = ":reload smash"
            r5.append(r6)     // Catch:{ all -> 0x006e }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x006e }
            r3.log(r4, r5, r0)     // Catch:{ all -> 0x006e }
            r3 = 1001(0x3e9, float:1.403E-42)
            r4 = 0
            java.lang.Object[][] r4 = (java.lang.Object[][]) r4     // Catch:{ all -> 0x006e }
            r7.logProviderEvent(r3, r2, r4)     // Catch:{ all -> 0x006e }
            r3 = r2
            com.ironsource.mediationsdk.RewardedVideoSmash r3 = (com.ironsource.mediationsdk.RewardedVideoSmash) r3     // Catch:{ all -> 0x006e }
            r3.fetchRewardedVideo()     // Catch:{ all -> 0x006e }
            goto L_0x002a
        L_0x006e:
            r3 = move-exception
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r4 = r7.mLoggerManager     // Catch:{ all -> 0x0097 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r5 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.NATIVE     // Catch:{ all -> 0x0097 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0097 }
            r6.<init>()     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = r2.getInstanceName()     // Catch:{ all -> 0x0097 }
            r6.append(r2)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = " Failed to call fetchVideo(), "
            r6.append(r2)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = r3.getLocalizedMessage()     // Catch:{ all -> 0x0097 }
            r6.append(r2)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = r6.toString()     // Catch:{ all -> 0x0097 }
            r4.log(r5, r2, r0)     // Catch:{ all -> 0x0097 }
            goto L_0x002a
        L_0x0093:
            monitor-exit(r7)
            return
        L_0x0095:
            monitor-exit(r7)
            return
        L_0x0097:
            r0 = move-exception
            monitor-exit(r7)
            goto L_0x009b
        L_0x009a:
            throw r0
        L_0x009b:
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.loadRewardedVideo():void");
    }

    private synchronized boolean shouldNotifyAvailabilityChanged(boolean z) {
        boolean z2;
        z2 = true;
        if (this.mLastMediationAvailabilityState == null) {
            scheduleFetchTimer();
            if (z) {
                this.mLastMediationAvailabilityState = true;
            } else if (!isBackFillAvailable() && isAllAdaptersInactive()) {
                this.mLastMediationAvailabilityState = false;
            }
        } else if (z && !this.mLastMediationAvailabilityState.booleanValue()) {
            this.mLastMediationAvailabilityState = true;
        } else if (!z && this.mLastMediationAvailabilityState.booleanValue() && !hasAvailableSmash() && !isBackFillAvailable()) {
            this.mLastMediationAvailabilityState = false;
        }
        z2 = false;
        return z2;
    }

    private synchronized boolean isAllAdaptersInactive() {
        boolean z;
        Iterator it = this.mSmashArray.iterator();
        z = false;
        int i = 0;
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.INIT_FAILED || abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY || abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION || abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE || abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.EXHAUSTED) {
                i++;
            }
        }
        if (this.mSmashArray.size() == i) {
            z = true;
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean isAvailableAdaptersToLoad() {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.concurrent.CopyOnWriteArrayList r0 = r4.mSmashArray     // Catch:{ all -> 0x0041 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0041 }
        L_0x0007:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x003e
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash r1 = (com.ironsource.mediationsdk.AbstractSmash) r1     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE     // Catch:{ all -> 0x0041 }
            if (r2 == r3) goto L_0x003b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.AVAILABLE     // Catch:{ all -> 0x0041 }
            if (r2 == r3) goto L_0x003b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ all -> 0x0041 }
            if (r2 == r3) goto L_0x003b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INIT_PENDING     // Catch:{ all -> 0x0041 }
            if (r2 == r3) goto L_0x003b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r1 = r1.getMediationState()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING     // Catch:{ all -> 0x0041 }
            if (r1 != r2) goto L_0x0007
        L_0x003b:
            r0 = 1
            monitor-exit(r4)
            return r0
        L_0x003e:
            r0 = 0
            monitor-exit(r4)
            return r0
        L_0x0041:
            r0 = move-exception
            monitor-exit(r4)
            goto L_0x0045
        L_0x0044:
            throw r0
        L_0x0045:
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.isAvailableAdaptersToLoad():boolean");
    }

    private synchronized boolean hasAvailableSmash() {
        boolean z;
        z = false;
        Iterator it = this.mSmashArray.iterator();
        while (true) {
            if (it.hasNext()) {
                if (((AbstractSmash) it.next()).getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                    z = true;
                    break;
                }
            } else {
                break;
            }
        }
        return z;
    }

    private synchronized boolean isBackFillAvailable() {
        if (getBackfillSmash() == null) {
            return false;
        }
        return ((RewardedVideoSmash) getBackfillSmash()).isRewardedVideoAvailable();
    }

    private void sendShowChanceEvents(AbstractSmash abstractSmash, int i, String str) {
        logProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW_CHANCE, abstractSmash, new Object[][]{new Object[]{"placement", str}, new Object[]{"status", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE}});
        int i2 = 0;
        while (i2 < this.mSmashArray.size() && i2 < i) {
            AbstractSmash abstractSmash2 = (AbstractSmash) this.mSmashArray.get(i2);
            if (abstractSmash2.getMediationState() == AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE) {
                logProviderEvent(IronSourceConstants.RV_INSTANCE_SHOW_CHANCE, abstractSmash2, new Object[][]{new Object[]{"placement", str}, new Object[]{"status", "false"}});
            }
            i2++;
        }
    }

    private synchronized void notifyAvailabilityChange() {
        if (getBackfillSmash() != null && !this.mBackFillInitStarted) {
            this.mBackFillInitStarted = true;
            if (startAdapter((RewardedVideoSmash) getBackfillSmash()) == null) {
                this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
            }
        } else if (!isBackFillAvailable()) {
            this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
        } else if (shouldNotifyAvailabilityChanged(true)) {
            this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void completeAdapterCap() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.ironsource.mediationsdk.AbstractAdapter r0 = r4.loadNextAdapter()     // Catch:{ all -> 0x0037 }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r4)
            return
        L_0x0009:
            r0 = 3
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE[] r0 = new com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[r0]     // Catch:{ all -> 0x0037 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r1 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE     // Catch:{ all -> 0x0037 }
            r2 = 0
            r0[r2] = r1     // Catch:{ all -> 0x0037 }
            r1 = 1
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION     // Catch:{ all -> 0x0037 }
            r0[r1] = r3     // Catch:{ all -> 0x0037 }
            r1 = 2
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY     // Catch:{ all -> 0x0037 }
            r0[r1] = r3     // Catch:{ all -> 0x0037 }
            int r0 = r4.smashesCount(r0)     // Catch:{ all -> 0x0037 }
            java.util.concurrent.CopyOnWriteArrayList r1 = r4.mSmashArray     // Catch:{ all -> 0x0037 }
            int r1 = r1.size()     // Catch:{ all -> 0x0037 }
            if (r0 >= r1) goto L_0x002c
            r4.completeIterationRound()     // Catch:{ all -> 0x0037 }
            monitor-exit(r4)
            return
        L_0x002c:
            boolean r0 = r4.shouldNotifyAvailabilityChanged(r2)     // Catch:{ all -> 0x0037 }
            if (r0 == 0) goto L_0x0035
            r4.notifyAvailabilityChange()     // Catch:{ all -> 0x0037 }
        L_0x0035:
            monitor-exit(r4)
            return
        L_0x0037:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.completeAdapterCap():void");
    }

    private synchronized void completeIterationRound() {
        if (isIterationRoundComplete()) {
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "Reset Iteration", 0);
            Iterator it = this.mSmashArray.iterator();
            boolean z = false;
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.EXHAUSTED) {
                    abstractSmash.completeIteration();
                }
                if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                    z = true;
                }
            }
            this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "End of Reset Iteration", 0);
            if (shouldNotifyAvailabilityChanged(z)) {
                this.mListenersWrapper.onRewardedVideoAvailabilityChanged(this.mLastMediationAvailabilityState.booleanValue());
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean isIterationRoundComplete() {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.concurrent.CopyOnWriteArrayList r0 = r4.mSmashArray     // Catch:{ all -> 0x0031 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0031 }
        L_0x0007:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x0031 }
            if (r1 == 0) goto L_0x002e
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash r1 = (com.ironsource.mediationsdk.AbstractSmash) r1     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_INITIATED     // Catch:{ all -> 0x0031 }
            if (r2 == r3) goto L_0x002b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.getMediationState()     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ all -> 0x0031 }
            if (r2 == r3) goto L_0x002b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r1 = r1.getMediationState()     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.AVAILABLE     // Catch:{ all -> 0x0031 }
            if (r1 != r2) goto L_0x0007
        L_0x002b:
            r0 = 0
            monitor-exit(r4)
            return r0
        L_0x002e:
            r0 = 1
            monitor-exit(r4)
            return r0
        L_0x0031:
            r0 = move-exception
            monitor-exit(r4)
            goto L_0x0035
        L_0x0034:
            throw r0
        L_0x0035:
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.isIterationRoundComplete():boolean");
    }

    private void logMediationEvent(int i) {
        logMediationEvent(i, null);
    }

    private void logMediationEvent(int i, Object[][] objArr) {
        JSONObject mediationAdditionalData = IronSourceUtils.getMediationAdditionalData(false);
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    mediationAdditionalData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "RewardedVideoManager logMediationEvent " + Log.getStackTraceString(e), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, mediationAdditionalData));
    }

    private void logProviderEvent(int i, AbstractSmash abstractSmash, Object[][] objArr) {
        JSONObject providerAdditionalData = IronSourceUtils.getProviderAdditionalData(abstractSmash);
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerAdditionalData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                this.mLoggerManager.log(IronSourceLogger.IronSourceTag.INTERNAL, "RewardedVideoManager logProviderEvent " + Log.getStackTraceString(e), 3);
            }
        }
        RewardedVideoEventsManager.getInstance().log(new EventData(i, providerAdditionalData));
    }

    private int smashesCount(AbstractSmash.MEDIATION_STATE... mediation_stateArr) {
        int i;
        synchronized (this.mSmashArray) {
            Iterator it = this.mSmashArray.iterator();
            i = 0;
            while (it.hasNext()) {
                AbstractSmash abstractSmash = (AbstractSmash) it.next();
                int i2 = i;
                for (AbstractSmash.MEDIATION_STATE mediation_state : mediation_stateArr) {
                    if (abstractSmash.getMediationState() == mediation_state) {
                        i2++;
                    }
                }
                i = i2;
            }
        }
        return i;
    }

    private void sendMediationLoadEvents() {
        if (isRewardedVideoAvailable()) {
            logMediationEvent(1000);
            logMediationEvent(1003, new Object[][]{new Object[]{"duration", 0}});
            this.mShouldSendMediationLoadSuccessEvent = false;
        } else if (isAvailableAdaptersToLoad()) {
            logMediationEvent(1000);
            this.mShouldSendMediationLoadSuccessEvent = true;
            this.mLoadStartTime = new Date().getTime();
        }
    }

    public void onDailyCapReleased() {
        Iterator it = this.mSmashArray.iterator();
        boolean z = false;
        while (it.hasNext()) {
            AbstractSmash abstractSmash = (AbstractSmash) it.next();
            if (abstractSmash.getMediationState() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY) {
                logProviderEvent(IronSourceConstants.REWARDED_VIDEO_DAILY_CAPPED, abstractSmash, new Object[][]{new Object[]{"status", "false"}});
                abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE);
                if (((RewardedVideoSmash) abstractSmash).isRewardedVideoAvailable() && abstractSmash.isMediationAvailable()) {
                    abstractSmash.setMediationState(AbstractSmash.MEDIATION_STATE.AVAILABLE);
                    z = true;
                }
            }
        }
        if (z && shouldNotifyAvailabilityChanged(true)) {
            this.mListenersWrapper.onRewardedVideoAvailabilityChanged(true);
        }
    }

    private void prepareSDK5() {
        for (int i = 0; i < this.mSmashArray.size(); i++) {
            String providerTypeForReflection = ((AbstractSmash) this.mSmashArray.get(i)).mAdapterConfigs.getProviderTypeForReflection();
            if (providerTypeForReflection.equalsIgnoreCase(IronSourceConstants.IRONSOURCE_CONFIG_NAME) || providerTypeForReflection.equalsIgnoreCase(IronSourceConstants.SUPERSONIC_CONFIG_NAME)) {
                AdapterRepository.getInstance().getAdapter(((AbstractSmash) this.mSmashArray.get(i)).mAdapterConfigs, ((AbstractSmash) this.mSmashArray.get(i)).mAdapterConfigs.getRewardedVideoSettings(), this.mActivity);
                return;
            }
        }
    }
}
