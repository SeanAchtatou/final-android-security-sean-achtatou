package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionCappingManager;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.constants.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;

class ProgIsManager implements ProgIsManagerListener, AuctionEventListener {
    /* access modifiers changed from: private */
    public Context mAppContext;
    private String mAuctionFallback = "";
    /* access modifiers changed from: private */
    public AuctionHandler mAuctionHandler;
    private int mAuctionTrial;
    /* access modifiers changed from: private */
    public String mCurrentAuctionId;
    private String mCurrentPlacement;
    /* access modifiers changed from: private */
    public long mInitMangerTime;
    private boolean mIsAuctionEnabled;
    private long mLoadStartTime;
    private int mMaxSmashesToLoad;
    /* access modifiers changed from: private */
    public SessionCappingManager mSessionCappingManager;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, ProgIsSmash> mSmashes;
    private MEDIATION_STATE mState;
    /* access modifiers changed from: private */
    public long mTimeToWaitBeforeFirstAction;
    private CopyOnWriteArrayList<ProgIsSmash> mWaterfall;
    private ConcurrentHashMap<String, AuctionResponseItem> mWaterfallServerData;

    enum MEDIATION_STATE {
        STATE_NOT_INITIALIZED,
        STATE_READY_TO_LOAD,
        STATE_AUCTION,
        STATE_LOADING_SMASHES,
        STATE_READY_TO_SHOW,
        STATE_SHOWING
    }

    private boolean shouldAddAuctionParams(int i) {
        return i == 2002 || i == 2003 || i == 2200 || i == 2005 || i == 2204 || i == 2201 || i == 2203 || i == 2006 || i == 2004 || i == 2110 || i == 2301 || i == 2300;
    }

    public ProgIsManager(Activity activity, List<ProviderSettings> list, InterstitialConfigurations interstitialConfigurations, String str, String str2, int i) {
        long time = new Date().getTime();
        sendMediationEvent(IronSourceConstants.IS_MANAGER_INIT_STARTED);
        setState(MEDIATION_STATE.STATE_NOT_INITIALIZED);
        this.mSmashes = new ConcurrentHashMap<>();
        this.mWaterfall = new CopyOnWriteArrayList<>();
        this.mWaterfallServerData = new ConcurrentHashMap<>();
        this.mCurrentPlacement = "";
        this.mCurrentAuctionId = "";
        this.mAppContext = activity.getApplicationContext();
        this.mMaxSmashesToLoad = interstitialConfigurations.getInterstitialAdaptersSmartLoadAmount();
        CallbackThrottler.getInstance().setDelayLoadFailureNotificationInSeconds(i);
        AuctionSettings interstitialAuctionSettings = interstitialConfigurations.getInterstitialAuctionSettings();
        this.mTimeToWaitBeforeFirstAction = interstitialAuctionSettings.getTimeToWaitBeforeFirstAuctionMs();
        this.mIsAuctionEnabled = interstitialAuctionSettings.getNumOfMaxTrials() > 0;
        if (this.mIsAuctionEnabled) {
            this.mAuctionHandler = new AuctionHandler("interstitial", interstitialAuctionSettings, this);
        }
        for (ProviderSettings next : list) {
            AbstractAdapter adapter = AdapterRepository.getInstance().getAdapter(next, next.getInterstitialSettings(), activity);
            if (adapter != null && AdaptersCompatibilityHandler.getInstance().isAdapterVersionISCompatible(adapter)) {
                ProgIsSmash progIsSmash = new ProgIsSmash(activity, str, str2, next, this, interstitialConfigurations.getInterstitialAdaptersSmartLoadTimeout(), adapter);
                this.mSmashes.put(progIsSmash.getInstanceName(), progIsSmash);
            }
        }
        this.mSessionCappingManager = new SessionCappingManager(new ArrayList(this.mSmashes.values()));
        for (ProgIsSmash next2 : this.mSmashes.values()) {
            if (next2.isBidder()) {
                next2.initForBidding();
            }
        }
        this.mInitMangerTime = new Date().getTime();
        setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
        sendMediationEvent(IronSourceConstants.IS_MANAGER_INIT_ENDED, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - time)}});
    }

    public void onResume(Activity activity) {
        for (ProgIsSmash onResume : this.mSmashes.values()) {
            onResume.onResume(activity);
        }
    }

    public void onPause(Activity activity) {
        for (ProgIsSmash onPause : this.mSmashes.values()) {
            onPause.onPause(activity);
        }
    }

    /* access modifiers changed from: private */
    public void makeAuction() {
        AsyncTask.execute(new Runnable() {
            public void run() {
                ProgIsManager.this.setState(MEDIATION_STATE.STATE_AUCTION);
                String unused = ProgIsManager.this.mCurrentAuctionId = "";
                StringBuilder sb = new StringBuilder();
                long access$300 = ProgIsManager.this.mTimeToWaitBeforeFirstAction - (new Date().getTime() - ProgIsManager.this.mInitMangerTime);
                if (access$300 > 0) {
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        public void run() {
                            ProgIsManager.this.makeAuction();
                        }
                    }, access$300);
                    return;
                }
                ProgIsManager.this.sendMediationEvent(2000, null);
                HashMap hashMap = new HashMap();
                ArrayList arrayList = new ArrayList();
                synchronized (ProgIsManager.this.mSmashes) {
                    for (ProgIsSmash progIsSmash : ProgIsManager.this.mSmashes.values()) {
                        if (!ProgIsManager.this.mSessionCappingManager.isCapped(progIsSmash)) {
                            if (progIsSmash.isBidder() && progIsSmash.isReadyToBid()) {
                                Map<String, Object> biddingData = progIsSmash.getBiddingData();
                                if (biddingData != null) {
                                    hashMap.put(progIsSmash.getInstanceName(), biddingData);
                                    sb.append("2" + progIsSmash.getInstanceName() + ",");
                                }
                            } else if (!progIsSmash.isBidder()) {
                                arrayList.add(progIsSmash.getInstanceName());
                                sb.append("1" + progIsSmash.getInstanceName() + ",");
                            }
                        }
                    }
                }
                if (hashMap.size() == 0 && arrayList.size() == 0) {
                    ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1005}, new Object[]{"duration", 0}});
                    CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(1005, "No candidates available for auctioning"));
                    ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1005}});
                    ProgIsManager.this.setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
                    return;
                }
                if (sb.length() > 256) {
                    sb.setLength(256);
                } else if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                ProgIsManager.this.sendMediationEvent(IronSourceConstants.IS_AUCTION_REQUEST_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
                int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(2);
                if (ProgIsManager.this.mAuctionHandler != null) {
                    ProgIsManager.this.mAuctionHandler.executeAuction(ProgIsManager.this.mAppContext, hashMap, arrayList, sessionDepth);
                }
            }
        });
    }

    public void onAuctionSuccess(List<AuctionResponseItem> list, String str, int i, long j) {
        this.mCurrentAuctionId = str;
        this.mAuctionTrial = i;
        this.mAuctionFallback = "";
        sendMediationEvent(IronSourceConstants.IS_AUCTION_SUCCESS, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        updateWaterfall(list);
        loadSmashes();
    }

    public void onAuctionFailed(int i, String str, int i2, String str2, long j) {
        logInternal("Auction failed | moving to fallback waterfall");
        this.mAuctionTrial = i2;
        this.mAuctionFallback = str2;
        if (TextUtils.isEmpty(str)) {
            sendMediationEvent(IronSourceConstants.IS_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{"duration", Long.valueOf(j)}});
        } else {
            sendMediationEvent(IronSourceConstants.IS_AUCTION_FAILED, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(i)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, str}, new Object[]{"duration", Long.valueOf(j)}});
        }
        updateWaterfallToNonBidding();
        loadSmashes();
    }

    private void updateWaterfallToNonBidding() {
        updateWaterfall(extractNonBidderProvidersFromWaterfall());
    }

    private List<AuctionResponseItem> extractNonBidderProvidersFromWaterfall() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (ProgIsSmash next : this.mSmashes.values()) {
            if (!next.isBidder() && !this.mSessionCappingManager.isCapped(next)) {
                copyOnWriteArrayList.add(new AuctionResponseItem(next.getInstanceName()));
            }
        }
        return copyOnWriteArrayList;
    }

    private String getAsString(AuctionResponseItem auctionResponseItem) {
        String str = TextUtils.isEmpty(auctionResponseItem.getServerData()) ? "1" : "2";
        return str + auctionResponseItem.getInstanceName();
    }

    private void updateWaterfall(List<AuctionResponseItem> list) {
        synchronized (this.mSmashes) {
            this.mWaterfall.clear();
            this.mWaterfallServerData.clear();
            StringBuilder sb = new StringBuilder();
            for (AuctionResponseItem next : list) {
                sb.append(getAsString(next) + ",");
                ProgIsSmash progIsSmash = this.mSmashes.get(next.getInstanceName());
                if (progIsSmash != null) {
                    progIsSmash.setIsLoadCandidate(true);
                    this.mWaterfall.add(progIsSmash);
                    this.mWaterfallServerData.put(progIsSmash.getInstanceName(), next);
                }
            }
            if (sb.length() > 256) {
                sb.setLength(256);
            } else if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
            sendMediationEvent(IronSourceConstants.IS_RESULT_WATERFALL, new Object[][]{new Object[]{IronSourceConstants.EVENTS_EXT1, sb.toString()}});
        }
    }

    private void loadSmashes() {
        synchronized (this.mSmashes) {
            if (this.mWaterfall.isEmpty()) {
                setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
                sendMediationEvent(IronSourceConstants.IS_CALLBACK_LOAD_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_IS_LOAD_FAILED_NO_CANDIDATES)}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, "Empty waterfall"}});
                CallbackThrottler.getInstance().onInterstitialAdLoadFailed(new IronSourceError(IronSourceError.ERROR_IS_LOAD_FAILED_NO_CANDIDATES, "Empty waterfall"));
                return;
            }
            setState(MEDIATION_STATE.STATE_LOADING_SMASHES);
            for (int i = 0; i < Math.min(this.mMaxSmashesToLoad, this.mWaterfall.size()); i++) {
                ProgIsSmash progIsSmash = this.mWaterfall.get(i);
                String serverData = this.mWaterfallServerData.get(progIsSmash.getInstanceName()).getServerData();
                sendProviderEvent(2002, progIsSmash);
                progIsSmash.loadInterstitial(serverData);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0067, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void loadInterstitial() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r0 = r4.mState     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r1 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_SHOWING     // Catch:{ all -> 0x0068 }
            if (r0 != r1) goto L_0x0023
            java.lang.String r0 = "loadInterstitial: load cannot be invoked while showing an ad"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = com.ironsource.mediationsdk.logger.IronSourceLoggerManager.getLogger()     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x0068 }
            r3 = 3
            r1.log(r2, r0, r3)     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.logger.IronSourceError r1 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x0068 }
            r2 = 1037(0x40d, float:1.453E-42)
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.ISListenerWrapper r0 = com.ironsource.mediationsdk.ISListenerWrapper.getInstance()     // Catch:{ all -> 0x0068 }
            r0.onInterstitialAdLoadFailed(r1)     // Catch:{ all -> 0x0068 }
            monitor-exit(r4)
            return
        L_0x0023:
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r0 = r4.mState     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r1 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_READY_TO_LOAD     // Catch:{ all -> 0x0068 }
            if (r0 == r1) goto L_0x002f
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r0 = r4.mState     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r1 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_READY_TO_SHOW     // Catch:{ all -> 0x0068 }
            if (r0 != r1) goto L_0x0039
        L_0x002f:
            com.ironsource.mediationsdk.CallbackThrottler r0 = com.ironsource.mediationsdk.CallbackThrottler.getInstance()     // Catch:{ all -> 0x0068 }
            boolean r0 = r0.hasPendingInvocation()     // Catch:{ all -> 0x0068 }
            if (r0 == 0) goto L_0x0040
        L_0x0039:
            java.lang.String r0 = "loadInterstitial: load is already in progress"
            r4.logInternal(r0)     // Catch:{ all -> 0x0068 }
            monitor-exit(r4)
            return
        L_0x0040:
            java.lang.String r0 = ""
            r4.mCurrentAuctionId = r0     // Catch:{ all -> 0x0068 }
            java.lang.String r0 = ""
            r4.mCurrentPlacement = r0     // Catch:{ all -> 0x0068 }
            r0 = 2001(0x7d1, float:2.804E-42)
            r4.sendMediationEvent(r0)     // Catch:{ all -> 0x0068 }
            java.util.Date r0 = new java.util.Date     // Catch:{ all -> 0x0068 }
            r0.<init>()     // Catch:{ all -> 0x0068 }
            long r0 = r0.getTime()     // Catch:{ all -> 0x0068 }
            r4.mLoadStartTime = r0     // Catch:{ all -> 0x0068 }
            boolean r0 = r4.mIsAuctionEnabled     // Catch:{ all -> 0x0068 }
            if (r0 == 0) goto L_0x0060
            r4.makeAuction()     // Catch:{ all -> 0x0068 }
            goto L_0x0066
        L_0x0060:
            r4.updateWaterfallToNonBidding()     // Catch:{ all -> 0x0068 }
            r4.loadSmashes()     // Catch:{ all -> 0x0068 }
        L_0x0066:
            monitor-exit(r4)
            return
        L_0x0068:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgIsManager.loadInterstitial():void");
    }

    public synchronized void showInterstitial(String str) {
        if (this.mState == MEDIATION_STATE.STATE_SHOWING) {
            logAPIError("showInterstitial error: can't show ad while an ad is already showing");
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_IS_SHOW_CALLED_DURING_SHOW, "showInterstitial error: can't show ad while an ad is already showing"));
            sendMediationEvent(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_IS_SHOW_CALLED_DURING_SHOW)}});
        } else if (this.mState != MEDIATION_STATE.STATE_READY_TO_SHOW) {
            logInternal("showInterstitial() error state=" + this.mState.toString());
            logAPIError("showInterstitial error: show called while no ads are available");
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW, "showInterstitial error: show called while no ads are available"));
            sendMediationEvent(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
        } else if (str == null) {
            logAPIError("showInterstitial error: empty default placement");
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(1020, "showInterstitial error: empty default placement"));
            sendMediationEvent(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, 1020}});
        } else {
            this.mCurrentPlacement = str;
            sendMediationEventWithPlacement(2100);
            if (CappingManager.isInterstitialPlacementCapped(this.mAppContext, this.mCurrentPlacement)) {
                String str2 = "placement " + this.mCurrentPlacement + " is capped";
                logAPIError(str2);
                ISListenerWrapper.getInstance().onInterstitialAdShowFailed(new IronSourceError(IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT, str2));
                sendMediationEventWithPlacement(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_REACHED_CAP_LIMIT_PER_PLACEMENT)}});
                return;
            }
            synchronized (this.mSmashes) {
                Iterator<ProgIsSmash> it = this.mWaterfall.iterator();
                while (it.hasNext()) {
                    ProgIsSmash next = it.next();
                    if (next.isReadyToShow()) {
                        showInterstitial(next, this.mCurrentPlacement);
                        return;
                    }
                    logInternal("showInterstitial " + next.getInstanceName() + " isReadyToShow() == false");
                }
                ISListenerWrapper.getInstance().onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
                sendMediationEventWithPlacement(IronSourceConstants.IS_CALLBACK_AD_SHOW_ERROR, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf((int) IronSourceError.ERROR_CODE_NO_ADS_TO_SHOW)}});
            }
        }
    }

    private void showInterstitial(ProgIsSmash progIsSmash, String str) {
        setState(MEDIATION_STATE.STATE_SHOWING);
        progIsSmash.showInterstitial();
        sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW, progIsSmash);
        this.mSessionCappingManager.increaseShowCounter(progIsSmash);
        if (this.mSessionCappingManager.isCapped(progIsSmash)) {
            progIsSmash.setCappedPerSession();
            sendProviderEvent(IronSourceConstants.IS_CAP_SESSION, progIsSmash);
            IronSourceUtils.sendAutomationLog(progIsSmash.getInstanceName() + " was session capped");
        }
        CappingManager.incrementIsShowCounter(this.mAppContext, str);
        if (CappingManager.isInterstitialPlacementCapped(this.mAppContext, str)) {
            sendMediationEventWithPlacement(IronSourceConstants.IS_CAP_PLACEMENT);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0037, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean isInterstitialReady() {
        /*
            r4 = this;
            monitor-enter(r4)
            android.content.Context r0 = r4.mAppContext     // Catch:{ all -> 0x0038 }
            boolean r0 = com.ironsource.mediationsdk.utils.IronSourceUtils.isNetworkConnected(r0)     // Catch:{ all -> 0x0038 }
            r1 = 0
            if (r0 == 0) goto L_0x0036
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r0 = r4.mState     // Catch:{ all -> 0x0038 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r2 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_READY_TO_SHOW     // Catch:{ all -> 0x0038 }
            if (r0 == r2) goto L_0x0011
            goto L_0x0036
        L_0x0011:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.ProgIsSmash> r0 = r4.mSmashes     // Catch:{ all -> 0x0038 }
            monitor-enter(r0)     // Catch:{ all -> 0x0038 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgIsSmash> r2 = r4.mWaterfall     // Catch:{ all -> 0x0033 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0033 }
        L_0x001a:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x0033 }
            if (r3 == 0) goto L_0x0030
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x0033 }
            com.ironsource.mediationsdk.ProgIsSmash r3 = (com.ironsource.mediationsdk.ProgIsSmash) r3     // Catch:{ all -> 0x0033 }
            boolean r3 = r3.isReadyToShow()     // Catch:{ all -> 0x0033 }
            if (r3 == 0) goto L_0x001a
            r1 = 1
            monitor-exit(r0)     // Catch:{ all -> 0x0033 }
            monitor-exit(r4)
            return r1
        L_0x0030:
            monitor-exit(r0)     // Catch:{ all -> 0x0033 }
            monitor-exit(r4)
            return r1
        L_0x0033:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0033 }
            throw r1     // Catch:{ all -> 0x0038 }
        L_0x0036:
            monitor-exit(r4)
            return r1
        L_0x0038:
            r0 = move-exception
            monitor-exit(r4)
            goto L_0x003c
        L_0x003b:
            throw r0
        L_0x003c:
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgIsManager.isInterstitialReady():boolean");
    }

    public void onInterstitialAdReady(ProgIsSmash progIsSmash, long j) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdReady");
            sendProviderEvent(2003, progIsSmash, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
            if (this.mState == MEDIATION_STATE.STATE_LOADING_SMASHES) {
                setState(MEDIATION_STATE.STATE_READY_TO_SHOW);
                ISListenerWrapper.getInstance().onInterstitialAdReady();
                sendMediationEvent(2004, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - this.mLoadStartTime)}});
                AuctionResponseItem auctionResponseItem = this.mWaterfallServerData.get(progIsSmash.getInstanceName());
                this.mAuctionHandler.reportLoadSuccess(auctionResponseItem);
                this.mAuctionHandler.reportAuctionLose(this.mWaterfall, this.mWaterfallServerData, auctionResponseItem);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d3, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onInterstitialAdLoadFailed(com.ironsource.mediationsdk.logger.IronSourceError r8, com.ironsource.mediationsdk.ProgIsSmash r9, long r10) {
        /*
            r7 = this;
            monitor-enter(r7)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d7 }
            r0.<init>()     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = "onInterstitialAdLoadFailed error="
            r0.append(r1)     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = r8.getErrorMessage()     // Catch:{ all -> 0x00d7 }
            r0.append(r1)     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = " state="
            r0.append(r1)     // Catch:{ all -> 0x00d7 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r1 = r7.mState     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = r1.name()     // Catch:{ all -> 0x00d7 }
            r0.append(r1)     // Catch:{ all -> 0x00d7 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d7 }
            r7.logSmashCallback(r9, r0)     // Catch:{ all -> 0x00d7 }
            r0 = 2200(0x898, float:3.083E-42)
            r1 = 3
            java.lang.Object[][] r1 = new java.lang.Object[r1][]     // Catch:{ all -> 0x00d7 }
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]     // Catch:{ all -> 0x00d7 }
            java.lang.String r4 = "errorCode"
            r5 = 0
            r3[r5] = r4     // Catch:{ all -> 0x00d7 }
            int r4 = r8.getErrorCode()     // Catch:{ all -> 0x00d7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x00d7 }
            r6 = 1
            r3[r6] = r4     // Catch:{ all -> 0x00d7 }
            r1[r5] = r3     // Catch:{ all -> 0x00d7 }
            java.lang.Object[] r3 = new java.lang.Object[r2]     // Catch:{ all -> 0x00d7 }
            java.lang.String r4 = "reason"
            r3[r5] = r4     // Catch:{ all -> 0x00d7 }
            java.lang.String r8 = r8.getErrorMessage()     // Catch:{ all -> 0x00d7 }
            r3[r6] = r8     // Catch:{ all -> 0x00d7 }
            r1[r6] = r3     // Catch:{ all -> 0x00d7 }
            java.lang.Object[] r8 = new java.lang.Object[r2]     // Catch:{ all -> 0x00d7 }
            java.lang.String r3 = "duration"
            r8[r5] = r3     // Catch:{ all -> 0x00d7 }
            java.lang.Long r10 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x00d7 }
            r8[r6] = r10     // Catch:{ all -> 0x00d7 }
            r1[r2] = r8     // Catch:{ all -> 0x00d7 }
            r7.sendProviderEvent(r0, r9, r1)     // Catch:{ all -> 0x00d7 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.ProgIsSmash> r8 = r7.mSmashes     // Catch:{ all -> 0x00d7 }
            monitor-enter(r8)     // Catch:{ all -> 0x00d7 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgIsSmash> r9 = r7.mWaterfall     // Catch:{ all -> 0x00d4 }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x00d4 }
            r10 = 0
        L_0x006a:
            boolean r11 = r9.hasNext()     // Catch:{ all -> 0x00d4 }
            if (r11 == 0) goto L_0x009f
            java.lang.Object r11 = r9.next()     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.ProgIsSmash r11 = (com.ironsource.mediationsdk.ProgIsSmash) r11     // Catch:{ all -> 0x00d4 }
            boolean r0 = r11.getIsLoadCandidate()     // Catch:{ all -> 0x00d4 }
            if (r0 == 0) goto L_0x0097
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r9 = r7.mWaterfallServerData     // Catch:{ all -> 0x00d4 }
            java.lang.String r10 = r11.getInstanceName()     // Catch:{ all -> 0x00d4 }
            java.lang.Object r9 = r9.get(r10)     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.AuctionResponseItem r9 = (com.ironsource.mediationsdk.AuctionResponseItem) r9     // Catch:{ all -> 0x00d4 }
            java.lang.String r9 = r9.getServerData()     // Catch:{ all -> 0x00d4 }
            r10 = 2002(0x7d2, float:2.805E-42)
            r7.sendProviderEvent(r10, r11)     // Catch:{ all -> 0x00d4 }
            r11.loadInterstitial(r9)     // Catch:{ all -> 0x00d4 }
            monitor-exit(r8)     // Catch:{ all -> 0x00d4 }
            monitor-exit(r7)     // Catch:{ all -> 0x00d7 }
            return
        L_0x0097:
            boolean r11 = r11.isLoadingInProgress()     // Catch:{ all -> 0x00d4 }
            if (r11 == 0) goto L_0x006a
            r10 = 1
            goto L_0x006a
        L_0x009f:
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r9 = r7.mState     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r11 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_LOADING_SMASHES     // Catch:{ all -> 0x00d4 }
            if (r9 != r11) goto L_0x00d1
            if (r10 != 0) goto L_0x00d1
            com.ironsource.mediationsdk.CallbackThrottler r9 = com.ironsource.mediationsdk.CallbackThrottler.getInstance()     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.logger.IronSourceError r10 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x00d4 }
            java.lang.String r11 = "No ads to show"
            r0 = 509(0x1fd, float:7.13E-43)
            r10.<init>(r0, r11)     // Catch:{ all -> 0x00d4 }
            r9.onInterstitialAdLoadFailed(r10)     // Catch:{ all -> 0x00d4 }
            r9 = 2110(0x83e, float:2.957E-42)
            java.lang.Object[][] r10 = new java.lang.Object[r6][]     // Catch:{ all -> 0x00d4 }
            java.lang.Object[] r11 = new java.lang.Object[r2]     // Catch:{ all -> 0x00d4 }
            java.lang.String r1 = "errorCode"
            r11[r5] = r1     // Catch:{ all -> 0x00d4 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00d4 }
            r11[r6] = r0     // Catch:{ all -> 0x00d4 }
            r10[r5] = r11     // Catch:{ all -> 0x00d4 }
            r7.sendMediationEvent(r9, r10)     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r9 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_READY_TO_LOAD     // Catch:{ all -> 0x00d4 }
            r7.setState(r9)     // Catch:{ all -> 0x00d4 }
        L_0x00d1:
            monitor-exit(r8)     // Catch:{ all -> 0x00d4 }
            monitor-exit(r7)     // Catch:{ all -> 0x00d7 }
            return
        L_0x00d4:
            r9 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00d4 }
            throw r9     // Catch:{ all -> 0x00d7 }
        L_0x00d7:
            r8 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00d7 }
            goto L_0x00db
        L_0x00da:
            throw r8
        L_0x00db:
            goto L_0x00da
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgIsManager.onInterstitialAdLoadFailed(com.ironsource.mediationsdk.logger.IronSourceError, com.ironsource.mediationsdk.ProgIsSmash, long):void");
    }

    public void onInterstitialAdOpened(ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdOpened");
            ISListenerWrapper.getInstance().onInterstitialAdOpened();
            sendProviderEventWithPlacement(2005, progIsSmash);
            if (this.mIsAuctionEnabled && this.mWaterfallServerData.containsKey(progIsSmash.getInstanceName())) {
                this.mAuctionHandler.reportImpression(this.mWaterfallServerData.get(progIsSmash.getInstanceName()));
            }
        }
    }

    public void onInterstitialAdClosed(ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdClosed");
            sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_CLOSED, progIsSmash);
            ISListenerWrapper.getInstance().onInterstitialAdClosed();
            setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
        }
    }

    public void onInterstitialAdShowSucceeded(ProgIsSmash progIsSmash) {
        logSmashCallback(progIsSmash, "onInterstitialAdShowSucceeded");
        ISListenerWrapper.getInstance().onInterstitialAdShowSucceeded();
        sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_SUCCESS, progIsSmash);
    }

    public void onInterstitialAdShowFailed(IronSourceError ironSourceError, ProgIsSmash progIsSmash) {
        synchronized (this) {
            logSmashCallback(progIsSmash, "onInterstitialAdShowFailed error=" + ironSourceError.getErrorMessage());
            ISListenerWrapper.getInstance().onInterstitialAdShowFailed(ironSourceError);
            sendProviderEventWithPlacement(IronSourceConstants.IS_INSTANCE_SHOW_FAILED, progIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_CODE, Integer.valueOf(ironSourceError.getErrorCode())}, new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
            setState(MEDIATION_STATE.STATE_READY_TO_LOAD);
        }
    }

    public void onInterstitialAdClicked(ProgIsSmash progIsSmash) {
        logSmashCallback(progIsSmash, Constants.JSMethods.ON_INTERSTITIAL_AD_CLICKED);
        ISListenerWrapper.getInstance().onInterstitialAdClicked();
        sendProviderEventWithPlacement(2006, progIsSmash);
    }

    public void onInterstitialAdVisible(ProgIsSmash progIsSmash) {
        logSmashCallback(progIsSmash, "onInterstitialAdVisible");
    }

    public void onInterstitialInitFailed(IronSourceError ironSourceError, ProgIsSmash progIsSmash) {
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_FAILED, progIsSmash, new Object[][]{new Object[]{IronSourceConstants.EVENTS_ERROR_REASON, ironSourceError.getErrorMessage()}});
    }

    public void onInterstitialInitSuccess(ProgIsSmash progIsSmash) {
        sendProviderEvent(IronSourceConstants.IS_INSTANCE_INIT_SUCCESS, progIsSmash);
    }

    /* access modifiers changed from: private */
    public void setState(MEDIATION_STATE mediation_state) {
        this.mState = mediation_state;
        logInternal("state=" + mediation_state);
    }

    private void logSmashCallback(ProgIsSmash progIsSmash, String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "ProgIsManager " + progIsSmash.getInstanceName() + " : " + str, 0);
    }

    private void logAPIError(String str) {
        IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.API, str, 3);
    }

    private void logInternal(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "ProgIsManager " + str, 0);
    }

    private void sendMediationEvent(int i) {
        sendMediationEvent(i, null, false);
    }

    /* access modifiers changed from: private */
    public void sendMediationEvent(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, false);
    }

    private void sendMediationEventWithPlacement(int i, Object[][] objArr) {
        sendMediationEvent(i, objArr, true);
    }

    private void sendMediationEventWithPlacement(int i) {
        sendMediationEvent(i, null, true);
    }

    private void sendMediationEvent(int i, Object[][] objArr, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put(IronSourceConstants.EVENTS_PROGRAMMATIC, 1);
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            hashMap.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && !TextUtils.isEmpty(this.mCurrentPlacement)) {
            hashMap.put("placement", this.mCurrentPlacement);
        }
        if (shouldAddAuctionParams(i)) {
            InterstitialEventsManager.getInstance().setEventAuctionParams(hashMap, this.mAuctionTrial, this.mAuctionFallback);
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    hashMap.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                logInternal("sendMediationEvent " + e.getMessage());
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(hashMap)));
    }

    private void sendProviderEvent(int i, ProgIsSmash progIsSmash) {
        sendProviderEvent(i, progIsSmash, null, false);
    }

    private void sendProviderEvent(int i, ProgIsSmash progIsSmash, Object[][] objArr) {
        sendProviderEvent(i, progIsSmash, objArr, false);
    }

    private void sendProviderEventWithPlacement(int i, ProgIsSmash progIsSmash, Object[][] objArr) {
        sendProviderEvent(i, progIsSmash, objArr, true);
    }

    private void sendProviderEventWithPlacement(int i, ProgIsSmash progIsSmash) {
        sendProviderEvent(i, progIsSmash, null, true);
    }

    private void sendProviderEvent(int i, ProgIsSmash progIsSmash, Object[][] objArr, boolean z) {
        Map<String, Object> providerEventData = progIsSmash.getProviderEventData();
        if (!TextUtils.isEmpty(this.mCurrentAuctionId)) {
            providerEventData.put(IronSourceConstants.EVENTS_AUCTION_ID, this.mCurrentAuctionId);
        }
        if (z && !TextUtils.isEmpty(this.mCurrentPlacement)) {
            providerEventData.put("placement", this.mCurrentPlacement);
        }
        if (shouldAddAuctionParams(i)) {
            InterstitialEventsManager.getInstance().setEventAuctionParams(providerEventData, this.mAuctionTrial, this.mAuctionFallback);
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    providerEventData.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "IS sendProviderEvent " + Log.getStackTraceString(e), 3);
            }
        }
        InterstitialEventsManager.getInstance().log(new EventData(i, new JSONObject(providerEventData)));
    }
}
