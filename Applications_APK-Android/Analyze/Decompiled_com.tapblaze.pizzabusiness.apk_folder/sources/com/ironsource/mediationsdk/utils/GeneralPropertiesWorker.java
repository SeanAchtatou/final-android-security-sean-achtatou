package com.ironsource.mediationsdk.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import com.ironsource.sdk.constants.Constants;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class GeneralPropertiesWorker implements Runnable {
    private static final int MAX_MINUTES_OFFSET = 840;
    private static final int MINUTES_OFFSET_STEP = 15;
    private static final int MIN_MINUTES_OFFSET = -720;
    public static final String SDK_VERSION = "sdkVersion";
    private final String ADVERTISING_ID = "advertisingId";
    private final String ADVERTISING_ID_IS_LIMIT_TRACKING = Constants.RequestParameters.isLAT;
    private final String ADVERTISING_ID_TYPE = "advertisingIdType";
    private final String ANDROID_OS_VERSION = "osVersion";
    private final String APPLICATION_KEY = ServerResponseWrapper.APP_KEY_FIELD;
    private final String BATTERY_LEVEL = "battery";
    private final String BUNDLE_ID = Constants.RequestParameters.PACKAGE_NAME;
    private final String CONNECTION_TYPE = Constants.RequestParameters.CONNECTION_TYPE;
    private final String DEVICE_MODEL = Constants.RequestParameters.DEVICE_MODEL;
    private final String DEVICE_OEM = Constants.RequestParameters.DEVICE_OEM;
    private final String DEVICE_OS = "deviceOS";
    private final String EXTERNAL_FREE_MEMORY = "externalFreeMemory";
    private final String GMT_MINUTES_OFFSET = "gmtMinutesOffset";
    private final String INTERNAL_FREE_MEMORY = "internalFreeMemory";
    private final String KEY_IS_ROOT = "jb";
    private final String KEY_PLUGIN_FW_VERSION = "plugin_fw_v";
    private final String KEY_PLUGIN_TYPE = "pluginType";
    private final String KEY_PLUGIN_VERSION = "pluginVersion";
    private final String KEY_SESSION_ID = "sessionId";
    private final String LANGUAGE = "language";
    private final String LOCATION_LAT = "lat";
    private final String LOCATION_LON = "lon";
    private final String MEDIATION_TYPE = "mt";
    private final String MOBILE_CARRIER = Constants.RequestParameters.MOBILE_CARRIER;
    private final String PUBLISHER_APP_VERSION = "appVersion";
    private final String TAG = getClass().getSimpleName();
    private Context mContext;

    private String getDeviceOS() {
        return Constants.JAVASCRIPT_INTERFACE_NAME;
    }

    private GeneralPropertiesWorker() {
    }

    public GeneralPropertiesWorker(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public void run() {
        try {
            GeneralProperties.getProperties().putKeys(collectInformation());
            IronSourceUtils.saveGeneralProperties(this.mContext, GeneralProperties.getProperties().toJSON());
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, "Thread name = " + getClass().getSimpleName(), e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0182  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map<java.lang.String, java.lang.Object> collectInformation() {
        /*
            r8 = this;
            java.lang.String r0 = ""
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            com.ironsource.mediationsdk.IronSourceObject r2 = com.ironsource.mediationsdk.IronSourceObject.getInstance()
            java.lang.String r2 = r2.getSessionId()
            java.lang.String r3 = "sessionId"
            r1.put(r3, r2)
            java.lang.String r2 = r8.getBundleId()
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            if (r3 != 0) goto L_0x0034
            java.lang.String r3 = "bundleId"
            r1.put(r3, r2)
            android.content.Context r3 = r8.mContext
            java.lang.String r2 = com.ironsource.environment.ApplicationContext.getPublisherApplicationVersion(r3, r2)
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            if (r3 != 0) goto L_0x0034
            java.lang.String r3 = "appVersion"
            r1.put(r3, r2)
        L_0x0034:
            java.lang.String r2 = r8.getApplicationKey()
            java.lang.String r3 = "appKey"
            r1.put(r3, r2)
            r2 = 1
            r3 = 2
            r4 = 0
            android.content.Context r5 = r8.mContext     // Catch:{ Exception -> 0x0062 }
            java.lang.String[] r5 = com.ironsource.environment.DeviceStatus.getAdvertisingIdInfo(r5)     // Catch:{ Exception -> 0x0062 }
            if (r5 == 0) goto L_0x0062
            int r6 = r5.length     // Catch:{ Exception -> 0x0062 }
            if (r6 != r3) goto L_0x0062
            r6 = r5[r4]     // Catch:{ Exception -> 0x0062 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Exception -> 0x0062 }
            if (r6 != 0) goto L_0x0056
            r6 = r5[r4]     // Catch:{ Exception -> 0x0062 }
            goto L_0x0057
        L_0x0056:
            r6 = r0
        L_0x0057:
            r5 = r5[r2]     // Catch:{ Exception -> 0x0063 }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ Exception -> 0x0063 }
            boolean r5 = r5.booleanValue()     // Catch:{ Exception -> 0x0063 }
            goto L_0x0064
        L_0x0062:
            r6 = r0
        L_0x0063:
            r5 = 0
        L_0x0064:
            boolean r7 = android.text.TextUtils.isEmpty(r6)
            if (r7 != 0) goto L_0x006d
            java.lang.String r0 = "GAID"
            goto L_0x007b
        L_0x006d:
            android.content.Context r6 = r8.mContext
            java.lang.String r6 = com.ironsource.environment.DeviceStatus.getOrGenerateOnceUniqueIdentifier(r6)
            boolean r7 = android.text.TextUtils.isEmpty(r6)
            if (r7 != 0) goto L_0x007b
            java.lang.String r0 = "UUID"
        L_0x007b:
            boolean r7 = android.text.TextUtils.isEmpty(r6)
            if (r7 != 0) goto L_0x0094
            java.lang.String r7 = "advertisingId"
            r1.put(r7, r6)
            java.lang.String r6 = "advertisingIdType"
            r1.put(r6, r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r5)
            java.lang.String r5 = "isLimitAdTrackingEnabled"
            r1.put(r5, r0)
        L_0x0094:
            java.lang.String r0 = r8.getDeviceOS()
            java.lang.String r5 = "deviceOS"
            r1.put(r5, r0)
            java.lang.String r0 = r8.getAndroidVersion()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00b0
            java.lang.String r0 = r8.getAndroidVersion()
            java.lang.String r5 = "osVersion"
            r1.put(r5, r0)
        L_0x00b0:
            android.content.Context r0 = r8.mContext
            java.lang.String r0 = com.ironsource.mediationsdk.utils.IronSourceUtils.getConnectionType(r0)
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 != 0) goto L_0x00c1
            java.lang.String r5 = "connectionType"
            r1.put(r5, r0)
        L_0x00c1:
            java.lang.String r0 = r8.getSDKVersion()
            java.lang.String r5 = "sdkVersion"
            r1.put(r5, r0)
            java.lang.String r0 = r8.getLanguage()
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 != 0) goto L_0x00d9
            java.lang.String r5 = "language"
            r1.put(r5, r0)
        L_0x00d9:
            java.lang.String r0 = r8.getDeviceOEM()
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 != 0) goto L_0x00e8
            java.lang.String r5 = "deviceOEM"
            r1.put(r5, r0)
        L_0x00e8:
            java.lang.String r0 = r8.getDeviceModel()
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 != 0) goto L_0x00f7
            java.lang.String r5 = "deviceModel"
            r1.put(r5, r0)
        L_0x00f7:
            java.lang.String r0 = r8.getMobileCarrier()
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 != 0) goto L_0x0106
            java.lang.String r5 = "mobileCarrier"
            r1.put(r5, r0)
        L_0x0106:
            long r5 = r8.getInternalStorageFreeSize()
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            java.lang.String r5 = "internalFreeMemory"
            r1.put(r5, r0)
            long r5 = r8.getExternalStorageFreeSize()
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            java.lang.String r5 = "externalFreeMemory"
            r1.put(r5, r0)
            int r0 = r8.getBatteryLevel()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r5 = "battery"
            r1.put(r5, r0)
            android.content.Context r0 = r8.mContext
            java.lang.String r5 = "GeneralProperties.ALLOW_LOCATION_SHARED_PREFS_KEY"
            boolean r0 = com.ironsource.mediationsdk.utils.IronSourceUtils.getBooleanFromSharedPrefs(r0, r5, r4)
            if (r0 == 0) goto L_0x0156
            double[] r0 = r8.getLastKnownLocation()
            if (r0 == 0) goto L_0x0156
            int r5 = r0.length
            if (r5 != r3) goto L_0x0156
            r3 = r0[r4]
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            java.lang.String r4 = "lat"
            r1.put(r4, r3)
            r2 = r0[r2]
            java.lang.Double r0 = java.lang.Double.valueOf(r2)
            java.lang.String r2 = "lon"
            r1.put(r2, r0)
        L_0x0156:
            int r0 = r8.getGmtMinutesOffset()
            boolean r2 = r8.validateGmtMinutesOffset(r0)
            if (r2 == 0) goto L_0x0169
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r2 = "gmtMinutesOffset"
            r1.put(r2, r0)
        L_0x0169:
            java.lang.String r0 = r8.getPluginType()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0178
            java.lang.String r2 = "pluginType"
            r1.put(r2, r0)
        L_0x0178:
            java.lang.String r0 = r8.getPluginVersion()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0187
            java.lang.String r2 = "pluginVersion"
            r1.put(r2, r0)
        L_0x0187:
            java.lang.String r0 = r8.getPluginFrameworkVersion()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0196
            java.lang.String r2 = "plugin_fw_v"
            r1.put(r2, r0)
        L_0x0196:
            boolean r0 = com.ironsource.environment.DeviceStatus.isRootedDevice()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x01a9
            java.lang.String r2 = "jb"
            r1.put(r2, r0)
        L_0x01a9:
            java.lang.String r0 = r8.getMediationType()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x01b8
            java.lang.String r2 = "mt"
            r1.put(r2, r0)
        L_0x01b8:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.GeneralPropertiesWorker.collectInformation():java.util.Map");
    }

    private String getPluginType() {
        try {
            return ConfigFile.getConfigFile().getPluginType();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceLogger.IronSourceTag.NATIVE, "getPluginType()", e);
            return "";
        }
    }

    private String getPluginVersion() {
        try {
            return ConfigFile.getConfigFile().getPluginVersion();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceLogger.IronSourceTag.NATIVE, "getPluginVersion()", e);
            return "";
        }
    }

    private String getPluginFrameworkVersion() {
        try {
            return ConfigFile.getConfigFile().getPluginFrameworkVersion();
        } catch (Exception e) {
            IronSourceLoggerManager.getLogger().logException(IronSourceLogger.IronSourceTag.NATIVE, "getPluginFrameworkVersion()", e);
            return "";
        }
    }

    private String getBundleId() {
        try {
            return this.mContext.getPackageName();
        } catch (Exception unused) {
            return "";
        }
    }

    private String getApplicationKey() {
        return IronSourceObject.getInstance().getIronSourceAppKey();
    }

    private String getAndroidVersion() {
        try {
            String str = Build.VERSION.RELEASE;
            int i = Build.VERSION.SDK_INT;
            return "" + i + "(" + str + ")";
        } catch (Exception unused) {
            return "";
        }
    }

    private String getSDKVersion() {
        return IronSourceUtils.getSDKVersion();
    }

    private String getLanguage() {
        try {
            return Locale.getDefault().getLanguage();
        } catch (Exception unused) {
            return "";
        }
    }

    private String getDeviceOEM() {
        try {
            return Build.MANUFACTURER;
        } catch (Exception unused) {
            return "";
        }
    }

    private String getDeviceModel() {
        try {
            return Build.MODEL;
        } catch (Exception unused) {
            return "";
        }
    }

    private String getMobileCarrier() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.mContext.getSystemService(PlaceFields.PHONE);
            if (telephonyManager == null) {
                return "";
            }
            String networkOperatorName = telephonyManager.getNetworkOperatorName();
            if (!networkOperatorName.equals("")) {
                return networkOperatorName;
            }
            return "";
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, this.TAG + ":getMobileCarrier()", e);
            return "";
        }
    }

    private boolean isExternalStorageAbvailable() {
        try {
            return Environment.getExternalStorageState().equals("mounted");
        } catch (Exception unused) {
            return false;
        }
    }

    private long getInternalStorageFreeSize() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
        } catch (Exception unused) {
            return -1;
        }
    }

    private long getExternalStorageFreeSize() {
        if (!isExternalStorageAbvailable()) {
            return -1;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    private int getBatteryLevel() {
        try {
            Intent registerReceiver = this.mContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = 0;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra("level", -1) : 0;
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("scale", -1);
            }
            if (intExtra == -1 || i == -1) {
                return -1;
            }
            return (int) ((((float) intExtra) / ((float) i)) * 100.0f);
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, this.TAG + ":getBatteryLevel()", e);
            return -1;
        }
    }

    private double[] getLastKnownLocation() {
        double[] dArr = new double[0];
        try {
            if (!locationPermissionGranted()) {
                return dArr;
            }
            LocationManager locationManager = (LocationManager) this.mContext.getApplicationContext().getSystemService("location");
            Location location = null;
            long j = Long.MIN_VALUE;
            for (String lastKnownLocation : locationManager.getAllProviders()) {
                Location lastKnownLocation2 = locationManager.getLastKnownLocation(lastKnownLocation);
                if (lastKnownLocation2 != null && lastKnownLocation2.getTime() > j) {
                    j = lastKnownLocation2.getTime();
                    location = lastKnownLocation2;
                }
            }
            if (location == null) {
                return dArr;
            }
            return new double[]{location.getLatitude(), location.getLongitude()};
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, this.TAG + ":getLastLocation()", e);
            return new double[0];
        }
    }

    private boolean locationPermissionGranted() {
        try {
            return this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0;
        } catch (Exception unused) {
            return false;
        }
    }

    private int getGmtMinutesOffset() {
        try {
            TimeZone timeZone = TimeZone.getDefault();
            return Math.round((float) (((timeZone.getOffset(GregorianCalendar.getInstance(timeZone).getTimeInMillis()) / 1000) / 60) / 15)) * 15;
        } catch (Exception e) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            logger.logException(ironSourceTag, this.TAG + ":getGmtMinutesOffset()", e);
            return 0;
        }
    }

    private boolean validateGmtMinutesOffset(int i) {
        return i <= MAX_MINUTES_OFFSET && i >= MIN_MINUTES_OFFSET && i % 15 == 0;
    }

    private String getMediationType() {
        return IronSourceObject.getInstance().getMediationType();
    }
}
