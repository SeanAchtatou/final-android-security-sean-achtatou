package com.ironsource.mediationsdk.utils;

import com.ironsource.mediationsdk.ProgSmash;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SessionCappingManager {
    private Map<String, Integer> mMaxAdsPerSessionMap = new HashMap();
    private Map<String, Integer> mShowCountMap = new HashMap();

    public SessionCappingManager(List<ProgSmash> list) {
        for (ProgSmash next : list) {
            this.mShowCountMap.put(next.getInstanceName(), 0);
            this.mMaxAdsPerSessionMap.put(next.getInstanceName(), Integer.valueOf(next.getMaxAdsPerSession()));
        }
    }

    public void increaseShowCounter(ProgSmash progSmash) {
        synchronized (this) {
            String instanceName = progSmash.getInstanceName();
            if (this.mShowCountMap.containsKey(instanceName)) {
                this.mShowCountMap.put(instanceName, Integer.valueOf(this.mShowCountMap.get(instanceName).intValue() + 1));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isCapped(com.ironsource.mediationsdk.ProgSmash r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.String r0 = r4.getInstanceName()     // Catch:{ all -> 0x0025 }
            java.util.Map<java.lang.String, java.lang.Integer> r1 = r3.mShowCountMap     // Catch:{ all -> 0x0025 }
            boolean r1 = r1.containsKey(r0)     // Catch:{ all -> 0x0025 }
            r2 = 0
            if (r1 == 0) goto L_0x0023
            java.util.Map<java.lang.String, java.lang.Integer> r1 = r3.mShowCountMap     // Catch:{ all -> 0x0025 }
            java.lang.Object r0 = r1.get(r0)     // Catch:{ all -> 0x0025 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x0025 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x0025 }
            int r4 = r4.getMaxAdsPerSession()     // Catch:{ all -> 0x0025 }
            if (r0 < r4) goto L_0x0021
            r2 = 1
        L_0x0021:
            monitor-exit(r3)     // Catch:{ all -> 0x0025 }
            return r2
        L_0x0023:
            monitor-exit(r3)     // Catch:{ all -> 0x0025 }
            return r2
        L_0x0025:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0025 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.SessionCappingManager.isCapped(com.ironsource.mediationsdk.ProgSmash):boolean");
    }

    public boolean areAllSmashesCapped() {
        for (String next : this.mMaxAdsPerSessionMap.keySet()) {
            if (this.mShowCountMap.get(next).intValue() < this.mMaxAdsPerSessionMap.get(next).intValue()) {
                return false;
            }
        }
        return true;
    }
}
