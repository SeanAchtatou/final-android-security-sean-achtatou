package com.ironsource.mediationsdk.utils;

public class AuctionSettings {
    private String mAuctionData;
    private long mAuctionRetryInterval;
    private long mAuctionTimeout;
    private boolean mIsAuctionOnShowStart;
    private boolean mIsProgrammatic;
    private int mMaxTrials;
    private long mMinTimeToWaitBeforeFirstAuction;
    private long mTimeToWaitBeforeAuction;
    private String mUrl;

    AuctionSettings() {
        this.mAuctionData = "";
        this.mUrl = "";
        this.mIsProgrammatic = false;
        this.mMinTimeToWaitBeforeFirstAuction = 0;
        this.mAuctionRetryInterval = 0;
        this.mTimeToWaitBeforeAuction = 0;
        this.mIsAuctionOnShowStart = true;
    }

    AuctionSettings(String str, String str2, int i, long j, boolean z, long j2, long j3, long j4, boolean z2) {
        this.mAuctionData = str;
        this.mUrl = str2;
        this.mMaxTrials = i;
        this.mAuctionTimeout = j;
        this.mIsProgrammatic = z;
        this.mMinTimeToWaitBeforeFirstAuction = j2;
        this.mAuctionRetryInterval = j3;
        this.mTimeToWaitBeforeAuction = j4;
        this.mIsAuctionOnShowStart = z2;
    }

    public boolean getIsProgrammatic() {
        return this.mIsProgrammatic;
    }

    public String getAuctionData() {
        return this.mAuctionData;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public long getTimeToWaitBeforeFirstAuctionMs() {
        return this.mMinTimeToWaitBeforeFirstAuction;
    }

    public long getAuctionRetryInterval() {
        return this.mAuctionRetryInterval;
    }

    public long getTimeToWaitBeforeAuctionMs() {
        return this.mTimeToWaitBeforeAuction;
    }

    public boolean getIsAuctionOnShowStart() {
        return this.mIsAuctionOnShowStart;
    }

    public int getNumOfMaxTrials() {
        return this.mMaxTrials;
    }

    public long getTrialsInterval() {
        return this.mAuctionTimeout;
    }
}
