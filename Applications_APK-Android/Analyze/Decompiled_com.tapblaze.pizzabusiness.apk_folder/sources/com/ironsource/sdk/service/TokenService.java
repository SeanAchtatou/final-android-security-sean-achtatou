package com.ironsource.sdk.service;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.utils.IronSourceQaProperties;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class TokenService {
    private static TokenService mInstance;
    private JSONObject tokeData = new JSONObject();

    public static synchronized TokenService getInstance() {
        TokenService tokenService;
        synchronized (TokenService.class) {
            if (mInstance == null) {
                mInstance = new TokenService();
            }
            tokenService = mInstance;
        }
        return tokenService;
    }

    private TokenService() {
    }

    public void collectQaParameters() {
        if (IronSourceQaProperties.isInitialized()) {
            mInstance.collectDataFromExternalParams(IronSourceQaProperties.getInstance().getParameters());
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void put(String str, Object obj) {
        try {
            this.tokeData.put(str, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return;
    }

    public void collectApplicationUserId(String str) {
        if (str != null) {
            put(Constants.RequestParameters.APPLICATION_USER_ID, SDKUtils.encodeString(str));
        }
    }

    public void collectApplicationKey(String str) {
        if (str != null) {
            put(Constants.RequestParameters.APPLICATION_KEY, SDKUtils.encodeString(str));
        }
    }

    public void collectDataFromActivity(Activity activity) {
        if (activity != null) {
            if (Build.VERSION.SDK_INT >= 19) {
                put(SDKUtils.encodeString(Constants.RequestParameters.IMMERSIVE), Boolean.valueOf(DeviceStatus.isImmersiveSupported(activity)));
            }
            put(Constants.RequestParameters.APP_ORIENTATION, SDKUtils.translateRequestedOrientation(DeviceStatus.getActivityRequestedOrientation(activity)));
        }
    }

    public void collectAdvertisingID(final Activity activity) {
        if (activity != null) {
            try {
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            TokenService.this.updateData(DeviceData.fetchAdvertiserIdData(activity));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void collectDataFromDevice(Context context) {
        if (context != null) {
            updateData(DeviceData.fetchPermanentData(context));
            updateData(DeviceData.fetchMutableData(context));
        }
    }

    public void collectDataFromExternalParams(Map<String, String> map) {
        if (map == null) {
            Log.d("TokenService", "collectDataFromExternalParams params=null");
            return;
        }
        for (String next : map.keySet()) {
            put(next, SDKUtils.encodeString(map.get(next)));
        }
    }

    public void collectDataFromControllerConfig(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                put("chinaCDN", new JSONObject(str).opt("chinaCDN"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateData(JSONObject jSONObject) {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            put(next, jSONObject.opt(next));
        }
    }

    public String getToken(Context context) {
        fetchIndependentData();
        collectDataFromDevice(context);
        return Gibberish.encode(this.tokeData.toString());
    }

    public void fetchDependentData(Activity activity, String str, String str2) {
        collectAdvertisingID(activity);
        collectDataFromActivity(activity);
        collectDataFromDevice(activity);
        collectApplicationUserId(str2);
        collectApplicationKey(str);
    }

    public void fetchIndependentData() {
        collectDataFromControllerConfig(SDKUtils.getControllerConfig());
        collectDataFromExternalParams(SDKUtils.getInitSDKParams());
        collectQaParameters();
    }
}
