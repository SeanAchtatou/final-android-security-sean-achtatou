package com.ironsource.sdk.utils;

import android.os.AsyncTask;

public class IronSourceAsyncHttpRequestTask extends AsyncTask<String, Integer, Integer> {
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer doInBackground(java.lang.String... r5) {
        /*
            r4 = this;
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x002f }
            r2 = 0
            r5 = r5[r2]     // Catch:{ Exception -> 0x002f }
            r1.<init>(r5)     // Catch:{ Exception -> 0x002f }
            java.net.URLConnection r5 = r1.openConnection()     // Catch:{ Exception -> 0x002f }
            java.lang.Object r5 = com.google.firebase.perf.network.FirebasePerfUrlConnection.instrument(r5)     // Catch:{ Exception -> 0x002f }
            java.net.URLConnection r5 = (java.net.URLConnection) r5     // Catch:{ Exception -> 0x002f }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ Exception -> 0x002f }
            r0 = 3000(0xbb8, float:4.204E-42)
            r5.setConnectTimeout(r0)     // Catch:{ Exception -> 0x0028, all -> 0x0023 }
            r5.getInputStream()     // Catch:{ Exception -> 0x0028, all -> 0x0023 }
            if (r5 == 0) goto L_0x0038
            r5.disconnect()
            goto L_0x0038
        L_0x0023:
            r0 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x003e
        L_0x0028:
            r0 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x0030
        L_0x002d:
            r5 = move-exception
            goto L_0x003e
        L_0x002f:
            r5 = move-exception
        L_0x0030:
            r5.printStackTrace()     // Catch:{ all -> 0x002d }
            if (r0 == 0) goto L_0x0038
            r0.disconnect()
        L_0x0038:
            r5 = 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            return r5
        L_0x003e:
            if (r0 == 0) goto L_0x0043
            r0.disconnect()
        L_0x0043:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask.doInBackground(java.lang.String[]):java.lang.Integer");
    }
}
