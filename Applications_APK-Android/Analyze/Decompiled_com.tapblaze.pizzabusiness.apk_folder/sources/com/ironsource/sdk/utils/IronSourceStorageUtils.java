package com.ironsource.sdk.utils;

import android.content.Context;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.data.SSAEnums;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceStorageUtils {
    private static final String SSA_DIRECTORY_NAME = "supersonicads";

    public static String initializeCacheDirectory(Context context) {
        createRootDirectory(context);
        return refreshRootDirectory(context);
    }

    private static String refreshRootDirectory(Context context) {
        String currentSDKVersion = IronSourceSharedPrefHelper.getSupersonicPrefHelper().getCurrentSDKVersion();
        String supersonicSdkVersion = DeviceProperties.getSupersonicSdkVersion();
        if (currentSDKVersion.equalsIgnoreCase(supersonicSdkVersion)) {
            return getDiskCacheDir(context, SSA_DIRECTORY_NAME).getPath();
        }
        IronSourceSharedPrefHelper.getSupersonicPrefHelper().setCurrentSDKVersion(supersonicSdkVersion);
        File externalCacheDir = DeviceStatus.getExternalCacheDir(context);
        if (externalCacheDir != null) {
            deleteAllFiles(externalCacheDir.getAbsolutePath() + File.separator + SSA_DIRECTORY_NAME + File.separator);
        }
        deleteAllFiles(DeviceStatus.getInternalCacheDirPath(context) + File.separator + SSA_DIRECTORY_NAME + File.separator);
        return createRootDirectory(context);
    }

    private static void deleteAllFiles(String str) {
        File[] listFiles = new File(str).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isDirectory()) {
                    deleteAllFiles(file.getAbsolutePath());
                    file.delete();
                } else {
                    file.delete();
                }
            }
        }
    }

    private static File getDiskCacheDir(Context context, String str) {
        return new File(getDiskCacheDirPath(context) + File.separator + str);
    }

    public static String getDiskCacheDirPath(Context context) {
        if (!SDKUtils.isExternalStorageAvailable()) {
            return DeviceStatus.getInternalCacheDirPath(context);
        }
        File externalCacheDir = DeviceStatus.getExternalCacheDir(context);
        if (externalCacheDir == null || !externalCacheDir.canWrite()) {
            return DeviceStatus.getInternalCacheDirPath(context);
        }
        return externalCacheDir.getPath();
    }

    private static String createRootDirectory(Context context) {
        File diskCacheDir = getDiskCacheDir(context, SSA_DIRECTORY_NAME);
        if (!diskCacheDir.exists()) {
            diskCacheDir.mkdir();
        }
        return diskCacheDir.getPath();
    }

    public static String makeDir(String str, String str2) {
        File file = new File(str, str2);
        if (file.exists() || file.mkdirs()) {
            return file.getPath();
        }
        return null;
    }

    public static synchronized boolean deleteFile(String str, String str2, String str3) {
        synchronized (IronSourceStorageUtils.class) {
            File file = new File(str, str2);
            if (!file.exists()) {
                return false;
            }
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                return false;
            }
            int length = listFiles.length;
            int i = 0;
            while (i < length) {
                File file2 = listFiles[i];
                if (!file2.isFile() || !file2.getName().equalsIgnoreCase(str3)) {
                    i++;
                } else {
                    boolean delete = file2.delete();
                    return delete;
                }
            }
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003c, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean isFileCached(java.lang.String r6, com.ironsource.sdk.data.SSAFile r7) {
        /*
            java.lang.Class<com.ironsource.sdk.utils.IronSourceStorageUtils> r0 = com.ironsource.sdk.utils.IronSourceStorageUtils.class
            monitor-enter(r0)
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x003d }
            java.lang.String r2 = r7.getPath()     // Catch:{ all -> 0x003d }
            r1.<init>(r6, r2)     // Catch:{ all -> 0x003d }
            java.io.File[] r6 = r1.listFiles()     // Catch:{ all -> 0x003d }
            r2 = 0
            if (r6 == 0) goto L_0x003b
            java.io.File[] r6 = r1.listFiles()     // Catch:{ all -> 0x003d }
            int r1 = r6.length     // Catch:{ all -> 0x003d }
            r3 = 0
        L_0x0019:
            if (r3 >= r1) goto L_0x003b
            r4 = r6[r3]     // Catch:{ all -> 0x003d }
            boolean r5 = r4.isFile()     // Catch:{ all -> 0x003d }
            if (r5 == 0) goto L_0x0038
            java.lang.String r4 = r4.getName()     // Catch:{ all -> 0x003d }
            java.lang.String r5 = r7.getFile()     // Catch:{ all -> 0x003d }
            java.lang.String r5 = com.ironsource.sdk.utils.SDKUtils.getFileName(r5)     // Catch:{ all -> 0x003d }
            boolean r4 = r4.equalsIgnoreCase(r5)     // Catch:{ all -> 0x003d }
            if (r4 == 0) goto L_0x0038
            monitor-exit(r0)
            r6 = 1
            return r6
        L_0x0038:
            int r3 = r3 + 1
            goto L_0x0019
        L_0x003b:
            monitor-exit(r0)
            return r2
        L_0x003d:
            r6 = move-exception
            monitor-exit(r0)
            goto L_0x0041
        L_0x0040:
            throw r6
        L_0x0041:
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.IronSourceStorageUtils.isFileCached(java.lang.String, com.ironsource.sdk.data.SSAFile):boolean");
    }

    public static boolean isPathExist(String str, String str2) {
        return new File(str, str2).exists();
    }

    public static synchronized boolean deleteFolder(String str, String str2) {
        boolean z;
        synchronized (IronSourceStorageUtils.class) {
            File file = new File(str, str2);
            z = deleteFolderContentRecursive(file) && file.delete();
        }
        return z;
    }

    private static boolean deleteFolderContentRecursive(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return true;
        }
        boolean z = true;
        for (File file2 : listFiles) {
            if (file2.isDirectory()) {
                z &= deleteFolderContentRecursive(file2);
            }
            if (!file2.delete()) {
                z = false;
            }
        }
        return z;
    }

    public static String getCachedFilesMap(String str, String str2) {
        JSONObject buildFilesMap = buildFilesMap(str, str2);
        try {
            buildFilesMap.put("path", str2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return buildFilesMap.toString();
    }

    private static JSONObject buildFilesMap(String str, String str2) {
        File file = new File(str, str2);
        JSONObject jSONObject = new JSONObject();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                try {
                    Object looping = looping(file2);
                    if (looping instanceof JSONArray) {
                        jSONObject.put("files", looping(file2));
                    } else if (looping instanceof JSONObject) {
                        jSONObject.put(file2.getName(), looping(file2));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    new IronSourceAsyncHttpRequestTask().execute(Constants.NATIVE_EXCEPTION_BASE_URL + e.getStackTrace()[0].getMethodName());
                }
            }
        }
        return jSONObject;
    }

    private static Object looping(File file) {
        String campaignLastUpdate;
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        try {
            if (file.isFile()) {
                jSONArray.put(file.getName());
                return jSONArray;
            }
            for (File file2 : file.listFiles()) {
                if (file2.isDirectory()) {
                    jSONObject.put(file2.getName(), looping(file2));
                } else {
                    jSONArray.put(file2.getName());
                    jSONObject.put("files", jSONArray);
                }
            }
            if (file.isDirectory() && (campaignLastUpdate = IronSourceSharedPrefHelper.getSupersonicPrefHelper().getCampaignLastUpdate(file.getName())) != null) {
                jSONObject.put("lastUpdateTime", campaignLastUpdate);
            }
            String lowerCase = file.getName().toLowerCase();
            SSAEnums.ProductType productType = null;
            if (lowerCase.startsWith(SSAEnums.ProductType.RewardedVideo.toString().toLowerCase())) {
                productType = SSAEnums.ProductType.RewardedVideo;
            } else if (lowerCase.startsWith(SSAEnums.ProductType.OfferWall.toString().toLowerCase())) {
                productType = SSAEnums.ProductType.OfferWall;
            } else if (lowerCase.startsWith(SSAEnums.ProductType.Interstitial.toString().toLowerCase())) {
                productType = SSAEnums.ProductType.Interstitial;
            }
            if (productType != null) {
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.APPLICATION_USER_ID), SDKUtils.encodeString(IronSourceSharedPrefHelper.getSupersonicPrefHelper().getUniqueId(productType)));
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.APPLICATION_KEY), SDKUtils.encodeString(IronSourceSharedPrefHelper.getSupersonicPrefHelper().getApplicationKey(productType)));
            }
            return jSONObject;
        } catch (JSONException e) {
            e.printStackTrace();
            new IronSourceAsyncHttpRequestTask().execute(Constants.NATIVE_EXCEPTION_BASE_URL + e.getStackTrace()[0].getMethodName());
        }
    }

    public static boolean renameFile(String str, String str2) throws Exception {
        return new File(str).renameTo(new File(str2));
    }

    public static int saveFile(byte[] bArr, String str) throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        try {
            byte[] bArr2 = new byte[102400];
            int i = 0;
            while (true) {
                int read = byteArrayInputStream.read(bArr2);
                if (read == -1) {
                    return i;
                }
                fileOutputStream.write(bArr2, 0, read);
                i += read;
            }
        } finally {
            fileOutputStream.close();
            byteArrayInputStream.close();
        }
    }
}
