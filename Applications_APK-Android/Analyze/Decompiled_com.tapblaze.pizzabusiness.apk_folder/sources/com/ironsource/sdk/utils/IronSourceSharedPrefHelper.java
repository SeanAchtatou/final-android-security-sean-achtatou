package com.ironsource.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.data.SSABCParameters;
import com.ironsource.sdk.data.SSAEnums;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.data.SSASession;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceSharedPrefHelper {
    private static final String APPLICATION_KEY = "application_key";
    private static final String APPLICATION_KEY_IS = "application_key_is";
    private static final String APPLICATION_KEY_OW = "application_key_ow";
    private static final String APPLICATION_KEY_RV = "application_key_rv";
    private static final String BACK_BUTTON_STATE = "back_button_state";
    private static final String IS_REPORTED = "is_reported";
    private static final String REGISTER_SESSIONS = "register_sessions";
    private static final String SEARCH_KEYS = "search_keys";
    private static final String SESSIONS = "sessions";
    private static final String SSA_RV_PARAMETER_CONNECTION_RETRIES = "ssa_rv_parameter_connection_retries";
    private static final String SSA_SDK_DOWNLOAD_URL = "ssa_sdk_download_url";
    private static final String SSA_SDK_LOAD_URL = "ssa_sdk_load_url";
    private static final String SUPERSONIC_SHARED_PREF = "supersonic_shared_preferen";
    private static final String UNIQUE_ID = "unique_id";
    private static final String UNIQUE_ID_IS = "unique_id_is";
    private static final String UNIQUE_ID_OW = "unique_id_ow";
    private static final String UNIQUE_ID_RV = "unique_id_rv";
    private static final String VERSION = "version";
    private static IronSourceSharedPrefHelper mInstance;
    private SharedPreferences mSharedPreferences;

    private IronSourceSharedPrefHelper(Context context) {
        this.mSharedPreferences = context.getSharedPreferences(SUPERSONIC_SHARED_PREF, 0);
    }

    public static synchronized IronSourceSharedPrefHelper getSupersonicPrefHelper(Context context) {
        IronSourceSharedPrefHelper ironSourceSharedPrefHelper;
        synchronized (IronSourceSharedPrefHelper.class) {
            if (mInstance == null) {
                mInstance = new IronSourceSharedPrefHelper(context);
            }
            ironSourceSharedPrefHelper = mInstance;
        }
        return ironSourceSharedPrefHelper;
    }

    public static synchronized IronSourceSharedPrefHelper getSupersonicPrefHelper() {
        IronSourceSharedPrefHelper ironSourceSharedPrefHelper;
        synchronized (IronSourceSharedPrefHelper.class) {
            ironSourceSharedPrefHelper = mInstance;
        }
        return ironSourceSharedPrefHelper;
    }

    public String getConnectionRetries() {
        return this.mSharedPreferences.getString(SSA_RV_PARAMETER_CONNECTION_RETRIES, "3");
    }

    public void setSSABCParameters(SSABCParameters sSABCParameters) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putString(SSA_RV_PARAMETER_CONNECTION_RETRIES, sSABCParameters.getConnectionRetries());
        edit.commit();
    }

    public void setBackButtonState(String str) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putString(BACK_BUTTON_STATE, str);
        edit.commit();
    }

    public SSAEnums.BackButtonState getBackButtonState() {
        int parseInt = Integer.parseInt(this.mSharedPreferences.getString(BACK_BUTTON_STATE, "2"));
        if (parseInt == 0) {
            return SSAEnums.BackButtonState.None;
        }
        if (parseInt == 1) {
            return SSAEnums.BackButtonState.Device;
        }
        if (parseInt == 2) {
            return SSAEnums.BackButtonState.Controller;
        }
        return SSAEnums.BackButtonState.Controller;
    }

    public void setSearchKeys(String str) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putString(SEARCH_KEYS, str);
        edit.commit();
    }

    public List<String> getSearchKeys() {
        String string = this.mSharedPreferences.getString(SEARCH_KEYS, null);
        ArrayList arrayList = new ArrayList();
        if (string != null) {
            SSAObj sSAObj = new SSAObj(string);
            if (sSAObj.containsKey(Constants.ParametersKeys.SEARCH_KEYS)) {
                try {
                    arrayList.addAll(sSAObj.toList((JSONArray) sSAObj.get(Constants.ParametersKeys.SEARCH_KEYS)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    public JSONArray getSessions() {
        String string = this.mSharedPreferences.getString(SESSIONS, null);
        if (string == null) {
            return new JSONArray();
        }
        try {
            return new JSONArray(string);
        } catch (JSONException unused) {
            return new JSONArray();
        }
    }

    public void deleteSessions() {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putString(SESSIONS, null);
        edit.commit();
    }

    public void addSession(SSASession sSASession) {
        if (getShouldRegisterSessions()) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("sessionStartTime", sSASession.getSessionStartTime());
                jSONObject.put("sessionEndTime", sSASession.getSessionEndTime());
                jSONObject.put("sessionType", sSASession.getSessionType());
                jSONObject.put("connectivity", sSASession.getConnectivity());
            } catch (JSONException unused) {
            }
            JSONArray sessions = getSessions();
            if (sessions == null) {
                sessions = new JSONArray();
            }
            sessions.put(jSONObject);
            SharedPreferences.Editor edit = this.mSharedPreferences.edit();
            edit.putString(SESSIONS, sessions.toString());
            edit.commit();
        }
    }

    private boolean getShouldRegisterSessions() {
        return this.mSharedPreferences.getBoolean(REGISTER_SESSIONS, true);
    }

    public void setShouldRegisterSessions(boolean z) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putBoolean(REGISTER_SESSIONS, z);
        edit.commit();
    }

    public boolean setUserData(String str, String str2) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putString(str, str2);
        return edit.commit();
    }

    public String getUserData(String str) {
        String string = this.mSharedPreferences.getString(str, null);
        return string != null ? string : "{}";
    }

    /* renamed from: com.ironsource.sdk.utils.IronSourceSharedPrefHelper$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType = new int[SSAEnums.ProductType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.ironsource.sdk.data.SSAEnums$ProductType[] r0 = com.ironsource.sdk.data.SSAEnums.ProductType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.ironsource.sdk.utils.IronSourceSharedPrefHelper.AnonymousClass1.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType = r0
                int[] r0 = com.ironsource.sdk.utils.IronSourceSharedPrefHelper.AnonymousClass1.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums.ProductType.RewardedVideo     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.ironsource.sdk.utils.IronSourceSharedPrefHelper.AnonymousClass1.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums.ProductType.OfferWall     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.ironsource.sdk.utils.IronSourceSharedPrefHelper.AnonymousClass1.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums.ProductType.Interstitial     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.IronSourceSharedPrefHelper.AnonymousClass1.<clinit>():void");
        }
    }

    public String getApplicationKey(SSAEnums.ProductType productType) {
        int i = AnonymousClass1.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType[productType.ordinal()];
        String str = null;
        if (i == 1) {
            str = this.mSharedPreferences.getString(APPLICATION_KEY_RV, null);
        } else if (i == 2) {
            str = this.mSharedPreferences.getString(APPLICATION_KEY_OW, null);
        } else if (i == 3) {
            str = this.mSharedPreferences.getString(APPLICATION_KEY_IS, null);
        }
        return str == null ? this.mSharedPreferences.getString(APPLICATION_KEY, "EMPTY_APPLICATION_KEY") : str;
    }

    public void setApplicationKey(String str) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putString(APPLICATION_KEY, str);
        edit.commit();
    }

    public boolean setUniqueId(String str) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putString(UNIQUE_ID, str);
        return edit.commit();
    }

    public String getCurrentSDKVersion() {
        return this.mSharedPreferences.getString("version", "UN_VERSIONED");
    }

    public void setCurrentSDKVersion(String str) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putString("version", str);
        edit.commit();
    }

    public String getSDKDownloadUrl() {
        return this.mSharedPreferences.getString(SSA_SDK_DOWNLOAD_URL, null);
    }

    public String getCampaignLastUpdate(String str) {
        return this.mSharedPreferences.getString(str, null);
    }

    public void setCampaignLastUpdate(String str, String str2) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public String getUniqueId(String str) {
        String str2 = null;
        if (str.equalsIgnoreCase(SSAEnums.ProductType.RewardedVideo.toString())) {
            str2 = this.mSharedPreferences.getString(UNIQUE_ID_RV, null);
        } else if (str.equalsIgnoreCase(SSAEnums.ProductType.OfferWall.toString())) {
            str2 = this.mSharedPreferences.getString(UNIQUE_ID_OW, null);
        } else if (str.equalsIgnoreCase(SSAEnums.ProductType.Interstitial.toString())) {
            str2 = this.mSharedPreferences.getString(UNIQUE_ID_IS, null);
        }
        return str2 == null ? this.mSharedPreferences.getString(UNIQUE_ID, "EMPTY_UNIQUE_ID") : str2;
    }

    public String getUniqueId(SSAEnums.ProductType productType) {
        return getUniqueId(productType.toString());
    }

    public boolean setLatestCompeltionsTime(String str, String str2, String str3) {
        String string = this.mSharedPreferences.getString("ssaUserData", null);
        if (!TextUtils.isEmpty(string)) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                if (!jSONObject.isNull(str2)) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(str2);
                    if (!jSONObject2.isNull(str3)) {
                        jSONObject2.getJSONObject(str3).put("timestamp", str);
                        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
                        edit.putString("ssaUserData", jSONObject.toString());
                        return edit.commit();
                    }
                }
            } catch (JSONException e) {
                IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
                ironSourceAsyncHttpRequestTask.execute(Constants.NATIVE_EXCEPTION_BASE_URL + e.getStackTrace()[0].getMethodName());
            }
        }
        return false;
    }

    public void setReportAppStarted(boolean z) {
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.putBoolean(IS_REPORTED, z);
        edit.apply();
    }

    public boolean getReportAppStarted() {
        return this.mSharedPreferences.getBoolean(IS_REPORTED, false);
    }
}
