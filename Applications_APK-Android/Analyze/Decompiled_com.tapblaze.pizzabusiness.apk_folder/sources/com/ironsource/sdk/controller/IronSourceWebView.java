package com.ironsource.sdk.controller;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.MutableContextWrapper;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.drive.DriveFile;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.environment.UrlHandler;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.data.AdUnitsState;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSABCParameters;
import com.ironsource.sdk.data.SSAEnums;
import com.ironsource.sdk.data.SSAFile;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.listeners.OnGenericFunctionListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.OnWebViewChangeListener;
import com.ironsource.sdk.listeners.internals.DSAdProductListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import com.ironsource.sdk.precache.DownloadManager;
import com.ironsource.sdk.utils.DeviceProperties;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.IronSourceStorageUtils;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import com.unity3d.ads.metadata.InAppPurchaseMetaData;
import com.vungle.warren.AdLoader;
import com.vungle.warren.model.Advertisement;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceWebView extends WebView implements DownloadManager.OnPreCacheCompletion, DownloadListener {
    public static String APP_IDS = "appIds";
    public static int DISPLAY_WEB_VIEW_INTENT = 0;
    public static String EXTERNAL_URL = "external_url";
    public static String IS_INSTALLED = "isInstalled";
    public static String IS_STORE = "is_store";
    public static String IS_STORE_CLOSE = "is_store_close";
    /* access modifiers changed from: private */
    public static String JSON_KEY_FAIL = "fail";
    /* access modifiers changed from: private */
    public static String JSON_KEY_SUCCESS = "success";
    public static int OPEN_URL_INTENT = 1;
    public static String REQUEST_ID = "requestId";
    public static String RESULT = "result";
    public static String SECONDARY_WEB_VIEW = "secondary_web_view";
    public static String WEBVIEW_TYPE = "webview_type";
    public static int mDebugMode;
    private final String GENERIC_MESSAGE = "We're sorry, some error occurred. we will investigate it";
    /* access modifiers changed from: private */
    public String PUB_TAG = IronSourceConstants.IRONSOURCE_CONFIG_NAME;
    /* access modifiers changed from: private */
    public String TAG = IronSourceWebView.class.getSimpleName();
    /* access modifiers changed from: private */
    public DownloadManager downloadManager;
    /* access modifiers changed from: private */
    public Boolean isKitkatAndAbove = null;
    /* access modifiers changed from: private */
    public boolean isRemoveCloseEventHandler;
    /* access modifiers changed from: private */
    public String mApplicationKey;
    /* access modifiers changed from: private */
    public BannerJSAdapter mBannerJsAdapter;
    /* access modifiers changed from: private */
    public String mCacheDirectory;
    /* access modifiers changed from: private */
    public OnWebViewChangeListener mChangeListener;
    /* access modifiers changed from: private */
    public CountDownTimer mCloseEventTimer;
    private BroadcastReceiver mConnectionReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String str;
            if (IronSourceWebView.this.mControllerState == SSAEnums.ControllerState.Ready) {
                if (ConnectivityService.isConnectedWifi(context)) {
                    str = ConnectivityService.NETWORK_TYPE_WIFI;
                } else {
                    str = ConnectivityService.isConnectedMobile(context) ? ConnectivityService.NETWORK_TYPE_3G : Constants.ParametersKeys.ORIENTATION_NONE;
                }
                IronSourceWebView.this.deviceStatusChanged(str);
            }
        }
    };
    private ControllerAdapter mControllerAdapter;
    private ArrayList<String> mControllerCommandsQueue;
    private String mControllerKeyPressed = "interrupt";
    /* access modifiers changed from: private */
    public FrameLayout mControllerLayout;
    /* access modifiers changed from: private */
    public SSAEnums.ControllerState mControllerState = SSAEnums.ControllerState.None;
    Context mCurrentActivityContext;
    /* access modifiers changed from: private */
    public View mCustomView;
    /* access modifiers changed from: private */
    public WebChromeClient.CustomViewCallback mCustomViewCallback;
    /* access modifiers changed from: private */
    public FrameLayout mCustomViewContainer;
    /* access modifiers changed from: private */
    public DSBannerListener mDSBannerListener;
    /* access modifiers changed from: private */
    public DSInterstitialListener mDSInterstitialListener;
    /* access modifiers changed from: private */
    public DSRewardedVideoListener mDSRewardedVideoListener;
    /* access modifiers changed from: private */
    public DemandSourceManager mDemandSourceManager;
    /* access modifiers changed from: private */
    public boolean mGlobalControllerTimeFinish;
    /* access modifiers changed from: private */
    public CountDownTimer mGlobalControllerTimer;
    /* access modifiers changed from: private */
    public int mHiddenForceCloseHeight = 50;
    /* access modifiers changed from: private */
    public String mHiddenForceCloseLocation = Constants.ForceClosePosition.TOP_RIGHT;
    /* access modifiers changed from: private */
    public int mHiddenForceCloseWidth = 50;
    /* access modifiers changed from: private */
    public boolean mIsImmersive = false;
    /* access modifiers changed from: private */
    public CountDownTimer mLoadControllerTimer;
    /* access modifiers changed from: private */
    public MOATJSAdapter mMoatJsAdapter;
    /* access modifiers changed from: private */
    public boolean mOWCreditsMiss;
    private Map<String, String> mOWExtraParameters;
    /* access modifiers changed from: private */
    public boolean mOWmiss;
    /* access modifiers changed from: private */
    public OnGenericFunctionListener mOnGenericFunctionListener;
    /* access modifiers changed from: private */
    public OnOfferWallListener mOnOfferWallListener;
    private String mOrientationState;
    /* access modifiers changed from: private */
    public PermissionsJSAdapter mPermissionsJsAdapter;
    /* access modifiers changed from: private */
    public AdUnitsState mSavedState;
    private Object mSavedStateLocker = new Object();
    /* access modifiers changed from: private */
    public State mState;
    /* access modifiers changed from: private */
    public TokenJSAdapter mTokenJSAdapter;
    Handler mUiHandler;
    /* access modifiers changed from: private */
    public String mUserId;
    /* access modifiers changed from: private */
    public VideoEventsListener mVideoEventsListener;
    private ChromeClient mWebChromeClient;
    private WebViewMessagingMediator mWebViewMessagingMediator;

    private interface OnInitProductHandler {
        void handleInitProductFailed(String str, SSAEnums.ProductType productType, DemandSource demandSource);
    }

    public enum State {
        Display,
        Gone
    }

    /* access modifiers changed from: private */
    public WebView getWebview() {
        return this;
    }

    private Map<String, String> getExtraParamsByProduct(SSAEnums.ProductType productType) {
        if (productType == SSAEnums.ProductType.OfferWall) {
            return this.mOWExtraParameters;
        }
        return null;
    }

    public IronSourceWebView(Context context, DemandSourceManager demandSourceManager) {
        super(context.getApplicationContext());
        Logger.i(this.TAG, "C'tor");
        this.mControllerCommandsQueue = new ArrayList<>();
        this.mCacheDirectory = initializeCacheDirectory(context.getApplicationContext());
        this.mCurrentActivityContext = context;
        this.mDemandSourceManager = demandSourceManager;
        initLayout(this.mCurrentActivityContext);
        this.mSavedState = new AdUnitsState();
        this.downloadManager = getDownloadManager();
        this.downloadManager.setOnPreCacheCompletion(this);
        this.mWebChromeClient = new ChromeClient();
        setWebViewClient(new ViewClient());
        setWebChromeClient(this.mWebChromeClient);
        setWebViewSettings();
        createSecuredCommunication();
        setDownloadListener(this);
        setOnTouchListener(new SupersonicWebViewTouchListener());
        this.mUiHandler = createMainThreadHandler();
    }

    private void createSecuredCommunication() {
        SecureMessagingService secureMessagingService = new SecureMessagingService(SecureMessagingService.generateToken());
        addJavascriptInterface(createControllerMessageHandler(secureMessagingService), Constants.JAVASCRIPT_INTERFACE_NAME);
        addJavascriptInterface(createSecureMessagingInterface(secureMessagingService), Constants.JAVASCRIPT_INERFACE_NAME_GENERATE_TOKEN);
    }

    /* access modifiers changed from: package-private */
    public ControllerMessageHandler createControllerMessageHandler(SecureMessagingService secureMessagingService) {
        return new ControllerMessageHandler(new ControllerAdapter(new NativeAPI()), secureMessagingService);
    }

    /* access modifiers changed from: package-private */
    public SecureMessagingInterface createSecureMessagingInterface(SecureMessagingService secureMessagingService) {
        return new SecureMessagingInterface(secureMessagingService);
    }

    /* access modifiers changed from: package-private */
    public Handler createMainThreadHandler() {
        return new Handler(Looper.getMainLooper());
    }

    /* access modifiers changed from: package-private */
    public DownloadManager getDownloadManager() {
        return DownloadManager.getInstance(this.mCacheDirectory);
    }

    /* access modifiers changed from: package-private */
    public String initializeCacheDirectory(Context context) {
        return IronSourceStorageUtils.initializeCacheDirectory(context.getApplicationContext());
    }

    public void addMoatJSInterface(MOATJSAdapter mOATJSAdapter) {
        this.mMoatJsAdapter = mOATJSAdapter;
    }

    public void addPermissionsJSInterface(PermissionsJSAdapter permissionsJSAdapter) {
        this.mPermissionsJsAdapter = permissionsJSAdapter;
    }

    public void addBannerJSInterface(BannerJSAdapter bannerJSAdapter) {
        this.mBannerJsAdapter = bannerJSAdapter;
    }

    public void addTokenJSInterface(TokenJSAdapter tokenJSAdapter) {
        this.mTokenJSAdapter = tokenJSAdapter;
    }

    public void notifyLifeCycle(String str, String str2) {
        injectJavascript(generateJSToInject(Constants.JSMethods.ON_NATIVE_LIFE_CYCLE_EVENT, parseToJson(Constants.ParametersKeys.LIFE_CYCLE_EVENT, str2, Constants.ParametersKeys.PRODUCT_TYPE, str, null, null, null, null, null, false)));
    }

    public WebViewMessagingMediator getControllerDelegate() {
        if (this.mWebViewMessagingMediator == null) {
            this.mWebViewMessagingMediator = new WebViewMessagingMediator() {
                public void sendMessageToController(String str, JSONObject jSONObject) {
                    IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(str, jSONObject.toString()));
                }
            };
        }
        return this.mWebViewMessagingMediator;
    }

    private class SupersonicWebViewTouchListener implements View.OnTouchListener {
        private SupersonicWebViewTouchListener() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                String access$500 = IronSourceWebView.this.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("X:");
                int i = (int) x;
                sb.append(i);
                sb.append(" Y:");
                int i2 = (int) y;
                sb.append(i2);
                Logger.i(access$500, sb.toString());
                int deviceWidth = DeviceStatus.getDeviceWidth();
                int deviceHeight = DeviceStatus.getDeviceHeight();
                String access$5002 = IronSourceWebView.this.TAG;
                Logger.i(access$5002, "Width:" + deviceWidth + " Height:" + deviceHeight);
                int dpToPx = SDKUtils.dpToPx((long) IronSourceWebView.this.mHiddenForceCloseWidth);
                int dpToPx2 = SDKUtils.dpToPx((long) IronSourceWebView.this.mHiddenForceCloseHeight);
                if (Constants.ForceClosePosition.TOP_RIGHT.equalsIgnoreCase(IronSourceWebView.this.mHiddenForceCloseLocation)) {
                    i = deviceWidth - i;
                } else if (!Constants.ForceClosePosition.TOP_LEFT.equalsIgnoreCase(IronSourceWebView.this.mHiddenForceCloseLocation)) {
                    if (Constants.ForceClosePosition.BOTTOM_RIGHT.equalsIgnoreCase(IronSourceWebView.this.mHiddenForceCloseLocation)) {
                        i = deviceWidth - i;
                    } else if (!Constants.ForceClosePosition.BOTTOM_LEFT.equalsIgnoreCase(IronSourceWebView.this.mHiddenForceCloseLocation)) {
                        i = 0;
                        i2 = 0;
                    }
                    i2 = deviceHeight - i2;
                }
                if (i <= dpToPx && i2 <= dpToPx2) {
                    boolean unused = IronSourceWebView.this.isRemoveCloseEventHandler = false;
                    if (IronSourceWebView.this.mCloseEventTimer != null) {
                        IronSourceWebView.this.mCloseEventTimer.cancel();
                    }
                    CountDownTimer unused2 = IronSourceWebView.this.mCloseEventTimer = new CountDownTimer(AdLoader.RETRY_DELAY, 500) {
                        public void onTick(long j) {
                            String access$500 = IronSourceWebView.this.TAG;
                            Logger.i(access$500, "Close Event Timer Tick " + j);
                        }

                        public void onFinish() {
                            Logger.i(IronSourceWebView.this.TAG, "Close Event Timer Finish");
                            if (IronSourceWebView.this.isRemoveCloseEventHandler) {
                                boolean unused = IronSourceWebView.this.isRemoveCloseEventHandler = false;
                            } else {
                                IronSourceWebView.this.engageEnd(Constants.ParametersKeys.FORCE_CLOSE);
                            }
                        }
                    }.start();
                }
            }
            return false;
        }
    }

    private void initLayout(Context context) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        this.mControllerLayout = new FrameLayout(context);
        this.mCustomViewContainer = new FrameLayout(context);
        this.mCustomViewContainer.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.mCustomViewContainer.setVisibility(8);
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        frameLayout.addView(this);
        this.mControllerLayout.addView(this.mCustomViewContainer, layoutParams);
        this.mControllerLayout.addView(frameLayout);
    }

    private void setWebViewSettings() {
        WebSettings settings = getSettings();
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        if (Build.VERSION.SDK_INT >= 16) {
            try {
                getSettings().setAllowFileAccessFromFileURLs(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        settings.setBuiltInZoomControls(false);
        settings.setJavaScriptEnabled(true);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setGeolocationEnabled(true);
        settings.setGeolocationDatabasePath("/data/data/org.itri.html5webview/databases/");
        settings.setDomStorageEnabled(true);
        try {
            setDisplayZoomControls(settings);
            setMediaPlaybackJellyBean(settings);
        } catch (Throwable th) {
            String str = this.TAG;
            Logger.e(str, "setWebSettings - " + th.toString());
        }
    }

    private void setDisplayZoomControls(WebSettings webSettings) {
        if (Build.VERSION.SDK_INT > 11) {
            webSettings.setDisplayZoomControls(false);
        }
    }

    public WebBackForwardList saveState(Bundle bundle) {
        return super.saveState(bundle);
    }

    private void setMediaPlaybackJellyBean(WebSettings webSettings) {
        if (Build.VERSION.SDK_INT >= 17) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }
    }

    private void setWebDebuggingEnabled() {
        if (Build.VERSION.SDK_INT >= 19) {
            setWebContentsDebuggingEnabled(true);
        }
    }

    public void downloadController() {
        IronSourceStorageUtils.deleteFile(this.mCacheDirectory, "", Constants.MOBILE_CONTROLLER_HTML);
        String controllerUrl = SDKUtils.getControllerUrl();
        SSAFile sSAFile = new SSAFile(controllerUrl, "");
        this.mGlobalControllerTimer = new CountDownTimer(200000, 1000) {
            public void onTick(long j) {
                String access$500 = IronSourceWebView.this.TAG;
                Logger.i(access$500, "Global Controller Timer Tick " + j);
            }

            public void onFinish() {
                Logger.i(IronSourceWebView.this.TAG, "Global Controller Timer Finish");
                boolean unused = IronSourceWebView.this.mGlobalControllerTimeFinish = true;
            }
        }.start();
        if (!this.downloadManager.isMobileControllerThreadLive()) {
            String str = this.TAG;
            Logger.i(str, "Download Mobile Controller: " + controllerUrl);
            this.downloadManager.downloadMobileControllerFile(sSAFile);
            return;
        }
        Logger.i(this.TAG, "Download Mobile Controller: already alive");
    }

    public void setDebugMode(int i) {
        mDebugMode = i;
    }

    public int getDebugMode() {
        return mDebugMode;
    }

    /* access modifiers changed from: private */
    public boolean shouldNotifyDeveloper(String str) {
        boolean z = false;
        if (TextUtils.isEmpty(str)) {
            Logger.d(this.TAG, "Trying to trigger a listener - no product was found");
            return false;
        }
        if (!str.equalsIgnoreCase(SSAEnums.ProductType.Interstitial.toString()) ? !str.equalsIgnoreCase(SSAEnums.ProductType.RewardedVideo.toString()) ? !str.equalsIgnoreCase(SSAEnums.ProductType.Banner.toString()) ? (str.equalsIgnoreCase(SSAEnums.ProductType.OfferWall.toString()) || str.equalsIgnoreCase(SSAEnums.ProductType.OfferWallCredits.toString())) && this.mOnOfferWallListener != null : this.mDSBannerListener != null : this.mDSRewardedVideoListener != null : this.mDSInterstitialListener != null) {
            z = true;
        }
        if (!z) {
            String str2 = this.TAG;
            Logger.d(str2, "Trying to trigger a listener - no listener was found for product " + str);
        }
        return z;
    }

    public void setOrientationState(String str) {
        this.mOrientationState = str;
    }

    public String getOrientationState() {
        return this.mOrientationState;
    }

    private class ViewClient extends WebViewClient {
        private ViewClient() {
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            Logger.i("onPageStarted", str);
            super.onPageStarted(webView, str, bitmap);
        }

        public void onPageFinished(WebView webView, String str) {
            Logger.i("onPageFinished", str);
            if (str.contains("adUnit") || str.contains("index.html")) {
                IronSourceWebView.this.pageFinished();
            }
            super.onPageFinished(webView, str);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            Logger.i("onReceivedError", str2 + " " + str);
            super.onReceivedError(webView, i, str, str2);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Logger.i("shouldOverrideUrlLoading", str);
            try {
                if (IronSourceWebView.this.handleSearchKeysURLs(str)) {
                    IronSourceWebView.this.interceptedUrlToStore();
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return super.shouldOverrideUrlLoading(webView, str);
        }

        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            boolean z;
            Logger.i("shouldInterceptRequest", str);
            try {
                z = new URL(str).getFile().contains("mraid.js");
            } catch (MalformedURLException unused) {
                z = false;
            }
            if (z) {
                String str2 = Advertisement.FILE_SCHEME + IronSourceWebView.this.mCacheDirectory + File.separator + "mraid.js";
                try {
                    new FileInputStream(new File(str2));
                    return new WebResourceResponse("text/javascript", "UTF-8", getClass().getResourceAsStream(str2));
                } catch (FileNotFoundException unused2) {
                }
            }
            return super.shouldInterceptRequest(webView, str);
        }
    }

    private class ChromeClient extends WebChromeClient {
        private ChromeClient() {
        }

        public boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
            WebView webView2 = new WebView(webView.getContext());
            webView2.setWebChromeClient(this);
            webView2.setWebViewClient(new FrameBustWebViewClient());
            ((WebView.WebViewTransport) message.obj).setWebView(webView2);
            message.sendToTarget();
            Logger.i("onCreateWindow", "onCreateWindow");
            return true;
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            Logger.i("MyApplication", consoleMessage.message() + " -- From line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
            return true;
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
            Logger.i("Test", "onShowCustomView");
            IronSourceWebView.this.setVisibility(8);
            if (IronSourceWebView.this.mCustomView != null) {
                Logger.i("Test", "mCustomView != null");
                customViewCallback.onCustomViewHidden();
                return;
            }
            Logger.i("Test", "mCustomView == null");
            IronSourceWebView.this.mCustomViewContainer.addView(view);
            View unused = IronSourceWebView.this.mCustomView = view;
            WebChromeClient.CustomViewCallback unused2 = IronSourceWebView.this.mCustomViewCallback = customViewCallback;
            IronSourceWebView.this.mCustomViewContainer.setVisibility(0);
        }

        public View getVideoLoadingProgressView() {
            FrameLayout frameLayout = new FrameLayout(IronSourceWebView.this.getCurrentActivityContext());
            frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            return frameLayout;
        }

        public void onHideCustomView() {
            Logger.i("Test", "onHideCustomView");
            if (IronSourceWebView.this.mCustomView != null) {
                IronSourceWebView.this.mCustomView.setVisibility(8);
                IronSourceWebView.this.mCustomViewContainer.removeView(IronSourceWebView.this.mCustomView);
                View unused = IronSourceWebView.this.mCustomView = null;
                IronSourceWebView.this.mCustomViewContainer.setVisibility(8);
                IronSourceWebView.this.mCustomViewCallback.onCustomViewHidden();
                IronSourceWebView.this.setVisibility(0);
            }
        }
    }

    private class FrameBustWebViewClient extends WebViewClient {
        private FrameBustWebViewClient() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Context currentActivityContext = IronSourceWebView.this.getCurrentActivityContext();
            Intent intent = new Intent(currentActivityContext, OpenUrlActivity.class);
            intent.putExtra(IronSourceWebView.EXTERNAL_URL, str);
            intent.putExtra(IronSourceWebView.SECONDARY_WEB_VIEW, false);
            currentActivityContext.startActivity(intent);
            return true;
        }
    }

    public class NativeAPI {
        volatile int udiaResults = 0;

        public NativeAPI() {
        }

        @JavascriptInterface
        public void removeMessagingInterface(String str) {
            IronSourceWebView.this.runOnUiThread(new Runnable() {
                public void run() {
                    IronSourceWebView.this.removeJavascriptInterface(Constants.JAVASCRIPT_INERFACE_NAME_GENERATE_TOKEN);
                }
            });
        }

        @JavascriptInterface
        public void initController(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "initController(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            if (sSAObj.containsKey(Constants.ParametersKeys.STAGE)) {
                String string = sSAObj.getString(Constants.ParametersKeys.STAGE);
                if (Constants.ParametersKeys.READY.equalsIgnoreCase(string)) {
                    handleControllerStageReady();
                } else if (Constants.ParametersKeys.LOADED.equalsIgnoreCase(string)) {
                    handleControllerStageLoaded();
                } else if (Constants.ParametersKeys.FAILED.equalsIgnoreCase(string)) {
                    handleControllerStageFailed();
                } else {
                    Logger.i(IronSourceWebView.this.TAG, "No STAGE mentioned! Should not get here!");
                }
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (Build.VERSION.SDK_INT >= 16) {
                            try {
                                IronSourceWebView.this.getSettings().setAllowFileAccessFromFileURLs(false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: package-private */
        public void handleControllerStageLoaded() {
            SSAEnums.ControllerState unused = IronSourceWebView.this.mControllerState = SSAEnums.ControllerState.Loaded;
        }

        /* access modifiers changed from: package-private */
        public void handleControllerStageReady() {
            SSAEnums.ControllerState unused = IronSourceWebView.this.mControllerState = SSAEnums.ControllerState.Ready;
            IronSourceWebView.this.mGlobalControllerTimer.cancel();
            if (IronSourceWebView.this.mLoadControllerTimer != null) {
                IronSourceWebView.this.mLoadControllerTimer.cancel();
            }
            IronSourceWebView.this.invokePendingCommands();
            IronSourceWebView ironSourceWebView = IronSourceWebView.this;
            ironSourceWebView.restoreState(ironSourceWebView.mSavedState);
        }

        /* access modifiers changed from: package-private */
        public void handleControllerStageFailed() {
            SSAEnums.ControllerState unused = IronSourceWebView.this.mControllerState = SSAEnums.ControllerState.Failed;
            for (DemandSource next : IronSourceWebView.this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.RewardedVideo)) {
                if (next.getDemandSourceInitState() == 1) {
                    IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.RewardedVideo, next);
                }
            }
            for (DemandSource next2 : IronSourceWebView.this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.Interstitial)) {
                if (next2.getDemandSourceInitState() == 1) {
                    IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.Interstitial, next2);
                }
            }
            for (DemandSource next3 : IronSourceWebView.this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.Banner)) {
                if (next3.getDemandSourceInitState() == 1) {
                    IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.Banner, next3);
                }
            }
            if (IronSourceWebView.this.mOWmiss) {
                IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.OfferWall, null);
            }
            if (IronSourceWebView.this.mOWCreditsMiss) {
                IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.OfferWallCredits, null);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0054, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0058;
         */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x005e  */
        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        @android.webkit.JavascriptInterface
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void getDeviceStatus(java.lang.String r5) {
            /*
                r4 = this;
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r0 = r0.TAG
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "getDeviceStatus("
                r1.append(r2)
                r1.append(r5)
                java.lang.String r2 = ")"
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                com.ironsource.sdk.utils.Logger.i(r0, r1)
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r0 = r0.extractSuccessFunctionToCall(r5)
                com.ironsource.sdk.controller.IronSourceWebView r1 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r5 = r1.extractFailFunctionToCall(r5)
                r1 = 2
                java.lang.Object[] r1 = new java.lang.Object[r1]
                com.ironsource.sdk.controller.IronSourceWebView r1 = com.ironsource.sdk.controller.IronSourceWebView.this
                android.content.Context r2 = r1.getContext()
                java.lang.Object[] r1 = r1.getDeviceParams(r2)
                r2 = 0
                r2 = r1[r2]
                java.lang.String r2 = (java.lang.String) r2
                r3 = 1
                r1 = r1[r3]
                java.lang.Boolean r1 = (java.lang.Boolean) r1
                boolean r1 = r1.booleanValue()
                if (r1 == 0) goto L_0x0050
                boolean r0 = android.text.TextUtils.isEmpty(r5)
                if (r0 != 0) goto L_0x0057
                r0 = r5
                goto L_0x0058
            L_0x0050:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x0057
                goto L_0x0058
            L_0x0057:
                r0 = 0
            L_0x0058:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x006d
                com.ironsource.sdk.controller.IronSourceWebView r5 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r1 = "onGetDeviceStatusSuccess"
                java.lang.String r3 = "onGetDeviceStatusFail"
                java.lang.String r5 = r5.generateJSToInject(r0, r2, r1, r3)
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                r0.injectJavascript(r5)
            L_0x006d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.getDeviceStatus(java.lang.String):void");
        }

        @JavascriptInterface
        public void setMixedContentAlwaysAllow(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "setMixedContentAlwaysAllow(" + str + ")");
            IronSourceWebView.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (Build.VERSION.SDK_INT >= 21) {
                        IronSourceWebView.this.getSettings().setMixedContentMode(0);
                    }
                }
            });
        }

        @JavascriptInterface
        public void setAllowFileAccessFromFileURLs(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "setAllowFileAccessFromFileURLs(" + str + ")");
            final boolean z = new SSAObj(str).getBoolean(Constants.ParametersKeys.ALLOW_FILE_ACCESS);
            IronSourceWebView.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (Build.VERSION.SDK_INT >= 16) {
                        try {
                            IronSourceWebView.this.getSettings().setAllowFileAccessFromFileURLs(z);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

        @JavascriptInterface
        public void getControllerConfig(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "getControllerConfig(" + str + ")");
            String string = new SSAObj(str).getString(IronSourceWebView.JSON_KEY_SUCCESS);
            if (!TextUtils.isEmpty(string)) {
                String controllerConfig = SDKUtils.getControllerConfig();
                String testerParameters = SDKUtils.getTesterParameters();
                if (areTesterParametersValid(testerParameters)) {
                    try {
                        controllerConfig = addTesterParametersToConfig(controllerConfig, testerParameters);
                    } catch (JSONException unused) {
                        Logger.d(IronSourceWebView.this.TAG, "getControllerConfig Error while parsing Tester AB Group parameters");
                    }
                }
                IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(string, controllerConfig));
            }
        }

        /* access modifiers changed from: package-private */
        public String addTesterParametersToConfig(String str, String str2) throws JSONException {
            JSONObject jSONObject = new JSONObject(str);
            JSONObject jSONObject2 = new JSONObject(str2);
            jSONObject.putOpt("testerABGroup", jSONObject2.get("testerABGroup"));
            jSONObject.putOpt("testFriendlyName", jSONObject2.get("testFriendlyName"));
            return jSONObject.toString();
        }

        /* access modifiers changed from: package-private */
        public boolean areTesterParametersValid(String str) {
            if (TextUtils.isEmpty(str) || str.contains("-1")) {
                return false;
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.getString("testerABGroup").isEmpty() || jSONObject.getString("testFriendlyName").isEmpty()) {
                    return false;
                }
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x005f, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0063;
         */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0069  */
        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        @android.webkit.JavascriptInterface
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void getApplicationInfo(java.lang.String r5) {
            /*
                r4 = this;
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r0 = r0.TAG
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "getApplicationInfo("
                r1.append(r2)
                r1.append(r5)
                java.lang.String r2 = ")"
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                com.ironsource.sdk.utils.Logger.i(r0, r1)
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r0 = r0.extractSuccessFunctionToCall(r5)
                com.ironsource.sdk.controller.IronSourceWebView r1 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r1 = r1.extractFailFunctionToCall(r5)
                com.ironsource.sdk.data.SSAObj r2 = new com.ironsource.sdk.data.SSAObj
                r2.<init>(r5)
                java.lang.String r5 = "productType"
                java.lang.String r5 = r2.getString(r5)
                java.lang.String r2 = com.ironsource.sdk.utils.SDKUtils.fetchDemandSourceId(r2)
                r3 = 2
                java.lang.Object[] r3 = new java.lang.Object[r3]
                com.ironsource.sdk.controller.IronSourceWebView r3 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.Object[] r5 = r3.getApplicationParams(r5, r2)
                r2 = 0
                r2 = r5[r2]
                java.lang.String r2 = (java.lang.String) r2
                r3 = 1
                r5 = r5[r3]
                java.lang.Boolean r5 = (java.lang.Boolean) r5
                boolean r5 = r5.booleanValue()
                if (r5 == 0) goto L_0x005b
                boolean r5 = android.text.TextUtils.isEmpty(r1)
                if (r5 != 0) goto L_0x0062
                r0 = r1
                goto L_0x0063
            L_0x005b:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x0062
                goto L_0x0063
            L_0x0062:
                r0 = 0
            L_0x0063:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x0078
                com.ironsource.sdk.controller.IronSourceWebView r5 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r1 = "onGetApplicationInfoSuccess"
                java.lang.String r3 = "onGetApplicationInfoFail"
                java.lang.String r5 = r5.generateJSToInject(r0, r2, r1, r3)
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                r0.injectJavascript(r5)
            L_0x0078:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.getApplicationInfo(java.lang.String):void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x005e, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0062;
         */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0068  */
        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        @android.webkit.JavascriptInterface
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void checkInstalledApps(java.lang.String r5) {
            /*
                r4 = this;
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r0 = r0.TAG
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "checkInstalledApps("
                r1.append(r2)
                r1.append(r5)
                java.lang.String r2 = ")"
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                com.ironsource.sdk.utils.Logger.i(r0, r1)
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r0 = r0.extractSuccessFunctionToCall(r5)
                com.ironsource.sdk.controller.IronSourceWebView r1 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r1 = r1.extractFailFunctionToCall(r5)
                com.ironsource.sdk.data.SSAObj r2 = new com.ironsource.sdk.data.SSAObj
                r2.<init>(r5)
                java.lang.String r5 = com.ironsource.sdk.controller.IronSourceWebView.APP_IDS
                java.lang.String r5 = r2.getString(r5)
                java.lang.String r3 = com.ironsource.sdk.controller.IronSourceWebView.REQUEST_ID
                java.lang.String r2 = r2.getString(r3)
                com.ironsource.sdk.controller.IronSourceWebView r3 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.Object[] r5 = r3.getAppsStatus(r5, r2)
                r2 = 0
                r2 = r5[r2]
                java.lang.String r2 = (java.lang.String) r2
                r3 = 1
                r5 = r5[r3]
                java.lang.Boolean r5 = (java.lang.Boolean) r5
                boolean r5 = r5.booleanValue()
                if (r5 == 0) goto L_0x005a
                boolean r5 = android.text.TextUtils.isEmpty(r1)
                if (r5 != 0) goto L_0x0061
                r0 = r1
                goto L_0x0062
            L_0x005a:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x0061
                goto L_0x0062
            L_0x0061:
                r0 = 0
            L_0x0062:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x0077
                com.ironsource.sdk.controller.IronSourceWebView r5 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r1 = "onCheckInstalledAppsSuccess"
                java.lang.String r3 = "onCheckInstalledAppsFail"
                java.lang.String r5 = r5.generateJSToInject(r0, r2, r1, r3)
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                r0.injectJavascript(r5)
            L_0x0077:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.checkInstalledApps(java.lang.String):void");
        }

        @JavascriptInterface
        public void saveFile(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "saveFile(" + str + ")");
            SSAFile sSAFile = new SSAFile(str);
            if (DeviceStatus.getAvailableMemorySizeInMegaBytes(IronSourceWebView.this.mCacheDirectory) <= 0) {
                IronSourceWebView.this.responseBack(str, false, DownloadManager.NO_DISK_SPACE, null);
            } else if (!SDKUtils.isExternalStorageAvailable()) {
                IronSourceWebView.this.responseBack(str, false, DownloadManager.STORAGE_UNAVAILABLE, null);
            } else if (IronSourceStorageUtils.isFileCached(IronSourceWebView.this.mCacheDirectory, sSAFile)) {
                IronSourceWebView.this.responseBack(str, false, DownloadManager.FILE_ALREADY_EXIST, null);
            } else if (!ConnectivityService.isConnected(IronSourceWebView.this.getContext())) {
                IronSourceWebView.this.responseBack(str, false, DownloadManager.NO_NETWORK_CONNECTION, null);
            } else {
                IronSourceWebView.this.responseBack(str, true, null, null);
                String lastUpdateTime = sSAFile.getLastUpdateTime();
                if (lastUpdateTime != null) {
                    String valueOf = String.valueOf(lastUpdateTime);
                    if (!TextUtils.isEmpty(valueOf)) {
                        String path = sSAFile.getPath();
                        if (path.contains("/")) {
                            String[] split = sSAFile.getPath().split("/");
                            path = split[split.length - 1];
                        }
                        IronSourceSharedPrefHelper.getSupersonicPrefHelper().setCampaignLastUpdate(path, valueOf);
                    }
                }
                IronSourceWebView.this.downloadManager.downloadFile(sSAFile);
            }
        }

        @JavascriptInterface
        public void adUnitsReady(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "adUnitsReady(" + str + ")");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            final AdUnitsReady adUnitsReady = new AdUnitsReady(str);
            if (!adUnitsReady.isNumOfAdUnitsExist()) {
                IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.NUM_OF_AD_UNITS_DO_NOT_EXIST, null);
                return;
            }
            IronSourceWebView.this.responseBack(str, true, null, null);
            String productType = adUnitsReady.getProductType();
            if (SSAEnums.ProductType.RewardedVideo.toString().equalsIgnoreCase(productType) && IronSourceWebView.this.shouldNotifyDeveloper(productType)) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (Integer.parseInt(adUnitsReady.getNumOfAdUnits()) > 0) {
                            Log.d(IronSourceWebView.this.TAG, "onRVInitSuccess()");
                            IronSourceWebView.this.mDSRewardedVideoListener.onAdProductInitSuccess(SSAEnums.ProductType.RewardedVideo, fetchDemandSourceId, adUnitsReady);
                            return;
                        }
                        IronSourceWebView.this.mDSRewardedVideoListener.onRVNoMoreOffers(fetchDemandSourceId);
                    }
                });
            }
        }

        @JavascriptInterface
        public void iabTokenAPI(String str) {
            try {
                String access$500 = IronSourceWebView.this.TAG;
                Logger.i(access$500, "iabTokenAPI(" + str + ")");
                IronSourceWebView.this.mTokenJSAdapter.call(new SSAObj(str).toString(), new JSCallbackTask());
            } catch (Exception e) {
                e.printStackTrace();
                String access$5002 = IronSourceWebView.this.TAG;
                Logger.i(access$5002, "iabTokenAPI failed with exception " + e.getMessage());
            }
        }

        @JavascriptInterface
        public void deleteFolder(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "deleteFolder(" + str + ")");
            SSAFile sSAFile = new SSAFile(str);
            if (!IronSourceStorageUtils.isPathExist(IronSourceWebView.this.mCacheDirectory, sSAFile.getPath())) {
                IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.FOLDER_NOT_EXIST_MSG, "1");
                return;
            }
            IronSourceWebView.this.responseBack(str, IronSourceStorageUtils.deleteFolder(IronSourceWebView.this.mCacheDirectory, sSAFile.getPath()), null, null);
        }

        @JavascriptInterface
        public void deleteFile(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "deleteFile(" + str + ")");
            SSAFile sSAFile = new SSAFile(str);
            if (!IronSourceStorageUtils.isPathExist(IronSourceWebView.this.mCacheDirectory, sSAFile.getPath())) {
                IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.FILE_NOT_EXIST_MSG, "1");
                return;
            }
            IronSourceWebView.this.responseBack(str, IronSourceStorageUtils.deleteFile(IronSourceWebView.this.mCacheDirectory, sSAFile.getPath(), sSAFile.getFile()), null, null);
        }

        @JavascriptInterface
        public void displayWebView(String str) {
            Intent intent;
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "displayWebView(" + str + ")");
            IronSourceWebView.this.responseBack(str, true, null, null);
            SSAObj sSAObj = new SSAObj(str);
            boolean booleanValue = ((Boolean) sSAObj.get("display")).booleanValue();
            String string = sSAObj.getString(Constants.ParametersKeys.PRODUCT_TYPE);
            boolean z = sSAObj.getBoolean(Constants.ParametersKeys.IS_STANDALONE_VIEW);
            String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (booleanValue) {
                boolean unused = IronSourceWebView.this.mIsImmersive = sSAObj.getBoolean(Constants.ParametersKeys.IMMERSIVE);
                boolean z2 = sSAObj.getBoolean(Constants.ParametersKeys.ACTIVITY_THEME_TRANSLUCENT);
                if (IronSourceWebView.this.getState() != State.Display) {
                    IronSourceWebView.this.setState(State.Display);
                    String access$5002 = IronSourceWebView.this.TAG;
                    Logger.i(access$5002, "State: " + IronSourceWebView.this.mState);
                    Context currentActivityContext = IronSourceWebView.this.getCurrentActivityContext();
                    String orientationState = IronSourceWebView.this.getOrientationState();
                    int applicationRotation = DeviceStatus.getApplicationRotation(currentActivityContext);
                    if (z) {
                        ControllerView controllerView = new ControllerView(currentActivityContext);
                        controllerView.addView(IronSourceWebView.this.mControllerLayout);
                        controllerView.showInterstitial(IronSourceWebView.this);
                        return;
                    }
                    if (z2) {
                        intent = new Intent(currentActivityContext, InterstitialActivity.class);
                    } else {
                        intent = new Intent(currentActivityContext, ControllerActivity.class);
                    }
                    if (SSAEnums.ProductType.RewardedVideo.toString().equalsIgnoreCase(string)) {
                        if (Constants.ParametersKeys.ORIENTATION_APPLICATION.equals(orientationState)) {
                            orientationState = SDKUtils.translateRequestedOrientation(DeviceStatus.getActivityRequestedOrientation(IronSourceWebView.this.getCurrentActivityContext()));
                        }
                        intent.putExtra(Constants.ParametersKeys.PRODUCT_TYPE, SSAEnums.ProductType.RewardedVideo.toString());
                        IronSourceWebView.this.mSavedState.adOpened(SSAEnums.ProductType.RewardedVideo.ordinal());
                        IronSourceWebView.this.mSavedState.setDisplayedDemandSourceId(fetchDemandSourceId);
                        if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.RewardedVideo.toString())) {
                            IronSourceWebView.this.mDSRewardedVideoListener.onAdProductOpen(SSAEnums.ProductType.RewardedVideo, fetchDemandSourceId);
                        }
                    } else if (SSAEnums.ProductType.OfferWall.toString().equalsIgnoreCase(string)) {
                        intent.putExtra(Constants.ParametersKeys.PRODUCT_TYPE, SSAEnums.ProductType.OfferWall.toString());
                        IronSourceWebView.this.mSavedState.adOpened(SSAEnums.ProductType.OfferWall.ordinal());
                    } else if (SSAEnums.ProductType.Interstitial.toString().equalsIgnoreCase(string)) {
                        if (Constants.ParametersKeys.ORIENTATION_APPLICATION.equals(orientationState)) {
                            orientationState = SDKUtils.translateRequestedOrientation(DeviceStatus.getActivityRequestedOrientation(IronSourceWebView.this.getCurrentActivityContext()));
                        }
                        intent.putExtra(Constants.ParametersKeys.PRODUCT_TYPE, SSAEnums.ProductType.Interstitial.toString());
                    }
                    intent.setFlags(DriveFile.MODE_WRITE_ONLY);
                    intent.putExtra(Constants.ParametersKeys.IMMERSIVE, IronSourceWebView.this.mIsImmersive);
                    intent.putExtra(Constants.ParametersKeys.ORIENTATION_SET_FLAG, orientationState);
                    intent.putExtra(Constants.ParametersKeys.ROTATION_SET_FLAG, applicationRotation);
                    currentActivityContext.startActivity(intent);
                    return;
                }
                String access$5003 = IronSourceWebView.this.TAG;
                Logger.i(access$5003, "State: " + IronSourceWebView.this.mState);
                return;
            }
            IronSourceWebView.this.setState(State.Gone);
            IronSourceWebView.this.closeWebView();
        }

        @JavascriptInterface
        public void getOrientation(String str) {
            String access$2600 = IronSourceWebView.this.extractSuccessFunctionToCall(str);
            String jSONObject = SDKUtils.getOrientation(IronSourceWebView.this.getCurrentActivityContext()).toString();
            if (!TextUtils.isEmpty(access$2600)) {
                IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(access$2600, jSONObject, Constants.JSMethods.ON_GET_ORIENTATION_SUCCESS, Constants.JSMethods.ON_GET_ORIENTATION_FAIL));
            }
        }

        @JavascriptInterface
        public void setOrientation(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "setOrientation(" + str + ")");
            String string = new SSAObj(str).getString("orientation");
            IronSourceWebView.this.setOrientationState(string);
            int applicationRotation = DeviceStatus.getApplicationRotation(IronSourceWebView.this.getCurrentActivityContext());
            if (IronSourceWebView.this.mChangeListener != null) {
                IronSourceWebView.this.mChangeListener.onOrientationChanged(string, applicationRotation);
            }
        }

        @JavascriptInterface
        public void getCachedFilesMap(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "getCachedFilesMap(" + str + ")");
            String access$2600 = IronSourceWebView.this.extractSuccessFunctionToCall(str);
            if (!TextUtils.isEmpty(access$2600)) {
                SSAObj sSAObj = new SSAObj(str);
                if (!sSAObj.containsKey("path")) {
                    IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.PATH_KEY_DOES_NOT_EXIST, null);
                    return;
                }
                String str2 = (String) sSAObj.get("path");
                if (!IronSourceStorageUtils.isPathExist(IronSourceWebView.this.mCacheDirectory, str2)) {
                    IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.PATH_FILE_DOES_NOT_EXIST_ON_DISK, null);
                    return;
                }
                IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(access$2600, IronSourceStorageUtils.getCachedFilesMap(IronSourceWebView.this.mCacheDirectory, str2), Constants.JSMethods.ON_GET_CACHED_FILES_MAP_SUCCESS, Constants.JSMethods.ON_GET_CACHED_FILES_MAP_FAIL));
            }
        }

        private void callJavaScriptFunction(String str, String str2) {
            if (!TextUtils.isEmpty(str)) {
                IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(str, str2));
            }
        }

        @JavascriptInterface
        public void getDemandSourceState(String str) {
            String str2;
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "getMediationState(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString("demandSourceName");
            String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            String string2 = sSAObj.getString(Constants.ParametersKeys.PRODUCT_TYPE);
            if (string2 != null && string != null) {
                try {
                    SSAEnums.ProductType productType = SDKUtils.getProductType(string2);
                    if (productType != null) {
                        DemandSource demandSourceById = IronSourceWebView.this.mDemandSourceManager.getDemandSourceById(productType, fetchDemandSourceId);
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put(Constants.ParametersKeys.PRODUCT_TYPE, string2);
                        jSONObject.put("demandSourceName", string);
                        jSONObject.put("demandSourceId", fetchDemandSourceId);
                        if (demandSourceById == null || demandSourceById.isMediationState(-1)) {
                            str2 = IronSourceWebView.this.extractFailFunctionToCall(str);
                        } else {
                            str2 = IronSourceWebView.this.extractSuccessFunctionToCall(str);
                            jSONObject.put("state", demandSourceById.getMediationState());
                        }
                        callJavaScriptFunction(str2, jSONObject.toString());
                    }
                } catch (Exception e) {
                    IronSourceWebView.this.responseBack(str, false, e.getMessage(), null);
                    e.printStackTrace();
                }
            }
        }

        @JavascriptInterface
        public void adCredited(String str) {
            final String str2;
            final boolean z;
            final boolean z2;
            Log.d(IronSourceWebView.this.PUB_TAG, "adCredited(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString(Constants.ParametersKeys.CREDITS);
            boolean z3 = false;
            final int parseInt = string != null ? Integer.parseInt(string) : 0;
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            final String string2 = sSAObj.getString(Constants.ParametersKeys.PRODUCT_TYPE);
            if (TextUtils.isEmpty(string2)) {
                Log.d(IronSourceWebView.this.PUB_TAG, "adCredited | not product NAME !!!!");
            }
            if (SSAEnums.ProductType.Interstitial.toString().equalsIgnoreCase(string2)) {
                handleAdCreditedOnInterstitial(fetchDemandSourceId, parseInt);
                return;
            }
            String string3 = sSAObj.getString(Constants.ParametersKeys.TOTAL);
            final int parseInt2 = string3 != null ? Integer.parseInt(string3) : 0;
            sSAObj.getBoolean("externalPoll");
            if (!SSAEnums.ProductType.OfferWall.toString().equalsIgnoreCase(string2)) {
                str2 = null;
                z2 = false;
                z = false;
            } else if (sSAObj.isNull(InAppPurchaseMetaData.KEY_SIGNATURE) || sSAObj.isNull("timestamp") || sSAObj.isNull("totalCreditsFlag")) {
                IronSourceWebView.this.responseBack(str, false, "One of the keys are missing: signature/timestamp/totalCreditsFlag", null);
                return;
            } else {
                if (sSAObj.getString(InAppPurchaseMetaData.KEY_SIGNATURE).equalsIgnoreCase(SDKUtils.getMD5(string3 + IronSourceWebView.this.mApplicationKey + IronSourceWebView.this.mUserId))) {
                    z3 = true;
                } else {
                    IronSourceWebView.this.responseBack(str, false, "Controller signature is not equal to SDK signature", null);
                }
                boolean z4 = sSAObj.getBoolean("totalCreditsFlag");
                str2 = sSAObj.getString("timestamp");
                z = z4;
                z2 = z3;
            }
            if (IronSourceWebView.this.shouldNotifyDeveloper(string2)) {
                final String str3 = str;
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (string2.equalsIgnoreCase(SSAEnums.ProductType.RewardedVideo.toString())) {
                            IronSourceWebView.this.mDSRewardedVideoListener.onRVAdCredited(fetchDemandSourceId, parseInt);
                        } else if (string2.equalsIgnoreCase(SSAEnums.ProductType.OfferWall.toString()) && z2 && IronSourceWebView.this.mOnOfferWallListener.onOWAdCredited(parseInt, parseInt2, z) && !TextUtils.isEmpty(str2)) {
                            if (IronSourceSharedPrefHelper.getSupersonicPrefHelper().setLatestCompeltionsTime(str2, IronSourceWebView.this.mApplicationKey, IronSourceWebView.this.mUserId)) {
                                IronSourceWebView.this.responseBack(str3, true, null, null);
                            } else {
                                IronSourceWebView.this.responseBack(str3, false, "Time Stamp could not be stored", null);
                            }
                        }
                    }
                });
            }
        }

        private void handleAdCreditedOnInterstitial(final String str, final int i) {
            DemandSource demandSourceById;
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Interstitial.toString()) && (demandSourceById = IronSourceWebView.this.mDemandSourceManager.getDemandSourceById(SSAEnums.ProductType.Interstitial, str)) != null && demandSourceById.isRewarded()) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        IronSourceWebView.this.mDSInterstitialListener.onInterstitialAdRewarded(str, i);
                    }
                });
            }
        }

        @JavascriptInterface
        public void removeCloseEventHandler(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "removeCloseEventHandler(" + str + ")");
            if (IronSourceWebView.this.mCloseEventTimer != null) {
                IronSourceWebView.this.mCloseEventTimer.cancel();
            }
            boolean unused = IronSourceWebView.this.isRemoveCloseEventHandler = true;
        }

        @JavascriptInterface
        public void onGetDeviceStatusSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGetDeviceStatusSuccess(" + str + ")");
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_GET_DEVICE_STATUS_SUCCESS, str);
        }

        @JavascriptInterface
        public void onGetDeviceStatusFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGetDeviceStatusFail(" + str + ")");
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_GET_DEVICE_STATUS_FAIL, str);
        }

        @JavascriptInterface
        public void onInitRewardedVideoSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onInitRewardedVideoSuccess(" + str + ")");
            IronSourceSharedPrefHelper.getSupersonicPrefHelper().setSSABCParameters(new SSABCParameters(str));
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_INIT_REWARDED_VIDEO_SUCCESS, str);
        }

        @JavascriptInterface
        public void onInitRewardedVideoFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onInitRewardedVideoFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            DemandSource demandSourceById = IronSourceWebView.this.mDemandSourceManager.getDemandSourceById(SSAEnums.ProductType.RewardedVideo, fetchDemandSourceId);
            if (demandSourceById != null) {
                demandSourceById.setDemandSourceInitState(3);
            }
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.RewardedVideo.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String access$500 = IronSourceWebView.this.TAG;
                        Log.d(access$500, "onRVInitFail(message:" + str + ")");
                        IronSourceWebView.this.mDSRewardedVideoListener.onAdProductInitFailed(SSAEnums.ProductType.RewardedVideo, fetchDemandSourceId, str);
                    }
                });
            }
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_INIT_REWARDED_VIDEO_FAIL, str);
        }

        @JavascriptInterface
        public void onGetApplicationInfoSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGetApplicationInfoSuccess(" + str + ")");
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_GET_APPLICATION_INFO_SUCCESS, str);
        }

        @JavascriptInterface
        public void onGetApplicationInfoFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGetApplicationInfoFail(" + str + ")");
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_GET_APPLICATION_INFO_FAIL, str);
        }

        @JavascriptInterface
        public void onShowRewardedVideoSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onShowRewardedVideoSuccess(" + str + ")");
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_SHOW_REWARDED_VIDEO_SUCCESS, str);
        }

        @JavascriptInterface
        public void onShowRewardedVideoFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onShowRewardedVideoFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.RewardedVideo.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String access$500 = IronSourceWebView.this.TAG;
                        Log.d(access$500, "onRVShowFail(message:" + string + ")");
                        IronSourceWebView.this.mDSRewardedVideoListener.onRVShowFail(fetchDemandSourceId, str);
                    }
                });
            }
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_SHOW_REWARDED_VIDEO_FAIL, str);
        }

        @JavascriptInterface
        public void onGetCachedFilesMapSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGetCachedFilesMapSuccess(" + str + ")");
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_GET_CACHED_FILES_MAP_SUCCESS, str);
        }

        @JavascriptInterface
        public void onGetCachedFilesMapFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGetCachedFilesMapFail(" + str + ")");
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_GET_CACHED_FILES_MAP_FAIL, str);
        }

        @JavascriptInterface
        public void onShowOfferWallSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onShowOfferWallSuccess(" + str + ")");
            IronSourceWebView.this.mSavedState.adOpened(SSAEnums.ProductType.OfferWall.ordinal());
            final String valueFromJsonObject = SDKUtils.getValueFromJsonObject(str, "placementId");
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.OfferWall.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        IronSourceWebView.this.mOnOfferWallListener.onOWShowSuccess(valueFromJsonObject);
                    }
                });
            }
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_SHOW_OFFER_WALL_SUCCESS, str);
        }

        @JavascriptInterface
        public void onShowOfferWallFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onShowOfferWallFail(" + str + ")");
            final String string = new SSAObj(str).getString("errMsg");
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.OfferWall.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        IronSourceWebView.this.mOnOfferWallListener.onOWShowFail(str);
                    }
                });
            }
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_SHOW_OFFER_WALL_FAIL, str);
        }

        @JavascriptInterface
        public void onInitInterstitialSuccess(String str) {
            Logger.i(IronSourceWebView.this.TAG, "onInitInterstitialSuccess()");
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_INIT_INTERSTITIAL_SUCCESS, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(IronSourceWebView.this.TAG, "onInitInterstitialSuccess failed with no demand source");
            } else if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Interstitial.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d(IronSourceWebView.this.TAG, "onInterstitialInitSuccess()");
                        IronSourceWebView.this.mDSInterstitialListener.onAdProductInitSuccess(SSAEnums.ProductType.Interstitial, fetchDemandSourceId, null);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onInitInterstitialFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onInitInterstitialFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(IronSourceWebView.this.TAG, "onInitInterstitialSuccess failed with no demand source");
                return;
            }
            DemandSource demandSourceById = IronSourceWebView.this.mDemandSourceManager.getDemandSourceById(SSAEnums.ProductType.Interstitial, fetchDemandSourceId);
            if (demandSourceById != null) {
                demandSourceById.setDemandSourceInitState(3);
            }
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Interstitial.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String access$500 = IronSourceWebView.this.TAG;
                        Log.d(access$500, "onInterstitialInitFail(message:" + str + ")");
                        IronSourceWebView.this.mDSInterstitialListener.onAdProductInitFailed(SSAEnums.ProductType.Interstitial, fetchDemandSourceId, str);
                    }
                });
            }
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_INIT_INTERSTITIAL_FAIL, str);
        }

        @JavascriptInterface
        public void adClicked(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "adClicked(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString(Constants.ParametersKeys.PRODUCT_TYPE);
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (!TextUtils.isEmpty(fetchDemandSourceId)) {
                final SSAEnums.ProductType access$4900 = IronSourceWebView.this.getStringProductTypeAsEnum(string);
                final DSAdProductListener access$5000 = IronSourceWebView.this.getAdProductListenerByProductType(access$4900);
                if (access$4900 != null && access$5000 != null) {
                    IronSourceWebView.this.runOnUiThread(new Runnable() {
                        public void run() {
                            access$5000.onAdProductClick(access$4900, fetchDemandSourceId);
                        }
                    });
                }
            }
        }

        @JavascriptInterface
        public void onShowInterstitialSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onShowInterstitialSuccess(" + str + ")");
            IronSourceWebView.this.responseBack(str, true, null, null);
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(IronSourceWebView.this.TAG, "onShowInterstitialSuccess called with no demand");
                return;
            }
            IronSourceWebView.this.mSavedState.adOpened(SSAEnums.ProductType.Interstitial.ordinal());
            IronSourceWebView.this.mSavedState.setDisplayedDemandSourceId(fetchDemandSourceId);
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Interstitial.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        IronSourceWebView.this.mDSInterstitialListener.onAdProductOpen(SSAEnums.ProductType.Interstitial, fetchDemandSourceId);
                        IronSourceWebView.this.mDSInterstitialListener.onInterstitialShowSuccess(fetchDemandSourceId);
                    }
                });
                IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_SHOW_INTERSTITIAL_SUCCESS, str);
            }
            setInterstitialAvailability(fetchDemandSourceId, false);
        }

        private void setInterstitialAvailability(String str, boolean z) {
            DemandSource demandSourceById = IronSourceWebView.this.mDemandSourceManager.getDemandSourceById(SSAEnums.ProductType.Interstitial, str);
            if (demandSourceById != null) {
                demandSourceById.setAvailabilityState(z);
            }
        }

        @JavascriptInterface
        public void onInitOfferWallSuccess(String str) {
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_INIT_OFFERWALL_SUCCESS, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
            IronSourceWebView.this.mSavedState.setOfferwallInitSuccess(true);
            if (IronSourceWebView.this.mSavedState.reportInitOfferwall()) {
                IronSourceWebView.this.mSavedState.setOfferwallReportInit(false);
                if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.OfferWall.toString())) {
                    IronSourceWebView.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Log.d(IronSourceWebView.this.TAG, "onOfferWallInitSuccess()");
                            IronSourceWebView.this.mOnOfferWallListener.onOfferwallInitSuccess();
                        }
                    });
                }
            }
        }

        @JavascriptInterface
        public void onInitOfferWallFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onInitOfferWallFail(" + str + ")");
            IronSourceWebView.this.mSavedState.setOfferwallInitSuccess(false);
            final String string = new SSAObj(str).getString("errMsg");
            if (IronSourceWebView.this.mSavedState.reportInitOfferwall()) {
                IronSourceWebView.this.mSavedState.setOfferwallReportInit(false);
                if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.OfferWall.toString())) {
                    IronSourceWebView.this.runOnUiThread(new Runnable() {
                        public void run() {
                            String str = string;
                            if (str == null) {
                                str = "We're sorry, some error occurred. we will investigate it";
                            }
                            String access$500 = IronSourceWebView.this.TAG;
                            Log.d(access$500, "onOfferWallInitFail(message:" + str + ")");
                            IronSourceWebView.this.mOnOfferWallListener.onOfferwallInitFail(str);
                        }
                    });
                }
            }
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_INIT_OFFERWALL_FAIL, str);
        }

        @JavascriptInterface
        public void onLoadInterstitialSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onLoadInterstitialSuccess(" + str + ")");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            setInterstitialAvailability(fetchDemandSourceId, true);
            IronSourceWebView.this.responseBack(str, true, null, null);
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Interstitial.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        IronSourceWebView.this.mDSInterstitialListener.onInterstitialLoadSuccess(fetchDemandSourceId);
                    }
                });
            }
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_LOAD_INTERSTITIAL_SUCCESS, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
        }

        @JavascriptInterface
        public void onLoadInterstitialFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onLoadInterstitialFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            IronSourceWebView.this.responseBack(str, true, null, null);
            if (!TextUtils.isEmpty(fetchDemandSourceId)) {
                setInterstitialAvailability(fetchDemandSourceId, false);
                if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Interstitial.toString())) {
                    IronSourceWebView.this.runOnUiThread(new Runnable() {
                        public void run() {
                            String str = string;
                            if (str == null) {
                                str = "We're sorry, some error occurred. we will investigate it";
                            }
                            IronSourceWebView.this.mDSInterstitialListener.onInterstitialLoadFailed(fetchDemandSourceId, str);
                        }
                    });
                }
                IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_LOAD_INTERSTITIAL_FAIL, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
            }
        }

        @JavascriptInterface
        public void onShowInterstitialFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onShowInterstitialFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            IronSourceWebView.this.responseBack(str, true, null, null);
            if (!TextUtils.isEmpty(fetchDemandSourceId)) {
                setInterstitialAvailability(fetchDemandSourceId, false);
                if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Interstitial.toString())) {
                    IronSourceWebView.this.runOnUiThread(new Runnable() {
                        public void run() {
                            String str = string;
                            if (str == null) {
                                str = "We're sorry, some error occurred. we will investigate it";
                            }
                            IronSourceWebView.this.mDSInterstitialListener.onInterstitialShowFailed(fetchDemandSourceId, str);
                        }
                    });
                }
                IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_SHOW_INTERSTITIAL_FAIL, str);
            }
        }

        @JavascriptInterface
        public void onInitBannerSuccess(String str) {
            Logger.i(IronSourceWebView.this.TAG, "onInitBannerSuccess()");
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_INIT_BANNER_SUCCESS, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(IronSourceWebView.this.TAG, "onInitBannerSuccess failed with no demand source");
            } else if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Banner.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d(IronSourceWebView.this.TAG, "onBannerInitSuccess()");
                        IronSourceWebView.this.mDSBannerListener.onAdProductInitSuccess(SSAEnums.ProductType.Banner, fetchDemandSourceId, null);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onInitBannerFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onInitBannerFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            if (TextUtils.isEmpty(fetchDemandSourceId)) {
                Logger.i(IronSourceWebView.this.TAG, "onInitBannerFail failed with no demand source");
                return;
            }
            DemandSource demandSourceById = IronSourceWebView.this.mDemandSourceManager.getDemandSourceById(SSAEnums.ProductType.Banner, fetchDemandSourceId);
            if (demandSourceById != null) {
                demandSourceById.setDemandSourceInitState(3);
            }
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Banner.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String access$500 = IronSourceWebView.this.TAG;
                        Log.d(access$500, "onBannerInitFail(message:" + str + ")");
                        IronSourceWebView.this.mDSBannerListener.onAdProductInitFailed(SSAEnums.ProductType.Banner, fetchDemandSourceId, str);
                    }
                });
            }
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_INIT_BANNER_FAIL, str);
        }

        @JavascriptInterface
        public void onLoadBannerSuccess(String str) {
            Logger.i(IronSourceWebView.this.TAG, "onLoadBannerSuccess()");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(new SSAObj(str));
            IronSourceWebView.this.responseBack(str, true, null, null);
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Banner.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d(IronSourceWebView.this.TAG, "onBannerLoadSuccess()");
                        IronSourceWebView.this.mDSBannerListener.onBannerLoadSuccess(fetchDemandSourceId);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onLoadBannerFail(String str) {
            Logger.i(IronSourceWebView.this.TAG, "onLoadBannerFail()");
            SSAObj sSAObj = new SSAObj(str);
            final String string = sSAObj.getString("errMsg");
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            IronSourceWebView.this.responseBack(str, true, null, null);
            if (!TextUtils.isEmpty(fetchDemandSourceId) && IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.Banner.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d(IronSourceWebView.this.TAG, "onLoadBannerFail()");
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        IronSourceWebView.this.mDSBannerListener.onBannerLoadFail(fetchDemandSourceId, str);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onGenericFunctionSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGenericFunctionSuccess(" + str + ")");
            if (IronSourceWebView.this.mOnGenericFunctionListener == null) {
                Logger.d(IronSourceWebView.this.TAG, "genericFunctionListener was not found");
                return;
            }
            IronSourceWebView.this.runOnUiThread(new Runnable() {
                public void run() {
                    IronSourceWebView.this.mOnGenericFunctionListener.onGFSuccess();
                }
            });
            IronSourceWebView.this.responseBack(str, true, null, null);
        }

        @JavascriptInterface
        public void onGenericFunctionFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGenericFunctionFail(" + str + ")");
            if (IronSourceWebView.this.mOnGenericFunctionListener == null) {
                Logger.d(IronSourceWebView.this.TAG, "genericFunctionListener was not found");
                return;
            }
            final String string = new SSAObj(str).getString("errMsg");
            IronSourceWebView.this.runOnUiThread(new Runnable() {
                public void run() {
                    IronSourceWebView.this.mOnGenericFunctionListener.onGFFail(string);
                }
            });
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_GENERIC_FUNCTION_FAIL, str);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        @JavascriptInterface
        public void openUrl(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "openUrl(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString("url");
            String string2 = sSAObj.getString("method");
            Context currentActivityContext = IronSourceWebView.this.getCurrentActivityContext();
            try {
                if (string2.equalsIgnoreCase(Constants.ParametersKeys.EXTERNAL_BROWSER)) {
                    UrlHandler.openUrl(currentActivityContext, string);
                } else if (string2.equalsIgnoreCase(Constants.ParametersKeys.WEB_VIEW)) {
                    Intent intent = new Intent(currentActivityContext, OpenUrlActivity.class);
                    intent.putExtra(IronSourceWebView.EXTERNAL_URL, string);
                    intent.putExtra(IronSourceWebView.SECONDARY_WEB_VIEW, true);
                    intent.putExtra(Constants.ParametersKeys.IMMERSIVE, IronSourceWebView.this.mIsImmersive);
                    currentActivityContext.startActivity(intent);
                } else if (string2.equalsIgnoreCase("store")) {
                    Intent intent2 = new Intent(currentActivityContext, OpenUrlActivity.class);
                    intent2.putExtra(IronSourceWebView.EXTERNAL_URL, string);
                    intent2.putExtra(IronSourceWebView.IS_STORE, true);
                    intent2.putExtra(IronSourceWebView.SECONDARY_WEB_VIEW, true);
                    currentActivityContext.startActivity(intent2);
                }
            } catch (Exception e) {
                IronSourceWebView.this.responseBack(str, false, e.getMessage(), null);
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void setForceClose(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "setForceClose(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString("width");
            String string2 = sSAObj.getString("height");
            int unused = IronSourceWebView.this.mHiddenForceCloseWidth = Integer.parseInt(string);
            int unused2 = IronSourceWebView.this.mHiddenForceCloseHeight = Integer.parseInt(string2);
            String unused3 = IronSourceWebView.this.mHiddenForceCloseLocation = sSAObj.getString(Constants.ParametersKeys.POSITION);
        }

        @JavascriptInterface
        public void setBackButtonState(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "setBackButtonState(" + str + ")");
            IronSourceSharedPrefHelper.getSupersonicPrefHelper().setBackButtonState(new SSAObj(str).getString("state"));
        }

        @JavascriptInterface
        public void setStoreSearchKeys(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "setStoreSearchKeys(" + str + ")");
            IronSourceSharedPrefHelper.getSupersonicPrefHelper().setSearchKeys(str);
        }

        @JavascriptInterface
        public void setWebviewBackgroundColor(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "setWebviewBackgroundColor(" + str + ")");
            IronSourceWebView.this.setWebviewBackground(str);
        }

        @JavascriptInterface
        public void toggleUDIA(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "toggleUDIA(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(Constants.ParametersKeys.TOGGLE)) {
                IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.TOGGLE_KEY_DOES_NOT_EXIST, null);
                return;
            }
            int parseInt = Integer.parseInt(sSAObj.getString(Constants.ParametersKeys.TOGGLE));
            if (parseInt != 0) {
                String binaryString = Integer.toBinaryString(parseInt);
                if (TextUtils.isEmpty(binaryString)) {
                    IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.FIALED_TO_CONVERT_TOGGLE, null);
                } else if (binaryString.toCharArray()[3] == '0') {
                    IronSourceSharedPrefHelper.getSupersonicPrefHelper().setShouldRegisterSessions(true);
                } else {
                    IronSourceSharedPrefHelper.getSupersonicPrefHelper().setShouldRegisterSessions(false);
                }
            }
        }

        @JavascriptInterface
        public void getUDIA(String str) {
            this.udiaResults = 0;
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "getUDIA(" + str + ")");
            String unused = IronSourceWebView.this.extractSuccessFunctionToCall(str);
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(Constants.ParametersKeys.GET_BY_FLAG)) {
                IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.GET_BY_FLAG_KEY_DOES_NOT_EXIST, null);
                return;
            }
            int parseInt = Integer.parseInt(sSAObj.getString(Constants.ParametersKeys.GET_BY_FLAG));
            if (parseInt != 0) {
                String binaryString = Integer.toBinaryString(parseInt);
                if (TextUtils.isEmpty(binaryString)) {
                    IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.FIALED_TO_CONVERT_GET_BY_FLAG, null);
                    return;
                }
                char[] charArray = new StringBuilder(binaryString).reverse().toString().toCharArray();
                JSONArray jSONArray = new JSONArray();
                if (charArray[3] == '0') {
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put("sessions", IronSourceSharedPrefHelper.getSupersonicPrefHelper().getSessions());
                        IronSourceSharedPrefHelper.getSupersonicPrefHelper().deleteSessions();
                        jSONArray.put(jSONObject);
                    } catch (JSONException unused2) {
                    }
                }
            }
        }

        @JavascriptInterface
        public void setUserUniqueId(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "setUserUniqueId(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(Constants.ParametersKeys.USER_UNIQUE_ID) || !sSAObj.containsKey(Constants.ParametersKeys.PRODUCT_TYPE)) {
                IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.UNIQUE_ID_OR_PRODUCT_TYPE_DOES_NOT_EXIST, null);
                return;
            }
            if (IronSourceSharedPrefHelper.getSupersonicPrefHelper().setUniqueId(sSAObj.getString(Constants.ParametersKeys.USER_UNIQUE_ID))) {
                IronSourceWebView.this.responseBack(str, true, null, null);
            } else {
                IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.SET_USER_UNIQUE_ID_FAILED, null);
            }
        }

        @JavascriptInterface
        public void getUserUniqueId(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "getUserUniqueId(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(Constants.ParametersKeys.PRODUCT_TYPE)) {
                IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.PRODUCT_TYPE_DOES_NOT_EXIST, null);
                return;
            }
            String access$2600 = IronSourceWebView.this.extractSuccessFunctionToCall(str);
            if (!TextUtils.isEmpty(access$2600)) {
                String string = sSAObj.getString(Constants.ParametersKeys.PRODUCT_TYPE);
                IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(access$2600, IronSourceWebView.this.parseToJson(Constants.ParametersKeys.USER_UNIQUE_ID, IronSourceSharedPrefHelper.getSupersonicPrefHelper().getUniqueId(string), Constants.ParametersKeys.PRODUCT_TYPE, string, null, null, null, null, null, false), Constants.JSMethods.ON_GET_USER_UNIQUE_ID_SUCCESS, Constants.JSMethods.ON_GET_USER_UNIQUE_ID_FAIL));
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0062, code lost:
            if (android.text.TextUtils.isEmpty(r5) == false) goto L_0x0064;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0055, code lost:
            if (android.text.TextUtils.isEmpty(r5) == false) goto L_0x0064;
         */
        @android.webkit.JavascriptInterface
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void getAppsInstallTime(java.lang.String r5) {
            /*
                r4 = this;
                com.ironsource.sdk.data.SSAObj r0 = new com.ironsource.sdk.data.SSAObj
                r0.<init>(r5)
                java.lang.String r1 = "systemApps"
                java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x001f }
                com.ironsource.sdk.controller.IronSourceWebView r1 = com.ironsource.sdk.controller.IronSourceWebView.this     // Catch:{ Exception -> 0x001f }
                android.content.Context r1 = r1.getContext()     // Catch:{ Exception -> 0x001f }
                boolean r0 = java.lang.Boolean.parseBoolean(r0)     // Catch:{ Exception -> 0x001f }
                org.json.JSONObject r0 = com.ironsource.environment.DeviceStatus.getAppsInstallTime(r1, r0)     // Catch:{ Exception -> 0x001f }
                java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x001f }
                r1 = 0
                goto L_0x0048
            L_0x001f:
                r0 = move-exception
                com.ironsource.sdk.controller.IronSourceWebView r1 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r1 = r1.TAG
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "getAppsInstallTime failed("
                r2.append(r3)
                java.lang.String r3 = r0.getLocalizedMessage()
                r2.append(r3)
                java.lang.String r3 = ")"
                r2.append(r3)
                java.lang.String r2 = r2.toString()
                com.ironsource.sdk.utils.Logger.i(r1, r2)
                java.lang.String r0 = r0.getLocalizedMessage()
                r1 = 1
            L_0x0048:
                r2 = 0
                if (r1 == 0) goto L_0x0058
                com.ironsource.sdk.controller.IronSourceWebView r1 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r5 = r1.extractFailFunctionToCall(r5)
                boolean r1 = android.text.TextUtils.isEmpty(r5)
                if (r1 != 0) goto L_0x0065
                goto L_0x0064
            L_0x0058:
                com.ironsource.sdk.controller.IronSourceWebView r1 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r5 = r1.extractSuccessFunctionToCall(r5)
                boolean r1 = android.text.TextUtils.isEmpty(r5)
                if (r1 != 0) goto L_0x0065
            L_0x0064:
                r2 = r5
            L_0x0065:
                boolean r5 = android.text.TextUtils.isEmpty(r2)
                if (r5 != 0) goto L_0x0087
                java.nio.charset.Charset r5 = java.nio.charset.Charset.defaultCharset()     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                java.lang.String r5 = r5.name()     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                java.lang.String r0 = java.net.URLDecoder.decode(r0, r5)     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                goto L_0x007c
            L_0x0078:
                r5 = move-exception
                r5.printStackTrace()
            L_0x007c:
                com.ironsource.sdk.controller.IronSourceWebView r5 = com.ironsource.sdk.controller.IronSourceWebView.this
                java.lang.String r5 = r5.generateJSToInject(r2, r0)
                com.ironsource.sdk.controller.IronSourceWebView r0 = com.ironsource.sdk.controller.IronSourceWebView.this
                r0.injectJavascript(r5)
            L_0x0087:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.getAppsInstallTime(java.lang.String):void");
        }

        @JavascriptInterface
        public void onGetUserUniqueIdSuccess(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGetUserUniqueIdSuccess(" + str + ")");
        }

        @JavascriptInterface
        public void onGetUserUniqueIdFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGetUserUniqueIdFail(" + str + ")");
        }

        @JavascriptInterface
        public void onOfferWallGeneric(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onOfferWallGeneric(" + str + ")");
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.OfferWall.toString())) {
                IronSourceWebView.this.mOnOfferWallListener.onOWGeneric("", "");
            }
        }

        @JavascriptInterface
        public void setUserData(String str) {
            String str2 = str;
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "setUserData(" + str2 + ")");
            SSAObj sSAObj = new SSAObj(str2);
            if (!sSAObj.containsKey(Constants.ParametersKeys.KEY)) {
                IronSourceWebView.this.responseBack(str2, false, Constants.ErrorCodes.KEY_DOES_NOT_EXIST, null);
            } else if (!sSAObj.containsKey("value")) {
                IronSourceWebView.this.responseBack(str2, false, Constants.ErrorCodes.VALUE_DOES_NOT_EXIST, null);
            } else {
                String string = sSAObj.getString(Constants.ParametersKeys.KEY);
                String string2 = sSAObj.getString("value");
                if (IronSourceSharedPrefHelper.getSupersonicPrefHelper().setUserData(string, string2)) {
                    IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(IronSourceWebView.this.extractSuccessFunctionToCall(str2), IronSourceWebView.this.parseToJson(string, string2, null, null, null, null, null, null, null, false)));
                    return;
                }
                IronSourceWebView.this.responseBack(str2, false, "SetUserData failed writing to shared preferences", null);
            }
        }

        @JavascriptInterface
        public void getUserData(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "getUserData(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.containsKey(Constants.ParametersKeys.KEY)) {
                IronSourceWebView.this.responseBack(str, false, Constants.ErrorCodes.KEY_DOES_NOT_EXIST, null);
                return;
            }
            String access$2600 = IronSourceWebView.this.extractSuccessFunctionToCall(str);
            String string = sSAObj.getString(Constants.ParametersKeys.KEY);
            IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(access$2600, IronSourceWebView.this.parseToJson(string, IronSourceSharedPrefHelper.getSupersonicPrefHelper().getUserData(string), null, null, null, null, null, null, null, false)));
        }

        @JavascriptInterface
        public void onGetUserCreditsFail(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onGetUserCreditsFail(" + str + ")");
            final String string = new SSAObj(str).getString("errMsg");
            if (IronSourceWebView.this.shouldNotifyDeveloper(SSAEnums.ProductType.OfferWall.toString())) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        String str = string;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        IronSourceWebView.this.mOnOfferWallListener.onGetOWCreditsFailed(str);
                    }
                });
            }
            IronSourceWebView.this.responseBack(str, true, null, null);
            IronSourceWebView.this.toastingErrMsg(Constants.JSMethods.ON_GET_USER_CREDITS_FAILED, str);
        }

        @JavascriptInterface
        public void onAdWindowsClosed(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "onAdWindowsClosed(" + str + ")");
            IronSourceWebView.this.mSavedState.adClosed();
            IronSourceWebView.this.mSavedState.setDisplayedDemandSourceId(null);
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString(Constants.ParametersKeys.PRODUCT_TYPE);
            final String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
            final SSAEnums.ProductType access$4900 = IronSourceWebView.this.getStringProductTypeAsEnum(string);
            String access$4300 = IronSourceWebView.this.PUB_TAG;
            Log.d(access$4300, "onAdClosed() with type " + access$4900);
            if (IronSourceWebView.this.shouldNotifyDeveloper(string)) {
                IronSourceWebView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (access$4900 == SSAEnums.ProductType.RewardedVideo || access$4900 == SSAEnums.ProductType.Interstitial) {
                            DSAdProductListener access$5000 = IronSourceWebView.this.getAdProductListenerByProductType(access$4900);
                            if (access$5000 != null) {
                                access$5000.onAdProductClose(access$4900, fetchDemandSourceId);
                            }
                        } else if (access$4900 == SSAEnums.ProductType.OfferWall) {
                            IronSourceWebView.this.mOnOfferWallListener.onOWAdClosed();
                        }
                    }
                });
            }
        }

        @JavascriptInterface
        public void onVideoStatusChanged(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Log.d(access$500, "onVideoStatusChanged(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String string = sSAObj.getString(Constants.ParametersKeys.PRODUCT_TYPE);
            if (IronSourceWebView.this.mVideoEventsListener != null && !TextUtils.isEmpty(string)) {
                String string2 = sSAObj.getString("status");
                if (Constants.ParametersKeys.VIDEO_STATUS_STARTED.equalsIgnoreCase(string2)) {
                    IronSourceWebView.this.mVideoEventsListener.onVideoStarted();
                } else if (Constants.ParametersKeys.VIDEO_STATUS_PAUSED.equalsIgnoreCase(string2)) {
                    IronSourceWebView.this.mVideoEventsListener.onVideoPaused();
                } else if (Constants.ParametersKeys.VIDEO_STATUS_PLAYING.equalsIgnoreCase(string2)) {
                    IronSourceWebView.this.mVideoEventsListener.onVideoResumed();
                } else if (Constants.ParametersKeys.VIDEO_STATUS_ENDED.equalsIgnoreCase(string2)) {
                    IronSourceWebView.this.mVideoEventsListener.onVideoEnded();
                } else if (Constants.ParametersKeys.VIDEO_STATUS_STOPPED.equalsIgnoreCase(string2)) {
                    IronSourceWebView.this.mVideoEventsListener.onVideoStopped();
                } else {
                    String access$5002 = IronSourceWebView.this.TAG;
                    Logger.i(access$5002, "onVideoStatusChanged: unknown status: " + string2);
                }
            }
        }

        @JavascriptInterface
        public void postAdEventNotification(String str) {
            String str2 = str;
            try {
                String access$500 = IronSourceWebView.this.TAG;
                Logger.i(access$500, "postAdEventNotification(" + str2 + ")");
                SSAObj sSAObj = new SSAObj(str2);
                final String string = sSAObj.getString(Constants.ParametersKeys.EVENT_NAME);
                if (TextUtils.isEmpty(string)) {
                    IronSourceWebView.this.responseBack(str2, false, Constants.ErrorCodes.EVENT_NAME_DOES_NOT_EXIST, null);
                    return;
                }
                String string2 = sSAObj.getString(Constants.ParametersKeys.NOTIFICATION_DEMAND_SOURCE_NAME);
                String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(sSAObj);
                String str3 = !TextUtils.isEmpty(fetchDemandSourceId) ? fetchDemandSourceId : string2;
                JSONObject jSONObject = (JSONObject) sSAObj.get(Constants.ParametersKeys.EXTRA_DATA);
                String string3 = sSAObj.getString(Constants.ParametersKeys.PRODUCT_TYPE);
                SSAEnums.ProductType access$4900 = IronSourceWebView.this.getStringProductTypeAsEnum(string3);
                if (IronSourceWebView.this.shouldNotifyDeveloper(string3)) {
                    String access$2600 = IronSourceWebView.this.extractSuccessFunctionToCall(str2);
                    if (!TextUtils.isEmpty(access$2600)) {
                        IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(access$2600, IronSourceWebView.this.parseToJson(Constants.ParametersKeys.PRODUCT_TYPE, string3, Constants.ParametersKeys.EVENT_NAME, string, "demandSourceName", string2, "demandSourceId", str3, null, false), Constants.JSMethods.POST_AD_EVENT_NOTIFICATION_SUCCESS, Constants.JSMethods.POST_AD_EVENT_NOTIFICATION_FAIL));
                    }
                    final SSAEnums.ProductType productType = access$4900;
                    final String str4 = str3;
                    final JSONObject jSONObject2 = jSONObject;
                    IronSourceWebView.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (productType == SSAEnums.ProductType.Interstitial || productType == SSAEnums.ProductType.RewardedVideo) {
                                DSAdProductListener access$5000 = IronSourceWebView.this.getAdProductListenerByProductType(productType);
                                if (access$5000 != null) {
                                    access$5000.onAdProductEventNotificationReceived(productType, str4, string, jSONObject2);
                                }
                            } else if (productType == SSAEnums.ProductType.OfferWall) {
                                IronSourceWebView.this.mOnOfferWallListener.onOfferwallEventNotificationReceived(string, jSONObject2);
                            }
                        }
                    });
                    return;
                }
                IronSourceWebView.this.responseBack(str2, false, Constants.ErrorCodes.PRODUCT_TYPE_DOES_NOT_EXIST, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: package-private */
        public void sendUnauthorizedError(String str) {
            IronSourceWebView.this.injectJavascript(IronSourceWebView.this.generateJSToInject(Constants.JSMethods.ON_UNAUTHORIZED_MESSAGE, str, null, null));
        }

        class JSCallbackTask {
            JSCallbackTask() {
            }

            /* access modifiers changed from: package-private */
            public void sendMessage(boolean z, String str, String str2) {
                SSAObj sSAObj = new SSAObj();
                sSAObj.put(z ? IronSourceWebView.JSON_KEY_SUCCESS : IronSourceWebView.JSON_KEY_FAIL, str);
                sSAObj.put("data", str2);
                IronSourceWebView.this.responseBack(sSAObj.toString(), z, null, null);
            }

            /* access modifiers changed from: package-private */
            public void sendMessage(boolean z, String str, SSAObj sSAObj) {
                sSAObj.put(z ? IronSourceWebView.JSON_KEY_SUCCESS : IronSourceWebView.JSON_KEY_FAIL, str);
                IronSourceWebView.this.responseBack(sSAObj.toString(), z, null, null);
            }

            /* access modifiers changed from: package-private */
            public void sendMessage(boolean z, String str, JSONObject jSONObject) {
                String str2;
                if (z) {
                    try {
                        str2 = IronSourceWebView.JSON_KEY_SUCCESS;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        e.getMessage();
                        return;
                    }
                } else {
                    str2 = IronSourceWebView.JSON_KEY_FAIL;
                }
                jSONObject.put(str2, str);
                IronSourceWebView.this.responseBack(jSONObject.toString(), z, null, null);
            }
        }

        @JavascriptInterface
        public void bannerViewAPI(String str) {
            try {
                IronSourceWebView.this.mBannerJsAdapter.sendMessageToISNAdView(str);
            } catch (Exception e) {
                e.printStackTrace();
                String access$500 = IronSourceWebView.this.TAG;
                Logger.e(access$500, "bannerViewAPI failed with exception " + e.getMessage());
            }
        }

        @JavascriptInterface
        public void moatAPI(final String str) {
            IronSourceWebView.this.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        String access$500 = IronSourceWebView.this.TAG;
                        Logger.i(access$500, "moatAPI(" + str + ")");
                        IronSourceWebView.this.mMoatJsAdapter.call(new SSAObj(str).toString(), new JSCallbackTask(), IronSourceWebView.this.getWebview());
                    } catch (Exception e) {
                        e.printStackTrace();
                        String access$5002 = IronSourceWebView.this.TAG;
                        Logger.i(access$5002, "moatAPI failed with exception " + e.getMessage());
                    }
                }
            });
        }

        @JavascriptInterface
        public void permissionsAPI(String str) {
            try {
                String access$500 = IronSourceWebView.this.TAG;
                Logger.i(access$500, "permissionsAPI(" + str + ")");
                IronSourceWebView.this.mPermissionsJsAdapter.call(new SSAObj(str).toString(), new JSCallbackTask());
            } catch (Exception e) {
                e.printStackTrace();
                String access$5002 = IronSourceWebView.this.TAG;
                Logger.i(access$5002, "permissionsAPI failed with exception " + e.getMessage());
            }
        }

        @JavascriptInterface
        public void getDeviceVolume(String str) {
            String access$500 = IronSourceWebView.this.TAG;
            Logger.i(access$500, "getDeviceVolume(" + str + ")");
            try {
                float deviceVolume = DeviceProperties.getInstance(IronSourceWebView.this.getCurrentActivityContext()).getDeviceVolume(IronSourceWebView.this.getCurrentActivityContext());
                SSAObj sSAObj = new SSAObj(str);
                sSAObj.put(Constants.RequestParameters.DEVICE_VOLUME, String.valueOf(deviceVolume));
                IronSourceWebView.this.responseBack(sSAObj.toString(), true, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void invokePendingCommands() {
        while (this.mControllerCommandsQueue.size() > 0) {
            injectJavascript(this.mControllerCommandsQueue.get(0));
            this.mControllerCommandsQueue.remove(0);
        }
    }

    /* access modifiers changed from: private */
    public DSAdProductListener getAdProductListenerByProductType(SSAEnums.ProductType productType) {
        if (productType == SSAEnums.ProductType.Interstitial) {
            return this.mDSInterstitialListener;
        }
        if (productType == SSAEnums.ProductType.RewardedVideo) {
            return this.mDSRewardedVideoListener;
        }
        if (productType == SSAEnums.ProductType.Banner) {
            return this.mDSBannerListener;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public SSAEnums.ProductType getStringProductTypeAsEnum(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.equalsIgnoreCase(SSAEnums.ProductType.Interstitial.toString())) {
            return SSAEnums.ProductType.Interstitial;
        }
        if (str.equalsIgnoreCase(SSAEnums.ProductType.RewardedVideo.toString())) {
            return SSAEnums.ProductType.RewardedVideo;
        }
        if (str.equalsIgnoreCase(SSAEnums.ProductType.OfferWall.toString())) {
            return SSAEnums.ProductType.OfferWall;
        }
        if (str.equalsIgnoreCase(SSAEnums.ProductType.Banner.toString())) {
            return SSAEnums.ProductType.Banner;
        }
        return null;
    }

    public static void setEXTERNAL_URL(String str) {
        EXTERNAL_URL = str;
    }

    public void setVideoEventsListener(VideoEventsListener videoEventsListener) {
        this.mVideoEventsListener = videoEventsListener;
    }

    public void removeVideoEventsListener() {
        this.mVideoEventsListener = null;
    }

    /* access modifiers changed from: private */
    public void setWebviewBackground(String str) {
        String string = new SSAObj(str).getString(Constants.ParametersKeys.COLOR);
        setBackgroundColor(!Constants.ParametersKeys.TRANSPARENT.equalsIgnoreCase(string) ? Color.parseColor(string) : 0);
    }

    public void load(int i) {
        try {
            loadUrl("about:blank");
        } catch (Throwable th) {
            Logger.e(this.TAG, "WebViewController:: load: " + th.toString());
            new IronSourceAsyncHttpRequestTask().execute("https://www.supersonicads.com/mobile/sdk5/log?method=webviewLoadBlank");
        }
        String str = Advertisement.FILE_SCHEME + this.mCacheDirectory + File.separator + Constants.MOBILE_CONTROLLER_HTML;
        if (new File(this.mCacheDirectory + File.separator + Constants.MOBILE_CONTROLLER_HTML).exists()) {
            JSONObject controllerConfigAsJSONObject = SDKUtils.getControllerConfigAsJSONObject();
            setWebDebuggingEnabled(controllerConfigAsJSONObject);
            String str2 = str + "?" + getRequestParameters(controllerConfigAsJSONObject);
            final int i2 = i;
            this.mLoadControllerTimer = new CountDownTimer(50000, 1000) {
                public void onTick(long j) {
                    String access$500 = IronSourceWebView.this.TAG;
                    Logger.i(access$500, "Loading Controller Timer Tick " + j);
                }

                public void onFinish() {
                    Logger.i(IronSourceWebView.this.TAG, "Loading Controller Timer Finish");
                    if (i2 == 3) {
                        IronSourceWebView.this.mGlobalControllerTimer.cancel();
                        for (DemandSource next : IronSourceWebView.this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.RewardedVideo)) {
                            if (next.getDemandSourceInitState() == 1) {
                                IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.RewardedVideo, next);
                            }
                        }
                        for (DemandSource next2 : IronSourceWebView.this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.Interstitial)) {
                            if (next2.getDemandSourceInitState() == 1) {
                                IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.Interstitial, next2);
                            }
                        }
                        for (DemandSource next3 : IronSourceWebView.this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.Banner)) {
                            if (next3.getDemandSourceInitState() == 1) {
                                IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.Banner, next3);
                            }
                        }
                        if (IronSourceWebView.this.mOWmiss) {
                            IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.OfferWall, null);
                        }
                        if (IronSourceWebView.this.mOWCreditsMiss) {
                            IronSourceWebView.this.sendProductErrorMessage(SSAEnums.ProductType.OfferWallCredits, null);
                            return;
                        }
                        return;
                    }
                    IronSourceWebView.this.load(2);
                }
            }.start();
            try {
                loadUrl(str2);
            } catch (Throwable th2) {
                Logger.e(this.TAG, "WebViewController:: load: " + th2.toString());
                new IronSourceAsyncHttpRequestTask().execute("https://www.supersonicads.com/mobile/sdk5/log?method=webviewLoadWithPath");
            }
            Logger.i(this.TAG, "load(): " + str2);
            return;
        }
        Logger.i(this.TAG, "load(): Mobile Controller HTML Does not exist");
        new IronSourceAsyncHttpRequestTask().execute("https://www.supersonicads.com/mobile/sdk5/log?method=htmlControllerDoesNotExistOnFileSystem");
    }

    private void setWebDebuggingEnabled(JSONObject jSONObject) {
        if (jSONObject.optBoolean("inspectWebview")) {
            setWebDebuggingEnabled();
        }
    }

    private void initProduct(String str, String str2, SSAEnums.ProductType productType, DemandSource demandSource, OnInitProductHandler onInitProductHandler) {
        if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str)) {
            onInitProductHandler.handleInitProductFailed("User id or Application key are missing", productType, demandSource);
        } else if (this.mControllerState == SSAEnums.ControllerState.Failed) {
            onInitProductHandler.handleInitProductFailed(SDKUtils.createErrorMessage(getErrorCodeByProductType(productType), Constants.ErrorCodes.InitiatingController), productType, demandSource);
        } else {
            IronSourceSharedPrefHelper.getSupersonicPrefHelper().setApplicationKey(str);
            Result createInitProductJSMethod = createInitProductJSMethod(productType, demandSource);
            injectJavascript(createInitProductJSMethod.methodName, createInitProductJSMethod.script);
            if (!isControllerStateReady()) {
                setMissProduct(productType, demandSource);
                if (this.mGlobalControllerTimeFinish) {
                    downloadController();
                    Logger.i(this.TAG, "initProduct | downloadController");
                }
            }
        }
    }

    public void initRewardedVideo(String str, String str2, DemandSource demandSource, DSRewardedVideoListener dSRewardedVideoListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mDSRewardedVideoListener = dSRewardedVideoListener;
        this.mSavedState.setRVAppKey(str);
        this.mSavedState.setRVUserId(str2);
        initProduct(str, str2, SSAEnums.ProductType.RewardedVideo, demandSource, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, SSAEnums.ProductType productType, DemandSource demandSource) {
                IronSourceWebView.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void initInterstitial(String str, String str2, DemandSource demandSource, DSInterstitialListener dSInterstitialListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mDSInterstitialListener = dSInterstitialListener;
        this.mSavedState.setInterstitialAppKey(this.mApplicationKey);
        this.mSavedState.setInterstitialUserId(this.mUserId);
        initProduct(this.mApplicationKey, this.mUserId, SSAEnums.ProductType.Interstitial, demandSource, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, SSAEnums.ProductType productType, DemandSource demandSource) {
                IronSourceWebView.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void loadInterstitial(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("demandSourceName", str);
        String flatMapToJsonAsString = SDKUtils.flatMapToJsonAsString(hashMap);
        this.mSavedState.setReportLoadInterstitial(str, true);
        injectJavascript(generateJSToInject(Constants.JSMethods.LOAD_INTERSTITIAL, flatMapToJsonAsString, Constants.JSMethods.ON_LOAD_INTERSTITIAL_SUCCESS, Constants.JSMethods.ON_LOAD_INTERSTITIAL_FAIL));
    }

    public void loadInterstitial(DemandSource demandSource, Map<String, String> map) {
        if (this.mControllerState == SSAEnums.ControllerState.Failed) {
            handleLoadDuringControllerFailed(demandSource);
        } else {
            handleLoadAd(demandSource, map);
        }
    }

    private void handleLoadDuringControllerFailed(final DemandSource demandSource) {
        if (demandSource != null && shouldNotifyDeveloper(SSAEnums.ProductType.Interstitial.toString())) {
            runOnUiThread(new Runnable() {
                public void run() {
                    IronSourceWebView.this.mDSInterstitialListener.onInterstitialLoadFailed(demandSource.getId(), "Load during controllerState failed");
                }
            });
        }
    }

    private void handleLoadAd(DemandSource demandSource, Map<String, String> map) {
        Map<String, String> mergeHashMaps = SDKUtils.mergeHashMaps(new Map[]{map, demandSource.convertToMap()});
        this.mSavedState.setReportLoadInterstitial(demandSource.getId(), true);
        injectJavascript(Constants.JSMethods.LOAD_INTERSTITIAL, generateJSToInject(Constants.JSMethods.LOAD_INTERSTITIAL, SDKUtils.flatMapToJsonAsString(mergeHashMaps), Constants.JSMethods.ON_LOAD_INTERSTITIAL_SUCCESS, Constants.JSMethods.ON_LOAD_INTERSTITIAL_FAIL));
    }

    public void showInterstitial(JSONObject jSONObject) {
        injectJavascript(createShowProductJSMethod(SSAEnums.ProductType.Interstitial, jSONObject));
    }

    public void showInterstitial(DemandSource demandSource, Map<String, String> map) {
        injectJavascript(createShowProductJSMethod(SSAEnums.ProductType.Interstitial, new JSONObject(SDKUtils.mergeHashMaps(new Map[]{map, demandSource.convertToMap()}))));
    }

    public boolean isInterstitialAdAvailable(String str) {
        DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(SSAEnums.ProductType.Interstitial, str);
        return demandSourceById != null && demandSourceById.getAvailabilityState();
    }

    public void initOfferWall(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mOWExtraParameters = map;
        this.mOnOfferWallListener = onOfferWallListener;
        this.mSavedState.setOfferWallExtraParams(this.mOWExtraParameters);
        this.mSavedState.setOfferwallReportInit(true);
        initProduct(this.mApplicationKey, this.mUserId, SSAEnums.ProductType.OfferWall, null, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, SSAEnums.ProductType productType, DemandSource demandSource) {
                IronSourceWebView.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void showOfferWall(Map<String, String> map) {
        this.mOWExtraParameters = map;
        injectJavascript(generateJSToInject(Constants.JSMethods.SHOW_OFFER_WALL, Constants.JSMethods.ON_SHOW_OFFER_WALL_SUCCESS, Constants.JSMethods.ON_SHOW_OFFER_WALL_FAIL));
    }

    public void getOfferWallCredits(String str, String str2, OnOfferWallListener onOfferWallListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mOnOfferWallListener = onOfferWallListener;
        initProduct(this.mApplicationKey, this.mUserId, SSAEnums.ProductType.OfferWallCredits, null, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, SSAEnums.ProductType productType, DemandSource demandSource) {
                IronSourceWebView.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void initBanner(String str, String str2, DemandSource demandSource, DSBannerListener dSBannerListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mDSBannerListener = dSBannerListener;
        initProduct(str, str2, SSAEnums.ProductType.Banner, demandSource, new OnInitProductHandler() {
            public void handleInitProductFailed(String str, SSAEnums.ProductType productType, DemandSource demandSource) {
                IronSourceWebView.this.triggerOnControllerInitProductFail(str, productType, demandSource);
            }
        });
    }

    public void loadBanner(JSONObject jSONObject) {
        if (jSONObject != null) {
            injectJavascript(generateJSToInject(Constants.JSMethods.LOAD_BANNER, jSONObject.toString(), Constants.JSMethods.ON_LOAD_BANNER_SUCCESS, Constants.JSMethods.ON_LOAD_BANNER_FAIL));
        }
    }

    public void updateConsentInfo(JSONObject jSONObject) {
        injectJavascript(Constants.JSMethods.UPDATE_CONSENT_INFO, generateJSToInject(Constants.JSMethods.UPDATE_CONSENT_INFO, jSONObject != null ? jSONObject.toString() : null));
    }

    static class Result {
        String methodName;
        String script;

        Result() {
        }
    }

    private Result createInitProductJSMethod(SSAEnums.ProductType productType, DemandSource demandSource) {
        Result result = new Result();
        if (productType == SSAEnums.ProductType.RewardedVideo || productType == SSAEnums.ProductType.Interstitial || productType == SSAEnums.ProductType.OfferWall || productType == SSAEnums.ProductType.Banner) {
            HashMap hashMap = new HashMap();
            hashMap.put(Constants.RequestParameters.APPLICATION_KEY, this.mApplicationKey);
            hashMap.put(Constants.RequestParameters.APPLICATION_USER_ID, this.mUserId);
            if (demandSource != null) {
                if (demandSource.getExtraParams() != null) {
                    hashMap.putAll(demandSource.getExtraParams());
                }
                hashMap.put("demandSourceName", demandSource.getDemandSourceName());
                hashMap.put("demandSourceId", demandSource.getId());
            }
            Map<String, String> extraParamsByProduct = getExtraParamsByProduct(productType);
            if (extraParamsByProduct != null) {
                hashMap.putAll(extraParamsByProduct);
            }
            String flatMapToJsonAsString = SDKUtils.flatMapToJsonAsString(hashMap);
            Constants.JSMethods initMethodByProduct = Constants.JSMethods.getInitMethodByProduct(productType);
            String generateJSToInject = generateJSToInject(initMethodByProduct.methodName, flatMapToJsonAsString, initMethodByProduct.successCallbackName, initMethodByProduct.failureCallbackName);
            result.methodName = initMethodByProduct.methodName;
            result.script = generateJSToInject;
        } else if (productType == SSAEnums.ProductType.OfferWallCredits) {
            String generateJSToInject2 = generateJSToInject(Constants.JSMethods.GET_USER_CREDITS, parseToJson(Constants.ParametersKeys.PRODUCT_TYPE, Constants.ParametersKeys.OFFER_WALL, Constants.RequestParameters.APPLICATION_KEY, this.mApplicationKey, Constants.RequestParameters.APPLICATION_USER_ID, this.mUserId, null, null, null, false), "null", Constants.JSMethods.ON_GET_USER_CREDITS_FAILED);
            result.methodName = Constants.JSMethods.GET_USER_CREDITS;
            result.script = generateJSToInject2;
        }
        return result;
    }

    private String createShowProductJSMethod(SSAEnums.ProductType productType, JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        hashMap.put("sessionDepth", Integer.toString(jSONObject.optInt("sessionDepth")));
        String optString = jSONObject.optString("demandSourceName");
        String fetchDemandSourceId = SDKUtils.fetchDemandSourceId(jSONObject);
        DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(productType, fetchDemandSourceId);
        if (demandSourceById != null) {
            if (demandSourceById.getExtraParams() != null) {
                hashMap.putAll(demandSourceById.getExtraParams());
            }
            if (!TextUtils.isEmpty(optString)) {
                hashMap.put("demandSourceName", optString);
            }
            if (!TextUtils.isEmpty(fetchDemandSourceId)) {
                hashMap.put("demandSourceId", fetchDemandSourceId);
            }
        }
        Map<String, String> extraParamsByProduct = getExtraParamsByProduct(productType);
        if (extraParamsByProduct != null) {
            hashMap.putAll(extraParamsByProduct);
        }
        String flatMapToJsonAsString = SDKUtils.flatMapToJsonAsString(hashMap);
        Constants.JSMethods showMethodByProduct = Constants.JSMethods.getShowMethodByProduct(productType);
        return generateJSToInject(showMethodByProduct.methodName, flatMapToJsonAsString, showMethodByProduct.successCallbackName, showMethodByProduct.failureCallbackName);
    }

    /* access modifiers changed from: package-private */
    public void setMissProduct(SSAEnums.ProductType productType, DemandSource demandSource) {
        if (productType == SSAEnums.ProductType.RewardedVideo || productType == SSAEnums.ProductType.Interstitial || productType == SSAEnums.ProductType.Banner) {
            if (demandSource != null) {
                demandSource.setDemandSourceInitState(1);
            }
        } else if (productType == SSAEnums.ProductType.OfferWall) {
            this.mOWmiss = true;
        } else if (productType == SSAEnums.ProductType.OfferWallCredits) {
            this.mOWCreditsMiss = true;
        }
        String str = this.TAG;
        Logger.i(str, "setMissProduct(" + productType + ")");
    }

    /* access modifiers changed from: private */
    public void triggerOnControllerInitProductFail(final String str, final SSAEnums.ProductType productType, final DemandSource demandSource) {
        if (shouldNotifyDeveloper(productType.toString())) {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (SSAEnums.ProductType.RewardedVideo == productType || SSAEnums.ProductType.Interstitial == productType || SSAEnums.ProductType.Banner == productType) {
                        DemandSource demandSource = demandSource;
                        if (demandSource != null && !TextUtils.isEmpty(demandSource.getId())) {
                            DSAdProductListener access$5000 = IronSourceWebView.this.getAdProductListenerByProductType(productType);
                            String access$500 = IronSourceWebView.this.TAG;
                            Log.d(access$500, "onAdProductInitFailed (message:" + str + ")(" + productType + ")");
                            if (access$5000 != null) {
                                access$5000.onAdProductInitFailed(productType, demandSource.getId(), str);
                            }
                        }
                    } else if (SSAEnums.ProductType.OfferWall == productType) {
                        IronSourceWebView.this.mOnOfferWallListener.onOfferwallInitFail(str);
                    } else if (SSAEnums.ProductType.OfferWallCredits == productType) {
                        IronSourceWebView.this.mOnOfferWallListener.onGetOWCreditsFailed(str);
                    }
                }
            });
        }
    }

    public void showRewardedVideo(JSONObject jSONObject) {
        injectJavascript(createShowProductJSMethod(SSAEnums.ProductType.RewardedVideo, jSONObject));
    }

    public void assetCached(String str, String str2) {
        injectJavascript(generateJSToInject(Constants.JSMethods.ASSET_CACHED, parseToJson(Constants.ParametersKeys.FILE, str, "path", str2, null, null, null, null, null, false)));
    }

    public void assetCachedFailed(String str, String str2, String str3) {
        injectJavascript(generateJSToInject(Constants.JSMethods.ASSET_CACHED_FAILED, parseToJson(Constants.ParametersKeys.FILE, str, "path", str2, "errMsg", str3, null, null, null, false)));
    }

    public void enterBackground() {
        if (this.mControllerState == SSAEnums.ControllerState.Ready) {
            injectJavascript(generateJSToInject(Constants.JSMethods.ENTER_BACKGROUND));
        }
    }

    public void enterForeground() {
        if (this.mControllerState == SSAEnums.ControllerState.Ready) {
            injectJavascript(generateJSToInject(Constants.JSMethods.ENTER_FOREGROUND));
        }
    }

    public void viewableChange(boolean z, String str) {
        injectJavascript(generateJSToInject(Constants.JSMethods.VIEWABLE_CHANGE, parseToJson(Constants.ParametersKeys.WEB_VIEW, str, null, null, null, null, null, null, Constants.ParametersKeys.IS_VIEWABLE, z)));
    }

    public void nativeNavigationPressed(String str) {
        injectJavascript(generateJSToInject(Constants.JSMethods.NATIVE_NAVIGATION_PRESSED, parseToJson("action", str, null, null, null, null, null, null, null, false)));
    }

    public void pageFinished() {
        injectJavascript(generateJSToInject(Constants.JSMethods.PAGE_FINISHED));
    }

    public void interceptedUrlToStore() {
        injectJavascript(generateJSToInject(Constants.JSMethods.INTERCEPTED_URL_TO_STORE));
    }

    public void failedToStartStoreActivity(String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            str2 = Constants.ErrorCodes.STORE_ACTIVITY_FAILED_UNKNOWN_URL;
        }
        String str3 = str2;
        if (TextUtils.isEmpty(str)) {
            str = Constants.ErrorCodes.STORE_ACTIVITY_FAILED_REASON_UNSPECIFIED;
        }
        injectJavascript(generateJSToInject(Constants.JSMethods.FAILED_TO_START_STORE_ACTIVITY, parseToJson("errMsg", str, "url", str3, null, null, null, null, null, false)));
    }

    private void injectJavascript(String str, String str2) {
        if (!isControllerStateReady()) {
            String str3 = this.TAG;
            Logger.d(str3, "injectJavascript | ControllerCommandsQueue | adding command to queue | " + str);
            this.mControllerCommandsQueue.add(str2);
            return;
        }
        injectJavascript(str2);
    }

    private boolean isControllerStateReady() {
        return SSAEnums.ControllerState.Ready.equals(this.mControllerState);
    }

    /* access modifiers changed from: private */
    public void injectJavascript(String str) {
        if (!TextUtils.isEmpty(str)) {
            String str2 = "console.log(\"JS exeption: \" + JSON.stringify(e));";
            if (getDebugMode() != SSAEnums.DebugMode.MODE_0.getValue() && (getDebugMode() < SSAEnums.DebugMode.MODE_1.getValue() || getDebugMode() > SSAEnums.DebugMode.MODE_3.getValue())) {
                str2 = "empty";
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("try{");
            sb.append(str);
            sb.append("}catch(e){");
            sb.append(str2);
            sb.append("}");
            final String str3 = "javascript:" + sb.toString();
            runOnUiThread(new Runnable() {
                public void run() {
                    Logger.i(IronSourceWebView.this.TAG, str3);
                    try {
                        if (IronSourceWebView.this.isKitkatAndAbove != null) {
                            if (IronSourceWebView.this.isKitkatAndAbove.booleanValue()) {
                                IronSourceWebView.this.evaluateJavascriptKitKat(sb.toString());
                            } else {
                                IronSourceWebView.this.loadUrl(str3);
                            }
                        } else if (Build.VERSION.SDK_INT >= 19) {
                            IronSourceWebView.this.evaluateJavascriptKitKat(sb.toString());
                            Boolean unused = IronSourceWebView.this.isKitkatAndAbove = true;
                        } else {
                            IronSourceWebView.this.loadUrl(str3);
                            Boolean unused2 = IronSourceWebView.this.isKitkatAndAbove = false;
                        }
                    } catch (NoSuchMethodError e) {
                        String access$500 = IronSourceWebView.this.TAG;
                        Logger.e(access$500, "evaluateJavascrip NoSuchMethodError: SDK version=" + Build.VERSION.SDK_INT + " " + e);
                        IronSourceWebView.this.loadUrl(str3);
                        Boolean unused3 = IronSourceWebView.this.isKitkatAndAbove = false;
                    } catch (Throwable th) {
                        String access$5002 = IronSourceWebView.this.TAG;
                        Logger.e(access$5002, "injectJavascript: " + th.toString());
                        new IronSourceAsyncHttpRequestTask().execute("https://www.supersonicads.com/mobile/sdk5/log?method=injectJavaScript");
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void evaluateJavascriptKitKat(String str) {
        evaluateJavascript(str, null);
    }

    public Context getCurrentActivityContext() {
        return ((MutableContextWrapper) this.mCurrentActivityContext).getBaseContext();
    }

    private String getRequestParameters(JSONObject jSONObject) {
        DeviceProperties instance = DeviceProperties.getInstance(getContext());
        StringBuilder sb = new StringBuilder();
        String supersonicSdkVersion = DeviceProperties.getSupersonicSdkVersion();
        if (!TextUtils.isEmpty(supersonicSdkVersion)) {
            sb.append(Constants.RequestParameters.SDK_VERSION);
            sb.append(Constants.RequestParameters.EQUAL);
            sb.append(supersonicSdkVersion);
            sb.append(Constants.RequestParameters.AMPERSAND);
        }
        String deviceOsType = instance.getDeviceOsType();
        if (!TextUtils.isEmpty(deviceOsType)) {
            sb.append(Constants.RequestParameters.DEVICE_OS);
            sb.append(Constants.RequestParameters.EQUAL);
            sb.append(deviceOsType);
        }
        Uri parse = Uri.parse(SDKUtils.getControllerUrl());
        if (parse != null) {
            String str = parse.getScheme() + ":";
            String host = parse.getHost();
            int port = parse.getPort();
            if (port != -1) {
                host = host + ":" + port;
            }
            sb.append(Constants.RequestParameters.AMPERSAND);
            sb.append(Constants.RequestParameters.PROTOCOL);
            sb.append(Constants.RequestParameters.EQUAL);
            sb.append(str);
            sb.append(Constants.RequestParameters.AMPERSAND);
            sb.append("domain");
            sb.append(Constants.RequestParameters.EQUAL);
            sb.append(host);
            if (jSONObject.keys().hasNext()) {
                try {
                    String jSONObject2 = new JSONObject(jSONObject, new String[]{Constants.RequestParameters.IS_SECURED, Constants.RequestParameters.APPLICATION_KEY}).toString();
                    if (!TextUtils.isEmpty(jSONObject2)) {
                        sb.append(Constants.RequestParameters.AMPERSAND);
                        sb.append(Constants.RequestParameters.CONTROLLER_CONFIG);
                        sb.append(Constants.RequestParameters.EQUAL);
                        sb.append(jSONObject2);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            sb.append(Constants.RequestParameters.AMPERSAND);
            sb.append(Constants.RequestParameters.DEBUG);
            sb.append(Constants.RequestParameters.EQUAL);
            sb.append(getDebugMode());
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public void closeWebView() {
        OnWebViewChangeListener onWebViewChangeListener = this.mChangeListener;
        if (onWebViewChangeListener != null) {
            onWebViewChangeListener.onCloseRequested();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0017, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x0023;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void responseBack(java.lang.String r4, boolean r5, java.lang.String r6, java.lang.String r7) {
        /*
            r3 = this;
            com.ironsource.sdk.data.SSAObj r0 = new com.ironsource.sdk.data.SSAObj
            r0.<init>(r4)
            java.lang.String r1 = com.ironsource.sdk.controller.IronSourceWebView.JSON_KEY_SUCCESS
            java.lang.String r1 = r0.getString(r1)
            java.lang.String r2 = com.ironsource.sdk.controller.IronSourceWebView.JSON_KEY_FAIL
            java.lang.String r0 = r0.getString(r2)
            if (r5 == 0) goto L_0x001a
            boolean r5 = android.text.TextUtils.isEmpty(r1)
            if (r5 != 0) goto L_0x0022
            goto L_0x0023
        L_0x001a:
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 != 0) goto L_0x0022
            r1 = r0
            goto L_0x0023
        L_0x0022:
            r1 = 0
        L_0x0023:
            boolean r5 = android.text.TextUtils.isEmpty(r1)
            if (r5 != 0) goto L_0x005c
            boolean r5 = android.text.TextUtils.isEmpty(r6)
            if (r5 != 0) goto L_0x0040
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x003f }
            r5.<init>(r4)     // Catch:{ JSONException -> 0x003f }
            java.lang.String r0 = "errMsg"
            org.json.JSONObject r5 = r5.put(r0, r6)     // Catch:{ JSONException -> 0x003f }
            java.lang.String r4 = r5.toString()     // Catch:{ JSONException -> 0x003f }
            goto L_0x0040
        L_0x003f:
        L_0x0040:
            boolean r5 = android.text.TextUtils.isEmpty(r7)
            if (r5 != 0) goto L_0x0055
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0055 }
            r5.<init>(r4)     // Catch:{ JSONException -> 0x0055 }
            java.lang.String r6 = "errCode"
            org.json.JSONObject r5 = r5.put(r6, r7)     // Catch:{ JSONException -> 0x0055 }
            java.lang.String r4 = r5.toString()     // Catch:{ JSONException -> 0x0055 }
        L_0x0055:
            java.lang.String r4 = r3.generateJSToInject(r1, r4)
            r3.injectJavascript(r4)
        L_0x005c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.IronSourceWebView.responseBack(java.lang.String, boolean, java.lang.String, java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public String extractSuccessFunctionToCall(String str) {
        return new SSAObj(str).getString(JSON_KEY_SUCCESS);
    }

    /* access modifiers changed from: private */
    public String extractFailFunctionToCall(String str) {
        return new SSAObj(str).getString(JSON_KEY_FAIL);
    }

    /* access modifiers changed from: private */
    public String parseToJson(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, boolean z) {
        JSONObject jSONObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                jSONObject.put(str, SDKUtils.encodeString(str2));
            }
            if (!TextUtils.isEmpty(str3) && !TextUtils.isEmpty(str4)) {
                jSONObject.put(str3, SDKUtils.encodeString(str4));
            }
            if (!TextUtils.isEmpty(str5) && !TextUtils.isEmpty(str6)) {
                jSONObject.put(str5, SDKUtils.encodeString(str6));
            }
            if (!TextUtils.isEmpty(str7) && !TextUtils.isEmpty(str8)) {
                jSONObject.put(str7, SDKUtils.encodeString(str8));
            }
            if (!TextUtils.isEmpty(str9)) {
                jSONObject.put(str9, z);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            ironSourceAsyncHttpRequestTask.execute(Constants.NATIVE_EXCEPTION_BASE_URL + e.getStackTrace()[0].getMethodName());
        }
        return jSONObject.toString();
    }

    private String mapToJson(Map<String, String> map) {
        JSONObject jSONObject = new JSONObject();
        if (map != null && !map.isEmpty()) {
            for (String next : map.keySet()) {
                try {
                    jSONObject.put(next, SDKUtils.encodeString(map.get(next)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return jSONObject.toString();
    }

    /* access modifiers changed from: private */
    public Object[] getDeviceParams(Context context) {
        boolean z;
        DeviceProperties instance = DeviceProperties.getInstance(context);
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(Constants.RequestParameters.APP_ORIENTATION, SDKUtils.translateRequestedOrientation(DeviceStatus.getActivityRequestedOrientation(getCurrentActivityContext())));
            String deviceOem = instance.getDeviceOem();
            if (deviceOem != null) {
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_OEM), SDKUtils.encodeString(deviceOem));
            }
            String deviceModel = instance.getDeviceModel();
            if (deviceModel != null) {
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_MODEL), SDKUtils.encodeString(deviceModel));
                z = false;
            } else {
                z = true;
            }
            try {
                SDKUtils.loadGoogleAdvertiserInfo(context);
                String advertiserId = SDKUtils.getAdvertiserId();
                Boolean valueOf = Boolean.valueOf(SDKUtils.isLimitAdTrackingEnabled());
                if (!TextUtils.isEmpty(advertiserId)) {
                    Logger.i(this.TAG, "add AID and LAT");
                    jSONObject.put(Constants.RequestParameters.isLAT, valueOf);
                    jSONObject.put(Constants.RequestParameters.DEVICE_IDS + Constants.RequestParameters.LEFT_BRACKETS + Constants.RequestParameters.AID + Constants.RequestParameters.RIGHT_BRACKETS, SDKUtils.encodeString(advertiserId));
                }
                String deviceOsType = instance.getDeviceOsType();
                if (deviceOsType != null) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_OS), SDKUtils.encodeString(deviceOsType));
                } else {
                    z = true;
                }
                String deviceOsVersion = instance.getDeviceOsVersion();
                if (deviceOsVersion != null) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_OS_VERSION), deviceOsVersion.replaceAll("[^0-9/.]", ""));
                } else {
                    z = true;
                }
                String deviceOsVersion2 = instance.getDeviceOsVersion();
                if (deviceOsVersion2 != null) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_OS_VERSION_FULL), SDKUtils.encodeString(deviceOsVersion2));
                }
                String valueOf2 = String.valueOf(instance.getDeviceApiLevel());
                if (valueOf2 != null) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_API_LEVEL), valueOf2);
                } else {
                    z = true;
                }
                String supersonicSdkVersion = DeviceProperties.getSupersonicSdkVersion();
                if (supersonicSdkVersion != null) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.SDK_VERSION), SDKUtils.encodeString(supersonicSdkVersion));
                }
                if (instance.getDeviceCarrier() != null && instance.getDeviceCarrier().length() > 0) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.MOBILE_CARRIER), SDKUtils.encodeString(instance.getDeviceCarrier()));
                }
                String connectionType = ConnectivityService.getConnectionType(context);
                if (!TextUtils.isEmpty(connectionType)) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.CONNECTION_TYPE), SDKUtils.encodeString(connectionType));
                } else {
                    z = true;
                }
                String language = context.getResources().getConfiguration().locale.getLanguage();
                if (!TextUtils.isEmpty(language)) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_LANGUAGE), SDKUtils.encodeString(language.toUpperCase()));
                }
                if (SDKUtils.isExternalStorageAvailable()) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DISK_FREE_SIZE), SDKUtils.encodeString(String.valueOf(DeviceStatus.getAvailableMemorySizeInMegaBytes(this.mCacheDirectory))));
                } else {
                    z = true;
                }
                String valueOf3 = String.valueOf(DeviceStatus.getDeviceWidth());
                if (!TextUtils.isEmpty(valueOf3)) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_SCREEN_SIZE) + Constants.RequestParameters.LEFT_BRACKETS + SDKUtils.encodeString("width") + Constants.RequestParameters.RIGHT_BRACKETS, SDKUtils.encodeString(valueOf3));
                } else {
                    z = true;
                }
                String valueOf4 = String.valueOf(DeviceStatus.getDeviceHeight());
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_SCREEN_SIZE) + Constants.RequestParameters.LEFT_BRACKETS + SDKUtils.encodeString("height") + Constants.RequestParameters.RIGHT_BRACKETS, SDKUtils.encodeString(valueOf4));
                String packageName = ApplicationContext.getPackageName(getContext());
                if (!TextUtils.isEmpty(packageName)) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.PACKAGE_NAME), SDKUtils.encodeString(packageName));
                }
                String valueOf5 = String.valueOf(DeviceStatus.getDeviceDensity());
                if (!TextUtils.isEmpty(valueOf5)) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_SCREEN_SCALE), SDKUtils.encodeString(valueOf5));
                }
                String valueOf6 = String.valueOf(DeviceStatus.isRootedDevice());
                if (!TextUtils.isEmpty(valueOf6)) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.IS_ROOT_DEVICE), SDKUtils.encodeString(valueOf6));
                }
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.DEVICE_VOLUME), (double) DeviceProperties.getInstance(context).getDeviceVolume(context));
                Context currentActivityContext = getCurrentActivityContext();
                if (Build.VERSION.SDK_INT >= 19 && (currentActivityContext instanceof Activity)) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.IMMERSIVE), DeviceStatus.isImmersiveSupported((Activity) currentActivityContext));
                }
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.BATTERY_LEVEL), DeviceStatus.getBatteryLevel(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.NETWORK_MCC), ConnectivityService.getNetworkMCC(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.NETWORK_MNC), ConnectivityService.getNetworkMNC(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.PHONE_TYPE), ConnectivityService.getPhoneType(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.SIM_OPERATOR), SDKUtils.encodeString(ConnectivityService.getSimOperator(currentActivityContext)));
                jSONObject.put(SDKUtils.encodeString("lastUpdateTime"), ApplicationContext.getLastUpdateTime(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.FIRST_INSTALL_TIME), ApplicationContext.getFirstInstallTime(currentActivityContext));
                jSONObject.put(SDKUtils.encodeString("appVersion"), SDKUtils.encodeString(ApplicationContext.getApplicationVersionName(currentActivityContext)));
                String installerPackageName = ApplicationContext.getInstallerPackageName(currentActivityContext);
                if (!TextUtils.isEmpty(installerPackageName)) {
                    jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.INSTALLER_PACKAGE_NAME), SDKUtils.encodeString(installerPackageName));
                }
            } catch (JSONException e) {
                e = e;
                e.printStackTrace();
                IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
                ironSourceAsyncHttpRequestTask.execute(Constants.NATIVE_EXCEPTION_BASE_URL + e.getStackTrace()[0].getMethodName());
                return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
            }
        } catch (JSONException e2) {
            e = e2;
            z = false;
            e.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask2 = new IronSourceAsyncHttpRequestTask();
            ironSourceAsyncHttpRequestTask2.execute(Constants.NATIVE_EXCEPTION_BASE_URL + e.getStackTrace()[0].getMethodName());
            return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
        }
        return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
    }

    /* access modifiers changed from: private */
    public Object[] getApplicationParams(String str, String str2) {
        boolean z;
        JSONObject jSONObject = new JSONObject();
        Map<String, String> map = null;
        if (!TextUtils.isEmpty(str)) {
            SSAEnums.ProductType stringProductTypeAsEnum = getStringProductTypeAsEnum(str);
            if (stringProductTypeAsEnum == SSAEnums.ProductType.OfferWall) {
                map = this.mOWExtraParameters;
            } else {
                DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(stringProductTypeAsEnum, str2);
                if (demandSourceById != null) {
                    Map<String, String> extraParams = demandSourceById.getExtraParams();
                    extraParams.put("demandSourceName", demandSourceById.getDemandSourceName());
                    extraParams.put("demandSourceId", demandSourceById.getId());
                    map = extraParams;
                }
            }
            try {
                jSONObject.put(Constants.ParametersKeys.PRODUCT_TYPE, str);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                Map<String, String> initSDKParams = SDKUtils.getInitSDKParams();
                if (initSDKParams != null) {
                    jSONObject = SDKUtils.mergeJSONObjects(jSONObject, new JSONObject(initSDKParams));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            z = false;
        } else {
            z = true;
        }
        if (!TextUtils.isEmpty(this.mUserId)) {
            try {
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.APPLICATION_USER_ID), SDKUtils.encodeString(this.mUserId));
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        } else {
            z = true;
        }
        if (!TextUtils.isEmpty(this.mApplicationKey)) {
            try {
                jSONObject.put(SDKUtils.encodeString(Constants.RequestParameters.APPLICATION_KEY), SDKUtils.encodeString(this.mApplicationKey));
            } catch (JSONException e4) {
                e4.printStackTrace();
            }
        } else {
            z = true;
        }
        if (map != null && !map.isEmpty()) {
            for (Map.Entry entry : map.entrySet()) {
                if (((String) entry.getKey()).equalsIgnoreCase("sdkWebViewCache")) {
                    setWebviewCache((String) entry.getValue());
                }
                try {
                    jSONObject.put(SDKUtils.encodeString((String) entry.getKey()), SDKUtils.encodeString((String) entry.getValue()));
                } catch (JSONException e5) {
                    e5.printStackTrace();
                }
            }
        }
        return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    /* access modifiers changed from: private */
    public Object[] getAppsStatus(String str, String str2) {
        boolean z;
        boolean z2;
        JSONObject jSONObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(str)) {
                if (!str.equalsIgnoreCase("null")) {
                    if (TextUtils.isEmpty(str2) || str2.equalsIgnoreCase("null")) {
                        jSONObject.put("error", "requestId is null or empty");
                        z = true;
                        return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
                    }
                    List<ApplicationInfo> installedApplications = DeviceStatus.getInstalledApplications(getContext());
                    JSONArray jSONArray = new JSONArray(str);
                    JSONObject jSONObject2 = new JSONObject();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        String trim = jSONArray.getString(i).trim();
                        if (!TextUtils.isEmpty(trim)) {
                            JSONObject jSONObject3 = new JSONObject();
                            Iterator<ApplicationInfo> it = installedApplications.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (trim.equalsIgnoreCase(it.next().packageName)) {
                                        jSONObject3.put(IS_INSTALLED, true);
                                        jSONObject2.put(trim, jSONObject3);
                                        z2 = true;
                                        break;
                                    }
                                } else {
                                    z2 = false;
                                    break;
                                }
                            }
                            if (!z2) {
                                jSONObject3.put(IS_INSTALLED, false);
                                jSONObject2.put(trim, jSONObject3);
                            }
                        }
                    }
                    jSONObject.put(RESULT, jSONObject2);
                    jSONObject.put(REQUEST_ID, str2);
                    z = false;
                    return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
                }
            }
            jSONObject.put("error", "appIds is null or empty");
        } catch (Exception unused) {
        }
        z = true;
        return new Object[]{jSONObject.toString(), Boolean.valueOf(z)};
    }

    public void onFileDownloadSuccess(SSAFile sSAFile) {
        if (sSAFile.getFile().contains(Constants.MOBILE_CONTROLLER_HTML)) {
            load(1);
        } else {
            assetCached(sSAFile.getFile(), sSAFile.getPath());
        }
    }

    public void onFileDownloadFail(SSAFile sSAFile) {
        if (sSAFile.getFile().contains(Constants.MOBILE_CONTROLLER_HTML)) {
            this.mGlobalControllerTimer.cancel();
            for (DemandSource next : this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.RewardedVideo)) {
                if (next.getDemandSourceInitState() == 1) {
                    sendProductErrorMessage(SSAEnums.ProductType.RewardedVideo, next);
                }
            }
            for (DemandSource next2 : this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.Interstitial)) {
                if (next2.getDemandSourceInitState() == 1) {
                    sendProductErrorMessage(SSAEnums.ProductType.Interstitial, next2);
                }
            }
            for (DemandSource next3 : this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.Banner)) {
                if (next3.getDemandSourceInitState() == 1) {
                    sendProductErrorMessage(SSAEnums.ProductType.Banner, next3);
                }
            }
            if (this.mOWmiss) {
                sendProductErrorMessage(SSAEnums.ProductType.OfferWall, null);
            }
            if (this.mOWCreditsMiss) {
                sendProductErrorMessage(SSAEnums.ProductType.OfferWallCredits, null);
                return;
            }
            return;
        }
        assetCachedFailed(sSAFile.getFile(), sSAFile.getPath(), sSAFile.getErrMsg());
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        String str5 = this.TAG;
        Logger.i(str5, str + " " + str4);
    }

    /* access modifiers changed from: private */
    public void toastingErrMsg(final String str, String str2) {
        final String string = new SSAObj(str2).getString("errMsg");
        if (!TextUtils.isEmpty(string)) {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (IronSourceWebView.this.getDebugMode() == SSAEnums.DebugMode.MODE_3.getValue()) {
                        Context currentActivityContext = IronSourceWebView.this.getCurrentActivityContext();
                        Toast.makeText(currentActivityContext, str + " : " + string, 1).show();
                    }
                }
            });
        }
    }

    public void setControllerKeyPressed(String str) {
        this.mControllerKeyPressed = str;
    }

    public String getControllerKeyPressed() {
        String str = this.mControllerKeyPressed;
        setControllerKeyPressed("interrupt");
        return str;
    }

    public void deviceStatusChanged(String str) {
        injectJavascript(generateJSToInject(Constants.JSMethods.DEVICE_STATUS_CHANGED, parseToJson(Constants.RequestParameters.CONNECTION_TYPE, str, null, null, null, null, null, null, null, false)));
    }

    public void engageEnd(String str) {
        if (str.equals(Constants.ParametersKeys.FORCE_CLOSE)) {
            closeWebView();
        }
        injectJavascript(generateJSToInject(Constants.JSMethods.ENGAGE_END, parseToJson("action", str, null, null, null, null, null, null, null, false)));
    }

    public void registerConnectionReceiver(Context context) {
        try {
            context.registerReceiver(this.mConnectionReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unregisterConnectionReceiver(Context context) {
        try {
            context.unregisterReceiver(this.mConnectionReceiver);
        } catch (IllegalArgumentException unused) {
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "unregisterConnectionReceiver - " + e);
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            ironSourceAsyncHttpRequestTask.execute(Constants.NATIVE_EXCEPTION_BASE_URL + e.getStackTrace()[0].getMethodName());
        }
    }

    public void pause() {
        if (Build.VERSION.SDK_INT > 10) {
            try {
                onPause();
            } catch (Throwable th) {
                String str = this.TAG;
                Logger.i(str, "WebViewController: pause() - " + th);
                new IronSourceAsyncHttpRequestTask().execute("https://www.supersonicads.com/mobile/sdk5/log?method=webviewPause");
            }
        }
    }

    public void resume() {
        if (Build.VERSION.SDK_INT > 10) {
            try {
                onResume();
            } catch (Throwable th) {
                String str = this.TAG;
                Logger.i(str, "WebViewController: onResume() - " + th);
                new IronSourceAsyncHttpRequestTask().execute("https://www.supersonicads.com/mobile/sdk5/log?method=webviewResume");
            }
        }
    }

    public void setOnWebViewControllerChangeListener(OnWebViewChangeListener onWebViewChangeListener) {
        this.mChangeListener = onWebViewChangeListener;
    }

    public FrameLayout getLayout() {
        return this.mControllerLayout;
    }

    public boolean inCustomView() {
        return this.mCustomView != null;
    }

    public void hideCustomView() {
        this.mWebChromeClient.onHideCustomView();
    }

    private void setWebviewCache(String str) {
        if (str.equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
            getSettings().setCacheMode(2);
        } else {
            getSettings().setCacheMode(-1);
        }
    }

    public boolean handleSearchKeysURLs(String str) {
        List<String> searchKeys = IronSourceSharedPrefHelper.getSupersonicPrefHelper().getSearchKeys();
        if (searchKeys == null) {
            return false;
        }
        try {
            if (searchKeys.isEmpty()) {
                return false;
            }
            for (String contains : searchKeys) {
                if (str.contains(contains)) {
                    UrlHandler.openUrl(getCurrentActivityContext(), str);
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setState(State state) {
        this.mState = state;
    }

    public State getState() {
        return this.mState;
    }

    /* access modifiers changed from: private */
    public void sendProductErrorMessage(SSAEnums.ProductType productType, DemandSource demandSource) {
        triggerOnControllerInitProductFail(SDKUtils.createErrorMessage(getErrorCodeByProductType(productType), Constants.ErrorCodes.InitiatingController), productType, demandSource);
    }

    /* renamed from: com.ironsource.sdk.controller.IronSourceWebView$14  reason: invalid class name */
    static /* synthetic */ class AnonymousClass14 {
        static final /* synthetic */ int[] $SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType = new int[SSAEnums.ProductType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.ironsource.sdk.data.SSAEnums$ProductType[] r0 = com.ironsource.sdk.data.SSAEnums.ProductType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.ironsource.sdk.controller.IronSourceWebView.AnonymousClass14.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType = r0
                int[] r0 = com.ironsource.sdk.controller.IronSourceWebView.AnonymousClass14.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums.ProductType.RewardedVideo     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.ironsource.sdk.controller.IronSourceWebView.AnonymousClass14.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums.ProductType.Interstitial     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.ironsource.sdk.controller.IronSourceWebView.AnonymousClass14.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums.ProductType.OfferWall     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.ironsource.sdk.controller.IronSourceWebView.AnonymousClass14.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums.ProductType.OfferWallCredits     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.ironsource.sdk.controller.IronSourceWebView.AnonymousClass14.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums.ProductType.Banner     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.IronSourceWebView.AnonymousClass14.<clinit>():void");
        }
    }

    private String getErrorCodeByProductType(SSAEnums.ProductType productType) {
        int i = AnonymousClass14.$SwitchMap$com$ironsource$sdk$data$SSAEnums$ProductType[productType.ordinal()];
        if (i == 1) {
            return Constants.ErrorCodes.InitRV;
        }
        if (i == 2) {
            return Constants.ErrorCodes.InitIS;
        }
        if (i == 3) {
            return Constants.ErrorCodes.InitOW;
        }
        if (i != 4) {
            return i != 5 ? "" : Constants.ErrorCodes.InitBN;
        }
        return Constants.ErrorCodes.ShowOWCredits;
    }

    public void destroy() {
        super.destroy();
        DownloadManager downloadManager2 = this.downloadManager;
        if (downloadManager2 != null) {
            downloadManager2.release();
        }
        if (this.mConnectionReceiver != null) {
            this.mConnectionReceiver = null;
        }
        this.mUiHandler = null;
        this.mCurrentActivityContext = null;
    }

    private String generateJSToInject(String str) {
        return "SSA_CORE.SDKController.runFunction('" + str + "');";
    }

    /* access modifiers changed from: private */
    public String generateJSToInject(String str, String str2) {
        return "SSA_CORE.SDKController.runFunction('" + str + "?parameters=" + str2 + "');";
    }

    private String generateJSToInject(String str, String str2, String str3) {
        return "SSA_CORE.SDKController.runFunction('" + str + "','" + str2 + "','" + str3 + "');";
    }

    /* access modifiers changed from: private */
    public String generateJSToInject(String str, String str2, String str3, String str4) {
        return "SSA_CORE.SDKController.runFunction('" + str + "?parameters=" + str2 + "','" + str3 + "','" + str4 + "');";
    }

    public AdUnitsState getSavedState() {
        return this.mSavedState;
    }

    public void restoreState(AdUnitsState adUnitsState) {
        synchronized (this.mSavedStateLocker) {
            if (adUnitsState.shouldRestore() && this.mControllerState.equals(SSAEnums.ControllerState.Ready)) {
                String str = this.TAG;
                Log.d(str, "restoreState(state:" + adUnitsState + ")");
                int displayedProduct = adUnitsState.getDisplayedProduct();
                if (displayedProduct != -1) {
                    if (displayedProduct == SSAEnums.ProductType.RewardedVideo.ordinal()) {
                        Log.d(this.TAG, "onRVAdClosed()");
                        SSAEnums.ProductType productType = SSAEnums.ProductType.RewardedVideo;
                        String displayedDemandSourceId = adUnitsState.getDisplayedDemandSourceId();
                        DSAdProductListener adProductListenerByProductType = getAdProductListenerByProductType(productType);
                        if (adProductListenerByProductType != null && !TextUtils.isEmpty(displayedDemandSourceId)) {
                            adProductListenerByProductType.onAdProductClose(productType, displayedDemandSourceId);
                        }
                    } else if (displayedProduct == SSAEnums.ProductType.Interstitial.ordinal()) {
                        Log.d(this.TAG, "onInterstitialAdClosed()");
                        SSAEnums.ProductType productType2 = SSAEnums.ProductType.Interstitial;
                        String displayedDemandSourceId2 = adUnitsState.getDisplayedDemandSourceId();
                        DSAdProductListener adProductListenerByProductType2 = getAdProductListenerByProductType(productType2);
                        if (adProductListenerByProductType2 != null && !TextUtils.isEmpty(displayedDemandSourceId2)) {
                            adProductListenerByProductType2.onAdProductClose(productType2, displayedDemandSourceId2);
                        }
                    } else if (displayedProduct == SSAEnums.ProductType.OfferWall.ordinal()) {
                        Log.d(this.TAG, "onOWAdClosed()");
                        if (this.mOnOfferWallListener != null) {
                            this.mOnOfferWallListener.onOWAdClosed();
                        }
                    }
                    adUnitsState.adOpened(-1);
                    adUnitsState.setDisplayedDemandSourceId(null);
                } else {
                    Log.d(this.TAG, "No ad was opened");
                }
                String interstitialAppKey = adUnitsState.getInterstitialAppKey();
                String interstitialUserId = adUnitsState.getInterstitialUserId();
                for (DemandSource next : this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.Interstitial)) {
                    if (next.getDemandSourceInitState() == 2) {
                        String str2 = this.TAG;
                        Log.d(str2, "initInterstitial(appKey:" + interstitialAppKey + ", userId:" + interstitialUserId + ", demandSource:" + next.getDemandSourceName() + ")");
                        initInterstitial(interstitialAppKey, interstitialUserId, next, this.mDSInterstitialListener);
                    }
                }
                String rVAppKey = adUnitsState.getRVAppKey();
                String rVUserId = adUnitsState.getRVUserId();
                for (DemandSource next2 : this.mDemandSourceManager.getDemandSources(SSAEnums.ProductType.RewardedVideo)) {
                    if (next2.getDemandSourceInitState() == 2) {
                        String demandSourceName = next2.getDemandSourceName();
                        Log.d(this.TAG, "onRVNoMoreOffers()");
                        this.mDSRewardedVideoListener.onRVNoMoreOffers(demandSourceName);
                        String str3 = this.TAG;
                        Log.d(str3, "initRewardedVideo(appKey:" + rVAppKey + ", userId:" + rVUserId + ", demandSource:" + demandSourceName + ")");
                        initRewardedVideo(rVAppKey, rVUserId, next2, this.mDSRewardedVideoListener);
                    }
                }
                adUnitsState.setShouldRestore(false);
            }
            this.mSavedState = adUnitsState;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (!this.mChangeListener.onBackButtonPressed()) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void runOnUiThread(Runnable runnable) {
        this.mUiHandler.post(runnable);
    }
}
