package com.ironsource.sdk.controller;

import android.app.Application;
import android.webkit.WebView;
import com.ironsource.sdk.analytics.moat.MOATManager;
import com.ironsource.sdk.controller.IronSourceWebView;
import org.json.JSONException;
import org.json.JSONObject;

public class MOATJSAdapter {
    private static final String createAdTracker = "createAdTracker";
    private static final String fail = "fail";
    private static final String initWithOptions = "initWithOptions";
    private static final String moatFunction = "moatFunction";
    private static final String moatParams = "moatParams";
    private static final String startTracking = "startTracking";
    private static final String stopTracking = "stopTracking";
    private static final String success = "success";
    private Application mApplication;

    public MOATJSAdapter(Application application) {
        this.mApplication = application;
    }

    private static class FunctionCall {
        String failCallback;
        String name;
        JSONObject params;
        String successCallback;

        private FunctionCall() {
        }
    }

    /* access modifiers changed from: package-private */
    public void call(String str, IronSourceWebView.NativeAPI.JSCallbackTask jSCallbackTask, WebView webView) throws Exception {
        FunctionCall fetchFunctionCall = fetchFunctionCall(str);
        if (initWithOptions.equals(fetchFunctionCall.name)) {
            MOATManager.initWithOptions(fetchFunctionCall.params, this.mApplication);
        } else if (createAdTracker.equals(fetchFunctionCall.name) && webView != null) {
            MOATManager.createAdTracker(webView);
        } else if (startTracking.equals(fetchFunctionCall.name)) {
            MOATManager.setEventListener(createEventListener(jSCallbackTask, fetchFunctionCall.successCallback, fetchFunctionCall.failCallback));
            MOATManager.startTracking();
        } else if (stopTracking.equals(fetchFunctionCall.name)) {
            MOATManager.setEventListener(createEventListener(jSCallbackTask, fetchFunctionCall.successCallback, fetchFunctionCall.failCallback));
            MOATManager.stopTracking();
        }
    }

    private MOATManager.EventsListener createEventListener(final IronSourceWebView.NativeAPI.JSCallbackTask jSCallbackTask, final String str, final String str2) {
        return new MOATManager.EventsListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, java.lang.String):void
             arg types: [int, java.lang.String, java.lang.String]
             candidates:
              com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, com.ironsource.sdk.data.SSAObj):void
              com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, org.json.JSONObject):void
              com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, java.lang.String):void */
            public void onTrackingStarted(String str) {
                IronSourceWebView.NativeAPI.JSCallbackTask jSCallbackTask = jSCallbackTask;
                if (jSCallbackTask != null) {
                    jSCallbackTask.sendMessage(true, str, str);
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, java.lang.String):void
             arg types: [int, java.lang.String, java.lang.String]
             candidates:
              com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, com.ironsource.sdk.data.SSAObj):void
              com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, org.json.JSONObject):void
              com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, java.lang.String):void */
            public void onTrackingFailedToStart(String str) {
                IronSourceWebView.NativeAPI.JSCallbackTask jSCallbackTask = jSCallbackTask;
                if (jSCallbackTask != null) {
                    jSCallbackTask.sendMessage(false, str2, str);
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, java.lang.String):void
             arg types: [int, java.lang.String, java.lang.String]
             candidates:
              com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, com.ironsource.sdk.data.SSAObj):void
              com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, org.json.JSONObject):void
              com.ironsource.sdk.controller.IronSourceWebView.NativeAPI.JSCallbackTask.sendMessage(boolean, java.lang.String, java.lang.String):void */
            public void onTrackingStopped(String str) {
                IronSourceWebView.NativeAPI.JSCallbackTask jSCallbackTask = jSCallbackTask;
                if (jSCallbackTask != null) {
                    jSCallbackTask.sendMessage(true, str, str);
                }
            }
        };
    }

    private FunctionCall fetchFunctionCall(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        FunctionCall functionCall = new FunctionCall();
        functionCall.name = jSONObject.optString(moatFunction);
        functionCall.params = jSONObject.optJSONObject(moatParams);
        functionCall.successCallback = jSONObject.optString("success");
        functionCall.failCallback = jSONObject.optString("fail");
        return functionCall;
    }
}
