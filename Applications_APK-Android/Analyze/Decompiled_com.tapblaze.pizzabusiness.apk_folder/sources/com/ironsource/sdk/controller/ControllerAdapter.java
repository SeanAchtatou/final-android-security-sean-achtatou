package com.ironsource.sdk.controller;

import android.os.Build;
import android.webkit.JavascriptInterface;
import com.ironsource.sdk.controller.IronSourceWebView;
import com.ironsource.sdk.utils.Logger;
import java.lang.reflect.Method;
import java.security.AccessControlException;

class ControllerAdapter {
    private static final String TAG = ControllerAdapter.class.getSimpleName();
    private final IronSourceWebView.NativeAPI mNativeAPI;

    ControllerAdapter(IronSourceWebView.NativeAPI nativeAPI) {
        this.mNativeAPI = nativeAPI;
    }

    /* access modifiers changed from: package-private */
    public synchronized void call(String str, String str2) throws Exception {
        if (this.mNativeAPI == null) {
            Logger.e(TAG, "!!! nativeAPI == null !!!");
            return;
        }
        Method declaredMethod = IronSourceWebView.NativeAPI.class.getDeclaredMethod(str, String.class);
        if (Build.VERSION.SDK_INT >= 17) {
            if (!declaredMethod.isAnnotationPresent(JavascriptInterface.class)) {
                throw new AccessControlException("Trying to access a private function: " + str);
            }
        }
        declaredMethod.invoke(this.mNativeAPI, str2);
    }

    /* access modifiers changed from: package-private */
    public void sendUnauthorizedError(String str) {
        IronSourceWebView.NativeAPI nativeAPI = this.mNativeAPI;
        if (nativeAPI != null) {
            nativeAPI.sendUnauthorizedError(str);
        }
    }
}
