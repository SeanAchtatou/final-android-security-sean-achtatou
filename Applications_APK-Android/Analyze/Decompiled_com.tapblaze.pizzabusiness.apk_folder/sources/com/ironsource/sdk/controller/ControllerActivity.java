package com.ironsource.sdk.controller;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import androidx.core.view.InputDeviceCompat;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.agent.IronSourceAdsPublisherAgent;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.controller.IronSourceWebView;
import com.ironsource.sdk.data.AdUnitsState;
import com.ironsource.sdk.data.SSAEnums;
import com.ironsource.sdk.handlers.BackButtonHandler;
import com.ironsource.sdk.listeners.OnWebViewChangeListener;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;

public class ControllerActivity extends Activity implements OnWebViewChangeListener, VideoEventsListener {
    private static final String TAG = ControllerActivity.class.getSimpleName();
    private static final int WEB_VIEW_VIEW_ID = 1;
    final RelativeLayout.LayoutParams MATCH_PARENT_LAYOUT_PARAMS = new RelativeLayout.LayoutParams(-1, -1);
    private boolean calledFromOnCreate = false;
    public int currentRequestedRotation = -1;
    /* access modifiers changed from: private */
    public final Runnable decorViewSettings = new Runnable() {
        public void run() {
            ControllerActivity.this.getWindow().getDecorView().setSystemUiVisibility(SDKUtils.getActivityUIFlags(ControllerActivity.this.mIsImmersive));
        }
    };
    private RelativeLayout mContainer;
    private boolean mControllerClearedFromOnPause;
    /* access modifiers changed from: private */
    public boolean mIsImmersive = false;
    private String mProductType;
    private AdUnitsState mState;
    /* access modifiers changed from: private */
    public Handler mUiThreadHandler = new Handler();
    private IronSourceWebView mWebViewController;
    private FrameLayout mWebViewFrameContainer;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            Logger.i(TAG, "onCreate");
            hideActivityTitle();
            hideActivtiyStatusBar();
            this.mWebViewController = IronSourceAdsPublisherAgent.getInstance(this).getWebViewController();
            this.mWebViewController.setId(1);
            this.mWebViewController.setOnWebViewControllerChangeListener(this);
            this.mWebViewController.setVideoEventsListener(this);
            Intent intent = getIntent();
            this.mProductType = intent.getStringExtra(Constants.ParametersKeys.PRODUCT_TYPE);
            this.mIsImmersive = intent.getBooleanExtra(Constants.ParametersKeys.IMMERSIVE, false);
            this.mControllerClearedFromOnPause = false;
            if (this.mIsImmersive) {
                getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                    public void onSystemUiVisibilityChange(int i) {
                        if ((i & InputDeviceCompat.SOURCE_TOUCHSCREEN) == 0) {
                            ControllerActivity.this.mUiThreadHandler.removeCallbacks(ControllerActivity.this.decorViewSettings);
                            ControllerActivity.this.mUiThreadHandler.postDelayed(ControllerActivity.this.decorViewSettings, 500);
                        }
                    }
                });
                runOnUiThread(this.decorViewSettings);
            }
            if (!TextUtils.isEmpty(this.mProductType) && SSAEnums.ProductType.OfferWall.toString().equalsIgnoreCase(this.mProductType)) {
                if (bundle != null) {
                    AdUnitsState adUnitsState = (AdUnitsState) bundle.getParcelable("state");
                    if (adUnitsState != null) {
                        this.mState = adUnitsState;
                        this.mWebViewController.restoreState(adUnitsState);
                    }
                    finish();
                } else {
                    this.mState = this.mWebViewController.getSavedState();
                }
            }
            this.mContainer = new RelativeLayout(this);
            setContentView(this.mContainer, this.MATCH_PARENT_LAYOUT_PARAMS);
            this.mWebViewFrameContainer = this.mWebViewController.getLayout();
            if (this.mContainer.findViewById(1) == null && this.mWebViewFrameContainer.getParent() != null) {
                this.calledFromOnCreate = true;
                finish();
            }
            initOrientationState();
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    private void initOrientationState() {
        Intent intent = getIntent();
        handleOrientationState(intent.getStringExtra(Constants.ParametersKeys.ORIENTATION_SET_FLAG), intent.getIntExtra(Constants.ParametersKeys.ROTATION_SET_FLAG, 0));
    }

    private void handleOrientationState(String str, int i) {
        if (str == null) {
            return;
        }
        if (Constants.ParametersKeys.ORIENTATION_LANDSCAPE.equalsIgnoreCase(str)) {
            setInitiateLandscapeOrientation();
        } else if (Constants.ParametersKeys.ORIENTATION_PORTRAIT.equalsIgnoreCase(str)) {
            setInitiatePortraitOrientation();
        } else if (Constants.ParametersKeys.ORIENTATION_DEVICE.equalsIgnoreCase(str)) {
            if (DeviceStatus.isDeviceOrientationLocked(this)) {
                setRequestedOrientation(1);
            }
        } else if (getRequestedOrientation() == -1) {
            setRequestedOrientation(4);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (!TextUtils.isEmpty(this.mProductType) && SSAEnums.ProductType.OfferWall.toString().equalsIgnoreCase(this.mProductType)) {
            this.mState.setShouldRestore(true);
            bundle.putParcelable("state", this.mState);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Logger.i(TAG, "onResume");
        this.mContainer.addView(this.mWebViewFrameContainer, this.MATCH_PARENT_LAYOUT_PARAMS);
        IronSourceWebView ironSourceWebView = this.mWebViewController;
        if (ironSourceWebView != null) {
            ironSourceWebView.registerConnectionReceiver(this);
            this.mWebViewController.resume();
            this.mWebViewController.viewableChange(true, Constants.ParametersKeys.MAIN);
        }
        ((AudioManager) getSystemService("audio")).requestAudioFocus(null, 3, 2);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Logger.i(TAG, "onPause");
        ((AudioManager) getSystemService("audio")).abandonAudioFocus(null);
        IronSourceWebView ironSourceWebView = this.mWebViewController;
        if (ironSourceWebView != null) {
            ironSourceWebView.unregisterConnectionReceiver(this);
            this.mWebViewController.pause();
            this.mWebViewController.viewableChange(false, Constants.ParametersKeys.MAIN);
        }
        removeWebViewContainerView();
        if (isFinishing()) {
            this.mControllerClearedFromOnPause = true;
            Logger.i(TAG, "onPause | isFinishing");
            clearWebviewController();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Logger.i(TAG, "onDestroy");
        if (this.calledFromOnCreate) {
            removeWebViewContainerView();
        }
        if (!this.mControllerClearedFromOnPause) {
            Logger.i(TAG, "onDestroy | destroyedFromBackground");
            clearWebviewController();
        }
    }

    private void clearWebviewController() {
        if (this.mWebViewController != null) {
            Logger.i(TAG, "clearWebviewController");
            this.mWebViewController.setState(IronSourceWebView.State.Gone);
            this.mWebViewController.removeVideoEventsListener();
            this.mWebViewController.notifyLifeCycle(this.mProductType, "onDestroy");
        }
    }

    private void removeWebViewContainerView() {
        if (this.mContainer != null) {
            ViewGroup viewGroup = (ViewGroup) this.mWebViewFrameContainer.getParent();
            if (viewGroup.findViewById(1) != null) {
                viewGroup.removeView(this.mWebViewFrameContainer);
            }
        }
    }

    public void onCloseRequested() {
        finish();
    }

    public void onOrientationChanged(String str, int i) {
        handleOrientationState(str, i);
    }

    public boolean onBackButtonPressed() {
        onBackPressed();
        return true;
    }

    public void onBackPressed() {
        Logger.i(TAG, "onBackPressed");
        if (!BackButtonHandler.getInstance().handleBackButton(this)) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onUserLeaveHint() {
        super.onUserLeaveHint();
        Logger.i(TAG, "onUserLeaveHint");
    }

    private void hideActivityTitle() {
        requestWindowFeature(1);
    }

    private void hideActivtiyStatusBar() {
        getWindow().setFlags(1024, 1024);
    }

    private void keepScreenOn() {
        runOnUiThread(new Runnable() {
            public void run() {
                ControllerActivity.this.getWindow().addFlags(128);
            }
        });
    }

    private void cancelScreenOn() {
        runOnUiThread(new Runnable() {
            public void run() {
                ControllerActivity.this.getWindow().clearFlags(128);
            }
        });
    }

    private void setInitiateLandscapeOrientation() {
        int applicationRotation = DeviceStatus.getApplicationRotation(this);
        Logger.i(TAG, "setInitiateLandscapeOrientation");
        if (applicationRotation == 0) {
            Logger.i(TAG, "ROTATION_0");
            setRequestedOrientation(0);
        } else if (applicationRotation == 2) {
            Logger.i(TAG, "ROTATION_180");
            setRequestedOrientation(8);
        } else if (applicationRotation == 3) {
            Logger.i(TAG, "ROTATION_270 Right Landscape");
            setRequestedOrientation(8);
        } else if (applicationRotation == 1) {
            Logger.i(TAG, "ROTATION_90 Left Landscape");
            setRequestedOrientation(0);
        } else {
            Logger.i(TAG, "No Rotation");
        }
    }

    private void setInitiatePortraitOrientation() {
        int applicationRotation = DeviceStatus.getApplicationRotation(this);
        Logger.i(TAG, "setInitiatePortraitOrientation");
        if (applicationRotation == 0) {
            Logger.i(TAG, "ROTATION_0");
            setRequestedOrientation(1);
        } else if (applicationRotation == 2) {
            Logger.i(TAG, "ROTATION_180");
            setRequestedOrientation(9);
        } else if (applicationRotation == 1) {
            Logger.i(TAG, "ROTATION_270 Right Landscape");
            setRequestedOrientation(1);
        } else if (applicationRotation == 3) {
            Logger.i(TAG, "ROTATION_90 Left Landscape");
            setRequestedOrientation(1);
        } else {
            Logger.i(TAG, "No Rotation");
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !this.mWebViewController.inCustomView()) {
            if (this.mIsImmersive && (i == 25 || i == 24)) {
                this.mUiThreadHandler.removeCallbacks(this.decorViewSettings);
                this.mUiThreadHandler.postDelayed(this.decorViewSettings, 500);
            }
            return super.onKeyDown(i, keyEvent);
        }
        this.mWebViewController.hideCustomView();
        return true;
    }

    public void setRequestedOrientation(int i) {
        if (this.currentRequestedRotation != i) {
            String str = TAG;
            Logger.i(str, "Rotation: Req = " + i + " Curr = " + this.currentRequestedRotation);
            this.currentRequestedRotation = i;
            super.setRequestedOrientation(i);
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.mIsImmersive && z) {
            runOnUiThread(this.decorViewSettings);
        }
    }

    public void onVideoStarted() {
        toggleKeepScreen(true);
    }

    public void onVideoPaused() {
        toggleKeepScreen(false);
    }

    public void onVideoResumed() {
        toggleKeepScreen(true);
    }

    public void onVideoEnded() {
        toggleKeepScreen(false);
    }

    public void onVideoStopped() {
        toggleKeepScreen(false);
    }

    public void toggleKeepScreen(boolean z) {
        if (z) {
            keepScreenOn();
        } else {
            cancelScreenOn();
        }
    }
}
