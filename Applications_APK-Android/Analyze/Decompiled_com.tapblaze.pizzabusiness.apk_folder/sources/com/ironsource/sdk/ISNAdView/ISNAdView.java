package com.ironsource.sdk.ISNAdView;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.ironsource.sdk.ISAdSize;
import com.ironsource.sdk.IronSourceNetwork;
import com.ironsource.sdk.SSAFactory;
import org.json.JSONObject;

public class ISNAdView extends FrameLayout {
    /* access modifiers changed from: private */
    public String TAG = ISNAdView.class.getSimpleName();
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public ISAdSize mAdViewSize;
    /* access modifiers changed from: private */
    public String mContainerIdentifier;
    /* access modifiers changed from: private */
    public ISNAdViewLogic mIsnAdViewLogic;
    /* access modifiers changed from: private */
    public WebView mWebView;

    interface IErrorReportDelegate {
        void reportOnError(String str);
    }

    public ISNAdView(Activity activity, String str, ISAdSize iSAdSize) {
        super(activity);
        this.mActivity = activity;
        this.mAdViewSize = iSAdSize;
        this.mContainerIdentifier = str;
        this.mIsnAdViewLogic = new ISNAdViewLogic();
    }

    public void setControllerDelegate(ISNAdViewDelegate iSNAdViewDelegate) {
        this.mIsnAdViewLogic.setControllerDelegate(iSNAdViewDelegate);
    }

    public void loadAd(JSONObject jSONObject) throws Exception {
        try {
            try {
                SSAFactory.getPublisherInstance(this.mActivity).loadBanner(this.mIsnAdViewLogic.buildDataForLoadingAd(jSONObject, this.mContainerIdentifier));
            } catch (Exception unused) {
                throw new Exception("ISNAdView | Failed to instantiate IronSourceAdsPublisherAgent");
            }
        } catch (Exception unused2) {
            throw new Exception("ISNAdView | loadAd | Failed to build load parameters");
        }
    }

    public void load(JSONObject jSONObject) throws Exception {
        try {
            try {
                IronSourceNetwork.loadBanner(this.mIsnAdViewLogic.buildDataForLoadingAd(jSONObject, this.mContainerIdentifier));
            } catch (Exception unused) {
                throw new Exception("ISNAdView | Failed to instantiate IronSourceAdsPublisherAgent");
            }
        } catch (Exception unused2) {
            throw new Exception("ISNAdView | loadAd | Failed to build load parameters");
        }
    }

    public void performCleanup() {
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    ISNAdView.this.mIsnAdViewLogic.reportAdContainerWasRemoved();
                    ISNAdView.this.removeView(ISNAdView.this.mWebView);
                    if (ISNAdView.this.mWebView != null) {
                        ISNAdView.this.mWebView.destroy();
                    }
                    Activity unused = ISNAdView.this.mActivity = null;
                    ISAdSize unused2 = ISNAdView.this.mAdViewSize = null;
                    String unused3 = ISNAdView.this.mContainerIdentifier = null;
                    ISNAdView.this.mIsnAdViewLogic.destroy();
                    ISNAdViewLogic unused4 = ISNAdView.this.mIsnAdViewLogic = null;
                } catch (Exception e) {
                    Log.e(ISNAdView.this.TAG, "performCleanup | could not destroy ISNAdView");
                    e.printStackTrace();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        ISNAdViewLogic iSNAdViewLogic = this.mIsnAdViewLogic;
        if (iSNAdViewLogic != null) {
            iSNAdViewLogic.updateViewVisibilityParameters(ISNAdViewConstants.IS_VISIBLE_KEY, i, isShown());
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        ISNAdViewLogic iSNAdViewLogic = this.mIsnAdViewLogic;
        if (iSNAdViewLogic != null) {
            iSNAdViewLogic.updateViewVisibilityParameters(ISNAdViewConstants.IS_WINDOW_VISIBLE_KEY, i, isShown());
        }
    }

    public void loadUrlIntoWebView(final String str, final String str2) {
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (ISNAdView.this.mWebView == null) {
                    ISNAdView.this.createWebView(str2);
                }
                ISNAdView iSNAdView = ISNAdView.this;
                iSNAdView.addView(iSNAdView.mWebView);
                ISNAdView.this.mWebView.loadUrl(str);
            }
        });
    }

    /* access modifiers changed from: private */
    public void createWebView(final String str) {
        this.mWebView = new WebView(this.mActivity);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.addJavascriptInterface(new ISNAdViewWebViewJSInterface(this), ISNAdViewConstants.CONTAINER_MSG_HANDLER);
        this.mWebView.setWebViewClient(new ISNAdViewWebClient(new IErrorReportDelegate() {
            public void reportOnError(String str) {
                ISNAdView.this.mIsnAdViewLogic.sendErrorMessageToController(str, str);
            }
        }));
        this.mWebView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.mIsnAdViewLogic.setAdViewWebView(this.mWebView);
    }

    /* access modifiers changed from: package-private */
    public void receiveMessageFromWebView(String str) {
        this.mIsnAdViewLogic.handleMessageFromWebView(str);
    }

    public void receiveMessageFromController(String str, JSONObject jSONObject, String str2, String str3) {
        try {
            if (str.equalsIgnoreCase(ISNAdViewConstants.LOAD_WITH_URL)) {
                loadUrlIntoWebView(jSONObject.getString(ISNAdViewConstants.URL_FOR_WEBVIEW), str3);
            } else {
                this.mIsnAdViewLogic.handleMessageFromController(str, jSONObject, str2, str3);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ISNAdViewLogic iSNAdViewLogic = this.mIsnAdViewLogic;
            iSNAdViewLogic.sendErrorMessageToController(str3, "Could not handle message from controller: " + str + " with params: " + jSONObject.toString());
        }
    }

    public ISAdSize getAdViewSize() {
        return this.mAdViewSize;
    }
}
