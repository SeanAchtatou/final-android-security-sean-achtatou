package com.ironsource.sdk.precache;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.ironsource.sdk.data.SSAFile;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.IronSourceStorageUtils;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;

public class DownloadManager {
    public static final String CAMPAIGNS = "campaigns";
    public static final String FILE_ALREADY_EXIST = "file_already_exist";
    protected static final String FILE_NOT_FOUND_EXCEPTION = "file not found exception";
    public static final String GLOBAL_ASSETS = "globalAssets";
    protected static final String HTTP_EMPTY_RESPONSE = "http empty response";
    protected static final String HTTP_ERROR_CODE = "http error code";
    protected static final String HTTP_NOT_FOUND = "http not found";
    protected static final String HTTP_OK = "http ok";
    protected static final String IO_EXCEPTION = "io exception";
    protected static final String MALFORMED_URL_EXCEPTION = "malformed url exception";
    static final int MESSAGE_EMPTY_URL = 1007;
    static final int MESSAGE_FILE_DOWNLOAD_FAIL = 1017;
    static final int MESSAGE_FILE_DOWNLOAD_SUCCESS = 1016;
    static final int MESSAGE_FILE_NOT_FOUND_EXCEPTION = 1018;
    static final int MESSAGE_GENERAL_HTTP_ERROR_CODE = 1011;
    static final int MESSAGE_HTTP_EMPTY_RESPONSE = 1006;
    static final int MESSAGE_HTTP_NOT_FOUND = 1005;
    static final int MESSAGE_INIT_BC_FAIL = 1014;
    static final int MESSAGE_IO_EXCEPTION = 1009;
    static final int MESSAGE_MALFORMED_URL_EXCEPTION = 1004;
    static final int MESSAGE_NUM_OF_BANNERS_TO_CACHE = 1013;
    static final int MESSAGE_NUM_OF_BANNERS_TO_INIT_SUCCESS = 1012;
    static final int MESSAGE_OUT_OF_MEMORY_EXCEPTION = 1019;
    static final int MESSAGE_SOCKET_TIMEOUT_EXCEPTION = 1008;
    static final int MESSAGE_TMP_FILE_RENAME_FAILED = 1020;
    static final int MESSAGE_URI_SYNTAX_EXCEPTION = 1010;
    static final int MESSAGE_ZERO_CAMPAIGNS_TO_INIT_SUCCESS = 1015;
    public static final String NO_DISK_SPACE = "no_disk_space";
    public static final String NO_NETWORK_CONNECTION = "no_network_connection";
    public static final int OPERATION_TIMEOUT = 5000;
    protected static final String OUT_OF_MEMORY_EXCEPTION = "out of memory exception";
    public static final String SETTINGS = "settings";
    protected static final String SOCKET_TIMEOUT_EXCEPTION = "socket timeout exception";
    public static final String STORAGE_UNAVAILABLE = "sotrage_unavailable";
    private static final String TAG = "DownloadManager";
    private static final String TEMP_DIR_FOR_FILES = "temp";
    private static final String TEMP_PREFIX_FOR_FILES = "tmp_";
    private static final String UNABLE_TO_CREATE_FOLDER = "unable_to_create_folder";
    protected static final String URI_SYNTAX_EXCEPTION = "uri syntax exception";
    public static final String UTF8_CHARSET = "UTF-8";
    private static DownloadManager mDownloadManager;
    private String mCacheRootDirectory;
    private DownloadHandler mDownloadHandler = getDownloadHandler();
    private Thread mMobileControllerThread;

    public interface OnPreCacheCompletion {
        void onFileDownloadFail(SSAFile sSAFile);

        void onFileDownloadSuccess(SSAFile sSAFile);
    }

    private DownloadManager(String str) {
        this.mCacheRootDirectory = str;
        IronSourceStorageUtils.deleteFolder(this.mCacheRootDirectory, TEMP_DIR_FOR_FILES);
        IronSourceStorageUtils.makeDir(this.mCacheRootDirectory, TEMP_DIR_FOR_FILES);
    }

    public static synchronized DownloadManager getInstance(String str) {
        DownloadManager downloadManager;
        synchronized (DownloadManager.class) {
            if (mDownloadManager == null) {
                mDownloadManager = new DownloadManager(str);
            }
            downloadManager = mDownloadManager;
        }
        return downloadManager;
    }

    static class DownloadHandler extends Handler {
        OnPreCacheCompletion mListener;

        DownloadHandler() {
        }

        /* access modifiers changed from: package-private */
        public void setOnPreCacheCompletion(OnPreCacheCompletion onPreCacheCompletion) {
            if (onPreCacheCompletion != null) {
                this.mListener = onPreCacheCompletion;
                return;
            }
            throw new IllegalArgumentException();
        }

        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 1016) {
                this.mListener.onFileDownloadSuccess((SSAFile) message.obj);
            } else if (i == 1017) {
                this.mListener.onFileDownloadFail((SSAFile) message.obj);
            }
        }

        public void release() {
            this.mListener = null;
        }
    }

    /* access modifiers changed from: package-private */
    public DownloadHandler getDownloadHandler() {
        return new DownloadHandler();
    }

    public void setOnPreCacheCompletion(OnPreCacheCompletion onPreCacheCompletion) {
        this.mDownloadHandler.setOnPreCacheCompletion(onPreCacheCompletion);
    }

    public void release() {
        mDownloadManager = null;
        this.mDownloadHandler.release();
        this.mDownloadHandler = null;
    }

    public void downloadFile(SSAFile sSAFile) {
        new Thread(new SingleFileWorkerThread(sSAFile, this.mDownloadHandler, this.mCacheRootDirectory, getTempFilesDirectory())).start();
    }

    public void downloadMobileControllerFile(SSAFile sSAFile) {
        this.mMobileControllerThread = new Thread(new SingleFileWorkerThread(sSAFile, this.mDownloadHandler, this.mCacheRootDirectory, getTempFilesDirectory()));
        this.mMobileControllerThread.start();
    }

    public boolean isMobileControllerThreadLive() {
        Thread thread = this.mMobileControllerThread;
        return thread != null && thread.isAlive();
    }

    static class SingleFileWorkerThread implements Runnable {
        private String mCacheRootDirectory;
        private long mConnectionRetries = getConnectionRetries();
        Handler mDownloadHandler;
        private String mFile;
        private String mFileName = guessFileName(this.mFile);
        private String mPath;
        private final String mTempFilesDirectory;

        SingleFileWorkerThread(SSAFile sSAFile, Handler handler, String str, String str2) {
            this.mFile = sSAFile.getFile();
            this.mPath = sSAFile.getPath();
            this.mCacheRootDirectory = str;
            this.mDownloadHandler = handler;
            this.mTempFilesDirectory = str2;
        }

        /* access modifiers changed from: package-private */
        public String guessFileName(String str) {
            return SDKUtils.getFileName(this.mFile);
        }

        /* access modifiers changed from: package-private */
        public FileWorkerThread getFileWorkerThread(String str, String str2, String str3, long j, String str4) {
            return new FileWorkerThread(str, str2, str3, j, str4);
        }

        /* access modifiers changed from: package-private */
        public Message getMessage() {
            return new Message();
        }

        /* access modifiers changed from: package-private */
        public String makeDir(String str, String str2) {
            return IronSourceStorageUtils.makeDir(str, str2);
        }

        public void run() {
            SSAFile sSAFile = new SSAFile(this.mFileName, this.mPath);
            Message message = getMessage();
            message.obj = sSAFile;
            String makeDir = makeDir(this.mCacheRootDirectory, this.mPath);
            if (makeDir == null) {
                message.what = 1017;
                sSAFile.setErrMsg(DownloadManager.UNABLE_TO_CREATE_FOLDER);
                this.mDownloadHandler.sendMessage(message);
                return;
            }
            int i = getFileWorkerThread(this.mFile, makeDir, sSAFile.getFile(), this.mConnectionRetries, this.mTempFilesDirectory).call().responseCode;
            if (i != 200) {
                if (!(i == 404 || i == 1018 || i == 1019)) {
                    switch (i) {
                        case 1004:
                        case 1005:
                        case 1006:
                            break;
                        default:
                            switch (i) {
                                case 1008:
                                case 1009:
                                case 1010:
                                case 1011:
                                    break;
                                default:
                                    return;
                            }
                    }
                }
                String convertErrorCodeToMessage = convertErrorCodeToMessage(i);
                message.what = 1017;
                sSAFile.setErrMsg(convertErrorCodeToMessage);
                this.mDownloadHandler.sendMessage(message);
                return;
            }
            message.what = 1016;
            this.mDownloadHandler.sendMessage(message);
        }

        /* access modifiers changed from: package-private */
        public String convertErrorCodeToMessage(int i) {
            String str = "not defined message for " + i;
            if (i != 404) {
                if (i == 1018) {
                    return DownloadManager.FILE_NOT_FOUND_EXCEPTION;
                }
                if (i == 1019) {
                    return DownloadManager.OUT_OF_MEMORY_EXCEPTION;
                }
                switch (i) {
                    case 1004:
                        return DownloadManager.MALFORMED_URL_EXCEPTION;
                    case 1005:
                        break;
                    case 1006:
                        return DownloadManager.HTTP_EMPTY_RESPONSE;
                    default:
                        switch (i) {
                            case 1008:
                                return DownloadManager.SOCKET_TIMEOUT_EXCEPTION;
                            case 1009:
                                return DownloadManager.IO_EXCEPTION;
                            case 1010:
                                return DownloadManager.URI_SYNTAX_EXCEPTION;
                            case 1011:
                                return DownloadManager.HTTP_ERROR_CODE;
                            default:
                                return str;
                        }
                }
            }
            return DownloadManager.HTTP_NOT_FOUND;
        }

        public long getConnectionRetries() {
            return Long.parseLong(IronSourceSharedPrefHelper.getSupersonicPrefHelper().getConnectionRetries());
        }
    }

    static class FileWorkerThread implements Callable<Result> {
        private long mConnectionRetries;
        private String mDirectory;
        private String mFileName;
        private String mFileUrl;
        private String mTmpFilesDirectory;

        public FileWorkerThread(String str, String str2, String str3, long j, String str4) {
            this.mFileUrl = str;
            this.mDirectory = str2;
            this.mFileName = str3;
            this.mConnectionRetries = j;
            this.mTmpFilesDirectory = str4;
        }

        /* access modifiers changed from: package-private */
        public int saveFile(byte[] bArr, String str) throws Exception {
            return IronSourceStorageUtils.saveFile(bArr, str);
        }

        /* access modifiers changed from: package-private */
        public boolean renameFile(String str, String str2) throws Exception {
            return IronSourceStorageUtils.renameFile(str, str2);
        }

        /* access modifiers changed from: package-private */
        public byte[] getBytes(InputStream inputStream) throws IOException {
            return DownloadManager.getBytes(inputStream);
        }

        public Result call() {
            if (this.mConnectionRetries == 0) {
                this.mConnectionRetries = 1;
            }
            int i = 0;
            Result result = null;
            while (((long) i) < this.mConnectionRetries && ((r3 = (result = downloadContent(this.mFileUrl, i)).responseCode) == 1008 || r3 == 1009)) {
                i++;
            }
            if (!(result == null || result.body == null)) {
                String str = this.mDirectory + File.separator + this.mFileName;
                String str2 = this.mTmpFilesDirectory + File.separator + DownloadManager.TEMP_PREFIX_FOR_FILES + this.mFileName;
                try {
                    if (saveFile(result.body, str2) == 0) {
                        result.responseCode = 1006;
                    } else if (!renameFile(str2, str)) {
                        result.responseCode = 1020;
                    }
                } catch (FileNotFoundException unused) {
                    result.responseCode = 1018;
                } catch (Exception e) {
                    if (!TextUtils.isEmpty(e.getMessage())) {
                        Logger.i(DownloadManager.TAG, e.getMessage());
                    }
                    result.responseCode = 1009;
                } catch (Error e2) {
                    if (!TextUtils.isEmpty(e2.getMessage())) {
                        Logger.i(DownloadManager.TAG, e2.getMessage());
                    }
                    result.responseCode = 1019;
                }
            }
            return result;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r2v1, types: [java.io.InputStream] */
        /* JADX WARN: Type inference failed for: r2v2, types: [java.io.InputStream] */
        /* JADX WARN: Type inference failed for: r2v3, types: [java.net.HttpURLConnection] */
        /* JADX WARN: Type inference failed for: r2v4 */
        /* JADX WARN: Type inference failed for: r2v5 */
        /* JADX WARN: Type inference failed for: r2v6, types: [java.io.InputStream] */
        /* JADX WARN: Type inference failed for: r2v8 */
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:100:0x012c, code lost:
            if (r4 != null) goto L_0x013e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:110:0x013c, code lost:
            if (r4 != null) goto L_0x013e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x013e, code lost:
            r4.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:112:0x0141, code lost:
            r1.url = r8;
            r1.responseCode = r9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e7, code lost:
            if (r4 != null) goto L_0x013e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x010c, code lost:
            if (r4 != null) goto L_0x013e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x011c, code lost:
            if (r4 != null) goto L_0x013e;
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:106:0x0134 A[SYNTHETIC, Splitter:B:106:0x0134] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0056 A[Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }] */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x007c A[SYNTHETIC, Splitter:B:19:0x007c] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0086  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00a6 A[Catch:{ all -> 0x00c2 }] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x00af A[SYNTHETIC, Splitter:B:39:0x00af] */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x00b9  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x00d4 A[Catch:{ all -> 0x00ea }] */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x00df A[SYNTHETIC, Splitter:B:56:0x00df] */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x00ed A[SYNTHETIC, Splitter:B:63:0x00ed] */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x00f7  */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0104 A[SYNTHETIC, Splitter:B:76:0x0104] */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x0114 A[SYNTHETIC, Splitter:B:86:0x0114] */
        /* JADX WARNING: Removed duplicated region for block: B:96:0x0124 A[SYNTHETIC, Splitter:B:96:0x0124] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.ironsource.sdk.precache.DownloadManager.Result downloadContent(java.lang.String r8, int r9) {
            /*
                r7 = this;
                java.lang.String r0 = "DownloadManager"
                com.ironsource.sdk.precache.DownloadManager$Result r1 = new com.ironsource.sdk.precache.DownloadManager$Result
                r1.<init>()
                boolean r2 = android.text.TextUtils.isEmpty(r8)
                if (r2 == 0) goto L_0x0014
                r1.url = r8
                r8 = 1007(0x3ef, float:1.411E-42)
                r1.responseCode = r8
                return r1
            L_0x0014:
                r2 = 0
                r3 = 0
                java.net.URL r4 = new java.net.URL     // Catch:{ MalformedURLException -> 0x012f, URISyntaxException -> 0x011f, SocketTimeoutException -> 0x010f, FileNotFoundException -> 0x00ff, Exception -> 0x00c8, Error -> 0x0098, all -> 0x0095 }
                r4.<init>(r8)     // Catch:{ MalformedURLException -> 0x012f, URISyntaxException -> 0x011f, SocketTimeoutException -> 0x010f, FileNotFoundException -> 0x00ff, Exception -> 0x00c8, Error -> 0x0098, all -> 0x0095 }
                r4.toURI()     // Catch:{ MalformedURLException -> 0x012f, URISyntaxException -> 0x011f, SocketTimeoutException -> 0x010f, FileNotFoundException -> 0x00ff, Exception -> 0x00c8, Error -> 0x0098, all -> 0x0095 }
                java.net.URLConnection r4 = r4.openConnection()     // Catch:{ MalformedURLException -> 0x012f, URISyntaxException -> 0x011f, SocketTimeoutException -> 0x010f, FileNotFoundException -> 0x00ff, Exception -> 0x00c8, Error -> 0x0098, all -> 0x0095 }
                java.lang.Object r4 = com.google.firebase.perf.network.FirebasePerfUrlConnection.instrument(r4)     // Catch:{ MalformedURLException -> 0x012f, URISyntaxException -> 0x011f, SocketTimeoutException -> 0x010f, FileNotFoundException -> 0x00ff, Exception -> 0x00c8, Error -> 0x0098, all -> 0x0095 }
                java.net.URLConnection r4 = (java.net.URLConnection) r4     // Catch:{ MalformedURLException -> 0x012f, URISyntaxException -> 0x011f, SocketTimeoutException -> 0x010f, FileNotFoundException -> 0x00ff, Exception -> 0x00c8, Error -> 0x0098, all -> 0x0095 }
                java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ MalformedURLException -> 0x012f, URISyntaxException -> 0x011f, SocketTimeoutException -> 0x010f, FileNotFoundException -> 0x00ff, Exception -> 0x00c8, Error -> 0x0098, all -> 0x0095 }
                java.lang.String r5 = "GET"
                r4.setRequestMethod(r5)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                r5 = 5000(0x1388, float:7.006E-42)
                r4.setConnectTimeout(r5)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                r4.setReadTimeout(r5)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                r4.connect()     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                int r3 = r4.getResponseCode()     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                r5 = 200(0xc8, float:2.8E-43)
                if (r3 < r5) goto L_0x0052
                r6 = 400(0x190, float:5.6E-43)
                if (r3 < r6) goto L_0x0047
                goto L_0x0052
            L_0x0047:
                java.io.InputStream r2 = r4.getInputStream()     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                byte[] r6 = r7.getBytes(r2)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                r1.body = r6     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                goto L_0x0054
            L_0x0052:
                r3 = 1011(0x3f3, float:1.417E-42)
            L_0x0054:
                if (r3 == r5) goto L_0x007a
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                r5.<init>()     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                java.lang.String r6 = " RESPONSE CODE: "
                r5.append(r6)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                r5.append(r3)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                java.lang.String r6 = " URL: "
                r5.append(r6)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                r5.append(r8)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                java.lang.String r6 = " ATTEMPT: "
                r5.append(r6)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                r5.append(r9)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                java.lang.String r9 = r5.toString()     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
                com.ironsource.sdk.utils.Logger.i(r0, r9)     // Catch:{ MalformedURLException -> 0x0130, URISyntaxException -> 0x0120, SocketTimeoutException -> 0x0110, FileNotFoundException -> 0x0100, Exception -> 0x0093, Error -> 0x008f }
            L_0x007a:
                if (r2 == 0) goto L_0x0084
                r2.close()     // Catch:{ IOException -> 0x0080 }
                goto L_0x0084
            L_0x0080:
                r9 = move-exception
                r9.printStackTrace()
            L_0x0084:
                if (r4 == 0) goto L_0x0089
                r4.disconnect()
            L_0x0089:
                r1.url = r8
                r1.responseCode = r3
                goto L_0x0145
            L_0x008f:
                r9 = move-exception
                r3 = r2
                r2 = r4
                goto L_0x009a
            L_0x0093:
                r9 = move-exception
                goto L_0x00ca
            L_0x0095:
                r9 = move-exception
                r4 = r2
                goto L_0x00eb
            L_0x0098:
                r9 = move-exception
                r3 = r2
            L_0x009a:
                r4 = 1019(0x3fb, float:1.428E-42)
                java.lang.String r5 = r9.getMessage()     // Catch:{ all -> 0x00c2 }
                boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x00c2 }
                if (r5 != 0) goto L_0x00ad
                java.lang.String r9 = r9.getMessage()     // Catch:{ all -> 0x00c2 }
                com.ironsource.sdk.utils.Logger.i(r0, r9)     // Catch:{ all -> 0x00c2 }
            L_0x00ad:
                if (r3 == 0) goto L_0x00b7
                r3.close()     // Catch:{ IOException -> 0x00b3 }
                goto L_0x00b7
            L_0x00b3:
                r9 = move-exception
                r9.printStackTrace()
            L_0x00b7:
                if (r2 == 0) goto L_0x00bc
                r2.disconnect()
            L_0x00bc:
                r1.url = r8
                r1.responseCode = r4
                goto L_0x0145
            L_0x00c2:
                r9 = move-exception
                r4 = r2
                r2 = r3
                r3 = 1019(0x3fb, float:1.428E-42)
                goto L_0x00eb
            L_0x00c8:
                r9 = move-exception
                r4 = r2
            L_0x00ca:
                java.lang.String r5 = r9.getMessage()     // Catch:{ all -> 0x00ea }
                boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x00ea }
                if (r5 != 0) goto L_0x00db
                java.lang.String r9 = r9.getMessage()     // Catch:{ all -> 0x00ea }
                com.ironsource.sdk.utils.Logger.i(r0, r9)     // Catch:{ all -> 0x00ea }
            L_0x00db:
                r9 = 1009(0x3f1, float:1.414E-42)
                if (r2 == 0) goto L_0x00e7
                r2.close()     // Catch:{ IOException -> 0x00e3 }
                goto L_0x00e7
            L_0x00e3:
                r0 = move-exception
                r0.printStackTrace()
            L_0x00e7:
                if (r4 == 0) goto L_0x0141
                goto L_0x013e
            L_0x00ea:
                r9 = move-exception
            L_0x00eb:
                if (r2 == 0) goto L_0x00f5
                r2.close()     // Catch:{ IOException -> 0x00f1 }
                goto L_0x00f5
            L_0x00f1:
                r0 = move-exception
                r0.printStackTrace()
            L_0x00f5:
                if (r4 == 0) goto L_0x00fa
                r4.disconnect()
            L_0x00fa:
                r1.url = r8
                r1.responseCode = r3
                throw r9
            L_0x00ff:
                r4 = r2
            L_0x0100:
                r9 = 1018(0x3fa, float:1.427E-42)
                if (r2 == 0) goto L_0x010c
                r2.close()     // Catch:{ IOException -> 0x0108 }
                goto L_0x010c
            L_0x0108:
                r0 = move-exception
                r0.printStackTrace()
            L_0x010c:
                if (r4 == 0) goto L_0x0141
                goto L_0x013e
            L_0x010f:
                r4 = r2
            L_0x0110:
                r9 = 1008(0x3f0, float:1.413E-42)
                if (r2 == 0) goto L_0x011c
                r2.close()     // Catch:{ IOException -> 0x0118 }
                goto L_0x011c
            L_0x0118:
                r0 = move-exception
                r0.printStackTrace()
            L_0x011c:
                if (r4 == 0) goto L_0x0141
                goto L_0x013e
            L_0x011f:
                r4 = r2
            L_0x0120:
                r9 = 1010(0x3f2, float:1.415E-42)
                if (r2 == 0) goto L_0x012c
                r2.close()     // Catch:{ IOException -> 0x0128 }
                goto L_0x012c
            L_0x0128:
                r0 = move-exception
                r0.printStackTrace()
            L_0x012c:
                if (r4 == 0) goto L_0x0141
                goto L_0x013e
            L_0x012f:
                r4 = r2
            L_0x0130:
                r9 = 1004(0x3ec, float:1.407E-42)
                if (r2 == 0) goto L_0x013c
                r2.close()     // Catch:{ IOException -> 0x0138 }
                goto L_0x013c
            L_0x0138:
                r0 = move-exception
                r0.printStackTrace()
            L_0x013c:
                if (r4 == 0) goto L_0x0141
            L_0x013e:
                r4.disconnect()
            L_0x0141:
                r1.url = r8
                r1.responseCode = r9
            L_0x0145:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.precache.DownloadManager.FileWorkerThread.downloadContent(java.lang.String, int):com.ironsource.sdk.precache.DownloadManager$Result");
        }
    }

    /* access modifiers changed from: package-private */
    public String getTempFilesDirectory() {
        return this.mCacheRootDirectory + File.separator + TEMP_DIR_FOR_FILES;
    }

    static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[8192];
        while (true) {
            int read = inputStream.read(bArr, 0, bArr.length);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }

    static class Result {
        byte[] body;
        int responseCode;
        public String url;

        Result() {
        }
    }
}
