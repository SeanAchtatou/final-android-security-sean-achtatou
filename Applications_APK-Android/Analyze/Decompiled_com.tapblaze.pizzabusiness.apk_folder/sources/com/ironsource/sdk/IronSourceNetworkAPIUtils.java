package com.ironsource.sdk;

import com.ironsource.sdk.constants.Constants;
import org.json.JSONObject;

public class IronSourceNetworkAPIUtils {
    static String manual_rewarded_instance_prefix = "ManRewInst_";

    public static String generateInstanceId(JSONObject jSONObject) {
        if (!jSONObject.optBoolean(Constants.CONVERT_REWARDED)) {
            return jSONObject.optString("name");
        }
        return manual_rewarded_instance_prefix + jSONObject.optString("name");
    }
}
