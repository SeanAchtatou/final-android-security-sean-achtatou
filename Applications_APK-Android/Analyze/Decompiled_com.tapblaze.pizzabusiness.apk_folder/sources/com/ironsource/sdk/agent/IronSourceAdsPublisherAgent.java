package com.ironsource.sdk.agent;

import android.app.Activity;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.ISAdSize;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.IronSourceAdInstance;
import com.ironsource.sdk.IronSourceNetworkAPI;
import com.ironsource.sdk.SSAPublisher;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.controller.BannerJSAdapter;
import com.ironsource.sdk.controller.CommandExecutor;
import com.ironsource.sdk.controller.DemandSourceManager;
import com.ironsource.sdk.controller.IronSourceWebView;
import com.ironsource.sdk.controller.MOATJSAdapter;
import com.ironsource.sdk.controller.PermissionsJSAdapter;
import com.ironsource.sdk.controller.TokenJSAdapter;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSAEnums;
import com.ironsource.sdk.data.SSASession;
import com.ironsource.sdk.listeners.OnBannerListener;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.OnRewardedVideoListener;
import com.ironsource.sdk.listeners.internals.DSAdProductListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import com.ironsource.sdk.service.TokenService;
import com.ironsource.sdk.utils.DeviceProperties;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class IronSourceAdsPublisherAgent implements SSAPublisher, DSRewardedVideoListener, DSInterstitialListener, DSAdProductListener, DSBannerListener, IronSourceNetworkAPI {
    private static final String TAG = "IronSourceAdsPublisherAgent";
    /* access modifiers changed from: private */
    public static MutableContextWrapper mutableContextWrapper;
    private static IronSourceAdsPublisherAgent sInstance;
    private final String SUPERSONIC_ADS = IronSourceConstants.SUPERSONIC_CONFIG_NAME;
    private long adViewContainerCounter;
    /* access modifiers changed from: private */
    public String mApplicationKey;
    /* access modifiers changed from: private */
    public BannerJSAdapter mBannerJSAdapter;
    /* access modifiers changed from: private */
    public CommandExecutor mCommandExecutor;
    /* access modifiers changed from: private */
    public DemandSourceManager mDemandSourceManager;
    /* access modifiers changed from: private */
    public TokenService mTokenService;
    /* access modifiers changed from: private */
    public String mUserId;
    private SSASession session;
    /* access modifiers changed from: private */
    public IronSourceWebView wvc;

    private IronSourceAdsPublisherAgent(Activity activity, int i) {
        initPublisherAgent(activity);
    }

    IronSourceAdsPublisherAgent(String str, String str2, Activity activity) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        initPublisherAgent(activity);
    }

    private void initPublisherAgent(Activity activity) {
        this.mTokenService = createToken(activity);
        this.mCommandExecutor = new CommandExecutor();
        IronSourceSharedPrefHelper.getSupersonicPrefHelper(activity);
        this.mDemandSourceManager = new DemandSourceManager();
        Logger.enableLogging(SDKUtils.getDebugMode());
        Logger.i(TAG, "C'tor");
        mutableContextWrapper = new MutableContextWrapper(activity);
        this.adViewContainerCounter = 0;
        createWebView(activity);
        startSession(activity);
    }

    private TokenService createToken(Activity activity) {
        TokenService instance = TokenService.getInstance();
        instance.fetchIndependentData();
        instance.fetchDependentData(activity, this.mApplicationKey, this.mUserId);
        return instance;
    }

    private void createWebView(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                IronSourceWebView unused = IronSourceAdsPublisherAgent.this.wvc = new IronSourceWebView(IronSourceAdsPublisherAgent.mutableContextWrapper, IronSourceAdsPublisherAgent.this.mDemandSourceManager);
                IronSourceAdsPublisherAgent.this.wvc.addTokenJSInterface(new TokenJSAdapter(IronSourceAdsPublisherAgent.this.mTokenService));
                IronSourceAdsPublisherAgent.this.wvc.addMoatJSInterface(new MOATJSAdapter(activity.getApplication()));
                IronSourceAdsPublisherAgent.this.wvc.addPermissionsJSInterface(new PermissionsJSAdapter(activity.getApplicationContext()));
                BannerJSAdapter unused2 = IronSourceAdsPublisherAgent.this.mBannerJSAdapter = new BannerJSAdapter();
                IronSourceAdsPublisherAgent.this.mBannerJSAdapter.setCommunicationWithController(IronSourceAdsPublisherAgent.this.wvc.getControllerDelegate());
                IronSourceAdsPublisherAgent.this.wvc.addBannerJSInterface(IronSourceAdsPublisherAgent.this.mBannerJSAdapter);
                IronSourceAdsPublisherAgent.this.wvc.registerConnectionReceiver(activity);
                IronSourceAdsPublisherAgent.this.wvc.setDebugMode(SDKUtils.getDebugMode());
                IronSourceAdsPublisherAgent.this.wvc.downloadController();
                IronSourceAdsPublisherAgent.this.mCommandExecutor.setReady();
                IronSourceAdsPublisherAgent.this.mCommandExecutor.purgeDelayedCommands();
            }
        });
    }

    public static IronSourceNetworkAPI createInstance(Activity activity, String str, String str2) {
        return getInstance(str, str2, activity);
    }

    public static synchronized IronSourceNetworkAPI getInstance(String str, String str2, Activity activity) {
        IronSourceAdsPublisherAgent ironSourceAdsPublisherAgent;
        synchronized (IronSourceAdsPublisherAgent.class) {
            if (sInstance == null) {
                sInstance = new IronSourceAdsPublisherAgent(str, str2, activity);
            } else {
                mutableContextWrapper.setBaseContext(activity);
                TokenService.getInstance().collectApplicationKey(str);
                TokenService.getInstance().collectApplicationUserId(str2);
            }
            ironSourceAdsPublisherAgent = sInstance;
        }
        return ironSourceAdsPublisherAgent;
    }

    public static synchronized IronSourceAdsPublisherAgent getInstance(Activity activity) throws Exception {
        IronSourceAdsPublisherAgent instance;
        synchronized (IronSourceAdsPublisherAgent.class) {
            instance = getInstance(activity, 0);
        }
        return instance;
    }

    public static synchronized IronSourceAdsPublisherAgent getInstance(Activity activity, int i) throws Exception {
        IronSourceAdsPublisherAgent ironSourceAdsPublisherAgent;
        synchronized (IronSourceAdsPublisherAgent.class) {
            Logger.i(TAG, "getInstance()");
            if (sInstance == null) {
                sInstance = new IronSourceAdsPublisherAgent(activity, i);
            } else {
                mutableContextWrapper.setBaseContext(activity);
            }
            ironSourceAdsPublisherAgent = sInstance;
        }
        return ironSourceAdsPublisherAgent;
    }

    public IronSourceWebView getWebViewController() {
        return this.wvc;
    }

    private OnRewardedVideoListener getAdProductListenerAsRVListener(DemandSource demandSource) {
        if (demandSource == null) {
            return null;
        }
        return (OnRewardedVideoListener) demandSource.getListener();
    }

    private OnInterstitialListener getAdProductListenerAsISListener(DemandSource demandSource) {
        if (demandSource == null) {
            return null;
        }
        return (OnInterstitialListener) demandSource.getListener();
    }

    private OnBannerListener getAdProductListenerAsBNListener(DemandSource demandSource) {
        if (demandSource == null) {
            return null;
        }
        return (OnBannerListener) demandSource.getListener();
    }

    private void startSession(Context context) {
        this.session = new SSASession(context, SSASession.SessionType.launched);
    }

    public void resumeSession(Context context) {
        this.session = new SSASession(context, SSASession.SessionType.backFromBG);
    }

    private void endSession() {
        SSASession sSASession = this.session;
        if (sSASession != null) {
            sSASession.endSession();
            IronSourceSharedPrefHelper.getSupersonicPrefHelper().addSession(this.session);
            this.session = null;
        }
    }

    public void initRewardedVideo(final String str, final String str2, String str3, Map<String, String> map, OnRewardedVideoListener onRewardedVideoListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        final DemandSource createDemandSource = this.mDemandSourceManager.createDemandSource(SSAEnums.ProductType.RewardedVideo, str3, map, onRewardedVideoListener);
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.initRewardedVideo(str, str2, createDemandSource, IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public void showRewardedVideo(final JSONObject jSONObject) {
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.showRewardedVideo(jSONObject);
            }
        });
    }

    public void initOfferWall(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        final String str3 = str;
        final String str4 = str2;
        final Map<String, String> map2 = map;
        final OnOfferWallListener onOfferWallListener2 = onOfferWallListener;
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.initOfferWall(str3, str4, map2, onOfferWallListener2);
            }
        });
    }

    public void initOfferWall(final Map<String, String> map, final OnOfferWallListener onOfferWallListener) {
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.initOfferWall(IronSourceAdsPublisherAgent.this.mApplicationKey, IronSourceAdsPublisherAgent.this.mUserId, map, onOfferWallListener);
            }
        });
    }

    public void showOfferWall(final Map<String, String> map) {
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.showOfferWall(map);
            }
        });
    }

    public void getOfferWallCredits(final String str, final String str2, final OnOfferWallListener onOfferWallListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.getOfferWallCredits(str, str2, onOfferWallListener);
            }
        });
    }

    public void getOfferWallCredits(final OnOfferWallListener onOfferWallListener) {
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.getOfferWallCredits(IronSourceAdsPublisherAgent.this.mApplicationKey, IronSourceAdsPublisherAgent.this.mUserId, onOfferWallListener);
            }
        });
    }

    public void initInterstitial(final String str, final String str2, String str3, Map<String, String> map, OnInterstitialListener onInterstitialListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        final DemandSource createDemandSource = this.mDemandSourceManager.createDemandSource(SSAEnums.ProductType.Interstitial, str3, map, onInterstitialListener);
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.initInterstitial(str, str2, createDemandSource, IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public void loadInterstitial(JSONObject jSONObject) {
        if (jSONObject != null) {
            final String optString = jSONObject.optString("demandSourceName");
            if (!TextUtils.isEmpty(optString)) {
                this.mCommandExecutor.executeCommand(new Runnable() {
                    public void run() {
                        IronSourceAdsPublisherAgent.this.wvc.loadInterstitial(optString);
                    }
                });
            }
        }
    }

    public boolean isInterstitialAdAvailable(String str) {
        IronSourceWebView ironSourceWebView = this.wvc;
        if (ironSourceWebView == null) {
            return false;
        }
        return ironSourceWebView.isInterstitialAdAvailable(str);
    }

    public void showInterstitial(final JSONObject jSONObject) {
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.showInterstitial(jSONObject);
            }
        });
    }

    public void initBanner(final String str, final String str2, String str3, Map<String, String> map, OnBannerListener onBannerListener) {
        this.mApplicationKey = str;
        this.mUserId = str2;
        final DemandSource createDemandSource = this.mDemandSourceManager.createDemandSource(SSAEnums.ProductType.Banner, str3, map, onBannerListener);
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.initBanner(str, str2, createDemandSource, IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public void initBanner(String str, Map<String, String> map, OnBannerListener onBannerListener) {
        final DemandSource createDemandSource = this.mDemandSourceManager.createDemandSource(SSAEnums.ProductType.Banner, str, map, onBannerListener);
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.initBanner(IronSourceAdsPublisherAgent.this.mApplicationKey, IronSourceAdsPublisherAgent.this.mUserId, createDemandSource, IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public void loadBanner(final JSONObject jSONObject) {
        if (jSONObject != null) {
            this.mCommandExecutor.executeCommand(new Runnable() {
                public void run() {
                    IronSourceAdsPublisherAgent.this.wvc.loadBanner(jSONObject);
                }
            });
        }
    }

    public void onResume(Activity activity) {
        mutableContextWrapper.setBaseContext(activity);
        this.wvc.enterForeground();
        this.wvc.registerConnectionReceiver(activity);
        if (this.session == null) {
            resumeSession(activity);
        }
    }

    public void onPause(Activity activity) {
        try {
            this.wvc.enterBackground();
            this.wvc.unregisterConnectionReceiver(activity);
            endSession();
        } catch (Exception e) {
            e.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            ironSourceAsyncHttpRequestTask.execute(Constants.NATIVE_EXCEPTION_BASE_URL + e.getStackTrace()[0].getMethodName());
        }
    }

    public void release(Activity activity) {
        try {
            Logger.i(TAG, "release()");
            DeviceProperties.release();
            this.wvc.unregisterConnectionReceiver(activity);
            if (Looper.getMainLooper().equals(Looper.myLooper())) {
                this.wvc.destroy();
                this.wvc = null;
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        IronSourceAdsPublisherAgent.this.wvc.destroy();
                        IronSourceWebView unused = IronSourceAdsPublisherAgent.this.wvc = null;
                    }
                });
            }
        } catch (Exception unused) {
        }
        sInstance = null;
        endSession();
    }

    public void onAdProductInitSuccess(SSAEnums.ProductType productType, String str, AdUnitsReady adUnitsReady) {
        OnBannerListener adProductListenerAsBNListener;
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName != null) {
            demandSourceByName.setDemandSourceInitState(2);
            if (productType == SSAEnums.ProductType.RewardedVideo) {
                OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
                if (adProductListenerAsRVListener != null) {
                    adProductListenerAsRVListener.onRVInitSuccess(adUnitsReady);
                }
            } else if (productType == SSAEnums.ProductType.Interstitial) {
                OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
                if (adProductListenerAsISListener != null) {
                    adProductListenerAsISListener.onInterstitialInitSuccess();
                }
            } else if (productType == SSAEnums.ProductType.Banner && (adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName)) != null) {
                adProductListenerAsBNListener.onBannerInitSuccess();
            }
        }
    }

    public void onAdProductInitFailed(SSAEnums.ProductType productType, String str, String str2) {
        OnBannerListener adProductListenerAsBNListener;
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName != null) {
            demandSourceByName.setDemandSourceInitState(3);
            if (productType == SSAEnums.ProductType.RewardedVideo) {
                OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
                if (adProductListenerAsRVListener != null) {
                    adProductListenerAsRVListener.onRVInitFail(str2);
                }
            } else if (productType == SSAEnums.ProductType.Interstitial) {
                OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
                if (adProductListenerAsISListener != null) {
                    adProductListenerAsISListener.onInterstitialInitFailed(str2);
                }
            } else if (productType == SSAEnums.ProductType.Banner && (adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName)) != null) {
                adProductListenerAsBNListener.onBannerInitFailed(str2);
            }
        }
    }

    public void onRVNoMoreOffers(String str) {
        OnRewardedVideoListener adProductListenerAsRVListener;
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.RewardedVideo, str);
        if (demandSourceByName != null && (adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName)) != null) {
            adProductListenerAsRVListener.onRVNoMoreOffers();
        }
    }

    public void onRVAdCredited(String str, int i) {
        OnRewardedVideoListener adProductListenerAsRVListener;
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.RewardedVideo, str);
        if (demandSourceByName != null && (adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName)) != null) {
            adProductListenerAsRVListener.onRVAdCredited(i);
        }
    }

    public void onAdProductClose(SSAEnums.ProductType productType, String str) {
        OnInterstitialListener adProductListenerAsISListener;
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName == null) {
            return;
        }
        if (productType == SSAEnums.ProductType.RewardedVideo) {
            OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
            if (adProductListenerAsRVListener != null) {
                adProductListenerAsRVListener.onRVAdClosed();
            }
        } else if (productType == SSAEnums.ProductType.Interstitial && (adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName)) != null) {
            adProductListenerAsISListener.onInterstitialClose();
        }
    }

    public void onRVShowFail(String str, String str2) {
        OnRewardedVideoListener adProductListenerAsRVListener;
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.RewardedVideo, str);
        if (demandSourceByName != null && (adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName)) != null) {
            adProductListenerAsRVListener.onRVShowFail(str2);
        }
    }

    public void onAdProductClick(SSAEnums.ProductType productType, String str) {
        OnBannerListener adProductListenerAsBNListener;
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName == null) {
            return;
        }
        if (productType == SSAEnums.ProductType.RewardedVideo) {
            OnRewardedVideoListener adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName);
            if (adProductListenerAsRVListener != null) {
                adProductListenerAsRVListener.onRVAdClicked();
            }
        } else if (productType == SSAEnums.ProductType.Interstitial) {
            OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
            if (adProductListenerAsISListener != null) {
                adProductListenerAsISListener.onInterstitialClick();
            }
        } else if (productType == SSAEnums.ProductType.Banner && (adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName)) != null) {
            adProductListenerAsBNListener.onBannerClick();
        }
    }

    public void onAdProductEventNotificationReceived(SSAEnums.ProductType productType, String str, String str2, JSONObject jSONObject) {
        OnRewardedVideoListener adProductListenerAsRVListener;
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName != null) {
            try {
                if (productType == SSAEnums.ProductType.Interstitial) {
                    OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
                    if (adProductListenerAsISListener != null) {
                        jSONObject.put("demandSourceName", str);
                        adProductListenerAsISListener.onInterstitialEventNotificationReceived(str2, jSONObject);
                    }
                } else if (productType == SSAEnums.ProductType.RewardedVideo && (adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName)) != null) {
                    jSONObject.put("demandSourceName", str);
                    adProductListenerAsRVListener.onRVEventNotificationReceived(str2, jSONObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onAdProductOpen(SSAEnums.ProductType productType, String str) {
        OnRewardedVideoListener adProductListenerAsRVListener;
        DemandSource demandSourceByName = getDemandSourceByName(productType, str);
        if (demandSourceByName == null) {
            return;
        }
        if (productType == SSAEnums.ProductType.Interstitial) {
            OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
            if (adProductListenerAsISListener != null) {
                adProductListenerAsISListener.onInterstitialOpen();
            }
        } else if (productType == SSAEnums.ProductType.RewardedVideo && (adProductListenerAsRVListener = getAdProductListenerAsRVListener(demandSourceByName)) != null) {
            adProductListenerAsRVListener.onRVAdOpened();
        }
    }

    public void onInterstitialLoadSuccess(String str) {
        OnInterstitialListener adProductListenerAsISListener;
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.Interstitial, str);
        if (demandSourceByName != null && (adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName)) != null) {
            adProductListenerAsISListener.onInterstitialLoadSuccess();
        }
    }

    public void onInterstitialLoadFailed(String str, String str2) {
        OnInterstitialListener adProductListenerAsISListener;
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.Interstitial, str);
        if (demandSourceByName != null && (adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName)) != null) {
            adProductListenerAsISListener.onInterstitialLoadFailed(str2);
        }
    }

    public void onInterstitialShowSuccess(String str) {
        OnInterstitialListener adProductListenerAsISListener;
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.Interstitial, str);
        if (demandSourceByName != null && (adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName)) != null) {
            adProductListenerAsISListener.onInterstitialShowSuccess();
        }
    }

    public void onInterstitialShowFailed(String str, String str2) {
        OnInterstitialListener adProductListenerAsISListener;
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.Interstitial, str);
        if (demandSourceByName != null && (adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName)) != null) {
            adProductListenerAsISListener.onInterstitialShowFailed(str2);
        }
    }

    public void onInterstitialAdRewarded(String str, int i) {
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.Interstitial, str);
        OnInterstitialListener adProductListenerAsISListener = getAdProductListenerAsISListener(demandSourceByName);
        if (demandSourceByName != null && adProductListenerAsISListener != null) {
            adProductListenerAsISListener.onInterstitialAdRewarded(str, i);
        }
    }

    private DemandSource getDemandSourceByName(SSAEnums.ProductType productType, String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return this.mDemandSourceManager.getDemandSourceById(productType, str);
    }

    public void setMediationState(String str, String str2, int i) {
        SSAEnums.ProductType productType;
        DemandSource demandSourceById;
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && (productType = SDKUtils.getProductType(str)) != null && (demandSourceById = this.mDemandSourceManager.getDemandSourceById(productType, str2)) != null) {
            demandSourceById.setMediationState(i);
        }
    }

    public void updateConsentInfo(final JSONObject jSONObject) {
        updateConsentInToken(jSONObject);
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.wvc.updateConsentInfo(jSONObject);
            }
        });
    }

    private void updateConsentInToken(JSONObject jSONObject) {
        if (jSONObject != null && jSONObject.has(Constants.RequestParameters.GDPR_CONSENT_STATUS)) {
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put(Constants.RequestParameters.CONSENT, Boolean.valueOf(jSONObject.getString(Constants.RequestParameters.GDPR_CONSENT_STATUS)).booleanValue());
                this.mTokenService.updateData(jSONObject2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ISNAdView createBanner(Activity activity, ISAdSize iSAdSize) {
        this.adViewContainerCounter++;
        ISNAdView iSNAdView = new ISNAdView(activity, "SupersonicAds_" + this.adViewContainerCounter, iSAdSize);
        this.mBannerJSAdapter.setCommunicationWithAdView(iSNAdView);
        return iSNAdView;
    }

    public void onBannerLoadSuccess(String str) {
        OnBannerListener adProductListenerAsBNListener;
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.Banner, str);
        if (demandSourceByName != null && (adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName)) != null) {
            adProductListenerAsBNListener.onBannerLoadSuccess();
        }
    }

    public void onBannerLoadFail(String str, String str2) {
        OnBannerListener adProductListenerAsBNListener;
        DemandSource demandSourceByName = getDemandSourceByName(SSAEnums.ProductType.Banner, str);
        if (demandSourceByName != null && (adProductListenerAsBNListener = getAdProductListenerAsBNListener(demandSourceByName)) != null) {
            adProductListenerAsBNListener.onBannerLoadFail(str2);
        }
    }

    public void loadAd(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) {
        Logger.d(TAG, "loadAd " + ironSourceAdInstance.getId());
        if (ironSourceAdInstance.isInAppBidding()) {
            loadInAppBiddingAd(ironSourceAdInstance, map);
        } else {
            loadInstance(ironSourceAdInstance, map);
        }
    }

    private void loadInAppBiddingAd(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) {
        try {
            map = decodeADM(map);
        } catch (Exception e) {
            e.printStackTrace();
            Logger.d(TAG, "loadInAppBiddingAd failed decoding ADM " + e.getMessage());
        }
        loadInstance(ironSourceAdInstance, map);
    }

    private void loadInstance(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) {
        if (ironSourceAdInstance.isInitialized()) {
            loadInitializedInstance(ironSourceAdInstance, map);
        } else {
            loadUninitializedInstance(ironSourceAdInstance, map);
        }
    }

    private Map<String, String> decodeADM(Map<String, String> map) {
        map.put(Constants.ParametersKeys.ADM, SDKUtils.decodeString(map.get(Constants.ParametersKeys.ADM)));
        return map;
    }

    private void loadInitializedInstance(final IronSourceAdInstance ironSourceAdInstance, final Map<String, String> map) {
        Logger.d(TAG, "loadOnInitializedInstance " + ironSourceAdInstance.getId());
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                DemandSource demandSourceById = IronSourceAdsPublisherAgent.this.mDemandSourceManager.getDemandSourceById(SSAEnums.ProductType.Interstitial, ironSourceAdInstance.getId());
                if (demandSourceById != null) {
                    IronSourceAdsPublisherAgent.this.wvc.loadInterstitial(demandSourceById, map);
                }
            }
        });
    }

    private void loadUninitializedInstance(final IronSourceAdInstance ironSourceAdInstance, final Map<String, String> map) {
        Logger.d(TAG, "loadOnNewInstance " + ironSourceAdInstance.getId());
        this.mCommandExecutor.executeCommand(new Runnable() {
            public void run() {
                DemandSource createDemandSource = IronSourceAdsPublisherAgent.this.mDemandSourceManager.createDemandSource(SSAEnums.ProductType.Interstitial, ironSourceAdInstance);
                IronSourceAdsPublisherAgent.this.wvc.initInterstitial(IronSourceAdsPublisherAgent.this.mApplicationKey, IronSourceAdsPublisherAgent.this.mUserId, createDemandSource, IronSourceAdsPublisherAgent.this);
                ironSourceAdInstance.setInitialized(true);
                IronSourceAdsPublisherAgent.this.wvc.loadInterstitial(createDemandSource, map);
            }
        });
    }

    public void showAd(IronSourceAdInstance ironSourceAdInstance, final Map<String, String> map) {
        Logger.i(TAG, "showAd " + ironSourceAdInstance.getId());
        final DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(SSAEnums.ProductType.Interstitial, ironSourceAdInstance.getId());
        if (demandSourceById != null) {
            this.mCommandExecutor.executeCommand(new Runnable() {
                public void run() {
                    IronSourceAdsPublisherAgent.this.wvc.showInterstitial(demandSourceById, map);
                }
            });
        }
    }

    public boolean isAdAvailable(IronSourceAdInstance ironSourceAdInstance) {
        if (this.wvc == null) {
            return false;
        }
        Logger.d(TAG, "isAdAvailable " + ironSourceAdInstance.getId());
        DemandSource demandSourceById = this.mDemandSourceManager.getDemandSourceById(SSAEnums.ProductType.Interstitial, ironSourceAdInstance.getId());
        if (demandSourceById == null) {
            return false;
        }
        return demandSourceById.getAvailabilityState();
    }
}
