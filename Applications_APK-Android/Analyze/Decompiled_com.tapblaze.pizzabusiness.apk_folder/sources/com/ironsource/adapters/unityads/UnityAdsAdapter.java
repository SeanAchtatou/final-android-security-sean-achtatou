package com.ironsource.adapters.unityads;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;
import com.unity3d.ads.mediation.IUnityAdsExtendedListener;
import com.unity3d.ads.metadata.MediationMetaData;
import com.unity3d.ads.metadata.MetaData;
import com.unity3d.ads.metadata.PlayerMetaData;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

class UnityAdsAdapter extends AbstractAdapter implements IUnityAdsListener, IUnityAdsExtendedListener {
    private static final String GitHash = "7d0d9b16a";
    private static final String VERSION = "4.1.6";
    private final String CONSENT_GDPR = "gdpr.consent";
    private final String GAME_ID = "sourceId";
    private final String PLACEMENT_ID = "zoneId";
    private Activity mActivity;
    private Boolean mConsentCollectingUserData = null;
    private AtomicBoolean mDidInit = new AtomicBoolean(false);
    private CopyOnWriteArraySet<String> mISZoneReceivedFirstStatus = new CopyOnWriteArraySet<>();
    private final Object mUnityAdsStorageLock = new Object();
    private ConcurrentHashMap<String, InterstitialSmashListener> mZoneIdToIsListener = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, RewardedVideoSmashListener> mZoneIdToRvListener = new ConcurrentHashMap<>();

    public String getVersion() {
        return VERSION;
    }

    public static UnityAdsAdapter startAdapter(String str) {
        return new UnityAdsAdapter(str);
    }

    private UnityAdsAdapter(String str) {
        super(str);
    }

    public static IntegrationData getIntegrationData(Activity activity) {
        IntegrationData integrationData = new IntegrationData("UnityAds", VERSION);
        integrationData.activities = new String[]{"com.unity3d.services.ads.adunit.AdUnitActivity", "com.unity3d.services.ads.adunit.AdUnitTransparentActivity", "com.unity3d.services.ads.adunit.AdUnitTransparentSoftwareActivity", "com.unity3d.services.ads.adunit.AdUnitSoftwareActivity"};
        return integrationData;
    }

    public static String getAdapterSDKVersion() {
        try {
            return UnityAds.getVersion();
        } catch (Exception unused) {
            return null;
        }
    }

    public String getCoreSDKVersion() {
        return UnityAds.getVersion();
    }

    public void onResume(Activity activity) {
        this.mActivity = activity;
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        log("setConsent: " + z);
        if (this.mDidInit.get()) {
            synchronized (this.mUnityAdsStorageLock) {
                MetaData metaData = new MetaData(this.mActivity);
                metaData.set("gdpr.consent", Boolean.valueOf(z));
                metaData.commit();
            }
            return;
        }
        this.mConsentCollectingUserData = Boolean.valueOf(z);
    }

    private void log(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + " " + str, 0);
    }

    private void initSDK(Activity activity, String str) {
        boolean z = false;
        if (this.mDidInit.compareAndSet(false, true)) {
            log("initiating unityAds SDK in manual mode");
            this.mActivity = activity;
            synchronized (this.mUnityAdsStorageLock) {
                MediationMetaData mediationMetaData = new MediationMetaData(activity);
                mediationMetaData.setName(IronSourceConstants.IRONSOURCE_CONFIG_NAME);
                mediationMetaData.setVersion(VERSION);
                mediationMetaData.commit();
            }
            UnityAds.initialize(activity, str, this, false, true);
            try {
                z = isAdaptersDebugEnabled();
            } catch (NoSuchMethodError unused) {
            }
            UnityAds.setDebugMode(z);
            Boolean bool = this.mConsentCollectingUserData;
            if (bool != null) {
                setConsent(bool.booleanValue());
            }
        }
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        String optString = jSONObject.optString("sourceId");
        String optString2 = jSONObject.optString("zoneId");
        log("initRewardedVideo gameId: <" + optString + "> placementId: <" + optString2 + ">");
        if (rewardedVideoSmashListener == null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            logger.log(ironSourceTag, getProviderName() + " initRewardedVideo: null listener", 3);
        } else if (TextUtils.isEmpty(optString)) {
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
        } else {
            if (!TextUtils.isEmpty(optString2)) {
                this.mZoneIdToRvListener.put(optString2, rewardedVideoSmashListener);
            }
            if (this.mISZoneReceivedFirstStatus.contains(optString2)) {
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(UnityAds.isReady(optString2));
            }
            initSDK(activity, optString);
            loadRewardedVideo(optString2);
        }
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        String optString = jSONObject.optString("zoneId");
        log("fetchRewardedVideo with placementId: <" + optString + "> state: " + UnityAds.getPlacementState(optString));
        RewardedVideoSmashListener rewardedVideoSmashListener = this.mZoneIdToRvListener.get(optString);
        if (rewardedVideoSmashListener == null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            logger.log(ironSourceTag, getProviderName() + " fetchRewardedVideo: null listener", 3);
            return;
        }
        loadRewardedVideo(optString);
        if (UnityAds.getPlacementState(optString) == UnityAds.PlacementState.READY) {
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(true);
        } else if (UnityAds.getPlacementState(optString) == UnityAds.PlacementState.NO_FILL || UnityAds.getPlacementState(optString) == UnityAds.PlacementState.DISABLED) {
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        String optString = jSONObject.optString("zoneId");
        log("showRewardedVideo placementId: <" + optString + ">");
        if (UnityAds.isReady(optString)) {
            if (!TextUtils.isEmpty(getDynamicUserId())) {
                synchronized (this.mUnityAdsStorageLock) {
                    PlayerMetaData playerMetaData = new PlayerMetaData(this.mActivity);
                    playerMetaData.setServerId(getDynamicUserId());
                    playerMetaData.commit();
                }
            }
            UnityAds.show(this.mActivity, optString);
        } else if (rewardedVideoSmashListener != null) {
            rewardedVideoSmashListener.onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
        }
        if (rewardedVideoSmashListener != null) {
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
        }
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        return UnityAds.isReady(jSONObject.optString("zoneId"));
    }

    private void loadRewardedVideo(String str) {
        log("loadRewardedVideo placementId: <" + str + ">");
        UnityAds.load(str);
    }

    public void initInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        String optString = jSONObject.optString("sourceId");
        if (!TextUtils.isEmpty(optString)) {
            String optString2 = jSONObject.optString("zoneId");
            log("initInterstitial gameId: <" + optString + "> placementId: <" + optString2 + ">");
            if (!TextUtils.isEmpty(optString2) && interstitialSmashListener != null) {
                this.mZoneIdToIsListener.put(optString2, interstitialSmashListener);
            }
            initSDK(activity, jSONObject.optString("sourceId"));
            if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialInitSuccess();
            }
        } else if (interstitialSmashListener != null) {
            interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("Missing params", "Interstitial"));
        }
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        String optString = jSONObject.optString("zoneId");
        log("loadInterstitial placementId <" + optString + ">: " + UnityAds.getPlacementState(optString));
        if (interstitialSmashListener == null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            logger.log(ironSourceTag, getProviderName() + "null listener for placement Id <" + optString + ">", 3);
            return;
        }
        UnityAds.load(optString);
        if (UnityAds.getPlacementState(optString) == UnityAds.PlacementState.READY) {
            interstitialSmashListener.onInterstitialAdReady();
        } else if (UnityAds.getPlacementState(optString) == UnityAds.PlacementState.NO_FILL || UnityAds.getPlacementState(optString) == UnityAds.PlacementState.DISABLED) {
            interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError("Ad unavailable: " + UnityAds.getPlacementState(optString)));
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        String optString = jSONObject.optString("zoneId");
        log("showInterstitial placementId <" + optString + ">");
        if (UnityAds.isReady(optString)) {
            UnityAds.show(this.mActivity, optString);
        } else if (interstitialSmashListener != null) {
            interstitialSmashListener.onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
        }
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        String optString = jSONObject.optString("zoneId");
        log(" isInterstitialReady placementId <" + optString + ">  state: " + UnityAds.getPlacementState(optString));
        return UnityAds.isReady(optString);
    }

    public void onUnityAdsReady(String str) {
        log("onUnityAdsReady placementId <" + str + ">");
    }

    public void onUnityAdsStart(String str) {
        log("onUnityAdsStart placementId <" + str + ">");
        if (!TextUtils.isEmpty(str)) {
            RewardedVideoSmashListener rewardedVideoSmashListener = this.mZoneIdToRvListener.get(str);
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAdOpened();
                rewardedVideoSmashListener.onRewardedVideoAdStarted();
                return;
            }
            InterstitialSmashListener interstitialSmashListener = this.mZoneIdToIsListener.get(str);
            if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialAdOpened();
                interstitialSmashListener.onInterstitialAdShowSucceeded();
            }
        }
    }

    public void onUnityAdsFinish(String str, UnityAds.FinishState finishState) {
        log("onUnityAdsFinish placementId: <" + str + "> finishState: " + finishState + ")");
        if (!TextUtils.isEmpty(str)) {
            RewardedVideoSmashListener rewardedVideoSmashListener = this.mZoneIdToRvListener.get(str);
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(UnityAds.isReady(str));
                if (finishState.equals(UnityAds.FinishState.COMPLETED)) {
                    rewardedVideoSmashListener.onRewardedVideoAdEnded();
                    rewardedVideoSmashListener.onRewardedVideoAdRewarded();
                }
                rewardedVideoSmashListener.onRewardedVideoAdClosed();
                return;
            }
            InterstitialSmashListener interstitialSmashListener = this.mZoneIdToIsListener.get(str);
            if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialAdClosed();
            }
        }
    }

    public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String str) {
        log("onUnityAdsError(errorType: " + unityAdsError + ", errorMessage: " + str + ")");
    }

    public void onUnityAdsClick(String str) {
        log("onUnityAdsClick placementId: <" + str + ">");
        if (!TextUtils.isEmpty(str)) {
            RewardedVideoSmashListener rewardedVideoSmashListener = this.mZoneIdToRvListener.get(str);
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAdClicked();
                return;
            }
            InterstitialSmashListener interstitialSmashListener = this.mZoneIdToIsListener.get(str);
            if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialAdClicked();
            }
        }
    }

    public void onUnityAdsPlacementStateChanged(String str, UnityAds.PlacementState placementState, UnityAds.PlacementState placementState2) {
        log("onUnityAdsPlacementStateChanged placementId: <" + str + "> from " + placementState + " to " + placementState2 + " is Ready: " + UnityAds.isReady(str));
        if (!placementState2.equals(placementState) && !placementState2.equals(UnityAds.PlacementState.WAITING)) {
            this.mISZoneReceivedFirstStatus.add(str);
            if (!TextUtils.isEmpty(str)) {
                RewardedVideoSmashListener rewardedVideoSmashListener = this.mZoneIdToRvListener.get(str);
                if (rewardedVideoSmashListener != null) {
                    rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(placementState2.equals(UnityAds.PlacementState.READY));
                    return;
                }
                InterstitialSmashListener interstitialSmashListener = this.mZoneIdToIsListener.get(str);
                if (interstitialSmashListener == null) {
                    return;
                }
                if (placementState2.equals(UnityAds.PlacementState.READY)) {
                    interstitialSmashListener.onInterstitialAdReady();
                    return;
                }
                interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(str + " placement state: " + placementState2.toString()));
            }
        }
    }
}
