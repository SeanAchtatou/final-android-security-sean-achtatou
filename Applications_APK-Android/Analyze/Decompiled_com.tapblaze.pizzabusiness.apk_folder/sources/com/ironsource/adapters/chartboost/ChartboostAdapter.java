package com.ironsource.adapters.chartboost;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.adcolony.sdk.AdColonyAppOptions;
import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class ChartboostAdapter extends AbstractAdapter {
    private static final String GitHash = "27340881d";
    private static final String VERSION = "4.1.9";
    private final String AD_LOCATION = "adLocation";
    private final String APP_ID = "appID";
    private final String APP_SIGNATURE = "appSignature";
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public Boolean mAlreadyCalledInit = false;
    /* access modifiers changed from: private */
    public Boolean mConsentCollectingUserData = null;
    /* access modifiers changed from: private */
    public CbDelegate mDelegate;
    /* access modifiers changed from: private */
    public boolean mDidInitSuccessfully = false;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, InterstitialSmashListener> mLocationToIsListener = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, RewardedVideoSmashListener> mLocationToRvListener = new ConcurrentHashMap<>();

    public String getVersion() {
        return VERSION;
    }

    public static ChartboostAdapter startAdapter(String str) {
        return new ChartboostAdapter(str);
    }

    private ChartboostAdapter(String str) {
        super(str);
    }

    public static IntegrationData getIntegrationData(Activity activity) {
        IntegrationData integrationData = new IntegrationData("Chartboost", VERSION);
        integrationData.activities = new String[]{"com.chartboost.sdk.CBImpressionActivity"};
        return integrationData;
    }

    public static String getAdapterSDKVersion() {
        try {
            return Chartboost.getSDKVersion();
        } catch (Exception unused) {
            return null;
        }
    }

    public String getCoreSDKVersion() {
        return Chartboost.getSDKVersion();
    }

    /* access modifiers changed from: protected */
    public synchronized void setConsent(boolean z) {
        if (!this.mAlreadyCalledInit.booleanValue()) {
            this.mConsentCollectingUserData = Boolean.valueOf(z);
        } else if (z) {
            Chartboost.setPIDataUseConsent(this.mActivity, Chartboost.CBPIDataUseConsent.YES_BEHAVIORAL);
        } else {
            Chartboost.setPIDataUseConsent(this.mActivity, Chartboost.CBPIDataUseConsent.NO_BEHAVIORAL);
        }
    }

    public void onResume(Activity activity) {
        if (activity != null) {
            this.mActivity = activity;
            Chartboost.onStart(activity);
            Chartboost.onResume(activity);
        }
    }

    public void onPause(Activity activity) {
        if (activity != null) {
            Chartboost.onPause(activity);
            Chartboost.onStop(activity);
        }
    }

    private void init(Activity activity, String str, String str2, String str3, String str4) {
        final Activity activity2 = activity;
        final String str5 = str3;
        final String str6 = str4;
        final String str7 = str;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                synchronized (this) {
                    if (!ChartboostAdapter.this.mAlreadyCalledInit.booleanValue()) {
                        Boolean unused = ChartboostAdapter.this.mAlreadyCalledInit = true;
                        Activity unused2 = ChartboostAdapter.this.mActivity = activity2;
                        CbDelegate unused3 = ChartboostAdapter.this.mDelegate = new CbDelegate();
                        if (ChartboostAdapter.this.mConsentCollectingUserData != null) {
                            ChartboostAdapter.this.setConsent(ChartboostAdapter.this.mConsentCollectingUserData.booleanValue());
                        }
                        Chartboost.setDelegate(ChartboostAdapter.this.mDelegate);
                        Chartboost.startWithAppId(activity2, str5, str6);
                        boolean z = false;
                        try {
                            z = ChartboostAdapter.this.isAdaptersDebugEnabled();
                        } catch (NoSuchMethodError unused4) {
                        }
                        if (z) {
                            Chartboost.setLoggingLevel(CBLogging.Level.ALL);
                        } else {
                            Chartboost.setLoggingLevel(CBLogging.Level.NONE);
                        }
                        if (AdColonyAppOptions.UNITY.equals(ChartboostAdapter.this.getPluginType()) && !TextUtils.isEmpty(ChartboostAdapter.this.getPluginFrameworkVersion())) {
                            Chartboost.setFramework(Chartboost.CBFramework.CBFrameworkUnity, ChartboostAdapter.this.getPluginFrameworkVersion());
                        }
                        Chartboost.setMediation(Chartboost.CBMediation.CBMediationironSource, ChartboostAdapter.VERSION);
                        Chartboost.setCustomId(str7);
                        Chartboost.setAutoCacheAds(true);
                        Chartboost.onCreate(activity2);
                        Chartboost.onStart(activity2);
                        Chartboost.onResume(activity2);
                    }
                }
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0066, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void initRewardedVideo(android.app.Activity r8, java.lang.String r9, java.lang.String r10, org.json.JSONObject r11, com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener r12) {
        /*
            r7 = this;
            monitor-enter(r7)
            java.lang.String r9 = "appID"
            java.lang.String r9 = r11.optString(r9)     // Catch:{ all -> 0x006f }
            boolean r9 = android.text.TextUtils.isEmpty(r9)     // Catch:{ all -> 0x006f }
            if (r9 != 0) goto L_0x0067
            java.lang.String r9 = "appSignature"
            java.lang.String r9 = r11.optString(r9)     // Catch:{ all -> 0x006f }
            boolean r9 = android.text.TextUtils.isEmpty(r9)     // Catch:{ all -> 0x006f }
            if (r9 == 0) goto L_0x001a
            goto L_0x0067
        L_0x001a:
            java.lang.String r9 = r7.getLocationId(r11)     // Catch:{ all -> 0x006f }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = com.ironsource.mediationsdk.logger.IronSourceLoggerManager.getLogger()     // Catch:{ all -> 0x006f }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_API     // Catch:{ all -> 0x006f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x006f }
            r2.<init>()     // Catch:{ all -> 0x006f }
            java.lang.String r3 = r7.getProviderName()     // Catch:{ all -> 0x006f }
            r2.append(r3)     // Catch:{ all -> 0x006f }
            java.lang.String r3 = " initRewardedVideo placementId: <"
            r2.append(r3)     // Catch:{ all -> 0x006f }
            r2.append(r9)     // Catch:{ all -> 0x006f }
            java.lang.String r3 = ">"
            r2.append(r3)     // Catch:{ all -> 0x006f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x006f }
            r3 = 1
            r0.log(r1, r2, r3)     // Catch:{ all -> 0x006f }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener> r0 = r7.mLocationToRvListener     // Catch:{ all -> 0x006f }
            r0.put(r9, r12)     // Catch:{ all -> 0x006f }
            java.lang.String r4 = "RV"
            java.lang.String r12 = "appID"
            java.lang.String r5 = r11.optString(r12)     // Catch:{ all -> 0x006f }
            java.lang.String r12 = "appSignature"
            java.lang.String r6 = r11.optString(r12)     // Catch:{ all -> 0x006f }
            r1 = r7
            r2 = r8
            r3 = r10
            r1.init(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x006f }
            boolean r8 = r7.mDidInitSuccessfully     // Catch:{ all -> 0x006f }
            if (r8 == 0) goto L_0x0065
            com.chartboost.sdk.Chartboost.cacheRewardedVideo(r9)     // Catch:{ all -> 0x006f }
        L_0x0065:
            monitor-exit(r7)
            return
        L_0x0067:
            if (r12 == 0) goto L_0x006d
            r8 = 0
            r12.onRewardedVideoAvailabilityChanged(r8)     // Catch:{ all -> 0x006f }
        L_0x006d:
            monitor-exit(r7)
            return
        L_0x006f:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.adapters.chartboost.ChartboostAdapter.initRewardedVideo(android.app.Activity, java.lang.String, java.lang.String, org.json.JSONObject, com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener):void");
    }

    public synchronized void fetchRewardedVideo(JSONObject jSONObject) {
        String locationId = getLocationId(jSONObject);
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + " fetchRewardedVideo placementId: <" + locationId + ">", 1);
        if (!Chartboost.hasRewardedVideo(locationId)) {
            Chartboost.cacheRewardedVideo(locationId);
        } else if (this.mLocationToRvListener.containsKey(locationId)) {
            this.mLocationToRvListener.get(locationId).onRewardedVideoAvailabilityChanged(true);
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        String locationId = getLocationId(jSONObject);
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + " showRewardedVideo placementId: <" + locationId + ">", 1);
        if (Chartboost.hasRewardedVideo(locationId)) {
            Chartboost.showRewardedVideo(locationId);
            return;
        }
        Chartboost.cacheRewardedVideo(locationId);
        if (this.mLocationToRvListener.containsKey(locationId)) {
            this.mLocationToRvListener.get(locationId).onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
        }
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        return Chartboost.hasRewardedVideo(getLocationId(jSONObject));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0066, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0075, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void initInterstitial(android.app.Activity r8, java.lang.String r9, java.lang.String r10, org.json.JSONObject r11, com.ironsource.mediationsdk.sdk.InterstitialSmashListener r12) {
        /*
            r7 = this;
            monitor-enter(r7)
            java.lang.String r9 = "appID"
            java.lang.String r9 = r11.optString(r9)     // Catch:{ all -> 0x0076 }
            boolean r9 = android.text.TextUtils.isEmpty(r9)     // Catch:{ all -> 0x0076 }
            if (r9 != 0) goto L_0x0067
            java.lang.String r9 = "appSignature"
            java.lang.String r9 = r11.optString(r9)     // Catch:{ all -> 0x0076 }
            boolean r9 = android.text.TextUtils.isEmpty(r9)     // Catch:{ all -> 0x0076 }
            if (r9 == 0) goto L_0x001a
            goto L_0x0067
        L_0x001a:
            java.lang.String r9 = r7.getLocationId(r11)     // Catch:{ all -> 0x0076 }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = com.ironsource.mediationsdk.logger.IronSourceLoggerManager.getLogger()     // Catch:{ all -> 0x0076 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_API     // Catch:{ all -> 0x0076 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0076 }
            r2.<init>()     // Catch:{ all -> 0x0076 }
            java.lang.String r3 = r7.getProviderName()     // Catch:{ all -> 0x0076 }
            r2.append(r3)     // Catch:{ all -> 0x0076 }
            java.lang.String r3 = " initInterstitial placementId: <"
            r2.append(r3)     // Catch:{ all -> 0x0076 }
            r2.append(r9)     // Catch:{ all -> 0x0076 }
            java.lang.String r3 = ">"
            r2.append(r3)     // Catch:{ all -> 0x0076 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0076 }
            r3 = 1
            r0.log(r1, r2, r3)     // Catch:{ all -> 0x0076 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.sdk.InterstitialSmashListener> r0 = r7.mLocationToIsListener     // Catch:{ all -> 0x0076 }
            r0.put(r9, r12)     // Catch:{ all -> 0x0076 }
            java.lang.String r4 = "IS"
            java.lang.String r9 = "appID"
            java.lang.String r5 = r11.optString(r9)     // Catch:{ all -> 0x0076 }
            java.lang.String r9 = "appSignature"
            java.lang.String r6 = r11.optString(r9)     // Catch:{ all -> 0x0076 }
            r1 = r7
            r2 = r8
            r3 = r10
            r1.init(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0076 }
            boolean r8 = r7.mDidInitSuccessfully     // Catch:{ all -> 0x0076 }
            if (r8 == 0) goto L_0x0065
            r12.onInterstitialInitSuccess()     // Catch:{ all -> 0x0076 }
        L_0x0065:
            monitor-exit(r7)
            return
        L_0x0067:
            if (r12 == 0) goto L_0x0074
            java.lang.String r8 = "Missing params"
            java.lang.String r9 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r8 = com.ironsource.mediationsdk.utils.ErrorBuilder.buildInitFailedError(r8, r9)     // Catch:{ all -> 0x0076 }
            r12.onInterstitialInitFailed(r8)     // Catch:{ all -> 0x0076 }
        L_0x0074:
            monitor-exit(r7)
            return
        L_0x0076:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.adapters.chartboost.ChartboostAdapter.initInterstitial(android.app.Activity, java.lang.String, java.lang.String, org.json.JSONObject, com.ironsource.mediationsdk.sdk.InterstitialSmashListener):void");
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        Handler handler = new Handler(Looper.getMainLooper());
        final String locationId = getLocationId(jSONObject);
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + " loadInterstitial placementId: <" + locationId + ">", 1);
        handler.post(new Runnable() {
            public void run() {
                Chartboost.cacheInterstitial(locationId);
            }
        });
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        String locationId = getLocationId(jSONObject);
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + " showInterstitial placementId: <" + locationId + ">", 1);
        if (Chartboost.hasInterstitial(locationId)) {
            Chartboost.showInterstitial(locationId);
        } else if (this.mLocationToIsListener.containsKey(locationId)) {
            this.mLocationToIsListener.get(locationId).onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
        }
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        return Chartboost.hasInterstitial(getLocationId(jSONObject));
    }

    private class CbDelegate extends ChartboostDelegate {
        private CbDelegate() {
        }

        public void didDisplayRewardedVideo(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didDisplayRewardedVideo placementId: <" + str + ">", 1);
            if (ChartboostAdapter.this.mLocationToRvListener.get(str) != null) {
                ((RewardedVideoSmashListener) ChartboostAdapter.this.mLocationToRvListener.get(str)).onRewardedVideoAdOpened();
            }
        }

        public void didCacheRewardedVideo(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didCacheRewardedVideo placementId: <" + str + ">", 1);
            if (ChartboostAdapter.this.mLocationToRvListener.get(str) != null) {
                ((RewardedVideoSmashListener) ChartboostAdapter.this.mLocationToRvListener.get(str)).onRewardedVideoAvailabilityChanged(true);
            }
        }

        public void didFailToLoadRewardedVideo(String str, CBError.CBImpressionError cBImpressionError) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, "didFailToLoadRewardedVideo placementId: <" + str + "> error: " + cBImpressionError.toString(), 1);
            if (ChartboostAdapter.this.mLocationToRvListener.get(str) != null) {
                ((RewardedVideoSmashListener) ChartboostAdapter.this.mLocationToRvListener.get(str)).onRewardedVideoAvailabilityChanged(Chartboost.hasRewardedVideo(str));
            }
        }

        public void didDismissRewardedVideo(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didDismissRewardedVideo placementId: <" + str + ">", 1);
            if (ChartboostAdapter.this.mLocationToRvListener.get(str) != null) {
                ((RewardedVideoSmashListener) ChartboostAdapter.this.mLocationToRvListener.get(str)).onRewardedVideoAdClosed();
            }
        }

        public void didCloseRewardedVideo(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didCloseRewardedVideo placementId: <" + str + ">", 1);
        }

        public void didClickRewardedVideo(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didClickRewardedVideo placementId: <" + str + ">", 1);
            if (ChartboostAdapter.this.mLocationToRvListener.get(str) != null) {
                ((RewardedVideoSmashListener) ChartboostAdapter.this.mLocationToRvListener.get(str)).onRewardedVideoAdClicked();
            }
        }

        public void didCompleteRewardedVideo(String str, int i) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didCompleteRewardedVideo placementId: <" + str + ">", 1);
            if (ChartboostAdapter.this.mLocationToRvListener.get(str) != null) {
                ((RewardedVideoSmashListener) ChartboostAdapter.this.mLocationToRvListener.get(str)).onRewardedVideoAdRewarded();
            }
        }

        public void willDisplayVideo(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " willDisplayVideo placementId: <" + str + ">", 1);
        }

        public void didCacheInterstitial(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didCacheInterstitial placementId: <" + str + ">", 1);
            if (ChartboostAdapter.this.mLocationToIsListener.get(str) != null) {
                ((InterstitialSmashListener) ChartboostAdapter.this.mLocationToIsListener.get(str)).onInterstitialAdReady();
            }
        }

        public void didFailToLoadInterstitial(String str, CBError.CBImpressionError cBImpressionError) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didFailToLoadInterstitial placementId: <" + str + ">  error: " + cBImpressionError.toString(), 1);
            if (ChartboostAdapter.this.mLocationToIsListener.get(str) != null) {
                ((InterstitialSmashListener) ChartboostAdapter.this.mLocationToIsListener.get(str)).onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(cBImpressionError.toString()));
            }
        }

        public void didDismissInterstitial(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didDismissInterstitial placementId: <" + str + ">", 1);
            if (ChartboostAdapter.this.mLocationToIsListener.get(str) != null) {
                ((InterstitialSmashListener) ChartboostAdapter.this.mLocationToIsListener.get(str)).onInterstitialAdClosed();
            }
        }

        public void didCloseInterstitial(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didCloseInterstitial placementId: <" + str + ">", 1);
        }

        public void didClickInterstitial(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didClickInterstitial placementId: <" + str + ">", 1);
            if (ChartboostAdapter.this.mLocationToIsListener.get(str) != null) {
                ((InterstitialSmashListener) ChartboostAdapter.this.mLocationToIsListener.get(str)).onInterstitialAdClicked();
            }
        }

        public void didDisplayInterstitial(String str) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didDisplayInterstitial placementId: <" + str + ">", 1);
            if (ChartboostAdapter.this.mLocationToIsListener.get(str) != null) {
                ((InterstitialSmashListener) ChartboostAdapter.this.mLocationToIsListener.get(str)).onInterstitialAdOpened();
                ((InterstitialSmashListener) ChartboostAdapter.this.mLocationToIsListener.get(str)).onInterstitialAdShowSucceeded();
            }
        }

        public void didInitialize() {
            synchronized (this) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                logger.log(ironSourceTag, ChartboostAdapter.this.getProviderName() + " didInitialize", 1);
                boolean unused = ChartboostAdapter.this.mDidInitSuccessfully = true;
                for (String cacheRewardedVideo : ChartboostAdapter.this.mLocationToRvListener.keySet()) {
                    Chartboost.cacheRewardedVideo(cacheRewardedVideo);
                }
                for (String str : ChartboostAdapter.this.mLocationToIsListener.keySet()) {
                    ((InterstitialSmashListener) ChartboostAdapter.this.mLocationToIsListener.get(str)).onInterstitialInitSuccess();
                }
            }
        }
    }

    private String getLocationId(JSONObject jSONObject) {
        String optString = jSONObject.optString("adLocation");
        return TextUtils.isEmpty(optString) ? CBLocation.LOCATION_DEFAULT : optString;
    }
}
