package com.ironsource.adapters.adcolony;

import android.app.Activity;
import android.text.TextUtils;
import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.adcolony.sdk.AdColonyZone;
import com.facebook.appevents.AppEventsConstants;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

class AdColonyAdapter extends AbstractAdapter {
    private static final String GitHash = "cc537bda7";
    private static final String VERSION = "4.1.9";
    private final String ALL_ZONE_IDS = "zoneIds";
    private final String APP_ID = "appID";
    private final String ZONE_ID = "zoneId";
    /* access modifiers changed from: private */
    public AdColonyInterstitialListener mAdColonyInterstitialListener;
    private AdColonyAppOptions mAdColonyOptions = new AdColonyAppOptions();
    private AdColonyRewardListener mAdColonyRewardListener;
    /* access modifiers changed from: private */
    public AdColonyInterstitialListener mAdColonyRewardedVideoListener;
    private AtomicBoolean mAlreadyInitiated = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, InterstitialSmashListener> mZoneIdToIsListener = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, RewardedVideoSmashListener> mZoneIdToRvListener = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AdColonyInterstitial> mZoneToAdMap = new ConcurrentHashMap<>();

    public String getVersion() {
        return VERSION;
    }

    public static AdColonyAdapter startAdapter(String str) {
        return new AdColonyAdapter(str);
    }

    private AdColonyAdapter(String str) {
        super(str);
    }

    public static IntegrationData getIntegrationData(Activity activity) {
        IntegrationData integrationData = new IntegrationData("AdColony", VERSION);
        integrationData.activities = new String[]{"com.adcolony.sdk.AdColonyInterstitialActivity"};
        return integrationData;
    }

    public String getCoreSDKVersion() {
        return AdColony.getSDKVersion();
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        this.mAdColonyOptions.setGDPRConsentString(z ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        this.mAdColonyOptions.setGDPRRequired(true);
        if (this.mAlreadyInitiated.get()) {
            AdColony.setAppOptions(this.mAdColonyOptions);
        }
    }

    public void setAge(int i) {
        try {
            AdColony.getAppOptions().getUserMetadata().setUserAge(i);
        } catch (Exception unused) {
        }
    }

    public void setGender(String str) {
        try {
            AdColony.getAppOptions().getUserMetadata().setUserGender(str);
        } catch (Exception unused) {
        }
    }

    private void init(Activity activity, String str, String str2, String[] strArr) {
        if (this.mAlreadyInitiated.compareAndSet(false, true)) {
            this.mAdColonyOptions.setUserID(str);
            AdColony.configure(activity, this.mAdColonyOptions, str2, strArr);
        }
    }

    private void loadRewardedVideo(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, getProviderName() + " loadRewardedVideo with " + str, 0);
        if (this.mAdColonyRewardListener == null) {
            this.mAdColonyRewardListener = new AdColonyRewardListener() {
                public void onReward(AdColonyReward adColonyReward) {
                    IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    logger.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :RewardedVideo:onReward", 0);
                    try {
                        RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) AdColonyAdapter.this.mZoneIdToRvListener.get(adColonyReward.getZoneID());
                        if (adColonyReward.success() && rewardedVideoSmashListener != null) {
                            rewardedVideoSmashListener.onRewardedVideoAdRewarded();
                        }
                    } catch (Throwable th) {
                        IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
                        IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                        logger2.logException(ironSourceTag2, AdColonyAdapter.this.getProviderName() + ":onReward()", th);
                    }
                }
            };
        }
        if (this.mAdColonyRewardedVideoListener == null) {
            this.mAdColonyRewardedVideoListener = new AdColonyInterstitialListener() {
                public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
                    AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :RewardedVideo:onRequestFilled():", 0);
                    if (adColonyInterstitial != null && !TextUtils.isEmpty(adColonyInterstitial.getZoneID())) {
                        AdColonyAdapter.this.mZoneToAdMap.put(adColonyInterstitial.getZoneID(), adColonyInterstitial);
                        if (AdColonyAdapter.this.mZoneIdToRvListener.containsKey(adColonyInterstitial.getZoneID())) {
                            ((RewardedVideoSmashListener) AdColonyAdapter.this.mZoneIdToRvListener.get(adColonyInterstitial.getZoneID())).onRewardedVideoAvailabilityChanged(true);
                        }
                    }
                }

                public void onRequestNotFilled(AdColonyZone adColonyZone) {
                    AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :RewardedVideo:onRequestNotFilled():zone: " + adColonyZone.getZoneID(), 0);
                    RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) AdColonyAdapter.this.mZoneIdToRvListener.get(adColonyZone.getZoneID());
                    if (rewardedVideoSmashListener != null) {
                        rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
                    }
                }

                public void onOpened(AdColonyInterstitial adColonyInterstitial) {
                    AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + ":RewardedVideo:onOpened():", 0);
                    RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) AdColonyAdapter.this.mZoneIdToRvListener.get(adColonyInterstitial.getZoneID());
                    if (rewardedVideoSmashListener != null) {
                        rewardedVideoSmashListener.onRewardedVideoAdOpened();
                        rewardedVideoSmashListener.onRewardedVideoAdStarted();
                    }
                }

                public void onClosed(AdColonyInterstitial adColonyInterstitial) {
                    AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + ":RewardedVideo:onClosed():", 0);
                    RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) AdColonyAdapter.this.mZoneIdToRvListener.get(adColonyInterstitial.getZoneID());
                    if (rewardedVideoSmashListener != null) {
                        rewardedVideoSmashListener.onRewardedVideoAdEnded();
                        rewardedVideoSmashListener.onRewardedVideoAdClosed();
                    }
                }

                public void onClicked(AdColonyInterstitial adColonyInterstitial) {
                    AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :RewardedVideo:onClicked():", 0);
                    RewardedVideoSmashListener rewardedVideoSmashListener = (RewardedVideoSmashListener) AdColonyAdapter.this.mZoneIdToRvListener.get(adColonyInterstitial.getZoneID());
                    if (rewardedVideoSmashListener != null) {
                        rewardedVideoSmashListener.onRewardedVideoAdClicked();
                    }
                }

                public void onExpiring(AdColonyInterstitial adColonyInterstitial) {
                    AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :RewardedVideo:onExpiring():", 0);
                    AdColony.requestInterstitial(adColonyInterstitial.getZoneID(), AdColonyAdapter.this.mAdColonyRewardedVideoListener);
                }
            };
        }
        AdColonyInterstitial adColonyInterstitial = this.mZoneToAdMap.get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.isExpired()) {
            AdColony.setRewardListener(this.mAdColonyRewardListener);
            AdColony.requestInterstitial(str, this.mAdColonyRewardedVideoListener);
        } else if (adColonyInterstitial != null && !adColonyInterstitial.isExpired() && this.mZoneIdToRvListener.containsKey(adColonyInterstitial.getZoneID())) {
            this.mZoneIdToRvListener.get(adColonyInterstitial.getZoneID()).onRewardedVideoAvailabilityChanged(true);
        }
    }

    public Map<String, Object> getRvBiddingData(JSONObject jSONObject) {
        return new HashMap();
    }

    public void initRvForBidding(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        if (rewardedVideoSmashListener != null) {
            try {
                String optString = jSONObject.optString("appID");
                String optString2 = jSONObject.optString("zoneId");
                String optString3 = jSONObject.optString("zoneIds");
                if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2)) {
                    if (!TextUtils.isEmpty(optString3)) {
                        this.mZoneIdToRvListener.put(optString2, rewardedVideoSmashListener);
                        init(activity, str2, optString, optString3.split(","));
                        rewardedVideoSmashListener.onRewardedVideoInitSuccess();
                        return;
                    }
                }
                rewardedVideoSmashListener.onRewardedVideoInitFailed(ErrorBuilder.buildInitFailedError("missing parameters", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            } catch (Exception e) {
                rewardedVideoSmashListener.onRewardedVideoInitFailed(ErrorBuilder.buildInitFailedError(e.getMessage(), IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
            }
        }
    }

    public void loadVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
        loadRewardedVideo(jSONObject.optString("zoneId"));
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        try {
            String optString = jSONObject.optString("appID");
            String optString2 = jSONObject.optString("zoneId");
            if (!TextUtils.isEmpty(optString2) && rewardedVideoSmashListener != null) {
                this.mZoneIdToRvListener.put(optString2, rewardedVideoSmashListener);
            }
            if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2)) {
                if (!TextUtils.isEmpty(jSONObject.optString("zoneIds"))) {
                    init(activity, str2, optString, jSONObject.optString("zoneIds").split(","));
                    loadRewardedVideo(optString2);
                    return;
                }
            }
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
            }
        } catch (Exception unused) {
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
            }
        }
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        loadRewardedVideo(jSONObject.optString("zoneId"));
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        try {
            String optString = jSONObject.optString("zoneId");
            AdColonyInterstitial adColonyInterstitial = this.mZoneToAdMap.get(optString);
            if (adColonyInterstitial == null || adColonyInterstitial.isExpired()) {
                if (rewardedVideoSmashListener != null) {
                    rewardedVideoSmashListener.onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                    rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
                }
                AdColony.requestInterstitial(optString, this.mAdColonyRewardedVideoListener);
                return;
            }
            adColonyInterstitial.show();
        } catch (Exception unused) {
            if (rewardedVideoSmashListener != null) {
                rewardedVideoSmashListener.onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
            }
        }
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        try {
            String optString = jSONObject.optString("zoneId");
            if (!TextUtils.isEmpty(optString) && this.mZoneToAdMap.get(optString) != null) {
                return !this.mZoneToAdMap.get(optString).isExpired();
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public Map<String, Object> getIsBiddingData(JSONObject jSONObject) {
        return new HashMap();
    }

    public void initInterstitialForBidding(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        initInterstitial(activity, str, str2, jSONObject, interstitialSmashListener);
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener, String str) {
        loadInterstitial(jSONObject, interstitialSmashListener);
    }

    public void initInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        try {
            String optString = jSONObject.optString("appID");
            if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(jSONObject.optString("zoneId"))) {
                if (!TextUtils.isEmpty(jSONObject.optString("zoneIds"))) {
                    if (!TextUtils.isEmpty(jSONObject.optString("zoneId")) && interstitialSmashListener != null) {
                        this.mZoneIdToIsListener.put(jSONObject.optString("zoneId"), interstitialSmashListener);
                    }
                    init(activity, str2, optString, jSONObject.optString("zoneIds").split(","));
                    interstitialSmashListener.onInterstitialInitSuccess();
                    return;
                }
            }
            if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("Missing params", "Interstitial"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        try {
            String optString = jSONObject.optString("zoneId");
            AdColonyInterstitial adColonyInterstitial = this.mZoneToAdMap.get(optString);
            if (!(adColonyInterstitial != null && !adColonyInterstitial.isExpired())) {
                if (this.mAdColonyInterstitialListener == null) {
                    this.mAdColonyInterstitialListener = new AdColonyInterstitialListener() {
                        public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
                            AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                            adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :Interstitial:onRequestFilled():", 0);
                            if (adColonyInterstitial != null && !TextUtils.isEmpty(adColonyInterstitial.getZoneID())) {
                                AdColonyAdapter.this.mZoneToAdMap.put(adColonyInterstitial.getZoneID(), adColonyInterstitial);
                            }
                            InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) AdColonyAdapter.this.mZoneIdToIsListener.get(adColonyInterstitial.getZoneID());
                            if (interstitialSmashListener != null) {
                                interstitialSmashListener.onInterstitialAdReady();
                            }
                        }

                        public void onRequestNotFilled(AdColonyZone adColonyZone) {
                            AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                            adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :Interstitial:onRequestNotFilled():zone: " + adColonyZone.getZoneID(), 0);
                            InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) AdColonyAdapter.this.mZoneIdToIsListener.get(adColonyZone.getZoneID());
                            if (interstitialSmashListener != null) {
                                interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError("Request Not Filled"));
                            }
                        }

                        public void onOpened(AdColonyInterstitial adColonyInterstitial) {
                            AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                            adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :Interstitial:onOpened():", 0);
                            InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) AdColonyAdapter.this.mZoneIdToIsListener.get(adColonyInterstitial.getZoneID());
                            if (interstitialSmashListener != null) {
                                interstitialSmashListener.onInterstitialAdOpened();
                                interstitialSmashListener.onInterstitialAdShowSucceeded();
                            }
                        }

                        public void onClosed(AdColonyInterstitial adColonyInterstitial) {
                            AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                            adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :Interstitial:onClosed():", 0);
                            InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) AdColonyAdapter.this.mZoneIdToIsListener.get(adColonyInterstitial.getZoneID());
                            if (interstitialSmashListener != null) {
                                interstitialSmashListener.onInterstitialAdClosed();
                                if (AdColonyAdapter.this.mZoneToAdMap.containsKey(adColonyInterstitial.getZoneID())) {
                                    AdColonyAdapter.this.mZoneToAdMap.remove(adColonyInterstitial.getZoneID());
                                }
                            }
                        }

                        public void onClicked(AdColonyInterstitial adColonyInterstitial) {
                            AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                            adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :Interstitial:onClicked():", 0);
                            InterstitialSmashListener interstitialSmashListener = (InterstitialSmashListener) AdColonyAdapter.this.mZoneIdToIsListener.get(adColonyInterstitial.getZoneID());
                            if (interstitialSmashListener != null) {
                                interstitialSmashListener.onInterstitialAdClicked();
                            }
                        }

                        public void onExpiring(AdColonyInterstitial adColonyInterstitial) {
                            AdColonyAdapter adColonyAdapter = AdColonyAdapter.this;
                            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                            adColonyAdapter.log(ironSourceTag, AdColonyAdapter.this.getProviderName() + " :Interstitial:onExpiring():", 0);
                            AdColony.requestInterstitial(adColonyInterstitial.getZoneID(), AdColonyAdapter.this.mAdColonyInterstitialListener);
                        }
                    };
                }
                if (adColonyInterstitial == null || adColonyInterstitial.isExpired()) {
                    AdColony.requestInterstitial(optString, this.mAdColonyInterstitialListener);
                }
            } else if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialAdReady();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        try {
            AdColonyInterstitial adColonyInterstitial = this.mZoneToAdMap.get(jSONObject.optString("zoneId"));
            if (adColonyInterstitial != null && !adColonyInterstitial.isExpired()) {
                adColonyInterstitial.show();
            } else if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        try {
            AdColonyInterstitial adColonyInterstitial = this.mZoneToAdMap.get(jSONObject.optString("zoneId"));
            if (adColonyInterstitial == null || adColonyInterstitial.isExpired()) {
                return false;
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }
}
