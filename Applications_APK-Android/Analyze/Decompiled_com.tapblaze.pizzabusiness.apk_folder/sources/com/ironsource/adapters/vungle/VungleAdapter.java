package com.ironsource.adapters.vungle;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.vungle.warren.AdConfig;
import com.vungle.warren.BuildConfig;
import com.vungle.warren.InitCallback;
import com.vungle.warren.LoadAdCallback;
import com.vungle.warren.PlayAdCallback;
import com.vungle.warren.Vungle;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

class VungleAdapter extends AbstractAdapter {
    private static final String APP_ID = "AppID";
    private static final String CONSENT_MESSAGE_VERSION = "1.0.0";
    private static final String GitHash = "e4a4735e4";
    private static final String PLACEMENT_ID = "PlacementId";
    private static final String VERSION = "4.1.6";
    private AtomicBoolean mInitCalled = new AtomicBoolean(false);
    private EInitState mInitState = EInitState.NOT_INIT;
    /* access modifiers changed from: private */
    public Set<String> mInitiatedAdUnits;
    /* access modifiers changed from: private */
    public Boolean mIsConsent = null;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, InterstitialSmashListener> mPlacementIdToInterstitialSmashListener = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, RewardedVideoSmashListener> mPlacementIdToRewardedVideoSmashListener = new ConcurrentHashMap<>();

    private enum EInitState {
        NOT_INIT,
        INIT_IN_PROGRESS,
        INIT_SUCCESS,
        INIT_FAIL
    }

    public static String getAdapterSDKVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public String getVersion() {
        return VERSION;
    }

    public void onPause(Activity activity) {
    }

    public void onResume(Activity activity) {
    }

    public static VungleAdapter startAdapter(String str) {
        return new VungleAdapter(str);
    }

    private VungleAdapter(String str) {
        super(str);
    }

    public static IntegrationData getIntegrationData(Activity activity) {
        IntegrationData integrationData = new IntegrationData("Vungle", VERSION);
        integrationData.validateWriteExternalStorage = true;
        return integrationData;
    }

    public String getCoreSDKVersion() {
        return getAdapterSDKVersion();
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        if (getCurrentInitState() == EInitState.INIT_SUCCESS) {
            Vungle.updateConsentStatus(z ? Vungle.Consent.OPTED_IN : Vungle.Consent.OPTED_OUT, CONSENT_MESSAGE_VERSION);
        } else {
            this.mIsConsent = Boolean.valueOf(z);
        }
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        if (!TextUtils.isEmpty(jSONObject.optString(APP_ID)) && !TextUtils.isEmpty(jSONObject.optString(PLACEMENT_ID))) {
            if (!TextUtils.isEmpty(jSONObject.optString(PLACEMENT_ID)) && rewardedVideoSmashListener != null) {
                this.mPlacementIdToRewardedVideoSmashListener.put(jSONObject.optString(PLACEMENT_ID), rewardedVideoSmashListener);
            }
            addInitiatedAdUnit(IronSourceConstants.REWARDED_VIDEO_AD_UNIT);
            int i = AnonymousClass6.$SwitchMap$com$ironsource$adapters$vungle$VungleAdapter$EInitState[getCurrentInitState().ordinal()];
            if (i == 1) {
                initVungleSdk(activity, jSONObject.optString(APP_ID));
            } else if (i == 2) {
            } else {
                if (i != 3) {
                    if (i == 4 && rewardedVideoSmashListener != null) {
                        rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
                    }
                } else if (!Vungle.canPlayAd(jSONObject.optString(PLACEMENT_ID))) {
                    loadRewardedVideoAd(jSONObject.optString(PLACEMENT_ID));
                } else if (rewardedVideoSmashListener != null) {
                    rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(true);
                }
            }
        } else if (rewardedVideoSmashListener != null) {
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
        }
    }

    /* renamed from: com.ironsource.adapters.vungle.VungleAdapter$6  reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {
        static final /* synthetic */ int[] $SwitchMap$com$ironsource$adapters$vungle$VungleAdapter$EInitState = new int[EInitState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.ironsource.adapters.vungle.VungleAdapter$EInitState[] r0 = com.ironsource.adapters.vungle.VungleAdapter.EInitState.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.ironsource.adapters.vungle.VungleAdapter.AnonymousClass6.$SwitchMap$com$ironsource$adapters$vungle$VungleAdapter$EInitState = r0
                int[] r0 = com.ironsource.adapters.vungle.VungleAdapter.AnonymousClass6.$SwitchMap$com$ironsource$adapters$vungle$VungleAdapter$EInitState     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.ironsource.adapters.vungle.VungleAdapter$EInitState r1 = com.ironsource.adapters.vungle.VungleAdapter.EInitState.NOT_INIT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.ironsource.adapters.vungle.VungleAdapter.AnonymousClass6.$SwitchMap$com$ironsource$adapters$vungle$VungleAdapter$EInitState     // Catch:{ NoSuchFieldError -> 0x001f }
                com.ironsource.adapters.vungle.VungleAdapter$EInitState r1 = com.ironsource.adapters.vungle.VungleAdapter.EInitState.INIT_IN_PROGRESS     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.ironsource.adapters.vungle.VungleAdapter.AnonymousClass6.$SwitchMap$com$ironsource$adapters$vungle$VungleAdapter$EInitState     // Catch:{ NoSuchFieldError -> 0x002a }
                com.ironsource.adapters.vungle.VungleAdapter$EInitState r1 = com.ironsource.adapters.vungle.VungleAdapter.EInitState.INIT_SUCCESS     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.ironsource.adapters.vungle.VungleAdapter.AnonymousClass6.$SwitchMap$com$ironsource$adapters$vungle$VungleAdapter$EInitState     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.ironsource.adapters.vungle.VungleAdapter$EInitState r1 = com.ironsource.adapters.vungle.VungleAdapter.EInitState.INIT_FAIL     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.adapters.vungle.VungleAdapter.AnonymousClass6.<clinit>():void");
        }
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        String optString = jSONObject.optString(PLACEMENT_ID);
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        log(ironSourceTag, getProviderName() + ": in fetchRewardedVideo for placementId " + optString, 0);
        if (TextUtils.isEmpty(optString)) {
            return;
        }
        if (Vungle.canPlayAd(optString)) {
            this.mPlacementIdToRewardedVideoSmashListener.get(optString).onRewardedVideoAvailabilityChanged(true);
        } else if (this.mPlacementIdToRewardedVideoSmashListener.containsKey(optString)) {
            loadRewardedVideoAd(optString);
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, final RewardedVideoSmashListener rewardedVideoSmashListener) {
        AdConfig adConfig = new AdConfig();
        if (Vungle.canPlayAd(jSONObject.optString(PLACEMENT_ID))) {
            if (!TextUtils.isEmpty(getDynamicUserId())) {
                Vungle.setIncentivizedFields(getDynamicUserId(), null, null, null, null);
            }
            Vungle.playAd(jSONObject.optString(PLACEMENT_ID), adConfig, new PlayAdCallback() {
                public void onAdStart(String str) {
                    VungleAdapter vungleAdapter = VungleAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": RewardedVideo ad started for placementReferenceId: " + str, 1);
                    RewardedVideoSmashListener rewardedVideoSmashListener = rewardedVideoSmashListener;
                    if (rewardedVideoSmashListener != null) {
                        rewardedVideoSmashListener.onRewardedVideoAdOpened();
                        rewardedVideoSmashListener.onRewardedVideoAdStarted();
                    }
                }

                public void onAdEnd(String str, boolean z, boolean z2) {
                    VungleAdapter vungleAdapter = VungleAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": RewardedVideo ad ended for placementReferenceId: " + str, 1);
                    RewardedVideoSmashListener rewardedVideoSmashListener = rewardedVideoSmashListener;
                    if (rewardedVideoSmashListener != null) {
                        if (z2) {
                            rewardedVideoSmashListener.onRewardedVideoAdClicked();
                        }
                        rewardedVideoSmashListener.onRewardedVideoAdEnded();
                        if (z) {
                            rewardedVideoSmashListener.onRewardedVideoAdRewarded();
                        }
                        rewardedVideoSmashListener.onRewardedVideoAdClosed();
                    }
                }

                public void onError(String str, Throwable th) {
                    VungleAdapter vungleAdapter = VungleAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": RewardedVideo ad failed to show for placementReferenceId: " + str + "error: " + th.getLocalizedMessage(), 1);
                    RewardedVideoSmashListener rewardedVideoSmashListener = rewardedVideoSmashListener;
                    if (rewardedVideoSmashListener != null) {
                        rewardedVideoSmashListener.onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                        rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
                    }
                }
            });
        }
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        return Vungle.isInitialized() && Vungle.canPlayAd(jSONObject.optString(PLACEMENT_ID));
    }

    public void initInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        if (!TextUtils.isEmpty(jSONObject.optString(APP_ID)) && !TextUtils.isEmpty(jSONObject.optString(PLACEMENT_ID))) {
            if (!TextUtils.isEmpty(jSONObject.optString(PLACEMENT_ID)) && interstitialSmashListener != null) {
                this.mPlacementIdToInterstitialSmashListener.put(jSONObject.optString(PLACEMENT_ID), interstitialSmashListener);
            }
            addInitiatedAdUnit("Interstitial");
            new HashSet();
            int i = AnonymousClass6.$SwitchMap$com$ironsource$adapters$vungle$VungleAdapter$EInitState[getCurrentInitState().ordinal()];
            if (i == 1) {
                initVungleSdk(activity, jSONObject.optString(APP_ID));
            } else if (i == 2) {
            } else {
                if (i != 3) {
                    if (i == 4 && interstitialSmashListener != null) {
                        interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("Init Failed", "Interstitial"));
                    }
                } else if (interstitialSmashListener != null) {
                    interstitialSmashListener.onInterstitialInitSuccess();
                }
            }
        } else if (interstitialSmashListener != null) {
            interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("Missing params", "Interstitial"));
        }
    }

    public void loadInterstitial(JSONObject jSONObject, final InterstitialSmashListener interstitialSmashListener) {
        if (Vungle.isInitialized()) {
            String optString = jSONObject.optString(PLACEMENT_ID);
            if (!Vungle.canPlayAd(optString)) {
                Vungle.loadAd(optString, new LoadAdCallback() {
                    public void onAdLoad(String str) {
                        VungleAdapter vungleAdapter = VungleAdapter.this;
                        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                        vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": Interstitial loaded for placementReferenceId: " + str, 1);
                        InterstitialSmashListener interstitialSmashListener = interstitialSmashListener;
                        if (interstitialSmashListener != null) {
                            interstitialSmashListener.onInterstitialAdReady();
                        }
                    }

                    public void onError(String str, Throwable th) {
                        VungleAdapter vungleAdapter = VungleAdapter.this;
                        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                        vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": Interstitial failed to load for placementReferenceId: " + str + " ,error: " + th.getLocalizedMessage(), 1);
                        InterstitialSmashListener interstitialSmashListener = interstitialSmashListener;
                        if (interstitialSmashListener != null) {
                            interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError("Error loading Ad: " + th.getLocalizedMessage()));
                        }
                    }
                });
            } else if (interstitialSmashListener != null) {
                interstitialSmashListener.onInterstitialAdReady();
            }
        }
    }

    public void showInterstitial(JSONObject jSONObject, final InterstitialSmashListener interstitialSmashListener) {
        if (Vungle.canPlayAd(jSONObject.optString(PLACEMENT_ID))) {
            Vungle.playAd(jSONObject.optString(PLACEMENT_ID), new AdConfig(), new PlayAdCallback() {
                public void onAdStart(String str) {
                    VungleAdapter vungleAdapter = VungleAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": Interstitial ad started for placementReferenceId: " + str, 1);
                    InterstitialSmashListener interstitialSmashListener = interstitialSmashListener;
                    if (interstitialSmashListener != null) {
                        interstitialSmashListener.onInterstitialAdOpened();
                        interstitialSmashListener.onInterstitialAdShowSucceeded();
                    }
                }

                public void onAdEnd(String str, boolean z, boolean z2) {
                    VungleAdapter vungleAdapter = VungleAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": Interstitial ad ended for placementReferenceId: " + str, 1);
                    InterstitialSmashListener interstitialSmashListener = interstitialSmashListener;
                    if (interstitialSmashListener != null) {
                        if (z2) {
                            interstitialSmashListener.onInterstitialAdClicked();
                        }
                        interstitialSmashListener.onInterstitialAdClosed();
                    }
                }

                public void onError(String str, Throwable th) {
                    VungleAdapter vungleAdapter = VungleAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": Interstitial ad failed to show for placementReferenceId: " + str + "error: " + th.getLocalizedMessage(), 1);
                    InterstitialSmashListener interstitialSmashListener = interstitialSmashListener;
                    if (interstitialSmashListener != null) {
                        interstitialSmashListener.onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
                    }
                }
            });
        } else if (interstitialSmashListener != null) {
            interstitialSmashListener.onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
        }
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        return Vungle.isInitialized() && Vungle.canPlayAd(jSONObject.optString(PLACEMENT_ID));
    }

    /* access modifiers changed from: private */
    public void setInitState(EInitState eInitState) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        log(ironSourceTag, getProviderName() + ":init state changed from " + this.mInitState + " to " + eInitState + ")", 1);
        this.mInitState = eInitState;
    }

    private EInitState getCurrentInitState() {
        return this.mInitState;
    }

    private void addInitiatedAdUnit(String str) {
        if (this.mInitiatedAdUnits == null) {
            this.mInitiatedAdUnits = new HashSet();
        }
        this.mInitiatedAdUnits.add(str);
    }

    private void initVungleSdk(Activity activity, String str) {
        if (this.mInitCalled.compareAndSet(false, true)) {
            setInitState(EInitState.INIT_IN_PROGRESS);
            Vungle.init(str, activity.getApplicationContext(), new InitCallback() {
                public void onSuccess() {
                    VungleAdapter vungleAdapter = VungleAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": Succeeded to initialize SDK ", 1);
                    VungleAdapter.this.setInitState(EInitState.INIT_SUCCESS);
                    if (VungleAdapter.this.mIsConsent != null) {
                        VungleAdapter vungleAdapter2 = VungleAdapter.this;
                        vungleAdapter2.setConsent(vungleAdapter2.mIsConsent.booleanValue());
                    }
                    if (VungleAdapter.this.mInitiatedAdUnits != null) {
                        if (VungleAdapter.this.mInitiatedAdUnits.contains(IronSourceConstants.REWARDED_VIDEO_AD_UNIT)) {
                            for (Map.Entry key : VungleAdapter.this.mPlacementIdToRewardedVideoSmashListener.entrySet()) {
                                VungleAdapter.this.loadRewardedVideoAd((String) key.getKey());
                            }
                        }
                        if (VungleAdapter.this.mInitiatedAdUnits.contains("Interstitial")) {
                            for (Map.Entry entry : VungleAdapter.this.mPlacementIdToInterstitialSmashListener.entrySet()) {
                                if (entry.getValue() != null) {
                                    ((InterstitialSmashListener) entry.getValue()).onInterstitialInitSuccess();
                                }
                            }
                        }
                    }
                }

                public void onError(Throwable th) {
                    VungleAdapter vungleAdapter = VungleAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": Failed to initialize SDK ", 1);
                    VungleAdapter.this.setInitState(EInitState.INIT_FAIL);
                    if (VungleAdapter.this.mInitiatedAdUnits != null) {
                        if (VungleAdapter.this.mInitiatedAdUnits.contains(IronSourceConstants.REWARDED_VIDEO_AD_UNIT)) {
                            for (Map.Entry entry : VungleAdapter.this.mPlacementIdToRewardedVideoSmashListener.entrySet()) {
                                if (entry.getValue() != null) {
                                    ((RewardedVideoSmashListener) entry.getValue()).onRewardedVideoAvailabilityChanged(false);
                                }
                            }
                        }
                        if (VungleAdapter.this.mInitiatedAdUnits.contains("Interstitial")) {
                            for (Map.Entry entry2 : VungleAdapter.this.mPlacementIdToInterstitialSmashListener.entrySet()) {
                                if (entry2.getValue() != null) {
                                    ((InterstitialSmashListener) entry2.getValue()).onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("Vungle failed to init: " + th.getLocalizedMessage(), "Interstitial"));
                                }
                            }
                        }
                    }
                }

                public void onAutoCacheAdAvailable(String str) {
                    VungleAdapter vungleAdapter = VungleAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": Cache ad is available for placementId " + str, 1);
                    if (VungleAdapter.this.mInitiatedAdUnits.contains(IronSourceConstants.REWARDED_VIDEO_AD_UNIT)) {
                        for (Map.Entry entry : VungleAdapter.this.mPlacementIdToRewardedVideoSmashListener.entrySet()) {
                            if (((String) entry.getKey()).equals(str) && entry.getValue() != null) {
                                ((RewardedVideoSmashListener) entry.getValue()).onRewardedVideoAvailabilityChanged(true);
                            }
                        }
                    }
                    if (VungleAdapter.this.mInitiatedAdUnits.contains("Interstitial")) {
                        for (Map.Entry entry2 : VungleAdapter.this.mPlacementIdToInterstitialSmashListener.entrySet()) {
                            if (((String) entry2.getKey()).equals(str) && entry2.getValue() != null) {
                                ((InterstitialSmashListener) entry2.getValue()).onInterstitialAdReady();
                            }
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void loadRewardedVideoAd(String str) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        log(ironSourceTag, getProviderName() + ": loadRewardedVideoAd placementId " + str, 1);
        Vungle.loadAd(str, new LoadAdCallback() {
            public void onAdLoad(String str) {
                VungleAdapter vungleAdapter = VungleAdapter.this;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": RewardedVideo Ad loaded for placementReferenceId: " + str, 1);
                if (VungleAdapter.this.mPlacementIdToRewardedVideoSmashListener.get(str) != null) {
                    ((RewardedVideoSmashListener) VungleAdapter.this.mPlacementIdToRewardedVideoSmashListener.get(str)).onRewardedVideoAvailabilityChanged(true);
                }
            }

            public void onError(String str, Throwable th) {
                VungleAdapter vungleAdapter = VungleAdapter.this;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                vungleAdapter.log(ironSourceTag, VungleAdapter.this.getProviderName() + ": RewardedVideo Ad failed to load for placementReferenceId: " + str + ", error: " + th.getLocalizedMessage(), 1);
                if (VungleAdapter.this.mPlacementIdToRewardedVideoSmashListener.get(str) != null) {
                    ((RewardedVideoSmashListener) VungleAdapter.this.mPlacementIdToRewardedVideoSmashListener.get(str)).onRewardedVideoAvailabilityChanged(false);
                }
            }
        });
    }
}
