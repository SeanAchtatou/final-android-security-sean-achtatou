package com.ironsource.adapters.admob;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.FrameLayout;
import com.adcolony.sdk.AdColonyAppOptions;
import com.google.android.gms.ads.AdActivity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.AdapterUtils;
import com.ironsource.mediationsdk.ISBannerSize;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.BannerSmashListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class AdMobAdapter extends AbstractAdapter {
    public static int BN_FAILED_TO_RELOAD_ERROR_CODE = 103;
    private static final String CORE_SDK_VERSION = "18.2.0";
    private static final String GitHash = "cff0a05c3";
    public static int IS_NOT_READY_ERROR_CODE = 104;
    public static int RV_NOT_READY_ERROR_CODE = 101;
    private static final String VERSION = "4.3.6";
    private final String AD_UNIT_ID = "adUnitId";
    private final String APP_ID = "appId";
    private final String IRONSOURCE_REQUEST_AGENT = AdColonyAppOptions.IRONSOURCE;
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AdView> mAdIdToBannerAd;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, InterstitialAd> mAdIdToIsAd;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, RewardedAd> mAdIdToRvAd;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, BannerSmashListener> mAdUnitIdToBnListener;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, InterstitialSmashListener> mAdUnitIdToIsListener;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, RewardedVideoSmashListener> mAdUnitIdToRvListener;
    private Boolean mConsent = null;
    /* access modifiers changed from: private */
    public Context mContext;
    private AtomicBoolean mDidInitSdk;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, Boolean> mInterstitialAdsAvailability;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, Boolean> mRvAdsAvailability;

    /* access modifiers changed from: private */
    public String getErrorString(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? "Unknown error" : "The ad request was successful, but no ad was returned due to lack of ad inventory" : "The ad request was unsuccessful due to network connectivity" : "The ad request was invalid" : "Internal error";
    }

    public String getCoreSDKVersion() {
        return CORE_SDK_VERSION;
    }

    public String getVersion() {
        return VERSION;
    }

    public static AdMobAdapter startAdapter(String str) {
        return new AdMobAdapter(str);
    }

    private AdMobAdapter(String str) {
        super(str);
        this.mAllBannerSmashes = new CopyOnWriteArrayList();
        this.mAdIdToIsAd = new ConcurrentHashMap<>();
        this.mInterstitialAdsAvailability = new ConcurrentHashMap<>();
        this.mAdIdToBannerAd = new ConcurrentHashMap<>();
        this.mAdUnitIdToRvListener = new ConcurrentHashMap<>();
        this.mAdIdToRvAd = new ConcurrentHashMap<>();
        this.mRvAdsAvailability = new ConcurrentHashMap<>();
        this.mAdUnitIdToIsListener = new ConcurrentHashMap<>();
        this.mAdUnitIdToBnListener = new ConcurrentHashMap<>();
        this.mDidInitSdk = new AtomicBoolean(false);
    }

    public static IntegrationData getIntegrationData(Activity activity) {
        IntegrationData integrationData = new IntegrationData(AdColonyAppOptions.ADMOB, VERSION);
        integrationData.activities = new String[]{AdActivity.CLASS_NAME};
        return integrationData;
    }

    public void setAge(int i) {
        int i2 = i < 13 ? 1 : 0;
        RequestConfiguration build = MobileAds.getRequestConfiguration().toBuilder().setTagForChildDirectedTreatment(i2).build();
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + "set coppaValue=" + i2, 1);
        MobileAds.setRequestConfiguration(build);
    }

    /* access modifiers changed from: private */
    public void initSDK(Context context, String str) {
        if (this.mDidInitSdk.compareAndSet(false, true)) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logger.log(ironSourceTag, getProviderName() + " initSDK", 1);
            MobileAds.initialize(context, new OnInitializationCompleteListener() {
                public void onInitializationComplete(InitializationStatus initializationStatus) {
                    IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + " sdk initialized", 0);
                }
            });
        }
    }

    public void initRewardedVideo(Activity activity, String str, String str2, final JSONObject jSONObject, final RewardedVideoSmashListener rewardedVideoSmashListener) {
        this.mActivity = activity;
        this.mContext = activity.getApplicationContext();
        if (TextUtils.isEmpty(jSONObject.optString("adUnitId"))) {
            rewardedVideoSmashListener.onRewardedVideoInitFailed(ErrorBuilder.buildInitFailedError("Missing params: 'adUnitId' ", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    AdMobAdapter adMobAdapter = AdMobAdapter.this;
                    adMobAdapter.initSDK(adMobAdapter.mContext, jSONObject.optString("appId"));
                    String optString = jSONObject.optString("adUnitId");
                    AdMobAdapter.this.mAdUnitIdToRvListener.put(optString, rewardedVideoSmashListener);
                    AdMobAdapter.this.loadRewardedVideoAdFromAdmob(optString);
                }
            });
        }
    }

    public void fetchRewardedVideo(final JSONObject jSONObject) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                AdMobAdapter.this.loadRewardedVideoAdFromAdmob(jSONObject.optString("adUnitId"));
            }
        });
    }

    public void onResume(Activity activity) {
        super.onResume(activity);
        this.mActivity = this.mActivity;
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        String optString = jSONObject.optString("adUnitId");
        return this.mRvAdsAvailability.containsKey(optString) && this.mRvAdsAvailability.get(optString).booleanValue();
    }

    public void showRewardedVideo(final JSONObject jSONObject, final RewardedVideoSmashListener rewardedVideoSmashListener) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                String optString = jSONObject.optString("adUnitId");
                RewardedAd rewardedAd = (RewardedAd) AdMobAdapter.this.mAdIdToRvAd.get(optString);
                if (AdMobAdapter.this.mActivity == null || rewardedAd == null || !rewardedAd.isLoaded()) {
                    RewardedVideoSmashListener rewardedVideoSmashListener = rewardedVideoSmashListener;
                    int i = AdMobAdapter.RV_NOT_READY_ERROR_CODE;
                    rewardedVideoSmashListener.onRewardedVideoAdShowFailed(new IronSourceError(i, AdMobAdapter.this.getProviderName() + "showRewardedVideo rv not ready " + optString));
                    rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
                    return;
                }
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + " RV show  " + optString, 1);
                AdMobAdapter.this.mRvAdsAvailability.put(optString, false);
                AdMobAdapter.this.mAdIdToRvAd.remove(rewardedAd);
                rewardedAd.show(AdMobAdapter.this.mActivity, AdMobAdapter.this.createRewardedAdCallback(jSONObject.optString("adUnitId")));
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
            }
        });
    }

    /* access modifiers changed from: private */
    public void loadRewardedVideoAdFromAdmob(String str) {
        RewardedAd rewardedAd = new RewardedAd(this.mContext, str);
        this.mAdIdToRvAd.put(str, rewardedAd);
        this.mRvAdsAvailability.put(str, false);
        AdRequest createAdRequest = createAdRequest();
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + " rv loadAd " + str, 1);
        rewardedAd.loadAd(createAdRequest, createRewardedAdLoadCallback(str));
    }

    public void initInterstitial(Activity activity, String str, String str2, final JSONObject jSONObject, final InterstitialSmashListener interstitialSmashListener) {
        this.mActivity = activity;
        this.mContext = activity.getApplicationContext();
        final String optString = jSONObject.optString("adUnitId");
        if (TextUtils.isEmpty(optString)) {
            interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("Missing params: 'adUnitId' ", "Interstitial"));
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    AdMobAdapter adMobAdapter = AdMobAdapter.this;
                    adMobAdapter.initSDK(adMobAdapter.mContext, jSONObject.optString("appId"));
                    InterstitialAd interstitialAd = new InterstitialAd(AdMobAdapter.this.mContext);
                    interstitialAd.setAdUnitId(optString);
                    AdMobAdapter.this.mAdIdToIsAd.put(optString, interstitialAd);
                    interstitialAd.setAdListener(AdMobAdapter.this.createInterstitialAdListener(jSONObject, optString));
                    AdMobAdapter.this.mAdUnitIdToIsListener.put(optString, interstitialSmashListener);
                    interstitialSmashListener.onInterstitialInitSuccess();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public RewardedAdCallback createRewardedAdCallback(final String str) {
        return new RewardedAdCallback() {
            public void onRewardedAdOpened() {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + " onRewardedAdOpened " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToRvListener.containsKey(str)) {
                    ((RewardedVideoSmashListener) AdMobAdapter.this.mAdUnitIdToRvListener.get(str)).onRewardedVideoAdOpened();
                }
            }

            public void onRewardedAdClosed() {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + " onRewardedAdClosed " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToRvListener.containsKey(str)) {
                    ((RewardedVideoSmashListener) AdMobAdapter.this.mAdUnitIdToRvListener.get(str)).onRewardedVideoAdClosed();
                }
            }

            public void onUserEarnedReward(RewardItem rewardItem) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + " onUserEarnedReward " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToRvListener.containsKey(str)) {
                    ((RewardedVideoSmashListener) AdMobAdapter.this.mAdUnitIdToRvListener.get(str)).onRewardedVideoAdRewarded();
                }
            }

            public void onRewardedAdFailedToShow(int i) {
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, AdMobAdapter.this.getProviderName() + " onRewardedAdFailedToShow: " + i + " " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToRvListener.containsKey(str)) {
                    ((RewardedVideoSmashListener) AdMobAdapter.this.mAdUnitIdToRvListener.get(str)).onRewardedVideoAdShowFailed(new IronSourceError(i, AdMobAdapter.this.getProviderName() + "onRewardedAdFailedToShow " + str + " " + (AdMobAdapter.this.getErrorString(i) + "( " + i + " )")));
                    ((RewardedVideoSmashListener) AdMobAdapter.this.mAdUnitIdToRvListener.get(str)).onRewardedVideoAvailabilityChanged(false);
                }
            }
        };
    }

    private RewardedAdLoadCallback createRewardedAdLoadCallback(final String str) {
        return new RewardedAdLoadCallback() {
            public void onRewardedAdLoaded() {
                AdMobAdapter.this.mRvAdsAvailability.put(str, true);
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + " onRewardedAdLoaded " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToRvListener.containsKey(str)) {
                    ((RewardedVideoSmashListener) AdMobAdapter.this.mAdUnitIdToRvListener.get(str)).onRewardedVideoAvailabilityChanged(true);
                }
            }

            public void onRewardedAdFailedToLoad(int i) {
                AdMobAdapter.this.mRvAdsAvailability.put(str, false);
                String str = AdMobAdapter.this.getErrorString(i) + "( " + i + " )";
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, AdMobAdapter.this.getProviderName() + " onRewardedAdFailedToLoad " + str + " " + str, 3);
                ((RewardedVideoSmashListener) AdMobAdapter.this.mAdUnitIdToRvListener.get(str)).onRewardedVideoAvailabilityChanged(false);
                try {
                    ((RewardedVideoSmashListener) AdMobAdapter.this.mAdUnitIdToRvListener.get(str)).onRewardedVideoLoadFailed(new IronSourceError(i, str));
                } catch (Throwable unused) {
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public AdListener createInterstitialAdListener(JSONObject jSONObject, final String str) {
        return new AdListener() {
            public void onAdClosed() {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, "onAdClosed " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToIsListener.containsKey(str)) {
                    ((InterstitialSmashListener) AdMobAdapter.this.mAdUnitIdToIsListener.get(str)).onInterstitialAdClosed();
                }
            }

            public void onAdFailedToLoad(int i) {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, "onAdFailedToLoad " + str, 1);
                AdMobAdapter.this.mInterstitialAdsAvailability.put(str, false);
                if (AdMobAdapter.this.mAdUnitIdToIsListener.containsKey(str)) {
                    ((InterstitialSmashListener) AdMobAdapter.this.mAdUnitIdToIsListener.get(str)).onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(AdMobAdapter.this.getErrorString(i) + "( " + i + " )"));
                }
            }

            public void onAdLeftApplication() {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, "onAdLeftApplication " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToIsListener.containsKey(str)) {
                    ((InterstitialSmashListener) AdMobAdapter.this.mAdUnitIdToIsListener.get(str)).onInterstitialAdClicked();
                }
            }

            public void onAdOpened() {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, "onAdOpened " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToIsListener.containsKey(str)) {
                    ((InterstitialSmashListener) AdMobAdapter.this.mAdUnitIdToIsListener.get(str)).onInterstitialAdOpened();
                    ((InterstitialSmashListener) AdMobAdapter.this.mAdUnitIdToIsListener.get(str)).onInterstitialAdShowSucceeded();
                }
            }

            public void onAdLoaded() {
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, "onAdLoaded " + str, 1);
                AdMobAdapter.this.mInterstitialAdsAvailability.put(str, true);
                if (AdMobAdapter.this.mAdUnitIdToIsListener.containsKey(str)) {
                    ((InterstitialSmashListener) AdMobAdapter.this.mAdUnitIdToIsListener.get(str)).onInterstitialAdReady();
                }
            }
        };
    }

    public void loadInterstitial(final JSONObject jSONObject, final InterstitialSmashListener interstitialSmashListener) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                String optString = jSONObject.optString("adUnitId");
                AdMobAdapter.this.mInterstitialAdsAvailability.put(optString, false);
                InterstitialAd access$1300 = AdMobAdapter.this.getInterstitialAd(jSONObject);
                if (access$1300 == null) {
                    InterstitialSmashListener interstitialSmashListener = interstitialSmashListener;
                    if (interstitialSmashListener != null) {
                        interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError("invalid adUnitId"));
                    }
                } else if (access$1300.isLoaded()) {
                    if (AdMobAdapter.this.mAdUnitIdToIsListener.containsKey(optString)) {
                        AdMobAdapter.this.mInterstitialAdsAvailability.put(optString, true);
                        ((InterstitialSmashListener) AdMobAdapter.this.mAdUnitIdToIsListener.get(optString)).onInterstitialAdReady();
                    }
                } else if (!access$1300.isLoading()) {
                    IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + " interstitial loadAd " + optString, 1);
                    access$1300.loadAd(AdMobAdapter.this.createAdRequest());
                }
            }
        });
    }

    public void showInterstitial(final JSONObject jSONObject, final InterstitialSmashListener interstitialSmashListener) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                InterstitialAd access$1300 = AdMobAdapter.this.getInterstitialAd(jSONObject);
                String optString = jSONObject.optString("adUnitId");
                if (access$1300 == null || !access$1300.isLoaded()) {
                    InterstitialSmashListener interstitialSmashListener = interstitialSmashListener;
                    if (interstitialSmashListener != null) {
                        int i = AdMobAdapter.IS_NOT_READY_ERROR_CODE;
                        interstitialSmashListener.onInterstitialAdShowFailed(new IronSourceError(i, AdMobAdapter.this.getProviderName() + "showInterstitial is not ready " + optString));
                        return;
                    }
                    return;
                }
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + " interstitial show " + optString, 1);
                access$1300.show();
                AdMobAdapter.this.mInterstitialAdsAvailability.put(jSONObject.optString("adUnitId"), false);
            }
        });
    }

    public final boolean isInterstitialReady(JSONObject jSONObject) {
        String optString = jSONObject.optString("adUnitId");
        if (this.mInterstitialAdsAvailability.get(optString) != null) {
            return this.mInterstitialAdsAvailability.get(optString).booleanValue();
        }
        return false;
    }

    public void initBanners(Activity activity, String str, String str2, final JSONObject jSONObject, final BannerSmashListener bannerSmashListener) {
        this.mActivity = activity;
        this.mContext = activity.getApplicationContext();
        if (TextUtils.isEmpty(jSONObject.optString("adUnitId"))) {
            bannerSmashListener.onBannerInitFailed(ErrorBuilder.buildInitFailedError("AdMobAdapter loadBanner adUnitId is empty", IronSourceConstants.BANNER_AD_UNIT));
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    AdMobAdapter adMobAdapter = AdMobAdapter.this;
                    adMobAdapter.initSDK(adMobAdapter.mContext, jSONObject.optString("appId"));
                    bannerSmashListener.onBannerInitSuccess();
                }
            });
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private AdSize getAdSize(ISBannerSize iSBannerSize, boolean z) {
        char c;
        String description = iSBannerSize.getDescription();
        switch (description.hashCode()) {
            case -387072689:
                if (description.equals("RECTANGLE")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 72205083:
                if (description.equals("LARGE")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 79011241:
                if (description.equals("SMART")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 1951953708:
                if (description.equals("BANNER")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 1999208305:
                if (description.equals("CUSTOM")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c == 0) {
            return AdSize.BANNER;
        }
        if (c == 1) {
            return AdSize.LARGE_BANNER;
        }
        if (c == 2) {
            return AdSize.MEDIUM_RECTANGLE;
        }
        if (c == 3) {
            return z ? AdSize.LEADERBOARD : AdSize.BANNER;
        }
        if (c != 4) {
            return null;
        }
        return new AdSize(iSBannerSize.getWidth(), iSBannerSize.getHeight());
    }

    /* access modifiers changed from: private */
    public AdListener createBannerAdListener(final AdView adView, final String str) {
        return new AdListener() {
            public void onAdClosed() {
                super.onAdClosed();
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, "onAdClosed " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToBnListener.containsKey(str)) {
                    ((BannerSmashListener) AdMobAdapter.this.mAdUnitIdToBnListener.get(str)).onBannerAdScreenDismissed();
                }
            }

            public void onAdFailedToLoad(int i) {
                IronSourceError ironSourceError;
                super.onAdFailedToLoad(i);
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "onAdFailedToLoad: " + i + " " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToBnListener.containsKey(str)) {
                    String str = AdMobAdapter.this.getErrorString(i) + "( " + i + " )";
                    if (i == 3) {
                        ironSourceError = new IronSourceError(IronSourceError.ERROR_BN_LOAD_NO_FILL, str);
                    } else {
                        ironSourceError = ErrorBuilder.buildLoadFailedError(str);
                    }
                    ((BannerSmashListener) AdMobAdapter.this.mAdUnitIdToBnListener.get(str)).onBannerAdLoadFailed(ironSourceError);
                }
            }

            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, "onAdLeftApplication " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToBnListener.containsKey(str)) {
                    ((BannerSmashListener) AdMobAdapter.this.mAdUnitIdToBnListener.get(str)).onBannerAdClicked();
                    ((BannerSmashListener) AdMobAdapter.this.mAdUnitIdToBnListener.get(str)).onBannerAdLeftApplication();
                }
            }

            public void onAdOpened() {
                super.onAdOpened();
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, "onAdOpened " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToBnListener.containsKey(str)) {
                    ((BannerSmashListener) AdMobAdapter.this.mAdUnitIdToBnListener.get(str)).onBannerAdScreenPresented();
                }
            }

            public void onAdLoaded() {
                super.onAdLoaded();
                IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger.log(ironSourceTag, "onAdLoaded " + str, 1);
                if (AdMobAdapter.this.mAdUnitIdToBnListener.containsKey(str)) {
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
                    layoutParams.gravity = 17;
                    ((BannerSmashListener) AdMobAdapter.this.mAdUnitIdToBnListener.get(str)).onBannerAdLoaded(adView, layoutParams);
                }
            }
        };
    }

    public void loadBanner(IronSourceBannerLayout ironSourceBannerLayout, JSONObject jSONObject, BannerSmashListener bannerSmashListener) {
        if (ironSourceBannerLayout == null) {
            IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "AdMob loadBanner banner == null", 3);
            return;
        }
        final String optString = jSONObject.optString("adUnitId");
        final AdSize adSize = getAdSize(ironSourceBannerLayout.getSize(), AdapterUtils.isLargeScreen(ironSourceBannerLayout.getActivity()));
        if (adSize == null) {
            bannerSmashListener.onBannerAdLoadFailed(ErrorBuilder.unsupportedBannerSize(AdColonyAppOptions.ADMOB));
            return;
        }
        final IronSourceBannerLayout ironSourceBannerLayout2 = ironSourceBannerLayout;
        final BannerSmashListener bannerSmashListener2 = bannerSmashListener;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    AdView adView = new AdView(ironSourceBannerLayout2.getActivity());
                    adView.setAdSize(adSize);
                    adView.setAdUnitId(optString);
                    adView.setAdListener(AdMobAdapter.this.createBannerAdListener(adView, optString));
                    AdMobAdapter.this.mAdIdToBannerAd.put(optString, adView);
                    AdMobAdapter.this.mAdUnitIdToBnListener.put(optString, bannerSmashListener2);
                    adView.loadAd(AdMobAdapter.this.createAdRequest());
                    IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + "banner loadAd " + optString, 1);
                } catch (Exception e) {
                    bannerSmashListener2.onBannerAdLoadFailed(ErrorBuilder.buildLoadFailedError("AdMobAdapter loadBanner exception " + e.getMessage()));
                }
            }
        });
    }

    public void destroyBanner(final JSONObject jSONObject) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    String optString = jSONObject.optString("adUnitId");
                    if (AdMobAdapter.this.mAdIdToBannerAd.containsKey(optString)) {
                        ((AdView) AdMobAdapter.this.mAdIdToBannerAd.get(optString)).destroy();
                        AdMobAdapter.this.mAdIdToBannerAd.remove(optString);
                        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                        logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + "banner destroy " + optString, 1);
                    }
                } catch (Exception e) {
                    AdMobAdapter adMobAdapter = AdMobAdapter.this;
                    IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    adMobAdapter.log(ironSourceTag2, "AdMob destroyBanner() exception: " + e, 3);
                }
            }
        });
    }

    public void reloadBanner(final JSONObject jSONObject) {
        final String optString = jSONObject.optString("adUnitId");
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (AdMobAdapter.this.mAdIdToBannerAd.get(jSONObject.optString("adUnitId")) != null) {
                    AdRequest access$1400 = AdMobAdapter.this.createAdRequest();
                    AdView adView = (AdView) AdMobAdapter.this.mAdIdToBannerAd.get(jSONObject.optString("adUnitId"));
                    if (adView != null) {
                        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                        logger.log(ironSourceTag, AdMobAdapter.this.getProviderName() + "banner loadAd " + optString, 1);
                        adView.loadAd(access$1400);
                    } else if (AdMobAdapter.this.mAdUnitIdToBnListener.containsKey(optString)) {
                        int i = AdMobAdapter.BN_FAILED_TO_RELOAD_ERROR_CODE;
                        ((BannerSmashListener) AdMobAdapter.this.mAdUnitIdToBnListener.get(optString)).onBannerAdLoadFailed(new IronSourceError(i, AdMobAdapter.this.getProviderName() + "reloadBanner missing banner " + optString));
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public AdRequest createAdRequest() {
        AdRequest.Builder builder = new AdRequest.Builder();
        builder.setRequestAgent(AdColonyAppOptions.IRONSOURCE);
        Boolean bool = this.mConsent;
        if (bool != null && !bool.booleanValue()) {
            Bundle bundle = new Bundle();
            bundle.putString("npa", "1");
            builder.addNetworkExtrasBundle(com.google.ads.mediation.admob.AdMobAdapter.class, bundle);
        }
        return builder.build();
    }

    /* access modifiers changed from: private */
    public InterstitialAd getInterstitialAd(JSONObject jSONObject) {
        String optString = jSONObject.optString("adUnitId");
        if (TextUtils.isEmpty(optString) || !this.mAdIdToIsAd.containsKey(optString)) {
            return null;
        }
        return this.mAdIdToIsAd.get(optString);
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        this.mConsent = Boolean.valueOf(z);
    }
}
