package com.ironsource.adapters.ironsource;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.ServerProtocol;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.IronSourceAdInstance;
import com.ironsource.sdk.IronSourceAdInstanceBuilder;
import com.ironsource.sdk.IronSourceNetwork;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceAdapter extends AbstractAdapter {
    private static final int IS_LOAD_EXCEPTION = 1000;
    private static final int IS_SHOW_EXCEPTION = 1001;
    private static final int RV_LOAD_EXCEPTION = 1002;
    private static final int RV_SHOW_EXCEPTION = 1003;
    private static final String VERSION = "6.11.0";
    private final String ADM_KEY = Constants.ParametersKeys.ADM;
    private final String APPLICATION_USER_AGE_GROUP = "applicationUserAgeGroup";
    private final String APPLICATION_USER_GENDER = "applicationUserGender";
    private final String CUSTOM_SEGMENT = "custom_Segment";
    private final String DEMAND_SOURCE_NAME = "demandSourceName";
    private final String DYNAMIC_CONTROLLER_CONFIG = Constants.RequestParameters.CONTROLLER_CONFIG;
    private final String DYNAMIC_CONTROLLER_DEBUG_MODE = "debugMode";
    private final String DYNAMIC_CONTROLLER_URL = "controllerUrl";
    private final String SDK_PLUGIN_TYPE = "SDKPluginType";
    private Context mContext;
    private ConcurrentHashMap<String, IronSourceAdInstance> mDemandSourceToISAd;
    private ConcurrentHashMap<String, IronSourceAdInstance> mDemandSourceToRvAd;
    private AtomicBoolean mDidInitSdk;
    private String mMediationSegment;
    private String mUserAgeGroup;
    private String mUserGender;

    public String getVersion() {
        return "6.11.0";
    }

    public static IronSourceAdapter startAdapter(String str) {
        return new IronSourceAdapter(str);
    }

    private IronSourceAdapter(String str) {
        super(str);
        log(str + ": new instance");
        this.mDemandSourceToISAd = new ConcurrentHashMap<>();
        this.mDemandSourceToRvAd = new ConcurrentHashMap<>();
        this.mUserAgeGroup = null;
        this.mUserGender = null;
        this.mMediationSegment = null;
        this.mDidInitSdk = new AtomicBoolean(false);
    }

    public String getCoreSDKVersion() {
        return SDKUtils.getSDKVersion();
    }

    public void onPause(Activity activity) {
        IronSourceNetwork.onPause(activity);
    }

    public void onResume(Activity activity) {
        IronSourceNetwork.onResume(activity);
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("setConsent (");
        sb.append(z ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false");
        sb.append(")");
        log(sb.toString());
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(Constants.RequestParameters.GDPR_CONSENT_STATUS, String.valueOf(z));
            IronSourceNetwork.updateConsentInfo(jSONObject);
        } catch (JSONException e) {
            logError("setConsent exception " + e.getMessage());
        }
    }

    public void setAge(int i) {
        if (i >= 13 && i <= 17) {
            this.mUserAgeGroup = "1";
        } else if (i >= 18 && i <= 20) {
            this.mUserAgeGroup = "2";
        } else if (i >= 21 && i <= 24) {
            this.mUserAgeGroup = "3";
        } else if (i >= 25 && i <= 34) {
            this.mUserAgeGroup = "4";
        } else if (i >= 35 && i <= 44) {
            this.mUserAgeGroup = "5";
        } else if (i >= 45 && i <= 54) {
            this.mUserAgeGroup = "6";
        } else if (i >= 55 && i <= 64) {
            this.mUserAgeGroup = "7";
        } else if (i <= 65 || i > 120) {
            this.mUserAgeGroup = AppEventsConstants.EVENT_PARAM_VALUE_NO;
        } else {
            this.mUserAgeGroup = "8";
        }
    }

    public void setGender(String str) {
        this.mUserGender = str;
    }

    public void setMediationSegment(String str) {
        this.mMediationSegment = str;
    }

    private HashMap<String, String> getInitParams() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(this.mUserAgeGroup)) {
            hashMap.put("applicationUserAgeGroup", this.mUserAgeGroup);
        }
        if (!TextUtils.isEmpty(this.mUserGender)) {
            hashMap.put("applicationUserGender", this.mUserGender);
        }
        String pluginType = getPluginType();
        if (!TextUtils.isEmpty(pluginType)) {
            hashMap.put("SDKPluginType", pluginType);
        }
        if (!TextUtils.isEmpty(this.mMediationSegment)) {
            hashMap.put("custom_Segment", this.mMediationSegment);
        }
        return hashMap;
    }

    public void earlyInit(Activity activity, String str, String str2, JSONObject jSONObject) {
        IronSourceUtils.sendAutomationLog(getDemandSourceName(jSONObject) + ": " + "earlyInit");
        initSDK(activity, str, str2, jSONObject);
    }

    public Map<String, Object> getIsBiddingData(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        String token = IronSourceNetwork.getToken(this.mContext);
        if (token != null) {
            hashMap.put("token", token);
        } else {
            logError("IS bidding token is null");
            hashMap.put("token", "");
        }
        return hashMap;
    }

    public void initInterstitialForBidding(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(jSONObject, "initInterstitialForBidding");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToISAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceInterstitialListener(interstitialSmashListener, demandSourceName)).setInAppBidding().build());
        interstitialSmashListener.onInterstitialInitSuccess();
    }

    public void initInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(jSONObject, Constants.JSMethods.INIT_INTERSTITIAL);
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToISAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceInterstitialListener(interstitialSmashListener, demandSourceName)).build());
        interstitialSmashListener.onInterstitialInitSuccess();
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener, String str) {
        log(jSONObject, Constants.JSMethods.LOAD_INTERSTITIAL);
        try {
            HashMap hashMap = new HashMap();
            hashMap.put(Constants.ParametersKeys.ADM, str);
            IronSourceNetwork.loadAd(this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            logError("loadInterstitial for bidding exception " + e.getMessage());
            interstitialSmashListener.onInterstitialAdLoadFailed(new IronSourceError(1000, e.getMessage()));
        }
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(jSONObject, Constants.JSMethods.LOAD_INTERSTITIAL);
        try {
            IronSourceNetwork.loadAd(this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)));
        } catch (Exception e) {
            logError("loadInterstitial exception " + e.getMessage());
            interstitialSmashListener.onInterstitialAdLoadFailed(new IronSourceError(1000, e.getMessage()));
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(jSONObject, Constants.JSMethods.SHOW_INTERSTITIAL);
        try {
            int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(2);
            HashMap hashMap = new HashMap();
            hashMap.put("sessionDepth", String.valueOf(sessionDepth));
            IronSourceNetwork.showAd(this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            logError("showInterstitial exception " + e.getMessage());
            interstitialSmashListener.onInterstitialAdShowFailed(new IronSourceError(1001, e.getMessage()));
        }
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        IronSourceAdInstance ironSourceAdInstance = this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject));
        return ironSourceAdInstance != null && IronSourceNetwork.isAdAvailableForInstance(ironSourceAdInstance);
    }

    public Map<String, Object> getRvBiddingData(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        String token = IronSourceNetwork.getToken(this.mContext);
        if (token != null) {
            hashMap.put("token", token);
        } else {
            logError("RV bidding token is null");
            hashMap.put("token", "");
        }
        return hashMap;
    }

    public void initRvForBidding(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, "initRvForBidding");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToRvAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceRewardedVideoListener(rewardedVideoSmashListener, demandSourceName)).setInAppBidding().setRewarded().build());
        rewardedVideoSmashListener.onRewardedVideoInitSuccess();
    }

    public void initRvForDemandOnly(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, "initRvForDemandOnly");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToRvAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceRewardedVideoListener(rewardedVideoSmashListener, demandSourceName, true)).setRewarded().build());
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, Constants.JSMethods.INIT_REWARDED_VIDEO);
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        this.mDemandSourceToRvAd.put(demandSourceName, new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceRewardedVideoListener(rewardedVideoSmashListener, demandSourceName)).setRewarded().build());
        fetchRewardedVideo(jSONObject);
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        log(jSONObject, "fetchRewardedVideo");
        IronSourceAdInstance ironSourceAdInstance = this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject));
        if (ironSourceAdInstance == null) {
            logError("fetchRewardedVideo exception: null adInstance ");
            return;
        }
        try {
            IronSourceNetwork.loadAd(ironSourceAdInstance);
        } catch (Exception e) {
            logError("fetchRewardedVideo exception " + e.getMessage());
            OnInterstitialListener adListener = ironSourceAdInstance.getAdListener();
            if (adListener != null) {
                adListener.onInterstitialLoadFailed("fetch exception");
            }
        }
    }

    public void loadVideoForDemandOnly(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, "loadVideoForDemandOnly");
        try {
            IronSourceNetwork.loadAd(this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject)));
        } catch (Exception e) {
            logError("loadVideoForDemandOnly exception " + e.getMessage());
            rewardedVideoSmashListener.onRewardedVideoLoadFailed(new IronSourceError(1002, e.getMessage()));
        }
    }

    public void loadVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
        log(jSONObject, "loadVideo (RV in bidding mode)");
        try {
            HashMap hashMap = new HashMap();
            hashMap.put(Constants.ParametersKeys.ADM, str);
            IronSourceNetwork.loadAd(this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            logError("loadVideo exception " + e.getMessage());
            rewardedVideoSmashListener.onRewardedVideoLoadFailed(new IronSourceError(1002, e.getMessage()));
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(jSONObject, Constants.JSMethods.SHOW_REWARDED_VIDEO);
        try {
            int sessionDepth = SessionDepthManager.getInstance().getSessionDepth(1);
            HashMap hashMap = new HashMap();
            hashMap.put("sessionDepth", String.valueOf(sessionDepth));
            IronSourceNetwork.showAd(this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            logError("showRewardedVideo exception " + e.getMessage());
            rewardedVideoSmashListener.onRewardedVideoAdShowFailed(new IronSourceError(1003, e.getMessage()));
        }
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        IronSourceAdInstance ironSourceAdInstance = this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject));
        return ironSourceAdInstance != null && IronSourceNetwork.isAdAvailableForInstance(ironSourceAdInstance);
    }

    private void initSDK(Activity activity, String str, String str2, JSONObject jSONObject) {
        if (activity == null) {
            logError("initSDK: null activity");
        } else if (this.mDidInitSdk.compareAndSet(false, true)) {
            if (isAdaptersDebugEnabled()) {
                SDKUtils.setDebugMode(3);
            } else {
                SDKUtils.setDebugMode(jSONObject.optInt("debugMode", 0));
            }
            SDKUtils.setControllerUrl(jSONObject.optString("controllerUrl"));
            SDKUtils.setControllerConfig(jSONObject.optString(Constants.RequestParameters.CONTROLLER_CONFIG, ""));
            this.mContext = activity.getApplicationContext();
            IronSourceNetwork.initSDK(activity, str, str2, getInitParams());
        }
    }

    private class IronSourceInterstitialListener implements OnInterstitialListener {
        private String mDemandSourceName;
        private InterstitialSmashListener mListener;

        IronSourceInterstitialListener(InterstitialSmashListener interstitialSmashListener, String str) {
            this.mDemandSourceName = str;
            this.mListener = interstitialSmashListener;
        }

        public void onInterstitialInitSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialInitSuccess");
        }

        public void onInterstitialInitFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialInitFailed");
        }

        public void onInterstitialLoadSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialLoadSuccess");
            this.mListener.onInterstitialAdReady();
        }

        public void onInterstitialLoadFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialLoadFailed " + str);
            this.mListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(str));
        }

        public void onInterstitialOpen() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialOpen");
            this.mListener.onInterstitialAdOpened();
        }

        public void onInterstitialAdRewarded(String str, int i) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialAdRewarded demandSourceId=" + str + " amount=" + i);
        }

        public void onInterstitialClose() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialClose");
            this.mListener.onInterstitialAdClosed();
        }

        public void onInterstitialShowSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialShowSuccess");
            this.mListener.onInterstitialAdShowSucceeded();
        }

        public void onInterstitialShowFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialShowFailed " + str);
            this.mListener.onInterstitialAdShowFailed(ErrorBuilder.buildShowFailedError("Interstitial", str));
        }

        public void onInterstitialClick() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialClick");
            this.mListener.onInterstitialAdClicked();
        }

        public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " interstitialListener onInterstitialEventNotificationReceived eventName=" + str);
            this.mListener.onInterstitialAdVisible();
        }
    }

    private class IronSourceRewardedVideoListener implements OnInterstitialListener {
        private String mDemandSourceName;
        boolean mIsRvDemandOnly;
        RewardedVideoSmashListener mListener;

        IronSourceRewardedVideoListener(RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
            this.mDemandSourceName = str;
            this.mListener = rewardedVideoSmashListener;
            this.mIsRvDemandOnly = false;
        }

        IronSourceRewardedVideoListener(RewardedVideoSmashListener rewardedVideoSmashListener, String str, boolean z) {
            this.mDemandSourceName = str;
            this.mListener = rewardedVideoSmashListener;
            this.mIsRvDemandOnly = z;
        }

        public void onInterstitialInitSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialInitSuccess");
        }

        public void onInterstitialInitFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialInitFailed");
        }

        public void onInterstitialLoadSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialLoadSuccess");
            if (this.mIsRvDemandOnly) {
                this.mListener.onRewardedVideoLoadSuccess();
            } else {
                this.mListener.onRewardedVideoAvailabilityChanged(true);
            }
        }

        public void onInterstitialLoadFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialLoadFailed " + str);
            if (this.mIsRvDemandOnly) {
                this.mListener.onRewardedVideoLoadFailed(ErrorBuilder.buildLoadFailedError(str));
            } else {
                this.mListener.onRewardedVideoAvailabilityChanged(false);
            }
        }

        public void onInterstitialOpen() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialOpen");
            this.mListener.onRewardedVideoAdOpened();
        }

        public void onInterstitialAdRewarded(String str, int i) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialAdRewarded demandSourceId=" + str + " amount=" + i);
            this.mListener.onRewardedVideoAdRewarded();
        }

        public void onInterstitialClose() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialClose");
            this.mListener.onRewardedVideoAdClosed();
        }

        public void onInterstitialShowSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialShowSuccess");
        }

        public void onInterstitialShowFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log("rewardedVideoListener onInterstitialShowSuccess " + str);
            this.mListener.onRewardedVideoAdShowFailed(ErrorBuilder.buildShowFailedError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT, str));
        }

        public void onInterstitialClick() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialClick");
            this.mListener.onRewardedVideoAdClicked();
        }

        public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            ironSourceAdapter.log(this.mDemandSourceName + " rewardedVideoListener onInterstitialEventNotificationReceived eventName=" + str);
            this.mListener.onRewardedVideoAdVisible();
        }
    }

    private String getDemandSourceName(JSONObject jSONObject) {
        if (!TextUtils.isEmpty(jSONObject.optString("demandSourceName"))) {
            return jSONObject.optString("demandSourceName");
        }
        return getProviderName();
    }

    private void logError(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "IronSourceAdapter: " + str, 3);
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "IronSourceAdapter: " + str, 0);
    }

    private void log(JSONObject jSONObject, String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, "IronSourceAdapter " + getDemandSourceName(jSONObject) + ": " + str, 0);
    }
}
