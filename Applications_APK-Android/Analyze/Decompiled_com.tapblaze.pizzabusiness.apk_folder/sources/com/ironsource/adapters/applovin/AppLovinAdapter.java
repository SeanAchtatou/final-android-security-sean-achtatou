package com.ironsource.adapters.applovin;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.FrameLayout;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinAdViewDisplayErrorCode;
import com.applovin.adview.AppLovinAdViewEventListener;
import com.applovin.adview.AppLovinIncentivizedInterstitial;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinPrivacySettings;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.AdapterUtils;
import com.ironsource.mediationsdk.ISBannerSize;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.BannerSmashListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import cz.msebera.android.httpclient.HttpStatus;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

class AppLovinAdapter extends AbstractAdapter {
    private static final String GitHash = "70281170f";
    private static final String SDK_KEY = "sdkKey";
    private static final String VERSION = "4.3.6";
    private static final String ZONE_ID = "zoneId";
    private Activity mActivity;
    /* access modifiers changed from: private */
    public AppLovinSdk mAppLovinSdk;
    private volatile Boolean mConsentCollectingUserData = null;
    /* access modifiers changed from: private */
    public Boolean mDidInitSdkFinished;
    private AtomicBoolean mDidInitSdkStarted;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, ALBannerListener> mZoneIdToAppLovinListener;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AppLovinAdView> mZoneIdToBannerAd;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, FrameLayout.LayoutParams> mZoneIdToBannerLayout;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, BannerSmashListener> mZoneIdToBannerSmashListener;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AppLovinAd> mZoneIdToIsAd;
    private ConcurrentHashMap<String, AppLovinInterstitialAdDialog> mZoneIdToIsAdDialog;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, Boolean> mZoneIdToIsAdReadyStatus;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, InterstitialSmashListener> mZoneIdToIsListener;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AppLovinIncentivizedInterstitial> mZoneIdToRvAd;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, RewardedVideoSmashListener> mZoneIdToRvListener;

    /* access modifiers changed from: private */
    public String getErrorString(int i) {
        return i != -600 ? i != -500 ? i != -400 ? i != -300 ? i != -6 ? i != -1 ? i != 204 ? i != -103 ? i != -102 ? "Unknown error" : "Ad fetch timeout" : "No network available" : "No fill" : "Unspecified error" : "Unable to render ad" : "No ad pre-loaded" : "Unknown server error" : "Server timeout" : "User closed video before reward";
    }

    public String getVersion() {
        return VERSION;
    }

    public static AppLovinAdapter startAdapter(String str) {
        return new AppLovinAdapter(str);
    }

    private AppLovinAdapter(String str) {
        super(str);
        this.mAllBannerSmashes = new CopyOnWriteArrayList();
        this.mZoneIdToAppLovinListener = new ConcurrentHashMap<>();
        this.mZoneIdToBannerSmashListener = new ConcurrentHashMap<>();
        this.mZoneIdToBannerLayout = new ConcurrentHashMap<>();
        this.mZoneIdToBannerAd = new ConcurrentHashMap<>();
        this.mZoneIdToIsAd = new ConcurrentHashMap<>();
        this.mZoneIdToIsAdDialog = new ConcurrentHashMap<>();
        this.mZoneIdToRvAd = new ConcurrentHashMap<>();
        this.mZoneIdToIsListener = new ConcurrentHashMap<>();
        this.mZoneIdToRvListener = new ConcurrentHashMap<>();
        this.mZoneIdToIsAdReadyStatus = new ConcurrentHashMap<>();
        this.mDidInitSdkStarted = new AtomicBoolean(false);
        this.mDidInitSdkFinished = false;
    }

    public static IntegrationData getIntegrationData(Activity activity) {
        IntegrationData integrationData = new IntegrationData("AppLovin", VERSION);
        integrationData.activities = new String[]{"com.applovin.adview.AppLovinInterstitialActivity", "com.applovin.sdk.AppLovinWebViewActivity"};
        return integrationData;
    }

    public static String getAdapterSDKVersion() {
        try {
            return AppLovinSdk.VERSION;
        } catch (Exception unused) {
            return null;
        }
    }

    public String getCoreSDKVersion() {
        return AppLovinSdk.VERSION;
    }

    public void onResume(Activity activity) {
        this.mActivity = activity;
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        this.mConsentCollectingUserData = Boolean.valueOf(z);
        if (this.mDidInitSdkStarted.get()) {
            AppLovinPrivacySettings.setHasUserConsent(z, this.mActivity);
        }
    }

    /* access modifiers changed from: private */
    public void initSdk(Activity activity, String str, String str2) {
        boolean z = false;
        if (this.mDidInitSdkStarted.compareAndSet(false, true)) {
            AppLovinSdkSettings appLovinSdkSettings = new AppLovinSdkSettings();
            try {
                z = isAdaptersDebugEnabled();
            } catch (NoSuchMethodError unused) {
            }
            appLovinSdkSettings.setVerboseLogging(z);
            this.mAppLovinSdk = AppLovinSdk.getInstance(str, appLovinSdkSettings, activity);
            this.mAppLovinSdk.setUserIdentifier(str2);
            this.mAppLovinSdk.initializeSdk(new AppLovinSdk.SdkInitializationListener() {
                public void onSdkInitialized(AppLovinSdkConfiguration appLovinSdkConfiguration) {
                    for (String access$100 : AppLovinAdapter.this.mZoneIdToIsListener.keySet()) {
                        AppLovinAdapter.this.createInterstitialAd(access$100);
                    }
                    for (String access$300 : AppLovinAdapter.this.mZoneIdToRvListener.keySet()) {
                        AppLovinAdapter.this.createRewardedAd(access$300);
                    }
                    for (BannerSmashListener onBannerInitSuccess : AppLovinAdapter.this.mZoneIdToBannerSmashListener.values()) {
                        onBannerInitSuccess.onBannerInitSuccess();
                    }
                    Boolean unused = AppLovinAdapter.this.mDidInitSdkFinished = true;
                }
            });
            if (this.mConsentCollectingUserData != null) {
                setConsent(this.mConsentCollectingUserData.booleanValue());
            }
        }
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        if (rewardedVideoSmashListener == null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            logger.log(ironSourceTag, getProviderName() + " RV init failed: RewardedVideoSmashListener is empty", 2);
            return;
        }
        final String optString = jSONObject.optString(SDK_KEY);
        if (TextUtils.isEmpty(optString)) {
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
            return;
        }
        final String zoneId = getZoneId(jSONObject);
        this.mActivity = activity;
        this.mZoneIdToRvListener.put(zoneId, rewardedVideoSmashListener);
        final Activity activity2 = activity;
        final String str3 = str2;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                AppLovinAdapter.this.initSdk(activity2, optString, str3);
                if (AppLovinAdapter.this.mDidInitSdkFinished.booleanValue()) {
                    AppLovinAdapter.this.createRewardedAd(zoneId);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void createRewardedAd(String str) {
        AppLovinIncentivizedInterstitial appLovinIncentivizedInterstitial;
        if (!TextUtils.isEmpty(str)) {
            appLovinIncentivizedInterstitial = AppLovinIncentivizedInterstitial.create(str, this.mAppLovinSdk);
        } else {
            appLovinIncentivizedInterstitial = AppLovinIncentivizedInterstitial.create(this.mAppLovinSdk);
        }
        this.mZoneIdToRvAd.put(str, appLovinIncentivizedInterstitial);
        loadRewardedVideo(str);
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        log(ironSourceTag, getProviderName() + ": in fetchRewardedVideo", 0);
        String zoneId = getZoneId(jSONObject);
        if (this.mZoneIdToRvAd.containsKey(zoneId)) {
            loadRewardedVideo(zoneId);
        }
    }

    private void loadRewardedVideo(String str) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        log(ironSourceTag, getProviderName() + ": in loadRewardedVideo", 0);
        this.mZoneIdToRvAd.get(str).preload(createAdLoadListener(str));
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        String zoneId = getZoneId(jSONObject);
        if (rewardedVideoSmashListener == null || (this.mZoneIdToRvAd.containsKey(zoneId) && this.mZoneIdToRvAd.get(zoneId).isAdReadyToDisplay())) {
            if (!TextUtils.isEmpty(getDynamicUserId())) {
                this.mAppLovinSdk.setUserIdentifier(getDynamicUserId());
            }
            this.mZoneIdToRvAd.get(zoneId).show(this.mActivity, new AppLovinAdRewardListener() {
                public void userRewardVerified(AppLovinAd appLovinAd, Map<String, String> map) {
                }

                public void userOverQuota(AppLovinAd appLovinAd, Map<String, String> map) {
                    IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "userOverQuota", 1);
                }

                public void userRewardRejected(AppLovinAd appLovinAd, Map<String, String> map) {
                    IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "userRewardRejected", 1);
                }

                public void validationRequestFailed(AppLovinAd appLovinAd, int i) {
                    IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    logger.log(ironSourceTag, "validationRequestFailed " + AppLovinAdapter.this.getErrorString(i) + "(" + i + ")", 1);
                }

                public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
                    IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "userDeclinedToViewAd", 1);
                    String access$800 = AppLovinAdapter.this.getZoneId(appLovinAd);
                    if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(access$800)) {
                        ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAdClosed();
                    }
                }
            }, new AppLovinAdVideoPlaybackListener() {
                public void videoPlaybackBegan(AppLovinAd appLovinAd) {
                    IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "videoPlaybackBegan", 1);
                    String access$800 = AppLovinAdapter.this.getZoneId(appLovinAd);
                    if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(access$800)) {
                        ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAdStarted();
                    }
                }

                public void videoPlaybackEnded(AppLovinAd appLovinAd, double d, boolean z) {
                    IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    logger.log(ironSourceTag, "videoPlaybackEnded ; isFullyWatched: " + z, 1);
                    String access$800 = AppLovinAdapter.this.getZoneId(appLovinAd);
                    if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(access$800)) {
                        ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAdEnded();
                        if (z) {
                            ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAdRewarded();
                        }
                    }
                }
            }, new AppLovinAdDisplayListener() {
                public void adDisplayed(AppLovinAd appLovinAd) {
                    IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "adDisplayed", 1);
                    String access$800 = AppLovinAdapter.this.getZoneId(appLovinAd);
                    if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(access$800)) {
                        ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAdOpened();
                    }
                }

                public void adHidden(AppLovinAd appLovinAd) {
                    IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "adHidden", 1);
                    final String access$800 = AppLovinAdapter.this.getZoneId(appLovinAd);
                    if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(access$800)) {
                        ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAvailabilityChanged(false);
                        ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAdClosed();
                    }
                    if (AppLovinAdapter.this.mZoneIdToRvAd.containsKey(access$800)) {
                        ((AppLovinIncentivizedInterstitial) AppLovinAdapter.this.mZoneIdToRvAd.get(access$800)).preload(new AppLovinAdLoadListener() {
                            public void adReceived(AppLovinAd appLovinAd) {
                                if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(access$800)) {
                                    ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAvailabilityChanged(true);
                                }
                            }

                            public void failedToReceiveAd(int i) {
                                if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(access$800)) {
                                    ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAvailabilityChanged(false);
                                }
                            }
                        });
                    }
                }
            }, new AppLovinAdClickListener() {
                public void adClicked(AppLovinAd appLovinAd) {
                    IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "adClicked", 1);
                    String access$800 = AppLovinAdapter.this.getZoneId(appLovinAd);
                    if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(access$800)) {
                        ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(access$800)).onRewardedVideoAdClicked();
                    }
                }
            });
            return;
        }
        rewardedVideoSmashListener.onRewardedVideoAdShowFailed(ErrorBuilder.buildNoAdsToShowError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
        rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
    }

    private AppLovinAdLoadListener createAdLoadListener(final String str) {
        return new AppLovinAdLoadListener() {
            public void adReceived(AppLovinAd appLovinAd) {
                if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(str)) {
                    ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(str)).onRewardedVideoAvailabilityChanged(true);
                }
            }

            public void failedToReceiveAd(int i) {
                String str = AppLovinAdapter.this.getErrorString(i) + "( " + i + " )";
                IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, AppLovinAdapter.this.getProviderName() + " failedToReceiveAd " + str + " " + str, 3);
                IronSourceError ironSourceError = new IronSourceError(i, str);
                if (AppLovinAdapter.this.mZoneIdToRvListener.containsKey(str)) {
                    ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(str)).onRewardedVideoAvailabilityChanged(false);
                }
                try {
                    ((RewardedVideoSmashListener) AppLovinAdapter.this.mZoneIdToRvListener.get(str)).onRewardedVideoLoadFailed(ironSourceError);
                } catch (Throwable unused) {
                }
            }
        };
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        String zoneId = getZoneId(jSONObject);
        return this.mZoneIdToRvAd.containsKey(zoneId) && this.mZoneIdToRvAd.get(zoneId).isAdReadyToDisplay();
    }

    public void initInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        if (interstitialSmashListener == null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            logger.log(ironSourceTag, getProviderName() + " IS init failed: InterstitialSmashListener is empty", 2);
            return;
        }
        final String optString = jSONObject.optString(SDK_KEY);
        if (TextUtils.isEmpty(optString)) {
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.INTERNAL;
            log(ironSourceTag2, getProviderName() + " initInterstitial empty sdkKey", 3);
            interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("initInterstitial empty sdkKey", "Interstitial"));
            return;
        }
        final String zoneId = getZoneId(jSONObject);
        this.mActivity = activity;
        this.mZoneIdToIsListener.put(zoneId, interstitialSmashListener);
        final Activity activity2 = activity;
        final String str3 = str2;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                AppLovinAdapter.this.initSdk(activity2, optString, str3);
                if (AppLovinAdapter.this.mDidInitSdkFinished.booleanValue()) {
                    AppLovinAdapter.this.createInterstitialAd(zoneId);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void createInterstitialAd(final String str) {
        AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(this.mAppLovinSdk, this.mActivity);
        this.mZoneIdToIsAdDialog.put(str, create);
        create.setAdClickListener(new AppLovinAdClickListener() {
            public void adClicked(AppLovinAd appLovinAd) {
                if (AppLovinAdapter.this.mZoneIdToIsListener.containsKey(str)) {
                    ((InterstitialSmashListener) AppLovinAdapter.this.mZoneIdToIsListener.get(str)).onInterstitialAdClicked();
                }
            }
        });
        create.setAdDisplayListener(new AppLovinAdDisplayListener() {
            public void adDisplayed(AppLovinAd appLovinAd) {
                if (AppLovinAdapter.this.mZoneIdToIsListener.containsKey(str)) {
                    ((InterstitialSmashListener) AppLovinAdapter.this.mZoneIdToIsListener.get(str)).onInterstitialAdOpened();
                    ((InterstitialSmashListener) AppLovinAdapter.this.mZoneIdToIsListener.get(str)).onInterstitialAdShowSucceeded();
                }
            }

            public void adHidden(AppLovinAd appLovinAd) {
                if (AppLovinAdapter.this.mZoneIdToIsListener.containsKey(str)) {
                    ((InterstitialSmashListener) AppLovinAdapter.this.mZoneIdToIsListener.get(str)).onInterstitialAdClosed();
                }
            }
        });
        if (this.mZoneIdToIsListener.containsKey(str)) {
            this.mZoneIdToIsListener.get(str).onInterstitialInitSuccess();
        }
    }

    public void loadInterstitial(JSONObject jSONObject, final InterstitialSmashListener interstitialSmashListener) {
        if (interstitialSmashListener == null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            logger.log(ironSourceTag, getProviderName() + " IS load failed: InterstitialSmashListener is empty", 2);
            return;
        }
        final String zoneId = getZoneId(jSONObject);
        IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger2.log(ironSourceTag2, getProviderName() + " loadInterstitial <" + zoneId + ">", 0);
        if (!TextUtils.isEmpty(zoneId)) {
            this.mAppLovinSdk.getAdService().loadNextAdForZoneId(zoneId, new AppLovinAdLoadListener() {
                public void adReceived(AppLovinAd appLovinAd) {
                    IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                    logger.log(ironSourceTag, "Interstitial adReceived zoneId=" + AppLovinAdapter.this.getZoneId(appLovinAd), 1);
                    interstitialSmashListener.onInterstitialAdReady();
                    AppLovinAdapter.this.mZoneIdToIsAd.put(zoneId, appLovinAd);
                    AppLovinAdapter.this.mZoneIdToIsAdReadyStatus.put(zoneId, true);
                }

                public void failedToReceiveAd(int i) {
                    interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(AppLovinAdapter.this.getErrorString(i) + "( " + i + " )"));
                    AppLovinAdapter.this.mZoneIdToIsAdReadyStatus.put(zoneId, false);
                }
            });
        } else {
            this.mAppLovinSdk.getAdService().loadNextAd(AppLovinAdSize.INTERSTITIAL, new AppLovinAdLoadListener() {
                public void adReceived(AppLovinAd appLovinAd) {
                    IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.INTERNAL, "Interstitial adReceived", 1);
                    interstitialSmashListener.onInterstitialAdReady();
                    AppLovinAdapter.this.mZoneIdToIsAd.put(zoneId, appLovinAd);
                    AppLovinAdapter.this.mZoneIdToIsAdReadyStatus.put(zoneId, true);
                }

                public void failedToReceiveAd(int i) {
                    interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(AppLovinAdapter.this.getErrorString(i) + "( " + i + " )"));
                    AppLovinAdapter.this.mZoneIdToIsAdReadyStatus.put(zoneId, false);
                }
            });
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        String zoneId = getZoneId(jSONObject);
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + " showInterstitial <" + zoneId + ">", 0);
        if (interstitialSmashListener == null || (this.mZoneIdToIsAd.containsKey(zoneId) && this.mZoneIdToIsAdDialog.containsKey(zoneId))) {
            this.mZoneIdToIsAdDialog.get(zoneId).showAndRender(this.mZoneIdToIsAd.get(zoneId));
            this.mZoneIdToIsAdReadyStatus.put(zoneId, false);
            return;
        }
        interstitialSmashListener.onInterstitialAdShowFailed(ErrorBuilder.buildNoAdsToShowError("Interstitial"));
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        String zoneId = getZoneId(jSONObject);
        return this.mZoneIdToIsAdDialog.containsKey(zoneId) && this.mZoneIdToIsAdReadyStatus.containsKey(zoneId) && this.mZoneIdToIsAdReadyStatus.get(zoneId).booleanValue();
    }

    public void initBanners(Activity activity, String str, String str2, JSONObject jSONObject, BannerSmashListener bannerSmashListener) {
        if (bannerSmashListener == null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            logger.log(ironSourceTag, getProviderName() + " initBanners listener == null", 3);
        }
        if (jSONObject == null || activity == null) {
            bannerSmashListener.onBannerInitFailed(ErrorBuilder.buildInitFailedError("null parameters", IronSourceConstants.BANNER_AD_UNIT));
            return;
        }
        final String optString = jSONObject.optString(SDK_KEY);
        if (TextUtils.isEmpty(optString)) {
            bannerSmashListener.onBannerInitFailed(ErrorBuilder.buildInitFailedError("Missing params", IronSourceConstants.BANNER_AD_UNIT));
            return;
        }
        String zoneId = getZoneId(jSONObject);
        this.mActivity = activity;
        this.mZoneIdToBannerSmashListener.put(zoneId, bannerSmashListener);
        final Activity activity2 = activity;
        final String str3 = str2;
        final BannerSmashListener bannerSmashListener2 = bannerSmashListener;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                AppLovinAdapter.this.initSdk(activity2, optString, str3);
                if (AppLovinAdapter.this.mDidInitSdkFinished.booleanValue()) {
                    bannerSmashListener2.onBannerInitSuccess();
                }
            }
        });
    }

    public void loadBanner(IronSourceBannerLayout ironSourceBannerLayout, JSONObject jSONObject, BannerSmashListener bannerSmashListener) {
        if (bannerSmashListener == null) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            logger.log(ironSourceTag, getProviderName() + " loadBanner listener == null", 3);
        } else if (ironSourceBannerLayout == null) {
            IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.INTERNAL;
            logger2.log(ironSourceTag2, getProviderName() + " loadBanner banner == null", 3);
        } else {
            final AppLovinAdSize calculateBannerSize = calculateBannerSize(ironSourceBannerLayout.getSize(), AdapterUtils.isLargeScreen(ironSourceBannerLayout.getActivity()));
            if (calculateBannerSize == null) {
                bannerSmashListener.onBannerAdLoadFailed(ErrorBuilder.unsupportedBannerSize(getProviderName()));
                return;
            }
            final String zoneId = getZoneId(jSONObject);
            IronSourceLoggerManager logger3 = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.INTERNAL;
            logger3.log(ironSourceTag3, getProviderName() + " loadBanner: " + getProviderName() + ", zoneID <" + zoneId + ">", 1);
            final IronSourceBannerLayout ironSourceBannerLayout2 = ironSourceBannerLayout;
            final BannerSmashListener bannerSmashListener2 = bannerSmashListener;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    AppLovinAdView appLovinAdView;
                    FrameLayout.LayoutParams layoutParams;
                    try {
                        if (TextUtils.isEmpty(zoneId)) {
                            layoutParams = AppLovinAdapter.this.calcLayoutParams(ironSourceBannerLayout2.getSize(), calculateBannerSize, ironSourceBannerLayout2.getActivity());
                            appLovinAdView = new AppLovinAdView(AppLovinAdapter.this.mAppLovinSdk, calculateBannerSize, ironSourceBannerLayout2.getActivity());
                        } else {
                            layoutParams = AppLovinAdapter.this.calcLayoutParams(ISBannerSize.BANNER, AppLovinAdSize.BANNER, ironSourceBannerLayout2.getActivity());
                            appLovinAdView = new AppLovinAdView(AppLovinAdapter.this.mAppLovinSdk, AppLovinAdSize.BANNER, ironSourceBannerLayout2.getActivity());
                        }
                        ALBannerListener aLBannerListener = new ALBannerListener(zoneId);
                        appLovinAdView.setAdLoadListener(aLBannerListener);
                        appLovinAdView.setAdClickListener(aLBannerListener);
                        appLovinAdView.setAdDisplayListener(aLBannerListener);
                        appLovinAdView.setAdViewEventListener(aLBannerListener);
                        AppLovinAdapter.this.mZoneIdToBannerAd.put(zoneId, appLovinAdView);
                        AppLovinAdapter.this.mZoneIdToBannerLayout.put(zoneId, layoutParams);
                        AppLovinAdapter.this.mZoneIdToAppLovinListener.put(zoneId, aLBannerListener);
                        if (TextUtils.isEmpty(zoneId)) {
                            appLovinAdView.loadNextAd();
                        } else {
                            AppLovinAdapter.this.mAppLovinSdk.getAdService().loadNextAdForZoneId(zoneId, aLBannerListener);
                        }
                    } catch (Exception e) {
                        bannerSmashListener2.onBannerAdLoadFailed(ErrorBuilder.buildLoadFailedError(AppLovinAdapter.this.getProviderName() + " loadBanner exception " + e.getMessage()));
                    }
                }
            });
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private AppLovinAdSize calculateBannerSize(ISBannerSize iSBannerSize, boolean z) {
        char c;
        String description = iSBannerSize.getDescription();
        switch (description.hashCode()) {
            case -387072689:
                if (description.equals("RECTANGLE")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 72205083:
                if (description.equals("LARGE")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 79011241:
                if (description.equals("SMART")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 1951953708:
                if (description.equals("BANNER")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 1999208305:
                if (description.equals("CUSTOM")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c == 0 || c == 1) {
            return AppLovinAdSize.BANNER;
        }
        if (c == 2) {
            return AppLovinAdSize.MREC;
        }
        if (c == 3) {
            return z ? AppLovinAdSize.LEADER : AppLovinAdSize.BANNER;
        }
        if (c == 4 && iSBannerSize.getHeight() >= 40 && iSBannerSize.getHeight() <= 60) {
            return AppLovinAdSize.BANNER;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public FrameLayout.LayoutParams calcLayoutParams(ISBannerSize iSBannerSize, AppLovinAdSize appLovinAdSize, Activity activity) {
        int i;
        if (iSBannerSize.getDescription().equals("RECTANGLE")) {
            i = HttpStatus.SC_MULTIPLE_CHOICES;
        } else {
            i = (!iSBannerSize.getDescription().equals("SMART") || !AdapterUtils.isLargeScreen(activity)) ? 320 : 728;
        }
        return new FrameLayout.LayoutParams(AdapterUtils.dpToPixels(activity, i), AdapterUtils.dpToPixels(activity, appLovinAdSize.getHeight()), 17);
    }

    public void destroyBanner(JSONObject jSONObject) {
        String zoneId = getZoneId(jSONObject);
        AppLovinAdView appLovinAdView = this.mZoneIdToBannerAd.get(zoneId);
        if (appLovinAdView != null) {
            appLovinAdView.destroy();
        }
        ConcurrentHashMap<String, AppLovinAdView> concurrentHashMap = this.mZoneIdToBannerAd;
        if (concurrentHashMap != null) {
            concurrentHashMap.remove(zoneId);
        }
    }

    public void reloadBanner(JSONObject jSONObject) {
        final String zoneId = getZoneId(jSONObject);
        final AppLovinAdView appLovinAdView = this.mZoneIdToBannerAd.get(zoneId);
        final ALBannerListener aLBannerListener = this.mZoneIdToAppLovinListener.get(zoneId);
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        logger.log(ironSourceTag, getProviderName() + "Banner reloadBanner: <" + zoneId + ">", 1);
        if (appLovinAdView == null || aLBannerListener == null) {
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_API;
            log(ironSourceTag2, getProviderName() + ":reloadBanner() failed, null parameters", 2);
            return;
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (TextUtils.isEmpty(zoneId)) {
                    appLovinAdView.loadNextAd();
                } else {
                    AppLovinAdapter.this.mAppLovinSdk.getAdService().loadNextAdForZoneId(zoneId, aLBannerListener);
                }
            }
        });
    }

    private class ALBannerListener implements AppLovinAdLoadListener, AppLovinAdDisplayListener, AppLovinAdClickListener, AppLovinAdViewEventListener {
        private String mZoneId;

        ALBannerListener(String str) {
            this.mZoneId = str;
        }

        public void adClicked(AppLovinAd appLovinAd) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            logger.log(ironSourceTag, AppLovinAdapter.this.getProviderName() + " Banner adClicked:  <" + this.mZoneId + ">", 1);
            BannerSmashListener bannerSmashListener = (BannerSmashListener) AppLovinAdapter.this.mZoneIdToBannerSmashListener.get(this.mZoneId);
            if (bannerSmashListener != null) {
                bannerSmashListener.onBannerAdClicked();
            }
        }

        public void adDisplayed(AppLovinAd appLovinAd) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            logger.log(ironSourceTag, AppLovinAdapter.this.getProviderName() + " Banner adDisplayed: <" + this.mZoneId + ">", 1);
        }

        public void adHidden(AppLovinAd appLovinAd) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            logger.log(ironSourceTag, AppLovinAdapter.this.getProviderName() + " Banner adHidden: <" + this.mZoneId + ">", 1);
        }

        public void adReceived(AppLovinAd appLovinAd) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            logger.log(ironSourceTag, AppLovinAdapter.this.getProviderName() + " Banner adReceived: <" + this.mZoneId + ">", 1);
            final AppLovinAdView appLovinAdView = (AppLovinAdView) AppLovinAdapter.this.mZoneIdToBannerAd.get(this.mZoneId);
            final FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) AppLovinAdapter.this.mZoneIdToBannerLayout.get(this.mZoneId);
            final BannerSmashListener bannerSmashListener = (BannerSmashListener) AppLovinAdapter.this.mZoneIdToBannerSmashListener.get(this.mZoneId);
            if (appLovinAdView == null || bannerSmashListener == null || layoutParams == null) {
                IronSourceLoggerManager logger2 = IronSourceLoggerManager.getLogger();
                IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
                logger2.log(ironSourceTag2, AppLovinAdapter.this.getProviderName() + " adReceived: null parameter", 3);
                return;
            }
            final AppLovinAd appLovinAd2 = appLovinAd;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    appLovinAdView.renderAd(appLovinAd2);
                    bannerSmashListener.onBannerAdLoaded(appLovinAdView, layoutParams);
                }
            });
        }

        public void failedToReceiveAd(int i) {
            IronSourceLoggerManager.getLogger().log(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, AppLovinAdapter.this.getProviderName() + " Banner failedToReceiveAd - errorCode = " + i, 1);
            BannerSmashListener bannerSmashListener = (BannerSmashListener) AppLovinAdapter.this.mZoneIdToBannerSmashListener.get(this.mZoneId);
            if (bannerSmashListener != null) {
                String str = AppLovinAdapter.this.getErrorString(i) + "(" + i + ")";
                bannerSmashListener.onBannerAdLoadFailed(i == 204 ? new IronSourceError(IronSourceError.ERROR_BN_LOAD_NO_FILL, str) : ErrorBuilder.buildLoadFailedError(str));
            }
        }

        public void adOpenedFullscreen(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            logger.log(ironSourceTag, AppLovinAdapter.this.getProviderName() + " Banner adOpenedFullscreen", 1);
        }

        public void adClosedFullscreen(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            logger.log(ironSourceTag, AppLovinAdapter.this.getProviderName() + " Banner adClosedFullscreen", 1);
        }

        public void adLeftApplication(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            logger.log(ironSourceTag, AppLovinAdapter.this.getProviderName() + " adLeftApplication:  <" + this.mZoneId + ">", 1);
            BannerSmashListener bannerSmashListener = (BannerSmashListener) AppLovinAdapter.this.mZoneIdToBannerSmashListener.get(this.mZoneId);
            if (bannerSmashListener != null) {
                bannerSmashListener.onBannerAdLeftApplication();
            }
        }

        public void adFailedToDisplay(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, AppLovinAdViewDisplayErrorCode appLovinAdViewDisplayErrorCode) {
            IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            logger.log(ironSourceTag, AppLovinAdapter.this.getProviderName() + " Banner adFailedToDisplay", 1);
        }
    }

    private String getZoneId(JSONObject jSONObject) {
        return !TextUtils.isEmpty(jSONObject.optString(ZONE_ID)) ? jSONObject.optString(ZONE_ID) : "";
    }

    /* access modifiers changed from: private */
    public String getZoneId(AppLovinAd appLovinAd) {
        return appLovinAd.getZoneId() != null ? appLovinAd.getZoneId() : "";
    }
}
