package com.ironsource.adapters.facebook;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.BidderTokenProvider;
import com.facebook.ads.BuildConfig;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.RewardData;
import com.facebook.ads.RewardedVideoAd;
import com.facebook.ads.RewardedVideoAdListener;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.AdapterUtils;
import com.ironsource.mediationsdk.ISBannerSize;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.config.VersionInfo;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.BannerSmashListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants;
import cz.msebera.android.httpclient.HttpStatus;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class FacebookAdapter extends AbstractAdapter {
    public static int BN_FAILED_TO_RELOAD_ERROR_CODE = 103;
    private static final String GitHash = "ef6353210";
    private static final String MEDIATION_SERVICE_NAME = "ironSource";
    private static final String VERSION = "4.3.8";
    private final String PLACEMENT_ID = "placementId";
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AdView> mBNPlacementToAdMap = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, BannerSmashListener> mBNPlacementToListenerMap = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public Context mContext;
    private AtomicBoolean mDidCallInit = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public Boolean mDidInitSuccess = null;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, InterstitialAd> mISPlacementToAdMap = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, FacebookInterstitialAdListener> mISPlacementToFBListener = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, InterstitialSmashListener> mISPlacementToListenerMap = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, Boolean> mInterstitialAdsAvailability = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public CopyOnWriteArraySet<String> mProgrammaticPlacements = new CopyOnWriteArraySet<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, RewardedVideoAd> mRVPlacementToAdMap = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, FacebookRewardedVideoAdListener> mRVPlacementToFBListener = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, RewardedVideoSmashListener> mRVPlacementToListenerMap = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, Boolean> mRvAdsAvailability = new ConcurrentHashMap<>();

    public static String getAdapterSDKVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public String getVersion() {
        return VERSION;
    }

    public static FacebookAdapter startAdapter(String str) {
        return new FacebookAdapter(str);
    }

    public static IntegrationData getIntegrationData(Activity activity) {
        IntegrationData integrationData = new IntegrationData("Facebook", VERSION);
        integrationData.activities = new String[]{"com.facebook.ads.AudienceNetworkActivity", "com.facebook.ads.internal.ipc.RemoteANActivity"};
        integrationData.services = new String[]{"com.facebook.ads.internal.ipc.AdsProcessPriorityService", "com.facebook.ads.internal.ipc.AdsMessengerService"};
        return integrationData;
    }

    private FacebookAdapter(String str) {
        super(str);
        AdSettings.setMediationService(getMediationServiceName());
    }

    private String getMediationServiceName() {
        String format = String.format("%s_%s:%s", "ironSource", VersionInfo.VERSION, VERSION);
        log("mediationServiceName is " + format);
        return format;
    }

    public String getCoreSDKVersion() {
        return getAdapterSDKVersion();
    }

    public Map<String, Object> getRvBiddingData(JSONObject jSONObject) {
        return getBiddingData();
    }

    public void initRvForBidding(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        if (rewardedVideoSmashListener == null) {
            logWarning("initRvForBidding failed: listener is null");
        } else if (activity == null) {
            logWarning("initRvForBidding failed: context is null");
            rewardedVideoSmashListener.onRewardedVideoInitFailed(ErrorBuilder.buildInitFailedError("Missing activity", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
        } else {
            String optString = jSONObject.optString("placementId");
            if (TextUtils.isEmpty(optString)) {
                logWarning("initRvForBidding failed: placementId is empty");
                rewardedVideoSmashListener.onRewardedVideoInitFailed(ErrorBuilder.buildInitFailedError("Missing placementId", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                return;
            }
            log("initRvForBidding <" + optString + ">");
            this.mRVPlacementToFBListener.put(optString, new FacebookRewardedVideoAdListener(rewardedVideoSmashListener, optString));
            this.mRVPlacementToListenerMap.put(optString, rewardedVideoSmashListener);
            this.mProgrammaticPlacements.add(optString);
            this.mContext = activity.getApplicationContext();
            initSdk();
            Boolean bool = this.mDidInitSuccess;
            if (bool == null) {
                return;
            }
            if (bool.booleanValue()) {
                log("initRvForBidding was successful for <" + optString + ">");
                rewardedVideoSmashListener.onRewardedVideoInitSuccess();
                return;
            }
            log("initRvForBidding failed for <" + optString + ">");
            rewardedVideoSmashListener.onRewardedVideoInitFailed(ErrorBuilder.buildInitFailedError("FAN Sdk failed to initiate", IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
        }
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        if (rewardedVideoSmashListener == null) {
            logWarning("initRewardedVideo failed: listener is null");
        } else if (activity == null) {
            logWarning("initRewardedVideo failed: context is null");
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
        } else {
            String optString = jSONObject.optString("placementId");
            if (TextUtils.isEmpty(optString)) {
                logWarning("initRewardedVideo failed: placementId is empty");
                rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
                return;
            }
            log("initRewardedVideo <" + optString + ">");
            this.mRVPlacementToFBListener.put(optString, new FacebookRewardedVideoAdListener(rewardedVideoSmashListener, optString));
            this.mRVPlacementToListenerMap.put(optString, rewardedVideoSmashListener);
            this.mContext = activity.getApplicationContext();
            initSdk();
            Boolean bool = this.mDidInitSuccess;
            if (bool == null) {
                return;
            }
            if (bool.booleanValue()) {
                loadRewardedVideo(optString);
                return;
            }
            log("initRewardedVideo failed for <" + optString + ">");
            rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
        }
    }

    public void loadVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
        log("fetchRewardedVideo as a bidder <" + jSONObject.optString("placementId") + ">");
        loadRewardedVideo(jSONObject.optString("placementId"), str);
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        log("fetchRewardedVideo <" + jSONObject.optString("placementId") + ">");
        loadRewardedVideo(jSONObject.optString("placementId"));
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        String optString = jSONObject.optString("placementId");
        return this.mRvAdsAvailability.containsKey(optString) && this.mRvAdsAvailability.get(optString).booleanValue();
    }

    public void showRewardedVideo(JSONObject jSONObject, final RewardedVideoSmashListener rewardedVideoSmashListener) {
        final String optString = jSONObject.optString("placementId");
        log("showRewardedVideo <" + optString + ">");
        this.mRvAdsAvailability.put(optString, false);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                String str;
                try {
                    RewardedVideoAd rewardedVideoAd = (RewardedVideoAd) FacebookAdapter.this.mRVPlacementToAdMap.get(optString);
                    if (rewardedVideoAd == null || !rewardedVideoAd.isAdLoaded() || rewardedVideoAd.isAdInvalidated()) {
                        if (rewardedVideoAd != null) {
                            str = "videoAd.isAdInvalidated() = " + rewardedVideoAd.isAdInvalidated();
                        } else {
                            str = "no ads to show";
                        }
                        IronSourceError buildShowFailedError = ErrorBuilder.buildShowFailedError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT, str);
                        FacebookAdapter.this.logError("showRewardedVideo <" + optString + "> - error = " + buildShowFailedError.getErrorMessage());
                        rewardedVideoSmashListener.onRewardedVideoAdShowFailed(buildShowFailedError);
                        rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
                    }
                    rewardedVideoAd.show();
                    rewardedVideoSmashListener.onRewardedVideoAvailabilityChanged(false);
                } catch (Exception e) {
                    FacebookAdapter.this.logError("rewarded video show failed - " + e.getMessage());
                    rewardedVideoSmashListener.onRewardedVideoAdShowFailed(ErrorBuilder.buildShowFailedError(IronSourceConstants.REWARDED_VIDEO_AD_UNIT, e.getMessage()));
                }
            }
        });
    }

    public Map<String, Object> getIsBiddingData(JSONObject jSONObject) {
        return getBiddingData();
    }

    public void initInterstitialForBidding(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log("initInterstitialForBidding: calling regular initInterstitial");
        initInterstitial(activity, str, str2, jSONObject, interstitialSmashListener);
    }

    public void initInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(Constants.JSMethods.INIT_INTERSTITIAL);
        if (interstitialSmashListener == null) {
            logWarning("Interstitial init failed: listener is null");
        } else if (activity == null) {
            logWarning("Interstitial init failed: activity is null");
            interstitialSmashListener.onInterstitialInitFailed(new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, "empty mContext"));
        } else {
            String optString = jSONObject.optString("placementId");
            if (TextUtils.isEmpty(optString)) {
                logWarning("Interstitial init failed: placement is empty");
                interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("Missing params", "Interstitial"));
                return;
            }
            log("initInterstitial <" + optString + ">");
            FacebookInterstitialAdListener facebookInterstitialAdListener = new FacebookInterstitialAdListener(interstitialSmashListener, optString);
            this.mContext = activity.getApplicationContext();
            this.mISPlacementToListenerMap.put(optString, interstitialSmashListener);
            this.mISPlacementToFBListener.put(optString, facebookInterstitialAdListener);
            initSdk();
            Boolean bool = this.mDidInitSuccess;
            if (bool == null) {
                return;
            }
            if (bool.booleanValue()) {
                interstitialSmashListener.onInterstitialInitSuccess();
            } else {
                interstitialSmashListener.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError("init failed", "Interstitial"));
            }
        }
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener, String str) {
        log("loadInterstitial as bidder <" + jSONObject.optString("placementId") + ">");
        loadInterstitial(interstitialSmashListener, jSONObject, str);
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log("loadInterstitial <" + jSONObject.optString("placementId") + ">");
        loadInterstitial(interstitialSmashListener, jSONObject, (String) null);
    }

    private void loadInterstitial(final InterstitialSmashListener interstitialSmashListener, JSONObject jSONObject, final String str) {
        final String optString = jSONObject.optString("placementId");
        if (TextUtils.isEmpty(optString)) {
            logWarning("loadInterstitial failed: placement is null");
            interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError("Empty placementId"));
            return;
        }
        this.mInterstitialAdsAvailability.put(optString, false);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    if (FacebookAdapter.this.mISPlacementToAdMap.containsKey(optString)) {
                        ((InterstitialAd) FacebookAdapter.this.mISPlacementToAdMap.get(optString)).destroy();
                        FacebookAdapter.this.mISPlacementToAdMap.remove(optString);
                    }
                    InterstitialAd interstitialAd = new InterstitialAd(FacebookAdapter.this.mContext, optString);
                    InterstitialAd.InterstitialAdLoadConfigBuilder buildLoadAdConfig = interstitialAd.buildLoadAdConfig();
                    buildLoadAdConfig.withAdListener((InterstitialAdListener) FacebookAdapter.this.mISPlacementToFBListener.get(optString));
                    if (str != null) {
                        buildLoadAdConfig.withBid(str);
                    }
                    interstitialAd.loadAd(buildLoadAdConfig.build());
                    FacebookAdapter.this.mISPlacementToAdMap.put(optString, interstitialAd);
                } catch (Exception e) {
                    interstitialSmashListener.onInterstitialAdLoadFailed(ErrorBuilder.buildLoadFailedError(e.getLocalizedMessage()));
                }
            }
        });
    }

    public void showInterstitial(JSONObject jSONObject, final InterstitialSmashListener interstitialSmashListener) {
        final String optString = jSONObject.optString("placementId");
        log("showInterstitial <" + optString + ">");
        this.mInterstitialAdsAvailability.put(optString, false);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                String str;
                try {
                    InterstitialAd interstitialAd = (InterstitialAd) FacebookAdapter.this.mISPlacementToAdMap.get(optString);
                    if (interstitialAd == null || !interstitialAd.isAdLoaded() || interstitialAd.isAdInvalidated()) {
                        if (interstitialAd != null) {
                            str = "interstitialAd.isAdInvalidated() = " + interstitialAd.isAdInvalidated();
                        } else {
                            str = "no ads to show";
                        }
                        IronSourceError buildShowFailedError = ErrorBuilder.buildShowFailedError("Interstitial", str);
                        FacebookAdapter.this.logError("showInterstitial <" + optString + "> error: " + buildShowFailedError.getErrorMessage());
                        interstitialSmashListener.onInterstitialAdShowFailed(buildShowFailedError);
                        return;
                    }
                    interstitialAd.show();
                } catch (Exception e) {
                    FacebookAdapter.this.logError("showInterstitial failed: " + e.getMessage());
                    interstitialSmashListener.onInterstitialAdShowFailed(ErrorBuilder.buildShowFailedError("Interstitial", e.getMessage()));
                }
            }
        });
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        String optString = jSONObject.optString("placementId");
        return this.mInterstitialAdsAvailability.containsKey(optString) && this.mInterstitialAdsAvailability.get(optString).booleanValue();
    }

    public void initBanners(Activity activity, String str, String str2, JSONObject jSONObject, BannerSmashListener bannerSmashListener) {
        if (bannerSmashListener == null) {
            logWarning("initBanners failed: listener is null");
        } else if (activity == null) {
            logWarning("initBanners failed: activity is null");
            bannerSmashListener.onBannerInitFailed(new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, "empty mContext"));
        } else {
            String optString = jSONObject.optString("placementId");
            log("initBanners <" + optString + ">");
            this.mContext = activity.getApplicationContext();
            this.mBNPlacementToListenerMap.put(optString, bannerSmashListener);
            initSdk();
            Boolean bool = this.mDidInitSuccess;
            if (bool == null) {
                return;
            }
            if (bool.booleanValue()) {
                bannerSmashListener.onBannerInitSuccess();
            } else {
                bannerSmashListener.onBannerInitFailed(new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, "init failed"));
            }
        }
    }

    public void loadBanner(IronSourceBannerLayout ironSourceBannerLayout, JSONObject jSONObject, BannerSmashListener bannerSmashListener) {
        if (bannerSmashListener == null) {
            logWarning("loadBanner: listener is null");
        } else if (ironSourceBannerLayout == null) {
            logWarning("loadBanner: banner is null");
            bannerSmashListener.onBannerAdLoadFailed(ErrorBuilder.buildLoadFailedError("banner layout is null"));
        } else {
            final String optString = jSONObject.optString("placementId");
            if (TextUtils.isEmpty(optString)) {
                logWarning("loadBanner: placement is empty");
                bannerSmashListener.onBannerAdLoadFailed(ErrorBuilder.buildLoadFailedError("FacebookAdapter loadBanner placementId is empty"));
                return;
            }
            final AdSize calculateBannerSize = calculateBannerSize(ironSourceBannerLayout.getSize(), AdapterUtils.isLargeScreen(ironSourceBannerLayout.getActivity()));
            if (calculateBannerSize == null) {
                bannerSmashListener.onBannerAdLoadFailed(ErrorBuilder.unsupportedBannerSize("Facebook"));
                return;
            }
            log("loadBanner <" + optString + ">");
            final IronSourceBannerLayout ironSourceBannerLayout2 = ironSourceBannerLayout;
            final BannerSmashListener bannerSmashListener2 = bannerSmashListener;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    try {
                        AdView adView = new AdView(ironSourceBannerLayout2.getActivity(), optString, calculateBannerSize);
                        AdListener access$600 = FacebookAdapter.this.createBannerAdListener(FacebookAdapter.this.calcLayoutParams(ironSourceBannerLayout2.getSize(), ironSourceBannerLayout2.getActivity()));
                        FacebookAdapter.this.mBNPlacementToAdMap.put(optString, adView);
                        adView.loadAd(adView.buildLoadAdConfig().withAdListener(access$600).build());
                    } catch (Exception e) {
                        bannerSmashListener2.onBannerAdLoadFailed(ErrorBuilder.buildLoadFailedError("FacebookAdapter loadBanner exception " + e.getMessage()));
                    }
                }
            });
        }
    }

    public void destroyBanner(final JSONObject jSONObject) {
        log("destroyBanner <" + jSONObject.optString("placementId") + ">");
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                try {
                    String optString = jSONObject.optString("placementId");
                    if (FacebookAdapter.this.mBNPlacementToAdMap.containsKey(optString)) {
                        ((AdView) FacebookAdapter.this.mBNPlacementToAdMap.get(optString)).destroy();
                        FacebookAdapter.this.mBNPlacementToAdMap.remove(optString);
                    }
                } catch (Exception e) {
                    FacebookAdapter facebookAdapter = FacebookAdapter.this;
                    facebookAdapter.logWarning("destroyBanner failed with an exception: " + e);
                }
            }
        });
    }

    public void reloadBanner(final JSONObject jSONObject) {
        log("reloadBanner <" + jSONObject.optString("placementId") + ">");
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                String optString = jSONObject.optString("placementId");
                if (FacebookAdapter.this.mBNPlacementToAdMap.containsKey(optString)) {
                    ((AdView) FacebookAdapter.this.mBNPlacementToAdMap.get(optString)).loadAd();
                } else if (FacebookAdapter.this.mBNPlacementToListenerMap.containsKey(optString)) {
                    int i = FacebookAdapter.BN_FAILED_TO_RELOAD_ERROR_CODE;
                    ((BannerSmashListener) FacebookAdapter.this.mBNPlacementToListenerMap.get(optString)).onBannerAdLoadFailed(new IronSourceError(i, FacebookAdapter.this.getProviderName() + "reloadBanner missing banner " + optString));
                }
            }
        });
    }

    private Map<String, Object> getBiddingData() {
        HashMap hashMap = new HashMap();
        hashMap.put("token", BidderTokenProvider.getBidderToken(this.mContext));
        return hashMap;
    }

    /* access modifiers changed from: private */
    public void loadRewardedVideo(String str) {
        log("loading rewarded video with placement id <" + str + ">");
        loadRewardedVideo(str, null);
    }

    private void loadRewardedVideo(final String str, final String str2) {
        if (TextUtils.isEmpty(str)) {
            logWarning("loadRewardedVideo: placementId is empty");
            return;
        }
        this.mRvAdsAvailability.put(str, false);
        if (this.mContext != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    try {
                        if (FacebookAdapter.this.mRVPlacementToAdMap.containsKey(str)) {
                            ((RewardedVideoAd) FacebookAdapter.this.mRVPlacementToAdMap.get(str)).destroy();
                            FacebookAdapter.this.mRVPlacementToAdMap.remove(str);
                        }
                        RewardedVideoAd rewardedVideoAd = new RewardedVideoAd(FacebookAdapter.this.mContext, str);
                        RewardedVideoAd.RewardedVideoAdLoadConfigBuilder buildLoadAdConfig = rewardedVideoAd.buildLoadAdConfig();
                        buildLoadAdConfig.withAdListener((RewardedVideoAdListener) FacebookAdapter.this.mRVPlacementToFBListener.get(str));
                        if (str2 != null) {
                            buildLoadAdConfig.withBid(str2);
                        }
                        if (!TextUtils.isEmpty(FacebookAdapter.this.getDynamicUserId())) {
                            buildLoadAdConfig.withRewardData(new RewardData(FacebookAdapter.this.getDynamicUserId(), "1"));
                        }
                        rewardedVideoAd.loadAd(buildLoadAdConfig.build());
                        FacebookAdapter.this.mRVPlacementToAdMap.put(str, rewardedVideoAd);
                    } catch (Exception unused) {
                        if (FacebookAdapter.this.mRVPlacementToListenerMap.containsKey(str)) {
                            ((RewardedVideoSmashListener) FacebookAdapter.this.mRVPlacementToListenerMap.get(str)).onRewardedVideoAvailabilityChanged(false);
                        }
                    }
                }
            });
        } else if (this.mRVPlacementToListenerMap.containsKey(str)) {
            this.mRVPlacementToListenerMap.get(str).onRewardedVideoAvailabilityChanged(false);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private AdSize calculateBannerSize(ISBannerSize iSBannerSize, boolean z) {
        char c;
        String description = iSBannerSize.getDescription();
        switch (description.hashCode()) {
            case -387072689:
                if (description.equals("RECTANGLE")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 72205083:
                if (description.equals("LARGE")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 79011241:
                if (description.equals("SMART")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 1951953708:
                if (description.equals("BANNER")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 1999208305:
                if (description.equals("CUSTOM")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c == 0) {
            return AdSize.BANNER_HEIGHT_50;
        }
        if (c == 1) {
            return AdSize.BANNER_HEIGHT_90;
        }
        if (c == 2) {
            return AdSize.RECTANGLE_HEIGHT_250;
        }
        if (c == 3) {
            return z ? AdSize.BANNER_HEIGHT_90 : AdSize.BANNER_HEIGHT_50;
        }
        if (c != 4) {
            return null;
        }
        if (iSBannerSize.getHeight() == 50) {
            return AdSize.BANNER_HEIGHT_50;
        }
        if (iSBannerSize.getHeight() == 90) {
            return AdSize.BANNER_HEIGHT_90;
        }
        if (iSBannerSize.getHeight() == 250) {
            return AdSize.RECTANGLE_HEIGHT_250;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public FrameLayout.LayoutParams calcLayoutParams(ISBannerSize iSBannerSize, Activity activity) {
        int i;
        if (iSBannerSize.getDescription().equals("RECTANGLE")) {
            i = HttpStatus.SC_MULTIPLE_CHOICES;
        } else {
            i = (!iSBannerSize.getDescription().equals("SMART") || !AdapterUtils.isLargeScreen(activity)) ? 320 : 728;
        }
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(AdapterUtils.dpToPixels(activity, i), -2);
        layoutParams.gravity = 17;
        return layoutParams;
    }

    private void initSdk() {
        if (this.mDidCallInit.compareAndSet(false, true)) {
            log("initSdk");
            if (AudienceNetworkAds.isInAdsProcess(this.mContext)) {
                log("initSdk: isInAdsProcess is true, no need to init");
                this.mDidInitSuccess = true;
                return;
            }
            AudienceNetworkAds.buildInitSettings(this.mContext).withInitListener(new AudienceNetworkAds.InitListener() {
                public void onInitialized(AudienceNetworkAds.InitResult initResult) {
                    FacebookAdapter.this.log("init SDK is completed with status: " + initResult.isSuccess() + ", " + initResult.getMessage());
                    if (initResult.isSuccess()) {
                        Boolean unused = FacebookAdapter.this.mDidInitSuccess = true;
                        for (BannerSmashListener onBannerInitSuccess : FacebookAdapter.this.mBNPlacementToListenerMap.values()) {
                            onBannerInitSuccess.onBannerInitSuccess();
                        }
                        for (InterstitialSmashListener onInterstitialInitSuccess : FacebookAdapter.this.mISPlacementToListenerMap.values()) {
                            onInterstitialInitSuccess.onInterstitialInitSuccess();
                        }
                        for (String str : FacebookAdapter.this.mRVPlacementToListenerMap.keySet()) {
                            if (FacebookAdapter.this.mProgrammaticPlacements.contains(str)) {
                                ((RewardedVideoSmashListener) FacebookAdapter.this.mRVPlacementToListenerMap.get(str)).onRewardedVideoInitSuccess();
                            } else {
                                FacebookAdapter.this.loadRewardedVideo(str);
                            }
                        }
                        return;
                    }
                    Boolean unused2 = FacebookAdapter.this.mDidInitSuccess = false;
                    String str2 = "init failed:" + initResult.getMessage();
                    for (BannerSmashListener onBannerInitFailed : FacebookAdapter.this.mBNPlacementToListenerMap.values()) {
                        onBannerInitFailed.onBannerInitFailed(new IronSourceError(IronSourceError.ERROR_CODE_INIT_FAILED, str2));
                    }
                    for (InterstitialSmashListener onInterstitialInitFailed : FacebookAdapter.this.mISPlacementToListenerMap.values()) {
                        onInterstitialInitFailed.onInterstitialInitFailed(ErrorBuilder.buildInitFailedError(str2, "Interstitial"));
                    }
                    for (String str3 : FacebookAdapter.this.mRVPlacementToListenerMap.keySet()) {
                        if (FacebookAdapter.this.mProgrammaticPlacements.contains(str3)) {
                            ((RewardedVideoSmashListener) FacebookAdapter.this.mRVPlacementToListenerMap.get(str3)).onRewardedVideoInitFailed(ErrorBuilder.buildInitFailedError(initResult.getMessage(), IronSourceConstants.REWARDED_VIDEO_AD_UNIT));
                        } else {
                            ((RewardedVideoSmashListener) FacebookAdapter.this.mRVPlacementToListenerMap.get(str3)).onRewardedVideoAvailabilityChanged(false);
                        }
                    }
                }
            }).initialize();
        }
    }

    /* access modifiers changed from: private */
    public AdListener createBannerAdListener(final FrameLayout.LayoutParams layoutParams) {
        return new AdListener() {
            public void onError(Ad ad, AdError adError) {
                FacebookAdapter facebookAdapter = FacebookAdapter.this;
                facebookAdapter.log("Banner Ad, onError (" + adError.getErrorCode() + "): " + adError.getErrorMessage());
                if (FacebookAdapter.this.mBNPlacementToListenerMap.containsKey(ad.getPlacementId())) {
                    BannerSmashListener bannerSmashListener = (BannerSmashListener) FacebookAdapter.this.mBNPlacementToListenerMap.get(ad.getPlacementId());
                    int errorCode = adError.getErrorCode() == 1001 ? IronSourceError.ERROR_BN_LOAD_NO_FILL : adError.getErrorCode();
                    if (adError.getErrorMessage() != null) {
                        bannerSmashListener.onBannerAdLoadFailed(new IronSourceError(errorCode, adError.getErrorCode() + ":" + adError.getErrorMessage()));
                        return;
                    }
                    bannerSmashListener.onBannerAdLoadFailed(new IronSourceError(errorCode, "Empty error string"));
                }
            }

            public void onAdLoaded(Ad ad) {
                FacebookAdapter facebookAdapter = FacebookAdapter.this;
                facebookAdapter.log("Banner Ad, onAdLoaded <" + ad.getPlacementId() + ">");
                if (FacebookAdapter.this.mBNPlacementToListenerMap.containsKey(ad.getPlacementId()) && FacebookAdapter.this.mBNPlacementToAdMap.containsKey(ad.getPlacementId())) {
                    ((BannerSmashListener) FacebookAdapter.this.mBNPlacementToListenerMap.get(ad.getPlacementId())).onBannerAdLoaded((View) FacebookAdapter.this.mBNPlacementToAdMap.get(ad.getPlacementId()), layoutParams);
                }
            }

            public void onAdClicked(Ad ad) {
                FacebookAdapter facebookAdapter = FacebookAdapter.this;
                facebookAdapter.log("Banner Ad, onAdClicked <" + ad.getPlacementId() + ">");
                if (FacebookAdapter.this.mBNPlacementToListenerMap.containsKey(ad.getPlacementId())) {
                    ((BannerSmashListener) FacebookAdapter.this.mBNPlacementToListenerMap.get(ad.getPlacementId())).onBannerAdClicked();
                }
            }

            public void onLoggingImpression(Ad ad) {
                FacebookAdapter facebookAdapter = FacebookAdapter.this;
                facebookAdapter.log("Banner Ad, onLoggingImpression <" + ad.getPlacementId() + ">");
            }
        };
    }

    private class FacebookInterstitialAdListener implements InterstitialAdListener {
        private InterstitialSmashListener mListener;
        private String mPlacementId;

        public void onInterstitialDisplayed(Ad ad) {
        }

        FacebookInterstitialAdListener(InterstitialSmashListener interstitialSmashListener, String str) {
            this.mPlacementId = str;
            this.mListener = interstitialSmashListener;
        }

        public void onInterstitialDismissed(Ad ad) {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("onInterstitialDismissed <" + this.mPlacementId + ">");
            this.mListener.onInterstitialAdClosed();
        }

        public void onError(Ad ad, AdError adError) {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("Interstitial Ad <" + this.mPlacementId + ">  onError: (" + adError.getErrorCode() + "): " + adError.getErrorMessage());
            InterstitialSmashListener interstitialSmashListener = this.mListener;
            int errorCode = adError.getErrorCode();
            StringBuilder sb = new StringBuilder();
            sb.append(adError.getErrorMessage());
            sb.append("");
            interstitialSmashListener.onInterstitialAdLoadFailed(new IronSourceError(errorCode, sb.toString()));
            FacebookAdapter.this.mInterstitialAdsAvailability.put(this.mPlacementId, false);
        }

        public void onAdLoaded(Ad ad) {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("Interstitial Ad, onAdLoaded <" + this.mPlacementId + ">");
            this.mListener.onInterstitialAdReady();
            FacebookAdapter.this.mInterstitialAdsAvailability.put(this.mPlacementId, true);
        }

        public void onAdClicked(Ad ad) {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("Interstitial Ad, onAdClicked <" + this.mPlacementId + ">");
            this.mListener.onInterstitialAdClicked();
        }

        public void onLoggingImpression(Ad ad) {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("Interstitial Ad, onLoggingImpression <" + this.mPlacementId + ">");
            this.mListener.onInterstitialAdOpened();
            this.mListener.onInterstitialAdShowSucceeded();
        }
    }

    private class FacebookRewardedVideoAdListener implements RewardedVideoAdListener {
        private RewardedVideoSmashListener mListener;
        private String mPlacementId;

        FacebookRewardedVideoAdListener(RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
            this.mPlacementId = str;
            this.mListener = rewardedVideoSmashListener;
        }

        public void onRewardedVideoCompleted() {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("onRewardedVideoCompleted <" + this.mPlacementId + ">");
            this.mListener.onRewardedVideoAdEnded();
            this.mListener.onRewardedVideoAdRewarded();
        }

        public void onLoggingImpression(Ad ad) {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("RewardedVideo onLoggingImpression <" + this.mPlacementId + ">");
            this.mListener.onRewardedVideoAdOpened();
            this.mListener.onRewardedVideoAdStarted();
        }

        public void onRewardedVideoClosed() {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("onRewardedVideoClosed <" + this.mPlacementId + ">");
            this.mListener.onRewardedVideoAdClosed();
        }

        public void onError(Ad ad, AdError adError) {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("RewardedVideo onError <" + this.mPlacementId + ">  onError: (" + adError.getErrorCode() + "): " + adError.getErrorMessage());
            this.mListener.onRewardedVideoAvailabilityChanged(false);
            FacebookAdapter.this.mRvAdsAvailability.put(this.mPlacementId, false);
        }

        public void onAdLoaded(Ad ad) {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("RewardedVideo onAdLoaded <" + this.mPlacementId + ">");
            this.mListener.onRewardedVideoAvailabilityChanged(true);
            FacebookAdapter.this.mRvAdsAvailability.put(this.mPlacementId, true);
        }

        public void onAdClicked(Ad ad) {
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            facebookAdapter.log("RewardedVideo onAdClicked <" + this.mPlacementId + ">");
            this.mListener.onRewardedVideoAdClicked();
        }
    }

    /* access modifiers changed from: private */
    public void log(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + ": " + str, 1);
    }

    /* access modifiers changed from: private */
    public void logWarning(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + ": " + str, 2);
    }

    /* access modifiers changed from: private */
    public void logError(String str) {
        IronSourceLoggerManager logger = IronSourceLoggerManager.getLogger();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        logger.log(ironSourceTag, getProviderName() + ": " + str, 3);
    }
}
