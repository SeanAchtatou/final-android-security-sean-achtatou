package com.kochava.base;

import bolts.MeasurementEvent;
import com.vungle.warren.analytics.AnalyticsEvent;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;

final class u extends j {
    u(i iVar) {
        super(iVar, false);
    }

    private boolean o() {
        int c;
        int b;
        if (!this.a.t || (c = x.c()) >= (b = x.b(this.a.d.b("initial_sent_time"), 0) + x.b(this.a.d.b("networking_tracking_wait"), 10))) {
            return false;
        }
        int i = b - c;
        Tracker.a(5, "TQU", "checkForInsta", "Delaying queue task from install for " + i + " seconds");
        a((long) (i * 1000));
        return true;
    }

    private boolean p() {
        int c;
        int b;
        if (!this.a.t || (c = x.c()) >= (b = x.b(this.a.d.b("click_sent_time"), 0) + x.b(this.a.d.b("networking_tracking_wait"), 10))) {
            return false;
        }
        int i = b - c;
        Tracker.a(5, "TQU", "checkForClick", "Delaying queue task from click for " + i + " seconds");
        a((long) (i * 1000));
        return true;
    }

    private boolean q() {
        int c;
        int b;
        if (!this.a.t || (c = x.c()) >= (b = x.b(this.a.d.b("update_sent_time"), 0) + x.b(this.a.d.b("networking_tracking_wait"), 10))) {
            return false;
        }
        int i = b - c;
        Tracker.a(5, "TQU", "checkForUpdat", "Delaying queue task from update/idlink for " + i + " seconds");
        a((long) (i * 1000));
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.kochava.base.u.a(org.json.JSONObject, org.json.JSONArray):boolean
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String */
    private boolean r() {
        if (this.a.d.k() <= 0) {
            return true;
        }
        if (n()) {
            return false;
        }
        Tracker.a(5, "TQU", "flushClickQue", SettingsJsonConstants.PROMPT_SEND_BUTTON_TITLE_DEFAULT);
        String a = x.a(this.a.d.j().opt(AnalyticsEvent.Ad.clickUrl), "");
        if (!a.isEmpty()) {
            String a2 = a(a, false);
            if (a2 == null && k() < 2) {
                m();
                return false;
            } else if (a2 != null) {
                this.a.d.a("click_sent_time", Integer.valueOf(x.c()));
            }
        }
        if (this.a.d.i()) {
            l();
            return true;
        }
        throw new IOException("Unknown error in clicks queue remove");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
     arg types: [org.json.JSONObject, int]
     candidates:
      com.kochava.base.u.a(org.json.JSONObject, org.json.JSONArray):boolean
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
     arg types: [int, org.json.JSONObject]
     candidates:
      com.kochava.base.u.a(org.json.JSONObject, org.json.JSONArray):boolean
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject */
    private boolean s() {
        if (this.a.d.h() <= 0) {
            return true;
        }
        if (n()) {
            return false;
        }
        Tracker.a(5, "TQU", "flushUpdateQu", SettingsJsonConstants.PROMPT_SEND_BUTTON_TITLE_DEFAULT);
        JSONArray g = x.g(this.a.d.b("blacklist"));
        JSONObject g2 = this.a.d.g();
        if (a(g2, g)) {
            if (a(a(a(g2), (Object) g2), true)) {
                return false;
            }
            this.a.d.a("update_sent_time", Integer.valueOf(x.c()));
        }
        if (this.a.d.f()) {
            l();
            return true;
        }
        throw new IOException("Unknown error in update queue remove");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
     arg types: [org.json.JSONObject, int]
     candidates:
      com.kochava.base.u.a(org.json.JSONObject, org.json.JSONArray):boolean
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
     arg types: [int, org.json.JSONObject]
     candidates:
      com.kochava.base.u.a(org.json.JSONObject, org.json.JSONArray):boolean
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject */
    private boolean t() {
        if (this.a.d.e() <= 0) {
            return true;
        }
        if (n()) {
            return false;
        }
        Tracker.a(5, "TQU", "flushEventQue", SettingsJsonConstants.PROMPT_SEND_BUTTON_TITLE_DEFAULT);
        JSONArray g = x.g(this.a.d.b("blacklist"));
        JSONArray g2 = x.g(this.a.d.b("eventname_blacklist"));
        boolean a = x.a(this.a.d.b("session_tracking"), true);
        JSONObject d = this.a.d.d();
        if (a(d, g, g2, a) && a(a(6, (Object) d), true)) {
            return false;
        }
        if (this.a.d.c()) {
            l();
            return true;
        }
        throw new IOException("Unknown error in events queue remove");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final boolean a(JSONObject jSONObject, JSONArray jSONArray) {
        if (!jSONObject.has("action") || !jSONObject.has("sdk_version")) {
            return false;
        }
        int a = a(jSONObject);
        a(jSONObject, this.a.d);
        boolean a2 = x.a(this.a.d.b("push"), false);
        if ((a == 8 || a == 9) && !a2) {
            return false;
        }
        if (a != 4) {
            return true;
        }
        b(jSONObject, jSONArray);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final boolean a(JSONObject jSONObject, JSONArray jSONArray, JSONArray jSONArray2, boolean z) {
        if (!jSONObject.has("action") || !jSONObject.has("sdk_version")) {
            return false;
        }
        JSONObject b = x.b(jSONObject.opt("data"), true);
        int a = a(jSONObject);
        String a2 = x.a(b.opt(MeasurementEvent.MEASUREMENT_EVENT_NAME_KEY));
        if (a2 != null && a == 6 && x.a(jSONArray2, a2)) {
            return false;
        }
        if (!z && (a == 3 || a == 2)) {
            return false;
        }
        a(jSONObject, this.a.d);
        boolean a3 = x.a(jSONObject.opt("backfilled"), false);
        jSONObject.remove("backfilled");
        if (a3) {
            x.a("data", b(a, b), jSONObject);
        }
        b(jSONObject, jSONArray);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    /* access modifiers changed from: package-private */
    public final void b(JSONObject jSONObject, JSONArray jSONArray) {
        if (jSONArray != null) {
            JSONObject b = x.b(jSONObject.opt("data"), true);
            JSONArray c = x.c(b.names(), true);
            for (int i = 0; i < c.length(); i++) {
                String a = x.a(c.opt(i));
                if (a != null && x.a(jSONArray, a)) {
                    b.remove(a);
                }
            }
        }
    }

    public final void run() {
        Tracker.a(4, "TQU", "run", new Object[0]);
        while (true) {
            try {
                if (this.a.d.k() <= 0 && this.a.d.h() <= 0 && this.a.d.e() <= 0) {
                    break;
                } else if (this.a.r) {
                    throw new Exception("Sleep enabled after launching.");
                } else if (!o()) {
                    if (this.a.d.k() > 0) {
                        if (!r()) {
                            return;
                        }
                    } else if (!p()) {
                        if (this.a.d.h() > 0) {
                            if (!s()) {
                                return;
                            }
                        } else if (!q()) {
                            if (this.a.d.e() <= 0) {
                                continue;
                            } else if (!t()) {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } catch (Exception e) {
                Tracker.a(4, "TQU", "run", "Failed to process queue", e.getMessage());
            }
        }
        this.a.d.a();
        h();
        Tracker.a(4, "TQU", "run", "Complete");
    }
}
