package com.kochava.base;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.kochava.base.c;
import java.util.HashMap;
import java.util.Map;

final class e implements Runnable {
    private final int a;
    private final i b;
    private final Handler c = new Handler(Looper.getMainLooper());
    private final long d = x.b();
    private DeepLinkListener e;
    private Map<String, String> f = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, boolean, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    e(Uri uri, int i, i iVar, DeepLinkListener deepLinkListener) {
        this.a = x.a(i, (int) IronSourceConstants.INTERSTITIAL_DAILY_CAPPED, 10000);
        this.e = deepLinkListener;
        this.b = iVar;
        if (uri != null) {
            this.f = x.a(uri.getQuery());
        }
        boolean a2 = x.a(iVar.d.b("deeplink_ran"), false);
        iVar.d.a("deeplink_ran", (Object) true, true);
        if (this.f != null || !iVar.q || a2) {
            b();
        } else {
            run();
        }
    }

    private void b() {
        synchronized (this) {
            try {
                if (this.e != null) {
                    this.e.onDeepLink(this.f != null ? this.f : new HashMap());
                }
                a();
            } catch (Throwable th) {
                Tracker.a(2, "DLT", "sendDeepLink", "Exception in Host App", th);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    private Map<String, String> c() {
        synchronized (this) {
            InstallReferrer a2 = c.a.a(x.b(this.b.d.b("install_referrer"), true), false, x.a(this.b.d.b("referrer")));
            if (!a2.isValid()) {
                return null;
            }
            Map<String, String> a3 = x.a(a2.referrer);
            return a3;
        }
    }

    public final void a() {
        synchronized (this) {
            this.c.removeCallbacks(this);
            this.e = null;
            this.f = null;
        }
    }

    public final void run() {
        synchronized (this) {
            this.f = c();
            if (this.f == null) {
                if (x.b() - this.d < ((long) this.a)) {
                    this.c.postDelayed(this, 250);
                    return;
                }
            }
            b();
        }
    }
}
