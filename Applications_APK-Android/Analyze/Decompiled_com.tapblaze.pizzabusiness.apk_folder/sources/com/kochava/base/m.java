package com.kochava.base;

import android.os.Handler;
import android.os.Looper;
import org.json.JSONObject;

final class m extends j {
    /* access modifiers changed from: private */
    public final AttributionUpdateListener b;
    private final boolean c;
    private JSONObject d = null;

    m(i iVar, AttributionUpdateListener attributionUpdateListener, boolean z) {
        super(iVar, false);
        this.b = attributionUpdateListener;
        this.c = z;
    }

    static String a(String str) {
        JSONObject f;
        String a = (str == null || "false".equalsIgnoreCase(str) || (f = x.f(str)) == null) ? null : x.a(f);
        return a == null ? "{\"attribution\":\"false\"}" : a;
    }

    private void b(final String str) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                try {
                    m.this.b.onAttributionUpdated(str);
                } catch (Throwable th) {
                    Tracker.a(2, "TGA", "notifyCallbac", "Exception in Host App", th);
                }
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, double):double
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, double):double */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
     arg types: [int, org.json.JSONObject]
     candidates:
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
     arg types: [org.json.JSONObject, int]
     candidates:
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean */
    public final void run() {
        Tracker.a(4, "TGA", "run", "run");
        String a = x.a(this.a.d.b("attribution"));
        if (a != null) {
            Object[] objArr = new Object[1];
            if (this.c) {
                objArr[0] = "Returning cached response";
                Tracker.a(4, "TGA", "run", objArr);
                b(a);
            } else {
                objArr[0] = "Skip";
                Tracker.a(4, "TGA", "run", objArr);
            }
            d();
            i();
            return;
        }
        double b2 = (double) x.b();
        double b3 = (double) (((long) x.b(this.a.d.b("initial_sent_time"), 0)) * 1000);
        Double.isNaN(b3);
        double a2 = b3 + (x.a(this.a.d.b("getattribution_wait"), 3.0d) * 1000.0d);
        if (b2 < a2) {
            Double.isNaN(b2);
            long round = Math.round(a2 - b2);
            Tracker.a(5, "TGA", "run", "Delaying for " + round + " milliseconds");
            a(round);
            return;
        }
        if (this.d == null) {
            Tracker.a(5, "TGA", "run", "Gather");
            this.d = new JSONObject();
            a(5, this.d, new JSONObject());
        }
        JSONObject a3 = a(5, (Object) this.d);
        if (!a(a3, true)) {
            String str = null;
            JSONObject f = x.f(a3.opt("data"));
            if (f != null) {
                str = f.optString("attribution");
            }
            String a4 = a(str);
            this.a.d.a("attribution", a4);
            b(a4);
            this.a.d.a("attribution_time", Integer.valueOf(x.c()));
            Tracker.a(3, "TGA", "Attribution", "Complete");
            Tracker.a(4, "TGA", "run", "Complete");
            d();
            i();
        }
    }
}
