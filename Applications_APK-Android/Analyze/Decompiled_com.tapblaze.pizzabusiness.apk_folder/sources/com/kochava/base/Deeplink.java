package com.kochava.base;

import org.json.JSONObject;

public final class Deeplink {
    public final String destination;
    public final JSONObject raw;

    Deeplink(JSONObject jSONObject, String str) {
        this.destination = x.a(jSONObject.opt("destination"), str);
        x.a("destination", this.destination, jSONObject);
        this.raw = jSONObject;
    }

    public final JSONObject toJson() {
        JSONObject jSONObject = new JSONObject();
        x.a("destination", this.destination, jSONObject);
        x.a("raw", this.raw, jSONObject);
        return jSONObject;
    }

    public final String toString() {
        return x.a(toJson());
    }
}
