package com.kochava.base;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import com.ironsource.eventsmodule.DataBaseEventsStorage;
import com.ironsource.sdk.constants.Constants;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class d extends SQLiteOpenHelper {
    private static final Object c = new Object();
    private static final Object d = new Object();
    final Map<String, Object> a = new HashMap();
    SQLiteDatabase b = null;
    private final List<String> e = Arrays.asList(Constants.RequestParameters.CONSENT, "init_last_sent", "kvinit_staleness", "kvinit_wait", "consent_last_prompt", "deeplink_ran");
    private final SharedPreferences f;
    private int g = -1;
    private int h = -1;
    private int i = -1;
    private boolean j = false;

    d(Context context, boolean z) {
        super(context, "kodb", (SQLiteDatabase.CursorFactory) null, 5);
        Tracker.a(5, "DAB", "Database", new Object[0]);
        this.f = context.getSharedPreferences("kosp", 0);
        a(context, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0101 A[Catch:{ Exception -> 0x012e }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01cd A[ADDED_TO_REGION, Catch:{ Exception -> 0x020c }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01e3 A[Catch:{ Exception -> 0x020c }] */
    /* JADX WARNING: Removed duplicated region for block: B:93:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r20, boolean r21) {
        /*
            r19 = this;
            r1 = r19
            r2 = r20
            java.lang.String r3 = "2016"
            java.lang.String r4 = "2017"
            java.lang.String r5 = "initial"
            java.lang.String r6 = "attribution"
            java.lang.String r7 = "kochava_queue_storage"
            java.lang.String r8 = "watchlistProperties"
            java.lang.String r9 = "kochava_device_id"
            java.lang.String r0 = "has_upgraded"
            java.lang.Object r10 = r1.b(r0)
            r11 = 0
            boolean r10 = com.kochava.base.x.a(r10, r11)
            r12 = 4
            java.lang.String r13 = "upgradeSdk"
            java.lang.String r14 = "DAB"
            r15 = 1
            if (r10 == 0) goto L_0x002f
            java.lang.Object[] r0 = new java.lang.Object[r15]
            java.lang.String r2 = "Skip"
            r0[r11] = r2
            com.kochava.base.Tracker.a(r12, r14, r13, r0)
            return
        L_0x002f:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r15)
            r1.a(r0, r10)
            r10 = 2
            android.content.ComponentName r0 = new android.content.ComponentName     // Catch:{ all -> 0x0056 }
            java.lang.String r12 = "com.kochava.android.tracker.ReferralCapture"
            r0.<init>(r2, r12)     // Catch:{ all -> 0x0056 }
            android.content.pm.PackageManager r12 = r20.getPackageManager()     // Catch:{ all -> 0x0056 }
            android.content.pm.ActivityInfo r0 = r12.getReceiverInfo(r0, r11)     // Catch:{ all -> 0x0056 }
            if (r0 == 0) goto L_0x0060
            java.lang.Object[] r0 = new java.lang.Object[r10]     // Catch:{ all -> 0x0056 }
            java.lang.String r12 = "Legacy Broadcast Receiver found. Remove the following from your manifest!"
            r0[r11] = r12     // Catch:{ all -> 0x0056 }
            java.lang.String r12 = "<receiver android:name=\"com.kochava.android.tracker.ReferralCapture\"\n    android:exported=\"true\">\n    <intent-filter>\n        <action android:name=\"com.android.vending.INSTALL_REFERRER\" />\n    </intent-filter>\n</receiver>"
            r0[r15] = r12     // Catch:{ all -> 0x0056 }
            com.kochava.base.Tracker.a(r15, r14, r13, r0)     // Catch:{ all -> 0x0056 }
            goto L_0x0060
        L_0x0056:
            r0 = 5
            java.lang.Object[] r12 = new java.lang.Object[r15]
            java.lang.String r16 = "Legacy broadcast receiver correctly removed"
            r12[r11] = r16
            com.kochava.base.Tracker.a(r0, r14, r13, r12)
        L_0x0060:
            r12 = 0
            java.lang.String r0 = "ko.tr"
            android.content.SharedPreferences r0 = r2.getSharedPreferences(r0, r11)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r10 = "ko.dt.pt"
            android.content.SharedPreferences r10 = r2.getSharedPreferences(r10, r11)     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r15 = "initial_sent"
            boolean r15 = r0.getBoolean(r15, r11)     // Catch:{ Exception -> 0x00c7 }
            if (r15 == 0) goto L_0x007d
            boolean r15 = r0.contains(r5)     // Catch:{ Exception -> 0x00c7 }
            if (r15 != 0) goto L_0x007d
            r15 = 1
            goto L_0x007e
        L_0x007d:
            r15 = 0
        L_0x007e:
            java.lang.String r11 = "attribution_data"
            java.lang.String r11 = r0.getString(r11, r12)     // Catch:{ Exception -> 0x00c7 }
            r17 = r6
            java.lang.String r6 = r10.getString(r9, r12)     // Catch:{ Exception -> 0x00c5 }
            r18 = r9
            if (r6 == 0) goto L_0x00c3
            r12 = 1
            java.lang.Object[] r9 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x00c1 }
            r12 = 0
            r9[r12] = r4     // Catch:{ Exception -> 0x00c1 }
            r12 = 4
            com.kochava.base.Tracker.a(r12, r14, r13, r9)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r9 = "STR::"
            java.lang.String r12 = ""
            java.lang.String r6 = r6.replace(r9, r12)     // Catch:{ Exception -> 0x00c1 }
            r1.a(r15, r6, r11)     // Catch:{ Exception -> 0x00c1 }
            if (r21 == 0) goto L_0x00c0
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x00c1 }
            android.content.SharedPreferences$Editor r0 = r0.clear()     // Catch:{ Exception -> 0x00c1 }
            r0.apply()     // Catch:{ Exception -> 0x00c1 }
            android.content.SharedPreferences$Editor r0 = r10.edit()     // Catch:{ Exception -> 0x00c1 }
            android.content.SharedPreferences$Editor r0 = r0.clear()     // Catch:{ Exception -> 0x00c1 }
            r0.apply()     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r0 = "ko.db"
            r2.deleteDatabase(r0)     // Catch:{ Exception -> 0x00c1 }
        L_0x00c0:
            return
        L_0x00c1:
            r0 = move-exception
            goto L_0x00cc
        L_0x00c3:
            r10 = 0
            goto L_0x00d8
        L_0x00c5:
            r0 = move-exception
            goto L_0x00ca
        L_0x00c7:
            r0 = move-exception
            r17 = r6
        L_0x00ca:
            r18 = r9
        L_0x00cc:
            r6 = 2
            java.lang.Object[] r9 = new java.lang.Object[r6]
            r10 = 0
            r9[r10] = r4
            r4 = 1
            r9[r4] = r0
            com.kochava.base.Tracker.a(r6, r14, r13, r9)
        L_0x00d8:
            java.lang.String r0 = "initPrefs"
            android.content.SharedPreferences r0 = r2.getSharedPreferences(r0, r10)     // Catch:{ Exception -> 0x012e }
            java.lang.String r4 = "attributionPref"
            android.content.SharedPreferences r4 = r2.getSharedPreferences(r4, r10)     // Catch:{ Exception -> 0x012e }
            java.lang.String r6 = "true"
            java.lang.String r9 = "initBool"
            java.lang.String r10 = ""
            java.lang.String r9 = r0.getString(r9, r10)     // Catch:{ Exception -> 0x012e }
            boolean r6 = r6.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x012e }
            java.lang.String r9 = "attributionData"
            r10 = 0
            java.lang.String r9 = r4.getString(r9, r10)     // Catch:{ Exception -> 0x012e }
            java.lang.String r11 = "kochava_app_id_generated"
            java.lang.String r11 = r0.getString(r11, r10)     // Catch:{ Exception -> 0x012e }
            if (r11 == 0) goto L_0x012c
            r10 = 1
            java.lang.Object[] r12 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x012e }
            r10 = 0
            r12[r10] = r3     // Catch:{ Exception -> 0x012e }
            r10 = 4
            com.kochava.base.Tracker.a(r10, r14, r13, r12)     // Catch:{ Exception -> 0x012e }
            r1.a(r6, r11, r9)     // Catch:{ Exception -> 0x012e }
            if (r21 == 0) goto L_0x012b
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x012e }
            android.content.SharedPreferences$Editor r0 = r0.clear()     // Catch:{ Exception -> 0x012e }
            r0.apply()     // Catch:{ Exception -> 0x012e }
            android.content.SharedPreferences$Editor r0 = r4.edit()     // Catch:{ Exception -> 0x012e }
            android.content.SharedPreferences$Editor r0 = r0.clear()     // Catch:{ Exception -> 0x012e }
            r0.apply()     // Catch:{ Exception -> 0x012e }
            java.lang.String r0 = "KochavaFeatureTracker"
            r2.deleteDatabase(r0)     // Catch:{ Exception -> 0x012e }
        L_0x012b:
            return
        L_0x012c:
            r9 = 0
            goto L_0x013b
        L_0x012e:
            r0 = move-exception
            r4 = 2
            java.lang.Object[] r6 = new java.lang.Object[r4]
            r9 = 0
            r6[r9] = r3
            r3 = 1
            r6[r3] = r0
            com.kochava.base.Tracker.a(r4, r14, r13, r6)
        L_0x013b:
            java.lang.String r0 = r20.getPackageName()     // Catch:{ Exception -> 0x0195 }
            android.content.SharedPreferences r0 = r2.getSharedPreferences(r0, r9)     // Catch:{ Exception -> 0x0195 }
            boolean r3 = r0.contains(r8)     // Catch:{ Exception -> 0x0195 }
            r4 = 0
            java.lang.String r6 = r0.getString(r7, r4)     // Catch:{ Exception -> 0x0195 }
            if (r3 == 0) goto L_0x0158
            if (r6 == 0) goto L_0x0156
            boolean r3 = r6.contains(r5)     // Catch:{ Exception -> 0x0195 }
            if (r3 != 0) goto L_0x0158
        L_0x0156:
            r3 = 1
            goto L_0x0159
        L_0x0158:
            r3 = 0
        L_0x0159:
            r4 = r17
            r6 = 0
            java.lang.String r9 = r0.getString(r4, r6)     // Catch:{ Exception -> 0x0193 }
            r10 = r18
            java.lang.String r11 = r0.getString(r10, r6)     // Catch:{ Exception -> 0x0191 }
            if (r11 == 0) goto L_0x01a8
            r6 = 1
            java.lang.Object[] r12 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0191 }
            java.lang.String r6 = "unityV1_1"
            r15 = 0
            r12[r15] = r6     // Catch:{ Exception -> 0x0191 }
            r6 = 4
            com.kochava.base.Tracker.a(r6, r14, r13, r12)     // Catch:{ Exception -> 0x0191 }
            r1.a(r3, r11, r9)     // Catch:{ Exception -> 0x0191 }
            if (r21 == 0) goto L_0x0190
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x0191 }
            android.content.SharedPreferences$Editor r0 = r0.remove(r8)     // Catch:{ Exception -> 0x0191 }
            android.content.SharedPreferences$Editor r0 = r0.remove(r7)     // Catch:{ Exception -> 0x0191 }
            android.content.SharedPreferences$Editor r0 = r0.remove(r4)     // Catch:{ Exception -> 0x0191 }
            android.content.SharedPreferences$Editor r0 = r0.remove(r10)     // Catch:{ Exception -> 0x0191 }
            r0.apply()     // Catch:{ Exception -> 0x0191 }
        L_0x0190:
            return
        L_0x0191:
            r0 = move-exception
            goto L_0x019a
        L_0x0193:
            r0 = move-exception
            goto L_0x0198
        L_0x0195:
            r0 = move-exception
            r4 = r17
        L_0x0198:
            r10 = r18
        L_0x019a:
            r3 = 2
            java.lang.Object[] r6 = new java.lang.Object[r3]
            java.lang.String r9 = "unityV1_1"
            r11 = 0
            r6[r11] = r9
            r9 = 1
            r6[r9] = r0
            com.kochava.base.Tracker.a(r3, r14, r13, r6)
        L_0x01a8:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020c }
            r0.<init>()     // Catch:{ Exception -> 0x020c }
            java.lang.String r3 = r20.getPackageName()     // Catch:{ Exception -> 0x020c }
            r0.append(r3)     // Catch:{ Exception -> 0x020c }
            java.lang.String r3 = ".v2.playerprefs"
            r0.append(r3)     // Catch:{ Exception -> 0x020c }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x020c }
            r3 = 0
            android.content.SharedPreferences r0 = r2.getSharedPreferences(r0, r3)     // Catch:{ Exception -> 0x020c }
            boolean r2 = r0.contains(r8)     // Catch:{ Exception -> 0x020c }
            r3 = 0
            java.lang.String r6 = r0.getString(r7, r3)     // Catch:{ Exception -> 0x020c }
            if (r2 == 0) goto L_0x01d7
            if (r6 == 0) goto L_0x01d5
            boolean r2 = r6.contains(r5)     // Catch:{ Exception -> 0x020c }
            if (r2 != 0) goto L_0x01d7
        L_0x01d5:
            r2 = 1
            goto L_0x01d8
        L_0x01d7:
            r2 = 0
        L_0x01d8:
            r3 = 0
            java.lang.String r5 = r0.getString(r4, r3)     // Catch:{ Exception -> 0x020c }
            java.lang.String r3 = r0.getString(r10, r3)     // Catch:{ Exception -> 0x020c }
            if (r3 == 0) goto L_0x021b
            r6 = 1
            java.lang.Object[] r9 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x020c }
            java.lang.String r6 = "unityV1_2"
            r11 = 0
            r9[r11] = r6     // Catch:{ Exception -> 0x020c }
            r6 = 4
            com.kochava.base.Tracker.a(r6, r14, r13, r9)     // Catch:{ Exception -> 0x020c }
            r1.a(r2, r3, r5)     // Catch:{ Exception -> 0x020c }
            if (r21 == 0) goto L_0x021b
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x020c }
            android.content.SharedPreferences$Editor r0 = r0.remove(r8)     // Catch:{ Exception -> 0x020c }
            android.content.SharedPreferences$Editor r0 = r0.remove(r7)     // Catch:{ Exception -> 0x020c }
            android.content.SharedPreferences$Editor r0 = r0.remove(r4)     // Catch:{ Exception -> 0x020c }
            android.content.SharedPreferences$Editor r0 = r0.remove(r10)     // Catch:{ Exception -> 0x020c }
            r0.apply()     // Catch:{ Exception -> 0x020c }
            goto L_0x021b
        L_0x020c:
            r0 = move-exception
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]
            java.lang.String r4 = "unityV1_2"
            r5 = 0
            r3[r5] = r4
            r4 = 1
            r3[r4] = r0
            com.kochava.base.Tracker.a(r2, r14, r13, r3)
        L_0x021b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.d.a(android.content.Context, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    private void a(boolean z, String str, String str2) {
        Tracker.a(4, "DAB", "applySdkUpgra", Boolean.valueOf(z), str, str2);
        if (z) {
            a("initial_ever_sent", (Object) true);
            a("initial_needs_sent", (Object) false);
        }
        a("kochava_device_id", str);
        if (str2 != null) {
            a("attribution", m.a(str2));
        }
    }

    private SQLiteDatabase l() {
        SQLiteDatabase sQLiteDatabase = this.b;
        if (sQLiteDatabase == null || !sQLiteDatabase.isOpen()) {
            Tracker.a(4, "DAB", "openDb", "Opening");
            this.b = getWritableDatabase();
            if (Build.VERSION.SDK_INT <= 16) {
                this.b.setLockingEnabled(false);
            }
            this.h = (int) DatabaseUtils.queryNumEntries(this.b, DataBaseEventsStorage.EventEntry.TABLE_NAME);
            this.g = (int) DatabaseUtils.queryNumEntries(this.b, "updates");
            this.i = (int) DatabaseUtils.queryNumEntries(this.b, "clicks");
        } else {
            Tracker.a(5, "DAB", "openDb", "Already Open");
        }
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        synchronized (c) {
            this.h = -1;
            this.g = -1;
            this.i = -1;
            try {
                if (this.b == null || !this.b.isOpen()) {
                    Tracker.a(5, "DAB", "closeDb", "Already Closed");
                } else {
                    Tracker.a(4, "DAB", "closeDb", new Object[0]);
                    this.b.execSQL("VACUUM");
                    close();
                    this.b = null;
                }
            } catch (Throwable th) {
                Tracker.a(5, "DAB", "closeDb", th);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        synchronized (d) {
            this.f.edit().remove(str).apply();
            this.a.remove(str);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, Object obj) {
        a(str, obj, this.j);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, Object obj, boolean z) {
        Map<String, Object> map;
        SharedPreferences.Editor putString;
        synchronized (d) {
            if (obj instanceof Boolean) {
                if (z) {
                    putString = this.f.edit().putBoolean(str, ((Boolean) obj).booleanValue());
                } else {
                    map = this.a;
                    map.put(str, obj);
                }
            } else if (obj instanceof Integer) {
                if (z) {
                    putString = this.f.edit().putInt(str, ((Integer) obj).intValue());
                } else {
                    map = this.a;
                    map.put(str, obj);
                }
            } else if (obj instanceof Long) {
                if (z) {
                    putString = this.f.edit().putLong(str + "-long", ((Long) obj).longValue());
                } else {
                    map = this.a;
                    str = str + "-long";
                    map.put(str, obj);
                }
            } else if (obj instanceof Float) {
                if (z) {
                    putString = this.f.edit().putFloat(str, ((Float) obj).floatValue());
                } else {
                    map = this.a;
                    map.put(str, obj);
                }
            } else if (obj instanceof Double) {
                if (z) {
                    putString = this.f.edit().putLong(str, Double.doubleToRawLongBits(((Double) obj).doubleValue()));
                } else {
                    map = this.a;
                    obj = Long.valueOf(Double.doubleToRawLongBits(((Double) obj).doubleValue()));
                    map.put(str, obj);
                }
            } else if (obj instanceof String) {
                if (z) {
                    putString = this.f.edit().putString(str, "STR::" + obj);
                } else {
                    map = this.a;
                    obj = "STR::" + obj;
                    map.put(str, obj);
                }
            } else if (obj instanceof JSONObject) {
                if (z) {
                    putString = this.f.edit().putString(str, "JSO::" + obj.toString());
                } else {
                    map = this.a;
                    obj = "JSO::" + obj.toString();
                    map.put(str, obj);
                }
            } else if (!(obj instanceof JSONArray)) {
                Tracker.a(2, "DAB", "put", str + " Unrecognized Type");
            } else if (z) {
                putString = this.f.edit().putString(str, "JSA::" + obj.toString());
            } else {
                map = this.a;
                obj = "JSA::" + obj.toString();
                map.put(str, obj);
            }
            putString.apply();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(JSONObject jSONObject) {
        synchronized (c) {
            Tracker.a(5, "DAB", "putEvent", jSONObject);
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("data", x.a(jSONObject));
                if (l().insert(DataBaseEventsStorage.EventEntry.TABLE_NAME, null, contentValues) != -1) {
                    this.h++;
                }
            } catch (Throwable th) {
                Tracker.a(4, "DAB", "putEvent", th);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.Object, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        Object[] objArr = new Object[this.e.size()];
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            objArr[i2] = b(this.e.get(i2));
        }
        b();
        b(z);
        for (int i3 = 0; i3 < this.e.size(); i3++) {
            Object obj = objArr[i3];
            if (obj != null) {
                a(this.e.get(i3), obj, true);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final Object b(String str) {
        synchronized (d) {
            Object obj = this.a.get(str);
            Map<String, Object> map = this.a;
            Object obj2 = map.get(str + "-long");
            if (obj == null) {
                obj = this.f.getAll().get(str);
            }
            if (obj2 == null) {
                Map<String, ?> all = this.f.getAll();
                obj2 = all.get(str + "-long");
            }
            if (obj instanceof Boolean) {
                return obj;
            }
            if (obj instanceof Integer) {
                return obj;
            }
            if (obj2 instanceof Long) {
                return obj2;
            }
            if (obj instanceof Float) {
                return obj;
            }
            if (obj instanceof Long) {
                Double valueOf = Double.valueOf(Double.longBitsToDouble(((Long) obj).longValue()));
                return valueOf;
            }
            if (obj instanceof String) {
                String str2 = (String) obj;
                if (str2.startsWith("STR::")) {
                    String substring = str2.substring(5);
                    return substring;
                }
                try {
                    if (str2.startsWith("JSO::")) {
                        JSONObject jSONObject = new JSONObject(str2.substring(5));
                        return jSONObject;
                    } else if (str2.startsWith("JSA::")) {
                        JSONArray jSONArray = new JSONArray(str2.substring(5));
                        return jSONArray;
                    }
                } catch (JSONException e2) {
                    Tracker.a(4, "DAB", "get", e2);
                }
            }
            if (!(obj == null || obj2 == null)) {
                Tracker.a(2, "DAB", "get", str + " Unrecognized Type");
            }
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        synchronized (c) {
            while (g().length() != 0) {
                try {
                    f();
                } catch (Throwable th) {
                    Tracker.a(4, "DAB", "deleteDb", th);
                }
            }
            while (d().length() != 0) {
                c();
            }
            while (j().length() != 0) {
                i();
            }
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(JSONObject jSONObject) {
        synchronized (c) {
            Tracker.a(5, "DAB", "putUpdate", jSONObject);
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("data", x.a(jSONObject));
                if (l().insert("updates", null, contentValues) != -1) {
                    this.g++;
                }
            } catch (Throwable th) {
                Tracker.a(4, "DAB", "putUpdate", th);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(boolean z) {
        synchronized (d) {
            this.f.edit().clear().apply();
            if (!z) {
                this.a.clear();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(JSONObject jSONObject) {
        synchronized (c) {
            Tracker.a(5, "DAB", "putClick", jSONObject);
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("data", x.a(jSONObject));
                if (l().insert("clicks", null, contentValues) != -1) {
                    this.i++;
                }
            } catch (Throwable th) {
                Tracker.a(4, "DAB", "putClick", th);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ac, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(boolean r6) {
        /*
            r5 = this;
            java.lang.Object r0 = com.kochava.base.d.d
            monitor-enter(r0)
            boolean r1 = r5.j     // Catch:{ all -> 0x00ad }
            if (r1 != r6) goto L_0x0009
            monitor-exit(r0)     // Catch:{ all -> 0x00ad }
            return
        L_0x0009:
            r5.j = r6     // Catch:{ all -> 0x00ad }
            boolean r6 = r5.j     // Catch:{ all -> 0x00ad }
            if (r6 == 0) goto L_0x007c
            android.content.SharedPreferences r6 = r5.f     // Catch:{ all -> 0x00ad }
            android.content.SharedPreferences$Editor r6 = r6.edit()     // Catch:{ all -> 0x00ad }
            java.util.Map<java.lang.String, java.lang.Object> r1 = r5.a     // Catch:{ all -> 0x00ad }
            java.util.Set r1 = r1.keySet()     // Catch:{ all -> 0x00ad }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x00ad }
        L_0x001f:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x00ad }
            if (r2 == 0) goto L_0x0073
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x00ad }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x00ad }
            java.util.Map<java.lang.String, java.lang.Object> r3 = r5.a     // Catch:{ all -> 0x00ad }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ all -> 0x00ad }
            boolean r4 = r3 instanceof java.lang.Boolean     // Catch:{ all -> 0x00ad }
            if (r4 == 0) goto L_0x003f
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ all -> 0x00ad }
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x00ad }
            r6.putBoolean(r2, r3)     // Catch:{ all -> 0x00ad }
            goto L_0x001f
        L_0x003f:
            boolean r4 = r3 instanceof java.lang.Float     // Catch:{ all -> 0x00ad }
            if (r4 == 0) goto L_0x004d
            java.lang.Float r3 = (java.lang.Float) r3     // Catch:{ all -> 0x00ad }
            float r3 = r3.floatValue()     // Catch:{ all -> 0x00ad }
            r6.putFloat(r2, r3)     // Catch:{ all -> 0x00ad }
            goto L_0x001f
        L_0x004d:
            boolean r4 = r3 instanceof java.lang.Integer     // Catch:{ all -> 0x00ad }
            if (r4 == 0) goto L_0x005b
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ all -> 0x00ad }
            int r3 = r3.intValue()     // Catch:{ all -> 0x00ad }
            r6.putInt(r2, r3)     // Catch:{ all -> 0x00ad }
            goto L_0x001f
        L_0x005b:
            boolean r4 = r3 instanceof java.lang.Long     // Catch:{ all -> 0x00ad }
            if (r4 == 0) goto L_0x0069
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ all -> 0x00ad }
            long r3 = r3.longValue()     // Catch:{ all -> 0x00ad }
            r6.putLong(r2, r3)     // Catch:{ all -> 0x00ad }
            goto L_0x001f
        L_0x0069:
            boolean r4 = r3 instanceof java.lang.String     // Catch:{ all -> 0x00ad }
            if (r4 == 0) goto L_0x001f
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x00ad }
            r6.putString(r2, r3)     // Catch:{ all -> 0x00ad }
            goto L_0x001f
        L_0x0073:
            r6.apply()     // Catch:{ all -> 0x00ad }
            java.util.Map<java.lang.String, java.lang.Object> r6 = r5.a     // Catch:{ all -> 0x00ad }
            r6.clear()     // Catch:{ all -> 0x00ad }
            goto L_0x00ab
        L_0x007c:
            android.content.SharedPreferences r6 = r5.f     // Catch:{ all -> 0x00ad }
            java.util.Map r6 = r6.getAll()     // Catch:{ all -> 0x00ad }
            java.util.Set r1 = r6.keySet()     // Catch:{ all -> 0x00ad }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x00ad }
        L_0x008a:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x00ad }
            if (r2 == 0) goto L_0x00ab
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x00ad }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x00ad }
            java.util.List<java.lang.String> r3 = r5.e     // Catch:{ all -> 0x00ad }
            boolean r3 = r3.contains(r2)     // Catch:{ all -> 0x00ad }
            if (r3 == 0) goto L_0x009f
            goto L_0x008a
        L_0x009f:
            java.lang.Object r3 = r6.get(r2)     // Catch:{ all -> 0x00ad }
            if (r3 == 0) goto L_0x008a
            java.util.Map<java.lang.String, java.lang.Object> r4 = r5.a     // Catch:{ all -> 0x00ad }
            r4.put(r2, r3)     // Catch:{ all -> 0x00ad }
            goto L_0x008a
        L_0x00ab:
            monitor-exit(r0)     // Catch:{ all -> 0x00ad }
            return
        L_0x00ad:
            r6 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00ad }
            goto L_0x00b1
        L_0x00b0:
            throw r6
        L_0x00b1:
            goto L_0x00b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.d.c(boolean):void");
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        synchronized (c) {
            Tracker.a(4, "DAB", "removeEvent", new Object[0]);
            try {
                SQLiteDatabase l = l();
                if (this.h <= 0) {
                    Tracker.a(2, "DAB", "removeEvent", "No events to remove");
                } else if (l.delete(DataBaseEventsStorage.EventEntry.TABLE_NAME, "_id IN (SELECT _id FROM events ORDER BY _id ASC LIMIT 1)", null) <= 0) {
                    return false;
                } else {
                    this.h--;
                }
                return true;
            } catch (Throwable th) {
                Tracker.a(4, "DAB", "removeEvent", th);
                return false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        if (r4 != null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        if (r4 != null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        com.kochava.base.Tracker.a(5, "DAB", "takeEvent", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0052, code lost:
        if (r2 == null) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0055, code lost:
        r2 = new org.json.JSONObject();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.json.JSONObject d() {
        /*
            r11 = this;
            java.lang.Object r0 = com.kochava.base.d.c
            monitor-enter(r0)
            java.lang.String r1 = "DAB"
            java.lang.String r2 = "takeEvent"
            r3 = 0
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x0063 }
            r5 = 5
            com.kochava.base.Tracker.a(r5, r1, r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 1
            r2 = 0
            android.database.sqlite.SQLiteDatabase r4 = r11.l()     // Catch:{ all -> 0x0036 }
            int r6 = r11.h     // Catch:{ all -> 0x0036 }
            if (r6 <= 0) goto L_0x002f
            java.lang.String r6 = "SELECT data FROM events ORDER BY _id ASC LIMIT 1"
            android.database.Cursor r4 = r4.rawQuery(r6, r2)     // Catch:{ all -> 0x0036 }
            boolean r6 = r4.moveToFirst()     // Catch:{ all -> 0x002d }
            if (r6 == 0) goto L_0x0030
            java.lang.String r6 = r4.getString(r3)     // Catch:{ all -> 0x002d }
            org.json.JSONObject r2 = com.kochava.base.x.f(r6)     // Catch:{ all -> 0x002d }
            goto L_0x0030
        L_0x002d:
            r6 = move-exception
            goto L_0x0038
        L_0x002f:
            r4 = r2
        L_0x0030:
            if (r4 == 0) goto L_0x0047
        L_0x0032:
            r4.close()     // Catch:{ all -> 0x0063 }
            goto L_0x0047
        L_0x0036:
            r6 = move-exception
            r4 = r2
        L_0x0038:
            r7 = 4
            java.lang.String r8 = "DAB"
            java.lang.String r9 = "takeEvent"
            java.lang.Object[] r10 = new java.lang.Object[r1]     // Catch:{ all -> 0x005c }
            r10[r3] = r6     // Catch:{ all -> 0x005c }
            com.kochava.base.Tracker.a(r7, r8, r9, r10)     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x0047
            goto L_0x0032
        L_0x0047:
            java.lang.String r4 = "DAB"
            java.lang.String r6 = "takeEvent"
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0063 }
            r1[r3] = r2     // Catch:{ all -> 0x0063 }
            com.kochava.base.Tracker.a(r5, r4, r6, r1)     // Catch:{ all -> 0x0063 }
            if (r2 == 0) goto L_0x0055
            goto L_0x005a
        L_0x0055:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ all -> 0x0063 }
            r2.<init>()     // Catch:{ all -> 0x0063 }
        L_0x005a:
            monitor-exit(r0)     // Catch:{ all -> 0x0063 }
            return r2
        L_0x005c:
            r1 = move-exception
            if (r4 == 0) goto L_0x0062
            r4.close()     // Catch:{ all -> 0x0063 }
        L_0x0062:
            throw r1     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0063 }
            goto L_0x0067
        L_0x0066:
            throw r1
        L_0x0067:
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.d.d():org.json.JSONObject");
    }

    /* access modifiers changed from: package-private */
    public final int e() {
        int i2;
        synchronized (c) {
            try {
                l();
                Tracker.a(4, "DAB", "getEventCount", Integer.valueOf(this.h));
                i2 = this.h;
            } catch (Throwable th) {
                Tracker.a(4, "DAB", "getEventCount", th);
                return 0;
            }
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        synchronized (c) {
            Tracker.a(4, "DAB", "removeUpdate", new Object[0]);
            try {
                SQLiteDatabase l = l();
                if (this.g <= 0) {
                    Tracker.a(2, "DAB", "removeUpdate", "No updates to remove");
                } else if (l.delete("updates", "_id IN (SELECT _id FROM updates ORDER BY _id ASC LIMIT 1)", null) <= 0) {
                    return false;
                } else {
                    this.g--;
                }
                return true;
            } catch (Throwable th) {
                Tracker.a(4, "DAB", "removeUpdate", th);
                return false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        if (r4 != null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        if (r4 != null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        com.kochava.base.Tracker.a(5, "DAB", "takeUpdate", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0052, code lost:
        if (r2 == null) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0055, code lost:
        r2 = new org.json.JSONObject();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.json.JSONObject g() {
        /*
            r11 = this;
            java.lang.Object r0 = com.kochava.base.d.c
            monitor-enter(r0)
            java.lang.String r1 = "DAB"
            java.lang.String r2 = "takeUpdate"
            r3 = 0
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x0063 }
            r5 = 5
            com.kochava.base.Tracker.a(r5, r1, r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 1
            r2 = 0
            android.database.sqlite.SQLiteDatabase r4 = r11.l()     // Catch:{ all -> 0x0036 }
            int r6 = r11.g     // Catch:{ all -> 0x0036 }
            if (r6 <= 0) goto L_0x002f
            java.lang.String r6 = "SELECT data FROM updates ORDER BY _id ASC LIMIT 1"
            android.database.Cursor r4 = r4.rawQuery(r6, r2)     // Catch:{ all -> 0x0036 }
            boolean r6 = r4.moveToFirst()     // Catch:{ all -> 0x002d }
            if (r6 == 0) goto L_0x0030
            java.lang.String r6 = r4.getString(r3)     // Catch:{ all -> 0x002d }
            org.json.JSONObject r2 = com.kochava.base.x.f(r6)     // Catch:{ all -> 0x002d }
            goto L_0x0030
        L_0x002d:
            r6 = move-exception
            goto L_0x0038
        L_0x002f:
            r4 = r2
        L_0x0030:
            if (r4 == 0) goto L_0x0047
        L_0x0032:
            r4.close()     // Catch:{ all -> 0x0063 }
            goto L_0x0047
        L_0x0036:
            r6 = move-exception
            r4 = r2
        L_0x0038:
            r7 = 4
            java.lang.String r8 = "DAB"
            java.lang.String r9 = "takeUpdate"
            java.lang.Object[] r10 = new java.lang.Object[r1]     // Catch:{ all -> 0x005c }
            r10[r3] = r6     // Catch:{ all -> 0x005c }
            com.kochava.base.Tracker.a(r7, r8, r9, r10)     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x0047
            goto L_0x0032
        L_0x0047:
            java.lang.String r4 = "DAB"
            java.lang.String r6 = "takeUpdate"
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0063 }
            r1[r3] = r2     // Catch:{ all -> 0x0063 }
            com.kochava.base.Tracker.a(r5, r4, r6, r1)     // Catch:{ all -> 0x0063 }
            if (r2 == 0) goto L_0x0055
            goto L_0x005a
        L_0x0055:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ all -> 0x0063 }
            r2.<init>()     // Catch:{ all -> 0x0063 }
        L_0x005a:
            monitor-exit(r0)     // Catch:{ all -> 0x0063 }
            return r2
        L_0x005c:
            r1 = move-exception
            if (r4 == 0) goto L_0x0062
            r4.close()     // Catch:{ all -> 0x0063 }
        L_0x0062:
            throw r1     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0063 }
            goto L_0x0067
        L_0x0066:
            throw r1
        L_0x0067:
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.d.g():org.json.JSONObject");
    }

    /* access modifiers changed from: package-private */
    public final int h() {
        int i2;
        synchronized (c) {
            try {
                l();
                Tracker.a(4, "DAB", "getUpdateCoun", Integer.valueOf(this.g));
                i2 = this.g;
            } catch (Throwable th) {
                Tracker.a(4, "DAB", "getUpdateCoun", th);
                return 0;
            }
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public final boolean i() {
        synchronized (c) {
            Tracker.a(4, "DAB", "removeClick", new Object[0]);
            try {
                SQLiteDatabase l = l();
                if (this.i <= 0) {
                    Tracker.a(2, "DAB", "removeClick", "No clicks to remove");
                } else if (l.delete("clicks", "_id IN (SELECT _id FROM clicks ORDER BY _id ASC LIMIT 1)", null) <= 0) {
                    return false;
                } else {
                    this.i--;
                }
                return true;
            } catch (Throwable th) {
                Tracker.a(4, "DAB", "removeClick", th);
                return false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        if (r4 != null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        if (r4 != null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        com.kochava.base.Tracker.a(5, "DAB", "takeClick", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0052, code lost:
        if (r2 == null) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0055, code lost:
        r2 = new org.json.JSONObject();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.json.JSONObject j() {
        /*
            r11 = this;
            java.lang.Object r0 = com.kochava.base.d.c
            monitor-enter(r0)
            java.lang.String r1 = "DAB"
            java.lang.String r2 = "takeClick"
            r3 = 0
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x0063 }
            r5 = 5
            com.kochava.base.Tracker.a(r5, r1, r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 1
            r2 = 0
            android.database.sqlite.SQLiteDatabase r4 = r11.l()     // Catch:{ all -> 0x0036 }
            int r6 = r11.i     // Catch:{ all -> 0x0036 }
            if (r6 <= 0) goto L_0x002f
            java.lang.String r6 = "SELECT data FROM clicks ORDER BY _id ASC LIMIT 1"
            android.database.Cursor r4 = r4.rawQuery(r6, r2)     // Catch:{ all -> 0x0036 }
            boolean r6 = r4.moveToFirst()     // Catch:{ all -> 0x002d }
            if (r6 == 0) goto L_0x0030
            java.lang.String r6 = r4.getString(r3)     // Catch:{ all -> 0x002d }
            org.json.JSONObject r2 = com.kochava.base.x.f(r6)     // Catch:{ all -> 0x002d }
            goto L_0x0030
        L_0x002d:
            r6 = move-exception
            goto L_0x0038
        L_0x002f:
            r4 = r2
        L_0x0030:
            if (r4 == 0) goto L_0x0047
        L_0x0032:
            r4.close()     // Catch:{ all -> 0x0063 }
            goto L_0x0047
        L_0x0036:
            r6 = move-exception
            r4 = r2
        L_0x0038:
            r7 = 4
            java.lang.String r8 = "DAB"
            java.lang.String r9 = "takeClick"
            java.lang.Object[] r10 = new java.lang.Object[r1]     // Catch:{ all -> 0x005c }
            r10[r3] = r6     // Catch:{ all -> 0x005c }
            com.kochava.base.Tracker.a(r7, r8, r9, r10)     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x0047
            goto L_0x0032
        L_0x0047:
            java.lang.String r4 = "DAB"
            java.lang.String r6 = "takeClick"
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0063 }
            r1[r3] = r2     // Catch:{ all -> 0x0063 }
            com.kochava.base.Tracker.a(r5, r4, r6, r1)     // Catch:{ all -> 0x0063 }
            if (r2 == 0) goto L_0x0055
            goto L_0x005a
        L_0x0055:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ all -> 0x0063 }
            r2.<init>()     // Catch:{ all -> 0x0063 }
        L_0x005a:
            monitor-exit(r0)     // Catch:{ all -> 0x0063 }
            return r2
        L_0x005c:
            r1 = move-exception
            if (r4 == 0) goto L_0x0062
            r4.close()     // Catch:{ all -> 0x0063 }
        L_0x0062:
            throw r1     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0063 }
            goto L_0x0067
        L_0x0066:
            throw r1
        L_0x0067:
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.d.j():org.json.JSONObject");
    }

    /* access modifiers changed from: package-private */
    public final int k() {
        int i2;
        synchronized (c) {
            try {
                l();
                Tracker.a(4, "DAB", "getClickCount", Integer.valueOf(this.i));
                i2 = this.i;
            } catch (Throwable th) {
                Tracker.a(4, "DAB", "getClickCount", th);
                return 0;
            }
        }
        return i2;
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        synchronized (c) {
            Tracker.a(5, "DAB", "onCreate", new Object[0]);
            try {
                sQLiteDatabase.execSQL("CREATE TABLE events (_id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT NOT NULL);");
                sQLiteDatabase.execSQL("CREATE TABLE updates (_id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT NOT NULL);");
                sQLiteDatabase.execSQL("CREATE TABLE clicks (_id INTEGER PRIMARY KEY AUTOINCREMENT, data TEXT NOT NULL);");
            } catch (Throwable th) {
                Tracker.a(5, "DAB", "onCreate", th);
            }
        }
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i2, int i3) {
        synchronized (c) {
            Tracker.a(5, "DAB", "onUpgrade", Integer.toString(i2) + "," + Integer.toString(i3));
            try {
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS updates");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS clicks");
                onCreate(sQLiteDatabase);
                a();
            } catch (Throwable th) {
                Tracker.a(5, "DAB", "onUpgrade", th);
            }
        }
    }
}
