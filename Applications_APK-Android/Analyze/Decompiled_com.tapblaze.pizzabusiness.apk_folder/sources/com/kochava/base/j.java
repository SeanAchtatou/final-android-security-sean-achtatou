package com.kochava.base;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import com.flurry.android.Constants;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

abstract class j implements Runnable {
    private static final Object b = new Object();
    final i a;
    private final boolean c;
    private final long d;
    private final boolean e;
    private final long f;
    private int g = 0;
    private boolean h = false;
    private boolean i = false;

    j(i iVar, boolean z) {
        this.a = iVar;
        this.c = z;
        this.e = iVar.g.m();
        this.d = iVar.s;
        this.f = x.b();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    static int a(JSONObject jSONObject) {
        char c2;
        String a2 = x.a(jSONObject.opt("action"), "");
        switch (a2.hashCode()) {
            case -1239656817:
                if (a2.equals("push_token_remove")) {
                    c2 = 8;
                    break;
                }
                c2 = 65535;
                break;
            case -838846263:
                if (a2.equals("update")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case -120977960:
                if (a2.equals("identityLink")) {
                    c2 = 6;
                    break;
                }
                c2 = 65535;
                break;
            case 3237136:
                if (a2.equals("init")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case 72642707:
                if (a2.equals("location_update")) {
                    c2 = 9;
                    break;
                }
                c2 = 65535;
                break;
            case 96891546:
                if (a2.equals(NotificationCompat.CATEGORY_EVENT)) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            case 530263318:
                if (a2.equals("get_attribution")) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case 991290412:
                if (a2.equals("geo_event")) {
                    c2 = 10;
                    break;
                }
                c2 = 65535;
                break;
            case 1948342084:
                if (a2.equals("initial")) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case 1951714934:
                if (a2.equals("push_token_add")) {
                    c2 = 7;
                    break;
                }
                c2 = 65535;
                break;
            case 1984987798:
                if (a2.equals(SettingsJsonConstants.SESSION_KEY)) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                JSONObject f2 = x.f(jSONObject.opt("data"));
                return (f2 == null || !"pause".equalsIgnoreCase(x.a(f2.opt("state"), ""))) ? 2 : 3;
            case 3:
                return 4;
            case 4:
                return 5;
            case 5:
                return 6;
            case 6:
                return 7;
            case 7:
                return 8;
            case 8:
                return 9;
            case 9:
                return 10;
            case 10:
                return 11;
            default:
                return 6;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    private String a(Object obj) {
        if (obj instanceof JSONObject) {
            JSONObject jSONObject = (JSONObject) obj;
            b(jSONObject);
            return x.a(jSONObject);
        } else if (obj instanceof JSONArray) {
            JSONArray jSONArray = (JSONArray) obj;
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                b(x.b(jSONArray.opt(i2), true));
            }
            return x.a(jSONArray);
        } else {
            throw new IOException("Invalid Payload Type");
        }
    }

    private static String a(String str) {
        long j = 0;
        for (byte b2 : str.getBytes(x.a())) {
            j += (long) (b2 & Constants.UNKNOWN);
        }
        return String.format(Locale.US, "%03d", Long.valueOf(j % 1000));
    }

    static void a(int i2, JSONObject jSONObject) {
        String str;
        switch (i2) {
            case 0:
                str = "init";
                break;
            case 1:
                str = "initial";
                break;
            case 2:
            case 3:
                str = SettingsJsonConstants.SESSION_KEY;
                break;
            case 4:
                str = "update";
                break;
            case 5:
                str = "get_attribution";
                break;
            case 6:
                str = NotificationCompat.CATEGORY_EVENT;
                break;
            case 7:
                str = "identityLink";
                break;
            case 8:
                str = "push_token_add";
                break;
            case 9:
                str = "push_token_remove";
                break;
            case 10:
                str = "location_update";
                break;
            case 11:
                str = "geo_event";
                break;
            default:
                return;
        }
        x.a("action", str, jSONObject);
    }

    private static void a(Context context, String str, JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append("c");
        sb.append(a(str));
        sb.append("-");
        try {
            sb.append("s");
            sb.append(new File(context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).publicSourceDir).length() / 1000);
            sb.append("-");
        } catch (PackageManager.NameNotFoundException unused) {
        }
        x.a("sdk_id", sb.toString(), jSONObject);
    }

    private static void a(Context context, JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        if (Build.VERSION.SDK_INT >= 24) {
            x.a("min_api", Integer.valueOf(context.getApplicationInfo().minSdkVersion), jSONObject2);
        }
        x.a("target_api", Integer.valueOf(context.getApplicationInfo().targetSdkVersion), jSONObject2);
        x.a("metrics", jSONObject2, jSONObject);
    }

    private static void a(d dVar, JSONObject jSONObject) {
        String str;
        String a2 = x.a(dVar.b("ext_date"));
        StringBuilder sb = new StringBuilder();
        sb.append("2020-01-16T23:32:11Z");
        if (a2 != null) {
            str = " (" + a2 + ")";
        } else {
            str = "";
        }
        sb.append(str);
        x.a("sdk_build_date", sb.toString(), jSONObject);
    }

    private static void a(d dVar, JSONObject jSONObject, JSONArray jSONArray) {
        JSONObject f2;
        if (jSONArray != null && !x.a(jSONArray, "identity_link") && (f2 = x.f(dVar.b("identity_link_all"))) != null) {
            dVar.a("identity_link");
            x.a("identity_link", f2, jSONObject);
        }
    }

    private static void a(d dVar, JSONObject jSONObject, JSONArray jSONArray, JSONArray jSONArray2) {
        JSONObject f2 = x.f(dVar.b("custom"));
        if (f2 != null && f2.length() > 0) {
            Iterator<String> keys = f2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                String a2 = x.a(f2.opt(next));
                if (a2 == null || !x.a(jSONArray2, next) || x.a(jSONArray, next)) {
                    Tracker.a(4, "TSK", "addCustomItem", "Custom item not in whitelist. Ignoring.", next, a2);
                } else {
                    x.a(next, a2, jSONObject);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    private static void a(d dVar, JSONObject jSONObject, JSONArray jSONArray, boolean z) {
        if (jSONArray != null && !x.a(jSONArray, "app_limit_tracking")) {
            if (!z || x.a(dVar.b("app_limit_trackingupd"), false)) {
                dVar.a("app_limit_trackingupd", (Object) false);
                x.a("app_limit_tracking", Boolean.valueOf(x.a(dVar.b("app_limit_tracking"), false)), jSONObject);
            }
        }
    }

    static void a(JSONObject jSONObject, d dVar) {
        String a2 = x.a(dVar.b("kochava_app_id_override"));
        if (!(a2 == null && (a2 = x.a(dVar.b("kochava_app_id"))) == null)) {
            x.a("kochava_app_id", a2, jSONObject);
        }
        String a3 = x.a(dVar.b("kochava_device_id"));
        if (a3 != null) {
            x.a("kochava_device_id", a3, jSONObject);
        }
    }

    private static void a(JSONObject jSONObject, JSONArray jSONArray, int i2) {
        if (jSONArray != null && !x.a(jSONArray, "state_active_count")) {
            x.a("state_active_count", Integer.valueOf(i2), jSONObject);
        }
    }

    private static void a(JSONObject jSONObject, JSONArray jSONArray, boolean z) {
        if (jSONArray != null && !x.a(jSONArray, "state_active")) {
            x.a("state_active", Boolean.valueOf(z), jSONObject);
        }
    }

    private static void b(d dVar, JSONObject jSONObject) {
        String a2 = x.a(dVar.b("partner_name"));
        if (a2 != null) {
            x.a("partner_name", a2, jSONObject);
        }
    }

    private static void b(d dVar, JSONObject jSONObject, JSONArray jSONArray) {
        if (!x.a(jSONArray, "conversion_type") && !x.a(jSONArray, "conversion_data")) {
            String a2 = x.a(dVar.b("referrer"));
            if (a2 == null) {
                String a3 = x.a(jSONObject.opt("installer_package"), "");
                boolean z = x.f(jSONObject.opt("install_referrer")) != null && "ok".equals(x.a(jSONObject.opt("status"), ""));
                if ("com.android.vending".equalsIgnoreCase(a3) && !z) {
                    try {
                        ReferralReceiver.a.await(10, TimeUnit.SECONDS);
                    } catch (InterruptedException e2) {
                        Tracker.a(4, "TSK", "addConversion", e2);
                    }
                }
                a2 = x.a(dVar.b("referrer"));
            }
            String a4 = x.a(dVar.b("referrer_source"));
            if (a2 != null && a4 != null) {
                x.a("conversion_type", a4, jSONObject);
                x.a("conversion_data", a2, jSONObject);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    private void b(JSONObject jSONObject) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String format = simpleDateFormat.format(new Date(x.b()));
        StringBuilder sb = new StringBuilder();
        sb.append(x.a(jSONObject.opt("nt_id"), ""));
        sb.append(x.a(jSONObject.opt("kochava_app_id"), ""));
        sb.append(x.a(jSONObject.opt("kochava_device_id"), ""));
        sb.append(x.a(jSONObject.opt("sdk_version"), ""));
        sb.append(format);
        JSONObject b2 = x.b(jSONObject.opt("data"), true);
        for (String opt : new String[]{"adid", "android_id", "fire_adid", "fb_attribution_id", "custom", "custom_id", "conversion_data"}) {
            sb.append(x.a(b2.opt(opt), ""));
        }
        for (String opt2 : new String[]{"usertime"}) {
            sb.append(Integer.toString(x.b(b2.opt(opt2), 0)));
        }
        JSONObject f2 = x.f(b2.opt("ids"));
        if (f2 != null) {
            sb.append(x.a(f2.opt("email"), ""));
        }
        JSONObject f3 = x.f(b2.opt("install_referrer"));
        if (f3 != null) {
            sb.append(x.a(f3.opt("referrer"), ""));
            sb.append(x.a(f3.opt("status"), ""));
            Integer c2 = x.c(f3.opt("install_begin_time"));
            if (c2 != null) {
                sb.append(Integer.toString(c2.intValue()));
            }
            Integer c3 = x.c(f3.opt("referrer_click_time"));
            if (c3 != null) {
                sb.append(Integer.toString(c3.intValue()));
            }
        }
        x.a("send_date", format + "." + a(sb.toString()) + "Z", jSONObject);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, org.json.JSONObject, int]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void */
    private static void c(d dVar, JSONObject jSONObject) {
        JSONObject f2 = x.f(dVar.b("identity_link"));
        if (f2 != null) {
            dVar.a("identity_link");
            x.a(jSONObject, f2, false);
        }
    }

    /* access modifiers changed from: package-private */
    public final long a() {
        return this.c ? this.d : this.a.s;
    }

    /* access modifiers changed from: package-private */
    public final String a(String str, boolean z) {
        try {
            if (x.b(this.a.a)) {
                return x.a(str, z);
            }
            Tracker.a(4, "TSK", "httpGet", "Error: No Network Connection");
            return null;
        } catch (Throwable th) {
            Tracker.a(4, "TSK", "httpGet", th);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final JSONObject a(int i2, Object obj) {
        try {
            if (x.b(this.a.a)) {
                return new JSONObject(x.a(this.a.a(i2), a(obj)));
            }
            Tracker.a(4, "TSK", "httpPost", "Error: No Network Connection");
            return null;
        } catch (Throwable th) {
            Tracker.a(4, "TSK", "httpPost", th);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
     arg types: [java.lang.String, boolean, org.json.JSONObject]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject, org.json.JSONArray, boolean):void
     arg types: [com.kochava.base.d, org.json.JSONObject, org.json.JSONArray, int]
     candidates:
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject, org.json.JSONArray, org.json.JSONArray):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject, org.json.JSONArray, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(org.json.JSONObject, org.json.JSONArray, boolean):void
     arg types: [org.json.JSONObject, org.json.JSONArray, int]
     candidates:
      com.kochava.base.j.a(android.content.Context, java.lang.String, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject, org.json.JSONArray):void
      com.kochava.base.j.a(org.json.JSONObject, org.json.JSONArray, int):void
      com.kochava.base.j.a(int, org.json.JSONObject, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, org.json.JSONArray, boolean):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x018d  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x01b1  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0217  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r20, org.json.JSONObject r21, org.json.JSONObject r22) {
        /*
            r19 = this;
            r1 = r19
            r0 = r20
            r11 = r21
            r12 = r22
            com.kochava.base.i r2 = r1.a
            com.kochava.base.d r2 = r2.d
            java.lang.String r3 = "dp_options"
            java.lang.Object r2 = r2.b(r3)
            r13 = 1
            org.json.JSONObject r4 = com.kochava.base.x.b(r2, r13)
            com.kochava.base.i r2 = r1.a
            com.kochava.base.d r2 = r2.d
            java.lang.String r3 = "dp_override"
            java.lang.Object r2 = r2.b(r3)
            org.json.JSONObject r5 = com.kochava.base.x.b(r2, r13)
            com.kochava.base.i r2 = r1.a
            com.kochava.base.d r2 = r2.d
            java.lang.String r3 = "blacklist"
            java.lang.Object r2 = r2.b(r3)
            org.json.JSONArray r14 = com.kochava.base.x.g(r2)
            com.kochava.base.i r2 = r1.a
            com.kochava.base.d r2 = r2.d
            java.lang.String r3 = "whitelist"
            java.lang.Object r2 = r2.b(r3)
            org.json.JSONArray r15 = com.kochava.base.x.g(r2)
            java.lang.Object[] r2 = new java.lang.Object[r13]
            r10 = 0
            java.lang.String r3 = "Main"
            r2[r10] = r3
            r9 = 5
            java.lang.String r3 = "TSK"
            java.lang.String r6 = "buildPayload"
            com.kochava.base.Tracker.a(r9, r3, r6, r2)
            com.kochava.base.i r2 = r1.a
            android.content.Context r2 = r2.a
            com.kochava.base.i r3 = r1.a
            com.kochava.base.d r3 = r3.d
            com.kochava.base.i r6 = r1.a
            java.util.List<java.lang.String> r6 = r6.b
            r7 = r14
            r8 = r15
            r9 = r20
            r10 = r22
            com.kochava.base.c.a(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            java.lang.Object r2 = com.kochava.base.j.b
            monitor-enter(r2)
            java.lang.String r3 = "TSK"
            java.lang.String r4 = "buildPayload"
            java.lang.Object[] r5 = new java.lang.Object[r13]     // Catch:{ all -> 0x023a }
            java.lang.String r6 = "Extra in lock"
            r7 = 0
            r5[r7] = r6     // Catch:{ all -> 0x023a }
            r6 = 5
            com.kochava.base.Tracker.a(r6, r3, r4, r5)     // Catch:{ all -> 0x023a }
            r3 = 3
            r4 = 2
            if (r14 != 0) goto L_0x008b
            r5 = 6
            if (r0 == r5) goto L_0x0082
            if (r0 == r4) goto L_0x0082
            if (r0 != r3) goto L_0x008b
        L_0x0082:
            java.lang.String r5 = "backfilled"
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r13)     // Catch:{ all -> 0x023a }
            com.kochava.base.x.a(r5, r8, r11)     // Catch:{ all -> 0x023a }
        L_0x008b:
            a(r20, r21)     // Catch:{ all -> 0x023a }
            com.kochava.base.i r5 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r5 = r5.d     // Catch:{ all -> 0x023a }
            a(r11, r5)     // Catch:{ all -> 0x023a }
            java.lang.String r5 = "sdk_protocol"
            r8 = 14
            java.lang.String r8 = java.lang.Integer.toString(r8)     // Catch:{ all -> 0x023a }
            com.kochava.base.x.a(r5, r8, r11)     // Catch:{ all -> 0x023a }
            java.lang.String r5 = "sdk_version"
            com.kochava.base.i r8 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r8 = r8.d     // Catch:{ all -> 0x023a }
            java.lang.String r9 = "sdk_version"
            java.lang.Object r8 = r8.b(r9)     // Catch:{ all -> 0x023a }
            java.lang.String r9 = ""
            java.lang.String r8 = com.kochava.base.x.a(r8, r9)     // Catch:{ all -> 0x023a }
            com.kochava.base.x.a(r5, r8, r11)     // Catch:{ all -> 0x023a }
            java.lang.String r5 = "nt_id"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x023a }
            r8.<init>()     // Catch:{ all -> 0x023a }
            com.kochava.base.i r9 = r1.a     // Catch:{ all -> 0x023a }
            java.lang.String r9 = r9.l     // Catch:{ all -> 0x023a }
            r8.append(r9)     // Catch:{ all -> 0x023a }
            java.lang.String r9 = "-"
            r8.append(r9)     // Catch:{ all -> 0x023a }
            java.util.UUID r9 = java.util.UUID.randomUUID()     // Catch:{ all -> 0x023a }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x023a }
            r8.append(r9)     // Catch:{ all -> 0x023a }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x023a }
            com.kochava.base.x.a(r5, r8, r11)     // Catch:{ all -> 0x023a }
            java.lang.String r5 = "data"
            com.kochava.base.x.a(r5, r12, r11)     // Catch:{ all -> 0x023a }
            java.lang.String r5 = "usertime"
            long r8 = r19.c()     // Catch:{ all -> 0x023a }
            r16 = 1000(0x3e8, double:4.94E-321)
            long r8 = r8 / r16
            java.lang.Long r8 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x023a }
            com.kochava.base.x.a(r5, r8, r12)     // Catch:{ all -> 0x023a }
            boolean r5 = r19.b()     // Catch:{ all -> 0x023a }
            r8 = 4666723172467343360(0x40c3880000000000, double:10000.0)
            r16 = 10
            if (r5 != 0) goto L_0x011e
            if (r0 == r3) goto L_0x011e
            if (r0 != r4) goto L_0x0102
            goto L_0x011e
        L_0x0102:
            java.lang.String r3 = "uptime"
            long r4 = r19.c()     // Catch:{ all -> 0x023a }
            com.kochava.base.i r10 = r1.a     // Catch:{ all -> 0x023a }
            r18 = r14
            long r13 = r10.p     // Catch:{ all -> 0x023a }
            long r4 = r4 - r13
            long r4 = r4 * r16
            double r4 = (double) r4
            java.lang.Double.isNaN(r4)
            double r4 = r4 / r8
            java.lang.Double r4 = java.lang.Double.valueOf(r4)     // Catch:{ all -> 0x023a }
        L_0x011a:
            com.kochava.base.x.a(r3, r4, r12)     // Catch:{ all -> 0x023a }
            goto L_0x014e
        L_0x011e:
            r18 = r14
            if (r0 == r4) goto L_0x0131
            com.kochava.base.i r3 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r3 = r3.d     // Catch:{ all -> 0x023a }
            java.lang.String r4 = "session_window_uptime"
            java.lang.Object r3 = r3.b(r4)     // Catch:{ all -> 0x023a }
            int r10 = com.kochava.base.x.b(r3, r7)     // Catch:{ all -> 0x023a }
            goto L_0x0132
        L_0x0131:
            r10 = 0
        L_0x0132:
            java.lang.String r3 = "uptime"
            long r4 = r19.c()     // Catch:{ all -> 0x023a }
            long r13 = r19.a()     // Catch:{ all -> 0x023a }
            long r4 = r4 - r13
            long r4 = r4 * r16
            double r4 = (double) r4
            java.lang.Double.isNaN(r4)
            double r4 = r4 / r8
            double r8 = (double) r10
            java.lang.Double.isNaN(r8)
            double r4 = r4 + r8
            java.lang.Double r4 = java.lang.Double.valueOf(r4)     // Catch:{ all -> 0x023a }
            goto L_0x011a
        L_0x014e:
            com.kochava.base.i r3 = r1.a     // Catch:{ all -> 0x023a }
            boolean r3 = r3.m     // Catch:{ all -> 0x023a }
            if (r3 == 0) goto L_0x018a
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ all -> 0x023a }
            r3.<init>()     // Catch:{ all -> 0x023a }
            java.lang.String r4 = "required"
            com.kochava.base.i r5 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.b r5 = r5.g     // Catch:{ all -> 0x023a }
            boolean r5 = r5.d()     // Catch:{ all -> 0x023a }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ all -> 0x023a }
            com.kochava.base.x.a(r4, r5, r3)     // Catch:{ all -> 0x023a }
            com.kochava.base.i r4 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.b r4 = r4.g     // Catch:{ all -> 0x023a }
            boolean r4 = r4.e()     // Catch:{ all -> 0x023a }
            if (r4 == 0) goto L_0x0185
            java.lang.String r4 = "time"
            com.kochava.base.i r5 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.b r5 = r5.g     // Catch:{ all -> 0x023a }
            long r8 = r5.f()     // Catch:{ all -> 0x023a }
            java.lang.Long r5 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x023a }
            com.kochava.base.x.a(r4, r5, r3)     // Catch:{ all -> 0x023a }
        L_0x0185:
            java.lang.String r4 = "consent"
            com.kochava.base.x.a(r4, r3, r11)     // Catch:{ all -> 0x023a }
        L_0x018a:
            switch(r0) {
                case 0: goto L_0x0217;
                case 1: goto L_0x01e6;
                case 2: goto L_0x01db;
                case 3: goto L_0x01bc;
                case 4: goto L_0x01b1;
                case 5: goto L_0x022c;
                case 6: goto L_0x01aa;
                case 7: goto L_0x01a1;
                case 8: goto L_0x022c;
                case 9: goto L_0x022c;
                case 10: goto L_0x019a;
                case 11: goto L_0x018f;
                default: goto L_0x018d;
            }     // Catch:{ all -> 0x023a }
        L_0x018d:
            goto L_0x022c
        L_0x018f:
            boolean r0 = r19.b()     // Catch:{ all -> 0x023a }
            r3 = r18
        L_0x0195:
            a(r12, r3, r0)     // Catch:{ all -> 0x023a }
            goto L_0x022c
        L_0x019a:
            r3 = r18
            boolean r0 = r19.b()     // Catch:{ all -> 0x023a }
            goto L_0x0195
        L_0x01a1:
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x023a }
            c(r0, r12)     // Catch:{ all -> 0x023a }
            goto L_0x022c
        L_0x01aa:
            r3 = r18
            boolean r0 = r19.b()     // Catch:{ all -> 0x023a }
            goto L_0x0195
        L_0x01b1:
            r3 = r18
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x023a }
            r4 = 1
            a(r0, r12, r3, r4)     // Catch:{ all -> 0x023a }
            goto L_0x022c
        L_0x01bc:
            r3 = r18
            r4 = 1
            a(r12, r3, r4)     // Catch:{ all -> 0x023a }
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x023a }
            java.lang.String r5 = "session_state_active_count"
            java.lang.Object r0 = r0.b(r5)     // Catch:{ all -> 0x023a }
            int r0 = com.kochava.base.x.b(r0, r4)     // Catch:{ all -> 0x023a }
            a(r12, r3, r0)     // Catch:{ all -> 0x023a }
            java.lang.String r0 = "state"
            java.lang.String r3 = "pause"
        L_0x01d7:
            com.kochava.base.x.a(r0, r3, r12)     // Catch:{ all -> 0x023a }
            goto L_0x022c
        L_0x01db:
            r3 = r18
            r0 = 1
            a(r12, r3, r0)     // Catch:{ all -> 0x023a }
            java.lang.String r0 = "state"
            java.lang.String r3 = "resume"
            goto L_0x01d7
        L_0x01e6:
            r3 = r18
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            android.content.Context r0 = r0.a     // Catch:{ all -> 0x023a }
            com.kochava.base.i r4 = r1.a     // Catch:{ all -> 0x023a }
            java.lang.String r4 = r4.o     // Catch:{ all -> 0x023a }
            a(r0, r4, r11)     // Catch:{ all -> 0x023a }
            boolean r0 = r19.b()     // Catch:{ all -> 0x023a }
            a(r12, r3, r0)     // Catch:{ all -> 0x023a }
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x023a }
            a(r0, r12, r3, r7)     // Catch:{ all -> 0x023a }
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x023a }
            a(r0, r12, r3)     // Catch:{ all -> 0x023a }
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x023a }
            b(r0, r12, r3)     // Catch:{ all -> 0x023a }
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x023a }
            a(r0, r12, r3, r15)     // Catch:{ all -> 0x023a }
            goto L_0x022c
        L_0x0217:
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x023a }
            a(r0, r11)     // Catch:{ all -> 0x023a }
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x023a }
            b(r0, r12)     // Catch:{ all -> 0x023a }
            com.kochava.base.i r0 = r1.a     // Catch:{ all -> 0x023a }
            android.content.Context r0 = r0.a     // Catch:{ all -> 0x023a }
            a(r0, r11)     // Catch:{ all -> 0x023a }
        L_0x022c:
            monitor-exit(r2)     // Catch:{ all -> 0x023a }
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r0[r7] = r11
            java.lang.String r2 = "TSK"
            java.lang.String r3 = "buildPayload"
            com.kochava.base.Tracker.a(r6, r2, r3, r0)
            return
        L_0x023a:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x023a }
            goto L_0x023e
        L_0x023d:
            throw r0
        L_0x023e:
            goto L_0x023d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.j.a(int, org.json.JSONObject, org.json.JSONObject):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: package-private */
    public final void a(long j) {
        Tracker.a(4, "TSK", "wakeSelf", Long.toString(j));
        this.a.a(this, Math.max(0L, j));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, double):double
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, double):double */
    /* access modifiers changed from: package-private */
    public final boolean a(JSONObject jSONObject, boolean z) {
        if (jSONObject == null) {
            Tracker.a(4, "TSK", "checkErrorAnd", "Network Error");
            if (z) {
                m();
            }
            return true;
        }
        String a2 = x.a(jSONObject.opt("error"), "");
        if (!a2.isEmpty()) {
            Tracker.a(2, "TSK", "checkErrorAnd", "Error: " + a2);
        }
        if (!x.a(jSONObject.opt("success"), false)) {
            Tracker.a(4, "TSK", "checkErrorAnd", "Success False");
            if (z) {
                m();
            }
            return true;
        }
        JSONObject f2 = x.f(jSONObject.opt("data"));
        if (f2 != null) {
            double a3 = x.a(f2.opt("retry"), -1.0d);
            if (a3 >= FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                Tracker.a(4, "TSK", "checkErrorAnd", "Retry Time");
                if (z) {
                    a(Math.round(a3 * 1000.0d));
                }
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, org.json.JSONObject, int]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void */
    /* access modifiers changed from: package-private */
    public final JSONObject b(int i2, JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        JSONObject jSONObject3 = new JSONObject();
        a(i2, jSONObject2, jSONObject3);
        x.a(jSONObject3, jSONObject, false);
        return jSONObject3;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.c ? this.e : this.a.g.m();
    }

    /* access modifiers changed from: package-private */
    public final long c() {
        return this.c ? this.f : x.b();
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        h();
        this.h = true;
    }

    public final boolean e() {
        return this.h;
    }

    public final void f() {
        this.i = true;
    }

    public final boolean g() {
        return this.i;
    }

    public final void h() {
        this.a.a(this);
        this.h = false;
        this.i = false;
        l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [java.lang.Runnable, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* access modifiers changed from: package-private */
    public final void i() {
        i iVar = this.a;
        iVar.a(iVar.f, false);
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        if (!this.a.g.l()) {
            Tracker.a(4, "TSK", "wakeControlle", "Controller Busy. Returning.");
            return;
        }
        i();
    }

    /* access modifiers changed from: package-private */
    public final int k() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        this.g = 0;
    }

    /* access modifiers changed from: package-private */
    public final void m() {
        Tracker.a(4, "TSK", "retry", new Object[0]);
        this.g = x.a(this.g + 1, 1, 5);
        int i2 = this.g;
        a(i2 != 2 ? i2 != 3 ? i2 != 4 ? i2 != 5 ? 3000 : 300000 : 60000 : 30000 : 10000);
    }

    /* access modifiers changed from: package-private */
    public final boolean n() {
        long a2 = this.a.u.a();
        if (a2 == 0) {
            return false;
        }
        if (a2 == -1) {
            Tracker.a(4, "TSK", "checkRateLimi", "Rate limited Permanent. Cannot send until disabled.");
            return true;
        }
        Tracker.a(4, "TSK", "checkRateLimi", "Rate limited, delaying for " + a2 + " milliseconds");
        a(a2);
        return true;
    }
}
