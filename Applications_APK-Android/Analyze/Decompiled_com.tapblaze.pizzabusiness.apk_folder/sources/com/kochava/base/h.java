package com.kochava.base;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import java.lang.ref.WeakReference;

final class h implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    private final Handler a;
    /* access modifiers changed from: private */
    public final g b;
    private WeakReference<Activity> c = null;
    /* access modifiers changed from: private */
    public boolean d = false;
    private final Runnable e = new Runnable() {
        public final void run() {
            Tracker.a(4, "SMO", "goInactive", new Object[0]);
            boolean unused = h.this.d = false;
            h.this.b.f(false);
        }
    };

    h(Context context, Handler handler, g gVar) {
        this.b = gVar;
        this.a = handler;
        Tracker.a(5, "SMO", "SessionMonito", new Object[0]);
        if (context instanceof Application) {
            ((Application) context).registerActivityLifecycleCallbacks(this);
            context.registerComponentCallbacks(this);
        } else {
            Tracker.a(2, "SMO", "SessionMonito", "Invalid Application Context");
        }
        if (x.a(context)) {
            this.d = true;
            gVar.f(true);
        }
    }

    private void b() {
        this.a.removeCallbacks(this.e);
        if (!this.d) {
            Tracker.a(4, "SMO", "goActive", "goActive");
            this.d = true;
            this.b.f(true);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.d;
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        Tracker.a(5, "SMO", "onActivityCre", new Object[0]);
    }

    public final void onActivityDestroyed(Activity activity) {
        Tracker.a(5, "SMO", "onActivityDes", new Object[0]);
    }

    public final void onActivityPaused(Activity activity) {
        Tracker.a(5, "SMO", "onActivityPau", new Object[0]);
        if (this.c == null) {
            this.c = new WeakReference<>(activity);
        }
    }

    public final void onActivityResumed(Activity activity) {
        Tracker.a(5, "SMO", "onActivityRes", new Object[0]);
        if (this.c == null) {
            this.c = new WeakReference<>(activity);
        }
        b();
        this.b.k();
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Tracker.a(5, "SMO", "onActivitySav", new Object[0]);
    }

    public final void onActivityStarted(Activity activity) {
        Tracker.a(5, "SMO", "onActivitySta", Boolean.toString(this.d));
        this.c = new WeakReference<>(activity);
        b();
    }

    public final void onActivityStopped(Activity activity) {
        WeakReference<Activity> weakReference;
        Activity activity2;
        Tracker.a(5, "SMO", "onActivitySto", Boolean.toString(this.d));
        if (this.d && (weakReference = this.c) != null && (activity2 = weakReference.get()) != null && activity2.equals(activity)) {
            Tracker.a(5, "SMO", "onActivitySto", "?GoInactive?");
            this.a.removeCallbacks(this.e);
            this.a.postDelayed(this.e, 3000);
        }
        this.c = null;
    }

    public final void onConfigurationChanged(Configuration configuration) {
        Tracker.a(5, "SMO", "onConfigurati", new Object[0]);
    }

    public final void onLowMemory() {
        Tracker.a(5, "SMO", "onLowMemory", new Object[0]);
    }

    public final void onTrimMemory(int i) {
        Tracker.a(5, "SMO", "onTrimMemory", Boolean.toString(this.d));
        if (this.d && i == 20) {
            Tracker.a(5, "SMO", "onTrimMemory", "GoInactive");
            this.a.removeCallbacks(this.e);
            this.d = false;
            this.b.f(false);
        }
    }
}
