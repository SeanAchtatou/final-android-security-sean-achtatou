package com.kochava.base;

import bolts.MeasurementEvent;
import com.kochava.base.location.LocationTracker;
import com.kochava.base.location.LocationTrackerListener;
import org.json.JSONObject;

final class q extends j implements LocationTrackerListener, s {
    q(i iVar) {
        super(iVar, false);
    }

    public final void a(boolean z) {
        try {
            LocationTracker.getInstance().stop(this.a.a, z);
        } catch (Throwable unused) {
            Tracker.a(5, "LTS", "stop", "Location module not found");
        }
    }

    public final void b(JSONObject jSONObject) {
        try {
            LocationTracker.getInstance().applySettings(this.a.a, jSONObject);
        } catch (Throwable unused) {
            Tracker.a(5, "LTS", "applySettings", "Location module not found");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
     arg types: [int, org.json.JSONObject]
     candidates:
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
     arg types: [org.json.JSONObject, int]
     candidates:
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean */
    public final JSONObject locationUpdate(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        x.a("location", jSONObject, jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        a(10, jSONObject3, jSONObject2);
        JSONObject a = a(10, (Object) jSONObject3);
        if (a(a, false)) {
            return null;
        }
        return a;
    }

    public final void log(int i, String str, String str2, Object... objArr) {
        Tracker.a(i, str, str2, objArr);
    }

    public final void o() {
        try {
            LocationTracker.getInstance().onActivityResume(this.a.a);
        } catch (Throwable unused) {
            Tracker.a(5, "LTS", "onActivityRes", "Location module not found");
        }
    }

    public final void run() {
        try {
            LocationTracker.getInstance().start(this.a.a, this);
        } catch (Throwable unused) {
            Tracker.a(5, "LTS", "run", "Location module not found");
        }
        d();
    }

    public final void sendGeoEvent(String str, JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        x.a("event_data", jSONObject, jSONObject2);
        x.a(MeasurementEvent.MEASUREMENT_EVENT_NAME_KEY, str, jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        a(11, jSONObject3, jSONObject2);
        this.a.d.b(jSONObject3);
        j();
    }
}
