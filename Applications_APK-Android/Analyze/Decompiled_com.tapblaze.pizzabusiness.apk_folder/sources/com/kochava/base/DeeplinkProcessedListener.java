package com.kochava.base;

public interface DeeplinkProcessedListener {
    void onDeeplinkProcessed(Deeplink deeplink);
}
