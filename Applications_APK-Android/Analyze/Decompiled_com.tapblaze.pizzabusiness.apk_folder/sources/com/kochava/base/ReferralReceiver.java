package com.kochava.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import java.util.concurrent.CountDownLatch;

public final class ReferralReceiver extends BroadcastReceiver {
    static final CountDownLatch a = new CountDownLatch(1);

    public final void onReceive(Context context, Intent intent) {
        if (!(context == null || intent == null)) {
            try {
                if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction())) {
                    String stringExtra = intent.getStringExtra("referrer");
                    if (stringExtra != null) {
                        if (!stringExtra.trim().isEmpty()) {
                            Tracker.a(3, "RRC", "onReceive", stringExtra);
                            SharedPreferences sharedPreferences = context.getSharedPreferences("kosp", 0);
                            if (sharedPreferences.getString("referrer_source", null) == null) {
                                SharedPreferences.Editor putString = sharedPreferences.edit().putString("referrer_source", "STR::gplay");
                                putString.putString("referrer", "STR::" + stringExtra).apply();
                            } else {
                                Tracker.a(2, "RRC", "onReceive", "Skip: Previous referrer exists");
                            }
                            a.countDown();
                            return;
                        }
                    }
                    Tracker.a(2, "RRC", "onReceive", "Invalid Referrer");
                    return;
                }
            } catch (Throwable th) {
                Tracker.a(1, "RRC", "onReceive", "Unknown error when receiving install referrer", th);
            }
        }
        Tracker.a(2, "RRC", "onReceive", "Invalid Intent/Action");
    }
}
