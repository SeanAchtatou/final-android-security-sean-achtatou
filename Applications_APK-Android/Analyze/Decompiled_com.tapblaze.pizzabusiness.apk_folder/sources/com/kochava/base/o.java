package com.kochava.base;

import android.os.Handler;
import android.os.Looper;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.ironsource.sdk.constants.Constants;
import com.kochava.base.Tracker;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import org.json.JSONArray;
import org.json.JSONObject;

final class o extends j {
    private JSONObject b = null;

    o(i iVar) {
        super(iVar, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    static void a(i iVar, JSONObject jSONObject) {
        JSONArray g = x.g(jSONObject.opt("blacklist"));
        if (g != null) {
            if (x.a(g, "email")) {
                g.put("ids");
            }
            iVar.d.a("blacklist", g);
        } else {
            iVar.d.a("blacklist", new JSONArray());
        }
        JSONArray g2 = x.g(jSONObject.opt("whitelist"));
        if (g2 != null) {
            if (x.a(g2, "email")) {
                g2.put("ids");
            }
            iVar.d.a("whitelist", g2);
        } else {
            iVar.d.a("whitelist", new JSONArray());
        }
        JSONArray g3 = x.g(jSONObject.opt("eventname_blacklist"));
        if (g3 != null) {
            iVar.d.a("eventname_blacklist", g3);
        } else {
            iVar.d.a("eventname_blacklist", new JSONArray());
        }
        b(iVar, x.b(jSONObject.opt("flags"), true));
        iVar.g.a(x.f(jSONObject.opt("location_services")));
        c(iVar, x.b(jSONObject.opt("networking"), true));
        d(iVar, x.b(jSONObject.opt("deeplinks"), true));
        e(iVar, x.b(jSONObject.opt("internal_logging"), true));
        i(iVar, x.b(jSONObject.opt("install"), true));
        h(iVar, x.b(jSONObject.opt("push_notifications"), true));
        b(jSONObject);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.Integer, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, double):double
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, double):double */
    private static void b(i iVar, JSONObject jSONObject) {
        String a = x.a(jSONObject.opt("kochava_app_id"));
        if (a != null && !a.isEmpty()) {
            iVar.d.a("kochava_app_id_override", a);
        }
        String a2 = x.a(jSONObject.opt("kochava_device_id"));
        if (a2 != null && !a2.isEmpty()) {
            iVar.d.a("kochava_device_id", a2);
        }
        iVar.d.a("session_tracking", Boolean.valueOf(x.a(jSONObject.opt("session_tracking"), true) && ("NONE".equalsIgnoreCase(x.a(jSONObject.opt("session_tracking"))) ^ true)));
        iVar.d.a("send_updates", Boolean.valueOf(x.a(jSONObject.opt("send_updates"), true)));
        iVar.d.a("session_minimum", Integer.valueOf(x.a(x.b(jSONObject.opt("session_minimum"), 45), 0, Integer.MAX_VALUE)));
        iVar.d.a("session_window", Integer.valueOf(x.a(x.b(jSONObject.opt("session_window"), 600), 0, Integer.MAX_VALUE)));
        int a3 = x.a(x.b(jSONObject.opt("kvinit_wait"), 60), 0, Integer.MAX_VALUE);
        iVar.d.a("kvinit_wait", (Object) Integer.valueOf(a3), true);
        iVar.d.a("kvinit_staleness", (Object) Integer.valueOf(x.a(x.b(jSONObject.opt("kvinit_staleness"), 86400), a3, Integer.MAX_VALUE)), true);
        iVar.d.a("getattribution_wait", Double.valueOf(Math.max((double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, x.a(jSONObject.opt("getattribution_wait"), 3.0d))));
        g(iVar, jSONObject);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, int):int
     arg types: [java.lang.String, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, int):int */
    private static void b(JSONObject jSONObject) {
        JSONArray g = x.g(jSONObject.opt("log_messages"));
        if (g != null) {
            for (int i = 0; i < g.length(); i++) {
                JSONObject f = x.f(g.opt(i));
                if (f != null) {
                    String a = x.a(f.opt(ViewHierarchyConstants.TEXT_KEY));
                    int a2 = x.a((Object) x.a(f.opt("level")), 0);
                    if (!(a2 == 0 || a == null || a.isEmpty())) {
                        Tracker.a(a2, "TIN", "decodeLogMess", a);
                    }
                }
            }
        }
    }

    private static void c(i iVar, JSONObject jSONObject) {
        iVar.d.a("networking_tracking_wait", Integer.valueOf(Math.max(0, x.b(jSONObject.opt("tracking_wait"), 10))));
        double max = Math.max(-1.0d, x.a(jSONObject.opt("seconds_per_request"), (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE));
        iVar.d.a("networking_seconds_per_request", Double.valueOf(max));
        iVar.u.a(max);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private static void d(i iVar, JSONObject jSONObject) {
        iVar.d.a("deeplinks_allow_deferred", Boolean.valueOf(x.a(jSONObject.opt("allow_deferred"), true)));
        iVar.d.a("deeplinks_timeout_minimum", Long.valueOf(Math.max(0L, x.a(x.a(jSONObject.opt("timeout_minimum"), x.a(250L))))));
        iVar.d.a("deeplinks_timeout_maximum", Long.valueOf(Math.max(0L, x.a(x.a(jSONObject.opt("timeout_maximum"), x.a(30000L))))));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    private static void e(i iVar, JSONObject jSONObject) {
        iVar.d.a("internal_logging_enabled", Boolean.valueOf(x.a(jSONObject.opt("enabled"), true)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, long):long
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, org.json.JSONObject, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
     arg types: [java.lang.String, long, org.json.JSONObject]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.Integer, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    private static boolean f(i iVar, JSONObject jSONObject) {
        boolean z;
        int i;
        boolean z2;
        i iVar2 = iVar;
        int c = x.c();
        int b2 = x.b(iVar2.d.b("consent_last_prompt"), 0);
        JSONObject b3 = x.b(iVar2.d.b(Constants.RequestParameters.CONSENT), true);
        JSONArray c2 = x.c(b3.opt(Tracker.ConsentPartner.KEY_PARTNERS), true);
        boolean a = x.a(b3.opt("required"), true);
        boolean a2 = x.a(b3.opt(Tracker.ConsentPartner.KEY_GRANTED), false);
        long a3 = x.a(b3.opt(Tracker.ConsentPartner.KEY_RESPONSE_TIME), 0L);
        boolean a4 = x.a(b3.opt("should_prompt"), false);
        String str = "consent_last_prompt";
        int i2 = c;
        String a5 = x.a(b3.opt("prompt_id"), "");
        if (b3.length() != 0) {
            i = b2;
            z = true;
        } else {
            i = b2;
            z = false;
        }
        String str2 = a5;
        JSONObject b4 = x.b(jSONObject.opt(Constants.RequestParameters.CONSENT), true);
        String str3 = "";
        boolean a6 = x.a(b4.opt("required"), true);
        JSONArray c3 = x.c(b4.opt(Tracker.ConsentPartner.KEY_PARTNERS), true);
        String str4 = "prompt_id";
        if (b4.length() == 0) {
            Tracker.a(2, "TIN", "decodeConsent", "Consent information not returned by server. Ensure it is enabled on the Kochava dashboard.");
            return false;
        } else if (!a6) {
            x.a("required", (Object) null, b3);
            x.a("should_prompt", (Object) null, b3);
            iVar2.d.a(Constants.RequestParameters.CONSENT, (Object) b3, true);
            iVar2.d.c(true);
            return a;
        } else {
            x.a(Tracker.ConsentPartner.KEY_GRANTED, Boolean.valueOf(a2), b4);
            x.a(Tracker.ConsentPartner.KEY_RESPONSE_TIME, Long.valueOf(a3), b4);
            x.a("should_prompt", Boolean.valueOf(a4), b4);
            int b5 = x.b(c2, c3);
            boolean z3 = !x.a((Object) x.a(b4.opt(str4), str3), (Object) str2);
            boolean z4 = b5 == 2;
            if ((z3 || z4) && a2) {
                x.a(Tracker.ConsentPartner.KEY_GRANTED, (Object) null, b4);
                x.a(Tracker.ConsentPartner.KEY_RESPONSE_TIME, (Object) 0L, b4);
                iVar2.d.c(false);
                iVar2.d.a(true);
                iVar2.g.g(true);
            }
            boolean z5 = i != 0 && i2 - i > x.b(b4.opt("prompt_retry_interval"), 2592000);
            boolean a7 = x.a(b4.opt(Tracker.ConsentPartner.KEY_GRANTED), false);
            boolean z6 = !a7 && (!z || a2 || z5 || i == 0);
            x.a("should_prompt", Boolean.valueOf(z6 || (!a7 && a4)), b4);
            if (z6) {
                z2 = true;
                iVar2.d.a(str, (Object) Integer.valueOf(i2), true);
            } else {
                z2 = true;
            }
            iVar2.d.a(Constants.RequestParameters.CONSENT, b4, z2);
            return ((z4 || z3) && a2) || !a || z6;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, double):double
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, double):double */
    private static void g(i iVar, JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        JSONObject jSONObject3 = new JSONObject();
        x.a("install_referrer_attempts", Integer.valueOf(x.a(x.b(jSONObject.opt("install_referrer_attempts"), 2), 1, Integer.MAX_VALUE)), jSONObject3);
        x.a("install_referrer_wait", Integer.valueOf(x.a(x.b(jSONObject.opt("install_referrer_wait"), 10), 1, Integer.MAX_VALUE)), jSONObject3);
        x.a("install_referrer_retry_wait", Double.valueOf(x.a(x.a(jSONObject.opt("install_referrer_retry_wait"), 1.0d), (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, Double.MAX_VALUE)), jSONObject3);
        x.a("install_referrer", jSONObject3, jSONObject2);
        iVar.d.a("dp_options", jSONObject2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    private static void h(i iVar, JSONObject jSONObject) {
        iVar.d.a("push", Boolean.valueOf(x.a(jSONObject.opt("enabled"), false)));
        String a = x.a(jSONObject.opt("resend_id"), "");
        if (!a.isEmpty() && !x.a(iVar.d.b("push_notifications_resend_id"), "").equals(a)) {
            iVar.d.a("push_token_sent", (Object) false);
            iVar.d.a("push_notifications_resend_id", a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    private static void i(i iVar, JSONObject jSONObject) {
        String a = x.a(jSONObject.opt("resend_id"), "");
        if (!a.isEmpty() && !x.a(iVar.d.b("install_resend_id"), "").equals(a)) {
            iVar.d.a("initial_needs_sent", (Object) true);
            iVar.d.a("install_resend_id", a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
     arg types: [int, org.json.JSONObject]
     candidates:
      com.kochava.base.o.a(com.kochava.base.i, org.json.JSONObject):void
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
     arg types: [org.json.JSONObject, int]
     candidates:
      com.kochava.base.o.a(com.kochava.base.i, org.json.JSONObject):void
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.Integer, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    public final void run() {
        Tracker.a(4, "TIN", "run", new Object[0]);
        int b2 = x.b(this.a.d.b("init_last_sent"), 0);
        int b3 = x.b(this.a.d.b("kvinit_wait"), 60);
        Tracker.a(5, "TIN", "run", "LastSent: " + b2, "InitWait: " + b3);
        if (b2 + b3 >= x.c()) {
            Tracker.a(4, "TIN", "run", "Skip");
            d();
            i();
            return;
        }
        if (this.b == null) {
            Tracker.a(5, "TIN", "run", "Gather");
            this.b = new JSONObject();
            a(0, this.b, new JSONObject());
        }
        Tracker.a(5, "TIN", "run", SettingsJsonConstants.PROMPT_SEND_BUTTON_TITLE_DEFAULT);
        JSONObject a = a(0, (Object) this.b);
        if (a(a, true)) {
            Tracker.a(5, "TIN", "run", "Retry");
            return;
        }
        Tracker.a(5, "TIN", "run", a);
        if (!x.a((Object) x.a(this.b.opt("nt_id")), (Object) x.a(a.opt("nt_id")))) {
            Tracker.a(4, "TIN", "run", "nt_id mismatch");
        }
        a(this.a, a);
        boolean z = this.a.m && !this.a.n && f(this.a, a);
        this.a.d.a("init_last_sent", (Object) Integer.valueOf(x.c()), true);
        this.b = null;
        d();
        Tracker.a(3, "TIN", "init", "Complete");
        Tracker.a(4, "TIN", "run", "Complete");
        i();
        if (z) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    try {
                        if (o.this.a.e != null) {
                            o.this.a.e.onConsentStatusChange();
                        }
                    } catch (Throwable th) {
                        Tracker.a(2, "TIN", "run", "Exception in Host App", th);
                    }
                }
            });
        }
    }
}
