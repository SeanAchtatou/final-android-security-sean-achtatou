package com.kochava.base;

public interface ConsentStatusChangeListener {
    void onConsentStatusChange();
}
