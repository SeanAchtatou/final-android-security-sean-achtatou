package com.kochava.base;

import java.util.List;

final class w extends j {
    private static final Object b = new Object();
    private static final List<String> c = c.a(4);

    w(i iVar) {
        super(iVar, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x010c, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean a(com.kochava.base.j r16, boolean r17) {
        /*
            r0 = r16
            java.lang.Object r1 = com.kochava.base.w.b
            monitor-enter(r1)
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ all -> 0x010d }
            r2.<init>()     // Catch:{ all -> 0x010d }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ all -> 0x010d }
            r3.<init>()     // Catch:{ all -> 0x010d }
            r4 = 4
            if (r17 == 0) goto L_0x0015
            r0.a(r4, r2, r3)     // Catch:{ all -> 0x010d }
        L_0x0015:
            com.kochava.base.i r5 = r0.a     // Catch:{ all -> 0x010d }
            com.kochava.base.d r5 = r5.d     // Catch:{ all -> 0x010d }
            java.lang.String r6 = "send_updates"
            java.lang.Object r5 = r5.b(r6)     // Catch:{ all -> 0x010d }
            r6 = 1
            boolean r5 = com.kochava.base.x.a(r5, r6)     // Catch:{ all -> 0x010d }
            com.kochava.base.i r7 = r0.a     // Catch:{ all -> 0x010d }
            com.kochava.base.d r7 = r7.d     // Catch:{ all -> 0x010d }
            java.lang.String r8 = "initial_data"
            java.lang.Object r7 = r7.b(r8)     // Catch:{ all -> 0x010d }
            java.lang.String r7 = com.kochava.base.x.a(r7)     // Catch:{ all -> 0x010d }
            r8 = 0
            if (r7 == 0) goto L_0x0037
            r7 = 1
            goto L_0x0038
        L_0x0037:
            r7 = 0
        L_0x0038:
            com.kochava.base.i r9 = r0.a     // Catch:{ all -> 0x010d }
            com.kochava.base.d r9 = r9.d     // Catch:{ all -> 0x010d }
            java.lang.String r10 = "initial_needs_sent"
            java.lang.Object r9 = r9.b(r10)     // Catch:{ all -> 0x010d }
            boolean r9 = com.kochava.base.x.a(r9, r6)     // Catch:{ all -> 0x010d }
            r10 = 5
            java.lang.String r11 = "TUP"
            java.lang.String r12 = "performUpdate"
            r13 = 3
            java.lang.Object[] r13 = new java.lang.Object[r13]     // Catch:{ all -> 0x010d }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x010d }
            r14.<init>()     // Catch:{ all -> 0x010d }
            java.lang.String r15 = "sendUpdates: "
            r14.append(r15)     // Catch:{ all -> 0x010d }
            r14.append(r5)     // Catch:{ all -> 0x010d }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x010d }
            r13[r8] = r14     // Catch:{ all -> 0x010d }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x010d }
            r14.<init>()     // Catch:{ all -> 0x010d }
            java.lang.String r15 = "gathered: "
            r14.append(r15)     // Catch:{ all -> 0x010d }
            r14.append(r7)     // Catch:{ all -> 0x010d }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x010d }
            r13[r6] = r14     // Catch:{ all -> 0x010d }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x010d }
            r14.<init>()     // Catch:{ all -> 0x010d }
            java.lang.String r15 = "needsSent: "
            r14.append(r15)     // Catch:{ all -> 0x010d }
            r14.append(r9)     // Catch:{ all -> 0x010d }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x010d }
            r15 = 2
            r13[r15] = r14     // Catch:{ all -> 0x010d }
            com.kochava.base.Tracker.a(r10, r11, r12, r13)     // Catch:{ all -> 0x010d }
            if (r5 == 0) goto L_0x010b
            if (r7 != 0) goto L_0x0091
            if (r9 != 0) goto L_0x010b
        L_0x0091:
            if (r17 != 0) goto L_0x0096
            r0.a(r4, r2, r3)     // Catch:{ all -> 0x010d }
        L_0x0096:
            int r2 = r3.length()     // Catch:{ all -> 0x010d }
            if (r2 <= r15) goto L_0x010b
            java.lang.String r2 = "notifications_enabled"
            boolean r2 = r3.has(r2)     // Catch:{ all -> 0x010d }
            if (r2 != 0) goto L_0x00ac
            java.lang.String r2 = "background_location"
            boolean r2 = r3.has(r2)     // Catch:{ all -> 0x010d }
            if (r2 == 0) goto L_0x00b9
        L_0x00ac:
            com.kochava.base.i r2 = r0.a     // Catch:{ all -> 0x010d }
            com.kochava.base.d r2 = r2.d     // Catch:{ all -> 0x010d }
            java.lang.String r3 = "push_token_sent"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r8)     // Catch:{ all -> 0x010d }
            r2.a(r3, r5)     // Catch:{ all -> 0x010d }
        L_0x00b9:
            java.util.List<java.lang.String> r2 = com.kochava.base.w.c     // Catch:{ all -> 0x010d }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x010d }
        L_0x00bf:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x010d }
            if (r3 == 0) goto L_0x00e8
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x010d }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x010d }
            com.kochava.base.i r5 = r0.a     // Catch:{ all -> 0x010d }
            com.kochava.base.d r5 = r5.d     // Catch:{ all -> 0x010d }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x010d }
            r7.<init>()     // Catch:{ all -> 0x010d }
            r7.append(r3)     // Catch:{ all -> 0x010d }
            java.lang.String r3 = "_upd"
            r7.append(r3)     // Catch:{ all -> 0x010d }
            java.lang.String r3 = r7.toString()     // Catch:{ all -> 0x010d }
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x010d }
            r5.a(r3, r7)     // Catch:{ all -> 0x010d }
            goto L_0x00bf
        L_0x00e8:
            com.kochava.base.i r2 = r0.a     // Catch:{ all -> 0x010d }
            com.kochava.base.d r2 = r2.d     // Catch:{ all -> 0x010d }
            java.lang.String r3 = "app_limit_trackingupd"
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x010d }
            r2.a(r3, r5)     // Catch:{ all -> 0x010d }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ all -> 0x010d }
            r2.<init>()     // Catch:{ all -> 0x010d }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ all -> 0x010d }
            r3.<init>()     // Catch:{ all -> 0x010d }
            r0.a(r4, r2, r3)     // Catch:{ all -> 0x010d }
            com.kochava.base.i r0 = r0.a     // Catch:{ all -> 0x010d }
            com.kochava.base.d r0 = r0.d     // Catch:{ all -> 0x010d }
            r0.b(r2)     // Catch:{ all -> 0x010d }
            monitor-exit(r1)     // Catch:{ all -> 0x010d }
            return r6
        L_0x010b:
            monitor-exit(r1)     // Catch:{ all -> 0x010d }
            return r8
        L_0x010d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x010d }
            goto L_0x0111
        L_0x0110:
            throw r0
        L_0x0111:
            goto L_0x0110
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.w.a(com.kochava.base.j, boolean):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.w.a(com.kochava.base.j, boolean):boolean
     arg types: [com.kochava.base.w, int]
     candidates:
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.w.a(com.kochava.base.j, boolean):boolean */
    public final void run() {
        Tracker.a(4, "TUP", "run", new Object[0]);
        a((j) this, true);
        d();
        i();
        Tracker.a(4, "TUP", "run", "Complete");
    }
}
