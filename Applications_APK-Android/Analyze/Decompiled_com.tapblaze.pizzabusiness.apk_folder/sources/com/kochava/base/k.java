package com.kochava.base;

final class k extends j {
    private final boolean b;

    k(i iVar, boolean z) {
        super(iVar, true);
        this.b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.w.a(com.kochava.base.j, boolean):boolean
     arg types: [com.kochava.base.k, int]
     candidates:
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.w.a(com.kochava.base.j, boolean):boolean */
    public final void run() {
        Tracker.a(4, "TAL", "run", new Object[0]);
        Boolean b2 = x.b(this.a.d.b("app_limit_tracking"));
        boolean a = x.a(Boolean.valueOf(this.b), b2);
        Tracker.a(5, "TAL", "run", "cachedAppLimitAdTracking: " + b2, "isEqual: " + a);
        if (b2 == null || !a) {
            this.a.d.a("app_limit_tracking", Boolean.valueOf(this.b));
            this.a.d.a("app_limit_trackingupd", (Object) true);
            if (w.a((j) this, false) && this.a.g.l()) {
                i();
            }
            Tracker.a(4, "TAL", "run", "Complete");
            return;
        }
        Tracker.a(4, "TAL", "run", "Skip");
    }
}
