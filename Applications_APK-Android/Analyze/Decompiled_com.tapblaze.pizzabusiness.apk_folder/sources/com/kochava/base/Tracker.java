package com.kochava.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.applovin.sdk.AppLovinEventParameters;
import com.chartboost.sdk.CBLocation;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kochava.base.c;
import java.util.Date;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class Tracker {
    public static final int EVENT_TYPE_ACHIEVEMENT = 1;
    public static final int EVENT_TYPE_ADD_TO_CART = 2;
    public static final int EVENT_TYPE_ADD_TO_WISH_LIST = 3;
    public static final int EVENT_TYPE_AD_CLICK = 17;
    public static final int EVENT_TYPE_AD_VIEW = 12;
    public static final int EVENT_TYPE_CHECKOUT_START = 4;
    public static final int EVENT_TYPE_CONSENT_GRANTED = 15;
    public static final int EVENT_TYPE_DEEP_LINK = 16;
    public static final int EVENT_TYPE_LEVEL_COMPLETE = 5;
    public static final int EVENT_TYPE_PURCHASE = 6;
    public static final int EVENT_TYPE_PUSH_OPENED = 14;
    public static final int EVENT_TYPE_PUSH_RECEIVED = 13;
    public static final int EVENT_TYPE_RATING = 7;
    public static final int EVENT_TYPE_REGISTRATION_COMPLETE = 8;
    public static final int EVENT_TYPE_SEARCH = 9;
    public static final int EVENT_TYPE_START_TRIAL = 18;
    public static final int EVENT_TYPE_SUBSCRIBE = 19;
    public static final int EVENT_TYPE_TUTORIAL_COMPLETE = 10;
    public static final int EVENT_TYPE_VIEW = 11;
    public static final int LOG_LEVEL_DEBUG = 4;
    public static final int LOG_LEVEL_ERROR = 1;
    public static final int LOG_LEVEL_INFO = 3;
    public static final int LOG_LEVEL_NONE = 0;
    public static final int LOG_LEVEL_TRACE = 5;
    public static final int LOG_LEVEL_WARN = 2;
    private static final Object a = new Object();
    private static final Tracker b = new Tracker();
    private String c = null;
    private String d = null;
    private transient int e = 5;
    private transient LogListener f = null;
    private transient int g = 0;
    private a h = null;

    public static class Configuration {
        final String a = "!SDK-VERSION-STRING!:com.kochava:tracker:release:3.7.0";
        /* access modifiers changed from: private */
        public final Context b;
        /* access modifiers changed from: private */
        public String c = null;
        /* access modifiers changed from: private */
        public String d = null;
        /* access modifiers changed from: private */
        public Integer e = null;
        /* access modifiers changed from: private */
        public AttributionUpdateListener f = null;
        /* access modifiers changed from: private */
        public ConsentStatusChangeListener g = null;
        /* access modifiers changed from: private */
        public boolean h = false;
        /* access modifiers changed from: private */
        public boolean i = false;
        /* access modifiers changed from: private */
        public Boolean j = null;
        /* access modifiers changed from: private */
        public Boolean k = null;
        /* access modifiers changed from: private */
        public IdentityLink l = null;
        /* access modifiers changed from: private */
        public JSONObject m = null;

        public Configuration(Context context) {
            this.b = context;
        }

        public final Configuration addCustom(String str, String str2) {
            if (this.m == null) {
                this.m = new JSONObject();
            }
            x.a(str, str2, this.m);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
         arg types: [org.json.JSONObject, org.json.JSONObject, int]
         candidates:
          com.kochava.base.x.a(double, double, double):double
          com.kochava.base.x.a(int, int, int):int
          com.kochava.base.x.a(long, long, long):long
          com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
          com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
          com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void */
        public final Configuration addCustom(JSONObject jSONObject) {
            if (jSONObject != null && jSONObject.length() > 0) {
                if (this.m == null) {
                    this.m = new JSONObject();
                }
                x.a(this.m, jSONObject, false);
            }
            return this;
        }

        public final Configuration setAppGuid(String str) {
            this.c = str;
            return this;
        }

        public final Configuration setAppLimitAdTracking(boolean z) {
            this.j = Boolean.valueOf(z);
            return this;
        }

        public final Configuration setAttributionUpdateListener(AttributionUpdateListener attributionUpdateListener) {
            this.f = attributionUpdateListener;
            return this;
        }

        public final Configuration setConsentStatusChangeListener(ConsentStatusChangeListener consentStatusChangeListener) {
            this.g = consentStatusChangeListener;
            return this;
        }

        public final Configuration setIdentityLink(IdentityLink identityLink) {
            this.l = identityLink;
            return this;
        }

        public final Configuration setIntelligentConsentManagement(boolean z) {
            this.h = z;
            return this;
        }

        public final Configuration setLogLevel(int i2) {
            this.e = Integer.valueOf(i2);
            return this;
        }

        public final Configuration setManualManagedConsentRequirements(boolean z) {
            this.i = z;
            return this;
        }

        public final Configuration setPartnerName(String str) {
            this.d = str;
            return this;
        }

        public final Configuration setSleep(boolean z) {
            this.k = Boolean.valueOf(z);
            return this;
        }
    }

    public static class ConsentPartner implements Parcelable {
        public static final Parcelable.Creator<ConsentPartner> CREATOR = new Parcelable.Creator<ConsentPartner>() {
            /* renamed from: a */
            public ConsentPartner createFromParcel(Parcel parcel) {
                return new ConsentPartner(parcel);
            }

            /* renamed from: a */
            public ConsentPartner[] newArray(int i) {
                return new ConsentPartner[i];
            }
        };
        public static final String KEY_DESCRIPTION = "description";
        public static final String KEY_GRANTED = "granted";
        public static final String KEY_NAME = "name";
        public static final String KEY_PARTNERS = "partners";
        public static final String KEY_RESPONSE_TIME = "response_time";
        public final String description;
        public final boolean granted;
        public final String name;
        public final long responseTime;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.kochava.base.x.a(java.lang.Object, double):double
          com.kochava.base.x.a(java.lang.Object, int):int
          com.kochava.base.x.a(java.lang.Object, long):long
          com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
          com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
          com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
          com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
          com.kochava.base.x.a(org.json.JSONObject, boolean):void
          com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
          com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
          com.kochava.base.x.a(java.lang.Object, boolean):boolean
          com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
          com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
          com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
          com.kochava.base.x.a(int[], int):boolean
          com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String */
        protected ConsentPartner(Parcel parcel) {
            this.name = x.a((Object) parcel.readString(), "");
            this.description = x.a((Object) parcel.readString(), "");
            this.granted = parcel.readByte() != 0;
            this.responseTime = parcel.readLong();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(java.lang.Object, long):long
         arg types: [java.lang.Object, int]
         candidates:
          com.kochava.base.x.a(java.lang.Object, double):double
          com.kochava.base.x.a(java.lang.Object, int):int
          com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
          com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
          com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
          com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
          com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
          com.kochava.base.x.a(org.json.JSONObject, boolean):void
          com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
          com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
          com.kochava.base.x.a(java.lang.Object, boolean):boolean
          com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
          com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
          com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
          com.kochava.base.x.a(int[], int):boolean
          com.kochava.base.x.a(java.lang.Object, long):long */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
         arg types: [java.lang.Object, int]
         candidates:
          com.kochava.base.x.a(java.lang.Object, double):double
          com.kochava.base.x.a(java.lang.Object, int):int
          com.kochava.base.x.a(java.lang.Object, long):long
          com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
          com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
          com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
          com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
          com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
          com.kochava.base.x.a(org.json.JSONObject, boolean):void
          com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
          com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
          com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
          com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
          com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
          com.kochava.base.x.a(int[], int):boolean
          com.kochava.base.x.a(java.lang.Object, boolean):boolean */
        ConsentPartner(JSONObject jSONObject) {
            this.name = x.a(jSONObject.opt("name"), "");
            this.description = x.a(jSONObject.opt("description"), "");
            this.responseTime = x.a(jSONObject.opt(KEY_RESPONSE_TIME), 0L);
            this.granted = x.a(jSONObject.opt(KEY_GRANTED), false);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.name);
            parcel.writeString(this.description);
            parcel.writeByte(this.granted ? (byte) 1 : 0);
            parcel.writeLong(this.responseTime);
        }
    }

    public static class Event implements Parcelable {
        public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
            /* renamed from: a */
            public final Event createFromParcel(Parcel parcel) {
                return new Event(parcel);
            }

            /* renamed from: a */
            public final Event[] newArray(int i) {
                return new Event[i];
            }
        };
        final JSONObject a;
        final String b;
        String c;
        String d;

        public Event(int i) {
            String str;
            this.a = new JSONObject();
            this.c = null;
            this.d = null;
            switch (i) {
                case 1:
                    str = "Achievement";
                    break;
                case 2:
                    str = "Add to Cart";
                    break;
                case 3:
                    str = "Add to Wish List";
                    break;
                case 4:
                    str = "Checkout Start";
                    break;
                case 5:
                    str = CBLocation.LOCATION_LEVEL_COMPLETE;
                    break;
                case 6:
                    str = "Purchase";
                    break;
                case 7:
                    str = "Rating";
                    break;
                case 8:
                    str = "Registration Complete";
                    break;
                case 9:
                    str = "Search";
                    break;
                case 10:
                    str = "Tutorial Complete";
                    break;
                case 11:
                    str = "View";
                    break;
                case 12:
                    str = "Ad View";
                    break;
                case 13:
                    str = "Push Received";
                    break;
                case 14:
                    str = "Push Opened";
                    break;
                case 15:
                    str = "Consent Granted";
                    break;
                case 16:
                    str = "_Deeplink";
                    break;
                case 17:
                    str = "Ad Click";
                    break;
                case 18:
                    str = "Start Trial";
                    break;
                case 19:
                    str = AppEventsConstants.EVENT_NAME_SUBSCRIBE;
                    break;
                default:
                    str = "";
                    break;
            }
            this.b = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
         arg types: [org.json.JSONObject, org.json.JSONObject, int]
         candidates:
          com.kochava.base.x.a(double, double, double):double
          com.kochava.base.x.a(int, int, int):int
          com.kochava.base.x.a(long, long, long):long
          com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
          com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
          com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.kochava.base.x.a(java.lang.Object, double):double
          com.kochava.base.x.a(java.lang.Object, int):int
          com.kochava.base.x.a(java.lang.Object, long):long
          com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
          com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
          com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
          com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
          com.kochava.base.x.a(org.json.JSONObject, boolean):void
          com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
          com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
          com.kochava.base.x.a(java.lang.Object, boolean):boolean
          com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
          com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
          com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
          com.kochava.base.x.a(int[], int):boolean
          com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String */
        protected Event(Parcel parcel) {
            this.a = new JSONObject();
            JSONObject jSONObject = null;
            this.c = null;
            this.d = null;
            try {
                jSONObject = new JSONObject(parcel.readString());
            } catch (JSONException e) {
                Tracker.a(2, "EVT", "Event", e);
            }
            if (jSONObject != null) {
                x.a(this.a, jSONObject, false);
            }
            this.b = x.a((Object) parcel.readString(), "");
            this.c = parcel.readString();
            this.d = parcel.readString();
        }

        public Event(String str) {
            this.a = new JSONObject();
            this.c = null;
            this.d = null;
            this.b = str == null ? "" : str;
        }

        public final Event addCustom(String str, double d2) {
            x.a(str, Double.valueOf(d2), this.a);
            return this;
        }

        public final Event addCustom(String str, long j) {
            x.a(str, Long.valueOf(j), this.a);
            return this;
        }

        public final Event addCustom(String str, String str2) {
            x.a(str, str2, this.a);
            return this;
        }

        public final Event addCustom(String str, Date date) {
            x.a(str, date, this.a);
            return this;
        }

        public final Event addCustom(String str, boolean z) {
            x.a(str, Boolean.valueOf(z), this.a);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
         arg types: [org.json.JSONObject, org.json.JSONObject, int]
         candidates:
          com.kochava.base.x.a(double, double, double):double
          com.kochava.base.x.a(int, int, int):int
          com.kochava.base.x.a(long, long, long):long
          com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
          com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
          com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void */
        public final Event addCustom(JSONObject jSONObject) {
            if (jSONObject == null || jSONObject.length() < 1) {
                Tracker.a(2, "EVT", "addCustom", "Invalid keyValue object");
                return this;
            }
            x.a(this.a, jSONObject, false);
            return this;
        }

        public final int describeContents() {
            return 0;
        }

        public final Event setAction(String str) {
            x.a("action", str, this.a);
            return this;
        }

        public final Event setAdCampaignId(String str) {
            x.a("ad_campaign_id", str, this.a);
            return this;
        }

        public final Event setAdCampaignName(String str) {
            x.a("ad_campaign_name", str, this.a);
            return this;
        }

        public final Event setAdDeviceType(String str) {
            x.a("device_type", str, this.a);
            return this;
        }

        public final Event setAdGroupId(String str) {
            x.a("ad_group_id", str, this.a);
            return this;
        }

        public final Event setAdGroupName(String str) {
            x.a("ad_group_name", str, this.a);
            return this;
        }

        public final Event setAdMediationName(String str) {
            x.a("ad_mediation_name", str, this.a);
            return this;
        }

        public final Event setAdNetworkName(String str) {
            x.a("ad_network_name", str, this.a);
            return this;
        }

        public final Event setAdPlacement(String str) {
            x.a("placement", str, this.a);
            return this;
        }

        public final Event setAdSize(String str) {
            x.a("ad_size", str, this.a);
            return this;
        }

        public final Event setAdType(String str) {
            x.a("ad_type", str, this.a);
            return this;
        }

        public final Event setBackground(boolean z) {
            x.a("background", Boolean.valueOf(z), this.a);
            return this;
        }

        public final Event setCheckoutAsGuest(String str) {
            x.a("checkout_as_guest", str, this.a);
            return this;
        }

        public final Event setCompleted(boolean z) {
            x.a("completed", Boolean.valueOf(z), this.a);
            return this;
        }

        public final Event setContentId(String str) {
            x.a(AppLovinEventParameters.CONTENT_IDENTIFIER, str, this.a);
            return this;
        }

        public final Event setContentType(String str) {
            x.a(FirebaseAnalytics.Param.CONTENT_TYPE, str, this.a);
            return this;
        }

        public final Event setCurrency(String str) {
            x.a("currency", str, this.a);
            return this;
        }

        public final Event setDate(String str) {
            x.a("date", str, this.a);
            return this;
        }

        public final Event setDate(Date date) {
            x.a("date", date, this.a);
            return this;
        }

        public final Event setDescription(String str) {
            x.a("description", str, this.a);
            return this;
        }

        public final Event setDestination(String str) {
            x.a("destination", str, this.a);
            return this;
        }

        public final Event setDuration(double d2) {
            x.a("duration", Double.valueOf(d2), this.a);
            return this;
        }

        public final Event setEndDate(String str) {
            x.a("end_date", str, this.a);
            return this;
        }

        public final Event setEndDate(Date date) {
            x.a("end_date", date, this.a);
            return this;
        }

        public final Event setGooglePlayReceipt(String str, String str2) {
            if (str == null || str2 == null || str.trim().isEmpty() || str2.trim().isEmpty()) {
                Tracker.a(2, "EVT", "setGooglePlay", "Invalid Input");
            } else {
                this.c = str;
                this.d = str2;
            }
            return this;
        }

        public final Event setItemAddedFrom(String str) {
            x.a("item_added_from", str, this.a);
            return this;
        }

        public final Event setLevel(String str) {
            x.a("level", str, this.a);
            return this;
        }

        public final Event setMaxRatingValue(double d2) {
            x.a("max_rating_value", Double.valueOf(d2), this.a);
            return this;
        }

        public final Event setName(String str) {
            x.a("name", str, this.a);
            return this;
        }

        public final Event setOrderId(String str) {
            x.a("order_id", str, this.a);
            return this;
        }

        public final Event setOrigin(String str) {
            x.a("origin", str, this.a);
            return this;
        }

        public final Event setPayload(Bundle bundle) {
            x.a(MessengerShareContentUtility.ATTACHMENT_PAYLOAD, x.f(bundle), this.a);
            return this;
        }

        public final Event setPayload(JSONObject jSONObject) {
            x.a(MessengerShareContentUtility.ATTACHMENT_PAYLOAD, jSONObject, this.a);
            return this;
        }

        public final Event setPrice(double d2) {
            x.a("price", Double.valueOf(d2), this.a);
            return this;
        }

        public final Event setQuantity(double d2) {
            x.a(FirebaseAnalytics.Param.QUANTITY, Double.valueOf(d2), this.a);
            return this;
        }

        public final Event setRatingValue(double d2) {
            x.a("rating_value", Double.valueOf(d2), this.a);
            return this;
        }

        public final Event setReceiptId(String str) {
            x.a("receipt_id", str, this.a);
            return this;
        }

        public final Event setReferralFrom(String str) {
            x.a("referral_from", str, this.a);
            return this;
        }

        public final Event setRegistrationMethod(String str) {
            x.a("registration_method", str, this.a);
            return this;
        }

        public final Event setResults(String str) {
            x.a("results", str, this.a);
            return this;
        }

        public final Event setScore(String str) {
            x.a(FirebaseAnalytics.Param.SCORE, str, this.a);
            return this;
        }

        public final Event setSearchTerm(String str) {
            x.a(FirebaseAnalytics.Param.SEARCH_TERM, str, this.a);
            return this;
        }

        public final Event setSource(String str) {
            x.a("source", str, this.a);
            return this;
        }

        public final Event setSpatialX(double d2) {
            x.a("spatial_x", Double.valueOf(d2), this.a);
            return this;
        }

        public final Event setSpatialY(double d2) {
            x.a("spatial_y", Double.valueOf(d2), this.a);
            return this;
        }

        public final Event setSpatialZ(double d2) {
            x.a("spatial_z", Double.valueOf(d2), this.a);
            return this;
        }

        public final Event setStartDate(String str) {
            x.a("start_date", str, this.a);
            return this;
        }

        public final Event setStartDate(Date date) {
            x.a("start_date", date, this.a);
            return this;
        }

        public final Event setSuccess(String str) {
            x.a("success", str, this.a);
            return this;
        }

        public final Event setUri(Uri uri) {
            x.a(ShareConstants.MEDIA_URI, uri, this.a);
            return this;
        }

        public final Event setUri(String str) {
            x.a(ShareConstants.MEDIA_URI, str, this.a);
            return this;
        }

        public final Event setUserId(String str) {
            x.a("user_id", str, this.a);
            return this;
        }

        public final Event setUserName(String str) {
            x.a("user_name", str, this.a);
            return this;
        }

        public final Event setValidated(String str) {
            x.a("validated", str, this.a);
            return this;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(x.a(this.a));
            parcel.writeString(this.b);
            parcel.writeString(this.c);
            parcel.writeString(this.d);
        }
    }

    public static class IdentityLink {
        final JSONObject a = new JSONObject();

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject, boolean):void
         arg types: [java.lang.String, java.lang.String, org.json.JSONObject, int]
         candidates:
          com.kochava.base.x.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.net.HttpURLConnection
          com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject, boolean):void */
        public final IdentityLink add(String str, String str2) {
            x.a(str, (Object) str2, this.a, false);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
         arg types: [org.json.JSONObject, org.json.JSONObject, int]
         candidates:
          com.kochava.base.x.a(double, double, double):double
          com.kochava.base.x.a(int, int, int):int
          com.kochava.base.x.a(long, long, long):long
          com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
          com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
          com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void */
        public final IdentityLink add(Map<String, String> map) {
            JSONObject f = x.f(map);
            if (f == null || f.length() < 1) {
                Tracker.a(2, "IDL", "add", "Invalid Input");
            } else {
                x.a(this.a, f, false);
            }
            return this;
        }
    }

    private Tracker() {
    }

    static String a() {
        String str;
        synchronized (b) {
            if (b.c != null) {
                str = "AndroidTracker 3.7.0 (" + b.c + ")";
            } else {
                str = "AndroidTracker 3.7.0";
            }
        }
        return str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    /* JADX WARNING: Can't wrap try/catch for region: R(12:3|4|(2:8|(2:10|(1:12)))|13|(3:15|(4:18|(1:95)(5:20|21|22|(4:24|(1:26)|(1:29)|(1:31))(2:32|(1:34)(2:35|(1:37)(2:38|(1:40)(1:41))))|(2:46|(2:48|92)(1:94))(1:93))|49|16)|91)|50|(1:52)|53|(2:59|60)|61|62|(3:66|(3:68|(1:98)(1:(1:(1:(2:73|(2:75|(2:77|99)(2:78|100))(2:79|101))(2:80|102))(2:81|103))(2:82|97))|83)|96)) */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x014d, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x014f, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x0110 */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0125 A[Catch:{ all -> 0x00c0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(int r9, java.lang.String r10, java.lang.String r11, java.lang.Object... r12) {
        /*
            java.lang.Object r0 = com.kochava.base.Tracker.a
            monitor-enter(r0)
            if (r9 == 0) goto L_0x014e
            com.kochava.base.Tracker r1 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0150 }
            int r1 = r1.e     // Catch:{ all -> 0x0150 }
            if (r1 == 0) goto L_0x0011
            com.kochava.base.Tracker r1 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0150 }
            int r1 = r1.e     // Catch:{ all -> 0x0150 }
            if (r1 >= r9) goto L_0x001f
        L_0x0011:
            com.kochava.base.Tracker r1 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0150 }
            int r1 = r1.g     // Catch:{ all -> 0x0150 }
            if (r1 == 0) goto L_0x014e
            com.kochava.base.Tracker r1 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0150 }
            int r1 = r1.g     // Catch:{ all -> 0x0150 }
            if (r1 >= r9) goto L_0x001f
            goto L_0x014e
        L_0x001f:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0150 }
            r1.<init>()     // Catch:{ all -> 0x0150 }
            java.lang.String r2 = "KO/"
            r1.append(r2)     // Catch:{ all -> 0x0150 }
            java.lang.String r2 = "TR/"
            r1.append(r2)     // Catch:{ all -> 0x0150 }
            int r2 = r10.length()     // Catch:{ all -> 0x0150 }
            r3 = 3
            int r2 = java.lang.Math.min(r2, r3)     // Catch:{ all -> 0x0150 }
            r4 = 0
            r1.append(r10, r4, r2)     // Catch:{ all -> 0x0150 }
            java.lang.String r10 = "/"
            r1.append(r10)     // Catch:{ all -> 0x0150 }
            int r10 = r11.length()     // Catch:{ all -> 0x0150 }
            r2 = 13
            int r10 = java.lang.Math.min(r10, r2)     // Catch:{ all -> 0x0150 }
            r1.append(r11, r4, r10)     // Catch:{ all -> 0x0150 }
            java.lang.String r10 = r1.toString()     // Catch:{ all -> 0x0150 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0150 }
            r11.<init>()     // Catch:{ all -> 0x0150 }
            r1 = 1
            r2 = 2
            if (r12 == 0) goto L_0x00e8
            r5 = 0
        L_0x005b:
            int r6 = r12.length     // Catch:{ all -> 0x0150 }
            if (r5 >= r6) goto L_0x00e8
            r6 = r12[r5]     // Catch:{ all -> 0x0150 }
            if (r6 != 0) goto L_0x0064
            goto L_0x00e4
        L_0x0064:
            r6 = 0
            r7 = r12[r5]     // Catch:{ all -> 0x00c0 }
            boolean r7 = r7 instanceof java.lang.String     // Catch:{ all -> 0x00c0 }
            if (r7 == 0) goto L_0x008c
            r7 = r12[r5]     // Catch:{ all -> 0x00c0 }
            org.json.JSONObject r7 = com.kochava.base.x.f(r7)     // Catch:{ all -> 0x00c0 }
            r8 = r12[r5]     // Catch:{ all -> 0x00c0 }
            org.json.JSONArray r8 = com.kochava.base.x.g(r8)     // Catch:{ all -> 0x00c0 }
            if (r7 == 0) goto L_0x007d
            java.lang.String r6 = r7.toString(r2)     // Catch:{ all -> 0x00c0 }
        L_0x007d:
            if (r6 != 0) goto L_0x0085
            if (r8 == 0) goto L_0x0085
            java.lang.String r6 = r8.toString(r2)     // Catch:{ all -> 0x00c0 }
        L_0x0085:
            if (r6 != 0) goto L_0x00d6
            r6 = r12[r5]     // Catch:{ all -> 0x00c0 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x00c0 }
            goto L_0x00d6
        L_0x008c:
            r6 = r12[r5]     // Catch:{ all -> 0x00c0 }
            boolean r6 = r6 instanceof org.json.JSONObject     // Catch:{ all -> 0x00c0 }
            if (r6 == 0) goto L_0x009b
            r6 = r12[r5]     // Catch:{ all -> 0x00c0 }
            org.json.JSONObject r6 = (org.json.JSONObject) r6     // Catch:{ all -> 0x00c0 }
            java.lang.String r6 = r6.toString(r2)     // Catch:{ all -> 0x00c0 }
            goto L_0x00d6
        L_0x009b:
            r6 = r12[r5]     // Catch:{ all -> 0x00c0 }
            boolean r6 = r6 instanceof org.json.JSONArray     // Catch:{ all -> 0x00c0 }
            if (r6 == 0) goto L_0x00aa
            r6 = r12[r5]     // Catch:{ all -> 0x00c0 }
            org.json.JSONArray r6 = (org.json.JSONArray) r6     // Catch:{ all -> 0x00c0 }
            java.lang.String r6 = r6.toString(r2)     // Catch:{ all -> 0x00c0 }
            goto L_0x00d6
        L_0x00aa:
            r6 = r12[r5]     // Catch:{ all -> 0x00c0 }
            boolean r6 = r6 instanceof java.lang.Throwable     // Catch:{ all -> 0x00c0 }
            if (r6 == 0) goto L_0x00b9
            r6 = r12[r5]     // Catch:{ all -> 0x00c0 }
            java.lang.Throwable r6 = (java.lang.Throwable) r6     // Catch:{ all -> 0x00c0 }
            java.lang.String r6 = android.util.Log.getStackTraceString(r6)     // Catch:{ all -> 0x00c0 }
            goto L_0x00d6
        L_0x00b9:
            r6 = r12[r5]     // Catch:{ all -> 0x00c0 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x00c0 }
            goto L_0x00d6
        L_0x00c0:
            r6 = move-exception
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0150 }
            r7.<init>()     // Catch:{ all -> 0x0150 }
            java.lang.String r8 = "Failed to build message.\n"
            r7.append(r8)     // Catch:{ all -> 0x0150 }
            java.lang.String r6 = android.util.Log.getStackTraceString(r6)     // Catch:{ all -> 0x0150 }
            r7.append(r6)     // Catch:{ all -> 0x0150 }
            java.lang.String r6 = r7.toString()     // Catch:{ all -> 0x0150 }
        L_0x00d6:
            if (r6 == 0) goto L_0x00e4
            r11.append(r6)     // Catch:{ all -> 0x0150 }
            int r6 = r12.length     // Catch:{ all -> 0x0150 }
            int r6 = r6 - r1
            if (r5 >= r6) goto L_0x00e4
            java.lang.String r6 = "\n"
            r11.append(r6)     // Catch:{ all -> 0x0150 }
        L_0x00e4:
            int r5 = r5 + 1
            goto L_0x005b
        L_0x00e8:
            int r12 = r11.length()     // Catch:{ all -> 0x0150 }
            if (r12 != 0) goto L_0x00f3
            java.lang.String r12 = " "
            r11.append(r12)     // Catch:{ all -> 0x0150 }
        L_0x00f3:
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x0150 }
            com.kochava.base.Tracker r12 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0150 }
            int r12 = r12.g     // Catch:{ all -> 0x0150 }
            if (r12 <= 0) goto L_0x0110
            com.kochava.base.Tracker r12 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0150 }
            int r12 = r12.g     // Catch:{ all -> 0x0150 }
            if (r9 > r12) goto L_0x0110
            com.kochava.base.Tracker r12 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0150 }
            com.kochava.base.LogListener r12 = r12.f     // Catch:{ all -> 0x0150 }
            if (r12 == 0) goto L_0x0110
            com.kochava.base.Tracker r12 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0110 }
            com.kochava.base.LogListener r12 = r12.f     // Catch:{ all -> 0x0110 }
            r12.onLog(r9, r10, r11)     // Catch:{ all -> 0x0110 }
        L_0x0110:
            com.kochava.base.Tracker r12 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0150 }
            int r12 = r12.e     // Catch:{ all -> 0x0150 }
            if (r12 <= 0) goto L_0x014c
            com.kochava.base.Tracker r12 = com.kochava.base.Tracker.b     // Catch:{ all -> 0x0150 }
            int r12 = r12.e     // Catch:{ all -> 0x0150 }
            if (r9 > r12) goto L_0x014c
            java.lang.String r12 = "\n"
            java.lang.String[] r11 = r11.split(r12)     // Catch:{ all -> 0x0150 }
            int r12 = r11.length     // Catch:{ all -> 0x0150 }
        L_0x0123:
            if (r4 >= r12) goto L_0x014c
            r5 = r11[r4]     // Catch:{ all -> 0x0150 }
            if (r9 == 0) goto L_0x0149
            if (r9 == r1) goto L_0x0146
            if (r9 == r2) goto L_0x0142
            if (r9 == r3) goto L_0x013e
            r6 = 4
            if (r9 == r6) goto L_0x013a
            r6 = 5
            if (r9 == r6) goto L_0x0136
            goto L_0x0149
        L_0x0136:
            android.util.Log.v(r10, r5)     // Catch:{ all -> 0x0150 }
            goto L_0x0149
        L_0x013a:
            android.util.Log.d(r10, r5)     // Catch:{ all -> 0x0150 }
            goto L_0x0149
        L_0x013e:
            android.util.Log.i(r10, r5)     // Catch:{ all -> 0x0150 }
            goto L_0x0149
        L_0x0142:
            android.util.Log.w(r10, r5)     // Catch:{ all -> 0x0150 }
            goto L_0x0149
        L_0x0146:
            android.util.Log.e(r10, r5)     // Catch:{ all -> 0x0150 }
        L_0x0149:
            int r4 = r4 + 1
            goto L_0x0123
        L_0x014c:
            monitor-exit(r0)     // Catch:{ all -> 0x0150 }
            return
        L_0x014e:
            monitor-exit(r0)     // Catch:{ all -> 0x0150 }
            return
        L_0x0150:
            r9 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0150 }
            goto L_0x0154
        L_0x0153:
            throw r9
        L_0x0154:
            goto L_0x0153
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.Tracker.a(int, java.lang.String, java.lang.String, java.lang.Object[]):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.kochava.base.a.a(com.kochava.base.AttributionUpdateListener, boolean):void
      com.kochava.base.b.a(com.kochava.base.AttributionUpdateListener, boolean):void
      com.kochava.base.a.a(java.lang.String, boolean):void */
    public static void addPushToken(String str) {
        synchronized (b) {
            a(3, "TRA", "addPushToken", "addPushToken");
            if (b.h == null || str == null || str.isEmpty()) {
                a(2, "TRA", "addPushToken", "Invalid Configuration or Parameter");
            } else {
                b.h.a(str, true);
            }
        }
    }

    public static void clearConsentShouldPrompt() {
        synchronized (b) {
            a(3, "TRA", "clearConsentS", "clearConsentShouldPrompt");
            if (b.h != null) {
                b.h.i();
            } else {
                a(2, "TRA", "clearConsentS", "Invalid Configuration or Parameter");
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, int):int
     arg types: [java.lang.Integer, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.a.a(com.kochava.base.AttributionUpdateListener, boolean):void
     arg types: [com.kochava.base.AttributionUpdateListener, int]
     candidates:
      com.kochava.base.a.a(java.lang.String, boolean):void
      com.kochava.base.a.a(com.kochava.base.AttributionUpdateListener, boolean):void */
    public static void configure(Configuration configuration) {
        String str;
        synchronized (b) {
            if (Build.VERSION.SDK_INT < 14) {
                a(1, "TRA", "configure", "Below API 14 is unsupported. Cannot Configure.");
                return;
            }
            try {
                if (b.h != null) {
                    a(2, "TRA", "configure", "Already Configured");
                } else if (configuration == null) {
                    a(1, "TRA", "configure", "Null Configuration");
                } else {
                    synchronized (a) {
                        b.e = x.a((Object) configuration.e, 3);
                        if (b.e > 3) {
                            a(2, "TRA", "configure", "Log Level set higher than recommended for publishing");
                        }
                    }
                    if (configuration.b != null) {
                        if (configuration.b.getApplicationContext() != null) {
                            Context applicationContext = configuration.b.getApplicationContext();
                            SharedPreferences sharedPreferences = applicationContext.getSharedPreferences("koov", 0);
                            sharedPreferences.edit().apply();
                            int a2 = x.a((Object) Integer.valueOf(sharedPreferences.getInt("log_level", -1)), b.e);
                            synchronized (a) {
                                if (a2 != b.e) {
                                    b.e = a2;
                                    a(4, "TRA", "configure", "Override LogLevel " + a2);
                                }
                            }
                            JSONObject jSONObject = new JSONObject();
                            x.a("url_init", sharedPreferences.getString("url_init", null), jSONObject);
                            x.a("url_push_token_add", sharedPreferences.getString("url_push_token_add", null), jSONObject);
                            x.a("url_push_token_remove", sharedPreferences.getString("url_push_token_remove", null), jSONObject);
                            x.a("url_get_attribution", sharedPreferences.getString("url_get_attribution", null), jSONObject);
                            x.a("url_initial", sharedPreferences.getString("url_initial", null), jSONObject);
                            x.a("url_update", sharedPreferences.getString("url_update", null), jSONObject);
                            x.a("url_identity_link", sharedPreferences.getString("url_identity_link", null), jSONObject);
                            x.a("url_event", sharedPreferences.getString("url_event", null), jSONObject);
                            x.a("url_location_update", sharedPreferences.getString("url_location_update", null), jSONObject);
                            x.a("url_geo_event", sharedPreferences.getString("url_geo_event", null), jSONObject);
                            x.a("url_internal_error", sharedPreferences.getString("url_internal_error", null), jSONObject);
                            x.a("url_smartlinks", sharedPreferences.getString("url_smartlinks", null), jSONObject);
                            if (jSONObject.length() != 0) {
                                a(4, "TRA", "configure", "Override URLs", jSONObject);
                            }
                            if (configuration.c == null && configuration.d == null) {
                                a(1, "TRA", "configure", "Either App Guid or Partner Name required");
                                return;
                            }
                            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
                            if (stackTrace.length >= 4) {
                                StackTraceElement stackTraceElement = stackTrace[3];
                                str = stackTraceElement.getClassName() + "/" + stackTraceElement.getMethodName();
                            } else {
                                str = "";
                            }
                            b.h = new a(applicationContext, a(), b.d, configuration.c, configuration.d, configuration.g, jSONObject, configuration.m, configuration.h, configuration.i, str);
                            if (configuration.k != null) {
                                b.h.b(configuration.k.booleanValue());
                            }
                            if (configuration.j != null) {
                                b.h.c(configuration.j.booleanValue());
                            }
                            if (configuration.l != null) {
                                b.h.a(configuration.l);
                            }
                            if (configuration.f != null) {
                                b.h.a(configuration.f, false);
                            }
                            a(3, "TRA", "configure", "Complete: " + getVersion());
                            StringBuilder sb = new StringBuilder();
                            sb.append("SDK ID: ");
                            configuration.getClass();
                            sb.append("!SDK-VERSION-STRING!:com.kochava:tracker:release:3.7.0");
                            a(5, "TRA", "configure", sb.toString());
                        }
                    }
                    a(1, "TRA", "configure", "Null Context");
                }
            } catch (Throwable th) {
                a(1, "TRA", "configure", AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, th);
                b.h = null;
            }
        }
    }

    public static void ext(String str, String str2) {
        synchronized (b) {
            if (str != null) {
                if (b.c == null && b.d == null && b.h == null) {
                    b.c = str;
                    b.d = str2;
                }
            }
        }
    }

    public static String getAttribution() {
        synchronized (b) {
            if (b.h != null) {
                String a2 = b.h.a("attribution");
                return a2;
            }
            a(2, "TRA", "getAttributio", "Invalid Configuration or Parameter");
            return "";
        }
    }

    public static String getConsentDescription() {
        synchronized (b) {
            if (b.h != null) {
                String c2 = b.h.c();
                return c2;
            }
            a(2, "TRA", "getConsentDes", "Invalid Configuration or Parameter");
            return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    public static ConsentPartner[] getConsentPartners() {
        synchronized (b) {
            if (b.h != null) {
                JSONArray j = b.h.j();
                ConsentPartner[] consentPartnerArr = new ConsentPartner[j.length()];
                for (int i = 0; i < consentPartnerArr.length; i++) {
                    consentPartnerArr[i] = new ConsentPartner(x.b(j.opt(i), true));
                }
                return consentPartnerArr;
            }
            a(2, "TRA", "getConsentPar", "Invalid Configuration or Parameter");
            ConsentPartner[] consentPartnerArr2 = new ConsentPartner[0];
            return consentPartnerArr2;
        }
    }

    public static String getConsentPartnersJson() {
        synchronized (b) {
            if (b.h != null) {
                String a2 = x.a(b.h.j());
                return a2;
            }
            a(2, "TRA", "getConsentPar", "Invalid Configuration or Parameter");
            return "[]";
        }
    }

    public static long getConsentResponseTime() {
        synchronized (b) {
            if (b.h != null) {
                long f2 = b.h.f();
                return f2;
            }
            a(2, "TRA", "getConsentRes", "Invalid Configuration or Parameter");
            return 0;
        }
    }

    public static String getDeviceId() {
        synchronized (b) {
            if (b.h != null) {
                String a2 = b.h.a("kochava_device_id");
                return a2;
            }
            a(2, "TRA", "getDeviceId", "Invalid Configuration or Parameter");
            return "";
        }
    }

    public static InstallReferrer getInstallReferrer() {
        synchronized (b) {
            if (b.h != null) {
                InstallReferrer a2 = c.a.a(b.h.b("install_referrer"), false, b.h.a("referrer"));
                return a2;
            }
            a(2, "TRA", "getInstallRef", "Invalid Configuration or Parameter");
            InstallReferrer a3 = c.a.a(new JSONObject(), false, null);
            return a3;
        }
    }

    public static String getVersion() {
        synchronized (b) {
            if (b.h != null) {
                String a2 = a();
                return a2;
            }
            a(2, "TRA", "getVersion", "Invalid Configuration or Parameter");
            return "";
        }
    }

    public static boolean isConfigured() {
        boolean z;
        synchronized (b) {
            z = b.h != null;
        }
        return z;
    }

    public static boolean isConsentGranted() {
        synchronized (b) {
            if (b.h != null) {
                boolean e2 = b.h.e();
                return e2;
            }
            a(2, "TRA", "isConsentGran", "Invalid Configuration or Parameter");
            return false;
        }
    }

    public static boolean isConsentGrantedOrNotRequired() {
        return !isConsentRequired() || isConsentGranted();
    }

    public static boolean isConsentRequired() {
        synchronized (b) {
            if (b.h != null) {
                boolean d2 = b.h.d();
                return d2;
            }
            a(2, "TRA", "isConsentRequ", "Invalid Configuration or Parameter");
            return true;
        }
    }

    public static boolean isConsentRequirementsKnown() {
        synchronized (b) {
            if (b.h != null) {
                boolean b2 = b.h.b();
                return b2;
            }
            a(2, "TRA", "isConsentRequ", "Invalid Configuration or Parameter");
            return false;
        }
    }

    public static boolean isConsentShouldPrompt() {
        synchronized (b) {
            if (b.h != null) {
                boolean h2 = b.h.h();
                return h2;
            }
            a(2, "TRA", "isConsentShou", "Invalid Configuration or Parameter");
            return false;
        }
    }

    public static boolean isSessionActive() {
        synchronized (b) {
            if (b.h != null) {
                boolean m = b.h.m();
                return m;
            }
            a(2, "TRA", "isSessionActi", "Invalid Configuration or Parameter");
            return false;
        }
    }

    public static boolean isSleep() {
        synchronized (b) {
            if (b.h != null) {
                boolean a2 = b.h.a();
                return a2;
            }
            a(2, "TRA", "isSleep", "Invalid Configuration or Parameter");
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public static void processDeeplink(String str, double d2, DeeplinkProcessedListener deeplinkProcessedListener) {
        synchronized (b) {
            a(3, "TRA", "processDeepli", "processDeeplink");
            if (b.h == null || deeplinkProcessedListener == null) {
                a(2, "TRA", "processDeepli", "Invalid Configuration or Parameter");
            } else {
                if (str == null) {
                    str = "";
                }
                b.h.a(str, Math.max(0L, Math.round(d2 * 1000.0d)), deeplinkProcessedListener);
            }
        }
    }

    public static void processDeeplink(String str, DeeplinkProcessedListener deeplinkProcessedListener) {
        processDeeplink(str, 10.0d, deeplinkProcessedListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.kochava.base.a.a(com.kochava.base.AttributionUpdateListener, boolean):void
      com.kochava.base.b.a(com.kochava.base.AttributionUpdateListener, boolean):void
      com.kochava.base.a.a(java.lang.String, boolean):void */
    public static void removePushToken(String str) {
        synchronized (b) {
            a(3, "TRA", "removePushTok", "removePushToken");
            if (b.h == null || str == null || str.isEmpty()) {
                a(2, "TRA", "removePushTok", "Invalid Configuration or Parameter");
            } else {
                b.h.a(str, false);
            }
        }
    }

    public static void sendEvent(Event event) {
        synchronized (b) {
            a(3, "TRA", "sendEvent", "sendEvent(Event)");
            if (b.h == null || event == null || event.b.trim().isEmpty()) {
                a(2, "TRA", "sendEvent", "Invalid Configuration or Parameter");
            } else {
                b.h.a(event.b, x.a(event.a), event.c, event.d);
            }
        }
    }

    public static void sendEvent(String str, String str2) {
        synchronized (b) {
            a(3, "TRA", "sendEvent", "sendEvent(String,String)");
            if (b.h == null || str == null || str.trim().isEmpty()) {
                a(2, "TRA", "sendEvent", "Invalid Configuration or Parameter");
            } else {
                b.h.a(str, str2, null, null);
            }
        }
    }

    @Deprecated
    public static void sendEventDeepLink(String str) {
        synchronized (b) {
            a(3, "TRA", "sendEventDeep", "sendEventDeepLink");
            if (b.h == null || str == null || str.trim().isEmpty()) {
                a(2, "TRA", "sendEventDeep", "Invalid Configuration or Parameter");
            } else {
                Event uri = new Event(16).setUri(str);
                b.h.a(uri.b, x.a(uri.a), uri.c, uri.d);
            }
        }
    }

    public static void setAppLimitAdTracking(boolean z) {
        synchronized (b) {
            a(3, "TRA", "setAppLimitAd", "setAppLimitAdTracking");
            if (b.h != null) {
                b.h.c(z);
            } else {
                a(2, "TRA", "setAppLimitAd", "Invalid Configuration or Parameter");
            }
        }
    }

    public static void setConsentGranted(boolean z) {
        synchronized (b) {
            a(3, "TRA", "setConsentGra", "setConsentGranted");
            if (b.h != null) {
                b.h.e(z);
            } else {
                a(2, "TRA", "setConsentGra", "Invalid Configuration or Parameter");
            }
        }
    }

    public static void setConsentRequired(boolean z) {
        synchronized (b) {
            if (b.h != null) {
                b.h.d(z);
            } else {
                a(2, "TRA", "setConsentReq", "Invalid Configuration or Parameter");
            }
        }
    }

    public static void setDeepLinkListener(Uri uri, int i, DeepLinkListener deepLinkListener) {
        synchronized (b) {
            a(3, "TRA", "setDeepLinkLi", "setDeepLinkListener");
            if (b.h == null || deepLinkListener == null) {
                a(2, "TRA", "setDeepLinkLi", "Invalid Configuration or Parameter");
            } else {
                b.h.a(uri, i, deepLinkListener);
            }
        }
    }

    public static void setDeepLinkListener(Uri uri, DeepLinkListener deepLinkListener) {
        setDeepLinkListener(uri, 4000, deepLinkListener);
    }

    public static void setIdentityLink(IdentityLink identityLink) {
        synchronized (b) {
            a(3, "TRA", "setIdentityLi", "setIdentityLink");
            if (b.h == null || identityLink == null || identityLink.a.length() <= 0) {
                a(2, "TRA", "setIdentityLi", "Invalid Configuration or Parameter");
            } else {
                b.h.a(identityLink);
            }
        }
    }

    static void setLogListener(LogListener logListener, int i) {
        synchronized (b) {
            synchronized (a) {
                b.f = logListener;
                if (i > 0 && i <= 5) {
                    b.g = i;
                }
            }
        }
    }

    public static void setSleep(boolean z) {
        synchronized (b) {
            a(3, "TRA", "setSleep", "setSleep");
            if (b.h != null) {
                b.h.b(z);
            } else {
                a(2, "TRA", "setSleep", "Invalid Configuration or Parameter");
            }
        }
    }

    public static void unConfigure(boolean z) {
        synchronized (b) {
            try {
                a(2, "TRA", "unConfigure", "UnConfigure Tracker");
                b.c = null;
                b.d = null;
                if (b.h != null) {
                    b.h.a(z);
                }
                b.h = null;
            } catch (Throwable th) {
                a(1, "TRA", "unConfigure", th);
            }
        }
        synchronized (a) {
            b.e = 3;
            b.f = null;
            b.g = 0;
        }
    }
}
