package com.kochava.base;

import org.json.JSONObject;

final class n extends j {
    private final JSONObject b;

    n(i iVar, JSONObject jSONObject) {
        super(iVar, true);
        this.b = jSONObject;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, org.json.JSONObject, int]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    public final void run() {
        Tracker.a(4, "TIL", "run", new Object[0]);
        JSONObject b2 = x.b(this.a.d.b("identity_link_all"), true);
        JSONObject b3 = x.b(this.a.d.b("identity_link"), true);
        boolean z = b3.length() != 0;
        x.a(b3, this.b, false);
        if (z || !x.a(b2, b3)) {
            x.a(b2, b3, false);
            if (b2.length() > 250) {
                b2 = new JSONObject();
                Tracker.a(4, "TIL", "run", "Max Size Exceeded. Resetting Saved List.");
            }
            this.a.d.a("identity_link_all", b2);
            this.a.d.a("identity_link", b3);
            boolean z2 = x.a(this.a.d.b("initial_data")) != null;
            boolean a = x.a(this.a.d.b("initial_needs_sent"), true);
            if (z2 || !a) {
                JSONObject jSONObject = new JSONObject();
                a(7, jSONObject, new JSONObject());
                this.a.d.b(jSONObject);
                if (this.a.g.l()) {
                    i();
                }
            }
            d();
            Tracker.a(4, "TIL", "run", "Complete");
            return;
        }
        this.a.d.a("identity_link");
        Tracker.a(4, "TIL", "run", "Skip");
    }
}
