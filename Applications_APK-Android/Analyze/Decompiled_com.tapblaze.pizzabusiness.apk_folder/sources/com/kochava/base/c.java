package com.kochava.base;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.UiModeManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.Display;
import android.view.WindowManager;
import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.facebook.internal.NativeProtocol;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.instantapps.InstantApps;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.ironsource.sdk.constants.Constants;
import com.kochava.base.network.DataPointsNetwork;
import cz.msebera.android.httpclient.message.TokenParser;
import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

final class c {
    static final c[] a = {new c("screen_brightness", 15, new int[]{1, 2, 3, 6, 10, 11}, null), new c("device_orientation", 15, new int[]{1, 2, 3, 6, 10, 11}, null), new c("volume", 10, new int[]{1, 2, 3, 6, 10, 11}, null), new c("carrier_name", 60, new int[]{1, 2, 3, 6, 10, 11}, null), new c("adid", -1, new int[]{1, 4}, null), new c("fire_adid", -1, new int[]{1, 4}, null), new c("oaid", -1, new int[]{1, 4}, null), new c("platform", -1, new int[]{0}, null), new c(Constants.ParametersKeys.ORIENTATION_DEVICE, -1, new int[]{1, 2, 3, 6, 10, 11}, null), new c("disp_h", 60, new int[]{1, 2, 3, 6, 10, 11}, null), new c("disp_w", 60, new int[]{1, 2, 3, 6, 10, 11}, null), new c("package", -1, new int[]{0, 1}, null), new c("installed_date", -1, new int[]{1}, null), new c(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, -1, new int[]{1, 2, 3, 6, 10, 11}, null), new c("app_version", -1, new int[]{1, 2, 3, 4, 6, 10, 11}, null), new c("app_short_string", -1, new int[]{1, 2, 3, 4, 6, 10, 11}, null), new c("android_id", 60, new int[]{1, 4}, null), new c("os_version", -1, new int[]{1, 2, 3, 4, 6, 10, 11}, null), new c("device_limit_tracking", -1, new int[]{1, 4}, null), new c("fb_attribution_id", -1, new int[]{1}, null), new c("ids", -1, null, new int[]{1, 4}), new c("is_genuine", -1, new int[]{1, 4}, null), new c("language", 60, new int[]{1, 4}, null), new c("screen_dpi", 60, new int[]{1, 2, 3, 6, 10, 11}, null), new c("screen_inches", 60, new int[]{1}, null), new c("manufacturer", -1, new int[]{1, 2, 3, 6, 10, 11}, null), new c("product_name", -1, new int[]{1, 2, 3, 6, 10, 11}, null), new c("architecture", -1, new int[]{1, 2, 3, 6, 10, 11}, null), new c("battery_status", 60, new int[]{1, 2, 3, 6, 10, 11}, null), new c("battery_level", 60, new int[]{1, 2, 3, 6, 10, 11}, null), new c("device_cores", -1, new int[]{1}, null), new c("signal_bars", 30, new int[]{1, 2, 3, 6, 10, 11}, null), new c("installer_package", -1, new int[]{1}, null), new c("instant_app", -1, new int[]{1, 2, 3, 6, 10, 11}, null), new c("locale", 60, new int[]{0, 1, 2, 3, 6, 10, 11, 8, 9}, null), new c("timezone", 60, new int[]{0, 1, 2, 3, 6, 10, 11, 8, 9}, null), new c("bluetooth_name", 30, new int[]{1, 2, 3, 6, 10, 11}, null), new c("ui_mode", 30, new int[]{1, 2, 3, 6, 10, 11}, null), new c("notifications_enabled", 1, new int[]{1, 2, 3, 6, 10, 11, 4, 8, 9}, null), new c("background_location", 1, new int[]{1, 2, 3, 6, 10, 11, 4, 8, 9}, null), new c("bms", -1, new int[]{1, 2, 3, 6, 10, 11}, null), new c("install_referrer", -1, new int[]{1}, null), new c("network_conn_type", 30, new int[]{1, 2, 3, 6, 10, 11}, null), new c("ssid", 60, new int[]{1, 2, 3, 6, 10, 11}, null), new c("bssid", 60, new int[]{1, 2, 3, 6, 10, 11}, null), new c("network_metered", 30, new int[]{1, 2, 3, 6, 10, 11}, null)};
    final String b;
    private final int c;
    private final int[] d;
    private final int[] e;

    static final class a implements Runnable {
        final CountDownLatch a = new CountDownLatch(1);
        final JSONObject b = new JSONObject();
        private final Context c;

        a(Context context) {
            this.c = context;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        static int a(String str) {
            char c2;
            switch (str.hashCode()) {
                case -693070394:
                    if (str.equals("service_unavailable")) {
                        c2 = 2;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -319671111:
                    if (str.equals("feature_not_supported")) {
                        c2 = 3;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -236196924:
                    if (str.equals("missing_dependency")) {
                        c2 = 6;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -63726253:
                    if (str.equals("developer_error")) {
                        c2 = 4;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 3548:
                    if (str.equals("ok")) {
                        c2 = 1;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 40661574:
                    if (str.equals("timed_out")) {
                        c2 = 5;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 920378310:
                    if (str.equals("not_gathered")) {
                        c2 = 7;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 2057740165:
                    if (str.equals("service_disconnected")) {
                        c2 = 0;
                        break;
                    }
                    c2 = 65535;
                    break;
                default:
                    c2 = 65535;
                    break;
            }
            switch (c2) {
                case 0:
                    return -1;
                case 1:
                    return 0;
                case 2:
                    return 1;
                case 3:
                    return 2;
                case 4:
                    return 3;
                case 5:
                    return 4;
                case 6:
                    return 5;
                case 7:
                default:
                    return 6;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.a(java.lang.Object, double):double
         arg types: [java.lang.Object, int]
         candidates:
          com.kochava.base.x.a(java.lang.Object, int):int
          com.kochava.base.x.a(java.lang.Object, long):long
          com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
          com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
          com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
          com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
          com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
          com.kochava.base.x.a(org.json.JSONObject, boolean):void
          com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
          com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
          com.kochava.base.x.a(java.lang.Object, boolean):boolean
          com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
          com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
          com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
          com.kochava.base.x.a(int[], int):boolean
          com.kochava.base.x.a(java.lang.Object, double):double */
        static InstallReferrer a(JSONObject jSONObject, boolean z, String str) {
            boolean z2;
            int i;
            long j;
            String str2;
            String a2 = x.a(jSONObject.opt("status"));
            int i2 = z ? 4 : 6;
            if (a2 != null) {
                i2 = a(a2);
            }
            int b2 = x.b(jSONObject.opt(InstallReferrer.KEY_ATTEMPT_COUNT), -1);
            double a3 = x.a(jSONObject.opt("duration"), -1.0d);
            long j2 = -1;
            if (i2 == 0) {
                str = x.a(jSONObject.opt("referrer"), "");
                i = i2;
                j = (long) x.b(jSONObject.opt("referrer_click_time"), -1);
                z2 = false;
                j2 = (long) x.b(jSONObject.opt("install_begin_time"), -1);
            } else if (str == null || str.isEmpty()) {
                i = i2;
                str2 = "";
                z2 = false;
                j = -1;
                return new InstallReferrer(str2, j2, j, i, z2, b2, a3);
            } else {
                j2 = 0;
                j = 0;
                i = 0;
                z2 = true;
            }
            str2 = str;
            return new InstallReferrer(str2, j2, j, i, z2, b2, a3);
        }

        static String a(int i) {
            switch (i) {
                case -1:
                    return "service_disconnected";
                case 0:
                    return "ok";
                case 1:
                    return "service_unavailable";
                case 2:
                    return "feature_not_supported";
                case 3:
                    return "developer_error";
                case 4:
                    return "timed_out";
                case 5:
                    return "missing_dependency";
                case 6:
                default:
                    return "not_gathered";
            }
        }

        static JSONObject a(InstallReferrer installReferrer) {
            JSONObject jSONObject = new JSONObject();
            x.a("status", a(installReferrer.status), jSONObject);
            x.a(InstallReferrer.KEY_ATTEMPT_COUNT, Integer.valueOf(installReferrer.attemptCount), jSONObject);
            x.a("duration", Double.valueOf(installReferrer.duration), jSONObject);
            if (installReferrer.isValid()) {
                x.a("referrer", installReferrer.referrer, jSONObject);
                x.a("install_begin_time", Long.valueOf(installReferrer.installBeginTime), jSONObject);
                x.a("referrer_click_time", Long.valueOf(installReferrer.referrerClickTime), jSONObject);
            }
            return jSONObject;
        }

        /* access modifiers changed from: private */
        public void a() {
            if (x.a(this.b.opt("status")) == null) {
                x.a("status", a(5), this.b);
            }
        }

        /* access modifiers changed from: package-private */
        public final void b(int i) {
            try {
                this.a.await((long) i, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Tracker.a(5, "IRH", "waitOnLock", e);
            }
        }

        public final void run() {
            try {
                final InstallReferrerClient build = InstallReferrerClient.newBuilder(this.c).build();
                build.startConnection(new InstallReferrerStateListener() {
                    public final void onInstallReferrerServiceDisconnected() {
                        try {
                            Tracker.a(5, "IRH", "onInstallRefe", "Disconnected");
                            x.a("status", a.a(-1), a.this.b);
                            build.endConnection();
                        } catch (Throwable th) {
                            Tracker.a(4, "IRH", "onInstallRefe", th);
                            a.this.a();
                        }
                        a.this.a.countDown();
                    }

                    public final void onInstallReferrerSetupFinished(int i) {
                        Object a2;
                        JSONObject jSONObject;
                        try {
                            Tracker.a(5, "IRH", "onInstallRefe", "Setup Finished", "Response Code: " + i);
                            String str = "status";
                            if (i == 0) {
                                ReferrerDetails installReferrer = build.getInstallReferrer();
                                if (installReferrer != null) {
                                    x.a(str, a.a(0), a.this.b);
                                    x.a("referrer", installReferrer.getInstallReferrer(), a.this.b);
                                    x.a("install_begin_time", Long.valueOf(installReferrer.getInstallBeginTimestampSeconds()), a.this.b);
                                    str = "referrer_click_time";
                                    a2 = Long.valueOf(installReferrer.getReferrerClickTimestampSeconds());
                                    jSONObject = a.this.b;
                                }
                                build.endConnection();
                                a.this.a.countDown();
                            } else if (i == 1) {
                                a2 = a.a(1);
                                jSONObject = a.this.b;
                            } else if (i == 2) {
                                a2 = a.a(2);
                                jSONObject = a.this.b;
                            } else {
                                if (i == 3) {
                                    a2 = a.a(3);
                                    jSONObject = a.this.b;
                                }
                                build.endConnection();
                                a.this.a.countDown();
                            }
                            x.a(str, a2, jSONObject);
                            build.endConnection();
                        } catch (Throwable th) {
                            Tracker.a(4, "IRH", "onInstallRefe", th);
                            a.this.a();
                        }
                        a.this.a.countDown();
                    }
                });
            } catch (Throwable th) {
                Tracker.a(5, "IRH", "run", th);
                a();
                this.a.countDown();
            }
        }
    }

    private c(String str, int i, int[] iArr, int[] iArr2) {
        Tracker.a(5, "DPT", "Data", str + "," + i);
        this.b = str;
        this.c = i;
        this.d = iArr;
        this.e = iArr2;
    }

    private static String A(Context context) {
        return context.getPackageManager().getInstallerPackageName(context.getPackageName());
    }

    private static boolean B(Context context) {
        return InstantApps.isInstantApp(context);
    }

    private static String C(Context context) {
        BluetoothAdapter defaultAdapter;
        if (x.b(context, "android.permission.BLUETOOTH") && (defaultAdapter = BluetoothAdapter.getDefaultAdapter()) != null && defaultAdapter.isEnabled()) {
            return defaultAdapter.getName();
        }
        return null;
    }

    private static String D(Context context) {
        UiModeManager uiModeManager = (UiModeManager) context.getSystemService("uimode");
        if (uiModeManager == null) {
            return null;
        }
        switch (uiModeManager.getCurrentModeType()) {
            case 0:
                return "Undefined";
            case 1:
                return "Normal";
            case 2:
                return "Desk";
            case 3:
                return "Car";
            case 4:
                return "Television";
            case 5:
                return "Appliance";
            case 6:
                return "Watch";
            case 7:
                return "VR_Headset";
            default:
                return null;
        }
    }

    private static boolean E(Context context) {
        NotificationManager notificationManager;
        boolean z = true;
        if (Build.VERSION.SDK_INT < 24 || (notificationManager = (NotificationManager) context.getSystemService("notification")) == null) {
            if (Build.VERSION.SDK_INT >= 19) {
                try {
                    Object invoke = Class.forName("androidx.core.app.NotificationManagerCompat").getMethod("from", Context.class).invoke(null, context);
                    return ((Boolean) invoke.getClass().getMethod("areNotificationsEnabled", new Class[0]).invoke(invoke, new Object[0])).booleanValue();
                } catch (Throwable unused) {
                }
            }
            return true;
        }
        if (Build.VERSION.SDK_INT >= 26) {
            List<NotificationChannel> notificationChannels = notificationManager.getNotificationChannels();
            for (NotificationChannel importance : notificationChannels) {
                if (importance.getImportance() != 0) {
                    z = false;
                }
            }
            if (z && !notificationChannels.isEmpty()) {
                return false;
            }
        }
        return notificationManager.areNotificationsEnabled();
    }

    private static boolean F(Context context) {
        if (x.a(context, "android.permission.ACCESS_COARSE_LOCATION") || x.a(context, "android.permission.ACCESS_FINE_LOCATION")) {
            return Build.VERSION.SDK_INT < 29 || x.a(context, "android.permission.ACCESS_BACKGROUND_LOCATION");
        }
        return false;
    }

    private static double a(Context context) {
        double d2 = (double) Settings.System.getInt(context.getContentResolver(), "screen_brightness");
        Double.isNaN(d2);
        double round = (double) Math.round((d2 / 255.0d) * 10000.0d);
        Double.isNaN(round);
        return x.a(round / 10000.0d, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, 1.0d);
    }

    static InstallReferrer a(Context context, int i, int i2, double d2) {
        int i3;
        InstallReferrer installReferrer;
        int i4;
        long b2 = x.b();
        Handler handler = new Handler(Looper.getMainLooper());
        InstallReferrer installReferrer2 = null;
        int i5 = 0;
        int i6 = 0;
        while (true) {
            if (i6 >= i) {
                i3 = i5;
                installReferrer = installReferrer2;
                break;
            }
            i4 = i5 + 1;
            a aVar = new a(context);
            handler.post(aVar);
            aVar.b(i2);
            x.a(InstallReferrer.KEY_ATTEMPT_COUNT, Integer.valueOf(i4), aVar.b);
            double b3 = (double) (x.b() - b2);
            Double.isNaN(b3);
            x.a("duration", Double.valueOf(b3 / 1000.0d), aVar.b);
            installReferrer = a.a(aVar.b, true, null);
            if (installReferrer.isValid() || !installReferrer.isSupported()) {
                i3 = i4;
            } else {
                try {
                    Thread.sleep(Math.round(1000.0d * d2));
                } catch (InterruptedException e2) {
                    Tracker.a(4, "DPT", "getInstallRef", e2);
                }
                i6++;
                i5 = i4;
                installReferrer2 = installReferrer;
            }
        }
        i3 = i4;
        if (installReferrer != null) {
            return installReferrer;
        }
        double b4 = (double) (x.b() - b2);
        Double.isNaN(b4);
        return new InstallReferrer("", -1, -1, 4, false, i3, b4 / 1000.0d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    private Object a(Context context, d dVar, Object obj, Object obj2, boolean z, List<String> list, JSONObject jSONObject) {
        Object b2 = obj != null ? obj : dVar.b(this.b);
        if (obj == null || z || obj2 != null) {
            if (obj2 == null) {
                try {
                    obj2 = a(this, context, jSONObject, b2);
                } catch (Throwable th) {
                    Tracker.a(4, "DPT", "getValueNew", th);
                }
            }
            if (obj2 != null) {
                if (!list.contains(this.b)) {
                    list.add(this.b);
                }
                if (obj == null || !x.a(obj2, obj)) {
                    dVar.a(this.b, obj2);
                    dVar.a(this.b + "_ts", Integer.valueOf(x.c()));
                    if (!x.a(obj2, b2)) {
                        dVar.a(this.b + "_upd", (Object) true);
                    }
                }
            }
        }
        return obj2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    static Object a(c cVar, Context context, JSONObject jSONObject, Object obj) {
        char c2;
        String str = cVar.b;
        switch (str.hashCode()) {
            case -2086471997:
                if (str.equals("instant_app")) {
                    c2 = TokenParser.SP;
                    break;
                }
                c2 = 65535;
                break;
            case -2076227591:
                if (str.equals("timezone")) {
                    c2 = '#';
                    break;
                }
                c2 = 65535;
                break;
            case -1969347631:
                if (str.equals("manufacturer")) {
                    c2 = 24;
                    break;
                }
                c2 = 65535;
                break;
            case -1958212269:
                if (str.equals("installed_date")) {
                    c2 = 14;
                    break;
                }
                c2 = 65535;
                break;
            case -1613589672:
                if (str.equals("language")) {
                    c2 = TokenParser.DQUOTE;
                    break;
                }
                c2 = 65535;
                break;
            case -1335157162:
                if (str.equals(Constants.ParametersKeys.ORIENTATION_DEVICE)) {
                    c2 = 10;
                    break;
                }
                c2 = 65535;
                break;
            case -1331545845:
                if (str.equals("disp_h")) {
                    c2 = 11;
                    break;
                }
                c2 = 65535;
                break;
            case -1331545830:
                if (str.equals("disp_w")) {
                    c2 = 12;
                    break;
                }
                c2 = 65535;
                break;
            case -1211390364:
                if (str.equals("battery_status")) {
                    c2 = 27;
                    break;
                }
                c2 = 65535;
                break;
            case -1144512572:
                if (str.equals("device_limit_tracking")) {
                    c2 = 8;
                    break;
                }
                c2 = 65535;
                break;
            case -1097462182:
                if (str.equals("locale")) {
                    c2 = '!';
                    break;
                }
                c2 = 65535;
                break;
            case -901870406:
                if (str.equals("app_version")) {
                    c2 = 16;
                    break;
                }
                c2 = 65535;
                break;
            case -877252910:
                if (str.equals("battery_level")) {
                    c2 = 28;
                    break;
                }
                c2 = 65535;
                break;
            case -810883302:
                if (str.equals("volume")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case -807062458:
                if (str.equals("package")) {
                    c2 = TokenParser.CR;
                    break;
                }
                c2 = 65535;
                break;
            case -600298101:
                if (str.equals("device_cores")) {
                    c2 = 29;
                    break;
                }
                c2 = 65535;
                break;
            case -439099282:
                if (str.equals("ui_mode")) {
                    c2 = '%';
                    break;
                }
                c2 = 65535;
                break;
            case -417046774:
                if (str.equals("screen_dpi")) {
                    c2 = 22;
                    break;
                }
                c2 = 65535;
                break;
            case -345765233:
                if (str.equals("installer_package")) {
                    c2 = 31;
                    break;
                }
                c2 = 65535;
                break;
            case -286797593:
                if (str.equals("fire_adid")) {
                    c2 = 6;
                    break;
                }
                c2 = 65535;
                break;
            case 97672:
                if (str.equals("bms")) {
                    c2 = '(';
                    break;
                }
                c2 = 65535;
                break;
            case 104120:
                if (str.equals("ids")) {
                    c2 = 20;
                    break;
                }
                c2 = 65535;
                break;
            case 2989182:
                if (str.equals("adid")) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            case 3403373:
                if (str.equals("oaid")) {
                    c2 = 7;
                    break;
                }
                c2 = 65535;
                break;
            case 224914812:
                if (str.equals("bluetooth_name")) {
                    c2 = '$';
                    break;
                }
                c2 = 65535;
                break;
            case 672545271:
                if (str.equals("signal_bars")) {
                    c2 = 30;
                    break;
                }
                c2 = 65535;
                break;
            case 672836989:
                if (str.equals("os_version")) {
                    c2 = 19;
                    break;
                }
                c2 = 65535;
                break;
            case 722989291:
                if (str.equals("android_id")) {
                    c2 = 18;
                    break;
                }
                c2 = 65535;
                break;
            case 816209642:
                if (str.equals("notifications_enabled")) {
                    c2 = '&';
                    break;
                }
                c2 = 65535;
                break;
            case 839674195:
                if (str.equals("architecture")) {
                    c2 = 26;
                    break;
                }
                c2 = 65535;
                break;
            case 954101670:
                if (str.equals("background_location")) {
                    c2 = '\'';
                    break;
                }
                c2 = 65535;
                break;
            case 1014375387:
                if (str.equals("product_name")) {
                    c2 = 25;
                    break;
                }
                c2 = 65535;
                break;
            case 1167648233:
                if (str.equals(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING)) {
                    c2 = 15;
                    break;
                }
                c2 = 65535;
                break;
            case 1241166251:
                if (str.equals("screen_inches")) {
                    c2 = 23;
                    break;
                }
                c2 = 65535;
                break;
            case 1328981571:
                if (str.equals("install_referrer")) {
                    c2 = ')';
                    break;
                }
                c2 = 65535;
                break;
            case 1420630150:
                if (str.equals("is_genuine")) {
                    c2 = 21;
                    break;
                }
                c2 = 65535;
                break;
            case 1735689732:
                if (str.equals("screen_brightness")) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case 1741791591:
                if (str.equals("device_orientation")) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            case 1757114046:
                if (str.equals("fb_attribution_id")) {
                    c2 = 9;
                    break;
                }
                c2 = 65535;
                break;
            case 1874684019:
                if (str.equals("platform")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case 1974464370:
                if (str.equals("carrier_name")) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case 2118140562:
                if (str.equals("app_short_string")) {
                    c2 = 17;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                return "android";
            case 1:
                return Double.valueOf(a(context));
            case 2:
                return b(context);
            case 3:
                return c(context);
            case 4:
                return d(context);
            case 5:
                return g(context);
            case 6:
                return e(context);
            case 7:
                return i(context);
            case 8:
                return Boolean.valueOf(k(context));
            case 9:
                return t(context);
            case 10:
                return a();
            case 11:
                return l(context);
            case 12:
                return m(context);
            case 13:
                return n(context);
            case 14:
                return Integer.valueOf(o(context));
            case 15:
                return p(context);
            case 16:
                return q(context);
            case 17:
                return r(context);
            case 18:
                return s(context);
            case 19:
                return b();
            case 20:
                return u(context);
            case 21:
                return Boolean.valueOf(c());
            case 22:
                return Integer.valueOf(v(context));
            case 23:
                return w(context);
            case 24:
                return d();
            case 25:
                return e();
            case 26:
                return f();
            case 27:
                return x(context);
            case 28:
                return y(context);
            case 29:
                return Integer.valueOf(g());
            case 30:
                return z(context);
            case 31:
                return A(context);
            case ' ':
                return Boolean.valueOf(B(context));
            case '!':
            case '\"':
                return h();
            case '#':
                return i();
            case '$':
                return C(context);
            case '%':
                return D(context);
            case '&':
                return Boolean.valueOf(E(context));
            case '\'':
                return Boolean.valueOf(F(context));
            case '(':
                return Long.valueOf(j());
            case ')':
                return a(context, jSONObject);
            default:
                try {
                    Object obj2 = DataPointsNetwork.getNew(cVar.b, context, jSONObject, obj);
                    if (obj2 != null) {
                        return obj2;
                    }
                    return null;
                } catch (Throwable unused) {
                    Tracker.a(5, "DPT", "getNew", "Optional Network Library dependency missing, cannot gather " + cVar.b);
                    return null;
                }
        }
    }

    private static String a() {
        return Build.MODEL + "-" + Build.BRAND;
    }

    static List<String> a(int i) {
        int[] iArr;
        ArrayList arrayList = new ArrayList();
        for (c cVar : a) {
            int[] iArr2 = cVar.d;
            if ((iArr2 != null && x.a(iArr2, i)) || ((iArr = cVar.e) != null && x.a(iArr, i))) {
                arrayList.add(cVar.b);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, double):double
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, double):double */
    static JSONObject a(Context context, JSONObject jSONObject) {
        int i;
        int i2 = 10;
        double d2 = 1.0d;
        if (jSONObject != null) {
            i = jSONObject.has("install_referrer_attempts") ? x.a(x.b(jSONObject.opt("install_referrer_attempts"), 2), 1, Integer.MAX_VALUE) : 2;
            if (jSONObject.has("install_referrer_wait")) {
                i2 = x.a(x.b(jSONObject.opt("install_referrer_wait"), 10), 1, Integer.MAX_VALUE);
            }
            if (jSONObject.has("install_referrer_retry_wait")) {
                d2 = x.a(x.a(jSONObject.opt("install_referrer_retry_wait"), 1.0d), (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, Double.MAX_VALUE);
            }
        } else {
            i = 2;
        }
        Tracker.a(4, "DPT", "getInstallRef", "Attempts: " + i, "AttemptWait: " + i2, "AttemptRetryWait: " + d2);
        return a.a(a(context, i, i2, d2));
    }

    static void a(Context context, d dVar, JSONObject jSONObject, JSONObject jSONObject2, List<String> list, JSONArray jSONArray, JSONArray jSONArray2, int i, JSONObject jSONObject3) {
        int i2 = i;
        boolean z = i2 == 4;
        boolean z2 = i2 == 1;
        int i3 = 0;
        while (true) {
            c[] cVarArr = a;
            if (i3 < cVarArr.length) {
                c cVar = cVarArr[i3];
                if (a(cVar, i2, jSONArray2, jSONArray)) {
                    Tracker.a(5, "DPT", "get", i2 + "," + cVar.b);
                    cVar.a(context, dVar, jSONObject3, z, z2, list, jSONObject2.opt(cVar.b), x.f(jSONObject.opt(cVar.b)));
                }
                i3++;
            } else {
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    private void a(Context context, d dVar, JSONObject jSONObject, boolean z, boolean z2, List<String> list, Object obj, JSONObject jSONObject2) {
        boolean z3 = z;
        synchronized (this) {
            Object a2 = a(dVar, z3, list.contains(this.b));
            Object a3 = a(context, dVar, a2, obj, z, list, jSONObject2);
            boolean a4 = x.a(dVar.b(this.b + "_upd"), false);
            Tracker.a(4, "DPT", "addToPayload", this.b + ": " + a2 + "," + a3 + " hasUpdated: " + a4 + " isEqual: " + x.a(a3, a2));
            a(jSONObject, a3, a2, z, a4);
            if (z2 || z3) {
                dVar.a(this.b + "_upd", (Object) false);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0010, code lost:
        if (com.kochava.base.x.a(r6, r7) == false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(org.json.JSONObject r5, java.lang.Object r6, java.lang.Object r7, boolean r8, boolean r9) {
        /*
            r4 = this;
            r0 = 0
            r1 = 1
            java.lang.String r2 = "addToData"
            java.lang.String r3 = "DPT"
            if (r8 == 0) goto L_0x0018
            if (r6 == 0) goto L_0x0018
            if (r9 != 0) goto L_0x0012
            boolean r9 = com.kochava.base.x.a(r6, r7)     // Catch:{ JSONException -> 0x001f }
            if (r9 != 0) goto L_0x0018
        L_0x0012:
            java.lang.String r7 = r4.b     // Catch:{ JSONException -> 0x001f }
        L_0x0014:
            r4.a(r5, r7, r6)     // Catch:{ JSONException -> 0x001f }
            goto L_0x003e
        L_0x0018:
            if (r8 != 0) goto L_0x0021
            if (r6 == 0) goto L_0x0021
            java.lang.String r7 = r4.b     // Catch:{ JSONException -> 0x001f }
            goto L_0x0014
        L_0x001f:
            r5 = move-exception
            goto L_0x0036
        L_0x0021:
            if (r8 != 0) goto L_0x002b
            if (r7 == 0) goto L_0x002b
            java.lang.String r6 = r4.b     // Catch:{ JSONException -> 0x001f }
            r4.a(r5, r6, r7)     // Catch:{ JSONException -> 0x001f }
            goto L_0x003e
        L_0x002b:
            r5 = 5
            java.lang.Object[] r6 = new java.lang.Object[r1]     // Catch:{ JSONException -> 0x001f }
            java.lang.String r7 = "Skip"
            r6[r0] = r7     // Catch:{ JSONException -> 0x001f }
            com.kochava.base.Tracker.a(r5, r3, r2, r6)     // Catch:{ JSONException -> 0x001f }
            goto L_0x003e
        L_0x0036:
            r6 = 4
            java.lang.Object[] r7 = new java.lang.Object[r1]
            r7[r0] = r5
            com.kochava.base.Tracker.a(r6, r3, r2, r7)
        L_0x003e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.c.a(org.json.JSONObject, java.lang.Object, java.lang.Object, boolean, boolean):void");
    }

    private void a(JSONObject jSONObject, String str, Object obj) {
        if ((obj instanceof JSONObject) && ((JSONObject) obj).length() == 0) {
            return;
        }
        if (!(obj instanceof JSONArray) || ((JSONArray) obj).length() != 0) {
            jSONObject.put(str, obj);
        }
    }

    static boolean a(c cVar, int i, JSONArray jSONArray, JSONArray jSONArray2) {
        if (jSONArray2 != null) {
            if (x.a(jSONArray2, cVar.b)) {
                return false;
            }
        } else if (i != 0) {
            return false;
        }
        int[] iArr = cVar.d;
        if (iArr == null || !x.a(iArr, i)) {
            return cVar.e != null && x.a(jSONArray, cVar.b) && x.a(cVar.e, i);
        }
        return true;
    }

    private static String b() {
        return "Android " + Build.VERSION.RELEASE;
    }

    private static String b(Context context) {
        return context.getResources().getConfiguration().orientation == 2 ? Constants.ParametersKeys.ORIENTATION_LANDSCAPE : Constants.ParametersKeys.ORIENTATION_PORTRAIT;
    }

    private static Double c(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        if (audioManager == null) {
            return null;
        }
        double streamVolume = (double) audioManager.getStreamVolume(3);
        Double.isNaN(streamVolume);
        double streamMaxVolume = (double) audioManager.getStreamMaxVolume(3);
        Double.isNaN(streamMaxVolume);
        double round = (double) Math.round(((streamVolume * 1.0d) / streamMaxVolume) * 10000.0d);
        Double.isNaN(round);
        return Double.valueOf(x.a(round / 10000.0d, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, 1.0d));
    }

    private static boolean c() {
        String str = Build.TAGS;
        if (str != null && str.contains("test-keys")) {
            return false;
        }
        for (String file : new String[]{"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"}) {
            if (new File(file).exists()) {
                return false;
            }
        }
        return true;
    }

    private static String d() {
        return Build.MANUFACTURER;
    }

    private static String d(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        if (telephonyManager == null) {
            return null;
        }
        return telephonyManager.getNetworkOperatorName();
    }

    private static String e() {
        return Build.PRODUCT;
    }

    private static String e(Context context) {
        try {
            String string = Settings.Secure.getString(context.getContentResolver(), "advertising_id");
            if (string != null) {
                return string;
            }
            throw new Exception();
        } catch (Throwable unused) {
            Tracker.a(4, "DPT", "getFireAdvert", "Cannot retrieve Amazon Kindle Fire Advertising ID. Not running on Kindle Fire Device.");
            return null;
        }
    }

    private static Boolean f(Context context) {
        try {
            int i = Settings.Secure.getInt(context.getContentResolver(), "limit_ad_tracking", -1);
            if (i >= 0) {
                return Boolean.valueOf(i != 0);
            }
            throw new Exception();
        } catch (Throwable unused) {
            Tracker.a(4, "DPT", "getFireDevice", "Cannot retrieve Amazon Kindle Fire Device Limit Tracking. Not running on Kindle Fire Device.");
            return null;
        }
    }

    private static String f() {
        return System.getProperty("os.arch");
    }

    private static int g() {
        return x.a(Runtime.getRuntime().availableProcessors(), 1, Integer.MAX_VALUE);
    }

    private static String g(Context context) {
        try {
            Object invoke = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context);
            if (invoke != null) {
                String str = (String) invoke.getClass().getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
                if (str != null) {
                    return str;
                }
                throw new Exception();
            }
            throw new Exception();
        } catch (Throwable unused) {
            Tracker.a(4, "DPT", "getGoogleAdve", "Cannot retrieve Google Advertising ID, Not running on device with Google Play Services or missing required library.");
            return null;
        }
    }

    private static Boolean h(Context context) {
        try {
            Object invoke = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context);
            if (invoke != null) {
                return Boolean.valueOf(((Boolean) invoke.getClass().getMethod(Constants.RequestParameters.isLAT, new Class[0]).invoke(invoke, new Object[0])).booleanValue());
            }
            throw new Exception();
        } catch (Throwable unused) {
            Tracker.a(4, "DPT", "getGoogleDevi", "Cannot retrieve Google Device Limit Tracking, Not running on device with Google Play Services or missing required library.");
            return null;
        }
    }

    private static String h() {
        return Locale.getDefault().getLanguage() + "-" + Locale.getDefault().getCountry();
    }

    private static String i() {
        return TimeZone.getDefault().getID();
    }

    private static String i(Context context) {
        try {
            Object invoke = Class.forName("com.huawei.hms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context);
            if (invoke != null) {
                String str = (String) invoke.getClass().getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
                if (str != null) {
                    return str;
                }
                throw new Exception();
            }
            throw new Exception();
        } catch (Throwable unused) {
            Tracker.a(4, "DPT", "getHuaweiAdve", "Cannot retrieve Huawei Advertising ID, Not running on device with HMS Core or missing required library.");
            return null;
        }
    }

    private static long j() {
        return x.b() - x.d();
    }

    private static Boolean j(Context context) {
        try {
            Object invoke = Class.forName("com.huawei.hms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context);
            if (invoke != null) {
                return Boolean.valueOf(((Boolean) invoke.getClass().getMethod(Constants.RequestParameters.isLAT, new Class[0]).invoke(invoke, new Object[0])).booleanValue());
            }
            throw new Exception();
        } catch (Throwable unused) {
            Tracker.a(4, "DPT", "getHuaweiDevi", "Cannot retrieve Huawei Device Limit Tracking, Not running on device with HMS Core or missing required library.");
            return null;
        }
    }

    private static boolean k(Context context) {
        Boolean f = f(context);
        Boolean h = h(context);
        Boolean j = j(context);
        return (f != null && f.booleanValue()) || (h != null && h.booleanValue()) || (j != null && j.booleanValue());
    }

    private static Integer l(Context context) {
        Display defaultDisplay;
        Point point;
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager == null || (defaultDisplay = windowManager.getDefaultDisplay()) == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 17) {
            point = new Point();
            defaultDisplay.getRealSize(point);
        } else {
            point = new Point();
            defaultDisplay.getSize(point);
        }
        return Integer.valueOf(point.y);
    }

    private static Integer m(Context context) {
        Display defaultDisplay;
        Point point;
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager == null || (defaultDisplay = windowManager.getDefaultDisplay()) == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 17) {
            point = new Point();
            defaultDisplay.getRealSize(point);
        } else {
            point = new Point();
            defaultDisplay.getSize(point);
        }
        return Integer.valueOf(point.x);
    }

    private static String n(Context context) {
        return context.getPackageName();
    }

    private static int o(Context context) {
        return (int) (context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime / 1000);
    }

    private static String p(Context context) {
        return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    }

    private static String q(Context context) {
        return Integer.toString(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode);
    }

    private static String r(Context context) {
        return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
    }

    private static String s(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0059, code lost:
        if (r12 != null) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005b, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x007a, code lost:
        if (r12 != null) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x007d, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String t(android.content.Context r12) {
        /*
            java.lang.String r0 = "aid"
            r1 = 0
            android.content.pm.PackageManager r2 = r12.getPackageManager()     // Catch:{ all -> 0x0079 }
            java.lang.String r3 = "com.facebook.katana"
            r4 = 64
            android.content.pm.PackageInfo r2 = r2.getPackageInfo(r3, r4)     // Catch:{ all -> 0x0079 }
            android.content.pm.Signature[] r2 = r2.signatures     // Catch:{ all -> 0x0079 }
            if (r2 == 0) goto L_0x0073
            int r3 = r2.length     // Catch:{ all -> 0x0079 }
            r4 = 1
            if (r3 != r4) goto L_0x0073
            int r3 = r2.length     // Catch:{ all -> 0x0079 }
            r5 = 0
            r6 = 0
            r7 = 0
        L_0x001b:
            if (r6 >= r3) goto L_0x002f
            r8 = r2[r6]     // Catch:{ all -> 0x0079 }
            java.lang.String r9 = "30820268308201d102044a9c4610300d06092a864886f70d0101040500307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e3020170d3039303833313231353231365a180f32303530303932353231353231365a307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e30819f300d06092a864886f70d010101050003818d0030818902818100c207d51df8eb8c97d93ba0c8c1002c928fab00dc1b42fca5e66e99cc3023ed2d214d822bc59e8e35ddcf5f44c7ae8ade50d7e0c434f500e6c131f4a2834f987fc46406115de2018ebbb0d5a3c261bd97581ccfef76afc7135a6d59e8855ecd7eacc8f8737e794c60a761c536b72b11fac8e603f5da1a2d54aa103b8a13c0dbc10203010001300d06092a864886f70d0101040500038181005ee9be8bcbb250648d3b741290a82a1c9dc2e76a0af2f2228f1d9f9c4007529c446a70175c5a900d5141812866db46be6559e2141616483998211f4a673149fb2232a10d247663b26a9031e15f84bc1c74d141ff98a02d76f85b2c8ab2571b6469b232d8e768a7f7ca04f7abe4a775615916c07940656b58717457b42bd928a2"
            java.lang.String r8 = r8.toCharsString()     // Catch:{ all -> 0x0079 }
            boolean r8 = r9.equals(r8)     // Catch:{ all -> 0x0079 }
            if (r8 == 0) goto L_0x002c
            r7 = 1
        L_0x002c:
            int r6 = r6 + 1
            goto L_0x001b
        L_0x002f:
            if (r7 == 0) goto L_0x006d
            java.lang.String r2 = "content://com.facebook.katana.provider.AttributionIdProvider"
            android.net.Uri r7 = android.net.Uri.parse(r2)     // Catch:{ all -> 0x0079 }
            android.content.ContentResolver r6 = r12.getContentResolver()     // Catch:{ all -> 0x0079 }
            java.lang.String[] r8 = new java.lang.String[r4]     // Catch:{ all -> 0x0079 }
            r8[r5] = r0     // Catch:{ all -> 0x0079 }
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r12 = r6.query(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x0079 }
            if (r12 == 0) goto L_0x0065
            boolean r2 = r12.moveToFirst()     // Catch:{ all -> 0x006b }
            if (r2 == 0) goto L_0x0065
            int r0 = r12.getColumnIndex(r0)     // Catch:{ all -> 0x006b }
            r2 = -1
            if (r0 == r2) goto L_0x005f
            java.lang.String r1 = r12.getString(r0)     // Catch:{ all -> 0x006b }
            if (r12 == 0) goto L_0x007d
        L_0x005b:
            r12.close()
            goto L_0x007d
        L_0x005f:
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ all -> 0x006b }
            r0.<init>()     // Catch:{ all -> 0x006b }
            throw r0     // Catch:{ all -> 0x006b }
        L_0x0065:
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ all -> 0x006b }
            r0.<init>()     // Catch:{ all -> 0x006b }
            throw r0     // Catch:{ all -> 0x006b }
        L_0x006b:
            goto L_0x007a
        L_0x006d:
            java.lang.Exception r12 = new java.lang.Exception     // Catch:{ all -> 0x0079 }
            r12.<init>()     // Catch:{ all -> 0x0079 }
            throw r12     // Catch:{ all -> 0x0079 }
        L_0x0073:
            java.lang.Exception r12 = new java.lang.Exception     // Catch:{ all -> 0x0079 }
            r12.<init>()     // Catch:{ all -> 0x0079 }
            throw r12     // Catch:{ all -> 0x0079 }
        L_0x0079:
            r12 = r1
        L_0x007a:
            if (r12 == 0) goto L_0x007d
            goto L_0x005b
        L_0x007d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.c.t(android.content.Context):java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
     arg types: [java.lang.String, org.json.JSONArray, int]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void */
    private static JSONObject u(Context context) {
        if (!x.a(context, "android.permission.GET_ACCOUNTS")) {
            Tracker.a(4, "DPT", "ids", "Missing Permission: android.permission.GET_ACCOUNTS");
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        try {
            Class<?> cls = Class.forName("android.accounts.AccountManager");
            Field field = Class.forName("android.accounts.Account").getField("name");
            Object invoke = cls.getMethod("get", Context.class).invoke(null, context);
            for (Object obj : (Object[]) invoke.getClass().getMethod("getAccounts", new Class[0]).invoke(invoke, new Object[0])) {
                String str = (String) field.get(obj);
                if (Patterns.EMAIL_ADDRESS.matcher(str).matches()) {
                    x.a((Object) str, jSONArray, false);
                }
            }
        } catch (Throwable th) {
            Tracker.a(4, "DPT", "getIds", th);
        }
        if (jSONArray.length() <= 0) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        x.a("email", x.a(jSONArray).replaceAll("\"", ""), jSONObject);
        return jSONObject;
    }

    private static int v(Context context) {
        return context.getResources().getDisplayMetrics().densityDpi;
    }

    private static Double w(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        if (displayMetrics == null) {
            return null;
        }
        double round = (double) Math.round(Math.sqrt(Math.pow((double) (((float) displayMetrics.widthPixels) / displayMetrics.xdpi), 2.0d) + Math.pow((double) (((float) displayMetrics.heightPixels) / displayMetrics.ydpi), 2.0d)) * 10.0d);
        Double.isNaN(round);
        return Double.valueOf(round / 10.0d);
    }

    private static String x(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null || !registerReceiver.hasExtra("status")) {
            return null;
        }
        int intExtra = registerReceiver.getIntExtra("status", -1);
        return intExtra != 2 ? intExtra != 3 ? intExtra != 4 ? intExtra != 5 ? "unknown" : MessengerShareContentUtility.WEBVIEW_RATIO_FULL : "not_charging" : "discharging" : "charging";
    }

    private static Integer y(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null || !registerReceiver.hasExtra("level")) {
            return null;
        }
        return Integer.valueOf(x.a(registerReceiver.getIntExtra("level", -1), 0, 100));
    }

    private static Integer z(Context context) {
        TelephonyManager telephonyManager;
        List<CellInfo> allCellInfo;
        int i;
        if (!(x.a(context, "android.permission.ACCESS_COARSE_LOCATION") || x.a(context, "android.permission.ACCESS_FINE_LOCATION")) || Build.VERSION.SDK_INT < 17 || (telephonyManager = (TelephonyManager) context.getSystemService((String) PlaceFields.PHONE)) == null || (allCellInfo = telephonyManager.getAllCellInfo()) == null) {
            return null;
        }
        Iterator<CellInfo> it = allCellInfo.iterator();
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            }
            CellInfo next = it.next();
            if (next.isRegistered()) {
                if (!(next instanceof CellInfoGsm)) {
                    if (!(next instanceof CellInfoCdma)) {
                        if (!(next instanceof CellInfoLte)) {
                            if (Build.VERSION.SDK_INT >= 18 && (next instanceof CellInfoWcdma)) {
                                i = ((CellInfoWcdma) next).getCellSignalStrength().getLevel();
                                break;
                            }
                        } else {
                            i = ((CellInfoLte) next).getCellSignalStrength().getLevel();
                            break;
                        }
                    } else {
                        i = ((CellInfoCdma) next).getCellSignalStrength().getLevel();
                        break;
                    }
                } else {
                    i = ((CellInfoGsm) next).getCellSignalStrength().getLevel();
                    break;
                }
            }
        }
        if (i == -1) {
            return null;
        }
        return Integer.valueOf(x.a(i * 25, 0, 100));
    }

    /* access modifiers changed from: package-private */
    public final Object a(d dVar, boolean z, boolean z2) {
        Object b2 = dVar.b(this.b);
        if (b2 == null) {
            return null;
        }
        if (z) {
            return b2;
        }
        if (this.c != -1) {
            Integer c2 = x.c(dVar.b(this.b + "_ts"));
            if (c2 == null || c2.intValue() + this.c < x.c()) {
                return null;
            }
            return b2;
        } else if (z2) {
            return b2;
        } else {
            return null;
        }
    }
}
