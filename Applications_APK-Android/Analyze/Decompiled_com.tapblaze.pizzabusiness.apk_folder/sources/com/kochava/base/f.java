package com.kochava.base;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

final class f {
    private double a = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    private boolean b = false;
    private long c = 0;

    f() {
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized long a() {
        /*
            r9 = this;
            monitor-enter(r9)
            double r0 = r9.a     // Catch:{ all -> 0x0051 }
            r2 = -4676364914835832019(0xbf1a36e2eb1c432d, double:-1.0E-4)
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x0010
            r0 = -1
        L_0x000e:
            monitor-exit(r9)
            return r0
        L_0x0010:
            r0 = 0
            java.lang.Double r0 = java.lang.Double.valueOf(r0)     // Catch:{ all -> 0x0051 }
            double r1 = r9.a     // Catch:{ all -> 0x0051 }
            java.lang.Double r1 = java.lang.Double.valueOf(r1)     // Catch:{ all -> 0x0051 }
            boolean r0 = com.kochava.base.x.a(r0, r1)     // Catch:{ all -> 0x0051 }
            r1 = 0
            if (r0 == 0) goto L_0x0026
            monitor-exit(r9)
            return r1
        L_0x0026:
            long r3 = com.kochava.base.x.d()     // Catch:{ all -> 0x0051 }
            double r5 = r9.a     // Catch:{ all -> 0x0051 }
            r7 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r5 = r5 * r7
            long r5 = java.lang.Math.round(r5)     // Catch:{ all -> 0x0051 }
            long r7 = r9.c     // Catch:{ all -> 0x0051 }
            long r7 = r7 + r5
            int r0 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r0 < 0) goto L_0x0043
            r9.c = r3     // Catch:{ all -> 0x0051 }
            r0 = 0
            r9.b = r0     // Catch:{ all -> 0x0051 }
        L_0x0043:
            boolean r0 = r9.b     // Catch:{ all -> 0x0051 }
            if (r0 == 0) goto L_0x004c
            long r0 = r9.c     // Catch:{ all -> 0x0051 }
            long r0 = r0 + r5
            long r0 = r0 - r3
            goto L_0x000e
        L_0x004c:
            r0 = 1
            r9.b = r0     // Catch:{ all -> 0x0051 }
            monitor-exit(r9)
            return r1
        L_0x0051:
            r0 = move-exception
            monitor-exit(r9)
            goto L_0x0055
        L_0x0054:
            throw r0
        L_0x0055:
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.f.a():long");
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(double d) {
        this.a = d;
    }
}
