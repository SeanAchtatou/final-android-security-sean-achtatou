package com.kochava.base.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.sdk.constants.Constants;
import org.json.JSONObject;

public final class DataPointsNetwork {
    private static String a(Context context) {
        ConnectivityManager connectivityManager;
        if (!a(context, "android.permission.ACCESS_NETWORK_STATE") || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null) {
            return null;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return Constants.ParametersKeys.ORIENTATION_NONE;
        }
        int type = activeNetworkInfo.getType();
        return (type == 0 || type == 4 || type == 5 || type == 2 || type == 3) ? "cellular" : type == 9 ? "wired" : ConnectivityService.NETWORK_TYPE_WIFI;
    }

    private static boolean a(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    private static boolean b(Context context) {
        ConnectivityManager connectivityManager;
        if (Build.VERSION.SDK_INT >= 16 && a(context, "android.permission.ACCESS_NETWORK_STATE") && (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) != null) {
            return connectivityManager.isActiveNetworkMetered();
        }
        return false;
    }

    private static String c(Context context) {
        WifiManager wifiManager;
        if (a(context, "android.permission.ACCESS_WIFI_STATE") && (wifiManager = (WifiManager) context.getApplicationContext().getSystemService((String) ConnectivityService.NETWORK_TYPE_WIFI)) != null) {
            return wifiManager.getConnectionInfo().getSSID();
        }
        return null;
    }

    private static String d(Context context) {
        WifiManager wifiManager;
        if (a(context, "android.permission.ACCESS_WIFI_STATE") && (wifiManager = (WifiManager) context.getApplicationContext().getSystemService((String) ConnectivityService.NETWORK_TYPE_WIFI)) != null) {
            return wifiManager.getConnectionInfo().getBSSID();
        }
        return null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static Object getNew(String str, Context context, JSONObject jSONObject, Object obj) {
        char c;
        switch (str.hashCode()) {
            case -184604772:
                if (str.equals("network_conn_type")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 3539835:
                if (str.equals("ssid")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 94044893:
                if (str.equals("bssid")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 1974856919:
                if (str.equals("network_metered")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (c == 0) {
            return a(context);
        }
        if (c == 1) {
            return Boolean.valueOf(b(context));
        }
        if (c == 2) {
            return c(context);
        }
        if (c != 3) {
            return null;
        }
        return d(context);
    }
}
