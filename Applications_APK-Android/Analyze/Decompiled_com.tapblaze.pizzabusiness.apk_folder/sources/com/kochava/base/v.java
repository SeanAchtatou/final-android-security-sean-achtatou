package com.kochava.base;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.vungle.warren.analytics.AnalyticsEvent;
import org.json.JSONObject;

final class v extends j implements AttributionUpdateListener {
    /* access modifiers changed from: private */
    public final String b;
    /* access modifiers changed from: private */
    public final DeeplinkProcessedListener c;
    private final Handler d = new Handler(Looper.getMainLooper());
    private final Runnable e = new Runnable() {
        public final void run() {
            v.this.a(new Deeplink(new JSONObject(), v.this.b));
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, long):long
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, long):long */
    v(i iVar, String str, long j, DeeplinkProcessedListener deeplinkProcessedListener) {
        super(iVar, true);
        this.b = str;
        this.c = deeplinkProcessedListener;
        this.d.postDelayed(this.e, x.a(j, x.a(iVar.d.b("deeplinks_timeout_minimum"), 250L), x.a(iVar.d.b("deeplinks_timeout_maximum"), 30000L)));
    }

    /* access modifiers changed from: private */
    public void a(final Deeplink deeplink) {
        synchronized (this) {
            if (!e()) {
                d();
                this.d.removeCallbacks(this.e);
                this.d.post(new Runnable() {
                    public final void run() {
                        try {
                            v.this.c.onDeeplinkProcessed(deeplink);
                        } catch (Throwable th) {
                            Tracker.a(2, "TSL", "callback", "Exception in Host App", th);
                        }
                    }
                });
            }
        }
    }

    private void a(String str) {
        if (!str.isEmpty()) {
            String replace = str.replace("{device_id}", x.a(this.a.d.b("kochava_device_id"), "")).replace("{type}", "kochava_device_id");
            JSONObject jSONObject = new JSONObject();
            x.a(AnalyticsEvent.Ad.clickUrl, replace, jSONObject);
            this.a.d.c(jSONObject);
        }
    }

    private String o() {
        try {
            return Uri.parse(this.a.a(13)).buildUpon().appendQueryParameter("path", this.b).build().toString();
        } catch (Throwable unused) {
            return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.String, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    private void p() {
        boolean a = x.a(this.a.d.b("deeplinks_allow_deferred"), true);
        if (!this.a.q || !a) {
            a(new Deeplink(new JSONObject(), ""));
            return;
        }
        String a2 = x.a(this.a.d.b("attribution"));
        if (a2 == null || a2.isEmpty()) {
            Tracker.a(4, "TSL", "runNoInputDes", "First launch, requesting attribution");
            this.a.g.a(this, true);
            return;
        }
        Tracker.a(4, "TSL", "runNoInputDes", "First launch, using attribution");
        a(new Deeplink(x.b(x.b((Object) a2, true).opt("deferred_deeplink"), true), ""));
    }

    public final void onAttributionUpdated(String str) {
        Tracker.a(4, "TSL", "onAttribution", "Retrieved attribution, waking to process smartlink");
        a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.kochava.base.v.a(com.kochava.base.v, com.kochava.base.Deeplink):void
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    public final void run() {
        Tracker.a(4, "TSL", "run", new Object[0]);
        if (e()) {
            Tracker.a(4, "TSL", "run", "Task complete");
            i();
        } else if (this.b.trim().isEmpty()) {
            p();
        } else {
            Tracker.a(4, "TSL", "run", "has path, querying smartlink API");
            JSONObject f = x.f(a(o(), true));
            if (f == null || e()) {
                a(new Deeplink(new JSONObject(), this.b));
                return;
            }
            a(x.a(x.b(f.opt("app_link"), true).opt(AnalyticsEvent.Ad.clickUrl), ""));
            a(new Deeplink(x.b(f.opt("deeplink"), true), this.b));
            i();
        }
    }
}
