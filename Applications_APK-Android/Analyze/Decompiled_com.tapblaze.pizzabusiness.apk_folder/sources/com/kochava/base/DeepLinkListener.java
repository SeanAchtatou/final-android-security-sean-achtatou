package com.kochava.base;

import java.util.Map;

public interface DeepLinkListener {
    void onDeepLink(Map<String, String> map);
}
