package com.kochava.base;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.ironsource.sdk.constants.Constants;
import com.kochava.base.Tracker;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.json.JSONArray;
import org.json.JSONObject;

final class a implements b, g, Runnable {
    final i a;
    final o b;
    final w c;
    final p d;
    final u e;
    final c f;
    s g = null;
    private final List<m> h = new ArrayList();
    private final h i;
    private final C0044a j;
    private e k = null;

    /* renamed from: com.kochava.base.a$a  reason: collision with other inner class name */
    private static class C0044a {
        final String a;
        final String b;
        final String c;
        final JSONObject d;
        final String e;

        C0044a(String str, String str2, String str3, JSONObject jSONObject, String str4) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = jSONObject;
            this.e = str4;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
         arg types: [java.lang.String, boolean]
         candidates:
          com.kochava.base.d.a(android.content.Context, boolean):void
          com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
        /* access modifiers changed from: package-private */
        public final void a(i iVar) {
            iVar.d.a("sdk_version", this.a);
            if (this.b != null) {
                iVar.d.a("kochava_app_id", this.b);
            } else {
                iVar.d.a("kochava_app_id");
            }
            if (this.c != null) {
                iVar.d.a("partner_name", this.c);
            } else {
                iVar.d.a("partner_name");
            }
            if (this.d != null) {
                iVar.d.a("custom", this.d);
            } else {
                iVar.d.a("custom");
            }
            if (this.e != null) {
                iVar.d.a("ext_date", this.e);
            } else {
                iVar.d.a("ext_date");
            }
            if (x.b(iVar.d.b("first_launch_time"), 0) == 0) {
                iVar.d.a("first_launch_time", Integer.valueOf(x.c()));
            }
            if (x.a(iVar.d.b("kochava_device_id")) == null) {
                String replace = "3.7.0".replace(".", "");
                int c2 = x.c();
                String replaceAll = UUID.randomUUID().toString().replaceAll("-", "");
                d dVar = iVar.d;
                dVar.a("kochava_device_id", "KA" + replace + c2 + "t" + replaceAll);
                iVar.d.a("initial_needs_sent", (Object) true);
            }
        }
    }

    private static class b {
        final String a;
        final Bundle b;

        b(String str, Bundle bundle) {
            this.a = str;
            this.b = bundle;
        }
    }

    static class c implements Runnable {
        final BlockingQueue<b> a = new ArrayBlockingQueue(100);
        private final i b;
        private final b c;

        c(i iVar, b bVar) {
            this.b = iVar;
            this.c = bVar;
        }

        /* access modifiers changed from: package-private */
        public final void a() {
            Tracker.a(5, "QUP", "queueProcess", "start");
            this.b.b(this);
        }

        /* access modifiers changed from: package-private */
        public final void a(b bVar) {
            Tracker.a(5, "QUP", "queueProcess", "add");
            this.a.offer(bVar);
            a();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
         arg types: [java.lang.String, int]
         candidates:
          com.kochava.base.x.b(java.lang.Object, int):int
          com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
          com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
          com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
          com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
          com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
        public final void run() {
            Tracker.a(5, "QUP", "queueProcess", "run");
            while (!this.a.isEmpty()) {
                if (!this.b.m || !this.c.d() || this.c.f() != 0) {
                    try {
                        b poll = this.a.poll();
                        if (poll != null) {
                            if (!this.b.m || !this.c.d() || this.c.e()) {
                                String str = poll.a;
                                char c2 = 65535;
                                switch (str.hashCode()) {
                                    case -1878682790:
                                        if (str.equals("setIdentityLink")) {
                                            c2 = 2;
                                            break;
                                        }
                                        break;
                                    case -548851274:
                                        if (str.equals("setAppLimitAdTracking")) {
                                            c2 = 1;
                                            break;
                                        }
                                        break;
                                    case 2762738:
                                        if (str.equals("sendEvent")) {
                                            c2 = 3;
                                            break;
                                        }
                                        break;
                                    case 760458429:
                                        if (str.equals("setPushToken")) {
                                            c2 = 0;
                                            break;
                                        }
                                        break;
                                }
                                if (c2 == 0) {
                                    new t(this.b, poll.b.getString("token", ""), poll.b.getBoolean("enable"), false).run();
                                } else if (c2 == 1) {
                                    new k(this.b, poll.b.getBoolean("appLimitAdTracking")).run();
                                } else if (c2 == 2) {
                                    new n(this.b, x.b((Object) poll.b.getString("identityLink"), true)).run();
                                } else if (c2 == 3) {
                                    new l(this.b, 6, poll.b.getString(Constants.ParametersKeys.EVENT_NAME), poll.b.getString("eventData"), poll.b.getString("receiptJson"), poll.b.getString("receiptSignature")).run();
                                }
                            } else {
                                Tracker.a(3, "QUP", "queueProcess", "Consent denied. Dropping Incoming Action: " + poll.a);
                            }
                        }
                    } catch (Throwable th) {
                        Tracker.a(2, "QUP", "queue", th);
                    }
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    a(Context context, String str, String str2, String str3, String str4, ConsentStatusChangeListener consentStatusChangeListener, JSONObject jSONObject, JSONObject jSONObject2, boolean z, boolean z2, String str5) {
        String str6 = str;
        Object[] objArr = new Object[8];
        objArr[0] = "version: " + str6;
        objArr[1] = "extensionDate: " + str2;
        objArr[2] = "appGuid: " + str3;
        objArr[3] = "partnerName: " + str4;
        objArr[4] = "intelligentConsentManagement: " + z;
        objArr[5] = "selfManagedConsentRequirements" + z2;
        StringBuilder sb = new StringBuilder();
        sb.append("consentStatusChangeListener: ");
        sb.append(consentStatusChangeListener != null);
        objArr[6] = sb.toString();
        objArr[7] = "custom: " + jSONObject2;
        Tracker.a(4, "CTR", "Controller", objArr);
        C0044a aVar = r0;
        C0044a aVar2 = new C0044a(str, str3, str4, jSONObject2, str2);
        this.j = aVar;
        String str7 = "CTR";
        this.a = new i(context, this, this, jSONObject, consentStatusChangeListener, z, z2, str5);
        this.f = new c(this.a, this);
        this.b = new o(this.a);
        this.c = new w(this.a);
        this.d = new p(this.a);
        this.e = new u(this.a);
        try {
            Class.forName("com.kochava.base.location.LocationTracker");
            this.g = new q(this.a);
        } catch (Throwable unused) {
            Tracker.a(3, str7, "Controller", "LocationTracker module not present");
        }
        this.a.d.a("sdk_version", str6);
        this.j.a(this.a);
        this.i = new h(this.a.a, this.a.h, this);
        if (!this.a.m || !d() || e()) {
            this.a.d.c(true);
            if (this.g != null && x.a(this.a.d.b("initial_ever_sent"), false)) {
                this.g.run();
            }
        }
        i iVar = this.a;
        iVar.a(iVar.f, 50);
    }

    /* access modifiers changed from: package-private */
    public final String a(String str) {
        return x.a(this.a.d.b(str), "");
    }

    /* access modifiers changed from: package-private */
    public final void a(Uri uri, int i2, DeepLinkListener deepLinkListener) {
        e eVar = this.k;
        if (eVar != null) {
            eVar.a();
            this.k = null;
        }
        this.k = new e(uri, i2, this.a, deepLinkListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.a, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    public final void a(AttributionUpdateListener attributionUpdateListener, boolean z) {
        m mVar = new m(this.a, attributionUpdateListener, z);
        synchronized (this.h) {
            this.h.add(mVar);
        }
        this.a.a((Runnable) this, false);
    }

    /* access modifiers changed from: package-private */
    public final void a(Tracker.IdentityLink identityLink) {
        Bundle bundle = new Bundle();
        bundle.putString("identityLink", x.a(identityLink.a));
        this.f.a(new b("setIdentityLink", bundle));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.v, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(String str, long j2, DeeplinkProcessedListener deeplinkProcessedListener) {
        this.a.a((Runnable) new v(this.a, str, j2, deeplinkProcessedListener), true);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2, String str3, String str4) {
        Tracker.a(5, "CTR", "sendEvent", new Object[0]);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.ParametersKeys.EVENT_NAME, str);
        bundle.putString("eventData", str2);
        bundle.putString("receiptJson", str3);
        bundle.putString("receiptSignature", str4);
        this.f.a(new b("sendEvent", bundle));
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putString("token", str);
        bundle.putBoolean("enable", z);
        this.f.a(new b("setPushToken", bundle));
    }

    public final void a(JSONObject jSONObject) {
        s sVar = this.g;
        if (sVar != null) {
            sVar.b(jSONObject);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        i iVar = this.a;
        if (iVar != null) {
            if (iVar.h != null) {
                this.a.h.removeCallbacksAndMessages(null);
            }
            if (this.a.i != null) {
                this.a.i.removeCallbacksAndMessages(null);
            }
            if (this.a.j != null) {
                this.a.j.quit();
            }
            if (this.a.k != null) {
                this.a.k.quit();
            }
            if (!(this.i == null || this.a.a == null)) {
                ((Application) this.a.a).unregisterActivityLifecycleCallbacks(this.i);
                this.a.a.unregisterComponentCallbacks(this.i);
            }
            if (z && this.a.d != null) {
                this.a.d.b();
                this.a.d.b(false);
            }
            if (this.a.d != null) {
                this.a.d.a();
            }
            s sVar = this.g;
            if (sVar != null) {
                sVar.a(z);
                this.g = null;
            }
            this.h.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.a.r;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    /* access modifiers changed from: package-private */
    public final JSONObject b(String str) {
        return x.b(this.a.d.b(str), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [java.lang.Runnable, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* access modifiers changed from: package-private */
    public final void b(boolean z) {
        if (this.a.r == z) {
            Tracker.a(4, "CTR", "setSleep", "Rejecting same as current");
            return;
        }
        Tracker.a(3, "CTR", "setSleep", Boolean.valueOf(z));
        i iVar = this.a;
        iVar.r = z;
        if (!iVar.r) {
            i iVar2 = this.a;
            iVar2.a(iVar2.f, true);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        if (this.a.m) {
            return this.a.n || b(Constants.RequestParameters.CONSENT).length() >= 1;
        }
        Tracker.a(3, "CTR", "isConsentRequ", "Consent system disabled: Requirements Unknown");
        return false;
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        if (this.a.m) {
            return x.a(b(Constants.RequestParameters.CONSENT).opt("description"), "");
        }
        Tracker.a(3, "CTR", "getConsentDes", "Consent system disabled: Ignoring");
        return "";
    }

    /* access modifiers changed from: package-private */
    public final void c(boolean z) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("appLimitAdTracking", z);
        this.f.a(new b("setAppLimitAdTracking", bundle));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, org.json.JSONObject, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.a, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    public final void d(boolean z) {
        if (!this.a.m) {
            Tracker.a(3, "CTR", "setConsentReq", "Consent system disabled: Ignoring");
        } else if (!this.a.n) {
            Tracker.a(3, "CTR", "setConsentReq", "Consent is Kochava Managed not Self Managed. Cannot set required.");
        } else if (d() == z) {
            Tracker.a(4, "CTR", "setConsentReq", "Consent required did not change: Ignoring");
        } else {
            JSONObject b2 = b(Constants.RequestParameters.CONSENT);
            x.a("required", Boolean.valueOf(z), b2);
            this.a.d.a(Constants.RequestParameters.CONSENT, (Object) b2, true);
            if (!z) {
                this.a.d.c(true);
                this.a.a((Runnable) this, true);
            } else if (!e()) {
                this.a.d.c(false);
                this.a.d.a(true);
                this.j.a(this.a);
                g(true);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    public final boolean d() {
        if (this.a.m) {
            return x.a(b(Constants.RequestParameters.CONSENT).opt("required"), true);
        }
        Tracker.a(3, "CTR", "isConsentRequ", "Consent system disabled: Ignoring");
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
     arg types: [java.lang.String, boolean, org.json.JSONObject]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.Integer, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, org.json.JSONObject, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, int, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* access modifiers changed from: package-private */
    public final void e(boolean z) {
        if (!this.a.m) {
            Tracker.a(3, "CTR", "setConsentGra", "Consent system disabled: Ignoring");
            return;
        }
        JSONObject b2 = b(Constants.RequestParameters.CONSENT);
        x.a(b2, z);
        x.a("should_prompt", (Object) false, b2);
        this.a.d.a("consent_last_prompt", (Object) Integer.valueOf(x.c()), true);
        this.a.d.a(Constants.RequestParameters.CONSENT, (Object) b2, true);
        if (z) {
            this.a.d.c(true);
            this.d.h();
            this.c.h();
            this.e.h();
            s sVar = this.g;
            if (sVar != null) {
                sVar.h();
            }
            if (this.a.d.b("blacklist") == null) {
                this.b.h();
                this.a.d.a("init_last_sent", (Object) 0, true);
            }
            i iVar = this.a;
            iVar.a(iVar.f, 50);
        } else {
            this.a.i.removeCallbacks(this);
            this.a.i.removeCallbacks(this.b);
            this.a.i.removeCallbacks(this.d);
            this.a.i.removeCallbacks(this.c);
            this.a.i.removeCallbacks(this.e);
            if (this.g != null) {
                this.a.i.removeCallbacks(this.g);
            }
            this.a.d.a(false);
            this.a.d.c(false);
            g(true);
        }
        this.j.a(this.a);
        this.f.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    public final boolean e() {
        if (this.a.m) {
            return x.a(b(Constants.RequestParameters.CONSENT).opt(Tracker.ConsentPartner.KEY_GRANTED), false);
        }
        Tracker.a(3, "CTR", "isConsentGran", "Consent system disabled: Ignoring");
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, long):long
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, long):long */
    public final long f() {
        if (this.a.m) {
            return x.a(b(Constants.RequestParameters.CONSENT).opt(Tracker.ConsentPartner.KEY_RESPONSE_TIME), 0L);
        }
        Tracker.a(3, "CTR", "getConsentTim", "Consent system disabled: Ignoring");
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.l, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    public final void f(boolean z) {
        l lVar;
        boolean z2 = z;
        boolean z3 = true;
        boolean a2 = x.a(this.a.d.b("session_tracking"), true);
        if (x.a(this.a.d.b("initial_data")) == null) {
            z3 = false;
        }
        boolean a3 = x.a(this.a.d.b("initial_ever_sent"), false);
        if (a2 && (z3 || a3)) {
            i iVar = this.a;
            if (z2) {
                iVar.s = x.b();
                iVar = this.a;
                lVar = new l(iVar, 2, null, null, null, null);
            } else {
                lVar = new l(iVar, 3, null, null, null, null);
            }
            iVar.a((Runnable) lVar, false);
        }
        if (z2 && this.c.e()) {
            this.c.h();
        }
        if (!z2) {
            this.e.h();
        }
        i iVar2 = this.a;
        iVar2.t = z2;
        iVar2.a(iVar2.f, 50);
    }

    public final String g() {
        if (this.a.m) {
            return x.a(b(Constants.RequestParameters.CONSENT).opt("prompt_id"), "");
        }
        Tracker.a(3, "CTR", "getConsentPro", "Consent system disabled: Ignoring");
        return "";
    }

    public final void g(boolean z) {
        s sVar = this.g;
        if (sVar != null) {
            sVar.a(z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* access modifiers changed from: package-private */
    public final boolean h() {
        if (this.a.m) {
            return x.a(b(Constants.RequestParameters.CONSENT).opt("should_prompt"), false);
        }
        Tracker.a(3, "CTR", "isConsentShou", "Consent system disabled: Ignoring");
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
     arg types: [java.lang.String, boolean, org.json.JSONObject]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, org.json.JSONObject, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.Integer, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* access modifiers changed from: package-private */
    public final void i() {
        if (!this.a.m) {
            Tracker.a(3, "CTR", "clearConsentS", "Consent system disabled: Ignoring");
            return;
        }
        JSONObject b2 = b(Constants.RequestParameters.CONSENT);
        x.a("should_prompt", (Object) false, b2);
        this.a.d.a(Constants.RequestParameters.CONSENT, (Object) b2, true);
        this.a.d.a("consent_last_prompt", (Object) Integer.valueOf(x.c()), true);
    }

    /* access modifiers changed from: package-private */
    public final JSONArray j() {
        if (this.a.m) {
            return x.c(b(Constants.RequestParameters.CONSENT).opt(Tracker.ConsentPartner.KEY_PARTNERS), true);
        }
        Tracker.a(3, "CTR", "getConsentPar", "Consent system disabled: Ignoring");
        return new JSONArray();
    }

    public final void k() {
        s sVar = this.g;
        if (sVar != null) {
            sVar.o();
        }
    }

    public final boolean l() {
        return this.b.e() && this.c.e() && this.d.e();
    }

    public final boolean m() {
        h hVar = this.i;
        return hVar == null || hVar.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, int, int]
     candidates:
      com.kochava.base.d.a(boolean, java.lang.String, java.lang.String):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.o, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.t, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.w, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.p, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.m, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.s, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.i.a(java.lang.Runnable, boolean):void
     arg types: [com.kochava.base.u, int]
     candidates:
      com.kochava.base.i.a(java.lang.Runnable, long):void
      com.kochava.base.i.a(java.lang.Runnable, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x022b, code lost:
        r1 = r9.g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x022d, code lost:
        if (r1 == null) goto L_0x0264;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0233, code lost:
        if (r1.e() != false) goto L_0x0264;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0235, code lost:
        com.kochava.base.Tracker.a(5, "CTR", "Controller", "LOCATION_TRACKER");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0248, code lost:
        if (r9.g.g() != false) goto L_0x0257;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x024a, code lost:
        r9.g.f();
        r9.a.a((java.lang.Runnable) r9.g, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0257, code lost:
        com.kochava.base.Tracker.a(5, "CTR", "Controller", "LOCATION_TRACKER SKIP");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x026a, code lost:
        if (r9.e.e() != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x026c, code lost:
        com.kochava.base.Tracker.a(5, "CTR", "Controller", "QUEUE");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x027f, code lost:
        if (r9.e.g() != false) goto L_0x028e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0281, code lost:
        r9.e.f();
        r9.a.a((java.lang.Runnable) r9.e, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x028e, code lost:
        com.kochava.base.Tracker.a(5, "CTR", "Controller", "QUEUE SKIP");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r9 = this;
            r0 = 1
            java.lang.Object[] r1 = new java.lang.Object[r0]
            r2 = 0
            java.lang.String r3 = "WAKE"
            r1[r2] = r3
            r3 = 5
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            com.kochava.base.Tracker.a(r3, r4, r5, r1)
            com.kochava.base.i r1 = r9.a
            boolean r1 = r1.r
            if (r1 == 0) goto L_0x0024
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r1 = "SLEEP: SKIP"
            r0[r2] = r1
            java.lang.String r1 = "CTR"
            java.lang.String r2 = "Controller"
            com.kochava.base.Tracker.a(r3, r1, r2, r0)
            return
        L_0x0024:
            com.kochava.base.o r1 = r9.b
            boolean r1 = r1.e()
            if (r1 == 0) goto L_0x00b0
            com.kochava.base.i r1 = r9.a
            com.kochava.base.d r1 = r1.d
            java.lang.String r4 = "kvinit_staleness"
            java.lang.Object r1 = r1.b(r4)
            r4 = 86400(0x15180, float:1.21072E-40)
            int r1 = com.kochava.base.x.b(r1, r4)
            com.kochava.base.i r4 = r9.a
            com.kochava.base.d r4 = r4.d
            java.lang.String r5 = "init_last_sent"
            java.lang.Object r4 = r4.b(r5)
            int r5 = com.kochava.base.x.c()
            int r4 = com.kochava.base.x.b(r4, r5)
            r5 = 3
            java.lang.Object[] r5 = new java.lang.Object[r5]
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "initLastSent: "
            r6.append(r7)
            r6.append(r4)
            java.lang.String r6 = r6.toString()
            r5[r2] = r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "initStaleness: "
            r6.append(r7)
            r6.append(r1)
            java.lang.String r6 = r6.toString()
            r5[r0] = r6
            r6 = 2
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "now: "
            r7.append(r8)
            int r8 = com.kochava.base.x.c()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r5[r6] = r7
            java.lang.String r6 = "CTR"
            java.lang.String r7 = "Controller"
            com.kochava.base.Tracker.a(r3, r6, r7, r5)
            int r4 = r4 + r1
            int r1 = com.kochava.base.x.c()
            if (r4 > r1) goto L_0x00b0
            com.kochava.base.i r1 = r9.a
            com.kochava.base.d r1 = r1.d
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            java.lang.String r5 = "init_last_sent"
            r1.a(r5, r4, r0)
            com.kochava.base.o r1 = r9.b
            r1.h()
        L_0x00b0:
            com.kochava.base.o r1 = r9.b
            boolean r1 = r1.e()
            if (r1 != 0) goto L_0x00e8
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.String r4 = "INIT"
            r1[r2] = r4
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            com.kochava.base.Tracker.a(r3, r4, r5, r1)
            com.kochava.base.o r1 = r9.b
            boolean r1 = r1.g()
            if (r1 != 0) goto L_0x00da
            com.kochava.base.o r1 = r9.b
            r1.f()
            com.kochava.base.i r1 = r9.a
            com.kochava.base.o r2 = r9.b
            r1.a(r2, r0)
            goto L_0x00e7
        L_0x00da:
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r1 = "INIT SKIP"
            r0[r2] = r1
            java.lang.String r1 = "CTR"
            java.lang.String r2 = "Controller"
            com.kochava.base.Tracker.a(r3, r1, r2, r0)
        L_0x00e7:
            return
        L_0x00e8:
            com.kochava.base.i r1 = r9.a
            boolean r1 = r1.m
            if (r1 == 0) goto L_0x00fb
            boolean r1 = r9.d()
            if (r1 == 0) goto L_0x00fb
            boolean r1 = r9.e()
            if (r1 != 0) goto L_0x00fb
            return
        L_0x00fb:
            com.kochava.base.a$c r1 = r9.f
            r1.a()
            com.kochava.base.i r1 = r9.a
            com.kochava.base.d r1 = r1.d
            java.lang.String r4 = "push"
            java.lang.Object r1 = r1.b(r4)
            boolean r1 = com.kochava.base.x.a(r1, r2)
            if (r1 == 0) goto L_0x014e
            com.kochava.base.i r1 = r9.a
            com.kochava.base.d r1 = r1.d
            java.lang.String r4 = "push_token"
            java.lang.Object r1 = r1.b(r4)
            java.lang.String r1 = com.kochava.base.x.a(r1)
            com.kochava.base.i r4 = r9.a
            com.kochava.base.d r4 = r4.d
            java.lang.String r5 = "push_token_enable"
            java.lang.Object r4 = r4.b(r5)
            java.lang.Boolean r4 = com.kochava.base.x.b(r4)
            com.kochava.base.i r5 = r9.a
            com.kochava.base.d r5 = r5.d
            java.lang.String r6 = "push_token_sent"
            java.lang.Object r5 = r5.b(r6)
            boolean r5 = com.kochava.base.x.a(r5, r2)
            if (r1 == 0) goto L_0x014e
            if (r4 == 0) goto L_0x014e
            if (r5 != 0) goto L_0x014e
            com.kochava.base.i r5 = r9.a
            com.kochava.base.t r6 = new com.kochava.base.t
            boolean r4 = r4.booleanValue()
            r6.<init>(r5, r1, r4, r0)
            r5.a(r6, r0)
        L_0x014e:
            com.kochava.base.w r1 = r9.c
            boolean r1 = r1.e()
            if (r1 != 0) goto L_0x0186
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.String r4 = "UPDATE"
            r1[r2] = r4
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            com.kochava.base.Tracker.a(r3, r4, r5, r1)
            com.kochava.base.w r1 = r9.c
            boolean r1 = r1.g()
            if (r1 != 0) goto L_0x0178
            com.kochava.base.w r1 = r9.c
            r1.f()
            com.kochava.base.i r1 = r9.a
            com.kochava.base.w r2 = r9.c
            r1.a(r2, r0)
            goto L_0x0185
        L_0x0178:
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r1 = "UPDATE SKIP"
            r0[r2] = r1
            java.lang.String r1 = "CTR"
            java.lang.String r2 = "Controller"
            com.kochava.base.Tracker.a(r3, r1, r2, r0)
        L_0x0185:
            return
        L_0x0186:
            com.kochava.base.p r1 = r9.d
            boolean r1 = r1.e()
            if (r1 != 0) goto L_0x01be
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.String r4 = "INITIAL"
            r1[r2] = r4
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            com.kochava.base.Tracker.a(r3, r4, r5, r1)
            com.kochava.base.p r1 = r9.d
            boolean r1 = r1.g()
            if (r1 != 0) goto L_0x01b0
            com.kochava.base.p r1 = r9.d
            r1.f()
            com.kochava.base.i r1 = r9.a
            com.kochava.base.p r2 = r9.d
            r1.a(r2, r0)
            goto L_0x01bd
        L_0x01b0:
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r1 = "INITIAL SKIP"
            r0[r2] = r1
            java.lang.String r1 = "CTR"
            java.lang.String r2 = "Controller"
            com.kochava.base.Tracker.a(r3, r1, r2, r0)
        L_0x01bd:
            return
        L_0x01be:
            java.util.List<com.kochava.base.m> r1 = r9.h
            monitor-enter(r1)
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            java.lang.Object[] r6 = new java.lang.Object[r0]     // Catch:{ all -> 0x029c }
            java.lang.String r7 = "ATTRIBUTION_QUEUE"
            r6[r2] = r7     // Catch:{ all -> 0x029c }
            com.kochava.base.Tracker.a(r3, r4, r5, r6)     // Catch:{ all -> 0x029c }
        L_0x01ce:
            java.util.List<com.kochava.base.m> r4 = r9.h     // Catch:{ all -> 0x029c }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x029c }
            if (r4 != 0) goto L_0x0220
            java.util.List<com.kochava.base.m> r4 = r9.h     // Catch:{ all -> 0x029c }
            java.lang.Object r4 = r4.get(r2)     // Catch:{ all -> 0x029c }
            com.kochava.base.m r4 = (com.kochava.base.m) r4     // Catch:{ all -> 0x029c }
            boolean r5 = r4.e()     // Catch:{ all -> 0x029c }
            if (r5 == 0) goto L_0x01f7
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            java.lang.Object[] r6 = new java.lang.Object[r0]     // Catch:{ all -> 0x029c }
            java.lang.String r7 = "ATTRIBUTION_QUEUE Item Complete"
            r6[r2] = r7     // Catch:{ all -> 0x029c }
            com.kochava.base.Tracker.a(r3, r4, r5, r6)     // Catch:{ all -> 0x029c }
            java.util.List<com.kochava.base.m> r4 = r9.h     // Catch:{ all -> 0x029c }
            r4.remove(r2)     // Catch:{ all -> 0x029c }
            goto L_0x01ce
        L_0x01f7:
            boolean r5 = r4.g()     // Catch:{ all -> 0x029c }
            if (r5 == 0) goto L_0x020b
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            java.lang.Object[] r6 = new java.lang.Object[r0]     // Catch:{ all -> 0x029c }
            java.lang.String r7 = "ATTRIBUTION_QUEUE Item Processing"
            r6[r2] = r7     // Catch:{ all -> 0x029c }
            com.kochava.base.Tracker.a(r3, r4, r5, r6)     // Catch:{ all -> 0x029c }
            goto L_0x0220
        L_0x020b:
            java.lang.String r5 = "CTR"
            java.lang.String r6 = "Controller"
            java.lang.Object[] r7 = new java.lang.Object[r0]     // Catch:{ all -> 0x029c }
            java.lang.String r8 = "ATTRIBUTION_QUEUE Item Start"
            r7[r2] = r8     // Catch:{ all -> 0x029c }
            com.kochava.base.Tracker.a(r3, r5, r6, r7)     // Catch:{ all -> 0x029c }
            r4.f()     // Catch:{ all -> 0x029c }
            com.kochava.base.i r5 = r9.a     // Catch:{ all -> 0x029c }
            r5.a(r4, r2)     // Catch:{ all -> 0x029c }
        L_0x0220:
            java.util.List<com.kochava.base.m> r4 = r9.h     // Catch:{ all -> 0x029c }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x029c }
            if (r4 != 0) goto L_0x022a
            monitor-exit(r1)     // Catch:{ all -> 0x029c }
            return
        L_0x022a:
            monitor-exit(r1)     // Catch:{ all -> 0x029c }
            com.kochava.base.s r1 = r9.g
            if (r1 == 0) goto L_0x0264
            boolean r1 = r1.e()
            if (r1 != 0) goto L_0x0264
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.String r4 = "LOCATION_TRACKER"
            r1[r2] = r4
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            com.kochava.base.Tracker.a(r3, r4, r5, r1)
            com.kochava.base.s r1 = r9.g
            boolean r1 = r1.g()
            if (r1 != 0) goto L_0x0257
            com.kochava.base.s r1 = r9.g
            r1.f()
            com.kochava.base.i r1 = r9.a
            com.kochava.base.s r4 = r9.g
            r1.a(r4, r2)
            goto L_0x0264
        L_0x0257:
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.String r4 = "LOCATION_TRACKER SKIP"
            r1[r2] = r4
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            com.kochava.base.Tracker.a(r3, r4, r5, r1)
        L_0x0264:
            com.kochava.base.u r1 = r9.e
            boolean r1 = r1.e()
            if (r1 != 0) goto L_0x029b
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.String r4 = "QUEUE"
            r1[r2] = r4
            java.lang.String r4 = "CTR"
            java.lang.String r5 = "Controller"
            com.kochava.base.Tracker.a(r3, r4, r5, r1)
            com.kochava.base.u r1 = r9.e
            boolean r1 = r1.g()
            if (r1 != 0) goto L_0x028e
            com.kochava.base.u r0 = r9.e
            r0.f()
            com.kochava.base.i r0 = r9.a
            com.kochava.base.u r1 = r9.e
            r0.a(r1, r2)
            goto L_0x029b
        L_0x028e:
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r1 = "QUEUE SKIP"
            r0[r2] = r1
            java.lang.String r1 = "CTR"
            java.lang.String r2 = "Controller"
            com.kochava.base.Tracker.a(r3, r1, r2, r0)
        L_0x029b:
            return
        L_0x029c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x029c }
            goto L_0x02a0
        L_0x029f:
            throw r0
        L_0x02a0:
            goto L_0x029f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.a.run():void");
    }
}
