package com.kochava.base;

import bolts.MeasurementEvent;
import com.applovin.sdk.AppLovinEventParameters;
import com.unity3d.services.purchasing.core.TransactionDetailsUtilities;
import org.json.JSONArray;
import org.json.JSONObject;

final class l extends j {
    private final int b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;

    l(i iVar, int i, String str, String str2, String str3, String str4) {
        super(iVar, true);
        this.b = i;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, int]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    private boolean b(JSONObject jSONObject) {
        int i = this.b;
        if (i != 2 && i != 3) {
            return true;
        }
        int b2 = x.b(this.a.d.b("session_minimum"), 45);
        int b3 = x.b(this.a.d.b("session_window"), 600);
        if (b3 == 0 || b2 == 0) {
            return true;
        }
        int c2 = (int) (c() / 1000);
        int b4 = x.b(this.a.d.b("session_resume_time"), 0);
        boolean a = x.a(this.a.d.b("session_pause_sent_this_window"), false);
        boolean a2 = x.a(this.a.d.b("session_pause_ever_sent"), false);
        int i2 = b2;
        boolean z = a;
        if (this.b == 2) {
            this.a.d.a("session_state_active_count", Integer.valueOf(x.b(this.a.d.b("session_state_active_count"), 0) + 1));
            if (c2 < b4 + b3) {
                return false;
            }
            Tracker.a(4, "TBE", "processSessio", "Resume: Sufficient Time");
            JSONObject f2 = x.f(this.a.d.b("session_pause"));
            if (f2 != null) {
                Tracker.a(4, "TBE", "processSessio", "Resume: Queuing Cached Pause");
                this.a.d.a(f2);
                this.a.d.a("session_pause");
                this.a.d.a("session_pause_ever_sent", (Object) true);
                this.a.d.a("session_state_active_count", (Object) 1);
            }
            this.a.d.a("session_resume_time", Integer.valueOf(c2));
            this.a.d.a("session_window_uptime", (Object) 0);
            this.a.d.a("session_pause_sent_this_window", (Object) false);
            return true;
        }
        String str = "session_state_active_count";
        int b5 = (int) (((long) x.b(this.a.d.b("session_window_uptime"), 0)) + ((c() - a()) / 1000));
        this.a.d.a("session_window_uptime", Integer.valueOf(b5));
        if (!a2 || (!z && (b5 >= i2 || c2 >= b4 + b3))) {
            Tracker.a(4, "TBE", "processSessio", "Pause: Sending");
            this.a.d.a("session_pause");
            this.a.d.a("session_pause_ever_sent", (Object) true);
            this.a.d.a("session_pause_sent_this_window", (Object) true);
            this.a.d.a(str, (Object) 0);
            return true;
        } else if (!z) {
            Tracker.a(4, "TBE", "processSessio", "Pause: Updating");
            this.a.d.a("session_pause", jSONObject);
            return false;
        } else {
            Tracker.a(4, "TBE", "processSessio", "Pause: Not Updating");
            return false;
        }
    }

    /* JADX INFO: additional move instructions added (5) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    public final void run() {
        Object obj;
        Tracker.a(4, "TBE", "run", new Object[0]);
        if (this.a.d.e() >= 500) {
            Tracker.a(2, "TBE", "run", "Database Full. Dropping: " + this.c);
            return;
        }
        JSONArray g = x.g(this.a.d.b("eventname_blacklist"));
        String str = this.c;
        if (str == null || !x.a(g, str)) {
            JSONObject jSONObject = new JSONObject();
            String str2 = this.c;
            if (str2 != null) {
                x.a(MeasurementEvent.MEASUREMENT_EVENT_NAME_KEY, str2, jSONObject);
            }
            String str3 = this.d;
            if (str3 != null) {
                JSONObject jSONObject2 = str3.isEmpty() ? new JSONObject() : x.f(this.d);
                if (jSONObject2 != null) {
                    obj = jSONObject2;
                    if ("Consent Granted".equals(this.c)) {
                        obj = jSONObject2;
                        if (this.a.m) {
                            if (jSONObject2.opt(AppLovinEventParameters.CONTENT_IDENTIFIER) == null) {
                                x.a(AppLovinEventParameters.CONTENT_IDENTIFIER, this.a.g.g(), jSONObject2);
                            }
                            obj = jSONObject2;
                            if (this.a.g.e()) {
                                Object opt = jSONObject2.opt("date");
                                obj = jSONObject2;
                                if (opt == null) {
                                    x.a("date", Long.toString(this.a.g.f()), jSONObject2);
                                    obj = jSONObject2;
                                }
                            }
                        }
                    }
                } else {
                    obj = this.d;
                }
                x.a("event_data", obj, jSONObject);
            }
            if (!(this.e == null || this.f == null)) {
                JSONObject jSONObject3 = new JSONObject();
                x.a("purchaseData", this.e, jSONObject3);
                x.a("dataSignature", this.f, jSONObject3);
                x.a(TransactionDetailsUtilities.RECEIPT, jSONObject3, jSONObject);
            }
            JSONObject jSONObject4 = new JSONObject();
            a(this.b, jSONObject4, jSONObject);
            boolean b2 = b(jSONObject4);
            if (b2) {
                this.a.d.a(jSONObject4);
            } else {
                Tracker.a(4, "TBE", "run", "Not sending deferred/dropped event.");
            }
            if (b2) {
                j();
            }
            Tracker.a(4, "TBE", "run", "Complete");
            return;
        }
        Tracker.a(3, "TBE", "run", this.c + " blacklisted");
    }
}
