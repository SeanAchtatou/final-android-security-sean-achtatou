package com.kochava.base;

public interface AttributionUpdateListener {
    void onAttributionUpdated(String str);
}
