package com.kochava.base;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import com.ironsource.sdk.constants.Constants;
import com.kochava.base.Tracker;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

final class x {
    static double a(double d, double d2, double d3) {
        return d < d2 ? d2 : d > d3 ? d3 : d;
    }

    static double a(long j) {
        double d = (double) j;
        Double.isNaN(d);
        return d / 1000.0d;
    }

    static double a(Object obj, double d) {
        Double e = e(obj);
        return e != null ? e.doubleValue() : d;
    }

    static int a(int i, int i2, int i3) {
        return i < i2 ? i2 : i > i3 ? i3 : i;
    }

    static int a(Object obj, int i) {
        if (obj instanceof Integer) {
            int intValue = ((Integer) obj).intValue();
            return (intValue < 0 || intValue > 5) ? i : intValue;
        } else if (obj instanceof String) {
            String str = (String) obj;
            if ("NONE".equalsIgnoreCase(str)) {
                return 0;
            }
            if ("ERROR".equalsIgnoreCase(str)) {
                return 1;
            }
            if ("WARN".equalsIgnoreCase(str)) {
                return 2;
            }
            if ("INFO".equalsIgnoreCase(str)) {
                return 3;
            }
            if ("DEBUG".equalsIgnoreCase(str)) {
                return 4;
            }
            if ("TRACE".equalsIgnoreCase(str)) {
                return 5;
            }
        }
    }

    static long a(double d) {
        return Math.round(d * 1000.0d);
    }

    static long a(long j, long j2, long j3) {
        return j < j2 ? j2 : j > j3 ? j3 : j;
    }

    public static long a(Object obj, long j) {
        Long d = d(obj);
        return d != null ? d.longValue() : j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        throw new java.io.IOException("Failed to read input stream");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r2.close();
        r1.close();
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0041, code lost:
        throw r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x002f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.io.InputStream r4, boolean r5) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.io.InputStreamReader r1 = new java.io.InputStreamReader
            java.nio.charset.Charset r2 = a()
            r1.<init>(r4, r2)
            java.io.BufferedReader r2 = new java.io.BufferedReader
            r2.<init>(r1)
        L_0x0013:
            java.lang.String r3 = r2.readLine()     // Catch:{ IOException -> 0x002f }
            if (r3 == 0) goto L_0x001f
            if (r5 == 0) goto L_0x0013
            r0.append(r3)     // Catch:{ IOException -> 0x002f }
            goto L_0x0013
        L_0x001f:
            r2.close()     // Catch:{ IOException -> 0x0028 }
            r1.close()     // Catch:{ IOException -> 0x0028 }
            r4.close()     // Catch:{ IOException -> 0x0028 }
        L_0x0028:
            java.lang.String r4 = r0.toString()
            return r4
        L_0x002d:
            r5 = move-exception
            goto L_0x0037
        L_0x002f:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x002d }
            java.lang.String r0 = "Failed to read input stream"
            r5.<init>(r0)     // Catch:{ all -> 0x002d }
            throw r5     // Catch:{ all -> 0x002d }
        L_0x0037:
            r2.close()     // Catch:{ IOException -> 0x0040 }
            r1.close()     // Catch:{ IOException -> 0x0040 }
            r4.close()     // Catch:{ IOException -> 0x0040 }
        L_0x0040:
            goto L_0x0042
        L_0x0041:
            throw r5
        L_0x0042:
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String");
    }

    static String a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if ((obj instanceof JSONObject) || (obj instanceof JSONArray)) {
            return obj.toString();
        }
        return null;
    }

    static String a(Object obj, String str) {
        String a = a(obj);
        return a != null ? a : str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.net.HttpURLConnection
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.net.HttpURLConnection */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
     arg types: [java.io.InputStream, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String */
    static String a(String str, String str2) {
        try {
            String f = f();
            Tracker.a(4, "UTL", "httpPost", "SEND>", f, str, str2, "<SEND");
            long b = b();
            HttpURLConnection a = a(str, "POST", f, true);
            a.connect();
            a(a.getOutputStream(), str2);
            String a2 = a(a.getInputStream(), true);
            a.disconnect();
            Tracker.a(4, "UTL", "httpPost", "RECEIVE>", f, str, a2, "<RECEIVE");
            Object[] objArr = new Object[1];
            StringBuilder sb = new StringBuilder();
            sb.append("Post Duration: ");
            double b2 = (double) (b() - b);
            Double.isNaN(b2);
            sb.append(b2 / 1000.0d);
            sb.append("s to URL: ");
            sb.append(str);
            objArr[0] = sb.toString();
            Tracker.a(5, "UTL", "httpPost", objArr);
            return a2;
        } catch (Throwable th) {
            Tracker.a(4, "UTL", "httpPost", th);
            throw new IOException(th.getMessage());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.net.HttpURLConnection
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.net.HttpURLConnection */
    static String a(String str, boolean z) {
        try {
            String f = f();
            Tracker.a(4, "UTL", "httpGet", "SEND>", f, str, "<SEND");
            long b = b();
            HttpURLConnection a = a(str, "GET", f, false);
            a.connect();
            String a2 = a(a.getInputStream(), z);
            a.disconnect();
            Tracker.a(4, "UTL", "httpGet", "RECEIVE>", f, str, a2, "<RECEIVE");
            Object[] objArr = new Object[1];
            StringBuilder sb = new StringBuilder();
            sb.append("Get Duration: ");
            double b2 = (double) (b() - b);
            Double.isNaN(b2);
            sb.append(b2 / 1000.0d);
            sb.append("s to URL: ");
            sb.append(str);
            objArr[0] = sb.toString();
            Tracker.a(5, "UTL", "httpGet", objArr);
            return a2;
        } catch (Throwable th) {
            Tracker.a(4, "UTL", "httpGet", th);
            throw new IOException(th.getMessage());
        }
    }

    static String a(JSONArray jSONArray) {
        String str;
        try {
            str = jSONArray.toString();
        } catch (Throwable th) {
            Tracker.a(2, "UTL", "jsonArrayToSt", th);
            str = null;
        }
        return str == null ? "[]" : str;
    }

    static String a(JSONObject jSONObject) {
        String str;
        try {
            str = jSONObject.toString();
        } catch (Throwable th) {
            Tracker.a(2, "UTL", "jsonObjectToS", th);
            str = null;
        }
        return str == null ? "{}" : str;
    }

    private static HttpURLConnection a(String str, String str2, String str3, boolean z) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) ((URLConnection) FirebasePerfUrlConnection.instrument(new URL(str).openConnection()));
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.setRequestMethod(str2);
        if (!str3.trim().isEmpty()) {
            httpURLConnection.setRequestProperty("User-Agent", str3);
        }
        httpURLConnection.setDoInput(true);
        if (z) {
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=" + a().name());
        }
        return httpURLConnection;
    }

    static Charset a() {
        return Charset.isSupported("UTF-8") ? Charset.forName("UTF-8") : Charset.defaultCharset();
    }

    static Map<String, String> a(String str) {
        int i;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (str == null || str.isEmpty()) {
            return linkedHashMap;
        }
        try {
            for (String str2 : str.split(Constants.RequestParameters.AMPERSAND)) {
                int indexOf = str2.indexOf(Constants.RequestParameters.EQUAL);
                String decode = indexOf > 0 ? URLDecoder.decode(str2.substring(0, indexOf), a().name()) : str2;
                if (!linkedHashMap.containsKey(decode)) {
                    String decode2 = (indexOf <= 0 || str2.length() <= (i = indexOf + 1)) ? null : URLDecoder.decode(str2.substring(i), a().name());
                    if (decode2 != null) {
                        linkedHashMap.put(decode, decode2);
                    }
                }
            }
            return linkedHashMap;
        } catch (Throwable unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r0.close();
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
        throw new java.io.IOException("Failed to write output stream");
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0019 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.io.OutputStream r2, java.lang.String r3) {
        /*
            java.nio.charset.Charset r0 = a()
            byte[] r3 = r3.getBytes(r0)
            java.io.BufferedOutputStream r0 = new java.io.BufferedOutputStream
            r0.<init>(r2)
            r0.write(r3)     // Catch:{ IOException -> 0x0019 }
            r0.close()     // Catch:{ IOException -> 0x0016 }
            r2.close()     // Catch:{ IOException -> 0x0016 }
        L_0x0016:
            return
        L_0x0017:
            r3 = move-exception
            goto L_0x0021
        L_0x0019:
            java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x0017 }
            java.lang.String r1 = "Failed to write output stream"
            r3.<init>(r1)     // Catch:{ all -> 0x0017 }
            throw r3     // Catch:{ all -> 0x0017 }
        L_0x0021:
            r0.close()     // Catch:{ IOException -> 0x0027 }
            r2.close()     // Catch:{ IOException -> 0x0027 }
        L_0x0027:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void");
    }

    static void a(Object obj, JSONArray jSONArray, boolean z) {
        if (obj != null) {
            if (!z) {
                int i = 0;
                while (i < jSONArray.length()) {
                    try {
                        if (!obj.equals(jSONArray.opt(i))) {
                            i++;
                        } else {
                            return;
                        }
                    } catch (Throwable th) {
                        Tracker.a(4, "UTL", "putJsonObject", obj, th);
                        return;
                    }
                }
            }
            jSONArray.put(obj);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject, boolean):void
     arg types: [java.lang.String, java.lang.Object, org.json.JSONObject, int]
     candidates:
      com.kochava.base.x.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.net.HttpURLConnection
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject, boolean):void */
    static void a(String str, Object obj, JSONObject jSONObject) {
        a(str, obj, jSONObject, true);
    }

    /* JADX INFO: additional move instructions added (4) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    static void a(String str, Object obj, JSONObject jSONObject, boolean z) {
        if (str != null && obj != null && !str.trim().isEmpty()) {
            try {
                boolean z2 = obj instanceof Boolean;
                String str2 = obj;
                if (!z2) {
                    boolean z3 = obj instanceof Number;
                    str2 = obj;
                    if (!z3) {
                        boolean z4 = obj instanceof JSONObject;
                        str2 = obj;
                        if (!z4) {
                            if (obj instanceof JSONArray) {
                                str2 = obj;
                            } else if (obj instanceof String) {
                                String str3 = (String) obj;
                                str2 = str3;
                                if (!z) {
                                    boolean isEmpty = str3.trim().isEmpty();
                                    str2 = str3;
                                    if (isEmpty) {
                                        return;
                                    }
                                }
                            } else if (obj instanceof Date) {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
                                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                str2 = simpleDateFormat.format((Date) obj);
                            } else {
                                if (!(obj instanceof Bundle)) {
                                    if (!(obj instanceof Map)) {
                                        if (!(obj instanceof Collection)) {
                                            if (!obj.getClass().isArray()) {
                                                str2 = obj.toString();
                                            }
                                        }
                                        str2 = g(obj);
                                    }
                                }
                                str2 = f(obj);
                            }
                        }
                    }
                }
                jSONObject.put(str, str2);
            } catch (Throwable unused) {
            }
        }
    }

    static void a(JSONObject jSONObject, JSONObject jSONObject2, boolean z) {
        Iterator<String> keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            Object opt = jSONObject2.opt(next);
            if (opt != null) {
                a(next, opt, jSONObject, z);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    static void a(JSONObject jSONObject, boolean z) {
        int c = c();
        a(Tracker.ConsentPartner.KEY_GRANTED, Boolean.valueOf(z), jSONObject);
        a(Tracker.ConsentPartner.KEY_RESPONSE_TIME, Integer.valueOf(c), jSONObject);
        JSONArray c2 = c(jSONObject.opt(Tracker.ConsentPartner.KEY_PARTNERS), true);
        for (int i = 0; i < c2.length(); i++) {
            JSONObject b = b(c2.opt(i), true);
            if (!z || !a(b.opt(Tracker.ConsentPartner.KEY_GRANTED), false)) {
                a(Tracker.ConsentPartner.KEY_GRANTED, Boolean.valueOf(z), b);
                a(Tracker.ConsentPartner.KEY_RESPONSE_TIME, Integer.valueOf(c), b);
            }
        }
    }

    static boolean a(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        String packageName = context.getPackageName();
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager == null || packageName == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null || runningAppProcesses.size() == 0) {
            return true;
        }
        Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
        while (true) {
            if (!it.hasNext()) {
                return false;
            }
            ActivityManager.RunningAppProcessInfo next = it.next();
            if (next != null && next.importance == 100) {
                for (String equals : next.pkgList) {
                    if (packageName.equals(equals)) {
                        return true;
                    }
                }
                continue;
            }
        }
    }

    static boolean a(Context context, String str) {
        if (!b(context, str)) {
            return false;
        }
        return Build.VERSION.SDK_INT < 23 || context.checkSelfPermission(str) == 0;
    }

    static boolean a(Object obj, Object obj2) {
        return (!(obj instanceof String) || !(obj2 instanceof String)) ? (!(obj instanceof JSONObject) || !(obj2 instanceof JSONObject)) ? (!(obj instanceof JSONArray) || !(obj2 instanceof JSONArray)) ? (!(obj instanceof Boolean) || !(obj2 instanceof Boolean)) ? (!(obj instanceof Integer) || !(obj2 instanceof Integer)) ? (!(obj instanceof Long) || !(obj2 instanceof Long)) ? (!(obj instanceof Float) || !(obj2 instanceof Float)) ? (!(obj instanceof Double) || !(obj2 instanceof Double)) ? (obj instanceof Number) && (obj2 instanceof Number) && Math.abs(((Number) obj).doubleValue() - ((Number) obj2).doubleValue()) < 1.0E-6d : Math.abs(((Double) obj).doubleValue() - ((Double) obj2).doubleValue()) < 1.0E-6d : Math.abs(((Float) obj).floatValue() - ((Float) obj2).floatValue()) < 1.0E-5f : obj.equals(obj2) : obj.equals(obj2) : obj.equals(obj2) : a((JSONArray) obj, (JSONArray) obj2) : b((JSONObject) obj, (JSONObject) obj2) : obj.equals(obj2);
    }

    static boolean a(Object obj, boolean z) {
        Boolean b = b(obj);
        return b != null ? b.booleanValue() : z;
    }

    static boolean a(JSONArray jSONArray, String str) {
        if (!(jSONArray == null || str == null)) {
            for (int i = 0; i < jSONArray.length(); i++) {
                if (str.equalsIgnoreCase(a(jSONArray.opt(i)))) {
                    return true;
                }
            }
        }
        return false;
    }

    static boolean a(JSONArray jSONArray, JSONArray jSONArray2) {
        boolean z;
        if (jSONArray == null || jSONArray2 == null || jSONArray.length() != jSONArray2.length()) {
            return false;
        }
        if (jSONArray.length() == 0) {
            return true;
        }
        boolean[] zArr = new boolean[jSONArray.length()];
        for (int i = 0; i < jSONArray.length(); i++) {
            Object opt = jSONArray.opt(i);
            int i2 = 0;
            while (true) {
                if (i2 < jSONArray2.length()) {
                    if (!zArr[i2] && a(opt, jSONArray2.opt(i2))) {
                        zArr[i2] = true;
                        z = true;
                        break;
                    }
                    i2++;
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    static boolean a(JSONObject jSONObject, JSONObject jSONObject2) {
        Iterator<String> keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (!a(jSONObject2.opt(next), jSONObject.opt(next))) {
                return false;
            }
        }
        return true;
    }

    static boolean a(int[] iArr, int i) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    static int b(Object obj, int i) {
        Integer c = c(obj);
        return c != null ? c.intValue() : i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
     arg types: [java.lang.String, boolean, org.json.JSONObject]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void
     arg types: [java.lang.String, long, org.json.JSONObject]
     candidates:
      com.kochava.base.x.a(double, double, double):double
      com.kochava.base.x.a(int, int, int):int
      com.kochava.base.x.a(long, long, long):long
      com.kochava.base.x.a(java.lang.Object, org.json.JSONArray, boolean):void
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject, boolean):void
      com.kochava.base.x.a(java.lang.String, java.lang.Object, org.json.JSONObject):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, long):long
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, long):long */
    static int b(JSONArray jSONArray, JSONArray jSONArray2) {
        boolean z = false;
        for (int i = 0; i < jSONArray2.length(); i++) {
            JSONObject b = b(jSONArray2.opt(i), true);
            JSONObject b2 = b(jSONArray, a(b.opt("name"), ""));
            if (b2 == null) {
                a(Tracker.ConsentPartner.KEY_GRANTED, (Object) false, b);
                a(Tracker.ConsentPartner.KEY_RESPONSE_TIME, (Object) 0L, b);
                z = true;
            } else {
                a(Tracker.ConsentPartner.KEY_GRANTED, Boolean.valueOf(a(b2.opt(Tracker.ConsentPartner.KEY_GRANTED), false)), b);
                a(Tracker.ConsentPartner.KEY_RESPONSE_TIME, Long.valueOf(a(b2.opt(Tracker.ConsentPartner.KEY_RESPONSE_TIME), 0L)), b);
            }
        }
        if (z) {
            return 2;
        }
        return jSONArray.length() != jSONArray2.length() ? 1 : 0;
    }

    static long b() {
        return System.currentTimeMillis();
    }

    static Boolean b(Object obj) {
        if (obj instanceof Boolean) {
            return (Boolean) obj;
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (Boolean.toString(true).equalsIgnoreCase(str) || Integer.toString(1).equalsIgnoreCase(str)) {
                return true;
            }
            if (Boolean.toString(false).equalsIgnoreCase(str) || Integer.toString(0).equalsIgnoreCase(str)) {
                return false;
            }
        }
        if (!(obj instanceof Integer)) {
            return null;
        }
        Integer num = (Integer) obj;
        if (1 == num.intValue()) {
            return true;
        }
        return num.intValue() == 0 ? false : null;
    }

    static JSONObject b(Object obj, boolean z) {
        JSONObject f = f(obj);
        return (f != null || !z) ? f : new JSONObject();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.b(java.lang.Object, int):int
      com.kochava.base.x.b(org.json.JSONArray, org.json.JSONArray):int
      com.kochava.base.x.b(org.json.JSONArray, java.lang.String):org.json.JSONObject
      com.kochava.base.x.b(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.b(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.b(java.lang.Object, boolean):org.json.JSONObject */
    static JSONObject b(JSONArray jSONArray, String str) {
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject b = b(jSONArray.opt(i), true);
            if (str.equals(a(b.opt("name")))) {
                return b;
            }
        }
        return null;
    }

    static boolean b(Context context) {
        NetworkInfo activeNetworkInfo;
        if (!b(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return true;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            return (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnectedOrConnecting()) ? false : true;
        } catch (Throwable th) {
            Tracker.a(4, "UTL", "hasNetworkCon", th);
            return true;
        }
    }

    static boolean b(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    static boolean b(JSONObject jSONObject, JSONObject jSONObject2) {
        if (jSONObject == null || jSONObject2 == null || jSONObject.length() != jSONObject2.length()) {
            return false;
        }
        if (jSONObject.length() == 0) {
            return true;
        }
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (!a(jSONObject.opt(next), jSONObject2.opt(next))) {
                return false;
            }
        }
        return true;
    }

    static int c() {
        return (int) (b() / 1000);
    }

    static Integer c(Object obj) {
        if (obj instanceof Number) {
            return Integer.valueOf(((Number) obj).intValue());
        }
        if (!(obj instanceof String)) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt((String) obj));
        } catch (Throwable unused) {
            return null;
        }
    }

    static JSONArray c(Object obj, boolean z) {
        JSONArray g = g(obj);
        return (g != null || !z) ? g : new JSONArray();
    }

    static long d() {
        return SystemClock.elapsedRealtime();
    }

    public static Long d(Object obj) {
        if (obj instanceof Number) {
            return Long.valueOf(((Number) obj).longValue());
        }
        if (!(obj instanceof String)) {
            return null;
        }
        try {
            return Long.valueOf(Long.parseLong((String) obj));
        } catch (Throwable unused) {
            return null;
        }
    }

    static Double e(Object obj) {
        if (obj instanceof Number) {
            return Double.valueOf(((Number) obj).doubleValue());
        }
        if (!(obj instanceof String)) {
            return null;
        }
        try {
            return Double.valueOf(Double.parseDouble((String) obj));
        } catch (Throwable unused) {
            return null;
        }
    }

    static boolean e() {
        return Thread.currentThread().equals(Looper.getMainLooper().getThread());
    }

    private static String f() {
        try {
            String property = System.getProperty("http.agent");
            return property == null ? "" : property;
        } catch (Exception e) {
            Tracker.a(4, "UTL", "post", e);
            return "";
        }
    }

    static JSONObject f(Object obj) {
        if (obj instanceof JSONObject) {
            return (JSONObject) obj;
        }
        if (obj instanceof Bundle) {
            Bundle bundle = (Bundle) obj;
            JSONObject jSONObject = new JSONObject();
            for (String next : bundle.keySet()) {
                a(next, bundle.get(next), jSONObject);
            }
            return jSONObject;
        }
        try {
            if (obj instanceof String) {
                return new JSONObject((String) obj);
            }
            if (obj instanceof Map) {
                return new JSONObject((Map) obj);
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    static JSONArray g(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof JSONArray) {
            return (JSONArray) obj;
        }
        if (obj instanceof Collection) {
            return new JSONArray((Collection) obj);
        }
        try {
            if (obj instanceof String) {
                return new JSONArray((String) obj);
            }
            if (obj.getClass().isArray()) {
                JSONArray jSONArray = new JSONArray();
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    jSONArray.put(Array.get(obj, i));
                }
                return jSONArray;
            }
            return null;
        } catch (Throwable unused) {
        }
    }
}
