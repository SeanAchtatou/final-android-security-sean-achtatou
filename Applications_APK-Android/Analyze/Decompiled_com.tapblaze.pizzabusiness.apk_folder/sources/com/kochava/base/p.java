package com.kochava.base;

import org.json.JSONObject;

final class p extends j {
    p(i iVar) {
        super(iVar, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
     arg types: [java.lang.Object, int]
     candidates:
      com.kochava.base.x.a(java.lang.Object, double):double
      com.kochava.base.x.a(java.lang.Object, int):int
      com.kochava.base.x.a(java.lang.Object, long):long
      com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
      com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
      com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
      com.kochava.base.x.a(org.json.JSONObject, boolean):void
      com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
      com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
      com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
      com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
      com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
      com.kochava.base.x.a(int[], int):boolean
      com.kochava.base.x.a(java.lang.Object, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
     arg types: [int, org.json.JSONObject]
     candidates:
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.j.a(org.json.JSONObject, boolean):boolean
     arg types: [org.json.JSONObject, int]
     candidates:
      com.kochava.base.j.a(int, org.json.JSONObject):void
      com.kochava.base.j.a(android.content.Context, org.json.JSONObject):void
      com.kochava.base.j.a(com.kochava.base.d, org.json.JSONObject):void
      com.kochava.base.j.a(org.json.JSONObject, com.kochava.base.d):void
      com.kochava.base.j.a(java.lang.String, boolean):java.lang.String
      com.kochava.base.j.a(int, java.lang.Object):org.json.JSONObject
      com.kochava.base.j.a(org.json.JSONObject, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, int]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kochava.base.d.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.kochava.base.d.a(android.content.Context, boolean):void
      com.kochava.base.d.a(java.lang.String, java.lang.Object):void */
    public final void run() {
        Tracker.a(4, "TIL", "run", new Object[0]);
        if (!x.a(this.a.d.b("initial_needs_sent"), true)) {
            Tracker.a(4, "TIL", "run", "Skip");
            d();
        } else {
            JSONObject f = x.f(this.a.d.b("initial_data"));
            if (f == null) {
                Tracker.a(5, "TIL", "run", "Gather");
                f = new JSONObject();
                JSONObject jSONObject = new JSONObject();
                a(1, f, jSONObject);
                int b = x.b(this.a.d.b("first_launch_time"), 0);
                if (2592000 + b > x.c()) {
                    x.a("usertime", Integer.valueOf(b), jSONObject);
                }
                this.a.d.a("initial_data", f);
            }
            if (!n()) {
                JSONObject a = a(1, (Object) f);
                if (!a(a, true)) {
                    Tracker.a(5, "TIL", "run", a);
                    if (!x.a(this.a.d.b("initial_ever_sent"), false)) {
                        this.a.d.a("session_resume_time", Integer.valueOf(x.c()));
                        this.a.d.a("session_state_active_count", (Object) 1);
                    }
                    this.a.d.a("initial_data");
                    this.a.d.a("initial_ever_sent", (Object) true);
                    this.a.d.a("initial_needs_sent", (Object) false);
                    this.a.d.a("initial_sent_time", Integer.valueOf(x.c()));
                    d();
                    Tracker.a(3, "TIL", "initial", "Complete");
                    Tracker.a(4, "TIL", "run", "Complete");
                } else {
                    return;
                }
            } else {
                return;
            }
        }
        i();
    }
}
