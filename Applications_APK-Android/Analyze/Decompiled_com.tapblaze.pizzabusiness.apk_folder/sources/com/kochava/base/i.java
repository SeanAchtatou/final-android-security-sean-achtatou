package com.kochava.base;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

final class i {
    final Context a;
    final List<String> b = Collections.synchronizedList(new ArrayList());
    final JSONObject c;
    final d d;
    final ConsentStatusChangeListener e;
    final Runnable f;
    final b g;
    final Handler h;
    final Handler i;
    final HandlerThread j = new HandlerThread("EventThread");
    final HandlerThread k = new HandlerThread("ControllerThread");
    final String l;
    final boolean m;
    final boolean n;
    final String o;
    final long p = x.b();
    final boolean q;
    volatile boolean r = false;
    volatile long s = x.b();
    boolean t = true;
    final f u = new f();

    i(Context context, Runnable runnable, b bVar, JSONObject jSONObject, ConsentStatusChangeListener consentStatusChangeListener, boolean z, boolean z2, String str) {
        this.a = context;
        this.f = runnable;
        this.g = bVar;
        this.c = jSONObject;
        this.e = consentStatusChangeListener;
        this.m = z;
        this.n = z2;
        this.o = str;
        this.d = new d(context, z);
        AnonymousClass1 r4 = new Thread.UncaughtExceptionHandler() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.kochava.base.x.a(java.lang.Object, boolean):boolean
             arg types: [java.lang.Object, int]
             candidates:
              com.kochava.base.x.a(java.lang.Object, double):double
              com.kochava.base.x.a(java.lang.Object, int):int
              com.kochava.base.x.a(java.lang.Object, long):long
              com.kochava.base.x.a(java.io.InputStream, boolean):java.lang.String
              com.kochava.base.x.a(java.lang.Object, java.lang.String):java.lang.String
              com.kochava.base.x.a(java.lang.String, java.lang.String):java.lang.String
              com.kochava.base.x.a(java.lang.String, boolean):java.lang.String
              com.kochava.base.x.a(java.io.OutputStream, java.lang.String):void
              com.kochava.base.x.a(org.json.JSONObject, boolean):void
              com.kochava.base.x.a(android.content.Context, java.lang.String):boolean
              com.kochava.base.x.a(java.lang.Object, java.lang.Object):boolean
              com.kochava.base.x.a(org.json.JSONArray, java.lang.String):boolean
              com.kochava.base.x.a(org.json.JSONArray, org.json.JSONArray):boolean
              com.kochava.base.x.a(org.json.JSONObject, org.json.JSONObject):boolean
              com.kochava.base.x.a(int[], int):boolean
              com.kochava.base.x.a(java.lang.Object, boolean):boolean */
            public final void uncaughtException(Thread thread, Throwable th) {
                boolean z = true;
                Tracker.a(1, "STT", "uncaughtExcep", "Critical Error: Shutting Down", th);
                boolean a2 = x.a(i.this.d.b("internal_logging_enabled"), true);
                if (i.this.d.b("blacklist") == null) {
                    z = false;
                }
                if (!x.e() && a2 && z) {
                    i.this.a(thread.getName(), th.getClass().getCanonicalName(), th.getMessage(), th.getStackTrace());
                }
                Tracker.unConfigure(false);
            }
        };
        this.j.setUncaughtExceptionHandler(r4);
        this.k.setUncaughtExceptionHandler(r4);
        this.j.start();
        this.k.start();
        this.h = new Handler(this.j.getLooper());
        this.i = new Handler(this.k.getLooper());
        this.q = this.d.b("kochava_device_id") == null && this.d.b("kvinit_wait") == null;
        int b2 = x.b(this.d.b("launch_count"), 1);
        this.d.a("launch_count", Integer.valueOf(x.a(b2 + 1, 0, Integer.MAX_VALUE)));
        this.l = UUID.randomUUID().toString().substring(0, 5) + "-" + Long.toString((long) b2);
        this.u.a(x.a(this.d.b("networking_seconds_per_request"), (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE));
    }

    /* access modifiers changed from: package-private */
    public final String a(int i2) {
        switch (i2) {
            case 0:
                String optString = this.c.optString("url_init", null);
                return optString == null ? "https://kvinit-prod.api.kochava.com/track/kvinit" : optString;
            case 1:
                String optString2 = this.c.optString("url_initial", null);
                return optString2 == null ? "https://control.kochava.com/track/json" : optString2;
            case 2:
            case 3:
            case 6:
                String optString3 = this.c.optString("url_event", null);
                return optString3 == null ? "https://control.kochava.com/track/json" : optString3;
            case 4:
                String optString4 = this.c.optString("url_update", null);
                return optString4 == null ? "https://control.kochava.com/track/json" : optString4;
            case 5:
                String optString5 = this.c.optString("url_get_attribution", null);
                return optString5 == null ? "https://control.kochava.com/track/kvquery" : optString5;
            case 7:
                String optString6 = this.c.optString("url_identity_link", null);
                return optString6 == null ? "https://control.kochava.com/track/json" : optString6;
            case 8:
                String optString7 = this.c.optString("url_push_token_add", null);
                return optString7 == null ? "https://token.api.kochava.com/token/add" : optString7;
            case 9:
                String optString8 = this.c.optString("url_push_token_remove", null);
                return optString8 == null ? "https://token.api.kochava.com/token/remove" : optString8;
            case 10:
                String optString9 = this.c.optString("url_location_update", null);
                return optString9 == null ? "https://location.api.kochava.com/location" : optString9;
            case 11:
                String optString10 = this.c.optString("url_geo_event", null);
                return optString10 == null ? "https://location.api.kochava.com/geoevent" : optString10;
            case 12:
                String optString11 = this.c.optString("url_internal_error", null);
                return optString11 == null ? "https://control.kochava.com/track/json" : optString11;
            case 13:
                String optString12 = this.c.optString("url_smartlinks", null);
                return optString12 == null ? "https://smart.link/v1/links-sdk" : optString12;
            default:
                return "https://control.kochava.com/track/json";
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.i.removeCallbacks(runnable);
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable, long j2) {
        this.i.postDelayed(runnable, j2);
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable, boolean z) {
        if (z) {
            this.i.postAtFrontOfQueue(runnable);
        } else {
            this.i.post(runnable);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2, String str3, StackTraceElement[] stackTraceElementArr) {
        try {
            String a2 = x.a(this.d.b("kochava_app_id"), "");
            String a3 = x.a(this.d.b("sdk_version"), "");
            JSONObject jSONObject = new JSONObject();
            x.a("sdk_version", a3, jSONObject);
            x.a("kochava_app_id", a2, jSONObject);
            x.a("thread", str, jSONObject);
            x.a("exception", str2, jSONObject);
            x.a("message", str3, jSONObject);
            if (stackTraceElementArr != null && !"java.lang.OutOfMemoryError".equals(str2)) {
                JSONArray jSONArray = new JSONArray();
                for (int i2 = 0; i2 < Math.min(3, stackTraceElementArr.length); i2++) {
                    jSONArray.put(stackTraceElementArr[i2].toString());
                }
                x.a("stack", jSONArray, jSONObject);
            }
            JSONObject jSONObject2 = new JSONObject();
            x.a("message", "sdk.internal " + x.a(jSONObject), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            x.a("action", "error", jSONObject3);
            x.a("kochava_app_id", a2, jSONObject3);
            x.a("data", jSONObject2, jSONObject3);
            x.a(a(12), x.a(jSONObject3));
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(Runnable runnable) {
        this.h.post(runnable);
    }
}
