package com.android.billingclient.api;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
public class BillingFlowParams {
    public static final String EXTRA_PARAM_CHILD_DIRECTED = "childDirected";
    public static final String EXTRA_PARAM_KEY_ACCOUNT_ID = "accountId";
    public static final String EXTRA_PARAM_KEY_DEVELOPER_ID = "developerId";
    public static final String EXTRA_PARAM_KEY_OLD_SKUS = "skusToReplace";
    public static final String EXTRA_PARAM_KEY_OLD_SKU_PURCHASE_TOKEN = "oldSkuPurchaseToken";
    public static final String EXTRA_PARAM_KEY_REPLACE_SKUS_PRORATION_MODE = "prorationMode";
    public static final String EXTRA_PARAM_KEY_RSKU = "rewardToken";
    public static final String EXTRA_PARAM_KEY_VR = "vr";
    public static final String EXTRA_PARAM_UNDER_AGE_OF_CONSENT = "underAgeOfConsent";
    /* access modifiers changed from: private */
    public SkuDetails zza;
    /* access modifiers changed from: private */
    public String zzb;
    /* access modifiers changed from: private */
    public String zzc;
    /* access modifiers changed from: private */
    public String zzd;
    /* access modifiers changed from: private */
    public boolean zze;
    /* access modifiers changed from: private */
    public int zzf = 0;
    /* access modifiers changed from: private */
    public String zzg;

    @Retention(RetentionPolicy.SOURCE)
    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    public @interface ProrationMode {
        public static final int DEFERRED = 4;
        public static final int IMMEDIATE_AND_CHARGE_PRORATED_PRICE = 2;
        public static final int IMMEDIATE_WITHOUT_PRORATION = 3;
        public static final int IMMEDIATE_WITH_TIME_PRORATION = 1;
        public static final int UNKNOWN_SUBSCRIPTION_UPGRADE_DOWNGRADE_POLICY = 0;
    }

    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    public static class Builder {
        private SkuDetails zza;
        private String zzb;
        private String zzc;
        private String zzd;
        private boolean zze;
        private int zzf;
        private String zzg;

        private Builder() {
            this.zzf = 0;
        }

        public Builder setSkuDetails(SkuDetails skuDetails) {
            this.zza = skuDetails;
            return this;
        }

        @Deprecated
        public Builder setOldSkus(ArrayList<String> arrayList) {
            if (arrayList != null && arrayList.size() > 0) {
                this.zzb = arrayList.get(0);
            }
            return this;
        }

        @Deprecated
        public Builder setOldSku(String str) {
            this.zzb = str;
            return this;
        }

        public Builder setOldSku(String str, String str2) {
            this.zzb = str;
            this.zzc = str2;
            return this;
        }

        @Deprecated
        public Builder addOldSku(String str) {
            this.zzb = str;
            return this;
        }

        public Builder setReplaceSkusProrationMode(int i) {
            this.zzf = i;
            return this;
        }

        public Builder setAccountId(String str) {
            this.zzd = str;
            return this;
        }

        public Builder setVrPurchaseFlow(boolean z) {
            this.zze = z;
            return this;
        }

        public Builder setDeveloperId(String str) {
            this.zzg = str;
            return this;
        }

        public BillingFlowParams build() {
            BillingFlowParams billingFlowParams = new BillingFlowParams();
            SkuDetails unused = billingFlowParams.zza = this.zza;
            String unused2 = billingFlowParams.zzb = this.zzb;
            String unused3 = billingFlowParams.zzc = this.zzc;
            String unused4 = billingFlowParams.zzd = this.zzd;
            boolean unused5 = billingFlowParams.zze = this.zze;
            int unused6 = billingFlowParams.zzf = this.zzf;
            String unused7 = billingFlowParams.zzg = this.zzg;
            return billingFlowParams;
        }
    }

    public String getSku() {
        SkuDetails skuDetails = this.zza;
        if (skuDetails == null) {
            return null;
        }
        return skuDetails.getSku();
    }

    public String getSkuType() {
        SkuDetails skuDetails = this.zza;
        if (skuDetails == null) {
            return null;
        }
        return skuDetails.getType();
    }

    public SkuDetails getSkuDetails() {
        return this.zza;
    }

    @Deprecated
    public ArrayList<String> getOldSkus() {
        return new ArrayList<>(Arrays.asList(this.zzb));
    }

    public String getOldSku() {
        return this.zzb;
    }

    public String getOldSkuPurchaseToken() {
        return this.zzc;
    }

    public String getAccountId() {
        return this.zzd;
    }

    public boolean getVrPurchaseFlow() {
        return this.zze;
    }

    public int getReplaceSkusProrationMode() {
        return this.zzf;
    }

    /* access modifiers changed from: package-private */
    public boolean hasExtraParams() {
        return (!this.zze && this.zzd == null && this.zzg == null && this.zzf == 0) ? false : true;
    }

    public String getDeveloperId() {
        return this.zzg;
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
