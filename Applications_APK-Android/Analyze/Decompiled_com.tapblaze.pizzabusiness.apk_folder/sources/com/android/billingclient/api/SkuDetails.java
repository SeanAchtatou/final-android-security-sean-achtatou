package com.android.billingclient.api;

import android.text.TextUtils;
import com.android.billingclient.util.BillingHelper;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
public class SkuDetails {
    private final String zza;
    private final JSONObject zzb = new JSONObject(this.zza);

    public SkuDetails(String str) throws JSONException {
        this.zza = str;
    }

    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    public static class SkuDetailsResult {
        private List<SkuDetails> zza;
        private final int zzb;
        private final String zzc;

        public SkuDetailsResult(int i, String str, List<SkuDetails> list) {
            this.zzb = i;
            this.zzc = str;
            this.zza = list;
        }

        public List<SkuDetails> getSkuDetailsList() {
            return this.zza;
        }

        public int getResponseCode() {
            return this.zzb;
        }

        public String getDebugMessage() {
            return this.zzc;
        }
    }

    public String getOriginalJson() {
        return this.zza;
    }

    public String getSku() {
        return this.zzb.optString("productId");
    }

    public String getType() {
        return this.zzb.optString("type");
    }

    public String getPrice() {
        return this.zzb.optString("price");
    }

    public long getPriceAmountMicros() {
        return this.zzb.optLong("price_amount_micros");
    }

    public String getPriceCurrencyCode() {
        return this.zzb.optString("price_currency_code");
    }

    public String getOriginalPrice() {
        if (this.zzb.has("original_price")) {
            return this.zzb.optString("original_price");
        }
        return getPrice();
    }

    public long getOriginalPriceAmountMicros() {
        if (this.zzb.has("original_price_micros")) {
            return this.zzb.optLong("original_price_micros");
        }
        return getPriceAmountMicros();
    }

    public String getTitle() {
        return this.zzb.optString("title");
    }

    public String getDescription() {
        return this.zzb.optString("description");
    }

    public String getSubscriptionPeriod() {
        return this.zzb.optString("subscriptionPeriod");
    }

    public String getFreeTrialPeriod() {
        return this.zzb.optString("freeTrialPeriod");
    }

    public String getIntroductoryPrice() {
        return this.zzb.optString("introductoryPrice");
    }

    public long getIntroductoryPriceAmountMicros() {
        return this.zzb.optLong("introductoryPriceAmountMicros");
    }

    public String getIntroductoryPricePeriod() {
        return this.zzb.optString("introductoryPricePeriod");
    }

    public String getIntroductoryPriceCycles() {
        return this.zzb.optString("introductoryPriceCycles");
    }

    public String getIconUrl() {
        return this.zzb.optString("iconUrl");
    }

    public boolean isRewarded() {
        return this.zzb.has(BillingFlowParams.EXTRA_PARAM_KEY_RSKU);
    }

    /* access modifiers changed from: package-private */
    public String getSkuDetailsToken() {
        return this.zzb.optString(BillingHelper.EXTRA_PARAM_KEY_SKU_DETAILS_TOKEN);
    }

    /* access modifiers changed from: package-private */
    public String rewardToken() {
        return this.zzb.optString(BillingFlowParams.EXTRA_PARAM_KEY_RSKU);
    }

    public String toString() {
        String valueOf = String.valueOf(this.zza);
        return valueOf.length() != 0 ? "SkuDetails: ".concat(valueOf) : new String("SkuDetails: ");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SkuDetails)) {
            return false;
        }
        return TextUtils.equals(this.zza, ((SkuDetails) obj).zza);
    }

    public int hashCode() {
        return this.zza.hashCode();
    }
}
