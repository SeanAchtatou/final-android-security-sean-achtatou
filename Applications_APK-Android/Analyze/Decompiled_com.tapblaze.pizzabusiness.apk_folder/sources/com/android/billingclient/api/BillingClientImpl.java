package com.android.billingclient.api;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.text.TextUtils;
import com.android.billingclient.BuildConfig;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.util.BillingHelper;
import com.google.android.gms.internal.play_billing.zza;
import com.google.android.gms.internal.play_billing.zzc;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
class BillingClientImpl extends BillingClient {
    /* access modifiers changed from: private */
    public int zza;
    /* access modifiers changed from: private */
    public final String zzb;
    private final Handler zzc;
    /* access modifiers changed from: private */
    public BillingBroadcastManager zzd;
    /* access modifiers changed from: private */
    public Context zze;
    /* access modifiers changed from: private */
    public final int zzf;
    /* access modifiers changed from: private */
    public final int zzg;
    /* access modifiers changed from: private */
    public zza zzh;
    private BillingServiceConnection zzi;
    /* access modifiers changed from: private */
    public boolean zzj;
    /* access modifiers changed from: private */
    public boolean zzk;
    /* access modifiers changed from: private */
    public boolean zzl;
    /* access modifiers changed from: private */
    public boolean zzm;
    /* access modifiers changed from: private */
    public boolean zzn;
    /* access modifiers changed from: private */
    public boolean zzo;
    private boolean zzp;
    private ExecutorService zzq;
    private final ResultReceiver zzr;

    @Retention(RetentionPolicy.SOURCE)
    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    public @interface ClientState {
        public static final int CLOSED = 3;
        public static final int CONNECTED = 2;
        public static final int CONNECTING = 1;
        public static final int DISCONNECTED = 0;
    }

    @Retention(RetentionPolicy.SOURCE)
    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    private @interface NativeUsage {
    }

    /* access modifiers changed from: package-private */
    public void setExecutorService(ExecutorService executorService) {
        this.zzq = executorService;
    }

    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    private static class PurchaseHistoryResult {
        private final List<PurchaseHistoryRecord> zza;
        private final BillingResult zzb;

        PurchaseHistoryResult(BillingResult billingResult, List<PurchaseHistoryRecord> list) {
            this.zza = list;
            this.zzb = billingResult;
        }

        /* access modifiers changed from: package-private */
        public BillingResult getBillingResult() {
            return this.zzb;
        }

        /* access modifiers changed from: package-private */
        public List<PurchaseHistoryRecord> getPurchaseHistoryRecordList() {
            return this.zza;
        }
    }

    BillingClientImpl(Context context, int i, int i2, boolean z, PurchasesUpdatedListener purchasesUpdatedListener) {
        this(context, i, i2, z, purchasesUpdatedListener, zza());
    }

    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    private final class BillingServiceConnection implements ServiceConnection {
        /* access modifiers changed from: private */
        public final Object zza;
        /* access modifiers changed from: private */
        public boolean zzb;
        /* access modifiers changed from: private */
        public BillingClientStateListener zzc;

        private BillingServiceConnection(BillingClientStateListener billingClientStateListener) {
            this.zza = new Object();
            this.zzb = false;
            this.zzc = billingClientStateListener;
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            BillingHelper.logWarn("BillingClient", "Billing service disconnected.");
            zza unused = BillingClientImpl.this.zzh = null;
            int unused2 = BillingClientImpl.this.zza = 0;
            synchronized (this.zza) {
                if (this.zzc != null) {
                    this.zzc.onBillingServiceDisconnected();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public final void markDisconnectedAndCleanUp() {
            synchronized (this.zza) {
                this.zzc = null;
                this.zzb = true;
            }
        }

        /* access modifiers changed from: private */
        public final void zza(final BillingResult billingResult) {
            BillingClientImpl.this.zza(new Runnable() {
                public void run() {
                    synchronized (BillingServiceConnection.this.zza) {
                        if (BillingServiceConnection.this.zzc != null) {
                            BillingServiceConnection.this.zzc.onBillingSetupFinished(billingResult);
                        }
                    }
                }
            });
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            BillingHelper.logVerbose("BillingClient", "Billing service connected.");
            zza unused = BillingClientImpl.this.zzh = zzc.zza(iBinder);
            if (BillingClientImpl.this.zza(new Callable<Void>() {
                /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
                    r3 = com.android.billingclient.api.BillingClientImpl.access$200(r10.this$1.this$0).getPackageName();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
                    r5 = 10;
                    r6 = 3;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
                    if (r5 < 3) goto L_0x003c;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
                    r6 = com.android.billingclient.api.BillingClientImpl.access$300(r10.this$1.this$0).zza(r5, r3, com.android.billingclient.api.BillingClient.SkuType.SUBS);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
                    if (r6 != 0) goto L_0x0039;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
                    r5 = r5 - 1;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:18:0x003c, code lost:
                    r5 = 0;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:19:0x003d, code lost:
                    r7 = r10.this$1.this$0;
                    r9 = true;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:20:0x0043, code lost:
                    if (r5 < 5) goto L_0x0047;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:21:0x0045, code lost:
                    r8 = true;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:22:0x0047, code lost:
                    r8 = false;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
                    com.android.billingclient.api.BillingClientImpl.access$1602(r7, r8);
                    r7 = r10.this$1.this$0;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x004f, code lost:
                    if (r5 < 3) goto L_0x0053;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:25:0x0051, code lost:
                    r8 = true;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:26:0x0053, code lost:
                    r8 = false;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:27:0x0054, code lost:
                    com.android.billingclient.api.BillingClientImpl.access$1702(r7, r8);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:28:0x0057, code lost:
                    if (r5 >= 3) goto L_0x0060;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
                    com.android.billingclient.util.BillingHelper.logVerbose("BillingClient", "In-app billing API does not support subscription on this device.");
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:30:0x0060, code lost:
                    r5 = 10;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:31:0x0062, code lost:
                    if (r5 < 3) goto L_0x0078;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:32:0x0064, code lost:
                    r6 = com.android.billingclient.api.BillingClientImpl.access$300(r10.this$1.this$0).zza(r5, r3, com.android.billingclient.api.BillingClient.SkuType.INAPP);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:33:0x0072, code lost:
                    if (r6 != 0) goto L_0x0075;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:35:0x0075, code lost:
                    r5 = r5 - 1;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:36:0x0078, code lost:
                    r5 = 0;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:37:0x0079, code lost:
                    r3 = r10.this$1.this$0;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:38:0x007d, code lost:
                    if (r5 < 10) goto L_0x0081;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:39:0x007f, code lost:
                    r4 = true;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:40:0x0081, code lost:
                    r4 = false;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:41:0x0082, code lost:
                    com.android.billingclient.api.BillingClientImpl.access$1802(r3, r4);
                    r3 = r10.this$1.this$0;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:42:0x008b, code lost:
                    if (r5 < 9) goto L_0x008f;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:43:0x008d, code lost:
                    r4 = true;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:44:0x008f, code lost:
                    r4 = false;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:45:0x0090, code lost:
                    com.android.billingclient.api.BillingClientImpl.access$1902(r3, r4);
                    r3 = r10.this$1.this$0;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:46:0x0099, code lost:
                    if (r5 < 8) goto L_0x009d;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:47:0x009b, code lost:
                    r4 = true;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:48:0x009d, code lost:
                    r4 = false;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:49:0x009e, code lost:
                    com.android.billingclient.api.BillingClientImpl.access$2002(r3, r4);
                    r3 = r10.this$1.this$0;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:50:0x00a6, code lost:
                    if (r5 < 6) goto L_0x00a9;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:52:0x00a9, code lost:
                    r9 = false;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:53:0x00aa, code lost:
                    com.android.billingclient.api.BillingClientImpl.access$2102(r3, r9);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ad, code lost:
                    if (r5 >= 3) goto L_0x00b6;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:55:0x00af, code lost:
                    com.android.billingclient.util.BillingHelper.logWarn("BillingClient", "In-app billing API version 3 is not supported on this device.");
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:56:0x00b6, code lost:
                    if (r6 != 0) goto L_0x00c1;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:57:0x00b8, code lost:
                    com.android.billingclient.api.BillingClientImpl.access$1202(r10.this$1.this$0, 2);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:58:0x00c1, code lost:
                    com.android.billingclient.api.BillingClientImpl.access$1202(r10.this$1.this$0, 0);
                    com.android.billingclient.api.BillingClientImpl.access$302(r10.this$1.this$0, null);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:59:0x00d0, code lost:
                    r6 = 3;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:61:0x00d1, code lost:
                    com.android.billingclient.util.BillingHelper.logWarn("BillingClient", "Exception while checking if billing is supported; try to reconnect");
                    com.android.billingclient.api.BillingClientImpl.access$1202(r10.this$1.this$0, 0);
                    com.android.billingclient.api.BillingClientImpl.access$302(r10.this$1.this$0, null);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:63:0x00e6, code lost:
                    if (r6 != 0) goto L_0x00f0;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:64:0x00e8, code lost:
                    com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.access$2200(r10.this$1, com.android.billingclient.api.BillingResults.OK);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:65:0x00f0, code lost:
                    com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.access$2200(r10.this$1, com.android.billingclient.api.BillingResults.API_VERSION_NOT_V3);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:66:0x00f7, code lost:
                    return null;
                 */
                /* JADX WARNING: Removed duplicated region for block: B:64:0x00e8  */
                /* JADX WARNING: Removed duplicated region for block: B:65:0x00f0  */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public java.lang.Void call() {
                    /*
                        r10 = this;
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r0 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this
                        java.lang.Object r0 = r0.zza
                        monitor-enter(r0)
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r1 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ all -> 0x00f8 }
                        boolean r1 = r1.zzb     // Catch:{ all -> 0x00f8 }
                        r2 = 0
                        if (r1 == 0) goto L_0x0012
                        monitor-exit(r0)     // Catch:{ all -> 0x00f8 }
                        return r2
                    L_0x0012:
                        monitor-exit(r0)     // Catch:{ all -> 0x00f8 }
                        r0 = 3
                        r1 = 0
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r3 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d0 }
                        com.android.billingclient.api.BillingClientImpl r3 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d0 }
                        android.content.Context r3 = r3.zze     // Catch:{ Exception -> 0x00d0 }
                        java.lang.String r3 = r3.getPackageName()     // Catch:{ Exception -> 0x00d0 }
                        r4 = 10
                        r5 = 10
                        r6 = 3
                    L_0x0026:
                        if (r5 < r0) goto L_0x003c
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r7 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r7 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        com.google.android.gms.internal.play_billing.zza r7 = r7.zzh     // Catch:{ Exception -> 0x00d1 }
                        java.lang.String r8 = "subs"
                        int r6 = r7.zza(r5, r3, r8)     // Catch:{ Exception -> 0x00d1 }
                        if (r6 != 0) goto L_0x0039
                        goto L_0x003d
                    L_0x0039:
                        int r5 = r5 + -1
                        goto L_0x0026
                    L_0x003c:
                        r5 = 0
                    L_0x003d:
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r7 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r7 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        r8 = 5
                        r9 = 1
                        if (r5 < r8) goto L_0x0047
                        r8 = 1
                        goto L_0x0048
                    L_0x0047:
                        r8 = 0
                    L_0x0048:
                        boolean unused = r7.zzk = r8     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r7 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r7 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        if (r5 < r0) goto L_0x0053
                        r8 = 1
                        goto L_0x0054
                    L_0x0053:
                        r8 = 0
                    L_0x0054:
                        boolean unused = r7.zzj = r8     // Catch:{ Exception -> 0x00d1 }
                        if (r5 >= r0) goto L_0x0060
                        java.lang.String r5 = "BillingClient"
                        java.lang.String r7 = "In-app billing API does not support subscription on this device."
                        com.android.billingclient.util.BillingHelper.logVerbose(r5, r7)     // Catch:{ Exception -> 0x00d1 }
                    L_0x0060:
                        r5 = 10
                    L_0x0062:
                        if (r5 < r0) goto L_0x0078
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r7 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r7 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        com.google.android.gms.internal.play_billing.zza r7 = r7.zzh     // Catch:{ Exception -> 0x00d1 }
                        java.lang.String r8 = "inapp"
                        int r6 = r7.zza(r5, r3, r8)     // Catch:{ Exception -> 0x00d1 }
                        if (r6 != 0) goto L_0x0075
                        goto L_0x0079
                    L_0x0075:
                        int r5 = r5 + -1
                        goto L_0x0062
                    L_0x0078:
                        r5 = 0
                    L_0x0079:
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r3 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r3 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        if (r5 < r4) goto L_0x0081
                        r4 = 1
                        goto L_0x0082
                    L_0x0081:
                        r4 = 0
                    L_0x0082:
                        boolean unused = r3.zzo = r4     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r3 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r3 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        r4 = 9
                        if (r5 < r4) goto L_0x008f
                        r4 = 1
                        goto L_0x0090
                    L_0x008f:
                        r4 = 0
                    L_0x0090:
                        boolean unused = r3.zzn = r4     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r3 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r3 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        r4 = 8
                        if (r5 < r4) goto L_0x009d
                        r4 = 1
                        goto L_0x009e
                    L_0x009d:
                        r4 = 0
                    L_0x009e:
                        boolean unused = r3.zzm = r4     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r3 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r3 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        r4 = 6
                        if (r5 < r4) goto L_0x00a9
                        goto L_0x00aa
                    L_0x00a9:
                        r9 = 0
                    L_0x00aa:
                        boolean unused = r3.zzl = r9     // Catch:{ Exception -> 0x00d1 }
                        if (r5 >= r0) goto L_0x00b6
                        java.lang.String r0 = "BillingClient"
                        java.lang.String r3 = "In-app billing API version 3 is not supported on this device."
                        com.android.billingclient.util.BillingHelper.logWarn(r0, r3)     // Catch:{ Exception -> 0x00d1 }
                    L_0x00b6:
                        if (r6 != 0) goto L_0x00c1
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r0 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r0 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        r3 = 2
                        int unused = r0.zza = r3     // Catch:{ Exception -> 0x00d1 }
                        goto L_0x00e6
                    L_0x00c1:
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r0 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r0 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        int unused = r0.zza = r1     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r0 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this     // Catch:{ Exception -> 0x00d1 }
                        com.android.billingclient.api.BillingClientImpl r0 = com.android.billingclient.api.BillingClientImpl.this     // Catch:{ Exception -> 0x00d1 }
                        com.google.android.gms.internal.play_billing.zza unused = r0.zzh = r2     // Catch:{ Exception -> 0x00d1 }
                        goto L_0x00e6
                    L_0x00d0:
                        r6 = 3
                    L_0x00d1:
                        java.lang.String r0 = "BillingClient"
                        java.lang.String r3 = "Exception while checking if billing is supported; try to reconnect"
                        com.android.billingclient.util.BillingHelper.logWarn(r0, r3)
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r0 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this
                        com.android.billingclient.api.BillingClientImpl r0 = com.android.billingclient.api.BillingClientImpl.this
                        int unused = r0.zza = r1
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r0 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this
                        com.android.billingclient.api.BillingClientImpl r0 = com.android.billingclient.api.BillingClientImpl.this
                        com.google.android.gms.internal.play_billing.zza unused = r0.zzh = r2
                    L_0x00e6:
                        if (r6 != 0) goto L_0x00f0
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r0 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this
                        com.android.billingclient.api.BillingResult r1 = com.android.billingclient.api.BillingResults.OK
                        r0.zza(r1)
                        goto L_0x00f7
                    L_0x00f0:
                        com.android.billingclient.api.BillingClientImpl$BillingServiceConnection r0 = com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.this
                        com.android.billingclient.api.BillingResult r1 = com.android.billingclient.api.BillingResults.API_VERSION_NOT_V3
                        r0.zza(r1)
                    L_0x00f7:
                        return r2
                    L_0x00f8:
                        r1 = move-exception
                        monitor-exit(r0)     // Catch:{ all -> 0x00f8 }
                        goto L_0x00fc
                    L_0x00fb:
                        throw r1
                    L_0x00fc:
                        goto L_0x00fb
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.android.billingclient.api.BillingClientImpl.BillingServiceConnection.AnonymousClass2.call():java.lang.Void");
                }
            }, 30000, new Runnable() {
                public void run() {
                    int unused = BillingClientImpl.this.zza = 0;
                    zza unused2 = BillingClientImpl.this.zzh = null;
                    BillingServiceConnection.this.zza(BillingResults.SERVICE_TIMEOUT);
                }
            }) == null) {
                zza(BillingClientImpl.this.zzc());
            }
        }
    }

    private static String zza() {
        try {
            return (String) Class.forName("com.android.billingclient.ktx.BuildConfig").getField("VERSION_NAME").get(null);
        } catch (Exception unused) {
            return BuildConfig.VERSION_NAME;
        }
    }

    private BillingClientImpl(Context context, int i, int i2, boolean z, PurchasesUpdatedListener purchasesUpdatedListener, String str) {
        this.zza = 0;
        this.zzc = new Handler(Looper.getMainLooper());
        this.zzr = new ResultReceiver(this.zzc) {
            public void onReceiveResult(int i, Bundle bundle) {
                PurchasesUpdatedListener listener = BillingClientImpl.this.zzd.getListener();
                if (listener == null) {
                    BillingHelper.logWarn("BillingClient", "PurchasesUpdatedListener is null - no way to return the response.");
                    return;
                }
                listener.onPurchasesUpdated(BillingResult.newBuilder().setResponseCode(i).setDebugMessage(BillingHelper.getDebugMessageFromBundle(bundle, "BillingClient")).build(), BillingHelper.extractPurchases(bundle));
            }
        };
        this.zzf = i;
        this.zzg = i2;
        this.zzb = str;
        this.zze = context.getApplicationContext();
        this.zzd = new BillingBroadcastManager(this.zze, purchasesUpdatedListener);
        this.zzp = z;
    }

    public BillingResult isFeatureSupported(String str) {
        if (!isReady()) {
            return BillingResults.SERVICE_DISCONNECTED;
        }
        char c = 65535;
        switch (str.hashCode()) {
            case -422092961:
                if (str.equals(BillingClient.FeatureType.SUBSCRIPTIONS_UPDATE)) {
                    c = 1;
                    break;
                }
                break;
            case 207616302:
                if (str.equals(BillingClient.FeatureType.PRICE_CHANGE_CONFIRMATION)) {
                    c = 4;
                    break;
                }
                break;
            case 292218239:
                if (str.equals(BillingClient.FeatureType.IN_APP_ITEMS_ON_VR)) {
                    c = 2;
                    break;
                }
                break;
            case 1219490065:
                if (str.equals(BillingClient.FeatureType.SUBSCRIPTIONS_ON_VR)) {
                    c = 3;
                    break;
                }
                break;
            case 1987365622:
                if (str.equals(BillingClient.FeatureType.SUBSCRIPTIONS)) {
                    c = 0;
                    break;
                }
                break;
        }
        if (c == 0) {
            return this.zzj ? BillingResults.OK : BillingResults.FEATURE_NOT_SUPPORTED;
        }
        if (c != 1) {
            if (c == 2) {
                return zzb(BillingClient.SkuType.INAPP);
            }
            if (c == 3) {
                return zzb(BillingClient.SkuType.SUBS);
            }
            if (c == 4) {
                return this.zzm ? BillingResults.OK : BillingResults.FEATURE_NOT_SUPPORTED;
            }
            String valueOf = String.valueOf(str);
            BillingHelper.logWarn("BillingClient", valueOf.length() != 0 ? "Unsupported feature: ".concat(valueOf) : new String("Unsupported feature: "));
            return BillingResults.UNKNOWN_FEATURE;
        } else if (this.zzk) {
            return BillingResults.OK;
        } else {
            return BillingResults.FEATURE_NOT_SUPPORTED;
        }
    }

    public boolean isReady() {
        return (this.zza != 2 || this.zzh == null || this.zzi == null) ? false : true;
    }

    public void startConnection(BillingClientStateListener billingClientStateListener) {
        if (isReady()) {
            BillingHelper.logVerbose("BillingClient", "Service connection is valid. No need to re-initialize.");
            billingClientStateListener.onBillingSetupFinished(BillingResults.OK);
            return;
        }
        int i = this.zza;
        if (i == 1) {
            BillingHelper.logWarn("BillingClient", "Client is already in the process of connecting to billing service.");
            billingClientStateListener.onBillingSetupFinished(BillingResults.CLIENT_CONNECTING);
        } else if (i == 3) {
            BillingHelper.logWarn("BillingClient", "Client was already closed and can't be reused. Please create another instance.");
            billingClientStateListener.onBillingSetupFinished(BillingResults.SERVICE_DISCONNECTED);
        } else {
            this.zza = 1;
            this.zzd.registerReceiver();
            BillingHelper.logVerbose("BillingClient", "Starting in-app billing setup.");
            this.zzi = new BillingServiceConnection(billingClientStateListener);
            Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            intent.setPackage("com.android.vending");
            List<ResolveInfo> queryIntentServices = this.zze.getPackageManager().queryIntentServices(intent, 0);
            if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
                ResolveInfo resolveInfo = queryIntentServices.get(0);
                if (resolveInfo.serviceInfo != null) {
                    String str = resolveInfo.serviceInfo.packageName;
                    String str2 = resolveInfo.serviceInfo.name;
                    if (!"com.android.vending".equals(str) || str2 == null) {
                        BillingHelper.logWarn("BillingClient", "The device doesn't have valid Play Store.");
                    } else {
                        ComponentName componentName = new ComponentName(str, str2);
                        Intent intent2 = new Intent(intent);
                        intent2.setComponent(componentName);
                        intent2.putExtra(BillingHelper.LIBRARY_VERSION_KEY, this.zzb);
                        if (this.zze.bindService(intent2, this.zzi, 1)) {
                            BillingHelper.logVerbose("BillingClient", "Service was bonded successfully.");
                            return;
                        }
                        BillingHelper.logWarn("BillingClient", "Connection to Billing service is blocked.");
                    }
                }
            }
            this.zza = 0;
            BillingHelper.logVerbose("BillingClient", "Billing service unavailable on device.");
            billingClientStateListener.onBillingSetupFinished(BillingResults.BILLING_UNAVAILABLE);
        }
    }

    public void endConnection() {
        try {
            this.zzd.destroy();
            if (this.zzi != null) {
                this.zzi.markDisconnectedAndCleanUp();
            }
            if (!(this.zzi == null || this.zzh == null)) {
                BillingHelper.logVerbose("BillingClient", "Unbinding from service.");
                this.zze.unbindService(this.zzi);
                this.zzi = null;
            }
            this.zzh = null;
            if (this.zzq != null) {
                this.zzq.shutdownNow();
                this.zzq = null;
            }
        } catch (Exception e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
            sb.append("There was an exception while ending connection: ");
            sb.append(valueOf);
            BillingHelper.logWarn("BillingClient", sb.toString());
        } finally {
            this.zza = 3;
        }
    }

    public void launchPriceChangeConfirmationFlow(Activity activity, PriceChangeFlowParams priceChangeFlowParams, final PriceChangeConfirmationListener priceChangeConfirmationListener) {
        if (!isReady()) {
            priceChangeConfirmationListener.onPriceChangeConfirmationResult(BillingResults.SERVICE_DISCONNECTED);
        } else if (priceChangeFlowParams == null || priceChangeFlowParams.getSkuDetails() == null) {
            BillingHelper.logWarn("BillingClient", "Please fix the input params. priceChangeFlowParams must contain valid sku.");
            priceChangeConfirmationListener.onPriceChangeConfirmationResult(BillingResults.NULL_SKU);
        } else {
            final String sku = priceChangeFlowParams.getSkuDetails().getSku();
            if (sku == null) {
                BillingHelper.logWarn("BillingClient", "Please fix the input params. priceChangeFlowParams must contain valid sku.");
                priceChangeConfirmationListener.onPriceChangeConfirmationResult(BillingResults.NULL_SKU);
            } else if (!this.zzm) {
                BillingHelper.logWarn("BillingClient", "Current client doesn't support price change confirmation flow.");
                priceChangeConfirmationListener.onPriceChangeConfirmationResult(BillingResults.FEATURE_NOT_SUPPORTED);
            } else {
                final Bundle bundle = new Bundle();
                bundle.putString(BillingHelper.LIBRARY_VERSION_KEY, this.zzb);
                bundle.putBoolean(BillingHelper.EXTRA_PARAM_KEY_SUBS_PRICE_CHANGE, true);
                try {
                    Bundle bundle2 = (Bundle) zza(new Callable<Bundle>() {
                        public Bundle call() throws Exception {
                            return BillingClientImpl.this.zzh.zzb(8, BillingClientImpl.this.zze.getPackageName(), sku, BillingClient.SkuType.SUBS, bundle);
                        }
                    }, 5000, null).get(5000, TimeUnit.MILLISECONDS);
                    int responseCodeFromBundle = BillingHelper.getResponseCodeFromBundle(bundle2, "BillingClient");
                    BillingResult build = BillingResult.newBuilder().setResponseCode(responseCodeFromBundle).setDebugMessage(BillingHelper.getDebugMessageFromBundle(bundle2, "BillingClient")).build();
                    if (responseCodeFromBundle != 0) {
                        StringBuilder sb = new StringBuilder(68);
                        sb.append("Unable to launch price change flow, error response code: ");
                        sb.append(responseCodeFromBundle);
                        BillingHelper.logWarn("BillingClient", sb.toString());
                        priceChangeConfirmationListener.onPriceChangeConfirmationResult(build);
                        return;
                    }
                    AnonymousClass3 r4 = new ResultReceiver(this, this.zzc) {
                        public void onReceiveResult(int i, Bundle bundle) {
                            priceChangeConfirmationListener.onPriceChangeConfirmationResult(BillingResult.newBuilder().setResponseCode(i).setDebugMessage(BillingHelper.getDebugMessageFromBundle(bundle, "BillingClient")).build());
                        }
                    };
                    Intent intent = new Intent(activity, ProxyBillingActivity.class);
                    intent.putExtra(BillingHelper.RESPONSE_SUBS_MANAGEMENT_INTENT_KEY, (PendingIntent) bundle2.getParcelable(BillingHelper.RESPONSE_SUBS_MANAGEMENT_INTENT_KEY));
                    intent.putExtra("result_receiver", r4);
                    activity.startActivity(intent);
                } catch (CancellationException | TimeoutException unused) {
                    StringBuilder sb2 = new StringBuilder(String.valueOf(sku).length() + 70);
                    sb2.append("Time out while launching Price Change Flow for sku: ");
                    sb2.append(sku);
                    sb2.append("; try to reconnect");
                    BillingHelper.logWarn("BillingClient", sb2.toString());
                    priceChangeConfirmationListener.onPriceChangeConfirmationResult(BillingResults.SERVICE_TIMEOUT);
                } catch (Exception unused2) {
                    StringBuilder sb3 = new StringBuilder(String.valueOf(sku).length() + 78);
                    sb3.append("Exception caught while launching Price Change Flow for sku: ");
                    sb3.append(sku);
                    sb3.append("; try to reconnect");
                    BillingHelper.logWarn("BillingClient", sb3.toString());
                    priceChangeConfirmationListener.onPriceChangeConfirmationResult(BillingResults.SERVICE_DISCONNECTED);
                }
            }
        }
    }

    public BillingResult launchBillingFlow(Activity activity, final BillingFlowParams billingFlowParams) {
        Future future;
        final int i;
        if (!isReady()) {
            return zza(BillingResults.SERVICE_DISCONNECTED);
        }
        final String skuType = billingFlowParams.getSkuType();
        final String sku = billingFlowParams.getSku();
        SkuDetails skuDetails = billingFlowParams.getSkuDetails();
        boolean z = true;
        boolean z2 = skuDetails != null && skuDetails.isRewarded();
        if (sku == null) {
            BillingHelper.logWarn("BillingClient", "Please fix the input params. SKU can't be null.");
            return zza(BillingResults.NULL_SKU);
        } else if (skuType == null) {
            BillingHelper.logWarn("BillingClient", "Please fix the input params. SkuType can't be null.");
            return zza(BillingResults.NULL_SKU_TYPE);
        } else if (!skuType.equals(BillingClient.SkuType.SUBS) || this.zzj) {
            if (billingFlowParams.getOldSku() == null) {
                z = false;
            }
            if (z && !this.zzk) {
                BillingHelper.logWarn("BillingClient", "Current client doesn't support subscriptions update.");
                return zza(BillingResults.SUBSCRIPTIONS_UPDATE_NOT_SUPPORTED);
            } else if (billingFlowParams.hasExtraParams() && !this.zzl) {
                BillingHelper.logWarn("BillingClient", "Current client doesn't support extra params for buy intent.");
                return zza(BillingResults.EXTRA_PARAMS_NOT_SUPPORTED);
            } else if (!z2 || this.zzl) {
                StringBuilder sb = new StringBuilder(String.valueOf(sku).length() + 41 + String.valueOf(skuType).length());
                sb.append("Constructing buy intent for ");
                sb.append(sku);
                sb.append(", item type: ");
                sb.append(skuType);
                BillingHelper.logVerbose("BillingClient", sb.toString());
                if (this.zzl) {
                    final Bundle constructExtraParamsForLaunchBillingFlow = BillingHelper.constructExtraParamsForLaunchBillingFlow(billingFlowParams, this.zzn, this.zzp, this.zzb);
                    if (!skuDetails.getSkuDetailsToken().isEmpty()) {
                        constructExtraParamsForLaunchBillingFlow.putString(BillingHelper.EXTRA_PARAM_KEY_SKU_DETAILS_TOKEN, skuDetails.getSkuDetailsToken());
                    }
                    if (z2) {
                        constructExtraParamsForLaunchBillingFlow.putString(BillingFlowParams.EXTRA_PARAM_KEY_RSKU, skuDetails.rewardToken());
                        int i2 = this.zzf;
                        if (i2 != 0) {
                            constructExtraParamsForLaunchBillingFlow.putInt(BillingFlowParams.EXTRA_PARAM_CHILD_DIRECTED, i2);
                        }
                        int i3 = this.zzg;
                        if (i3 != 0) {
                            constructExtraParamsForLaunchBillingFlow.putInt(BillingFlowParams.EXTRA_PARAM_UNDER_AGE_OF_CONSENT, i3);
                        }
                    }
                    if (this.zzn) {
                        i = 9;
                    } else {
                        i = billingFlowParams.getVrPurchaseFlow() ? 7 : 6;
                    }
                    final String str = sku;
                    future = zza(new Callable<Bundle>() {
                        public Bundle call() throws Exception {
                            return BillingClientImpl.this.zzh.zza(i, BillingClientImpl.this.zze.getPackageName(), str, skuType, (String) null, constructExtraParamsForLaunchBillingFlow);
                        }
                    }, 5000, null);
                } else if (z) {
                    future = zza(new Callable<Bundle>() {
                        public Bundle call() throws Exception {
                            return BillingClientImpl.this.zzh.zza(5, BillingClientImpl.this.zze.getPackageName(), Arrays.asList(billingFlowParams.getOldSku()), sku, BillingClient.SkuType.SUBS, (String) null);
                        }
                    }, 5000, null);
                } else {
                    future = zza(new Callable<Bundle>() {
                        public Bundle call() throws Exception {
                            return BillingClientImpl.this.zzh.zza(3, BillingClientImpl.this.zze.getPackageName(), sku, skuType, (String) null);
                        }
                    }, 5000, null);
                }
                try {
                    Bundle bundle = (Bundle) future.get(5000, TimeUnit.MILLISECONDS);
                    int responseCodeFromBundle = BillingHelper.getResponseCodeFromBundle(bundle, "BillingClient");
                    String debugMessageFromBundle = BillingHelper.getDebugMessageFromBundle(bundle, "BillingClient");
                    if (responseCodeFromBundle != 0) {
                        StringBuilder sb2 = new StringBuilder(52);
                        sb2.append("Unable to buy item, Error response code: ");
                        sb2.append(responseCodeFromBundle);
                        BillingHelper.logWarn("BillingClient", sb2.toString());
                        return zza(BillingResult.newBuilder().setResponseCode(responseCodeFromBundle).setDebugMessage(debugMessageFromBundle).build());
                    }
                    Intent intent = new Intent(activity, ProxyBillingActivity.class);
                    intent.putExtra("result_receiver", this.zzr);
                    intent.putExtra(BillingHelper.RESPONSE_BUY_INTENT_KEY, (PendingIntent) bundle.getParcelable(BillingHelper.RESPONSE_BUY_INTENT_KEY));
                    activity.startActivity(intent);
                    return BillingResults.OK;
                } catch (CancellationException | TimeoutException unused) {
                    StringBuilder sb3 = new StringBuilder(String.valueOf(sku).length() + 68);
                    sb3.append("Time out while launching billing flow: ; for sku: ");
                    sb3.append(sku);
                    sb3.append("; try to reconnect");
                    BillingHelper.logWarn("BillingClient", sb3.toString());
                    return zza(BillingResults.SERVICE_TIMEOUT);
                } catch (Exception unused2) {
                    StringBuilder sb4 = new StringBuilder(String.valueOf(sku).length() + 69);
                    sb4.append("Exception while launching billing flow: ; for sku: ");
                    sb4.append(sku);
                    sb4.append("; try to reconnect");
                    BillingHelper.logWarn("BillingClient", sb4.toString());
                    return zza(BillingResults.SERVICE_DISCONNECTED);
                }
            } else {
                BillingHelper.logWarn("BillingClient", "Current client doesn't support extra params for buy intent.");
                return zza(BillingResults.EXTRA_PARAMS_NOT_SUPPORTED);
            }
        } else {
            BillingHelper.logWarn("BillingClient", "Current client doesn't support subscriptions.");
            return zza(BillingResults.SUBSCRIPTIONS_NOT_SUPPORTED);
        }
    }

    private final BillingResult zza(BillingResult billingResult) {
        this.zzd.getListener().onPurchasesUpdated(billingResult, null);
        return billingResult;
    }

    public Purchase.PurchasesResult queryPurchases(final String str) {
        if (!isReady()) {
            return new Purchase.PurchasesResult(BillingResults.SERVICE_DISCONNECTED, null);
        }
        if (TextUtils.isEmpty(str)) {
            BillingHelper.logWarn("BillingClient", "Please provide a valid SKU type.");
            return new Purchase.PurchasesResult(BillingResults.EMPTY_SKU_TYPE, null);
        }
        try {
            return (Purchase.PurchasesResult) zza(new Callable<Purchase.PurchasesResult>() {
                public Purchase.PurchasesResult call() throws Exception {
                    return BillingClientImpl.this.zzc(str);
                }
            }, 5000, null).get(5000, TimeUnit.MILLISECONDS);
        } catch (CancellationException | TimeoutException unused) {
            return new Purchase.PurchasesResult(BillingResults.SERVICE_TIMEOUT, null);
        } catch (Exception unused2) {
            return new Purchase.PurchasesResult(BillingResults.INTERNAL_ERROR, null);
        }
    }

    public void querySkuDetailsAsync(SkuDetailsParams skuDetailsParams, final SkuDetailsResponseListener skuDetailsResponseListener) {
        if (!isReady()) {
            skuDetailsResponseListener.onSkuDetailsResponse(BillingResults.SERVICE_DISCONNECTED, null);
            return;
        }
        final String skuType = skuDetailsParams.getSkuType();
        final List<String> skusList = skuDetailsParams.getSkusList();
        if (TextUtils.isEmpty(skuType)) {
            BillingHelper.logWarn("BillingClient", "Please fix the input params. SKU type can't be empty.");
            skuDetailsResponseListener.onSkuDetailsResponse(BillingResults.EMPTY_SKU_TYPE, null);
        } else if (skusList == null) {
            BillingHelper.logWarn("BillingClient", "Please fix the input params. The list of SKUs can't be empty.");
            skuDetailsResponseListener.onSkuDetailsResponse(BillingResults.EMPTY_SKU_LIST, null);
        } else if (zza(new Callable<Void>() {
            public Void call() {
                final SkuDetails.SkuDetailsResult querySkuDetailsInternal = BillingClientImpl.this.querySkuDetailsInternal(skuType, skusList);
                BillingClientImpl.this.zza(new Runnable() {
                    public void run() {
                        skuDetailsResponseListener.onSkuDetailsResponse(BillingResult.newBuilder().setResponseCode(querySkuDetailsInternal.getResponseCode()).setDebugMessage(querySkuDetailsInternal.getDebugMessage()).build(), querySkuDetailsInternal.getSkuDetailsList());
                    }
                });
                return null;
            }
        }, 30000, new Runnable(this) {
            public void run() {
                skuDetailsResponseListener.onSkuDetailsResponse(BillingResults.SERVICE_TIMEOUT, null);
            }
        }) == null) {
            skuDetailsResponseListener.onSkuDetailsResponse(zzc(), null);
        }
    }

    public void consumeAsync(final ConsumeParams consumeParams, final ConsumeResponseListener consumeResponseListener) {
        if (!isReady()) {
            consumeResponseListener.onConsumeResponse(BillingResults.SERVICE_DISCONNECTED, null);
        } else if (zza(new Callable<Void>() {
            public Void call() {
                BillingClientImpl.this.zza(consumeParams, consumeResponseListener);
                return null;
            }
        }, 30000, new Runnable(this) {
            public void run() {
                consumeResponseListener.onConsumeResponse(BillingResults.SERVICE_TIMEOUT, null);
            }
        }) == null) {
            consumeResponseListener.onConsumeResponse(zzc(), null);
        }
    }

    public void queryPurchaseHistoryAsync(final String str, final PurchaseHistoryResponseListener purchaseHistoryResponseListener) {
        if (!isReady()) {
            purchaseHistoryResponseListener.onPurchaseHistoryResponse(BillingResults.SERVICE_DISCONNECTED, null);
        } else if (zza(new Callable<Void>() {
            public Void call() {
                final PurchaseHistoryResult access$700 = BillingClientImpl.this.zza(str);
                BillingClientImpl.this.zza(new Runnable() {
                    public void run() {
                        purchaseHistoryResponseListener.onPurchaseHistoryResponse(access$700.getBillingResult(), access$700.getPurchaseHistoryRecordList());
                    }
                });
                return null;
            }
        }, 30000, new Runnable(this) {
            public void run() {
                purchaseHistoryResponseListener.onPurchaseHistoryResponse(BillingResults.SERVICE_TIMEOUT, null);
            }
        }) == null) {
            purchaseHistoryResponseListener.onPurchaseHistoryResponse(zzc(), null);
        }
    }

    /* access modifiers changed from: private */
    public final PurchaseHistoryResult zza(String str) {
        String valueOf = String.valueOf(str);
        BillingHelper.logVerbose("BillingClient", valueOf.length() != 0 ? "Querying purchase history, item type: ".concat(valueOf) : new String("Querying purchase history, item type: "));
        ArrayList arrayList = new ArrayList();
        Bundle constructExtraParamsForQueryPurchases = BillingHelper.constructExtraParamsForQueryPurchases(this.zzn, this.zzp, this.zzb);
        String str2 = null;
        while (this.zzl) {
            try {
                Bundle zza2 = this.zzh.zza(6, this.zze.getPackageName(), str, str2, constructExtraParamsForQueryPurchases);
                BillingResult checkPurchasesBundleValidity = PurchaseApiResponseChecker.checkPurchasesBundleValidity(zza2, "BillingClient", "getPurchaseHistory()");
                if (checkPurchasesBundleValidity != BillingResults.OK) {
                    return new PurchaseHistoryResult(checkPurchasesBundleValidity, null);
                }
                ArrayList<String> stringArrayList = zza2.getStringArrayList(BillingHelper.RESPONSE_INAPP_ITEM_LIST);
                ArrayList<String> stringArrayList2 = zza2.getStringArrayList(BillingHelper.RESPONSE_INAPP_PURCHASE_DATA_LIST);
                ArrayList<String> stringArrayList3 = zza2.getStringArrayList(BillingHelper.RESPONSE_INAPP_SIGNATURE_LIST);
                int i = 0;
                while (i < stringArrayList2.size()) {
                    String str3 = stringArrayList2.get(i);
                    String str4 = stringArrayList3.get(i);
                    String valueOf2 = String.valueOf(stringArrayList.get(i));
                    BillingHelper.logVerbose("BillingClient", valueOf2.length() != 0 ? "Purchase record found for sku : ".concat(valueOf2) : new String("Purchase record found for sku : "));
                    try {
                        PurchaseHistoryRecord purchaseHistoryRecord = new PurchaseHistoryRecord(str3, str4);
                        if (TextUtils.isEmpty(purchaseHistoryRecord.getPurchaseToken())) {
                            BillingHelper.logWarn("BillingClient", "BUG: empty/null token!");
                        }
                        arrayList.add(purchaseHistoryRecord);
                        i++;
                    } catch (JSONException e) {
                        String valueOf3 = String.valueOf(e);
                        StringBuilder sb = new StringBuilder(String.valueOf(valueOf3).length() + 48);
                        sb.append("Got an exception trying to decode the purchase: ");
                        sb.append(valueOf3);
                        BillingHelper.logWarn("BillingClient", sb.toString());
                        return new PurchaseHistoryResult(BillingResults.INTERNAL_ERROR, null);
                    }
                }
                str2 = zza2.getString(BillingHelper.INAPP_CONTINUATION_TOKEN);
                String valueOf4 = String.valueOf(str2);
                BillingHelper.logVerbose("BillingClient", valueOf4.length() != 0 ? "Continuation token: ".concat(valueOf4) : new String("Continuation token: "));
                if (TextUtils.isEmpty(str2)) {
                    return new PurchaseHistoryResult(BillingResults.OK, arrayList);
                }
            } catch (RemoteException e2) {
                String valueOf5 = String.valueOf(e2);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf5).length() + 64);
                sb2.append("Got exception trying to get purchase history: ");
                sb2.append(valueOf5);
                sb2.append("; try to reconnect");
                BillingHelper.logWarn("BillingClient", sb2.toString());
                return new PurchaseHistoryResult(BillingResults.SERVICE_DISCONNECTED, null);
            }
        }
        BillingHelper.logWarn("BillingClient", "getPurchaseHistory is not supported on current device");
        return new PurchaseHistoryResult(BillingResults.GET_PURCHASE_HISTORY_NOT_SUPPORTED, null);
    }

    public void loadRewardedSku(final RewardLoadParams rewardLoadParams, final RewardResponseListener rewardResponseListener) {
        if (!this.zzl) {
            rewardResponseListener.onRewardResponse(BillingResults.ITEM_UNAVAILABLE);
        } else if (zza(new Callable<Void>() {
            public Void call() {
                try {
                    Bundle zza = BillingClientImpl.this.zzh.zza(6, BillingClientImpl.this.zze.getPackageName(), rewardLoadParams.getSkuDetails().getSku(), rewardLoadParams.getSkuDetails().getType(), (String) null, BillingHelper.constructExtraParamsForLoadRewardedSku(rewardLoadParams.getSkuDetails().rewardToken(), BillingClientImpl.this.zzf, BillingClientImpl.this.zzg, BillingClientImpl.this.zzb));
                    final BillingResult build = BillingResult.newBuilder().setResponseCode(BillingHelper.getResponseCodeFromBundle(zza, "BillingClient")).setDebugMessage(BillingHelper.getDebugMessageFromBundle(zza, "BillingClient")).build();
                    BillingClientImpl.this.zza(new Runnable() {
                        public void run() {
                            rewardResponseListener.onRewardResponse(build);
                        }
                    });
                    return null;
                } catch (Exception unused) {
                    BillingClientImpl.this.zza(new Runnable() {
                        public void run() {
                            rewardResponseListener.onRewardResponse(BillingResults.INTERNAL_ERROR);
                        }
                    });
                    return null;
                }
            }
        }, 30000, new Runnable(this) {
            public void run() {
                rewardResponseListener.onRewardResponse(BillingResults.SERVICE_TIMEOUT);
            }
        }) == null) {
            rewardResponseListener.onRewardResponse(zzc());
        }
    }

    public void acknowledgePurchase(final AcknowledgePurchaseParams acknowledgePurchaseParams, final AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener) {
        if (!isReady()) {
            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(BillingResults.SERVICE_DISCONNECTED);
        } else if (TextUtils.isEmpty(acknowledgePurchaseParams.getPurchaseToken())) {
            BillingHelper.logWarn("BillingClient", "Please provide a valid purchase token.");
            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(BillingResults.INVALID_PURCHASE_TOKEN);
        } else if (!this.zzn) {
            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(BillingResults.API_VERSION_NOT_V9);
        } else if (zza(new Callable<Void>() {
            public Void call() {
                try {
                    Bundle zzd = BillingClientImpl.this.zzh.zzd(9, BillingClientImpl.this.zze.getPackageName(), acknowledgePurchaseParams.getPurchaseToken(), BillingHelper.constructExtraParamsForAcknowledgePurchase(acknowledgePurchaseParams, BillingClientImpl.this.zzb));
                    final int responseCodeFromBundle = BillingHelper.getResponseCodeFromBundle(zzd, "BillingClient");
                    final String debugMessageFromBundle = BillingHelper.getDebugMessageFromBundle(zzd, "BillingClient");
                    BillingClientImpl.this.zza(new Runnable() {
                        public void run() {
                            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(BillingResult.newBuilder().setResponseCode(responseCodeFromBundle).setDebugMessage(debugMessageFromBundle).build());
                        }
                    });
                    return null;
                } catch (Exception e) {
                    BillingClientImpl.this.zza(new Runnable() {
                        public void run() {
                            String valueOf = String.valueOf(e);
                            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 32);
                            sb.append("Error acknowledge purchase; ex: ");
                            sb.append(valueOf);
                            BillingHelper.logWarn("BillingClient", sb.toString());
                            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(BillingResults.SERVICE_DISCONNECTED);
                        }
                    });
                    return null;
                }
            }
        }, 30000, new Runnable(this) {
            public void run() {
                acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(BillingResults.SERVICE_TIMEOUT);
            }
        }) == null) {
            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(zzc());
        }
    }

    /* access modifiers changed from: private */
    public final <T> Future<T> zza(Callable<T> callable, long j, final Runnable runnable) {
        double d = (double) j;
        Double.isNaN(d);
        long j2 = (long) (d * 0.95d);
        if (this.zzq == null) {
            this.zzq = Executors.newFixedThreadPool(BillingHelper.NUMBER_OF_CORES);
        }
        try {
            final Future<T> submit = this.zzq.submit(callable);
            this.zzc.postDelayed(new Runnable(this) {
                public void run() {
                    if (!submit.isDone() && !submit.isCancelled()) {
                        submit.cancel(true);
                        BillingHelper.logWarn("BillingClient", "Async task is taking too long, cancel it!");
                        Runnable runnable = runnable;
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                }
            }, j2);
            return submit;
        } catch (Exception e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 28);
            sb.append("Async task throws exception ");
            sb.append(valueOf);
            BillingHelper.logWarn("BillingClient", sb.toString());
            return null;
        }
    }

    private final BillingResult zzb(final String str) {
        try {
            if (((Integer) zza(new Callable<Integer>() {
                public Integer call() throws Exception {
                    return Integer.valueOf(BillingClientImpl.this.zzh.zzb(7, BillingClientImpl.this.zze.getPackageName(), str, BillingClientImpl.zzb()));
                }
            }, 5000, null).get(5000, TimeUnit.MILLISECONDS)).intValue() == 0) {
                return BillingResults.OK;
            }
            return BillingResults.FEATURE_NOT_SUPPORTED;
        } catch (Exception unused) {
            BillingHelper.logWarn("BillingClient", "Exception while checking if billing is supported; try to reconnect");
            return BillingResults.SERVICE_DISCONNECTED;
        }
    }

    /* access modifiers changed from: private */
    public static Bundle zzb() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(BillingFlowParams.EXTRA_PARAM_KEY_VR, true);
        return bundle;
    }

    /* access modifiers changed from: package-private */
    public SkuDetails.SkuDetailsResult querySkuDetailsInternal(String str, List<String> list) {
        Bundle bundle;
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        int i = 0;
        while (i < size) {
            int i2 = i + 20;
            ArrayList arrayList2 = new ArrayList(list.subList(i, i2 > size ? size : i2));
            Bundle bundle2 = new Bundle();
            bundle2.putStringArrayList("ITEM_ID_LIST", arrayList2);
            bundle2.putString(BillingHelper.LIBRARY_VERSION_KEY, this.zzb);
            try {
                if (this.zzo) {
                    bundle = this.zzh.zza(10, this.zze.getPackageName(), str, bundle2, BillingHelper.constructExtraParamsForGetSkuDetails(this.zzn, this.zzp, this.zzb));
                    String str2 = str;
                } else {
                    bundle = this.zzh.zza(3, this.zze.getPackageName(), str, bundle2);
                }
                if (bundle == null) {
                    BillingHelper.logWarn("BillingClient", "querySkuDetailsAsync got null sku details list");
                    return new SkuDetails.SkuDetailsResult(4, "Null sku details list", null);
                } else if (!bundle.containsKey(BillingHelper.RESPONSE_GET_SKU_DETAILS_LIST)) {
                    int responseCodeFromBundle = BillingHelper.getResponseCodeFromBundle(bundle, "BillingClient");
                    String debugMessageFromBundle = BillingHelper.getDebugMessageFromBundle(bundle, "BillingClient");
                    if (responseCodeFromBundle != 0) {
                        StringBuilder sb = new StringBuilder(50);
                        sb.append("getSkuDetails() failed. Response code: ");
                        sb.append(responseCodeFromBundle);
                        BillingHelper.logWarn("BillingClient", sb.toString());
                        return new SkuDetails.SkuDetailsResult(responseCodeFromBundle, debugMessageFromBundle, arrayList);
                    }
                    BillingHelper.logWarn("BillingClient", "getSkuDetails() returned a bundle with neither an error nor a detail list.");
                    return new SkuDetails.SkuDetailsResult(6, debugMessageFromBundle, arrayList);
                } else {
                    ArrayList<String> stringArrayList = bundle.getStringArrayList(BillingHelper.RESPONSE_GET_SKU_DETAILS_LIST);
                    if (stringArrayList == null) {
                        BillingHelper.logWarn("BillingClient", "querySkuDetailsAsync got null response list");
                        return new SkuDetails.SkuDetailsResult(4, "querySkuDetailsAsync got null response list", null);
                    }
                    int i3 = 0;
                    while (i3 < stringArrayList.size()) {
                        try {
                            SkuDetails skuDetails = new SkuDetails(stringArrayList.get(i3));
                            String valueOf = String.valueOf(skuDetails);
                            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 17);
                            sb2.append("Got sku details: ");
                            sb2.append(valueOf);
                            BillingHelper.logVerbose("BillingClient", sb2.toString());
                            arrayList.add(skuDetails);
                            i3++;
                        } catch (JSONException unused) {
                            BillingHelper.logWarn("BillingClient", "Got a JSON exception trying to decode SkuDetails.");
                            return new SkuDetails.SkuDetailsResult(6, "Error trying to decode SkuDetails.", null);
                        }
                    }
                    i = i2;
                }
            } catch (Exception e) {
                String valueOf2 = String.valueOf(e);
                StringBuilder sb3 = new StringBuilder("querySkuDetailsAsync got a remote exception (try to reconnect).".length() + String.valueOf(valueOf2).length());
                sb3.append("querySkuDetailsAsync got a remote exception (try to reconnect).");
                sb3.append(valueOf2);
                BillingHelper.logWarn("BillingClient", sb3.toString());
                return new SkuDetails.SkuDetailsResult(-1, "Service connection is disconnected.", null);
            }
        }
        return new SkuDetails.SkuDetailsResult(0, "", arrayList);
    }

    /* access modifiers changed from: private */
    public final Purchase.PurchasesResult zzc(String str) {
        Bundle bundle;
        String valueOf = String.valueOf(str);
        BillingHelper.logVerbose("BillingClient", valueOf.length() != 0 ? "Querying owned items, item type: ".concat(valueOf) : new String("Querying owned items, item type: "));
        ArrayList arrayList = new ArrayList();
        Bundle constructExtraParamsForQueryPurchases = BillingHelper.constructExtraParamsForQueryPurchases(this.zzn, this.zzp, this.zzb);
        String str2 = null;
        do {
            try {
                if (this.zzn) {
                    bundle = this.zzh.zzc(9, this.zze.getPackageName(), str, str2, constructExtraParamsForQueryPurchases);
                } else {
                    bundle = this.zzh.zza(3, this.zze.getPackageName(), str, str2);
                }
                BillingResult checkPurchasesBundleValidity = PurchaseApiResponseChecker.checkPurchasesBundleValidity(bundle, "BillingClient", "getPurchase()");
                if (checkPurchasesBundleValidity != BillingResults.OK) {
                    return new Purchase.PurchasesResult(checkPurchasesBundleValidity, null);
                }
                ArrayList<String> stringArrayList = bundle.getStringArrayList(BillingHelper.RESPONSE_INAPP_ITEM_LIST);
                ArrayList<String> stringArrayList2 = bundle.getStringArrayList(BillingHelper.RESPONSE_INAPP_PURCHASE_DATA_LIST);
                ArrayList<String> stringArrayList3 = bundle.getStringArrayList(BillingHelper.RESPONSE_INAPP_SIGNATURE_LIST);
                int i = 0;
                while (i < stringArrayList2.size()) {
                    String str3 = stringArrayList2.get(i);
                    String str4 = stringArrayList3.get(i);
                    String valueOf2 = String.valueOf(stringArrayList.get(i));
                    BillingHelper.logVerbose("BillingClient", valueOf2.length() != 0 ? "Sku is owned: ".concat(valueOf2) : new String("Sku is owned: "));
                    try {
                        Purchase purchase = new Purchase(str3, str4);
                        if (TextUtils.isEmpty(purchase.getPurchaseToken())) {
                            BillingHelper.logWarn("BillingClient", "BUG: empty/null token!");
                        }
                        arrayList.add(purchase);
                        i++;
                    } catch (JSONException e) {
                        String valueOf3 = String.valueOf(e);
                        StringBuilder sb = new StringBuilder(String.valueOf(valueOf3).length() + 48);
                        sb.append("Got an exception trying to decode the purchase: ");
                        sb.append(valueOf3);
                        BillingHelper.logWarn("BillingClient", sb.toString());
                        return new Purchase.PurchasesResult(BillingResults.INTERNAL_ERROR, null);
                    }
                }
                str2 = bundle.getString(BillingHelper.INAPP_CONTINUATION_TOKEN);
                String valueOf4 = String.valueOf(str2);
                BillingHelper.logVerbose("BillingClient", valueOf4.length() != 0 ? "Continuation token: ".concat(valueOf4) : new String("Continuation token: "));
            } catch (Exception e2) {
                String valueOf5 = String.valueOf(e2);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf5).length() + 57);
                sb2.append("Got exception trying to get purchases: ");
                sb2.append(valueOf5);
                sb2.append("; try to reconnect");
                BillingHelper.logWarn("BillingClient", sb2.toString());
                return new Purchase.PurchasesResult(BillingResults.SERVICE_DISCONNECTED, null);
            }
        } while (!TextUtils.isEmpty(str2));
        return new Purchase.PurchasesResult(BillingResults.OK, arrayList);
    }

    /* access modifiers changed from: private */
    public final void zza(Runnable runnable) {
        if (!Thread.interrupted()) {
            this.zzc.post(runnable);
        }
    }

    /* access modifiers changed from: private */
    public final void zza(ConsumeParams consumeParams, final ConsumeResponseListener consumeResponseListener) {
        String str;
        final int i;
        final String purchaseToken = consumeParams.getPurchaseToken();
        try {
            String valueOf = String.valueOf(purchaseToken);
            BillingHelper.logVerbose("BillingClient", valueOf.length() != 0 ? "Consuming purchase with token: ".concat(valueOf) : new String("Consuming purchase with token: "));
            if (this.zzn) {
                Bundle zzc2 = this.zzh.zzc(9, this.zze.getPackageName(), purchaseToken, BillingHelper.constructExtraParamsForConsume(consumeParams, this.zzn, this.zzb));
                int i2 = zzc2.getInt(BillingHelper.RESPONSE_CODE);
                str = BillingHelper.getDebugMessageFromBundle(zzc2, "BillingClient");
                i = i2;
            } else {
                i = this.zzh.zzb(3, this.zze.getPackageName(), purchaseToken);
                str = "";
            }
            final BillingResult build = BillingResult.newBuilder().setResponseCode(i).setDebugMessage(str).build();
            if (i == 0) {
                zza(new Runnable(this) {
                    public void run() {
                        BillingHelper.logVerbose("BillingClient", "Successfully consumed purchase.");
                        consumeResponseListener.onConsumeResponse(build, purchaseToken);
                    }
                });
                return;
            }
            final ConsumeResponseListener consumeResponseListener2 = consumeResponseListener;
            final String str2 = purchaseToken;
            zza(new Runnable(this) {
                public void run() {
                    int i = i;
                    StringBuilder sb = new StringBuilder(63);
                    sb.append("Error consuming purchase with token. Response code: ");
                    sb.append(i);
                    BillingHelper.logWarn("BillingClient", sb.toString());
                    consumeResponseListener2.onConsumeResponse(build, str2);
                }
            });
        } catch (Exception e) {
            zza(new Runnable(this) {
                public void run() {
                    String valueOf = String.valueOf(e);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 30);
                    sb.append("Error consuming purchase; ex: ");
                    sb.append(valueOf);
                    BillingHelper.logWarn("BillingClient", sb.toString());
                    consumeResponseListener.onConsumeResponse(BillingResults.SERVICE_DISCONNECTED, purchaseToken);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public final BillingResult zzc() {
        int i = this.zza;
        if (i == 0 || i == 3) {
            return BillingResults.SERVICE_DISCONNECTED;
        }
        return BillingResults.INTERNAL_ERROR;
    }
}
