package com.android.billingclient.api;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
public interface RewardResponseListener {
    void onRewardResponse(BillingResult billingResult);
}
