package com.android.billingclient.api;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
public final class AcknowledgePurchaseParams {
    /* access modifiers changed from: private */
    public String zza;
    /* access modifiers changed from: private */
    public String zzb;

    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    public static final class Builder {
        private String zza;
        private String zzb;

        private Builder() {
        }

        public final Builder setPurchaseToken(String str) {
            this.zzb = str;
            return this;
        }

        public final Builder setDeveloperPayload(String str) {
            this.zza = str;
            return this;
        }

        public final AcknowledgePurchaseParams build() {
            AcknowledgePurchaseParams acknowledgePurchaseParams = new AcknowledgePurchaseParams();
            String unused = acknowledgePurchaseParams.zza = this.zza;
            String unused2 = acknowledgePurchaseParams.zzb = this.zzb;
            return acknowledgePurchaseParams;
        }
    }

    private AcknowledgePurchaseParams() {
    }

    public final String getDeveloperPayload() {
        return this.zza;
    }

    public final String getPurchaseToken() {
        return this.zzb;
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
