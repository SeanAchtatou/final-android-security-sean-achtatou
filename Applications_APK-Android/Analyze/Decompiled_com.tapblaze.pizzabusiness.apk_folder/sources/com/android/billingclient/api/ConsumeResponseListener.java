package com.android.billingclient.api;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
public interface ConsumeResponseListener {
    void onConsumeResponse(BillingResult billingResult, String str);
}
