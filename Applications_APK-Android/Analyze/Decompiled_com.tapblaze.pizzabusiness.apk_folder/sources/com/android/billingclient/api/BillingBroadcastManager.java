package com.android.billingclient.api;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.android.billingclient.util.BillingHelper;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
class BillingBroadcastManager {
    private final Context zza;
    /* access modifiers changed from: private */
    public final BillingBroadcastReceiver zzb;

    BillingBroadcastManager(Context context, PurchasesUpdatedListener purchasesUpdatedListener) {
        this.zza = context;
        this.zzb = new BillingBroadcastReceiver(purchasesUpdatedListener);
    }

    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    private class BillingBroadcastReceiver extends BroadcastReceiver {
        /* access modifiers changed from: private */
        public final PurchasesUpdatedListener zza;
        private boolean zzb;

        private BillingBroadcastReceiver(PurchasesUpdatedListener purchasesUpdatedListener) {
            this.zza = purchasesUpdatedListener;
        }

        public void register(Context context, IntentFilter intentFilter) {
            if (!this.zzb) {
                context.registerReceiver(BillingBroadcastManager.this.zzb, intentFilter);
                this.zzb = true;
            }
        }

        public void unRegister(Context context) {
            if (this.zzb) {
                context.unregisterReceiver(BillingBroadcastManager.this.zzb);
                this.zzb = false;
                return;
            }
            BillingHelper.logWarn("BillingBroadcastManager", "Receiver is not registered.");
        }

        public void onReceive(Context context, Intent intent) {
            this.zza.onPurchasesUpdated(BillingHelper.getBillingResultFromIntent(intent, "BillingBroadcastManager"), BillingHelper.extractPurchases(intent.getExtras()));
        }
    }

    /* access modifiers changed from: package-private */
    public void registerReceiver() {
        this.zzb.register(this.zza, new IntentFilter("com.android.vending.billing.PURCHASES_UPDATED"));
    }

    /* access modifiers changed from: package-private */
    public PurchasesUpdatedListener getListener() {
        return this.zzb.zza;
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        this.zzb.unRegister(this.zza);
    }
}
