package com.android.billingclient.api;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
public class RewardLoadParams {
    /* access modifiers changed from: private */
    public SkuDetails zza;

    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    public static class Builder {
        private SkuDetails zza;

        public Builder setSkuDetails(SkuDetails skuDetails) {
            this.zza = skuDetails;
            return this;
        }

        public RewardLoadParams build() {
            if (this.zza != null) {
                RewardLoadParams rewardLoadParams = new RewardLoadParams();
                SkuDetails unused = rewardLoadParams.zza = this.zza;
                return rewardLoadParams;
            }
            throw new IllegalArgumentException("SkuDetails must be set");
        }
    }

    public SkuDetails getSkuDetails() {
        return this.zza;
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
