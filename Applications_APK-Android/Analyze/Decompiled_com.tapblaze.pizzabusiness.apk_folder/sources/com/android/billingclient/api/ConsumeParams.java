package com.android.billingclient.api;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
public final class ConsumeParams {
    /* access modifiers changed from: private */
    public String zza;
    /* access modifiers changed from: private */
    public String zzb;

    /* compiled from: com.android.billingclient:billing@@2.1.0 */
    public static final class Builder {
        private String zza;
        private String zzb;

        private Builder() {
        }

        public final Builder setPurchaseToken(String str) {
            this.zzb = str;
            return this;
        }

        public final Builder setDeveloperPayload(String str) {
            this.zza = str;
            return this;
        }

        public final ConsumeParams build() {
            ConsumeParams consumeParams = new ConsumeParams();
            String unused = consumeParams.zza = this.zzb;
            String unused2 = consumeParams.zzb = this.zza;
            return consumeParams;
        }
    }

    private ConsumeParams() {
    }

    public final String getDeveloperPayload() {
        return this.zzb;
    }

    public final String getPurchaseToken() {
        return this.zza;
    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
