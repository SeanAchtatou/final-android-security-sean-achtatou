package com.android.billingclient.api;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.ResultReceiver;
import com.android.billingclient.util.BillingHelper;

/* compiled from: com.android.billingclient:billing@@2.1.0 */
public class ProxyBillingActivity extends Activity {
    static final String KEY_RESULT_RECEIVER = "result_receiver";
    private ResultReceiver zza;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        PendingIntent pendingIntent;
        super.onCreate(bundle);
        if (bundle == null) {
            BillingHelper.logVerbose("ProxyBillingActivity", "Launching Play Store billing flow");
            this.zza = (ResultReceiver) getIntent().getParcelableExtra(KEY_RESULT_RECEIVER);
            if (getIntent().hasExtra(BillingHelper.RESPONSE_BUY_INTENT_KEY)) {
                pendingIntent = (PendingIntent) getIntent().getParcelableExtra(BillingHelper.RESPONSE_BUY_INTENT_KEY);
            } else {
                pendingIntent = getIntent().hasExtra(BillingHelper.RESPONSE_SUBS_MANAGEMENT_INTENT_KEY) ? (PendingIntent) getIntent().getParcelableExtra(BillingHelper.RESPONSE_SUBS_MANAGEMENT_INTENT_KEY) : null;
            }
            try {
                startIntentSenderForResult(pendingIntent.getIntentSender(), 100, new Intent(), 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 53);
                sb.append("Got exception while trying to start a purchase flow: ");
                sb.append(valueOf);
                BillingHelper.logWarn("ProxyBillingActivity", sb.toString());
                this.zza.send(6, null);
                finish();
            }
        } else {
            BillingHelper.logVerbose("ProxyBillingActivity", "Launching Play Store billing flow from savedInstanceState");
            this.zza = (ResultReceiver) bundle.getParcelable(KEY_RESULT_RECEIVER);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putParcelable(KEY_RESULT_RECEIVER, this.zza);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 100) {
            int responseCodeFromIntent = BillingHelper.getResponseCodeFromIntent(intent, "ProxyBillingActivity");
            if (!(i2 == -1 && responseCodeFromIntent == 0)) {
                StringBuilder sb = new StringBuilder(85);
                sb.append("Activity finished with resultCode ");
                sb.append(i2);
                sb.append(" and billing's responseCode: ");
                sb.append(responseCodeFromIntent);
                BillingHelper.logWarn("ProxyBillingActivity", sb.toString());
            }
            this.zza.send(responseCodeFromIntent, intent == null ? null : intent.getExtras());
        } else {
            StringBuilder sb2 = new StringBuilder(69);
            sb2.append("Got onActivityResult with wrong requestCode: ");
            sb2.append(i);
            sb2.append("; skipping...");
            BillingHelper.logWarn("ProxyBillingActivity", sb2.toString());
        }
        finish();
    }
}
