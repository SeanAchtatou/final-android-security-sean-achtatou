package com.unity3d.services.banners;

import android.os.ConditionVariable;
import com.ironsource.sdk.constants.Constants;
import com.unity3d.services.ads.properties.AdsProperties;
import com.unity3d.services.core.webview.WebViewApp;
import com.unity3d.services.core.webview.bridge.CallbackStatus;
import java.lang.reflect.Method;

public class BannerShow {
    private static ConditionVariable _waitShowStatus;

    public static synchronized boolean show(String str) throws NoSuchMethodException {
        boolean block;
        Class<BannerShow> cls = BannerShow.class;
        synchronized (cls) {
            Method method = cls.getMethod("showCallback", CallbackStatus.class);
            _waitShowStatus = new ConditionVariable();
            WebViewApp.getCurrentApp().invokeMethod(Constants.ParametersKeys.WEB_VIEW, "showBanner", method, str);
            block = _waitShowStatus.block((long) AdsProperties.getShowTimeout());
            _waitShowStatus = null;
        }
        return block;
    }

    public static void showCallback(CallbackStatus callbackStatus) {
        if (_waitShowStatus != null && callbackStatus.equals(CallbackStatus.OK)) {
            _waitShowStatus.open();
        }
    }
}
