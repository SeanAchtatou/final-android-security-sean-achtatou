package com.unity3d.services.ads.api;

import android.view.View;
import com.unity3d.services.ads.adunit.IAdUnitViewHandler;
import com.unity3d.services.ads.webplayer.WebPlayerError;
import com.unity3d.services.banners.view.BannerView;
import com.unity3d.services.core.log.DeviceLog;
import com.unity3d.services.core.misc.Utilities;
import com.unity3d.services.core.webview.bridge.WebViewCallback;
import com.unity3d.services.core.webview.bridge.WebViewExposed;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebPlayer {
    private static JSONObject _webPlayerEventSettings;
    private static JSONObject _webPlayerSettings;
    private static JSONObject _webSettings;

    public static JSONObject getWebPlayerSettings() {
        return _webPlayerSettings;
    }

    public static JSONObject getWebSettings() {
        return _webSettings;
    }

    public static JSONObject getWebPlayerEventSettings() {
        return _webPlayerEventSettings;
    }

    @WebViewExposed
    public static void setUrl(final String str, String str2, WebViewCallback webViewCallback) {
        final com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str2);
        if (webPlayer != null) {
            Utilities.runOnUiThread(new Runnable() {
                public void run() {
                    webPlayer.loadUrl(str);
                }
            });
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    @WebViewExposed
    public static void setData(final String str, final String str2, final String str3, String str4, WebViewCallback webViewCallback) {
        final com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str4);
        if (webPlayer != null) {
            Utilities.runOnUiThread(new Runnable() {
                public void run() {
                    webPlayer.loadData(str, str2, str3);
                }
            });
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    @WebViewExposed
    public static void setDataWithUrl(String str, String str2, String str3, String str4, String str5, WebViewCallback webViewCallback) {
        final com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str5);
        if (webPlayer != null) {
            final String str6 = str;
            final String str7 = str2;
            final String str8 = str3;
            final String str9 = str4;
            Utilities.runOnUiThread(new Runnable() {
                public void run() {
                    webPlayer.loadDataWithBaseURL(str6, str7, str8, str9, null);
                }
            });
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030  */
    @com.unity3d.services.core.webview.bridge.WebViewExposed
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void setSettings(final org.json.JSONObject r4, final org.json.JSONObject r5, java.lang.String r6, com.unity3d.services.core.webview.bridge.WebViewCallback r7) {
        /*
            int r0 = r6.hashCode()
            r1 = -318269643(0xffffffffed079735, float:-2.6227047E27)
            r2 = 0
            r3 = 1
            if (r0 == r1) goto L_0x001b
            r1 = 1497041165(0x593b090d, float:3.29036076E15)
            if (r0 == r1) goto L_0x0011
            goto L_0x0025
        L_0x0011:
            java.lang.String r0 = "bannerplayer"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0025
            r6 = 0
            goto L_0x0026
        L_0x001b:
            java.lang.String r0 = "webplayer"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0025
            r6 = 1
            goto L_0x0026
        L_0x0025:
            r6 = -1
        L_0x0026:
            if (r6 == 0) goto L_0x0030
            if (r6 == r3) goto L_0x002b
            goto L_0x0038
        L_0x002b:
            com.unity3d.services.ads.api.WebPlayer._webSettings = r4
            com.unity3d.services.ads.api.WebPlayer._webPlayerSettings = r5
            goto L_0x0038
        L_0x0030:
            com.unity3d.services.ads.api.WebPlayer$4 r6 = new com.unity3d.services.ads.api.WebPlayer$4
            r6.<init>(r4, r5)
            com.unity3d.services.core.misc.Utilities.runOnUiThread(r6)
        L_0x0038:
            java.lang.Object[] r4 = new java.lang.Object[r2]
            r7.invoke(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.unity3d.services.ads.api.WebPlayer.setSettings(org.json.JSONObject, org.json.JSONObject, java.lang.String, com.unity3d.services.core.webview.bridge.WebViewCallback):void");
    }

    @WebViewExposed
    public static void setEventSettings(final JSONObject jSONObject, String str, WebViewCallback webViewCallback) {
        if (str.equals("webplayer")) {
            _webPlayerEventSettings = jSONObject;
        } else {
            Utilities.runOnUiThread(new Runnable() {
                public void run() {
                    com.unity3d.services.ads.webplayer.WebPlayer access$000 = WebPlayer.getBannerWebPlayer();
                    if (access$000 != null) {
                        access$000.setEventSettings(jSONObject);
                    } else {
                        BannerView.setWebPlayerEventSettings(jSONObject);
                    }
                }
            });
        }
        webViewCallback.invoke(new Object[0]);
    }

    @WebViewExposed
    public static void clearSettings(WebViewCallback webViewCallback) {
        _webSettings = null;
        _webPlayerSettings = null;
        _webPlayerEventSettings = null;
        webViewCallback.invoke(new Object[0]);
    }

    @WebViewExposed
    public static void getErroredSettings(String str, WebViewCallback webViewCallback) {
        com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str);
        if (webPlayer != null) {
            Map<String, String> erroredSettings = webPlayer.getErroredSettings();
            JSONObject jSONObject = new JSONObject();
            try {
                for (Map.Entry next : erroredSettings.entrySet()) {
                    jSONObject.put((String) next.getKey(), next.getValue());
                }
            } catch (Exception e) {
                DeviceLog.exception("Error forming JSON object", e);
            }
            webViewCallback.invoke(jSONObject);
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    @WebViewExposed
    public static void sendEvent(JSONArray jSONArray, String str, WebViewCallback webViewCallback) {
        com.unity3d.services.ads.webplayer.WebPlayer webPlayer = getWebPlayer(str);
        if (webPlayer != null) {
            webPlayer.sendEvent(jSONArray);
            webViewCallback.invoke(new Object[0]);
            return;
        }
        webViewCallback.error(WebPlayerError.WEBPLAYER_NULL, new Object[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.unity3d.services.ads.webplayer.WebPlayer getWebPlayer(java.lang.String r3) {
        /*
            int r0 = r3.hashCode()
            r1 = -318269643(0xffffffffed079735, float:-2.6227047E27)
            r2 = 1
            if (r0 == r1) goto L_0x001a
            r1 = 1497041165(0x593b090d, float:3.29036076E15)
            if (r0 == r1) goto L_0x0010
            goto L_0x0024
        L_0x0010:
            java.lang.String r0 = "bannerplayer"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0024
            r3 = 1
            goto L_0x0025
        L_0x001a:
            java.lang.String r0 = "webplayer"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0024
            r3 = 0
            goto L_0x0025
        L_0x0024:
            r3 = -1
        L_0x0025:
            if (r3 == 0) goto L_0x0030
            if (r3 == r2) goto L_0x002b
            r3 = 0
            return r3
        L_0x002b:
            com.unity3d.services.ads.webplayer.WebPlayer r3 = getBannerWebPlayer()
            return r3
        L_0x0030:
            com.unity3d.services.ads.webplayer.WebPlayer r3 = getAdUnitWebPlayer()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.unity3d.services.ads.api.WebPlayer.getWebPlayer(java.lang.String):com.unity3d.services.ads.webplayer.WebPlayer");
    }

    private static com.unity3d.services.ads.webplayer.WebPlayer getAdUnitWebPlayer() {
        IAdUnitViewHandler viewHandler;
        View view;
        if (AdUnit.getAdUnitActivity() == null || (viewHandler = AdUnit.getAdUnitActivity().getViewHandler("webplayer")) == null || (view = viewHandler.getView()) == null) {
            return null;
        }
        return (com.unity3d.services.ads.webplayer.WebPlayer) view;
    }

    /* access modifiers changed from: private */
    public static com.unity3d.services.ads.webplayer.WebPlayer getBannerWebPlayer() {
        if (BannerView.getInstance() == null) {
            return null;
        }
        return BannerView.getInstance().getWebPlayer();
    }
}
