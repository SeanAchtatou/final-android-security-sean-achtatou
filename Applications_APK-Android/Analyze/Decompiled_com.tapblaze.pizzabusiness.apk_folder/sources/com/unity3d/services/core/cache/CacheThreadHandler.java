package com.unity3d.services.core.cache;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import com.unity3d.services.core.api.Request;
import com.unity3d.services.core.log.DeviceLog;
import com.unity3d.services.core.request.WebRequest;
import com.unity3d.services.core.webview.WebViewApp;
import com.unity3d.services.core.webview.WebViewEventCategory;
import java.io.File;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class CacheThreadHandler extends Handler {
    private boolean _active = false;
    private boolean _canceled = false;
    private WebRequest _currentRequest = null;

    CacheThreadHandler() {
    }

    public void handleMessage(Message message) {
        HashMap hashMap;
        Bundle data = message.getData();
        String string = data.getString("source");
        data.remove("source");
        String string2 = data.getString("target");
        data.remove("target");
        int i = data.getInt("connectTimeout");
        data.remove("connectTimeout");
        int i2 = data.getInt("readTimeout");
        data.remove("readTimeout");
        int i3 = data.getInt("progressInterval");
        data.remove("progressInterval");
        boolean z = data.getBoolean("append", false);
        data.remove("append");
        if (data.size() > 0) {
            DeviceLog.debug("There are headers left in data, reading them");
            HashMap hashMap2 = new HashMap();
            for (String next : data.keySet()) {
                hashMap2.put(next, Arrays.asList(data.getStringArray(next)));
            }
            hashMap = hashMap2;
        } else {
            hashMap = null;
        }
        File file = new File(string2);
        if ((z && !file.exists()) || (!z && file.exists())) {
            this._active = false;
            WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.CACHE, CacheEvent.DOWNLOAD_ERROR, CacheError.FILE_STATE_WRONG, string, string2, Boolean.valueOf(z), Boolean.valueOf(file.exists()));
        } else if (message.what == 1) {
            downloadFile(string, string2, i, i2, i3, hashMap, z);
        }
    }

    public void setCancelStatus(boolean z) {
        WebRequest webRequest;
        this._canceled = z;
        if (z && (webRequest = this._currentRequest) != null) {
            this._active = false;
            webRequest.cancel();
        }
    }

    public boolean isActive() {
        return this._active;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:106:0x02cd */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:95:0x027e */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:84:0x022d */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:73:0x01dc */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:62:0x018b */
    /* JADX WARN: Type inference failed for: r6v1 */
    /* JADX WARN: Type inference failed for: r6v2, types: [boolean] */
    /* JADX WARN: Type inference failed for: r6v3 */
    /* JADX WARN: Type inference failed for: r6v4, types: [boolean] */
    /* JADX WARN: Type inference failed for: r6v5 */
    /* JADX WARN: Type inference failed for: r6v6, types: [boolean] */
    /* JADX WARN: Type inference failed for: r6v7 */
    /* JADX WARN: Type inference failed for: r6v8, types: [boolean] */
    /* JADX WARN: Type inference failed for: r6v9 */
    /* JADX WARN: Type inference failed for: r6v10, types: [boolean] */
    /* JADX WARN: Type inference failed for: r6v11 */
    /* JADX WARN: Type inference failed for: r6v38 */
    /* JADX WARN: Type inference failed for: r6v39 */
    /* JADX WARN: Type inference failed for: r6v40 */
    /* JADX WARN: Type inference failed for: r6v41 */
    /* JADX WARN: Type inference failed for: r6v42 */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x02ef A[SYNTHETIC, Splitter:B:110:0x02ef] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0319 A[SYNTHETIC, Splitter:B:117:0x0319] */
    /* JADX WARNING: Removed duplicated region for block: B:124:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:126:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:128:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:130:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:132:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01ad A[SYNTHETIC, Splitter:B:66:0x01ad] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01fe A[SYNTHETIC, Splitter:B:77:0x01fe] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x024f A[SYNTHETIC, Splitter:B:88:0x024f] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x02a0 A[SYNTHETIC, Splitter:B:99:0x02a0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void downloadFile(java.lang.String r24, java.lang.String r25, int r26, int r27, int r28, java.util.HashMap<java.lang.String, java.util.List<java.lang.String>> r29, boolean r30) {
        /*
            r23 = this;
            r13 = r23
            r14 = r24
            r0 = r25
            r1 = r30
            java.lang.String r15 = "Error closing stream"
            boolean r2 = r13._canceled
            if (r2 != 0) goto L_0x033c
            if (r14 == 0) goto L_0x033c
            if (r0 != 0) goto L_0x0014
            goto L_0x033c
        L_0x0014:
            java.io.File r5 = new java.io.File
            r5.<init>(r0)
            java.lang.String r2 = " to "
            if (r1 == 0) goto L_0x0049
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Unity Ads cache: resuming download "
            r3.append(r4)
            r3.append(r14)
            r3.append(r2)
            r3.append(r0)
            java.lang.String r0 = " at "
            r3.append(r0)
            long r6 = r5.length()
            r3.append(r6)
            java.lang.String r0 = " bytes"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            com.unity3d.services.core.log.DeviceLog.debug(r0)
            goto L_0x0063
        L_0x0049:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Unity Ads cache: start downloading "
            r3.append(r4)
            r3.append(r14)
            r3.append(r2)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            com.unity3d.services.core.log.DeviceLog.debug(r0)
        L_0x0063:
            boolean r0 = com.unity3d.services.core.device.Device.isActiveNetworkConnected()
            r12 = 2
            r11 = 1
            r10 = 0
            if (r0 != 0) goto L_0x0085
            java.lang.String r0 = "Unity Ads cache: download cancelled, no internet connection available"
            com.unity3d.services.core.log.DeviceLog.debug(r0)
            com.unity3d.services.core.webview.WebViewApp r0 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()
            com.unity3d.services.core.webview.WebViewEventCategory r1 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE
            com.unity3d.services.core.cache.CacheEvent r2 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR
            java.lang.Object[] r3 = new java.lang.Object[r12]
            com.unity3d.services.core.cache.CacheError r4 = com.unity3d.services.core.cache.CacheError.NO_INTERNET
            r3[r10] = r4
            r3[r11] = r14
            r0.sendEvent(r1, r2, r3)
            return
        L_0x0085:
            r13._active = r11
            long r2 = android.os.SystemClock.elapsedRealtime()
            r8 = 0
            r9 = 3
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x02c3, MalformedURLException -> 0x0274, IOException -> 0x0223, IllegalStateException -> 0x01d2, NetworkIOException -> 0x0181, all -> 0x0176 }
            r6.<init>(r5, r1)     // Catch:{ FileNotFoundException -> 0x02c3, MalformedURLException -> 0x0274, IOException -> 0x0223, IllegalStateException -> 0x01d2, NetworkIOException -> 0x0181, all -> 0x0176 }
            r0 = r26
            r1 = r27
            r4 = r29
            com.unity3d.services.core.request.WebRequest r0 = r13.getWebRequest(r14, r0, r1, r4)     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            r13._currentRequest = r0     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            com.unity3d.services.core.request.WebRequest r0 = r13._currentRequest     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            com.unity3d.services.core.cache.CacheThreadHandler$1 r1 = new com.unity3d.services.core.cache.CacheThreadHandler$1     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            r4 = r28
            r1.<init>(r5, r4)     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            r0.setProgressListener(r1)     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            com.unity3d.services.core.request.WebRequest r0 = r13._currentRequest     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            long r16 = r0.makeStreamRequest(r6)     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            r13._active = r10     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            com.unity3d.services.core.request.WebRequest r0 = r13._currentRequest     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            long r18 = r0.getContentLength()     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            com.unity3d.services.core.request.WebRequest r0 = r13._currentRequest     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            boolean r0 = r0.isCanceled()     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            com.unity3d.services.core.request.WebRequest r1 = r13._currentRequest     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            int r20 = r1.getResponseCode()     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            com.unity3d.services.core.request.WebRequest r1 = r13._currentRequest     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            java.util.Map r21 = r1.getResponseHeaders()     // Catch:{ FileNotFoundException -> 0x0168, MalformedURLException -> 0x015a, IOException -> 0x014c, IllegalStateException -> 0x013e, NetworkIOException -> 0x0131, all -> 0x0122 }
            r1 = r23
            r4 = r24
            r22 = r6
            r6 = r16
            r14 = r8
            r8 = r18
            r10 = r0
            r16 = 1
            r11 = r20
            r17 = 2
            r12 = r21
            r1.postProcessDownload(r2, r4, r5, r6, r8, r10, r11, r12)     // Catch:{ FileNotFoundException -> 0x011e, MalformedURLException -> 0x011a, IOException -> 0x0116, IllegalStateException -> 0x0112, NetworkIOException -> 0x010e, all -> 0x010a }
            r13._currentRequest = r14
            r22.close()     // Catch:{ Exception -> 0x00e8 }
            goto L_0x0311
        L_0x00e8:
            r0 = move-exception
            r1 = r0
            com.unity3d.services.core.log.DeviceLog.exception(r15, r1)
            com.unity3d.services.core.webview.WebViewApp r0 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.unity3d.services.core.cache.CacheError r5 = com.unity3d.services.core.cache.CacheError.FILE_IO_ERROR
            r6 = 0
            r4[r6] = r5
            r4[r16] = r24
            java.lang.String r1 = r1.getMessage()
            r4[r17] = r1
            r0.sendEvent(r2, r3, r4)
            goto L_0x0311
        L_0x010a:
            r0 = move-exception
            r4 = 3
            r6 = 0
            goto L_0x012c
        L_0x010e:
            r0 = move-exception
            r4 = 3
            r6 = 0
            goto L_0x013b
        L_0x0112:
            r0 = move-exception
            r4 = 3
            r6 = 0
            goto L_0x0148
        L_0x0116:
            r0 = move-exception
            r4 = 3
            r6 = 0
            goto L_0x0156
        L_0x011a:
            r0 = move-exception
            r4 = 3
            r6 = 0
            goto L_0x0164
        L_0x011e:
            r0 = move-exception
            r4 = 3
            r6 = 0
            goto L_0x0172
        L_0x0122:
            r0 = move-exception
            r22 = r6
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x012c:
            r1 = r0
            r8 = r22
            goto L_0x0315
        L_0x0131:
            r0 = move-exception
            r22 = r6
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x013b:
            r8 = r22
            goto L_0x0189
        L_0x013e:
            r0 = move-exception
            r22 = r6
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x0148:
            r8 = r22
            goto L_0x01da
        L_0x014c:
            r0 = move-exception
            r22 = r6
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x0156:
            r8 = r22
            goto L_0x022b
        L_0x015a:
            r0 = move-exception
            r22 = r6
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x0164:
            r8 = r22
            goto L_0x027c
        L_0x0168:
            r0 = move-exception
            r22 = r6
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x0172:
            r8 = r22
            goto L_0x02cb
        L_0x0176:
            r0 = move-exception
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x017e:
            r1 = r0
            goto L_0x0315
        L_0x0181:
            r0 = move-exception
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x0189:
            java.lang.String r1 = "Network error"
            com.unity3d.services.core.log.DeviceLog.exception(r1, r0)     // Catch:{ all -> 0x0312 }
            r13._active = r6     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewApp r1 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR     // Catch:{ all -> 0x0312 }
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheError r7 = com.unity3d.services.core.cache.CacheError.NETWORK_ERROR     // Catch:{ all -> 0x0312 }
            r5[r6] = r7     // Catch:{ all -> 0x0312 }
            r5[r16] = r24     // Catch:{ all -> 0x0312 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0312 }
            r5[r17] = r0     // Catch:{ all -> 0x0312 }
            r1.sendEvent(r2, r3, r5)     // Catch:{ all -> 0x0312 }
            r13._currentRequest = r14
            if (r8 == 0) goto L_0x0311
            r8.close()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0311
        L_0x01b2:
            r0 = move-exception
            r1 = r0
            com.unity3d.services.core.log.DeviceLog.exception(r15, r1)
            com.unity3d.services.core.webview.WebViewApp r0 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.unity3d.services.core.cache.CacheError r5 = com.unity3d.services.core.cache.CacheError.FILE_IO_ERROR
            r4[r6] = r5
            r4[r16] = r24
            java.lang.String r1 = r1.getMessage()
            r4[r17] = r1
            r0.sendEvent(r2, r3, r4)
            goto L_0x0311
        L_0x01d2:
            r0 = move-exception
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x01da:
            java.lang.String r1 = "Illegal state"
            com.unity3d.services.core.log.DeviceLog.exception(r1, r0)     // Catch:{ all -> 0x0312 }
            r13._active = r6     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewApp r1 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR     // Catch:{ all -> 0x0312 }
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheError r7 = com.unity3d.services.core.cache.CacheError.ILLEGAL_STATE     // Catch:{ all -> 0x0312 }
            r5[r6] = r7     // Catch:{ all -> 0x0312 }
            r5[r16] = r24     // Catch:{ all -> 0x0312 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0312 }
            r5[r17] = r0     // Catch:{ all -> 0x0312 }
            r1.sendEvent(r2, r3, r5)     // Catch:{ all -> 0x0312 }
            r13._currentRequest = r14
            if (r8 == 0) goto L_0x0311
            r8.close()     // Catch:{ Exception -> 0x0203 }
            goto L_0x0311
        L_0x0203:
            r0 = move-exception
            r1 = r0
            com.unity3d.services.core.log.DeviceLog.exception(r15, r1)
            com.unity3d.services.core.webview.WebViewApp r0 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.unity3d.services.core.cache.CacheError r5 = com.unity3d.services.core.cache.CacheError.FILE_IO_ERROR
            r4[r6] = r5
            r4[r16] = r24
            java.lang.String r1 = r1.getMessage()
            r4[r17] = r1
            r0.sendEvent(r2, r3, r4)
            goto L_0x0311
        L_0x0223:
            r0 = move-exception
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x022b:
            java.lang.String r1 = "Couldn't request stream"
            com.unity3d.services.core.log.DeviceLog.exception(r1, r0)     // Catch:{ all -> 0x0312 }
            r13._active = r6     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewApp r1 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR     // Catch:{ all -> 0x0312 }
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheError r7 = com.unity3d.services.core.cache.CacheError.FILE_IO_ERROR     // Catch:{ all -> 0x0312 }
            r5[r6] = r7     // Catch:{ all -> 0x0312 }
            r5[r16] = r24     // Catch:{ all -> 0x0312 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0312 }
            r5[r17] = r0     // Catch:{ all -> 0x0312 }
            r1.sendEvent(r2, r3, r5)     // Catch:{ all -> 0x0312 }
            r13._currentRequest = r14
            if (r8 == 0) goto L_0x0311
            r8.close()     // Catch:{ Exception -> 0x0254 }
            goto L_0x0311
        L_0x0254:
            r0 = move-exception
            r1 = r0
            com.unity3d.services.core.log.DeviceLog.exception(r15, r1)
            com.unity3d.services.core.webview.WebViewApp r0 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.unity3d.services.core.cache.CacheError r5 = com.unity3d.services.core.cache.CacheError.FILE_IO_ERROR
            r4[r6] = r5
            r4[r16] = r24
            java.lang.String r1 = r1.getMessage()
            r4[r17] = r1
            r0.sendEvent(r2, r3, r4)
            goto L_0x0311
        L_0x0274:
            r0 = move-exception
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x027c:
            java.lang.String r1 = "Malformed URL"
            com.unity3d.services.core.log.DeviceLog.exception(r1, r0)     // Catch:{ all -> 0x0312 }
            r13._active = r6     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewApp r1 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR     // Catch:{ all -> 0x0312 }
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheError r7 = com.unity3d.services.core.cache.CacheError.MALFORMED_URL     // Catch:{ all -> 0x0312 }
            r5[r6] = r7     // Catch:{ all -> 0x0312 }
            r5[r16] = r24     // Catch:{ all -> 0x0312 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0312 }
            r5[r17] = r0     // Catch:{ all -> 0x0312 }
            r1.sendEvent(r2, r3, r5)     // Catch:{ all -> 0x0312 }
            r13._currentRequest = r14
            if (r8 == 0) goto L_0x0311
            r8.close()     // Catch:{ Exception -> 0x02a4 }
            goto L_0x0311
        L_0x02a4:
            r0 = move-exception
            r1 = r0
            com.unity3d.services.core.log.DeviceLog.exception(r15, r1)
            com.unity3d.services.core.webview.WebViewApp r0 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.unity3d.services.core.cache.CacheError r5 = com.unity3d.services.core.cache.CacheError.FILE_IO_ERROR
            r4[r6] = r5
            r4[r16] = r24
            java.lang.String r1 = r1.getMessage()
            r4[r17] = r1
            r0.sendEvent(r2, r3, r4)
            goto L_0x0311
        L_0x02c3:
            r0 = move-exception
            r14 = r8
            r4 = 3
            r6 = 0
            r16 = 1
            r17 = 2
        L_0x02cb:
            java.lang.String r1 = "Couldn't create target file"
            com.unity3d.services.core.log.DeviceLog.exception(r1, r0)     // Catch:{ all -> 0x0312 }
            r13._active = r6     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewApp r1 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR     // Catch:{ all -> 0x0312 }
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ all -> 0x0312 }
            com.unity3d.services.core.cache.CacheError r7 = com.unity3d.services.core.cache.CacheError.FILE_IO_ERROR     // Catch:{ all -> 0x0312 }
            r5[r6] = r7     // Catch:{ all -> 0x0312 }
            r5[r16] = r24     // Catch:{ all -> 0x0312 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0312 }
            r5[r17] = r0     // Catch:{ all -> 0x0312 }
            r1.sendEvent(r2, r3, r5)     // Catch:{ all -> 0x0312 }
            r13._currentRequest = r14
            if (r8 == 0) goto L_0x0311
            r8.close()     // Catch:{ Exception -> 0x02f3 }
            goto L_0x0311
        L_0x02f3:
            r0 = move-exception
            r1 = r0
            com.unity3d.services.core.log.DeviceLog.exception(r15, r1)
            com.unity3d.services.core.webview.WebViewApp r0 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()
            com.unity3d.services.core.webview.WebViewEventCategory r2 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE
            com.unity3d.services.core.cache.CacheEvent r3 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.unity3d.services.core.cache.CacheError r5 = com.unity3d.services.core.cache.CacheError.FILE_IO_ERROR
            r4[r6] = r5
            r4[r16] = r24
            java.lang.String r1 = r1.getMessage()
            r4[r17] = r1
            r0.sendEvent(r2, r3, r4)
        L_0x0311:
            return
        L_0x0312:
            r0 = move-exception
            goto L_0x017e
        L_0x0315:
            r13._currentRequest = r14
            if (r8 == 0) goto L_0x033b
            r8.close()     // Catch:{ Exception -> 0x031d }
            goto L_0x033b
        L_0x031d:
            r0 = move-exception
            r2 = r0
            com.unity3d.services.core.log.DeviceLog.exception(r15, r2)
            com.unity3d.services.core.webview.WebViewApp r0 = com.unity3d.services.core.webview.WebViewApp.getCurrentApp()
            com.unity3d.services.core.webview.WebViewEventCategory r3 = com.unity3d.services.core.webview.WebViewEventCategory.CACHE
            com.unity3d.services.core.cache.CacheEvent r5 = com.unity3d.services.core.cache.CacheEvent.DOWNLOAD_ERROR
            java.lang.Object[] r4 = new java.lang.Object[r4]
            com.unity3d.services.core.cache.CacheError r7 = com.unity3d.services.core.cache.CacheError.FILE_IO_ERROR
            r4[r6] = r7
            r4[r16] = r24
            java.lang.String r2 = r2.getMessage()
            r4[r17] = r2
            r0.sendEvent(r3, r5, r4)
        L_0x033b:
            throw r1
        L_0x033c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.unity3d.services.core.cache.CacheThreadHandler.downloadFile(java.lang.String, java.lang.String, int, int, int, java.util.HashMap, boolean):void");
    }

    private void postProcessDownload(long j, String str, File file, long j2, long j3, boolean z, int i, Map<String, List<String>> map) {
        String str2 = str;
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        if (!file.setReadable(true, false)) {
            DeviceLog.debug("Unity Ads cache: could not set file readable!");
        }
        if (!z) {
            DeviceLog.debug("Unity Ads cache: File " + file.getName() + " of " + j2 + " bytes downloaded in " + elapsedRealtime + "ms");
            WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.CACHE, CacheEvent.DOWNLOAD_END, str2, Long.valueOf(j2), Long.valueOf(j3), Long.valueOf(elapsedRealtime), Integer.valueOf(i), Request.getResponseHeadersMap(map));
            return;
        }
        DeviceLog.debug("Unity Ads cache: downloading of " + str2 + " stopped");
        WebViewApp.getCurrentApp().sendEvent(WebViewEventCategory.CACHE, CacheEvent.DOWNLOAD_STOPPED, str2, Long.valueOf(j2), Long.valueOf(j3), Long.valueOf(elapsedRealtime), Integer.valueOf(i), Request.getResponseHeadersMap(map));
    }

    private WebRequest getWebRequest(String str, int i, int i2, HashMap<String, List<String>> hashMap) throws MalformedURLException {
        HashMap hashMap2 = new HashMap();
        if (hashMap != null) {
            hashMap2.putAll(hashMap);
        }
        return new WebRequest(str, "GET", hashMap2, i, i2);
    }
}
