package com.oppo.oiface.engine;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOIfaceService extends IInterface {
    int getMemoryUsage(int i) throws RemoteException;

    String getOifaceVersion() throws RemoteException;

    void onAppRegister() throws RemoteException;

    void registerEngineClient(IOIfaceNotifier iOIfaceNotifier) throws RemoteException;

    void updateGameEngineInfo(String str) throws RemoteException;

    public static abstract class Stub extends Binder implements IOIfaceService {
        private static final String DESCRIPTOR = "com.oppo.oiface.IOIfaceService";
        static final int TRANSACTION_REGISTER_ENGINE_CLIENT = 154;
        static final int TRANSACTION_getMemoryUsage = 109;
        static final int TRANSACTION_getOifaceversion = 105;
        static final int TRANSACTION_onAppRegister = 104;
        static final int TRANSACTION_updateGameEngineInfo = 155;

        public IBinder asBinder() {
            return this;
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IOIfaceService asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IOIfaceService)) {
                return new Proxy(iBinder);
            }
            return (IOIfaceService) queryLocalInterface;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            return super.onTransact(i, parcel, parcel2, i2);
        }

        private static class Proxy implements IOIfaceService {
            private IBinder mRemote;

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void registerEngineClient(IOIfaceNotifier iOIfaceNotifier) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iOIfaceNotifier != null ? iOIfaceNotifier.asBinder() : null);
                    this.mRemote.transact(Stub.TRANSACTION_REGISTER_ENGINE_CLIENT, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onAppRegister() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(104, obtain, obtain2, 1);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getOifaceVersion() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(105, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int getMemoryUsage(int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(109, obtain, obtain2, 0);
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void updateGameEngineInfo(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str);
                    this.mRemote.transact(Stub.TRANSACTION_updateGameEngineInfo, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }
        }
    }
}
