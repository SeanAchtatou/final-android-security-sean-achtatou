package com.moat.analytics.mobile.vng;

import android.view.ViewGroup;
import android.webkit.WebView;

class z extends b implements WebAdTracker {
    z(ViewGroup viewGroup) {
        this(aa.a(viewGroup, false).c(null));
        if (viewGroup == null) {
            o.a("[ERROR] ", 3, "WebAdTracker", this, "WebAdTracker initialization not successful, " + "Target ViewGroup is null");
            this.a = new m("Target ViewGroup is null");
        }
        if (this.b == null) {
            o.a("[ERROR] ", 3, "WebAdTracker", this, "WebAdTracker initialization not successful, " + "No WebView to track inside of ad container");
            this.a = new m("No WebView to track inside of ad container");
        }
    }

    z(WebView webView) {
        super(webView, false, false);
        o.a(3, "WebAdTracker", this, "Initializing.");
        if (webView == null) {
            o.a("[ERROR] ", 3, "WebAdTracker", this, "WebAdTracker initialization not successful, " + "WebView is null");
            this.a = new m("WebView is null");
            return;
        }
        try {
            super.a(webView);
            o.a("[SUCCESS] ", a() + " created for " + g());
        } catch (m e) {
            this.a = e;
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return "WebAdTracker";
    }
}
