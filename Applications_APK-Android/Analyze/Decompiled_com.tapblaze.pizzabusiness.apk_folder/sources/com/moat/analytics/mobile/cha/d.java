package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants;
import com.moat.analytics.mobile.cha.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class d {

    /* renamed from: ʻ  reason: contains not printable characters */
    private WeakReference<View> f42;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f43;

    /* renamed from: ʽ  reason: contains not printable characters */
    final boolean f44;

    /* renamed from: ˊ  reason: contains not printable characters */
    TrackerListener f45;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private boolean f46;

    /* renamed from: ˋ  reason: contains not printable characters */
    final String f47;

    /* renamed from: ˎ  reason: contains not printable characters */
    j f48;

    /* renamed from: ˏ  reason: contains not printable characters */
    WeakReference<WebView> f49;

    /* renamed from: ͺ  reason: contains not printable characters */
    private boolean f50;

    /* renamed from: ॱ  reason: contains not printable characters */
    o f51 = null;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final u f52;

    @Deprecated
    public void setActivity(Activity activity) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public abstract String m36();

    d(View view, boolean z, boolean z2) {
        String str;
        a.m6(3, "BaseTracker", this, "Initializing.");
        if (z) {
            str = "m" + hashCode();
        } else {
            str = "";
        }
        this.f47 = str;
        this.f42 = new WeakReference<>(view);
        this.f43 = z;
        this.f44 = z2;
        this.f46 = false;
        this.f50 = false;
        this.f52 = new u();
    }

    public void setListener(TrackerListener trackerListener) {
        this.f45 = trackerListener;
    }

    public void removeListener() {
        this.f45 = null;
    }

    public void startTracking() {
        try {
            a.m6(3, "BaseTracker", this, "In startTracking method.");
            m39();
            if (this.f45 != null) {
                this.f45.onTrackingStarted("Tracking started on " + a.m5(this.f42.get()));
            }
            String str = "startTracking succeeded for " + a.m5(this.f42.get());
            a.m6(3, "BaseTracker", this, str);
            a.m3("[SUCCESS] ", m36() + " " + str);
        } catch (Exception e) {
            m42("startTracking", e);
        }
    }

    public void stopTracking() {
        boolean z = false;
        try {
            a.m6(3, "BaseTracker", this, "In stopTracking method.");
            this.f50 = true;
            if (this.f48 != null) {
                this.f48.m96(this);
                z = true;
            }
        } catch (Exception e) {
            o.m130(e);
        }
        StringBuilder sb = new StringBuilder("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        a.m6(3, "BaseTracker", this, sb.toString());
        String str = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(m36());
        sb2.append(" stopTracking ");
        sb2.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : Constants.ParametersKeys.FAILED);
        sb2.append(" for ");
        sb2.append(a.m5(this.f42.get()));
        a.m3(str, sb2.toString());
        TrackerListener trackerListener = this.f45;
        if (trackerListener != null) {
            trackerListener.onTrackingStopped("");
            this.f45 = null;
        }
    }

    public void changeTargetView(View view) {
        a.m6(3, "BaseTracker", this, "changing view to " + a.m5(view));
        this.f42 = new WeakReference<>(view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m39() throws o {
        a.m6(3, "BaseTracker", this, "Attempting to start impression.");
        m38();
        if (this.f46) {
            throw new o("Tracker already started");
        } else if (!this.f50) {
            m37(new ArrayList());
            j jVar = this.f48;
            if (jVar != null) {
                jVar.m97(this);
                this.f46 = true;
                a.m6(3, "BaseTracker", this, "Impression started.");
                return;
            }
            a.m6(3, "BaseTracker", this, "Bridge is null, won't start tracking");
            throw new o("Bridge is null");
        } else {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m41(WebView webView) throws o {
        if (webView != null) {
            this.f49 = new WeakReference<>(webView);
            if (this.f48 == null) {
                if (!(this.f43 || this.f44)) {
                    a.m6(3, "BaseTracker", this, "Attempting bridge installation.");
                    if (this.f49.get() != null) {
                        this.f48 = new j(this.f49.get(), j.e.f121);
                        a.m6(3, "BaseTracker", this, "Bridge installed.");
                    } else {
                        this.f48 = null;
                        a.m6(3, "BaseTracker", this, "Bridge not installed, WebView is null.");
                    }
                }
            }
            j jVar = this.f48;
            if (jVar != null) {
                jVar.m95(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m38() throws o {
        if (this.f51 != null) {
            throw new o("Tracker initialization failed: " + this.f51.getMessage());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m35() {
        return this.f46 && !this.f50;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final View m33() {
        return this.f42.get();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m34() {
        this.f52.m196(this.f47, this.f42.get());
        return this.f52.f214;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m42(String str, Exception exc) {
        try {
            o.m130(exc);
            String r3 = o.m129(str, exc);
            if (this.f45 != null) {
                this.f45.onTrackingFailedToStart(r3);
            }
            a.m6(3, "BaseTracker", this, r3);
            a.m3("[ERROR] ", m36() + " " + r3);
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m40() throws o {
        if (this.f46) {
            throw new o("Tracker already started");
        } else if (this.f50) {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m37(List<String> list) throws o {
        if (this.f42.get() == null && !this.f44) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new o(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m32() {
        return a.m5(this.f42.get());
    }
}
