package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.moat.analytics.mobile.cha.j;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class a {

    /* renamed from: ˊ  reason: contains not printable characters */
    final String f14;

    /* renamed from: ˋ  reason: contains not printable characters */
    WebView f15;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean f16;

    /* renamed from: ˏ  reason: contains not printable characters */
    j f17;

    /* renamed from: ॱ  reason: contains not printable characters */
    private final int f18;

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    static final class d extends Enum<d> {

        /* renamed from: ˋ  reason: contains not printable characters */
        public static final int f21 = 2;

        /* renamed from: ˏ  reason: contains not printable characters */
        public static final int f22 = 1;
    }

    a(Application application, int i) {
        this.f18 = i;
        this.f16 = false;
        this.f14 = String.format(Locale.ROOT, "_moatTracker%d", Integer.valueOf((int) (Math.random() * 1.0E8d)));
        this.f15 = new WebView(application);
        WebSettings settings = this.f15.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(false);
        settings.setDatabaseEnabled(false);
        settings.setDomStorageEnabled(false);
        settings.setGeolocationEnabled(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setSaveFormData(false);
        if (Build.VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(1);
        }
        try {
            this.f17 = new j(this.f15, i == d.f21 ? j.e.f119 : j.e.f120);
        } catch (o e) {
            o.m130(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m10(String str) {
        if (this.f18 == d.f22) {
            this.f15.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f16) {
                        try {
                            boolean unused = a.this.f16 = true;
                            a.this.f17.m99();
                        } catch (Exception e) {
                            o.m130(e);
                        }
                    }
                }
            });
            WebView webView = this.f15;
            webView.loadData("<!DOCTYPE html>\n<html>\n<head lang=\"en\">\n   <meta charset=\"UTF-8\">\n   <title></title>\n</head>\n<body style=\"margin:0;padding:0;\">\n    <script src=\"https://z.moatads.com/" + str + "/moatad.js\" type=\"text/javascript\"></script>\n</body>\n</html>", "text/html", "utf-8");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m11(String str, Map<String, String> map, Integer num, Integer num2, Integer num3) {
        if (this.f18 == d.f21) {
            this.f15.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f16) {
                        try {
                            boolean unused = a.this.f16 = true;
                            a.this.f17.m99();
                            a.this.f17.m98(a.this.f14);
                        } catch (Exception e) {
                            o.m130(e);
                        }
                    }
                }
            });
            JSONObject jSONObject = new JSONObject(map);
            WebView webView = this.f15;
            String str2 = this.f14;
            webView.loadData(String.format(Locale.ROOT, "<html><head></head><body><div id=\"%s\" style=\"width: %dpx; height: %dpx;\"></div><script>(function initMoatTracking(apiname, pcode, ids, duration) {var events = [];window[pcode + '_moatElToTrack'] = document.getElementById('%s');var moatapi = {'dropTime':%d,'adData': {'ids': ids, 'duration': duration, 'url': 'n/a'},'dispatchEvent': function(ev) {if (this.sendEvent) {if (events) { events.push(ev); ev = events; events = false; }this.sendEvent(ev);} else {events.push(ev);}},'dispatchMany': function(evs){for (var i=0, l=evs.length; i<l; i++) {this.dispatchEvent(evs[i]);}}};Object.defineProperty(window, apiname, {'value': moatapi});var s = document.createElement('script');s.src = 'https://z.moatads.com/' + pcode + '/moatvideo.js?' + apiname + '#' + apiname;document.body.appendChild(s);})('%s', '%s', %s, %s);</script></body></html>", "mianahwvc", num, num2, "mianahwvc", Long.valueOf(System.currentTimeMillis()), str2, str, jSONObject.toString(), num3), "text/html", null);
        }
    }

    a() {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static void m6(int i, String str, Object obj, String str2) {
        if (!t.m174().f184) {
            return;
        }
        if (obj == null) {
            Log.println(i, "Moat" + str, String.format("message = %s", str2));
            return;
        }
        Log.println(i, "Moat" + str, String.format("id = %s, message = %s", Integer.valueOf(obj.hashCode()), str2));
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static void m7(String str, Object obj, String str2) {
        if (t.m174().f181) {
            String str3 = "Moat" + str;
            Object[] objArr = new Object[2];
            objArr[0] = obj == null ? "null" : Integer.valueOf(obj.hashCode());
            objArr[1] = str2;
            Log.println(2, str3, String.format("id = %s, message = %s", objArr));
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m8(String str, Object obj, String str2, Exception exc) {
        if (t.m174().f184) {
            Log.e("Moat" + str, String.format("id = %s, message = %s", Integer.valueOf(obj.hashCode()), str2), exc);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static void m3(String str, String str2) {
        if (!t.m174().f184 && ((f) MoatAnalytics.getInstance()).f60) {
            int i = 2;
            if (str.equals("[ERROR] ")) {
                i = 6;
            }
            Log.println(i, "MoatAnalytics", str + str2);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static String m5(View view) {
        if (view == null) {
            return "null";
        }
        return view.getClass().getSimpleName() + "@" + view.hashCode();
    }
}
