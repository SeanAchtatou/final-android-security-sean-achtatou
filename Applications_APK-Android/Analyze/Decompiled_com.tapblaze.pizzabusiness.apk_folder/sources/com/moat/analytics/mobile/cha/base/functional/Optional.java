package com.moat.analytics.mobile.cha.base.functional;

import java.util.NoSuchElementException;

public final class Optional<T> {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final Optional<?> f35 = new Optional<>();

    /* renamed from: ॱ  reason: contains not printable characters */
    private final T f36;

    private Optional() {
        this.f36 = null;
    }

    public static <T> Optional<T> empty() {
        return f35;
    }

    private Optional(T t) {
        if (t != null) {
            this.f36 = t;
            return;
        }
        throw new NullPointerException("Optional of null value.");
    }

    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    public final T get() {
        T t = this.f36;
        if (t != null) {
            return t;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean isPresent() {
        return this.f36 != null;
    }

    public final T orElse(T t) {
        T t2 = this.f36;
        return t2 != null ? t2 : t;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        T t = this.f36;
        T t2 = ((Optional) obj).f36;
        return t == t2 || !(t == null || t2 == null || !t.equals(t2));
    }

    public final int hashCode() {
        T t = this.f36;
        if (t == null) {
            return 0;
        }
        return t.hashCode();
    }

    public final String toString() {
        T t = this.f36;
        if (t == null) {
            return "Optional.empty";
        }
        return String.format("Optional[%s]", t);
    }
}
