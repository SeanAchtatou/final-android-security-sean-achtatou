package com.moat.analytics.mobile.cha;

import android.util.Log;

final class o extends Exception {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Exception f142 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final Long f143 = 60000L;

    /* renamed from: ॱ  reason: contains not printable characters */
    private static Long f144;

    o(String str) {
        super(str);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static String m129(String str, Exception exc) {
        if (exc instanceof o) {
            return str + " failed: " + exc.getMessage();
        }
        return str + " failed unexpectedly";
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m130(Exception exc) {
        if (t.m174().f184) {
            Log.e("MoatException", Log.getStackTraceString(exc));
        } else {
            m128(exc);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(13:7|(4:9|10|11|(1:13))|14|(1:16)(1:17)|18|(5:19|20|21|(1:23)(2:24|25)|26)|27|29|30|(4:31|32|33|34)|40|41|(2:45|48)(1:49)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x00dc */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m128(java.lang.Exception r13) {
        /*
            java.lang.String r0 = ""
            com.moat.analytics.mobile.cha.t r1 = com.moat.analytics.mobile.cha.t.m174()     // Catch:{ Exception -> 0x0197 }
            int r1 = r1.f183     // Catch:{ Exception -> 0x0197 }
            int r2 = com.moat.analytics.mobile.cha.t.a.f194     // Catch:{ Exception -> 0x0197 }
            if (r1 != r2) goto L_0x0195
            com.moat.analytics.mobile.cha.t r1 = com.moat.analytics.mobile.cha.t.m174()     // Catch:{ Exception -> 0x0197 }
            int r1 = r1.f186     // Catch:{ Exception -> 0x0197 }
            if (r1 != 0) goto L_0x0015
            return
        L_0x0015:
            r2 = 100
            if (r1 >= r2) goto L_0x0029
            double r2 = (double) r1
            r4 = 4636737291354636288(0x4059000000000000, double:100.0)
            java.lang.Double.isNaN(r2)
            double r2 = r2 / r4
            double r4 = java.lang.Math.random()     // Catch:{ Exception -> 0x0197 }
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 >= 0) goto L_0x0029
            return
        L_0x0029:
            java.lang.String r2 = "https://px.moatads.com/pixel.gif?e=0&i=MOATSDK1&ac=1"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0197 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0197 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0197 }
            java.lang.String r4 = "&zt="
            r2.<init>(r4)     // Catch:{ Exception -> 0x0197 }
            boolean r4 = r13 instanceof com.moat.analytics.mobile.cha.o     // Catch:{ Exception -> 0x0197 }
            r5 = 1
            r6 = 0
            if (r4 == 0) goto L_0x003f
            r4 = 1
            goto L_0x0040
        L_0x003f:
            r4 = 0
        L_0x0040:
            r2.append(r4)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0197 }
            r3.append(r2)     // Catch:{ Exception -> 0x0197 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0197 }
            java.lang.String r4 = "&zr="
            r2.<init>(r4)     // Catch:{ Exception -> 0x0197 }
            r2.append(r1)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x0197 }
            r3.append(r1)     // Catch:{ Exception -> 0x0197 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "&zm="
            r1.<init>(r2)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = r13.getMessage()     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r4 = "UTF-8"
            if (r2 != 0) goto L_0x006d
            java.lang.String r2 = "null"
            goto L_0x007d
        L_0x006d:
            java.lang.String r2 = r13.getMessage()     // Catch:{ Exception -> 0x00a8 }
            byte[] r2 = r2.getBytes(r4)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r6)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = java.net.URLEncoder.encode(r2, r4)     // Catch:{ Exception -> 0x00a8 }
        L_0x007d:
            r1.append(r2)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00a8 }
            r3.append(r1)     // Catch:{ Exception -> 0x00a8 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r2 = "&k="
            r1.<init>(r2)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r13 = android.util.Log.getStackTraceString(r13)     // Catch:{ Exception -> 0x00a8 }
            byte[] r13 = r13.getBytes(r4)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r13 = android.util.Base64.encodeToString(r13, r6)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r13 = java.net.URLEncoder.encode(r13, r4)     // Catch:{ Exception -> 0x00a8 }
            r1.append(r13)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r13 = r1.toString()     // Catch:{ Exception -> 0x00a8 }
            r3.append(r13)     // Catch:{ Exception -> 0x00a8 }
        L_0x00a8:
            java.lang.String r13 = "CHA"
            java.lang.String r1 = "&zMoatMMAKv=35d482907bc2811c2e46b96f16eb5f9fe00185f3"
            r3.append(r1)     // Catch:{ Exception -> 0x00d7 }
            java.lang.String r1 = "2.4.1"
            com.moat.analytics.mobile.cha.r$e r2 = com.moat.analytics.mobile.cha.r.m147()     // Catch:{ Exception -> 0x00d5 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r7 = "&zMoatMMAKan="
            r4.<init>(r7)     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r7 = r2.m158()     // Catch:{ Exception -> 0x00d5 }
            r4.append(r7)     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00d5 }
            r3.append(r4)     // Catch:{ Exception -> 0x00d5 }
            java.lang.String r2 = r2.m157()     // Catch:{ Exception -> 0x00d5 }
            int r4 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00dc }
            java.lang.String r0 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x00dc }
            goto L_0x00dc
        L_0x00d5:
            r2 = r0
            goto L_0x00dc
        L_0x00d7:
            r1 = r0
            goto L_0x00db
        L_0x00d9:
            r13 = r0
            r1 = r13
        L_0x00db:
            r2 = r1
        L_0x00dc:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0197 }
            java.lang.String r7 = "&d=Android:"
            r4.<init>(r7)     // Catch:{ Exception -> 0x0197 }
            r4.append(r13)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r13 = ":"
            r4.append(r13)     // Catch:{ Exception -> 0x0197 }
            r4.append(r2)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r13 = ":-"
            r4.append(r13)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r13 = r4.toString()     // Catch:{ Exception -> 0x0197 }
            r3.append(r13)     // Catch:{ Exception -> 0x0197 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0197 }
            java.lang.String r2 = "&bo="
            r13.<init>(r2)     // Catch:{ Exception -> 0x0197 }
            r13.append(r1)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0197 }
            r3.append(r13)     // Catch:{ Exception -> 0x0197 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0197 }
            java.lang.String r1 = "&bd="
            r13.<init>(r1)     // Catch:{ Exception -> 0x0197 }
            r13.append(r0)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0197 }
            r3.append(r13)     // Catch:{ Exception -> 0x0197 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0197 }
            java.lang.Long r13 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x0197 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0197 }
            java.lang.String r1 = "&t="
            r0.<init>(r1)     // Catch:{ Exception -> 0x0197 }
            r0.append(r13)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0197 }
            r3.append(r0)     // Catch:{ Exception -> 0x0197 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0197 }
            java.lang.String r1 = "&de="
            r0.<init>(r1)     // Catch:{ Exception -> 0x0197 }
            java.util.Locale r1 = java.util.Locale.ROOT     // Catch:{ Exception -> 0x0197 }
            java.lang.String r2 = "%.0f"
            java.lang.Object[] r4 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0197 }
            double r7 = java.lang.Math.random()     // Catch:{ Exception -> 0x0197 }
            r9 = 4621819117588971520(0x4024000000000000, double:10.0)
            r11 = 4622945017495814144(0x4028000000000000, double:12.0)
            double r9 = java.lang.Math.pow(r9, r11)     // Catch:{ Exception -> 0x0197 }
            double r7 = r7 * r9
            double r7 = java.lang.Math.floor(r7)     // Catch:{ Exception -> 0x0197 }
            java.lang.Double r5 = java.lang.Double.valueOf(r7)     // Catch:{ Exception -> 0x0197 }
            r4[r6] = r5     // Catch:{ Exception -> 0x0197 }
            java.lang.String r1 = java.lang.String.format(r1, r2, r4)     // Catch:{ Exception -> 0x0197 }
            r0.append(r1)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0197 }
            r3.append(r0)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r0 = "&cs=0"
            r3.append(r0)     // Catch:{ Exception -> 0x0197 }
            java.lang.Long r0 = com.moat.analytics.mobile.cha.o.f144     // Catch:{ Exception -> 0x0197 }
            if (r0 == 0) goto L_0x0186
            long r0 = r13.longValue()     // Catch:{ Exception -> 0x0197 }
            java.lang.Long r2 = com.moat.analytics.mobile.cha.o.f144     // Catch:{ Exception -> 0x0197 }
            long r4 = r2.longValue()     // Catch:{ Exception -> 0x0197 }
            long r0 = r0 - r4
            java.lang.Long r2 = com.moat.analytics.mobile.cha.o.f143     // Catch:{ Exception -> 0x0197 }
            long r4 = r2.longValue()     // Catch:{ Exception -> 0x0197 }
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0194
        L_0x0186:
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x0197 }
            com.moat.analytics.mobile.cha.m$2 r1 = new com.moat.analytics.mobile.cha.m$2     // Catch:{ Exception -> 0x0197 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0197 }
            r1.start()     // Catch:{ Exception -> 0x0197 }
            com.moat.analytics.mobile.cha.o.f144 = r13     // Catch:{ Exception -> 0x0197 }
        L_0x0194:
            return
        L_0x0195:
            com.moat.analytics.mobile.cha.o.f142 = r13     // Catch:{ Exception -> 0x0197 }
        L_0x0197:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.cha.o.m128(java.lang.Exception):void");
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m131() {
        Exception exc = f142;
        if (exc != null) {
            m128(exc);
            f142 = null;
        }
    }
}
