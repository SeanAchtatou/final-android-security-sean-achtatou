package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.chartboost.sdk.impl.b;
import com.facebook.places.model.PlaceFields;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.lang.ref.WeakReference;

final class r {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int f158 = 1;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static e f159 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static d f160 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static int f161 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public static String f162;

    /* renamed from: ॱ  reason: contains not printable characters */
    private static int[] f163 = {-39340411, 1646369784, -593413711, -1069164445, -50787683, -1327220997, 423245644, -742130253, 54775946, -495304555, 1880137505, 1742082653, 65717847, 1497802820, 828947133, -614454858, 941569790, -1897799303};

    r() {
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static double m154() {
        try {
            int r1 = m145();
            double d2 = (double) r1;
            double streamMaxVolume = (double) ((AudioManager) c.m27().getSystemService(m151(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamMaxVolume(3);
            Double.isNaN(d2);
            Double.isNaN(streamMaxVolume);
            return d2 / streamMaxVolume;
        } catch (Exception e2) {
            o.m130(e2);
            return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int m145() {
        try {
            return ((AudioManager) c.m27().getSystemService(m151(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamVolume(3);
        } catch (Exception e2) {
            o.m130(e2);
            return 0;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m152(final Application application) {
        try {
            AsyncTask.execute(new Runnable() {
                public final void run() {
                    try {
                        AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(application);
                        if (!advertisingIdInfo.isLimitAdTrackingEnabled()) {
                            String unused = r.f162 = advertisingIdInfo.getId();
                            a.m6(3, "Util", this, "Retrieved Advertising ID = " + r.f162);
                            return;
                        }
                        a.m6(3, "Util", this, "User has limited ad tracking");
                    } catch (Exception e) {
                        o.m130(e);
                    }
                }
            });
        } catch (Exception e2) {
            o.m130(e2);
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static String m150() {
        return f162;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static Context m153() {
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f58;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static e m147() {
        e eVar = f159;
        if (eVar == null || !eVar.f173) {
            f159 = new e((byte) 0);
        }
        return f159;
    }

    static class e {

        /* renamed from: ˊ  reason: contains not printable characters */
        private String f171;

        /* renamed from: ˋ  reason: contains not printable characters */
        private String f172;
        /* access modifiers changed from: private */

        /* renamed from: ˏ  reason: contains not printable characters */
        public boolean f173;

        /* renamed from: ॱ  reason: contains not printable characters */
        private String f174;

        /* synthetic */ e(byte b) {
            this();
        }

        private e() {
            this.f173 = false;
            this.f171 = "_unknown_";
            this.f172 = "_unknown_";
            this.f174 = "_unknown_";
            try {
                Context r0 = r.m153();
                if (r0 != null) {
                    this.f173 = true;
                    PackageManager packageManager = r0.getPackageManager();
                    this.f172 = r0.getPackageName();
                    this.f171 = packageManager.getApplicationLabel(r0.getApplicationInfo()).toString();
                    this.f174 = packageManager.getInstallerPackageName(this.f172);
                    return;
                }
                a.m6(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                o.m130(e);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˎ  reason: contains not printable characters */
        public final String m158() {
            return this.f171;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final String m157() {
            return this.f172;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ॱ  reason: contains not printable characters */
        public final String m159() {
            String str = this.f174;
            return str != null ? str : "_unknown_";
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static String m151(int[] iArr, int i) {
        char[] cArr = new char[4];
        char[] cArr2 = new char[(iArr.length << 1)];
        int[] iArr2 = (int[]) f163.clone();
        int i2 = 0;
        while (true) {
            if (i2 >= iArr.length) {
                return new String(cArr2, 0, i);
            }
            cArr[0] = iArr[i2] >>> 16;
            cArr[1] = (char) iArr[i2];
            int i3 = i2 + 1;
            cArr[2] = iArr[i3] >>> 16;
            cArr[3] = (char) iArr[i3];
            b.a(cArr, iArr2, false);
            int i4 = i2 << 1;
            cArr2[i4] = cArr[0];
            cArr2[i4 + 1] = cArr[1];
            cArr2[i4 + 2] = cArr[2];
            cArr2[i4 + 3] = cArr[3];
            i2 += 2;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static d m148() {
        d dVar = f160;
        if (dVar == null || !dVar.f165) {
            f160 = new d((byte) 0);
        }
        return f160;
    }

    static class d {

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f165;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f166;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f167;

        /* renamed from: ˎ  reason: contains not printable characters */
        String f168;

        /* renamed from: ˏ  reason: contains not printable characters */
        String f169;

        /* renamed from: ॱ  reason: contains not printable characters */
        Integer f170;

        /* synthetic */ d(byte b) {
            this();
        }

        private d() {
            this.f168 = "_unknown_";
            this.f169 = "_unknown_";
            this.f170 = -1;
            this.f167 = false;
            this.f166 = false;
            this.f165 = false;
            try {
                Context r0 = r.m153();
                if (r0 != null) {
                    this.f165 = true;
                    TelephonyManager telephonyManager = (TelephonyManager) r0.getSystemService(PlaceFields.PHONE);
                    this.f168 = telephonyManager.getSimOperatorName();
                    this.f169 = telephonyManager.getNetworkOperatorName();
                    this.f170 = Integer.valueOf(telephonyManager.getPhoneType());
                    this.f167 = r.m144();
                    this.f166 = r.m149(r0);
                }
            } catch (Exception e) {
                o.m130(e);
            }
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static boolean m149(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static /* synthetic */ boolean m144() {
        Context context;
        int i;
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f58;
        if (weakReference != null) {
            context = weakReference.get();
        } else {
            context = null;
        }
        if (context != null) {
            int i2 = f158 + 27;
            f161 = i2 % 128;
            int i3 = i2 % 2;
            if ((Build.VERSION.SDK_INT < 17 ? (char) 22 : 19) != 22) {
                int i4 = f158 + 87;
                f161 = i4 % 128;
                int i5 = i4 % 2;
                i = Settings.Global.getInt(context.getContentResolver(), m151(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            } else {
                i = Settings.Secure.getInt(context.getContentResolver(), m151(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            }
        } else {
            i = 0;
        }
        if (!(i == 1)) {
            return false;
        }
        int i6 = f161 + 33;
        f158 = i6 % 128;
        int i7 = i6 % 2;
        return true;
    }
}
