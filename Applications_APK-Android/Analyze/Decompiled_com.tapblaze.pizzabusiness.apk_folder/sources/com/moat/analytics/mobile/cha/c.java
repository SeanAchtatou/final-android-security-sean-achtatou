package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.moat.analytics.mobile.cha.t;
import java.lang.ref.WeakReference;

final class c {
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean f37 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Application f38 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static boolean f39 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    static WeakReference<Activity> f40;
    /* access modifiers changed from: private */

    /* renamed from: ॱ  reason: contains not printable characters */
    public static int f41;

    c() {
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m29(Application application) {
        f38 = application;
        if (!f39) {
            f39 = true;
            f38.registerActivityLifecycleCallbacks(new a());
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static Application m27() {
        return f38;
    }

    static class a implements Application.ActivityLifecycleCallbacks {
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        a() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            int unused = c.f41 = 1;
        }

        public final void onActivityStarted(Activity activity) {
            try {
                c.f40 = new WeakReference<>(activity);
                int unused = c.f41 = 2;
                if (!c.f37) {
                    m31(true);
                }
                boolean unused2 = c.f37 = true;
                a.m6(3, "ActivityState", this, "Activity started: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m130(e);
            }
        }

        public final void onActivityResumed(Activity activity) {
            try {
                c.f40 = new WeakReference<>(activity);
                int unused = c.f41 = 3;
                t.m174().m180();
                a.m6(3, "ActivityState", this, "Activity resumed: " + activity.getClass() + "@" + activity.hashCode());
                if (((f) MoatAnalytics.getInstance()).f61) {
                    e.m43(activity);
                }
            } catch (Exception e) {
                o.m130(e);
            }
        }

        public final void onActivityPaused(Activity activity) {
            try {
                int unused = c.f41 = 4;
                if (c.m25(activity)) {
                    c.f40 = new WeakReference<>(null);
                }
                a.m6(3, "ActivityState", this, "Activity paused: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m130(e);
            }
        }

        public final void onActivityStopped(Activity activity) {
            try {
                if (c.f41 != 3) {
                    boolean unused = c.f37 = false;
                    m31(false);
                }
                int unused2 = c.f41 = 5;
                if (c.m25(activity)) {
                    c.f40 = new WeakReference<>(null);
                }
                a.m6(3, "ActivityState", this, "Activity stopped: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m130(e);
            }
        }

        public final void onActivityDestroyed(Activity activity) {
            try {
                if (!(c.f41 == 3 || c.f41 == 5)) {
                    if (c.f37) {
                        m31(false);
                    }
                    boolean unused = c.f37 = false;
                }
                int unused2 = c.f41 = 6;
                a.m6(3, "ActivityState", this, "Activity destroyed: " + activity.getClass() + "@" + activity.hashCode());
                if (c.m25(activity)) {
                    c.f40 = new WeakReference<>(null);
                }
            } catch (Exception e) {
                o.m130(e);
            }
        }

        /* renamed from: ॱ  reason: contains not printable characters */
        private static void m31(boolean z) {
            if (z) {
                a.m6(3, "ActivityState", null, "App became visible");
                if (t.m174().f183 == t.a.f194 && !((f) MoatAnalytics.getInstance()).f59) {
                    n.m116().m126();
                    return;
                }
                return;
            }
            a.m6(3, "ActivityState", null, "App became invisible");
            if (t.m174().f183 == t.a.f194 && !((f) MoatAnalytics.getInstance()).f59) {
                n.m116().m127();
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static /* synthetic */ boolean m25(Activity activity) {
        WeakReference<Activity> weakReference = f40;
        return weakReference != null && weakReference.get() == activity;
    }
}
