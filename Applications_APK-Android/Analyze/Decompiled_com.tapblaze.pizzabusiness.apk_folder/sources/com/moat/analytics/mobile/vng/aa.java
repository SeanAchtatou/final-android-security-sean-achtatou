package com.moat.analytics.mobile.vng;

import java.util.Iterator;
import java.util.LinkedHashSet;

class aa {
    private static final LinkedHashSet<String> a = new LinkedHashSet<>();

    aa() {
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [android.view.View, java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.moat.analytics.mobile.vng.a.b.a<android.webkit.WebView> a(android.view.ViewGroup r11, boolean r12) {
        /*
            java.lang.String r0 = "WebViewHound"
            if (r11 != 0) goto L_0x0009
            com.moat.analytics.mobile.vng.a.b.a r11 = com.moat.analytics.mobile.vng.a.b.a.a()     // Catch:{ Exception -> 0x007f }
            return r11
        L_0x0009:
            boolean r1 = r11 instanceof android.webkit.WebView     // Catch:{ Exception -> 0x007f }
            if (r1 == 0) goto L_0x0014
            android.webkit.WebView r11 = (android.webkit.WebView) r11     // Catch:{ Exception -> 0x007f }
            com.moat.analytics.mobile.vng.a.b.a r11 = com.moat.analytics.mobile.vng.a.b.a.a(r11)     // Catch:{ Exception -> 0x007f }
            return r11
        L_0x0014:
            java.util.LinkedList r1 = new java.util.LinkedList     // Catch:{ Exception -> 0x007f }
            r1.<init>()     // Catch:{ Exception -> 0x007f }
            r1.add(r11)     // Catch:{ Exception -> 0x007f }
            r11 = 0
            r2 = 0
            r4 = r2
            r3 = 0
        L_0x0020:
            boolean r5 = r1.isEmpty()     // Catch:{ Exception -> 0x007f }
            if (r5 != 0) goto L_0x007a
            r5 = 100
            if (r3 >= r5) goto L_0x007a
            int r3 = r3 + 1
            java.lang.Object r5 = r1.poll()     // Catch:{ Exception -> 0x007f }
            android.view.ViewGroup r5 = (android.view.ViewGroup) r5     // Catch:{ Exception -> 0x007f }
            int r6 = r5.getChildCount()     // Catch:{ Exception -> 0x007f }
            r7 = r4
            r4 = 0
        L_0x0038:
            if (r4 >= r6) goto L_0x0078
            android.view.View r8 = r5.getChildAt(r4)     // Catch:{ Exception -> 0x007f }
            boolean r9 = r8 instanceof android.webkit.WebView     // Catch:{ Exception -> 0x007f }
            if (r9 == 0) goto L_0x006c
            java.lang.String r9 = "Found WebView"
            r10 = 3
            com.moat.analytics.mobile.vng.o.a(r10, r0, r8, r9)     // Catch:{ Exception -> 0x007f }
            if (r12 != 0) goto L_0x0058
            int r9 = r8.hashCode()     // Catch:{ Exception -> 0x007f }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x007f }
            boolean r9 = a(r9)     // Catch:{ Exception -> 0x007f }
            if (r9 == 0) goto L_0x006c
        L_0x0058:
            if (r7 != 0) goto L_0x005e
            r7 = r8
            android.webkit.WebView r7 = (android.webkit.WebView) r7     // Catch:{ Exception -> 0x007f }
            goto L_0x006c
        L_0x005e:
            java.lang.String r4 = "Ambiguous ad container: multiple WebViews reside within it."
            com.moat.analytics.mobile.vng.o.a(r10, r0, r8, r4)     // Catch:{ Exception -> 0x007f }
            java.lang.String r4 = "[ERROR] "
            java.lang.String r5 = "WebAdTracker not created, ambiguous ad container: multiple WebViews reside within it"
            com.moat.analytics.mobile.vng.o.a(r4, r5)     // Catch:{ Exception -> 0x007f }
            r4 = r2
            goto L_0x0020
        L_0x006c:
            boolean r9 = r8 instanceof android.view.ViewGroup     // Catch:{ Exception -> 0x007f }
            if (r9 == 0) goto L_0x0075
            android.view.ViewGroup r8 = (android.view.ViewGroup) r8     // Catch:{ Exception -> 0x007f }
            r1.add(r8)     // Catch:{ Exception -> 0x007f }
        L_0x0075:
            int r4 = r4 + 1
            goto L_0x0038
        L_0x0078:
            r4 = r7
            goto L_0x0020
        L_0x007a:
            com.moat.analytics.mobile.vng.a.b.a r11 = com.moat.analytics.mobile.vng.a.b.a.b(r4)     // Catch:{ Exception -> 0x007f }
            return r11
        L_0x007f:
            com.moat.analytics.mobile.vng.a.b.a r11 = com.moat.analytics.mobile.vng.a.b.a.a()
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.vng.aa.a(android.view.ViewGroup, boolean):com.moat.analytics.mobile.vng.a.b.a");
    }

    private static boolean a(String str) {
        try {
            boolean add = a.add(str);
            if (a.size() > 50) {
                Iterator<String> it = a.iterator();
                for (int i = 0; i < 25 && it.hasNext(); i++) {
                    it.next();
                    it.remove();
                }
            }
            o.a(3, "WebViewHound", (Object) null, add ? "Newly Found WebView" : "Already Found WebView");
            return add;
        } catch (Exception e) {
            m.a(e);
            return false;
        }
    }
}
