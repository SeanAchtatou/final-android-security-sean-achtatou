package com.moat.analytics.mobile.cha;

import android.media.MediaPlayer;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class s extends i implements NativeVideoTracker {

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private WeakReference<MediaPlayer> f175;

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m160() {
        return "NativeVideoTracker";
    }

    s(String str) {
        super(str);
        a.m6(3, "NativeVideoTracker", this, "In initialization method.");
        if (str == null || str.isEmpty()) {
            StringBuilder sb = new StringBuilder("PartnerCode is ");
            sb.append(str == null ? "null" : "empty");
            String sb2 = sb.toString();
            String str2 = "NativeDisplayTracker creation problem, " + sb2;
            a.m6(3, "NativeVideoTracker", this, str2);
            a.m3("[ERROR] ", str2);
            this.f51 = new o(sb2);
        }
        a.m3("[SUCCESS] ", "NativeVideoTracker created");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ͺ  reason: contains not printable characters */
    public final boolean m162() {
        WeakReference<MediaPlayer> weakReference = this.f175;
        return (weakReference == null || weakReference.get() == null) ? false : true;
    }

    public final boolean trackVideoAd(Map<String, String> map, MediaPlayer mediaPlayer, View view) {
        try {
            m38();
            m40();
            if (mediaPlayer != null) {
                mediaPlayer.getCurrentPosition();
                this.f175 = new WeakReference<>(mediaPlayer);
                return super.m70(map, view);
            }
            throw new o("Null player instance");
        } catch (Exception unused) {
            throw new o("Playback has already completed");
        } catch (Exception e) {
            o.m130(e);
            String r2 = o.m129("trackVideoAd", e);
            if (this.f45 != null) {
                this.f45.onTrackingFailedToStart(r2);
            }
            a.m6(3, "NativeVideoTracker", this, r2);
            a.m3("[ERROR] ", "NativeVideoTracker " + r2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˋ  reason: contains not printable characters */
    public final Integer m163() {
        return Integer.valueOf(this.f175.get().getCurrentPosition());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˎ  reason: contains not printable characters */
    public final boolean m164() {
        return this.f175.get().isPlaying();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱᐝ  reason: contains not printable characters */
    public final Integer m165() {
        return Integer.valueOf(this.f175.get().getDuration());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public final Map<String, Object> m166() throws o {
        MediaPlayer mediaPlayer = this.f175.get();
        HashMap hashMap = new HashMap();
        hashMap.put("width", Integer.valueOf(mediaPlayer.getVideoWidth()));
        hashMap.put("height", Integer.valueOf(mediaPlayer.getVideoHeight()));
        hashMap.put("duration", Integer.valueOf(mediaPlayer.getDuration()));
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m161(List<String> list) throws o {
        WeakReference<MediaPlayer> weakReference = this.f175;
        if (!((weakReference == null || weakReference.get() == null) ? false : true)) {
            list.add("Player is null");
        }
        super.m16(list);
    }
}
