package com.moat.analytics.mobile.vng;

import android.graphics.Rect;
import android.view.View;
import androidx.core.app.NotificationCompat;
import com.moat.analytics.mobile.vng.NativeDisplayTracker;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

class s extends b implements NativeDisplayTracker {
    private final Map<String, String> g;
    private final Set<NativeDisplayTracker.MoatUserInteractionType> h = new HashSet();

    s(View view, Map<String, String> map) {
        super(view, true, false);
        m e;
        m mVar;
        o.a(3, "NativeDisplayTracker", this, "Initializing.");
        this.g = map;
        if (view == null) {
            o.a("[ERROR] ", 3, "NativeDisplayTracker", this, "NativeDisplayTracker initialization not successful, " + "Target view is null");
            mVar = new m("Target view is null");
        } else {
            if (map == null || map.isEmpty()) {
                o.a("[ERROR] ", 3, "NativeDisplayTracker", this, "NativeDisplayTracker initialization not successful, " + "AdIds is null or empty");
                e = new m("AdIds is null or empty");
            } else {
                g gVar = ((k) k.getInstance()).c;
                if (gVar == null) {
                    o.a("[ERROR] ", 3, "NativeDisplayTracker", this, "NativeDisplayTracker initialization not successful, " + "prepareNativeDisplayTracking was not called successfully");
                    mVar = new m("prepareNativeDisplayTracking was not called successfully");
                } else {
                    super.a(gVar.b);
                    try {
                        super.a(gVar.a);
                        i();
                        o.a("[SUCCESS] ", a() + " created for " + g() + ", with adIds:" + map.toString());
                        return;
                    } catch (m e2) {
                        e = e2;
                    }
                }
            }
            this.a = e;
            return;
        }
        this.a = mVar;
    }

    private static String a(Map<String, String> map) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (int i = 0; i < 8; i++) {
            String str = "moatClientLevel" + i;
            if (map.containsKey(str)) {
                linkedHashMap.put(str, map.get(str));
            }
        }
        for (int i2 = 0; i2 < 8; i2++) {
            String str2 = "moatClientSlicer" + i2;
            if (map.containsKey(str2)) {
                linkedHashMap.put(str2, map.get(str2));
            }
        }
        for (String next : map.keySet()) {
            if (!linkedHashMap.containsKey(next)) {
                linkedHashMap.put(next, map.get(next));
            }
        }
        return new JSONObject(linkedHashMap).toString();
    }

    private void i() {
        if (this.c != null) {
            this.c.a(j());
        }
    }

    private String j() {
        try {
            String a = a(this.g);
            o.a(3, "NativeDisplayTracker", this, "Parsed ad ids = " + a);
            return "{\"adIds\":" + a + ", \"adKey\":\"" + this.e + "\", \"adSize\":" + k() + "}";
        } catch (Exception e) {
            m.a(e);
            return "";
        }
    }

    private String k() {
        try {
            Rect a = y.a(super.f());
            int width = a.width();
            int height = a.height();
            HashMap hashMap = new HashMap();
            hashMap.put("width", Integer.toString(width));
            hashMap.put("height", Integer.toString(height));
            return new JSONObject(hashMap).toString();
        } catch (Exception e) {
            m.a(e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return "NativeDisplayTracker";
    }

    public void reportUserInteractionEvent(NativeDisplayTracker.MoatUserInteractionType moatUserInteractionType) {
        try {
            o.a(3, "NativeDisplayTracker", this, "reportUserInteractionEvent:" + moatUserInteractionType.name());
            if (!this.h.contains(moatUserInteractionType)) {
                this.h.add(moatUserInteractionType);
                JSONObject jSONObject = new JSONObject();
                jSONObject.accumulate("adKey", this.e);
                jSONObject.accumulate(NotificationCompat.CATEGORY_EVENT, moatUserInteractionType.name().toLowerCase());
                if (this.c != null) {
                    this.c.b(jSONObject.toString());
                }
            }
        } catch (JSONException e) {
            e = e;
            o.b(2, "NativeDisplayTracker", this, "Got JSON exception");
            m.a(e);
        } catch (Exception e2) {
            e = e2;
            m.a(e);
        }
    }
}
