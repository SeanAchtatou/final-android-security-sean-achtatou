package com.moat.analytics.mobile.iro;

import java.io.InputStream;
import java.io.InputStreamReader;

class q {
    q() {
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:13|14|(2:16|17)|18) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x004b, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0056, code lost:
        return com.moat.analytics.mobile.iro.a.b.a.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0057, code lost:
        if (r0 != null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005c, code lost:
        throw r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.moat.analytics.mobile.iro.a.b.a<java.lang.String> a(java.lang.String r3) {
        /*
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ IOException -> 0x004d }
            r1.<init>(r3)     // Catch:{ IOException -> 0x004d }
            java.net.URLConnection r3 = r1.openConnection()     // Catch:{ IOException -> 0x004d }
            java.lang.Object r3 = com.google.firebase.perf.network.FirebasePerfUrlConnection.instrument(r3)     // Catch:{ IOException -> 0x004d }
            java.net.URLConnection r3 = (java.net.URLConnection) r3     // Catch:{ IOException -> 0x004d }
            java.net.HttpURLConnection r3 = (java.net.HttpURLConnection) r3     // Catch:{ IOException -> 0x004d }
            r1 = 0
            r3.setUseCaches(r1)     // Catch:{ IOException -> 0x004d }
            r1 = 10000(0x2710, float:1.4013E-41)
            r3.setReadTimeout(r1)     // Catch:{ IOException -> 0x004d }
            r1 = 15000(0x3a98, float:2.102E-41)
            r3.setConnectTimeout(r1)     // Catch:{ IOException -> 0x004d }
            java.lang.String r1 = "GET"
            r3.setRequestMethod(r1)     // Catch:{ IOException -> 0x004d }
            r1 = 1
            r3.setDoInput(r1)     // Catch:{ IOException -> 0x004d }
            r3.connect()     // Catch:{ IOException -> 0x004d }
            int r1 = r3.getResponseCode()     // Catch:{ IOException -> 0x004d }
            r2 = 400(0x190, float:5.6E-43)
            if (r1 < r2) goto L_0x0039
            com.moat.analytics.mobile.iro.a.b.a r3 = com.moat.analytics.mobile.iro.a.b.a.a()     // Catch:{ IOException -> 0x004d }
            return r3
        L_0x0039:
            java.io.InputStream r0 = r3.getInputStream()     // Catch:{ IOException -> 0x004d }
            java.lang.String r3 = a(r0)     // Catch:{ IOException -> 0x004d }
            com.moat.analytics.mobile.iro.a.b.a r3 = com.moat.analytics.mobile.iro.a.b.a.a(r3)     // Catch:{ IOException -> 0x004d }
            if (r0 == 0) goto L_0x004a
            r0.close()     // Catch:{ IOException -> 0x004a }
        L_0x004a:
            return r3
        L_0x004b:
            r3 = move-exception
            goto L_0x0057
        L_0x004d:
            com.moat.analytics.mobile.iro.a.b.a r3 = com.moat.analytics.mobile.iro.a.b.a.a()     // Catch:{ all -> 0x004b }
            if (r0 == 0) goto L_0x0056
            r0.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0056:
            return r3
        L_0x0057:
            if (r0 == 0) goto L_0x005c
            r0.close()     // Catch:{ IOException -> 0x005c }
        L_0x005c:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.iro.q.a(java.lang.String):com.moat.analytics.mobile.iro.a.b.a");
    }

    private static String a(InputStream inputStream) {
        char[] cArr = new char[256];
        StringBuilder sb = new StringBuilder();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
        int i = 0;
        do {
            int read = inputStreamReader.read(cArr, 0, cArr.length);
            if (read <= 0) {
                break;
            }
            i += read;
            sb.append(cArr, 0, read);
        } while (i < 1024);
        return sb.toString();
    }

    static void b(final String str) {
        new Thread() {
            public void run() {
                try {
                    q.a(str);
                } catch (Exception unused) {
                }
            }
        }.start();
    }
}
