package com.moat.analytics.mobile.cha;

import android.graphics.Rect;
import android.view.View;
import androidx.core.app.NotificationCompat;
import com.moat.analytics.mobile.cha.NativeDisplayTracker;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class q extends d implements NativeDisplayTracker {

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private final Map<String, String> f156;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Set<NativeDisplayTracker.MoatUserInteractionType> f157 = new HashSet();

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m143() {
        return "NativeDisplayTracker";
    }

    q(View view, Map<String, String> map) {
        super(view, true, false);
        a.m6(3, "NativeDisplayTracker", this, "Initializing.");
        this.f156 = map;
        if (view == null) {
            String str = "NativeDisplayTracker initialization not successful, " + "Target view is null";
            a.m6(3, "NativeDisplayTracker", this, str);
            a.m3("[ERROR] ", str);
            this.f51 = new o("Target view is null");
        } else if (map == null || map.isEmpty()) {
            String str2 = "NativeDisplayTracker initialization not successful, " + "AdIds is null or empty";
            a.m6(3, "NativeDisplayTracker", this, str2);
            a.m3("[ERROR] ", str2);
            this.f51 = new o("AdIds is null or empty");
        } else {
            a aVar = ((f) f.getInstance()).f62;
            if (aVar == null) {
                String str3 = "NativeDisplayTracker initialization not successful, " + "prepareNativeDisplayTracking was not called successfully";
                a.m6(3, "NativeDisplayTracker", this, str3);
                a.m3("[ERROR] ", str3);
                this.f51 = new o("prepareNativeDisplayTracking was not called successfully");
                return;
            }
            this.f48 = aVar.f17;
            try {
                super.m41(aVar.f15);
                if (this.f48 != null) {
                    this.f48.m100(m141());
                }
                a.m3("[SUCCESS] ", "NativeDisplayTracker created for " + m32() + ", with adIds:" + map.toString());
            } catch (o e) {
                this.f51 = e;
            }
        }
    }

    public final void reportUserInteractionEvent(NativeDisplayTracker.MoatUserInteractionType moatUserInteractionType) {
        try {
            a.m6(3, "NativeDisplayTracker", this, "reportUserInteractionEvent:" + moatUserInteractionType.name());
            if (!this.f157.contains(moatUserInteractionType)) {
                this.f157.add(moatUserInteractionType);
                JSONObject jSONObject = new JSONObject();
                jSONObject.accumulate("adKey", this.f47);
                jSONObject.accumulate(NotificationCompat.CATEGORY_EVENT, moatUserInteractionType.name().toLowerCase());
                if (this.f48 != null) {
                    this.f48.m101(jSONObject.toString());
                }
            }
        } catch (JSONException e) {
            a.m7("NativeDisplayTracker", this, "Got JSON exception");
            o.m130(e);
        } catch (Exception e2) {
            o.m130(e2);
        }
    }

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private String m141() {
        try {
            Map<String, String> map = this.f156;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (int i = 0; i < 8; i++) {
                String str = "moatClientLevel" + i;
                if (map.containsKey(str)) {
                    linkedHashMap.put(str, map.get(str));
                }
            }
            for (int i2 = 0; i2 < 8; i2++) {
                String str2 = "moatClientSlicer" + i2;
                if (map.containsKey(str2)) {
                    linkedHashMap.put(str2, map.get(str2));
                }
            }
            for (String next : map.keySet()) {
                if (!linkedHashMap.containsKey(next)) {
                    linkedHashMap.put(next, map.get(next));
                }
            }
            String jSONObject = new JSONObject(linkedHashMap).toString();
            a.m6(3, "NativeDisplayTracker", this, "Parsed ad ids = " + jSONObject);
            return "{\"adIds\":" + jSONObject + ", \"adKey\":\"" + this.f47 + "\", \"adSize\":" + m142() + "}";
        } catch (Exception e) {
            o.m130(e);
            return "";
        }
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private String m142() {
        try {
            Rect r0 = u.m189(super.m33());
            int width = r0.width();
            int height = r0.height();
            HashMap hashMap = new HashMap();
            hashMap.put("width", Integer.toString(width));
            hashMap.put("height", Integer.toString(height));
            return new JSONObject(hashMap).toString();
        } catch (Exception e) {
            o.m130(e);
            return null;
        }
    }
}
