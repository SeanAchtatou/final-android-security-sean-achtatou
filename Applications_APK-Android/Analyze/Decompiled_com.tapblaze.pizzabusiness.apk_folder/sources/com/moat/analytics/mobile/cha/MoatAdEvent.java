package com.moat.analytics.mobile.cha;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.ironsource.sdk.constants.Constants;
import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent {
    public static final Double VOLUME_MUTED = Double.valueOf((double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
    public static final Double VOLUME_UNMUTED = Double.valueOf(1.0d);

    /* renamed from: ˋ  reason: contains not printable characters */
    static final Integer f1 = Integer.MIN_VALUE;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final Double f2 = Double.valueOf(Double.NaN);

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Long f3;

    /* renamed from: ˊ  reason: contains not printable characters */
    Double f4;

    /* renamed from: ˏ  reason: contains not printable characters */
    Integer f5;

    /* renamed from: ॱ  reason: contains not printable characters */
    MoatAdEventType f6;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Double f7;

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num, Double d) {
        this.f3 = Long.valueOf(System.currentTimeMillis());
        this.f6 = moatAdEventType;
        this.f4 = d;
        this.f5 = num;
        this.f7 = Double.valueOf(r.m154());
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num) {
        this(moatAdEventType, num, f2);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType) {
        this(moatAdEventType, f1, f2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final Map<String, Object> m0() {
        HashMap hashMap = new HashMap();
        hashMap.put("adVolume", this.f4);
        hashMap.put("playhead", this.f5);
        hashMap.put("aTimeStamp", this.f3);
        hashMap.put("type", this.f6.toString());
        hashMap.put(Constants.RequestParameters.DEVICE_VOLUME, this.f7);
        return hashMap;
    }
}
