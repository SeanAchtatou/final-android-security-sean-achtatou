package com.moat.analytics.mobile.vng;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.vng.a.b.a;
import com.moat.analytics.mobile.vng.u;
import com.moat.analytics.mobile.vng.w;
import java.lang.ref.WeakReference;
import java.util.Map;

class n extends MoatFactory {
    n() {
        if (!a()) {
            o.a("[ERROR] ", 3, "Factory", this, "Failed to initialize MoatFactory" + ", SDK was not started");
            throw new m("Failed to initialize MoatFactory");
        }
    }

    private NativeDisplayTracker a(View view, final Map<String, String> map) {
        final WeakReference weakReference = new WeakReference(view);
        return (NativeDisplayTracker) w.a(new w.a<NativeDisplayTracker>() {
            public a<NativeDisplayTracker> a() {
                View view = (View) weakReference.get();
                o.a("[INFO] ", 3, "Factory", this, "Attempting to create NativeDisplayTracker for " + o.a(view));
                return a.a(new s(view, map));
            }
        }, NativeDisplayTracker.class);
    }

    private NativeVideoTracker a(final String str) {
        return (NativeVideoTracker) w.a(new w.a<NativeVideoTracker>() {
            public a<NativeVideoTracker> a() {
                o.a("[INFO] ", 3, "Factory", this, "Attempting to create NativeVideoTracker");
                return a.a(new t(str));
            }
        }, NativeVideoTracker.class);
    }

    private WebAdTracker a(ViewGroup viewGroup) {
        final WeakReference weakReference = new WeakReference(viewGroup);
        return (WebAdTracker) w.a(new w.a<WebAdTracker>() {
            public a<WebAdTracker> a() {
                ViewGroup viewGroup = (ViewGroup) weakReference.get();
                o.a("[INFO] ", 3, "Factory", this, "Attempting to create WebAdTracker for adContainer " + o.a(viewGroup));
                return a.a(new z(viewGroup));
            }
        }, WebAdTracker.class);
    }

    private WebAdTracker a(WebView webView) {
        final WeakReference weakReference = new WeakReference(webView);
        return (WebAdTracker) w.a(new w.a<WebAdTracker>() {
            public a<WebAdTracker> a() {
                WebView webView = (WebView) weakReference.get();
                o.a("[INFO] ", 3, "Factory", this, "Attempting to create WebAdTracker for " + o.a(webView));
                return a.a(new z(webView));
            }
        }, WebAdTracker.class);
    }

    private <T> T a(MoatPlugin<T> moatPlugin) {
        return moatPlugin.a();
    }

    private boolean a() {
        return ((k) k.getInstance()).a();
    }

    public <T> T createCustomTracker(MoatPlugin<T> moatPlugin) {
        try {
            return a(moatPlugin);
        } catch (Exception e) {
            m.a(e);
            return moatPlugin.b();
        }
    }

    public NativeDisplayTracker createNativeDisplayTracker(View view, Map<String, String> map) {
        try {
            return a(view, map);
        } catch (Exception e) {
            m.a(e);
            return new u.c();
        }
    }

    public NativeVideoTracker createNativeVideoTracker(String str) {
        try {
            return a(str);
        } catch (Exception e) {
            m.a(e);
            return new u.d();
        }
    }

    public WebAdTracker createWebAdTracker(ViewGroup viewGroup) {
        try {
            return a(viewGroup);
        } catch (Exception e) {
            m.a(e);
            return new u.e();
        }
    }

    public WebAdTracker createWebAdTracker(WebView webView) {
        try {
            return a(webView);
        } catch (Exception e) {
            m.a(e);
            return new u.e();
        }
    }
}
