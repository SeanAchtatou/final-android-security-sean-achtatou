package com.moat.analytics.mobile.cha;

import android.view.ViewGroup;
import android.webkit.WebView;

final class v extends d implements WebAdTracker {
    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m197() {
        return "WebAdTracker";
    }

    v(ViewGroup viewGroup) {
        this(x.m202(viewGroup, false).orElse(null));
        if (viewGroup == null) {
            String str = "WebAdTracker initialization not successful, " + "Target ViewGroup is null";
            a.m6(3, "WebAdTracker", this, str);
            a.m3("[ERROR] ", str);
            this.f51 = new o("Target ViewGroup is null");
        }
        if (this.f49 == null) {
            String str2 = "WebAdTracker initialization not successful, " + "No WebView to track inside of ad container";
            a.m6(3, "WebAdTracker", this, str2);
            a.m3("[ERROR] ", str2);
            this.f51 = new o("No WebView to track inside of ad container");
        }
    }

    v(WebView webView) {
        super(webView, false, false);
        a.m6(3, "WebAdTracker", this, "Initializing.");
        if (webView == null) {
            String str = "WebAdTracker initialization not successful, " + "WebView is null";
            a.m6(3, "WebAdTracker", this, str);
            a.m3("[ERROR] ", str);
            this.f51 = new o("WebView is null");
            return;
        }
        try {
            super.m41(webView);
            a.m3("[SUCCESS] ", "WebAdTracker created for " + m32());
        } catch (o e) {
            this.f51 = e;
        }
    }
}
