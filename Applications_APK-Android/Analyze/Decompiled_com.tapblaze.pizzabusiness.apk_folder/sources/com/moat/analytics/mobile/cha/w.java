package com.moat.analytics.mobile.cha;

import android.view.View;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class w extends b implements ReactiveVideoTracker {

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private Integer f224;

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m198() {
        return "ReactiveVideoTracker";
    }

    public w(String str) {
        super(str);
        a.m6(3, "ReactiveVideoTracker", this, "Initializing.");
        a.m3("[SUCCESS] ", "ReactiveVideoTracker created");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public final Map<String, Object> m201() throws o {
        Integer num;
        HashMap hashMap = new HashMap();
        View view = (View) this.f26.get();
        int i = 0;
        if (view != null) {
            i = Integer.valueOf(view.getWidth());
            num = Integer.valueOf(view.getHeight());
        } else {
            num = 0;
        }
        hashMap.put("duration", this.f224);
        hashMap.put("width", i);
        hashMap.put("height", num);
        return hashMap;
    }

    public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
        try {
            m38();
            m40();
            this.f224 = num;
            return super.m21(map, view);
        } catch (Exception e) {
            m42("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final JSONObject m200(MoatAdEvent moatAdEvent) {
        if (moatAdEvent.f6 == MoatAdEventType.AD_EVT_COMPLETE && !moatAdEvent.f5.equals(MoatAdEvent.f1) && !m13(moatAdEvent.f5, this.f224)) {
            moatAdEvent.f6 = MoatAdEventType.AD_EVT_STOPPED;
        }
        return super.m18(moatAdEvent);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m199(List<String> list) throws o {
        if (this.f224.intValue() >= 1000) {
            super.m16(list);
            return;
        }
        throw new o(String.format(Locale.ROOT, "Invalid duration = %d. Please make sure duration is in milliseconds.", this.f224));
    }
}
