package com.codeexotics.tools;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import androidx.core.view.ViewCompat;
import com.tapblaze.pizzabusiness.AppActivity;
import com.vungle.warren.model.Advertisement;
import cz.msebera.android.httpclient.protocol.HTTP;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class SharingController {
    public static String getTempFile() {
        File file = new File(AppActivity.getContext().getExternalFilesDir(null), "/Codeexotics/.temp/");
        if (file.exists() || file.mkdirs()) {
            return new File(AppActivity.getContext().getExternalFilesDir(null), "/Codeexotics/.temp/temp.png").getAbsolutePath();
        }
        Log.e("Codeexotics Tools", "Unable to create temp directory (" + file.getPath() + ").");
        return "";
    }

    public static File getTempFileObject() {
        return new File(new File(AppActivity.getContext().getExternalFilesDir(null), "/Codeexotics/.temp/"), "temp.png");
    }

    private static boolean findTwitterClient() {
        try {
            AppActivity.getInstance().getPackageManager().getApplicationInfo("com.twitter.android", 0);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static void shareOnTwitter() {
        Uri uri;
        if (findTwitterClient()) {
            Intent intent = new Intent("android.intent.action.SEND", Uri.parse("twitter:"));
            intent.setType("image/*");
            intent.putExtra("android.intent.extra.TEXT", FacebookSharing.shareMessage + " " + FacebookSharing.shareURL);
            if (Build.VERSION.SDK_INT >= 24) {
                intent.setFlags(1);
                Context context = AppActivity.getContext();
                uri = FileProvider.getUriForFile(context, getAppPackageName() + ".fileprovider", getTempFileObject());
            } else {
                uri = Uri.parse(Advertisement.FILE_SCHEME + getTempFile());
            }
            intent.putExtra("android.intent.extra.STREAM", uri);
            intent.setPackage("com.twitter.android");
            AppActivity.getInstance().startActivity(intent);
            return;
        }
        AppActivity.getInstance().runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(AppActivity.getInstance());
                builder.setTitle("Good Pizza, Great Pizza").setMessage("Sorry, you need to install Twitter application first!").setNeutralButton("Ok", (DialogInterface.OnClickListener) null);
                builder.show();
            }
        });
    }

    public static void shareViaEmail() {
        Intent intent = new Intent("android.intent.action.SEND", Uri.parse("mailto:"));
        intent.putExtra("android.intent.extra.SUBJECT", FacebookSharing.shareTitle);
        intent.putExtra("android.intent.extra.TEXT", Html.fromHtml(FacebookSharing.shareMessage));
        intent.putExtra("android.intent.extra.STREAM", Uri.parse(Advertisement.FILE_SCHEME + getTempFile()));
        intent.setType("message/rfc822");
        AppActivity.getInstance().startActivity(Intent.createChooser(intent, "Share photo:"));
    }

    public static void tweetAbout() {
        if (findTwitterClient()) {
            Intent intent = new Intent("android.intent.action.SEND", Uri.parse("twitter:"));
            intent.setType(HTTP.PLAIN_TEXT_TYPE);
            intent.putExtra("android.intent.extra.TEXT", FacebookSharing.shareMessage);
            intent.setPackage("com.twitter.android");
            AppActivity.getInstance().startActivity(intent);
            return;
        }
        AppActivity.getInstance().runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(AppActivity.getInstance());
                builder.setTitle("Good Pizza, Great Pizza").setMessage("Sorry, you need to install Twitter application first!").setNeutralButton("Ok", (DialogInterface.OnClickListener) null);
                builder.show();
            }
        });
    }

    public static void followInstagram(String str, String str2) {
        Uri parse = Uri.parse(str);
        Uri parse2 = Uri.parse(str2);
        Intent intent = new Intent("android.intent.action.VIEW", parse);
        intent.setPackage("com.instagram.android");
        try {
            AppActivity.getInstance().startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            AppActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", parse2));
        }
    }

    public static void shareOnInstagram() {
        AppActivity.getInstance().runOnUiThread(new Runnable() {
            public void run() {
                boolean z;
                Uri uri;
                try {
                    AppActivity.getInstance().getPackageManager().getApplicationInfo("com.instagram.android", 0);
                    z = true;
                } catch (PackageManager.NameNotFoundException unused) {
                    z = false;
                }
                if (!z) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AppActivity.getInstance());
                    builder.setTitle("Cannot Share!").setMessage("Instagram is not installed on your device.").setPositiveButton("Ok", (DialogInterface.OnClickListener) null);
                    builder.show();
                    return;
                }
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("image/*");
                Bitmap decodeFile = BitmapFactory.decodeFile(SharingController.getTempFile());
                if (decodeFile != null) {
                    float min = Math.min(1080.0f / ((float) decodeFile.getWidth()), 1080.0f / ((float) decodeFile.getHeight()));
                    int ceil = (int) Math.ceil((double) (((float) decodeFile.getWidth()) * min));
                    int ceil2 = (int) Math.ceil((double) (((float) decodeFile.getHeight()) * min));
                    Bitmap createBitmap = Bitmap.createBitmap(ceil, ceil2, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(createBitmap);
                    Paint paint = new Paint();
                    paint.setColor((int) ViewCompat.MEASURED_STATE_MASK);
                    canvas.drawRect(new Rect(0, 0, ceil, ceil2), paint);
                    canvas.drawBitmap(decodeFile, (Rect) null, new Rect(0, 0, ceil + 0, ceil2 + 0), (Paint) null);
                    File file = new File(AppActivity.getContext().getExternalFilesDir(null), "/Codeexotics/.temp/instagram.png");
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(file.getAbsolutePath());
                        createBitmap.compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
                        fileOutputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (Build.VERSION.SDK_INT >= 24) {
                        intent.setFlags(1);
                        Context context = AppActivity.getContext();
                        uri = FileProvider.getUriForFile(context, SharingController.getAppPackageName() + ".fileprovider", file);
                    } else {
                        uri = Uri.parse(Advertisement.FILE_SCHEME + file.getAbsolutePath());
                    }
                    intent.putExtra("android.intent.extra.TEXT", FacebookSharing.shareMessage + " " + FacebookSharing.shareURL);
                    intent.putExtra("android.intent.extra.TITLE", FacebookSharing.shareTitle);
                    intent.putExtra("android.intent.extra.SUBJECT", FacebookSharing.shareHashTag);
                    intent.putExtra("android.intent.extra.STREAM", uri);
                    intent.setPackage("com.instagram.android");
                    try {
                        AppActivity.getInstance().startActivity(intent);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        });
    }

    private static void addImageToGallery(String str, Context context) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("datetaken", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("mime_type", "image/png");
        contentValues.put("_data", str);
        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
    }

    public static void saveToGallery() {
        File file = new File(Environment.getExternalStorageDirectory() + "/Codeexotics/Documents/");
        if (file.exists() || file.mkdirs()) {
            int integer = LocalStorage.getInteger("CurrentPhotoID") + 1;
            LocalStorage.setInteger("CurrentPhotoID", integer);
            String str = file + "/Photo" + integer + ".png";
            File file2 = new File(str);
            try {
                FileInputStream fileInputStream = new FileInputStream(new File(getTempFile()));
                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read > 0) {
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        fileInputStream.close();
                        fileOutputStream.close();
                        addImageToGallery(str, AppActivity.getContext());
                        AppActivity.getInstance().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(AppActivity.getInstance(), "Photo has been saved", 0).show();
                            }
                        });
                        return;
                    }
                }
            } catch (Exception unused) {
                Log.e("Codeexotics Tools", "Unable to copy photo to documents directory.");
            }
        } else {
            Log.e("Codeexotics Tools", "Unable to create documents directory (" + file.getPath() + ").");
        }
    }

    public static void inviteWithEmail(String str) {
        Intent intent = new Intent("android.intent.action.SEND", Uri.parse("mailto:"));
        intent.putExtra("android.intent.extra.SUBJECT", FacebookSharing.shareTitle);
        intent.putExtra("android.intent.extra.TEXT", Html.fromHtml(FacebookSharing.shareMessage));
        intent.setType("message/rfc822");
        AppActivity.getInstance().startActivity(Intent.createChooser(intent, "Invite friends with email:"));
    }

    public static void inviteWithSMS(String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setType("vnd.android-dir/mms-sms");
            intent.putExtra("sms_body", FacebookSharing.shareMessage);
            AppActivity.getInstance().startActivity(intent);
        } catch (Exception unused) {
            AppActivity.getInstance().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(AppActivity.getInstance(), "You haven't any SMS app installed on your device", 1).show();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static String getAppPackageName() {
        try {
            return AppActivity.getInstance().getPackageManager().getPackageInfo(AppActivity.getInstance().getPackageName(), 0).applicationInfo.packageName;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
