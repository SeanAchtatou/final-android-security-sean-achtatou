package com.codeexotics.tools;

import android.content.Context;

public class Resources {
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001a A[ADDED_TO_REGION, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getString(java.lang.String r3, android.content.Context r4) {
        /*
            r0 = 0
            android.content.Context r1 = r4.getApplicationContext()     // Catch:{ Exception -> 0x0016 }
            java.lang.String r1 = r1.getPackageName()     // Catch:{ Exception -> 0x0016 }
            android.content.Context r4 = r4.getApplicationContext()     // Catch:{ Exception -> 0x0017 }
            android.content.pm.PackageManager r4 = r4.getPackageManager()     // Catch:{ Exception -> 0x0017 }
            android.content.res.Resources r4 = r4.getResourcesForApplication(r1)     // Catch:{ Exception -> 0x0017 }
            goto L_0x0018
        L_0x0016:
            r1 = r0
        L_0x0017:
            r4 = r0
        L_0x0018:
            if (r4 != 0) goto L_0x001b
            return r0
        L_0x001b:
            java.lang.String r2 = "string"
            int r3 = r4.getIdentifier(r3, r2, r1)
            if (r3 != 0) goto L_0x0024
            return r0
        L_0x0024:
            java.lang.String r3 = r4.getString(r3)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.codeexotics.tools.Resources.getString(java.lang.String, android.content.Context):java.lang.String");
    }

    public static int getLayout(String str, Context context) {
        String str2;
        android.content.res.Resources resources = null;
        try {
            str2 = context.getApplicationContext().getPackageName();
            try {
                resources = context.getApplicationContext().getPackageManager().getResourcesForApplication(str2);
            } catch (Exception unused) {
            }
        } catch (Exception unused2) {
            str2 = null;
        }
        if (resources == null) {
            return 0;
        }
        return resources.getIdentifier(str, "layout", str2);
    }

    public static int getXML(String str, Context context) {
        String str2;
        android.content.res.Resources resources = null;
        try {
            str2 = context.getApplicationContext().getPackageName();
            try {
                resources = context.getApplicationContext().getPackageManager().getResourcesForApplication(str2);
            } catch (Exception unused) {
            }
        } catch (Exception unused2) {
            str2 = null;
        }
        if (resources == null) {
            return 0;
        }
        return resources.getIdentifier(str, "xml", str2);
    }

    public static int getDrawable(String str, Context context) {
        String str2;
        android.content.res.Resources resources = null;
        try {
            str2 = context.getApplicationContext().getPackageName();
            try {
                resources = context.getApplicationContext().getPackageManager().getResourcesForApplication(str2);
            } catch (Exception unused) {
            }
        } catch (Exception unused2) {
            str2 = null;
        }
        if (resources == null) {
            return 0;
        }
        return resources.getIdentifier(str, "drawable", str2);
    }
}
