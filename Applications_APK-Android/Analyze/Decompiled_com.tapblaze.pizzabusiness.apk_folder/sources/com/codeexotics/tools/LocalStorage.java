package com.codeexotics.tools;

import android.content.SharedPreferences;
import com.tapblaze.pizzabusiness.AppActivity;

public class LocalStorage {
    private static SharedPreferences getPreferences() {
        return AppActivity.getInstance().getPreferences(0);
    }

    public static void setString(String str, String str2) {
        SharedPreferences.Editor edit = getPreferences().edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public static String getString(String str) {
        return getPreferences().getString(str, "");
    }

    public static void setInteger(String str, int i) {
        SharedPreferences.Editor edit = getPreferences().edit();
        edit.putInt(str, i);
        edit.commit();
    }

    public static int getInteger(String str) {
        return getPreferences().getInt(str, 0);
    }

    public static void setFloat(String str, float f) {
        SharedPreferences.Editor edit = getPreferences().edit();
        edit.putFloat(str, f);
        edit.commit();
    }

    public static float getFloat(String str) {
        return getPreferences().getFloat(str, 0.0f);
    }

    public static void setBoolean(String str, boolean z) {
        SharedPreferences.Editor edit = getPreferences().edit();
        edit.putBoolean(str, z);
        edit.commit();
    }

    public static boolean getBoolean(String str) {
        return getPreferences().getBoolean(str, false);
    }
}
