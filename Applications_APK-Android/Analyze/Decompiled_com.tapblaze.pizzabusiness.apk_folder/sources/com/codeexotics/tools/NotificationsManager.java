package com.codeexotics.tools;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.google.android.gms.drive.DriveFile;
import com.tapblaze.pizzabusiness.AppActivity;
import java.util.Calendar;

public class NotificationsManager {
    public static void clearNotification() {
        int integer = LocalStorage.getInteger("MaxNotificationIndex");
        ((NotificationManager) AppActivity.getInstance().getSystemService("notification")).cancelAll();
        for (int i = 1; i < integer; i++) {
            Intent intent = new Intent(AppActivity.getInstance(), MyReceiver.class);
            String substring = AppActivity.getInstance().getClass().getName().substring(0, AppActivity.getInstance().getClass().getName().lastIndexOf("."));
            intent.setAction(substring + ".LOCAL_NOTIFICATION");
            ((AlarmManager) AppActivity.getInstance().getSystemService(NotificationCompat.CATEGORY_ALARM)).cancel(PendingIntent.getBroadcast(AppActivity.getInstance(), i, intent, 0));
        }
        LocalStorage.setInteger("MaxNotificationIndex", 1);
    }

    public static void scheduleNotification(String str, String str2, int i) {
        if (i > 0) {
            int integer = LocalStorage.getInteger("MaxNotificationIndex");
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(System.currentTimeMillis());
            instance.add(13, i);
            Intent intent = new Intent(AppActivity.getInstance(), MyReceiver.class);
            intent.putExtra(ViewHierarchyConstants.TEXT_KEY, str);
            String substring = AppActivity.getInstance().getClass().getName().substring(0, AppActivity.getInstance().getClass().getName().lastIndexOf("."));
            intent.setAction(substring + ".LOCAL_NOTIFICATION");
            intent.addFlags(DriveFile.MODE_READ_ONLY);
            int i2 = integer + 1;
            PendingIntent broadcast = PendingIntent.getBroadcast(AppActivity.getInstance(), integer, intent, 134217728);
            AlarmManager alarmManager = (AlarmManager) AppActivity.getInstance().getSystemService(NotificationCompat.CATEGORY_ALARM);
            if (Build.VERSION.SDK_INT >= 19) {
                alarmManager.setExact(0, instance.getTimeInMillis(), broadcast);
            } else {
                alarmManager.set(0, instance.getTimeInMillis(), broadcast);
            }
            LocalStorage.setInteger("MaxNotificationIndex", i2);
        }
    }

    public static void scheduleRepeatingNotification(String str, String str2, int i, int i2) {
        if (i > 0) {
            long j = (long) (i2 * 1000);
            int integer = LocalStorage.getInteger("MaxNotificationIndex");
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(System.currentTimeMillis());
            instance.add(13, i);
            Intent intent = new Intent(AppActivity.getInstance(), MyReceiver.class);
            intent.putExtra(ViewHierarchyConstants.TEXT_KEY, str);
            String substring = AppActivity.getInstance().getClass().getName().substring(0, AppActivity.getInstance().getClass().getName().lastIndexOf("."));
            intent.setAction(substring + ".LOCAL_NOTIFICATION");
            intent.addFlags(DriveFile.MODE_READ_ONLY);
            PendingIntent broadcast = PendingIntent.getBroadcast(AppActivity.getInstance(), integer, intent, 134217728);
            ((AlarmManager) AppActivity.getInstance().getSystemService(NotificationCompat.CATEGORY_ALARM)).setRepeating(0, instance.getTimeInMillis(), j, broadcast);
            LocalStorage.setInteger("MaxNotificationIndex", integer + 1);
        }
    }
}
