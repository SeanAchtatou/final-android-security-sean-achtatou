package com.codeexotics.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.kochava.base.ReferralReceiver;

public class MyReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        new ReferralReceiver().onReceive(context, intent);
        Log.i("App", "called receiver method");
        try {
            NotificationUtils.generateNotification(context, intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
