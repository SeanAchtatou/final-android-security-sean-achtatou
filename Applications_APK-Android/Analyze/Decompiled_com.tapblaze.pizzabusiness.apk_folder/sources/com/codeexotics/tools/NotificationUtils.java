package com.codeexotics.tools;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.tapblaze.pizzabusiness.AppActivity;
import com.tapblaze.pizzabusiness.R;

public class NotificationUtils {
    public static NotificationManager mManager;

    public static void generateNotification(Context context, Intent intent) {
        Notification notification;
        mManager = (NotificationManager) context.getSystemService("notification");
        Intent intent2 = new Intent(context, AppActivity.class);
        intent2.addFlags(603979776);
        PendingIntent activity = PendingIntent.getActivity(context, 0, intent2, 134217728);
        if (Build.VERSION.SDK_INT >= 16) {
            notification = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.statusbar).setContentTitle(context.getString(R.string.app_name)).setContentText(intent.getStringExtra(ViewHierarchyConstants.TEXT_KEY)).setWhen(System.currentTimeMillis()).setAutoCancel(true).setDefaults(1).setContentIntent(activity).setPriority(1).build();
        } else {
            notification = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.statusbar).setContentTitle(context.getString(R.string.app_name)).setContentText(intent.getStringExtra(ViewHierarchyConstants.TEXT_KEY)).setWhen(System.currentTimeMillis()).setAutoCancel(true).setDefaults(1).setContentIntent(activity).setPriority(1).build();
        }
        mManager.notify(0, notification);
    }
}
