package com.codeexotics.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.Toast;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.tapblaze.pizzabusiness.AppActivity;

public class FacebookSharing {
    public static String shareHashTag;
    public static String shareMessage;
    public static String sharePictureURL;
    public static String shareTitle;
    public static String shareURL;

    public static void setMessage(String str) {
        shareMessage = str;
    }

    public static void setLink(String str) {
        shareURL = str;
    }

    public static void setPictureLink(String str) {
        sharePictureURL = str;
    }

    public static void setTitle(String str) {
        shareTitle = str;
    }

    public static void setHashTag(String str) {
        shareHashTag = str;
    }

    public static void ShareOnFacebookMessage() {
        ShareLinkContent build = ((ShareLinkContent.Builder) new ShareLinkContent.Builder().setContentTitle(shareTitle).setContentDescription(shareMessage).setContentUrl(Uri.parse(shareURL))).setImageUrl(Uri.parse(sharePictureURL)).build();
        ShareDialog shareDialog = new ShareDialog(AppActivity.getInstance());
        if (shareDialog.canShow(build, ShareDialog.Mode.NATIVE)) {
            shareDialog.show(build, ShareDialog.Mode.NATIVE);
        } else {
            shareDialog.show(build, ShareDialog.Mode.FEED);
        }
        shareURL = "";
        shareMessage = "";
        sharePictureURL = "";
        shareTitle = "";
    }

    public static void ShareOnFacebookPhoto() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap decodeFile = BitmapFactory.decodeFile(SharingController.getTempFile(), options);
        SharePhotoContent build = ((SharePhotoContent.Builder) new SharePhotoContent.Builder().addPhoto(new SharePhoto.Builder().setUserGenerated(true).setBitmap(decodeFile).build()).setShareHashtag(new ShareHashtag.Builder().setHashtag(shareHashTag).build())).build();
        ShareDialog shareDialog = new ShareDialog(AppActivity.getInstance());
        if (shareDialog.canShow(build, ShareDialog.Mode.NATIVE)) {
            shareDialog.show(build, ShareDialog.Mode.NATIVE);
        } else if (shareDialog.canShow(build, ShareDialog.Mode.FEED)) {
            shareDialog.show(build, ShareDialog.Mode.FEED);
        } else {
            AppActivity.getInstance().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(AppActivity.getContext(), "To share photo you need Facebook application installed.", 1).show();
                }
            });
        }
        shareURL = "";
        shareMessage = "";
        sharePictureURL = "";
        shareTitle = "";
    }
}
