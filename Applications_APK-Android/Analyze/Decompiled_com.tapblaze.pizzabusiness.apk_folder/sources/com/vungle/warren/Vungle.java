package com.vungle.warren;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.google.android.gms.drive.DriveFile;
import com.google.gson.JsonObject;
import com.vungle.warren.AdConfig;
import com.vungle.warren.VungleSettings;
import com.vungle.warren.analytics.VungleAnalytics;
import com.vungle.warren.downloader.AssetDownloader;
import com.vungle.warren.downloader.DownloadRequest;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.DatabaseHelper;
import com.vungle.warren.persistence.GraphicDesigner;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.tasks.ReconfigJob;
import com.vungle.warren.tasks.SendReportsJob;
import com.vungle.warren.tasks.VungleJobCreator;
import com.vungle.warren.tasks.utility.JobRunnerThreadPriorityHelper;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.ui.VungleActivity;
import com.vungle.warren.ui.VungleFlexViewActivity;
import com.vungle.warren.ui.contract.AdContract;
import com.vungle.warren.ui.view.VungleNativeView;
import com.vungle.warren.utility.NetworkProvider;
import com.vungle.warren.utility.SDKExecutors;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Vungle {
    private static final String COM_VUNGLE_SDK = "com.vungle.sdk";
    /* access modifiers changed from: private */
    public static final String TAG = Vungle.class.getCanonicalName();
    static final Vungle _instance = new Vungle();
    /* access modifiers changed from: private */
    public static CacheManager.Listener cacheListener = new CacheManager.Listener() {
        public void onCacheChanged() {
            Vungle.stopPlaying();
            if (Vungle._instance != null && Vungle._instance.cacheManager.getCache() != null) {
                List<DownloadRequest> allRequests = Vungle._instance.downloader.getAllRequests();
                String path = Vungle._instance.cacheManager.getCache().getPath();
                for (DownloadRequest next : allRequests) {
                    if (!next.path.startsWith(path)) {
                        Vungle._instance.downloader.cancel(next);
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public static AtomicBoolean isDepInit = new AtomicBoolean(false);
    private static volatile boolean isInitialized = false;
    private static AtomicBoolean isInitializing = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public static ReconfigJob.ReconfigCall reconfigCall = new ReconfigJob.ReconfigCall() {
        public void reConfigVungle() {
            Vungle.reConfigure();
        }
    };
    static SDKExecutors sdkExecutors = new SDKExecutors();
    /* access modifiers changed from: private */
    public static final VungleStaticApi vungleApi = new VungleStaticApi() {
        public boolean isInitialized() {
            return Vungle.isInitialized();
        }

        public Collection<String> getValidPlacements() {
            return Vungle.getValidPlacements();
        }
    };
    /* access modifiers changed from: private */
    public AdLoader adLoader;
    /* access modifiers changed from: private */
    public volatile String appID;
    /* access modifiers changed from: private */
    public volatile CacheManager cacheManager;
    /* access modifiers changed from: private */
    public volatile Consent consent;
    /* access modifiers changed from: private */
    public volatile String consentVersion;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public volatile AssetDownloader downloader;
    volatile JobRunner jobRunner;
    /* access modifiers changed from: private */
    public Map<String, Boolean> playOperations = new ConcurrentHashMap();
    volatile Repository repository;
    /* access modifiers changed from: private */
    public volatile RuntimeValues runtimeValues = new RuntimeValues();
    /* access modifiers changed from: private */
    public volatile boolean shouldTransmitIMEI;
    /* access modifiers changed from: private */
    public volatile String userIMEI;
    volatile VungleApiClient vungleApiClient;

    public enum Consent {
        OPTED_IN,
        OPTED_OUT
    }

    private Vungle() {
    }

    @Deprecated
    public static void init(Collection<String> collection, String str, Context context2, InitCallback initCallback) throws IllegalArgumentException {
        init(str, context2, initCallback, new VungleSettings.Builder().build());
    }

    public static void init(String str, Context context2, InitCallback initCallback) throws IllegalArgumentException {
        init(str, context2, initCallback, new VungleSettings.Builder().build());
    }

    public static void init(final String str, final Context context2, InitCallback initCallback, VungleSettings vungleSettings) throws IllegalArgumentException {
        if (initCallback != null) {
            _instance.runtimeValues.settings = vungleSettings;
            RuntimeValues runtimeValues2 = _instance.runtimeValues;
            if (!(initCallback instanceof InitCallbackWrapper)) {
                initCallback = new InitCallbackWrapper(sdkExecutors.getUIExecutor(), initCallback);
            }
            runtimeValues2.initCallback = initCallback;
            if (context2 == null || str == null || str.isEmpty()) {
                _instance.runtimeValues.initCallback.onError(new VungleException(6));
            } else if (!(context2 instanceof Application)) {
                _instance.runtimeValues.initCallback.onError(new VungleException(7));
            } else if (isInitialized()) {
                Log.d(TAG, "init already complete");
                _instance.runtimeValues.initCallback.onSuccess();
            } else if (isInitializing.getAndSet(true)) {
                Log.d(TAG, "init ongoing");
                _instance.runtimeValues.initCallback.onError(new VungleException(8));
            } else {
                sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        if (!Vungle.isDepInit.getAndSet(true)) {
                            if (Vungle._instance.downloader == null) {
                                AssetDownloader unused = Vungle._instance.downloader = new AssetDownloader(4, NetworkProvider.getInstance(context2), Vungle.sdkExecutors.getUIExecutor());
                            }
                            if (Vungle._instance.cacheManager == null) {
                                CacheManager unused2 = Vungle._instance.cacheManager = new CacheManager(context2);
                            }
                            if (Vungle._instance.runtimeValues.settings == null || Vungle._instance.cacheManager.getBytesAvailable() >= Vungle._instance.runtimeValues.settings.getMinimumSpaceForInit()) {
                                Vungle._instance.cacheManager.addListener(Vungle.cacheListener);
                                GraphicDesigner graphicDesigner = new GraphicDesigner(Vungle._instance.cacheManager);
                                if (Vungle._instance.repository == null) {
                                    Vungle._instance.repository = new Repository(context2, graphicDesigner, Vungle.sdkExecutors.getIOExecutor(), Vungle.sdkExecutors.getUIExecutor());
                                }
                                Context unused3 = Vungle._instance.context = context2;
                                String unused4 = Vungle._instance.appID = str;
                                try {
                                    Vungle._instance.repository.init();
                                    if (Vungle._instance.vungleApiClient == null) {
                                        Vungle._instance.vungleApiClient = new VungleApiClient(context2, str, Vungle._instance.cacheManager, Vungle._instance.repository);
                                    }
                                    if (!TextUtils.isEmpty(Vungle._instance.userIMEI)) {
                                        Vungle._instance.vungleApiClient.updateIMEI(Vungle._instance.userIMEI, Vungle._instance.shouldTransmitIMEI);
                                    }
                                    if (Vungle._instance.runtimeValues.settings != null) {
                                        Vungle._instance.vungleApiClient.setDefaultIdFallbackDisabled(Vungle._instance.runtimeValues.settings.getAndroidIdOptOut());
                                    }
                                    if (Vungle._instance.adLoader == null) {
                                        AdLoader unused5 = Vungle._instance.adLoader = new AdLoader(Vungle.sdkExecutors, Vungle._instance.repository, Vungle._instance.vungleApiClient, Vungle._instance.cacheManager, Vungle._instance.downloader, Vungle._instance.runtimeValues, Vungle.vungleApi);
                                    }
                                    if (Vungle._instance.jobRunner == null) {
                                        VungleJobCreator vungleJobCreator = new VungleJobCreator(Vungle._instance.repository, graphicDesigner, Vungle._instance.vungleApiClient, new VungleAnalytics(Vungle._instance.vungleApiClient), Vungle.reconfigCall, Vungle._instance.adLoader, Vungle.vungleApi);
                                        Vungle._instance.jobRunner = new VungleJobRunner(vungleJobCreator, Vungle.sdkExecutors.getJobExecutor(), new JobRunnerThreadPriorityHelper());
                                    }
                                    Vungle._instance.adLoader.setJobRunner(Vungle._instance.jobRunner);
                                    if (Vungle._instance.consent == null || TextUtils.isEmpty(Vungle._instance.consentVersion)) {
                                        Cookie cookie = (Cookie) Vungle._instance.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get();
                                        Consent unused6 = Vungle._instance.consent = Vungle.getConsent(cookie);
                                        String unused7 = Vungle._instance.consentVersion = Vungle.getConsentMessageVersion(cookie);
                                    } else {
                                        Vungle.updateConsentStatus(Vungle._instance.consent, Vungle._instance.consentVersion);
                                    }
                                    Cookie cookie2 = (Cookie) Vungle._instance.repository.load("appId", Cookie.class).get();
                                    if (cookie2 == null) {
                                        cookie2 = new Cookie("appId");
                                    }
                                    cookie2.putValue("appId", str);
                                    try {
                                        Vungle._instance.repository.save(cookie2);
                                    } catch (DatabaseHelper.DBException unused8) {
                                        Vungle.onError(Vungle._instance.runtimeValues.initCallback, new VungleException(16));
                                        Vungle.deInit();
                                        return;
                                    }
                                } catch (DatabaseHelper.DBException unused9) {
                                    Vungle.onError(Vungle._instance.runtimeValues.initCallback, new VungleException(26));
                                    Vungle.deInit();
                                    return;
                                }
                            } else {
                                Vungle.onError(Vungle._instance.runtimeValues.initCallback, new VungleException(16));
                                Vungle.deInit();
                                return;
                            }
                        }
                        Vungle._instance.configure(Vungle._instance.runtimeValues.initCallback);
                    }
                });
            }
        } else {
            throw new IllegalArgumentException("A valid InitCallback required to ensure API calls are being made after initialize is successful");
        }
    }

    /* access modifiers changed from: private */
    public static void onError(InitCallback initCallback, VungleException vungleException) {
        if (initCallback != null) {
            initCallback.onError(vungleException);
        }
    }

    static void reConfigure() {
        if (isInitialized()) {
            sdkExecutors.getVungleExecutor().execute(new Runnable() {
                public void run() {
                    Vungle._instance.configure(Vungle._instance.runtimeValues.initCallback);
                }
            });
            return;
        }
        String str = _instance.appID;
        Vungle vungle = _instance;
        init(str, vungle.context, vungle.runtimeValues.initCallback);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0283 A[Catch:{ all -> 0x0328 }] */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02a2 A[Catch:{ all -> 0x0326 }] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x02ad A[Catch:{ all -> 0x0328 }] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x02ef A[Catch:{ all -> 0x0328 }] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x033e  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0348  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0242 A[Catch:{ all -> 0x0328 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void configure(com.vungle.warren.InitCallback r21) {
        /*
            r20 = this;
            r1 = r20
            r2 = r21
            java.lang.String r0 = "gdpr"
            java.lang.String r3 = "button_deny"
            java.lang.String r4 = "button_accept"
            java.lang.String r5 = "consent_message_version"
            java.lang.String r6 = "consent_message"
            java.lang.String r7 = "consent_title"
            java.lang.String r8 = "is_country_data_protected"
            java.lang.String r9 = "apk_direct_download"
            r10 = 2
            r12 = 0
            com.vungle.warren.VungleApiClient r13 = r1.vungleApiClient     // Catch:{ all -> 0x0328 }
            retrofit2.Response r13 = r13.config()     // Catch:{ all -> 0x0328 }
            if (r13 != 0) goto L_0x002c
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x0328 }
            r0.<init>(r10)     // Catch:{ all -> 0x0328 }
            r2.onError(r0)     // Catch:{ all -> 0x0328 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.vungle.warren.Vungle.isInitializing     // Catch:{ all -> 0x0328 }
            r0.set(r12)     // Catch:{ all -> 0x0328 }
            return
        L_0x002c:
            boolean r14 = r13.isSuccessful()     // Catch:{ all -> 0x0328 }
            r10 = 0
            if (r14 != 0) goto L_0x006e
            com.vungle.warren.VungleApiClient r0 = r1.vungleApiClient     // Catch:{ all -> 0x0328 }
            long r3 = r0.getRetryAfterHeaderValue(r13)     // Catch:{ all -> 0x0328 }
            int r0 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x005f
            com.vungle.warren.tasks.JobRunner r0 = r1.jobRunner     // Catch:{ all -> 0x0328 }
            com.vungle.warren.Vungle r5 = com.vungle.warren.Vungle._instance     // Catch:{ all -> 0x0328 }
            java.lang.String r5 = r5.appID     // Catch:{ all -> 0x0328 }
            com.vungle.warren.tasks.JobInfo r5 = com.vungle.warren.tasks.ReconfigJob.makeJobInfo(r5)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.tasks.JobInfo r3 = r5.setDelay(r3)     // Catch:{ all -> 0x0328 }
            r0.execute(r3)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x0328 }
            r3 = 14
            r0.<init>(r3)     // Catch:{ all -> 0x0328 }
            r2.onError(r0)     // Catch:{ all -> 0x0328 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.vungle.warren.Vungle.isInitializing     // Catch:{ all -> 0x0328 }
            r0.set(r12)     // Catch:{ all -> 0x0328 }
            return
        L_0x005f:
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x0328 }
            r3 = 3
            r0.<init>(r3)     // Catch:{ all -> 0x0328 }
            r2.onError(r0)     // Catch:{ all -> 0x0328 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.vungle.warren.Vungle.isInitializing     // Catch:{ all -> 0x0328 }
            r0.set(r12)     // Catch:{ all -> 0x0328 }
            return
        L_0x006e:
            android.content.Context r14 = r1.context     // Catch:{ all -> 0x0328 }
            java.lang.String r15 = "com.vungle.sdk"
            android.content.SharedPreferences r14 = r14.getSharedPreferences(r15, r12)     // Catch:{ all -> 0x0328 }
            java.lang.String r15 = "reported"
            boolean r14 = r14.getBoolean(r15, r12)     // Catch:{ all -> 0x0328 }
            if (r14 != 0) goto L_0x008c
            com.vungle.warren.VungleApiClient r14 = r1.vungleApiClient     // Catch:{ all -> 0x0328 }
            retrofit2.Call r14 = r14.reportNew()     // Catch:{ all -> 0x0328 }
            com.vungle.warren.Vungle$4 r15 = new com.vungle.warren.Vungle$4     // Catch:{ all -> 0x0328 }
            r15.<init>()     // Catch:{ all -> 0x0328 }
            r14.enqueue(r15)     // Catch:{ all -> 0x0328 }
        L_0x008c:
            java.lang.Object r13 = r13.body()     // Catch:{ all -> 0x0328 }
            com.google.gson.JsonObject r13 = (com.google.gson.JsonObject) r13     // Catch:{ all -> 0x0328 }
            java.lang.String r14 = "placements"
            com.google.gson.JsonArray r14 = r13.getAsJsonArray(r14)     // Catch:{ all -> 0x0328 }
            int r15 = r14.size()     // Catch:{ all -> 0x0328 }
            if (r15 != 0) goto L_0x00ac
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException     // Catch:{ all -> 0x0328 }
            r0.<init>(r12)     // Catch:{ all -> 0x0328 }
            r2.onError(r0)     // Catch:{ all -> 0x0328 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.vungle.warren.Vungle.isInitializing     // Catch:{ all -> 0x0328 }
            r0.set(r12)     // Catch:{ all -> 0x0328 }
            return
        L_0x00ac:
            java.util.Map<java.lang.String, java.lang.Boolean> r15 = r1.playOperations     // Catch:{ all -> 0x0328 }
            r15.clear()     // Catch:{ all -> 0x0328 }
            com.vungle.warren.Vungle r15 = com.vungle.warren.Vungle._instance     // Catch:{ all -> 0x0328 }
            com.vungle.warren.AdLoader r15 = r15.adLoader     // Catch:{ all -> 0x0328 }
            r15.clear()     // Catch:{ all -> 0x0328 }
            java.util.ArrayList r15 = new java.util.ArrayList     // Catch:{ all -> 0x0328 }
            r15.<init>()     // Catch:{ all -> 0x0328 }
            java.util.Iterator r14 = r14.iterator()     // Catch:{ all -> 0x0328 }
        L_0x00c1:
            boolean r16 = r14.hasNext()     // Catch:{ all -> 0x0328 }
            if (r16 == 0) goto L_0x00dd
            java.lang.Object r16 = r14.next()     // Catch:{ all -> 0x0328 }
            com.google.gson.JsonElement r16 = (com.google.gson.JsonElement) r16     // Catch:{ all -> 0x0328 }
            com.vungle.warren.model.Placement r12 = new com.vungle.warren.model.Placement     // Catch:{ all -> 0x0328 }
            com.google.gson.JsonObject r10 = r16.getAsJsonObject()     // Catch:{ all -> 0x0328 }
            r12.<init>(r10)     // Catch:{ all -> 0x0328 }
            r15.add(r12)     // Catch:{ all -> 0x0328 }
            r10 = 0
            r12 = 0
            goto L_0x00c1
        L_0x00dd:
            com.vungle.warren.Vungle r10 = com.vungle.warren.Vungle._instance     // Catch:{ all -> 0x0328 }
            com.vungle.warren.persistence.Repository r10 = r10.repository     // Catch:{ all -> 0x0328 }
            r10.setValidPlacements(r15)     // Catch:{ all -> 0x0328 }
            boolean r10 = r13.has(r0)     // Catch:{ all -> 0x0328 }
            if (r10 == 0) goto L_0x01df
            com.vungle.warren.Vungle r10 = com.vungle.warren.Vungle._instance     // Catch:{ all -> 0x0328 }
            com.vungle.warren.persistence.Repository r10 = r10.repository     // Catch:{ all -> 0x0328 }
            java.lang.String r11 = "consentIsImportantToVungle"
            java.lang.Class<com.vungle.warren.model.Cookie> r14 = com.vungle.warren.model.Cookie.class
            com.vungle.warren.persistence.Repository$FutureResult r10 = r10.load(r11, r14)     // Catch:{ all -> 0x0328 }
            java.lang.Object r10 = r10.get()     // Catch:{ all -> 0x0328 }
            com.vungle.warren.model.Cookie r10 = (com.vungle.warren.model.Cookie) r10     // Catch:{ all -> 0x0328 }
            if (r10 != 0) goto L_0x011e
            com.vungle.warren.model.Cookie r10 = new com.vungle.warren.model.Cookie     // Catch:{ all -> 0x0328 }
            java.lang.String r11 = "consentIsImportantToVungle"
            r10.<init>(r11)     // Catch:{ all -> 0x0328 }
            java.lang.String r11 = "consent_status"
            java.lang.String r14 = "unknown"
            r10.putValue(r11, r14)     // Catch:{ all -> 0x0328 }
            java.lang.String r11 = "consent_source"
            java.lang.String r14 = "no_interaction"
            r10.putValue(r11, r14)     // Catch:{ all -> 0x0328 }
            java.lang.String r11 = "timestamp"
            r14 = 0
            java.lang.Long r12 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x0328 }
            r10.putValue(r11, r12)     // Catch:{ all -> 0x0328 }
        L_0x011e:
            com.google.gson.JsonObject r0 = r13.getAsJsonObject(r0)     // Catch:{ all -> 0x0328 }
            boolean r11 = com.vungle.warren.model.JsonUtil.hasNonNull(r0, r8)     // Catch:{ all -> 0x0328 }
            if (r11 == 0) goto L_0x0134
            com.google.gson.JsonElement r11 = r0.get(r8)     // Catch:{ all -> 0x0328 }
            boolean r11 = r11.getAsBoolean()     // Catch:{ all -> 0x0328 }
            if (r11 == 0) goto L_0x0134
            r11 = 1
            goto L_0x0135
        L_0x0134:
            r11 = 0
        L_0x0135:
            boolean r12 = com.vungle.warren.model.JsonUtil.hasNonNull(r0, r7)     // Catch:{ all -> 0x0328 }
            java.lang.String r14 = ""
            if (r12 == 0) goto L_0x0146
            com.google.gson.JsonElement r12 = r0.get(r7)     // Catch:{ all -> 0x0328 }
            java.lang.String r12 = r12.getAsString()     // Catch:{ all -> 0x0328 }
            goto L_0x0147
        L_0x0146:
            r12 = r14
        L_0x0147:
            boolean r15 = com.vungle.warren.model.JsonUtil.hasNonNull(r0, r6)     // Catch:{ all -> 0x0328 }
            if (r15 == 0) goto L_0x0156
            com.google.gson.JsonElement r15 = r0.get(r6)     // Catch:{ all -> 0x0328 }
            java.lang.String r15 = r15.getAsString()     // Catch:{ all -> 0x0328 }
            goto L_0x0157
        L_0x0156:
            r15 = r14
        L_0x0157:
            boolean r17 = com.vungle.warren.model.JsonUtil.hasNonNull(r0, r5)     // Catch:{ all -> 0x0328 }
            if (r17 == 0) goto L_0x0166
            com.google.gson.JsonElement r17 = r0.get(r5)     // Catch:{ all -> 0x0328 }
            java.lang.String r17 = r17.getAsString()     // Catch:{ all -> 0x0328 }
            goto L_0x0168
        L_0x0166:
            r17 = r14
        L_0x0168:
            boolean r18 = com.vungle.warren.model.JsonUtil.hasNonNull(r0, r4)     // Catch:{ all -> 0x0328 }
            if (r18 == 0) goto L_0x0177
            com.google.gson.JsonElement r18 = r0.get(r4)     // Catch:{ all -> 0x0328 }
            java.lang.String r18 = r18.getAsString()     // Catch:{ all -> 0x0328 }
            goto L_0x0179
        L_0x0177:
            r18 = r14
        L_0x0179:
            boolean r19 = com.vungle.warren.model.JsonUtil.hasNonNull(r0, r3)     // Catch:{ all -> 0x0328 }
            if (r19 == 0) goto L_0x0188
            com.google.gson.JsonElement r0 = r0.get(r3)     // Catch:{ all -> 0x0328 }
            java.lang.String r0 = r0.getAsString()     // Catch:{ all -> 0x0328 }
            goto L_0x0189
        L_0x0188:
            r0 = r14
        L_0x0189:
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ all -> 0x0328 }
            r10.putValue(r8, r11)     // Catch:{ all -> 0x0328 }
            boolean r8 = android.text.TextUtils.isEmpty(r12)     // Catch:{ all -> 0x0328 }
            if (r8 == 0) goto L_0x0198
            java.lang.String r12 = "Targeted Ads"
        L_0x0198:
            r10.putValue(r7, r12)     // Catch:{ all -> 0x0328 }
            boolean r7 = android.text.TextUtils.isEmpty(r15)     // Catch:{ all -> 0x0328 }
            if (r7 == 0) goto L_0x01a3
            java.lang.String r15 = "To receive more relevant ad content based on your interactions with our ads, click \"I Consent\" below. Either way, you will see the same amount of ads."
        L_0x01a3:
            r10.putValue(r6, r15)     // Catch:{ all -> 0x0328 }
            java.lang.String r6 = "consent_source"
            java.lang.String r6 = r10.getString(r6)     // Catch:{ all -> 0x0328 }
            java.lang.String r7 = "publisher"
            boolean r6 = r7.equalsIgnoreCase(r6)     // Catch:{ all -> 0x0328 }
            if (r6 != 0) goto L_0x01c0
            boolean r6 = android.text.TextUtils.isEmpty(r17)     // Catch:{ all -> 0x0328 }
            if (r6 == 0) goto L_0x01bb
            goto L_0x01bd
        L_0x01bb:
            r14 = r17
        L_0x01bd:
            r10.putValue(r5, r14)     // Catch:{ all -> 0x0328 }
        L_0x01c0:
            boolean r5 = android.text.TextUtils.isEmpty(r18)     // Catch:{ all -> 0x0328 }
            if (r5 == 0) goto L_0x01c8
            java.lang.String r18 = "I Consent"
        L_0x01c8:
            r5 = r18
            r10.putValue(r4, r5)     // Catch:{ all -> 0x0328 }
            boolean r4 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x0328 }
            if (r4 == 0) goto L_0x01d5
            java.lang.String r0 = "I Do Not Consent"
        L_0x01d5:
            r10.putValue(r3, r0)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.Vungle r0 = com.vungle.warren.Vungle._instance     // Catch:{ all -> 0x0328 }
            com.vungle.warren.persistence.Repository r0 = r0.repository     // Catch:{ all -> 0x0328 }
            r0.save(r10)     // Catch:{ all -> 0x0328 }
        L_0x01df:
            boolean r0 = r13.has(r9)     // Catch:{ all -> 0x0328 }
            java.lang.String r3 = "enabled"
            if (r0 == 0) goto L_0x021f
            com.google.gson.JsonObject r0 = r13.getAsJsonObject(r9)     // Catch:{ all -> 0x0328 }
            boolean r0 = r0.has(r3)     // Catch:{ all -> 0x0328 }
            if (r0 == 0) goto L_0x021f
            com.google.gson.JsonObject r0 = r13.getAsJsonObject(r9)     // Catch:{ all -> 0x0328 }
            com.google.gson.JsonElement r0 = r0.get(r3)     // Catch:{ all -> 0x0328 }
            boolean r0 = r0.getAsBoolean()     // Catch:{ all -> 0x0328 }
            if (r0 == 0) goto L_0x0216
            android.content.Context r4 = r1.context     // Catch:{ all -> 0x0328 }
            com.vungle.warren.downloader.AssetDownloader r5 = new com.vungle.warren.downloader.AssetDownloader     // Catch:{ all -> 0x0328 }
            r6 = 4
            android.content.Context r7 = r1.context     // Catch:{ all -> 0x0328 }
            com.vungle.warren.utility.NetworkProvider r7 = com.vungle.warren.utility.NetworkProvider.getInstance(r7)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.utility.SDKExecutors r8 = com.vungle.warren.Vungle.sdkExecutors     // Catch:{ all -> 0x0328 }
            java.util.concurrent.ExecutorService r8 = r8.getUIExecutor()     // Catch:{ all -> 0x0328 }
            r5.<init>(r6, r7, r8)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.download.APKDirectDownloadManager.init(r4, r5)     // Catch:{ all -> 0x0328 }
        L_0x0216:
            if (r0 == 0) goto L_0x021a
            r0 = 1
            goto L_0x021b
        L_0x021a:
            r0 = 0
        L_0x021b:
            com.vungle.warren.download.APKDirectDownloadManager.setDirectDownloadStatus(r0)     // Catch:{ all -> 0x0328 }
            goto L_0x023a
        L_0x021f:
            android.content.Context r0 = r1.context     // Catch:{ all -> 0x0328 }
            com.vungle.warren.downloader.AssetDownloader r4 = new com.vungle.warren.downloader.AssetDownloader     // Catch:{ all -> 0x0328 }
            r5 = 4
            android.content.Context r6 = r1.context     // Catch:{ all -> 0x0328 }
            com.vungle.warren.utility.NetworkProvider r6 = com.vungle.warren.utility.NetworkProvider.getInstance(r6)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.utility.SDKExecutors r7 = com.vungle.warren.Vungle.sdkExecutors     // Catch:{ all -> 0x0328 }
            java.util.concurrent.ExecutorService r7 = r7.getUIExecutor()     // Catch:{ all -> 0x0328 }
            r4.<init>(r5, r6, r7)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.download.APKDirectDownloadManager.init(r0, r4)     // Catch:{ all -> 0x0328 }
            r0 = -1
            com.vungle.warren.download.APKDirectDownloadManager.setDirectDownloadStatus(r0)     // Catch:{ all -> 0x0328 }
        L_0x023a:
            java.lang.String r0 = "ri"
            boolean r0 = r13.has(r0)     // Catch:{ all -> 0x0328 }
            if (r0 == 0) goto L_0x027b
            com.vungle.warren.Vungle r0 = com.vungle.warren.Vungle._instance     // Catch:{ all -> 0x0328 }
            com.vungle.warren.persistence.Repository r0 = r0.repository     // Catch:{ all -> 0x0328 }
            java.lang.String r4 = "configSettings"
            java.lang.Class<com.vungle.warren.model.Cookie> r5 = com.vungle.warren.model.Cookie.class
            com.vungle.warren.persistence.Repository$FutureResult r0 = r0.load(r4, r5)     // Catch:{ all -> 0x0328 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0328 }
            com.vungle.warren.model.Cookie r0 = (com.vungle.warren.model.Cookie) r0     // Catch:{ all -> 0x0328 }
            if (r0 != 0) goto L_0x025d
            com.vungle.warren.model.Cookie r0 = new com.vungle.warren.model.Cookie     // Catch:{ all -> 0x0328 }
            java.lang.String r4 = "configSettings"
            r0.<init>(r4)     // Catch:{ all -> 0x0328 }
        L_0x025d:
            java.lang.String r4 = "ri"
            com.google.gson.JsonObject r4 = r13.getAsJsonObject(r4)     // Catch:{ all -> 0x0328 }
            com.google.gson.JsonElement r3 = r4.get(r3)     // Catch:{ all -> 0x0328 }
            boolean r3 = r3.getAsBoolean()     // Catch:{ all -> 0x0328 }
            java.lang.String r4 = "isReportIncentivizedEnabled"
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x0328 }
            r0.putValue(r4, r3)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.Vungle r3 = com.vungle.warren.Vungle._instance     // Catch:{ all -> 0x0328 }
            com.vungle.warren.persistence.Repository r3 = r3.repository     // Catch:{ all -> 0x0328 }
            r3.save(r0)     // Catch:{ all -> 0x0328 }
        L_0x027b:
            java.lang.String r0 = "attribution_reporting"
            boolean r0 = r13.has(r0)     // Catch:{ all -> 0x0328 }
            if (r0 == 0) goto L_0x02a2
            java.lang.String r0 = "attribution_reporting"
            com.google.gson.JsonObject r0 = r13.getAsJsonObject(r0)     // Catch:{ all -> 0x0328 }
            java.lang.String r3 = "should_transmit_imei"
            boolean r3 = r0.has(r3)     // Catch:{ all -> 0x0328 }
            if (r3 == 0) goto L_0x029e
            java.lang.String r3 = "should_transmit_imei"
            com.google.gson.JsonElement r0 = r0.get(r3)     // Catch:{ all -> 0x0328 }
            boolean r0 = r0.getAsBoolean()     // Catch:{ all -> 0x0328 }
            r1.shouldTransmitIMEI = r0     // Catch:{ all -> 0x0328 }
            goto L_0x02a5
        L_0x029e:
            r3 = 0
            r1.shouldTransmitIMEI = r3     // Catch:{ all -> 0x0326 }
            goto L_0x02a5
        L_0x02a2:
            r3 = 0
            r1.shouldTransmitIMEI = r3     // Catch:{ all -> 0x0326 }
        L_0x02a5:
            java.lang.String r0 = "config"
            boolean r0 = r13.has(r0)     // Catch:{ all -> 0x0328 }
            if (r0 == 0) goto L_0x02cc
            java.lang.String r0 = "config"
            com.google.gson.JsonObject r0 = r13.getAsJsonObject(r0)     // Catch:{ all -> 0x0328 }
            java.lang.String r3 = "refresh_time"
            com.google.gson.JsonElement r0 = r0.get(r3)     // Catch:{ all -> 0x0328 }
            long r3 = r0.getAsLong()     // Catch:{ all -> 0x0328 }
            com.vungle.warren.tasks.JobRunner r0 = r1.jobRunner     // Catch:{ all -> 0x0328 }
            java.lang.String r5 = r1.appID     // Catch:{ all -> 0x0328 }
            com.vungle.warren.tasks.JobInfo r5 = com.vungle.warren.tasks.ReconfigJob.makeJobInfo(r5)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.tasks.JobInfo r3 = r5.setDelay(r3)     // Catch:{ all -> 0x0328 }
            r0.execute(r3)     // Catch:{ all -> 0x0328 }
        L_0x02cc:
            r0 = 1
            com.vungle.warren.Vungle.isInitialized = r0     // Catch:{ all -> 0x0328 }
            r21.onSuccess()     // Catch:{ all -> 0x0328 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.vungle.warren.Vungle.isInitializing     // Catch:{ all -> 0x0328 }
            r3 = 0
            r0.set(r3)     // Catch:{ all -> 0x0326 }
            com.vungle.warren.persistence.Repository r0 = r1.repository     // Catch:{ all -> 0x0328 }
            com.vungle.warren.persistence.Repository$FutureResult r0 = r0.loadValidPlacements()     // Catch:{ all -> 0x0328 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0328 }
            java.util.Collection r0 = (java.util.Collection) r0     // Catch:{ all -> 0x0328 }
            com.vungle.warren.tasks.JobRunner r3 = r1.jobRunner     // Catch:{ all -> 0x0328 }
            com.vungle.warren.tasks.JobInfo r4 = com.vungle.warren.tasks.CleanupJob.makeJobInfo()     // Catch:{ all -> 0x0328 }
            r3.execute(r4)     // Catch:{ all -> 0x0328 }
            if (r0 == 0) goto L_0x031b
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0328 }
        L_0x02f3:
            boolean r3 = r0.hasNext()     // Catch:{ all -> 0x0328 }
            if (r3 == 0) goto L_0x031b
            java.lang.Object r3 = r0.next()     // Catch:{ all -> 0x0328 }
            com.vungle.warren.model.Placement r3 = (com.vungle.warren.model.Placement) r3     // Catch:{ all -> 0x0328 }
            boolean r4 = r3.isAutoCached()     // Catch:{ all -> 0x0328 }
            if (r4 == 0) goto L_0x0318
            java.lang.String r4 = com.vungle.warren.Vungle.TAG     // Catch:{ all -> 0x0328 }
            java.lang.String r5 = "starting jobs for autocached advs"
            android.util.Log.d(r4, r5)     // Catch:{ all -> 0x0328 }
            com.vungle.warren.AdLoader r4 = r1.adLoader     // Catch:{ all -> 0x0328 }
            java.lang.String r3 = r3.getId()     // Catch:{ all -> 0x0328 }
            r5 = 0
            r4.loadEndless(r3, r5)     // Catch:{ all -> 0x0328 }
            goto L_0x02f3
        L_0x0318:
            r5 = 0
            goto L_0x02f3
        L_0x031b:
            com.vungle.warren.tasks.JobRunner r0 = r1.jobRunner     // Catch:{ all -> 0x0328 }
            r3 = 1
            com.vungle.warren.tasks.JobInfo r3 = com.vungle.warren.tasks.SendReportsJob.makeJobInfo(r3)     // Catch:{ all -> 0x0328 }
            r0.execute(r3)     // Catch:{ all -> 0x0328 }
            goto L_0x0360
        L_0x0326:
            r0 = move-exception
            goto L_0x032a
        L_0x0328:
            r0 = move-exception
            r3 = 0
        L_0x032a:
            com.vungle.warren.Vungle.isInitialized = r3
            java.util.concurrent.atomic.AtomicBoolean r4 = com.vungle.warren.Vungle.isInitializing
            r4.set(r3)
            java.lang.String r3 = com.vungle.warren.Vungle.TAG
            java.lang.String r4 = android.util.Log.getStackTraceString(r0)
            android.util.Log.e(r3, r4)
            boolean r3 = r0 instanceof retrofit2.HttpException
            if (r3 == 0) goto L_0x0348
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException
            r3 = 3
            r0.<init>(r3)
            r2.onError(r0)
            goto L_0x0360
        L_0x0348:
            boolean r0 = r0 instanceof com.vungle.warren.persistence.DatabaseHelper.DBException
            if (r0 == 0) goto L_0x0357
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException
            r3 = 26
            r0.<init>(r3)
            r2.onError(r0)
            goto L_0x0360
        L_0x0357:
            com.vungle.warren.error.VungleException r0 = new com.vungle.warren.error.VungleException
            r3 = 2
            r0.<init>(r3)
            r2.onError(r0)
        L_0x0360:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.Vungle.configure(com.vungle.warren.InitCallback):void");
    }

    public static boolean isInitialized() {
        return (!isInitialized || _instance.repository == null || _instance.repository.getValidPlacements() == null || _instance.repository.getValidPlacements().get().size() <= 0 || _instance.context == null) ? false : true;
    }

    public static void setIncentivizedFields(String str, String str2, String str3, String str4, String str5) {
        final String str6 = str2;
        final String str7 = str3;
        final String str8 = str4;
        final String str9 = str5;
        final String str10 = str;
        sdkExecutors.getVungleExecutor().execute(new Runnable() {
            public void run() {
                if (!Vungle.isInitialized()) {
                    Log.e(Vungle.TAG, "Vungle is not initialized");
                    return;
                }
                Cookie cookie = (Cookie) Vungle._instance.repository.load(Cookie.INCENTIVIZED_TEXT_COOKIE, Cookie.class).get();
                if (cookie == null) {
                    cookie = new Cookie(Cookie.INCENTIVIZED_TEXT_COOKIE);
                }
                boolean z = false;
                if (!TextUtils.isEmpty(str6)) {
                    cookie.putValue("title", str6);
                    z = true;
                }
                if (!TextUtils.isEmpty(str7)) {
                    cookie.putValue("body", str7);
                    z = true;
                }
                if (!TextUtils.isEmpty(str8)) {
                    cookie.putValue("continue", str8);
                    z = true;
                }
                if (!TextUtils.isEmpty(str9)) {
                    cookie.putValue(JavascriptBridge.MraidHandler.CLOSE_ACTION, str9);
                    z = true;
                }
                if (!TextUtils.isEmpty(str10)) {
                    cookie.putValue("userID", str10);
                    z = true;
                }
                if (z) {
                    try {
                        Vungle._instance.repository.save(cookie);
                    } catch (DatabaseHelper.DBException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public static boolean canPlayAd(String str) {
        if (isInitialized()) {
            return canPlayAd(_instance.repository.findValidAdvertisementForPlacement(str).get());
        }
        Log.e(TAG, "Vungle is not initialized");
        return false;
    }

    static boolean canPlayAd(Advertisement advertisement) {
        return _instance.adLoader.canPlayAd(advertisement);
    }

    public static void playAd(final String str, final AdConfig adConfig, PlayAdCallback playAdCallback) {
        if (isInitialized()) {
            final PlayAdCallbackWrapper playAdCallbackWrapper = new PlayAdCallbackWrapper(sdkExecutors.getUIExecutor(), playAdCallback);
            sdkExecutors.getVungleExecutor().execute(new Runnable() {
                public void run() {
                    VungleException vungleException;
                    Placement placement = (Placement) Vungle._instance.repository.load(str, Placement.class).get();
                    if (Boolean.TRUE.equals(Vungle._instance.playOperations.get(str)) || Vungle._instance.adLoader.isLoading(str)) {
                        vungleException = new VungleException(8);
                    } else {
                        vungleException = null;
                    }
                    if (placement == null) {
                        vungleException = new VungleException(13);
                    }
                    if (vungleException != null) {
                        playAdCallbackWrapper.onError(str, vungleException);
                        return;
                    }
                    final boolean z = false;
                    final Advertisement advertisement = Vungle._instance.repository.findValidAdvertisementForPlacement(str).get();
                    try {
                        if (!Vungle.canPlayAd(advertisement)) {
                            if (advertisement != null && advertisement.getState() == 1) {
                                Vungle._instance.repository.saveAndApplyState(advertisement, str, 4);
                                if (placement.isAutoCached()) {
                                    Vungle._instance.adLoader.loadEndless(placement.getId(), 0);
                                }
                            }
                            playAdCallbackWrapper.onError(str, new VungleException(10));
                            z = true;
                        } else {
                            advertisement.configure(adConfig);
                            Vungle._instance.repository.save(advertisement);
                        }
                        if (Vungle._instance.context != null) {
                            Vungle._instance.vungleApiClient.willPlayAd(placement.getId(), placement.isAutoCached(), z ? "" : advertisement.getAdToken()).enqueue(new Callback<JsonObject>() {
                                public void onResponse(Call<JsonObject> call, final Response<JsonObject> response) {
                                    Vungle.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                                        /* JADX WARNING: Removed duplicated region for block: B:21:0x0054  */
                                        /* JADX WARNING: Removed duplicated region for block: B:22:0x0058  */
                                        /* JADX WARNING: Removed duplicated region for block: B:28:0x0071  */
                                        /* JADX WARNING: Removed duplicated region for block: B:31:0x009f  */
                                        /* Code decompiled incorrectly, please refer to instructions dump. */
                                        public void run() {
                                            /*
                                                r6 = this;
                                                java.lang.String r0 = "Error using will_play_ad!"
                                                java.lang.String r1 = "Vungle"
                                                retrofit2.Response r2 = r3
                                                boolean r2 = r2.isSuccessful()
                                                r3 = 0
                                                if (r2 == 0) goto L_0x006b
                                                retrofit2.Response r2 = r3
                                                java.lang.Object r2 = r2.body()
                                                com.google.gson.JsonObject r2 = (com.google.gson.JsonObject) r2
                                                if (r2 == 0) goto L_0x006b
                                                java.lang.String r4 = "ad"
                                                boolean r5 = r2.has(r4)
                                                if (r5 == 0) goto L_0x006b
                                                com.google.gson.JsonObject r2 = r2.getAsJsonObject(r4)     // Catch:{ IllegalArgumentException -> 0x0066, Exception -> 0x004b, VungleError -> 0x0049 }
                                                com.vungle.warren.model.Advertisement r4 = new com.vungle.warren.model.Advertisement     // Catch:{ IllegalArgumentException -> 0x0066, Exception -> 0x004b, VungleError -> 0x0049 }
                                                r4.<init>(r2)     // Catch:{ IllegalArgumentException -> 0x0066, Exception -> 0x004b, VungleError -> 0x0049 }
                                                com.vungle.warren.Vungle$6$1 r2 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                com.vungle.warren.Vungle$6 r2 = com.vungle.warren.Vungle.AnonymousClass6.this     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                com.vungle.warren.AdConfig r2 = r3     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                r4.configure(r2)     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                com.vungle.warren.Vungle r2 = com.vungle.warren.Vungle._instance     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                com.vungle.warren.persistence.Repository r2 = r2.repository     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                com.vungle.warren.Vungle$6$1 r3 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                com.vungle.warren.Vungle$6 r3 = com.vungle.warren.Vungle.AnonymousClass6.this     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                java.lang.String r3 = r2     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                r5 = 0
                                                r2.saveAndApplyState(r4, r3, r5)     // Catch:{ IllegalArgumentException -> 0x0047, Exception -> 0x0044, VungleError -> 0x0041 }
                                                r3 = r4
                                                goto L_0x006b
                                            L_0x0041:
                                                r2 = move-exception
                                                r3 = r4
                                                goto L_0x004d
                                            L_0x0044:
                                                r2 = move-exception
                                                r3 = r4
                                                goto L_0x0062
                                            L_0x0047:
                                                r3 = r4
                                                goto L_0x0066
                                            L_0x0049:
                                                r2 = move-exception
                                                goto L_0x004d
                                            L_0x004b:
                                                r2 = move-exception
                                                goto L_0x0062
                                            L_0x004d:
                                                int r4 = r2.getErrorCode()
                                                r5 = 6
                                                if (r4 == r5) goto L_0x0058
                                                android.util.Log.e(r1, r0, r2)
                                                goto L_0x006b
                                            L_0x0058:
                                                java.lang.String r0 = com.vungle.warren.Vungle.TAG
                                                java.lang.String r1 = "will_play_ad was disabled by the configuration settings. This is expected."
                                                android.util.Log.e(r0, r1)
                                                goto L_0x006b
                                            L_0x0062:
                                                android.util.Log.e(r1, r0, r2)
                                                goto L_0x006b
                                            L_0x0066:
                                                java.lang.String r0 = "Will Play Ad did not respond with a replacement. Move on."
                                                android.util.Log.v(r1, r0)
                                            L_0x006b:
                                                com.vungle.warren.Vungle$6$1 r0 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                boolean r0 = r1
                                                if (r0 == 0) goto L_0x009f
                                                if (r3 != 0) goto L_0x0089
                                                com.vungle.warren.Vungle$6$1 r0 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                com.vungle.warren.Vungle$6 r0 = com.vungle.warren.Vungle.AnonymousClass6.this
                                                com.vungle.warren.PlayAdCallback r0 = r0
                                                com.vungle.warren.Vungle$6$1 r1 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                com.vungle.warren.Vungle$6 r1 = com.vungle.warren.Vungle.AnonymousClass6.this
                                                java.lang.String r1 = r2
                                                com.vungle.warren.error.VungleException r2 = new com.vungle.warren.error.VungleException
                                                r3 = 1
                                                r2.<init>(r3)
                                                r0.onError(r1, r2)
                                                goto L_0x00b8
                                            L_0x0089:
                                                com.vungle.warren.Vungle$6$1 r0 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                com.vungle.warren.Vungle$6 r0 = com.vungle.warren.Vungle.AnonymousClass6.this
                                                java.lang.String r0 = r2
                                                com.vungle.warren.Vungle$6$1 r1 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                com.vungle.warren.Vungle$6 r1 = com.vungle.warren.Vungle.AnonymousClass6.this
                                                com.vungle.warren.PlayAdCallback r1 = r0
                                                com.vungle.warren.Vungle$6$1 r2 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                com.vungle.warren.Vungle$6 r2 = com.vungle.warren.Vungle.AnonymousClass6.this
                                                java.lang.String r2 = r2
                                                com.vungle.warren.Vungle.renderAd(r0, r1, r2, r3)
                                                goto L_0x00b8
                                            L_0x009f:
                                                com.vungle.warren.Vungle$6$1 r0 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                com.vungle.warren.Vungle$6 r0 = com.vungle.warren.Vungle.AnonymousClass6.this
                                                java.lang.String r0 = r2
                                                com.vungle.warren.Vungle$6$1 r1 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                com.vungle.warren.Vungle$6 r1 = com.vungle.warren.Vungle.AnonymousClass6.this
                                                com.vungle.warren.PlayAdCallback r1 = r0
                                                com.vungle.warren.Vungle$6$1 r2 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                com.vungle.warren.Vungle$6 r2 = com.vungle.warren.Vungle.AnonymousClass6.this
                                                java.lang.String r2 = r2
                                                com.vungle.warren.Vungle$6$1 r3 = com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.this
                                                com.vungle.warren.model.Advertisement r3 = r2
                                                com.vungle.warren.Vungle.renderAd(r0, r1, r2, r3)
                                            L_0x00b8:
                                                return
                                            */
                                            throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.Vungle.AnonymousClass6.AnonymousClass1.AnonymousClass1.run():void");
                                        }
                                    });
                                }

                                public void onFailure(Call<JsonObject> call, Throwable th) {
                                    Vungle.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                                        public void run() {
                                            if (z) {
                                                playAdCallbackWrapper.onError(str, new VungleException(1));
                                            } else {
                                                Vungle.renderAd(str, playAdCallbackWrapper, str, advertisement);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    } catch (DatabaseHelper.DBException unused) {
                        playAdCallbackWrapper.onError(str, new VungleException(26));
                    }
                }
            });
        } else if (playAdCallback != null) {
            playAdCallback.onError(str, new VungleException(9));
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void renderAd(String str, final PlayAdCallback playAdCallback, final String str2, final Advertisement advertisement) {
        synchronized (Vungle.class) {
            boolean z = true;
            _instance.playOperations.put(str, true);
            VungleActivity.setEventListener(new AdContract.AdvertisementPresenter.EventListener() {
                int percentViewed = -1;
                boolean succesfulView = false;

                public void onNext(String str, String str2, String str3) {
                    boolean z;
                    try {
                        boolean z2 = false;
                        if (str.equals("start")) {
                            Vungle._instance.repository.saveAndApplyState(advertisement, str3, 2);
                            if (playAdCallback != null) {
                                playAdCallback.onAdStart(str3);
                            }
                            this.percentViewed = 0;
                            Placement placement = (Placement) Vungle._instance.repository.load(str2, Placement.class).get();
                            if (placement != null && placement.isAutoCached()) {
                                Vungle._instance.adLoader.loadEndless(str2, 0);
                            }
                        } else if (str.equals("end")) {
                            Log.d("Vungle", "Cleaning up metadata and assets for placement " + str3 + " and advertisement " + advertisement.getId());
                            Vungle._instance.repository.saveAndApplyState(advertisement, str3, 3);
                            Vungle._instance.repository.updateAndSaveReportState(str3, advertisement.getAppID(), 0, 1);
                            VungleActivity.setEventListener(null);
                            Vungle._instance.playOperations.put(str3, false);
                            Vungle._instance.jobRunner.execute(SendReportsJob.makeJobInfo(false));
                            if (playAdCallback != null) {
                                PlayAdCallback playAdCallback = playAdCallback;
                                if (!this.succesfulView) {
                                    if (this.percentViewed < 80) {
                                        z = false;
                                        if (str2 != null && str2.equals("isCTAClicked")) {
                                            z2 = true;
                                        }
                                        playAdCallback.onAdEnd(str3, z, z2);
                                    }
                                }
                                z = true;
                                z2 = true;
                                playAdCallback.onAdEnd(str3, z, z2);
                            }
                        } else if (str.equals("successfulView")) {
                            this.succesfulView = true;
                        } else if (str.startsWith("percentViewed")) {
                            String[] split = str.split(":");
                            if (split.length == 2) {
                                this.percentViewed = Integer.parseInt(split[1]);
                            }
                        }
                    } catch (DatabaseHelper.DBException unused) {
                        onError(new VungleException(26), str3);
                    }
                }

                public void onError(Throwable th, String str) {
                    if (!(VungleException.getExceptionCode(th) == 15 || VungleException.getExceptionCode(th) == 25)) {
                        try {
                            Vungle._instance.repository.saveAndApplyState(advertisement, str, 4);
                        } catch (DatabaseHelper.DBException unused) {
                            th = new VungleException(26);
                        }
                    }
                    VungleActivity.setEventListener(null);
                    Vungle._instance.playOperations.put(str, false);
                    PlayAdCallback playAdCallback = playAdCallback;
                    if (playAdCallback != null) {
                        playAdCallback.onError(str, th);
                    }
                }
            });
            if (advertisement == null || !"flexview".equals(advertisement.getTemplateType())) {
                z = false;
            }
            Intent intent = new Intent(_instance.context, z ? VungleFlexViewActivity.class : VungleActivity.class);
            intent.addFlags(DriveFile.MODE_READ_ONLY);
            intent.putExtra("placement", str);
            _instance.context.startActivity(intent);
        }
    }

    public static void loadAd(String str, LoadAdCallback loadAdCallback) {
        if (isInitialized()) {
            _instance.adLoader.load(str, new LoadAdCallbackWrapper(sdkExecutors.getUIExecutor(), loadAdCallback), null);
        } else if (loadAdCallback != null) {
            loadAdCallback.onError(str, new VungleException(9));
        }
    }

    private static void clearCache() {
        if (isInitialized()) {
            sdkExecutors.getVungleExecutor().execute(new Runnable() {
                public void run() {
                    Vungle._instance.repository.clearAllData();
                    Vungle._instance.configure(Vungle._instance.runtimeValues.initCallback);
                }
            });
        } else {
            Log.e(TAG, "Vungle not initialized");
        }
    }

    @Deprecated
    public static VungleNativeAd getNativeAd(String str, PlayAdCallback playAdCallback) {
        return getNativeAd(str, null, playAdCallback);
    }

    public static VungleNativeAd getNativeAd(String str, AdConfig adConfig, final PlayAdCallback playAdCallback) {
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized, returned VungleNativeAd = null");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(9));
            }
            return null;
        }
        Placement placement = (Placement) _instance.repository.load(str, Placement.class).get();
        if (placement == null) {
            Log.e(TAG, "No Placement for ID");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(13));
            }
            return null;
        }
        final Advertisement advertisement = _instance.repository.findValidAdvertisementForPlacement(str).get();
        if (advertisement == null) {
            Log.e(TAG, "No Advertisement for ID");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(10));
            }
            return null;
        } else if (!canPlayAd(advertisement)) {
            if (advertisement != null && advertisement.getState() == 1) {
                try {
                    _instance.repository.saveAndApplyState(advertisement, str, 4);
                } catch (DatabaseHelper.DBException unused) {
                    if (playAdCallback != null) {
                        playAdCallback.onError(str, new VungleException(26));
                    }
                }
                if (placement.isAutoCached()) {
                    _instance.adLoader.loadEndless(placement.getId(), 0, null);
                }
            }
            return null;
        } else if (Boolean.TRUE.equals(_instance.playOperations.get(str)) || _instance.adLoader.isLoading(str)) {
            Log.e(TAG, "Playing or Loading operation ongoing");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(8));
            }
            return null;
        } else if (advertisement.getAdType() != 1) {
            Log.e(TAG, "Invalid Ad Type for Native Ad.");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(10));
            }
            return null;
        } else {
            AdConfig.AdSize adSize = adConfig == null ? AdConfig.AdSize.VUNGLE_DEFAULT : adConfig.getAdSize();
            if ((!"mrec".equals(advertisement.getTemplateType()) || adSize == AdConfig.AdSize.VUNGLE_MREC) && (!"flexfeed".equals(advertisement.getTemplateType()) || adSize == AdConfig.AdSize.VUNGLE_DEFAULT)) {
                _instance.playOperations.put(str, true);
                try {
                    return new VungleNativeView(_instance.context.getApplicationContext(), str, null, new AdContract.AdvertisementPresenter.EventListener() {
                        int percentViewed = -1;
                        boolean succesfulView = false;

                        public void onNext(String str, String str2, String str3) {
                            boolean z;
                            try {
                                boolean z2 = false;
                                if (str.equals("start")) {
                                    Vungle._instance.repository.saveAndApplyState(advertisement, str3, 2);
                                    if (playAdCallback != null) {
                                        playAdCallback.onAdStart(str3);
                                    }
                                    this.percentViewed = 0;
                                    Placement placement = (Placement) Vungle._instance.repository.load(str3, Placement.class).get();
                                    if (placement != null && placement.isAutoCached()) {
                                        Vungle._instance.adLoader.loadEndless(str3, 0, null);
                                    }
                                } else if (str.equals("end")) {
                                    Log.d("Vungle", "Cleaning up metadata and assets for placement " + str3 + " and advertisement " + advertisement.getId());
                                    Vungle._instance.repository.saveAndApplyState(advertisement, str3, 3);
                                    Vungle._instance.repository.updateAndSaveReportState(str3, advertisement.getAppID(), 0, 1);
                                    Vungle._instance.jobRunner.execute(SendReportsJob.makeJobInfo(false));
                                    Vungle._instance.playOperations.put(str3, false);
                                    if (playAdCallback != null) {
                                        PlayAdCallback playAdCallback = playAdCallback;
                                        if (!this.succesfulView) {
                                            if (this.percentViewed < 80) {
                                                z = false;
                                                if (str2 != null && str2.equals("isCTAClicked")) {
                                                    z2 = true;
                                                }
                                                playAdCallback.onAdEnd(str3, z, z2);
                                            }
                                        }
                                        z = true;
                                        z2 = true;
                                        playAdCallback.onAdEnd(str3, z, z2);
                                    }
                                } else if (str.equals("successfulView")) {
                                    this.succesfulView = true;
                                } else if (str.startsWith("percentViewed")) {
                                    String[] split = str.split(":");
                                    if (split.length == 2) {
                                        this.percentViewed = Integer.parseInt(split[1]);
                                    }
                                }
                            } catch (DatabaseHelper.DBException unused) {
                                onError(new VungleException(26), str3);
                            }
                        }

                        public void onError(Throwable th, String str) {
                            Vungle._instance.playOperations.put(str, false);
                            if (VungleException.getExceptionCode(th) != 25) {
                                try {
                                    Vungle._instance.repository.saveAndApplyState(advertisement, str, 4);
                                } catch (DatabaseHelper.DBException unused) {
                                    th = new VungleException(26);
                                }
                            }
                            PlayAdCallback playAdCallback = playAdCallback;
                            if (playAdCallback != null) {
                                playAdCallback.onError(str, th);
                            }
                        }
                    });
                } catch (Exception e) {
                    _instance.playOperations.put(str, false);
                    if (playAdCallback != null) {
                        playAdCallback.onError(str, new InstantiationException(e.getMessage()));
                    }
                    return null;
                }
            } else {
                Log.e(TAG, "Corresponding AdConfig#setAdSize must be passed for the type/size of native ad");
                return null;
            }
        }
    }

    public static Collection<String> getValidPlacements() {
        if (isInitialized()) {
            return _instance.repository.getValidPlacements().get();
        }
        Log.e(TAG, "Vungle is not initialized return empty placemetns list");
        return Collections.emptyList();
    }

    public static void updateConsentStatus(final Consent consent2, final String str) {
        Vungle vungle = _instance;
        vungle.consent = consent2;
        vungle.consentVersion = str;
        if (isDepInit.get() && _instance.repository != null) {
            _instance.repository.load(Cookie.CONSENT_COOKIE, Cookie.class, new Repository.LoadCallback<Cookie>() {
                public void onLoaded(Cookie cookie) {
                    if (cookie == null) {
                        cookie = new Cookie(Cookie.CONSENT_COOKIE);
                    }
                    cookie.putValue("consent_status", consent2 == Consent.OPTED_IN ? "opted_in" : "opted_out");
                    cookie.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                    cookie.putValue("consent_source", "publisher");
                    String str = str;
                    if (str == null) {
                        str = "";
                    }
                    cookie.putValue("consent_message_version", str);
                    Vungle._instance.repository.save(cookie, null);
                }
            });
        }
    }

    public static Consent getConsentStatus() {
        if (isInitialized()) {
            return _instance.consent;
        }
        Log.e(TAG, "Vungle is not initialized, consent is null");
        return null;
    }

    public static String getConsentMessageVersion() {
        if (isInitialized()) {
            return _instance.consentVersion;
        }
        Log.e(TAG, "Vungle is not initialized, please wait initialize or wait until Vungle is intialized to get Consent Message Version");
        return null;
    }

    /* access modifiers changed from: private */
    public static Consent getConsent(Cookie cookie) {
        if (cookie == null) {
            return null;
        }
        return "opted_in".equals(cookie.getString("consent_status")) ? Consent.OPTED_IN : Consent.OPTED_OUT;
    }

    /* access modifiers changed from: private */
    public static String getConsentMessageVersion(Cookie cookie) {
        if (cookie == null) {
            return null;
        }
        return cookie.getString("consent_message_version");
    }

    public static boolean closeFlexViewAd(String str) {
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized, can't close flex view ad");
            return false;
        }
        Intent intent = new Intent(AdContract.AdvertisementBus.ACTION);
        intent.putExtra("placement", str);
        intent.putExtra(AdContract.AdvertisementBus.COMMAND, AdContract.AdvertisementBus.CLOSE_FLEX);
        LocalBroadcastManager.getInstance(_instance.context).sendBroadcast(intent);
        return true;
    }

    public static void setUserLegacyID(String str) {
        if (isInitialized() || isInitializing.get()) {
            _instance.vungleApiClient.updateIMEI(str, _instance.shouldTransmitIMEI);
        } else {
            _instance.userIMEI = str;
        }
    }

    public static void setHeaderBiddingCallback(HeaderBiddingCallback headerBiddingCallback) {
        _instance.runtimeValues.headerBiddingCallback = new HeaderBiddingCallbackWrapper(sdkExecutors.getUIExecutor(), headerBiddingCallback);
    }

    protected static void mockDependencies(Context context2, String str, AssetDownloader assetDownloader, Repository repository2, VungleApiClient vungleApiClient2, JobRunner jobRunner2, SDKExecutors sDKExecutors, CacheManager cacheManager2, AdLoader adLoader2) {
        sdkExecutors = sDKExecutors;
        Vungle vungle = _instance;
        vungle.context = context2;
        vungle.appID = str;
        vungle.downloader = assetDownloader;
        vungle.repository = repository2;
        vungle.vungleApiClient = vungleApiClient2;
        vungle.jobRunner = jobRunner2;
        vungle.cacheManager = cacheManager2;
        vungle.adLoader = adLoader2;
    }

    protected static void deInit() {
        if (_instance.cacheManager != null) {
            _instance.cacheManager.removeListener(cacheListener);
        }
        Vungle vungle = _instance;
        vungle.context = null;
        vungle.appID = null;
        vungle.downloader = null;
        vungle.repository = null;
        vungle.vungleApiClient = null;
        vungle.jobRunner = null;
        vungle.cacheManager = null;
        vungle.adLoader = null;
        vungle.runtimeValues.initCallback = null;
        isInitialized = false;
        isDepInit.set(false);
        isInitializing.set(false);
    }

    /* access modifiers changed from: private */
    public static void stopPlaying() {
        if (_instance.context != null) {
            Intent intent = new Intent(AdContract.AdvertisementBus.ACTION);
            intent.putExtra(AdContract.AdvertisementBus.COMMAND, AdContract.AdvertisementBus.STOP_ALL);
            LocalBroadcastManager.getInstance(_instance.context).sendBroadcast(intent);
        }
    }
}
