package com.vungle.warren.downloader;

import android.text.TextUtils;
import android.util.Log;
import com.vungle.warren.downloader.AssetDownloadListener;
import com.vungle.warren.utility.FileUtility;
import com.vungle.warren.utility.NetworkProvider;
import com.vungle.warren.utility.PriorityRunnable;
import java.io.File;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLException;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.RealResponseBody;
import okio.GzipSource;
import okio.Okio;

public class AssetDownloader {
    private static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final String ACCEPT_RANGES = "Accept-Ranges";
    private static final String BYTES = "bytes";
    private static final int CONNECTION_RETRY_TIMEOUT = 300;
    private static final String CONTENT_ENCODING = "Content-Encoding";
    private static final String CONTENT_RANGE = "Content-Range";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final int DOWNLOAD_CHUNK_SIZE = 2048;
    private static final String ETAG = "ETag";
    private static final String GZIP = "gzip";
    private static final String IDENTITY = "identity";
    private static final String IF_RANGE = "If-Range";
    private static final String LAST_MODIFIED = "Last-Modified";
    private static final long MAX_PERCENT = 100;
    private static final int MAX_RECONNECT_ATTEMPTS = 10;
    private static final String META_POSTFIX_EXT = ".vng_meta";
    private static final int PROGRESS_STEP = 5;
    private static final String RANGE = "Range";
    private static final int RANGE_NOT_SATISFIABLE = 416;
    private static final int RETRY_COUNT_ON_CONNECTION_LOST = 5;
    /* access modifiers changed from: private */
    public static final String TAG = AssetDownloader.class.getSimpleName();
    private static final int TIMEOUT = 30;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, DownloadRequest> connections = new ConcurrentHashMap<>();
    private final ExecutorService downloadExecutor;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, AssetDownloadListener> listeners = new ConcurrentHashMap<>();
    int maxReconnectAttempts = 10;
    /* access modifiers changed from: private */
    public final NetworkProvider.NetworkListener networkListener = new NetworkProvider.NetworkListener() {
        public void onChanged(int i) {
            String access$100 = AssetDownloader.TAG;
            Log.d(access$100, "Network changed: " + i);
            AssetDownloader.this.onNetworkChanged(i);
        }
    };
    /* access modifiers changed from: private */
    public final NetworkProvider networkProvider;
    /* access modifiers changed from: private */
    public final OkHttpClient okHttpClient;
    /* access modifiers changed from: private */
    public volatile int progressStep = 5;
    int reconnectTimeout = 300;
    int retryCountOnConnectionLost = 5;
    private final ExecutorService uiExecutor;

    public @interface NetworkType {
        public static final int ANY = 3;
        public static final int CELLULAR = 1;
        public static final int WIFI = 2;
    }

    public AssetDownloader(int i, NetworkProvider networkProvider2, ExecutorService executorService) {
        int max = Math.max(i, 1);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(max, max, 1, TimeUnit.SECONDS, new PriorityBlockingQueue());
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        this.downloadExecutor = threadPoolExecutor;
        this.networkProvider = networkProvider2;
        this.uiExecutor = executorService;
        this.okHttpClient = new OkHttpClient.Builder().readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS).cache(null).followRedirects(true).followSslRedirects(true).build();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0077, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0010, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void download(final com.vungle.warren.downloader.DownloadRequest r5, final com.vungle.warren.downloader.AssetDownloadListener r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            if (r5 != 0) goto L_0x0011
            if (r6 == 0) goto L_0x000f
            java.util.concurrent.ExecutorService r5 = r4.uiExecutor     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.AssetDownloader$1 r0 = new com.vungle.warren.downloader.AssetDownloader$1     // Catch:{ all -> 0x0092 }
            r0.<init>(r6)     // Catch:{ all -> 0x0092 }
            r5.execute(r0)     // Catch:{ all -> 0x0092 }
        L_0x000f:
            monitor-exit(r4)
            return
        L_0x0011:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r0 = r4.connections     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = r5.id     // Catch:{ all -> 0x0092 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.DownloadRequest r0 = (com.vungle.warren.downloader.DownloadRequest) r0     // Catch:{ all -> 0x0092 }
            if (r0 == 0) goto L_0x0043
            java.lang.String r1 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0092 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0092 }
            r2.<init>()     // Catch:{ all -> 0x0092 }
            java.lang.String r3 = "Request with same url and path already present "
            r2.append(r3)     // Catch:{ all -> 0x0092 }
            java.lang.String r0 = r0.id     // Catch:{ all -> 0x0092 }
            r2.append(r0)     // Catch:{ all -> 0x0092 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0092 }
            android.util.Log.d(r1, r0)     // Catch:{ all -> 0x0092 }
            if (r6 == 0) goto L_0x0041
            java.util.concurrent.ExecutorService r0 = r4.uiExecutor     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.AssetDownloader$2 r1 = new com.vungle.warren.downloader.AssetDownloader$2     // Catch:{ all -> 0x0092 }
            r1.<init>(r6, r5)     // Catch:{ all -> 0x0092 }
            r0.execute(r1)     // Catch:{ all -> 0x0092 }
        L_0x0041:
            monitor-exit(r4)
            return
        L_0x0043:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r0 = r4.connections     // Catch:{ all -> 0x0092 }
            java.util.Collection r0 = r0.values()     // Catch:{ all -> 0x0092 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0092 }
        L_0x004d:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x0092 }
            if (r1 == 0) goto L_0x0078
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.DownloadRequest r1 = (com.vungle.warren.downloader.DownloadRequest) r1     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = r1.path     // Catch:{ all -> 0x0092 }
            java.lang.String r2 = r5.path     // Catch:{ all -> 0x0092 }
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x0092 }
            if (r1 == 0) goto L_0x004d
            java.lang.String r0 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = "Already present request for same path"
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x0092 }
            if (r6 == 0) goto L_0x0076
            java.util.concurrent.ExecutorService r0 = r4.uiExecutor     // Catch:{ all -> 0x0092 }
            com.vungle.warren.downloader.AssetDownloader$3 r1 = new com.vungle.warren.downloader.AssetDownloader$3     // Catch:{ all -> 0x0092 }
            r1.<init>(r6, r5)     // Catch:{ all -> 0x0092 }
            r0.execute(r1)     // Catch:{ all -> 0x0092 }
        L_0x0076:
            monitor-exit(r4)
            return
        L_0x0078:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.AssetDownloadListener> r0 = r4.listeners     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = r5.id     // Catch:{ all -> 0x0092 }
            r0.put(r1, r6)     // Catch:{ all -> 0x0092 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r6 = r4.connections     // Catch:{ all -> 0x0092 }
            java.lang.String r0 = r5.id     // Catch:{ all -> 0x0092 }
            r6.put(r0, r5)     // Catch:{ all -> 0x0092 }
            com.vungle.warren.utility.NetworkProvider r6 = r4.networkProvider     // Catch:{ all -> 0x0092 }
            com.vungle.warren.utility.NetworkProvider$NetworkListener r0 = r4.networkListener     // Catch:{ all -> 0x0092 }
            r6.addListener(r0)     // Catch:{ all -> 0x0092 }
            r4.load(r5)     // Catch:{ all -> 0x0092 }
            monitor-exit(r4)
            return
        L_0x0092:
            r5 = move-exception
            monitor-exit(r4)
            goto L_0x0096
        L_0x0095:
            throw r5
        L_0x0096:
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.download(com.vungle.warren.downloader.DownloadRequest, com.vungle.warren.downloader.AssetDownloadListener):void");
    }

    public List<DownloadRequest> getAllRequests() {
        return new ArrayList(this.connections.values());
    }

    private synchronized void load(final DownloadRequest downloadRequest) {
        downloadRequest.set(1);
        this.downloadExecutor.execute(new DownloadPriorityRunnable(downloadRequest.priority) {
            /* JADX INFO: finally extract failed */
            /* JADX WARN: Failed to insert an additional move for type inference into block B:484:0x0012 */
            /* JADX INFO: additional move instructions added (1) to help type inference */
            /* JADX INFO: additional move instructions added (3) to help type inference */
            /* JADX WARN: Type inference failed for: r3v1 */
            /* JADX WARN: Type inference failed for: r3v2, types: [java.io.Closeable, java.io.File] */
            /* JADX WARN: Type inference failed for: r3v3 */
            /* JADX WARN: Type inference failed for: r3v4 */
            /* JADX WARN: Type inference failed for: r12v5, types: [java.io.Closeable] */
            /* JADX WARN: Type inference failed for: r12v6 */
            /* JADX WARN: Type inference failed for: r12v7 */
            /* JADX WARN: Type inference failed for: r12v8 */
            /* JADX WARN: Type inference failed for: r12v9 */
            /* JADX WARN: Type inference failed for: r12v10 */
            /* JADX WARN: Type inference failed for: r12v11 */
            /* JADX WARN: Type inference failed for: r12v12 */
            /* JADX WARN: Type inference failed for: r12v13 */
            /* JADX WARN: Type inference failed for: r12v14 */
            /* JADX WARN: Type inference failed for: r3v37 */
            /* JADX WARN: Type inference failed for: r12v16 */
            /* JADX WARN: Type inference failed for: r10v17, types: [okio.BufferedSource, java.io.Closeable] */
            /* JADX WARN: Type inference failed for: r12v20 */
            /* JADX WARN: Type inference failed for: r12v35 */
            /* JADX WARN: Type inference failed for: r12v36 */
            /* JADX WARN: Type inference failed for: r12v37 */
            /* JADX WARN: Type inference failed for: r12v38 */
            /* JADX WARN: Type inference failed for: r12v39 */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            /* JADX WARNING: Multi-variable type inference failed */
            /* JADX WARNING: Removed duplicated region for block: B:339:0x077a A[SYNTHETIC, Splitter:B:339:0x077a] */
            /* JADX WARNING: Removed duplicated region for block: B:363:0x07ce A[Catch:{ all -> 0x085e }] */
            /* JADX WARNING: Removed duplicated region for block: B:379:0x0813 A[SYNTHETIC, Splitter:B:379:0x0813] */
            /* JADX WARNING: Removed duplicated region for block: B:397:0x0861  */
            /* JADX WARNING: Removed duplicated region for block: B:411:0x0885  */
            /* JADX WARNING: Removed duplicated region for block: B:414:0x08ab  */
            /* JADX WARNING: Removed duplicated region for block: B:451:0x0977  */
            /* JADX WARNING: Removed duplicated region for block: B:454:0x099d  */
            /* JADX WARNING: Removed duplicated region for block: B:509:0x07cd A[SYNTHETIC] */
            public void run() {
                /*
                    r30 = this;
                    r1 = r30
                    com.vungle.warren.downloader.AssetDownloadListener$Progress r2 = new com.vungle.warren.downloader.AssetDownloadListener$Progress
                    r2.<init>()
                    long r3 = java.lang.System.currentTimeMillis()
                    r2.timestampDownloadStart = r3
                    r3 = 0
                    r6 = r3
                    r5 = 0
                    r7 = 0
                    r8 = 0
                L_0x0012:
                    if (r5 != 0) goto L_0x0a4d
                    com.vungle.warren.downloader.DownloadRequest r5 = r4
                    java.lang.String r5 = r5.url
                    com.vungle.warren.downloader.DownloadRequest r9 = r4
                    java.lang.String r9 = r9.path
                    java.lang.String r10 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder
                    r11.<init>()
                    java.lang.String r12 = "Start load: "
                    r11.append(r12)
                    com.vungle.warren.downloader.DownloadRequest r12 = r4
                    java.lang.String r12 = r12.id
                    r11.append(r12)
                    java.lang.String r12 = " url: "
                    r11.append(r12)
                    r11.append(r5)
                    java.lang.String r11 = r11.toString()
                    android.util.Log.d(r10, r11)
                    r11 = 2
                    r12 = 4
                    r13 = 3
                    r14 = 5
                    r15 = 1
                    com.vungle.warren.downloader.DownloadRequest r10 = r4     // Catch:{ all -> 0x075a }
                    boolean r10 = r10.is(r15)     // Catch:{ all -> 0x075a }
                    if (r10 != 0) goto L_0x0126
                    java.lang.String r5 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0119 }
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0119 }
                    r9.<init>()     // Catch:{ all -> 0x0119 }
                    java.lang.String r10 = "Abort download, wrong state "
                    r9.append(r10)     // Catch:{ all -> 0x0119 }
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0119 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x0119 }
                    java.lang.String r4 = r10.debugString(r4)     // Catch:{ all -> 0x0119 }
                    r9.append(r4)     // Catch:{ all -> 0x0119 }
                    java.lang.String r4 = r9.toString()     // Catch:{ all -> 0x0119 }
                    android.util.Log.w(r5, r4)     // Catch:{ all -> 0x0119 }
                    java.lang.String r4 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r8 = "request is done "
                    r5.append(r8)
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r9 = r4
                    java.lang.String r8 = r8.debugString(r9)
                    r5.append(r8)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r4, r5)
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r4)
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0115 }
                    int r5 = r5.getStatus()     // Catch:{ all -> 0x0115 }
                    if (r5 == r11) goto L_0x00d2
                    if (r5 == r13) goto L_0x00af
                    if (r5 == r12) goto L_0x00a7
                    if (r5 == r14) goto L_0x009f
                    goto L_0x00b6
                L_0x009f:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0115 }
                    r2.onError(r6, r5)     // Catch:{ all -> 0x0115 }
                    goto L_0x00d2
                L_0x00a7:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0115 }
                    r2.onSuccess(r3, r5)     // Catch:{ all -> 0x0115 }
                    goto L_0x00d2
                L_0x00af:
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x0115 }
                    r5.onCancelled(r6, r2)     // Catch:{ all -> 0x0115 }
                L_0x00b6:
                    if (r7 != 0) goto L_0x00d2
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0115 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0115 }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x0115 }
                    r2.remove(r5)     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0115 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.listeners     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0115 }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x0115 }
                    r2.remove(r5)     // Catch:{ all -> 0x0115 }
                L_0x00d2:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0115 }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0115 }
                    r5.<init>()     // Catch:{ all -> 0x0115 }
                    java.lang.String r6 = "Removing connections and listener "
                    r5.append(r6)     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x0115 }
                    java.lang.String r6 = r6.debugString(r7)     // Catch:{ all -> 0x0115 }
                    r5.append(r6)     // Catch:{ all -> 0x0115 }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0115 }
                    android.util.Log.d(r2, r5)     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0115 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0115 }
                    boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0115 }
                    if (r2 == 0) goto L_0x010d
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.utility.NetworkProvider r2 = r2.networkProvider     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r5 = r5.networkListener     // Catch:{ all -> 0x0115 }
                    r2.removeListener(r5)     // Catch:{ all -> 0x0115 }
                L_0x010d:
                    monitor-exit(r4)     // Catch:{ all -> 0x0115 }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r3)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r3)
                    return
                L_0x0115:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r4)     // Catch:{ all -> 0x0115 }
                    throw r2
                L_0x0119:
                    r0 = move-exception
                    r4 = r3
                    r5 = r4
                    r10 = r5
                    r12 = r10
                    r13 = r6
                    r27 = r7
                    r11 = -1
                    r3 = r0
                    r6 = r12
                    goto L_0x0767
                L_0x0126:
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x075a }
                    com.vungle.warren.downloader.DownloadRequest r10 = r4     // Catch:{ all -> 0x075a }
                    boolean r4 = r4.isConnected(r10)     // Catch:{ all -> 0x075a }
                    if (r4 == 0) goto L_0x073f
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x075a }
                    r4.setConnected(r15)     // Catch:{ all -> 0x075a }
                    java.io.File r4 = new java.io.File     // Catch:{ all -> 0x075a }
                    r4.<init>(r9)     // Catch:{ all -> 0x075a }
                    java.io.File r10 = new java.io.File     // Catch:{ all -> 0x0734 }
                    java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ all -> 0x0734 }
                    r15.<init>()     // Catch:{ all -> 0x0734 }
                    r15.append(r9)     // Catch:{ all -> 0x0734 }
                    java.lang.String r9 = ".vng_meta"
                    r15.append(r9)     // Catch:{ all -> 0x0734 }
                    java.lang.String r9 = r15.toString()     // Catch:{ all -> 0x0734 }
                    r10.<init>(r9)     // Catch:{ all -> 0x0734 }
                    java.io.File r9 = r4.getParentFile()     // Catch:{ all -> 0x0734 }
                    if (r9 == 0) goto L_0x0175
                    java.io.File r9 = r4.getParentFile()     // Catch:{ all -> 0x0168 }
                    boolean r9 = r9.exists()     // Catch:{ all -> 0x0168 }
                    if (r9 != 0) goto L_0x0175
                    java.io.File r9 = r4.getParentFile()     // Catch:{ all -> 0x0168 }
                    r9.mkdirs()     // Catch:{ all -> 0x0168 }
                    goto L_0x0175
                L_0x0168:
                    r0 = move-exception
                    r5 = r3
                    r10 = r5
                    r12 = r10
                    r13 = r6
                    r27 = r7
                    r11 = -1
                    r3 = r0
                    r6 = r4
                    r4 = r12
                    goto L_0x0767
                L_0x0175:
                    boolean r9 = r4.exists()     // Catch:{ all -> 0x0734 }
                    r19 = 0
                    if (r9 == 0) goto L_0x0184
                    long r21 = r4.length()     // Catch:{ all -> 0x0168 }
                    r14 = r21
                    goto L_0x0186
                L_0x0184:
                    r14 = r19
                L_0x0186:
                    java.lang.String r9 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0734 }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0734 }
                    r3.<init>()     // Catch:{ all -> 0x0734 }
                    java.lang.String r13 = "already downloaded : "
                    r3.append(r13)     // Catch:{ all -> 0x0734 }
                    r3.append(r14)     // Catch:{ all -> 0x0734 }
                    java.lang.String r13 = ", file exists = "
                    r3.append(r13)     // Catch:{ all -> 0x0734 }
                    boolean r13 = r4.exists()     // Catch:{ all -> 0x0734 }
                    r3.append(r13)     // Catch:{ all -> 0x0734 }
                    com.vungle.warren.downloader.AssetDownloader r13 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0734 }
                    com.vungle.warren.downloader.DownloadRequest r11 = r4     // Catch:{ all -> 0x0734 }
                    java.lang.String r11 = r13.debugString(r11)     // Catch:{ all -> 0x0734 }
                    r3.append(r11)     // Catch:{ all -> 0x0734 }
                    java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0734 }
                    android.util.Log.d(r9, r3)     // Catch:{ all -> 0x0734 }
                    okhttp3.Request$Builder r3 = new okhttp3.Request$Builder     // Catch:{ all -> 0x0734 }
                    r3.<init>()     // Catch:{ all -> 0x0734 }
                    okhttp3.Request$Builder r3 = r3.url(r5)     // Catch:{ all -> 0x0734 }
                    java.lang.String r5 = "Accept-Encoding"
                    java.lang.String r9 = "identity"
                    okhttp3.Request$Builder r3 = r3.addHeader(r5, r9)     // Catch:{ all -> 0x0734 }
                    boolean r5 = r4.exists()     // Catch:{ all -> 0x0734 }
                    if (r5 == 0) goto L_0x0245
                    boolean r5 = r10.exists()     // Catch:{ all -> 0x0238 }
                    if (r5 == 0) goto L_0x0245
                    java.lang.String r5 = r10.getPath()     // Catch:{ all -> 0x0238 }
                    java.util.Map r5 = com.vungle.warren.utility.FileUtility.readMap(r5)     // Catch:{ all -> 0x0238 }
                    java.lang.String r9 = "bytes"
                    java.lang.String r11 = "Accept-Ranges"
                    java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x0238 }
                    java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x0238 }
                    boolean r9 = r9.equalsIgnoreCase(r11)     // Catch:{ all -> 0x0238 }
                    if (r9 == 0) goto L_0x0245
                    java.lang.String r9 = "identity"
                    java.lang.String r11 = "Content-Encoding"
                    java.lang.Object r11 = r5.get(r11)     // Catch:{ all -> 0x0238 }
                    java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x0238 }
                    boolean r9 = r9.equalsIgnoreCase(r11)     // Catch:{ all -> 0x0238 }
                    if (r9 != 0) goto L_0x0202
                    java.lang.String r9 = "Content-Encoding"
                    java.lang.Object r9 = r5.get(r9)     // Catch:{ all -> 0x0238 }
                    if (r9 != 0) goto L_0x0245
                L_0x0202:
                    java.lang.String r9 = "Range"
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0238 }
                    r11.<init>()     // Catch:{ all -> 0x0238 }
                    java.lang.String r13 = "bytes="
                    r11.append(r13)     // Catch:{ all -> 0x0238 }
                    r11.append(r14)     // Catch:{ all -> 0x0238 }
                    java.lang.String r13 = "-"
                    r11.append(r13)     // Catch:{ all -> 0x0238 }
                    java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x0238 }
                    r3.addHeader(r9, r11)     // Catch:{ all -> 0x0238 }
                    java.lang.String r9 = "ETag"
                    java.lang.Object r9 = r5.get(r9)     // Catch:{ all -> 0x0238 }
                    java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x0238 }
                    if (r9 != 0) goto L_0x0230
                    java.lang.String r9 = "Last-Modified"
                    java.lang.Object r5 = r5.get(r9)     // Catch:{ all -> 0x0238 }
                    r9 = r5
                    java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x0238 }
                L_0x0230:
                    if (r9 == 0) goto L_0x0245
                    java.lang.String r5 = "If-Range"
                    r3.addHeader(r5, r9)     // Catch:{ all -> 0x0238 }
                    goto L_0x0245
                L_0x0238:
                    r0 = move-exception
                    r3 = r0
                    r13 = r6
                    r27 = r7
                    r5 = 0
                    r10 = 0
                    r11 = -1
                    r12 = 0
                    r6 = r4
                    r4 = 0
                    goto L_0x0767
                L_0x0245:
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0734 }
                    okhttp3.OkHttpClient r5 = r5.okHttpClient     // Catch:{ all -> 0x0734 }
                    okhttp3.Request r3 = r3.build()     // Catch:{ all -> 0x0734 }
                    okhttp3.Call r3 = r5.newCall(r3)     // Catch:{ all -> 0x0734 }
                    okhttp3.Response r5 = com.google.firebase.perf.network.FirebasePerfOkHttpClient.execute(r3)     // Catch:{ all -> 0x0726 }
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x071e }
                    long r23 = r9.getContentLength(r5)     // Catch:{ all -> 0x071e }
                    java.lang.String r9 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x071e }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x071e }
                    r11.<init>()     // Catch:{ all -> 0x071e }
                    java.lang.String r13 = "Response code: "
                    r11.append(r13)     // Catch:{ all -> 0x071e }
                    int r13 = r5.code()     // Catch:{ all -> 0x071e }
                    r11.append(r13)     // Catch:{ all -> 0x071e }
                    java.lang.String r13 = " "
                    r11.append(r13)     // Catch:{ all -> 0x071e }
                    com.vungle.warren.downloader.DownloadRequest r13 = r4     // Catch:{ all -> 0x071e }
                    java.lang.String r13 = r13.id     // Catch:{ all -> 0x071e }
                    r11.append(r13)     // Catch:{ all -> 0x071e }
                    java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x071e }
                    android.util.Log.d(r9, r11)     // Catch:{ all -> 0x071e }
                    int r11 = r5.code()     // Catch:{ all -> 0x071e }
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0712 }
                    com.vungle.warren.downloader.DownloadRequest r13 = r4     // Catch:{ all -> 0x0712 }
                    boolean r9 = r9.fullyDownloadedContent(r4, r5, r13)     // Catch:{ all -> 0x0712 }
                    if (r9 == 0) goto L_0x0365
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x035c }
                    r9.set(r12)     // Catch:{ all -> 0x035c }
                    if (r5 == 0) goto L_0x02a7
                    okhttp3.ResponseBody r8 = r5.body()
                    if (r8 == 0) goto L_0x02a7
                    okhttp3.ResponseBody r5 = r5.body()
                    r5.close()
                L_0x02a7:
                    if (r3 == 0) goto L_0x02ac
                    r3.cancel()
                L_0x02ac:
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r8 = "request is done "
                    r5.append(r8)
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r9 = r4
                    java.lang.String r8 = r8.debugString(r9)
                    r5.append(r8)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r3, r5)
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r10)
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ all -> 0x0358 }
                    int r3 = r3.getStatus()     // Catch:{ all -> 0x0358 }
                    r5 = 2
                    if (r3 == r5) goto L_0x0314
                    r5 = 3
                    if (r3 == r5) goto L_0x02f1
                    if (r3 == r12) goto L_0x02e9
                    r2 = 5
                    if (r3 == r2) goto L_0x02e1
                    goto L_0x02f8
                L_0x02e1:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ all -> 0x0358 }
                    r2.onError(r6, r3)     // Catch:{ all -> 0x0358 }
                    goto L_0x0314
                L_0x02e9:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ all -> 0x0358 }
                    r2.onSuccess(r4, r3)     // Catch:{ all -> 0x0358 }
                    goto L_0x0314
                L_0x02f1:
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x0358 }
                    r3.onCancelled(r4, r2)     // Catch:{ all -> 0x0358 }
                L_0x02f8:
                    if (r7 != 0) goto L_0x0314
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0358 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ all -> 0x0358 }
                    java.lang.String r3 = r3.id     // Catch:{ all -> 0x0358 }
                    r2.remove(r3)     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0358 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.listeners     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ all -> 0x0358 }
                    java.lang.String r3 = r3.id     // Catch:{ all -> 0x0358 }
                    r2.remove(r3)     // Catch:{ all -> 0x0358 }
                L_0x0314:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0358 }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0358 }
                    r3.<init>()     // Catch:{ all -> 0x0358 }
                    java.lang.String r4 = "Removing connections and listener "
                    r3.append(r4)     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0358 }
                    java.lang.String r4 = r4.debugString(r5)     // Catch:{ all -> 0x0358 }
                    r3.append(r4)     // Catch:{ all -> 0x0358 }
                    java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0358 }
                    android.util.Log.d(r2, r3)     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0358 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0358 }
                    boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0358 }
                    if (r2 == 0) goto L_0x034f
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.utility.NetworkProvider r2 = r2.networkProvider     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0358 }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r3 = r3.networkListener     // Catch:{ all -> 0x0358 }
                    r2.removeListener(r3)     // Catch:{ all -> 0x0358 }
                L_0x034f:
                    monitor-exit(r10)     // Catch:{ all -> 0x0358 }
                    r2 = 0
                    com.vungle.warren.utility.FileUtility.closeQuietly(r2)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r2)
                    return
                L_0x0358:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r10)     // Catch:{ all -> 0x0358 }
                    throw r2
                L_0x035c:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r10 = 0
                L_0x0361:
                    r12 = 0
                L_0x0362:
                    r6 = r4
                    goto L_0x0731
                L_0x0365:
                    r13 = 206(0xce, float:2.89E-43)
                    if (r11 != r13) goto L_0x0373
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x035c }
                    com.vungle.warren.downloader.DownloadRequest r12 = r4     // Catch:{ all -> 0x035c }
                    boolean r9 = r9.satisfiesPartialDownload(r5, r14, r12)     // Catch:{ all -> 0x035c }
                    if (r9 == 0) goto L_0x0377
                L_0x0373:
                    r9 = 416(0x1a0, float:5.83E-43)
                    if (r11 != r9) goto L_0x043d
                L_0x0377:
                    boolean r9 = r4.exists()     // Catch:{ all -> 0x0435 }
                    if (r9 == 0) goto L_0x0380
                    r4.delete()     // Catch:{ all -> 0x035c }
                L_0x0380:
                    boolean r9 = r10.exists()     // Catch:{ all -> 0x0435 }
                    if (r9 == 0) goto L_0x0389
                    r10.delete()     // Catch:{ all -> 0x035c }
                L_0x0389:
                    int r9 = r8 + 1
                    com.vungle.warren.downloader.AssetDownloader r10 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x042e }
                    int r10 = r10.maxReconnectAttempts     // Catch:{ all -> 0x042e }
                    if (r8 >= r10) goto L_0x0414
                    if (r5 == 0) goto L_0x03a0
                    okhttp3.ResponseBody r4 = r5.body()
                    if (r4 == 0) goto L_0x03a0
                    okhttp3.ResponseBody r4 = r5.body()
                    r4.close()
                L_0x03a0:
                    if (r3 == 0) goto L_0x03a5
                    r3.cancel()
                L_0x03a5:
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "request is done "
                    r4.append(r5)
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r8 = r4
                    java.lang.String r5 = r5.debugString(r8)
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    android.util.Log.d(r3, r4)
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r8)
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0410 }
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0410 }
                    r4.<init>()     // Catch:{ all -> 0x0410 }
                    java.lang.String r5 = "Not removing connections and listener "
                    r4.append(r5)     // Catch:{ all -> 0x0410 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0410 }
                    com.vungle.warren.downloader.DownloadRequest r10 = r4     // Catch:{ all -> 0x0410 }
                    java.lang.String r5 = r5.debugString(r10)     // Catch:{ all -> 0x0410 }
                    r4.append(r5)     // Catch:{ all -> 0x0410 }
                    java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0410 }
                    android.util.Log.d(r3, r4)     // Catch:{ all -> 0x0410 }
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0410 }
                    java.util.concurrent.ConcurrentHashMap r3 = r3.connections     // Catch:{ all -> 0x0410 }
                    boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x0410 }
                    if (r3 == 0) goto L_0x0403
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0410 }
                    com.vungle.warren.utility.NetworkProvider r3 = r3.networkProvider     // Catch:{ all -> 0x0410 }
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0410 }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r4 = r4.networkListener     // Catch:{ all -> 0x0410 }
                    r3.removeListener(r4)     // Catch:{ all -> 0x0410 }
                L_0x0403:
                    monitor-exit(r8)     // Catch:{ all -> 0x0410 }
                    r12 = 0
                    com.vungle.warren.utility.FileUtility.closeQuietly(r12)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r12)
                    r8 = r9
                    r3 = r12
                    r5 = 0
                    goto L_0x0012
                L_0x0410:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r8)     // Catch:{ all -> 0x0410 }
                    throw r2
                L_0x0414:
                    r12 = 0
                    com.vungle.warren.downloader.AssetDownloader$RequestException r8 = new com.vungle.warren.downloader.AssetDownloader$RequestException     // Catch:{ all -> 0x042c }
                    java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x042c }
                    r10.<init>()     // Catch:{ all -> 0x042c }
                    java.lang.String r13 = "Code: "
                    r10.append(r13)     // Catch:{ all -> 0x042c }
                    r10.append(r11)     // Catch:{ all -> 0x042c }
                    java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x042c }
                    r8.<init>(r10)     // Catch:{ all -> 0x042c }
                    throw r8     // Catch:{ all -> 0x042c }
                L_0x042c:
                    r0 = move-exception
                    goto L_0x0430
                L_0x042e:
                    r0 = move-exception
                    r12 = 0
                L_0x0430:
                    r13 = r6
                    r27 = r7
                    r8 = r9
                    goto L_0x043a
                L_0x0435:
                    r0 = move-exception
                    r12 = 0
                L_0x0437:
                    r13 = r6
                    r27 = r7
                L_0x043a:
                    r10 = r12
                    goto L_0x0362
                L_0x043d:
                    r12 = 0
                    boolean r9 = r5.isSuccessful()     // Catch:{ all -> 0x0712 }
                    if (r9 == 0) goto L_0x06ea
                    if (r11 == r13) goto L_0x0454
                    boolean r9 = r4.exists()     // Catch:{ all -> 0x0452 }
                    if (r9 == 0) goto L_0x044f
                    r4.delete()     // Catch:{ all -> 0x0452 }
                L_0x044f:
                    r14 = r19
                    goto L_0x0454
                L_0x0452:
                    r0 = move-exception
                    goto L_0x0437
                L_0x0454:
                    r10.delete()     // Catch:{ all -> 0x06e0 }
                    okhttp3.Headers r9 = r5.headers()     // Catch:{ all -> 0x06e0 }
                    java.lang.String r13 = "Content-Encoding"
                    java.lang.String r13 = r9.get(r13)     // Catch:{ all -> 0x06e0 }
                    if (r13 == 0) goto L_0x047c
                    java.lang.String r12 = "gzip"
                    boolean r12 = r12.equalsIgnoreCase(r13)     // Catch:{ all -> 0x035c }
                    if (r12 != 0) goto L_0x047c
                    java.lang.String r12 = "identity"
                    boolean r12 = r12.equalsIgnoreCase(r13)     // Catch:{ all -> 0x035c }
                    if (r12 == 0) goto L_0x0474
                    goto L_0x047c
                L_0x0474:
                    java.io.IOException r9 = new java.io.IOException     // Catch:{ all -> 0x035c }
                    java.lang.String r10 = "Unknown Content-Encoding"
                    r9.<init>(r10)     // Catch:{ all -> 0x035c }
                    throw r9     // Catch:{ all -> 0x035c }
                L_0x047c:
                    java.util.HashMap r12 = new java.util.HashMap     // Catch:{ all -> 0x06e0 }
                    r12.<init>()     // Catch:{ all -> 0x06e0 }
                    java.lang.String r13 = "ETag"
                    r25 = r8
                    java.lang.String r8 = "ETag"
                    java.lang.String r8 = r9.get(r8)     // Catch:{ all -> 0x06d9 }
                    r12.put(r13, r8)     // Catch:{ all -> 0x06d9 }
                    java.lang.String r8 = "Last-Modified"
                    java.lang.String r13 = "Last-Modified"
                    java.lang.String r13 = r9.get(r13)     // Catch:{ all -> 0x06d9 }
                    r12.put(r8, r13)     // Catch:{ all -> 0x06d9 }
                    java.lang.String r8 = "Accept-Ranges"
                    java.lang.String r13 = "Accept-Ranges"
                    java.lang.String r13 = r9.get(r13)     // Catch:{ all -> 0x06d9 }
                    r12.put(r8, r13)     // Catch:{ all -> 0x06d9 }
                    java.lang.String r8 = "Content-Encoding"
                    java.lang.String r13 = "Content-Encoding"
                    java.lang.String r9 = r9.get(r13)     // Catch:{ all -> 0x06d9 }
                    r12.put(r8, r9)     // Catch:{ all -> 0x06d9 }
                    java.lang.String r8 = r10.getPath()     // Catch:{ all -> 0x06d9 }
                    com.vungle.warren.utility.FileUtility.writeMap(r8, r12)     // Catch:{ all -> 0x06d9 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06d9 }
                    okhttp3.ResponseBody r8 = r8.decodeGzipIfNeeded(r5)     // Catch:{ all -> 0x06d9 }
                    if (r8 == 0) goto L_0x06c7
                    okio.BufferedSource r10 = r8.source()     // Catch:{ all -> 0x06d9 }
                    java.lang.String r9 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x06bc }
                    java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x06bc }
                    r12.<init>()     // Catch:{ all -> 0x06bc }
                    java.lang.String r13 = "Start download from bytes: "
                    r12.append(r13)     // Catch:{ all -> 0x06bc }
                    r12.append(r14)     // Catch:{ all -> 0x06bc }
                    com.vungle.warren.downloader.AssetDownloader r13 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06bc }
                    r16 = r11
                    com.vungle.warren.downloader.DownloadRequest r11 = r4     // Catch:{ all -> 0x06b4 }
                    java.lang.String r11 = r13.debugString(r11)     // Catch:{ all -> 0x06b4 }
                    r12.append(r11)     // Catch:{ all -> 0x06b4 }
                    java.lang.String r11 = r12.toString()     // Catch:{ all -> 0x06b4 }
                    android.util.Log.d(r9, r11)     // Catch:{ all -> 0x06b4 }
                    long r23 = r23 + r14
                    java.lang.String r9 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x06b4 }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x06b4 }
                    r11.<init>()     // Catch:{ all -> 0x06b4 }
                    java.lang.String r12 = "final offset = "
                    r11.append(r12)     // Catch:{ all -> 0x06b4 }
                    r11.append(r14)     // Catch:{ all -> 0x06b4 }
                    java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x06b4 }
                    android.util.Log.d(r9, r11)     // Catch:{ all -> 0x06b4 }
                    int r9 = (r14 > r19 ? 1 : (r14 == r19 ? 0 : -1))
                    if (r9 != 0) goto L_0x0514
                    okio.Sink r9 = okio.Okio.sink(r4)     // Catch:{ all -> 0x050a }
                    goto L_0x0518
                L_0x050a:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r11 = r16
                    r8 = r25
                    goto L_0x0361
                L_0x0514:
                    okio.Sink r9 = okio.Okio.appendingSink(r4)     // Catch:{ all -> 0x06b4 }
                L_0x0518:
                    okio.BufferedSink r11 = okio.Okio.buffer(r9)     // Catch:{ all -> 0x06b4 }
                    r9 = 0
                    r2.status = r9     // Catch:{ all -> 0x06a8 }
                    long r8 = r8.contentLength()     // Catch:{ all -> 0x06a8 }
                    r2.sizeBytes = r8     // Catch:{ all -> 0x06a8 }
                    r2.startBytes = r14     // Catch:{ all -> 0x06a8 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a8 }
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x06a8 }
                    java.lang.String r9 = r9.id     // Catch:{ all -> 0x06a8 }
                    r8.deliverProgress(r9, r2)     // Catch:{ all -> 0x06a8 }
                    r8 = r19
                    r12 = 0
                L_0x0533:
                    com.vungle.warren.downloader.DownloadRequest r13 = r4     // Catch:{ all -> 0x06a8 }
                    r26 = r12
                    r12 = 1
                    boolean r13 = r13.is(r12)     // Catch:{ all -> 0x06a8 }
                    if (r13 == 0) goto L_0x0598
                    okio.Buffer r12 = r11.buffer()     // Catch:{ all -> 0x06a8 }
                    r13 = r6
                    r27 = r7
                    r6 = 2048(0x800, double:1.0118E-320)
                    long r6 = r10.read(r12, r6)     // Catch:{ all -> 0x06a6 }
                    r28 = -1
                    int r12 = (r6 > r28 ? 1 : (r6 == r28 ? 0 : -1))
                    if (r12 == 0) goto L_0x059b
                    r11.emit()     // Catch:{ all -> 0x06a6 }
                    long r8 = r8 + r6
                    long r6 = r14 + r8
                    int r12 = (r23 > r19 ? 1 : (r23 == r19 ? 0 : -1))
                    if (r12 <= 0) goto L_0x0564
                    r28 = 100
                    long r6 = r6 * r28
                    long r6 = r6 / r23
                    int r7 = (int) r6     // Catch:{ all -> 0x06a6 }
                    r12 = r7
                    goto L_0x0566
                L_0x0564:
                    r12 = r26
                L_0x0566:
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x06a6 }
                    boolean r6 = r6.isConnected()     // Catch:{ all -> 0x06a6 }
                    if (r6 == 0) goto L_0x0590
                    int r6 = r2.progressPercent     // Catch:{ all -> 0x06a6 }
                    if (r12 > r6) goto L_0x0573
                    goto L_0x058c
                L_0x0573:
                    r2.progressPercent = r12     // Catch:{ all -> 0x06a6 }
                    int r6 = r2.progressPercent     // Catch:{ all -> 0x06a6 }
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a6 }
                    int r7 = r7.progressStep     // Catch:{ all -> 0x06a6 }
                    int r6 = r6 % r7
                    if (r6 != 0) goto L_0x058c
                    r6 = 1
                    r2.status = r6     // Catch:{ all -> 0x06a6 }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a6 }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x06a6 }
                    java.lang.String r7 = r7.id     // Catch:{ all -> 0x06a6 }
                    r6.deliverProgress(r7, r2)     // Catch:{ all -> 0x06a6 }
                L_0x058c:
                    r6 = r13
                    r7 = r27
                    goto L_0x0533
                L_0x0590:
                    java.io.IOException r6 = new java.io.IOException     // Catch:{ all -> 0x06a6 }
                    java.lang.String r7 = "Request is not connected"
                    r6.<init>(r7)     // Catch:{ all -> 0x06a6 }
                    throw r6     // Catch:{ all -> 0x06a6 }
                L_0x0598:
                    r13 = r6
                    r27 = r7
                L_0x059b:
                    r11.flush()     // Catch:{ all -> 0x06a6 }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x06a6 }
                    r7 = 1
                    boolean r6 = r6.is(r7)     // Catch:{ all -> 0x06a6 }
                    if (r6 == 0) goto L_0x05ae
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x06a6 }
                    r7 = 4
                    r6.set(r7)     // Catch:{ all -> 0x06a6 }
                    goto L_0x05da
                L_0x05ae:
                    r6 = 6
                    r2.status = r6     // Catch:{ all -> 0x06a6 }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a6 }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x06a6 }
                    java.lang.String r7 = r7.id     // Catch:{ all -> 0x06a6 }
                    r6.deliverProgress(r7, r2)     // Catch:{ all -> 0x06a6 }
                    java.lang.String r6 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x06a6 }
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x06a6 }
                    r7.<init>()     // Catch:{ all -> 0x06a6 }
                    java.lang.String r8 = "State has changed, cancelling download "
                    r7.append(r8)     // Catch:{ all -> 0x06a6 }
                    com.vungle.warren.downloader.AssetDownloader r8 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a6 }
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x06a6 }
                    java.lang.String r8 = r8.debugString(r9)     // Catch:{ all -> 0x06a6 }
                    r7.append(r8)     // Catch:{ all -> 0x06a6 }
                    java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x06a6 }
                    android.util.Log.d(r6, r7)     // Catch:{ all -> 0x06a6 }
                L_0x05da:
                    if (r5 == 0) goto L_0x05e9
                    okhttp3.ResponseBody r6 = r5.body()
                    if (r6 == 0) goto L_0x05e9
                    okhttp3.ResponseBody r5 = r5.body()
                    r5.close()
                L_0x05e9:
                    if (r3 == 0) goto L_0x05ee
                    r3.cancel()
                L_0x05ee:
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r6 = "request is done "
                    r5.append(r6)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r7 = r4
                    java.lang.String r6 = r6.debugString(r7)
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r3, r5)
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r6)
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ all -> 0x06a2 }
                    int r3 = r3.getStatus()     // Catch:{ all -> 0x06a2 }
                    r5 = 2
                    if (r3 == r5) goto L_0x0657
                    r5 = 3
                    if (r3 == r5) goto L_0x0634
                    r5 = 4
                    if (r3 == r5) goto L_0x062c
                    r5 = 5
                    if (r3 == r5) goto L_0x0624
                    goto L_0x063b
                L_0x0624:
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x06a2 }
                    r3.onError(r13, r4)     // Catch:{ all -> 0x06a2 }
                    goto L_0x0657
                L_0x062c:
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x06a2 }
                    r3.onSuccess(r4, r5)     // Catch:{ all -> 0x06a2 }
                    goto L_0x0657
                L_0x0634:
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x06a2 }
                    r3.onCancelled(r4, r2)     // Catch:{ all -> 0x06a2 }
                L_0x063b:
                    if (r27 != 0) goto L_0x0657
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a2 }
                    java.util.concurrent.ConcurrentHashMap r3 = r3.connections     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x06a2 }
                    java.lang.String r4 = r4.id     // Catch:{ all -> 0x06a2 }
                    r3.remove(r4)     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a2 }
                    java.util.concurrent.ConcurrentHashMap r3 = r3.listeners     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x06a2 }
                    java.lang.String r4 = r4.id     // Catch:{ all -> 0x06a2 }
                    r3.remove(r4)     // Catch:{ all -> 0x06a2 }
                L_0x0657:
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x06a2 }
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x06a2 }
                    r4.<init>()     // Catch:{ all -> 0x06a2 }
                    java.lang.String r5 = "Removing connections and listener "
                    r4.append(r5)     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x06a2 }
                    java.lang.String r5 = r5.debugString(r7)     // Catch:{ all -> 0x06a2 }
                    r4.append(r5)     // Catch:{ all -> 0x06a2 }
                    java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x06a2 }
                    android.util.Log.d(r3, r4)     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a2 }
                    java.util.concurrent.ConcurrentHashMap r3 = r3.connections     // Catch:{ all -> 0x06a2 }
                    boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x06a2 }
                    if (r3 == 0) goto L_0x0692
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.utility.NetworkProvider r3 = r3.networkProvider     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r4 = r4.networkListener     // Catch:{ all -> 0x06a2 }
                    r3.removeListener(r4)     // Catch:{ all -> 0x06a2 }
                L_0x0692:
                    monitor-exit(r6)     // Catch:{ all -> 0x06a2 }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r11)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r10)
                    r6 = r13
                    r8 = r25
                    r7 = r27
                    r5 = 1
                    r15 = 0
                    goto L_0x095a
                L_0x06a2:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r6)     // Catch:{ all -> 0x06a2 }
                    throw r2
                L_0x06a6:
                    r0 = move-exception
                    goto L_0x06ac
                L_0x06a8:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                L_0x06ac:
                    r6 = r4
                    r12 = r11
                    r11 = r16
                    r8 = r25
                    goto L_0x0731
                L_0x06b4:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r6 = r4
                    r11 = r16
                    goto L_0x06c3
                L_0x06bc:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r16 = r11
                    r6 = r4
                L_0x06c3:
                    r8 = r25
                    goto L_0x0730
                L_0x06c7:
                    r13 = r6
                    r27 = r7
                    r16 = r11
                    java.io.IOException r6 = new java.io.IOException     // Catch:{ all -> 0x06d4 }
                    java.lang.String r7 = "Response body is null"
                    r6.<init>(r7)     // Catch:{ all -> 0x06d4 }
                    throw r6     // Catch:{ all -> 0x06d4 }
                L_0x06d4:
                    r0 = move-exception
                    r6 = r4
                    r11 = r16
                    goto L_0x071a
                L_0x06d9:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r16 = r11
                    goto L_0x0719
                L_0x06e0:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r25 = r8
                    r16 = r11
                    r6 = r4
                    goto L_0x071c
                L_0x06ea:
                    r13 = r6
                    r27 = r7
                    r25 = r8
                    r16 = r11
                    com.vungle.warren.downloader.AssetDownloader$RequestException r6 = new com.vungle.warren.downloader.AssetDownloader$RequestException     // Catch:{ all -> 0x070c }
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x070c }
                    r7.<init>()     // Catch:{ all -> 0x070c }
                    java.lang.String r8 = "Code: "
                    r7.append(r8)     // Catch:{ all -> 0x070c }
                    r8 = r16
                    r7.append(r8)     // Catch:{ all -> 0x070a }
                    java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x070a }
                    r6.<init>(r7)     // Catch:{ all -> 0x070a }
                    throw r6     // Catch:{ all -> 0x070a }
                L_0x070a:
                    r0 = move-exception
                    goto L_0x070f
                L_0x070c:
                    r0 = move-exception
                    r8 = r16
                L_0x070f:
                    r6 = r4
                    r11 = r8
                    goto L_0x071a
                L_0x0712:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r25 = r8
                    r8 = r11
                L_0x0719:
                    r6 = r4
                L_0x071a:
                    r8 = r25
                L_0x071c:
                    r10 = 0
                    goto L_0x0730
                L_0x071e:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r25 = r8
                    r6 = r4
                    goto L_0x072e
                L_0x0726:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r25 = r8
                    r6 = r4
                    r5 = 0
                L_0x072e:
                    r10 = 0
                    r11 = -1
                L_0x0730:
                    r12 = 0
                L_0x0731:
                    r4 = r3
                    r3 = r0
                    goto L_0x0767
                L_0x0734:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r25 = r8
                    r3 = r0
                    r6 = r4
                    r4 = 0
                    r5 = 0
                    goto L_0x0764
                L_0x073f:
                    r13 = r6
                    r27 = r7
                    r25 = r8
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0755 }
                    java.lang.String r4 = "Request is not connected to reuired network"
                    android.util.Log.d(r3, r4)     // Catch:{ all -> 0x0755 }
                    java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x0755 }
                    java.lang.String r4 = "Not connected to correct network"
                    r3.<init>(r4)     // Catch:{ all -> 0x0755 }
                    throw r3     // Catch:{ all -> 0x0755 }
                L_0x0755:
                    r0 = move-exception
                    r3 = r0
                    r8 = r25
                    goto L_0x0761
                L_0x075a:
                    r0 = move-exception
                    r13 = r6
                    r27 = r7
                    r25 = r8
                    r3 = r0
                L_0x0761:
                    r4 = 0
                    r5 = 0
                    r6 = 0
                L_0x0764:
                    r10 = 0
                    r11 = -1
                    r12 = 0
                L_0x0767:
                    java.lang.String r7 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0961 }
                    java.lang.String r14 = "Exception on download"
                    android.util.Log.e(r7, r14, r3)     // Catch:{ all -> 0x0961 }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x0961 }
                    r9 = 5
                    r7.set(r9)     // Catch:{ all -> 0x0961 }
                    boolean r7 = r3 instanceof java.io.IOException     // Catch:{ all -> 0x0961 }
                    if (r7 == 0) goto L_0x0861
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x085e }
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ all -> 0x085e }
                    boolean r7 = r7.isConnected(r14)     // Catch:{ all -> 0x085e }
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ all -> 0x085e }
                    r14.setConnected(r7)     // Catch:{ all -> 0x085e }
                    if (r7 != 0) goto L_0x080e
                    r9 = 5
                    r2.status = r9     // Catch:{ all -> 0x085e }
                    com.vungle.warren.downloader.AssetDownloader r14 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x085e }
                    com.vungle.warren.downloader.DownloadRequest r15 = r4     // Catch:{ all -> 0x085e }
                    java.lang.String r15 = r15.id     // Catch:{ all -> 0x085e }
                    r14.deliverProgress(r15, r2)     // Catch:{ all -> 0x085e }
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ all -> 0x085e }
                    r15 = 3
                    boolean r14 = r14.is(r15)     // Catch:{ all -> 0x085e }
                    if (r14 != 0) goto L_0x080e
                    int r14 = r8 + 1
                    com.vungle.warren.downloader.AssetDownloader r15 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x085e }
                    int r15 = r15.maxReconnectAttempts     // Catch:{ all -> 0x085e }
                    if (r8 >= r15) goto L_0x080a
                    r8 = 0
                L_0x07a7:
                    com.vungle.warren.downloader.AssetDownloader r15 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x085e }
                    int r15 = r15.retryCountOnConnectionLost     // Catch:{ all -> 0x085e }
                    if (r8 >= r15) goto L_0x080a
                    com.vungle.warren.downloader.AssetDownloader r15 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ InterruptedException -> 0x07be }
                    int r15 = r15.reconnectTimeout     // Catch:{ InterruptedException -> 0x07be }
                    r9 = 0
                    int r15 = java.lang.Math.max(r9, r15)     // Catch:{ InterruptedException -> 0x07be }
                    r9 = r14
                    long r14 = (long) r15
                    java.lang.Thread.sleep(r14)     // Catch:{ InterruptedException -> 0x07bc }
                    goto L_0x07c4
                L_0x07bc:
                    r0 = move-exception
                    goto L_0x07c0
                L_0x07be:
                    r0 = move-exception
                    r9 = r14
                L_0x07c0:
                    r14 = r0
                    r14.printStackTrace()     // Catch:{ all -> 0x085e }
                L_0x07c4:
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ all -> 0x085e }
                    r15 = 3
                    boolean r14 = r14.is(r15)     // Catch:{ all -> 0x085e }
                    if (r14 == 0) goto L_0x07ce
                    goto L_0x080b
                L_0x07ce:
                    java.lang.String r14 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x085e }
                    java.lang.String r15 = "Trying to reconnect"
                    android.util.Log.d(r14, r15)     // Catch:{ all -> 0x085e }
                    com.vungle.warren.downloader.AssetDownloader r14 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x085e }
                    com.vungle.warren.downloader.DownloadRequest r15 = r4     // Catch:{ all -> 0x085e }
                    boolean r14 = r14.isConnected(r15)     // Catch:{ all -> 0x085e }
                    if (r14 == 0) goto L_0x0800
                    java.lang.String r8 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x085e }
                    java.lang.String r14 = "Reconnected, starting download again"
                    android.util.Log.d(r8, r14)     // Catch:{ all -> 0x085e }
                    com.vungle.warren.downloader.DownloadRequest r8 = r4     // Catch:{ all -> 0x07fa }
                    r14 = 1
                    r8.setConnected(r14)     // Catch:{ all -> 0x07fa }
                    com.vungle.warren.downloader.DownloadRequest r8 = r4     // Catch:{ all -> 0x07fa }
                    r8.set(r14)     // Catch:{ all -> 0x07fa }
                    r8 = r9
                    r15 = 0
                    r17 = 0
                    goto L_0x0811
                L_0x07fa:
                    r0 = move-exception
                    r3 = r0
                    r18 = 0
                    goto L_0x0966
                L_0x0800:
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ all -> 0x085e }
                    r15 = 0
                    r14.setConnected(r15)     // Catch:{ all -> 0x085e }
                    int r8 = r8 + 1
                    r14 = r9
                    goto L_0x07a7
                L_0x080a:
                    r9 = r14
                L_0x080b:
                    r15 = 0
                    r8 = r9
                    goto L_0x080f
                L_0x080e:
                    r15 = 0
                L_0x080f:
                    r17 = 1
                L_0x0811:
                    if (r17 == 0) goto L_0x0856
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x0850 }
                    r14 = 3
                    boolean r9 = r9.is(r14)     // Catch:{ all -> 0x0850 }
                    if (r9 != 0) goto L_0x0856
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0850 }
                    com.vungle.warren.downloader.DownloadRequest r14 = r4     // Catch:{ all -> 0x0850 }
                    boolean r9 = r9.shouldPause(r14)     // Catch:{ all -> 0x0850 }
                    if (r9 == 0) goto L_0x083d
                    com.vungle.warren.downloader.DownloadRequest r3 = r4     // Catch:{ all -> 0x0835 }
                    r7 = 2
                    r3.set(r7)     // Catch:{ all -> 0x0835 }
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0835 }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x0835 }
                    r3.onPaused(r2, r7)     // Catch:{ all -> 0x0835 }
                    r7 = 1
                    goto L_0x0858
                L_0x0835:
                    r0 = move-exception
                    r3 = r0
                    r18 = r17
                    r27 = 1
                    goto L_0x0966
                L_0x083d:
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x0850 }
                    r14 = 5
                    r9.set(r14)     // Catch:{ all -> 0x0850 }
                    com.vungle.warren.downloader.AssetDownloadListener$DownloadError r14 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x0850 }
                    com.vungle.warren.downloader.AssetDownloader r9 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0850 }
                    int r7 = r9.mapExceptionToReason(r3, r7)     // Catch:{ all -> 0x0850 }
                    r14.<init>(r11, r3, r7)     // Catch:{ all -> 0x0850 }
                    r13 = r14
                    goto L_0x0856
                L_0x0850:
                    r0 = move-exception
                    r3 = r0
                    r18 = r17
                    goto L_0x0966
                L_0x0856:
                    r7 = r27
                L_0x0858:
                    r27 = r7
                    r7 = r13
                    r14 = r17
                    goto L_0x0874
                L_0x085e:
                    r0 = move-exception
                    goto L_0x0963
                L_0x0861:
                    r15 = 0
                    boolean r7 = r3 instanceof com.vungle.warren.downloader.AssetDownloader.RequestException     // Catch:{ all -> 0x0961 }
                    if (r7 == 0) goto L_0x086d
                    com.vungle.warren.downloader.AssetDownloadListener$DownloadError r7 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x0961 }
                    r14 = 1
                    r7.<init>(r11, r3, r14)     // Catch:{ all -> 0x085e }
                    goto L_0x0874
                L_0x086d:
                    r14 = 1
                    com.vungle.warren.downloader.AssetDownloadListener$DownloadError r7 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x085e }
                    r9 = 4
                    r7.<init>(r11, r3, r9)     // Catch:{ all -> 0x085e }
                L_0x0874:
                    if (r5 == 0) goto L_0x0883
                    okhttp3.ResponseBody r3 = r5.body()
                    if (r3 == 0) goto L_0x0883
                    okhttp3.ResponseBody r3 = r5.body()
                    r3.close()
                L_0x0883:
                    if (r4 == 0) goto L_0x0888
                    r4.cancel()
                L_0x0888:
                    java.lang.String r3 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "request is done "
                    r4.append(r5)
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r9 = r4
                    java.lang.String r5 = r5.debugString(r9)
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    android.util.Log.d(r3, r4)
                    com.vungle.warren.downloader.AssetDownloader r3 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r3)
                    if (r14 == 0) goto L_0x0914
                    com.vungle.warren.downloader.DownloadRequest r4 = r4     // Catch:{ all -> 0x095d }
                    int r4 = r4.getStatus()     // Catch:{ all -> 0x095d }
                    r5 = 2
                    if (r4 == r5) goto L_0x08f3
                    r5 = 3
                    if (r4 == r5) goto L_0x08d0
                    r5 = 4
                    if (r4 == r5) goto L_0x08c8
                    r5 = 5
                    if (r4 == r5) goto L_0x08c0
                    goto L_0x08d7
                L_0x08c0:
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x095d }
                    r4.onError(r7, r5)     // Catch:{ all -> 0x095d }
                    goto L_0x08f3
                L_0x08c8:
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x095d }
                    r4.onSuccess(r6, r5)     // Catch:{ all -> 0x095d }
                    goto L_0x08f3
                L_0x08d0:
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x095d }
                    r4.onCancelled(r5, r2)     // Catch:{ all -> 0x095d }
                L_0x08d7:
                    if (r27 != 0) goto L_0x08f3
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    java.util.concurrent.ConcurrentHashMap r4 = r4.connections     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x095d }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x095d }
                    r4.remove(r5)     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    java.util.concurrent.ConcurrentHashMap r4 = r4.listeners     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x095d }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x095d }
                    r4.remove(r5)     // Catch:{ all -> 0x095d }
                L_0x08f3:
                    java.lang.String r4 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x095d }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x095d }
                    r5.<init>()     // Catch:{ all -> 0x095d }
                    java.lang.String r6 = "Removing connections and listener "
                    r5.append(r6)     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x095d }
                    java.lang.String r6 = r6.debugString(r9)     // Catch:{ all -> 0x095d }
                    r5.append(r6)     // Catch:{ all -> 0x095d }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x095d }
                    android.util.Log.d(r4, r5)     // Catch:{ all -> 0x095d }
                    goto L_0x0934
                L_0x0914:
                    java.lang.String r4 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x095d }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x095d }
                    r5.<init>()     // Catch:{ all -> 0x095d }
                    java.lang.String r6 = "Not removing connections and listener "
                    r5.append(r6)     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.DownloadRequest r9 = r4     // Catch:{ all -> 0x095d }
                    java.lang.String r6 = r6.debugString(r9)     // Catch:{ all -> 0x095d }
                    r5.append(r6)     // Catch:{ all -> 0x095d }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x095d }
                    android.util.Log.d(r4, r5)     // Catch:{ all -> 0x095d }
                L_0x0934:
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    java.util.concurrent.ConcurrentHashMap r4 = r4.connections     // Catch:{ all -> 0x095d }
                    boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x095d }
                    if (r4 == 0) goto L_0x094f
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    com.vungle.warren.utility.NetworkProvider r4 = r4.networkProvider     // Catch:{ all -> 0x095d }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x095d }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r5 = r5.networkListener     // Catch:{ all -> 0x095d }
                    r4.removeListener(r5)     // Catch:{ all -> 0x095d }
                L_0x094f:
                    monitor-exit(r3)     // Catch:{ all -> 0x095d }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r12)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r10)
                    r6 = r7
                    r5 = r14
                    r7 = r27
                L_0x095a:
                    r3 = 0
                    goto L_0x0012
                L_0x095d:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r3)     // Catch:{ all -> 0x095d }
                    throw r2
                L_0x0961:
                    r0 = move-exception
                    r14 = 1
                L_0x0963:
                    r3 = r0
                    r18 = 1
                L_0x0966:
                    if (r5 == 0) goto L_0x0975
                    okhttp3.ResponseBody r7 = r5.body()
                    if (r7 == 0) goto L_0x0975
                    okhttp3.ResponseBody r5 = r5.body()
                    r5.close()
                L_0x0975:
                    if (r4 == 0) goto L_0x097a
                    r4.cancel()
                L_0x097a:
                    java.lang.String r4 = com.vungle.warren.downloader.AssetDownloader.TAG
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r7 = "request is done "
                    r5.append(r7)
                    com.vungle.warren.downloader.AssetDownloader r7 = com.vungle.warren.downloader.AssetDownloader.this
                    com.vungle.warren.downloader.DownloadRequest r8 = r4
                    java.lang.String r7 = r7.debugString(r8)
                    r5.append(r7)
                    java.lang.String r5 = r5.toString()
                    android.util.Log.d(r4, r5)
                    com.vungle.warren.downloader.AssetDownloader r4 = com.vungle.warren.downloader.AssetDownloader.this
                    monitor-enter(r4)
                    if (r18 == 0) goto L_0x0a06
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a49 }
                    int r5 = r5.getStatus()     // Catch:{ all -> 0x0a49 }
                    r7 = 2
                    if (r5 == r7) goto L_0x09e5
                    r7 = 3
                    if (r5 == r7) goto L_0x09c2
                    r7 = 4
                    if (r5 == r7) goto L_0x09ba
                    r2 = 5
                    if (r5 == r2) goto L_0x09b2
                    goto L_0x09c9
                L_0x09b2:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a49 }
                    r2.onError(r13, r5)     // Catch:{ all -> 0x0a49 }
                    goto L_0x09e5
                L_0x09ba:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a49 }
                    r2.onSuccess(r6, r5)     // Catch:{ all -> 0x0a49 }
                    goto L_0x09e5
                L_0x09c2:
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.DownloadRequest r6 = r4     // Catch:{ all -> 0x0a49 }
                    r5.onCancelled(r6, r2)     // Catch:{ all -> 0x0a49 }
                L_0x09c9:
                    if (r27 != 0) goto L_0x09e5
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a49 }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x0a49 }
                    r2.remove(r5)     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.listeners     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.DownloadRequest r5 = r4     // Catch:{ all -> 0x0a49 }
                    java.lang.String r5 = r5.id     // Catch:{ all -> 0x0a49 }
                    r2.remove(r5)     // Catch:{ all -> 0x0a49 }
                L_0x09e5:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0a49 }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0a49 }
                    r5.<init>()     // Catch:{ all -> 0x0a49 }
                    java.lang.String r6 = "Removing connections and listener "
                    r5.append(r6)     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x0a49 }
                    java.lang.String r6 = r6.debugString(r7)     // Catch:{ all -> 0x0a49 }
                    r5.append(r6)     // Catch:{ all -> 0x0a49 }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0a49 }
                    android.util.Log.d(r2, r5)     // Catch:{ all -> 0x0a49 }
                    goto L_0x0a26
                L_0x0a06:
                    java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0a49 }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0a49 }
                    r5.<init>()     // Catch:{ all -> 0x0a49 }
                    java.lang.String r6 = "Not removing connections and listener "
                    r5.append(r6)     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.AssetDownloader r6 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.DownloadRequest r7 = r4     // Catch:{ all -> 0x0a49 }
                    java.lang.String r6 = r6.debugString(r7)     // Catch:{ all -> 0x0a49 }
                    r5.append(r6)     // Catch:{ all -> 0x0a49 }
                    java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0a49 }
                    android.util.Log.d(r2, r5)     // Catch:{ all -> 0x0a49 }
                L_0x0a26:
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    java.util.concurrent.ConcurrentHashMap r2 = r2.connections     // Catch:{ all -> 0x0a49 }
                    boolean r2 = r2.isEmpty()     // Catch:{ all -> 0x0a49 }
                    if (r2 == 0) goto L_0x0a41
                    com.vungle.warren.downloader.AssetDownloader r2 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.utility.NetworkProvider r2 = r2.networkProvider     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.downloader.AssetDownloader r5 = com.vungle.warren.downloader.AssetDownloader.this     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.utility.NetworkProvider$NetworkListener r5 = r5.networkListener     // Catch:{ all -> 0x0a49 }
                    r2.removeListener(r5)     // Catch:{ all -> 0x0a49 }
                L_0x0a41:
                    monitor-exit(r4)     // Catch:{ all -> 0x0a49 }
                    com.vungle.warren.utility.FileUtility.closeQuietly(r12)
                    com.vungle.warren.utility.FileUtility.closeQuietly(r10)
                    throw r3
                L_0x0a49:
                    r0 = move-exception
                    r2 = r0
                    monitor-exit(r4)     // Catch:{ all -> 0x0a49 }
                    throw r2
                L_0x0a4d:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.AnonymousClass4.run():void");
            }
        });
    }

    /* access modifiers changed from: private */
    public ResponseBody decodeGzipIfNeeded(Response response) {
        if (!"gzip".equalsIgnoreCase(response.header("Content-Encoding")) || !HttpHeaders.hasBody(response)) {
            return response.body();
        }
        return new RealResponseBody(response.header("Content-Type"), -1, Okio.buffer(new GzipSource(response.body().source())));
    }

    /* access modifiers changed from: private */
    public void onCancelled(final DownloadRequest downloadRequest, final AssetDownloadListener.Progress progress) {
        this.connections.remove(downloadRequest.id);
        final AssetDownloadListener remove = this.listeners.remove(downloadRequest.id);
        if (progress == null) {
            progress = new AssetDownloadListener.Progress();
        }
        progress.status = 3;
        if (remove != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    remove.onProgress(progress, downloadRequest);
                }
            });
        }
        String str = TAG;
        Log.d(str, "Cancelled " + debugString(downloadRequest));
    }

    /* access modifiers changed from: private */
    public int mapExceptionToReason(Throwable th, boolean z) {
        if (th instanceof RuntimeException) {
            return 4;
        }
        if (!z || (th instanceof SocketException) || (th instanceof SocketTimeoutException)) {
            return 0;
        }
        return ((th instanceof UnknownHostException) || (th instanceof SSLException)) ? 1 : 2;
    }

    /* access modifiers changed from: private */
    public long getContentLength(Response response) {
        if (response == null) {
            return -1;
        }
        String str = response.headers().get("Content-Length");
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (Throwable unused) {
            return -1;
        }
    }

    /* access modifiers changed from: private */
    public boolean fullyDownloadedContent(File file, Response response, DownloadRequest downloadRequest) {
        boolean z = false;
        if (file.exists() && file.length() > 0 && !"gzip".equalsIgnoreCase(response.headers().get("Content-Encoding"))) {
            int code = response.code();
            long contentLength = getContentLength(response);
            if (code == 200 && contentLength == file.length()) {
                String str = TAG;
                Log.d(str, "200 code, data size matches file size " + debugString(downloadRequest));
                return responseVersionMatches(file, response);
            } else if (code == 416) {
                String str2 = response.headers().get("Content-Range");
                if (TextUtils.isEmpty(str2)) {
                    return false;
                }
                RangeResponse rangeResponse = new RangeResponse(str2);
                if (BYTES.equalsIgnoreCase(rangeResponse.dimension) && rangeResponse.total > 0 && rangeResponse.total == file.length() && responseVersionMatches(file, response)) {
                    z = true;
                }
                String str3 = TAG;
                Log.d(str3, "416 code, data size matches file size " + debugString(downloadRequest));
            }
        }
        return z;
    }

    /* access modifiers changed from: private */
    public synchronized void onSuccess(final File file, final DownloadRequest downloadRequest) {
        this.connections.remove(downloadRequest.id);
        final AssetDownloadListener remove = this.listeners.remove(downloadRequest.id);
        String str = TAG;
        Log.d(str, "OnComplete - Removing connections and listener " + downloadRequest.id);
        if (remove != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    remove.onSuccess(file, downloadRequest);
                }
            });
        }
        String str2 = TAG;
        Log.d(str2, "Finished " + debugString(downloadRequest));
    }

    /* access modifiers changed from: private */
    public synchronized void onPaused(AssetDownloadListener.Progress progress, DownloadRequest downloadRequest) {
        String str = TAG;
        Log.d(str, "Pausing download " + debugString(downloadRequest));
        progress.status = 2;
        deliverProgress(downloadRequest.id, progress);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0065, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onError(com.vungle.warren.downloader.AssetDownloadListener.DownloadError r6, final com.vungle.warren.downloader.DownloadRequest r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            if (r7 != 0) goto L_0x0005
            monitor-exit(r5)
            return
        L_0x0005:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.AssetDownloadListener> r0 = r5.listeners     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = r7.id     // Catch:{ all -> 0x0066 }
            java.lang.Object r0 = r0.remove(r1)     // Catch:{ all -> 0x0066 }
            com.vungle.warren.downloader.AssetDownloadListener r0 = (com.vungle.warren.downloader.AssetDownloadListener) r0     // Catch:{ all -> 0x0066 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r1 = r5.connections     // Catch:{ all -> 0x0066 }
            java.lang.String r2 = r7.id     // Catch:{ all -> 0x0066 }
            r1.remove(r2)     // Catch:{ all -> 0x0066 }
            r1 = 5
            r7.set(r1)     // Catch:{ all -> 0x0066 }
            if (r6 != 0) goto L_0x0029
            com.vungle.warren.downloader.AssetDownloadListener$DownloadError r1 = new com.vungle.warren.downloader.AssetDownloadListener$DownloadError     // Catch:{ all -> 0x0066 }
            r2 = -1
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ all -> 0x0066 }
            r3.<init>()     // Catch:{ all -> 0x0066 }
            r4 = 4
            r1.<init>(r2, r3, r4)     // Catch:{ all -> 0x0066 }
            goto L_0x002a
        L_0x0029:
            r1 = r6
        L_0x002a:
            java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0066 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0066 }
            r3.<init>()     // Catch:{ all -> 0x0066 }
            java.lang.String r4 = "OnError - Removing connections and listener "
            r3.append(r4)     // Catch:{ all -> 0x0066 }
            java.lang.String r4 = r7.id     // Catch:{ all -> 0x0066 }
            r3.append(r4)     // Catch:{ all -> 0x0066 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0066 }
            android.util.Log.d(r2, r3)     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x0064
            java.lang.String r2 = com.vungle.warren.downloader.AssetDownloader.TAG     // Catch:{ all -> 0x0066 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0066 }
            r3.<init>()     // Catch:{ all -> 0x0066 }
            java.lang.String r4 = "On download error "
            r3.append(r4)     // Catch:{ all -> 0x0066 }
            r3.append(r6)     // Catch:{ all -> 0x0066 }
            java.lang.String r6 = r3.toString()     // Catch:{ all -> 0x0066 }
            android.util.Log.e(r2, r6)     // Catch:{ all -> 0x0066 }
            java.util.concurrent.ExecutorService r6 = r5.uiExecutor     // Catch:{ all -> 0x0066 }
            com.vungle.warren.downloader.AssetDownloader$7 r2 = new com.vungle.warren.downloader.AssetDownloader$7     // Catch:{ all -> 0x0066 }
            r2.<init>(r0, r1, r7)     // Catch:{ all -> 0x0066 }
            r6.execute(r2)     // Catch:{ all -> 0x0066 }
        L_0x0064:
            monitor-exit(r5)
            return
        L_0x0066:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.onError(com.vungle.warren.downloader.AssetDownloadListener$DownloadError, com.vungle.warren.downloader.DownloadRequest):void");
    }

    /* access modifiers changed from: private */
    public boolean satisfiesPartialDownload(Response response, long j, DownloadRequest downloadRequest) {
        RangeResponse rangeResponse = new RangeResponse(response.headers().get("Content-Range"));
        boolean z = response.code() == 206 && BYTES.equalsIgnoreCase(rangeResponse.dimension) && rangeResponse.rangeStart >= 0 && j == rangeResponse.rangeStart;
        String str = TAG;
        Log.d(str, "satisfies partial download: " + z + " " + debugString(downloadRequest));
        return z;
    }

    /* access modifiers changed from: private */
    public String debugString(DownloadRequest downloadRequest) {
        return " id - " + downloadRequest.id + ", url - " + downloadRequest.url + ", path - " + downloadRequest.path + ", th - " + Thread.currentThread().getName();
    }

    /* access modifiers changed from: private */
    public boolean shouldPause(DownloadRequest downloadRequest) {
        if (!downloadRequest.pauseOnConnectionLost) {
            return false;
        }
        return !isConnected(downloadRequest);
    }

    /* access modifiers changed from: private */
    public boolean isConnected(DownloadRequest downloadRequest) {
        int i;
        int currentNetworkType = this.networkProvider.getCurrentNetworkType();
        boolean z = true;
        if (currentNetworkType >= 0 && downloadRequest.networkType == 3) {
            return true;
        }
        if (currentNetworkType != 0) {
            if (currentNetworkType != 1) {
                if (currentNetworkType != 4) {
                    if (currentNetworkType != 9) {
                        if (currentNetworkType != 17) {
                            if (currentNetworkType != 6) {
                                if (currentNetworkType != 7) {
                                    i = -1;
                                    if (i <= 0 || (downloadRequest.networkType & i) != i) {
                                        z = false;
                                    }
                                    String str = TAG;
                                    Log.d(str, "checking pause for type: " + currentNetworkType + " connected " + z + debugString(downloadRequest));
                                    return z;
                                }
                            }
                        }
                    }
                }
            }
            i = 2;
            z = false;
            String str2 = TAG;
            Log.d(str2, "checking pause for type: " + currentNetworkType + " connected " + z + debugString(downloadRequest));
            return z;
        }
        i = 1;
        z = false;
        String str22 = TAG;
        Log.d(str22, "checking pause for type: " + currentNetworkType + " connected " + z + debugString(downloadRequest));
        return z;
    }

    /* access modifiers changed from: private */
    public void deliverProgress(String str, AssetDownloadListener.Progress progress) {
        final AssetDownloadListener.Progress copy = AssetDownloadListener.Progress.copy(progress);
        String str2 = TAG;
        Log.d(str2, "Progress " + progress.progressPercent + " status " + progress.status + " " + str);
        final AssetDownloadListener assetDownloadListener = this.listeners.get(str);
        final DownloadRequest downloadRequest = this.connections.get(str);
        if (assetDownloadListener != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    assetDownloadListener.onProgress(copy, downloadRequest);
                }
            });
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void cancel(com.vungle.warren.downloader.DownloadRequest r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            if (r4 != 0) goto L_0x0005
            monitor-exit(r3)
            return
        L_0x0005:
            r0 = 3
            int r1 = r4.getAndSetStatus(r0)     // Catch:{ all -> 0x0024 }
            r2 = 1
            if (r1 == r2) goto L_0x0022
            if (r1 == r0) goto L_0x0022
            r0 = 0
            r3.onCancelled(r4, r0)     // Catch:{ all -> 0x0024 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.vungle.warren.downloader.DownloadRequest> r4 = r3.connections     // Catch:{ all -> 0x0024 }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x0024 }
            if (r4 == 0) goto L_0x0022
            com.vungle.warren.utility.NetworkProvider r4 = r3.networkProvider     // Catch:{ all -> 0x0024 }
            com.vungle.warren.utility.NetworkProvider$NetworkListener r0 = r3.networkListener     // Catch:{ all -> 0x0024 }
            r4.removeListener(r0)     // Catch:{ all -> 0x0024 }
        L_0x0022:
            monitor-exit(r3)
            return
        L_0x0024:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.downloader.AssetDownloader.cancel(com.vungle.warren.downloader.DownloadRequest):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public boolean cancelAndAwait(DownloadRequest downloadRequest, long j) {
        Integer num;
        if (downloadRequest == null) {
            return true;
        }
        cancel(downloadRequest);
        String str = downloadRequest.id;
        long currentTimeMillis = System.currentTimeMillis() + Math.max(0L, j);
        while (System.currentTimeMillis() < currentTimeMillis) {
            DownloadRequest downloadRequest2 = this.connections.get(str);
            if (downloadRequest2 == null || downloadRequest2.getStatus() != 3) {
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Request is not present or status changed - finish await ");
                if (downloadRequest2 == null) {
                    num = null;
                } else {
                    num = Integer.valueOf(downloadRequest2.getStatus());
                }
                sb.append(num);
                Log.d(str2, sb.toString());
                return true;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public synchronized void cancelAll() {
        for (DownloadRequest cancel : this.connections.values()) {
            cancel(cancel);
        }
    }

    public void setProgressStep(int i) {
        if (i != 0) {
            this.progressStep = i;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onNetworkChanged(int i) {
        String str = TAG;
        Log.d(str, "Num of connections: " + this.connections.values().size());
        for (DownloadRequest next : this.connections.values()) {
            if (next.is(3)) {
                Log.d(TAG, "Result cancelled");
            } else {
                boolean isConnected = isConnected(next);
                String str2 = TAG;
                Log.d(str2, "Connected = " + isConnected + " for " + i);
                next.setConnected(isConnected);
                if (next.pauseOnConnectionLost && isConnected && next.is(2)) {
                    load(next);
                    String str3 = TAG;
                    Log.d(str3, "resumed " + next.id);
                }
            }
        }
    }

    private boolean responseVersionMatches(File file, Response response) {
        Map<String, String> readMap = FileUtility.readMap(new File(file.getPath() + META_POSTFIX_EXT).getPath());
        Headers headers = response.headers();
        String str = headers.get("ETag");
        String str2 = headers.get("Last-Modified");
        String str3 = TAG;
        Log.d(str3, "server etag: " + str);
        String str4 = TAG;
        Log.d(str4, "server lastModified: " + str2);
        if (str != null && !str.equals(readMap.get("ETag"))) {
            String str5 = TAG;
            Log.d(str5, "etags miss match current: " + readMap.get("ETag"));
            return false;
        } else if (str2 == null || str2.equals(readMap.get("Last-Modified"))) {
            return true;
        } else {
            String str6 = TAG;
            Log.d(str6, "lastModified miss match current: " + readMap.get("Last-Modified"));
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void shutdown() {
        cancel(null);
        this.listeners.clear();
        this.connections.clear();
        this.uiExecutor.shutdownNow();
        this.downloadExecutor.shutdownNow();
        try {
            this.downloadExecutor.awaitTermination(2, TimeUnit.SECONDS);
            this.uiExecutor.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return;
    }

    private static abstract class DownloadPriorityRunnable extends PriorityRunnable {
        private final int priority;

        private DownloadPriorityRunnable(int i) {
            this.priority = i;
        }

        public Integer getPriority() {
            return Integer.valueOf(this.priority);
        }
    }

    private static class RequestException extends Exception {
        RequestException(String str) {
            super(str);
        }
    }
}
