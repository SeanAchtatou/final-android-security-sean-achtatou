package com.vungle.warren.model;

import android.text.TextUtils;
import android.util.Pair;
import com.facebook.internal.ServerProtocol;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.vungle.warren.AdConfig;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import okhttp3.HttpUrl;

public class Advertisement {
    public static final int DONE = 3;
    public static final int ERROR = 4;
    public static final String FILE_SCHEME = "file://";
    public static final String KEY_POSTROLL = "postroll";
    public static final String KEY_TEMPLATE = "template";
    public static final String KEY_VIDEO = "video";
    public static final int LANDSCAPE = 1;
    public static final int NEW = 0;
    public static final int PORTRAIT = 0;
    public static final int READY = 1;
    public static final int ROTATE = 2;
    public static final String START_MUTED = "START_MUTED";
    private static final String TAG = "Advertisement";
    public static final int TYPE_VUNGLE_LOCAL = 0;
    public static final int TYPE_VUNGLE_MRAID = 1;
    public static final int VIEWING = 2;
    AdConfig adConfig;
    String adMarketId;
    String adToken;
    int adType;
    String appID;
    String bidToken;
    Map<String, Pair<String, String>> cacheableAssets = new HashMap();
    String campaign;
    ArrayList<Checkpoint> checkpoints;
    String[] clickUrls;
    String[] closeUrls;
    int countdown;
    int ctaClickArea = -1;
    String ctaDestinationUrl;
    boolean ctaOverlayEnabled;
    String ctaUrl;
    int delay;
    boolean enableMoat;
    long expireTime;
    String identifier;
    String md5;
    String moatExtraVast;
    Map<String, String> mraidFiles = new HashMap();
    String[] muteUrls;
    String placementId;
    String[] postRollClickUrls;
    String[] postRollViewUrls;
    String postrollBundleUrl;
    boolean requiresNonMarketInstall;
    int retryCount;
    int showCloseDelay;
    int showCloseIncentivized;
    int state = 0;
    String templateId;
    Map<String, String> templateSettings;
    String templateType;
    String templateUrl;
    String[] unmuteUrls;
    String[] videoClickUrls;
    int videoHeight;
    String videoIdentifier;
    String videoUrl;
    int videoWidth;

    public @interface AdType {
    }

    public @interface CacheKey {
    }

    public @interface Orientation {
    }

    public @interface State {
    }

    Advertisement() {
    }

    public String getPlacementId() {
        return this.placementId;
    }

    public void setPlacementId(String str) {
        this.placementId = str;
    }

    public boolean isCtaOverlayEnabled() {
        return this.ctaOverlayEnabled;
    }

    public boolean getCtaClickArea() {
        int i = this.ctaClickArea;
        return i > 0 || i == -1;
    }

    public boolean isRequiresNonMarketInstall() {
        return this.requiresNonMarketInstall;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:297:0x073d  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01df  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0219  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x021c  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0226  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Advertisement(com.google.gson.JsonObject r18) throws java.lang.IllegalArgumentException {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r17.<init>()
            r2 = -1
            r0.ctaClickArea = r2
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            r0.mraidFiles = r3
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            r0.cacheableAssets = r3
            r3 = 0
            r0.state = r3
            java.lang.String r4 = "ad_markup"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r5 == 0) goto L_0x074d
            com.google.gson.JsonObject r1 = r1.getAsJsonObject(r4)
            java.lang.String r4 = "adType"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r5 == 0) goto L_0x0745
            com.google.gson.JsonElement r4 = r1.get(r4)
            java.lang.String r4 = r4.getAsString()
            int r5 = r4.hashCode()
            r6 = -1852456483(0xffffffff9195c1dd, float:-2.3627532E-28)
            r7 = 1
            if (r5 == r6) goto L_0x0051
            r6 = -1851445271(0xffffffff91a52fe9, float:-2.6061937E-28)
            if (r5 == r6) goto L_0x0047
            goto L_0x005b
        L_0x0047:
            java.lang.String r5 = "vungle_mraid"
            boolean r5 = r4.equals(r5)
            if (r5 == 0) goto L_0x005b
            r5 = 1
            goto L_0x005c
        L_0x0051:
            java.lang.String r5 = "vungle_local"
            boolean r5 = r4.equals(r5)
            if (r5 == 0) goto L_0x005b
            r5 = 0
            goto L_0x005c
        L_0x005b:
            r5 = -1
        L_0x005c:
            r6 = 0
            java.lang.String r8 = "url"
            java.lang.String r9 = ""
            if (r5 == 0) goto L_0x01df
            if (r5 != r7) goto L_0x01c3
            r0.adType = r7
            r0.postrollBundleUrl = r9
            java.lang.String r4 = "templateSettings"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r5 == 0) goto L_0x01bb
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            r0.templateSettings = r5
            com.google.gson.JsonObject r4 = r1.getAsJsonObject(r4)
            java.lang.String r5 = "normal_replacements"
            boolean r10 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r5)
            if (r10 == 0) goto L_0x00d2
            com.google.gson.JsonObject r5 = r4.getAsJsonObject(r5)
            java.util.Set r5 = r5.entrySet()
            java.util.Iterator r5 = r5.iterator()
        L_0x0090:
            boolean r10 = r5.hasNext()
            if (r10 == 0) goto L_0x00d2
            java.lang.Object r10 = r5.next()
            java.util.Map$Entry r10 = (java.util.Map.Entry) r10
            java.lang.Object r11 = r10.getKey()
            java.lang.CharSequence r11 = (java.lang.CharSequence) r11
            boolean r11 = android.text.TextUtils.isEmpty(r11)
            if (r11 == 0) goto L_0x00a9
            goto L_0x0090
        L_0x00a9:
            java.lang.Object r11 = r10.getValue()
            if (r11 == 0) goto L_0x00c7
            java.lang.Object r11 = r10.getValue()
            com.google.gson.JsonElement r11 = (com.google.gson.JsonElement) r11
            boolean r11 = r11.isJsonNull()
            if (r11 == 0) goto L_0x00bc
            goto L_0x00c7
        L_0x00bc:
            java.lang.Object r11 = r10.getValue()
            com.google.gson.JsonElement r11 = (com.google.gson.JsonElement) r11
            java.lang.String r11 = r11.getAsString()
            goto L_0x00c8
        L_0x00c7:
            r11 = r6
        L_0x00c8:
            java.util.Map<java.lang.String, java.lang.String> r12 = r0.templateSettings
            java.lang.Object r10 = r10.getKey()
            r12.put(r10, r11)
            goto L_0x0090
        L_0x00d2:
            java.lang.String r5 = "cacheable_replacements"
            boolean r10 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r5)
            if (r10 == 0) goto L_0x0165
            com.google.gson.JsonObject r4 = r4.getAsJsonObject(r5)
            java.util.Set r4 = r4.entrySet()
            java.util.Iterator r4 = r4.iterator()
            r5 = r9
        L_0x00e7:
            boolean r10 = r4.hasNext()
            if (r10 == 0) goto L_0x0166
            java.lang.Object r10 = r4.next()
            java.util.Map$Entry r10 = (java.util.Map.Entry) r10
            java.lang.Object r11 = r10.getKey()
            java.lang.CharSequence r11 = (java.lang.CharSequence) r11
            boolean r11 = android.text.TextUtils.isEmpty(r11)
            if (r11 == 0) goto L_0x0100
            goto L_0x00e7
        L_0x0100:
            java.lang.Object r11 = r10.getValue()
            if (r11 != 0) goto L_0x0107
            goto L_0x00e7
        L_0x0107:
            java.lang.Object r11 = r10.getValue()
            com.google.gson.JsonElement r11 = (com.google.gson.JsonElement) r11
            boolean r11 = com.vungle.warren.model.JsonUtil.hasNonNull(r11, r8)
            if (r11 == 0) goto L_0x00e7
            java.lang.Object r11 = r10.getValue()
            com.google.gson.JsonElement r11 = (com.google.gson.JsonElement) r11
            java.lang.String r12 = "extension"
            boolean r11 = com.vungle.warren.model.JsonUtil.hasNonNull(r11, r12)
            if (r11 == 0) goto L_0x00e7
            java.lang.Object r11 = r10.getValue()
            com.google.gson.JsonElement r11 = (com.google.gson.JsonElement) r11
            com.google.gson.JsonObject r11 = r11.getAsJsonObject()
            com.google.gson.JsonElement r11 = r11.get(r8)
            java.lang.String r11 = r11.getAsString()
            java.lang.Object r12 = r10.getValue()
            com.google.gson.JsonElement r12 = (com.google.gson.JsonElement) r12
            com.google.gson.JsonObject r12 = r12.getAsJsonObject()
            java.lang.String r13 = "extension"
            com.google.gson.JsonElement r12 = r12.get(r13)
            java.lang.String r12 = r12.getAsString()
            java.util.Map<java.lang.String, android.util.Pair<java.lang.String, java.lang.String>> r13 = r0.cacheableAssets
            java.lang.Object r14 = r10.getKey()
            android.util.Pair r15 = new android.util.Pair
            r15.<init>(r11, r12)
            r13.put(r14, r15)
            java.lang.Object r10 = r10.getKey()
            java.lang.String r10 = (java.lang.String) r10
            java.lang.String r12 = "MAIN_VIDEO"
            boolean r10 = r10.equalsIgnoreCase(r12)
            if (r10 == 0) goto L_0x00e7
            r5 = r11
            goto L_0x00e7
        L_0x0165:
            r5 = r9
        L_0x0166:
            java.lang.String r4 = "templateId"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x01b3
            java.lang.String r4 = "templateId"
            com.google.gson.JsonElement r4 = r1.get(r4)
            java.lang.String r4 = r4.getAsString()
            r0.templateId = r4
            java.lang.String r4 = "template_type"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x01ab
            java.lang.String r4 = "template_type"
            com.google.gson.JsonElement r4 = r1.get(r4)
            java.lang.String r4 = r4.getAsString()
            r0.templateType = r4
            java.lang.String r4 = "templateURL"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x01a3
            java.lang.String r4 = "templateURL"
            com.google.gson.JsonElement r4 = r1.get(r4)
            java.lang.String r4 = r4.getAsString()
            r0.templateUrl = r4
            goto L_0x0213
        L_0x01a3:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Template URL missing!"
            r1.<init>(r2)
            throw r1
        L_0x01ab:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Template Type missing!"
            r1.<init>(r2)
            throw r1
        L_0x01b3:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Missing templateID!"
            r1.<init>(r2)
            throw r1
        L_0x01bb:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Missing template adConfig!"
            r1.<init>(r2)
            throw r1
        L_0x01c3:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unknown Ad Type "
            r2.append(r3)
            r2.append(r4)
            java.lang.String r3 = "! Please add this ad type"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x01df:
            r0.adType = r3
            java.lang.String r4 = "postBundle"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r5 == 0) goto L_0x01f2
            com.google.gson.JsonElement r4 = r1.get(r4)
            java.lang.String r4 = r4.getAsString()
            goto L_0x01f3
        L_0x01f2:
            r4 = r9
        L_0x01f3:
            r0.postrollBundleUrl = r4
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r8)
            if (r4 == 0) goto L_0x0205
            com.google.gson.JsonElement r4 = r1.get(r8)
            java.lang.String r4 = r4.getAsString()
            r5 = r4
            goto L_0x0206
        L_0x0205:
            r5 = r9
        L_0x0206:
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            r0.templateSettings = r4
            r0.templateUrl = r9
            r0.templateId = r9
            r0.templateType = r9
        L_0x0213:
            boolean r4 = android.text.TextUtils.isEmpty(r5)
            if (r4 != 0) goto L_0x021c
            r0.videoUrl = r5
            goto L_0x021e
        L_0x021c:
            r0.videoUrl = r9
        L_0x021e:
            java.lang.String r4 = "id"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x073d
            java.lang.String r4 = "id"
            com.google.gson.JsonElement r4 = r1.get(r4)
            java.lang.String r4 = r4.getAsString()
            r0.identifier = r4
            java.lang.String r4 = "campaign"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x0735
            java.lang.String r4 = "campaign"
            com.google.gson.JsonElement r4 = r1.get(r4)
            java.lang.String r4 = r4.getAsString()
            r0.campaign = r4
            java.lang.String r4 = "app_id"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x072d
            java.lang.String r4 = "app_id"
            com.google.gson.JsonElement r4 = r1.get(r4)
            java.lang.String r4 = r4.getAsString()
            r0.appID = r4
            java.lang.String r4 = "expiry"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r5 == 0) goto L_0x0287
            com.google.gson.JsonElement r5 = r1.get(r4)
            boolean r5 = r5.isJsonNull()
            if (r5 != 0) goto L_0x0287
            com.google.gson.JsonElement r4 = r1.get(r4)
            long r4 = r4.getAsLong()
            r10 = 0
            int r8 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r8 <= 0) goto L_0x027d
            r0.expireTime = r4
            goto L_0x0290
        L_0x027d:
            long r4 = java.lang.System.currentTimeMillis()
            r10 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 / r10
            r0.expireTime = r4
            goto L_0x0290
        L_0x0287:
            long r4 = java.lang.System.currentTimeMillis()
            r10 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 / r10
            r0.expireTime = r4
        L_0x0290:
            java.lang.String r4 = "tpat"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x054a
            java.lang.String r4 = "tpat"
            com.google.gson.JsonObject r4 = r1.getAsJsonObject(r4)
            java.util.ArrayList r5 = new java.util.ArrayList
            r8 = 5
            r5.<init>(r8)
            r0.checkpoints = r5
            int r5 = r0.adType
            if (r5 == 0) goto L_0x02e4
            if (r5 != r7) goto L_0x02dc
            r5 = 0
        L_0x02ad:
            r8 = 5
            if (r5 >= r8) goto L_0x031a
            int r8 = r5 * 25
            java.util.Locale r10 = java.util.Locale.ENGLISH
            java.lang.Object[] r11 = new java.lang.Object[r7]
            java.lang.Integer r12 = java.lang.Integer.valueOf(r8)
            r11[r3] = r12
            java.lang.String r12 = "checkpoint.%d"
            java.lang.String r10 = java.lang.String.format(r10, r12, r11)
            boolean r11 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r10)
            if (r11 == 0) goto L_0x02d3
            com.vungle.warren.model.Advertisement$Checkpoint r11 = new com.vungle.warren.model.Advertisement$Checkpoint
            com.google.gson.JsonArray r10 = r4.getAsJsonArray(r10)
            byte r8 = (byte) r8
            r11.<init>(r10, r8)
            goto L_0x02d4
        L_0x02d3:
            r11 = r6
        L_0x02d4:
            java.util.ArrayList<com.vungle.warren.model.Advertisement$Checkpoint> r8 = r0.checkpoints
            r8.add(r5, r11)
            int r5 = r5 + 1
            goto L_0x02ad
        L_0x02dc:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Unknown Ad Type!"
            r1.<init>(r2)
            throw r1
        L_0x02e4:
            java.lang.String r5 = "play_percentage"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r5)
            if (r5 == 0) goto L_0x031a
            java.lang.String r5 = "play_percentage"
            com.google.gson.JsonArray r5 = r4.getAsJsonArray(r5)
            r8 = 0
        L_0x02f3:
            int r10 = r5.size()
            if (r8 >= r10) goto L_0x0315
            com.google.gson.JsonElement r10 = r5.get(r8)
            if (r10 != 0) goto L_0x0300
            goto L_0x0312
        L_0x0300:
            java.util.ArrayList<com.vungle.warren.model.Advertisement$Checkpoint> r10 = r0.checkpoints
            com.vungle.warren.model.Advertisement$Checkpoint r11 = new com.vungle.warren.model.Advertisement$Checkpoint
            com.google.gson.JsonElement r12 = r5.get(r8)
            com.google.gson.JsonObject r12 = r12.getAsJsonObject()
            r11.<init>(r12)
            r10.add(r11)
        L_0x0312:
            int r8 = r8 + 1
            goto L_0x02f3
        L_0x0315:
            java.util.ArrayList<com.vungle.warren.model.Advertisement$Checkpoint> r5 = r0.checkpoints
            java.util.Collections.sort(r5)
        L_0x031a:
            java.lang.String r5 = "clickUrl"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r5)
            if (r5 == 0) goto L_0x034d
            java.lang.String r5 = "clickUrl"
            com.google.gson.JsonArray r5 = r4.getAsJsonArray(r5)
            int r8 = r5.size()
            java.lang.String[] r8 = new java.lang.String[r8]
            r0.clickUrls = r8
            java.util.Iterator r5 = r5.iterator()
            r8 = 0
        L_0x0335:
            boolean r10 = r5.hasNext()
            if (r10 == 0) goto L_0x0351
            java.lang.Object r10 = r5.next()
            com.google.gson.JsonElement r10 = (com.google.gson.JsonElement) r10
            java.lang.String[] r11 = r0.clickUrls
            int r12 = r8 + 1
            java.lang.String r10 = r10.getAsString()
            r11[r8] = r10
            r8 = r12
            goto L_0x0335
        L_0x034d:
            java.lang.String[] r5 = new java.lang.String[r3]
            r0.clickUrls = r5
        L_0x0351:
            java.lang.String r5 = "moat"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r5)
            if (r5 == 0) goto L_0x0378
            java.lang.String r5 = "moat"
            com.google.gson.JsonObject r5 = r4.getAsJsonObject(r5)
            java.lang.String r8 = "is_enabled"
            com.google.gson.JsonElement r8 = r5.get(r8)
            boolean r8 = r8.getAsBoolean()
            r0.enableMoat = r8
            java.lang.String r8 = "extra_vast"
            com.google.gson.JsonElement r5 = r5.get(r8)
            java.lang.String r5 = r5.getAsString()
            r0.moatExtraVast = r5
            goto L_0x037c
        L_0x0378:
            r0.enableMoat = r3
            r0.moatExtraVast = r9
        L_0x037c:
            java.lang.String r5 = "video_click"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r5)
            java.lang.String r8 = "null"
            if (r5 == 0) goto L_0x03c4
            java.lang.String r5 = "video_click"
            com.google.gson.JsonArray r5 = r4.getAsJsonArray(r5)
            int r10 = r5.size()
            java.lang.String[] r10 = new java.lang.String[r10]
            r0.videoClickUrls = r10
            r10 = 0
        L_0x0395:
            int r11 = r5.size()
            if (r10 >= r11) goto L_0x03c8
            com.google.gson.JsonElement r11 = r5.get(r10)
            if (r11 == 0) goto L_0x03bd
            com.google.gson.JsonElement r11 = r5.get(r10)
            java.lang.String r11 = r11.toString()
            boolean r11 = r8.equalsIgnoreCase(r11)
            if (r11 == 0) goto L_0x03b0
            goto L_0x03bd
        L_0x03b0:
            java.lang.String[] r11 = r0.videoClickUrls
            com.google.gson.JsonElement r12 = r5.get(r10)
            java.lang.String r12 = r12.getAsString()
            r11[r10] = r12
            goto L_0x03c1
        L_0x03bd:
            java.lang.String[] r11 = r0.videoClickUrls
            r11[r10] = r9
        L_0x03c1:
            int r10 = r10 + 1
            goto L_0x0395
        L_0x03c4:
            java.lang.String[] r5 = new java.lang.String[r3]
            r0.videoClickUrls = r5
        L_0x03c8:
            int r5 = r0.adType
            if (r5 == 0) goto L_0x03e1
            if (r5 != r7) goto L_0x03d9
            java.lang.String r5 = "video.mute"
            java.lang.String r10 = "video.unmute"
            java.lang.String r11 = "video.close"
            java.lang.String r12 = "postroll.click"
            java.lang.String r13 = "postroll.view"
            goto L_0x03eb
        L_0x03d9:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Unknown AdType!"
            r1.<init>(r2)
            throw r1
        L_0x03e1:
            java.lang.String r5 = "mute"
            java.lang.String r10 = "unmute"
            java.lang.String r11 = "video_close"
            java.lang.String r12 = "postroll_click"
            java.lang.String r13 = "postroll_view"
        L_0x03eb:
            boolean r14 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r5)
            if (r14 == 0) goto L_0x042d
            com.google.gson.JsonArray r5 = r4.getAsJsonArray(r5)
            int r14 = r5.size()
            java.lang.String[] r14 = new java.lang.String[r14]
            r0.muteUrls = r14
            r14 = 0
        L_0x03fe:
            int r15 = r5.size()
            if (r14 >= r15) goto L_0x0431
            com.google.gson.JsonElement r15 = r5.get(r14)
            if (r15 == 0) goto L_0x0426
            com.google.gson.JsonElement r15 = r5.get(r14)
            java.lang.String r15 = r15.toString()
            boolean r15 = r8.equalsIgnoreCase(r15)
            if (r15 == 0) goto L_0x0419
            goto L_0x0426
        L_0x0419:
            java.lang.String[] r15 = r0.muteUrls
            com.google.gson.JsonElement r16 = r5.get(r14)
            java.lang.String r16 = r16.getAsString()
            r15[r14] = r16
            goto L_0x042a
        L_0x0426:
            java.lang.String[] r15 = r0.muteUrls
            r15[r14] = r9
        L_0x042a:
            int r14 = r14 + 1
            goto L_0x03fe
        L_0x042d:
            java.lang.String[] r5 = new java.lang.String[r3]
            r0.muteUrls = r5
        L_0x0431:
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r10)
            if (r5 == 0) goto L_0x0473
            com.google.gson.JsonArray r5 = r4.getAsJsonArray(r10)
            int r10 = r5.size()
            java.lang.String[] r10 = new java.lang.String[r10]
            r0.unmuteUrls = r10
            r10 = 0
        L_0x0444:
            int r14 = r5.size()
            if (r10 >= r14) goto L_0x0477
            com.google.gson.JsonElement r14 = r5.get(r10)
            if (r14 == 0) goto L_0x046c
            com.google.gson.JsonElement r14 = r5.get(r10)
            java.lang.String r14 = r14.toString()
            boolean r14 = r8.equalsIgnoreCase(r14)
            if (r14 == 0) goto L_0x045f
            goto L_0x046c
        L_0x045f:
            java.lang.String[] r14 = r0.unmuteUrls
            com.google.gson.JsonElement r15 = r5.get(r10)
            java.lang.String r15 = r15.getAsString()
            r14[r10] = r15
            goto L_0x0470
        L_0x046c:
            java.lang.String[] r14 = r0.unmuteUrls
            r14[r10] = r9
        L_0x0470:
            int r10 = r10 + 1
            goto L_0x0444
        L_0x0473:
            java.lang.String[] r5 = new java.lang.String[r3]
            r0.unmuteUrls = r5
        L_0x0477:
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r11)
            if (r5 == 0) goto L_0x04b9
            com.google.gson.JsonArray r5 = r4.getAsJsonArray(r11)
            int r10 = r5.size()
            java.lang.String[] r10 = new java.lang.String[r10]
            r0.closeUrls = r10
            r10 = 0
        L_0x048a:
            int r11 = r5.size()
            if (r10 >= r11) goto L_0x04bd
            com.google.gson.JsonElement r11 = r5.get(r10)
            if (r11 == 0) goto L_0x04b2
            com.google.gson.JsonElement r11 = r5.get(r10)
            java.lang.String r11 = r11.toString()
            boolean r11 = r8.equalsIgnoreCase(r11)
            if (r11 == 0) goto L_0x04a5
            goto L_0x04b2
        L_0x04a5:
            java.lang.String[] r11 = r0.closeUrls
            com.google.gson.JsonElement r14 = r5.get(r10)
            java.lang.String r14 = r14.getAsString()
            r11[r10] = r14
            goto L_0x04b6
        L_0x04b2:
            java.lang.String[] r11 = r0.closeUrls
            r11[r10] = r9
        L_0x04b6:
            int r10 = r10 + 1
            goto L_0x048a
        L_0x04b9:
            java.lang.String[] r5 = new java.lang.String[r3]
            r0.closeUrls = r5
        L_0x04bd:
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r12)
            if (r5 == 0) goto L_0x04ff
            com.google.gson.JsonArray r5 = r4.getAsJsonArray(r12)
            int r10 = r5.size()
            java.lang.String[] r10 = new java.lang.String[r10]
            r0.postRollClickUrls = r10
            r10 = 0
        L_0x04d0:
            int r11 = r5.size()
            if (r10 >= r11) goto L_0x0503
            com.google.gson.JsonElement r11 = r5.get(r10)
            if (r11 == 0) goto L_0x04f8
            com.google.gson.JsonElement r11 = r5.get(r10)
            java.lang.String r11 = r11.toString()
            boolean r11 = r8.equalsIgnoreCase(r11)
            if (r11 == 0) goto L_0x04eb
            goto L_0x04f8
        L_0x04eb:
            java.lang.String[] r11 = r0.postRollClickUrls
            com.google.gson.JsonElement r12 = r5.get(r10)
            java.lang.String r12 = r12.getAsString()
            r11[r10] = r12
            goto L_0x04fc
        L_0x04f8:
            java.lang.String[] r11 = r0.postRollClickUrls
            r11[r10] = r9
        L_0x04fc:
            int r10 = r10 + 1
            goto L_0x04d0
        L_0x04ff:
            java.lang.String[] r5 = new java.lang.String[r3]
            r0.postRollClickUrls = r5
        L_0x0503:
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r13)
            if (r5 == 0) goto L_0x0545
            com.google.gson.JsonArray r4 = r4.getAsJsonArray(r13)
            int r5 = r4.size()
            java.lang.String[] r5 = new java.lang.String[r5]
            r0.postRollViewUrls = r5
            r5 = 0
        L_0x0516:
            int r10 = r4.size()
            if (r5 >= r10) goto L_0x0571
            com.google.gson.JsonElement r10 = r4.get(r5)
            if (r10 == 0) goto L_0x053e
            com.google.gson.JsonElement r10 = r4.get(r5)
            java.lang.String r10 = r10.toString()
            boolean r10 = r8.equalsIgnoreCase(r10)
            if (r10 == 0) goto L_0x0531
            goto L_0x053e
        L_0x0531:
            java.lang.String[] r10 = r0.postRollViewUrls
            com.google.gson.JsonElement r11 = r4.get(r5)
            java.lang.String r11 = r11.getAsString()
            r10[r5] = r11
            goto L_0x0542
        L_0x053e:
            java.lang.String[] r10 = r0.postRollViewUrls
            r10[r5] = r9
        L_0x0542:
            int r5 = r5 + 1
            goto L_0x0516
        L_0x0545:
            java.lang.String[] r4 = new java.lang.String[r3]
            r0.postRollViewUrls = r4
            goto L_0x0571
        L_0x054a:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r0.checkpoints = r4
            java.lang.String[] r4 = new java.lang.String[r3]
            r0.muteUrls = r4
            java.lang.String[] r4 = new java.lang.String[r3]
            r0.closeUrls = r4
            java.lang.String[] r4 = new java.lang.String[r3]
            r0.unmuteUrls = r4
            java.lang.String[] r4 = new java.lang.String[r3]
            r0.postRollViewUrls = r4
            java.lang.String[] r4 = new java.lang.String[r3]
            r0.postRollClickUrls = r4
            java.lang.String[] r4 = new java.lang.String[r3]
            r0.clickUrls = r4
            java.lang.String[] r4 = new java.lang.String[r3]
            r0.videoClickUrls = r4
            r0.enableMoat = r3
            r0.moatExtraVast = r9
        L_0x0571:
            java.lang.String r4 = "delay"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x0586
            java.lang.String r4 = "delay"
            com.google.gson.JsonElement r4 = r1.get(r4)
            int r4 = r4.getAsInt()
            r0.delay = r4
            goto L_0x0588
        L_0x0586:
            r0.delay = r3
        L_0x0588:
            java.lang.String r4 = "showClose"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x059d
            java.lang.String r4 = "showClose"
            com.google.gson.JsonElement r4 = r1.get(r4)
            int r4 = r4.getAsInt()
            r0.showCloseDelay = r4
            goto L_0x059f
        L_0x059d:
            r0.showCloseDelay = r3
        L_0x059f:
            java.lang.String r4 = "showCloseIncentivized"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x05b4
            java.lang.String r4 = "showCloseIncentivized"
            com.google.gson.JsonElement r4 = r1.get(r4)
            int r4 = r4.getAsInt()
            r0.showCloseIncentivized = r4
            goto L_0x05b6
        L_0x05b4:
            r0.showCloseIncentivized = r3
        L_0x05b6:
            java.lang.String r4 = "countdown"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x05cb
            java.lang.String r4 = "countdown"
            com.google.gson.JsonElement r4 = r1.get(r4)
            int r4 = r4.getAsInt()
            r0.countdown = r4
            goto L_0x05cd
        L_0x05cb:
            r0.countdown = r3
        L_0x05cd:
            java.lang.String r4 = "videoWidth"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x0725
            java.lang.String r4 = "videoWidth"
            com.google.gson.JsonElement r4 = r1.get(r4)
            int r4 = r4.getAsInt()
            r0.videoWidth = r4
            java.lang.String r4 = "videoHeight"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x071d
            java.lang.String r4 = "videoHeight"
            com.google.gson.JsonElement r4 = r1.get(r4)
            int r4 = r4.getAsInt()
            r0.videoHeight = r4
            java.lang.String r4 = "md5"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x060a
            java.lang.String r4 = "md5"
            com.google.gson.JsonElement r4 = r1.get(r4)
            java.lang.String r4 = r4.getAsString()
            r0.md5 = r4
            goto L_0x060c
        L_0x060a:
            r0.md5 = r9
        L_0x060c:
            java.lang.String r4 = "cta_overlay"
            boolean r4 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r4)
            if (r4 == 0) goto L_0x0658
            java.lang.String r4 = "cta_overlay"
            com.google.gson.JsonObject r4 = r1.getAsJsonObject(r4)
            java.lang.String r5 = "enabled"
            boolean r5 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r5)
            if (r5 == 0) goto L_0x062f
            java.lang.String r5 = "enabled"
            com.google.gson.JsonElement r5 = r4.get(r5)
            boolean r5 = r5.getAsBoolean()
            r0.ctaOverlayEnabled = r5
            goto L_0x0631
        L_0x062f:
            r0.ctaOverlayEnabled = r3
        L_0x0631:
            java.lang.String r5 = "click_area"
            boolean r8 = com.vungle.warren.model.JsonUtil.hasNonNull(r4, r5)
            if (r8 == 0) goto L_0x0655
            com.google.gson.JsonElement r8 = r4.get(r5)
            java.lang.String r8 = r8.getAsString()
            boolean r8 = r8.isEmpty()
            if (r8 == 0) goto L_0x064a
            r0.ctaClickArea = r2
            goto L_0x065c
        L_0x064a:
            com.google.gson.JsonElement r2 = r4.get(r5)
            int r2 = r2.getAsInt()
            r0.ctaClickArea = r2
            goto L_0x065c
        L_0x0655:
            r0.ctaClickArea = r2
            goto L_0x065c
        L_0x0658:
            r0.ctaOverlayEnabled = r3
            r0.ctaClickArea = r2
        L_0x065c:
            java.lang.String r2 = "callToActionDest"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r2)
            if (r2 == 0) goto L_0x066f
            java.lang.String r2 = "callToActionDest"
            com.google.gson.JsonElement r2 = r1.get(r2)
            java.lang.String r2 = r2.getAsString()
            goto L_0x0670
        L_0x066f:
            r2 = r6
        L_0x0670:
            r0.ctaDestinationUrl = r2
            java.lang.String r2 = "callToActionUrl"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r2)
            if (r2 == 0) goto L_0x0684
            java.lang.String r2 = "callToActionUrl"
            com.google.gson.JsonElement r2 = r1.get(r2)
            java.lang.String r6 = r2.getAsString()
        L_0x0684:
            r0.ctaUrl = r6
            java.lang.String r2 = "retryCount"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r2)
            if (r2 == 0) goto L_0x069b
            java.lang.String r2 = "retryCount"
            com.google.gson.JsonElement r2 = r1.get(r2)
            int r2 = r2.getAsInt()
            r0.retryCount = r2
            goto L_0x069d
        L_0x069b:
            r0.retryCount = r7
        L_0x069d:
            java.lang.String r2 = "ad_token"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r2)
            if (r2 == 0) goto L_0x0715
            java.lang.String r2 = "ad_token"
            com.google.gson.JsonElement r2 = r1.get(r2)
            java.lang.String r2 = r2.getAsString()
            r0.adToken = r2
            java.lang.String r2 = "video_object_id"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r2)
            if (r2 == 0) goto L_0x06c6
            java.lang.String r2 = "video_object_id"
            com.google.gson.JsonElement r2 = r1.get(r2)
            java.lang.String r2 = r2.getAsString()
            r0.videoIdentifier = r2
            goto L_0x06c8
        L_0x06c6:
            r0.videoIdentifier = r9
        L_0x06c8:
            java.lang.String r2 = "requires_sideloading"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r2)
            if (r2 == 0) goto L_0x06dd
            java.lang.String r2 = "requires_sideloading"
            com.google.gson.JsonElement r2 = r1.get(r2)
            boolean r2 = r2.getAsBoolean()
            r0.requiresNonMarketInstall = r2
            goto L_0x06df
        L_0x06dd:
            r0.requiresNonMarketInstall = r3
        L_0x06df:
            java.lang.String r2 = "ad_market_id"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r2)
            if (r2 == 0) goto L_0x06f4
            java.lang.String r2 = "ad_market_id"
            com.google.gson.JsonElement r2 = r1.get(r2)
            java.lang.String r2 = r2.getAsString()
            r0.adMarketId = r2
            goto L_0x06f6
        L_0x06f4:
            r0.adMarketId = r9
        L_0x06f6:
            java.lang.String r2 = "bid_token"
            boolean r2 = com.vungle.warren.model.JsonUtil.hasNonNull(r1, r2)
            if (r2 == 0) goto L_0x070b
            java.lang.String r2 = "bid_token"
            com.google.gson.JsonElement r1 = r1.get(r2)
            java.lang.String r1 = r1.getAsString()
            r0.bidToken = r1
            goto L_0x070d
        L_0x070b:
            r0.bidToken = r9
        L_0x070d:
            com.vungle.warren.AdConfig r1 = new com.vungle.warren.AdConfig
            r1.<init>()
            r0.adConfig = r1
            return
        L_0x0715:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "AdToken missing!"
            r1.<init>(r2)
            throw r1
        L_0x071d:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Missing video height!"
            r1.<init>(r2)
            throw r1
        L_0x0725:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Missing video width!"
            r1.<init>(r2)
            throw r1
        L_0x072d:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Missing app Id, cannot process advertisement!"
            r1.<init>(r2)
            throw r1
        L_0x0735:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Missing campaign information, cannot process advertisement!"
            r1.<init>(r2)
            throw r1
        L_0x073d:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Missing identifier, cannot process advertisement!"
            r1.<init>(r2)
            throw r1
        L_0x0745:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Advertisement did not contain an adType!"
            r1.<init>(r2)
            throw r1
        L_0x074d:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "JSON does not contain ad markup!"
            r1.<init>(r2)
            goto L_0x0756
        L_0x0755:
            throw r1
        L_0x0756:
            goto L_0x0755
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.model.Advertisement.<init>(com.google.gson.JsonObject):void");
    }

    public int getAdType() {
        return this.adType;
    }

    public List<Checkpoint> getCheckpoints() {
        return this.checkpoints;
    }

    public void configure(AdConfig adConfig2) {
        if (adConfig2 == null) {
            this.adConfig = new AdConfig();
        } else {
            this.adConfig = adConfig2;
        }
    }

    public AdConfig getAdConfig() {
        return this.adConfig;
    }

    public int getOrientation() {
        return this.videoWidth > this.videoHeight ? 1 : 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Advertisement)) {
            return false;
        }
        Advertisement advertisement = (Advertisement) obj;
        if (getId() == null || advertisement.getId() == null || !advertisement.getId().equals(getId()) || advertisement.adType != this.adType || advertisement.expireTime != this.expireTime || advertisement.delay != this.delay || advertisement.showCloseDelay != this.showCloseDelay || advertisement.showCloseIncentivized != this.showCloseIncentivized || advertisement.countdown != this.countdown || advertisement.videoWidth != this.videoWidth || advertisement.videoHeight != this.videoHeight || advertisement.ctaOverlayEnabled != this.ctaOverlayEnabled || advertisement.ctaClickArea != this.ctaClickArea || advertisement.retryCount != this.retryCount || advertisement.enableMoat != this.enableMoat || advertisement.requiresNonMarketInstall != this.requiresNonMarketInstall || !advertisement.identifier.equals(this.identifier) || !advertisement.campaign.equals(this.campaign) || !advertisement.videoUrl.equals(this.videoUrl) || !advertisement.md5.equals(this.md5) || !advertisement.postrollBundleUrl.equals(this.postrollBundleUrl) || !advertisement.ctaDestinationUrl.equals(this.ctaDestinationUrl) || !advertisement.ctaUrl.equals(this.ctaUrl) || !advertisement.adToken.equals(this.adToken) || !advertisement.videoIdentifier.equals(this.videoIdentifier) || !advertisement.moatExtraVast.equals(this.moatExtraVast) || advertisement.state != this.state || advertisement.muteUrls.length != this.muteUrls.length) {
            return false;
        }
        int i = 0;
        while (true) {
            String[] strArr = this.muteUrls;
            if (i < strArr.length) {
                if (!advertisement.muteUrls[i].equals(strArr[i])) {
                    return false;
                }
                i++;
            } else if (advertisement.unmuteUrls.length != this.unmuteUrls.length) {
                return false;
            } else {
                int i2 = 0;
                while (true) {
                    String[] strArr2 = this.unmuteUrls;
                    if (i2 < strArr2.length) {
                        if (!advertisement.unmuteUrls[i2].equals(strArr2[i2])) {
                            return false;
                        }
                        i2++;
                    } else if (advertisement.closeUrls.length != this.closeUrls.length) {
                        return false;
                    } else {
                        int i3 = 0;
                        while (true) {
                            String[] strArr3 = this.closeUrls;
                            if (i3 < strArr3.length) {
                                if (!advertisement.closeUrls[i3].equals(strArr3[i3])) {
                                    return false;
                                }
                                i3++;
                            } else if (advertisement.postRollClickUrls.length != this.postRollClickUrls.length) {
                                return false;
                            } else {
                                int i4 = 0;
                                while (true) {
                                    String[] strArr4 = this.postRollClickUrls;
                                    if (i4 < strArr4.length) {
                                        if (!advertisement.postRollClickUrls[i4].equals(strArr4[i4])) {
                                            return false;
                                        }
                                        i4++;
                                    } else if (advertisement.postRollViewUrls.length != this.postRollViewUrls.length) {
                                        return false;
                                    } else {
                                        int i5 = 0;
                                        while (true) {
                                            String[] strArr5 = this.postRollViewUrls;
                                            if (i5 < strArr5.length) {
                                                if (!advertisement.postRollViewUrls[i5].equals(strArr5[i5])) {
                                                    return false;
                                                }
                                                i5++;
                                            } else if (advertisement.videoClickUrls.length != this.videoClickUrls.length) {
                                                return false;
                                            } else {
                                                int i6 = 0;
                                                while (true) {
                                                    String[] strArr6 = this.videoClickUrls;
                                                    if (i6 < strArr6.length) {
                                                        if (!advertisement.videoClickUrls[i6].equals(strArr6[i6])) {
                                                            return false;
                                                        }
                                                        i6++;
                                                    } else if (advertisement.checkpoints.size() != this.checkpoints.size()) {
                                                        return false;
                                                    } else {
                                                        for (int i7 = 0; i7 < this.checkpoints.size(); i7++) {
                                                            if (!advertisement.checkpoints.get(i7).equals(this.checkpoints.get(i7))) {
                                                                return false;
                                                            }
                                                        }
                                                        if (advertisement.adMarketId.equals(this.adMarketId) && advertisement.bidToken.equals(this.bidToken)) {
                                                            return true;
                                                        }
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public int hashCode() {
        return super.hashCode();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0081, code lost:
        if (r12.equals("video.mute") != false) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d2, code lost:
        if (r12.equals(com.vungle.warren.analytics.AnalyticsEvent.Ad.videoClose) != false) goto L_0x0106;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String[] getTpatUrls(java.lang.String r12) {
        /*
            r11 = this;
            int r0 = r11.adType
            r1 = 4
            r2 = 6
            r3 = 2
            r4 = 3
            r5 = 5
            java.lang.String r6 = "Unknown TPAT Event "
            java.lang.String r7 = "video_click"
            r8 = -1
            r9 = 0
            r10 = 1
            if (r0 == 0) goto L_0x00ba
            if (r0 != r10) goto L_0x00b2
            java.lang.String r0 = "checkpoint"
            boolean r0 = r12.startsWith(r0)
            if (r0 == 0) goto L_0x0039
            java.lang.String[] r0 = new java.lang.String[r9]
            java.lang.String r1 = "\\."
            java.lang.String[] r12 = r12.split(r1)
            r12 = r12[r10]
            int r12 = java.lang.Integer.parseInt(r12)
            java.util.ArrayList<com.vungle.warren.model.Advertisement$Checkpoint> r1 = r11.checkpoints
            int r12 = r12 / 25
            java.lang.Object r12 = r1.get(r12)
            com.vungle.warren.model.Advertisement$Checkpoint r12 = (com.vungle.warren.model.Advertisement.Checkpoint) r12
            if (r12 == 0) goto L_0x0038
            java.lang.String[] r0 = r12.getUrls()
        L_0x0038:
            return r0
        L_0x0039:
            int r0 = r12.hashCode()
            switch(r0) {
                case -1663300692: goto L_0x007b;
                case -1293192841: goto L_0x0071;
                case -481751803: goto L_0x0067;
                case -32221499: goto L_0x005d;
                case 906443463: goto L_0x0053;
                case 1370600644: goto L_0x004b;
                case 1621415126: goto L_0x0041;
                default: goto L_0x0040;
            }
        L_0x0040:
            goto L_0x0084
        L_0x0041:
            java.lang.String r0 = "postroll.view"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0084
            r1 = 1
            goto L_0x0085
        L_0x004b:
            boolean r0 = r12.equals(r7)
            if (r0 == 0) goto L_0x0084
            r1 = 6
            goto L_0x0085
        L_0x0053:
            java.lang.String r0 = "clickUrl"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0084
            r1 = 3
            goto L_0x0085
        L_0x005d:
            java.lang.String r0 = "video.close"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0084
            r1 = 0
            goto L_0x0085
        L_0x0067:
            java.lang.String r0 = "video.unmute"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0084
            r1 = 5
            goto L_0x0085
        L_0x0071:
            java.lang.String r0 = "postroll.click"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0084
            r1 = 2
            goto L_0x0085
        L_0x007b:
            java.lang.String r0 = "video.mute"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0084
            goto L_0x0085
        L_0x0084:
            r1 = -1
        L_0x0085:
            switch(r1) {
                case 0: goto L_0x00af;
                case 1: goto L_0x00ac;
                case 2: goto L_0x00a9;
                case 3: goto L_0x00a6;
                case 4: goto L_0x00a3;
                case 5: goto L_0x00a0;
                case 6: goto L_0x009d;
                default: goto L_0x0088;
            }
        L_0x0088:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r6)
            r1.append(r12)
            java.lang.String r12 = r1.toString()
            r0.<init>(r12)
            throw r0
        L_0x009d:
            java.lang.String[] r12 = r11.videoClickUrls
            return r12
        L_0x00a0:
            java.lang.String[] r12 = r11.unmuteUrls
            return r12
        L_0x00a3:
            java.lang.String[] r12 = r11.muteUrls
            return r12
        L_0x00a6:
            java.lang.String[] r12 = r11.clickUrls
            return r12
        L_0x00a9:
            java.lang.String[] r12 = r11.postRollClickUrls
            return r12
        L_0x00ac:
            java.lang.String[] r12 = r11.postRollViewUrls
            return r12
        L_0x00af:
            java.lang.String[] r12 = r11.closeUrls
            return r12
        L_0x00b2:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "Unknown Advertisement Type!"
            r12.<init>(r0)
            throw r12
        L_0x00ba:
            int r0 = r12.hashCode()
            switch(r0) {
                case -1964722632: goto L_0x00fb;
                case -840405966: goto L_0x00f1;
                case 3363353: goto L_0x00e7;
                case 109635558: goto L_0x00dd;
                case 1370600644: goto L_0x00d5;
                case 1370606900: goto L_0x00cc;
                case 1666667655: goto L_0x00c2;
                default: goto L_0x00c1;
            }
        L_0x00c1:
            goto L_0x0105
        L_0x00c2:
            java.lang.String r0 = "postroll_view"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0105
            r1 = 0
            goto L_0x0106
        L_0x00cc:
            java.lang.String r0 = "video_close"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0105
            goto L_0x0106
        L_0x00d5:
            boolean r0 = r12.equals(r7)
            if (r0 == 0) goto L_0x0105
            r1 = 6
            goto L_0x0106
        L_0x00dd:
            java.lang.String r0 = "postroll_click"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0105
            r1 = 1
            goto L_0x0106
        L_0x00e7:
            java.lang.String r0 = "mute"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0105
            r1 = 2
            goto L_0x0106
        L_0x00f1:
            java.lang.String r0 = "unmute"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0105
            r1 = 3
            goto L_0x0106
        L_0x00fb:
            java.lang.String r0 = "click_url"
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x0105
            r1 = 5
            goto L_0x0106
        L_0x0105:
            r1 = -1
        L_0x0106:
            switch(r1) {
                case 0: goto L_0x0130;
                case 1: goto L_0x012d;
                case 2: goto L_0x012a;
                case 3: goto L_0x0127;
                case 4: goto L_0x0124;
                case 5: goto L_0x0121;
                case 6: goto L_0x011e;
                default: goto L_0x0109;
            }
        L_0x0109:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r6)
            r1.append(r12)
            java.lang.String r12 = r1.toString()
            r0.<init>(r12)
            throw r0
        L_0x011e:
            java.lang.String[] r12 = r11.videoClickUrls
            return r12
        L_0x0121:
            java.lang.String[] r12 = r11.clickUrls
            return r12
        L_0x0124:
            java.lang.String[] r12 = r11.closeUrls
            return r12
        L_0x0127:
            java.lang.String[] r12 = r11.unmuteUrls
            return r12
        L_0x012a:
            java.lang.String[] r12 = r11.muteUrls
            return r12
        L_0x012d:
            java.lang.String[] r12 = r11.postRollClickUrls
            return r12
        L_0x0130:
            java.lang.String[] r12 = r11.postRollViewUrls
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.model.Advertisement.getTpatUrls(java.lang.String):java.lang.String[]");
    }

    public String getId() {
        String str = this.identifier;
        return str == null ? "" : str;
    }

    public String getAdToken() {
        return this.adToken;
    }

    public String getAppID() {
        return this.appID;
    }

    /* access modifiers changed from: package-private */
    public String getUrl() {
        return this.videoUrl;
    }

    public String getCampaign() {
        return this.campaign;
    }

    /* access modifiers changed from: package-private */
    public String getTemplateId() {
        return this.templateId;
    }

    public String getTemplateType() {
        return this.templateType;
    }

    public int getShowCloseDelay(boolean z) {
        int i;
        if (z) {
            i = this.showCloseIncentivized;
        } else {
            i = this.showCloseDelay;
        }
        return i * 1000;
    }

    public boolean getMoatEnabled() {
        return this.enableMoat;
    }

    public String getMoatVastExtra() {
        return this.moatExtraVast;
    }

    public long getExpireTime() {
        return this.expireTime * 1000;
    }

    public JsonObject createMRAIDArgs() {
        Map<String, String> map = this.templateSettings;
        if (map != null) {
            HashMap hashMap = new HashMap(map);
            for (Map.Entry next : this.cacheableAssets.entrySet()) {
                hashMap.put(next.getKey(), ((Pair) next.getValue()).first);
            }
            if (!this.mraidFiles.isEmpty()) {
                hashMap.putAll(this.mraidFiles);
            }
            String str = ServerProtocol.DIALOG_RETURN_SCOPES_TRUE;
            if (!str.equalsIgnoreCase((String) hashMap.get(START_MUTED))) {
                if ((getAdConfig().getSettings() & 1) == 0) {
                    str = "false";
                }
                hashMap.put(START_MUTED, str);
            }
            JsonObject jsonObject = new JsonObject();
            for (Map.Entry entry : hashMap.entrySet()) {
                jsonObject.addProperty((String) entry.getKey(), (String) entry.getValue());
            }
            return jsonObject;
        }
        throw new IllegalArgumentException("Advertisement does not have MRAID Arguments!");
    }

    public String getCTAURL(boolean z) {
        int i = this.adType;
        if (i == 0) {
            return z ? this.ctaUrl : this.ctaDestinationUrl;
        }
        if (i == 1) {
            return this.ctaUrl;
        }
        throw new IllegalArgumentException("Unknown AdType " + this.adType);
    }

    public boolean hasPostroll() {
        return !TextUtils.isEmpty(this.postrollBundleUrl);
    }

    public Map<String, String> getDownloadableUrls() {
        HashMap hashMap = new HashMap();
        int i = this.adType;
        if (i == 0) {
            hashMap.put("video", this.videoUrl);
            if (!TextUtils.isEmpty(this.postrollBundleUrl)) {
                hashMap.put(KEY_POSTROLL, this.postrollBundleUrl);
            }
        } else if (i == 1) {
            hashMap.put("template", this.templateUrl);
            for (Map.Entry next : this.cacheableAssets.entrySet()) {
                String str = (String) ((Pair) next.getValue()).first;
                if (!TextUtils.isEmpty(str) && HttpUrl.parse(str) != null) {
                    hashMap.put(((String) next.getKey()) + "." + ((String) ((Pair) next.getValue()).second), str);
                }
            }
        } else {
            throw new IllegalStateException("Advertisement created without adType!");
        }
        return hashMap;
    }

    public void setMraidAssetDir(File file) {
        for (Map.Entry next : this.cacheableAssets.entrySet()) {
            File file2 = new File(file, ((String) next.getKey()) + "." + ((String) ((Pair) next.getValue()).second));
            if (file2.exists()) {
                Map<String, String> map = this.mraidFiles;
                Object key = next.getKey();
                map.put(key, FILE_SCHEME + file2.getPath());
            }
        }
    }

    public void setState(int i) {
        this.state = i;
    }

    public int getState() {
        return this.state;
    }

    public String getAdMarketId() {
        return this.adMarketId;
    }

    public String getBidToken() {
        return this.bidToken;
    }

    public static class Checkpoint implements Comparable<Checkpoint> {
        private byte percentage;
        private String[] urls;

        public Checkpoint(JsonObject jsonObject) throws IllegalArgumentException {
            if (JsonUtil.hasNonNull(jsonObject, "checkpoint")) {
                this.percentage = (byte) ((int) (jsonObject.get("checkpoint").getAsFloat() * 100.0f));
                if (JsonUtil.hasNonNull(jsonObject, "urls")) {
                    JsonArray asJsonArray = jsonObject.getAsJsonArray("urls");
                    this.urls = new String[asJsonArray.size()];
                    for (int i = 0; i < asJsonArray.size(); i++) {
                        if (asJsonArray.get(i) == null || "null".equalsIgnoreCase(asJsonArray.get(i).toString())) {
                            this.urls[i] = "";
                        } else {
                            this.urls[i] = asJsonArray.get(i).getAsString();
                        }
                    }
                    return;
                }
                throw new IllegalArgumentException("Checkpoint missing reporting URL!");
            }
            throw new IllegalArgumentException("Checkpoint missing percentage!");
        }

        public Checkpoint(JsonArray jsonArray, byte b) {
            if (jsonArray.size() != 0) {
                this.urls = new String[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    this.urls[i] = jsonArray.get(i).getAsString();
                }
                this.percentage = b;
                return;
            }
            throw new IllegalArgumentException("Empty URLS!");
        }

        public String[] getUrls() {
            return this.urls;
        }

        public byte getPercentage() {
            return this.percentage;
        }

        public int compareTo(Checkpoint checkpoint) {
            return Float.compare((float) this.percentage, (float) checkpoint.percentage);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Checkpoint)) {
                return false;
            }
            Checkpoint checkpoint = (Checkpoint) obj;
            if (checkpoint.percentage != this.percentage || checkpoint.urls.length != this.urls.length) {
                return false;
            }
            int i = 0;
            while (true) {
                String[] strArr = this.urls;
                if (i >= strArr.length) {
                    return true;
                }
                if (!checkpoint.urls[i].equals(strArr[i])) {
                    return false;
                }
                i++;
            }
        }

        public int hashCode() {
            return super.hashCode();
        }
    }

    public String toString() {
        return "Advertisement{adType=" + this.adType + ", identifier='" + this.identifier + '\'' + ", appID='" + this.appID + '\'' + ", expireTime=" + this.expireTime + ", checkpoints=" + this.checkpoints + ", muteUrls=" + Arrays.toString(this.muteUrls) + ", unmuteUrls=" + Arrays.toString(this.unmuteUrls) + ", closeUrls=" + Arrays.toString(this.closeUrls) + ", postRollClickUrls=" + Arrays.toString(this.postRollClickUrls) + ", postRollViewUrls=" + Arrays.toString(this.postRollViewUrls) + ", videoClickUrls=" + Arrays.toString(this.videoClickUrls) + ", clickUrls=" + Arrays.toString(this.clickUrls) + ", delay=" + this.delay + ", campaign='" + this.campaign + '\'' + ", showCloseDelay=" + this.showCloseDelay + ", showCloseIncentivized=" + this.showCloseIncentivized + ", countdown=" + this.countdown + ", videoUrl='" + this.videoUrl + '\'' + ", videoWidth=" + this.videoWidth + ", videoHeight=" + this.videoHeight + ", md5='" + this.md5 + '\'' + ", postrollBundleUrl='" + this.postrollBundleUrl + '\'' + ", ctaOverlayEnabled=" + this.ctaOverlayEnabled + ", ctaClickArea=" + this.ctaClickArea + ", ctaDestinationUrl='" + this.ctaDestinationUrl + '\'' + ", ctaUrl='" + this.ctaUrl + '\'' + ", adConfig=" + this.adConfig + ", retryCount=" + this.retryCount + ", adToken='" + this.adToken + '\'' + ", videoIdentifier='" + this.videoIdentifier + '\'' + ", templateUrl='" + this.templateUrl + '\'' + ", templateSettings=" + this.templateSettings + ", mraidFiles=" + this.mraidFiles + ", cacheableAssets=" + this.cacheableAssets + ", templateId='" + this.templateId + '\'' + ", templateType='" + this.templateType + '\'' + ", enableMoat=" + this.enableMoat + ", moatExtraVast='" + this.moatExtraVast + '\'' + ", requiresNonMarketInstall=" + this.requiresNonMarketInstall + ", adMarketId='" + this.adMarketId + '\'' + ", bidToken='" + this.bidToken + '\'' + ", state=" + this.state + '}';
    }
}
