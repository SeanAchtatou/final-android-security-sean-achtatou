package com.vungle.warren.model;

import com.google.gson.JsonObject;

public class Placement {
    private static final String TAG = "Placement";
    boolean autoCached;
    String identifier;
    boolean incentivized;
    boolean isValid = false;
    long wakeupTime;

    public Placement(String str) {
        this.identifier = str;
        this.autoCached = false;
        this.incentivized = false;
    }

    Placement() {
    }

    public Placement(JsonObject jsonObject) throws IllegalArgumentException {
        boolean z = false;
        if (jsonObject.has("reference_id")) {
            this.identifier = jsonObject.get("reference_id").getAsString();
            this.autoCached = jsonObject.has("is_auto_cached") && jsonObject.get("is_auto_cached").getAsBoolean();
            if (jsonObject.has("is_incentivized") && jsonObject.get("is_incentivized").getAsBoolean()) {
                z = true;
            }
            this.incentivized = z;
            return;
        }
        throw new IllegalArgumentException("Missing placement reference ID, cannot use placement!");
    }

    public void snooze(long j) {
        this.wakeupTime = System.currentTimeMillis() + (j * 1000);
    }

    public long getWakeupTime() {
        return this.wakeupTime;
    }

    public void setWakeupTime(long j) {
        this.wakeupTime = j;
    }

    public String getId() {
        return this.identifier;
    }

    public void setValid(boolean z) {
        this.isValid = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Placement placement = (Placement) obj;
        if (this.autoCached != placement.autoCached || this.incentivized != placement.incentivized || this.wakeupTime != placement.wakeupTime || this.isValid != placement.isValid) {
            return false;
        }
        String str = this.identifier;
        String str2 = placement.identifier;
        return str == null ? str2 == null : str.equals(str2);
    }

    public int hashCode() {
        String str = this.identifier;
        int hashCode = str != null ? str.hashCode() : 0;
        long j = this.wakeupTime;
        return (((((hashCode * 31) + (this.autoCached ? 1 : 0)) * 31) + (this.incentivized ? 1 : 0)) * 31) + ((int) (j ^ (j >>> 32)));
    }

    public boolean isAutoCached() {
        return this.autoCached;
    }

    public boolean isIncentivized() {
        return this.incentivized;
    }

    public String toString() {
        return "Placement{identifier='" + this.identifier + '\'' + ", autoCached=" + this.autoCached + ", incentivized=" + this.incentivized + ", wakeupTime=" + this.wakeupTime + '}';
    }
}
