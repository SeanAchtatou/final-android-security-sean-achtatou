package com.vungle.warren.utility;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.URLUtil;
import com.google.android.gms.drive.DriveFile;
import com.vungle.warren.ui.VungleWebViewActivity;

public class ExternalRouter {
    public static final String TAG = ExternalRouter.class.getSimpleName();

    public static boolean launch(String str, Context context) {
        if (str != null && !str.trim().isEmpty()) {
            try {
                Intent parseUri = Intent.parseUri(str, 0);
                parseUri.setFlags(DriveFile.MODE_READ_ONLY);
                if (parseUri.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(parseUri);
                    return true;
                }
                if (!URLUtil.isHttpsUrl(str)) {
                    if (!URLUtil.isHttpUrl(str)) {
                        String str2 = TAG;
                        Log.d(str2, "Cannot open url " + str + str);
                        return false;
                    }
                }
                parseUri.setComponent(new ComponentName(context, VungleWebViewActivity.class));
                parseUri.putExtra(VungleWebViewActivity.INTENT_URL, str);
                context.startActivity(parseUri);
                return true;
            } catch (Exception e) {
                String str3 = TAG;
                Log.e(str3, "Error while opening url" + e.getLocalizedMessage());
            }
        }
        return false;
    }
}
