package com.vungle.warren.utility;

import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipUtility {
    private static final int BUFFER_SIZE = 4096;
    private static final String TAG = UnzipUtility.class.getCanonicalName();

    public interface Filter {
        boolean matches(String str);
    }

    public static List<File> unzip(String str, String str2) throws IOException {
        return unzip(str, str2, null);
    }

    public static List<File> unzip(String str, String str2, Filter filter) throws IOException {
        File file = new File(str2);
        if (!file.exists()) {
            file.mkdir();
        }
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(str));
        ArrayList arrayList = new ArrayList();
        for (ZipEntry nextEntry = zipInputStream.getNextEntry(); nextEntry != null; nextEntry = zipInputStream.getNextEntry()) {
            String str3 = str2 + File.separator + nextEntry.getName();
            if (filter == null || filter.matches(str3)) {
                validateFilename(str3, str2);
                if (nextEntry.isDirectory()) {
                    File file2 = new File(str3);
                    if (!file2.exists()) {
                        file2.mkdir();
                    }
                } else {
                    extractFile(zipInputStream, str3);
                    arrayList.add(new File(str3));
                }
            }
            zipInputStream.closeEntry();
        }
        zipInputStream.close();
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0040, code lost:
        r0 = r0.getParentFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0048, code lost:
        if (r0.mkdir() == false) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004a, code lost:
        r0 = r0.getParentFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004f, code lost:
        extractFile(r6, r7);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0036 */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0040 A[Catch:{ all -> 0x0059 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void extractFile(java.util.zip.ZipInputStream r6, java.lang.String r7) throws java.io.IOException {
        /*
            java.io.File r0 = new java.io.File
            r0.<init>(r7)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x000e
            r0.delete()
        L_0x000e:
            r1 = 0
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ FileNotFoundException -> 0x0035, all -> 0x0032 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0035, all -> 0x0032 }
            r3.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0035, all -> 0x0032 }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0036 }
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]     // Catch:{ FileNotFoundException -> 0x0030, all -> 0x002d }
        L_0x001d:
            int r4 = r6.read(r1)     // Catch:{ FileNotFoundException -> 0x0030, all -> 0x002d }
            r5 = -1
            if (r4 == r5) goto L_0x0029
            r5 = 0
            r2.write(r1, r5, r4)     // Catch:{ FileNotFoundException -> 0x0030, all -> 0x002d }
            goto L_0x001d
        L_0x0029:
            com.vungle.warren.utility.FileUtility.closeQuietly(r2)
            goto L_0x0055
        L_0x002d:
            r6 = move-exception
            r1 = r2
            goto L_0x005a
        L_0x0030:
            r1 = r2
            goto L_0x0036
        L_0x0032:
            r6 = move-exception
            r3 = r1
            goto L_0x005a
        L_0x0035:
            r3 = r1
        L_0x0036:
            java.io.File r2 = r0.getParentFile()     // Catch:{ all -> 0x0059 }
            boolean r2 = r2.exists()     // Catch:{ all -> 0x0059 }
            if (r2 != 0) goto L_0x0052
            java.io.File r0 = r0.getParentFile()     // Catch:{ all -> 0x0059 }
        L_0x0044:
            boolean r2 = r0.mkdir()     // Catch:{ all -> 0x0059 }
            if (r2 != 0) goto L_0x004f
            java.io.File r0 = r0.getParentFile()     // Catch:{ all -> 0x0059 }
            goto L_0x0044
        L_0x004f:
            extractFile(r6, r7)     // Catch:{ all -> 0x0059 }
        L_0x0052:
            com.vungle.warren.utility.FileUtility.closeQuietly(r1)
        L_0x0055:
            com.vungle.warren.utility.FileUtility.closeQuietly(r3)
            return
        L_0x0059:
            r6 = move-exception
        L_0x005a:
            com.vungle.warren.utility.FileUtility.closeQuietly(r1)
            com.vungle.warren.utility.FileUtility.closeQuietly(r3)
            goto L_0x0062
        L_0x0061:
            throw r6
        L_0x0062:
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.utility.UnzipUtility.extractFile(java.util.zip.ZipInputStream, java.lang.String):void");
    }

    private static String validateFilename(String str, String str2) throws IOException {
        String canonicalPath = new File(str).getCanonicalPath();
        if (canonicalPath.startsWith(new File(str2).getCanonicalPath())) {
            return canonicalPath;
        }
        Log.e(TAG, "File is outside extraction target directory.");
        throw new ZipSecurityException("File is outside extraction target directory.");
    }

    static class ZipSecurityException extends IOException {
        ZipSecurityException(String str) {
            super(str);
        }
    }
}
