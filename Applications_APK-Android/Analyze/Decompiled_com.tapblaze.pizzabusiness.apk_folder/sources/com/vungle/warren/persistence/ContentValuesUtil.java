package com.vungle.warren.persistence;

import android.content.ContentValues;

public class ContentValuesUtil {
    public static boolean getBoolean(ContentValues contentValues, String str) {
        String asString = contentValues.getAsString(str);
        try {
            return ((Boolean) asString).booleanValue();
        } catch (ClassCastException unused) {
            if (asString instanceof CharSequence) {
                if (Boolean.valueOf(asString.toString()).booleanValue() || "1".equals(asString)) {
                    return true;
                }
                return false;
            } else if (!(asString instanceof Number) || ((Number) asString).intValue() == 0) {
                return false;
            } else {
                return true;
            }
        }
    }
}
