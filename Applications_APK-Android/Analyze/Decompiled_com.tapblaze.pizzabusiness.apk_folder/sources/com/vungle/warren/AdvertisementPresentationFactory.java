package com.vungle.warren;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.vungle.warren.analytics.JobDelegateAnalytics;
import com.vungle.warren.analytics.MoatTracker;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.ui.CloseDelegate;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.ui.OrientationDelegate;
import com.vungle.warren.ui.contract.AdContract;
import com.vungle.warren.ui.contract.WebAdContract;
import com.vungle.warren.ui.presenter.LocalAdPresenter;
import com.vungle.warren.ui.presenter.MRAIDAdPresenter;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.FullAdWidget;
import com.vungle.warren.ui.view.LocalAdView;
import com.vungle.warren.ui.view.MRAIDAdView;
import com.vungle.warren.ui.view.VungleWebClient;
import com.vungle.warren.utility.HandlerScheduler;
import com.vungle.warren.utility.SDKExecutors;
import java.io.File;

public class AdvertisementPresentationFactory {
    private static final String EXTRA_ADVERTISEMENT = "ADV_FACTORY_ADVERTISEMENT";
    /* access modifiers changed from: private */
    public static final String TAG = AdvertisementPresentationFactory.class.getSimpleName();
    /* access modifiers changed from: private */
    public Advertisement advertisement;
    /* access modifiers changed from: private */
    public VungleApiClient apiClient = Vungle._instance.vungleApiClient;
    /* access modifiers changed from: private */
    public final CloseDelegate closeDelegate;
    /* access modifiers changed from: private */
    public final JobRunner jobRunner = Vungle._instance.jobRunner;
    /* access modifiers changed from: private */
    public final OrientationDelegate orientationDelegate;
    /* access modifiers changed from: private */
    public final String placementId;
    /* access modifiers changed from: private */
    public Repository repository = Vungle._instance.repository;
    /* access modifiers changed from: private */
    public final Bundle savedState;
    private AsyncTask<Void, Void, PresentationResultHolder> task;

    public interface FullScreenCallback {
        void onResult(Pair<AdContract.AdView, AdContract.AdvertisementPresenter> pair, Exception exc);
    }

    public interface ViewCallback {
        void onResult(Pair<WebAdContract.WebAdPresenter, VungleWebClient> pair, Exception exc);
    }

    public AdvertisementPresentationFactory(String str, Bundle bundle, OrientationDelegate orientationDelegate2, CloseDelegate closeDelegate2) throws InstantiationException {
        this.orientationDelegate = orientationDelegate2;
        this.closeDelegate = closeDelegate2;
        if (TextUtils.isEmpty(str)) {
            throw new InstantiationException("Missing placementID! Cannot start advertisement.");
        } else if (this.repository == null || !Vungle.isInitialized()) {
            throw new InstantiationException("Vungle SDK is not initialized");
        } else {
            this.placementId = str;
            this.savedState = bundle;
        }
    }

    /* access modifiers changed from: private */
    public Pair<Advertisement, Placement> loadPresentationData(String str, Bundle bundle) throws Exception {
        Placement placement = (Placement) this.repository.load(str, Placement.class).get();
        if (placement != null) {
            if (bundle == null) {
                this.advertisement = this.repository.findValidAdvertisementForPlacement(str).get();
            } else {
                String string = bundle.getString(EXTRA_ADVERTISEMENT);
                if (!TextUtils.isEmpty(string)) {
                    this.advertisement = (Advertisement) this.repository.load(string, Advertisement.class).get();
                }
            }
            Advertisement advertisement2 = this.advertisement;
            if (advertisement2 == null) {
                throw new Exception("No ad found");
            } else if (this.repository.getAdvertisementAssetDirectory(advertisement2.getId()).get().isDirectory()) {
                return new Pair<>(this.advertisement, placement);
            } else {
                throw new Exception("No asset directory for " + str + " exists!");
            }
        } else {
            throw new Exception("No placement for ID " + str + " found. Please use a valid placement ID");
        }
    }

    public void getFullScreenPresentation(Context context, FullAdWidget fullAdWidget, OptionsState optionsState, FullScreenCallback fullScreenCallback) {
        AsyncTask<Void, Void, PresentationResultHolder> asyncTask = this.task;
        if (asyncTask != null) {
            asyncTask.cancel(true);
        }
        final SDKExecutors sDKExecutors = Vungle.sdkExecutors;
        final FullAdWidget fullAdWidget2 = fullAdWidget;
        final OptionsState optionsState2 = optionsState;
        final Context context2 = context;
        final FullScreenCallback fullScreenCallback2 = fullScreenCallback;
        this.task = new AsyncTask<Void, Void, PresentationResultHolder>() {
            /* access modifiers changed from: protected */
            public PresentationResultHolder doInBackground(Void... voidArr) {
                try {
                    Pair access$200 = AdvertisementPresentationFactory.this.loadPresentationData(AdvertisementPresentationFactory.this.placementId, AdvertisementPresentationFactory.this.savedState);
                    Advertisement unused = AdvertisementPresentationFactory.this.advertisement = (Advertisement) access$200.first;
                    Placement placement = (Placement) access$200.second;
                    if (!Vungle.canPlayAd(AdvertisementPresentationFactory.this.advertisement)) {
                        Log.e(AdvertisementPresentationFactory.TAG, "Advertisement is null or assets are missing");
                        return new PresentationResultHolder(new Exception("Advertisement is null or assets are missing"));
                    }
                    JobDelegateAnalytics jobDelegateAnalytics = new JobDelegateAnalytics(AdvertisementPresentationFactory.this.jobRunner);
                    String str = null;
                    Cookie cookie = (Cookie) AdvertisementPresentationFactory.this.repository.load("appId", Cookie.class).get();
                    if (cookie != null && !TextUtils.isEmpty(cookie.getString("appId"))) {
                        str = cookie.getString("appId");
                    }
                    VungleWebClient vungleWebClient = new VungleWebClient(AdvertisementPresentationFactory.this.advertisement, placement);
                    File file = AdvertisementPresentationFactory.this.repository.getAdvertisementAssetDirectory(AdvertisementPresentationFactory.this.advertisement.getId()).get();
                    int adType = AdvertisementPresentationFactory.this.advertisement.getAdType();
                    if (adType == 0) {
                        MoatTracker connect = MoatTracker.connect(fullAdWidget2.videoView, AdvertisementPresentationFactory.this.apiClient.getMoatEnabled());
                        return new PresentationResultHolder(new LocalAdView(context2, fullAdWidget2, AdvertisementPresentationFactory.this.orientationDelegate, AdvertisementPresentationFactory.this.closeDelegate), new LocalAdPresenter(AdvertisementPresentationFactory.this.advertisement, placement, AdvertisementPresentationFactory.this.repository, new HandlerScheduler(), jobDelegateAnalytics, connect, vungleWebClient, optionsState2, file, sDKExecutors.getIOExecutor(), sDKExecutors.getUIExecutor()), vungleWebClient, connect, str);
                    } else if (adType != 1) {
                        return new PresentationResultHolder(new Exception("No presenter available for ad type!"));
                    } else {
                        return new PresentationResultHolder(new MRAIDAdView(context2, fullAdWidget2, AdvertisementPresentationFactory.this.orientationDelegate, AdvertisementPresentationFactory.this.closeDelegate), new MRAIDAdPresenter(AdvertisementPresentationFactory.this.advertisement, placement, AdvertisementPresentationFactory.this.repository, new HandlerScheduler(), jobDelegateAnalytics, vungleWebClient, null, optionsState2, file, sDKExecutors.getIOExecutor(), sDKExecutors.getUIExecutor()), vungleWebClient, null, null);
                    }
                } catch (Exception e) {
                    return new PresentationResultHolder(e);
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(PresentationResultHolder presentationResultHolder) {
                super.onPostExecute((Object) presentationResultHolder);
                if (!isCancelled() && fullScreenCallback2 != null) {
                    if (presentationResultHolder.ex != null) {
                        Log.e(AdvertisementPresentationFactory.TAG, "Excpetion on creating presenter", presentationResultHolder.ex);
                        fullScreenCallback2.onResult(new Pair(null, null), presentationResultHolder.ex);
                        return;
                    }
                    fullAdWidget2.linkWebView(presentationResultHolder.webClient, new JavascriptBridge(presentationResultHolder.advertisementPresenter));
                    if (presentationResultHolder.tracker != null) {
                        presentationResultHolder.tracker.configure(AdvertisementPresentationFactory.this.placementId, AdvertisementPresentationFactory.this.advertisement, presentationResultHolder.appId);
                    }
                    fullScreenCallback2.onResult(new Pair(presentationResultHolder.adView, presentationResultHolder.advertisementPresenter), presentationResultHolder.ex);
                }
            }
        };
        this.task.execute(new Void[0]);
    }

    public void getNativeViewPresentation(final DirectDownloadAdapter directDownloadAdapter, final ViewCallback viewCallback) {
        final SDKExecutors sDKExecutors = Vungle.sdkExecutors;
        this.task = new AsyncTask<Void, Void, PresentationResultHolder>() {
            /* access modifiers changed from: protected */
            public PresentationResultHolder doInBackground(Void... voidArr) {
                try {
                    Pair access$200 = AdvertisementPresentationFactory.this.loadPresentationData(AdvertisementPresentationFactory.this.placementId, AdvertisementPresentationFactory.this.savedState);
                    Advertisement unused = AdvertisementPresentationFactory.this.advertisement = (Advertisement) access$200.first;
                    if (AdvertisementPresentationFactory.this.advertisement.getAdType() != 1) {
                        return new PresentationResultHolder(new IllegalArgumentException("No presenter available for ad type!"));
                    }
                    Placement placement = (Placement) access$200.second;
                    if (!Vungle.canPlayAd(AdvertisementPresentationFactory.this.advertisement)) {
                        Log.e(AdvertisementPresentationFactory.TAG, "Advertisement is null or assets are missing");
                        return new PresentationResultHolder(new Exception("Advertisement is null or assets are missing"));
                    }
                    JobDelegateAnalytics jobDelegateAnalytics = new JobDelegateAnalytics(AdvertisementPresentationFactory.this.jobRunner);
                    VungleWebClient vungleWebClient = new VungleWebClient(AdvertisementPresentationFactory.this.advertisement, placement);
                    return new PresentationResultHolder(null, new MRAIDAdPresenter(AdvertisementPresentationFactory.this.advertisement, placement, AdvertisementPresentationFactory.this.repository, new HandlerScheduler(), jobDelegateAnalytics, vungleWebClient, directDownloadAdapter, null, AdvertisementPresentationFactory.this.repository.getAdvertisementAssetDirectory(AdvertisementPresentationFactory.this.advertisement.getId()).get(), sDKExecutors.getIOExecutor(), sDKExecutors.getUIExecutor()), vungleWebClient, null, null);
                } catch (Exception e) {
                    return new PresentationResultHolder(e);
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(PresentationResultHolder presentationResultHolder) {
                ViewCallback viewCallback;
                super.onPostExecute((Object) presentationResultHolder);
                if (!isCancelled() && (viewCallback = viewCallback) != null) {
                    viewCallback.onResult(new Pair((WebAdContract.WebAdPresenter) presentationResultHolder.advertisementPresenter, presentationResultHolder.webClient), presentationResultHolder.ex);
                }
            }
        };
        this.task.execute(new Void[0]);
    }

    public void saveState(Bundle bundle) {
        Advertisement advertisement2 = this.advertisement;
        bundle.putString(EXTRA_ADVERTISEMENT, advertisement2 == null ? null : advertisement2.getId());
    }

    public void destroy() {
        AsyncTask<Void, Void, PresentationResultHolder> asyncTask = this.task;
        if (asyncTask != null) {
            asyncTask.cancel(true);
        }
    }

    private static class PresentationResultHolder {
        /* access modifiers changed from: private */
        public AdContract.AdView adView;
        /* access modifiers changed from: private */
        public AdContract.AdvertisementPresenter advertisementPresenter;
        /* access modifiers changed from: private */
        public String appId;
        /* access modifiers changed from: private */
        public Exception ex;
        /* access modifiers changed from: private */
        public MoatTracker tracker;
        /* access modifiers changed from: private */
        public VungleWebClient webClient;

        PresentationResultHolder(Exception exc) {
            this.ex = exc;
        }

        PresentationResultHolder(AdContract.AdView adView2, AdContract.AdvertisementPresenter advertisementPresenter2, VungleWebClient vungleWebClient, MoatTracker moatTracker, String str) {
            this.adView = adView2;
            this.advertisementPresenter = advertisementPresenter2;
            this.webClient = vungleWebClient;
            this.tracker = moatTracker;
            this.appId = str;
        }
    }
}
