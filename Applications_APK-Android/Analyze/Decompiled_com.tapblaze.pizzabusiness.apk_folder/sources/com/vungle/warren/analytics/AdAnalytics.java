package com.vungle.warren.analytics;

import com.google.gson.JsonObject;

public interface AdAnalytics {
    String[] ping(String[] strArr);

    void ri(JsonObject jsonObject);
}
