package com.vungle.warren;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.security.NetworkSecurityPolicy;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.ShareConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ironsource.sdk.constants.Constants;
import com.moat.analytics.mobile.vng.MoatAnalytics;
import com.moat.analytics.mobile.vng.MoatOptions;
import com.vungle.warren.error.VungleError;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.JsonUtil;
import com.vungle.warren.network.VungleApi;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.DatabaseHelper;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.utility.ViewUtility;
import cz.msebera.android.httpclient.HttpHeaders;
import cz.msebera.android.httpclient.HttpStatus;
import io.fabric.sdk.android.services.settings.AppSettingsData;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.GzipSink;
import okio.Okio;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VungleApiClient {
    private static String BASE_URL = "https://ads.api.vungle.com/";
    static String HEADER_UA = (MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "VungleAmazon/6.4.11" : "VungleDroid/6.4.11");
    static final String MANUFACTURER_AMAZON = "Amazon";
    private static Set<Interceptor> logInterceptors = new HashSet();
    private static Set<Interceptor> networkInterceptors = new HashSet();
    private final String TAG = "VungleApiClient";
    private VungleApi api;
    private JsonObject appBody;
    private CacheManager cacheManager;
    private OkHttpClient client;
    /* access modifiers changed from: private */
    public Context context;
    private boolean defaultIdFallbackDisabled;
    /* access modifiers changed from: private */
    public JsonObject deviceBody;
    private boolean enableMoat;
    private VungleApi gzipApi;
    private String newEndpoint;
    private String reportAdEndpoint;
    private Repository repository;
    private String requestAdEndpoint;
    /* access modifiers changed from: private */
    public Map<String, Long> retryAfterDataMap = new ConcurrentHashMap();
    private String riEndpoint;
    private boolean shouldTransmitIMEI;
    private VungleApi timeoutApi;
    /* access modifiers changed from: private */
    public String uaString = System.getProperty("http.agent");
    private JsonObject userBody;
    private String userImei;
    private boolean willPlayAdEnabled;
    private String willPlayAdEndpoint;
    private int willPlayAdTimeout;

    public enum WrapperFramework {
        admob,
        air,
        cocos2dx,
        corona,
        dfp,
        heyzap,
        marmalade,
        mopub,
        unity,
        fyber,
        ironsource,
        upsight,
        appodeal,
        aerserv,
        adtoapp,
        tapdaq,
        vunglehbs,
        none
    }

    private String getConnectionTypeDetail(int i) {
        switch (i) {
            case 1:
                return "GPRS";
            case 2:
                return "EDGE";
            case 3:
                return "UMTS";
            case 4:
                return "CDMA";
            case 5:
                return "EVDO_0";
            case 6:
                return "EVDO_A";
            case 7:
                return "1xRTT";
            case 8:
                return "HSDPA";
            case 9:
                return "HSUPA";
            case 10:
                return "HSPA";
            case 11:
                return "IDEN";
            case 12:
                return "EVDO_B";
            case 13:
                return "LTE";
            case 14:
                return "EHPRD";
            case 15:
                return "HSPAP";
            case 16:
                return "GSM";
            case 17:
                return "TD_SCDMA";
            case 18:
                return "IWLAN";
            default:
                return "UNKNOWN";
        }
    }

    VungleApiClient(Context context2, String str, CacheManager cacheManager2, Repository repository2) {
        this.context = context2.getApplicationContext();
        OkHttpClient.Builder addInterceptor = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            public Response intercept(Interceptor.Chain chain) throws IOException {
                int code;
                Request request = chain.request();
                String encodedPath = request.url().encodedPath();
                Long l = (Long) VungleApiClient.this.retryAfterDataMap.get(encodedPath);
                if (l != null) {
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(l.longValue() - System.currentTimeMillis());
                    if (seconds > 0) {
                        Response.Builder request2 = new Response.Builder().request(request);
                        return request2.addHeader(HttpHeaders.RETRY_AFTER, "" + seconds).code(HttpStatus.SC_INTERNAL_SERVER_ERROR).protocol(Protocol.HTTP_1_1).message("Server is busy").body(ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), "{\"Error\":\"Retry-After\"}")).build();
                    }
                    VungleApiClient.this.retryAfterDataMap.remove(encodedPath);
                }
                Response proceed = chain.proceed(request);
                if (proceed != null && ((code = proceed.code()) == 429 || code == 500 || code == 502 || code == 503)) {
                    String str = proceed.headers().get(HttpHeaders.RETRY_AFTER);
                    if (!TextUtils.isEmpty(str)) {
                        try {
                            long parseLong = Long.parseLong(str);
                            if (parseLong > 0) {
                                VungleApiClient.this.retryAfterDataMap.put(encodedPath, Long.valueOf((parseLong * 1000) + System.currentTimeMillis()));
                            }
                        } catch (NumberFormatException unused) {
                            Log.d("VungleApiClient", "Retry-After value is not an valid value");
                        }
                    }
                }
                return proceed;
            }
        });
        this.client = addInterceptor.build();
        OkHttpClient build = addInterceptor.addInterceptor(new GzipRequestInterceptor()).build();
        Retrofit build2 = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(this.client).build();
        this.api = (VungleApi) build2.create(VungleApi.class);
        this.gzipApi = (VungleApi) build2.newBuilder().client(build).build().create(VungleApi.class);
        init(context2, str, cacheManager2, repository2);
    }

    static class GzipRequestInterceptor implements Interceptor {
        private static final String CONTENT_ENCODING = "Content-Encoding";
        private static final String GZIP = "gzip";

        GzipRequestInterceptor() {
        }

        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();
            if (request.body() == null || request.header("Content-Encoding") != null) {
                return chain.proceed(request);
            }
            return chain.proceed(request.newBuilder().header("Content-Encoding", "gzip").method(request.method(), gzip(request.body())).build());
        }

        private RequestBody gzip(final RequestBody requestBody) throws IOException {
            final Buffer buffer = new Buffer();
            BufferedSink buffer2 = Okio.buffer(new GzipSink(buffer));
            requestBody.writeTo(buffer2);
            buffer2.close();
            return new RequestBody() {
                public MediaType contentType() {
                    return requestBody.contentType();
                }

                public long contentLength() {
                    return buffer.size();
                }

                public void writeTo(BufferedSink bufferedSink) throws IOException {
                    bufferedSink.write(buffer.snapshot());
                }
            };
        }
    }

    public void updateIMEI(String str, boolean z) {
        this.userImei = str;
        this.shouldTransmitIMEI = z;
    }

    public void setDefaultIdFallbackDisabled(boolean z) {
        this.defaultIdFallbackDisabled = z;
    }

    private synchronized void init(final Context context2, String str, CacheManager cacheManager2, Repository repository2) {
        this.repository = repository2;
        this.shouldTransmitIMEI = false;
        this.cacheManager = cacheManager2;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", str);
        jsonObject.addProperty("bundle", context2.getPackageName());
        String str2 = null;
        try {
            str2 = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException unused) {
        }
        if (str2 == null) {
            str2 = "1.0";
        }
        jsonObject.addProperty("ver", str2);
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("make", Build.MANUFACTURER);
        jsonObject2.addProperty("model", Build.MODEL);
        jsonObject2.addProperty("osv", Build.VERSION.RELEASE);
        jsonObject2.addProperty("carrier", ((TelephonyManager) context2.getSystemService(PlaceFields.PHONE)).getNetworkOperatorName());
        jsonObject2.addProperty("os", MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context2.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        jsonObject2.addProperty("w", Integer.valueOf(displayMetrics.widthPixels));
        jsonObject2.addProperty("h", Integer.valueOf(displayMetrics.heightPixels));
        JsonObject jsonObject3 = new JsonObject();
        jsonObject3.add("vungle", new JsonObject());
        jsonObject2.add("ext", jsonObject3);
        try {
            if (Build.VERSION.SDK_INT >= 17) {
                this.uaString = getUserAgentFromCookie();
                initUserAgentLazy();
            } else if (Looper.getMainLooper() == Looper.myLooper()) {
                this.uaString = ViewUtility.getWebView(context2.getApplicationContext()).getSettings().getUserAgentString();
            } else {
                final CountDownLatch countDownLatch = new CountDownLatch(1);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        try {
                            String unused = VungleApiClient.this.uaString = ViewUtility.getWebView(context2.getApplicationContext()).getSettings().getUserAgentString();
                        } catch (InstantiationException e) {
                            Log.e("VungleApiClient", "Cannot Get UserAgent. Setting Default Device UserAgent." + e.getLocalizedMessage());
                        }
                        countDownLatch.countDown();
                    }
                });
                if (!countDownLatch.await(2, TimeUnit.SECONDS)) {
                    Log.e("VungleApiClient", "Unable to get User Agent String in specified time");
                }
            }
        } catch (Exception e) {
            Log.e("VungleApiClient", "Cannot Get UserAgent. Setting Default Device UserAgent." + e.getLocalizedMessage());
        }
        jsonObject2.addProperty("ua", this.uaString);
        this.deviceBody = jsonObject2;
        this.appBody = jsonObject;
    }

    private void initUserAgentLazy() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    String unused = VungleApiClient.this.uaString = WebSettings.getDefaultUserAgent(VungleApiClient.this.context);
                    VungleApiClient.this.deviceBody.addProperty("ua", VungleApiClient.this.uaString);
                    VungleApiClient.this.addUserAgentInCookie(VungleApiClient.this.uaString);
                } catch (Exception e) {
                    Log.e("VungleApiClient", "Cannot Get UserAgent. Setting Default Device UserAgent." + e.getLocalizedMessage());
                }
            }
        }).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Character):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.String):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void */
    public retrofit2.Response<JsonObject> config() throws VungleException, IOException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add(Constants.ParametersKeys.ORIENTATION_DEVICE, getDeviceBody());
        jsonObject.add(SettingsJsonConstants.APP_KEY, this.appBody);
        jsonObject.add("user", getUserBody());
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("is_auto_cached_enforced", (Boolean) false);
        jsonObject.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject2);
        retrofit2.Response<JsonObject> execute = this.api.config(HEADER_UA, jsonObject).execute();
        if (!execute.isSuccessful()) {
            return execute;
        }
        JsonObject body = execute.body();
        Log.d("VungleApiClient", "Config Response: " + body);
        if (JsonUtil.hasNonNull(body, "sleep")) {
            String asString = JsonUtil.hasNonNull(body, "info") ? body.get("info").getAsString() : "";
            Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. " + asString);
            throw new VungleException(3);
        } else if (JsonUtil.hasNonNull(body, "endpoints")) {
            JsonObject asJsonObject = body.getAsJsonObject("endpoints");
            HttpUrl parse = HttpUrl.parse(asJsonObject.get(AppSettingsData.STATUS_NEW).getAsString());
            HttpUrl parse2 = HttpUrl.parse(asJsonObject.get("ads").getAsString());
            HttpUrl parse3 = HttpUrl.parse(asJsonObject.get("will_play_ad").getAsString());
            HttpUrl parse4 = HttpUrl.parse(asJsonObject.get("report_ad").getAsString());
            HttpUrl parse5 = HttpUrl.parse(asJsonObject.get("ri").getAsString());
            if (parse == null || parse2 == null || parse3 == null || parse4 == null || parse5 == null) {
                Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. ");
                throw new VungleException(3);
            }
            this.newEndpoint = parse.toString();
            this.requestAdEndpoint = parse2.toString();
            this.willPlayAdEndpoint = parse3.toString();
            this.reportAdEndpoint = parse4.toString();
            this.riEndpoint = parse5.toString();
            JsonObject asJsonObject2 = body.getAsJsonObject("will_play_ad");
            this.willPlayAdTimeout = asJsonObject2.get("request_timeout").getAsInt();
            this.willPlayAdEnabled = asJsonObject2.get("enabled").getAsBoolean();
            this.enableMoat = body.getAsJsonObject("viewability").get("moat").getAsBoolean();
            if (this.willPlayAdEnabled) {
                Log.v("VungleApiClient", "willPlayAd is enabled, generating a timeout client.");
                this.timeoutApi = (VungleApi) new Retrofit.Builder().client(this.client.newBuilder().readTimeout((long) this.willPlayAdTimeout, TimeUnit.MILLISECONDS).build()).addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.vungle.com/").build().create(VungleApi.class);
            }
            if (getMoatEnabled()) {
                MoatOptions moatOptions = new MoatOptions();
                moatOptions.disableAdIdCollection = true;
                moatOptions.disableLocationServices = true;
                moatOptions.loggingEnabled = true;
                MoatAnalytics.getInstance().start(moatOptions, (Application) this.context.getApplicationContext());
            }
            return execute;
        } else {
            Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. ");
            throw new VungleException(3);
        }
    }

    public Call<JsonObject> reportNew() throws IllegalStateException {
        if (this.newEndpoint != null) {
            HashMap hashMap = new HashMap(2);
            JsonElement jsonElement = this.appBody.get("id");
            JsonElement jsonElement2 = this.deviceBody.get("ifa");
            String str = "";
            hashMap.put("app_id", jsonElement != null ? jsonElement.getAsString() : str);
            if (jsonElement2 != null) {
                str = jsonElement2.getAsString();
            }
            hashMap.put("ifa", str);
            return this.api.reportNew(HEADER_UA, this.newEndpoint, hashMap);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> requestAd(String str, boolean z) throws IllegalStateException {
        if (this.requestAdEndpoint != null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(Constants.ParametersKeys.ORIENTATION_DEVICE, getDeviceBody());
            jsonObject.add(SettingsJsonConstants.APP_KEY, this.appBody);
            jsonObject.add("user", getUserBody());
            JsonObject jsonObject2 = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(str);
            jsonObject2.add("placements", jsonArray);
            jsonObject2.addProperty("header_bidding", Boolean.valueOf(z));
            jsonObject.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject2);
            return this.api.ads(HEADER_UA, this.requestAdEndpoint, jsonObject);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> willPlayAd(String str, boolean z, String str2) throws IllegalStateException, VungleError {
        if (this.willPlayAdEndpoint == null) {
            throw new IllegalStateException("API Client not configured yet! Must call /config first.");
        } else if (this.willPlayAdEnabled) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(Constants.ParametersKeys.ORIENTATION_DEVICE, getDeviceBody());
            jsonObject.add(SettingsJsonConstants.APP_KEY, this.appBody);
            jsonObject.add("user", getUserBody());
            JsonObject jsonObject2 = new JsonObject();
            JsonObject jsonObject3 = new JsonObject();
            jsonObject3.addProperty("reference_id", str);
            jsonObject3.addProperty("is_auto_cached", Boolean.valueOf(z));
            jsonObject2.add("placement", jsonObject3);
            jsonObject2.addProperty(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_AD_TOKEN, str2);
            jsonObject.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject2);
            return this.timeoutApi.willPlayAd(HEADER_UA, this.willPlayAdEndpoint, jsonObject);
        } else {
            throw new VungleError(6);
        }
    }

    public Call<JsonObject> reportAd(JsonObject jsonObject) {
        if (this.reportAdEndpoint != null) {
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.add(Constants.ParametersKeys.ORIENTATION_DEVICE, getDeviceBody());
            jsonObject2.add(SettingsJsonConstants.APP_KEY, this.appBody);
            jsonObject2.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject);
            jsonObject2.add("user", getUserBody());
            return this.gzipApi.reportAd(HEADER_UA, this.reportAdEndpoint, jsonObject2);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> ri(JsonObject jsonObject) {
        if (this.riEndpoint != null) {
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.add(Constants.ParametersKeys.ORIENTATION_DEVICE, getDeviceBody());
            jsonObject2.add(SettingsJsonConstants.APP_KEY, this.appBody);
            jsonObject2.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject);
            return this.api.ri(HEADER_UA, this.riEndpoint, jsonObject2);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public boolean pingTPAT(String str) throws ClearTextTrafficException, MalformedURLException {
        boolean z;
        if (TextUtils.isEmpty(str) || HttpUrl.parse(str) == null) {
            throw new MalformedURLException("Invalid URL : " + str);
        }
        try {
            String host = new URL(str).getHost();
            if (Build.VERSION.SDK_INT >= 24) {
                z = NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(host);
            } else {
                z = Build.VERSION.SDK_INT >= 23 ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : true;
            }
            if (z || !URLUtil.isHttpUrl(str)) {
                if (!TextUtils.isEmpty(this.userImei) && this.shouldTransmitIMEI) {
                    str = str.replace("%imei%", this.userImei);
                }
                try {
                    this.api.pingTPAT(this.uaString, str).execute();
                    return true;
                } catch (IOException unused) {
                    return false;
                }
            } else {
                throw new ClearTextTrafficException("Clear Text Traffic is blocked");
            }
        } catch (MalformedURLException unused2) {
            throw new MalformedURLException("Invalid URL : " + str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Boolean):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Character):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.String):void
      com.google.gson.JsonObject.addProperty(java.lang.String, java.lang.Number):void */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x01f6  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0259  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0355  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x039c  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x039f  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0177  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.gson.JsonObject getDeviceBody() throws java.lang.IllegalStateException {
        /*
            r15 = this;
            java.lang.String r0 = "ifa"
            java.lang.String r1 = "VungleApiClient"
            java.lang.String r2 = "Amazon"
            com.google.gson.JsonObject r3 = new com.google.gson.JsonObject
            r3.<init>()
            r4 = 0
            r5 = 0
            r6 = 1
            java.lang.String r7 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x008b }
            boolean r7 = r2.equals(r7)     // Catch:{ Exception -> 0x008b }
            java.lang.String r8 = "advertising_id"
            if (r7 == 0) goto L_0x003d
            android.content.Context r7 = r15.context     // Catch:{ SettingNotFoundException -> 0x0031 }
            android.content.ContentResolver r7 = r7.getContentResolver()     // Catch:{ SettingNotFoundException -> 0x0031 }
            java.lang.String r9 = "limit_ad_tracking"
            int r9 = android.provider.Settings.Secure.getInt(r7, r9)     // Catch:{ SettingNotFoundException -> 0x0031 }
            if (r9 != r6) goto L_0x0028
            r9 = 1
            goto L_0x0029
        L_0x0028:
            r9 = 0
        L_0x0029:
            java.lang.String r7 = android.provider.Settings.Secure.getString(r7, r8)     // Catch:{ SettingNotFoundException -> 0x002f }
            goto L_0x0092
        L_0x002f:
            r7 = move-exception
            goto L_0x0033
        L_0x0031:
            r7 = move-exception
            r9 = 1
        L_0x0033:
            java.lang.String r8 = "Error getting Amazon advertising info"
            android.util.Log.w(r1, r8, r7)     // Catch:{ Exception -> 0x003b }
            r7 = r4
            goto L_0x0092
        L_0x003b:
            r7 = r4
            goto L_0x008d
        L_0x003d:
            android.content.Context r7 = r15.context     // Catch:{ NoClassDefFoundError -> 0x005f }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r7 = com.google.android.gms.ads.identifier.AdvertisingIdClient.getAdvertisingIdInfo(r7)     // Catch:{ NoClassDefFoundError -> 0x005f }
            if (r7 == 0) goto L_0x0059
            java.lang.String r9 = r7.getId()     // Catch:{ NoClassDefFoundError -> 0x005f }
            boolean r7 = r7.isLimitAdTrackingEnabled()     // Catch:{ NoClassDefFoundError -> 0x0057, Exception -> 0x0055 }
            com.google.gson.JsonObject r10 = r15.deviceBody     // Catch:{ NoClassDefFoundError -> 0x0053 }
            r10.addProperty(r0, r9)     // Catch:{ NoClassDefFoundError -> 0x0053 }
            goto L_0x005b
        L_0x0053:
            r10 = move-exception
            goto L_0x0062
        L_0x0055:
            r7 = r9
            goto L_0x008c
        L_0x0057:
            r10 = move-exception
            goto L_0x0061
        L_0x0059:
            r9 = r4
            r7 = 1
        L_0x005b:
            r14 = r9
            r9 = r7
            r7 = r14
            goto L_0x0092
        L_0x005f:
            r10 = move-exception
            r9 = r4
        L_0x0061:
            r7 = 1
        L_0x0062:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0087 }
            r11.<init>()     // Catch:{ Exception -> 0x0087 }
            java.lang.String r12 = "Play services Not available: "
            r11.append(r12)     // Catch:{ Exception -> 0x0087 }
            java.lang.String r10 = r10.getLocalizedMessage()     // Catch:{ Exception -> 0x0087 }
            r11.append(r10)     // Catch:{ Exception -> 0x0087 }
            java.lang.String r10 = r11.toString()     // Catch:{ Exception -> 0x0087 }
            android.util.Log.e(r1, r10)     // Catch:{ Exception -> 0x0087 }
            android.content.Context r10 = r15.context     // Catch:{ Exception -> 0x0087 }
            android.content.ContentResolver r10 = r10.getContentResolver()     // Catch:{ Exception -> 0x0087 }
            java.lang.String r8 = android.provider.Settings.Secure.getString(r10, r8)     // Catch:{ Exception -> 0x0087 }
            r9 = r7
            r7 = r8
            goto L_0x0092
        L_0x0087:
            r14 = r9
            r9 = r7
            r7 = r14
            goto L_0x008d
        L_0x008b:
            r7 = r4
        L_0x008c:
            r9 = 1
        L_0x008d:
            java.lang.String r8 = "Cannot load Advertising ID"
            android.util.Log.e(r1, r8)
        L_0x0092:
            java.lang.String r8 = ""
            if (r7 == 0) goto L_0x00ac
            java.lang.String r10 = android.os.Build.MANUFACTURER
            boolean r10 = r2.equals(r10)
            if (r10 == 0) goto L_0x00a1
            java.lang.String r10 = "amazon_advertising_id"
            goto L_0x00a3
        L_0x00a1:
            java.lang.String r10 = "gaid"
        L_0x00a3:
            r3.addProperty(r10, r7)
            com.google.gson.JsonObject r10 = r15.deviceBody
            r10.addProperty(r0, r7)
            goto L_0x00d7
        L_0x00ac:
            android.content.Context r7 = r15.context
            android.content.ContentResolver r7 = r7.getContentResolver()
            java.lang.String r10 = "android_id"
            java.lang.String r7 = android.provider.Settings.Secure.getString(r7, r10)
            com.google.gson.JsonObject r11 = r15.deviceBody
            boolean r12 = r15.defaultIdFallbackDisabled
            if (r12 == 0) goto L_0x00c0
        L_0x00be:
            r12 = r8
            goto L_0x00c7
        L_0x00c0:
            boolean r12 = android.text.TextUtils.isEmpty(r7)
            if (r12 != 0) goto L_0x00be
            r12 = r7
        L_0x00c7:
            r11.addProperty(r0, r12)
            boolean r0 = android.text.TextUtils.isEmpty(r7)
            if (r0 != 0) goto L_0x00d7
            boolean r0 = r15.defaultIdFallbackDisabled
            if (r0 != 0) goto L_0x00d7
            r3.addProperty(r10, r7)
        L_0x00d7:
            com.google.gson.JsonObject r0 = r15.deviceBody
            java.lang.Integer r7 = java.lang.Integer.valueOf(r9)
            java.lang.String r9 = "lmt"
            r0.addProperty(r9, r7)
            android.content.Context r0 = r15.context
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            r7 = 128(0x80, float:1.794E-43)
            java.util.List r0 = r0.getInstalledPackages(r7)
            java.util.Iterator r0 = r0.iterator()
            r7 = 0
        L_0x00f3:
            boolean r9 = r0.hasNext()
            if (r9 == 0) goto L_0x010b
            java.lang.Object r9 = r0.next()
            android.content.pm.PackageInfo r9 = (android.content.pm.PackageInfo) r9
            java.lang.String r9 = r9.packageName
            java.lang.String r10 = "com.google.android.gms"
            boolean r9 = r9.equalsIgnoreCase(r10)
            if (r9 == 0) goto L_0x00f3
            r7 = 1
            goto L_0x00f3
        L_0x010b:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r7)
            java.lang.String r7 = "is_google_play_services_available"
            r3.addProperty(r7, r0)
            android.content.Context r0 = r15.context
            android.content.IntentFilter r7 = new android.content.IntentFilter
            java.lang.String r9 = "android.intent.action.BATTERY_CHANGED"
            r7.<init>(r9)
            android.content.Intent r0 = r0.registerReceiver(r4, r7)
            r4 = -1
            java.lang.String r7 = "level"
            int r7 = r0.getIntExtra(r7, r4)
            java.lang.String r9 = "scale"
            int r9 = r0.getIntExtra(r9, r4)
            if (r7 <= 0) goto L_0x013e
            if (r9 <= 0) goto L_0x013e
            float r7 = (float) r7
            float r9 = (float) r9
            float r7 = r7 / r9
            java.lang.Float r7 = java.lang.Float.valueOf(r7)
            java.lang.String r9 = "battery_level"
            r3.addProperty(r9, r7)
        L_0x013e:
            java.lang.String r7 = "status"
            int r7 = r0.getIntExtra(r7, r4)
            r9 = 4
            r10 = 2
            java.lang.String r11 = "UNKNOWN"
            if (r7 != r4) goto L_0x014c
            r0 = r11
            goto L_0x016c
        L_0x014c:
            if (r7 == r10) goto L_0x0155
            r12 = 5
            if (r7 != r12) goto L_0x0152
            goto L_0x0155
        L_0x0152:
            java.lang.String r0 = "NOT_CHARGING"
            goto L_0x016c
        L_0x0155:
            java.lang.String r7 = "plugged"
            int r0 = r0.getIntExtra(r7, r4)
            if (r0 == r6) goto L_0x016a
            if (r0 == r10) goto L_0x0167
            if (r0 == r9) goto L_0x0164
            java.lang.String r0 = "BATTERY_PLUGGED_OTHERS"
            goto L_0x016c
        L_0x0164:
            java.lang.String r0 = "BATTERY_PLUGGED_WIRELESS"
            goto L_0x016c
        L_0x0167:
            java.lang.String r0 = "BATTERY_PLUGGED_USB"
            goto L_0x016c
        L_0x016a:
            java.lang.String r0 = "BATTERY_PLUGGED_AC"
        L_0x016c:
            java.lang.String r4 = "battery_state"
            r3.addProperty(r4, r0)
            int r0 = android.os.Build.VERSION.SDK_INT
            r4 = 21
            if (r0 < r4) goto L_0x0195
            android.content.Context r0 = r15.context
            java.lang.String r4 = "power"
            java.lang.Object r0 = r0.getSystemService(r4)
            android.os.PowerManager r0 = (android.os.PowerManager) r0
            if (r0 == 0) goto L_0x018b
            boolean r0 = r0.isPowerSaveMode()
            if (r0 == 0) goto L_0x018b
            r0 = 1
            goto L_0x018c
        L_0x018b:
            r0 = 0
        L_0x018c:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r4 = "battery_saver_enabled"
            r3.addProperty(r4, r0)
        L_0x0195:
            android.content.Context r0 = r15.context
            java.lang.String r4 = "android.permission.ACCESS_NETWORK_STATE"
            int r0 = androidx.core.content.PermissionChecker.checkCallingOrSelfPermission(r0, r4)
            r4 = 3
            if (r0 != 0) goto L_0x022e
            android.content.Context r0 = r15.context
            java.lang.String r7 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r7)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            java.lang.String r7 = "NONE"
            if (r0 == 0) goto L_0x01e5
            android.net.NetworkInfo r12 = r0.getActiveNetworkInfo()
            if (r12 == 0) goto L_0x01e5
            int r7 = r12.getType()
            if (r7 == 0) goto L_0x01d7
            if (r7 == r6) goto L_0x01d2
            r12 = 6
            if (r7 == r12) goto L_0x01d2
            r12 = 7
            if (r7 == r12) goto L_0x01cd
            r12 = 9
            if (r7 == r12) goto L_0x01c8
            r7 = r11
            goto L_0x01e5
        L_0x01c8:
            java.lang.String r7 = "ETHERNET"
            java.lang.String r12 = "ETHERNET"
            goto L_0x01e6
        L_0x01cd:
            java.lang.String r7 = "BLUETOOTH"
            java.lang.String r12 = "BLUETOOTH"
            goto L_0x01e6
        L_0x01d2:
            java.lang.String r7 = "WIFI"
            java.lang.String r12 = "WIFI"
            goto L_0x01e6
        L_0x01d7:
            int r7 = r12.getSubtype()
            java.lang.String r7 = r15.getConnectionTypeDetail(r7)
            java.lang.String r12 = "MOBILE"
            r14 = r12
            r12 = r7
            r7 = r14
            goto L_0x01e6
        L_0x01e5:
            r12 = r7
        L_0x01e6:
            java.lang.String r13 = "connection_type"
            r3.addProperty(r13, r7)
            java.lang.String r7 = "connection_type_detail"
            r3.addProperty(r7, r12)
            int r7 = android.os.Build.VERSION.SDK_INT
            r12 = 24
            if (r7 < r12) goto L_0x022e
            boolean r7 = r0.isActiveNetworkMetered()
            if (r7 == 0) goto L_0x021e
            int r0 = r0.getRestrictBackgroundStatus()
            if (r0 == r6) goto L_0x020d
            if (r0 == r10) goto L_0x020a
            if (r0 == r4) goto L_0x0207
            goto L_0x020f
        L_0x0207:
            java.lang.String r11 = "ENABLED"
            goto L_0x020f
        L_0x020a:
            java.lang.String r11 = "WHITELISTED"
            goto L_0x020f
        L_0x020d:
            java.lang.String r11 = "DISABLED"
        L_0x020f:
            java.lang.String r0 = "data_saver_status"
            r3.addProperty(r0, r11)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            java.lang.String r7 = "network_metered"
            r3.addProperty(r7, r0)
            goto L_0x022e
        L_0x021e:
            java.lang.String r0 = "data_saver_status"
            java.lang.String r7 = "NOT_APPLICABLE"
            r3.addProperty(r0, r7)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            java.lang.String r7 = "network_metered"
            r3.addProperty(r7, r0)
        L_0x022e:
            java.util.Locale r0 = java.util.Locale.getDefault()
            java.lang.String r0 = r0.toString()
            java.lang.String r7 = "locale"
            r3.addProperty(r7, r0)
            java.util.Locale r0 = java.util.Locale.getDefault()
            java.lang.String r0 = r0.getLanguage()
            java.lang.String r7 = "language"
            r3.addProperty(r7, r0)
            java.util.TimeZone r0 = java.util.TimeZone.getDefault()
            java.lang.String r0 = r0.getID()
            java.lang.String r7 = "time_zone"
            r3.addProperty(r7, r0)
            android.content.Context r0 = r15.context
            if (r0 == 0) goto L_0x0355
            java.lang.String r7 = "audio"
            java.lang.Object r0 = r0.getSystemService(r7)
            android.media.AudioManager r0 = (android.media.AudioManager) r0
            if (r0 == 0) goto L_0x0285
            int r7 = r0.getStreamMaxVolume(r4)
            int r0 = r0.getStreamVolume(r4)
            float r4 = (float) r0
            float r7 = (float) r7
            float r4 = r4 / r7
            java.lang.Float r4 = java.lang.Float.valueOf(r4)
            java.lang.String r7 = "volume_level"
            r3.addProperty(r7, r4)
            if (r0 <= 0) goto L_0x027b
            r0 = 1
            goto L_0x027c
        L_0x027b:
            r0 = 0
        L_0x027c:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r4 = "sound_enabled"
            r3.addProperty(r4, r0)
        L_0x0285:
            com.vungle.warren.persistence.CacheManager r0 = r15.cacheManager
            java.io.File r0 = r0.getCache()
            r0.getPath()
            boolean r4 = r0.exists()
            if (r4 == 0) goto L_0x02a9
            boolean r0 = r0.isDirectory()
            if (r0 == 0) goto L_0x02a9
            com.vungle.warren.persistence.CacheManager r0 = r15.cacheManager
            long r10 = r0.getBytesAvailable()
            java.lang.Long r0 = java.lang.Long.valueOf(r10)
            java.lang.String r4 = "storage_bytes_available"
            r3.addProperty(r4, r0)
        L_0x02a9:
            java.lang.String r0 = android.os.Build.MANUFACTURER
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x02c2
            android.content.Context r0 = r15.context
            android.content.Context r0 = r0.getApplicationContext()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            java.lang.String r4 = "amazon.hardware.fire_tv"
            boolean r0 = r0.hasSystemFeature(r4)
            goto L_0x0301
        L_0x02c2:
            int r0 = android.os.Build.VERSION.SDK_INT
            r4 = 23
            if (r0 < r4) goto L_0x02dc
            android.content.Context r0 = r15.context
            java.lang.String r4 = "uimode"
            java.lang.Object r0 = r0.getSystemService(r4)
            android.app.UiModeManager r0 = (android.app.UiModeManager) r0
            int r0 = r0.getCurrentModeType()
            if (r0 != r9) goto L_0x02da
        L_0x02d8:
            r0 = 1
            goto L_0x0301
        L_0x02da:
            r0 = 0
            goto L_0x0301
        L_0x02dc:
            android.content.Context r0 = r15.context
            android.content.Context r0 = r0.getApplicationContext()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            java.lang.String r4 = "com.google.android.tv"
            boolean r0 = r0.hasSystemFeature(r4)
            if (r0 != 0) goto L_0x02d8
            android.content.Context r0 = r15.context
            android.content.Context r0 = r0.getApplicationContext()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            java.lang.String r4 = "android.hardware.touchscreen"
            boolean r0 = r0.hasSystemFeature(r4)
            if (r0 != 0) goto L_0x02da
            goto L_0x02d8
        L_0x0301:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            java.lang.String r4 = "is_tv"
            r3.addProperty(r4, r0)
            int r0 = android.os.Build.VERSION.SDK_INT
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r4 = "os_api_level"
            r3.addProperty(r4, r0)
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ SettingNotFoundException -> 0x0345 }
            r4 = 26
            if (r0 < r4) goto L_0x0335
            android.content.Context r0 = r15.context     // Catch:{ SettingNotFoundException -> 0x0345 }
            java.lang.String r4 = "android.permission.REQUEST_INSTALL_PACKAGES"
            int r0 = r0.checkCallingOrSelfPermission(r4)     // Catch:{ SettingNotFoundException -> 0x0345 }
            if (r0 != 0) goto L_0x034b
            android.content.Context r0 = r15.context     // Catch:{ SettingNotFoundException -> 0x0345 }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ SettingNotFoundException -> 0x0345 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ SettingNotFoundException -> 0x0345 }
            boolean r0 = r0.canRequestPackageInstalls()     // Catch:{ SettingNotFoundException -> 0x0345 }
            r5 = r0
            goto L_0x034b
        L_0x0335:
            android.content.Context r0 = r15.context     // Catch:{ SettingNotFoundException -> 0x0345 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ SettingNotFoundException -> 0x0345 }
            java.lang.String r4 = "install_non_market_apps"
            int r0 = android.provider.Settings.Secure.getInt(r0, r4)     // Catch:{ SettingNotFoundException -> 0x0345 }
            if (r0 != r6) goto L_0x034b
            r5 = 1
            goto L_0x034b
        L_0x0345:
            r0 = move-exception
            java.lang.String r4 = "isInstallNonMarketAppsEnabled Settings not found"
            android.util.Log.e(r1, r4, r0)
        L_0x034b:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r5)
            java.lang.String r1 = "is_sideload_enabled"
            r3.addProperty(r1, r0)
            goto L_0x0367
        L_0x0355:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            java.lang.String r1 = "volume_level"
            r3.addProperty(r1, r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            java.lang.String r1 = "sound_enabled"
            r3.addProperty(r1, r0)
        L_0x0367:
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r1 = "sd_card_available"
            r3.addProperty(r1, r0)
            java.lang.String r0 = android.os.Build.FINGERPRINT
            java.lang.String r1 = "os_name"
            r3.addProperty(r1, r0)
            java.lang.String r0 = "vduid"
            r3.addProperty(r0, r8)
            com.google.gson.JsonObject r0 = r15.deviceBody
            java.lang.String r1 = "ext"
            com.google.gson.JsonObject r0 = r0.getAsJsonObject(r1)
            java.lang.String r1 = "vungle"
            com.google.gson.JsonObject r0 = r0.getAsJsonObject(r1)
            java.lang.String r1 = android.os.Build.MANUFACTURER
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x039f
            java.lang.String r1 = "amazon"
            goto L_0x03a1
        L_0x039f:
            java.lang.String r1 = "android"
        L_0x03a1:
            r0.add(r1, r3)
            com.google.gson.JsonObject r0 = r15.deviceBody
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.VungleApiClient.getDeviceBody():com.google.gson.JsonObject");
    }

    private JsonObject getUserBody() {
        long j;
        String str;
        String str2;
        String str3;
        if (this.userBody == null) {
            this.userBody = new JsonObject();
        }
        Cookie cookie = (Cookie) this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get();
        if (cookie != null) {
            str2 = cookie.getString("consent_status");
            str = cookie.getString("consent_source");
            j = cookie.getLong("timestamp").longValue();
            str3 = cookie.getString("consent_message_version");
        } else {
            j = 0;
            str2 = "unknown";
            str = "no_interaction";
            str3 = "";
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("consent_status", str2);
        jsonObject.addProperty("consent_source", str);
        jsonObject.addProperty("consent_timestamp", Long.valueOf(j));
        if (TextUtils.isEmpty(str3)) {
            str3 = "";
        }
        jsonObject.addProperty("consent_message_version", str3);
        this.userBody.add("gdpr", jsonObject);
        return this.userBody;
    }

    public boolean getMoatEnabled() {
        return this.enableMoat && Build.VERSION.SDK_INT >= 16;
    }

    private String getUserAgentFromCookie() {
        Cookie cookie = (Cookie) this.repository.load(Cookie.USER_AGENT_ID_COOKIE, Cookie.class).get();
        if (cookie == null) {
            return System.getProperty("http.agent");
        }
        String string = cookie.getString(Cookie.USER_AGENT_ID_COOKIE);
        return TextUtils.isEmpty(string) ? System.getProperty("http.agent") : string;
    }

    /* access modifiers changed from: private */
    public void addUserAgentInCookie(String str) throws DatabaseHelper.DBException {
        Cookie cookie = new Cookie(Cookie.USER_AGENT_ID_COOKIE);
        cookie.putValue(Cookie.USER_AGENT_ID_COOKIE, str);
        this.repository.save(cookie);
    }

    public long getRetryAfterHeaderValue(retrofit2.Response<JsonObject> response) {
        try {
            return Long.parseLong(response.headers().get(HttpHeaders.RETRY_AFTER)) * 1000;
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    public static class ClearTextTrafficException extends IOException {
        ClearTextTrafficException(String str) {
            super(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void overrideApi(VungleApi vungleApi) {
        this.api = vungleApi;
    }
}
