package com.vungle.warren;

public class RuntimeValues {
    volatile HeaderBiddingCallback headerBiddingCallback;
    volatile InitCallback initCallback;
    volatile PublisherDirectDownload publisherDirectDownload;
    volatile VungleSettings settings;
}
