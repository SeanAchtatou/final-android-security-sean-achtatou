package com.vungle.warren.ui.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.ironsource.sdk.constants.Constants;
import com.vungle.warren.AdvertisementPresentationFactory;
import com.vungle.warren.DirectDownloadAdapter;
import com.vungle.warren.VungleNativeAd;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.ui.CloseDelegate;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.ui.OrientationDelegate;
import com.vungle.warren.ui.contract.AdContract;
import com.vungle.warren.ui.contract.WebAdContract;
import com.vungle.warren.utility.ExternalRouter;
import java.util.concurrent.atomic.AtomicReference;

public class VungleNativeView extends WebView implements WebAdContract.WebAdView, VungleNativeAd {
    private static final String TAG = VungleNativeView.class.getName();
    private BroadcastReceiver broadcastReceiver;
    private final DirectDownloadAdapter directDownloadAdapter;
    /* access modifiers changed from: private */
    public AtomicReference<Boolean> isAdVisible = new AtomicReference<>();
    /* access modifiers changed from: private */
    public final AdContract.AdvertisementPresenter.EventListener listener;
    /* access modifiers changed from: private */
    public final String placementID;
    /* access modifiers changed from: private */
    public WebAdContract.WebAdPresenter presenter;
    AdvertisementPresentationFactory presenterFactory;

    public View renderNativeView() {
        return this;
    }

    public void setImmersiveMode() {
    }

    public void setOrientation(int i) {
    }

    public void setPresenter(WebAdContract.WebAdPresenter webAdPresenter) {
    }

    public void updateWindow(boolean z) {
    }

    public void onResume() {
        super.onResume();
        Log.d(TAG, "Resuming Flex");
        setAdVisibility(true);
    }

    public void onPause() {
        super.onPause();
        setAdVisibility(false);
    }

    public VungleNativeView(Context context, String str, DirectDownloadAdapter directDownloadAdapter2, AdContract.AdvertisementPresenter.EventListener eventListener) {
        super(context);
        this.listener = eventListener;
        this.placementID = str;
        this.directDownloadAdapter = directDownloadAdapter2;
        try {
            this.presenterFactory = new AdvertisementPresentationFactory(str, null, new OrientationDelegate() {
                public void setOrientation(int i) {
                }
            }, new CloseDelegate() {
                public void close() {
                    VungleNativeView.this.finishDisplayingAd();
                }
            });
        } catch (InstantiationException e) {
            String str2 = TAG;
            Log.e(str2, "error on creating presentation factory" + e.getLocalizedMessage());
        }
        setLayerType(2, null);
        if (directDownloadAdapter2 != null) {
            directDownloadAdapter2.getSdkDownloadClient().setAdWebView(this);
        }
    }

    /* access modifiers changed from: private */
    public void prepare(Bundle bundle) {
        WebSettingsUtils.applyDefault(this);
        addJavascriptInterface(new JavascriptBridge(this.presenter), Constants.JAVASCRIPT_INTERFACE_NAME);
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        if (Build.VERSION.SDK_INT < 17) {
            setAdVisibility(true);
        } else {
            getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
    }

    public void finishDisplayingAd() {
        WebAdContract.WebAdPresenter webAdPresenter = this.presenter;
        if (webAdPresenter != null) {
            webAdPresenter.stop(false, true);
        } else {
            AdvertisementPresentationFactory advertisementPresentationFactory = this.presenterFactory;
            if (advertisementPresentationFactory != null) {
                advertisementPresentationFactory.destroy();
                this.presenterFactory = null;
                this.listener.onError(new VungleException(25), this.placementID);
            }
        }
        destroyAdView();
    }

    public void setAdVisibility(boolean z) {
        WebAdContract.WebAdPresenter webAdPresenter = this.presenter;
        if (webAdPresenter != null) {
            webAdPresenter.setAdVisibility(z);
        } else {
            this.isAdVisible.set(Boolean.valueOf(z));
        }
    }

    public void showWebsite(String str) {
        loadUrl(str);
    }

    public String getWebsiteUrl() {
        return getUrl();
    }

    public void close() {
        WebAdContract.WebAdPresenter webAdPresenter = this.presenter;
        if (webAdPresenter == null) {
            AdvertisementPresentationFactory advertisementPresentationFactory = this.presenterFactory;
            if (advertisementPresentationFactory != null) {
                advertisementPresentationFactory.destroy();
                this.presenterFactory = null;
                this.listener.onError(new VungleException(25), this.placementID);
            }
        } else if (webAdPresenter.handleExit(null)) {
            finishDisplayingAd();
        }
    }

    public void destroyAdView() {
        removeJavascriptInterface(Constants.JAVASCRIPT_INTERFACE_NAME);
        loadUrl("about:blank");
    }

    public void showCloseButton() {
        throw new UnsupportedOperationException("VungleNativeView does not implement a close button");
    }

    public void open(String str) {
        String str2 = TAG;
        Log.d(str2, "Opening " + str);
        if (!ExternalRouter.launch(str, getContext())) {
            String str3 = TAG;
            Log.e(str3, "Cannot open url " + str);
        }
    }

    public void showDialog(String str, String str2, String str3, String str4, DialogInterface.OnClickListener onClickListener) {
        throw new UnsupportedOperationException("VungleNativeView does not implement a dialog.");
    }

    public void resumeWeb() {
        onResume();
    }

    public void pauseWeb() {
        onPause();
    }

    public void setVisibility(boolean z) {
        setVisibility(z ? 0 : 4);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        AdvertisementPresentationFactory advertisementPresentationFactory = this.presenterFactory;
        if (advertisementPresentationFactory != null && this.presenter == null) {
            advertisementPresentationFactory.getNativeViewPresentation(this.directDownloadAdapter, new AdvertisementPresentationFactory.ViewCallback() {
                public void onResult(Pair<WebAdContract.WebAdPresenter, VungleWebClient> pair, Exception exc) {
                    if (pair == null || exc != null) {
                        VungleNativeView.this.listener.onError(new VungleException(10), VungleNativeView.this.placementID);
                        return;
                    }
                    WebAdContract.WebAdPresenter unused = VungleNativeView.this.presenter = (WebAdContract.WebAdPresenter) pair.first;
                    VungleNativeView.this.setWebViewClient((VungleWebClient) pair.second);
                    VungleNativeView.this.presenter.setEventListener(VungleNativeView.this.listener);
                    VungleNativeView.this.presenter.attach(VungleNativeView.this, null);
                    VungleNativeView.this.prepare(null);
                    if (VungleNativeView.this.isAdVisible.get() != null) {
                        VungleNativeView vungleNativeView = VungleNativeView.this;
                        vungleNativeView.setAdVisibility(((Boolean) vungleNativeView.isAdVisible.get()).booleanValue());
                    }
                }
            });
        }
        this.broadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String stringExtra = intent.getStringExtra(AdContract.AdvertisementBus.COMMAND);
                if (((stringExtra.hashCode() == -1884364225 && stringExtra.equals(AdContract.AdvertisementBus.STOP_ALL)) ? (char) 0 : 65535) == 0) {
                    VungleNativeView.this.finishDisplayingAd();
                }
            }
        };
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(this.broadcastReceiver, new IntentFilter(AdContract.AdvertisementBus.ACTION));
        resumeWeb();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(this.broadcastReceiver);
        super.onDetachedFromWindow();
        AdvertisementPresentationFactory advertisementPresentationFactory = this.presenterFactory;
        if (advertisementPresentationFactory != null) {
            advertisementPresentationFactory.destroy();
        }
        pauseWeb();
    }
}
