package com.vungle.warren.ui.presenter;

import android.content.ActivityNotFoundException;
import android.text.TextUtils;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.facebook.internal.ServerProtocol;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.vungle.warren.DirectDownloadAdapter;
import com.vungle.warren.analytics.AdAnalytics;
import com.vungle.warren.download.APKDirectDownloadManager;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.model.Report;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.ui.contract.AdContract;
import com.vungle.warren.ui.contract.WebAdContract;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.WebViewAPI;
import com.vungle.warren.utility.AsyncFileUtils;
import com.vungle.warren.utility.Scheduler;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

public class MRAIDAdPresenter implements WebAdContract.WebAdPresenter, WebViewAPI.MRAIDDelegate, WebViewAPI.WebClientErrorListener {
    private static final String ACTION = "action";
    private static final String ACTION_WITH_VALUE = "actionWithValue";
    private static final String CANCEL_DOWNLOAD = "cancelDownload";
    private static final String CLOSE = "close";
    private static final String CONSENT_ACTION = "consentAction";
    private static final String EXTRA_INCENTIVIZED_SENT = "incentivized_sent";
    private static String EXTRA_REPORT = "saved_report";
    private static final String EXTRA_WEB_READY = "web_ready";
    protected static final double NINE_BY_SIXTEEN_ASPECT_RATIO = 0.5625d;
    private static final String OPEN = "open";
    private static final String OPEN_APP_IN_DEVICE = "openAppInDevice";
    private static final String OPEN_PRIVACY = "openPrivacy";
    private static final String START_DOWNLOAD_APP_ON_DEVICE = "startDownloadAppOnDevice";
    private static final String SUCCESSFUL_VIEW = "successfulView";
    private static final String TAG = MRAIDAdPresenter.class.getCanonicalName();
    private static final String TPAT = "tpat";
    private static final String USE_CUSTOM_CLOSE = "useCustomClose";
    private static final String USE_CUSTOM_PRIVACY = "useCustomPrivacy";
    private static final String VIDEO_VIEWED = "videoViewed";
    private long adStartTime;
    /* access modifiers changed from: private */
    public WebAdContract.WebAdView adView;
    private Advertisement advertisement;
    private final AdAnalytics analytics;
    private File assetDir;
    /* access modifiers changed from: private */
    public boolean backEnabled;
    /* access modifiers changed from: private */
    public AdContract.AdvertisementPresenter.EventListener bus;
    private Map<String, Cookie> cookieMap = new HashMap();
    private DirectDownloadAdapter directDownloadAdapter;
    private boolean directDownloadApkEnabled;
    private long duration;
    private boolean hasSend80Percent = false;
    private boolean hasSendStart = false;
    private final ExecutorService ioExecutor;
    private AtomicBoolean isDestroying = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public Placement placement;
    /* access modifiers changed from: private */
    public Repository.SaveCallback repoCallback = new Repository.SaveCallback() {
        boolean errorHappened = false;

        public void onSaved() {
        }

        public void onError(Exception exc) {
            if (!this.errorHappened) {
                this.errorHappened = true;
                if (MRAIDAdPresenter.this.bus != null) {
                    MRAIDAdPresenter.this.bus.onError(new VungleException(26), MRAIDAdPresenter.this.placement.getId());
                }
                MRAIDAdPresenter.this.closeView();
            }
        }
    };
    /* access modifiers changed from: private */
    public Report report;
    /* access modifiers changed from: private */
    public Repository repository;
    private final Scheduler scheduler;
    private AtomicBoolean sendReportIncentivized = new AtomicBoolean(false);
    private final ExecutorService uiExecutor;
    private WebViewAPI webClient;

    public MRAIDAdPresenter(Advertisement advertisement2, Placement placement2, Repository repository2, Scheduler scheduler2, AdAnalytics adAnalytics, WebViewAPI webViewAPI, DirectDownloadAdapter directDownloadAdapter2, OptionsState optionsState, File file, ExecutorService executorService, ExecutorService executorService2) {
        this.advertisement = advertisement2;
        this.repository = repository2;
        this.placement = placement2;
        this.scheduler = scheduler2;
        this.analytics = adAnalytics;
        this.webClient = webViewAPI;
        this.directDownloadAdapter = directDownloadAdapter2;
        this.assetDir = file;
        this.ioExecutor = executorService;
        this.uiExecutor = executorService2;
        load(optionsState);
    }

    public void setEventListener(AdContract.AdvertisementPresenter.EventListener eventListener) {
        this.bus = eventListener;
    }

    public void reportAction(String str, String str2) {
        if (str.equals("videoLength")) {
            this.duration = Long.parseLong(str2);
            this.report.setVideoLength(this.duration);
            this.repository.save(this.report, this.repoCallback);
            return;
        }
        this.report.recordAction(str, str2, System.currentTimeMillis());
        this.repository.save(this.report, this.repoCallback);
    }

    public void onViewConfigurationChanged() {
        this.adView.updateWindow(this.advertisement.getTemplateType().equals("flexview"));
        this.webClient.notifyPropertiesChange(true);
    }

    public void attach(WebAdContract.WebAdView webAdView, OptionsState optionsState) {
        boolean z = false;
        this.isDestroying.set(false);
        this.adView = webAdView;
        webAdView.setPresenter(this);
        int settings = this.advertisement.getAdConfig().getSettings();
        if (settings > 0) {
            this.backEnabled = (settings & 2) == 2;
            if ((settings & 32) == 32) {
                z = true;
            }
            this.directDownloadApkEnabled = z;
        }
        int i = 4;
        if ((this.advertisement.getAdConfig().getSettings() & 16) != 16) {
            int orientation = this.advertisement.getOrientation();
            if (orientation == 0) {
                i = 7;
            } else if (orientation == 1) {
                i = 6;
            } else if (orientation != 2) {
                i = -1;
            }
        }
        webAdView.setOrientation(i);
        prepare(optionsState);
    }

    public void detach(boolean z) {
        stop(z, true);
        this.adView.destroyAdView();
    }

    private void prepare(OptionsState optionsState) {
        this.webClient.setMRAIDDelegate(this);
        this.webClient.setDownloadAdapter(this.directDownloadAdapter);
        this.webClient.setErrorListener(this);
        loadMraid(new File(this.assetDir.getPath() + File.separator + "template"));
        Cookie cookie = this.cookieMap.get(Cookie.INCENTIVIZED_TEXT_COOKIE);
        if ("flexview".equals(this.advertisement.getTemplateType()) && this.advertisement.getAdConfig().getFlexViewCloseTime() > 0) {
            this.scheduler.schedule(new Runnable() {
                public void run() {
                    long currentTimeMillis = System.currentTimeMillis();
                    MRAIDAdPresenter.this.report.recordAction("mraidCloseByTimer", "", currentTimeMillis);
                    MRAIDAdPresenter.this.report.recordAction("mraidClose", "", currentTimeMillis);
                    MRAIDAdPresenter.this.repository.save(MRAIDAdPresenter.this.report, MRAIDAdPresenter.this.repoCallback);
                    MRAIDAdPresenter.this.closeView();
                }
            }, (long) (this.advertisement.getAdConfig().getFlexViewCloseTime() * 1000));
        }
        String string = cookie == null ? null : cookie.getString("userID");
        if (this.report == null) {
            this.adStartTime = System.currentTimeMillis();
            this.report = new Report(this.advertisement, this.placement, this.adStartTime, string);
            this.repository.save(this.report, this.repoCallback);
        }
        Cookie cookie2 = this.cookieMap.get(Cookie.CONSENT_COOKIE);
        if (cookie2 != null) {
            boolean z = cookie2.getBoolean("is_country_data_protected").booleanValue() && "unknown".equals(cookie2.getString("consent_status"));
            this.webClient.setConsentStatus(z, cookie2.getString("consent_title"), cookie2.getString("consent_message"), cookie2.getString("button_accept"), cookie2.getString("button_deny"));
            if (z) {
                cookie2.putValue("consent_status", "opted_out_by_timeout");
                cookie2.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                cookie2.putValue("consent_source", "vungle_modal");
                this.repository.save(cookie2, this.repoCallback);
            }
        }
        int showCloseDelay = this.advertisement.getShowCloseDelay(this.placement.isIncentivized());
        if (showCloseDelay > 0) {
            this.scheduler.schedule(new Runnable() {
                public void run() {
                    boolean unused = MRAIDAdPresenter.this.backEnabled = true;
                }
            }, (long) showCloseDelay);
        } else {
            this.backEnabled = true;
        }
        this.adView.updateWindow(this.advertisement.getTemplateType().equals("flexview"));
        AdContract.AdvertisementPresenter.EventListener eventListener = this.bus;
        if (eventListener != null) {
            eventListener.onNext("start", null, this.placement.getId());
        }
    }

    private void loadMraid(File file) {
        File file2 = new File(file.getParent());
        final File file3 = new File(file2.getPath() + File.separator + "index.html");
        new AsyncFileUtils(this.ioExecutor, this.uiExecutor).isFileExistAsync(file3, new AsyncFileUtils.FileExist() {
            public void status(boolean z) {
                if (!z) {
                    if (MRAIDAdPresenter.this.bus != null) {
                        MRAIDAdPresenter.this.bus.onError(new VungleException(10), MRAIDAdPresenter.this.placement.getId());
                    }
                    MRAIDAdPresenter.this.adView.close();
                    return;
                }
                WebAdContract.WebAdView access$700 = MRAIDAdPresenter.this.adView;
                access$700.showWebsite(Advertisement.FILE_SCHEME + file3.getPath());
            }
        });
    }

    public void start() {
        this.webClient.setAdVisibility(true);
        this.adView.setImmersiveMode();
        this.adView.resumeWeb();
        setAdVisibility(true);
    }

    public void stop(boolean z, boolean z2) {
        this.adView.pauseWeb();
        setAdVisibility(false);
        if (!z && z2 && !this.isDestroying.getAndSet(true)) {
            WebViewAPI webViewAPI = this.webClient;
            String str = null;
            if (webViewAPI != null) {
                webViewAPI.setMRAIDDelegate(null);
            }
            AdContract.AdvertisementPresenter.EventListener eventListener = this.bus;
            if (eventListener != null) {
                if (this.report.isCTAClicked()) {
                    str = "isCTAClicked";
                }
                eventListener.onNext("end", str, this.placement.getId());
            }
            DirectDownloadAdapter directDownloadAdapter2 = this.directDownloadAdapter;
            if (directDownloadAdapter2 != null) {
                directDownloadAdapter2.getSdkDownloadClient().cleanUp();
            }
        }
    }

    public void setAdVisibility(boolean z) {
        this.webClient.setAdVisibility(z);
    }

    public void generateSaveState(OptionsState optionsState) {
        if (optionsState != null) {
            this.repository.save(this.report, this.repoCallback);
            optionsState.put(EXTRA_REPORT, this.report.getId());
            optionsState.put(EXTRA_INCENTIVIZED_SENT, this.sendReportIncentivized.get());
        }
    }

    public void restoreFromSave(OptionsState optionsState) {
        if (optionsState != null) {
            boolean z = optionsState.getBoolean(EXTRA_INCENTIVIZED_SENT, false);
            if (z) {
                this.sendReportIncentivized.set(z);
            }
            if (this.report == null) {
                this.adView.close();
            }
        }
    }

    public boolean handleExit(String str) {
        Placement placement2;
        if (str == null) {
            if (this.backEnabled) {
                this.adView.showWebsite("javascript:window.vungle.mraidBridgeExt.requestMRAIDClose()");
            }
            return false;
        } else if (this.advertisement == null || (placement2 = this.placement) == null) {
            Log.e(TAG, "Unable to close advertisement");
            return false;
        } else if (!placement2.getId().equals(str)) {
            Log.e(TAG, "Cannot close FlexView Ad with invalid placement reference id");
            return false;
        } else if (!"flexview".equals(this.advertisement.getTemplateType())) {
            Log.e(TAG, "Cannot close a Non FlexView ad");
            return false;
        } else {
            this.adView.showWebsite("javascript:window.vungle.mraidBridgeExt.requestMRAIDClose()");
            reportAction("mraidCloseByApi", null);
            return true;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean processCommand(String str, JsonObject jsonObject) {
        char c;
        int i;
        String str2;
        String str3;
        char c2 = 65535;
        switch (str.hashCode()) {
            case -1912374177:
                if (str.equals(SUCCESSFUL_VIEW)) {
                    c = 9;
                    break;
                }
                c = 65535;
                break;
            case -1891064718:
                if (str.equals(OPEN_APP_IN_DEVICE)) {
                    c = 12;
                    break;
                }
                c = 65535;
                break;
            case -1422950858:
                if (str.equals("action")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case -1382780692:
                if (str.equals(START_DOWNLOAD_APP_ON_DEVICE)) {
                    c = 10;
                    break;
                }
                c = 65535;
                break;
            case -735200587:
                if (str.equals(ACTION_WITH_VALUE)) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case -660787472:
                if (str.equals(CONSENT_ACTION)) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case -511324706:
                if (str.equals(OPEN_PRIVACY)) {
                    c = 8;
                    break;
                }
                c = 65535;
                break;
            case -503430878:
                if (str.equals(CANCEL_DOWNLOAD)) {
                    c = 11;
                    break;
                }
                c = 65535;
                break;
            case -348095344:
                if (str.equals(USE_CUSTOM_PRIVACY)) {
                    c = 7;
                    break;
                }
                c = 65535;
                break;
            case 3417674:
                if (str.equals(OPEN)) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            case 3566511:
                if (str.equals(TPAT)) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 94756344:
                if (str.equals("close")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 1614272768:
                if (str.equals(USE_CUSTOM_CLOSE)) {
                    c = 6;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                reportAction("mraidClose", null);
                closeView();
                return true;
            case 1:
                Cookie cookie = this.cookieMap.get(Cookie.CONSENT_COOKIE);
                if (cookie == null) {
                    cookie = new Cookie(Cookie.CONSENT_COOKIE);
                }
                cookie.putValue("consent_status", jsonObject.get(NotificationCompat.CATEGORY_EVENT).getAsString());
                cookie.putValue("consent_source", "vungle_modal");
                cookie.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                this.repository.save(cookie, this.repoCallback);
                return true;
            case 2:
                String asString = jsonObject.get(NotificationCompat.CATEGORY_EVENT).getAsString();
                String asString2 = jsonObject.get("value").getAsString();
                this.report.recordAction(asString, asString2, System.currentTimeMillis());
                this.repository.save(this.report, this.repoCallback);
                if (asString.equals("videoViewed") && this.duration > 0) {
                    try {
                        i = (int) ((Float.parseFloat(asString2) / ((float) this.duration)) * 100.0f);
                    } catch (NumberFormatException unused) {
                        Log.e(TAG, "value for videoViewed is null !");
                        i = 0;
                    }
                    if (i > 0) {
                        AdContract.AdvertisementPresenter.EventListener eventListener = this.bus;
                        if (eventListener != null) {
                            String str4 = "percentViewed:" + i;
                            Placement placement2 = this.placement;
                            if (placement2 == null) {
                                str2 = null;
                            } else {
                                str2 = placement2.getId();
                            }
                            eventListener.onNext(str4, null, str2);
                        }
                        if (!this.hasSendStart && i > 1) {
                            this.hasSendStart = true;
                            DirectDownloadAdapter directDownloadAdapter2 = this.directDownloadAdapter;
                            if (directDownloadAdapter2 != null) {
                                directDownloadAdapter2.getSdkDownloadClient().sendADDisplayingNotify(false, DirectDownloadAdapter.CONTRACT_TYPE.CPI);
                            }
                        }
                        if (!this.hasSend80Percent && i > 80) {
                            this.hasSend80Percent = true;
                            DirectDownloadAdapter directDownloadAdapter3 = this.directDownloadAdapter;
                            if (directDownloadAdapter3 != null) {
                                directDownloadAdapter3.getSdkDownloadClient().sendADDisplayingNotify(true, DirectDownloadAdapter.CONTRACT_TYPE.CPI);
                            }
                        }
                        Cookie cookie2 = this.cookieMap.get(Cookie.CONFIG_COOKIE);
                        if (this.placement.isIncentivized() && i > 75 && cookie2 != null && cookie2.getBoolean("isReportIncentivizedEnabled").booleanValue() && !this.sendReportIncentivized.getAndSet(true)) {
                            JsonObject jsonObject2 = new JsonObject();
                            jsonObject2.add("placement_reference_id", new JsonPrimitive(this.placement.getId()));
                            jsonObject2.add("app_id", new JsonPrimitive(this.advertisement.getAppID()));
                            jsonObject2.add(ReportDBAdapter.ReportColumns.COLUMN_AD_START_TIME, new JsonPrimitive((Number) Long.valueOf(this.report.getAdStartTime())));
                            jsonObject2.add("user", new JsonPrimitive(this.report.getUserID()));
                            this.analytics.ri(jsonObject2);
                        }
                    }
                }
                if (asString.equals("videoLength")) {
                    this.duration = Long.parseLong(asString2);
                    reportAction("videoLength", asString2);
                    this.webClient.notifyPropertiesChange(true);
                }
                this.adView.setVisibility(true);
                return true;
            case 3:
                this.analytics.ping(this.advertisement.getTpatUrls(jsonObject.get(NotificationCompat.CATEGORY_EVENT).getAsString()));
                return true;
            case 4:
                break;
            case 5:
                reportAction(JavascriptBridge.MraidHandler.DOWNLOAD_ACTION, null);
                reportAction("mraidOpen", null);
                String asString3 = jsonObject.get("url").getAsString();
                if (!APKDirectDownloadManager.isDirectDownloadEnabled(this.directDownloadApkEnabled, this.advertisement.isRequiresNonMarketInstall())) {
                    this.adView.open(asString3);
                    break;
                } else {
                    APKDirectDownloadManager.download(asString3);
                    break;
                }
            case 6:
                String asString4 = jsonObject.get("sdkCloseButton").getAsString();
                int hashCode = asString4.hashCode();
                if (hashCode != -1901805651) {
                    if (hashCode != 3178655) {
                        if (hashCode == 466743410 && asString4.equals("visible")) {
                            c2 = 2;
                        }
                    } else if (asString4.equals("gone")) {
                        c2 = 0;
                    }
                } else if (asString4.equals("invisible")) {
                    c2 = 1;
                }
                if (c2 == 0 || c2 == 1 || c2 == 2) {
                    return true;
                }
                throw new IllegalArgumentException("Unknown value " + asString4);
            case 7:
                String asString5 = jsonObject.get(USE_CUSTOM_PRIVACY).getAsString();
                int hashCode2 = asString5.hashCode();
                if (hashCode2 != 3178655) {
                    if (hashCode2 != 3569038) {
                        if (hashCode2 == 97196323 && asString5.equals("false")) {
                            c2 = 2;
                        }
                    } else if (asString5.equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
                        c2 = 1;
                    }
                } else if (asString5.equals("gone")) {
                    c2 = 0;
                }
                if (c2 == 0 || c2 == 1 || c2 == 2) {
                    return true;
                }
                throw new IllegalArgumentException("Unknown value " + asString5);
            case 8:
                this.adView.open(jsonObject.get("url").getAsString());
                return true;
            case 9:
                AdContract.AdvertisementPresenter.EventListener eventListener2 = this.bus;
                if (eventListener2 != null) {
                    Placement placement3 = this.placement;
                    if (placement3 == null) {
                        str3 = null;
                    } else {
                        str3 = placement3.getId();
                    }
                    eventListener2.onNext(SUCCESSFUL_VIEW, null, str3);
                }
                Cookie cookie3 = this.cookieMap.get(Cookie.CONFIG_COOKIE);
                if (this.placement.isIncentivized() && cookie3 != null && cookie3.getBoolean("isReportIncentivizedEnabled").booleanValue() && !this.sendReportIncentivized.getAndSet(true)) {
                    JsonObject jsonObject3 = new JsonObject();
                    jsonObject3.add("placement_reference_id", new JsonPrimitive(this.placement.getId()));
                    jsonObject3.add("app_id", new JsonPrimitive(this.advertisement.getAppID()));
                    jsonObject3.add(ReportDBAdapter.ReportColumns.COLUMN_AD_START_TIME, new JsonPrimitive((Number) Long.valueOf(this.report.getAdStartTime())));
                    jsonObject3.add("user", new JsonPrimitive(this.report.getUserID()));
                    this.analytics.ri(jsonObject3);
                }
                return true;
            case 10:
                DirectDownloadAdapter directDownloadAdapter4 = this.directDownloadAdapter;
                if (directDownloadAdapter4 != null) {
                    directDownloadAdapter4.getSdkDownloadClient().sendDownloadRequest();
                }
                return true;
            case 11:
                DirectDownloadAdapter directDownloadAdapter5 = this.directDownloadAdapter;
                if (directDownloadAdapter5 != null) {
                    directDownloadAdapter5.getSdkDownloadClient().cancelDownloadRequest();
                }
                return true;
            case 12:
                DirectDownloadAdapter directDownloadAdapter6 = this.directDownloadAdapter;
                if (directDownloadAdapter6 != null) {
                    directDownloadAdapter6.getSdkDownloadClient().sendOpenPackageRequest();
                }
                return true;
            default:
                return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0057  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMraidAction(java.lang.String r5) {
        /*
            r4 = this;
            int r0 = r5.hashCode()
            r1 = -314498168(0xffffffffed412388, float:-3.7358476E27)
            r2 = 2
            r3 = 1
            if (r0 == r1) goto L_0x002a
            r1 = 94756344(0x5a5ddf8, float:1.5598064E-35)
            if (r0 == r1) goto L_0x0020
            r1 = 1427818632(0x551ac888, float:1.06366291E13)
            if (r0 == r1) goto L_0x0016
            goto L_0x0034
        L_0x0016:
            java.lang.String r0 = "download"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = 1
            goto L_0x0035
        L_0x0020:
            java.lang.String r0 = "close"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = 0
            goto L_0x0035
        L_0x002a:
            java.lang.String r0 = "privacy"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = 2
            goto L_0x0035
        L_0x0034:
            r0 = -1
        L_0x0035:
            if (r0 == 0) goto L_0x0057
            if (r0 == r3) goto L_0x0053
            if (r0 != r2) goto L_0x003c
            goto L_0x005a
        L_0x003c:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown action "
            r1.append(r2)
            r1.append(r5)
            java.lang.String r5 = r1.toString()
            r0.<init>(r5)
            throw r0
        L_0x0053:
            r4.download()
            goto L_0x005a
        L_0x0057:
            r4.closeView()
        L_0x005a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.ui.presenter.MRAIDAdPresenter.onMraidAction(java.lang.String):void");
    }

    private void download() {
        reportAction("cta", "");
        try {
            this.analytics.ping(new String[]{this.advertisement.getCTAURL(true)});
            this.adView.open(this.advertisement.getCTAURL(false));
        } catch (ActivityNotFoundException unused) {
        }
    }

    /* access modifiers changed from: private */
    public void closeView() {
        this.report.setAdDuration((int) (System.currentTimeMillis() - this.adStartTime));
        this.repository.save(this.report, this.repoCallback);
        this.adView.close();
        this.scheduler.cancelAll();
    }

    private void load(OptionsState optionsState) {
        this.cookieMap.put(Cookie.INCENTIVIZED_TEXT_COOKIE, this.repository.load(Cookie.INCENTIVIZED_TEXT_COOKIE, Cookie.class).get());
        this.cookieMap.put(Cookie.CONSENT_COOKIE, this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get());
        this.cookieMap.put(Cookie.CONFIG_COOKIE, this.repository.load(Cookie.CONFIG_COOKIE, Cookie.class).get());
        if (optionsState != null) {
            String string = optionsState.getString(EXTRA_REPORT);
            this.report = TextUtils.isEmpty(string) ? null : (Report) this.repository.load(string, Report.class).get();
        }
    }

    public void onReceivedError(String str) {
        Report report2 = this.report;
        if (report2 != null) {
            report2.recordError(str);
            this.repository.save(this.report, this.repoCallback);
        }
    }
}
