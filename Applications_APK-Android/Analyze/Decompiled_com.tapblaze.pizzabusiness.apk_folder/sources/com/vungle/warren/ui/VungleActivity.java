package com.vungle.warren.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.ironsource.sdk.constants.Constants;
import com.vungle.warren.AdvertisementPresentationFactory;
import com.vungle.warren.Vungle;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.ui.contract.AdContract;
import com.vungle.warren.ui.state.BundleOptionsState;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.FullAdWidget;
import java.util.concurrent.atomic.AtomicBoolean;

public class VungleActivity extends Activity {
    public static final String PLACEMENT_EXTRA = "placement";
    public static final String PRESENTER_STATE = "presenter_state";
    private static final String TAG = "VungleActivity";
    /* access modifiers changed from: private */
    public static AdContract.AdvertisementPresenter.EventListener bus;
    private BroadcastReceiver broadcastReceiver;
    private AdvertisementPresentationFactory.FullScreenCallback fullscreenCallback = new AdvertisementPresentationFactory.FullScreenCallback() {
        public void onResult(Pair<AdContract.AdView, AdContract.AdvertisementPresenter> pair, Exception exc) {
            if (pair == null || exc != null) {
                VungleActivity vungleActivity = VungleActivity.this;
                vungleActivity.deliverError(10, vungleActivity.placementId);
                VungleActivity.this.finish();
                return;
            }
            AdContract.AdvertisementPresenter unused = VungleActivity.this.presenter = (AdContract.AdvertisementPresenter) pair.second;
            VungleActivity.this.presenter.setEventListener(VungleActivity.bus);
            VungleActivity.this.presenter.attach((AdContract.AdView) pair.first, VungleActivity.this.state);
            if (VungleActivity.this.pendingStart.getAndSet(false)) {
                VungleActivity.this.presenter.start();
            }
        }
    };
    /* access modifiers changed from: private */
    public AtomicBoolean pendingStart = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public String placementId;
    /* access modifiers changed from: private */
    public AdContract.AdvertisementPresenter presenter;
    private AdvertisementPresentationFactory presenterFactory;
    /* access modifiers changed from: private */
    public OptionsState state;

    /* access modifiers changed from: protected */
    public boolean canRotate() {
        return true;
    }

    public static void setEventListener(AdContract.AdvertisementPresenter.EventListener eventListener) {
        bus = eventListener;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(16777216, 16777216);
        this.placementId = getIntent().getStringExtra("placement");
        try {
            FullAdWidget fullAdWidget = new FullAdWidget(this, getWindow());
            if (!Vungle.isInitialized() || bus == null) {
                finish();
                return;
            }
            try {
                this.presenterFactory = new AdvertisementPresentationFactory(this.placementId, bundle, new OrientationDelegate() {
                    public void setOrientation(int i) {
                        VungleActivity.this.setRequestedOrientation(i);
                    }
                }, new CloseDelegate() {
                    public void close() {
                        VungleActivity.this.finish();
                    }
                });
                this.state = bundle == null ? null : (OptionsState) bundle.getParcelable(PRESENTER_STATE);
                this.presenterFactory.getFullScreenPresentation(this, fullAdWidget, this.state, this.fullscreenCallback);
                setContentView(fullAdWidget, fullAdWidget.getLayoutParams());
                connectBroadcastReceiver();
            } catch (InstantiationException e) {
                Log.e(TAG, "error on crreating presentations" + e.getLocalizedMessage());
                deliverError(10, this.placementId);
                finish();
            }
        } catch (InstantiationException e2) {
            AdContract.AdvertisementPresenter.EventListener eventListener = bus;
            if (eventListener != null) {
                eventListener.onError(e2, this.placementId);
            }
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String stringExtra = getIntent().getStringExtra("placement");
        String stringExtra2 = intent.getStringExtra("placement");
        if (stringExtra != null && stringExtra2 != null && !stringExtra.equals(stringExtra2)) {
            Log.d(TAG, "Tried to play another placement " + stringExtra2 + " while playing " + stringExtra);
            deliverError(15, stringExtra2);
        }
    }

    /* access modifiers changed from: private */
    public void deliverError(int i, String str) {
        AdContract.AdvertisementPresenter.EventListener eventListener = bus;
        if (eventListener != null) {
            eventListener.onError(new VungleException(i), str);
        }
    }

    private void connectBroadcastReceiver() {
        this.broadcastReceiver = new BroadcastReceiver() {
            /* JADX WARNING: Removed duplicated region for block: B:12:0x002d  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x004c  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void onReceive(android.content.Context r4, android.content.Intent r5) {
                /*
                    r3 = this;
                    java.lang.String r4 = "command"
                    java.lang.String r4 = r5.getStringExtra(r4)
                    int r0 = r4.hashCode()
                    r1 = -1884364225(0xffffffff8faee23f, float:-1.7244872E-29)
                    r2 = 1
                    if (r0 == r1) goto L_0x0020
                    r1 = -482896367(0xffffffffe3379611, float:-3.3865676E21)
                    if (r0 == r1) goto L_0x0016
                    goto L_0x002a
                L_0x0016:
                    java.lang.String r0 = "closeFlex"
                    boolean r0 = r4.equals(r0)
                    if (r0 == 0) goto L_0x002a
                    r0 = 0
                    goto L_0x002b
                L_0x0020:
                    java.lang.String r0 = "stopAll"
                    boolean r0 = r4.equals(r0)
                    if (r0 == 0) goto L_0x002a
                    r0 = 1
                    goto L_0x002b
                L_0x002a:
                    r0 = -1
                L_0x002b:
                    if (r0 == 0) goto L_0x004c
                    if (r0 != r2) goto L_0x0035
                    com.vungle.warren.ui.VungleActivity r4 = com.vungle.warren.ui.VungleActivity.this
                    r4.finish()
                    goto L_0x0063
                L_0x0035:
                    java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder
                    r0.<init>()
                    java.lang.String r1 = "No such command "
                    r0.append(r1)
                    r0.append(r4)
                    java.lang.String r4 = r0.toString()
                    r5.<init>(r4)
                    throw r5
                L_0x004c:
                    java.lang.String r4 = "placement"
                    java.lang.String r4 = r5.getStringExtra(r4)
                    com.vungle.warren.ui.VungleActivity r5 = com.vungle.warren.ui.VungleActivity.this
                    com.vungle.warren.ui.contract.AdContract$AdvertisementPresenter r5 = r5.presenter
                    if (r5 == 0) goto L_0x0063
                    com.vungle.warren.ui.VungleActivity r5 = com.vungle.warren.ui.VungleActivity.this
                    com.vungle.warren.ui.contract.AdContract$AdvertisementPresenter r5 = r5.presenter
                    r5.handleExit(r4)
                L_0x0063:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.vungle.warren.ui.VungleActivity.AnonymousClass3.onReceive(android.content.Context, android.content.Intent):void");
            }
        };
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(this.broadcastReceiver, new IntentFilter(AdContract.AdvertisementBus.ACTION));
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            AdContract.AdvertisementPresenter advertisementPresenter = this.presenter;
            if (advertisementPresenter != null) {
                advertisementPresenter.start();
            } else {
                this.pendingStart.set(true);
            }
        } else {
            AdContract.AdvertisementPresenter advertisementPresenter2 = this.presenter;
            if (advertisementPresenter2 != null) {
                advertisementPresenter2.stop(isChangingConfigurations(), isFinishing());
            }
            this.pendingStart.set(false);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation == 2) {
            Log.d(TAG, Constants.ParametersKeys.ORIENTATION_LANDSCAPE);
        } else if (configuration.orientation == 1) {
            Log.d(TAG, Constants.ParametersKeys.ORIENTATION_PORTRAIT);
        }
        AdContract.AdvertisementPresenter advertisementPresenter = this.presenter;
        if (advertisementPresenter != null) {
            advertisementPresenter.onViewConfigurationChanged();
        }
    }

    public void onBackPressed() {
        AdContract.AdvertisementPresenter advertisementPresenter = this.presenter;
        if (advertisementPresenter != null) {
            advertisementPresenter.handleExit(null);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Log.d(TAG, "onSaveInstanceState");
        BundleOptionsState bundleOptionsState = new BundleOptionsState();
        AdContract.AdvertisementPresenter advertisementPresenter = this.presenter;
        if (advertisementPresenter != null) {
            advertisementPresenter.generateSaveState(bundleOptionsState);
            bundle.putParcelable(PRESENTER_STATE, bundleOptionsState);
        }
        this.presenterFactory.saveState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        AdContract.AdvertisementPresenter advertisementPresenter;
        super.onRestoreInstanceState(bundle);
        Log.d(TAG, "onRestoreInstanceState(" + bundle + ")");
        if (bundle != null && (advertisementPresenter = this.presenter) != null) {
            advertisementPresenter.restoreFromSave((OptionsState) bundle.getParcelable(PRESENTER_STATE));
        }
    }

    public void setRequestedOrientation(int i) {
        if (canRotate()) {
            super.setRequestedOrientation(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(this.broadcastReceiver);
        AdContract.AdvertisementPresenter advertisementPresenter = this.presenter;
        if (advertisementPresenter != null) {
            advertisementPresenter.detach(isChangingConfigurations());
        } else {
            AdvertisementPresentationFactory advertisementPresentationFactory = this.presenterFactory;
            if (advertisementPresentationFactory != null) {
                advertisementPresentationFactory.destroy();
                this.presenterFactory = null;
                AdContract.AdvertisementPresenter.EventListener eventListener = bus;
                if (eventListener != null) {
                    eventListener.onError(new VungleException(25), this.placementId);
                }
            }
        }
        super.onDestroy();
        System.gc();
    }
}
