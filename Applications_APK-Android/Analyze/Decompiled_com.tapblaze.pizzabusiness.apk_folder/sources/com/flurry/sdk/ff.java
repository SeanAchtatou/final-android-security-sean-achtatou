package com.flurry.sdk;

import android.content.SharedPreferences;
import java.util.Locale;

public final class ff {
    public static void a(String str, long j) {
        SharedPreferences.Editor edit = b.a().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit();
        edit.putLong(String.format(Locale.US, "com.flurry.sdk.%s", str), j);
        edit.apply();
    }

    public static void a(String str, String str2) {
        SharedPreferences.Editor edit = b.a().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit();
        edit.putString(String.format(Locale.US, "com.flurry.sdk.%s", str), str2);
        edit.apply();
    }

    public static void a(String str, int i) {
        SharedPreferences.Editor edit = b.a().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit();
        edit.putInt(String.format(Locale.US, "com.flurry.sdk.%s", str), i);
        edit.apply();
    }

    public static String b(String str, String str2) {
        return b.a().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).getString(String.format(Locale.US, "com.flurry.sdk.%s", str), str2);
    }

    public static long b(String str, long j) {
        return b.a().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).getLong(String.format(Locale.US, "com.flurry.sdk.%s", str), j);
    }

    public static int b(String str, int i) {
        return b.a().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).getInt(String.format(Locale.US, "com.flurry.sdk.%s", str), i);
    }

    public static void a(String str) {
        b.a().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit().remove(str).apply();
    }
}
