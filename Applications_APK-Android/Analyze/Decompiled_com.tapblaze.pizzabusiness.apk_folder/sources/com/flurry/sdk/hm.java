package com.flurry.sdk;

import com.flurry.android.Consent;

public final class hm implements o<Consent> {
    public final /* synthetic */ void a(Object obj) {
        Consent consent = (Consent) obj;
        if (consent == null) {
            da.d("ConsentFrame", "Consent is null, do not send the frame.");
            return;
        }
        fc.a().a(new ij(new ik(consent.isGdprScope(), consent.getConsentStrings())));
    }
}
