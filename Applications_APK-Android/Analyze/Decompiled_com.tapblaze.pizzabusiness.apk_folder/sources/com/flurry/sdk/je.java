package com.flurry.sdk;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class je extends jl {
    public final Map<String, List<String>> a;

    public je(Map<String, List<String>> map) {
        this.a = map == null ? new HashMap<>() : map;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        for (Map.Entry next : this.a.entrySet()) {
            JSONArray jSONArray = new JSONArray();
            for (String str : (List) next.getValue()) {
                if (!TextUtils.isEmpty(str)) {
                    jSONArray.put(str);
                }
            }
            if (jSONArray.length() > 0) {
                jSONObject2.put((String) next.getKey(), jSONArray);
            }
        }
        if (jSONObject2.length() > 0) {
            jSONObject.put("fl.referrer.map", jSONObject2);
        }
        return jSONObject;
    }
}
