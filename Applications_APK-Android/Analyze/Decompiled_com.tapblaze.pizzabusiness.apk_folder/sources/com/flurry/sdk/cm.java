package com.flurry.sdk;

import android.util.SparseArray;
import com.flurry.sdk.ca;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class cm {
    Map<ci, SparseArray<cl>> a;
    Map<ci, Map<String, ca>> b;
    long c;
    private Map<ci, Map<String, ca>> d;

    public cm() {
        a();
    }

    public final synchronized void a(List<cl> list) {
        if (list != null) {
            if (!list.isEmpty()) {
                a(list, this.a);
                c(list);
            }
        }
    }

    public final synchronized boolean b(List<cl> list) {
        if (list == null) {
            return false;
        }
        if (list.size() != e()) {
            return true;
        }
        for (cl next : list) {
            SparseArray sparseArray = this.a.get(next.a);
            if (sparseArray == null) {
                return true;
            }
            cl clVar = (cl) sparseArray.get(next.b);
            if (clVar == null) {
                return true;
            }
            if (next.c != clVar.c) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, boolean, boolean):void
     arg types: [java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.HashMap, int, int]
     candidates:
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, boolean):void
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, boolean):void
     arg types: [java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.HashMap, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, boolean, boolean):void
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, boolean):org.json.JSONObject
     arg types: [java.util.HashMap, java.util.HashMap, int]
     candidates:
      com.flurry.sdk.cm.a(java.util.List<com.flurry.sdk.cl>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>):void
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, boolean, boolean):void
     arg types: [java.util.HashMap, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, int, int]
     candidates:
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, boolean):void
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, boolean):void
     arg types: [java.util.HashMap, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, boolean, boolean):void
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, boolean):void */
    public final synchronized boolean a(List<cl> list, boolean z) {
        if (list == null) {
            return true;
        }
        if (list.isEmpty()) {
            f();
            return true;
        } else if (z) {
            f();
            a(list, this.a);
            b(list, this.b);
            return true;
        } else {
            HashMap hashMap = new HashMap();
            a(this.a, (Map<ci, SparseArray<cl>>) hashMap, true, true);
            HashMap hashMap2 = new HashMap();
            a(this.b, (Map<ci, Map<String, ca>>) hashMap2, (ci) null, true);
            a(list, hashMap, hashMap2);
            a(list, hashMap);
            b(list, hashMap2);
            da.a("VariantsManager", "Verify ETag merged JSON: ".concat(String.valueOf(a((Map<ci, SparseArray<cl>>) hashMap, (Map<ci, Map<String, ca>>) hashMap2, true))));
            a((Map<ci, SparseArray<cl>>) hashMap, this.a, false, false);
            a((Map<ci, Map<String, ca>>) hashMap2, this.b, (ci) null, false);
            return true;
        }
    }

    private static void a(Map<ci, SparseArray<cl>> map, Map<ci, SparseArray<cl>> map2, boolean z, boolean z2) {
        SparseArray sparseArray;
        for (Map.Entry next : map.entrySet()) {
            ci ciVar = (ci) next.getKey();
            if (z) {
                SparseArray sparseArray2 = (SparseArray) next.getValue();
                sparseArray = new SparseArray(sparseArray2.size());
                for (int i = 0; i < sparseArray2.size(); i++) {
                    cl clVar = (cl) sparseArray2.valueAt(i);
                    int i2 = clVar.b;
                    if (z2) {
                        clVar = new cl(clVar);
                    }
                    sparseArray.put(i2, clVar);
                }
            } else {
                sparseArray = (SparseArray) next.getValue();
            }
            map2.put(ciVar, sparseArray);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, boolean, boolean):void
     arg types: [java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.HashMap, int, int]
     candidates:
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, boolean):void
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, boolean, boolean):void */
    private synchronized void a(List<cl> list, Map<ci, SparseArray<cl>> map, Map<ci, Map<String, ca>> map2) {
        HashMap hashMap = new HashMap();
        a(map, (Map<ci, SparseArray<cl>>) hashMap, true, false);
        for (cl next : list) {
            SparseArray sparseArray = (SparseArray) hashMap.get(next.a);
            if (sparseArray != null) {
                sparseArray.remove(next.b);
            }
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            ci ciVar = (ci) entry.getKey();
            SparseArray sparseArray2 = (SparseArray) entry.getValue();
            SparseArray sparseArray3 = map.get(ciVar);
            Map map3 = map2.get(ciVar);
            for (int i = 0; i < sparseArray2.size(); i++) {
                cl clVar = (cl) sparseArray2.valueAt(i);
                sparseArray3.remove(clVar.b);
                for (String remove : clVar.e.keySet()) {
                    map3.remove(remove);
                }
            }
        }
    }

    private synchronized void a(List<cl> list, Map<ci, SparseArray<cl>> map) {
        for (cl next : list) {
            int i = next.b;
            ci ciVar = next.a;
            SparseArray sparseArray = map.get(ciVar);
            if (sparseArray == null) {
                sparseArray = new SparseArray();
                map.put(ciVar, sparseArray);
            } else {
                cl clVar = (cl) sparseArray.get(i);
                if (clVar != null) {
                    next.a(clVar);
                }
            }
            sparseArray.put(i, next);
        }
    }

    private static void a(Map<ci, Map<String, ca>> map, Map<ci, Map<String, ca>> map2, ci ciVar, boolean z) {
        for (Map.Entry next : map.entrySet()) {
            ci ciVar2 = (ci) next.getKey();
            if (ciVar == null || ciVar == ciVar2) {
                Map map3 = (Map) next.getValue();
                if (z) {
                    map3 = new HashMap(map3);
                }
                map2.put(ciVar2, map3);
            }
        }
    }

    private synchronized void c(List<cl> list) {
        for (cl next : list) {
            ci ciVar = next.a;
            Map map = this.d.get(ciVar);
            if (map == null) {
                map = new HashMap();
                this.d.put(ciVar, map);
            }
            Map map2 = this.b.get(ciVar);
            if (map2 == null) {
                map2 = new HashMap();
                this.b.put(ciVar, map2);
            }
            for (Map.Entry next2 : next.a()) {
                String str = (String) next2.getKey();
                ca caVar = (ca) next2.getValue();
                map.put(str, caVar);
                map2.put(str, caVar);
            }
        }
    }

    private synchronized void b(List<cl> list, Map<ci, Map<String, ca>> map) {
        for (cl next : list) {
            ci ciVar = next.a;
            Map map2 = map.get(ciVar);
            if (map2 == null) {
                map2 = new HashMap();
                map.put(ciVar, map2);
            }
            for (Map.Entry next2 : next.a()) {
                String str = (String) next2.getKey();
                ca caVar = (ca) next2.getValue();
                if (caVar.a == ca.a.Tombstone) {
                    map2.remove(str);
                } else {
                    map2.put(str, caVar);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, boolean):void
     arg types: [java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, int]
     candidates:
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, boolean, boolean):void
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, com.flurry.sdk.ci, boolean):void */
    public final synchronized void a(ci ciVar) {
        da.a(3, "VariantsManager", "original Variants properties:" + this.d.keySet().toString() + " with: " + this.a.values().toString());
        a(this.b, this.d, ciVar, true);
        StringBuilder sb = new StringBuilder("new Variants properties:");
        sb.append(this.d.keySet().toString());
        da.a(3, "VariantsManager", sb.toString());
    }

    public final ca a(String str, ci ciVar) {
        if (ciVar == null) {
            for (Map<String, ca> map : this.d.values()) {
                ca caVar = (ca) map.get(str);
                if (caVar != null) {
                    return caVar;
                }
            }
            return null;
        }
        Map map2 = this.d.get(ciVar);
        if (map2 != null) {
            return (ca) map2.get(str);
        }
        return null;
    }

    public final synchronized void a() {
        f();
        this.d = new HashMap();
        for (ci put : ci.a()) {
            this.d.put(put, new HashMap());
        }
    }

    private synchronized void f() {
        this.a = new HashMap();
        this.b = new HashMap();
        for (ci next : ci.a()) {
            this.a.put(next, new SparseArray());
            this.b.put(next, new HashMap());
        }
    }

    public final synchronized List<cl> b() {
        return a(this.a);
    }

    private synchronized List<cl> a(Map<ci, SparseArray<cl>> map) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (SparseArray next : map.values()) {
            for (int i = 0; i < next.size(); i++) {
                arrayList.add(next.valueAt(i));
            }
        }
        return arrayList;
    }

    public final synchronized String c() {
        StringBuilder sb;
        sb = new StringBuilder();
        int i = 0;
        for (SparseArray next : this.a.values()) {
            i += next.size();
            for (int i2 = 0; i2 < next.size(); i2++) {
                cl clVar = (cl) next.valueAt(i2);
                sb.append("," + clVar.b);
                sb.append("," + clVar.c);
            }
        }
        sb.insert(0, i);
        return sb.toString();
    }

    public final synchronized List<ci> d() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (Map.Entry next : this.a.entrySet()) {
            if (((SparseArray) next.getValue()).size() > 0) {
                arrayList.add(next.getKey());
            }
        }
        return arrayList;
    }

    public final synchronized int e() {
        int i;
        i = 0;
        for (SparseArray<cl> size : this.a.values()) {
            i += size.size();
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public final JSONObject a(Map<ci, SparseArray<cl>> map, Map<ci, Map<String, ca>> map2, boolean z) {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONArray jSONArray = new JSONArray();
            List<cl> a2 = a(map);
            if (z) {
                Collections.sort(a2);
            }
            for (cl next : a2) {
                Map map3 = map2.get(next.a);
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("id", next.b);
                jSONObject2.put("version", next.c);
                jSONObject2.put("document", next.a.toString());
                JSONArray jSONArray2 = new JSONArray();
                for (Map.Entry key : z ? new TreeMap(next.e).entrySet() : next.a()) {
                    String str = (String) key.getKey();
                    ca caVar = (ca) map3.get(str);
                    if (caVar != null) {
                        jSONArray2.put(caVar.a(str));
                    }
                }
                jSONObject2.put("items", jSONArray2);
                jSONArray.put(jSONObject2);
            }
            jSONObject.put("variants", jSONArray);
            jSONObject.put("refreshInSeconds", this.c);
            return jSONObject;
        } catch (JSONException e) {
            da.a("VariantsManager", "Error to create JSON object.", e);
            return null;
        }
    }
}
