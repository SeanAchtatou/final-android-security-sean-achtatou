package com.flurry.sdk;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import java.util.Locale;
import okhttp3.internal.http.StatusLine;

public final class bn {
    private static bn c;
    public String a;
    public String b = String.format(Locale.getDefault(), "Flurry_Android_%d_%d.%d.%d%s%s", Integer.valueOf((int) StatusLine.HTTP_PERM_REDIRECT), 12, 1, 0, "", "");
    private String d;

    private bn() {
    }

    public static synchronized bn a() {
        bn bnVar;
        synchronized (bn.class) {
            if (c == null) {
                c = new bn();
            }
            bnVar = c;
        }
        return bnVar;
    }

    public static String a(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return null;
        }
        try {
            return packageManager.getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException unused) {
            return "unknown";
        }
    }

    public static int b(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return 0;
        }
        try {
            return packageManager.getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException unused) {
            return 0;
        }
    }

    public final synchronized String b() {
        if (!TextUtils.isEmpty(this.a)) {
            return this.a;
        } else if (!TextUtils.isEmpty(this.d)) {
            return this.d;
        } else {
            this.d = c();
            return this.d;
        }
    }

    private static String c() {
        try {
            Context a2 = b.a();
            PackageInfo packageInfo = a2.getPackageManager().getPackageInfo(a2.getPackageName(), 0);
            if (packageInfo.versionName != null) {
                return packageInfo.versionName;
            }
            if (packageInfo.versionCode != 0) {
                return Integer.toString(packageInfo.versionCode);
            }
            return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        } catch (Throwable th) {
            da.a(6, "VersionProvider", "", th);
            return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
    }
}
