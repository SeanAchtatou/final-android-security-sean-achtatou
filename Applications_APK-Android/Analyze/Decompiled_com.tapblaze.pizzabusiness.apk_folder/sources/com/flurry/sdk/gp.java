package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class gp extends jl {
    public final int a;
    public final boolean b;

    public gp(int i, boolean z) {
        this.a = i;
        this.b = z;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.event.count", this.a);
        jSONObject.put("fl.event.set.complete", this.b);
        return jSONObject;
    }
}
