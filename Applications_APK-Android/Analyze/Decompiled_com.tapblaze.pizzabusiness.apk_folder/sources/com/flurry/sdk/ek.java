package com.flurry.sdk;

import android.content.Context;
import com.flurry.android.FlurryModule;
import java.util.Iterator;
import java.util.List;

public final class ek {
    private static boolean a;
    private static boolean b;

    public static synchronized void a() {
        synchronized (ek.class) {
            if (!a) {
                try {
                    dd.a((Class<? extends de>) Class.forName("com.flurry.android.bridge.FlurryBridgeModule"));
                } catch (ClassNotFoundException | NoClassDefFoundError unused) {
                    da.a(3, "FlurrySDK", "Ads module not available");
                }
                a = true;
            }
        }
    }

    public static synchronized void a(Context context) {
        synchronized (ek.class) {
            dd.b(context);
        }
    }

    public static synchronized void a(List<FlurryModule> list) {
        synchronized (ek.class) {
            if (!b) {
                if (list != null) {
                    Iterator<FlurryModule> it = list.iterator();
                    while (it.hasNext()) {
                        dd.a((de) it.next());
                    }
                }
                b = true;
            }
        }
    }

    public static synchronized void b() {
        synchronized (ek.class) {
            dd.a();
            dd.b();
            a = false;
            b = false;
        }
    }
}
