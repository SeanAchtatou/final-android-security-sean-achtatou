package com.flurry.sdk;

final class hq implements o<aq> {
    hq() {
    }

    public final /* synthetic */ void a(Object obj) {
        aq aqVar = (aq) obj;
        String str = aqVar.a;
        String str2 = aqVar.b;
        if ((str == null || str.isEmpty()) && (str2 == null || str2.isEmpty())) {
            da.a(2, "LocaleFrame", "Locale is empty, do not send the frame.");
        } else {
            fc.a().a(new gs(new gt(str, str2)));
        }
        da.a(4, "LocaleObserver", "Locale language: " + aqVar.a + ". Locale country: " + aqVar.b);
    }
}
