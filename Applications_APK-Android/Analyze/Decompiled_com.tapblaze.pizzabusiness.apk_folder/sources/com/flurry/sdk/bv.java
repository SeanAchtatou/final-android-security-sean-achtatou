package com.flurry.sdk;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.util.BillingHelper;
import com.android.vending.billing.IInAppBillingService;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public final class bv {
    /* access modifiers changed from: private */
    public static Object a = new Object();
    /* access modifiers changed from: private */
    public static List<b> b = new ArrayList();
    /* access modifiers changed from: private */
    public static IInAppBillingService c;
    /* access modifiers changed from: private */
    public static ServiceConnection d;

    public static abstract class a {
        public abstract void a(int i, c cVar);
    }

    public static void a(final Context context, final String str, final a aVar) {
        try {
            Class.forName("com.android.vending.billing.IInAppBillingService");
            da.a(3, "GooglePlayIap", "Google play billing library is available");
            AnonymousClass1 r0 = new b() {
                public final void a(int i, IInAppBillingService iInAppBillingService) {
                    if (i == 0) {
                        c a2 = bv.b(iInAppBillingService, context, BillingClient.SkuType.INAPP, str);
                        if (a2 == null) {
                            a2 = bv.b(iInAppBillingService, context, BillingClient.SkuType.SUBS, str);
                        }
                        aVar.a(i, a2);
                        return;
                    }
                    aVar.a(i, null);
                }
            };
            Boolean bool = Boolean.FALSE;
            synchronized (a) {
                if (d == null) {
                    d = new ServiceConnection() {
                        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                            synchronized (bv.a) {
                                IInAppBillingService unused = bv.c = IInAppBillingService.Stub.asInterface(iBinder);
                                for (b b : bv.b) {
                                    b.b(0, bv.c);
                                }
                                bv.b.clear();
                            }
                        }

                        public final void onServiceDisconnected(ComponentName componentName) {
                            synchronized (bv.a) {
                                ServiceConnection unused = bv.d = null;
                                IInAppBillingService unused2 = bv.c = null;
                                for (b b : bv.b) {
                                    b.b(1, null);
                                }
                                bv.b.clear();
                            }
                        }
                    };
                    bool = Boolean.TRUE;
                }
                if (c == null) {
                    b.add(r0);
                } else {
                    r0.b(0, c);
                }
                if (bool.booleanValue()) {
                    Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
                    intent.setPackage("com.android.vending");
                    List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
                    if (queryIntentServices == null || queryIntentServices.isEmpty()) {
                        r0.b(1, null);
                        d = null;
                    } else {
                        context.bindService(intent, d, 1);
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            da.b("GooglePlayIap", "Could not find google play billing library");
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public static c b(IInAppBillingService iInAppBillingService, Context context, String str, String str2) {
        Bundle bundle = new Bundle();
        ArrayList arrayList = new ArrayList();
        arrayList.add(str2);
        bundle.putStringArrayList("ITEM_ID_LIST", arrayList);
        try {
            Bundle skuDetails = iInAppBillingService.getSkuDetails(3, context.getPackageName(), str, bundle);
            if (skuDetails.containsKey(BillingHelper.RESPONSE_GET_SKU_DETAILS_LIST)) {
                ArrayList<String> stringArrayList = skuDetails.getStringArrayList(BillingHelper.RESPONSE_GET_SKU_DETAILS_LIST);
                if (stringArrayList.size() > 0) {
                    return new c(str, stringArrayList.get(0));
                }
            }
            return null;
        } catch (RemoteException e) {
            da.a("GooglePlayIap", "RemoteException getting SKU Details", e);
            return null;
        } catch (JSONException e2) {
            da.a("GooglePlayIap", "JSONException parsing SKU Details", e2);
            return null;
        }
    }

    static abstract class b {
        public abstract void a(int i, IInAppBillingService iInAppBillingService);

        private b() {
        }

        /* synthetic */ b(byte b) {
            this();
        }

        public final void b(final int i, final IInAppBillingService iInAppBillingService) {
            new Thread(new Runnable() {
                public final void run() {
                    b.this.a(i, iInAppBillingService);
                }
            }).start();
        }
    }

    public static class c {
        public final String a;
        public final long b;
        public final String c;
        public final String d;
        private final String e;
        private final String f;
        private final String g;
        private final String h;
        private final String i;

        public c(String str, String str2) throws JSONException {
            this.e = str;
            this.i = str2;
            JSONObject jSONObject = new JSONObject(this.i);
            this.f = jSONObject.optString("productId");
            this.a = jSONObject.optString("type");
            this.g = jSONObject.optString("price");
            this.b = jSONObject.optLong("price_amount_micros");
            this.c = jSONObject.optString("price_currency_code");
            this.d = jSONObject.optString("title");
            this.h = jSONObject.optString("description");
        }

        public final String toString() {
            return "SkuDetails:" + this.i;
        }
    }
}
