package com.flurry.sdk;

import java.lang.Thread;
import java.util.Collections;
import java.util.Map;

public final class ab extends m<aa> implements Thread.UncaughtExceptionHandler {
    public w b = new w();
    public boolean h = false;

    public ab() {
        super("FlurryErrorProvider");
        z a = z.a();
        synchronized (a.b) {
            a.b.put(this, null);
        }
    }

    public final void c() {
        super.c();
        z.b();
        w wVar = this.b;
        if (wVar != null) {
            wVar.a = null;
            this.b = null;
        }
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        String str;
        th.printStackTrace();
        long currentTimeMillis = System.currentTimeMillis();
        if (this.h) {
            StackTraceElement[] stackTrace = th.getStackTrace();
            if (stackTrace == null || stackTrace.length <= 0) {
                str = th.getMessage() != null ? th.getMessage() : "";
            } else {
                StringBuilder sb = new StringBuilder();
                if (th.getMessage() != null) {
                    sb.append(" (");
                    sb.append(th.getMessage());
                    sb.append(")\n");
                }
                str = sb.toString();
            }
            a(new aa(y.UNCAUGHT_EXCEPTION_ID.c, currentTimeMillis, str, th.getClass().getName(), th, x.a(), null, this.b.a()));
        }
    }

    public final void a(String str, long j, String str2, String str3, Throwable th, Map<String, String> map, Map<String, String> map2) {
        a(new aa(str, j, str2, str3, th, map, map2, Collections.emptyList()));
    }
}
