package com.flurry.sdk;

import java.text.SimpleDateFormat;
import java.util.Locale;

public final class v {
    private static SimpleDateFormat c = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
    public String a;
    public long b;

    public v(String str, long j) {
        this.a = str;
        this.b = j;
    }

    public final String toString() {
        return c.format(Long.valueOf(this.b)) + ": " + this.a + "\n";
    }
}
