package com.flurry.sdk;

import android.text.TextUtils;
import com.google.android.gms.common.GoogleApiAvailability;
import java.util.concurrent.Future;

public final class ac extends m<ad> {
    public String b;
    public String h;
    public boolean i = false;
    public boolean j = false;
    /* access modifiers changed from: private */
    public am k;
    /* access modifiers changed from: private */
    public o<am> l = new o<am>() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.flurry.sdk.ac.a(com.flurry.sdk.ac, java.lang.Runnable):java.util.concurrent.Future
         arg types: [com.flurry.sdk.ac, com.flurry.sdk.ac$1$1]
         candidates:
          com.flurry.sdk.ac.a(com.flurry.sdk.ac, com.flurry.sdk.am):com.flurry.sdk.am
          com.flurry.sdk.m.a(com.flurry.sdk.m, java.lang.Runnable):java.util.concurrent.Future
          com.flurry.sdk.ac.a(com.flurry.sdk.ac, java.lang.Runnable):java.util.concurrent.Future */
        public final /* synthetic */ void a(Object obj) {
            final am amVar = (am) obj;
            Future unused = ac.this.b((Runnable) new ec() {
                public final void a() throws Exception {
                    da.a(3, "FlurryProvider", "isInstantApp: " + amVar.a);
                    am unused = ac.this.k = amVar;
                    ac.a(ac.this);
                    ac.this.m.b(ac.this.l);
                }
            });
        }
    };
    /* access modifiers changed from: private */
    public an m;
    private q n;
    private o<r> o = new o<r>() {
        public final /* bridge */ /* synthetic */ void a(Object obj) {
            ac.a(ac.this);
        }
    };

    public enum a {
        UNAVAILABLE(-2),
        UNKNOWN(-1),
        SUCCESS(0),
        SERVICE_MISSING(1),
        SERVICE_UPDATING(2),
        SERVICE_VERSION_UPDATE_REQUIRED(3),
        SERVICE_DISABLED(4),
        SERVICE_INVALID(5);
        
        public int i;

        private a(int i2) {
            this.i = i2;
        }
    }

    public ac(an anVar, q qVar) {
        super("FlurryProvider");
        this.m = anVar;
        this.m.a((o) this.l);
        this.n = qVar;
        this.n.a(this.o);
    }

    private static a d() {
        try {
            int isGooglePlayServicesAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(b.a());
            if (isGooglePlayServicesAvailable == 0) {
                return a.SUCCESS;
            }
            if (isGooglePlayServicesAvailable == 1) {
                return a.SERVICE_MISSING;
            }
            if (isGooglePlayServicesAvailable == 2) {
                return a.SERVICE_VERSION_UPDATE_REQUIRED;
            }
            if (isGooglePlayServicesAvailable == 3) {
                return a.SERVICE_DISABLED;
            }
            if (isGooglePlayServicesAvailable == 9) {
                return a.SERVICE_INVALID;
            }
            if (isGooglePlayServicesAvailable != 18) {
                return a.UNAVAILABLE;
            }
            return a.SERVICE_UPDATING;
        } catch (Exception | NoClassDefFoundError unused) {
            da.a(3, "FlurryProvider", "Error retrieving Google Play Services Availability. This probably means google play services is unavailable.");
            return a.UNAVAILABLE;
        }
    }

    public final void c() {
        super.c();
        this.m.b(this.l);
        this.n.b(this.o);
    }

    static /* synthetic */ void a(ac acVar) {
        if (!TextUtils.isEmpty(acVar.b) && acVar.k != null) {
            acVar.a(new ad(bk.a().b(), acVar.i, d(), acVar.k));
        }
    }

    static /* synthetic */ void d(ac acVar) {
        if (TextUtils.isEmpty(acVar.b)) {
            da.a(6, "FlurryProvider", "Streaming API Key is invalid");
            return;
        }
        int b2 = ff.b("prev_streaming_api_key", 0);
        int hashCode = ff.b("api_key", "").hashCode();
        int hashCode2 = acVar.b.hashCode();
        if (b2 != hashCode2 && hashCode != hashCode2) {
            da.a(3, "FlurryProvider", "Streaming API key is refreshed");
            ff.a("prev_streaming_api_key", hashCode2);
            bb bbVar = n.a().k;
            da.a(3, "ReportingProvider", "Reset initial timestamp.");
            bbVar.b(new ec() {
                public final void a() throws Exception {
                    long unused = bb.this.l = Long.MIN_VALUE;
                }
            });
        }
    }
}
