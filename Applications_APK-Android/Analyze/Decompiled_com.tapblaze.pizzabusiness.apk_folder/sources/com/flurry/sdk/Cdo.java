package com.flurry.sdk;

import com.flurry.sdk.dm;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.flurry.sdk.do  reason: invalid class name */
public final class Cdo {
    private static Cdo d;
    public dm.a a;
    dj b;
    Map<String, dj> c = new HashMap();

    private Cdo() {
    }

    public static synchronized Cdo a() {
        Cdo doVar;
        synchronized (Cdo.class) {
            if (d == null) {
                d = new Cdo();
            }
            doVar = d;
        }
        return doVar;
    }
}
