package com.flurry.sdk;

public final class bm {
    private static bm a;

    private bm() {
    }

    public static synchronized bm a() {
        bm bmVar;
        synchronized (bm.class) {
            if (a == null) {
                a = new bm();
            }
            bmVar = a;
        }
        return bmVar;
    }
}
