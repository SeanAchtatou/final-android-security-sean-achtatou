package com.flurry.sdk;

import android.os.Build;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;

public final class co extends cr {
    private static String i;
    private HttpsURLConnection j;
    private String k;
    private boolean l;

    co(String str) {
        this.a = str;
        i = "Flurry-Config/1.0 (Android " + Build.VERSION.RELEASE + "/" + Build.ID + ")";
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0123  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.InputStream a() throws java.io.IOException {
        /*
            r6 = this;
            java.net.URL r0 = new java.net.URL
            java.lang.String r1 = r6.a
            r0.<init>(r1)
            java.net.URLConnection r0 = r0.openConnection()
            java.lang.Object r0 = com.google.firebase.perf.network.FirebasePerfUrlConnection.instrument(r0)
            java.net.URLConnection r0 = (java.net.URLConnection) r0
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0
            r6.j = r0
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            r1 = 10000(0x2710, float:1.4013E-41)
            r0.setReadTimeout(r1)
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            r1 = 15000(0x3a98, float:2.102E-41)
            r0.setConnectTimeout(r1)
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            java.lang.String r1 = com.flurry.sdk.co.i
            java.lang.String r2 = "User-Agent"
            r0.setRequestProperty(r2, r1)
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = "application/json"
            r0.setRequestProperty(r1, r2)
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            r1 = 1
            r0.setDoInput(r1)
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            r0.setDoOutput(r1)
            r0 = 1234(0x4d2, float:1.729E-42)
            android.net.TrafficStats.setThreadStatsTag(r0)
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            r0.connect()
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            com.flurry.sdk.di.a(r0)
            java.util.UUID r0 = java.util.UUID.randomUUID()
            java.lang.String r0 = r0.toString()
            java.util.Locale r1 = java.util.Locale.ENGLISH
            java.lang.String r0 = r0.toUpperCase(r1)
            r6.c = r0
            r0 = 0
            javax.net.ssl.HttpsURLConnection r1 = r6.j     // Catch:{ all -> 0x0119 }
            java.io.OutputStream r1 = r1.getOutputStream()     // Catch:{ all -> 0x0119 }
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ all -> 0x0117 }
            java.io.OutputStreamWriter r3 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0117 }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r1, r4)     // Catch:{ all -> 0x0117 }
            r2.<init>(r3)     // Catch:{ all -> 0x0117 }
            java.lang.String r0 = r6.c     // Catch:{ all -> 0x0112 }
            java.lang.String r0 = com.flurry.sdk.cq.a(r0)     // Catch:{ all -> 0x0112 }
            r2.write(r0)     // Catch:{ all -> 0x0112 }
            r2.close()
            if (r1 == 0) goto L_0x008a
            r1.close()
        L_0x008a:
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            int r0 = r0.getResponseCode()
            r1 = 400(0x190, float:5.6E-43)
            if (r0 >= r1) goto L_0x0102
            javax.net.ssl.HttpsURLConnection r1 = r6.j
            java.lang.String r2 = "Content-Signature"
            java.lang.String r1 = r1.getHeaderField(r2)
            r6.k = r1
            javax.net.ssl.HttpsURLConnection r1 = r6.j
            java.lang.String r2 = "ETag"
            java.lang.String r1 = r1.getHeaderField(r2)
            r6.g = r1
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Content-Signature: "
            r1.<init>(r2)
            java.lang.String r2 = r6.k
            r1.append(r2)
            java.lang.String r2 = ", ETag: "
            r1.append(r2)
            java.lang.String r2 = r6.g
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "HttpTransport"
            com.flurry.sdk.da.a(r2, r1)
            r1 = 304(0x130, float:4.26E-43)
            if (r0 != r1) goto L_0x00fb
            java.lang.String r0 = r6.c
            boolean r0 = r6.a(r0)
            if (r0 == 0) goto L_0x00dd
            com.flurry.sdk.ce r0 = com.flurry.sdk.ce.b
            r6.b = r0
            java.lang.String r0 = "Empty 304 payload; No Change."
            com.flurry.sdk.da.a(r2, r0)
            goto L_0x00fb
        L_0x00dd:
            com.flurry.sdk.ce r0 = new com.flurry.sdk.ce
            com.flurry.sdk.ce$a r1 = com.flurry.sdk.ce.a.AUTHENTICATE
            java.lang.String r3 = "GUID Signature Error."
            r0.<init>(r1, r3)
            r6.b = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "Authentication error: "
            r0.<init>(r1)
            com.flurry.sdk.ce r1 = r6.b
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.flurry.sdk.da.b(r2, r0)
        L_0x00fb:
            javax.net.ssl.HttpsURLConnection r0 = r6.j
            java.io.InputStream r0 = r0.getInputStream()
            return r0
        L_0x0102:
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r2 = "Server response code is "
            java.lang.String r0 = r2.concat(r0)
            r1.<init>(r0)
            throw r1
        L_0x0112:
            r0 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x011c
        L_0x0117:
            r2 = move-exception
            goto L_0x011c
        L_0x0119:
            r1 = move-exception
            r2 = r1
            r1 = r0
        L_0x011c:
            if (r0 == 0) goto L_0x0121
            r0.close()
        L_0x0121:
            if (r1 == 0) goto L_0x0126
            r1.close()
        L_0x0126:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.co.a():java.io.InputStream");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        HttpsURLConnection httpsURLConnection = this.j;
        if (httpsURLConnection != null) {
            httpsURLConnection.disconnect();
        }
    }

    public final boolean c() {
        return "https://cfg.flurry.com/sdk/v1/config".equals(this.a);
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str) {
        boolean z;
        if (!b(this.k)) {
            return false;
        }
        if (this.l) {
            z = ct.c(this.e, str, this.f);
        } else {
            z = ct.b(this.e, str, this.f);
        }
        if (z) {
            return true;
        }
        da.b("HttpTransport", "Incorrect signature for response.");
        return false;
    }

    private boolean b(String str) {
        if (TextUtils.isEmpty(str)) {
            da.b("HttpTransport", "Content-Signature is empty.");
            return false;
        }
        HashMap hashMap = new HashMap();
        for (String str2 : str.split(";")) {
            int indexOf = str2.indexOf(Constants.RequestParameters.EQUAL);
            if (indexOf > 0) {
                hashMap.put(str2.substring(0, indexOf), str2.substring(indexOf + 1));
            }
        }
        this.d = (String) hashMap.get("keyid");
        if (TextUtils.isEmpty(this.d)) {
            da.b("HttpTransport", "Error to get keyid from Signature.");
            return false;
        }
        this.e = cu.a.get(this.d);
        da.a("HttpTransport", "Signature keyid: " + this.d + ", key: " + this.e);
        if (this.e == null) {
            da.b("HttpTransport", "Unknown keyid from Signature.");
            return false;
        }
        String str3 = "sha256ecdsa";
        this.l = hashMap.containsKey(str3);
        if (!this.l) {
            str3 = "sha256rsa";
        }
        this.f = (String) hashMap.get(str3);
        if (TextUtils.isEmpty(this.f)) {
            da.b("HttpTransport", "Error to get rsa from Signature.");
            return false;
        }
        da.a("HttpTransport", "Signature rsa: " + this.f);
        return true;
    }
}
