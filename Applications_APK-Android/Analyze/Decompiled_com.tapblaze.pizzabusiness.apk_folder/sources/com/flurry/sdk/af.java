package com.flurry.sdk;

import android.text.TextUtils;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class af {
    String a = "";
    final Set<String> b;

    af() {
        HashSet hashSet = new HashSet();
        hashSet.add("");
        hashSet.add(null);
        hashSet.add("null");
        hashSet.add("9774d56d682e549c");
        hashSet.add("dead00beef");
        this.b = Collections.unmodifiableSet(hashSet);
    }

    static void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            File fileStreamPath = b.a().getFileStreamPath(".flurryb.");
            if (dz.a(fileStreamPath)) {
                DataOutputStream dataOutputStream = null;
                try {
                    DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(fileStreamPath));
                    try {
                        dataOutputStream2.writeInt(1);
                        dataOutputStream2.writeUTF(str);
                        ea.a(dataOutputStream2);
                    } catch (Throwable th) {
                        th = th;
                        dataOutputStream = dataOutputStream2;
                        try {
                            da.a(6, "DeviceIdProvider", "Error when saving deviceId", th);
                        } finally {
                            ea.a(dataOutputStream);
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    da.a(6, "DeviceIdProvider", "Error when saving deviceId", th);
                }
            }
        }
    }

    static String a() {
        DataInputStream dataInputStream;
        File fileStreamPath = b.a().getFileStreamPath(".flurryb.");
        String str = null;
        if (fileStreamPath == null || !fileStreamPath.exists()) {
            return null;
        }
        try {
            dataInputStream = new DataInputStream(new FileInputStream(fileStreamPath));
            try {
                if (1 == dataInputStream.readInt()) {
                    str = dataInputStream.readUTF();
                }
            } catch (Throwable th) {
                th = th;
                try {
                    da.a(6, "DeviceIdProvider", "Error when loading deviceId", th);
                    ea.a(dataInputStream);
                    return str;
                } catch (Throwable th2) {
                    ea.a(dataInputStream);
                    throw th2;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            dataInputStream = null;
            da.a(6, "DeviceIdProvider", "Error when loading deviceId", th);
            ea.a(dataInputStream);
            return str;
        }
        ea.a(dataInputStream);
        return str;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        String[] list;
        File fileStreamPath;
        DataInputStream dataInputStream;
        File filesDir = b.a().getFilesDir();
        String str = null;
        if (filesDir == null || (list = filesDir.list(new FilenameFilter() {
            public final boolean accept(File file, String str) {
                return str.startsWith(".flurryagent.");
            }
        })) == null || list.length == 0 || (fileStreamPath = b.a().getFileStreamPath(list[0])) == null || !fileStreamPath.exists()) {
            return null;
        }
        try {
            dataInputStream = new DataInputStream(new FileInputStream(fileStreamPath));
            try {
                if (46586 == dataInputStream.readUnsignedShort() && 2 == dataInputStream.readUnsignedShort()) {
                    dataInputStream.readUTF();
                    str = dataInputStream.readUTF();
                }
            } catch (Throwable th) {
                th = th;
                try {
                    da.a(6, "DeviceIdProvider", "Error when loading deviceId", th);
                    ea.a(dataInputStream);
                    return str;
                } catch (Throwable th2) {
                    ea.a(dataInputStream);
                    throw th2;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            dataInputStream = null;
            da.a(6, "DeviceIdProvider", "Error when loading deviceId", th);
            ea.a(dataInputStream);
            return str;
        }
        ea.a(dataInputStream);
        return str;
    }
}
