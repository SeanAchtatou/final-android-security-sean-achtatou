package com.flurry.sdk;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

public final class cv extends cz<dh> {
    private static cv b;

    protected cv() {
        super("HttpRequestManager", TimeUnit.MILLISECONDS, new PriorityBlockingQueue(11, new cx()));
    }

    public static synchronized cv a() {
        cv cvVar;
        synchronized (cv.class) {
            if (b == null) {
                b = new cv();
            }
            cvVar = b;
        }
        return cvVar;
    }
}
