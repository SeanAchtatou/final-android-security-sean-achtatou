package com.flurry.sdk;

public final class ax extends m<aw> {
    public String b;
    public boolean h = false;
    protected o<r> i = new o<r>() {
        public final /* synthetic */ void a(Object obj) {
            ax axVar = ax.this;
            axVar.a(new aw(axVar.b, ax.this.h));
        }
    };
    private q j;

    public ax(q qVar) {
        super("NotificationProvider");
        this.j = qVar;
        this.j.a(this.i);
    }

    public final void a(final o<aw> oVar) {
        super.a((o) oVar);
        final aw awVar = new aw(this.b, this.h);
        b(new ec() {
            public final void a() {
                oVar.a(awVar);
            }
        });
    }

    public final void c() {
        super.c();
        this.j.b(this.i);
    }
}
