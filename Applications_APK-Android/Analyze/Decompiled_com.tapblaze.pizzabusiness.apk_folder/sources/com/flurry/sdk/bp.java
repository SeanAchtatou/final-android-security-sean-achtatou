package com.flurry.sdk;

import com.flurry.sdk.bs;
import com.flurry.sdk.df;
import com.flurry.sdk.dh;
import com.flurry.sdk.ey;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

public abstract class bp extends f {
    protected final String a;
    protected String b;
    protected bo h;
    Set<String> i = new HashSet();
    br j;
    private av k = n.a().b;
    private o<au> l = new o<au>() {
        public final /* synthetic */ void a(Object obj) {
            au auVar = (au) obj;
            String str = bp.this.a;
            da.c(str, "NetworkAvailabilityChanged : NetworkAvailable = " + auVar.a);
            if (auVar.a) {
                bp.this.b();
            }
        }
    };

    /* access modifiers changed from: protected */
    public abstract void a(int i2, String str, String str2);

    /* access modifiers changed from: protected */
    public abstract String d();

    public bp(String str, String str2) {
        super(str2, ey.a(ey.a.REPORTS));
        this.a = str2;
        this.b = "AnalyticsData_";
        this.k.a(this.l);
        this.j = new br(str);
    }

    public final void a() {
        br brVar = this.j;
        String str = brVar.b;
        brVar.c = new LinkedHashMap<>();
        ArrayList<String> arrayList = new ArrayList<>();
        File fileStreamPath = b.a().getFileStreamPath(".FlurrySenderIndex.info.".concat(String.valueOf(str)));
        da.a(5, "FlurryDataSenderIndex", "isOldIndexFilePresent: for " + str + fileStreamPath.exists());
        if (fileStreamPath.exists()) {
            List<String> a2 = brVar.a(str);
            if (a2 != null && a2.size() > 0) {
                arrayList.addAll(a2);
                for (String b2 : arrayList) {
                    brVar.b(b2);
                }
            }
            br.c(str);
        } else {
            List<bs> list = (List) new l(b.a().getFileStreamPath(br.d(brVar.b)), str, 1, new dw<List<bs>>() {
                public final dt<List<bs>> a(int i) {
                    return new ds(new bs.a());
                }
            }).a();
            if (list == null) {
                da.c("FlurryDataSenderIndex", "New main file also not found. returning..");
                b();
            }
            for (bs bsVar : list) {
                arrayList.add(bsVar.a);
            }
        }
        for (String str2 : arrayList) {
            List<String> e = brVar.e(str2);
            if (e != null && !e.isEmpty()) {
                brVar.c.put(str2, e);
            }
        }
        b();
    }

    public final void a(bo boVar) {
        this.h = boVar;
    }

    public final void a(final byte[] bArr, final String str, final String str2) {
        if (bArr == null || bArr.length == 0) {
            da.a(6, this.a, "Report that has to be sent is EMPTY or NULL");
            return;
        }
        b(new ec() {
            public final void a() {
                bp.this.b(bArr, str, str2);
            }
        });
        b();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        b(new ec() {
            public final void a() {
                bp.this.c();
            }
        });
    }

    /* JADX WARN: Type inference failed for: r6v8, types: [byte[], RequestObjectType] */
    /* access modifiers changed from: protected */
    public final void c() {
        if (!c.a()) {
            da.a(5, this.a, "Reports were not sent! No Internet connection!");
            return;
        }
        ArrayList<String> arrayList = new ArrayList<>(this.j.c.keySet());
        if (arrayList.isEmpty()) {
            da.a(4, this.a, "No more reports to send.");
            return;
        }
        for (final String str : arrayList) {
            if (e()) {
                List<String> f = this.j.f(str);
                String str2 = this.a;
                da.a(4, str2, "Number of not sent blocks = " + f.size());
                for (final String next : f) {
                    if (!this.i.contains(next)) {
                        if (!e()) {
                            break;
                        }
                        bq a2 = bq.b(next).a();
                        if (a2 == null) {
                            da.a(6, this.a, "Internal ERROR! Cannot read!");
                            this.j.a(next, str);
                        } else {
                            ? r6 = a2.b;
                            if (r6 == 0 || r6.length == 0) {
                                da.a(6, this.a, "Internal ERROR! Report is empty!");
                                this.j.a(next, str);
                            } else {
                                da.a(5, this.a, "Reading block info ".concat(String.valueOf(next)));
                                this.i.add(next);
                                final String d = d();
                                String str3 = this.a;
                                da.a(4, str3, "FlurryDataSender: start upload data with id = " + next + " to " + d);
                                df dfVar = new df();
                                dfVar.f = d;
                                dfVar.p = 100000;
                                dfVar.g = dh.a.kPost;
                                dfVar.a("Content-Type", "application/octet-stream");
                                dfVar.a("X-Flurry-Api-Key", bk.a().b());
                                dfVar.c = new dp();
                                dfVar.d = new du();
                                dfVar.b = r6;
                                ac acVar = n.a().h;
                                dfVar.n = acVar != null && acVar.j;
                                dfVar.a = new df.a<byte[], String>() {
                                    public final /* synthetic */ void a(df dfVar, Object obj) {
                                        final String str = (String) obj;
                                        final int i = dfVar.m;
                                        if (i != 200) {
                                            Future unused = bp.this.b(new ec() {
                                                public final void a() throws Exception {
                                                    bp.this.a(i, bp.a(str), next);
                                                }
                                            });
                                        }
                                        if (i == 200 || i == 400) {
                                            String str2 = bp.this.a;
                                            da.e(str2, "Analytics report sent to " + d);
                                            String str3 = bp.this.a;
                                            da.a(3, str3, "FlurryDataSender: report " + next + " sent. HTTP response: " + i);
                                            bp bpVar = bp.this;
                                            bpVar.b(new ec(i, next, str) {
                                                final /* synthetic */ int a;
                                                final /* synthetic */ String b;
                                                final /* synthetic */ String c;

                                                {
                                                    this.a = r2;
                                                    this.b = r3;
                                                    this.c = r4;
                                                }

                                                public final void a() {
                                                    if (bp.this.h != null) {
                                                        if (this.a == 200) {
                                                            bp.this.h.a();
                                                        } else {
                                                            bp.this.h.b();
                                                        }
                                                    }
                                                    if (!bp.this.j.a(this.b, this.c)) {
                                                        String str = bp.this.a;
                                                        da.a(6, str, "Internal error. Block wasn't deleted with id = " + this.b);
                                                    }
                                                    if (!bp.this.i.remove(this.b)) {
                                                        String str2 = bp.this.a;
                                                        da.a(6, str2, "Internal error. Block with id = " + this.b + " was not in progress state");
                                                    }
                                                }
                                            });
                                            bp.this.b();
                                            return;
                                        }
                                        String str4 = bp.this.a;
                                        da.e(str4, "Analytics report sent with error " + d);
                                        bp bpVar2 = bp.this;
                                        bpVar2.b(new ec(next) {
                                            final /* synthetic */ String a;

                                            {
                                                this.a = r2;
                                            }

                                            public final void a() {
                                                if (bp.this.h != null) {
                                                    bp.this.h.b();
                                                }
                                                if (!bp.this.i.remove(this.a)) {
                                                    String str = bp.this.a;
                                                    da.a(6, str, "Internal error. Block with id = " + this.a + " was not in progress state");
                                                }
                                            }
                                        });
                                    }
                                };
                                cv.a().a(this, dfVar);
                            }
                        }
                    }
                }
            } else {
                return;
            }
        }
    }

    private boolean e() {
        return f() <= 5;
    }

    private int f() {
        return this.i.size();
    }

    /* access modifiers changed from: protected */
    public final void b(byte[] bArr, String str, String str2) {
        String str3 = this.b + str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2;
        bq bqVar = new bq(bArr);
        String str4 = bqVar.a;
        bq.b(str4).a(bqVar);
        da.a(5, this.a, "Saving Block File " + str4 + " at " + b.a().getFileStreamPath(bq.a(str4)));
        this.j.a(bqVar, str3);
    }

    static /* synthetic */ String a(String str) {
        if (str != null && str.contains("<title>") && str.contains("</title>")) {
            return str.substring(str.indexOf("<title>") + 7, str.indexOf("</title>"));
        }
        StringBuilder sb = new StringBuilder("Can not parse http error message: ");
        if (str == null) {
            str = "NULL";
        }
        sb.append(str);
        return sb.toString();
    }
}
