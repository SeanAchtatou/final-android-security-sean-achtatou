package com.flurry.sdk;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class gn extends jl {
    public final String a;
    public int b;
    public final Map<String, String> c;
    public final Map<String, String> d;
    public final boolean e;
    public final boolean f;
    public long g;
    public final a h;
    public final long i;
    public final long j;

    public enum a {
        CUSTOM_EVENT,
        PURCHASE_EVENT
    }

    public gn(String str, int i2, a aVar, Map<String, String> map, Map<String, String> map2, List<String> list, boolean z, boolean z2, long j2, long j3) {
        this.a = ea.b(ea.a(str));
        this.b = i2;
        this.h = aVar;
        this.c = map != null ? a(map, list) : new HashMap<>();
        this.d = map2 != null ? a(map2, list) : new HashMap<>();
        this.e = z;
        this.f = z2;
        this.i = j2;
        this.j = j3;
        this.g = 0;
    }

    public gn(String str, int i2, Map<String, String> map, Map<String, String> map2, long j2, long j3, long j4) {
        this.a = str;
        this.b = i2;
        this.h = a.CUSTOM_EVENT;
        this.c = map;
        this.d = map2;
        this.e = true;
        this.f = false;
        this.i = j2;
        this.j = j3;
        this.g = j4;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.event.name", this.a);
        jSONObject.put("fl.event.id", this.b);
        jSONObject.put("fl.event.type", this.h.toString());
        jSONObject.put("fl.event.timed", this.e);
        jSONObject.put("fl.timed.event.starting", this.f);
        long j2 = this.g;
        if (j2 > 0) {
            jSONObject.put("fl.timed.event.duration", j2);
        }
        jSONObject.put("fl.event.timestamp", this.i);
        jSONObject.put("fl.event.uptime", this.j);
        jSONObject.put("fl.event.user.parameters", eb.a(this.c));
        jSONObject.put("fl.event.flurry.parameters", eb.a(this.d));
        return jSONObject;
    }

    private static Map<String, String> a(Map<String, String> map, List<String> list) {
        String str;
        String str2;
        HashMap hashMap = new HashMap();
        for (Map.Entry next : map.entrySet()) {
            if (!list.contains(next.getKey())) {
                str = ea.b((String) next.getKey());
                str2 = ea.b((String) next.getValue());
            } else {
                str = ea.b((String) next.getKey());
                str2 = (String) next.getValue();
            }
            if (!TextUtils.isEmpty(str)) {
                hashMap.put(str, str2);
            }
        }
        return hashMap;
    }
}
