package com.flurry.sdk;

import com.flurry.sdk.dh;
import java.io.InputStream;
import java.io.OutputStream;

public final class df<RequestObjectType, ResponseObjectType> extends dh {
    public a<RequestObjectType, ResponseObjectType> a;
    public RequestObjectType b;
    public dt<RequestObjectType> c;
    public dt<ResponseObjectType> d;
    /* access modifiers changed from: private */
    public ResponseObjectType q;

    public interface a<RequestObjectType, ResponseObjectType> {
        void a(df<RequestObjectType, ResponseObjectType> dfVar, ResponseObjectType responseobjecttype);
    }

    public final void a() {
        this.h = new dh.b() {
            public final void a(OutputStream outputStream) throws Exception {
                if (df.this.b != null && df.this.c != null) {
                    df.this.c.a(outputStream, df.this.b);
                }
            }

            public final void a() {
                df.d(df.this);
            }

            public final void a(dh dhVar, InputStream inputStream) throws Exception {
                if ((dhVar.m >= 200 && dhVar.m < 400 && !dhVar.o) && df.this.d != null) {
                    df dfVar = df.this;
                    Object unused = dfVar.q = dfVar.d.a(inputStream);
                }
            }
        };
        super.a();
    }

    static /* synthetic */ void d(df dfVar) {
        if (dfVar.a != null && !dfVar.c()) {
            dfVar.a.a(dfVar, dfVar.q);
        }
    }
}
