package com.flurry.sdk;

import android.os.Build;
import android.text.TextUtils;
import com.flurry.sdk.ah;
import com.flurry.sdk.k;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public final class ai {
    byte[] a;
    private aj b;
    private int c;
    private final k<byte[]> d;
    private l<ah> e;

    ai() {
        this.b = null;
        this.c = 0;
        this.a = null;
        this.e = null;
        this.d = new k<>(new dp());
        this.b = new aj();
    }

    public final void a() {
        if (this.e == null) {
            this.e = new l<>(c(), "installationNum", 1, new dw<ah>() {
                public final dt<ah> a(int i) {
                    return new ah.a();
                }
            });
            byte[] a2 = a(d());
            if (a2 != null && Build.VERSION.SDK_INT >= 23) {
                dz.b(c());
                a(a2, k.a.CRYPTO_ALGO_PADDING_7);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.k.a(byte[], java.security.Key, javax.crypto.spec.IvParameterSpec, com.flurry.sdk.k$a):byte[]
     arg types: [byte[], java.security.Key, javax.crypto.spec.IvParameterSpec, com.flurry.sdk.k$a]
     candidates:
      com.flurry.sdk.k.a(byte[], java.security.Key, javax.crypto.spec.IvParameterSpec, com.flurry.sdk.k$a):ObjectType
      com.flurry.sdk.k.a(byte[], java.security.Key, javax.crypto.spec.IvParameterSpec, com.flurry.sdk.k$a):byte[] */
    /* access modifiers changed from: package-private */
    public final boolean a(byte[] bArr, k.a aVar) {
        ah ahVar;
        try {
            dz.b(c());
            byte[] e2 = e();
            byte[] a2 = this.d.a(bArr, b(), new IvParameterSpec(e2), aVar);
            if (a2 != null) {
                ahVar = new ah(a2, e2, true, aVar.ordinal());
            } else {
                ahVar = new ah(bArr, new byte[0], false, aVar.ordinal());
            }
            this.e.a(ahVar);
            return true;
        } catch (IOException e3) {
            da.a(5, "InstallationIdProvider", "Error while generating UUID" + e3.getMessage(), e3);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.k.a(byte[], java.security.Key, javax.crypto.spec.IvParameterSpec, com.flurry.sdk.k$a):ObjectType
     arg types: [byte[], java.security.Key, javax.crypto.spec.IvParameterSpec, com.flurry.sdk.k$a]
     candidates:
      com.flurry.sdk.k.a(byte[], java.security.Key, javax.crypto.spec.IvParameterSpec, com.flurry.sdk.k$a):byte[]
      com.flurry.sdk.k.a(byte[], java.security.Key, javax.crypto.spec.IvParameterSpec, com.flurry.sdk.k$a):ObjectType */
    /* access modifiers changed from: package-private */
    public final byte[] a(Key key) {
        try {
            ah a2 = this.e.a();
            if (a2 == null) {
                return null;
            }
            if (!a2.a) {
                return a2.c;
            }
            byte[] bArr = a2.b;
            byte[] bArr2 = a2.c;
            k.a a3 = k.a.a(a2.d);
            if (bArr == null || bArr2 == null) {
                return null;
            }
            return this.d.a(bArr2, key, new IvParameterSpec(bArr), a3);
        } catch (IOException unused) {
            da.a(5, "InstallationIdProvider", "Error while reading Android Install Id. Deleting file.");
            return null;
        }
    }

    private static File c() {
        return new File(dz.b().getPath() + File.separator + "installationNum");
    }

    private static SecretKey d() {
        bk a2 = bk.a();
        da.a(3, "APIKeyProvider", "Getting legacy apikey: " + a2.b);
        String str = a2.b;
        String a3 = dy.a(b.a());
        try {
            return new SecretKeySpec(SecretKeyFactory.getInstance("PBEWithSHA256And256BitAES-CBC-BC").generateSecret(new PBEKeySpec(str.toCharArray(), ByteBuffer.allocate(8).putLong(!TextUtils.isEmpty(a3) ? ea.e(a3) : Long.MIN_VALUE).array(), 1000, 256)).getEncoded(), "AES");
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e2) {
            da.a(4, "InstallationIdProvider", "Error in generate secret key", e2);
            return null;
        }
    }

    private static byte[] e() {
        try {
            byte[] bArr = new byte[16];
            SecureRandom.getInstance("SHA1PRNG").nextBytes(bArr);
            return bArr;
        } catch (NoSuchAlgorithmException e2) {
            da.a(4, "InstallationIdProvider", "Error in generating iv", e2);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final Key b() {
        if (Build.VERSION.SDK_INT < 23) {
            return d();
        }
        return this.b.a();
    }
}
