package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class hd extends jl {
    public final String a;
    public final String b;

    public hd(String str, String str2) {
        this.a = str == null ? "" : str;
        this.b = str2 == null ? "" : str2;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.session.property.param.name", this.a);
        jSONObject.put("fl.session.property.param.value", this.b);
        return jSONObject;
    }
}
