package com.flurry.sdk;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ag extends m<ak> {
    protected ak b = null;
    protected q h;
    protected ai i;
    protected af j;
    protected ae k;
    protected AtomicBoolean l = new AtomicBoolean(false);
    protected o<r> m = new o<r>() {
        public final /* synthetic */ void a(Object obj) {
            ag.this.e();
            ag.a((r) obj);
        }
    };

    public ag(q qVar) {
        super("IdProvider");
        this.h = qVar;
        this.i = new ai();
        this.j = new af();
        this.k = new ae();
        this.b = new ak();
        this.h.a(this.m);
    }

    public final void a(o<ak> oVar) {
        super.a((o) oVar);
    }

    public final void b() {
        b(new ec() {
            public final void a() throws Exception {
                ag.this.i.a();
            }
        });
        b(new ec() {
            public final void a() throws Exception {
                n.a().p.a("IdProvider: Provider start");
                ag.this.e();
            }
        });
    }

    public final ak d() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final void e() {
        b(new ec() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.flurry.sdk.ag.a(com.flurry.sdk.ag, java.lang.Runnable):java.util.concurrent.Future
             arg types: [com.flurry.sdk.ag, com.flurry.sdk.ag$4$1]
             candidates:
              com.flurry.sdk.ag.a(com.flurry.sdk.ag, java.lang.Object):void
              com.flurry.sdk.m.a(com.flurry.sdk.m, java.lang.Runnable):java.util.concurrent.Future
              com.flurry.sdk.ag.a(com.flurry.sdk.ag, java.lang.Runnable):java.util.concurrent.Future */
            /* JADX WARNING: Removed duplicated region for block: B:33:0x012d A[Catch:{ Exception -> 0x0137 }] */
            /* JADX WARNING: Removed duplicated region for block: B:39:0x0163 A[Catch:{ Exception -> 0x01c9 }] */
            /* JADX WARNING: Removed duplicated region for block: B:56:0x01bb A[Catch:{ Exception -> 0x01c9 }] */
            /* JADX WARNING: Removed duplicated region for block: B:61:0x01fb  */
            /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void a() {
                /*
                    r13 = this;
                    java.lang.String r0 = "IdProvider"
                    com.flurry.sdk.ag r1 = com.flurry.sdk.ag.this
                    java.util.concurrent.atomic.AtomicBoolean r1 = r1.l
                    r2 = 0
                    r1.set(r2)
                    com.flurry.sdk.ag r1 = com.flurry.sdk.ag.this
                    com.flurry.sdk.ai r1 = r1.i
                    r1.a()
                    r1 = 6
                    r3 = 1
                    r4 = 0
                    com.flurry.sdk.ag r5 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.ae r5 = r5.k     // Catch:{ Exception -> 0x007e }
                    java.lang.String r6 = "advertising_id"
                    java.lang.String r6 = com.flurry.sdk.ff.b(r6, r4)     // Catch:{ Exception -> 0x007e }
                    java.lang.String r7 = "ad_tracking_enabled"
                    android.content.Context r8 = com.flurry.sdk.b.a()     // Catch:{ Exception -> 0x007e }
                    java.lang.String r9 = "FLURRY_SHARED_PREFERENCES"
                    android.content.SharedPreferences r8 = r8.getSharedPreferences(r9, r2)     // Catch:{ Exception -> 0x007e }
                    java.util.Locale r9 = java.util.Locale.US     // Catch:{ Exception -> 0x007e }
                    java.lang.String r10 = "com.flurry.sdk.%s"
                    java.lang.Object[] r11 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x007e }
                    r11[r2] = r7     // Catch:{ Exception -> 0x007e }
                    java.lang.String r7 = java.lang.String.format(r9, r10, r11)     // Catch:{ Exception -> 0x007e }
                    boolean r2 = r8.getBoolean(r7, r2)     // Catch:{ Exception -> 0x007e }
                    r2 = r2 ^ r3
                    if (r6 == 0) goto L_0x0042
                    r5.a = r6     // Catch:{ Exception -> 0x007e }
                    r5.b = r2     // Catch:{ Exception -> 0x007e }
                    goto L_0x0045
                L_0x0042:
                    r5.a()     // Catch:{ Exception -> 0x007e }
                L_0x0045:
                    com.flurry.sdk.ag r2 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.ae r2 = r2.k     // Catch:{ Exception -> 0x007e }
                    java.lang.String r2 = r2.a     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.ag r5 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.ae r5 = r5.k     // Catch:{ Exception -> 0x007e }
                    boolean r5 = r5.b     // Catch:{ Exception -> 0x007e }
                    boolean r6 = r2.isEmpty()     // Catch:{ Exception -> 0x007e }
                    if (r6 != 0) goto L_0x0066
                    com.flurry.sdk.ag r6 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.ak r6 = r6.b     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.al r7 = com.flurry.sdk.al.AndroidAdvertisingId     // Catch:{ Exception -> 0x007e }
                    r6.a(r7, r2)     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.ag r6 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.ak r6 = r6.b     // Catch:{ Exception -> 0x007e }
                    r6.b = r5     // Catch:{ Exception -> 0x007e }
                L_0x0066:
                    com.flurry.sdk.ag r6 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.ae r6 = r6.k     // Catch:{ Exception -> 0x007e }
                    java.util.concurrent.atomic.AtomicBoolean r6 = r6.h     // Catch:{ Exception -> 0x007e }
                    boolean r6 = r6.get()     // Catch:{ Exception -> 0x007e }
                    r6 = r6 ^ r3
                    if (r6 == 0) goto L_0x00a1
                    com.flurry.sdk.ag r6 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x007e }
                    com.flurry.sdk.ag$4$1 r7 = new com.flurry.sdk.ag$4$1     // Catch:{ Exception -> 0x007e }
                    r7.<init>(r5, r2)     // Catch:{ Exception -> 0x007e }
                    java.util.concurrent.Future unused = r6.b(r7)     // Catch:{ Exception -> 0x007e }
                    goto L_0x00a1
                L_0x007e:
                    r2 = move-exception
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    java.lang.String r6 = "Error Fetching Ad Id - "
                    r5.<init>(r6)
                    java.lang.String r6 = r2.getLocalizedMessage()
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    com.flurry.sdk.da.a(r1, r0, r5)
                    com.flurry.sdk.n r5 = com.flurry.sdk.n.a()
                    com.flurry.sdk.bg r5 = r5.p
                    java.lang.String r6 = "Error fetching Ad Id"
                    java.lang.String r7 = "Exception happened during fetching Ad Id"
                    r5.a(r6, r7, r2)
                L_0x00a1:
                    com.flurry.sdk.ag r2 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x0137 }
                    com.flurry.sdk.af r2 = r2.j     // Catch:{ Exception -> 0x0137 }
                    java.lang.String r5 = r2.a     // Catch:{ Exception -> 0x0137 }
                    boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x0137 }
                    if (r5 == 0) goto L_0x0129
                    android.content.Context r5 = com.flurry.sdk.b.a()     // Catch:{ Exception -> 0x0137 }
                    android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ Exception -> 0x0137 }
                    java.lang.String r6 = "android_id"
                    java.lang.String r5 = android.provider.Settings.Secure.getString(r5, r6)     // Catch:{ Exception -> 0x0137 }
                    java.util.Set<java.lang.String> r6 = r2.b     // Catch:{ Exception -> 0x0137 }
                    java.util.Locale r7 = java.util.Locale.US     // Catch:{ Exception -> 0x0137 }
                    java.lang.String r7 = r5.toLowerCase(r7)     // Catch:{ Exception -> 0x0137 }
                    boolean r6 = r6.contains(r7)     // Catch:{ Exception -> 0x0137 }
                    if (r6 == 0) goto L_0x00cb
                    r5 = r4
                    goto L_0x00d5
                L_0x00cb:
                    java.lang.String r6 = "AND"
                    java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0137 }
                    java.lang.String r5 = r6.concat(r5)     // Catch:{ Exception -> 0x0137 }
                L_0x00d5:
                    boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x0137 }
                    if (r6 != 0) goto L_0x00dc
                    goto L_0x012b
                L_0x00dc:
                    java.lang.String r5 = com.flurry.sdk.af.a()     // Catch:{ Exception -> 0x0137 }
                    boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x0137 }
                    if (r6 == 0) goto L_0x0127
                    java.lang.String r5 = r2.b()     // Catch:{ Exception -> 0x0137 }
                    boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x0137 }
                    if (r6 == 0) goto L_0x0124
                    double r5 = java.lang.Math.random()     // Catch:{ Exception -> 0x0137 }
                    long r5 = java.lang.Double.doubleToLongBits(r5)     // Catch:{ Exception -> 0x0137 }
                    long r7 = java.lang.System.nanoTime()     // Catch:{ Exception -> 0x0137 }
                    android.content.Context r9 = com.flurry.sdk.b.a()     // Catch:{ Exception -> 0x0137 }
                    java.lang.String r9 = com.flurry.sdk.dy.a(r9)     // Catch:{ Exception -> 0x0137 }
                    long r9 = com.flurry.sdk.ea.e(r9)     // Catch:{ Exception -> 0x0137 }
                    r11 = 37
                    long r9 = r9 * r11
                    long r7 = r7 + r9
                    long r7 = r7 * r11
                    long r5 = r5 + r7
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0137 }
                    java.lang.String r8 = "ID"
                    r7.<init>(r8)     // Catch:{ Exception -> 0x0137 }
                    r8 = 16
                    java.lang.String r5 = java.lang.Long.toString(r5, r8)     // Catch:{ Exception -> 0x0137 }
                    r7.append(r5)     // Catch:{ Exception -> 0x0137 }
                    java.lang.String r5 = r7.toString()     // Catch:{ Exception -> 0x0137 }
                L_0x0124:
                    com.flurry.sdk.af.a(r5)     // Catch:{ Exception -> 0x0137 }
                L_0x0127:
                    r2.a = r5     // Catch:{ Exception -> 0x0137 }
                L_0x0129:
                    java.lang.String r5 = r2.a     // Catch:{ Exception -> 0x0137 }
                L_0x012b:
                    if (r5 == 0) goto L_0x015a
                    com.flurry.sdk.ag r2 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x0137 }
                    com.flurry.sdk.ak r2 = r2.b     // Catch:{ Exception -> 0x0137 }
                    com.flurry.sdk.al r6 = com.flurry.sdk.al.DeviceId     // Catch:{ Exception -> 0x0137 }
                    r2.a(r6, r5)     // Catch:{ Exception -> 0x0137 }
                    goto L_0x015a
                L_0x0137:
                    r2 = move-exception
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    java.lang.String r6 = "Error Fetching Device Id - "
                    r5.<init>(r6)
                    java.lang.String r6 = r2.getLocalizedMessage()
                    r5.append(r6)
                    java.lang.String r5 = r5.toString()
                    com.flurry.sdk.da.a(r1, r0, r5)
                    com.flurry.sdk.n r5 = com.flurry.sdk.n.a()
                    com.flurry.sdk.bg r5 = r5.p
                    java.lang.String r6 = "Error fetching Device Id"
                    java.lang.String r7 = "Exception happened during fetching Device Id"
                    r5.a(r6, r7, r2)
                L_0x015a:
                    com.flurry.sdk.ag r2 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x01c9 }
                    com.flurry.sdk.ai r2 = r2.i     // Catch:{ Exception -> 0x01c9 }
                    byte[] r5 = r2.a     // Catch:{ Exception -> 0x01c9 }
                    r6 = 2
                    if (r5 != 0) goto L_0x01b9
                    int r5 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x01c9 }
                    r7 = 23
                    if (r5 >= r7) goto L_0x016c
                    com.flurry.sdk.k$a r5 = com.flurry.sdk.k.a.CRYPTO_ALGO_PADDING_5     // Catch:{ Exception -> 0x01c9 }
                    goto L_0x016e
                L_0x016c:
                    com.flurry.sdk.k$a r5 = com.flurry.sdk.k.a.CRYPTO_ALGO_PADDING_7     // Catch:{ Exception -> 0x01c9 }
                L_0x016e:
                    java.security.Key r7 = r2.b()     // Catch:{ Exception -> 0x01c9 }
                    byte[] r7 = r2.a(r7)     // Catch:{ Exception -> 0x01c9 }
                    if (r7 != 0) goto L_0x01b6
                    java.util.UUID r7 = java.util.UUID.randomUUID()     // Catch:{ Exception -> 0x01c9 }
                    java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x01c9 }
                    java.util.Locale r8 = java.util.Locale.ENGLISH     // Catch:{ Exception -> 0x01c9 }
                    java.lang.String r7 = r7.toLowerCase(r8)     // Catch:{ Exception -> 0x01c9 }
                    boolean r8 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Exception -> 0x01c9 }
                    if (r8 == 0) goto L_0x018d
                    goto L_0x01b1
                L_0x018d:
                    java.lang.String r8 = "[^a-f0-9]+"
                    java.lang.String r9 = ""
                    java.lang.String r7 = r7.replaceAll(r8, r9)     // Catch:{ Exception -> 0x01c9 }
                    int r8 = r7.length()     // Catch:{ Exception -> 0x01c9 }
                    int r8 = r8 % r6
                    if (r8 == 0) goto L_0x01ad
                    r8 = 4
                    java.lang.String r9 = "InstallationIdProvider"
                    java.lang.String r10 = "Input string must contain an even number of characters "
                    java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x01c9 }
                    java.lang.String r7 = r10.concat(r7)     // Catch:{ Exception -> 0x01c9 }
                    com.flurry.sdk.da.a(r8, r9, r7)     // Catch:{ Exception -> 0x01c9 }
                    goto L_0x01b1
                L_0x01ad:
                    byte[] r4 = com.flurry.sdk.ea.c(r7)     // Catch:{ Exception -> 0x01c9 }
                L_0x01b1:
                    r2.a(r4, r5)     // Catch:{ Exception -> 0x01c9 }
                    r5 = r4
                    goto L_0x01b7
                L_0x01b6:
                    r5 = r7
                L_0x01b7:
                    r2.a = r5     // Catch:{ Exception -> 0x01c9 }
                L_0x01b9:
                    if (r5 == 0) goto L_0x01ec
                    java.lang.String r2 = android.util.Base64.encodeToString(r5, r6)     // Catch:{ Exception -> 0x01c9 }
                    com.flurry.sdk.ag r4 = com.flurry.sdk.ag.this     // Catch:{ Exception -> 0x01c9 }
                    com.flurry.sdk.ak r4 = r4.b     // Catch:{ Exception -> 0x01c9 }
                    com.flurry.sdk.al r5 = com.flurry.sdk.al.AndroidInstallationId     // Catch:{ Exception -> 0x01c9 }
                    r4.a(r5, r2)     // Catch:{ Exception -> 0x01c9 }
                    goto L_0x01ec
                L_0x01c9:
                    r2 = move-exception
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    java.lang.String r5 = "Error Fetching Install Id - "
                    r4.<init>(r5)
                    java.lang.String r5 = r2.getLocalizedMessage()
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    com.flurry.sdk.da.a(r1, r0, r4)
                    com.flurry.sdk.n r0 = com.flurry.sdk.n.a()
                    com.flurry.sdk.bg r0 = r0.p
                    java.lang.String r1 = "Error fetching Install Id"
                    java.lang.String r4 = "Exception happened during fetching Install Id"
                    r0.a(r1, r4, r2)
                L_0x01ec:
                    com.flurry.sdk.ag r0 = com.flurry.sdk.ag.this
                    java.util.concurrent.atomic.AtomicBoolean r0 = r0.l
                    r0.set(r3)
                    com.flurry.sdk.ag r0 = com.flurry.sdk.ag.this
                    boolean r0 = r0.f()
                    if (r0 == 0) goto L_0x0206
                    com.flurry.sdk.ag r0 = com.flurry.sdk.ag.this
                    com.flurry.sdk.ak r1 = r0.b
                    com.flurry.sdk.ak r1 = r1.b()
                    r0.a(r1)
                L_0x0206:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.ag.AnonymousClass4.a():void");
            }
        });
    }

    public final boolean f() {
        boolean z = this.l.get();
        ak akVar = this.b;
        return z && (akVar != null && akVar.a() != null && this.b.a().size() > 0);
    }

    public final void c() {
        super.c();
        this.h.b(this.m);
    }

    public final void c(final o<ak> oVar) {
        super.a((o) oVar);
        if (f()) {
            b(new ec() {
                public final void a() throws Exception {
                    oVar.a(ag.this.b.b());
                }
            });
        }
    }

    static /* synthetic */ void a(r rVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("previous_state", rVar.a.name());
        hashMap.put("current_state", rVar.b.name());
        n.a().p.a("IdProvider: App State Change", hashMap);
    }
}
