package com.flurry.sdk;

import com.flurry.sdk.ed;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class cz<T extends ed> {
    final HashMap<T, Future<?>> a = new HashMap<>();
    private final cw<Object, T> b = new cw<>();
    private final HashMap<T, Object> c = new HashMap<>();
    private final ThreadPoolExecutor d;

    public cz(String str, TimeUnit timeUnit, BlockingQueue<Runnable> blockingQueue) {
        this.d = new ThreadPoolExecutor(timeUnit, blockingQueue) {
            /* access modifiers changed from: protected */
            public final void beforeExecute(Thread thread, Runnable runnable) {
                super.beforeExecute(thread, runnable);
                final ed a2 = cz.a(runnable);
                if (a2 != null) {
                    new ec() {
                        public final void a() {
                        }
                    }.run();
                }
            }

            /* access modifiers changed from: protected */
            public final void afterExecute(Runnable runnable, Throwable th) {
                super.afterExecute(runnable, th);
                final ed a2 = cz.a(runnable);
                if (a2 != null) {
                    synchronized (cz.this.a) {
                        cz.this.a.remove(a2);
                    }
                    cz.this.a(a2);
                    new ec() {
                        public final void a() {
                        }
                    }.run();
                }
            }

            /* access modifiers changed from: protected */
            public final <V> RunnableFuture<V> newTaskFor(Runnable runnable, V v) {
                cy cyVar = new cy(runnable, v);
                synchronized (cz.this.a) {
                    cz.this.a.put((ed) runnable, cyVar);
                }
                return cyVar;
            }

            /* access modifiers changed from: protected */
            public final <V> RunnableFuture<V> newTaskFor(Callable<V> callable) {
                throw new UnsupportedOperationException("Callable not supported");
            }
        };
        this.d.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy() {
            public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                super.rejectedExecution(runnable, threadPoolExecutor);
                final ed a2 = cz.a(runnable);
                if (a2 != null) {
                    synchronized (cz.this.a) {
                        cz.this.a.remove(a2);
                    }
                    cz.this.a(a2);
                    new ec() {
                        public final void a() {
                        }
                    }.run();
                }
            }
        });
        this.d.setThreadFactory(new dx(str));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(T t) {
        b(this.c.get(t), t);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cw.a(java.lang.String, boolean):java.util.List<V>
     arg types: [java.lang.Object, int]
     candidates:
      com.flurry.sdk.cw.a(java.lang.String, java.lang.String):void
      com.flurry.sdk.cw.a(java.lang.String, boolean):java.util.List<V> */
    private synchronized void b(Object obj, T t) {
        List<T> a2;
        cw<Object, T> cwVar = this.b;
        if (!(obj == null || (a2 = cwVar.a((String) obj, false)) == null)) {
            a2.remove(t);
            if (a2.size() == 0) {
                cwVar.a.remove(obj);
            }
        }
        this.c.remove(t);
    }

    public final synchronized void a(Object obj, T t) {
        if (obj != null) {
            c(obj, t);
            this.d.submit((Runnable) t);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cw.a(java.lang.String, java.lang.String):void
     arg types: [java.lang.Object, T]
     candidates:
      com.flurry.sdk.cw.a(java.lang.String, boolean):java.util.List<V>
      com.flurry.sdk.cw.a(java.lang.String, java.lang.String):void */
    private synchronized void c(Object obj, T t) {
        this.b.a((String) obj, (String) t);
        this.c.put(t, obj);
    }

    static /* synthetic */ ed a(Runnable runnable) {
        if (runnable instanceof cy) {
            return (ed) ((cy) runnable).a();
        }
        if (runnable instanceof ed) {
            return (ed) runnable;
        }
        da.a(6, "TrackedThreadPoolExecutor", "Unknown runnable class: " + runnable.getClass().getName());
        return null;
    }
}
