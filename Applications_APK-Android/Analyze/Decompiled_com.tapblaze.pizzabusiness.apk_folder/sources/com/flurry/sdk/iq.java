package com.flurry.sdk;

import com.facebook.internal.AnalyticsEvents;
import org.json.JSONException;
import org.json.JSONObject;

public final class iq extends jl {
    public int a = 0;
    public String b = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;

    public iq(int i, String str) {
        this.a = i;
        this.b = str;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.flush.frame.code", this.a);
        jSONObject.put("fl.flush.frame.reason", this.b);
        return jSONObject;
    }
}
