package com.flurry.sdk;

import android.util.Base64;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

final class ho implements o<ak> {
    ho() {
    }

    public final /* synthetic */ void a(Object obj) {
        ak akVar = (ak) obj;
        boolean z = akVar.b;
        Map<al, String> a = akVar.a();
        if (a == null || a.size() == 0) {
            da.a(2, "ReportedIDFrame", "Reported ids is empty, do not send the frame.");
        } else {
            fc.a().a(new jf(new jg(a, z)));
        }
        Map<al, String> a2 = akVar.a();
        HashMap hashMap = new HashMap();
        for (Map.Entry next : a2.entrySet()) {
            if (((al) next.getKey()).equals(al.AndroidInstallationId)) {
                hashMap.put(((al) next.getKey()).name(), ea.a(Base64.decode((String) next.getValue(), 2)).toUpperCase(Locale.getDefault()));
            } else {
                hashMap.put(((al) next.getKey()).name(), next.getValue());
            }
        }
        n.a().p.a("Reported Ids", hashMap);
        da.a(4, "IdObserver", "IdProvider" + akVar.a());
    }
}
