package com.flurry.sdk;

import java.util.Timer;
import java.util.TimerTask;

public final class dg {
    dh a;
    private Timer b;
    private a c;

    public dg(dh dhVar) {
        this.a = dhVar;
    }

    public final synchronized void a() {
        if (this.b != null) {
            this.b.cancel();
            this.b = null;
            da.a(3, "HttpRequestTimeoutTimer", "HttpRequestTimeoutTimer stopped.");
        }
        this.c = null;
    }

    class a extends TimerTask {
        private a() {
        }

        /* synthetic */ a(dg dgVar, byte b) {
            this();
        }

        public final void run() {
            da.a(3, "HttpRequestTimeoutTimer", "HttpRequest timed out. Cancelling.");
            dh dhVar = dg.this.a;
            long currentTimeMillis = System.currentTimeMillis() - dhVar.l;
            da.a(3, "HttpStreamRequest", "Timeout (" + currentTimeMillis + "MS) for url: " + dhVar.f);
            dhVar.m = 629;
            dhVar.o = true;
            dhVar.b();
            da.a(3, "HttpStreamRequest", "Cancelling http request: " + dhVar.f);
            synchronized (dhVar.e) {
                dhVar.k = true;
            }
            if (!dhVar.j) {
                dhVar.j = true;
                if (dhVar.i != null) {
                    new Thread() {
                        public final void run() {
                            try {
                                if (dh.this.i != null) {
                                    dh.this.i.disconnect();
                                }
                            } catch (Throwable unused) {
                            }
                        }
                    }.start();
                }
            }
        }
    }

    public final synchronized void a(long j) {
        if (this.b != null) {
            a();
        }
        this.b = new Timer("HttpRequestTimeoutTimer");
        this.c = new a(this, (byte) 0);
        this.b.schedule(this.c, j);
        da.a(3, "HttpRequestTimeoutTimer", "HttpRequestTimeoutTimer started: " + j + "MS");
    }
}
