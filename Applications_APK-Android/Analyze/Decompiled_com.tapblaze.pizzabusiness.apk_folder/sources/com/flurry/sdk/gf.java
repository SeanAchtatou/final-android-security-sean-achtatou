package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class gf extends jl {
    public final Long a;

    public gf(Long l) {
        this.a = l;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (this.a.longValue() != Long.MIN_VALUE) {
            jSONObject.put("fl.demo.birthdate", this.a);
        }
        return jSONObject;
    }
}
