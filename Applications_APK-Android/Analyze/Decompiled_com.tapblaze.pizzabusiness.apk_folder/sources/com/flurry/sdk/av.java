package com.flurry.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.flurry.sdk.au;

public final class av extends m<au> {
    public boolean b;
    protected BroadcastReceiver h = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            av.this.a();
        }
    };
    protected o<r> i = new o<r>() {
        public final /* bridge */ /* synthetic */ void a(Object obj) {
            if (((r) obj).b == p.FOREGROUND) {
                av.this.a();
            }
        }
    };
    private boolean j;
    private q k;

    public av(q qVar) {
        super("NetworkProvider");
        if (fe.c()) {
            g();
            this.k = qVar;
            this.k.a(this.i);
            return;
        }
        this.b = true;
    }

    /* access modifiers changed from: private */
    public static boolean f() {
        if (!fe.c()) {
            return true;
        }
        NetworkInfo activeNetworkInfo = h().getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    private synchronized void g() {
        if (!this.j) {
            this.b = f();
            b.a().registerReceiver(this.h, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.j = true;
        }
    }

    private static ConnectivityManager h() {
        return (ConnectivityManager) b.a().getSystemService("connectivity");
    }

    private synchronized void i() {
        if (this.j) {
            b.a().unregisterReceiver(this.h);
            this.j = false;
        }
    }

    public final void c() {
        super.c();
        i();
        q qVar = this.k;
        if (qVar != null) {
            qVar.b(this.i);
        }
    }

    public static au.a d() {
        if (!fe.c()) {
            return au.a.NONE_OR_UNKNOWN;
        }
        NetworkInfo activeNetworkInfo = h().getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return au.a.NONE_OR_UNKNOWN;
        }
        int type = activeNetworkInfo.getType();
        if (type != 0) {
            if (type == 1) {
                return au.a.WIFI;
            }
            if (!(type == 2 || type == 3 || type == 4 || type == 5)) {
                if (type != 8) {
                    return au.a.NETWORK_AVAILABLE;
                }
                return au.a.NONE_OR_UNKNOWN;
            }
        }
        return au.a.CELL;
    }

    public final void a(o<au> oVar) {
        super.a((o) oVar);
        b(new ec() {
            public final void a() {
                boolean unused = av.this.b = av.f();
                av.this.a(new au(av.d(), av.this.b));
            }
        });
    }

    public final void a() {
        b(new ec() {
            public final void a() {
                boolean e = av.f();
                if (av.this.b != e) {
                    boolean unused = av.this.b = e;
                    av.this.a(new au(av.d(), av.this.b));
                }
            }
        });
    }
}
