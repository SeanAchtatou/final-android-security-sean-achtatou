package com.flurry.sdk;

import cz.msebera.android.httpclient.client.config.CookieSpecs;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public final class ca {
    private static String b;
    private static String c;
    a a;
    private Object d;

    public enum a {
        String("string"),
        Locale("localizedString"),
        Tombstone("tombstone");
        
        private String d;

        private a(String str) {
            this.d = str;
        }

        public final String toString() {
            return this.d;
        }
    }

    public ca(JSONObject jSONObject) {
        String optString = jSONObject.optString("type");
        if (a.a(a.String).equals(optString)) {
            this.a = a.String;
            this.d = jSONObject.optString("value");
        } else if (a.a(a.Locale).equals(optString)) {
            this.a = a.Locale;
            this.d = jSONObject.optJSONObject("value");
        } else if (a.a(a.Tombstone).equals(optString)) {
            this.a = a.Tombstone;
        } else {
            da.b("ConfigItem", "Unknown ConfigItem type: ".concat(String.valueOf(optString)));
        }
    }

    public final String a() {
        if (this.d == null) {
            return null;
        }
        if (this.a != a.Locale) {
            return (String) this.d;
        }
        if (b == null) {
            b = Locale.getDefault().toString();
            c = Locale.getDefault().getLanguage();
        }
        JSONObject jSONObject = (JSONObject) this.d;
        String optString = jSONObject.optString(b, null);
        if (optString == null) {
            optString = jSONObject.optString(c, null);
        }
        return optString == null ? jSONObject.optString(CookieSpecs.DEFAULT) : optString;
    }

    public final JSONObject a(String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", str);
            jSONObject.put("type", this.a.toString());
            jSONObject.put("value", this.d);
            return jSONObject;
        } catch (JSONException e) {
            da.a("ConfigItem", "Error to create JSON object.", e);
            return null;
        }
    }
}
