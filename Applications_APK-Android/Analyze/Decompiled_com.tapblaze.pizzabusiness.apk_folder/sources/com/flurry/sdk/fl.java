package com.flurry.sdk;

import com.flurry.sdk.fv;
import com.flurry.sdk.fz;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class fl extends fs {
    protected List<fv> a = new ArrayList();
    protected final Map<String, List<jp>> b = new HashMap();

    fl(fn fnVar) {
        super("DropModule", fnVar);
        this.a.add(new fu());
        this.a.add(new ft());
        this.a.add(new fw());
        this.a.add(new fx());
    }

    public final void a(final jp jpVar) {
        b(new ec() {
            public final void a() {
                fl.a(fl.this, fl.a(fl.this, jpVar));
                fl.b(fl.this, jpVar);
            }
        });
    }

    private List<jp> e(jp jpVar) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry<String, List<jp>> value : this.b.entrySet()) {
            for (jp f : (List) value.getValue()) {
                gn gnVar = (gn) f.f();
                String str = gnVar.a;
                int i = gnVar.b;
                long currentTimeMillis = System.currentTimeMillis();
                arrayList.add(gm.a(str, i, gnVar.c, gnVar.d, currentTimeMillis, currentTimeMillis - gnVar.i));
            }
        }
        arrayList.add(jpVar);
        return arrayList;
    }

    private static boolean f(jp jpVar) {
        return jpVar.a().equals(jn.FLUSH_FRAME) && ((iq) jpVar.f()).b.equals(fz.a.REASON_SESSION_FINALIZE.j);
    }

    static /* synthetic */ List a(fl flVar, jp jpVar) {
        if (jpVar.a().equals(jn.ANALYTICS_EVENT) && ((gn) jpVar.f()).e) {
            ArrayList arrayList = new ArrayList();
            String str = ((gn) jpVar.f()).a;
            List list = flVar.b.get(str);
            if (((gn) jpVar.f()).f) {
                if (list == null) {
                    list = new ArrayList();
                }
                list.add(jpVar);
                flVar.b.put(str, list);
                arrayList.add(jpVar);
                return arrayList;
            } else if (list == null || list.isEmpty()) {
                return arrayList;
            } else {
                gn gnVar = (gn) ((jp) list.remove(0)).f();
                gn gnVar2 = (gn) jpVar.f();
                gnVar2.b = gnVar.b;
                gnVar2.g = gnVar2.i - gnVar.i;
                Map<String, String> map = gnVar.c;
                Map<String, String> map2 = gnVar2.c;
                if (!(map == null || map2 == null)) {
                    Map<String, String> map3 = gnVar.d;
                    Map<String, String> map4 = gnVar2.d;
                    if (map3.get(ea.b("fl.parameter.limit.exceeded")) != null) {
                        map4.putAll(map3);
                        map2.clear();
                    } else if (map.size() + map2.size() > 10) {
                        map4.put(ea.b("fl.parameter.limit.exceeded.on.endevent"), ea.b(String.valueOf(map2.size())));
                        map2.clear();
                        map2.putAll(map);
                    } else if (!map.isEmpty()) {
                        map2.putAll(map);
                    }
                }
                arrayList.add(jpVar);
                return arrayList;
            }
        } else if (f(jpVar)) {
            return flVar.e(jpVar);
        } else {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(jpVar);
            return arrayList2;
        }
    }

    static /* synthetic */ void a(fl flVar, List list) {
        boolean z;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            jp jpVar = (jp) it.next();
            Iterator<fv> it2 = flVar.a.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = false;
                    break;
                }
                fv.a a2 = it2.next().a(jpVar);
                if (!a2.a.equals(fv.b.DO_NOT_DROP)) {
                    z = true;
                    break;
                } else if (a2.b != null) {
                    flVar.d(a2.b);
                }
            }
            if (!z) {
                da.a(4, "DropModule", "Adding Frame:" + jpVar.e());
                flVar.d(jpVar);
            } else {
                da.a(4, "DropModule", "Dropping Frame: " + jpVar.a() + ": " + jpVar.e());
            }
        }
    }

    static /* synthetic */ void b(fl flVar, jp jpVar) {
        if (f(jpVar)) {
            da.a(4, "DropModule", "Resetting drop rules");
            for (fv a2 : flVar.a) {
                a2.a();
            }
            da.a(4, "DropModule", "Reset start timed event record");
            flVar.b.clear();
        }
    }
}
