package com.flurry.sdk;

import android.content.Intent;
import android.os.SystemClock;
import com.android.billingclient.util.BillingHelper;
import com.flurry.android.FlurryEventRecordStatus;
import com.flurry.sdk.bv;
import com.flurry.sdk.gn;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONObject;

public final class gm extends jm {
    private static final AtomicInteger a = new AtomicInteger(0);

    private gm(jo joVar) {
        super(joVar);
    }

    public static FlurryEventRecordStatus a(String str, Map<String, String> map, boolean z, boolean z2, long j, long j2) {
        HashMap hashMap;
        if (map.size() > 10) {
            hashMap = new HashMap();
            hashMap.put("fl.parameter.limit.exceeded", String.valueOf(map.size()));
            map.clear();
        } else {
            hashMap = null;
        }
        Map<String, String> map2 = map;
        fc.a().a(new gm(new gn(ea.a(str), a.incrementAndGet(), gn.a.CUSTOM_EVENT, map2, hashMap, new ArrayList(), z, z2, j, j2)));
        return FlurryEventRecordStatus.kFlurryEventRecorded;
    }

    public static void a(int i, Intent intent, Map<String, String> map, long j, long j2) {
        int i2 = i;
        Intent intent2 = intent;
        if (intent2 != null && intent.getExtras() != null) {
            Object obj = intent.getExtras().get(BillingHelper.RESPONSE_CODE);
            int i3 = 0;
            if (obj == null) {
                da.b("StreamingEventFrame", "Intent with no response code, assuming OK (known issue)");
            } else if (obj instanceof Integer) {
                i3 = ((Integer) obj).intValue();
            } else if (obj instanceof Long) {
                i3 = (int) ((Long) obj).longValue();
            }
            final String stringExtra = intent2.getStringExtra("INAPP_PURCHASE_DATA");
            final String stringExtra2 = intent2.getStringExtra("INAPP_DATA_SIGNATURE");
            JSONObject jSONObject = new JSONObject();
            if (stringExtra != null) {
                try {
                    jSONObject = new JSONObject(stringExtra);
                } catch (Throwable th) {
                    da.a("StreamingEventFrame", "Failed to log event: Flurry.purchase", th);
                    return;
                }
            }
            String optString = jSONObject.optString("productId");
            final String optString2 = jSONObject.optString("orderId");
            if (i2 == -1 && i3 == 0) {
                final Map<String, String> map2 = map;
                final String str = optString;
                final long j3 = j;
                final long j4 = j2;
                bv.a(b.a(), optString, new bv.a() {
                    public final void a(int i, bv.c cVar) {
                        if (cVar != null) {
                            double d2 = (double) cVar.b;
                            Double.isNaN(d2);
                            double d3 = d2 / 1000000.0d;
                            HashMap hashMap = new HashMap();
                            if (map2.size() > 10) {
                                hashMap.put("fl.parameter.limit.exceeded", String.valueOf(map2.size()));
                                map2.clear();
                            }
                            hashMap.put("fl.Quantity", "1");
                            hashMap.put("fl.ProductID", str);
                            hashMap.put("fl.Price", String.format(Locale.ENGLISH, "%1$.2f", Double.valueOf(d3)));
                            hashMap.put("fl.Currency", cVar.c);
                            hashMap.put("fl.ProductName", cVar.d);
                            hashMap.put("fl.ProductType", cVar.a);
                            hashMap.put("fl.TransactionIdentifier", optString2);
                            hashMap.put("fl.OrderJSON", stringExtra);
                            hashMap.put("fl.OrderJSONSignature", stringExtra2);
                            hashMap.put("fl.StoreId", "2");
                            hashMap.put("fl.Receipt", stringExtra2 + "\n" + stringExtra);
                            FlurryEventRecordStatus unused = gm.b(map2, hashMap, j3, j4, new ArrayList());
                            return;
                        }
                        da.b("StreamingEventFrame", "Failed to load SKU Details from Google for '" + str + "'. Result: " + i);
                    }
                });
                return;
            }
            da.b("StreamingEventFrame", "Invalid logPayment call. resultCode:" + i + ", responseCode:" + i3 + ", purchaseData:" + stringExtra + ", dataSignature:" + stringExtra2);
        }
    }

    public static FlurryEventRecordStatus a(String str, String str2, int i, double d, String str3, String str4, Map<String, String> map, long j, long j2) {
        HashMap hashMap = new HashMap();
        if (map.size() > 10) {
            hashMap.put("fl.parameter.limit.exceeded", String.valueOf(map.size()));
            map.clear();
        }
        try {
            hashMap.put("fl.ProductName", str);
            hashMap.put("fl.ProductID", str2);
            hashMap.put("fl.Quantity", String.valueOf(i));
            hashMap.put("fl.Price", String.format(Locale.ENGLISH, "%1$.2f", Double.valueOf(d)));
            hashMap.put("fl.Currency", str3);
            hashMap.put("fl.TransactionIdentifier", str4);
            return b(map, hashMap, j, j2, new ArrayList());
        } catch (Throwable th) {
            da.a("StreamingEventFrame", "Failed to log event: Flurry.purchase", th);
            return FlurryEventRecordStatus.kFlurryEventRecorded;
        }
    }

    /* access modifiers changed from: private */
    public static FlurryEventRecordStatus b(Map<String, String> map, Map<String, String> map2, long j, long j2, List<String> list) {
        fc.a().a(new gm(new gn("Flurry.purchase", a.incrementAndGet(), gn.a.PURCHASE_EVENT, map, map2, list, false, false, j, j2)));
        return FlurryEventRecordStatus.kFlurryEventRecorded;
    }

    public final jn a() {
        return jn.ANALYTICS_EVENT;
    }

    public static gm a(String str, int i, Map<String, String> map, Map<String, String> map2, long j, long j2) {
        return new gm(new gn(str, i, map, map2, j, SystemClock.elapsedRealtime(), j2));
    }
}
