package com.flurry.sdk;

public interface fv {
    public static final a a = new a(b.DO_NOT_DROP, null);
    public static final a b = new a(b.DROP_EVENTS_UNIQUE_NAME_EXCEEDED, null);
    public static final a c = new a(b.DROP_EVENTS_NAME_INVALID, null);
    public static final a d = new a(b.DROP_EVENTS_COUNT_EXCEEDED, null);
    public static final a e = new a(b.DROP_TIMED_EVENTS_START_NOT_FOUND, null);
    public static final a f = new a(b.DROP_EVENTS_REASON_UNKNOWN, null);
    public static final a g = new a(b.DROP_ERROR_COUNT_EXCEEDED, null);

    a a(jp jpVar);

    void a();

    public enum b {
        DO_NOT_DROP("DoNotDrop"),
        DROP_EVENTS_UNIQUE_NAME_EXCEEDED("Unique Event Name exceeded"),
        DROP_EVENTS_NAME_INVALID("Invalid Event Name"),
        DROP_EVENTS_COUNT_EXCEEDED("Events count exceeded"),
        DROP_TIMED_EVENTS_START_NOT_FOUND("End Timed Event but Start not found"),
        DROP_EVENTS_REASON_UNKNOWN("reason unknown"),
        DROP_ERROR_COUNT_EXCEEDED("Error count exceeded");
        
        public final String h;

        private b(String str) {
            this.h = str;
        }
    }

    public static class a {
        public b a;
        public jp b;

        a(b bVar, jp jpVar) {
            this.a = bVar;
            this.b = jpVar;
        }
    }
}
