package com.flurry.sdk;

import android.text.TextUtils;
import com.flurry.sdk.fv;
import java.util.HashSet;
import java.util.Set;

public final class fu implements fv {
    private final Set<String> h = new HashSet();
    private final Set<Integer> i = new HashSet();
    private final Set<Integer> j = new HashSet();

    public final fv.a a(jp jpVar) {
        if (jpVar.a().equals(jn.FLUSH_FRAME)) {
            return new fv.a(fv.b.DO_NOT_DROP, new go(new gp(this.i.size() + this.j.size(), this.j.isEmpty())));
        }
        if (!jpVar.a().equals(jn.ANALYTICS_EVENT)) {
            return a;
        }
        gn gnVar = (gn) jpVar.f();
        String str = gnVar.a;
        int i2 = gnVar.b;
        if (TextUtils.isEmpty(str)) {
            return c;
        }
        if (a(gnVar) && !this.i.contains(Integer.valueOf(i2))) {
            this.j.add(Integer.valueOf(i2));
            return e;
        } else if (this.i.size() >= 1000 && !a(gnVar)) {
            this.j.add(Integer.valueOf(i2));
            return d;
        } else if (this.h.contains(str) || this.h.size() < 500) {
            this.h.add(str);
            this.i.add(Integer.valueOf(i2));
            return a;
        } else {
            this.j.add(Integer.valueOf(i2));
            return b;
        }
    }

    public final void a() {
        this.h.clear();
        this.i.clear();
        this.j.clear();
    }

    private static boolean a(gn gnVar) {
        return gnVar.e && !gnVar.f;
    }
}
