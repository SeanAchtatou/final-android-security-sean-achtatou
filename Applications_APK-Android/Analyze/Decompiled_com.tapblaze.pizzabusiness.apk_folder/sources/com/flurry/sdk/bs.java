package com.flurry.sdk;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class bs {
    String a;

    /* synthetic */ bs(byte b) {
        this();
    }

    private bs() {
    }

    public bs(String str) {
        this.a = str;
    }

    public static class a implements dt<bs> {
        public final /* synthetic */ void a(OutputStream outputStream, Object obj) throws IOException {
            bs bsVar = (bs) obj;
            if (outputStream != null && bsVar != null) {
                AnonymousClass1 r0 = new DataOutputStream(outputStream) {
                    public final void close() {
                    }
                };
                r0.writeUTF(bsVar.a);
                r0.flush();
            }
        }

        public final /* synthetic */ Object a(InputStream inputStream) throws IOException {
            if (inputStream == null) {
                return null;
            }
            AnonymousClass2 r0 = new DataInputStream(inputStream) {
                public final void close() {
                }
            };
            bs bsVar = new bs((byte) 0);
            bsVar.a = r0.readUTF();
            return bsVar;
        }
    }
}
