package com.flurry.sdk;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class cd {
    public static List<cl> a(JSONObject jSONObject) {
        JSONArray optJSONArray = jSONObject.optJSONArray("variants");
        if (optJSONArray == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                arrayList.add(b(optJSONObject));
            }
        }
        return arrayList;
    }

    private static cl b(JSONObject jSONObject) {
        String optString;
        cl clVar = new cl(ci.a(jSONObject.optString("document", ci.a.toString())));
        clVar.b = jSONObject.optInt("id");
        clVar.c = jSONObject.optInt("version");
        clVar.d = jSONObject;
        JSONArray optJSONArray = jSONObject.optJSONArray("items");
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (!(optJSONObject == null || (optString = optJSONObject.optString("name", null)) == null)) {
                    clVar.e.put(optString, new ca(optJSONObject));
                }
            }
        }
        return clVar;
    }
}
