package com.flurry.sdk;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class ja extends jl {
    public final String a;
    public final Map<String, String> b;

    public ja(String str, Map<String, String> map) {
        this.a = str;
        this.b = map == null ? new HashMap<>() : map;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject a2 = eb.a(this.b);
        jSONObject.put("fl.origin.attribute.name", this.a);
        jSONObject.put("fl.origin.attribute.parameters", a2);
        return jSONObject;
    }
}
