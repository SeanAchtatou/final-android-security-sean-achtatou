package com.flurry.sdk;

import android.net.http.X509TrustManagerExtensions;
import android.os.Build;
import android.util.Base64;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class di {
    public static final Set<String> a;
    private static X509TrustManagerExtensions b;

    static {
        HashSet hashSet = new HashSet(13);
        a = hashSet;
        hashSet.add("WoiWRyIOVNa9ihaBciRSC7XHjliYS9VwUGOIud4PB18=");
        a.add("SVqWumuteCQHvVIaALrOZXuzVVVeS7f4FGxxu6V+es4=");
        a.add("cAajgxHlj7GTSEIzIYIQxmEloOSoJq7VOaxWHfv72QM=");
        a.add("I/Lt/z7ekCWanjD0Cvj5EqXls2lOaThEA0H2Bg4BT/o=");
        a.add("Wd8xe/qfTwq3ylFNd3IpaqLHZbh2ZNCLluVzmeNkcpw=");
        a.add("JbQbUG5JMJUoI6brnx0x3vZF6jilxsapbXGVfjhN8Fg=");
        a.add("r/mIkG3eEpVdm+u/ko/cwxzOMo1bk4TyHIlByibiA5E=");
        a.add("UZJDjsNp1+4M5x9cbbdflB779y5YRBcV6Z6rBMLIrO4=");
        a.add("lnsM2T/O9/J84sJFdnrpsFp3awZJ+ZZbYpCWhGloaHI=");
        a.add("i7WTqTvh0OioIruIfFR4kMPnBqrS2rdiVPl/s2uC/CY=");
        a.add("uUwZgwDOxcBXrQcntwu+kYFpkiVkOaezL0WYEZ3anJc=");
        a.add("dolnbtzEBnELx/9lOEQ22e6OZO/QNb6VSSX2XHA3E7A=");
        a.add("2fRAUXyxl4A1/XHrKNBmc8bTkzA7y4FB/GLJuNAzCqY=");
        b = null;
        if (Build.VERSION.SDK_INT >= 17) {
            b = new X509TrustManagerExtensions(a());
        }
    }

    public static void a(HttpsURLConnection httpsURLConnection) throws SSLException {
        if (Build.VERSION.SDK_INT >= 17 && b != null) {
            String str = "";
            try {
                Certificate[] serverCertificates = httpsURLConnection.getServerCertificates();
                List<X509Certificate> checkServerTrusted = b.checkServerTrusted((X509Certificate[]) Arrays.copyOf(serverCertificates, serverCertificates.length, X509Certificate[].class), "RSA", httpsURLConnection.getURL().getHost());
                if (checkServerTrusted != null) {
                    MessageDigest instance = MessageDigest.getInstance(CommonUtils.SHA256_INSTANCE);
                    for (X509Certificate next : checkServerTrusted) {
                        byte[] encoded = next.getPublicKey().getEncoded();
                        instance.update(encoded, 0, encoded.length);
                        String encodeToString = Base64.encodeToString(instance.digest(), 2);
                        if (a.contains(encodeToString)) {
                            da.a("SslPinningValidator", "Found matched pin: ".concat(String.valueOf(encodeToString)));
                            return;
                        }
                        str = str + "    sha256/" + encodeToString + ": " + next.getSubjectDN().toString() + "\n";
                    }
                    throw new SSLPeerUnverifiedException("Certificate pinning failure!\n  Peer certificate chain:\n".concat(String.valueOf(str)));
                }
                throw new SSLPeerUnverifiedException("Empty trusted chain Certificate.");
            } catch (NoSuchAlgorithmException e) {
                da.a("SslPinningValidator", "Error in validating pinning: ", e);
            } catch (CertificateException e2) {
                da.a("SslPinningValidator", "Error in getting certificate: ", e2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0022 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static javax.net.ssl.X509TrustManager a() {
        /*
            java.lang.String r0 = "Error in getting trust manager: "
            java.lang.String r1 = "SslPinningValidator"
            r2 = 0
            java.lang.String r3 = javax.net.ssl.TrustManagerFactory.getDefaultAlgorithm()     // Catch:{ NoSuchAlgorithmException -> 0x001b, KeyStoreException -> 0x0015 }
            javax.net.ssl.TrustManagerFactory r3 = javax.net.ssl.TrustManagerFactory.getInstance(r3)     // Catch:{ NoSuchAlgorithmException -> 0x001b, KeyStoreException -> 0x0015 }
            r3.init(r2)     // Catch:{ NoSuchAlgorithmException -> 0x0013, KeyStoreException -> 0x0011 }
            goto L_0x0020
        L_0x0011:
            r4 = move-exception
            goto L_0x0017
        L_0x0013:
            r4 = move-exception
            goto L_0x001d
        L_0x0015:
            r4 = move-exception
            r3 = r2
        L_0x0017:
            com.flurry.sdk.da.a(r1, r0, r4)
            goto L_0x0020
        L_0x001b:
            r4 = move-exception
            r3 = r2
        L_0x001d:
            com.flurry.sdk.da.a(r1, r0, r4)
        L_0x0020:
            if (r3 != 0) goto L_0x0023
            return r2
        L_0x0023:
            javax.net.ssl.TrustManager[] r0 = r3.getTrustManagers()
            int r1 = r0.length
            r3 = 0
        L_0x0029:
            if (r3 >= r1) goto L_0x0038
            r4 = r0[r3]
            boolean r5 = r4 instanceof javax.net.ssl.X509TrustManager
            if (r5 == 0) goto L_0x0035
            r2 = r4
            javax.net.ssl.X509TrustManager r2 = (javax.net.ssl.X509TrustManager) r2
            goto L_0x0038
        L_0x0035:
            int r3 = r3 + 1
            goto L_0x0029
        L_0x0038:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.di.a():javax.net.ssl.X509TrustManager");
    }
}
