package com.flurry.sdk;

import android.text.TextUtils;

public final class bu extends bp {
    public bu() {
        super("Streaming", "FlurryStreamingWithFramesDataSender");
    }

    /* access modifiers changed from: protected */
    public final String d() {
        String b = d.b();
        if (TextUtils.isEmpty(b)) {
            return "https://data.flurry.com/v1/flr.do";
        }
        return b + "/v1/flr.do";
    }

    /* access modifiers changed from: protected */
    public final void a(int i, String str, String str2) {
        if (n.a().k.i.get()) {
            ea.a(i, str, str2, true, true);
            return;
        }
        ff.a("last_streaming_http_error_code", i);
        ff.a("last_streaming_http_error_message", str);
        ff.a("last_streaming_http_report_identifier", str2);
    }
}
