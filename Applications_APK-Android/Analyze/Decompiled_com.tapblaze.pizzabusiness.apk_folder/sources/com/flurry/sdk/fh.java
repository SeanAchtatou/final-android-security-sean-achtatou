package com.flurry.sdk;

import android.text.TextUtils;

public final class fh {
    public static boolean a(String... strArr) {
        for (int i = 0; i < 4; i++) {
            if (TextUtils.isEmpty(strArr[i])) {
                return false;
            }
        }
        return true;
    }

    public static boolean a(Object... objArr) {
        for (int i = 0; i < 2; i++) {
            if (objArr[i] == null) {
                return false;
            }
        }
        return true;
    }
}
