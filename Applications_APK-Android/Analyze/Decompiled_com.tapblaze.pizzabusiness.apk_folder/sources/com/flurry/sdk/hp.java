package com.flurry.sdk;

import android.app.Activity;
import android.os.Bundle;
import com.flurry.sdk.ao;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

final class hp implements o<ao> {
    hp() {
    }

    public final /* synthetic */ void a(Object obj) {
        Bundle bundle;
        ao aoVar = (ao) obj;
        if (ao.a.APP_ORIENTATION_CHANGE.equals(aoVar.a) && (bundle = aoVar.b) != null && bundle.containsKey("orientation_name")) {
            int i = bundle.getInt("orientation_name");
            id.a(i);
            da.a(5, "LifecycleObserver", aoVar.a.name() + " orientation: " + i);
        }
        if (ao.a.CREATED.equals(aoVar.a)) {
            WeakReference<Activity> weakReference = aoVar.c;
            if (weakReference != null) {
                a(weakReference.get());
                return;
            }
            da.a(3, "LifecycleObserver", "Activity reference null on " + aoVar.a.name());
        }
    }

    private static void a(Activity activity) {
        Bundle extras;
        if (activity == null) {
            da.a(3, "LifecycleObserver", "Activity null adding Launch Options");
        } else if ((activity instanceof Activity) && (extras = activity.getIntent().getExtras()) != null) {
            da.a(3, "LifecycleObserver", "Launch Options Bundle is present " + extras.toString());
            for (String next : extras.keySet()) {
                if (next != null) {
                    Object obj = extras.get(next);
                    String obj2 = obj != null ? obj.toString() : "null";
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(obj2);
                    if (next == null || next.isEmpty()) {
                        da.a(2, "LaunchOptionsFrame", "Launch option key is empty, do not send the frame.");
                    } else if (arrayList.size() == 0) {
                        da.a(2, "LaunchOptionsFrame", "Launch option values is empty, do not send the frame.");
                    } else {
                        fc.a().a(new it(new iu(next, arrayList)));
                    }
                    da.a(3, "LifecycleObserver", "Launch options Key: " + next + ". Its value: " + obj2);
                }
            }
        }
    }
}
