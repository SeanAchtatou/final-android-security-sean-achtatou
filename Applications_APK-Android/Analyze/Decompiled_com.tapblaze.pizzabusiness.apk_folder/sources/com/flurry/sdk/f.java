package com.flurry.sdk;

import java.util.concurrent.Future;

public class f extends i {
    private static final ThreadLocal<f> a = new ThreadLocal<>();
    private Thread b;

    public f(String str, h hVar) {
        super(str, hVar, false);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.Runnable r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.Thread r0 = r2.b     // Catch:{ all -> 0x0021 }
            java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0021 }
            if (r0 != r1) goto L_0x001c
            boolean r0 = r3 instanceof com.flurry.sdk.h.a     // Catch:{ all -> 0x0021 }
            if (r0 == 0) goto L_0x0017
            com.flurry.sdk.h r0 = r2.e     // Catch:{ all -> 0x0021 }
            if (r0 == 0) goto L_0x001a
            com.flurry.sdk.h r0 = r2.e     // Catch:{ all -> 0x0021 }
            r0.a(r3)     // Catch:{ all -> 0x0021 }
            goto L_0x001a
        L_0x0017:
            r3.run()     // Catch:{ all -> 0x0021 }
        L_0x001a:
            monitor-exit(r2)     // Catch:{ all -> 0x0021 }
            return
        L_0x001c:
            monitor-exit(r2)     // Catch:{ all -> 0x0021 }
            super.a(r3)
            return
        L_0x0021:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0021 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.f.a(java.lang.Runnable):void");
    }

    public final Future<Void> b(Runnable runnable) {
        return super.b(runnable);
    }

    /* access modifiers changed from: protected */
    public final boolean c(Runnable runnable) {
        f fVar;
        Thread thread;
        synchronized (this) {
            fVar = a.get();
            a.set(this);
            thread = this.b;
            this.b = Thread.currentThread();
        }
        try {
            e(runnable);
            synchronized (this) {
                this.b = thread;
                a.set(fVar);
            }
            return true;
        } catch (Throwable th) {
            synchronized (this) {
                this.b = thread;
                a.set(fVar);
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void d(Runnable runnable) {
        if (Thread.currentThread() == this.b) {
            runnable.run();
        }
    }
}
