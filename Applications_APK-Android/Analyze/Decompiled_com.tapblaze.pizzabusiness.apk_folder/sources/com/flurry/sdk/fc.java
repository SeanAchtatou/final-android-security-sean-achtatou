package com.flurry.sdk;

public final class fc {
    static fc a;
    public fj b = new fj();
    public ju c;
    public jq d;

    private fc() {
        jq jqVar;
        int a2 = d.a();
        if (a2 == 0 || a2 == 2) {
            jqVar = d.c() ? new fa() : new ez();
        } else {
            jqVar = null;
        }
        this.d = jqVar;
        this.c = new ju(this.d);
    }

    public static synchronized fc a() {
        fc fcVar;
        synchronized (fc.class) {
            if (a == null) {
                a = new fc();
            }
            fcVar = a;
        }
        return fcVar;
    }

    public final void a(jp jpVar) {
        if (jpVar != null) {
            this.b.a(jpVar);
        } else {
            da.a(5, "StreamingManager", "sendFrameToStreaming failed -- message is null");
        }
    }

    public static void b() {
        fc fcVar = a;
        if (fcVar != null) {
            ju juVar = fcVar.c;
            if (juVar != null) {
                juVar.a.stopWatching();
                juVar.b = null;
                fcVar.c = null;
            }
            fj fjVar = fcVar.b;
            if (fjVar != null) {
                fjVar.a.b();
                fjVar.a = null;
                fcVar.b = null;
            }
            a = null;
        }
    }
}
