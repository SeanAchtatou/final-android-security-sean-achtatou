package com.flurry.sdk;

import android.content.Context;
import java.io.File;
import java.util.List;
import java.util.Map;

public final class bw {
    boolean a;
    private final bx b;
    private final File c;
    private String d;

    public bw() {
        this(b.a());
    }

    public bw(Context context) {
        this.b = new bx();
        this.c = context.getFileStreamPath(".flurryinstallreceiver.");
        da.a(3, "InstallLogger", "Referrer file name if it exists:  " + this.c);
    }

    private void b(String str) {
        if (str != null) {
            this.d = str;
        }
    }

    public final synchronized void a(String str) {
        this.a = true;
        b(str);
        dz.a(this.c, this.d);
    }

    public final synchronized Map<String, List<String>> a() {
        if (!this.a) {
            this.a = true;
            da.a(4, "InstallLogger", "Loading referrer info from file: " + this.c.getAbsolutePath());
            String c2 = dz.c(this.c);
            da.a("InstallLogger", "Referrer file contents: ".concat(String.valueOf(c2)));
            b(c2);
        }
        return bx.a(this.d);
    }
}
