package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class gr extends jl {
    public final int a;

    public gr(int i) {
        this.a = i;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        int i = this.a;
        if (i != Integer.MIN_VALUE) {
            jSONObject.put("fl.demo.gender", i);
        }
        return jSONObject;
    }
}
