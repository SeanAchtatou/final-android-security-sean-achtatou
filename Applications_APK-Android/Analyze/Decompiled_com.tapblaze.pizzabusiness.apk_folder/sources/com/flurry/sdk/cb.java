package com.flurry.sdk;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Pair;
import com.flurry.android.FlurryConfigListener;
import com.flurry.sdk.bz;
import com.flurry.sdk.ce;
import com.flurry.sdk.ey;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class cb extends f {
    private static volatile cb b;
    private static final Object o = new Object();
    private static cn t;
    public cc a;
    private by h;
    private cg i;
    /* access modifiers changed from: private */
    public cm j;
    private cs k;
    private Handler l;
    private final Map<FlurryConfigListener, Pair<ci, WeakReference<Handler>>> m;
    /* access modifiers changed from: private */
    public final Map<ci, Pair<Boolean, Boolean>> n;
    private volatile boolean p;
    /* access modifiers changed from: private */
    public volatile boolean q;
    /* access modifiers changed from: private */
    public volatile boolean r;
    /* access modifiers changed from: private */
    public a s;

    enum a {
        Complete("Complete", 3),
        CompleteNoChange("No Change", 2),
        Fail("Fail", 1),
        None("None", 0);
        
        int e;
        private String f;

        private a(String str, int i) {
            this.f = str;
            this.e = i;
        }

        public final String toString() {
            return this.f;
        }
    }

    public static synchronized cb a() {
        cb f;
        synchronized (cb.class) {
            f = f();
        }
        return f;
    }

    private static synchronized cb f() {
        cb cbVar;
        synchronized (cb.class) {
            if (b == null) {
                b = new cb((byte) 0);
            }
            cbVar = b;
        }
        return cbVar;
    }

    private cb() {
        this((byte) 0);
    }

    private cb(byte b2) {
        super("ConfigManager", ey.a(ey.a.CONFIG));
        this.m = new ConcurrentHashMap();
        this.n = new HashMap();
        this.p = false;
        this.q = false;
        this.r = false;
        this.s = a.None;
        t = null;
        for (ci put : ci.a()) {
            Map<ci, Pair<Boolean, Boolean>> map = this.n;
            Boolean bool = Boolean.FALSE;
            map.put(put, new Pair(bool, bool));
        }
        this.i = new cg();
        this.j = new cm();
        this.a = new cc();
        this.k = new cs();
        this.l = new Handler(Looper.getMainLooper());
        b(new ec() {
            /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
                jadx.core.utils.exceptions.JadxOverflowException: 
                	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
                	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
                */
            public final void a() throws java.lang.Exception {
                /*
                    r8 = this;
                    java.lang.String r0 = "ConfigManager"
                    r1 = 1
                    android.content.Context r2 = com.flurry.sdk.b.a()     // Catch:{ Exception -> 0x00c2 }
                    java.lang.String r2 = com.flurry.sdk.ct.b(r2)     // Catch:{ Exception -> 0x00c2 }
                    java.lang.String r3 = "Cached Data: "
                    java.lang.String r4 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x00c2 }
                    java.lang.String r3 = r3.concat(r4)     // Catch:{ Exception -> 0x00c2 }
                    com.flurry.sdk.da.a(r0, r3)     // Catch:{ Exception -> 0x00c2 }
                    if (r2 == 0) goto L_0x007c
                    com.flurry.sdk.cb r3 = com.flurry.sdk.cb.this     // Catch:{ Exception -> 0x00c2 }
                    com.flurry.sdk.cc r3 = r3.a     // Catch:{ Exception -> 0x00c2 }
                    java.lang.String r3 = r3.d()     // Catch:{ Exception -> 0x00c2 }
                    com.flurry.sdk.cb r4 = com.flurry.sdk.cb.this     // Catch:{ Exception -> 0x00c2 }
                    com.flurry.sdk.cc r4 = r4.a     // Catch:{ Exception -> 0x00c2 }
                    android.content.SharedPreferences r5 = r4.a     // Catch:{ Exception -> 0x00c2 }
                    r6 = 0
                    if (r5 == 0) goto L_0x0037
                    android.content.SharedPreferences r4 = r4.a     // Catch:{ Exception -> 0x00c2 }
                    java.lang.String r5 = "lastRSA"
                    java.lang.String r6 = r4.getString(r5, r6)     // Catch:{ Exception -> 0x00c2 }
                L_0x0037:
                    boolean r3 = com.flurry.sdk.ct.a(r3, r2, r6)     // Catch:{ Exception -> 0x00c2 }
                    if (r3 == 0) goto L_0x0067
                    com.flurry.sdk.cb r3 = com.flurry.sdk.cb.this     // Catch:{ Exception -> 0x00c2 }
                    com.flurry.sdk.cm r3 = r3.j     // Catch:{ Exception -> 0x00c2 }
                    if (r2 == 0) goto L_0x005a
                    org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x0052 }
                    r4.<init>(r2)     // Catch:{ Exception -> 0x0052 }
                    java.util.List r2 = com.flurry.sdk.cd.a(r4)     // Catch:{ Exception -> 0x0052 }
                    r3.a(r2)     // Catch:{ Exception -> 0x0052 }
                    goto L_0x005a
                L_0x0052:
                    r2 = move-exception
                    java.lang.String r4 = "VariantsManager"
                    java.lang.String r5 = "Cached variants parsing error: "
                    com.flurry.sdk.da.a(r4, r5, r2)     // Catch:{ Exception -> 0x00c2 }
                L_0x005a:
                    com.flurry.sdk.cn r2 = com.flurry.sdk.cb.b()     // Catch:{ Exception -> 0x00c2 }
                    if (r2 == 0) goto L_0x007c
                    com.flurry.sdk.cb.b()     // Catch:{ Exception -> 0x00c2 }
                    com.flurry.sdk.cn.a(r3)     // Catch:{ Exception -> 0x00c2 }
                    goto L_0x007c
                L_0x0067:
                    java.lang.String r2 = "Incorrect signature for cache."
                    com.flurry.sdk.da.b(r0, r2)     // Catch:{ Exception -> 0x00c2 }
                    android.content.Context r2 = com.flurry.sdk.b.a()     // Catch:{ Exception -> 0x00c2 }
                    com.flurry.sdk.ct.c(r2)     // Catch:{ Exception -> 0x00c2 }
                    com.flurry.sdk.cb r2 = com.flurry.sdk.cb.this     // Catch:{ Exception -> 0x00c2 }
                    com.flurry.sdk.cc r2 = r2.a     // Catch:{ Exception -> 0x00c2 }
                    r2.c()     // Catch:{ Exception -> 0x00c2 }
                L_0x007c:
                    com.flurry.sdk.cb r0 = com.flurry.sdk.cb.this
                    com.flurry.sdk.cb.c(r0)
                    com.flurry.sdk.cb r0 = com.flurry.sdk.cb.this
                    com.flurry.sdk.cm r0 = r0.j
                    int r0 = r0.e()
                    if (r0 <= 0) goto L_0x010b
                    com.flurry.sdk.cb r0 = com.flurry.sdk.cb.this
                    com.flurry.sdk.cm r0 = r0.j
                    java.util.List r0 = r0.d()
                    java.util.Iterator r0 = r0.iterator()
                L_0x009b:
                    boolean r2 = r0.hasNext()
                    if (r2 == 0) goto L_0x00bf
                    java.lang.Object r2 = r0.next()
                    com.flurry.sdk.ci r2 = (com.flurry.sdk.ci) r2
                    com.flurry.sdk.cb r3 = com.flurry.sdk.cb.this
                    java.util.Map r3 = r3.n
                    android.util.Pair r4 = new android.util.Pair
                    java.lang.Boolean r5 = java.lang.Boolean.TRUE
                    java.lang.Boolean r6 = java.lang.Boolean.FALSE
                    r4.<init>(r5, r6)
                    r3.put(r2, r4)
                    com.flurry.sdk.cb r3 = com.flurry.sdk.cb.this
                    r3.a(r2, r1)
                    goto L_0x009b
                L_0x00bf:
                    return
                L_0x00c0:
                    r0 = move-exception
                    goto L_0x010c
                L_0x00c2:
                    r2 = move-exception
                    java.lang.String r3 = "Exception!"
                    com.flurry.sdk.da.a(r0, r3, r2)     // Catch:{ all -> 0x00c0 }
                    com.flurry.sdk.cb r0 = com.flurry.sdk.cb.this
                    com.flurry.sdk.cb.c(r0)
                    com.flurry.sdk.cb r0 = com.flurry.sdk.cb.this
                    com.flurry.sdk.cm r0 = r0.j
                    int r0 = r0.e()
                    if (r0 <= 0) goto L_0x010b
                    com.flurry.sdk.cb r0 = com.flurry.sdk.cb.this
                    com.flurry.sdk.cm r0 = r0.j
                    java.util.List r0 = r0.d()
                    java.util.Iterator r0 = r0.iterator()
                L_0x00e7:
                    boolean r2 = r0.hasNext()
                    if (r2 == 0) goto L_0x010b
                    java.lang.Object r2 = r0.next()
                    com.flurry.sdk.ci r2 = (com.flurry.sdk.ci) r2
                    com.flurry.sdk.cb r3 = com.flurry.sdk.cb.this
                    java.util.Map r3 = r3.n
                    android.util.Pair r4 = new android.util.Pair
                    java.lang.Boolean r5 = java.lang.Boolean.TRUE
                    java.lang.Boolean r6 = java.lang.Boolean.FALSE
                    r4.<init>(r5, r6)
                    r3.put(r2, r4)
                    com.flurry.sdk.cb r3 = com.flurry.sdk.cb.this
                    r3.a(r2, r1)
                    goto L_0x00e7
                L_0x010b:
                    return
                L_0x010c:
                    com.flurry.sdk.cb r2 = com.flurry.sdk.cb.this
                    com.flurry.sdk.cb.c(r2)
                    com.flurry.sdk.cb r2 = com.flurry.sdk.cb.this
                    com.flurry.sdk.cm r2 = r2.j
                    int r2 = r2.e()
                    if (r2 <= 0) goto L_0x014f
                    com.flurry.sdk.cb r2 = com.flurry.sdk.cb.this
                    com.flurry.sdk.cm r2 = r2.j
                    java.util.List r2 = r2.d()
                    java.util.Iterator r2 = r2.iterator()
                L_0x012b:
                    boolean r3 = r2.hasNext()
                    if (r3 == 0) goto L_0x014f
                    java.lang.Object r3 = r2.next()
                    com.flurry.sdk.ci r3 = (com.flurry.sdk.ci) r3
                    com.flurry.sdk.cb r4 = com.flurry.sdk.cb.this
                    java.util.Map r4 = r4.n
                    android.util.Pair r5 = new android.util.Pair
                    java.lang.Boolean r6 = java.lang.Boolean.TRUE
                    java.lang.Boolean r7 = java.lang.Boolean.FALSE
                    r5.<init>(r6, r7)
                    r4.put(r3, r5)
                    com.flurry.sdk.cb r4 = com.flurry.sdk.cb.this
                    r4.a(r3, r1)
                    goto L_0x012b
                L_0x014f:
                    goto L_0x0151
                L_0x0150:
                    throw r0
                L_0x0151:
                    goto L_0x0150
                */
                throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.cb.AnonymousClass1.a():void");
            }
        });
    }

    public static cn b() {
        return t;
    }

    public final by c() {
        if (this.h == null) {
            g();
            this.h = new by(this.i, this.j);
        }
        return this.h;
    }

    private void g() {
        synchronized (o) {
            while (!this.p) {
                try {
                    o.wait();
                } catch (InterruptedException e) {
                    da.a("ConfigManager", "Interrupted Exception!", e);
                }
            }
        }
    }

    public final void d() {
        if (this.q) {
            da.a(3, "ConfigManager", "Preventing re-entry...");
            return;
        }
        this.q = true;
        da.a(3, "ConfigManager", "Fetch started");
        for (bz a2 : cf.a(cs.a(b.a(), "https://cfg.flurry.com/sdk/v1/config"), new bz.a() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.flurry.sdk.cb.a(com.flurry.sdk.cb, boolean):boolean
             arg types: [com.flurry.sdk.cb, int]
             candidates:
              com.flurry.sdk.cb.a(com.flurry.sdk.cb, com.flurry.sdk.cb$a):com.flurry.sdk.cb$a
              com.flurry.sdk.cb.a(com.flurry.sdk.ci, boolean):void
              com.flurry.sdk.cb.a(com.flurry.sdk.cb, boolean):boolean */
            public final void a(ce ceVar, boolean z) {
                a aVar;
                if (!z) {
                    boolean unused = cb.this.q = false;
                }
                if (ceVar.d == ce.a.SUCCEED) {
                    da.a("ConfigManager", "Fetch succeeded.");
                    aVar = a.Complete;
                    boolean unused2 = cb.this.r = true;
                    for (ci next : ci.a()) {
                        boolean z2 = false;
                        if (cb.this.n.containsKey(next)) {
                            z2 = ((Boolean) ((Pair) cb.this.n.get(next)).first).booleanValue();
                        }
                        cb.this.n.put(next, new Pair(Boolean.valueOf(z2), Boolean.FALSE));
                    }
                } else if (ceVar.d == ce.a.NO_CHANGE) {
                    da.a("ConfigManager", "Fetch finished.");
                    aVar = a.CompleteNoChange;
                } else {
                    da.a("ConfigManager", "Error occured while fetching: ".concat(String.valueOf(ceVar)));
                    aVar = a.Fail;
                }
                if (cb.this.s.e <= aVar.e) {
                    a unused3 = cb.this.s = aVar;
                }
                cb.b(cb.this, aVar);
            }
        }, this.a, this.j)) {
            a2.a();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008c, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.flurry.android.FlurryConfigListener r5, com.flurry.sdk.ci r6, android.os.Handler r7) {
        /*
            r4 = this;
            if (r5 != 0) goto L_0x0003
            return
        L_0x0003:
            java.util.Map<com.flurry.android.FlurryConfigListener, android.util.Pair<com.flurry.sdk.ci, java.lang.ref.WeakReference<android.os.Handler>>> r0 = r4.m
            monitor-enter(r0)
            java.util.Map<com.flurry.android.FlurryConfigListener, android.util.Pair<com.flurry.sdk.ci, java.lang.ref.WeakReference<android.os.Handler>>> r1 = r4.m     // Catch:{ all -> 0x008d }
            boolean r1 = r1.containsKey(r5)     // Catch:{ all -> 0x008d }
            if (r1 == 0) goto L_0x0018
            r5 = 5
            java.lang.String r6 = "ConfigManager"
            java.lang.String r7 = "The listener is already registered"
            com.flurry.sdk.da.a(r5, r6, r7)     // Catch:{ all -> 0x008d }
            monitor-exit(r0)     // Catch:{ all -> 0x008d }
            return
        L_0x0018:
            java.util.Map<com.flurry.android.FlurryConfigListener, android.util.Pair<com.flurry.sdk.ci, java.lang.ref.WeakReference<android.os.Handler>>> r1 = r4.m     // Catch:{ all -> 0x008d }
            android.util.Pair r2 = new android.util.Pair     // Catch:{ all -> 0x008d }
            java.lang.ref.WeakReference r3 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x008d }
            r3.<init>(r7)     // Catch:{ all -> 0x008d }
            r2.<init>(r6, r3)     // Catch:{ all -> 0x008d }
            r1.put(r5, r2)     // Catch:{ all -> 0x008d }
            int[] r7 = com.flurry.sdk.cb.AnonymousClass6.a     // Catch:{ all -> 0x008d }
            com.flurry.sdk.cb$a r1 = r4.s     // Catch:{ all -> 0x008d }
            int r1 = r1.ordinal()     // Catch:{ all -> 0x008d }
            r7 = r7[r1]     // Catch:{ all -> 0x008d }
            r1 = 1
            if (r7 == r1) goto L_0x004b
            r2 = 2
            if (r7 == r2) goto L_0x0048
            r2 = 3
            if (r7 == r2) goto L_0x0044
            r2 = 4
            if (r7 == r2) goto L_0x003e
            goto L_0x004b
        L_0x003e:
            boolean r7 = r4.q     // Catch:{ all -> 0x008d }
            r5.onFetchError(r7)     // Catch:{ all -> 0x008d }
            goto L_0x004b
        L_0x0044:
            r5.onFetchNoChange()     // Catch:{ all -> 0x008d }
            goto L_0x004b
        L_0x0048:
            r5.onFetchSuccess()     // Catch:{ all -> 0x008d }
        L_0x004b:
            java.util.Map<com.flurry.sdk.ci, android.util.Pair<java.lang.Boolean, java.lang.Boolean>> r7 = r4.n     // Catch:{ all -> 0x008d }
            boolean r7 = r7.containsKey(r6)     // Catch:{ all -> 0x008d }
            if (r7 == 0) goto L_0x007f
            java.util.Map<com.flurry.sdk.ci, android.util.Pair<java.lang.Boolean, java.lang.Boolean>> r7 = r4.n     // Catch:{ all -> 0x008d }
            java.lang.Object r6 = r7.get(r6)     // Catch:{ all -> 0x008d }
            android.util.Pair r6 = (android.util.Pair) r6     // Catch:{ all -> 0x008d }
            java.lang.Object r7 = r6.first     // Catch:{ all -> 0x008d }
            java.lang.Boolean r7 = (java.lang.Boolean) r7     // Catch:{ all -> 0x008d }
            boolean r7 = r7.booleanValue()     // Catch:{ all -> 0x008d }
            if (r7 != 0) goto L_0x006f
            java.lang.Object r7 = r6.second     // Catch:{ all -> 0x008d }
            java.lang.Boolean r7 = (java.lang.Boolean) r7     // Catch:{ all -> 0x008d }
            boolean r7 = r7.booleanValue()     // Catch:{ all -> 0x008d }
            if (r7 == 0) goto L_0x008b
        L_0x006f:
            java.lang.Object r6 = r6.second     // Catch:{ all -> 0x008d }
            java.lang.Boolean r6 = (java.lang.Boolean) r6     // Catch:{ all -> 0x008d }
            boolean r6 = r6.booleanValue()     // Catch:{ all -> 0x008d }
            if (r6 != 0) goto L_0x007a
            goto L_0x007b
        L_0x007a:
            r1 = 0
        L_0x007b:
            r5.onActivateComplete(r1)     // Catch:{ all -> 0x008d }
            goto L_0x008b
        L_0x007f:
            java.util.Map<com.flurry.sdk.ci, android.util.Pair<java.lang.Boolean, java.lang.Boolean>> r5 = r4.n     // Catch:{ all -> 0x008d }
            android.util.Pair r7 = new android.util.Pair     // Catch:{ all -> 0x008d }
            java.lang.Boolean r1 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x008d }
            r7.<init>(r1, r1)     // Catch:{ all -> 0x008d }
            r5.put(r6, r7)     // Catch:{ all -> 0x008d }
        L_0x008b:
            monitor-exit(r0)     // Catch:{ all -> 0x008d }
            return
        L_0x008d:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x008d }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.cb.a(com.flurry.android.FlurryConfigListener, com.flurry.sdk.ci, android.os.Handler):void");
    }

    /* renamed from: com.flurry.sdk.cb$6  reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {
        static final /* synthetic */ int[] a = new int[a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.flurry.sdk.cb$a[] r0 = com.flurry.sdk.cb.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.flurry.sdk.cb.AnonymousClass6.a = r0
                int[] r0 = com.flurry.sdk.cb.AnonymousClass6.a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.flurry.sdk.cb$a r1 = com.flurry.sdk.cb.a.None     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.flurry.sdk.cb.AnonymousClass6.a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.flurry.sdk.cb$a r1 = com.flurry.sdk.cb.a.Complete     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.flurry.sdk.cb.AnonymousClass6.a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.flurry.sdk.cb$a r1 = com.flurry.sdk.cb.a.CompleteNoChange     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.flurry.sdk.cb.AnonymousClass6.a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.flurry.sdk.cb$a r1 = com.flurry.sdk.cb.a.Fail     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.cb.AnonymousClass6.<clinit>():void");
        }
    }

    public final void a(FlurryConfigListener flurryConfigListener) {
        if (flurryConfigListener != null) {
            synchronized (this.m) {
                this.m.remove(flurryConfigListener);
            }
        }
    }

    public final void a(ci ciVar, final boolean z) {
        synchronized (this.m) {
            for (Map.Entry next : this.m.entrySet()) {
                if (ciVar == null || ciVar == ((Pair) next.getValue()).first) {
                    final FlurryConfigListener flurryConfigListener = (FlurryConfigListener) next.getKey();
                    Handler handler = (Handler) ((WeakReference) ((Pair) next.getValue()).second).get();
                    AnonymousClass4 r4 = new ec() {
                        public final void a() {
                            flurryConfigListener.onActivateComplete(z);
                        }
                    };
                    if (handler == null) {
                        this.l.post(r4);
                    } else {
                        handler.post(r4);
                    }
                }
            }
        }
    }

    public final List<cl> e() {
        cm cmVar = this.j;
        if (cmVar != null) {
            return cmVar.b();
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cb.a(com.flurry.sdk.ci, boolean):void
     arg types: [com.flurry.sdk.ci, int]
     candidates:
      com.flurry.sdk.cb.a(com.flurry.sdk.cb, com.flurry.sdk.cb$a):com.flurry.sdk.cb$a
      com.flurry.sdk.cb.a(com.flurry.sdk.cb, boolean):boolean
      com.flurry.sdk.cb.a(com.flurry.sdk.ci, boolean):void */
    public final boolean a(ci ciVar) {
        boolean z;
        if (!this.r) {
            return false;
        }
        boolean z2 = true;
        if (ciVar == null) {
            boolean z3 = false;
            for (Map.Entry next : this.n.entrySet()) {
                Pair pair = (Pair) next.getValue();
                if (!((Boolean) pair.second).booleanValue()) {
                    next.setValue(new Pair(pair.first, Boolean.TRUE));
                    z3 = true;
                }
            }
            z2 = z3;
        } else {
            Pair pair2 = this.n.get(ciVar);
            if (pair2 == null || !((Boolean) pair2.second).booleanValue()) {
                Map<ci, Pair<Boolean, Boolean>> map = this.n;
                if (pair2 == null) {
                    z = false;
                } else {
                    z = ((Boolean) pair2.first).booleanValue();
                }
                map.put(ciVar, new Pair(Boolean.valueOf(z), Boolean.TRUE));
            } else {
                z2 = false;
            }
        }
        if (!z2) {
            return z2;
        }
        this.j.a(ciVar);
        a(ciVar, false);
        return z2;
    }

    public final String toString() {
        g();
        ArrayList arrayList = new ArrayList();
        List<cl> e = e();
        if (e == null || e.isEmpty()) {
            return "No variants were found!";
        }
        for (cl clVar : e) {
            arrayList.add(clVar.toString());
        }
        return TextUtils.join(",", arrayList);
    }

    static /* synthetic */ void c(cb cbVar) {
        synchronized (o) {
            cbVar.p = true;
            o.notifyAll();
        }
    }

    static /* synthetic */ void b(cb cbVar, final a aVar) {
        synchronized (cbVar.m) {
            for (Map.Entry next : cbVar.m.entrySet()) {
                final FlurryConfigListener flurryConfigListener = (FlurryConfigListener) next.getKey();
                Handler handler = (Handler) ((WeakReference) ((Pair) next.getValue()).second).get();
                AnonymousClass3 r4 = new ec() {
                    public final void a() {
                        int i = AnonymousClass6.a[aVar.ordinal()];
                        if (i == 2) {
                            flurryConfigListener.onFetchSuccess();
                        } else if (i == 3) {
                            flurryConfigListener.onFetchNoChange();
                        } else if (i == 4) {
                            flurryConfigListener.onFetchError(cb.this.q);
                        }
                    }
                };
                if (handler == null) {
                    cbVar.l.post(r4);
                } else {
                    handler.post(r4);
                }
            }
        }
    }
}
