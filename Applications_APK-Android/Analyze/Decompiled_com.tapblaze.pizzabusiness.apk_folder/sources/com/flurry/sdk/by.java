package com.flurry.sdk;

public final class by {
    public final cg a;
    public cm b;

    public by(cg cgVar, cm cmVar) {
        this.a = cgVar;
        this.b = cmVar;
    }

    public final String a(String str, String str2, ci ciVar) {
        ca a2 = this.b.a(str, ciVar);
        if (a2 == null) {
            a2 = this.a.a(str);
        }
        return a2 != null ? a2.a() : str2;
    }

    public final int a(String str, int i, ci ciVar) {
        ca a2 = this.b.a(str, ciVar);
        if (a2 == null) {
            a2 = this.a.a(str);
        }
        if (a2 != null) {
            try {
                return Integer.decode(a2.a()).intValue();
            } catch (NumberFormatException unused) {
            }
        }
        return i;
    }

    public final float a(String str, float f, ci ciVar) {
        ca a2 = this.b.a(str, ciVar);
        if (a2 == null) {
            a2 = this.a.a(str);
        }
        if (a2 != null) {
            try {
                return Float.parseFloat(a2.a());
            } catch (NumberFormatException unused) {
            }
        }
        return f;
    }

    public final long a(String str, long j, ci ciVar) {
        ca a2 = this.b.a(str, ciVar);
        if (a2 == null) {
            a2 = this.a.a(str);
        }
        if (a2 != null) {
            try {
                return Long.decode(a2.a()).longValue();
            } catch (NumberFormatException unused) {
            }
        }
        return j;
    }

    public final double a(String str, double d, ci ciVar) {
        ca a2 = this.b.a(str, ciVar);
        if (a2 == null) {
            a2 = this.a.a(str);
        }
        if (a2 != null) {
            try {
                return Double.parseDouble(a2.a());
            } catch (NumberFormatException unused) {
            }
        }
        return d;
    }
}
