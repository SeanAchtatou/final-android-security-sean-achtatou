package com.flurry.sdk;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class ci {
    public static final ci a = new ci("APP");
    public static final ci b = new ci("KILLSWITCH");
    private static final Map<String, ci> c = new HashMap();
    private String d;

    private ci(String str) {
        this.d = str;
        c.put(str, this);
    }

    public static ci a(String str) {
        if (c.containsKey(str)) {
            return c.get(str);
        }
        return new ci(str);
    }

    public static Collection<ci> a() {
        return c.values();
    }

    public final String toString() {
        return this.d;
    }
}
