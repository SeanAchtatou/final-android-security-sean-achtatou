package com.flurry.sdk;

public final class ch {
    private static ch b;
    public cb a = cb.a();

    public static synchronized ch a() {
        ch chVar;
        synchronized (ch.class) {
            if (b == null) {
                if (a.i()) {
                    b = new ch();
                } else {
                    throw new IllegalStateException("Flurry SDK must be initialized before starting config");
                }
            }
            chVar = b;
        }
        return chVar;
    }

    private ch() {
    }

    public final String a(String str, String str2, ci ciVar) {
        return this.a.c().a(str, str2, ciVar);
    }
}
