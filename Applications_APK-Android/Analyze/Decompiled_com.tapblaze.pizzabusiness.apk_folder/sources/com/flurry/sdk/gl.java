package com.flurry.sdk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class gl extends jl {
    public final int a;
    public final String b;
    public final long c;
    public final String d;
    public final String e;
    public final String f;
    public final int g;
    public final int h;
    public final String i;
    public final String j;
    public final Map<String, String> k;
    public final Map<String, String> l;
    public int m;
    public List<v> n;

    public gl(int i2, String str, long j2, String str2, String str3, String str4, int i3, int i4, Map<String, String> map, Map<String, String> map2, int i5, List<v> list, String str5, String str6) {
        this.a = i2;
        this.b = str;
        this.c = j2;
        String str7 = "";
        this.d = str2 == null ? str7 : str2;
        this.e = str3 == null ? str7 : str3;
        this.f = str4 == null ? str7 : str4;
        this.g = i3;
        this.h = i4;
        this.k = map == null ? new HashMap<>() : map;
        this.l = map2 == null ? new HashMap<>() : map2;
        this.m = i5;
        this.n = list == null ? new ArrayList<>() : list;
        this.i = str5 != null ? ea.b(str5) : str7;
        this.j = str6 != null ? str6 : str7;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.error.id", this.a);
        jSONObject.put("fl.error.name", this.b);
        jSONObject.put("fl.error.timestamp", this.c);
        jSONObject.put("fl.error.message", this.d);
        jSONObject.put("fl.error.class", this.e);
        jSONObject.put("fl.error.type", this.g);
        jSONObject.put("fl.crash.report", this.f);
        jSONObject.put("fl.crash.platform", this.h);
        jSONObject.put("fl.error.user.crash.parameter", eb.a(this.l));
        jSONObject.put("fl.error.sdk.crash.parameter", eb.a(this.k));
        jSONObject.put("fl.breadcrumb.version", this.m);
        JSONArray jSONArray = new JSONArray();
        List<v> list = this.n;
        if (list != null) {
            for (v next : list) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("fl.breadcrumb.message", next.a);
                jSONObject2.put("fl.breadcrumb.timestamp", next.b);
                jSONArray.put(jSONObject2);
            }
        }
        jSONObject.put("fl.breadcrumb", jSONArray);
        jSONObject.put("fl.nativecrash.minidump", this.i);
        jSONObject.put("fl.nativecrash.logcat", this.j);
        return jSONObject;
    }
}
