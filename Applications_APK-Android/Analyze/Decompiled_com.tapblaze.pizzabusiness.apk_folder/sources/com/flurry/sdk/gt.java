package com.flurry.sdk;

import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

public final class gt extends jl {
    public final String a;
    public final String b;

    public gt(String str, String str2) {
        this.a = str == null ? "" : str;
        this.b = str2 == null ? "" : str2;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (!TextUtils.isEmpty(this.a)) {
            jSONObject.put("fl.language", this.a);
        }
        if (!TextUtils.isEmpty(this.b)) {
            jSONObject.put("fl.country", this.b);
        }
        return jSONObject;
    }
}
