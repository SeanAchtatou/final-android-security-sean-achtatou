package com.flurry.sdk;

public final class bl {
    private static bl a;

    private bl() {
    }

    public static synchronized bl a() {
        bl blVar;
        synchronized (bl.class) {
            if (a == null) {
                a = new bl();
            }
            blVar = a;
        }
        return blVar;
    }
}
