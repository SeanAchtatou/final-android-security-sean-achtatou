package com.flurry.sdk;

import android.content.Context;
import java.util.concurrent.atomic.AtomicBoolean;

public final class fe {
    private static AtomicBoolean a;
    private static AtomicBoolean b;
    private static AtomicBoolean c;

    public static boolean a() {
        AtomicBoolean atomicBoolean = a;
        if (atomicBoolean != null) {
            return atomicBoolean.get();
        }
        AtomicBoolean atomicBoolean2 = new AtomicBoolean(a("android.permission.ACCESS_FINE_LOCATION"));
        a = atomicBoolean2;
        return atomicBoolean2.get();
    }

    public static boolean b() {
        AtomicBoolean atomicBoolean = b;
        if (atomicBoolean != null) {
            return atomicBoolean.get();
        }
        AtomicBoolean atomicBoolean2 = new AtomicBoolean(a("android.permission.ACCESS_COARSE_LOCATION"));
        b = atomicBoolean2;
        return atomicBoolean2.get();
    }

    public static boolean c() {
        AtomicBoolean atomicBoolean = c;
        if (atomicBoolean != null) {
            return atomicBoolean.get();
        }
        AtomicBoolean atomicBoolean2 = new AtomicBoolean(a("android.permission.ACCESS_NETWORK_STATE"));
        c = atomicBoolean2;
        return atomicBoolean2.get();
    }

    private static boolean a(String str) {
        Context a2 = b.a();
        if (a2 == null) {
            da.a(6, "PermissionUtil", "Context is null when checking permission.");
            return false;
        } else if (a2.checkCallingOrSelfPermission(str) == 0) {
            return true;
        } else {
            return false;
        }
    }
}
