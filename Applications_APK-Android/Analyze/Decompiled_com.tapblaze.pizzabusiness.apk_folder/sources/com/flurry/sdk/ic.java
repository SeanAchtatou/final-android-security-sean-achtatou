package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class ic extends jl {
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final int e;

    public ic(String str, String str2, String str3, String str4) {
        this.a = str;
        this.b = str2 == null ? "" : str2;
        this.c = str3;
        this.d = str4;
        this.e = 3;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        a(jSONObject, "fl.app.version", this.a);
        a(jSONObject, "fl.app.version.override", this.b);
        a(jSONObject, "fl.app.version.code", this.c);
        a(jSONObject, "fl.bundle.id", this.d);
        jSONObject.put("fl.build.environment", this.e);
        return jSONObject;
    }

    private static void a(JSONObject jSONObject, String str, String str2) throws JSONException {
        if (str2 != null) {
            jSONObject.put(str, str2);
        }
    }
}
