package com.flurry.sdk;

public final class ce {
    public static final ce a = new ce(a.SUCCEED, null);
    public static final ce b = new ce(a.NO_CHANGE, null);
    String c;
    a d;

    public ce(a aVar, String str) {
        this.d = aVar;
        this.c = str;
    }

    public enum a {
        SUCCEED(1),
        NO_CHANGE(0),
        IO(-1),
        NOT_VALID_JSON(-2),
        AUTHENTICATE(-3),
        UNKNOWN_CERTIFICATE(-4),
        OTHER(-5);
        
        int h;

        private a(int i2) {
            this.h = i2;
        }
    }

    public final String toString() {
        return "[Error:" + this.d.name() + "] " + this.c;
    }
}
