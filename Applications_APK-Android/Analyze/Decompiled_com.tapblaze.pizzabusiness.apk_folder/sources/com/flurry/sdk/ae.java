package com.flurry.sdk;

import android.content.SharedPreferences;
import com.flurry.sdk.ey;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ae extends f {
    String a = "";
    boolean b = false;
    AtomicBoolean h = new AtomicBoolean(false);

    ae() {
        super("AdvertisingIdProvider", ey.a(ey.a.PROVIDER));
    }

    public final void a() {
        AdvertisingIdClient.Info b2 = b();
        if (b2 != null) {
            this.a = b2.getId();
            this.b = !b2.isLimitAdTrackingEnabled();
            this.h.set(true);
            if (b2 != null) {
                ff.a("advertising_id", b2.getId());
                boolean isLimitAdTrackingEnabled = b2.isLimitAdTrackingEnabled();
                SharedPreferences.Editor edit = b.a().getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0).edit();
                edit.putBoolean(String.format(Locale.US, "com.flurry.sdk.%s", "ad_tracking_enabled"), isLimitAdTrackingEnabled);
                edit.apply();
            }
        }
    }

    private static AdvertisingIdClient.Info b() {
        try {
            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(b.a());
            return new AdvertisingIdClient.Info(advertisingIdInfo.getId(), advertisingIdInfo.isLimitAdTrackingEnabled());
        } catch (NoClassDefFoundError unused) {
            da.b("AdvertisingIdProvider", "There is a problem with the Google Play Services library, which is required for Android Advertising ID support. The Google Play Services library should be integrated in any app shipping in the Play Store that uses analytics or advertising.");
            return null;
        } catch (Exception e) {
            da.b("AdvertisingIdProvider", "GOOGLE PLAY SERVICES ERROR: " + e.getMessage());
            da.b("AdvertisingIdProvider", "There is a problem with the Google Play Services library, which is required for Android Advertising ID support. The Google Play Services library should be integrated in any app shipping in the Play Store that uses analytics or advertising.");
            return null;
        }
    }
}
