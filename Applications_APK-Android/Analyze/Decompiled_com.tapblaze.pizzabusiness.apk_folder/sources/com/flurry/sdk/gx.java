package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class gx extends jl {
    public final int a;

    public gx(int i) {
        this.a = i;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.error.count", this.a);
        return jSONObject;
    }
}
