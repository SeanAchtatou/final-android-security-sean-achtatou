package com.flurry.sdk;

import android.location.Location;
import android.location.LocationManager;

public final class at extends m<as> {
    public boolean b = true;
    protected o<r> h = new o<r>() {
        public final /* bridge */ /* synthetic */ void a(Object obj) {
            if (((r) obj).b == p.FOREGROUND) {
                at.this.a();
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public Location j;
    private q k;

    public at(q qVar) {
        super("LocationProvider");
        this.k = qVar;
        this.k.a(this.h);
    }

    public final void a() {
        Location d = d();
        if (d != null) {
            this.j = d;
        }
        a(new as(this.b, this.i, this.j));
    }

    /* access modifiers changed from: private */
    public Location d() {
        if (!this.b) {
            return null;
        }
        if (fe.a() || fe.b()) {
            String str = fe.a() ? "passive" : "network";
            this.i = true;
            LocationManager locationManager = (LocationManager) b.a().getSystemService("location");
            if (locationManager != null) {
                return locationManager.getLastKnownLocation(str);
            }
            return null;
        }
        this.i = false;
        return null;
    }

    public final void a(final o<as> oVar) {
        super.a((o) oVar);
        b(new ec() {
            public final void a() {
                Location a2 = at.this.d();
                if (a2 != null) {
                    Location unused = at.this.j = a2;
                }
                oVar.a(new as(at.this.b, at.this.i, at.this.j));
            }
        });
    }

    public final void c() {
        super.c();
        this.k.b(this.h);
    }
}
