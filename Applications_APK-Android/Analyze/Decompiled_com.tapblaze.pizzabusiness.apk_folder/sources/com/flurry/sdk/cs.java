package com.flurry.sdk;

import android.content.Context;

public final class cs {
    public static cr a(Context context, String str) {
        if (str.startsWith("http://") || str.startsWith("https://")) {
            return new co(str);
        }
        return new cp(context, str);
    }
}
