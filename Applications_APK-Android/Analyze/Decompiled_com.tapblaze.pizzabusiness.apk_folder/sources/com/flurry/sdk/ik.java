package com.flurry.sdk;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class ik extends jl {
    public boolean a;
    public Map<String, String> b;

    public ik(boolean z, Map<String, String> map) {
        this.a = z;
        this.b = map;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.consent.isGdprScope", this.a);
        JSONObject jSONObject2 = new JSONObject();
        for (Map.Entry next : this.b.entrySet()) {
            jSONObject2.put((String) next.getKey(), next.getValue());
        }
        jSONObject.put("fl.consent.strings", jSONObject2);
        return jSONObject;
    }
}
