package com.flurry.sdk;

import com.flurry.sdk.ey;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.PriorityQueue;
import okhttp3.internal.http.StatusLine;

public final class ez extends f implements jq {
    /* access modifiers changed from: private */
    public PriorityQueue<String> a;
    private bp b;
    private bp h;

    public ez() {
        super("FrameLogDataSender", ey.a(ey.a.CORE));
        this.a = null;
        this.a = new PriorityQueue<>(4, new fi());
        this.b = new bu();
        this.h = new bt();
    }

    public final void a() {
        this.b.a();
        this.h.a();
    }

    public final void a(final List<String> list) {
        if (list.size() == 0) {
            da.a(6, "FrameLogDataSender", "File List is null or empty");
            return;
        }
        da.c("FrameLogDataSender", "Number of files being added:" + list.toString());
        b(new ec() {
            public final void a() throws Exception {
                ez.this.a.addAll(list);
                ez.this.b();
            }
        });
    }

    private static byte[] a(File file) throws IOException {
        int length = (int) file.length();
        byte[] bArr = new byte[length];
        byte[] bArr2 = new byte[length];
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            int read = fileInputStream.read(bArr, 0, length);
            if (read < length) {
                int i = length - read;
                while (i > 0) {
                    int read2 = fileInputStream.read(bArr2, 0, i);
                    System.arraycopy(bArr2, 0, bArr, length - i, read2);
                    i -= read2;
                }
            }
        } catch (IOException e) {
            da.a(6, "FrameLogDataSender", "Error reading file. ".concat(String.valueOf(e)));
        } catch (Throwable th) {
            fileInputStream.close();
            throw th;
        }
        fileInputStream.close();
        return bArr;
    }

    /* access modifiers changed from: private */
    public void b() {
        da.c("FrameLogDataSender", " Starting processNextFile " + this.a.size());
        if (this.a.peek() == null) {
            da.c("FrameLogDataSender", "No file present to process.");
            return;
        }
        String poll = this.a.poll();
        if (fg.b(poll)) {
            da.c("FrameLogDataSender", "Starting to upload file: ".concat(String.valueOf(poll)));
            byte[] bArr = new byte[0];
            try {
                bArr = a(new File(poll));
            } catch (IOException e) {
                da.a(6, "FrameLogDataSender", "Error in getting bytes form the file: " + e.getMessage());
            }
            String b2 = bk.a().b();
            StringBuilder sb = new StringBuilder();
            bn.a();
            sb.append((int) StatusLine.HTTP_PERM_REDIRECT);
            this.b.a(bArr, b2, sb.toString());
            this.b.a(new bo() {
                public final void a() {
                    fc.a().a(new jb(new jc(true)));
                }

                public final void b() {
                    fc.a().a(new jb(new jc(false)));
                }
            });
            a(poll);
            da.c("FrameLogDataSender", "File appended for upload: ".concat(String.valueOf(poll)));
            return;
        }
        da.a(6, "FrameLogDataSender", "Something wrong with the file. File does not exist.");
    }

    private synchronized void a(String str) {
        da.c("FrameLogDataSender", "File upload status: ".concat(String.valueOf(str)));
        boolean a2 = fg.a(str);
        da.a(2, "FrameLogDataSender", "Deleting file " + str + " deleted " + a2);
        b();
    }
}
