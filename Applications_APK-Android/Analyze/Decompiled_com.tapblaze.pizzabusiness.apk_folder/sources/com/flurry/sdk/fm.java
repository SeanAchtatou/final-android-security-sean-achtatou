package com.flurry.sdk;

import com.flurry.sdk.fn;
import com.flurry.sdk.fs;
import com.flurry.sdk.gd;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public final class fm extends fs {
    protected gd a;
    protected js b;

    fm() {
        super("FileWriterModule", null);
        this.a = null;
        this.b = null;
        this.a = new gb();
        this.b = new js();
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        if (this.a.b()) {
            da.a(6, "FileWriterModule", "File was open, closing now.");
            this.a.a();
        }
        return this.a.a(fg.d(), str);
    }

    public final void a(final jp jpVar) {
        b(new ec() {
            public final void a() {
                if (fm.this.h == fs.b.c) {
                    fm.this.i.add(jpVar);
                    da.a(4, "FileWriterModule", "In paused state, cannot process message now. " + jpVar.a());
                    return;
                }
                if (!fm.this.a.b()) {
                    if (fm.this.a("currentFile")) {
                        da.a(4, "FileWriterModule", "File created. Adding counter");
                        fm.this.a.a(ir.b(), (gd.a) null);
                    } else {
                        da.a(4, "FileWriterModule", "File creation failed.");
                        n.a().p.a("FileWriterModuleError: fail to create new frame log file");
                    }
                }
                if (jpVar.a().equals(jn.FLUSH_FRAME)) {
                    fm.this.h = fs.b.c;
                    da.a(4, "FileWriterModule", "Adding flush frame:" + jpVar.e());
                    fm.this.a.a(jpVar, new gd.a() {
                        public final void a() {
                            fm.this.h = fs.b.c;
                            fm.this.a.a();
                            fm.this.e();
                            fm.this.d();
                            fm.this.h = fs.b.d;
                        }
                    });
                    return;
                }
                da.a(4, "FileWriterModule", "Adding frame " + jpVar.a() + ": " + jpVar.e());
                fm.this.a.a(jpVar, (gd.a) null);
            }
        });
    }

    public final fn.a b(jp jpVar) {
        gb gbVar = new gb();
        if (gbVar.a(fg.d(), "crashFile")) {
            gbVar.a(jpVar);
            gbVar.a();
        } else {
            da.a(4, "FileWriterModule", "Can't create crash file. Cannot write crash frame to disc");
        }
        return fn.a.QUEUED;
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.a.b()) {
            this.a.a();
        }
        boolean a2 = js.a(new jr(fg.d(), "currentFile"), new jr(fg.b(), fg.c()));
        da.a(4, "FileWriterModule", "File moved status: " + a2 + " InProgress to Completed.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.flurry.sdk.gl f() {
        /*
            java.lang.String r0 = "FileWriterModule"
            r1 = 4
            java.lang.String r2 = "Start getting native crash entity."
            com.flurry.sdk.da.a(r1, r0, r2)
            android.content.Context r2 = com.flurry.sdk.b.a()
            java.lang.String r3 = ".yflurrynativecrash"
            java.io.File r2 = r2.getFileStreamPath(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = ".*"
            r3.<init>(r4)
            java.lang.String r5 = ".dmp"
            java.lang.String r5 = java.util.regex.Pattern.quote(r5)
            r3.append(r5)
            java.lang.String r5 = "$"
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            java.util.regex.Pattern r3 = java.util.regex.Pattern.compile(r3)
            boolean r5 = r2.exists()
            r6 = 0
            if (r5 != 0) goto L_0x0039
            java.lang.String[] r3 = new java.lang.String[r6]
            goto L_0x0046
        L_0x0039:
            com.flurry.sdk.fg$1 r5 = new com.flurry.sdk.fg$1
            r5.<init>(r3)
            java.lang.String[] r3 = r2.list(r5)
            if (r3 != 0) goto L_0x0046
            java.lang.String[] r3 = new java.lang.String[r6]
        L_0x0046:
            r5 = 0
            if (r3 == 0) goto L_0x01bb
            int r7 = r3.length
            if (r7 != 0) goto L_0x004e
            goto L_0x01bb
        L_0x004e:
            int r7 = r3.length
            r9 = r5
            r8 = 0
        L_0x0051:
            if (r8 >= r7) goto L_0x01b5
            r10 = r3[r8]
            java.lang.String r11 = java.lang.String.valueOf(r10)
            java.lang.String r12 = "Native crash occurred in previous session! Found minidump file - "
            java.lang.String r11 = r12.concat(r11)
            com.flurry.sdk.da.c(r0, r11)
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            r11.append(r10)
            java.lang.String r12 = ".fcb"
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>(r4)
            java.lang.String r11 = java.util.regex.Pattern.quote(r11)
            r12.append(r11)
            r12.append(r4)
            java.lang.String r11 = r12.toString()
            java.util.regex.Pattern r11 = java.util.regex.Pattern.compile(r11)
            java.lang.String[] r11 = com.flurry.sdk.dz.a(r2, r11)
            int r12 = r11.length
            if (r12 <= 0) goto L_0x0094
            r11 = r11[r6]
            goto L_0x0095
        L_0x0094:
            r11 = r5
        L_0x0095:
            boolean r12 = android.text.TextUtils.isEmpty(r11)
            if (r12 == 0) goto L_0x00a2
            java.lang.String r12 = "There was no breadcrumbs file associated with the minidump file."
            com.flurry.sdk.da.a(r1, r0, r12)
            r12 = 1
            goto L_0x00a3
        L_0x00a2:
            r12 = 0
        L_0x00a3:
            java.lang.String r14 = java.lang.String.valueOf(r11)
            java.lang.String r15 = "Breadcrumbs file associated with minidump file - "
            java.lang.String r14 = r15.concat(r14)
            com.flurry.sdk.da.a(r1, r0, r14)
            boolean r14 = android.text.TextUtils.isEmpty(r11)
            r15 = 5
            java.lang.String r6 = "\\."
            if (r14 == 0) goto L_0x00bb
        L_0x00b9:
            r13 = r5
            goto L_0x00c6
        L_0x00bb:
            java.lang.String[] r14 = r11.split(r6)
            int r13 = r14.length
            if (r13 == r15) goto L_0x00c3
            goto L_0x00b9
        L_0x00c3:
            r13 = 3
            r13 = r14[r13]
        L_0x00c6:
            boolean r14 = android.text.TextUtils.isEmpty(r11)
            if (r14 == 0) goto L_0x00ce
        L_0x00cc:
            r6 = r5
            goto L_0x00d8
        L_0x00ce:
            java.lang.String[] r6 = r11.split(r6)
            int r14 = r6.length
            if (r14 == r15) goto L_0x00d6
            goto L_0x00cc
        L_0x00d6:
            r6 = r6[r1]
        L_0x00d8:
            boolean r14 = android.text.TextUtils.isEmpty(r13)
            if (r14 == 0) goto L_0x00ec
            java.lang.String r12 = java.lang.String.valueOf(r11)
            java.lang.String r14 = "There is no session id specified with crash breadcrumbs file: "
            java.lang.String r12 = r14.concat(r12)
            com.flurry.sdk.da.a(r1, r0, r12)
            r12 = 1
        L_0x00ec:
            long r14 = java.lang.System.currentTimeMillis()
            java.lang.Long.parseLong(r13)     // Catch:{ NumberFormatException -> 0x00fb }
            long r14 = java.lang.Long.parseLong(r6)     // Catch:{ NumberFormatException -> 0x00fb }
            r13 = r12
            r20 = r14
            goto L_0x010b
        L_0x00fb:
            java.lang.String r6 = java.lang.String.valueOf(r13)
            java.lang.String r12 = "Issue parsing session id into start time: "
            java.lang.String r6 = r12.concat(r6)
            com.flurry.sdk.da.a(r1, r0, r6)
            r20 = r14
            r13 = 1
        L_0x010b:
            java.io.File r6 = new java.io.File
            r6.<init>(r2, r11)
            boolean r11 = r6.exists()
            if (r11 == 0) goto L_0x013c
            com.flurry.sdk.w r11 = new com.flurry.sdk.w
            r11.<init>(r6)
            java.util.List r11 = r11.a()
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r14 = "Number of crash breadcrumbs - "
            r12.<init>(r14)
            int r14 = r11.size()
            r12.append(r14)
            java.lang.String r12 = r12.toString()
            com.flurry.sdk.da.a(r1, r0, r12)
            r6.delete()
            r30 = r11
            r16 = r13
            goto L_0x0145
        L_0x013c:
            java.lang.String r6 = "Breadcrumbs file does not exist."
            com.flurry.sdk.da.a(r1, r0, r6)
            r30 = r5
            r16 = 1
        L_0x0145:
            com.flurry.sdk.y r6 = com.flurry.sdk.y.NATIVE_CRASH
            java.lang.String r6 = r6.c
            java.io.File r11 = new java.io.File
            r11.<init>(r2, r10)
            boolean r10 = r11.exists()
            if (r10 == 0) goto L_0x01ab
            if (r16 == 0) goto L_0x015f
            java.lang.String r6 = "Some error occurred with minidump file. Deleting it."
            com.flurry.sdk.da.a(r1, r0, r6)
            r11.delete()
            goto L_0x01b0
        L_0x015f:
            java.lang.String r31 = com.flurry.sdk.dz.c(r11)
            r11.delete()
            java.lang.String r32 = g()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "Logcat size: "
            r9.<init>(r10)
            int r10 = r32.length()
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            com.flurry.sdk.da.a(r1, r0, r9)
            java.util.concurrent.atomic.AtomicInteger r9 = com.flurry.sdk.gk.b()
            int r18 = r9.incrementAndGet()
            com.flurry.sdk.gl r9 = new com.flurry.sdk.gl
            com.flurry.sdk.gk$a r10 = com.flurry.sdk.gk.a.UNRECOVERABLE_CRASH
            int r10 = r10.d
            com.flurry.sdk.gk$b r11 = com.flurry.sdk.gk.b.NATIVE_CRASH_ATTACHED
            int r11 = r11.d
            r27 = 0
            r28 = 0
            int r29 = com.flurry.sdk.w.b()
            java.lang.String r22 = ""
            java.lang.String r23 = ""
            java.lang.String r24 = ""
            r17 = r9
            r19 = r6
            r25 = r10
            r26 = r11
            r17.<init>(r18, r19, r20, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32)
            goto L_0x01b0
        L_0x01ab:
            java.lang.String r6 = "Minidump file doesn't exist."
            com.flurry.sdk.da.a(r1, r0, r6)
        L_0x01b0:
            int r8 = r8 + 1
            r6 = 0
            goto L_0x0051
        L_0x01b5:
            java.lang.String r2 = "Finished getting native crash entity."
            com.flurry.sdk.da.a(r1, r0, r2)
            return r9
        L_0x01bb:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.fm.f():com.flurry.sdk.gl");
    }

    private static String g() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("logcat -d").getInputStream()));
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null || i >= 1000 || sb.length() + readLine.length() > 524288) {
                    da.a(4, "FileWriterModule", "Get Logcat lines: ".concat(String.valueOf(i)));
                } else {
                    sb.append(readLine);
                    sb.append("\n");
                    i++;
                }
            }
            da.a(4, "FileWriterModule", "Get Logcat lines: ".concat(String.valueOf(i)));
            return sb.toString();
        } catch (IOException unused) {
            return "";
        }
    }

    public final void a() {
        fg.a();
        File file = new File(fg.d());
        if (!file.exists()) {
            file.mkdirs();
        }
        fg.a();
        File file2 = new File(fg.b());
        if (!file2.exists()) {
            file2.mkdirs();
        }
        gl f = f();
        gk a2 = f != null ? gk.a(f) : null;
        if (fg.b(fg.d() + File.separator + "currentFile")) {
            if (fg.b(fg.d() + File.separator + "crashFile")) {
                jr jrVar = new jr(fg.d(), "currentFile");
                jr jrVar2 = new jr(fg.d(), "crashFile");
                boolean z = false;
                if (fh.a(jrVar, jrVar2)) {
                    if (fh.a(jrVar.a, jrVar.b, jrVar2.a, jrVar2.b)) {
                        boolean b2 = js.b(jrVar, jrVar2);
                        if (b2) {
                            b2 = js.a(jrVar2);
                        }
                        z = b2;
                    }
                }
                if (z) {
                    n.a().p.a("Append crash report");
                } else {
                    bg bgVar = n.a().p;
                    bgVar.a("Fail to append " + jrVar2.toString() + " to " + jrVar.toString());
                }
            }
            e();
        }
        if (a("currentFile")) {
            this.a.a(ir.b(), (gd.a) null);
            if (a2 != null) {
                this.a.a(a2);
            }
        }
    }
}
