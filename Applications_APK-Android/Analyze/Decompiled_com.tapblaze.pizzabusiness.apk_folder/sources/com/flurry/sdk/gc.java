package com.flurry.sdk;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.security.DigestOutputStream;

public final class gc {
    public static byte[] a(jp jpVar) {
        DataOutputStream dataOutputStream;
        byte[] bArr = null;
        try {
            j jVar = new j();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DigestOutputStream digestOutputStream = new DigestOutputStream(byteArrayOutputStream, jVar);
            dataOutputStream = new DataOutputStream(digestOutputStream);
            try {
                da.a(3, "FrameSerializer", "Adding frame " + jpVar.a() + " payload " + jpVar.e());
                dataOutputStream.writeByte(jpVar.g());
                int i = jpVar.a().N;
                byte[] bArr2 = new byte[4];
                bArr2[0] = (byte) (i >> 16);
                bArr2[1] = (byte) (i >> 8);
                bArr2[2] = (byte) (i >> 0);
                for (int i2 = 0; i2 < 3; i2++) {
                    dataOutputStream.write(bArr2[i2]);
                }
                dataOutputStream.writeLong(jpVar.c());
                dataOutputStream.writeLong(jpVar.d());
                byte[] bytes = jpVar.e().getBytes("UTF-8");
                dataOutputStream.writeInt(bytes.length);
                dataOutputStream.write(bytes);
                if (jpVar.h()) {
                    digestOutputStream.on(false);
                    dataOutputStream.writeInt(jVar.a());
                }
                dataOutputStream.close();
                bArr = byteArrayOutputStream.toByteArray();
            } catch (Throwable th) {
                th = th;
                try {
                    da.a(3, "FrameSerializer", "Error when generating report", th);
                    n.a().p.a("FrameSerializerException", "Exception caught when serialize report", th);
                    ea.a(dataOutputStream);
                    return bArr;
                } catch (Throwable th2) {
                    ea.a(dataOutputStream);
                    throw th2;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            da.a(3, "FrameSerializer", "Error when generating report", th);
            n.a().p.a("FrameSerializerException", "Exception caught when serialize report", th);
            ea.a(dataOutputStream);
            return bArr;
        }
        ea.a(dataOutputStream);
        return bArr;
    }
}
