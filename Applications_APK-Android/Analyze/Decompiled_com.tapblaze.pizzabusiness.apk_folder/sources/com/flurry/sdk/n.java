package com.flurry.sdk;

public final class n {
    private static n r;
    public at a = new at(this.i);
    public av b = new av(this.i);
    public ap c = new ap();
    public ar d = new ar();
    public bj e = new bj();
    public ab f = new ab();
    public ag g = new ag(this.i);
    public ac h = new ac(this.m, this.i);
    public q i = new q(this.c);
    public ax j = new ax(this.i);
    public bb k = new bb(this.i);
    public u l = new u();
    public an m = new an(this.c);
    public bi n = new bi(this.k, this.m);
    public az o = new az();
    public bg p = new bg();
    public t q = new t();

    private n() {
        this.a.b();
        this.c.b();
        this.d.b();
        this.e.b();
        this.f.b();
        this.m.b();
        this.i.b();
        this.g.b();
        this.h.b();
        this.j.b();
        this.b.b();
        this.k.b();
        this.l.b();
        this.n.b();
        this.o.b();
        this.p.b();
        this.q.b();
    }

    public static synchronized n a() {
        n nVar;
        synchronized (n.class) {
            if (r == null) {
                r = new n();
            }
            nVar = r;
        }
        return nVar;
    }

    public static synchronized void b() {
        synchronized (n.class) {
            if (r != null) {
                n nVar = r;
                nVar.a.c();
                nVar.c.c();
                nVar.d.c();
                nVar.e.c();
                nVar.f.c();
                nVar.m.c();
                nVar.i.c();
                nVar.g.c();
                nVar.h.c();
                nVar.j.c();
                nVar.b.c();
                nVar.k.c();
                nVar.l.c();
                nVar.n.c();
                nVar.o.c();
                nVar.p.c();
                nVar.q.c();
                nVar.a = null;
                nVar.c = null;
                nVar.d = null;
                nVar.e = null;
                nVar.f = null;
                nVar.m = null;
                nVar.i = null;
                nVar.g = null;
                nVar.h = null;
                nVar.j = null;
                nVar.b = null;
                nVar.k = null;
                nVar.l = null;
                nVar.n = null;
                nVar.o = null;
                nVar.p = null;
                nVar.q = null;
                r = null;
            }
        }
    }
}
