package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class gz extends jl {
    public final bd a;
    public final long b;
    public final long c;
    public final long d;
    public final bc e;
    public final boolean f;

    public gz(ba baVar) {
        this.a = baVar.a;
        this.b = baVar.b;
        this.c = baVar.c;
        this.d = baVar.d;
        this.e = baVar.e;
        this.f = baVar.f;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.session.timestamp", this.b);
        jSONObject.put("fl.initial.timestamp", this.c);
        jSONObject.put("fl.continue.session.millis", this.d);
        jSONObject.put("fl.session.state", this.a.d);
        jSONObject.put("fl.session.event", this.e.name());
        jSONObject.put("fl.session.manual", this.f);
        return jSONObject;
    }
}
