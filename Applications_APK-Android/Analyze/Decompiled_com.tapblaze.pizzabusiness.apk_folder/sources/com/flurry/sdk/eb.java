package com.flurry.sdk;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class eb {
    public static JSONObject a(Map<String, String> map) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                jSONObject.put((String) next.getKey(), next.getValue());
            }
        }
        return jSONObject;
    }

    public static JSONObject b(Map<String, Map<String, String>> map) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                for (Map.Entry entry : ((Map) next.getValue()).entrySet()) {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put((String) entry.getKey(), entry.getValue());
                    jSONObject2 = jSONObject3;
                }
                jSONObject.put((String) next.getKey(), jSONObject2);
            }
        }
        return jSONObject;
    }
}
