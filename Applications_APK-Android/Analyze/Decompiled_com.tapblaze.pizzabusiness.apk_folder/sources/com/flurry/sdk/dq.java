package com.flurry.sdk;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class dq<ObjectType> implements dt<ObjectType> {
    protected final dt<ObjectType> a;

    public dq(dt<ObjectType> dtVar) {
        this.a = dtVar;
    }

    public void a(OutputStream outputStream, ObjectType objecttype) throws IOException {
        dt<ObjectType> dtVar = this.a;
        if (dtVar != null && outputStream != null && objecttype != null) {
            dtVar.a(outputStream, objecttype);
        }
    }

    public ObjectType a(InputStream inputStream) throws IOException {
        dt<ObjectType> dtVar = this.a;
        if (dtVar == null || inputStream == null) {
            return null;
        }
        return dtVar.a(inputStream);
    }
}
