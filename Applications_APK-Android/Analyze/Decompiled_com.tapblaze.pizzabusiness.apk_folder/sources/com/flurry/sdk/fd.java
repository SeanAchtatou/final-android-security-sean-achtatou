package com.flurry.sdk;

public final class fd {
    public static final String a = System.getProperty("line.separator");

    public enum a {
        AGENT_REPORT_TYPE_MAIN_DEVICE(1),
        AGENT_REPORT_TYPE_WATCH(2),
        AGENT_REPORT_TYPE_INSTANT_APP(3);
        
        public final int d;

        private a(int i) {
            this.d = i;
        }
    }
}
