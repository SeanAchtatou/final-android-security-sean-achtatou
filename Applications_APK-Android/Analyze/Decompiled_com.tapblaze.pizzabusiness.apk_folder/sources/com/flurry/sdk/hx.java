package com.flurry.sdk;

import com.flurry.android.FlurryAgent;

public final class hx implements o<bf> {
    public final /* synthetic */ void a(Object obj) {
        bf bfVar = (bf) obj;
        int i = AnonymousClass1.a[bfVar.e - 1];
        if (i == 1) {
            FlurryAgent.logEvent(bfVar.a, bfVar.b);
            da.a(4, "SDKLogObserver", "Log SDK internal events. " + bfVar.a + "SDKLogProvider");
        } else if (i == 2) {
            FlurryAgent.onError(bfVar.a, bfVar.c, bfVar.d, bfVar.b);
            da.a(4, "SDKLogObserver", "Log SDK internal errors. " + bfVar.c + "SDKLogProvider");
        }
    }

    /* renamed from: com.flurry.sdk.hx$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[be.a().length];

        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
        static {
            /*
                int[] r0 = com.flurry.sdk.be.a()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.flurry.sdk.hx.AnonymousClass1.a = r0
                r0 = 1
                int[] r1 = com.flurry.sdk.hx.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0011 }
                int r2 = com.flurry.sdk.be.b     // Catch:{ NoSuchFieldError -> 0x0011 }
                int r2 = r2 - r0
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0011 }
            L_0x0011:
                int[] r1 = com.flurry.sdk.hx.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0019 }
                int r2 = com.flurry.sdk.be.a     // Catch:{ NoSuchFieldError -> 0x0019 }
                int r2 = r2 - r0
                r0 = 2
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0019 }
            L_0x0019:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.hx.AnonymousClass1.<clinit>():void");
        }
    }
}
