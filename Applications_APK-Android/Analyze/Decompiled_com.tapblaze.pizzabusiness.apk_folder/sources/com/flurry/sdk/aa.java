package com.flurry.sdk;

import java.util.List;
import java.util.Map;

public final class aa {
    public final String a;
    public final long b;
    public final String c;
    public final String d;
    public final Throwable e;
    public final Map<String, String> f;
    public final Map<String, String> g;
    public final List<v> h;

    public aa(String str, long j, String str2, String str3, Throwable th, Map<String, String> map, Map<String, String> map2, List<v> list) {
        this.a = str;
        this.b = j;
        this.c = str2;
        this.d = str3;
        this.e = th;
        this.f = map;
        this.g = map2;
        this.h = list;
    }
}
