package com.flurry.sdk;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public final class cl implements Comparable<cl> {
    ci a;
    public int b;
    public int c;
    JSONObject d;
    Map<String, ca> e = new HashMap();

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        cl clVar = (cl) obj;
        ci ciVar = this.a;
        if (ciVar != clVar.a) {
            return ciVar == ci.a ? -1 : 1;
        }
        return this.b - clVar.b;
    }

    public cl(cl clVar) {
        this.a = clVar.a;
        this.b = clVar.b;
        this.c = clVar.c;
        this.d = clVar.d;
        this.e = new HashMap(clVar.e);
    }

    public cl(ci ciVar) {
        this.a = ciVar;
    }

    public final ca a(String str) {
        return this.e.get(str);
    }

    public final Set<Map.Entry<String, ca>> a() {
        return this.e.entrySet();
    }

    public final void a(cl clVar) {
        for (Map.Entry next : clVar.a()) {
            String str = (String) next.getKey();
            if (!this.e.containsKey(str)) {
                this.e.put(str, next.getValue());
            }
        }
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cl)) {
            return false;
        }
        cl clVar = (cl) obj;
        return this.a == clVar.a && this.b == clVar.b;
    }

    public final int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }

    public final String toString() {
        return this.a + ":" + this.b + ":" + this.c;
    }
}
