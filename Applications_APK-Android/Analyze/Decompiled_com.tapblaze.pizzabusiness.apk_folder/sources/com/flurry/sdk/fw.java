package com.flurry.sdk;

import com.flurry.sdk.fv;
import java.util.HashSet;
import java.util.Set;

public final class fw implements fv {
    private static final Set<String> h = new HashSet();

    public final void a() {
    }

    public final fv.a a(jp jpVar) {
        if (!jpVar.a().equals(jn.ORIGIN_ATTRIBUTE)) {
            return a;
        }
        String str = ((ja) jpVar.f()).a;
        if (h.size() < 10 || h.contains(str)) {
            h.add(str);
            return a;
        }
        da.e("OriginAttributeDropRule", "MaxOrigins exceeded: " + h.size());
        return d;
    }
}
