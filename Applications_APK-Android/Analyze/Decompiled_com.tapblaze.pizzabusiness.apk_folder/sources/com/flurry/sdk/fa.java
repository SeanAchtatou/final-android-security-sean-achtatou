package com.flurry.sdk;

import com.flurry.sdk.ey;
import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.PriorityQueue;

public final class fa extends f implements jq {
    /* access modifiers changed from: private */
    public PriorityQueue<String> a;

    public final void a() {
    }

    public fa() {
        super("FrameLogTestHandler", ey.a(ey.a.CORE));
        this.a = null;
        this.a = new PriorityQueue<>(4, new fi());
    }

    public final void a(final List<String> list) {
        if (list.size() == 0) {
            da.b("FrameLogTestHandler", "File List is null or empty");
            return;
        }
        da.b("FrameLogTestHandler", "Number of files being added:" + list.toString());
        b(new ec() {
            public final void a() throws Exception {
                fa.this.a.addAll(list);
                fa.this.b();
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        da.b("FrameLogTestHandler", " Starting processNextFile " + this.a.size());
        if (this.a.peek() == null) {
            da.b("FrameLogTestHandler", "No file present to process.");
            return;
        }
        String poll = this.a.poll();
        if (fg.b(poll)) {
            File file = new File(poll);
            File a2 = dz.a();
            boolean a3 = js.a(file, new File(a2.toString() + File.separator + "fCompletedInApp", String.format(Locale.US, "completedInApp-%d", Long.valueOf(System.currentTimeMillis()))));
            if (a3) {
                a3 = file.delete();
            }
            a(poll, a3);
        }
    }

    private synchronized void a(String str, boolean z) {
        da.b("FrameLogTestHandler", "File move to test folder for file: " + str + " fileMoved:" + z);
        boolean a2 = fg.a(str);
        da.a(2, "FrameLogTestHandler", "Deleting file " + str + " deleted " + a2);
        b();
    }
}
