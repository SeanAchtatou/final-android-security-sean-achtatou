package com.flurry.sdk;

import com.flurry.android.FlurryAgent;
import java.io.IOException;
import java.util.HashMap;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public final class dl {
    public static void a(String str, String str2, String str3, long j, int i, String str4, long j2) {
        HashMap hashMap = new HashMap();
        hashMap.put("fl.id", str);
        hashMap.put("fl.http.method", str2);
        hashMap.put("fl.request.url", str3);
        hashMap.put("fl.request.length", Long.toString(j));
        hashMap.put("fl.response.code", Integer.toString(i));
        hashMap.put("fl.response.url", str4);
        hashMap.put("fl.response.time", Long.toString(j2));
        da.c("HttpLogging", "Logging parameters: ".concat(String.valueOf(hashMap)));
        FlurryAgent.logEvent("Flurry.HTTPRequestTime", hashMap);
    }

    public static class a implements Interceptor {
        private String a;

        public a(String str) {
            this.a = str;
        }

        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();
            long nanoTime = System.nanoTime();
            long contentLength = request.body() == null ? 0 : request.body().contentLength();
            String method = request.method();
            String httpUrl = request.url().toString();
            da.a(3, "HttpLogging", "Sending request " + method + " for " + httpUrl + " length " + contentLength);
            Response proceed = chain.proceed(request);
            double nanoTime2 = (double) (System.nanoTime() - nanoTime);
            Double.isNaN(nanoTime2);
            long j = (long) (nanoTime2 / 1000000.0d);
            int code = proceed.code();
            String httpUrl2 = proceed.request().url().toString();
            da.a(3, "HttpLogging", "Received response " + code + " for " + httpUrl2 + " in " + j + " ms");
            dl.a(this.a, method, httpUrl, contentLength, code, httpUrl2, j);
            return proceed;
        }
    }
}
