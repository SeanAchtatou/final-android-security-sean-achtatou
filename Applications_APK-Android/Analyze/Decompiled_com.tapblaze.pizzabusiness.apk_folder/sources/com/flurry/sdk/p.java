package com.flurry.sdk;

public enum p {
    UNKNOWN(-1),
    FOREGROUND(0),
    BACKGROUND(2);
    
    public int d;

    private p(int i) {
        this.d = i;
    }
}
