package com.flurry.sdk;

import com.flurry.sdk.bz;

public final class ck extends bz {
    /* access modifiers changed from: private */
    public final o<ak> a = new o<ak>() {
        public final /* synthetic */ void a(Object obj) {
            n.a().g.b(ck.this.a);
            ck.this.d();
        }
    };

    public ck(cr crVar, bz.a aVar, cc ccVar, cm cmVar) {
        super(crVar, aVar, ccVar, cmVar);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (n.a().g.f()) {
            d();
            return;
        }
        da.a("StreamingConfigFetcher", "Waiting for ID provider.");
        n.a().g.a(this.a);
    }

    /* access modifiers changed from: protected */
    public final String c() {
        return n.a().h.b;
    }
}
