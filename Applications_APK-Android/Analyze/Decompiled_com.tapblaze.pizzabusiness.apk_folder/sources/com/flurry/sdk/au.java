package com.flurry.sdk;

public final class au {
    public final boolean a;
    public final a b;

    public enum a {
        NONE_OR_UNKNOWN(0),
        NETWORK_AVAILABLE(1),
        WIFI(2),
        CELL(3);
        
        public int e;

        private a(int i) {
            this.e = i;
        }
    }

    au(a aVar, boolean z) {
        this.a = z;
        this.b = aVar;
    }
}
