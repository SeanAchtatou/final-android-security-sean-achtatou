package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class ig extends jl {
    public final int a;
    public final int b;

    public ig(int i, int i2) {
        this.b = i < 0 ? p.UNKNOWN.d : i;
        this.a = i2 < 0 ? p.UNKNOWN.d : i2;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.app.current.state", this.a);
        jSONObject.put("fl.app.previous.state", this.b);
        return jSONObject;
    }
}
