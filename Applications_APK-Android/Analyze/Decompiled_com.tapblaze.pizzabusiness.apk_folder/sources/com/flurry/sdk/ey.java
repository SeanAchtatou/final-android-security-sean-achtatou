package com.flurry.sdk;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class ey {
    private static final Map<a, e> a;

    public enum a {
        CORE,
        DATA_PROCESSOR,
        PROVIDER,
        PUBLIC_API,
        REPORTS,
        CONFIG,
        MISC
    }

    static {
        Executor executor;
        HashMap hashMap = new HashMap();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        for (a aVar : a.values()) {
            String name = aVar.name();
            int i = AnonymousClass1.a[aVar.ordinal()];
            if (i == 1) {
                executor = a();
            } else if (i == 2) {
                executor = a(availableProcessors);
            } else if (i == 3) {
                executor = a();
            } else if (i == 4) {
                executor = a(availableProcessors);
            } else if (i != 5) {
                executor = a();
            } else {
                executor = a();
            }
            hashMap.put(aVar, new e(executor, name));
        }
        a = Collections.unmodifiableMap(hashMap);
    }

    /* renamed from: com.flurry.sdk.ey$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.flurry.sdk.ey$a[] r0 = com.flurry.sdk.ey.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.flurry.sdk.ey.AnonymousClass1.a = r0
                int[] r0 = com.flurry.sdk.ey.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.flurry.sdk.ey$a r1 = com.flurry.sdk.ey.a.PUBLIC_API     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.flurry.sdk.ey.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.flurry.sdk.ey$a r1 = com.flurry.sdk.ey.a.CORE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.flurry.sdk.ey.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.flurry.sdk.ey$a r1 = com.flurry.sdk.ey.a.DATA_PROCESSOR     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.flurry.sdk.ey.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.flurry.sdk.ey$a r1 = com.flurry.sdk.ey.a.PROVIDER     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.flurry.sdk.ey.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.flurry.sdk.ey$a r1 = com.flurry.sdk.ey.a.CONFIG     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.ey.AnonymousClass1.<clinit>():void");
        }
    }

    public static synchronized e a(a aVar) {
        e eVar;
        synchronized (ey.class) {
            eVar = a.get(aVar);
        }
        return eVar;
    }

    private static Executor a(int i) {
        return new ThreadPoolExecutor(0, i, 6, TimeUnit.SECONDS, new LinkedBlockingQueue(), new c(), new b((byte) 0));
    }

    private static Executor a() {
        return new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new c(), new b((byte) 0));
    }

    static class b implements RejectedExecutionHandler {
        private b() {
        }

        /* synthetic */ b(byte b) {
            this();
        }

        public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            da.a(6, "ActorFactory", runnable.toString() + "is rejected");
        }
    }

    static class c implements ThreadFactory {
        private final AtomicInteger a = new AtomicInteger(0);
        private final String b = "Flurry #";

        c() {
        }

        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, this.b + this.a.incrementAndGet());
            if (thread.isDaemon()) {
                thread.setDaemon(false);
            }
            thread.setPriority(10);
            return thread;
        }
    }
}
