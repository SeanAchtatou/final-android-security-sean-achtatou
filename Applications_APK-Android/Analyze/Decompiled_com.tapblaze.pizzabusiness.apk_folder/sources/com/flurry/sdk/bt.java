package com.flurry.sdk;

public final class bt extends bp {
    /* access modifiers changed from: protected */
    public final String d() {
        return "https://data.flurry.com/aap.do";
    }

    public bt() {
        super("Analytics", "FlurryStreamingUpdateDataSender");
    }

    /* access modifiers changed from: protected */
    public final void a(int i, String str, String str2) {
        if (n.a().k.i.get()) {
            ea.a(i, str, str2, false, true);
            return;
        }
        ff.a("last_legacy_http_error_code", i);
        ff.a("last_legacy_http_error_message", str);
        ff.a("last_legacy_http_report_identifier", str2);
    }
}
