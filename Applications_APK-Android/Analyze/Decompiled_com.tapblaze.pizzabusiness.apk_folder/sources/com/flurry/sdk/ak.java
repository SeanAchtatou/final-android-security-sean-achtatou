package com.flurry.sdk;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class ak {
    protected Map<al, String> a;
    public boolean b;

    ak() {
        this.a = new HashMap();
    }

    private ak(Map<al, String> map, boolean z) {
        this.a = map;
        this.b = z;
    }

    /* access modifiers changed from: package-private */
    public final void a(al alVar, String str) {
        this.a.put(alVar, str);
    }

    public final Map<al, String> a() {
        return this.a;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append(this.b);
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final ak b() {
        return new ak(Collections.unmodifiableMap(this.a), this.b);
    }
}
