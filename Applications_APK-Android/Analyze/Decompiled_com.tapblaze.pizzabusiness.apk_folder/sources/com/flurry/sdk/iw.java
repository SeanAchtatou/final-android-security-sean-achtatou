package com.flurry.sdk;

import android.location.Location;
import android.os.Build;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

public final class iw extends jl {
    public final int a;
    public boolean b;
    public boolean c;
    public final Location d;

    public iw(int i, boolean z, boolean z2, Location location) {
        this.a = i;
        this.b = z;
        this.c = z2;
        this.d = location;
    }

    public final JSONObject a() throws JSONException {
        boolean z;
        double d2;
        double d3;
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.report.location.enabled", this.b);
        if (this.b) {
            jSONObject.put("fl.location.permission.status", this.c);
            if (this.c && this.d != null) {
                int i = Build.VERSION.SDK_INT;
                boolean z2 = false;
                double d4 = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
                if (i >= 26) {
                    d4 = (double) this.d.getVerticalAccuracyMeters();
                    d3 = (double) this.d.getBearingAccuracyDegrees();
                    d2 = (double) this.d.getSpeedAccuracyMetersPerSecond();
                    z2 = this.d.hasBearingAccuracy();
                    z = this.d.hasSpeedAccuracy();
                } else {
                    d3 = 0.0d;
                    d2 = 0.0d;
                    z = false;
                }
                jSONObject.put("fl.precision.value", this.a);
                jSONObject.put("fl.latitude.value", this.d.getLatitude());
                jSONObject.put("fl.longitude.value", this.d.getLongitude());
                jSONObject.put("fl.horizontal.accuracy.value", (double) this.d.getAccuracy());
                jSONObject.put("fl.time.epoch.value", this.d.getTime());
                if (Build.VERSION.SDK_INT >= 17) {
                    jSONObject.put("fl.time.uptime.value", TimeUnit.NANOSECONDS.toMillis(this.d.getElapsedRealtimeNanos()));
                }
                jSONObject.put("fl.altitude.value", this.d.getAltitude());
                jSONObject.put("fl.vertical.accuracy.value", d4);
                jSONObject.put("fl.bearing.value", (double) this.d.getBearing());
                jSONObject.put("fl.speed.value", (double) this.d.getSpeed());
                jSONObject.put("fl.bearing.accuracy.available", z2);
                jSONObject.put("fl.speed.accuracy.available", z);
                jSONObject.put("fl.bearing.accuracy.degrees", d3);
                jSONObject.put("fl.speed.accuracy.meters.per.sec", d2);
            }
        }
        return jSONObject;
    }
}
