package com.flurry.sdk;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class cw<K, V> {
    public final Map<K, List<V>> a = new HashMap();
    private int b;

    public final List<V> a(String str, boolean z) {
        List<V> list = this.a.get(str);
        if (z && list == null) {
            int i = this.b;
            if (i > 0) {
                list = new ArrayList<>(i);
            } else {
                list = new ArrayList<>();
            }
            this.a.put(str, list);
        }
        return list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cw.a(java.lang.String, boolean):java.util.List<V>
     arg types: [java.lang.String, int]
     candidates:
      com.flurry.sdk.cw.a(java.lang.String, java.lang.String):void
      com.flurry.sdk.cw.a(java.lang.String, boolean):java.util.List<V> */
    public final void a(String str, String str2) {
        if (str != null) {
            a(str, true).add(str2);
        }
    }

    public final Collection<Map.Entry<K, V>> a() {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : this.a.entrySet()) {
            for (Object simpleImmutableEntry : (List) next.getValue()) {
                arrayList.add(new AbstractMap.SimpleImmutableEntry(next.getKey(), simpleImmutableEntry));
            }
        }
        return arrayList;
    }
}
