package com.flurry.sdk;

import java.io.PrintStream;
import java.io.PrintWriter;

public abstract class ec implements Runnable {
    private PrintStream a;
    private PrintWriter b;

    public abstract void a() throws Exception;

    public final void run() {
        try {
            a();
        } catch (Throwable th) {
            PrintStream printStream = this.a;
            if (printStream != null) {
                th.printStackTrace(printStream);
            } else {
                PrintWriter printWriter = this.b;
                if (printWriter != null) {
                    th.printStackTrace(printWriter);
                } else {
                    th.printStackTrace();
                }
            }
            da.a(6, "SafeRunnable", "", th);
            n.a().p.a("SafeRunnableException", "Exception caught by SafeRunnable", th);
        }
    }
}
