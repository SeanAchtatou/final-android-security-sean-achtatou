package com.flurry.sdk;

import com.flurry.sdk.fn;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

final class fq extends fs {
    protected List<fz> a = new ArrayList();
    protected fy b = new fy() {
        public final void a(jp jpVar) {
            fq.this.c(jpVar);
        }

        public final void b(jp jpVar) {
            fq.this.d(jpVar);
        }

        public final void a(Runnable runnable) {
            Future unused = fq.this.b(runnable);
        }
    };

    fq(fn fnVar) {
        super("PolicyModule", fnVar);
        this.a.add(new ga(this.b));
    }

    public final void a(final jp jpVar) {
        b(new ec() {
            public final void a() {
                fq.a(fq.this, jpVar);
                fq.this.d(jpVar);
            }
        });
    }

    public final fn.a b(final jp jpVar) {
        b(new ec() {
            public final void a() throws Exception {
                fq.a(fq.this, jpVar);
            }
        });
        return super.b(jpVar);
    }

    public final void c() {
        for (fz a2 : this.a) {
            a2.a();
        }
    }

    static /* synthetic */ void a(fq fqVar, jp jpVar) {
        for (fz a2 : fqVar.a) {
            a2.a(jpVar);
        }
    }
}
