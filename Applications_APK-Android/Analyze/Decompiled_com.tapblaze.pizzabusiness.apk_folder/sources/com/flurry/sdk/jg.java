package com.flurry.sdk;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class jg extends jl {
    public final Map<al, String> a;
    public final boolean b;

    public jg(Map<al, String> map, boolean z) {
        this.a = new HashMap(map);
        this.b = z;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        for (Map.Entry next : this.a.entrySet()) {
            jSONObject2.put(((al) next.getKey()).name(), next.getValue());
        }
        jSONObject.put("fl.reported.id", jSONObject2);
        jSONObject.put("fl.ad.tracking", this.b);
        return jSONObject;
    }
}
