package com.flurry.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Timer;
import java.util.TimerTask;

public final class cc {
    public final SharedPreferences a;
    int b = 0;
    long c;
    private Timer d;
    private final Object e = new Object();

    public cc() {
        Context a2 = b.a();
        this.a = a2.getSharedPreferences("FLURRY_SHARED_PREFERENCES", 0);
        bn.a();
        this.b = bn.b(a2);
        SharedPreferences sharedPreferences = this.a;
        this.c = sharedPreferences != null ? sharedPreferences.getLong("refreshFetch", 604800000) : 604800000;
    }

    public final int a() {
        SharedPreferences sharedPreferences = this.a;
        if (sharedPreferences != null) {
            return sharedPreferences.getInt("appVersion", 0);
        }
        return 0;
    }

    private void e() {
        SharedPreferences sharedPreferences = this.a;
        if (sharedPreferences != null) {
            sharedPreferences.edit().remove("appVersion").apply();
        }
    }

    public final synchronized void a(TimerTask timerTask, long j) {
        synchronized (this.e) {
            da.a("ConfigMeta", "Record retry after " + j + " msecs.");
            this.d = new Timer("retry-scheduler");
            this.d.schedule(timerTask, j);
        }
    }

    public final void b() {
        synchronized (this.e) {
            if (this.d != null) {
                da.a(3, "ConfigMeta", "Clear retry.");
                this.d.cancel();
                this.d.purge();
                this.d = null;
            }
        }
    }

    public final void c() {
        da.a("ConfigMeta", "Clear all ConfigMeta data.");
        b();
        e();
        f();
        g();
        h();
        i();
        j();
    }

    private void f() {
        SharedPreferences sharedPreferences = this.a;
        if (sharedPreferences != null) {
            sharedPreferences.edit().remove("lastFetch").apply();
        }
    }

    public final void a(long j) {
        SharedPreferences sharedPreferences = this.a;
        if (sharedPreferences != null) {
            sharedPreferences.edit().putLong("lastFetch", j).apply();
        }
    }

    private void g() {
        SharedPreferences sharedPreferences = this.a;
        if (sharedPreferences != null) {
            sharedPreferences.edit().remove("lastETag").apply();
        }
    }

    public final String d() {
        SharedPreferences sharedPreferences = this.a;
        if (sharedPreferences != null) {
            return sharedPreferences.getString("lastKeyId", null);
        }
        return null;
    }

    private void h() {
        SharedPreferences sharedPreferences = this.a;
        if (sharedPreferences != null) {
            sharedPreferences.edit().remove("lastKeyId").apply();
        }
    }

    private void i() {
        SharedPreferences sharedPreferences = this.a;
        if (sharedPreferences != null) {
            sharedPreferences.edit().remove("lastRSA").apply();
        }
    }

    private void j() {
        SharedPreferences sharedPreferences = this.a;
        if (sharedPreferences != null) {
            sharedPreferences.edit().remove("com.flurry.sdk.variant_ids").apply();
        }
    }
}
