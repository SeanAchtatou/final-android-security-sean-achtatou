package com.flurry.sdk;

import com.flurry.sdk.ac;

public final class ad {
    public final String a;
    public final boolean b;
    public final ac.a c;
    public final am d;

    public ad(String str, boolean z, ac.a aVar, am amVar) {
        this.a = str;
        this.b = z;
        this.c = aVar;
        this.d = amVar;
    }
}
