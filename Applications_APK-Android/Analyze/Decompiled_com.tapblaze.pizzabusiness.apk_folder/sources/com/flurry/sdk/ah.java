package com.flurry.sdk;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class ah {
    public final boolean a;
    public final byte[] b;
    public final byte[] c;
    public final int d;

    ah(byte[] bArr, byte[] bArr2, boolean z, int i) {
        this.b = bArr2;
        this.c = bArr;
        this.a = z;
        this.d = i;
    }

    static class a implements dt<ah> {
        a() {
        }

        public final /* synthetic */ void a(OutputStream outputStream, Object obj) throws IOException {
            ah ahVar = (ah) obj;
            if (outputStream != null && ahVar != null) {
                AnonymousClass1 r0 = new DataOutputStream(outputStream) {
                    public final void close() {
                    }
                };
                r0.writeBoolean(ahVar.a);
                if (ahVar.b == null) {
                    r0.writeInt(0);
                } else {
                    r0.writeInt(ahVar.b.length);
                    r0.write(ahVar.b);
                }
                if (ahVar.c == null) {
                    r0.writeInt(0);
                } else {
                    r0.writeInt(ahVar.c.length);
                    r0.write(ahVar.c);
                }
                r0.writeInt(ahVar.d);
                r0.flush();
            }
        }

        public final /* synthetic */ Object a(InputStream inputStream) throws IOException {
            byte[] bArr;
            byte[] bArr2 = null;
            if (inputStream == null) {
                return null;
            }
            AnonymousClass2 r1 = new DataInputStream(inputStream) {
                public final void close() {
                }
            };
            boolean readBoolean = r1.readBoolean();
            int readInt = r1.readInt();
            if (readInt > 0) {
                bArr = new byte[readInt];
                r1.read(bArr, 0, readInt);
            } else {
                bArr = null;
            }
            int readInt2 = r1.readInt();
            if (readInt2 > 0) {
                bArr2 = new byte[readInt2];
                r1.read(bArr2, 0, readInt2);
            }
            return new ah(bArr2, bArr, readBoolean, r1.readInt());
        }
    }
}
