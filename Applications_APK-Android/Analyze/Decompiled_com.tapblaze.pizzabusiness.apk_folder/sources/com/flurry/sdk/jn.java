package com.flurry.sdk;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.ironsource.mediationsdk.utils.IronSourceConstants;

public enum jn {
    UNKNOWN(0),
    FLUSH_FRAME(1),
    FRAME_COUNTER(2),
    SESSION_ID(128),
    APP_STATE(132),
    APP_INFO(133),
    ANALYTICS_EVENT(134),
    ANALYTICS_ERROR(137),
    DEVICE_PROPERTIES(139),
    REPORTED_ID(IronSourceConstants.USING_CACHE_FOR_INIT_EVENT),
    SESSION_INFO(141),
    SERVER_COOKIES(142),
    DYNAMIC_SESSION_INFO(143),
    REFERRER(145),
    USER_ID(146),
    SESSION_ORIGIN(147),
    LOCALE(148),
    NETWORK(149),
    LOCATION(IronSourceConstants.REWARDED_VIDEO_DAILY_CAPPED),
    PAGE_VIEW(152),
    SESSION_PROPERTIES(153),
    LAUNCH_OPTIONS(155),
    APP_ORIENTATION(156),
    SESSION_PROPERTIES_PARAMS(157),
    NOTIFICATION(158),
    ORIGIN_ATTRIBUTE(160),
    TIMEZONE(162),
    VARIANT_IDS(163),
    REPORTING(164),
    PREVIOUS_SUCCESSFUL_REPORT(166),
    NUM_ERRORS(167),
    GENDER(168),
    BIRTHDATE(169),
    EVENTS_SUMMARY(170),
    USER_PROPERTY(171),
    CONSENT(172),
    CCPA_OPTOUT(174),
    CCPA_DELETION(175),
    EOF(FacebookRequestErrorClassification.EC_INVALID_TOKEN);
    
    public final int N;

    private jn(int i) {
        this.N = i;
    }
}
