package com.flurry.sdk;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

final class fr extends fs {
    protected static jn[] a = {jn.SESSION_INFO, jn.APP_INFO, jn.REPORTED_ID, jn.DEVICE_PROPERTIES, jn.NOTIFICATION, jn.REFERRER, jn.LAUNCH_OPTIONS, jn.CONSENT, jn.APP_STATE, jn.NETWORK, jn.LOCALE, jn.TIMEZONE, jn.APP_ORIENTATION, jn.DYNAMIC_SESSION_INFO, jn.LOCATION, jn.USER_ID, jn.BIRTHDATE, jn.GENDER};
    protected static jn[] b = {jn.ORIGIN_ATTRIBUTE, jn.USER_PROPERTY};
    /* access modifiers changed from: private */
    public EnumMap<jn, jp> k = new EnumMap<>(jn.class);
    /* access modifiers changed from: private */
    public EnumMap<jn, List<jp>> l = new EnumMap<>(jn.class);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [com.flurry.sdk.jn, ?[OBJECT, ARRAY]]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(com.flurry.sdk.jn, com.flurry.sdk.jp):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    fr(fn fnVar) {
        super("StickyModule", fnVar);
        for (jn jnVar : a) {
            this.k.put((Enum) jnVar, (Object) null);
        }
        for (jn jnVar2 : b) {
            this.l.put((Enum) jnVar2, (Object) null);
        }
    }

    public final void a(final jp jpVar) {
        b(new ec() {
            public final void a() {
                fr.this.d(jpVar);
                fr.a(fr.this, jpVar);
                if (jn.FLUSH_FRAME.equals(jpVar.a())) {
                    for (Map.Entry value : fr.this.k.entrySet()) {
                        jp jpVar = (jp) value.getValue();
                        if (jpVar != null) {
                            fr.this.d(jpVar);
                        }
                    }
                    for (Map.Entry value2 : fr.this.l.entrySet()) {
                        List list = (List) value2.getValue();
                        if (!(list == null || list.size() == 0)) {
                            for (int i = 0; i < list.size(); i++) {
                                fr.this.d((jp) list.get(i));
                            }
                        }
                    }
                }
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [com.flurry.sdk.jn, com.flurry.sdk.jp]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(com.flurry.sdk.jn, com.flurry.sdk.jp):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [com.flurry.sdk.jn, java.util.List]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(com.flurry.sdk.jn, com.flurry.sdk.jp):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    static /* synthetic */ void a(fr frVar, jp jpVar) {
        jn a2 = jpVar.a();
        List arrayList = new ArrayList();
        if (frVar.k.containsKey(a2)) {
            frVar.k.put((Enum) a2, (Object) jpVar);
        }
        if (frVar.l.containsKey(a2)) {
            if (frVar.l.get(a2) != null) {
                arrayList = frVar.l.get(a2);
            }
            arrayList.add(jpVar);
            frVar.l.put((Enum) a2, (Object) arrayList);
        }
    }
}
