package com.flurry.sdk;

import android.os.Bundle;
import com.flurry.sdk.ao;
import java.util.HashMap;

public final class q extends m<r> {
    protected o<ao> b = new o<ao>() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.flurry.sdk.q.a(com.flurry.sdk.q, boolean):void
         arg types: [com.flurry.sdk.q, int]
         candidates:
          com.flurry.sdk.m.a(com.flurry.sdk.m, java.lang.Runnable):java.util.concurrent.Future
          com.flurry.sdk.q.a(com.flurry.sdk.q, boolean):void */
        public final /* synthetic */ void a(Object obj) {
            Bundle bundle;
            ao aoVar = (ao) obj;
            int i = AnonymousClass3.a[aoVar.a.ordinal()];
            if (i == 1) {
                q.a(q.this, true);
            } else if (i == 2) {
                q.a(q.this, false);
            } else if (i == 3 && (bundle = aoVar.b) != null && bundle.containsKey("trim_memory_level") && bundle.getInt("trim_memory_level") == 20) {
                q.a(q.this, false);
            }
        }
    };
    private ap h;
    private r i = null;

    public q(ap apVar) {
        super("AppStateChangeProvider");
        this.h = apVar;
        p pVar = p.UNKNOWN;
        this.i = new r(pVar, pVar);
        this.h.a((o) this.b);
    }

    public final void a(final o<r> oVar) {
        super.a((o) oVar);
        final r rVar = this.i;
        b(new ec() {
            public final void a() throws Exception {
                oVar.a(rVar);
            }
        });
    }

    /* renamed from: com.flurry.sdk.q$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] a = new int[ao.a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.flurry.sdk.ao$a[] r0 = com.flurry.sdk.ao.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.flurry.sdk.q.AnonymousClass3.a = r0
                int[] r0 = com.flurry.sdk.q.AnonymousClass3.a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.flurry.sdk.ao$a r1 = com.flurry.sdk.ao.a.STARTED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.flurry.sdk.q.AnonymousClass3.a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.flurry.sdk.ao$a r1 = com.flurry.sdk.ao.a.APP_BACKGROUND     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.flurry.sdk.q.AnonymousClass3.a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.flurry.sdk.ao$a r1 = com.flurry.sdk.ao.a.TRIM_MEMORY     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.q.AnonymousClass3.<clinit>():void");
        }
    }

    public final p d() {
        r rVar = this.i;
        if (rVar == null) {
            return p.UNKNOWN;
        }
        return rVar.b;
    }

    public final void c() {
        super.c();
        this.h.b(this.b);
    }

    static /* synthetic */ void a(q qVar, boolean z) {
        p pVar = z ? p.FOREGROUND : p.BACKGROUND;
        if (qVar.i.b != pVar) {
            qVar.i = new r(qVar.i.b, pVar);
            da.a(2, "AppStateChangeProvider", "AppStateChangeRule: prev " + qVar.i.a + " stateData.currentState:" + qVar.i.b);
            HashMap hashMap = new HashMap();
            hashMap.put("previous_state", qVar.i.a.name());
            hashMap.put("current_state", qVar.i.b.name());
            n.a().p.a("AppStateChangeProvider: app state change", hashMap);
            qVar.a(new r(qVar.i.a, qVar.i.b));
        }
    }
}
