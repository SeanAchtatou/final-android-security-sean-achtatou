package com.flurry.sdk;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

public final class k<ObjectType> {
    private dt<ObjectType> a;

    public k(dt<ObjectType> dtVar) {
        this.a = dtVar;
    }

    public enum a {
        NONE(""),
        CRYPTO_ALGO_PADDING_7("AES/CBC/PKCS7Padding"),
        CRYPTO_ALGO_PADDING_5("AES/CBC/PKCS5Padding");
        
        String d;

        private a(String str) {
            this.d = str;
        }

        public static a a(int i) {
            for (a aVar : values()) {
                if (aVar.ordinal() == i) {
                    return aVar;
                }
            }
            return NONE;
        }
    }

    public final byte[] a(byte[] bArr, Key key, IvParameterSpec ivParameterSpec, a aVar) throws IOException {
        if (bArr == null || key == null || aVar == null) {
            da.a(5, "FlurryCrypto", "Cannot encrypt, invalid params.");
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.a.a(byteArrayOutputStream, bArr);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            Cipher instance = Cipher.getInstance(aVar.d);
            instance.init(1, key, ivParameterSpec);
            return instance.doFinal(byteArray);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            da.a(5, "FlurryCrypto", "Error in encrypt " + e.getMessage());
            return null;
        }
    }

    public final ObjectType a(byte[] bArr, Key key, IvParameterSpec ivParameterSpec, a aVar) throws IOException {
        if (bArr == null || key == null || aVar == null) {
            da.a(5, "FlurryCrypto", "Cannot decrypt, invalid params.");
            return null;
        }
        try {
            Cipher instance = Cipher.getInstance(aVar.d);
            instance.init(2, key, ivParameterSpec);
            return this.a.a(new ByteArrayInputStream(instance.doFinal(bArr)));
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            da.a(5, "FlurryCrypto", "Error in decrypt " + e.getMessage());
            return null;
        }
    }
}
