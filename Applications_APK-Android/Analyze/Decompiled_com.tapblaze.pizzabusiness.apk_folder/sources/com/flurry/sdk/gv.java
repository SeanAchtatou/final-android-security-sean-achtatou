package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class gv extends jl {
    public final int a;

    public gv(int i) {
        this.a = i < 0 ? -1 : i;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.network.status", this.a);
        return jSONObject;
    }
}
