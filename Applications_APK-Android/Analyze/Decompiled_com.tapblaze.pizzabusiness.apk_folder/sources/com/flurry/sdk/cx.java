package com.flurry.sdk;

import java.util.Comparator;

public final class cx implements Comparator<Runnable> {
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        int a = a((Runnable) obj);
        int a2 = a((Runnable) obj2);
        if (a < a2) {
            return -1;
        }
        return a > a2 ? 1 : 0;
    }

    private static int a(Runnable runnable) {
        if (runnable == null) {
            return Integer.MAX_VALUE;
        }
        if (runnable instanceof cy) {
            ed edVar = (ed) ((cy) runnable).a();
            if (edVar != null) {
                return edVar.p;
            }
            return Integer.MAX_VALUE;
        } else if (runnable instanceof ed) {
            return ((ed) runnable).p;
        } else {
            da.a(6, "PriorityComparator", "Unknown runnable class: " + runnable.getClass().getName());
            return Integer.MAX_VALUE;
        }
    }
}
