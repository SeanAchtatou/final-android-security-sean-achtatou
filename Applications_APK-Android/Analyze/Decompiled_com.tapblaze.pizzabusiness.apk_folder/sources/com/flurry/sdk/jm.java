package com.flurry.sdk;

import android.os.SystemClock;
import org.json.JSONException;

public abstract class jm implements jp {
    private long a = System.currentTimeMillis();
    private long b = SystemClock.elapsedRealtime();
    private jo c;
    private boolean d;

    public jm(jo joVar) {
        this.c = joVar;
        this.d = true;
    }

    public final long c() {
        return this.a;
    }

    public final long d() {
        return this.b;
    }

    public final String e() {
        try {
            return this.c.a().toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public final jo f() {
        return this.c;
    }

    public final boolean h() {
        return this.d;
    }

    public final byte g() {
        return (this.d ^ true) | true ? (byte) 1 : 0;
    }
}
