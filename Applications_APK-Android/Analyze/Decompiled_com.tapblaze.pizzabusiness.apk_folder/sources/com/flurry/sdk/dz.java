package com.flurry.sdk;

import android.content.Context;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.util.regex.Pattern;

public final class dz {
    private static String a = "FileUtil";

    public static File a() {
        return b.a().getFilesDir();
    }

    public static File b() {
        File file;
        Context a2 = b.a();
        if (ea.a(21)) {
            file = a2.getNoBackupFilesDir();
        } else {
            file = new File(a2.getFilesDir().getPath() + File.separator + "no_backup");
        }
        return new File(file.getPath() + File.separator + ".flurryNoBackup");
    }

    public static boolean a(File file) {
        if (file == null || file.getAbsoluteFile() == null) {
            return false;
        }
        File parentFile = file.getParentFile();
        if (parentFile == null || parentFile.mkdirs() || parentFile.isDirectory()) {
            return true;
        }
        da.a(6, a, "Unable to create persistent dir: ".concat(String.valueOf(parentFile)));
        return false;
    }

    public static boolean b(File file) {
        if (file == null || !file.isDirectory()) {
            return false;
        }
        for (String file2 : file.list()) {
            if (!b(new File(file, file2))) {
                return false;
            }
        }
        return file.delete();
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005b A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c(java.io.File r6) {
        /*
            r0 = 4
            r1 = 0
            if (r6 == 0) goto L_0x0061
            boolean r2 = r6.exists()
            if (r2 != 0) goto L_0x000b
            goto L_0x0061
        L_0x000b:
            java.lang.String r2 = com.flurry.sdk.dz.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Loading persistent data: "
            r3.<init>(r4)
            java.lang.String r4 = r6.getAbsolutePath()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            com.flurry.sdk.da.a(r0, r2, r3)
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x0046 }
            r0.<init>(r6)     // Catch:{ all -> 0x0046 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0044 }
            r6.<init>()     // Catch:{ all -> 0x0044 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ all -> 0x0044 }
        L_0x0030:
            int r3 = r0.read(r2)     // Catch:{ all -> 0x0044 }
            if (r3 <= 0) goto L_0x0040
            java.lang.String r4 = new java.lang.String     // Catch:{ all -> 0x0044 }
            r5 = 0
            r4.<init>(r2, r5, r3)     // Catch:{ all -> 0x0044 }
            r6.append(r4)     // Catch:{ all -> 0x0044 }
            goto L_0x0030
        L_0x0040:
            com.flurry.sdk.ea.a(r0)
            goto L_0x0054
        L_0x0044:
            r6 = move-exception
            goto L_0x0048
        L_0x0046:
            r6 = move-exception
            r0 = r1
        L_0x0048:
            r2 = 6
            java.lang.String r3 = com.flurry.sdk.dz.a     // Catch:{ all -> 0x005c }
            java.lang.String r4 = "Error when loading persistent file"
            com.flurry.sdk.da.a(r2, r3, r4, r6)     // Catch:{ all -> 0x005c }
            com.flurry.sdk.ea.a(r0)
            r6 = r1
        L_0x0054:
            if (r6 == 0) goto L_0x005b
            java.lang.String r6 = r6.toString()
            return r6
        L_0x005b:
            return r1
        L_0x005c:
            r6 = move-exception
            com.flurry.sdk.ea.a(r0)
            throw r6
        L_0x0061:
            java.lang.String r6 = com.flurry.sdk.dz.a
            java.lang.String r2 = "Persistent file doesn't exist."
            com.flurry.sdk.da.a(r0, r6, r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.dz.c(java.io.File):java.lang.String");
    }

    public static void a(File file, String str) {
        if (file == null) {
            da.a(4, a, "No persistent file specified.");
        } else if (str == null) {
            String str2 = a;
            da.a(4, str2, "No data specified; deleting persistent file: " + file.getAbsolutePath());
            file.delete();
        } else {
            String str3 = a;
            da.a(4, str3, "Writing persistent data: " + file.getAbsolutePath());
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                try {
                    fileOutputStream2.write(str.getBytes());
                    ea.a(fileOutputStream2);
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream = fileOutputStream2;
                    try {
                        da.a(6, a, "Error writing persistent file", th);
                    } finally {
                        ea.a(fileOutputStream);
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                da.a(6, a, "Error writing persistent file", th);
            }
        }
    }

    public static String[] a(File file, final Pattern pattern) {
        if (!file.exists()) {
            return new String[0];
        }
        String[] list = file.list(new FilenameFilter() {
            public final boolean accept(File file, String str) {
                return pattern.matcher(str).matches();
            }
        });
        return list == null ? new String[0] : list;
    }
}
