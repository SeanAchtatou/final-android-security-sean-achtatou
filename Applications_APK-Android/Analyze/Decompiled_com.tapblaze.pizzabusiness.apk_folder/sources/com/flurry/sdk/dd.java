package com.flurry.sdk;

import android.content.Context;
import android.os.Build;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class dd {
    private static List<de> a = new ArrayList();
    private static final Map<Class<? extends de>, db> b = new LinkedHashMap();
    private static List<db> c;
    private static final Map<Class<? extends de>, de> d = new LinkedHashMap();
    private static List<String> e;

    static {
        ArrayList arrayList = new ArrayList();
        e = arrayList;
        arrayList.add("com.flurry.android.marketing.core.FlurryMarketingCoreModule");
        e.add("com.flurry.android.marketing.FlurryMarketingModule");
        e.add("com.flurry.android.config.killswitch.FlurryKillSwitchModule");
        e.add("com.flurry.android.nativecrash.FlurryNativeCrashModule");
        e.add("com.flurry.android.nativecrash.internal.FlurryNativeCrashModuleStreamingImpl");
        e.add("com.flurry.android.FlurryAdModule");
        e.add("com.flurry.android.ymadlite.YahooAdModule");
    }

    public static void a(Class<? extends de> cls) {
        da.a(3, "FlurryModuleManager", "Register Ads ".concat(String.valueOf(cls)));
        if (cls != null) {
            synchronized (b) {
                b.put(cls, new db(cls));
            }
        }
    }

    public static void a(de deVar) {
        da.a(3, "FlurryModuleManager", "Register Add-On ".concat(String.valueOf(deVar)));
        if (deVar != null) {
            boolean z = false;
            Iterator<de> it = a.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().getClass().getSimpleName().equals(deVar.getClass().getSimpleName())) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!z) {
                a.add(deVar);
                return;
            }
            da.a(3, "FlurryModuleManager", deVar + " has been register already as addOn module");
            return;
        }
        da.a(5, "FlurryModuleManager", "Module is null, cannot register it");
    }

    public static void a(Context context) {
        da.a(3, "FlurryModuleManager", "Init Ads");
        synchronized (b) {
            c = new ArrayList(b.values());
        }
        for (db next : c) {
            da.a(5, "FlurryModuleManager", "registration ".concat(String.valueOf(next)));
            try {
                if (next.a != null && Build.VERSION.SDK_INT >= next.b) {
                    de deVar = (de) next.a.newInstance();
                    deVar.init(context);
                    d.put(next.a, deVar);
                }
            } catch (Exception e2) {
                da.a(5, "FlurryModuleManager", "Flurry Module for class " + next.a + " is not available:", e2);
            }
        }
    }

    public static void b(Context context) {
        da.a(2, "FlurryModuleManager", "Init Add on modules");
        synchronized (d) {
            for (de next : a) {
                try {
                    da.a(2, "FlurryModuleManager", "Module list: ".concat(String.valueOf(next)));
                    if (d.containsKey(next.getClass())) {
                        da.a(5, "FlurryModuleManager", next.getClass() + " has been initialized");
                    } else {
                        next.init(context);
                        d.put(next.getClass(), next);
                        da.a(3, "FlurryModuleManager", "Initialized modules: " + next.getClass());
                    }
                } catch (dc e2) {
                    da.b("FlurryModuleManager", e2.getMessage());
                }
            }
        }
    }

    public static void a() {
        da.a(3, "FlurryModuleManager", "Unregister Ads");
        synchronized (b) {
            b.clear();
        }
    }

    public static void b() {
        da.a(3, "FlurryModuleManager", "Unregister Add On");
        synchronized (a) {
            a.clear();
        }
    }

    public static synchronized void c() {
        synchronized (dd.class) {
            da.a(3, "FlurryModuleManager", "Destroy Streaming");
            List<de> d2 = d();
            for (int size = d2.size() - 1; size >= 0; size--) {
                try {
                    d.remove(d2.get(size).getClass()).destroy();
                } catch (Exception e2) {
                    da.a(5, "FlurryModuleManager", "Error destroying module:", e2);
                }
            }
        }
    }

    private static List<de> d() {
        ArrayList arrayList;
        da.a(3, "FlurryModuleManager", "Get Streaming module list");
        synchronized (d) {
            arrayList = new ArrayList(d.values());
        }
        return arrayList;
    }

    public static boolean a(String str) {
        return e.contains(str);
    }
}
