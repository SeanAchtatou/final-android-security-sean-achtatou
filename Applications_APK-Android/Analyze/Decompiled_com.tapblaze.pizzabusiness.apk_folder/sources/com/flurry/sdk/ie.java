package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class ie extends jl {
    public final int a;

    public ie(int i) {
        this.a = i < 0 ? 0 : i;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.app.orientation", this.a);
        return jSONObject;
    }
}
