package com.flurry.sdk;

import com.flurry.sdk.ey;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Future;

public class m<T> extends f {
    protected Set<o<T>> a;

    public void a() {
    }

    public void b() {
    }

    protected m(String str) {
        super(str, ey.a(ey.a.PROVIDER));
        this.a = null;
        this.a = new HashSet();
    }

    public void a(final o oVar) {
        if (oVar != null) {
            b(new ec() {
                public final void a() {
                    m.this.a.add(oVar);
                }
            });
        }
    }

    public final void b(final o oVar) {
        b(new ec() {
            public final void a() {
                m.this.a.remove(oVar);
            }
        });
    }

    public void c() {
        b(new ec() {
            public final void a() {
                m.this.a.clear();
            }
        });
    }

    public final void a(final Object obj) {
        b(new ec() {
            public final void a() {
                for (final o next : m.this.a) {
                    Future unused = m.this.b(new ec() {
                        public final void a() {
                            next.a(obj);
                        }
                    });
                }
            }
        });
    }
}
