package com.flurry.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.TimeZone;

public final class bj extends m<String> {
    protected BroadcastReceiver b = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            bj.this.a(TimeZone.getDefault().getID());
        }
    };

    public bj() {
        super("TimeZoneProvider");
        Context a = b.a();
        IntentFilter intentFilter = new IntentFilter("android.intent.action.TIMEZONE_CHANGED");
        if (a != null) {
            a.registerReceiver(this.b, intentFilter);
        } else {
            da.a(6, "TimeZoneProvider", "Context is null when initializing.");
        }
    }

    public final void a(final o<String> oVar) {
        super.a((o) oVar);
        b(new ec() {
            public final void a() throws Exception {
                oVar.a(TimeZone.getDefault().getID());
            }
        });
    }

    public final void c() {
        super.c();
        b.a().unregisterReceiver(this.b);
    }
}
