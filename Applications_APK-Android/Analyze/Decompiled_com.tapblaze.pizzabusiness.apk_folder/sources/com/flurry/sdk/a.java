package com.flurry.sdk;

import com.flurry.android.Consent;
import com.flurry.android.FlurryEventRecordStatus;
import com.flurry.android.FlurryModule;
import com.flurry.sdk.ey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import okhttp3.internal.http.StatusLine;

public final class a extends f {
    public static AtomicBoolean b = new AtomicBoolean(false);
    private static a h = null;
    public List<FlurryModule> a = new ArrayList();

    public a() {
        super("FlurryAgentImpl", ey.a(ey.a.PUBLIC_API));
    }

    public static a a() {
        if (h == null) {
            h = new a();
        }
        return h;
    }

    public static int b() {
        bn.a();
        return StatusLine.HTTP_PERM_REDIRECT;
    }

    public static String c() {
        return bn.a().b;
    }

    public static boolean i() {
        return b.get();
    }

    public final void a(final String str, final String str2, final Map<String, String> map) {
        if (!b.get()) {
            da.d("FlurryAgentImpl", "Invalid call to addOrigin. Flurry is not initialized");
        } else {
            b(new ec() {
                public final void a() {
                    az azVar = n.a().o;
                    String str = str;
                    String str2 = str2;
                    Map map = map;
                    if (map == null) {
                        map = new HashMap();
                    }
                    map.put("fl.origin.attribute.version", str2);
                    azVar.b.put(str, map);
                    azVar.a(new ay(str, str2, map));
                }
            });
        }
    }

    public static Consent d() {
        if (b.get()) {
            return n.a().l.b;
        }
        da.d("FlurryAgentImpl", "Invalid call to getFlurryConsent. Flurry is not initialized");
        return null;
    }

    public static String e() {
        if (b.get()) {
            return n.a().m.d();
        }
        da.d("FlurryAgentImpl", "Invalid call to getInstantAppName. Flurry is not initialized");
        return null;
    }

    public static boolean f() {
        if (b.get()) {
            return n.a().k.i.get();
        }
        da.d("FlurryAgentImpl", "Invalid call to isSessionActive. Flurry is not initialized");
        return false;
    }

    public static String g() {
        if (b.get()) {
            return n.a().k.d();
        }
        da.d("FlurryAgentImpl", "Invalid call to getSessionId. Flurry is not initialized");
        return null;
    }

    public final void a(String str, String str2, String str3, Map<String, String> map, StackTraceElement[] stackTraceElementArr) {
        if (!b.get()) {
            da.d("FlurryAgentImpl", "Invalid call to onError. Flurry is not initialized");
            return;
        }
        final Throwable th = new Throwable(str2);
        th.setStackTrace(stackTraceElementArr);
        final long currentTimeMillis = System.currentTimeMillis();
        final HashMap hashMap = new HashMap();
        if (map != null) {
            hashMap.putAll(map);
        }
        final String str4 = str;
        final String str5 = str2;
        final String str6 = str3;
        b(new ec() {
            public final void a() {
                n.a().f.a(str4, currentTimeMillis, str5, str6, th, null, hashMap);
            }
        });
    }

    public static void h() {
        if (b.get()) {
            dd.c();
            ek.b();
            hu.b();
            n.b();
            fc.b();
            b.set(false);
        }
    }

    public final FlurryEventRecordStatus a(String str, Map<String, String> map, boolean z, boolean z2, long j, long j2) {
        FlurryEventRecordStatus flurryEventRecordStatus;
        Map<String, String> map2 = map;
        if (!b.get()) {
            da.d("FlurryAgentImpl", "Invalid call to logEvent. Flurry is not initialized");
            return null;
        } else if (ea.a(str).length() == 0) {
            return FlurryEventRecordStatus.kFlurryEventFailed;
        } else {
            final HashMap hashMap = new HashMap();
            if (map2 != null) {
                hashMap.putAll(map);
            }
            if (hashMap.size() > 10) {
                flurryEventRecordStatus = FlurryEventRecordStatus.kFlurryEventParamsCountExceeded;
            } else {
                flurryEventRecordStatus = FlurryEventRecordStatus.kFlurryEventRecorded;
            }
            final String str2 = str;
            final boolean z3 = z;
            final boolean z4 = z2;
            final long j3 = j;
            final long j4 = j2;
            b(new ec() {
                public final void a() {
                    gm.a(str2, hashMap, z3, z4, j3, j4);
                }
            });
            return flurryEventRecordStatus;
        }
    }
}
