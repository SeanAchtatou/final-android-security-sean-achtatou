package com.flurry.sdk;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public final class ds<T> implements dt<List<T>> {
    dt<T> a;

    public final /* synthetic */ void a(OutputStream outputStream, Object obj) throws IOException {
        List list = (List) obj;
        if (outputStream != null) {
            AnonymousClass1 r0 = new DataOutputStream(outputStream) {
                public final void close() {
                }
            };
            int size = list != null ? list.size() : 0;
            r0.writeInt(size);
            for (int i = 0; i < size; i++) {
                this.a.a(outputStream, list.get(i));
            }
            r0.flush();
        }
    }

    public ds(dt<T> dtVar) {
        this.a = dtVar;
    }

    public final /* synthetic */ Object a(InputStream inputStream) throws IOException {
        if (inputStream == null) {
            return null;
        }
        int readInt = new DataInputStream(inputStream) {
            public final void close() {
            }
        }.readInt();
        ArrayList arrayList = new ArrayList(readInt);
        int i = 0;
        while (i < readInt) {
            T a2 = this.a.a(inputStream);
            if (a2 != null) {
                arrayList.add(a2);
                i++;
            } else {
                throw new IOException("Missing record.");
            }
        }
        return arrayList;
    }
}
