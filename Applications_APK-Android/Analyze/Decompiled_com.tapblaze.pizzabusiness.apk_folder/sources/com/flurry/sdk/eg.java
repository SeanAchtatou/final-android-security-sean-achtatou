package com.flurry.sdk;

import com.flurry.android.Consent;
import java.util.Map;

public abstract class eg extends Consent {
    private boolean a;

    public eg(boolean z, boolean z2, Map<String, String> map) {
        this.a = z;
        this.isGdprScope = z2;
        this.consentStrings = map;
    }

    public boolean isLICNEnabled() {
        return this.a;
    }

    public boolean equals(Object obj) {
        return super.equals(obj) && this.a == ((eg) obj).isLICNEnabled();
    }

    public int hashCode() {
        return (super.hashCode() * 31) + (this.a ? 1 : 0);
    }
}
