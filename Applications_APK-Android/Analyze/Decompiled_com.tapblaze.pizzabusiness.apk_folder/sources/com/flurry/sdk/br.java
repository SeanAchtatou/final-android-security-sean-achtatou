package com.flurry.sdk;

import com.flurry.sdk.bs;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public final class br {
    public static final Integer a = 50;
    String b;
    LinkedHashMap<String, List<String>> c;

    public br(String str) {
        this.b = str + "Main";
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public final synchronized List<String> a(String str) {
        DataInputStream dataInputStream;
        ea.a();
        da.a(5, "FlurryDataSenderIndex", "Reading Index File for " + str + " file name:" + b.a().getFileStreamPath(".FlurrySenderIndex.info.".concat(String.valueOf(str))));
        File fileStreamPath = b.a().getFileStreamPath(".FlurrySenderIndex.info.".concat(String.valueOf(str)));
        ArrayList arrayList = null;
        if (fileStreamPath.exists()) {
            da.a(5, "FlurryDataSenderIndex", "Reading Index File for " + str + " Found file.");
            try {
                dataInputStream = new DataInputStream(new FileInputStream(fileStreamPath));
                try {
                    int readUnsignedShort = dataInputStream.readUnsignedShort();
                    if (readUnsignedShort == 0) {
                        ea.a(dataInputStream);
                        return null;
                    }
                    ArrayList arrayList2 = new ArrayList(readUnsignedShort);
                    int i = 0;
                    while (i < readUnsignedShort) {
                        try {
                            int readUnsignedShort2 = dataInputStream.readUnsignedShort();
                            da.a(4, "FlurryDataSenderIndex", "read iter " + i + " dataLength = " + readUnsignedShort2);
                            byte[] bArr = new byte[readUnsignedShort2];
                            dataInputStream.readFully(bArr);
                            arrayList2.add(new String(bArr));
                            i++;
                        } catch (Throwable th) {
                            th = th;
                            arrayList = arrayList2;
                            try {
                                da.a(6, "FlurryDataSenderIndex", "Error when loading persistent file", th);
                                ea.a(dataInputStream);
                                return arrayList;
                            } catch (Throwable th2) {
                                ea.a(dataInputStream);
                                throw th2;
                            }
                        }
                    }
                    dataInputStream.readUnsignedShort();
                    ea.a(dataInputStream);
                    arrayList = arrayList2;
                } catch (Throwable th3) {
                    th = th3;
                    da.a(6, "FlurryDataSenderIndex", "Error when loading persistent file", th);
                    ea.a(dataInputStream);
                    return arrayList;
                }
            } catch (Throwable th4) {
                th = th4;
                dataInputStream = null;
                da.a(6, "FlurryDataSenderIndex", "Error when loading persistent file", th);
                ea.a(dataInputStream);
                return arrayList;
            }
        } else {
            da.a(5, "FlurryDataSenderIndex", "Agent cache file doesn't exist.");
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        List<String> a2 = a(str);
        if (a2 == null) {
            da.c("FlurryDataSenderIndex", "No old file to replace");
            return;
        }
        for (String next : a2) {
            byte[] g = g(next);
            if (g == null) {
                da.a(6, "FlurryDataSenderIndex", "File does not exist");
            } else {
                ea.a();
                da.a(5, "FlurryDataSenderIndex", "Saving Block File for " + next + " file name:" + b.a().getFileStreamPath(bq.a(next)));
                bq.b(next).a(new bq(g));
                ea.a();
                da.a(5, "FlurryDataSenderIndex", "Deleting  block File for " + next + " file name:" + b.a().getFileStreamPath(".flurrydatasenderblock.".concat(String.valueOf(next))));
                File fileStreamPath = b.a().getFileStreamPath(".flurrydatasenderblock.".concat(String.valueOf(next)));
                if (fileStreamPath.exists()) {
                    boolean delete = fileStreamPath.delete();
                    da.a(5, "FlurryDataSenderIndex", "Found file for " + next + ". Deleted - " + delete);
                }
            }
        }
        a(str, a2, ".YFlurrySenderIndex.info.");
        c(str);
    }

    static void c(String str) {
        ea.a();
        da.a(5, "FlurryDataSenderIndex", "Deleting Index File for " + str + " file name:" + b.a().getFileStreamPath(".FlurrySenderIndex.info.".concat(String.valueOf(str))));
        File fileStreamPath = b.a().getFileStreamPath(".FlurrySenderIndex.info.".concat(String.valueOf(str)));
        if (fileStreamPath.exists()) {
            boolean delete = fileStreamPath.delete();
            da.a(5, "FlurryDataSenderIndex", "Found file for " + str + ". Deleted - " + delete);
        }
    }

    static String d(String str) {
        return ".YFlurrySenderIndex.info.".concat(String.valueOf(str));
    }

    /* access modifiers changed from: package-private */
    public final synchronized List<String> e(String str) {
        ea.a();
        da.a(5, "FlurryDataSenderIndex", "Reading Index File for " + str + " file name:" + b.a().getFileStreamPath(d(str)));
        List<bs> list = (List) new l(b.a().getFileStreamPath(d(str)), ".YFlurrySenderIndex.info.", 1, new dw<List<bs>>() {
            public final dt<List<bs>> a(int i) {
                return new ds(new bs.a());
            }
        }).a();
        if (list == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (bs bsVar : list) {
            arrayList.add(bsVar.a);
        }
        return arrayList;
    }

    private static byte[] g(String str) {
        DataInputStream dataInputStream;
        ea.a();
        da.a(5, "FlurryDataSenderIndex", "Reading block File for " + str + " file name:" + b.a().getFileStreamPath(".flurrydatasenderblock.".concat(String.valueOf(str))));
        File fileStreamPath = b.a().getFileStreamPath(".flurrydatasenderblock.".concat(String.valueOf(str)));
        byte[] bArr = null;
        if (fileStreamPath.exists()) {
            da.a(5, "FlurryDataSenderIndex", "Reading Index File for " + str + " Found file.");
            try {
                dataInputStream = new DataInputStream(new FileInputStream(fileStreamPath));
                try {
                    int readUnsignedShort = dataInputStream.readUnsignedShort();
                    if (readUnsignedShort == 0) {
                        ea.a(dataInputStream);
                        return null;
                    }
                    bArr = new byte[readUnsignedShort];
                    dataInputStream.readFully(bArr);
                    dataInputStream.readUnsignedShort();
                    ea.a(dataInputStream);
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                dataInputStream = null;
                try {
                    da.a(6, "FlurryDataSenderIndex", "Error when loading persistent file", th);
                    ea.a(dataInputStream);
                    return bArr;
                } catch (Throwable th3) {
                    ea.a(dataInputStream);
                    throw th3;
                }
            }
        } else {
            da.a(4, "FlurryDataSenderIndex", "Agent cache file doesn't exist.");
        }
        return bArr;
    }

    private synchronized void a(String str, List<String> list, String str2) {
        ea.a();
        da.a(5, "FlurryDataSenderIndex", "Saving Index File for " + str + " file name:" + b.a().getFileStreamPath(d(str)));
        l lVar = new l(b.a().getFileStreamPath(d(str)), str2, 1, new dw<List<bs>>() {
            public final dt<List<bs>> a(int i) {
                return new ds(new bs.a());
            }
        });
        ArrayList arrayList = new ArrayList();
        for (String bsVar : list) {
            arrayList.add(new bs(bsVar));
        }
        lVar.a(arrayList);
    }

    public final synchronized void a(bq bqVar, String str) {
        boolean z;
        da.a(4, "FlurryDataSenderIndex", "addBlockInfo".concat(String.valueOf(str)));
        String str2 = bqVar.a;
        List list = this.c.get(str);
        if (list == null) {
            da.a(4, "FlurryDataSenderIndex", "New Data Key");
            list = new LinkedList();
            z = true;
        } else {
            z = false;
        }
        list.add(str2);
        if (list.size() > a.intValue()) {
            bq.b((String) list.get(0)).b();
            list.remove(0);
        }
        this.c.put(str, list);
        a(str, list, ".YFlurrySenderIndex.info.");
        if (z) {
            a();
        }
    }

    private synchronized void a() {
        LinkedList linkedList = new LinkedList(this.c.keySet());
        new l(b.a().getFileStreamPath(d(this.b)), ".YFlurrySenderIndex.info.", 1, new dw<List<bs>>() {
            public final dt<List<bs>> a(int i) {
                return new ds(new bs.a());
            }
        }).b();
        if (!linkedList.isEmpty()) {
            a(this.b, linkedList, this.b);
        }
    }

    public final boolean a(String str, String str2) {
        boolean z;
        List list = this.c.get(str2);
        if (list != null) {
            bq.b(str).b();
            z = list.remove(str);
        } else {
            z = false;
        }
        if (list == null || list.isEmpty()) {
            h(str2);
        } else {
            this.c.put(str2, list);
            a(str2, list, ".YFlurrySenderIndex.info.");
        }
        return z;
    }

    private synchronized boolean h(String str) {
        boolean b2;
        ea.a();
        l lVar = new l(b.a().getFileStreamPath(d(str)), ".YFlurrySenderIndex.info.", 1, new dw<List<bs>>() {
            public final dt<List<bs>> a(int i) {
                return new ds(new bs.a());
            }
        });
        List<String> f = f(str);
        if (f != null && !f.isEmpty()) {
            da.a(4, "FlurryDataSenderIndex", "discardOutdatedBlocksForDataKey: notSentBlocks = " + f.size());
            for (String next : f) {
                bq.b(next).b();
                da.a(4, "FlurryDataSenderIndex", "discardOutdatedBlocksForDataKey: removed block = ".concat(String.valueOf(next)));
            }
        }
        this.c.remove(str);
        b2 = lVar.b();
        a();
        return b2;
    }

    public final List<String> f(String str) {
        List list = this.c.get(str);
        if (list == null) {
            return Collections.emptyList();
        }
        return new ArrayList(list);
    }
}
