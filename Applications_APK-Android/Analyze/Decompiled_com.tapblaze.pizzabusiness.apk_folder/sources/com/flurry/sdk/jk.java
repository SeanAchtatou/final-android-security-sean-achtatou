package com.flurry.sdk;

import com.flurry.sdk.ac;
import okhttp3.internal.http.StatusLine;
import org.json.JSONException;
import org.json.JSONObject;

public final class jk extends jl {
    public final int a = 3;
    public final int b = StatusLine.HTTP_PERM_REDIRECT;
    public final String c;
    public final int d;
    public final boolean e;
    public final ac.a f;

    public jk(String str, int i, boolean z, ac.a aVar) {
        this.c = str;
        this.d = i;
        this.e = z;
        this.f = aVar;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.agent.version", this.b);
        jSONObject.put("fl.agent.platform", this.a);
        jSONObject.put("fl.apikey", this.c);
        jSONObject.put("fl.agent.report.key", this.d);
        jSONObject.put("fl.background.session.metrics", this.e);
        jSONObject.put("fl.play.service.availability", this.f.i);
        return jSONObject;
    }
}
