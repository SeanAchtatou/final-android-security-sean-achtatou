package com.flurry.sdk;

public final class fp {
    protected fk a = new fk(this.c);
    protected fr b = new fr(this.e);
    protected fq c = new fq(this.d);
    protected fl d = new fl(this.b);
    protected fm e = new fm();

    public final void a() {
        this.a.a((fo) null);
    }

    public final void b() {
        this.a.b();
        this.a = null;
        this.c = null;
        this.b = null;
        this.d = null;
        this.e = null;
    }

    public final void a(jp jpVar) {
        fk fkVar = this.a;
        if (fkVar != null) {
            fkVar.c(jpVar);
        }
    }

    public final void b(jp jpVar) {
        fk fkVar = this.a;
        if (fkVar != null) {
            fkVar.b(jpVar);
        }
    }
}
