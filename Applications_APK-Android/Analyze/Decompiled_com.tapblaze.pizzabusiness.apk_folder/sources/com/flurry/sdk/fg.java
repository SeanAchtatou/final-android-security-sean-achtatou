package com.flurry.sdk;

import android.text.TextUtils;
import java.io.File;
import java.util.Locale;

public final class fg {
    public static boolean a(String str) {
        File file = new File(str);
        if (file.exists()) {
            return file.delete();
        }
        return true;
    }

    public static boolean b(String str) {
        if (!TextUtils.isEmpty(str) && new File(str).exists()) {
            return true;
        }
        return false;
    }

    public static boolean a() {
        File file = new File(e());
        if (!file.exists()) {
            return file.mkdirs();
        }
        return true;
    }

    public static String b() {
        return e() + File.separator + "fCompleted";
    }

    public static String c() {
        return String.format(Locale.US, "completed-%d", Long.valueOf(System.currentTimeMillis()));
    }

    public static String d() {
        return e() + File.separator + "fInProgress";
    }

    private static String e() {
        return dz.a().toString() + File.separator + ".fstreaming";
    }
}
