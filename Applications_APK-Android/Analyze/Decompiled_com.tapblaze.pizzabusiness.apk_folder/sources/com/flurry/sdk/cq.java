package com.flurry.sdk;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.applovin.sdk.AppLovinEventTypes;
import com.flurry.android.FlurryAgent;
import com.google.firebase.remoteconfig.RemoteConfigConstants;
import com.ironsource.sdk.constants.Constants;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class cq {
    public static String a(String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("timestamp", System.currentTimeMillis() / 1000);
            jSONObject.put("guid", str);
            JSONArray jSONArray = new JSONArray();
            jSONArray.put("APP");
            try {
                Class.forName("com.flurry.android.config.killswitch.KillSwitch");
                jSONArray.put("KILLSWITCH");
            } catch (ClassNotFoundException unused) {
            }
            jSONObject.put("documents", jSONArray);
            JSONArray jSONArray2 = new JSONArray();
            jSONArray2.put(ct.a() ? "com.flurry.configkey.prod.ec.2" : "com.flurry.configkey.prod.rot.7");
            jSONArray2.put("com.flurry.configkey.prod.fs.0");
            jSONObject.put("signatureKeys", jSONArray2);
            bn a = bn.a();
            Context a2 = b.a();
            cb a3 = cb.a();
            cc ccVar = a3.a;
            if (ct.a(ccVar.d())) {
                String str2 = null;
                if (ccVar.a != null) {
                    str2 = ccVar.a.getString("lastETag", null);
                }
                if (str2 != null) {
                    jSONObject.put("etag", str2);
                }
            }
            jSONObject.put("apiKey", n.a().h.b);
            jSONObject.put("appVersion", a.b());
            jSONObject.put("appBuild", Integer.toString(bn.b(a2)));
            jSONObject.put("sdkVersion", FlurryAgent.getAgentVersion());
            jSONObject.put("platform", 3);
            jSONObject.put(RemoteConfigConstants.RequestFieldKey.PLATFORM_VERSION, Build.VERSION.RELEASE);
            jSONObject.put(Constants.RequestParameters.DEVICE_IDS, cf.a());
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("brand", Build.BRAND);
            jSONObject2.put(Constants.ParametersKeys.ORIENTATION_DEVICE, Build.DEVICE);
            jSONObject2.put("id", Build.ID);
            jSONObject2.put("model", Build.MODEL);
            jSONObject2.put(AppLovinEventTypes.USER_VIEWED_PRODUCT, Build.PRODUCT);
            jSONObject2.put("version_release", Build.VERSION.RELEASE);
            jSONObject.put("deviceTags", jSONObject2);
            jSONObject.put(Constants.RequestParameters.PACKAGE_NAME, dy.a(a2));
            n.a();
            jSONObject.put("locale", ar.d());
            String str3 = n.a().h.h;
            if (!TextUtils.isEmpty(str3)) {
                jSONObject.put("publisherUserId", str3);
            }
            List<cl> e = a3.e();
            if (e != null && e.size() > 0) {
                JSONArray jSONArray3 = new JSONArray();
                for (cl next : e) {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", next.b);
                    jSONObject3.put("version", next.c);
                    jSONArray3.put(jSONObject3);
                }
                jSONObject.put("currentVariants", jSONArray3);
            }
        } catch (JSONException e2) {
            da.a("ParameterProvider", "ParameterProvider error", e2);
        }
        String jSONObject4 = jSONObject.toString();
        da.a("ParameterProvider", "Request Parameters: ".concat(String.valueOf(jSONObject4)));
        return jSONObject4;
    }
}
