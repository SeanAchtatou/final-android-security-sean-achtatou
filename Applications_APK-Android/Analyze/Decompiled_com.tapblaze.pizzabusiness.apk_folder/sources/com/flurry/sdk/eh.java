package com.flurry.sdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.browser.customtabs.CustomTabsIntent;
import com.facebook.AccessToken;
import com.facebook.appevents.codeless.internal.Constants;
import com.flurry.android.FlurryPrivacySession;
import com.flurry.sdk.df;
import com.flurry.sdk.dh;
import com.flurry.sdk.ee;
import com.flurry.sdk.ey;
import com.google.android.gms.drive.DriveFile;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class eh extends f {
    private static eh a = new eh();
    /* access modifiers changed from: private */
    public FlurryPrivacySession.Request b;
    /* access modifiers changed from: private */
    public final o<ak> h = new o<ak>() {
        public final /* synthetic */ void a(Object obj) {
            n.a().g.b(eh.this.h);
            eh.this.b(new ec() {
                public final void a() throws Exception {
                    Map b = eh.b(eh.this.b);
                    df dfVar = new df();
                    dfVar.f = "https://api.login.yahoo.com/oauth2/device_session";
                    dfVar.g = dh.a.kPost;
                    dfVar.a("Content-Type", "application/json");
                    dfVar.b = new JSONObject(b).toString();
                    dfVar.d = new du();
                    dfVar.c = new du();
                    dfVar.a = new df.a<String, String>() {
                        public final /* synthetic */ void a(df dfVar, Object obj) {
                            String str = (String) obj;
                            try {
                                int i = dfVar.m;
                                if (i == 200) {
                                    JSONObject jSONObject = new JSONObject(str);
                                    eh.a(eh.this, new FlurryPrivacySession.a(jSONObject.getString(Constants.DEVICE_SESSION_ID), jSONObject.getLong(AccessToken.EXPIRES_IN_KEY), eh.this.b));
                                    eh.this.b.callback.success();
                                    return;
                                }
                                da.e("PrivacyManager", "Error in getting privacy dashboard url. Error code = ".concat(String.valueOf(i)));
                                eh.this.b.callback.failure();
                            } catch (JSONException e) {
                                da.b("PrivacyManager", "Error in getting privacy dashboard url. ", e);
                                eh.this.b.callback.failure();
                            }
                        }
                    };
                    cv.a().a(eh.this, dfVar);
                }
            });
        }
    };

    private eh() {
        super("PrivacyManager", ey.a(ey.a.MISC));
    }

    /* access modifiers changed from: private */
    public static void b(Context context, FlurryPrivacySession.a aVar) {
        Intent intent = new Intent("android.intent.action.VIEW", aVar.a);
        intent.setFlags(DriveFile.MODE_READ_ONLY);
        context.startActivity(intent);
    }

    public static void a(FlurryPrivacySession.Request request) {
        eh ehVar = a;
        ehVar.b = request;
        ehVar.b(new ec() {
            public final void a() throws Exception {
                if (n.a().g.f()) {
                    eh.this.b(new ec() {
                        public final void a() throws Exception {
                            Map b = eh.b(eh.this.b);
                            df dfVar = new df();
                            dfVar.f = "https://api.login.yahoo.com/oauth2/device_session";
                            dfVar.g = dh.a.kPost;
                            dfVar.a("Content-Type", "application/json");
                            dfVar.b = new JSONObject(b).toString();
                            dfVar.d = new du();
                            dfVar.c = new du();
                            dfVar.a = new df.a<String, String>() {
                                public final /* synthetic */ void a(df dfVar, Object obj) {
                                    String str = (String) obj;
                                    try {
                                        int i = dfVar.m;
                                        if (i == 200) {
                                            JSONObject jSONObject = new JSONObject(str);
                                            eh.a(eh.this, new FlurryPrivacySession.a(jSONObject.getString(Constants.DEVICE_SESSION_ID), jSONObject.getLong(AccessToken.EXPIRES_IN_KEY), eh.this.b));
                                            eh.this.b.callback.success();
                                            return;
                                        }
                                        da.e("PrivacyManager", "Error in getting privacy dashboard url. Error code = ".concat(String.valueOf(i)));
                                        eh.this.b.callback.failure();
                                    } catch (JSONException e) {
                                        da.b("PrivacyManager", "Error in getting privacy dashboard url. ", e);
                                        eh.this.b.callback.failure();
                                    }
                                }
                            };
                            cv.a().a(eh.this, dfVar);
                        }
                    });
                    return;
                }
                da.a(3, "PrivacyManager", "Waiting for ID provider.");
                n.a().g.a((o<ak>) eh.this.h);
            }
        });
    }

    static /* synthetic */ Map b(FlurryPrivacySession.Request request) {
        HashMap hashMap = new HashMap();
        hashMap.put("device_verifier", request.verifier);
        HashMap hashMap2 = new HashMap();
        ak d = n.a().g.d();
        String str = d.a().get(al.AndroidAdvertisingId);
        if (str != null) {
            hashMap2.put("gpaid", str);
        }
        String str2 = d.a().get(al.DeviceId);
        if (str2 != null) {
            hashMap2.put("andid", str2);
        }
        hashMap.putAll(hashMap2);
        HashMap hashMap3 = new HashMap();
        byte[] bytes = n.a().g.d().a().get(al.AndroidInstallationId).getBytes();
        if (bytes != null) {
            hashMap3.put("flurry_guid", ea.a(bytes));
        }
        hashMap3.put("flurry_project_api_key", n.a().h.b);
        hashMap.putAll(hashMap3);
        Context context = request.context;
        HashMap hashMap4 = new HashMap();
        hashMap4.put("src", "flurryandroidsdk");
        hashMap4.put("srcv", "12.1.0");
        hashMap4.put("appsrc", context.getPackageName());
        bn.a();
        hashMap4.put("appsrcv", bn.a(context));
        hashMap.putAll(hashMap4);
        return hashMap;
    }

    static /* synthetic */ void a(eh ehVar, final FlurryPrivacySession.a aVar) {
        Context a2 = b.a();
        if (ee.a(a2)) {
            ee.a(a2, new CustomTabsIntent.Builder().setShowTitle(true).build(), Uri.parse(aVar.a.toString()), new ee.a() {
                public final void a(Context context) {
                    eh.b(context, aVar);
                }
            });
        } else {
            b(a2, aVar);
        }
    }
}
