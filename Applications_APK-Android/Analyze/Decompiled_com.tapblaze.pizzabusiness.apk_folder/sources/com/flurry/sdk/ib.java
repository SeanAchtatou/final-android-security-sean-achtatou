package com.flurry.sdk;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;

public final class ib extends jm {
    private ib(jo joVar) {
        super(joVar);
    }

    public final jn a() {
        return jn.APP_INFO;
    }

    public static void b() {
        String b = bn.a().b();
        String str = bn.a().a;
        if (TextUtils.isEmpty(str)) {
            str = "";
        }
        Context a = b.a();
        int i = 0;
        try {
            i = a.getPackageManager().getPackageInfo(a.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        fc.a().a(new ib(new ic(b, str, String.valueOf(i), dy.a(b.a()))));
    }
}
