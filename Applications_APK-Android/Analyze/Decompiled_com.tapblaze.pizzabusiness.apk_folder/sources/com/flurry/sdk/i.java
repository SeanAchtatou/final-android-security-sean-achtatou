package com.flurry.sdk;

import com.flurry.sdk.h;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;

public class i extends h {
    private final Deque<h.a> a = new LinkedList();
    private h.a b;

    /* access modifiers changed from: protected */
    public boolean c(Runnable runnable) {
        return false;
    }

    i(String str, h hVar, boolean z) {
        super(str, hVar, z);
    }

    /* access modifiers changed from: protected */
    public void a(Runnable runnable) throws CancellationException {
        h.a aVar = new h.a(this, c);
        synchronized (this) {
            this.a.add(aVar);
            a();
        }
        if (this.g) {
            for (h hVar = this.e; hVar != null; hVar = hVar.e) {
                hVar.d(aVar);
            }
        }
        while (!aVar.isDone()) {
            try {
                aVar.get();
            } catch (CancellationException e) {
                throw e;
            } catch (Exception unused) {
            }
        }
        if (!c(runnable)) {
            e(runnable);
        }
        f(aVar);
    }

    /* access modifiers changed from: protected */
    public Future<Void> b(Runnable runnable) {
        h.a aVar;
        if (runnable instanceof h.a) {
            aVar = (h.a) runnable;
        } else {
            aVar = new h.a(this, this, runnable) {
                {
                    r2.getClass();
                }

                /* access modifiers changed from: protected */
                public final void done() {
                    this.a.f(this);
                }
            };
        }
        synchronized (this) {
            this.a.add(aVar);
            a();
        }
        return aVar;
    }

    /* access modifiers changed from: protected */
    public boolean a(h.a aVar) {
        if (this.e == null) {
            return true;
        }
        this.e.b(aVar);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void f(Runnable runnable) {
        synchronized (this) {
            if (this.b == runnable) {
                this.b = null;
            }
        }
        a();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0057, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void a() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.f     // Catch:{ all -> 0x0058 }
            r1 = 0
            if (r0 == 0) goto L_0x002d
        L_0x0006:
            java.util.Deque<com.flurry.sdk.h$a> r0 = r3.a     // Catch:{ all -> 0x0058 }
            int r0 = r0.size()     // Catch:{ all -> 0x0058 }
            if (r0 <= 0) goto L_0x0056
            java.util.Deque<com.flurry.sdk.h$a> r0 = r3.a     // Catch:{ all -> 0x0058 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x0058 }
            com.flurry.sdk.h$a r0 = (com.flurry.sdk.h.a) r0     // Catch:{ all -> 0x0058 }
            boolean r2 = r0.isDone()     // Catch:{ all -> 0x0058 }
            if (r2 != 0) goto L_0x0006
            r3.b = r0     // Catch:{ all -> 0x0058 }
            boolean r2 = r3.a(r0)     // Catch:{ all -> 0x0058 }
            if (r2 != 0) goto L_0x0006
            r3.b = r1     // Catch:{ all -> 0x0058 }
            java.util.Deque<com.flurry.sdk.h$a> r1 = r3.a     // Catch:{ all -> 0x0058 }
            r1.addFirst(r0)     // Catch:{ all -> 0x0058 }
            monitor-exit(r3)
            return
        L_0x002d:
            com.flurry.sdk.h$a r0 = r3.b     // Catch:{ all -> 0x0058 }
            if (r0 != 0) goto L_0x0056
            java.util.Deque<com.flurry.sdk.h$a> r0 = r3.a     // Catch:{ all -> 0x0058 }
            int r0 = r0.size()     // Catch:{ all -> 0x0058 }
            if (r0 <= 0) goto L_0x0056
            java.util.Deque<com.flurry.sdk.h$a> r0 = r3.a     // Catch:{ all -> 0x0058 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x0058 }
            com.flurry.sdk.h$a r0 = (com.flurry.sdk.h.a) r0     // Catch:{ all -> 0x0058 }
            boolean r2 = r0.isDone()     // Catch:{ all -> 0x0058 }
            if (r2 != 0) goto L_0x0056
            r3.b = r0     // Catch:{ all -> 0x0058 }
            boolean r2 = r3.a(r0)     // Catch:{ all -> 0x0058 }
            if (r2 != 0) goto L_0x0056
            r3.b = r1     // Catch:{ all -> 0x0058 }
            java.util.Deque<com.flurry.sdk.h$a> r1 = r3.a     // Catch:{ all -> 0x0058 }
            r1.addFirst(r0)     // Catch:{ all -> 0x0058 }
        L_0x0056:
            monitor-exit(r3)
            return
        L_0x0058:
            r0 = move-exception
            monitor-exit(r3)
            goto L_0x005c
        L_0x005b:
            throw r0
        L_0x005c:
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.i.a():void");
    }
}
