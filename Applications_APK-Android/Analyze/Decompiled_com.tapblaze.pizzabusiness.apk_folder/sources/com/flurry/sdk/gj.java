package com.flurry.sdk;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class gj extends jl {
    public List<a> a;

    public static class a {
        public final long a;
        public final int b;

        public a(long j, int i) {
            this.a = j;
            this.b = i;
        }
    }

    public gj(List<a> list) {
        this.a = new ArrayList(list);
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < this.a.size(); i++) {
            a aVar = this.a.get(i);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("fl.variant.id", aVar.a);
            jSONObject2.put("fl.variant.version", aVar.b);
            jSONArray.put(jSONObject2);
        }
        jSONObject.put("fl.variants", jSONArray);
        return jSONObject;
    }
}
