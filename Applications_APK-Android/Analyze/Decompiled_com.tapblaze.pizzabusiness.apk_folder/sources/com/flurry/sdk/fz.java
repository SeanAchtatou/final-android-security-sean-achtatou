package com.flurry.sdk;

public interface fz {
    void a();

    void a(jp jpVar);

    public enum a {
        DO_NOT_FLUSH("DO_NOT_FLUSH"),
        REASON_STICKY_SET_COMPLETE("Sticky set is complete"),
        REASON_APP_STATE_CHANGE("App State has changed"),
        REASON_SESSION_FINALIZE("Session Finalized"),
        REASON_APP_CRASH("App crashed"),
        REASON_FORCE_FLUSH("Force to Flush"),
        REASON_STARTUP("App Started"),
        REASON_PUSH_TOKEN_REFRESH("Push Token Refreshed"),
        REASON_DATA_DELETION("Delete Data");
        
        public final String j;

        private a(String str) {
            this.j = str;
        }
    }
}
