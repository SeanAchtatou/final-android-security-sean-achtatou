package com.flurry.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

public final class dm {
    private static dm b;
    final List<a> a = new ArrayList();
    private Application.ActivityLifecycleCallbacks c;

    public interface a {
        void a(Activity activity);

        void b(Activity activity);

        void c(Activity activity);
    }

    private dm() {
    }

    public static synchronized dm a() {
        dm dmVar;
        synchronized (dm.class) {
            if (b == null) {
                b = new dm();
            }
            dmVar = b;
        }
        return dmVar;
    }

    public final void a(a aVar) {
        synchronized (this.a) {
            this.a.add(aVar);
        }
    }

    public final void a(Context context, Cursor cursor) {
        if (this.c == null && context != null) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext instanceof Application) {
                dk.a().a(applicationContext, cursor);
                this.c = new Application.ActivityLifecycleCallbacks() {
                    public final void onActivityCreated(Activity activity, Bundle bundle) {
                    }

                    public final void onActivityDestroyed(Activity activity) {
                    }

                    public final void onActivityPaused(Activity activity) {
                    }

                    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                    }

                    public final void onActivityStarted(Activity activity) {
                        da.a(3, "PerformanceMonitor", "onActivityStarted for activity: " + activity.toString());
                        for (a a2 : dm.this.a) {
                            a2.a(activity);
                        }
                    }

                    public final void onActivityResumed(Activity activity) {
                        da.a(3, "PerformanceMonitor", "onActivityResumed for activity: " + activity.toString());
                        for (a b : dm.this.a) {
                            b.b(activity);
                        }
                    }

                    public final void onActivityStopped(Activity activity) {
                        da.a(3, "PerformanceMonitor", "onActivityStopped for activity: " + activity.toString());
                        for (a c : dm.this.a) {
                            c.c(activity);
                        }
                    }
                };
                ((Application) applicationContext).registerActivityLifecycleCallbacks(this.c);
            }
        }
    }
}
