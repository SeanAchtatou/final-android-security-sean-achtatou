package com.flurry.sdk;

import com.facebook.internal.AnalyticsEvents;

public enum y {
    UNCAUGHT_EXCEPTION_ID("uncaught"),
    NATIVE_CRASH(AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_NATIVE);
    
    public String c;

    private y(String str) {
        this.c = str;
    }
}
