package com.flurry.sdk;

import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

public final class hh extends jl {
    public final String a;

    public hh(String str) {
        this.a = str == null ? "" : str;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (!TextUtils.isEmpty(this.a)) {
            jSONObject.put("fl.timezone.value", this.a);
        }
        return jSONObject;
    }
}
