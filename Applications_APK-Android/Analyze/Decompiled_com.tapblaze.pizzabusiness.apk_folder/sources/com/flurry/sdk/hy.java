package com.flurry.sdk;

import com.flurry.sdk.fd;

final class hy implements o<ad> {
    hy() {
    }

    public final /* synthetic */ void a(Object obj) {
        ad adVar = (ad) obj;
        if (adVar == null) {
            da.a(2, "SessionInfoFrame", "Session info data is null, do not send the frame.");
        } else {
            bn.a();
            int i = fd.a.AGENT_REPORT_TYPE_MAIN_DEVICE.d;
            if (adVar.d != null && adVar.d.a) {
                i = fd.a.AGENT_REPORT_TYPE_INSTANT_APP.d;
            }
            fc.a().a(new jj(new jk(adVar.a, i, adVar.b, adVar.c)));
        }
        da.a(4, "SessionInfoObserver", "SessionInfoData".concat(String.valueOf(adVar)));
    }
}
