package com.flurry.sdk;

import com.flurry.sdk.ey;
import com.flurry.sdk.gd;
import cz.msebera.android.httpclient.protocol.HTTP;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public final class gb extends f implements gd {
    protected BufferedOutputStream a;
    private gc b;
    private int h;

    public gb() {
        super("BufferedFrameAppender", ey.a(ey.a.CORE));
        this.b = null;
        this.a = null;
        this.h = 0;
        this.b = new gc();
    }

    public final void a(final jp jpVar, final gd.a aVar) {
        da.a(2, "BufferedFrameAppender", "Appending Frame:" + jpVar.a());
        b(new ec() {
            public final void a() {
                gb.a(gb.this, jpVar);
                gd.a aVar = aVar;
                if (aVar != null) {
                    aVar.a();
                }
            }
        });
    }

    public final void a(final jp jpVar) {
        da.a(2, "BufferedFrameAppender", "Appending Frame:" + jpVar.a());
        a(new ec() {
            public final void a() {
                gb.a(gb.this, jpVar);
            }
        });
    }

    private boolean a(byte[] bArr) {
        if (bArr == null) {
            return false;
        }
        try {
            this.a.write(bArr);
            this.a.flush();
            return true;
        } catch (IOException e) {
            da.a(2, "BufferedFrameAppender", "Error appending frame:" + e.getMessage());
            n.a().p.a("BufferedFrameAppenderException", "Exception cfaught when append frame to file", e);
            return false;
        }
    }

    public final void a() {
        da.a(2, "BufferedFrameAppender", HTTP.CONN_CLOSE);
        this.h = 0;
        ea.a(this.a);
        this.a = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public final boolean a(String str, String str2) {
        boolean z;
        da.a(2, "BufferedFrameAppender", "Open");
        boolean z2 = true;
        try {
            File file = new File(str, str2);
            if (file.exists()) {
                z = false;
            } else if (dz.a(file)) {
                z = true;
            } else {
                throw new IOException("Frame file: Error creating directory for :" + file.getAbsolutePath());
            }
            try {
                this.a = new BufferedOutputStream(new FileOutputStream(file, true));
                try {
                    this.h = 0;
                } catch (IOException e) {
                    e = e;
                }
            } catch (IOException e2) {
                z2 = z;
                e = e2;
                da.a(6, "BufferedFrameAppender", "Error in opening file:" + str2 + " Message:" + e.getMessage());
                n.a().p.a("BufferedFrameAppenderException", "Exception caught when open: ".concat(String.valueOf(str2)), e);
                return z2;
            }
        } catch (IOException e3) {
            e = e3;
            z2 = false;
            da.a(6, "BufferedFrameAppender", "Error in opening file:" + str2 + " Message:" + e.getMessage());
            n.a().p.a("BufferedFrameAppenderException", "Exception caught when open: ".concat(String.valueOf(str2)), e);
            return z2;
        }
        return z2;
    }

    public final boolean b() {
        return this.a != null;
    }

    static /* synthetic */ void a(gb gbVar, jp jpVar) {
        gbVar.h++;
        boolean a2 = gbVar.a(gc.a(jpVar));
        if (!a2) {
            n.a().p.a("Fail to append frame to file");
        }
        da.a(2, "BufferedFrameAppender", "Appending Frame " + jpVar.a() + " frameSaved:" + a2 + " frameCount:" + gbVar.h);
    }
}
