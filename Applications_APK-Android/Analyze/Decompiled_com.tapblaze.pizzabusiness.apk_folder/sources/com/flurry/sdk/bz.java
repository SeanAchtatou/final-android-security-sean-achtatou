package com.flurry.sdk;

import android.content.Context;
import com.flurry.android.FlurryAgent;
import com.flurry.sdk.ce;
import com.flurry.sdk.cj;
import com.flurry.sdk.ey;
import java.util.List;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class bz extends f {
    private static boolean a = false;
    /* access modifiers changed from: private */
    public ce b;
    private cr h;
    /* access modifiers changed from: private */
    public a i;
    /* access modifiers changed from: private */
    public cc j;
    private cm k;
    /* access modifiers changed from: private */
    public long l;
    /* access modifiers changed from: private */
    public cj m;

    public interface a {
        void a(ce ceVar, boolean z);
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public abstract String c();

    public bz(cr crVar, a aVar, cc ccVar, cm cmVar) {
        super("ConfigFetcher", ey.a(ey.a.CONFIG));
        this.h = crVar;
        this.i = aVar;
        this.j = ccVar;
        this.k = cmVar;
    }

    public final synchronized void a() {
        da.a("ConfigFetcher", "Starting Config fetch.");
        b(new ec() {
            public final void a() throws Exception {
                ce unused = bz.this.b = ce.b;
                long unused2 = bz.this.l = System.currentTimeMillis();
                cj unused3 = bz.this.m = null;
                bz.this.j.b();
                if (!bz.c(bz.this)) {
                    bz.this.i.a(bz.this.b, false);
                } else {
                    bz.this.b();
                }
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, boolean):org.json.JSONObject
     arg types: [java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, int]
     candidates:
      com.flurry.sdk.cm.a(java.util.List<com.flurry.sdk.cl>, java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>):void
      com.flurry.sdk.cm.a(java.util.Map<com.flurry.sdk.ci, android.util.SparseArray<com.flurry.sdk.cl>>, java.util.Map<com.flurry.sdk.ci, java.util.Map<java.lang.String, com.flurry.sdk.ca>>, boolean):org.json.JSONObject */
    /* access modifiers changed from: protected */
    public final synchronized void d() {
        JSONObject a2;
        da.a("ConfigFetcher", "Fetching Config data.");
        this.h.run();
        this.b = this.h.h();
        if (this.b == ce.a) {
            da.a("ConfigFetcher", "Processing Config fetched data.");
            try {
                String str = this.h.h;
                da.a("ConfigFetcher", "JSON body: ".concat(String.valueOf(str)));
                JSONObject jSONObject = new JSONObject(str);
                String d = this.h.d();
                String c = c();
                String optString = jSONObject.optString("requestGuid");
                String optString2 = jSONObject.optString("apiKey");
                if (d.equals(optString)) {
                    if (c.equals(optString2)) {
                        List<cl> a3 = cd.a(jSONObject);
                        long optLong = jSONObject.optLong("refreshInSeconds");
                        this.k.c = optLong;
                        if (!ct.a(this.j.d()) || !this.h.c() || this.k.b(a3)) {
                            this.k.a(a3, this.h.c());
                            this.b = ce.a;
                            cm cmVar = this.k;
                            Context a4 = b.a();
                            if (!this.h.c()) {
                                str = null;
                            }
                            if (str == null && (a2 = cmVar.a(cmVar.a, cmVar.b, false)) != null) {
                                str = a2.toString();
                            }
                            if (str != null) {
                                ct.a(a4, str);
                            }
                            cc ccVar = this.j;
                            String g = this.h.g();
                            if (ccVar.a != null) {
                                ccVar.a.edit().putString("lastETag", g).apply();
                            }
                            cc ccVar2 = this.j;
                            String e = this.h.e();
                            if (ccVar2.a != null) {
                                ccVar2.a.edit().putString("lastKeyId", e).apply();
                            }
                            cc ccVar3 = this.j;
                            String f = this.h.f();
                            if (ccVar3.a != null) {
                                ccVar3.a.edit().putString("lastRSA", f).apply();
                            }
                        } else {
                            this.b = ce.b;
                        }
                        a = true;
                        gi.a(this.k.b());
                        cc ccVar4 = this.j;
                        String c2 = this.k.c();
                        if (ccVar4.a != null) {
                            da.a("ConfigMeta", "Save serialized variant IDs: ".concat(String.valueOf(c2)));
                            ccVar4.a.edit().putString("com.flurry.sdk.variant_ids", c2).apply();
                        }
                        cc ccVar5 = this.j;
                        if (ccVar5.a != null) {
                            ccVar5.a.edit().putInt("appVersion", ccVar5.b).apply();
                        }
                        this.j.a(System.currentTimeMillis());
                        cc ccVar6 = this.j;
                        long j2 = optLong * 1000;
                        if (j2 == 0) {
                            ccVar6.c = 0;
                        } else if (j2 > 604800000) {
                            ccVar6.c = 604800000;
                        } else if (j2 < 60000) {
                            ccVar6.c = 60000;
                        } else {
                            ccVar6.c = j2;
                        }
                        if (ccVar6.a != null) {
                            ccVar6.a.edit().putLong("refreshFetch", ccVar6.c).apply();
                        }
                        if (cb.b() != null) {
                            cb.b();
                            cn.a(this.k);
                        }
                        this.j.b();
                        if (cb.b() != null) {
                            cb.b();
                            cn.a(this.b.d.h, System.currentTimeMillis() - this.l, this.b.toString());
                        }
                        this.i.a(this.b, false);
                        return;
                    }
                }
                ce.a aVar = ce.a.AUTHENTICATE;
                this.b = new ce(aVar, "Guid: " + d + ", payload: " + optString + " APIKey: " + c + ", payload: " + optString2);
                StringBuilder sb = new StringBuilder("Authentication error: ");
                sb.append(this.b);
                da.b("ConfigFetcher", sb.toString());
                e();
            } catch (JSONException e2) {
                da.a("ConfigFetcher", "Json parse error", e2);
                this.b = new ce(ce.a.NOT_VALID_JSON, e2.toString());
            } catch (Exception e3) {
                da.a("ConfigFetcher", "Fetch result error", e3);
                this.b = new ce(ce.a.OTHER, e3.toString());
            }
        } else if (this.b == ce.b) {
            this.j.a(System.currentTimeMillis());
            this.j.b();
            this.i.a(this.b, false);
        } else {
            da.a(5, "ConfigFetcher", "fetch error:" + this.b.toString());
            if (this.m == null && this.b.d == ce.a.UNKNOWN_CERTIFICATE) {
                FlurryAgent.onError("FlurryUnknownCertificate", this.b.c, "ConfigFetcher");
            }
            if (cb.b() != null) {
                cb.b();
                cn.a(this.b.d.h, System.currentTimeMillis() - this.l, this.b.toString());
            }
            e();
        }
    }

    private void e() {
        da.a("ConfigFetcher", "Retry fetching Config data.");
        cj cjVar = this.m;
        if (cjVar == null) {
            this.m = new cj(cj.a.values()[0]);
        } else {
            this.m = new cj(cjVar.a.a());
        }
        if (this.m.a == cj.a.ABANDON) {
            this.i.a(this.b, false);
            return;
        }
        this.i.a(this.b, true);
        this.j.a(new TimerTask() {
            public final void run() {
                bz.this.d();
            }
        }, ((long) this.m.a()) * 1000);
    }

    static /* synthetic */ boolean c(bz bzVar) {
        if (!ct.a(b.a())) {
            return true;
        }
        da.a("ConfigFetcher", "Compare version: current=" + bzVar.j.b + ", recorded=" + bzVar.j.a());
        if (bzVar.j.a() < bzVar.j.b) {
            return true;
        }
        long j2 = bzVar.j.c;
        long j3 = 0;
        if (j2 != 0) {
            cc ccVar = bzVar.j;
            if (ccVar.a != null) {
                j3 = ccVar.a.getLong("lastFetch", 0);
            }
            if (System.currentTimeMillis() - j3 > j2) {
                return true;
            }
        } else if (!a) {
            return true;
        }
        da.a("ConfigFetcher", "It does not meet any criterias for data fetch.");
        return false;
    }
}
