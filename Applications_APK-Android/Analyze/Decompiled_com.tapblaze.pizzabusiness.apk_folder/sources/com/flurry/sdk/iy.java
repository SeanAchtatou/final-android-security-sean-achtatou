package com.flurry.sdk;

import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

public final class iy extends jl {
    public final String a;
    public final boolean b;

    public iy(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (!TextUtils.isEmpty(this.a)) {
            jSONObject.put("fl.notification.key", this.a);
        }
        jSONObject.put("fl.notification.enabled", this.b);
        return jSONObject;
    }
}
