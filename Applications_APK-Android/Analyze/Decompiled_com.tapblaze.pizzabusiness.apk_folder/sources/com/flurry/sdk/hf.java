package com.flurry.sdk;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class hf extends jl {
    public final Map<String, Map<String, String>> a;

    public hf(Map<String, Map<String, String>> map) {
        this.a = new HashMap(map);
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.session.property", eb.b(this.a));
        return jSONObject;
    }
}
