package com.flurry.sdk;

public final class ir extends jm {
    private ir(jo joVar) {
        super(joVar);
    }

    public final jn a() {
        return jn.FRAME_COUNTER;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.ff.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.flurry.sdk.ff.b(java.lang.String, int):int
      com.flurry.sdk.ff.b(java.lang.String, java.lang.String):java.lang.String
      com.flurry.sdk.ff.b(java.lang.String, long):long */
    public static ir b() {
        long b = ff.b("frame.counter", 0L) + 1;
        ff.a("frame.counter", b);
        return new ir(new is(b));
    }
}
