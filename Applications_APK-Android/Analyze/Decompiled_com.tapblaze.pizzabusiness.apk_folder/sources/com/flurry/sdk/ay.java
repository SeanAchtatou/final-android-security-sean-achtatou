package com.flurry.sdk;

import java.util.Map;

public final class ay {
    public final String a;
    public final String b;
    public final Map<String, String> c;

    public ay(String str, String str2, Map<String, String> map) {
        this.a = str;
        this.b = str2;
        this.c = map;
    }
}
