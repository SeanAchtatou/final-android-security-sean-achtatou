package com.flurry.sdk;

public final class ba {
    public final bd a;
    public final long b;
    public final long c;
    public final long d;
    public final bc e;
    public final boolean f;

    public ba(bd bdVar, long j, long j2, long j3, bc bcVar, boolean z) {
        this.a = bdVar;
        this.b = j;
        this.c = j2;
        this.d = j3;
        this.e = bcVar;
        this.f = z;
    }
}
