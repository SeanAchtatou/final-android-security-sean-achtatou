package com.flurry.sdk;

import android.os.FileObserver;

public final class jv extends FileObserver {
    private jt a;
    private String b;

    public jv(String str, jt jtVar) {
        super(str);
        this.b = str;
        this.a = jtVar;
    }

    public final void onEvent(int i, String str) {
        if (str != null) {
            StringBuilder sb = new StringBuilder();
            if ((i & 8) != 0) {
                sb.append(this.b + "/" + str + " is written and closed\n");
                StringBuilder sb2 = new StringBuilder("Observer triggered ");
                sb2.append(sb.toString());
                da.a(3, "VNodeObserver", sb2.toString());
                this.a.a(str);
            }
        }
    }
}
