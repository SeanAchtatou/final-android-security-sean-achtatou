package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class io extends jl {
    public final boolean a = true;
    public final String b;

    public io(String str) {
        this.b = str;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.background.enabled", this.a);
        jSONObject.put("fl.sdk.version.code", this.b);
        return jSONObject;
    }
}
