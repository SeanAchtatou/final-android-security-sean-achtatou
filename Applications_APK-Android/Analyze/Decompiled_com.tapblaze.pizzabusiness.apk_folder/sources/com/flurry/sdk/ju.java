package com.flurry.sdk;

import com.flurry.sdk.ey;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ju extends f implements jt {
    public jv a = null;
    public jq b;

    public ju(jq jqVar) {
        super("VNodeFileProcessor", ey.a(ey.a.DATA_PROCESSOR));
        this.b = jqVar;
    }

    public final void a(final List<File> list) {
        if (list != null && list.size() != 0) {
            b(new ec() {
                public final void a() throws Exception {
                    da.a(2, "VNodeFileProcessor", "Number of files already pending: in VNodeListener " + list.size());
                    ArrayList arrayList = new ArrayList();
                    for (File file : list) {
                        if (file.exists()) {
                            arrayList.add(file.getAbsolutePath());
                        }
                    }
                    if (ju.this.b != null) {
                        ju.this.b.a(arrayList);
                    }
                }
            });
        }
    }

    public final void a(String str) {
        String b2 = fg.b();
        File file = new File(b2 + File.separator + str);
        if (file.exists()) {
            a(Arrays.asList(file));
        }
    }
}
