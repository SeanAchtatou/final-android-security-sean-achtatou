package com.flurry.sdk;

import com.flurry.sdk.bz;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class cf {
    public static List<bz> a(cr crVar, bz.a aVar, cc ccVar, cm cmVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new ck(crVar, aVar, ccVar, cmVar));
        return arrayList;
    }

    public static JSONArray a() throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry next : n.a().g.d().a().entrySet()) {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("id", next.getValue());
            jSONObject.put("type", ((al) next.getKey()).d);
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }
}
