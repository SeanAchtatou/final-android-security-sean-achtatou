package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class hb extends jl {
    public final String a;
    public final String b;

    public hb(String str, String str2) {
        this.a = str == null ? "" : str;
        this.b = str2 == null ? "" : str2;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.session.deeplink", this.b);
        jSONObject.put("fl.session.origin.name", this.a);
        return jSONObject;
    }
}
