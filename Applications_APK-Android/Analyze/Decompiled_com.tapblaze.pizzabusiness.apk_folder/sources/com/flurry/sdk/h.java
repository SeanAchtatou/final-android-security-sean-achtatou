package com.flurry.sdk;

import java.util.TimerTask;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public abstract class h {
    static Runnable c = new Runnable() {
        public final void run() {
        }
    };
    protected final String d;
    protected final h e;
    protected final boolean f;
    protected final boolean g;

    /* access modifiers changed from: protected */
    public abstract void a(Runnable runnable) throws CancellationException;

    /* access modifiers changed from: protected */
    public abstract Future<Void> b(Runnable runnable);

    /* access modifiers changed from: protected */
    public boolean c(Runnable runnable) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void d(Runnable runnable) {
    }

    /* access modifiers changed from: protected */
    public abstract void f(Runnable runnable);

    public class a extends FutureTask<Void> {
        public final h a;
        private TimerTask c;
        private final int d = 0;
        private final int e = 1;
        private final int f = 2;
        private int g;

        a(h hVar, Runnable runnable) {
            super(runnable, null);
            this.a = hVar;
            if (runnable == h.c) {
                this.g = 0;
            } else {
                this.g = 1;
            }
        }

        public final synchronized boolean a() {
            return this.g == 0;
        }

        public synchronized boolean cancel(boolean z) {
            super.cancel(z);
            if (this.c != null) {
                this.c.cancel();
            }
            return true;
        }

        public synchronized void run() {
            if (this.g == 1) {
                this.g = 2;
                if (!this.a.c(this)) {
                    this.a.e(this);
                }
                this.g = 1;
                return;
            }
            super.run();
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    h(String str, h hVar, boolean z) {
        this(str, hVar, z, hVar == null ? false : hVar.g);
    }

    private h(String str, h hVar, boolean z, boolean z2) {
        this.d = str;
        this.e = hVar;
        this.f = z;
        this.g = z2;
    }

    /* access modifiers changed from: protected */
    public final boolean e(Runnable runnable) {
        for (h hVar = this.e; hVar != null; hVar = hVar.e) {
            if (hVar.c(runnable)) {
                return true;
            }
        }
        runnable.run();
        return true;
    }
}
