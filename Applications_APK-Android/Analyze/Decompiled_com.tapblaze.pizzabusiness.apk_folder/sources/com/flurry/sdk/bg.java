package com.flurry.sdk;

import java.util.Collections;
import java.util.Map;

public final class bg extends m<bf> {
    private static boolean b = false;

    public bg() {
        super("FlurryErrorProvider");
    }

    public static void a(boolean z) {
        b = z;
    }

    public final void a(String str) {
        a(str, Collections.emptyMap());
    }

    public final void a(String str, Map<String, String> map) {
        if (b) {
            a(new bf(str, map));
        }
    }

    public final void a(String str, String str2, Throwable th) {
        a(str, str2, th, Collections.emptyMap());
    }

    private void a(String str, String str2, Throwable th, Map<String, String> map) {
        if (b) {
            a(new bf(str, str2, th, map));
        }
    }
}
