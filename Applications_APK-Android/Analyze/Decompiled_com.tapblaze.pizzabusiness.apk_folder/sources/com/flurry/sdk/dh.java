package com.flurry.sdk;

import android.net.TrafficStats;
import android.net.Uri;
import android.text.TextUtils;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

public class dh extends ed {
    private final cw<String, String> a = new cw<>();
    private final cw<String, String> b = new cw<>();
    private int c = 10000;
    private int d = 15000;
    final Object e = new Object();
    public String f;
    public a g;
    b h;
    HttpURLConnection i;
    boolean j;
    boolean k;
    long l = -1;
    public int m = -1;
    public boolean n = false;
    boolean o;
    private int q;
    private int r;
    private boolean s = true;
    private boolean t;
    private long u = -1;
    private Exception v;
    private boolean w;
    private int x = 25000;
    private dg y = new dg(this);

    public interface b {
        void a();

        void a(dh dhVar, InputStream inputStream) throws Exception;

        void a(OutputStream outputStream) throws Exception;
    }

    public final void a(String str, String str2) {
        this.a.a(str, str2);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.h != null && !c()) {
            this.h.a();
        }
    }

    public final boolean c() {
        boolean z;
        synchronized (this.e) {
            z = this.k;
        }
        return z;
    }

    public void a() {
        try {
            if (this.f != null) {
                if (!c.a()) {
                    da.a(3, "HttpStreamRequest", "Network not available, aborting http request: " + this.f);
                } else {
                    if (this.g == null || a.kUnknown.equals(this.g)) {
                        this.g = a.kGet;
                    }
                    d();
                    da.a(4, "HttpStreamRequest", "HTTP status: " + this.m + " for url: " + this.f);
                }
            }
        } catch (Exception e2) {
            da.a(4, "HttpStreamRequest", "HTTP status: " + this.m + " for url: " + this.f);
            StringBuilder sb = new StringBuilder("Exception during http request: ");
            sb.append(this.f);
            da.a(3, "HttpStreamRequest", sb.toString(), e2);
            if (this.i != null) {
                this.r = this.i.getReadTimeout();
                this.q = this.i.getConnectTimeout();
            }
            this.v = e2;
        } catch (Throwable th) {
            this.y.a();
            b();
            throw th;
        }
        this.y.a();
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.cw.a(java.lang.String, java.lang.String):void
     arg types: [java.lang.Object, java.lang.String]
     candidates:
      com.flurry.sdk.cw.a(java.lang.String, boolean):java.util.List<V>
      com.flurry.sdk.cw.a(java.lang.String, java.lang.String):void */
    private void d() throws Exception {
        InputStream inputStream;
        InputStream inputStream2;
        BufferedOutputStream bufferedOutputStream;
        Throwable th;
        OutputStream outputStream;
        if (!this.k) {
            String str = this.f;
            if (!TextUtils.isEmpty(str) && Uri.parse(str).getScheme() == null) {
                str = "http://".concat(String.valueOf(str));
            }
            this.f = str;
            this.i = (HttpURLConnection) ((URLConnection) FirebasePerfUrlConnection.instrument(new URL(this.f).openConnection()));
            this.i.setConnectTimeout(this.c);
            this.i.setReadTimeout(this.d);
            this.i.setRequestMethod(this.g.toString());
            this.i.setInstanceFollowRedirects(this.s);
            this.i.setDoOutput(a.kPost.equals(this.g));
            this.i.setDoInput(true);
            TrafficStats.setThreadStatsTag(1234);
            for (Map.Entry next : this.a.a()) {
                this.i.addRequestProperty((String) next.getKey(), (String) next.getValue());
            }
            if (!a.kGet.equals(this.g) && !a.kPost.equals(this.g)) {
                this.i.setRequestProperty("Accept-Encoding", "");
            }
            if (this.k) {
                e();
                return;
            }
            if (this.n && (this.i instanceof HttpsURLConnection)) {
                this.i.connect();
                di.a((HttpsURLConnection) this.i);
            }
            BufferedInputStream bufferedInputStream = null;
            if (a.kPost.equals(this.g)) {
                try {
                    outputStream = this.i.getOutputStream();
                    try {
                        bufferedOutputStream = new BufferedOutputStream(outputStream);
                        try {
                            if (this.h != null && !c()) {
                                this.h.a(bufferedOutputStream);
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            ea.a(bufferedOutputStream);
                            ea.a(outputStream);
                            throw th;
                        }
                        try {
                            ea.a(bufferedOutputStream);
                            ea.a(outputStream);
                        } catch (Exception e2) {
                            da.a(6, "HttpStreamRequest", "Exception is:" + e2.getLocalizedMessage());
                            e();
                            return;
                        } catch (Throwable th3) {
                            e();
                            throw th3;
                        }
                    } catch (Throwable th4) {
                        Throwable th5 = th4;
                        bufferedOutputStream = null;
                        th = th5;
                        ea.a(bufferedOutputStream);
                        ea.a(outputStream);
                        throw th;
                    }
                } catch (Throwable th6) {
                    bufferedOutputStream = null;
                    th = th6;
                    outputStream = null;
                    ea.a(bufferedOutputStream);
                    ea.a(outputStream);
                    throw th;
                }
            }
            if (this.t) {
                this.l = System.currentTimeMillis();
            }
            if (this.w) {
                this.y.a((long) this.x);
            }
            this.m = this.i.getResponseCode();
            if (this.t && this.l != -1) {
                this.u = System.currentTimeMillis() - this.l;
            }
            this.y.a();
            for (Map.Entry next2 : this.i.getHeaderFields().entrySet()) {
                for (String a2 : (List) next2.getValue()) {
                    this.b.a((String) next2.getKey(), a2);
                }
            }
            if (!a.kGet.equals(this.g) && !a.kPost.equals(this.g)) {
                e();
            } else if (this.k) {
                e();
            } else {
                try {
                    if (this.m == 200) {
                        inputStream2 = this.i.getInputStream();
                    } else {
                        inputStream2 = this.i.getErrorStream();
                    }
                    try {
                        BufferedInputStream bufferedInputStream2 = new BufferedInputStream(inputStream2);
                        try {
                            if (this.h != null && !c()) {
                                this.h.a(this, bufferedInputStream2);
                            }
                            ea.a(bufferedInputStream2);
                            ea.a(inputStream2);
                            e();
                        } catch (Throwable th7) {
                            BufferedInputStream bufferedInputStream3 = bufferedInputStream2;
                            inputStream = inputStream2;
                            th = th7;
                            bufferedInputStream = bufferedInputStream3;
                            ea.a(bufferedInputStream);
                            ea.a(inputStream);
                            throw th;
                        }
                    } catch (Throwable th8) {
                        Throwable th9 = th8;
                        inputStream = inputStream2;
                        th = th9;
                        ea.a(bufferedInputStream);
                        ea.a(inputStream);
                        throw th;
                    }
                } catch (Throwable th10) {
                    th = th10;
                    inputStream = null;
                    ea.a(bufferedInputStream);
                    ea.a(inputStream);
                    throw th;
                }
            }
        }
    }

    private void e() {
        if (!this.j) {
            this.j = true;
            HttpURLConnection httpURLConnection = this.i;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    /* renamed from: com.flurry.sdk.dh$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] a = new int[a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.flurry.sdk.dh$a[] r0 = com.flurry.sdk.dh.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.flurry.sdk.dh.AnonymousClass2.a = r0
                int[] r0 = com.flurry.sdk.dh.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.flurry.sdk.dh$a r1 = com.flurry.sdk.dh.a.kPost     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.flurry.sdk.dh.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.flurry.sdk.dh$a r1 = com.flurry.sdk.dh.a.kPut     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.flurry.sdk.dh.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.flurry.sdk.dh$a r1 = com.flurry.sdk.dh.a.kDelete     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.flurry.sdk.dh.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.flurry.sdk.dh$a r1 = com.flurry.sdk.dh.a.kHead     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.flurry.sdk.dh.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.flurry.sdk.dh$a r1 = com.flurry.sdk.dh.a.kGet     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.dh.AnonymousClass2.<clinit>():void");
        }
    }

    public enum a {
        kUnknown,
        kGet,
        kPost,
        kPut,
        kDelete,
        kHead;

        public final String toString() {
            int i = AnonymousClass2.a[ordinal()];
            if (i == 1) {
                return "POST";
            }
            if (i == 2) {
                return "PUT";
            }
            if (i == 3) {
                return "DELETE";
            }
            if (i == 4) {
                return "HEAD";
            }
            if (i != 5) {
                return null;
            }
            return "GET";
        }
    }
}
