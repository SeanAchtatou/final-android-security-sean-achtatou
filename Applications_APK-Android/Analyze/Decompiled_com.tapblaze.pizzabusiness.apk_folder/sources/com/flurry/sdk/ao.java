package com.flurry.sdk;

import android.app.Activity;
import android.os.Bundle;
import java.lang.ref.WeakReference;

public final class ao {
    public final a a;
    public final Bundle b;
    public WeakReference<Activity> c;

    public enum a {
        CREATED,
        STARTED,
        RESUMED,
        PAUSED,
        STOPPED,
        DESTROYED,
        SAVE_STATE,
        APP_ORIENTATION_CHANGE,
        APP_BACKGROUND,
        TRIM_MEMORY
    }

    public ao(a aVar, Bundle bundle) {
        this.a = aVar;
        this.b = bundle;
    }
}
