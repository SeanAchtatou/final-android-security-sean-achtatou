package com.flurry.sdk;

import com.flurry.sdk.gj;
import java.util.ArrayList;
import java.util.List;

public final class gi extends jm {
    private gi(jo joVar) {
        super(joVar);
    }

    public final jn a() {
        return jn.VARIANT_IDS;
    }

    public static void a(List<cl> list) {
        if (list == null || list.size() == 0) {
            da.d("VariantIdsFrame", "Variant list is empty, do not send the frame.");
            return;
        }
        ArrayList arrayList = new ArrayList();
        for (cl next : list) {
            arrayList.add(new gj.a((long) next.b, next.c));
        }
        fc.a().a(new gi(new gj(arrayList)));
    }
}
