package com.flurry.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import com.flurry.sdk.ao;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;

public final class ap extends m<ao> implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    private static String i;
    public int b = 0;
    private final Set<String> h;

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        a(activity, ao.a.CREATED);
        synchronized (this) {
            if (i == null) {
                i = activity.getClass().getName();
            }
        }
    }

    public final void onActivityStarted(Activity activity) {
        this.h.add(activity.toString());
        a(activity, ao.a.STARTED);
    }

    public final void onActivityResumed(Activity activity) {
        a(activity, ao.a.RESUMED);
    }

    public final void onActivityPaused(Activity activity) {
        a(activity, ao.a.PAUSED);
    }

    public final void onActivityStopped(Activity activity) {
        this.h.remove(activity.toString());
        a(activity, ao.a.STOPPED);
        if (this.h.isEmpty()) {
            a(activity, ao.a.APP_BACKGROUND);
        }
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        a(activity, ao.a.SAVE_STATE);
    }

    public final void onActivityDestroyed(Activity activity) {
        a(activity, ao.a.DESTROYED);
    }

    public final void onTrimMemory(int i2) {
        a(i2);
    }

    public final void onConfigurationChanged(Configuration configuration) {
        if (this.b != configuration.orientation) {
            this.b = configuration.orientation;
            Bundle bundle = new Bundle();
            bundle.putInt("orientation_name", this.b);
            a(new ao(ao.a.APP_ORIENTATION_CHANGE, bundle));
        }
    }

    public final void onLowMemory() {
        a(80);
    }

    public ap() {
        super("ApplicationLifecycleProvider");
        Application application = (Application) b.a();
        if (application != null) {
            this.b = application.getResources().getConfiguration().orientation;
            application.registerActivityLifecycleCallbacks(this);
            application.registerComponentCallbacks(this);
        } else {
            da.a(6, "ApplicationLifecycleProvider", "Context is null when initializing.");
        }
        this.h = new HashSet();
    }

    public final void c() {
        super.c();
        Application application = (Application) b.a();
        application.unregisterActivityLifecycleCallbacks(this);
        application.unregisterComponentCallbacks(this);
    }

    private void a(Activity activity, ao.a aVar) {
        Bundle bundle = new Bundle();
        bundle.putString("activity_name", activity.getLocalClassName());
        ao aoVar = new ao(aVar, bundle);
        aoVar.c = new WeakReference<>(activity);
        a(aoVar);
    }

    private void a(int i2) {
        Bundle bundle = new Bundle();
        bundle.putInt("trim_memory_level", i2);
        a(new ao(ao.a.TRIM_MEMORY, bundle));
    }
}
