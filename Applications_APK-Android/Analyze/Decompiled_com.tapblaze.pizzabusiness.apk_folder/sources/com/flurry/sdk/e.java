package com.flurry.sdk;

import java.util.Timer;
import java.util.concurrent.Executor;

public final class e extends g {
    private static Timer b = new Timer("ExecutorQueue Global Timer", true);
    Executor a;

    public e(Executor executor, String str) {
        super(str);
        this.a = executor;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0018, code lost:
        return false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean a(com.flurry.sdk.h.a r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r0 = r2.a()     // Catch:{ Exception -> 0x0016, all -> 0x0013 }
            if (r0 == 0) goto L_0x000b
            r2.run()     // Catch:{ Exception -> 0x0016, all -> 0x0013 }
            goto L_0x0010
        L_0x000b:
            java.util.concurrent.Executor r0 = r1.a     // Catch:{ Exception -> 0x0016, all -> 0x0013 }
            r0.execute(r2)     // Catch:{ Exception -> 0x0016, all -> 0x0013 }
        L_0x0010:
            r2 = 1
            monitor-exit(r1)
            return r2
        L_0x0013:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        L_0x0016:
            r2 = 0
            monitor-exit(r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.e.a(com.flurry.sdk.h$a):boolean");
    }
}
