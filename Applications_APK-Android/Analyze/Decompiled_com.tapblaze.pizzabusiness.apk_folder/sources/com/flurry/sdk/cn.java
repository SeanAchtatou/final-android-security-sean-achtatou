package com.flurry.sdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public final class cn {
    public static void a(cm cmVar) {
        if (cmVar.e() != 0) {
            ArrayList<cl> arrayList = new ArrayList<>(cmVar.b());
            Collections.sort(arrayList);
            ArrayList arrayList2 = new ArrayList();
            for (cl clVar : arrayList) {
                arrayList2.add(Integer.toString(clVar.b));
            }
        }
    }

    public static void a(int i, long j, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("exp_code", String.valueOf(i));
        hashMap.put("exp_ms", String.valueOf(j));
        if (str != null) {
            hashMap.put("exp_det", str);
        }
        if (da.c() <= 2) {
            da.a(2, "AnalyticsImpl", String.format("YWA event: %1$s {%2$s}", "expsdk_data", hashMap.toString()));
        }
    }
}
