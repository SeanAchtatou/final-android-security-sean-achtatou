package com.flurry.sdk;

import java.lang.Thread;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public final class z {
    private static z c;
    final Thread.UncaughtExceptionHandler a = Thread.getDefaultUncaughtExceptionHandler();
    final Map<Thread.UncaughtExceptionHandler, Void> b = new WeakHashMap();

    private z() {
        Thread.setDefaultUncaughtExceptionHandler(new a(this, (byte) 0));
    }

    public static synchronized z a() {
        z zVar;
        synchronized (z.class) {
            if (c == null) {
                c = new z();
            }
            zVar = c;
        }
        return zVar;
    }

    public static synchronized void b() {
        synchronized (z.class) {
            if (c != null) {
                Thread.setDefaultUncaughtExceptionHandler(c.a);
            }
            c = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final Set<Thread.UncaughtExceptionHandler> c() {
        Set<Thread.UncaughtExceptionHandler> keySet;
        synchronized (this.b) {
            keySet = this.b.keySet();
        }
        return keySet;
    }

    final class a implements Thread.UncaughtExceptionHandler {
        private a() {
        }

        /* synthetic */ a(z zVar, byte b) {
            this();
        }

        public final void uncaughtException(Thread thread, Throwable th) {
            for (Thread.UncaughtExceptionHandler uncaughtException : z.this.c()) {
                try {
                    uncaughtException.uncaughtException(thread, th);
                } catch (Throwable unused) {
                }
            }
            if (z.this.a != null) {
                try {
                    z.this.a.uncaughtException(thread, th);
                } catch (Throwable unused2) {
                }
            }
        }
    }
}
