package com.flurry.sdk;

import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;

public final class bk {
    private static bk c;
    public String a;
    public String b;

    private bk() {
    }

    public static bk a() {
        if (c == null) {
            c = new bk();
        }
        return c;
    }

    public final String b() {
        if (TextUtils.isEmpty(this.a)) {
            c();
        }
        da.a(3, "APIKeyProvider", "Getting streaming apikey: " + this.a);
        return this.a;
    }

    public final void c() {
        if (TextUtils.isEmpty(this.a)) {
            this.a = this.b;
            if (!d()) {
                this.a += AppEventsConstants.EVENT_PARAM_VALUE_NO;
            }
            da.a(3, "APIKeyProvider", "Generating a streaming apikey: " + this.a);
        }
    }

    private static boolean d() {
        return d.a() == 0;
    }
}
