package com.flurry.sdk;

import java.util.Map;

public final class bf {
    public final String a;
    public final Map<String, String> b;
    public final String c;
    public final Throwable d;
    public final int e;

    bf(String str, Map<String, String> map) {
        this.a = str;
        this.b = map;
        this.e = be.b;
        this.c = null;
        this.d = null;
    }

    bf(String str, String str2, Throwable th, Map<String, String> map) {
        this.a = str;
        this.c = str2;
        this.d = th;
        this.b = map;
        this.e = be.a;
    }
}
