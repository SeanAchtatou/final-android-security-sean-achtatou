package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class ji extends jl {
    public final long a;
    public final long b;
    public final long c;
    public final int d;

    public ji(long j, long j2, long j3, int i) {
        this.a = j;
        this.b = j2;
        this.c = j3;
        this.d = i;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.session.id", this.a);
        jSONObject.put("fl.session.elapsed.start.time", this.b);
        long j = this.c;
        if (j >= this.b) {
            jSONObject.put("fl.session.elapsed.end.time", j);
        }
        jSONObject.put("fl.session.id.current.state", this.d);
        return jSONObject;
    }
}
