package com.flurry.sdk;

import java.util.HashMap;
import java.util.Map;

public final class hv implements o<ay> {
    public final /* synthetic */ void a(Object obj) {
        ay ayVar = (ay) obj;
        String str = ayVar.a;
        String str2 = ayVar.b;
        Map map = ayVar.c;
        if (map == null) {
            map = new HashMap();
        }
        map.put("fl.origin.attribute.version", str2);
        if (map.size() > 10) {
            int size = map.size();
            da.d("OriginAttributeFrame", "MaxOriginParams exceeded: ".concat(String.valueOf(size)));
            map.clear();
            map.put("fl.parameter.limit.exceeded", String.valueOf(size));
        }
        fc.a().a(new iz(new ja(str, map)));
        da.a(4, "OriginAttributeObserver", "Origin attribute name: " + ayVar.a + ". Origin attribute version: " + ayVar.b + ". Origin attribute params: " + ayVar.c);
    }
}
