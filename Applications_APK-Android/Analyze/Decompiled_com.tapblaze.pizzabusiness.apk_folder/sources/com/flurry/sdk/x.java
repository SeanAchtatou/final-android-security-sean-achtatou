package com.flurry.sdk;

import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.os.StatFs;
import java.util.HashMap;
import java.util.Map;

public final class x {
    public static Map<String, String> a() {
        HashMap hashMap = new HashMap();
        a(hashMap);
        b(hashMap);
        d(hashMap);
        g(hashMap);
        h(hashMap);
        c(hashMap);
        e(hashMap);
        f(hashMap);
        return hashMap;
    }

    private static void a(Map<String, String> map) {
        try {
            map.put("mem.java.max", Long.toString(Runtime.getRuntime().maxMemory()));
        } catch (RuntimeException e) {
            da.a(6, "CrashParameterCollector", "Error retrieving max memory", e);
        }
    }

    private static void b(Map<String, String> map) {
        try {
            Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
            Debug.getMemoryInfo(memoryInfo);
            map.put("mem.pss", Long.toString((long) (memoryInfo.getTotalPss() * 1024)));
        } catch (RuntimeException e) {
            da.a(6, "CrashParameterCollector", "Error retrieving pss memory", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0095 A[Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00a6 A[Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:63:0x00d7=Splitter:B:63:0x00d7, B:58:0x00ce=Splitter:B:58:0x00ce} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void c(java.util.Map<java.lang.String, java.lang.String> r10) {
        /*
            java.lang.String r0 = "^Vm(RSS|Size|Peak):\\s+(\\d+)\\s+kB$"
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r0)
            java.io.File r1 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "/proc/"
            r2.<init>(r3)
            int r3 = android.os.Process.myPid()
            java.lang.String r3 = java.lang.Integer.toString(r3)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            java.io.File r2 = new java.io.File
            java.lang.String r3 = "status"
            r2.<init>(r1, r3)
            r1 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00d5, IOException -> 0x00cc, all -> 0x00c8 }
            r3.<init>(r2)     // Catch:{ FileNotFoundException -> 0x00d5, IOException -> 0x00cc, all -> 0x00c8 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x00c4, IOException -> 0x00c0, all -> 0x00bd }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x00c4, IOException -> 0x00c0, all -> 0x00bd }
            r4.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00c4, IOException -> 0x00c0, all -> 0x00bd }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x00c4, IOException -> 0x00c0, all -> 0x00bd }
            java.lang.String r1 = r2.readLine()     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
        L_0x003c:
            if (r1 == 0) goto L_0x00b0
            java.util.regex.Matcher r1 = r0.matcher(r1)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            boolean r4 = r1.find()     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            if (r4 == 0) goto L_0x00ab
            r4 = 1
            java.lang.String r5 = r1.group(r4)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            r6 = 2
            java.lang.String r1 = r1.group(r6)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            boolean r7 = android.text.TextUtils.isEmpty(r5)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            if (r7 != 0) goto L_0x00ab
            boolean r7 = android.text.TextUtils.isEmpty(r1)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            if (r7 == 0) goto L_0x005f
            goto L_0x00ab
        L_0x005f:
            r7 = -1
            int r8 = r5.hashCode()     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            r9 = 81458(0x13e32, float:1.14147E-40)
            if (r8 == r9) goto L_0x0088
            r9 = 2483455(0x25e4ff, float:3.480062E-39)
            if (r8 == r9) goto L_0x007e
            r9 = 2577441(0x275421, float:3.611764E-39)
            if (r8 == r9) goto L_0x0074
            goto L_0x0092
        L_0x0074:
            java.lang.String r8 = "Size"
            boolean r5 = r5.equals(r8)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            if (r5 == 0) goto L_0x0092
            r5 = 1
            goto L_0x0093
        L_0x007e:
            java.lang.String r8 = "Peak"
            boolean r5 = r5.equals(r8)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            if (r5 == 0) goto L_0x0092
            r5 = 2
            goto L_0x0093
        L_0x0088:
            java.lang.String r8 = "RSS"
            boolean r5 = r5.equals(r8)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            if (r5 == 0) goto L_0x0092
            r5 = 0
            goto L_0x0093
        L_0x0092:
            r5 = -1
        L_0x0093:
            if (r5 == 0) goto L_0x00a6
            if (r5 == r4) goto L_0x00a0
            if (r5 == r6) goto L_0x009a
            goto L_0x00ab
        L_0x009a:
            java.lang.String r4 = "mem.virt.max"
            r10.put(r4, r1)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            goto L_0x00ab
        L_0x00a0:
            java.lang.String r4 = "mem.virt"
            r10.put(r4, r1)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            goto L_0x00ab
        L_0x00a6:
            java.lang.String r4 = "mem.rss"
            r10.put(r4, r1)     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
        L_0x00ab:
            java.lang.String r1 = r2.readLine()     // Catch:{ FileNotFoundException -> 0x00bb, IOException -> 0x00b9, all -> 0x00b7 }
            goto L_0x003c
        L_0x00b0:
            com.flurry.sdk.ea.a(r3)
        L_0x00b3:
            com.flurry.sdk.ea.a(r2)
            return
        L_0x00b7:
            r10 = move-exception
            goto L_0x00dd
        L_0x00b9:
            r10 = move-exception
            goto L_0x00c2
        L_0x00bb:
            r10 = move-exception
            goto L_0x00c6
        L_0x00bd:
            r10 = move-exception
            r2 = r1
            goto L_0x00dd
        L_0x00c0:
            r10 = move-exception
            r2 = r1
        L_0x00c2:
            r1 = r3
            goto L_0x00ce
        L_0x00c4:
            r10 = move-exception
            r2 = r1
        L_0x00c6:
            r1 = r3
            goto L_0x00d7
        L_0x00c8:
            r10 = move-exception
            r2 = r1
            r3 = r2
            goto L_0x00dd
        L_0x00cc:
            r10 = move-exception
            r2 = r1
        L_0x00ce:
            r10.printStackTrace()     // Catch:{ all -> 0x00db }
        L_0x00d1:
            com.flurry.sdk.ea.a(r1)
            goto L_0x00b3
        L_0x00d5:
            r10 = move-exception
            r2 = r1
        L_0x00d7:
            r10.printStackTrace()     // Catch:{ all -> 0x00db }
            goto L_0x00d1
        L_0x00db:
            r10 = move-exception
            r3 = r1
        L_0x00dd:
            com.flurry.sdk.ea.a(r3)
            com.flurry.sdk.ea.a(r2)
            goto L_0x00e5
        L_0x00e4:
            throw r10
        L_0x00e5:
            goto L_0x00e4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.x.c(java.util.Map):void");
    }

    private static void d(Map<String, String> map) {
        map.put("application.state", Integer.toString(n.a().i.d().d));
    }

    private static void e(Map<String, String> map) {
        long j;
        long j2;
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        if (Build.VERSION.SDK_INT >= 18) {
            j = statFs.getBlockSizeLong();
        } else {
            j = (long) statFs.getBlockSize();
        }
        if (Build.VERSION.SDK_INT >= 18) {
            j2 = statFs.getAvailableBlocksLong();
        } else {
            j2 = (long) statFs.getAvailableBlocks();
        }
        map.put("disk.size.free", Long.toString(j2 * j));
    }

    private static void f(Map<String, String> map) {
        long j;
        long j2;
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        if (Build.VERSION.SDK_INT >= 18) {
            j = statFs.getBlockSizeLong();
        } else {
            j = (long) statFs.getBlockSize();
        }
        if (Build.VERSION.SDK_INT >= 18) {
            j2 = statFs.getBlockCountLong();
        } else {
            j2 = (long) statFs.getBlockCount();
        }
        map.put("disk.size.total", Long.toString(j2 * j));
    }

    private static void g(Map<String, String> map) {
        n.a();
        map.put("net.status", Integer.toString(av.d().ordinal()));
    }

    private static void h(Map<String, String> map) {
        map.put("orientation", Integer.toString(n.a().c.b));
    }
}
