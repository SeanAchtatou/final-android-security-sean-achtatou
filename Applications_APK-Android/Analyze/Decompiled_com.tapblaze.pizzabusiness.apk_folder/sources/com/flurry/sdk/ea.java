package com.flurry.sdk;

import android.os.Build;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Base64;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

public final class ea {
    public static void a() {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            throw new IllegalStateException("Must be called from a background thread!");
        }
    }

    public static String a(String str) {
        if (str == null) {
            return "";
        }
        if (str.length() <= 255) {
            return str;
        }
        return str.substring(0, 255);
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
                n.a().p.a("GeneralUtilException", "Exception caught when close file", th);
            }
        }
    }

    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return Base64.encodeToString(str.getBytes("UTF-8"), 2);
        } catch (UnsupportedEncodingException e) {
            da.a(5, "GeneralUtil", "Unsupported UTF-8: " + e.getMessage());
            return "";
        }
    }

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        for (byte b : bArr) {
            sb.append(cArr[(byte) ((b & 240) >> 4)]);
            sb.append(cArr[(byte) (b & 15)]);
        }
        return sb.toString();
    }

    public static byte[] c(String str) {
        byte[] bArr = new byte[(str.length() / 2)];
        char[] charArray = str.toCharArray();
        for (int i = 0; i < charArray.length; i += 2) {
            StringBuilder sb = new StringBuilder(2);
            sb.append(charArray[i]);
            sb.append(charArray[i + 1]);
            bArr[i / 2] = (byte) Integer.parseInt(sb.toString(), 16);
        }
        return bArr;
    }

    public static boolean a(int i) {
        return Build.VERSION.SDK_INT >= i;
    }

    public static String d(String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException unused) {
            da.a(5, "GeneralUtil", "Cannot decode '" + str + "'");
            return "";
        }
    }

    public static long e(String str) {
        if (str == null) {
            return 0;
        }
        long j = 1125899906842597L;
        int length = str.length();
        for (int i = 0; i < length; i++) {
            j = (j * 31) + ((long) str.charAt(i));
        }
        return j;
    }

    public static long a(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[1024];
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read < 0) {
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    public static boolean a(String str, String str2) {
        if (str2.equals("12.1.0")) {
            return true;
        }
        da.e("GeneralUtil", "Flurry version mismatch detected: " + str + ": " + str2 + ", flurryAnalytics: " + "12.1.0");
        da.e("GeneralUtil", "Please use same version name for all flurry modules");
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.sdk.ff.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.flurry.sdk.ff.b(java.lang.String, int):int
      com.flurry.sdk.ff.b(java.lang.String, java.lang.String):java.lang.String
      com.flurry.sdk.ff.b(java.lang.String, long):long */
    public static void a(int i, String str, String str2, boolean z, boolean z2) {
        String d = n.a().k.d();
        long b = ff.b("last_streaming_session_id", Long.MIN_VALUE);
        HashMap hashMap = new HashMap();
        hashMap.put("data.source", z ? "streaming" : "legacy");
        hashMap.put("status.code", String.valueOf(i));
        hashMap.put("message", str);
        hashMap.put("report.identifier", str2);
        hashMap.put("current.streaming.session.id", d);
        if (b != Long.MIN_VALUE) {
            hashMap.put("last.streaming.session.id", String.valueOf(b));
        }
        bg bgVar = n.a().p;
        StringBuilder sb = new StringBuilder("Server Error in ");
        sb.append(z2 ? "current session" : "previous session");
        bgVar.a(sb.toString(), hashMap);
    }
}
