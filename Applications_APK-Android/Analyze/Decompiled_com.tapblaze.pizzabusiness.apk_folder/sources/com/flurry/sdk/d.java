package com.flurry.sdk;

import com.flurry.sdk.fz;

public final class d {
    private static String a;
    private static String b;
    private static String c;
    private static boolean d;
    private static int e;

    public static void a(int i) {
        e = i;
    }

    public static int a() {
        return e;
    }

    public static void a(String str) {
        a = str;
    }

    public static void b(String str) {
        b = str;
    }

    public static void c(String str) {
        c = str;
    }

    public static String b() {
        return c;
    }

    public static boolean c() {
        return d;
    }

    public static void a(boolean z) {
        d = z;
    }

    public static void d() {
        fz.a aVar = fz.a.REASON_FORCE_FLUSH;
        fc.a().a(ip.a(aVar.ordinal(), aVar.j));
    }
}
