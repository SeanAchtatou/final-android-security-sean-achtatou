package com.flurry.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.flurry.sdk.ao;
import com.google.android.instantapps.InstantApps;
import java.util.concurrent.Future;

public final class an extends m<am> {
    public String b;
    /* access modifiers changed from: private */
    public ap h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public o<ao> k = new o<ao>() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.flurry.sdk.an.a(com.flurry.sdk.an, java.lang.Runnable):java.util.concurrent.Future
         arg types: [com.flurry.sdk.an, com.flurry.sdk.an$1$1]
         candidates:
          com.flurry.sdk.an.a(com.flurry.sdk.an, java.lang.String):java.lang.String
          com.flurry.sdk.an.a(com.flurry.sdk.an, boolean):boolean
          com.flurry.sdk.m.a(com.flurry.sdk.m, java.lang.Runnable):java.util.concurrent.Future
          com.flurry.sdk.an.a(com.flurry.sdk.an, java.lang.Runnable):java.util.concurrent.Future */
        public final /* synthetic */ void a(Object obj) {
            final ao aoVar = (ao) obj;
            Future unused = an.this.b((Runnable) new ec() {
                public final void a() throws Exception {
                    if (an.this.j == null && aoVar.a.equals(ao.a.CREATED)) {
                        String unused = an.this.j = aoVar.c.get().getClass().getName();
                        an.this.e();
                        an.this.h.b(an.this.k);
                    }
                }
            });
        }
    };

    public an(ap apVar) {
        super("InstantAppProvider");
        this.h = apVar;
        this.h.a((o) this.k);
    }

    public final void b() {
        b(new ec() {
            public final void a() throws Exception {
                Context a2 = b.a();
                if (a2 == null) {
                    da.a(6, "InstantAppProvider", "Context is null");
                    return;
                }
                try {
                    Class.forName("com.google.android.instantapps.InstantApps");
                    boolean unused = an.this.i = InstantApps.isInstantApp(a2);
                    da.a(3, "InstantAppProvider", "isInstantApp: " + String.valueOf(an.this.i));
                } catch (ClassNotFoundException unused2) {
                    da.a(3, "InstantAppProvider", "isInstantApps dependency is not added");
                }
                an.this.e();
            }
        });
    }

    public final String d() {
        if (!this.i) {
            return null;
        }
        return !TextUtils.isEmpty(this.b) ? this.b : this.j;
    }

    public final void e() {
        if (!this.i || d() != null) {
            boolean z = this.i;
            a(new am(z, z ? d() : null));
            return;
        }
        da.a(3, "InstantAppProvider", "Fetching instant app name");
    }

    public final void c() {
        super.c();
        this.h.b(this.k);
    }
}
