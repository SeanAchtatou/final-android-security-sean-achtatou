package com.flurry.sdk;

import java.util.concurrent.ThreadFactory;

public final class dx implements ThreadFactory {
    private final ThreadGroup a;
    private final int b = 1;

    public dx(String str) {
        this.a = new ThreadGroup(str);
    }

    public final Thread newThread(Runnable runnable) {
        Thread thread = new Thread(this.a, runnable);
        thread.setName(this.a.getName() + ":" + thread.getId());
        thread.setPriority(this.b);
        return thread;
    }
}
