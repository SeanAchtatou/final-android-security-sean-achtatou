package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class ii extends jl {
    public final boolean a;

    public ii(boolean z) {
        this.a = z;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.ccpa.optout", this.a);
        return jSONObject;
    }
}
