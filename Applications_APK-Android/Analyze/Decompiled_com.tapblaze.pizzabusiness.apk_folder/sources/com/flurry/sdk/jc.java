package com.flurry.sdk;

import org.json.JSONException;
import org.json.JSONObject;

public final class jc extends jl {
    public final boolean a;

    public jc(boolean z) {
        this.a = z;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.report.previous.success", this.a);
        return jSONObject;
    }
}
