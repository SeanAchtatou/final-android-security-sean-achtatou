package com.flurry.sdk;

import com.flurry.sdk.fv;

public final class ft implements fv {
    private int h = 0;

    public final fv.a a(jp jpVar) {
        if (jpVar.a().equals(jn.FLUSH_FRAME)) {
            return new fv.a(fv.b.DO_NOT_DROP, new gw(new gx(this.h)));
        }
        if (!jpVar.a().equals(jn.ANALYTICS_ERROR)) {
            return a;
        }
        if (y.UNCAUGHT_EXCEPTION_ID.c.equals(((gl) jpVar.f()).b)) {
            return a;
        }
        int i = this.h;
        this.h = i + 1;
        if (i >= 50) {
            return g;
        }
        return a;
    }

    public final void a() {
        this.h = 0;
    }
}
