package com.flurry.sdk;

import com.flurry.sdk.ce;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import javax.net.ssl.SSLException;

public abstract class cr implements Runnable {
    protected String a;
    protected ce b;
    protected String c;
    protected String d;
    protected String e;
    protected String f;
    protected String g;
    public String h;

    /* access modifiers changed from: protected */
    public abstract InputStream a() throws IOException;

    /* access modifiers changed from: protected */
    public boolean a(String str) {
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    public boolean c() {
        return true;
    }

    public void run() {
        this.b = ce.a;
        InputStream inputStream = null;
        try {
            inputStream = a();
            if (this.b != ce.a) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                        da.a(5, "Transport", e2.getMessage(), e2);
                    }
                }
                b();
            } else if (inputStream == null) {
                da.b("Transport", "Null InputStream");
                this.b = new ce(ce.a.IO, "Null InputStream");
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e3) {
                        da.a(5, "Transport", e3.getMessage(), e3);
                    }
                }
                b();
            } else {
                ReadableByteChannel newChannel = Channels.newChannel(inputStream);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                WritableByteChannel newChannel2 = Channels.newChannel(byteArrayOutputStream);
                ByteBuffer allocateDirect = ByteBuffer.allocateDirect(16384);
                while (true) {
                    if (newChannel.read(allocateDirect) < 0) {
                        if (allocateDirect.position() <= 0) {
                            break;
                        }
                    }
                    allocateDirect.flip();
                    newChannel2.write(allocateDirect);
                    allocateDirect.compact();
                }
                byteArrayOutputStream.flush();
                if (!a(byteArrayOutputStream.toString())) {
                    this.b = new ce(ce.a.AUTHENTICATE, "Signature Error.");
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e4) {
                            da.a(5, "Transport", e4.getMessage(), e4);
                        }
                    }
                    b();
                    return;
                }
                this.h = new String(byteArrayOutputStream.toByteArray(), "utf-8");
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e5) {
                        da.a(5, "Transport", e5.getMessage(), e5);
                    }
                }
                b();
            }
        } catch (MalformedURLException e6) {
            this.b = new ce(ce.a.OTHER, e6.toString());
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e7) {
                    da.a(5, "Transport", e7.getMessage(), e7);
                }
            }
            b();
        } catch (SSLException e8) {
            da.a("Transport", e8.getMessage(), e8);
            this.b = new ce(ce.a.UNKNOWN_CERTIFICATE, e8.toString());
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e9) {
                    da.a(5, "Transport", e9.getMessage(), e9);
                }
            }
            b();
        } catch (IOException e10) {
            da.a("Transport", e10.getMessage(), e10);
            this.b = new ce(ce.a.IO, e10.toString());
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e11) {
                    da.a(5, "Transport", e11.getMessage(), e11);
                }
            }
            b();
        } catch (Exception e12) {
            this.b = new ce(ce.a.OTHER, e12.toString());
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e13) {
                    da.a(5, "Transport", e13.getMessage(), e13);
                }
            }
            b();
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e14) {
                    da.a(5, "Transport", e14.getMessage(), e14);
                }
            }
            b();
            throw th;
        }
    }

    public final String d() {
        return this.c;
    }

    public final String e() {
        return this.d;
    }

    public final String f() {
        return this.f;
    }

    public final String g() {
        return this.g;
    }

    public final ce h() {
        return this.b;
    }
}
