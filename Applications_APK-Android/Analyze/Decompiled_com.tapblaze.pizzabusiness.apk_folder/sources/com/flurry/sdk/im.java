package com.flurry.sdk;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class im extends jl {
    public final Map<String, String> a;

    public im(Map<String, String> map) {
        this.a = new HashMap(map);
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("fl.device.properties", eb.a(this.a));
        return jSONObject;
    }
}
