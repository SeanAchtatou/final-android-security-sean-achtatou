package com.flurry.sdk;

import com.flurry.sdk.ey;
import com.flurry.sdk.fn;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Future;

public abstract class fs extends f implements fn {
    private fn a;
    volatile int h = b.a;
    protected Queue<jp> i;
    protected fo j;

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public abstract void a(jp jpVar);

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class b extends Enum<b> {
        public static final int a = 1;
        public static final int b = 2;
        public static final int c = 3;
        public static final int d = 4;
        public static final int e = 5;
        private static final /* synthetic */ int[] f = {a, b, c, d, e};

        public static int[] a() {
            return (int[]) f.clone();
        }
    }

    fs(String str, fn fnVar) {
        super(str, ey.a(ey.a.CORE));
        this.a = fnVar;
        this.i = new LinkedList();
        this.h = b.b;
    }

    public final void a(fo foVar) {
        this.h = b.c;
        this.j = foVar;
        a();
        fn fnVar = this.a;
        if (fnVar != null) {
            fnVar.a(new a(this, (byte) 0));
            return;
        }
        if (foVar != null) {
            foVar.a();
        }
        this.h = b.d;
    }

    /* renamed from: com.flurry.sdk.fs$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[b.a().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0019 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0021 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0029 */
        static {
            /*
                int[] r0 = com.flurry.sdk.fs.b.a()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.flurry.sdk.fs.AnonymousClass1.a = r0
                r0 = 1
                int[] r1 = com.flurry.sdk.fs.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0011 }
                int r2 = com.flurry.sdk.fs.b.a     // Catch:{ NoSuchFieldError -> 0x0011 }
                int r2 = r2 - r0
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0011 }
            L_0x0011:
                int[] r1 = com.flurry.sdk.fs.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0019 }
                int r2 = com.flurry.sdk.fs.b.e     // Catch:{ NoSuchFieldError -> 0x0019 }
                int r2 = r2 - r0
                r3 = 2
                r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x0019 }
            L_0x0019:
                int[] r1 = com.flurry.sdk.fs.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0021 }
                int r2 = com.flurry.sdk.fs.b.b     // Catch:{ NoSuchFieldError -> 0x0021 }
                int r2 = r2 - r0
                r3 = 3
                r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x0021 }
            L_0x0021:
                int[] r1 = com.flurry.sdk.fs.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0029 }
                int r2 = com.flurry.sdk.fs.b.c     // Catch:{ NoSuchFieldError -> 0x0029 }
                int r2 = r2 - r0
                r3 = 4
                r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x0029 }
            L_0x0029:
                int[] r1 = com.flurry.sdk.fs.AnonymousClass1.a     // Catch:{ NoSuchFieldError -> 0x0031 }
                int r2 = com.flurry.sdk.fs.b.d     // Catch:{ NoSuchFieldError -> 0x0031 }
                int r2 = r2 - r0
                r0 = 5
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0031 }
            L_0x0031:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.fs.AnonymousClass1.<clinit>():void");
        }
    }

    public final fn.a c(jp jpVar) {
        fn.a aVar = fn.a.ERROR;
        int i2 = AnonymousClass1.a[this.h - 1];
        if (i2 == 1 || i2 == 2) {
            return aVar;
        }
        if (i2 == 3 || i2 == 4) {
            fn.a aVar2 = fn.a.DEFERRED;
            this.i.add(jpVar);
            da.a(4, "StreamingCoreModule", "Adding frame to deferred queue:" + jpVar.e());
            return aVar2;
        } else if (i2 != 5) {
            return aVar;
        } else {
            fn.a aVar3 = fn.a.QUEUED;
            a(jpVar);
            return aVar3;
        }
    }

    public final void d(jp jpVar) {
        fn fnVar = this.a;
        if (fnVar != null) {
            fn.a c = fnVar.c(jpVar);
            da.a(4, "StreamingCoreModule", "Enqueue message status for module: " + this.a + " is: " + c);
        }
    }

    public fn.a b(jp jpVar) {
        fn.a aVar = fn.a.ERROR;
        fn fnVar = this.a;
        return fnVar != null ? fnVar.b(jpVar) : aVar;
    }

    class a implements fo {
        private a() {
        }

        /* synthetic */ a(fs fsVar, byte b) {
            this();
        }

        public final void a() {
            Future unused = fs.this.b(new ec() {
                public final void a() {
                    fs.this.d();
                    fs.this.h = b.d;
                    Future unused = fs.this.b(new ec() {
                        public final void a() {
                            if (fs.this.j != null) {
                                fs.this.j.a();
                            }
                        }
                    });
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        while (this.i.peek() != null) {
            jp poll = this.i.poll();
            da.a(4, "StreamingCoreModule", "Processing deferred message status for module: " + poll.e());
            a(poll);
        }
    }

    public final void b() {
        c();
        fn fnVar = this.a;
        if (fnVar != null) {
            fnVar.b();
        }
    }
}
