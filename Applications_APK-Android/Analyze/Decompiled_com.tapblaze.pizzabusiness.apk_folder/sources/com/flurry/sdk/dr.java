package com.flurry.sdk;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public final class dr<ObjectType> extends dq<ObjectType> {
    public dr(dt<ObjectType> dtVar) {
        super(dtVar);
    }

    public final void a(OutputStream outputStream, ObjectType objecttype) throws IOException {
        if (outputStream != null) {
            GZIPOutputStream gZIPOutputStream = null;
            try {
                GZIPOutputStream gZIPOutputStream2 = new GZIPOutputStream(outputStream);
                try {
                    super.a(gZIPOutputStream2, objecttype);
                    ea.a(gZIPOutputStream2);
                } catch (Throwable th) {
                    th = th;
                    gZIPOutputStream = gZIPOutputStream2;
                    ea.a(gZIPOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                ea.a(gZIPOutputStream);
                throw th;
            }
        }
    }

    public final ObjectType a(InputStream inputStream) throws IOException {
        GZIPInputStream gZIPInputStream = null;
        if (inputStream == null) {
            return null;
        }
        try {
            GZIPInputStream gZIPInputStream2 = new GZIPInputStream(inputStream);
            try {
                ObjectType a = super.a(gZIPInputStream2);
                ea.a(gZIPInputStream2);
                return a;
            } catch (Throwable th) {
                th = th;
                gZIPInputStream = gZIPInputStream2;
                ea.a(gZIPInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ea.a(gZIPInputStream);
            throw th;
        }
    }
}
