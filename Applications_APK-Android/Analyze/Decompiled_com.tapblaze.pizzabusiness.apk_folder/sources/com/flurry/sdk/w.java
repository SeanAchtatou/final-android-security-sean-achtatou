package com.flurry.sdk;

import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class w {
    private static final char[] b = {'F', 'C', 'B', 'M'};
    private static final String c = new String(b);
    private static final int d;
    private static final int e;
    private static final int f;
    private static final int g;
    public ByteBuffer a;
    private short h;
    private boolean i;

    public static int b() {
        return 1;
    }

    static {
        char[] cArr = b;
        d = (cArr.length * 2) + 2 + 1 + 105984;
        int length = cArr.length * 2;
        e = length;
        int i2 = length + 2;
        f = i2;
        g = i2 + 1;
    }

    w() {
        this.a = ByteBuffer.allocateDirect(d);
        this.a.asCharBuffer().put(b);
    }

    public w(File file) {
        int i2;
        boolean z = true;
        da.a(6, "com.flurry.android.common.newProviders.errorCrashBreadcrumbsManager", String.format(Locale.getDefault(), "YCrashBreadcrumbs from %s", file.getAbsolutePath()));
        this.a = ByteBuffer.allocate(d);
        if (file.length() != ((long) this.a.capacity())) {
            da.a(6, "com.flurry.android.common.newProviders.errorCrashBreadcrumbsManager", String.format(Locale.getDefault(), "Crash breadcrumbs invalid file length %s != %s", Long.valueOf(file.length()), Integer.valueOf(this.a.capacity())));
            this.a = null;
            return;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            FileChannel channel = fileInputStream.getChannel();
            try {
                i2 = channel.read(this.a);
            } catch (IOException unused) {
                da.a(6, "com.flurry.android.common.newProviders.errorCrashBreadcrumbsManager", "Issue reading breadcrumbs from file.");
                i2 = 0;
            }
            ea.a(channel);
            ea.a(fileInputStream);
            if (i2 != this.a.capacity()) {
                da.a(6, "com.flurry.android.common.newProviders.errorCrashBreadcrumbsManager", String.format(Locale.getDefault(), "YCrashBreadcrumbs unexpected read size %s != %s", Integer.valueOf(i2), Integer.valueOf(this.a.capacity())));
                this.a = null;
                return;
            }
            this.a.position(0);
            String obj = this.a.asCharBuffer().limit(b.length).toString();
            if (!obj.equals(c)) {
                da.a(6, "com.flurry.android.common.newProviders.errorCrashBreadcrumbsManager", String.format(Locale.getDefault(), "YCrashBreadcrumbs invalid magic string: '%s'", obj));
                this.a = null;
                return;
            }
            this.h = this.a.getShort(e);
            short s = this.h;
            if (s < 0 || s >= 207) {
                da.a(6, "com.flurry.android.common.newProviders.errorCrashBreadcrumbsManager", String.format(Locale.getDefault(), "YCrashBreadcrumbs invalid index: '%s'", Short.valueOf(this.h)));
                this.a = null;
                return;
            }
            this.i = this.a.get(f) != 1 ? false : z;
        } catch (FileNotFoundException unused2) {
            da.a(6, "com.flurry.android.common.newProviders.errorCrashBreadcrumbsManager", "Issue reading breadcrumbs file.");
            this.a = null;
        }
    }

    private v a(int i2) {
        this.a.position(g + (i2 * 512));
        return new v(this.a.asCharBuffer().limit(this.a.getInt()).toString(), this.a.getLong());
    }

    public final List<v> a() {
        ArrayList arrayList = new ArrayList();
        if (this.a == null) {
            return arrayList;
        }
        if (this.i) {
            for (int i2 = this.h; i2 < 207; i2++) {
                arrayList.add(a(i2));
            }
        }
        for (int i3 = 0; i3 < this.h; i3++) {
            arrayList.add(a(i3));
        }
        return arrayList;
    }

    public final synchronized void a(v vVar) {
        String str = vVar.a;
        if (TextUtils.isEmpty(str)) {
            da.a(6, "com.flurry.android.common.newProviders.errorCrashBreadcrumbsManager", "Breadcrumb may not be null or empty.");
            return;
        }
        long j = vVar.b;
        int min = Math.min(str.length(), (int) IronSourceConstants.INTERSTITIAL_DAILY_CAPPED);
        this.a.position((this.h * 512) + g);
        this.a.putLong(j);
        this.a.putInt(min);
        this.a.asCharBuffer().put(str, 0, min);
        byte b2 = 1;
        this.h = (short) (this.h + 1);
        if (this.h >= 207) {
            this.h = 0;
            this.i = true;
        }
        this.a.putShort(e, this.h);
        ByteBuffer byteBuffer = this.a;
        int i2 = f;
        if (!this.i) {
            b2 = 0;
        }
        byteBuffer.put(i2, b2);
    }

    public final synchronized String toString() {
        short s;
        StringBuilder sb;
        if (this.a == null) {
            s = 0;
        } else {
            s = this.i ? 207 : this.h;
        }
        sb = new StringBuilder();
        sb.append("Total number of breadcrumbs: " + ((int) s) + "\n");
        for (v vVar : a()) {
            sb.append(vVar.toString());
        }
        return sb.toString();
    }
}
