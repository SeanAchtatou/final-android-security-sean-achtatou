package com.flurry.sdk;

import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.crypto.KeyGenerator;

public final class aj {
    private KeyStore a;

    aj() {
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                this.a = KeyStore.getInstance("AndroidKeyStore");
                this.a.load(null);
                if (this.a != null && !this.a.containsAlias("fl.install.id.sec.key")) {
                    KeyGenerator instance = KeyGenerator.getInstance("AES", "AndroidKeyStore");
                    instance.init(new KeyGenParameterSpec.Builder("fl.install.id.sec.key", 3).setBlockModes("CBC").setEncryptionPaddings("PKCS7Padding").setRandomizedEncryptionRequired(false).setDigests(CommonUtils.SHA256_INSTANCE, "SHA-512").build());
                    instance.generateKey();
                }
            } catch (IOException | NullPointerException | InvalidAlgorithmParameterException | KeyStoreException | NoSuchAlgorithmException | NoSuchProviderException | CertificateException e) {
                da.a(5, "KeystoreProvider", "Error while generating Key" + e.getMessage(), e);
            } catch (Exception e2) {
                da.a(5, "KeystoreProvider", "Unknown Error while generating Key" + e2.getMessage(), e2);
            }
        }
    }

    public final Key a() {
        KeyStore keyStore;
        if (Build.VERSION.SDK_INT < 23 || (keyStore = this.a) == null) {
            return null;
        }
        try {
            return keyStore.getKey("fl.install.id.sec.key", null);
        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException unused) {
            da.a(6, "KeystoreProvider", "Error in getting key.");
            return null;
        }
    }
}
