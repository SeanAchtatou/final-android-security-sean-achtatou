package com.flurry.sdk;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

public final class js {
    public static boolean a(jr jrVar, jr jrVar2) {
        if (!fh.a(jrVar, jrVar2)) {
            return false;
        }
        if (!fh.a(jrVar.a, jrVar.b, jrVar2.a, jrVar2.b)) {
            return false;
        }
        boolean c = c(jrVar, jrVar2);
        return c ? a(jrVar) : c;
    }

    public static boolean a(jr jrVar) {
        boolean delete = new File(jrVar.a, jrVar.b).delete();
        if (!delete) {
            bg bgVar = n.a().p;
            bgVar.a("Fail to delete file: " + jrVar.toString());
        }
        return delete;
    }

    private static boolean c(jr jrVar, jr jrVar2) {
        FileChannel fileChannel;
        FileChannel fileChannel2 = null;
        try {
            File file = new File(jrVar.a, jrVar.b);
            File file2 = new File(jrVar2.a, jrVar2.b);
            file2.getParentFile().mkdirs();
            file2.delete();
            FileChannel channel = new FileInputStream(file).getChannel();
            try {
                fileChannel2 = new FileOutputStream(file2).getChannel();
                fileChannel2.transferFrom(channel, 0, channel.size());
                ea.a(channel);
                ea.a(fileChannel2);
                return true;
            } catch (Exception e) {
                e = e;
                FileChannel fileChannel3 = channel;
                fileChannel = fileChannel2;
                fileChannel2 = fileChannel3;
                try {
                    da.a(6, "FileProcessor", "Copy file failed. " + e.getMessage());
                    n.a().p.a("FileProcessorUtilException", "Exception caught when copy file from: " + jrVar.toString() + " to: " + jrVar2.toString(), e);
                    ea.a(fileChannel2);
                    ea.a(fileChannel);
                    return false;
                } catch (Throwable th) {
                    th = th;
                    ea.a(fileChannel2);
                    ea.a(fileChannel);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                FileChannel fileChannel4 = channel;
                fileChannel = fileChannel2;
                fileChannel2 = fileChannel4;
                ea.a(fileChannel2);
                ea.a(fileChannel);
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            fileChannel = null;
            da.a(6, "FileProcessor", "Copy file failed. " + e.getMessage());
            n.a().p.a("FileProcessorUtilException", "Exception caught when copy file from: " + jrVar.toString() + " to: " + jrVar2.toString(), e);
            ea.a(fileChannel2);
            ea.a(fileChannel);
            return false;
        } catch (Throwable th3) {
            th = th3;
            fileChannel = null;
            ea.a(fileChannel2);
            ea.a(fileChannel);
            throw th;
        }
    }

    public static boolean a(File file, File file2) {
        FileChannel fileChannel;
        FileChannel fileChannel2 = null;
        try {
            file2.getParentFile().mkdirs();
            file2.delete();
            file2.createNewFile();
            FileChannel channel = new FileInputStream(file).getChannel();
            try {
                fileChannel = new FileOutputStream(file2).getChannel();
            } catch (Exception e) {
                fileChannel2 = channel;
                e = e;
                fileChannel = null;
                try {
                    da.a(6, "FileProcessor", "Copy file failed. " + e.getMessage());
                    ea.a(fileChannel2);
                    ea.a(fileChannel);
                    return false;
                } catch (Throwable th) {
                    th = th;
                    ea.a(fileChannel2);
                    ea.a(fileChannel);
                    throw th;
                }
            } catch (Throwable th2) {
                fileChannel2 = channel;
                th = th2;
                fileChannel = null;
                ea.a(fileChannel2);
                ea.a(fileChannel);
                throw th;
            }
            try {
                fileChannel.transferFrom(channel, 0, channel.size());
                ea.a(channel);
                ea.a(fileChannel);
                return true;
            } catch (Exception e2) {
                Exception exc = e2;
                fileChannel2 = channel;
                e = exc;
                da.a(6, "FileProcessor", "Copy file failed. " + e.getMessage());
                ea.a(fileChannel2);
                ea.a(fileChannel);
                return false;
            } catch (Throwable th3) {
                Throwable th4 = th3;
                fileChannel2 = channel;
                th = th4;
                ea.a(fileChannel2);
                ea.a(fileChannel);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            fileChannel = null;
            da.a(6, "FileProcessor", "Copy file failed. " + e.getMessage());
            ea.a(fileChannel2);
            ea.a(fileChannel);
            return false;
        } catch (Throwable th5) {
            th = th5;
            fileChannel = null;
            ea.a(fileChannel2);
            ea.a(fileChannel);
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static boolean b(jr jrVar, jr jrVar2) {
        FileChannel fileChannel;
        FileChannel fileChannel2 = null;
        try {
            File file = new File(jrVar.a, jrVar.b);
            FileChannel channel = new FileInputStream(new File(jrVar2.a, jrVar2.b)).getChannel();
            try {
                fileChannel2 = new FileOutputStream(file, true).getChannel();
                fileChannel2.transferFrom(channel, fileChannel2.size(), channel.size());
                ea.a(channel);
                ea.a(fileChannel2);
                return true;
            } catch (Exception e) {
                e = e;
                fileChannel = fileChannel2;
                fileChannel2 = channel;
                try {
                    n.a().p.a("FileProcessorUtilException", "Exception caught when append file: " + jrVar2.toString() + " to: " + jrVar.toString(), e);
                    ea.a(fileChannel2);
                    ea.a(fileChannel);
                    return false;
                } catch (Throwable th) {
                    th = th;
                    ea.a(fileChannel2);
                    ea.a(fileChannel);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileChannel = fileChannel2;
                fileChannel2 = channel;
                ea.a(fileChannel2);
                ea.a(fileChannel);
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            fileChannel = null;
            n.a().p.a("FileProcessorUtilException", "Exception caught when append file: " + jrVar2.toString() + " to: " + jrVar.toString(), e);
            ea.a(fileChannel2);
            ea.a(fileChannel);
            return false;
        } catch (Throwable th3) {
            th = th3;
            fileChannel = null;
            ea.a(fileChannel2);
            ea.a(fileChannel);
            throw th;
        }
    }
}
