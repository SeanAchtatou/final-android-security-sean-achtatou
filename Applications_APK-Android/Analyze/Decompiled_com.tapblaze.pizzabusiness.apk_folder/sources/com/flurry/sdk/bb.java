package com.flurry.sdk;

import com.flurry.android.FlurryAgentListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public final class bb extends m<ba> {
    public AtomicLong b = new AtomicLong(0);
    public AtomicLong h = new AtomicLong(0);
    public AtomicBoolean i = new AtomicBoolean(true);
    public long j;
    public List<FlurryAgentListener> k = new ArrayList();
    /* access modifiers changed from: private */
    public long l;
    private q m;
    private o<r> n = new o<r>() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.flurry.sdk.bb.a(com.flurry.sdk.bd, boolean):void
         arg types: [com.flurry.sdk.bd, int]
         candidates:
          com.flurry.sdk.bb.a(com.flurry.sdk.bb, long):long
          com.flurry.sdk.m.a(com.flurry.sdk.m, java.lang.Runnable):java.util.concurrent.Future
          com.flurry.sdk.bb.a(com.flurry.sdk.bd, boolean):void */
        public final /* synthetic */ void a(Object obj) {
            int i = AnonymousClass7.a[((r) obj).b.ordinal()];
            if (i == 1) {
                bb.this.a(bd.FOREGROUND, false);
            } else if (i == 2) {
                bb.this.b(bd.FOREGROUND, false);
            }
        }
    };

    /* renamed from: com.flurry.sdk.bb$7  reason: invalid class name */
    static /* synthetic */ class AnonymousClass7 {
        static final /* synthetic */ int[] a = new int[p.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.flurry.sdk.p[] r0 = com.flurry.sdk.p.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.flurry.sdk.bb.AnonymousClass7.a = r0
                int[] r0 = com.flurry.sdk.bb.AnonymousClass7.a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.flurry.sdk.p r1 = com.flurry.sdk.p.FOREGROUND     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.flurry.sdk.bb.AnonymousClass7.a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.flurry.sdk.p r1 = com.flurry.sdk.p.BACKGROUND     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.bb.AnonymousClass7.<clinit>():void");
        }
    }

    public bb(q qVar) {
        super("ReportingProvider");
        this.m = qVar;
        this.m.a(this.n);
        b(new ec() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.flurry.sdk.ff.b(java.lang.String, long):long
             arg types: [java.lang.String, int]
             candidates:
              com.flurry.sdk.ff.b(java.lang.String, int):int
              com.flurry.sdk.ff.b(java.lang.String, java.lang.String):java.lang.String
              com.flurry.sdk.ff.b(java.lang.String, long):long */
            public final void a() throws Exception {
                long unused = bb.this.l = ff.b("initial_run_time", Long.MIN_VALUE);
            }
        });
    }

    public final void a(FlurryAgentListener flurryAgentListener) {
        if (flurryAgentListener == null) {
            da.a(2, "ReportingProvider", "Cannot register with null listener");
        } else {
            this.k.add(flurryAgentListener);
        }
    }

    public final String d() {
        return String.valueOf(this.b.get());
    }

    public final void a(final bd bdVar, final boolean z) {
        b(new ec() {
            public final void a() throws Exception {
                da.a(3, "ReportingProvider", "Start session: " + bdVar.name() + ", isManualSession: " + z);
                bb.a(bb.this, bdVar, bc.SESSION_START, z);
            }
        });
    }

    public final void b(final bd bdVar, final boolean z) {
        b(new ec() {
            public final void a() throws Exception {
                da.a(3, "ReportingProvider", "End session: " + bdVar.name() + ", isManualSession: " + z);
                bb.a(bb.this, bdVar, bc.SESSION_END, z);
            }
        });
    }

    public final void c() {
        super.c();
        this.m.b(this.n);
    }

    static /* synthetic */ void a(bb bbVar, bd bdVar, bc bcVar, boolean z) {
        long j2;
        long currentTimeMillis = System.currentTimeMillis();
        if (bbVar.l == Long.MIN_VALUE) {
            bbVar.l = currentTimeMillis;
            ff.a("initial_run_time", bbVar.l);
            da.a(3, "ReportingProvider", "Refresh initial timestamp");
        }
        if (bdVar.equals(bd.FOREGROUND)) {
            j2 = bbVar.j;
        } else {
            j2 = 60000;
        }
        bd bdVar2 = bdVar;
        bbVar.a(new ba(bdVar2, currentTimeMillis, bbVar.l, j2, bcVar, z));
    }
}
