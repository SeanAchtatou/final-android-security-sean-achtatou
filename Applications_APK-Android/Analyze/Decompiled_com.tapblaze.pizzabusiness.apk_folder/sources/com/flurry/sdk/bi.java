package com.flurry.sdk;

import java.util.concurrent.Future;

public final class bi extends m<bh> {
    /* access modifiers changed from: private */
    public am b;
    private bb h;
    private an i;
    private o<ba> j = new o<ba>() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.flurry.sdk.bi.a(com.flurry.sdk.bi, java.lang.Runnable):java.util.concurrent.Future
         arg types: [com.flurry.sdk.bi, com.flurry.sdk.bi$1$1]
         candidates:
          com.flurry.sdk.bi.a(com.flurry.sdk.bi, com.flurry.sdk.am):com.flurry.sdk.am
          com.flurry.sdk.bi.a(com.flurry.sdk.bi, java.lang.Object):void
          com.flurry.sdk.m.a(com.flurry.sdk.m, java.lang.Runnable):java.util.concurrent.Future
          com.flurry.sdk.bi.a(com.flurry.sdk.bi, java.lang.Runnable):java.util.concurrent.Future */
        public final /* synthetic */ void a(Object obj) {
            final ba baVar = (ba) obj;
            Future unused = bi.this.b((Runnable) new ec() {
                public final void a() throws Exception {
                    bh bhVar;
                    if (baVar.e.equals(bc.SESSION_START)) {
                        bhVar = new bh(true, bi.this.b);
                    } else {
                        bhVar = new bh(false, bi.this.b);
                    }
                    bi.this.a(bhVar);
                }
            });
        }
    };
    private o<am> k = new o<am>() {
        public final /* synthetic */ void a(Object obj) {
            final am amVar = (am) obj;
            Future unused = bi.this.b(new ec() {
                public final void a() throws Exception {
                    da.a(3, "SessionPropertyProvider", "Receive instant app data");
                    am unused = bi.this.b = amVar;
                }
            });
        }
    };

    public bi(bb bbVar, an anVar) {
        super("SessionPropertyProvider");
        this.h = bbVar;
        this.h.a((o) this.j);
        this.i = anVar;
        this.i.a((o) this.k);
    }

    public final void c() {
        super.c();
        this.h.b(this.j);
        this.i.b(this.k);
    }
}
