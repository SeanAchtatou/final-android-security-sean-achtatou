package com.flurry.sdk;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.flurry.android.FlurryAgentListener;
import com.flurry.sdk.fz;
import com.flurry.sdk.gk;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ga implements fz {
    Map<jn, jp> a;
    long b = Long.MIN_VALUE;
    long c = Long.MIN_VALUE;
    long d = Long.MIN_VALUE;
    int e = bd.BACKGROUND.d;
    private AtomicBoolean f;
    private fy g;
    private boolean h = false;
    private Timer i = null;
    private TimerTask j = null;
    private b k = b.INACTIVE;

    enum b {
        INACTIVE,
        FOREGROUND_RUNNING,
        FOREGROUND_ENDING,
        BACKGROUND_RUNNING,
        BACKGROUND_ENDING
    }

    static boolean a(long j2) {
        return j2 > 0;
    }

    public ga(fy fyVar) {
        this.g = fyVar;
        if (this.a == null) {
            this.a = new HashMap();
        }
        this.a.clear();
        this.a.put(jn.SESSION_INFO, null);
        this.a.put(jn.APP_STATE, null);
        this.a.put(jn.APP_INFO, null);
        this.a.put(jn.REPORTED_ID, null);
        this.a.put(jn.DEVICE_PROPERTIES, null);
        this.a.put(jn.SESSION_ID, null);
        this.a = this.a;
        this.f = new AtomicBoolean(false);
    }

    public final void a() {
        e();
    }

    public final void a(jp jpVar) {
        if (jpVar.a().equals(jn.FLUSH_FRAME)) {
            iq iqVar = (iq) jpVar.f();
            if (!fz.a.REASON_SESSION_FINALIZE.j.equals(iqVar.b)) {
                if (!fz.a.REASON_STICKY_SET_COMPLETE.j.equals(iqVar.b)) {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    a(this.c, elapsedRealtime, "Flush In Middle");
                    b(jh.a(this.b, this.c, elapsedRealtime, this.e));
                }
                jp jpVar2 = this.a.get(jn.SESSION_ID);
                if (jpVar2 != null) {
                    c(jpVar2);
                    return;
                }
                return;
            }
            return;
        }
        if (jpVar.a().equals(jn.REPORTING)) {
            gz gzVar = (gz) jpVar.f();
            int i2 = AnonymousClass2.a[this.k.ordinal()];
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 != 3) {
                        if (i2 != 4) {
                            if (i2 != 5) {
                                da.a(6, "SessionRule", "Unreachable Code");
                            } else if (b(gzVar)) {
                                this.h = gzVar.f;
                                a(b.FOREGROUND_RUNNING);
                                a(gzVar);
                            } else if (c(gzVar)) {
                                a(b.BACKGROUND_RUNNING);
                                a(gzVar);
                            }
                        } else if (b(gzVar)) {
                            e();
                            a(b.FOREGROUND_RUNNING);
                            a(gzVar);
                        } else if (c(gzVar)) {
                            b();
                            this.d = Long.MIN_VALUE;
                            a(b.BACKGROUND_RUNNING);
                        }
                    } else if (b(gzVar)) {
                        e();
                        a(b.FOREGROUND_RUNNING);
                        a(gzVar);
                    } else {
                        if (gzVar.a.equals(bd.BACKGROUND) && gzVar.e.equals(bc.SESSION_END)) {
                            b(gzVar.d);
                            a(b.BACKGROUND_ENDING);
                        }
                    }
                } else if (b(gzVar)) {
                    b();
                    this.d = Long.MIN_VALUE;
                    a(b.FOREGROUND_RUNNING);
                }
            } else if (!b(gzVar)) {
                if ((gzVar.a.equals(bd.FOREGROUND) && gzVar.e.equals(bc.SESSION_END)) && (!this.h || gzVar.f)) {
                    b(gzVar.d);
                    a(b.FOREGROUND_ENDING);
                }
            } else if (this.h && !gzVar.f) {
                this.h = false;
            }
        }
        if (jpVar.a().equals(jn.ANALYTICS_ERROR) && ((gl) jpVar.f()).g == gk.a.UNRECOVERABLE_CRASH.d) {
            b();
            this.d = SystemClock.elapsedRealtime();
            if (a(this.b)) {
                a(this.c, this.d, "Process Crash");
                b(jh.a(this.b, this.c, this.d, this.e));
            } else {
                da.a(6, "SessionRule", "Session id is invalid. Not appending this session id frame.");
            }
        }
        if (jpVar.a().equals(jn.CCPA_DELETION)) {
            fz.a aVar = fz.a.REASON_DATA_DELETION;
            c(ip.a(aVar.ordinal(), aVar.j));
        }
        jn a2 = jpVar.a();
        if (this.a.containsKey(a2)) {
            da.a(3, "SessionRule", "Adding Sticky Frame:" + jpVar.e());
            this.a.put(a2, jpVar);
        }
        if (!this.f.get() && d()) {
            this.f.set(true);
            fz.a aVar2 = fz.a.REASON_STICKY_SET_COMPLETE;
            c(ip.a(aVar2.ordinal(), aVar2.j));
            int b2 = ff.b("last_streaming_http_error_code", Integer.MIN_VALUE);
            String b3 = ff.b("last_streaming_http_error_message", "");
            String b4 = ff.b("last_streaming_http_report_identifier", "");
            if (b2 != Integer.MIN_VALUE) {
                ea.a(b2, b3, b4, true, false);
                ff.a("last_streaming_http_error_code");
                ff.a("last_streaming_http_error_message");
                ff.a("last_streaming_http_report_identifier");
            }
            int b5 = ff.b("last_legacy_http_error_code", Integer.MIN_VALUE);
            String b6 = ff.b("last_legacy_http_error_message", "");
            String b7 = ff.b("last_legacy_http_report_identifier", "");
            if (b5 != Integer.MIN_VALUE) {
                ea.a(b5, b6, b7, false, false);
                ff.a("last_legacy_http_error_code");
                ff.a("last_legacy_http_error_message");
                ff.a("last_legacy_http_report_identifier");
            }
            ff.a("last_streaming_session_id", this.b);
            HashMap hashMap = new HashMap();
            hashMap.put("streaming.session.id", String.valueOf(this.b));
            n.a().p.a("Session Ids", hashMap);
        } else if (this.f.get() && jpVar.a().equals(jn.NOTIFICATION)) {
            n.a().p.a("Flush Token Refreshed");
            fz.a aVar3 = fz.a.REASON_PUSH_TOKEN_REFRESH;
            c(ip.a(aVar3.ordinal(), aVar3.j));
        }
    }

    /* renamed from: com.flurry.sdk.ga$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] a = new int[b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.flurry.sdk.ga$b[] r0 = com.flurry.sdk.ga.b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.flurry.sdk.ga.AnonymousClass2.a = r0
                int[] r0 = com.flurry.sdk.ga.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.flurry.sdk.ga$b r1 = com.flurry.sdk.ga.b.FOREGROUND_RUNNING     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.flurry.sdk.ga.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.flurry.sdk.ga$b r1 = com.flurry.sdk.ga.b.FOREGROUND_ENDING     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.flurry.sdk.ga.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.flurry.sdk.ga$b r1 = com.flurry.sdk.ga.b.BACKGROUND_RUNNING     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.flurry.sdk.ga.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.flurry.sdk.ga$b r1 = com.flurry.sdk.ga.b.BACKGROUND_ENDING     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.flurry.sdk.ga.AnonymousClass2.a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.flurry.sdk.ga$b r1 = com.flurry.sdk.ga.b.INACTIVE     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.ga.AnonymousClass2.<clinit>():void");
        }
    }

    private void a(b bVar) {
        if (this.k.equals(bVar)) {
            da.a(3, "SessionRule", "Invalid state transition.");
            return;
        }
        da.a(3, "SessionRule", "Previous session state: " + this.k.name());
        this.k = bVar;
        da.a(3, "SessionRule", "Current session state: " + this.k.name());
    }

    private boolean d() {
        boolean z = true;
        for (Map.Entry<jn, jp> value : this.a.entrySet()) {
            if (value.getValue() == null) {
                z = false;
            }
        }
        return z;
    }

    private void e() {
        if (this.b <= 0) {
            da.a(6, "SessionRule", "Finalize session " + this.b);
            return;
        }
        b();
        this.d = SystemClock.elapsedRealtime();
        if (a(this.b)) {
            b(jh.a(this.b, this.c, this.d, this.e));
        } else {
            da.a(6, "SessionRule", "Session id is invalid. Not appending this session id frame.");
        }
        fz.a aVar = fz.a.REASON_SESSION_FINALIZE;
        b(ip.a(aVar.ordinal(), aVar.j));
        a(false);
        c();
    }

    private void a(gz gzVar) {
        if (!gzVar.e.equals(bc.SESSION_START)) {
            da.a(3, "SessionRule", "Only generate session id during session start");
        } else if (this.b == Long.MIN_VALUE && this.a.get(jn.SESSION_ID) == null) {
            da.a(3, "SessionRule", "Generating Session Id:" + gzVar.b);
            this.b = gzVar.b;
            this.c = SystemClock.elapsedRealtime();
            this.e = gzVar.a.d == 1 ? 2 : 0;
            if (a(this.b)) {
                a(this.c, this.d, "Generate Session Id");
                c(jh.a(this.b, this.c, this.d, this.e));
            } else {
                da.a(6, "SessionRule", "Session id is invalid. Not appending this session id frame.");
            }
            a(true);
        }
    }

    private void c(jp jpVar) {
        if (this.g != null) {
            da.a(3, "SessionRule", "Appending Frame:" + jpVar.e());
            this.g.a(jpVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(jp jpVar) {
        if (this.g != null) {
            da.a(3, "SessionRule", "Forwarding Frame:" + jpVar.e());
            this.g.b(jpVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b() {
        if (this.i != null) {
            this.i.cancel();
            this.i = null;
        }
        if (this.j != null) {
            this.j.cancel();
            this.j = null;
        }
    }

    public class a extends TimerTask {
        protected a() {
        }

        public final void run() {
            ga.this.b();
            ga gaVar = ga.this;
            if (gaVar.d <= 0) {
                gaVar.d = SystemClock.elapsedRealtime();
            }
            if (ga.a(gaVar.b)) {
                gaVar.b(jh.a(gaVar.b, gaVar.c, gaVar.d, gaVar.e));
            } else {
                da.a(6, "SessionRule", "Session id is invalid. Not appending this session id frame.");
            }
            fz.a aVar = fz.a.REASON_SESSION_FINALIZE;
            gaVar.b(ip.a(aVar.ordinal(), aVar.j));
            gaVar.a(false);
            gaVar.c();
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        da.a(3, "SessionRule", "Reset session rule");
        this.a.put(jn.SESSION_ID, null);
        this.f.set(false);
        this.b = Long.MIN_VALUE;
        this.c = Long.MIN_VALUE;
        this.d = Long.MIN_VALUE;
        this.k = b.INACTIVE;
        this.h = false;
    }

    private static boolean b(gz gzVar) {
        return gzVar.a.equals(bd.FOREGROUND) && gzVar.e.equals(bc.SESSION_START);
    }

    private static boolean c(gz gzVar) {
        return gzVar.a.equals(bd.BACKGROUND) && gzVar.e.equals(bc.SESSION_START);
    }

    /* access modifiers changed from: package-private */
    public final void a(final boolean z) {
        fy fyVar = this.g;
        if (fyVar != null) {
            fyVar.a(new ec() {
                public final void a() throws Exception {
                    if (z) {
                        bb bbVar = n.a().k;
                        long j = ga.this.b;
                        long j2 = ga.this.c;
                        bbVar.b.set(j);
                        bbVar.h.set(j2);
                        if (!bbVar.k.isEmpty()) {
                            new Handler(Looper.getMainLooper()).post(new ec(new ArrayList(bbVar.k)) {
                                final /* synthetic */ List a;

                                {
                                    this.a = r2;
                                }

                                public final void a() {
                                    for (FlurryAgentListener flurryAgentListener : this.a) {
                                        if (flurryAgentListener != null) {
                                            flurryAgentListener.onSessionStarted();
                                        }
                                    }
                                }
                            });
                        }
                    }
                    bb bbVar2 = n.a().k;
                    bbVar2.i.set(z);
                }
            });
        }
    }

    private static void a(long j2, long j3, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("fl.session.elapsed.start.time", String.valueOf(j2));
        if (j3 != Long.MIN_VALUE) {
            hashMap.put("fl.session.elapsed.end.time", String.valueOf(j3));
            hashMap.put("fl.session.duration", String.valueOf(j3 - j2));
        }
        hashMap.put("fl.session.message", str);
        n.a().p.a("Session Duration", hashMap);
    }

    private void b(long j2) {
        b();
        this.d = SystemClock.elapsedRealtime();
        if (a(this.b)) {
            a(this.c, this.d, "Start Session Finalize Timer");
            c(jh.a(this.b, this.c, this.d, this.e));
        } else {
            da.a(6, "SessionRule", "Session id is invalid. Not appending this session id frame.");
        }
        c(j2);
    }

    private synchronized void c(long j2) {
        if (this.i != null) {
            b();
        }
        this.i = new Timer("FlurrySessionTimer");
        this.j = new a();
        this.i.schedule(this.j, j2);
    }
}
