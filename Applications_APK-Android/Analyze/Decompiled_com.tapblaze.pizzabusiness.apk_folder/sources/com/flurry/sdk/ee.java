package com.flurry.sdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import androidx.browser.customtabs.CustomTabsIntent;
import com.google.android.gms.drive.DriveFile;

public final class ee {
    private static Boolean a;

    public interface a {
        void a(Context context);
    }

    public static void a(Context context, CustomTabsIntent customTabsIntent, Uri uri, a aVar) {
        if (!a(context)) {
            aVar.a(context);
            return;
        }
        if (Build.VERSION.SDK_INT >= 22) {
            Intent intent = customTabsIntent.intent;
            intent.putExtra("android.intent.extra.REFERRER", Uri.parse("2//" + context.getPackageName()));
        }
        String a2 = ef.a(context);
        customTabsIntent.intent.setFlags(DriveFile.MODE_READ_ONLY);
        customTabsIntent.intent.setPackage(a2);
        customTabsIntent.launchUrl(context, uri);
    }

    public static boolean a(Context context) {
        Boolean bool = a;
        if (bool != null) {
            return bool.booleanValue();
        }
        a = Boolean.TRUE;
        try {
            Class.forName("androidx.browser.customtabs.CustomTabsClient");
        } catch (ClassNotFoundException unused) {
            da.e("CustomTabsHelper", "Couldn't find Chrome Custom Tab dependency. For better user experience include Chrome Custom Tab dependency in gradle");
            a = Boolean.FALSE;
        }
        Boolean valueOf = Boolean.valueOf(a.booleanValue() && ef.a(context) != null);
        a = valueOf;
        return valueOf.booleanValue();
    }
}
