package com.flurry.sdk;

import com.flurry.sdk.fv;
import java.util.HashSet;
import java.util.Set;

public final class fx implements fv {
    protected static final Set<String> h = new HashSet();

    public final fv.a a(jp jpVar) {
        if (!jpVar.a().equals(jn.SESSION_PROPERTIES_PARAMS)) {
            return a;
        }
        String str = ((hd) jpVar.f()).a;
        if (h.size() < 10 || h.contains(str)) {
            h.add(str);
            return a;
        }
        da.e("SessionPropertiesParamsDropRule", "MaxSessionPropertiesParams exceeded: 10");
        return d;
    }

    public final void a() {
        h.clear();
    }
}
