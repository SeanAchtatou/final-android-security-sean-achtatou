package com.flurry.sdk;

import android.content.Context;
import android.text.TextUtils;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public final class cp extends cr {
    private final Context i;

    /* access modifiers changed from: protected */
    public final void b() {
    }

    cp(Context context, String str) {
        this.i = context;
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public final InputStream a() throws IOException {
        if (this.i != null && !TextUtils.isEmpty(this.a)) {
            try {
                return this.i.getAssets().open(this.a);
            } catch (FileNotFoundException unused) {
                da.b("LocalAssetsTransport", "File Not Found when opening " + this.a);
            } catch (IOException unused2) {
                da.b("LocalAssetsTransport", "IO Exception when opening " + this.a);
            }
        }
        return null;
    }
}
