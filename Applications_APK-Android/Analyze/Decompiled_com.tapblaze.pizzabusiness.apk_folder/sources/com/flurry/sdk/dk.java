package com.flurry.sdk;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.view.ViewTreeObserver;
import com.flurry.android.FlurryAgent;
import com.flurry.sdk.dm;
import java.util.HashMap;
import java.util.Map;

public final class dk {
    private static dk d;
    private static final long e = System.currentTimeMillis();
    public boolean a = false;
    public boolean b = false;
    public boolean c = false;
    private long f;
    private long g;
    private long h;
    private Map<String, String> i = new HashMap();
    private dm.a j;

    private dk() {
    }

    public static synchronized dk a() {
        dk dkVar;
        synchronized (dk.class) {
            if (d == null) {
                d = new dk();
            }
            dkVar = d;
        }
        return dkVar;
    }

    public final void a(Context context, Cursor cursor) {
        if (this.j == null) {
            boolean z = true;
            if (cursor != null) {
                cursor.moveToFirst();
                this.f = cursor.getLong(0);
                this.g = cursor.getLong(1);
                this.h = cursor.getLong(2);
                cursor.close();
            } else {
                Runtime runtime = Runtime.getRuntime();
                ActivityManager.MemoryInfo a2 = dn.a(context);
                this.f = e;
                this.g = runtime.totalMemory() - runtime.freeMemory();
                this.h = a2.totalMem - a2.availMem;
            }
            StringBuilder sb = new StringBuilder("Registered with Content Provider: ");
            if (cursor == null) {
                z = false;
            }
            sb.append(z);
            sb.append(", start time: ");
            sb.append(this.f);
            sb.append(", runtime memory: ");
            sb.append(this.g);
            sb.append(", system memory: ");
            sb.append(this.h);
            da.a(3, "ColdStartMonitor", sb.toString());
            this.j = new dm.a() {
                public final void c(Activity activity) {
                }

                public final void a(final Activity activity) {
                    activity.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        public final void onGlobalLayout() {
                            activity.getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            dk dkVar = dk.this;
                            activity.getApplication();
                            dk.a(dkVar);
                            dk.this.a(activity, "onGlobalLayout", "fl.layout.time", "fl.layout.runtime.memory", "fl.layout.system.memory");
                            boolean unused = dk.this.b = true;
                            if (dk.this.a) {
                                dk.this.b();
                            }
                        }
                    });
                }

                public final void b(Activity activity) {
                    dk.this.a(activity, "onActivityResumed", "fl.resume.time", "fl.resume.runtime.memory", "fl.resume.system.memory");
                }
            };
            dm.a().a(this.j);
        }
    }

    public final synchronized void b() {
        if (!this.i.isEmpty()) {
            da.a(4, "ColdStartMonitor", "Log Cold Start time event: " + this.i);
            FlurryAgent.logEvent("Flurry.ColdStartTime", this.i);
            this.i.clear();
        }
    }

    public final void a(Context context, String str, String str2, String str3, String str4) {
        long currentTimeMillis = System.currentTimeMillis() - this.f;
        Runtime runtime = Runtime.getRuntime();
        long freeMemory = runtime.totalMemory() - runtime.freeMemory();
        long j2 = freeMemory - this.g;
        if (j2 < 0) {
            j2 = 0;
        }
        ActivityManager.MemoryInfo a2 = dn.a(context);
        long j3 = a2.totalMem - a2.availMem;
        long j4 = j3 - this.h;
        if (j4 < 0) {
            j4 = 0;
        }
        da.a(3, "ColdStartMonitor", str + " time: " + currentTimeMillis + ", runtime memory usage: " + freeMemory + ", system memory usage: " + j3);
        this.i.put(str2, Long.toString(currentTimeMillis));
        this.i.put(str3, Long.toString(j2));
        this.i.put(str4, Long.toString(j4));
    }

    static /* synthetic */ void a(dk dkVar) {
        if (dkVar.j != null) {
            dm a2 = dm.a();
            dm.a aVar = dkVar.j;
            synchronized (a2.a) {
                a2.a.remove(aVar);
            }
            dkVar.j = null;
        }
    }
}
