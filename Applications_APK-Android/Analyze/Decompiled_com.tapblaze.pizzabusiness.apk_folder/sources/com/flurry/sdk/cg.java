package com.flurry.sdk;

public final class cg {
    private cs a = new cs();
    private cl b;

    public final ca a(String str) {
        cl clVar;
        if (str == null || (clVar = this.b) == null) {
            return null;
        }
        return clVar.a(str);
    }

    public final String toString() {
        return "Variant: {" + this.b.toString() + "}";
    }
}
