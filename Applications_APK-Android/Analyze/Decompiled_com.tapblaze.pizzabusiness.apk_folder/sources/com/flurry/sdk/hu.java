package com.flurry.sdk;

public final class hu {
    protected static hu a;
    public hk b = new hk();
    public hw c = new hw();
    public hn d = new hn();
    public ho e = new ho();
    public hp f = new hp();
    public hq g = new hq();
    public hr h = new hr();
    public hs i = new hs();
    public ht j = new ht();
    public hy k = new hy();
    public ia l = new ia();
    public hm m = new hm();
    public hz n = new hz();
    public hv o = new hv();
    public hx p = new hx();
    public hl q = new hl();

    private hu() {
    }

    public static synchronized hu a() {
        hu huVar;
        synchronized (hu.class) {
            if (a == null) {
                a = new hu();
            }
            huVar = a;
        }
        return huVar;
    }

    public static synchronized void b() {
        synchronized (hu.class) {
            if (a != null) {
                hu huVar = a;
                n a2 = n.a();
                if (a2 != null) {
                    a2.a.b(huVar.h);
                    a2.b.b(huVar.i);
                    a2.c.b(huVar.f);
                    a2.d.b(huVar.g);
                    a2.e.b(huVar.l);
                    a2.f.b(huVar.d);
                    a2.g.b(huVar.e);
                    a2.h.b(huVar.k);
                    a2.i.b(huVar.b);
                    a2.j.b(huVar.j);
                    a2.k.b(huVar.c);
                    a2.l.b(huVar.m);
                    a2.n.b(huVar.n);
                    a2.o.b(huVar.o);
                    a2.p.b(huVar.p);
                    a2.q.b(huVar.q);
                }
                a = null;
            }
        }
    }
}
