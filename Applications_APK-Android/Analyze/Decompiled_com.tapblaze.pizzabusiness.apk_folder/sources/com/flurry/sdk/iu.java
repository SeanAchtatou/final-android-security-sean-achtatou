package com.flurry.sdk;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class iu extends jl {
    public final String a;
    public final List<String> b;

    public iu(String str, List<String> list) {
        this.a = str;
        this.b = list;
    }

    public final JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (String put : this.b) {
            jSONArray.put(put);
        }
        jSONObject.put("fl.launch.options.key", this.a);
        jSONObject.put("fl.launch.options.values", jSONArray);
        return jSONObject;
    }
}
