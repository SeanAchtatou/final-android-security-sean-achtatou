package com.flurry.sdk;

import com.flurry.android.FlurryEventRecordStatus;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public final class gk extends jm {
    private static final AtomicInteger a = new AtomicInteger(0);

    private gk(jo joVar) {
        super(joVar);
    }

    public enum a {
        RECOVERABLE_ERROR(1),
        CAUGHT_EXCEPTION(2),
        UNRECOVERABLE_CRASH(3);
        
        public int d;

        private a(int i) {
            this.d = i;
        }
    }

    public enum b {
        NO_LOG(0),
        ANDROID_LOG_ATTACHED(2),
        NATIVE_CRASH_ATTACHED(3);
        
        public int d;

        private b(int i) {
            this.d = i;
        }
    }

    public final jn a() {
        return jn.ANALYTICS_ERROR;
    }

    public static FlurryEventRecordStatus a(aa aaVar) {
        int i;
        int i2;
        aa aaVar2 = aaVar;
        if (aaVar2 == null) {
            da.d("StreamingErrorFrame", "Error is null, do not send the frame.");
            return FlurryEventRecordStatus.kFlurryEventFailed;
        }
        boolean equals = y.UNCAUGHT_EXCEPTION_ID.c.equals(aaVar2.a);
        List<v> list = equals ? aaVar2.h : null;
        int incrementAndGet = a.incrementAndGet();
        String str = aaVar2.a;
        long j = aaVar2.b;
        String str2 = aaVar2.c;
        String str3 = aaVar2.d;
        String a2 = a(aaVar2.e);
        String str4 = aaVar2.a;
        if (aaVar2.e != null) {
            if (y.UNCAUGHT_EXCEPTION_ID.c.equals(str4)) {
                i = a.UNRECOVERABLE_CRASH.d;
            } else {
                i = a.CAUGHT_EXCEPTION.d;
            }
        } else if (y.NATIVE_CRASH.c.equals(str4)) {
            i = a.UNRECOVERABLE_CRASH.d;
        } else {
            i = a.RECOVERABLE_ERROR.d;
        }
        int i3 = i;
        if (aaVar2.e == null) {
            i2 = b.NO_LOG.d;
        } else {
            i2 = b.ANDROID_LOG_ATTACHED.d;
        }
        gk gkVar = new gk(new gl(incrementAndGet, str, j, str2, str3, a2, i3, i2, aaVar2.f, aaVar2.g, w.b(), list, "", ""));
        if (equals) {
            fc.a().b.a.b(gkVar);
        } else {
            fc.a().a(gkVar);
        }
        return FlurryEventRecordStatus.kFlurryEventRecorded;
    }

    public static gk a(gl glVar) {
        return new gk(glVar);
    }

    public static AtomicInteger b() {
        return a;
    }

    private static String a(Throwable th) {
        if (th == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement append : th.getStackTrace()) {
            sb.append(append);
            sb.append(fd.a);
        }
        if (th.getCause() != null) {
            sb.append(fd.a);
            sb.append("Caused by: ");
            for (StackTraceElement append2 : th.getCause().getStackTrace()) {
                sb.append(append2);
                sb.append(fd.a);
            }
        }
        return sb.toString();
    }
}
