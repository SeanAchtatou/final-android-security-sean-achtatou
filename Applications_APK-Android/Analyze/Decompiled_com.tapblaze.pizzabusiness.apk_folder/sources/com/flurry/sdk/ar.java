package com.flurry.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Locale;

public final class ar extends m<aq> {
    protected BroadcastReceiver b = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            ar.this.a(ar.e());
        }
    };

    public ar() {
        super("LocaleProvider");
        Context a = b.a();
        IntentFilter intentFilter = new IntentFilter("android.intent.action.LOCALE_CHANGED");
        if (a != null) {
            a.registerReceiver(this.b, intentFilter);
        } else {
            da.a(6, "LocaleProvider", "Context is null when initializing.");
        }
    }

    public final void a(final o<aq> oVar) {
        super.a((o) oVar);
        b(new ec() {
            public final void a() throws Exception {
                oVar.a(ar.e());
            }
        });
    }

    public final void c() {
        super.c();
        b.a().unregisterReceiver(this.b);
    }

    public static String d() {
        return Locale.getDefault().getLanguage() + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + Locale.getDefault().getCountry();
    }

    public static aq e() {
        return new aq(Locale.getDefault().getLanguage(), Locale.getDefault().getCountry());
    }
}
