package com.flurry.android;

import android.os.Handler;
import android.util.Pair;
import com.flurry.sdk.a;
import com.flurry.sdk.by;
import com.flurry.sdk.ca;
import com.flurry.sdk.cb;
import com.flurry.sdk.ci;
import com.flurry.sdk.ec;
import java.util.Map;

public final class FlurryConfig {
    private static FlurryConfig a;
    private cb b = cb.a();

    public static synchronized FlurryConfig getInstance() {
        FlurryConfig flurryConfig;
        synchronized (FlurryConfig.class) {
            if (a == null) {
                if (a.i()) {
                    a = new FlurryConfig();
                } else {
                    throw new IllegalStateException("Flurry SDK must be initialized before starting config");
                }
            }
            flurryConfig = a;
        }
        return flurryConfig;
    }

    private FlurryConfig() {
    }

    public final void fetchConfig() {
        this.b.d();
    }

    public final boolean activateConfig() {
        return this.b.a((ci) null);
    }

    public final void resetState() {
        cb cbVar = this.b;
        cbVar.b(new ec() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.flurry.sdk.cb.a(com.flurry.sdk.cb, boolean):boolean
             arg types: [com.flurry.sdk.cb, int]
             candidates:
              com.flurry.sdk.cb.a(com.flurry.sdk.cb, com.flurry.sdk.cb$a):com.flurry.sdk.cb$a
              com.flurry.sdk.cb.a(com.flurry.sdk.ci, boolean):void
              com.flurry.sdk.cb.a(com.flurry.sdk.cb, boolean):boolean */
            public final void a() throws Exception {
                ct.c(b.a());
                if (cb.this.j != null) {
                    cb.this.j.a();
                }
                cb.this.a.c();
                boolean unused = cb.this.q = false;
                a unused2 = cb.this.s = a.None;
                boolean unused3 = cb.this.r = false;
                for (ci put : ci.a()) {
                    Map d = cb.this.n;
                    Boolean bool = Boolean.FALSE;
                    d.put(put, new Pair(bool, bool));
                }
            }
        });
    }

    public final void registerListener(FlurryConfigListener flurryConfigListener) {
        this.b.a(flurryConfigListener, ci.a, null);
    }

    public final void registerListener(FlurryConfigListener flurryConfigListener, Handler handler) {
        this.b.a(flurryConfigListener, ci.a, handler);
    }

    public final void unregisterListener(FlurryConfigListener flurryConfigListener) {
        this.b.a(flurryConfigListener);
    }

    public final String getString(String str, String str2) {
        return this.b.c().a(str, str2, ci.a);
    }

    public final boolean getBoolean(String str, boolean z) {
        by c = this.b.c();
        ca a2 = c.b.a(str, ci.a);
        if (a2 == null) {
            a2 = c.a.a(str);
        }
        return a2 != null ? Boolean.parseBoolean(a2.a()) : z;
    }

    public final int getInt(String str, int i) {
        return this.b.c().a(str, i, ci.a);
    }

    public final long getLong(String str, long j) {
        return this.b.c().a(str, j, ci.a);
    }

    public final double getDouble(String str, double d) {
        return this.b.c().a(str, d, ci.a);
    }

    public final float getFloat(String str, float f) {
        return this.b.c().a(str, f, ci.a);
    }

    public final String toString() {
        return this.b.toString();
    }
}
