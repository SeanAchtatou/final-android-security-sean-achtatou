package com.flurry.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.flurry.sdk.bw;
import com.flurry.sdk.da;
import com.flurry.sdk.ea;
import com.ironsource.sdk.constants.Constants;

public final class FlurryInstallReceiver extends BroadcastReceiver {
    public final void onReceive(Context context, Intent intent) {
        da.a(4, "FlurryInstallReceiver", "Received an Install notification of " + intent.getAction());
        String string = intent.getExtras().getString("referrer");
        da.a(4, "FlurryInstallReceiver", "Received an Install referrer of ".concat(String.valueOf(string)));
        if (string == null || !"com.android.vending.INSTALL_REFERRER".equals(intent.getAction())) {
            da.a(5, "FlurryInstallReceiver", "referrer is null");
            return;
        }
        if (!string.contains(Constants.RequestParameters.EQUAL)) {
            da.a(4, "FlurryInstallReceiver", "referrer is before decoding: ".concat(String.valueOf(string)));
            string = ea.d(string);
            da.a(4, "FlurryInstallReceiver", "referrer is: ".concat(String.valueOf(string)));
        }
        new bw(context).a(string);
    }
}
