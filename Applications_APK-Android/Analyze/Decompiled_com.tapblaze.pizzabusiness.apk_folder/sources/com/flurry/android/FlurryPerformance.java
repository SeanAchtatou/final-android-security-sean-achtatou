package com.flurry.android;

import com.flurry.sdk.b;
import com.flurry.sdk.da;
import com.flurry.sdk.dk;
import com.flurry.sdk.dl;
import com.flurry.sdk.ea;
import java.io.IOException;
import java.util.Locale;
import okhttp3.Request;
import okhttp3.Response;

public class FlurryPerformance {
    public static int ALL = (COLD_START | SCREEN_TIME);
    public static int COLD_START = 1;
    public static int NONE = 0;
    public static int SCREEN_TIME = 2;

    public static void reportFullyDrawn() {
        if (!ea.a(16)) {
            da.b("FlurryPerformance", String.format(Locale.getDefault(), "Device SDK Version older than %d", 16));
            return;
        }
        dk a = dk.a();
        if (a.a && !a.c) {
            a.c = true;
            a.a(b.a(), "onReportFullyDrawn", "fl.fully.drawn.time", "fl.fully.drawn.runtime.memory", "fl.fully.drawn.system.memory");
            if (a.b) {
                a.b();
            }
        }
    }

    public static class HttpInterceptor extends dl.a {
        public HttpInterceptor(String str) {
            super(str);
        }
    }

    public static class HttpLogger {
        private long a = System.nanoTime();

        public void logEvent(String str, Request request, Response response) {
            double nanoTime = (double) (System.nanoTime() - this.a);
            Double.isNaN(nanoTime);
            long j = (long) (nanoTime / 1000000.0d);
            long j2 = 0;
            try {
                if (request.body() != null) {
                    j2 = request.body().contentLength();
                }
            } catch (IOException unused) {
            }
            String str2 = str;
            dl.a(str2, request.method(), request.url().toString(), j2, response.code(), response.request().url().toString(), j);
        }

        public void logEvent(String str, String str2, String str3, long j, int i, String str4) {
            double nanoTime = (double) (System.nanoTime() - this.a);
            Double.isNaN(nanoTime);
            dl.a(str, str2, str3, j, i, str4, (long) (nanoTime / 1000000.0d));
        }
    }
}
