package com.flurry.android;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.AccessToken;
import com.facebook.appevents.codeless.internal.Constants;
import com.flurry.sdk.ei;
import com.flurry.sdk.ej;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Locale;

public interface FlurryPrivacySession {

    public interface Callback {
        void failure();

        void success();
    }

    public static class Request implements ei.a {
        final String a;
        final String b;
        final String c;
        public final Callback callback;
        public final Context context;
        public final String verifier = ej.b.a();

        public Request(Context context2, Callback callback2) {
            String str;
            this.context = context2;
            this.callback = callback2;
            this.b = context2.getPackageName();
            String str2 = this.verifier;
            MessageDigest a2 = ej.a.a(CommonUtils.SHA256_INSTANCE);
            if (a2 != null) {
                a2.update(str2.getBytes(Charset.defaultCharset()));
                str = Base64.encodeToString(a2.digest(), 11);
            } else {
                str = "";
            }
            this.a = str;
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            language = TextUtils.isEmpty(language) ? e : language;
            String country = locale.getCountry();
            country = TextUtils.isEmpty(country) ? d : country;
            this.c = language + "-" + country;
        }
    }

    public static class a {
        public final Uri a;

        public a(String str, long j, Request request) {
            this.a = new Uri.Builder().scheme("https").authority("flurry.mydashboard.oath.com").appendQueryParameter(Constants.DEVICE_SESSION_ID, str).appendQueryParameter(AccessToken.EXPIRES_IN_KEY, String.valueOf(j)).appendQueryParameter("device_verifier", request.a).appendQueryParameter("lang", request.c).build();
        }
    }
}
