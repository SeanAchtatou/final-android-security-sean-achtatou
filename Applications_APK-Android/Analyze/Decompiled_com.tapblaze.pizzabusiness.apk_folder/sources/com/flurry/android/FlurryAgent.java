package com.flurry.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;
import com.flurry.android.Consent;
import com.flurry.android.FlurryAgentListener;
import com.flurry.android.FlurryPerformance;
import com.flurry.android.FlurryPrivacySession;
import com.flurry.sdk.a;
import com.flurry.sdk.ak;
import com.flurry.sdk.aq;
import com.flurry.sdk.as;
import com.flurry.sdk.au;
import com.flurry.sdk.aw;
import com.flurry.sdk.b;
import com.flurry.sdk.bk;
import com.flurry.sdk.da;
import com.flurry.sdk.dd;
import com.flurry.sdk.ea;
import com.flurry.sdk.ec;
import com.flurry.sdk.fc;
import com.flurry.sdk.hu;
import com.flurry.sdk.n;
import com.flurry.sdk.o;
import com.flurry.sdk.r;
import com.flurry.sdk.s;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class FlurryAgent {
    public static String VERSION_STRING = "!SDK-VERSION-STRING!:com.flurry.android:analytics:12.1.0";
    private static String a;

    private FlurryAgent() {
    }

    public static int getAgentVersion() {
        a.a();
        return a.b();
    }

    public static String getReleaseVersion() {
        a.a();
        return a.c();
    }

    public static List<FlurryModule> getAddOnModules() {
        return a.a().a;
    }

    public static void setVersionName(String str) {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to setVersionName. Flurry is not initialized");
            } else {
                a2.b(new ec(str) {
                    final /* synthetic */ String a;

                    {
                        this.a = r2;
                    }

                    public final void a() {
                        bn.a().a = this.a;
                        ib.b();
                    }
                });
            }
        }
    }

    public static void setReportLocation(boolean z) {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to setReportLocation. Flurry is not initialized");
            } else {
                a2.b(new ec(z) {
                    final /* synthetic */ boolean a;

                    {
                        this.a = r2;
                    }

                    public final void a() {
                        at atVar = n.a().a;
                        atVar.b = this.a;
                        atVar.a();
                    }
                });
            }
        }
    }

    public static void addOrigin(String str, String str2) {
        a.a().a(str, str2, null);
    }

    public static void addOrigin(String str, String str2, Map<String, String> map) {
        if (b()) {
            a.a().a(str, str2, map);
        }
    }

    public static void setInstantAppName(String str) {
        a a2 = a.a();
        if (!a.b.get()) {
            da.d("FlurryAgentImpl", "Invalid call to addOrigin. Flurry is not initialized");
        } else {
            a2.b(new ec(str) {
                final /* synthetic */ String a;

                {
                    this.a = r2;
                }

                public final void a() throws Exception {
                    an anVar = n.a().m;
                    anVar.b = this.a;
                    anVar.e();
                }
            });
        }
    }

    public static String getInstantAppName() {
        a.a();
        return a.e();
    }

    public static synchronized Consent getFlurryConsent() {
        Consent d;
        synchronized (FlurryAgent.class) {
            a.a();
            d = a.d();
        }
        return d;
    }

    public static synchronized boolean updateFlurryConsent(Consent consent) {
        synchronized (FlurryAgent.class) {
            if (!b()) {
                return false;
            }
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to updateFlurryConsent. Flurry is not initialized");
            } else {
                a2.b(new ec(consent) {
                    final /* synthetic */ Consent a;

                    {
                        this.a = r2;
                    }

                    public final void a() throws Exception {
                        n.a().l.a(this.a);
                    }
                });
            }
            return true;
        }
    }

    public static void onStartSession(Context context) {
        if (b()) {
            a a2 = a.a();
            if (context instanceof Activity) {
                da.a("FlurryAgentImpl", "Activity's session is controlled by Flurry SDK");
            } else if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to onStartSession. Flurry is not initialized");
            } else {
                a2.b(new ec() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.flurry.sdk.bb.a(com.flurry.sdk.bd, boolean):void
                     arg types: [com.flurry.sdk.bd, int]
                     candidates:
                      com.flurry.sdk.bb.a(com.flurry.sdk.bb, long):long
                      com.flurry.sdk.m.a(com.flurry.sdk.m, java.lang.Runnable):java.util.concurrent.Future
                      com.flurry.sdk.bb.a(com.flurry.sdk.bd, boolean):void */
                    public final void a() {
                        in.b();
                        n.a().k.a(bd.b, true);
                    }
                });
            }
        }
    }

    public static void onEndSession(Context context) {
        if (b()) {
            a a2 = a.a();
            if (context instanceof Activity) {
                da.a("FlurryAgentImpl", "Activity's session is controlled by Flurry SDK");
            } else if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to onStartSession. Flurry is not initialized");
            } else {
                a2.b(new ec() {
                    public final void a() {
                        n.a().k.b(bd.b, false);
                    }
                });
            }
        }
    }

    public static boolean isSessionActive() {
        if (!b()) {
            return false;
        }
        a.a();
        return a.f();
    }

    public static String getSessionId() {
        if (!b()) {
            return null;
        }
        a.a();
        return a.g();
    }

    public static FlurryEventRecordStatus logEvent(String str) {
        FlurryEventRecordStatus flurryEventRecordStatus = FlurryEventRecordStatus.kFlurryEventFailed;
        if (!b()) {
            return flurryEventRecordStatus;
        }
        return a.a().a(str, Collections.emptyMap(), false, false, System.currentTimeMillis(), SystemClock.elapsedRealtime());
    }

    public static FlurryEventRecordStatus logEvent(String str, Map<String, String> map) {
        FlurryEventRecordStatus flurryEventRecordStatus = FlurryEventRecordStatus.kFlurryEventFailed;
        if (!b()) {
            return flurryEventRecordStatus;
        }
        if (str == null) {
            da.b("FlurryAgent", "String eventId passed to logEvent was null.");
            return flurryEventRecordStatus;
        }
        if (map == null) {
            da.c("FlurryAgent", "String parameters passed to logEvent was null.");
        }
        return a.a().a(str, map, false, false, System.currentTimeMillis(), SystemClock.elapsedRealtime());
    }

    public static FlurryEventRecordStatus logEvent(String str, boolean z) {
        FlurryEventRecordStatus flurryEventRecordStatus = FlurryEventRecordStatus.kFlurryEventFailed;
        if (!b()) {
            return flurryEventRecordStatus;
        }
        return a.a().a(str, Collections.emptyMap(), z, true, System.currentTimeMillis(), SystemClock.elapsedRealtime());
    }

    public static FlurryEventRecordStatus logEvent(String str, Map<String, String> map, boolean z) {
        FlurryEventRecordStatus flurryEventRecordStatus = FlurryEventRecordStatus.kFlurryEventFailed;
        if (!b()) {
            return flurryEventRecordStatus;
        }
        if (str == null) {
            da.b("FlurryAgent", "String eventId passed to logEvent was null.");
            return flurryEventRecordStatus;
        }
        if (map == null) {
            da.c("FlurryAgent", "String parameters passed to logEvent was null.");
        }
        return a.a().a(str, map, z, true, System.currentTimeMillis(), SystemClock.elapsedRealtime());
    }

    public static void logPayment(int i, Intent intent, Map<String, String> map) {
        if (b()) {
            a a2 = a.a();
            long currentTimeMillis = System.currentTimeMillis();
            long elapsedRealtime = SystemClock.elapsedRealtime();
            HashMap hashMap = new HashMap();
            if (map != null) {
                hashMap.putAll(map);
            }
            a2.b(new Runnable(i, intent, hashMap, currentTimeMillis, elapsedRealtime) {
                final /* synthetic */ int a;
                final /* synthetic */ Intent b;
                final /* synthetic */ Map c;
                final /* synthetic */ long d;
                final /* synthetic */ long e;

                {
                    this.a = r2;
                    this.b = r3;
                    this.c = r4;
                    this.d = r5;
                    this.e = r7;
                }

                public final void run() {
                    gm.a(this.a, this.b, this.c, this.d, this.e);
                }
            });
        }
    }

    public static FlurryEventRecordStatus logPayment(String str, String str2, int i, double d, String str3, String str4, Map<String, String> map) {
        Map<String, String> map2 = map;
        FlurryEventRecordStatus flurryEventRecordStatus = FlurryEventRecordStatus.kFlurryEventFailed;
        if (!b()) {
            return flurryEventRecordStatus;
        }
        a a2 = a.a();
        long currentTimeMillis = System.currentTimeMillis();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        HashMap hashMap = new HashMap();
        if (map2 != null) {
            hashMap.putAll(map2);
        }
        a2.b(new Runnable(str, str2, i, d, str3, str4, hashMap, currentTimeMillis, elapsedRealtime) {
            final /* synthetic */ String a;
            final /* synthetic */ String b;
            final /* synthetic */ int c;
            final /* synthetic */ double d;
            final /* synthetic */ String e;
            final /* synthetic */ String f;
            final /* synthetic */ Map g;
            final /* synthetic */ long h;
            final /* synthetic */ long i;

            {
                this.a = r2;
                this.b = r3;
                this.c = r4;
                this.d = r5;
                this.e = r7;
                this.f = r8;
                this.g = r9;
                this.h = r10;
                this.i = r12;
            }

            public final void run() {
                gm.a(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
            }
        });
        return FlurryEventRecordStatus.kFlurryEventRecorded;
    }

    public static void endTimedEvent(String str) {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to endTimedEvent. Flurry is not initialized");
                return;
            }
            a2.b(new ec(str, System.currentTimeMillis(), SystemClock.elapsedRealtime()) {
                final /* synthetic */ String a;
                final /* synthetic */ long b;
                final /* synthetic */ long c;

                {
                    this.a = r2;
                    this.b = r3;
                    this.c = r5;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.flurry.sdk.gm.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>, boolean, boolean, long, long):com.flurry.android.FlurryEventRecordStatus
                 arg types: [java.lang.String, java.util.Map, int, int, long, long]
                 candidates:
                  com.flurry.sdk.gm.a(java.lang.String, int, java.util.Map<java.lang.String, java.lang.String>, java.util.Map<java.lang.String, java.lang.String>, long, long):com.flurry.sdk.gm
                  com.flurry.sdk.gm.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>, boolean, boolean, long, long):com.flurry.android.FlurryEventRecordStatus */
                public final void a() {
                    gm.a(this.a, (Map<String, String>) Collections.emptyMap(), true, false, this.b, this.c);
                }
            });
        }
    }

    public static void endTimedEvent(String str, Map<String, String> map) {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to endTimedEvent. Flurry is not initialized");
                return;
            }
            long currentTimeMillis = System.currentTimeMillis();
            long elapsedRealtime = SystemClock.elapsedRealtime();
            HashMap hashMap = new HashMap();
            if (map != null) {
                hashMap.putAll(map);
            }
            a2.b(new ec(str, hashMap, currentTimeMillis, elapsedRealtime) {
                final /* synthetic */ String a;
                final /* synthetic */ Map b;
                final /* synthetic */ long c;
                final /* synthetic */ long d;

                {
                    this.a = r2;
                    this.b = r3;
                    this.c = r4;
                    this.d = r6;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.flurry.sdk.gm.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>, boolean, boolean, long, long):com.flurry.android.FlurryEventRecordStatus
                 arg types: [java.lang.String, java.util.Map, int, int, long, long]
                 candidates:
                  com.flurry.sdk.gm.a(java.lang.String, int, java.util.Map<java.lang.String, java.lang.String>, java.util.Map<java.lang.String, java.lang.String>, long, long):com.flurry.sdk.gm
                  com.flurry.sdk.gm.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>, boolean, boolean, long, long):com.flurry.android.FlurryEventRecordStatus */
                public final void a() {
                    gm.a(this.a, (Map<String, String>) this.b, true, false, this.c, this.d);
                }
            });
        }
    }

    public static void onError(String str, String str2, String str3) {
        StackTraceElement[] stackTraceElementArr;
        if (b()) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace == null || stackTrace.length <= 4) {
                stackTraceElementArr = stackTrace;
            } else {
                StackTraceElement[] stackTraceElementArr2 = new StackTraceElement[(stackTrace.length - 4)];
                System.arraycopy(stackTrace, 4, stackTraceElementArr2, 0, stackTraceElementArr2.length);
                stackTraceElementArr = stackTraceElementArr2;
            }
            a.a().a(str, str2, str3, null, stackTraceElementArr);
        }
    }

    public static void onError(String str, String str2, String str3, Map<String, String> map) {
        StackTraceElement[] stackTraceElementArr;
        if (b()) {
            if (TextUtils.isEmpty(str)) {
                da.b("FlurryAgent", "String errorId passed to onError was empty.");
            } else if (TextUtils.isEmpty(str2)) {
                da.b("FlurryAgent", "String message passed to onError was empty.");
            } else if (TextUtils.isEmpty(str3)) {
                da.b("FlurryAgent", "String errorClass passed to onError was empty.");
            } else {
                StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
                if (stackTrace == null || stackTrace.length <= 4) {
                    stackTraceElementArr = stackTrace;
                } else {
                    StackTraceElement[] stackTraceElementArr2 = new StackTraceElement[(stackTrace.length - 4)];
                    System.arraycopy(stackTrace, 4, stackTraceElementArr2, 0, stackTraceElementArr2.length);
                    stackTraceElementArr = stackTraceElementArr2;
                }
                a.a().a(str, str2, str3, map, stackTraceElementArr);
            }
        }
    }

    public static void onError(String str, String str2, Throwable th) {
        onError(str, str2, th, (Map<String, String>) null);
    }

    public static void onError(String str, String str2, Throwable th, Map<String, String> map) {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to onError. Flurry is not initialized");
                return;
            }
            long currentTimeMillis = System.currentTimeMillis();
            HashMap hashMap = new HashMap();
            if (map != null) {
                hashMap.putAll(map);
            }
            a2.b(new ec(str, currentTimeMillis, str2, th, hashMap) {
                final /* synthetic */ String a;
                final /* synthetic */ long b;
                final /* synthetic */ String c;
                final /* synthetic */ Throwable d;
                final /* synthetic */ Map e;

                {
                    this.a = r2;
                    this.b = r3;
                    this.c = r5;
                    this.d = r6;
                    this.e = r7;
                }

                public final void a() {
                    n.a().f.a(this.a, this.b, this.c, this.d.getClass().getName(), this.d, x.a(), this.e);
                }
            });
        }
    }

    public static void logBreadcrumb(String str) {
        if (b()) {
            if (TextUtils.isEmpty(str)) {
                da.b("FlurryAgent", "Crash breadcrumb cannot be empty.");
                return;
            }
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to logBreadcrumb. Flurry is not initialized");
            } else {
                a2.b(new ec(str) {
                    final /* synthetic */ String a;

                    {
                        this.a = r2;
                    }

                    public final void a() {
                        ab abVar = n.a().f;
                        v vVar = new v(this.a, System.currentTimeMillis());
                        if (abVar.b != null) {
                            abVar.b.a(vVar);
                        }
                    }
                });
            }
        }
    }

    public static void setAge(int i) {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to setAge. Flurry is not initialized");
            } else {
                a2.b(new ec(i) {
                    final /* synthetic */ int a;

                    {
                        this.a = r2;
                    }

                    public final void a() {
                        int i = this.a;
                        if (i > 0 && i < 110) {
                            long time = new Date(new Date(System.currentTimeMillis() - (((long) this.a) * 31449600000L)).getYear(), 1, 1).getTime();
                            if (time <= 0) {
                                da.d("BirthdateFrame", "Birth date is invalid, do not send the frame.");
                                return;
                            }
                            fc.a().a(new ge(new gf(Long.valueOf(time))));
                        }
                    }
                });
            }
        }
    }

    public static void setGender(byte b) {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to setGender. Flurry is not initialized");
                return;
            }
            boolean z = true;
            if (!(b == 0 || b == 1 || b == -1)) {
                z = false;
            }
            if (z) {
                a2.b(new ec(b) {
                    final /* synthetic */ byte a;

                    {
                        this.a = r2;
                    }

                    public final void a() {
                        fc.a().a(new gq(new gr(this.a)));
                    }
                });
            }
        }
    }

    public static void setUserId(String str) {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to setUserId. Flurry is not initialized");
            } else {
                a2.b(new ec(str) {
                    final /* synthetic */ String a;

                    {
                        this.a = r2;
                    }

                    public final void a() {
                        ac acVar = n.a().h;
                        String str = this.a;
                        acVar.h = str;
                        fc.a().a(new hi(new hj(str)));
                    }
                });
            }
        }
    }

    public static void setSessionOrigin(String str, String str2) {
        if (b()) {
            if (TextUtils.isEmpty(str)) {
                da.b("FlurryAgent", "String originName passed to setSessionOrigin was empty.");
                return;
            }
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to setSessionOrigin. Flurry is not initialized");
            } else {
                a2.b(new ec(str, str2) {
                    final /* synthetic */ String a;
                    final /* synthetic */ String b;

                    {
                        this.a = r2;
                        this.b = r3;
                    }

                    public final void a() {
                        fc.a().a(new ha(new hb(this.a, this.b)));
                    }
                });
            }
        }
    }

    public static void addSessionProperty(String str, String str2) {
        if (b()) {
            if (TextUtils.isEmpty(str)) {
                da.b("FlurryAgent", "Session property name was empty");
                return;
            }
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to addSessionProperty. Flurry is not initialized");
            } else {
                a2.b(new ec(str, str2) {
                    final /* synthetic */ String a;
                    final /* synthetic */ String b;

                    {
                        this.a = r2;
                        this.b = r3;
                    }

                    public final void a() {
                        fc.a().a(new hc(new hd(this.a, this.b)));
                    }
                });
            }
        }
    }

    public static void openPrivacyDashboard(FlurryPrivacySession.Request request) {
        if (b()) {
            a a2 = a.a();
            a2.b(new ec(request) {
                final /* synthetic */ FlurryPrivacySession.Request a;

                {
                    this.a = r2;
                }

                public final void a() throws Exception {
                    eh.a(this.a);
                }
            });
        }
    }

    public static void setDataSaleOptOut(boolean z) {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to setDataSaleOptOut. Flurry is not initialized");
            } else {
                a2.b(new ec(z) {
                    final /* synthetic */ boolean a;

                    {
                        this.a = r2;
                    }

                    public final void a() throws Exception {
                        n.a().q.a(this.a);
                    }
                });
            }
        }
    }

    public static void deleteData() {
        if (b()) {
            a a2 = a.a();
            if (!a.b.get()) {
                da.d("FlurryAgentImpl", "Invalid call to deleteData. Flurry is not initialized");
            } else {
                a2.b(new ec() {
                    public final void a() throws Exception {
                        n.a().q.a(new s(s.a.c));
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean b() {
        if (ea.a(16)) {
            return true;
        }
        da.b("FlurryAgent", String.format(Locale.getDefault(), "Device SDK Version older than %d", 16));
        return false;
    }

    public static class Builder {
        private FlurryAgentListener a = null;
        private boolean b = false;
        private int c = 5;
        private long d = 10000;
        private boolean e = true;
        private boolean f = true;
        private boolean g = false;
        private int h = FlurryPerformance.NONE;
        private List<FlurryModule> i = new ArrayList();
        private Consent j;
        private boolean k = false;

        @Deprecated
        public Builder withPulseEnabled(boolean z) {
            return this;
        }

        public Builder withListener(FlurryAgentListener flurryAgentListener) {
            this.a = flurryAgentListener;
            return this;
        }

        public Builder withLogEnabled(boolean z) {
            this.b = z;
            return this;
        }

        public Builder withLogLevel(int i2) {
            this.c = i2;
            return this;
        }

        public Builder withContinueSessionMillis(long j2) {
            if (j2 >= 5000) {
                this.d = j2;
            }
            return this;
        }

        public Builder withCaptureUncaughtExceptions(boolean z) {
            this.e = z;
            return this;
        }

        public Builder withIncludeBackgroundSessionsInMetrics(boolean z) {
            this.f = z;
            return this;
        }

        public Builder withSslPinningEnabled(boolean z) {
            this.g = z;
            return this;
        }

        public Builder withPerformanceMetrics(int i2) {
            this.h = i2;
            return this;
        }

        public Builder withModule(FlurryModule flurryModule) throws IllegalArgumentException {
            if (dd.a(flurryModule.getClass().getCanonicalName())) {
                this.i.add(flurryModule);
                return this;
            }
            throw new IllegalArgumentException("The Flurry module you have registered is invalid: " + flurryModule.getClass().getCanonicalName());
        }

        public Builder withConsent(Consent consent) {
            this.j = consent;
            return this;
        }

        public Builder withDataSaleOptOut(boolean z) {
            this.k = z;
            return this;
        }

        public void build(Context context, String str) {
            boolean z;
            Context context2 = context;
            if (FlurryAgent.b()) {
                if (!TextUtils.isEmpty(str)) {
                    b.a(context);
                    bk.a().b = str;
                    a a2 = a.a();
                    FlurryAgentListener flurryAgentListener = this.a;
                    boolean z2 = this.b;
                    int i2 = this.c;
                    long j2 = this.d;
                    boolean z3 = this.e;
                    boolean z4 = this.f;
                    boolean z5 = this.g;
                    int i3 = this.h;
                    List<FlurryModule> list = this.i;
                    Consent consent = this.j;
                    boolean z6 = this.k;
                    if (a.b.get()) {
                        da.d("FlurryAgentImpl", "Invalid call to Init. Flurry is already initialized");
                        return;
                    }
                    da.d("FlurryAgentImpl", "Initializing Flurry SDK");
                    if (a.b.get()) {
                        da.d("FlurryAgentImpl", "Invalid call to register. Flurry is already initialized");
                    } else {
                        a2.a = list;
                    }
                    fc.a();
                    a2.b(new ec(context2, list) {
                        final /* synthetic */ Context a;
                        final /* synthetic */ List b;

                        {
                            this.a = r2;
                            this.b = r3;
                        }

                        public final void a() throws Exception {
                            fc a2 = fc.a();
                            a2.d.a();
                            a2.b.a.a();
                            ju juVar = a2.c;
                            File[] listFiles = new File(fg.b()).listFiles();
                            if (listFiles != null) {
                                for (int i = 0; i < listFiles.length; i++) {
                                    if (listFiles[i].isFile()) {
                                        da.a(3, "StreamingFileUtil", "File " + listFiles[i].getName());
                                    } else if (listFiles[i].isDirectory()) {
                                        da.a(3, "StreamingFileUtil", "Directory " + listFiles[i].getName());
                                    }
                                }
                            }
                            System.out.println();
                            da.a(2, "VNodeFileProcessor", "Number of files already pending: in startWatching " + listFiles.length);
                            juVar.a(Arrays.asList(listFiles));
                            juVar.b(
                            /*  JADX ERROR: Method code generation error
                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0090: INVOKE  
                                  (r0v1 'juVar' com.flurry.sdk.ju)
                                  (wrap: com.flurry.sdk.ju$1 : 0x008d: CONSTRUCTOR  (r1v6 com.flurry.sdk.ju$1) = (r0v1 'juVar' com.flurry.sdk.ju), (r0v1 'juVar' com.flurry.sdk.ju) call: com.flurry.sdk.ju.1.<init>(com.flurry.sdk.ju, com.flurry.sdk.jt):void type: CONSTRUCTOR)
                                 type: VIRTUAL call: com.flurry.sdk.ju.b(java.lang.Runnable):java.util.concurrent.Future in method: com.flurry.sdk.a.1.a():void, dex: classes.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:255)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:110)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:56)
                                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:215)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:208)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:322)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:275)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:244)
                                	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
                                	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
                                	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
                                	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                                	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                                	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                                	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
                                	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
                                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
                                	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:678)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:608)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:363)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:230)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:122)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:106)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:790)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:730)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:367)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:249)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:110)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:56)
                                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:99)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:143)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:63)
                                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:99)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:143)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:63)
                                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:215)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:208)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:322)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:275)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:244)
                                	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
                                	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
                                	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
                                	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                                	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                                	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                                	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
                                	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
                                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
                                	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
                                	at jadx.core.codegen.ClassGen.addInnerClass(ClassGen.java:253)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:242)
                                	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
                                	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
                                	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
                                	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                                	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                                	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                                	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
                                	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
                                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
                                	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
                                	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:81)
                                	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:45)
                                	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:34)
                                	at jadx.core.codegen.CodeGen.generate(CodeGen.java:22)
                                	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
                                	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
                                	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x008d: CONSTRUCTOR  (r1v6 com.flurry.sdk.ju$1) = (r0v1 'juVar' com.flurry.sdk.ju), (r0v1 'juVar' com.flurry.sdk.ju) call: com.flurry.sdk.ju.1.<init>(com.flurry.sdk.ju, com.flurry.sdk.jt):void type: CONSTRUCTOR in method: com.flurry.sdk.a.1.a():void, dex: classes.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:255)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:122)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:106)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:790)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:730)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:367)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:249)
                                	... 95 more
                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: com.flurry.sdk.ju, state: NOT_LOADED
                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:270)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:607)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:363)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:230)
                                	... 101 more
                                */
                            /*
                                this = this;
                                com.flurry.sdk.fc r0 = com.flurry.sdk.fc.a()
                                com.flurry.sdk.jq r1 = r0.d
                                r1.a()
                                com.flurry.sdk.fj r1 = r0.b
                                com.flurry.sdk.fp r1 = r1.a
                                r1.a()
                                com.flurry.sdk.ju r0 = r0.c
                                java.lang.String r1 = com.flurry.sdk.fg.b()
                                java.io.File r2 = new java.io.File
                                r2.<init>(r1)
                                java.io.File[] r1 = r2.listFiles()
                                if (r1 == 0) goto L_0x006a
                                r2 = 0
                            L_0x0022:
                                int r3 = r1.length
                                if (r2 >= r3) goto L_0x006a
                                r3 = r1[r2]
                                boolean r3 = r3.isFile()
                                java.lang.String r4 = "StreamingFileUtil"
                                r5 = 3
                                if (r3 == 0) goto L_0x0048
                                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                                java.lang.String r6 = "File "
                                r3.<init>(r6)
                                r6 = r1[r2]
                                java.lang.String r6 = r6.getName()
                                r3.append(r6)
                                java.lang.String r3 = r3.toString()
                                com.flurry.sdk.da.a(r5, r4, r3)
                                goto L_0x0067
                            L_0x0048:
                                r3 = r1[r2]
                                boolean r3 = r3.isDirectory()
                                if (r3 == 0) goto L_0x0067
                                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                                java.lang.String r6 = "Directory "
                                r3.<init>(r6)
                                r6 = r1[r2]
                                java.lang.String r6 = r6.getName()
                                r3.append(r6)
                                java.lang.String r3 = r3.toString()
                                com.flurry.sdk.da.a(r5, r4, r3)
                            L_0x0067:
                                int r2 = r2 + 1
                                goto L_0x0022
                            L_0x006a:
                                java.io.PrintStream r2 = java.lang.System.out
                                r2.println()
                                r2 = 2
                                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                                java.lang.String r4 = "Number of files already pending: in startWatching "
                                r3.<init>(r4)
                                int r4 = r1.length
                                r3.append(r4)
                                java.lang.String r3 = r3.toString()
                                java.lang.String r4 = "VNodeFileProcessor"
                                com.flurry.sdk.da.a(r2, r4, r3)
                                java.util.List r1 = java.util.Arrays.asList(r1)
                                r0.a(r1)
                                com.flurry.sdk.ju$1 r1 = new com.flurry.sdk.ju$1
                                r1.<init>(r0, r0)
                                r0.b(r1)
                                com.flurry.sdk.ek.a()
                                android.content.Context r0 = r7.a
                                com.flurry.sdk.dd.a(r0)
                                java.util.List r0 = r7.b
                                com.flurry.sdk.ek.a(r0)
                                android.content.Context r0 = r7.a
                                com.flurry.sdk.ek.a(r0)
                                return
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.a.AnonymousClass1.a():void");
                        }
                    });
                    hu a3 = hu.a();
                    n a4 = n.a();
                    if (a4 != null) {
                        z = z6;
                        a4.a.a((o<as>) a3.h);
                        a4.b.a((o<au>) a3.i);
                        a4.c.a((o) a3.f);
                        a4.d.a((o<aq>) a3.g);
                        a4.e.a((o<String>) a3.l);
                        a4.f.a((o) a3.d);
                        a4.g.a((o<ak>) a3.e);
                        a4.h.a((o) a3.k);
                        a4.i.a((o<r>) a3.b);
                        a4.j.a((o<aw>) a3.j);
                        a4.k.a((o) a3.c);
                        a4.l.a((o) a3.m);
                        a4.n.a((o) a3.n);
                        a4.o.a((o) a3.o);
                        a4.p.a((o) a3.p);
                        a4.q.a((o) a3.q);
                    } else {
                        z = z6;
                    }
                    bk.a().c();
                    n.a().f.h = z3;
                    if (consent != null) {
                        a2.b(new ec(consent) {
                            final /* synthetic */ Consent a;

                            {
                                this.a = r2;
                            }

                            public final void a() throws Exception {
                                n.a().l.a(this.a);
                            }
                        });
                    }
                    if (z2) {
                        da.b();
                    } else {
                        da.a();
                    }
                    da.a(i2);
                    a2.b(new ec(j2, flurryAgentListener) {
                        final /* synthetic */ long a;
                        final /* synthetic */ FlurryAgentListener b;

                        {
                            this.a = r2;
                            this.b = r4;
                        }

                        public final void a() {
                            n.a().k.j = this.a;
                            n.a().k.a(this.b);
                        }
                    });
                    a2.b(new ec(z4, z5) {
                        final /* synthetic */ boolean a;
                        final /* synthetic */ boolean b;

                        {
                            this.a = r2;
                            this.b = r3;
                        }

                        public final void a() {
                            int identifier;
                            ac acVar = n.a().h;
                            String b2 = bk.a().b();
                            boolean z = this.a;
                            boolean z2 = this.b;
                            acVar.b = b2;
                            acVar.i = z;
                            acVar.j = z2;
                            acVar.b(new ec() {
                                public final void a() throws Exception {
                                    ac.d(ac.this);
                                    ac.a(ac.this);
                                }
                            });
                            String property = System.getProperty("os.arch");
                            String str = "";
                            if (TextUtils.isEmpty(property)) {
                                property = str;
                            }
                            HashMap hashMap = new HashMap();
                            hashMap.put("device.model", Build.MODEL);
                            hashMap.put("build.brand", Build.BRAND);
                            hashMap.put("build.id", Build.ID);
                            hashMap.put("version.release", Build.VERSION.RELEASE);
                            hashMap.put("build.device", Build.DEVICE);
                            hashMap.put("build.product", Build.PRODUCT);
                            bl.a();
                            Context a2 = b.a();
                            if (!(a2 == null || (identifier = a2.getResources().getIdentifier("com.flurry.crash.map_id", "string", a2.getPackageName())) == 0)) {
                                str = a2.getResources().getString(identifier);
                            }
                            hashMap.put("proguard.build.uuid", str);
                            hashMap.put("device.arch", property);
                            fc.a().a(new il(new im(hashMap)));
                            ib.b();
                            in.b();
                            Map<String, List<String>> a3 = new bw().a();
                            if (a3.size() > 0) {
                                fc.a().a(new jd(new je(a3)));
                            }
                            id.a(n.a().c.b);
                        }
                    });
                    a2.b(new ec(i3, context2) {
                        final /* synthetic */ int a;
                        final /* synthetic */ Context b;

                        {
                            this.a = r2;
                            this.b = r3;
                        }

                        public final void a() {
                            if (this.a != FlurryPerformance.NONE) {
                                dm.a().a(this.b, null);
                            }
                            if ((this.a & FlurryPerformance.COLD_START) == FlurryPerformance.COLD_START) {
                                dk a2 = dk.a();
                                a2.a = true;
                                if (a2.b) {
                                    a2.b();
                                }
                            }
                            if ((this.a & FlurryPerformance.SCREEN_TIME) == FlurryPerformance.SCREEN_TIME) {
                                Cdo a3 = Cdo.a();
                                if (a3.a == null) {
                                    da.a(3, "ScreenTimeMonitor", "Register Screen Time metrics.");
                                    a3.a = 
                                    /*  JADX ERROR: Method code generation error
                                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0045: IPUT  
                                          (wrap: com.flurry.sdk.do$1 : 0x0042: CONSTRUCTOR  (r1v7 com.flurry.sdk.do$1) = (r0v5 'a3' com.flurry.sdk.do) call: com.flurry.sdk.do.1.<init>(com.flurry.sdk.do):void type: CONSTRUCTOR)
                                          (r0v5 'a3' com.flurry.sdk.do)
                                         com.flurry.sdk.do.a com.flurry.sdk.dm$a in method: com.flurry.sdk.a.24.a():void, dex: classes.dex
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:255)
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
                                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:110)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:56)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:99)
                                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:143)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:63)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:99)
                                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:143)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:63)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:215)
                                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:208)
                                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:322)
                                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:275)
                                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:244)
                                        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
                                        	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                        	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                        	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
                                        	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
                                        	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                                        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                                        	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                                        	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
                                        	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
                                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
                                        	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:678)
                                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:608)
                                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:363)
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:230)
                                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:122)
                                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:106)
                                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:790)
                                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:730)
                                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:367)
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:249)
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
                                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:110)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:56)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:99)
                                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:143)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:63)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:99)
                                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:143)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:63)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:93)
                                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:59)
                                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:215)
                                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:208)
                                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:322)
                                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:275)
                                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:244)
                                        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
                                        	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                        	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                        	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
                                        	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
                                        	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                                        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                                        	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                                        	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
                                        	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
                                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
                                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
                                        	at jadx.core.codegen.ClassGen.addInnerClass(ClassGen.java:253)
                                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:242)
                                        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
                                        	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                        	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                        	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
                                        	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
                                        	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                                        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                                        	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                                        	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
                                        	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
                                        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
                                        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
                                        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:81)
                                        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:45)
                                        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:34)
                                        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:22)
                                        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
                                        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
                                        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
                                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0042: CONSTRUCTOR  (r1v7 com.flurry.sdk.do$1) = (r0v5 'a3' com.flurry.sdk.do) call: com.flurry.sdk.do.1.<init>(com.flurry.sdk.do):void type: CONSTRUCTOR in method: com.flurry.sdk.a.24.a():void, dex: classes.dex
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:255)
                                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:122)
                                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:106)
                                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:428)
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:249)
                                        	... 109 more
                                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: com.flurry.sdk.do, state: NOT_LOADED
                                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:270)
                                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:607)
                                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:363)
                                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:230)
                                        	... 113 more
                                        */
                                    /*
                                        this = this;
                                        int r0 = r4.a
                                        int r1 = com.flurry.android.FlurryPerformance.NONE
                                        if (r0 == r1) goto L_0x0010
                                        com.flurry.sdk.dm r0 = com.flurry.sdk.dm.a()
                                        android.content.Context r1 = r4.b
                                        r2 = 0
                                        r0.a(r1, r2)
                                    L_0x0010:
                                        int r0 = r4.a
                                        int r1 = com.flurry.android.FlurryPerformance.COLD_START
                                        r0 = r0 & r1
                                        int r1 = com.flurry.android.FlurryPerformance.COLD_START
                                        if (r0 != r1) goto L_0x0027
                                        com.flurry.sdk.dk r0 = com.flurry.sdk.dk.a()
                                        r1 = 1
                                        r0.a = r1
                                        boolean r1 = r0.b
                                        if (r1 == 0) goto L_0x0027
                                        r0.b()
                                    L_0x0027:
                                        int r0 = r4.a
                                        int r1 = com.flurry.android.FlurryPerformance.SCREEN_TIME
                                        r0 = r0 & r1
                                        int r1 = com.flurry.android.FlurryPerformance.SCREEN_TIME
                                        if (r0 != r1) goto L_0x0050
                                        com.flurry.sdk.do r0 = com.flurry.sdk.Cdo.a()
                                        com.flurry.sdk.dm$a r1 = r0.a
                                        if (r1 != 0) goto L_0x0050
                                        r1 = 3
                                        java.lang.String r2 = "ScreenTimeMonitor"
                                        java.lang.String r3 = "Register Screen Time metrics."
                                        com.flurry.sdk.da.a(r1, r2, r3)
                                        com.flurry.sdk.do$1 r1 = new com.flurry.sdk.do$1
                                        r1.<init>(r0)
                                        r0.a = r1
                                        com.flurry.sdk.dm r1 = com.flurry.sdk.dm.a()
                                        com.flurry.sdk.dm$a r0 = r0.a
                                        r1.a(r0)
                                    L_0x0050:
                                        return
                                    */
                                    throw new UnsupportedOperationException("Method not decompiled: com.flurry.sdk.a.AnonymousClass24.a():void");
                                }
                            });
                            a2.b(new ec(z) {
                                final /* synthetic */ boolean a;

                                {
                                    this.a = r2;
                                }

                                public final void a() throws Exception {
                                    n.a().q.a(this.a);
                                }
                            });
                            a.b.set(true);
                            return;
                        }
                        throw new IllegalArgumentException("API key not specified");
                    }
                }
            }
        }
