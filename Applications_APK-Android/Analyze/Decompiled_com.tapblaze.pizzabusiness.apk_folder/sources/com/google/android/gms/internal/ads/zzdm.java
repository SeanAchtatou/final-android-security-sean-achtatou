package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdm implements Runnable {
    private final /* synthetic */ Activity val$activity;
    private final /* synthetic */ Context zzwf;
    private final /* synthetic */ String zzwg;
    private final /* synthetic */ View zzwh;

    zzdm(zzdi zzdi, Context context, String str, View view, Activity activity) {
        this.zzwf = context;
        this.zzwg = str;
        this.zzwh = view;
        this.val$activity = activity;
    }

    public final void run() {
        zzdi.zzvc.zza(this.zzwf, this.zzwg, this.zzwh, this.val$activity);
    }
}
