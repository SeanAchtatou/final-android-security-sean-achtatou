package com.google.android.gms.internal.ads;

import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbum implements zzdxg<View> {
    private final zzbtv zzfje;

    private zzbum(zzbtv zzbtv) {
        this.zzfje = zzbtv;
    }

    public static zzbum zzd(zzbtv zzbtv) {
        return new zzbum(zzbtv);
    }

    public final /* synthetic */ Object get() {
        return this.zzfje.zzaif();
    }
}
