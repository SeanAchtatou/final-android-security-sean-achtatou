package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbf  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final /* synthetic */ class zzbf implements Runnable {
    private final zzbt zzbf;
    private final zzbc zzbu;

    zzbf(zzbc zzbc, zzbt zzbt) {
        this.zzbu = zzbc;
        this.zzbf = zzbt;
    }

    public final void run() {
        this.zzbu.zzi(this.zzbf);
    }
}
