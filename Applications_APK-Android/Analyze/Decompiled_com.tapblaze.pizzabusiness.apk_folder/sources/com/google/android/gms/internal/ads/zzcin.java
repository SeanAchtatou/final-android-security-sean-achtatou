package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcin<DelegateT, AdapterT> implements zzcio<AdapterT> {
    private final zzcio<DelegateT> zzfyd;
    private final zzded<DelegateT, AdapterT> zzfye;

    public zzcin(zzcio<DelegateT> zzcio, zzded<DelegateT, AdapterT> zzded) {
        this.zzfyd = zzcio;
        this.zzfye = zzded;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        return this.zzfyd.zza(zzczt, zzczl);
    }

    public final zzdhe<AdapterT> zzb(zzczt zzczt, zzczl zzczl) {
        return zzdgs.zzb(this.zzfyd.zzb(zzczt, zzczl), this.zzfye, zzazd.zzdwe);
    }
}
