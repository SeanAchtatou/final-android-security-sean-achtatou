package com.google.android.gms.internal.ads;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbln implements Runnable {
    private final AtomicReference zzfew;

    zzbln(AtomicReference atomicReference) {
        this.zzfew = atomicReference;
    }

    public final void run() {
        Runnable runnable = (Runnable) this.zzfew.getAndSet(null);
        if (runnable != null) {
            runnable.run();
        }
    }
}
