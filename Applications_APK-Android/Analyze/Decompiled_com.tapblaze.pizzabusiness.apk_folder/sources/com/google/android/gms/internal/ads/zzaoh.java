package com.google.android.gms.internal.ads;

import android.content.DialogInterface;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaoh implements DialogInterface.OnClickListener {
    private final /* synthetic */ zzaof zzdfw;

    zzaoh(zzaof zzaof) {
        this.zzdfw = zzaof;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.zzdfw.zzds("User canceled the download.");
    }
}
