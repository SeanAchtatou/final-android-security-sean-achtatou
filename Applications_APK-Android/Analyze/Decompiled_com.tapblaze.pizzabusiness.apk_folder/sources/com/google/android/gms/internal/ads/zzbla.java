package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbla implements zzdxg<zzbsu<zzbqb>> {
    private final zzdxp<zzblw> zzfdq;
    private final zzbkn zzfen;

    public zzbla(zzbkn zzbkn, zzdxp<zzblw> zzdxp) {
        this.zzfen = zzbkn;
        this.zzfdq = zzdxp;
    }

    public static zzbsu<zzbqb> zza(zzbkn zzbkn, zzblw zzblw) {
        return (zzbsu) zzdxm.zza(new zzbsu(zzblw, zzazd.zzdwi), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfen, this.zzfdq.get());
    }
}
