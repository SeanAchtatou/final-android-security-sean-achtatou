package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;
import java.util.Collections;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbsh implements zzdxg<Set<zzbsu<VideoController.VideoLifecycleCallbacks>>> {
    private final zzbrm zzfim;

    private zzbsh(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbsh zzy(zzbrm zzbrm) {
        return new zzbsh(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(Collections.emptySet(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
