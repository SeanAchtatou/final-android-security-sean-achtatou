package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzfh extends zzfw {
    private static final Object zzzs = new Object();
    private static volatile String zzzv;

    public zzfh(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 1);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        this.zzzt.zzaf("E");
        if (zzzv == null) {
            synchronized (zzzs) {
                if (zzzv == null) {
                    zzzv = (String) this.zzaae.invoke(null, new Object[0]);
                }
            }
        }
        synchronized (this.zzzt) {
            this.zzzt.zzaf(zzzv);
        }
    }
}
