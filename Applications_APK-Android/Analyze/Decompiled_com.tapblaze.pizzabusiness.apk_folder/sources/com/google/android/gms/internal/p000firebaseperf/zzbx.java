package com.google.android.gms.internal.p000firebaseperf;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbx  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzbx {
    private static Boolean zzic;

    public static boolean zzg(Context context) {
        Boolean bool = zzic;
        if (bool != null) {
            return bool.booleanValue();
        }
        try {
            Boolean valueOf = Boolean.valueOf(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getBoolean("firebase_performance_logcat_enabled", false));
            zzic = valueOf;
            return valueOf.booleanValue();
        } catch (PackageManager.NameNotFoundException | NullPointerException e) {
            String valueOf2 = String.valueOf(e.getMessage());
            Log.d("isEnabled", valueOf2.length() != 0 ? "No perf logcat meta data found ".concat(valueOf2) : new String("No perf logcat meta data found "));
            return false;
        }
    }
}
