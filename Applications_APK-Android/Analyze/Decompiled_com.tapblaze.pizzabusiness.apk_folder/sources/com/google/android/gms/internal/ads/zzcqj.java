package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcqj implements zzdxg<zzcqh> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzcrk<zzcue>> zzeky;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzave> zzgev;

    private zzcqj(zzdxp<zzcrk<zzcue>> zzdxp, zzdxp<zzczu> zzdxp2, zzdxp<Context> zzdxp3, zzdxp<zzave> zzdxp4) {
        this.zzeky = zzdxp;
        this.zzfep = zzdxp2;
        this.zzejv = zzdxp3;
        this.zzgev = zzdxp4;
    }

    public static zzcqj zze(zzdxp<zzcrk<zzcue>> zzdxp, zzdxp<zzczu> zzdxp2, zzdxp<Context> zzdxp3, zzdxp<zzave> zzdxp4) {
        return new zzcqj(zzdxp, zzdxp2, zzdxp3, zzdxp4);
    }

    public final /* synthetic */ Object get() {
        return new zzcqh(this.zzeky.get(), this.zzfep.get(), this.zzejv.get(), this.zzgev.get());
    }
}
