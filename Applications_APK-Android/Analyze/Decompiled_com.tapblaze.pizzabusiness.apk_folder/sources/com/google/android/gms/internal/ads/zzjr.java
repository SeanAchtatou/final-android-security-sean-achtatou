package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
interface zzjr {
    void reset();

    void zza(zzju zzju);

    boolean zzb(zzjg zzjg) throws IOException, InterruptedException;
}
