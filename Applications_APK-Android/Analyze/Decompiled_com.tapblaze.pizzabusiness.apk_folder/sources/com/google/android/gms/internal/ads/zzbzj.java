package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbzj implements zzdgf {
    private final String zzcyr;
    private final String zzdbl;
    private final zzbzh zzfpu;

    zzbzj(zzbzh zzbzh, String str, String str2) {
        this.zzfpu = zzbzh;
        this.zzcyr = str;
        this.zzdbl = str2;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfpu.zza(this.zzcyr, this.zzdbl, obj);
    }
}
