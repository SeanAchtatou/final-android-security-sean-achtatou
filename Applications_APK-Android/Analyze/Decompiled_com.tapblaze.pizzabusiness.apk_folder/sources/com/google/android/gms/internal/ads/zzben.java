package com.google.android.gms.internal.ads;

import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.zzq;
import java.io.File;
import java.util.Collections;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzben extends zzbdl {
    public zzben(zzbdi zzbdi, zzsm zzsm, boolean z) {
        super(zzbdi, zzsm, z);
    }

    /* access modifiers changed from: protected */
    public final WebResourceResponse zza(WebView webView, String str, Map<String, String> map) {
        String str2;
        if (!(webView instanceof zzbdi)) {
            zzavs.zzez("Tried to intercept request from a WebView that wasn't an AdWebView.");
            return null;
        }
        zzbdi zzbdi = (zzbdi) webView;
        if (this.zzeeq != null) {
            this.zzeeq.zza(str, map, 1);
        }
        if (!"mraid.js".equalsIgnoreCase(new File(str).getName())) {
            if (map == null) {
                map = Collections.emptyMap();
            }
            return super.zzd(str, map);
        }
        if (zzbdi.zzaaa() != null) {
            zzbdi.zzaaa().zztn();
        }
        if (zzbdi.zzzy().zzabt()) {
            str2 = (String) zzve.zzoy().zzd(zzzn.zzchn);
        } else if (zzbdi.zzaaf()) {
            str2 = (String) zzve.zzoy().zzd(zzzn.zzchm);
        } else {
            str2 = (String) zzve.zzoy().zzd(zzzn.zzchl);
        }
        zzq.zzkq();
        return zzawb.zzd(zzbdi.getContext(), zzbdi.zzyr().zzbma, str2);
    }
}
