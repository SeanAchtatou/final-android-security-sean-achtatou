package com.google.android.gms.ads.query;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class InterstitialQueryDataConfiguration extends QueryDataConfiguration {
    public InterstitialQueryDataConfiguration(Context context, String str) {
        super(context, str);
    }
}
