package com.google.android.gms.internal.p000firebaseperf;

import java.util.Iterator;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzn  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public abstract class zzn<E> extends zzk<E> implements Set<E> {
    @NullableDecl
    private transient zzj<E> zzf;

    zzn() {
    }

    public boolean equals(@NullableDecl Object obj) {
        if (obj == this) {
            return true;
        }
        return zzw.zza(this, obj);
    }

    public int hashCode() {
        return zzw.zza(this);
    }

    public zzj<E> zzc() {
        zzj<E> zzj = this.zzf;
        if (zzj != null) {
            return zzj;
        }
        zzj<E> zzg = zzg();
        this.zzf = zzg;
        return zzg;
    }

    /* access modifiers changed from: package-private */
    public zzj<E> zzg() {
        return zzj.zza(toArray());
    }

    public /* synthetic */ Iterator iterator() {
        return iterator();
    }
}
