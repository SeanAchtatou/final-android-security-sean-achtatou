package com.google.android.gms.internal.ads;

import com.google.android.gms.games.Notifications;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzdrb extends zzdqh {
    private static final Logger logger = Logger.getLogger(zzdrb.class.getName());
    /* access modifiers changed from: private */
    public static final boolean zzhiu = zzduy.zzbcj();
    zzdrf zzhiv;

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static final class zzc extends zza {
        private final ByteBuffer zzhix;
        private int zzhiy;

        zzc(ByteBuffer byteBuffer) {
            super(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining());
            this.zzhix = byteBuffer;
            this.zzhiy = byteBuffer.position();
        }

        public final void flush() {
            this.zzhix.position(this.zzhiy + zzazf());
        }
    }

    public static int zzbo(boolean z) {
        return 1;
    }

    public static int zzc(double d) {
        return 8;
    }

    public static int zzfk(long j) {
        int i;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    public static int zzfm(long j) {
        return 8;
    }

    public static int zzfn(long j) {
        return 8;
    }

    private static long zzfo(long j) {
        return (j >> 63) ^ (j << 1);
    }

    public static int zzg(float f) {
        return 4;
    }

    public static int zzgb(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    public static int zzgd(int i) {
        return 4;
    }

    public static int zzge(int i) {
        return 4;
    }

    private static int zzgg(int i) {
        return (i >> 31) ^ (i << 1);
    }

    public static zzdrb zzw(byte[] bArr) {
        return new zza(bArr, 0, bArr.length);
    }

    public abstract void flush() throws IOException;

    public abstract void write(byte[] bArr, int i, int i2) throws IOException;

    public abstract void zza(int i, zzdqk zzdqk) throws IOException;

    public abstract void zza(int i, zzdte zzdte) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zza(int i, zzdte zzdte, zzdua zzdua) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zza(zzdte zzdte, zzdua zzdua) throws IOException;

    public abstract void zzaa(int i, int i2) throws IOException;

    public abstract void zzab(int i, int i2) throws IOException;

    public abstract void zzac(int i, int i2) throws IOException;

    public abstract void zzae(int i, int i2) throws IOException;

    public abstract int zzazc();

    public abstract void zzb(int i, zzdqk zzdqk) throws IOException;

    public abstract void zzb(int i, zzdte zzdte) throws IOException;

    public abstract void zzbe(zzdqk zzdqk) throws IOException;

    public abstract void zzd(byte b) throws IOException;

    public abstract void zzf(int i, String str) throws IOException;

    public abstract void zzfg(long j) throws IOException;

    public abstract void zzfi(long j) throws IOException;

    public abstract void zzfv(int i) throws IOException;

    public abstract void zzfw(int i) throws IOException;

    public abstract void zzfy(int i) throws IOException;

    public abstract void zzg(int i, long j) throws IOException;

    public abstract void zzg(zzdte zzdte) throws IOException;

    public abstract void zzh(int i, boolean z) throws IOException;

    public abstract void zzhg(String str) throws IOException;

    public abstract void zzi(int i, long j) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zzk(byte[] bArr, int i, int i2) throws IOException;

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static class zzb extends IOException {
        zzb() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        zzb(java.lang.String r3) {
            /*
                r2 = this;
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r0 = r3.length()
                java.lang.String r1 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                if (r0 == 0) goto L_0x0011
                java.lang.String r3 = r1.concat(r3)
                goto L_0x0016
            L_0x0011:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r1)
            L_0x0016:
                r2.<init>(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdrb.zzb.<init>(java.lang.String):void");
        }

        zzb(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        zzb(java.lang.String r3, java.lang.Throwable r4) {
            /*
                r2 = this;
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r0 = r3.length()
                java.lang.String r1 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                if (r0 == 0) goto L_0x0011
                java.lang.String r3 = r1.concat(r3)
                goto L_0x0016
            L_0x0011:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r1)
            L_0x0016:
                r2.<init>(r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdrb.zzb.<init>(java.lang.String, java.lang.Throwable):void");
        }
    }

    public static zzdrb zzm(ByteBuffer byteBuffer) {
        if (byteBuffer.hasArray()) {
            return new zzc(byteBuffer);
        }
        if (!byteBuffer.isDirect() || byteBuffer.isReadOnly()) {
            throw new IllegalArgumentException("ByteBuffer is read-only");
        } else if (zzduy.zzbck()) {
            return new zzd(byteBuffer);
        } else {
            return new zze(byteBuffer);
        }
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static final class zze extends zzdrb {
        private final ByteBuffer zzako;
        private final int zzhiy;
        private final ByteBuffer zzhiz;

        zze(ByteBuffer byteBuffer) {
            super();
            this.zzhiz = byteBuffer;
            this.zzako = byteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
            this.zzhiy = byteBuffer.position();
        }

        public final void zzaa(int i, int i2) throws IOException {
            zzfw((i << 3) | i2);
        }

        public final void zzab(int i, int i2) throws IOException {
            zzaa(i, 0);
            zzfv(i2);
        }

        public final void zzac(int i, int i2) throws IOException {
            zzaa(i, 0);
            zzfw(i2);
        }

        public final void zzae(int i, int i2) throws IOException {
            zzaa(i, 5);
            zzfy(i2);
        }

        public final void zzg(int i, long j) throws IOException {
            zzaa(i, 0);
            zzfg(j);
        }

        public final void zzi(int i, long j) throws IOException {
            zzaa(i, 1);
            zzfi(j);
        }

        public final void zzh(int i, boolean z) throws IOException {
            zzaa(i, 0);
            zzd(z ? (byte) 1 : 0);
        }

        public final void zzf(int i, String str) throws IOException {
            zzaa(i, 2);
            zzhg(str);
        }

        public final void zza(int i, zzdqk zzdqk) throws IOException {
            zzaa(i, 2);
            zzbe(zzdqk);
        }

        public final void zza(int i, zzdte zzdte) throws IOException {
            zzaa(i, 2);
            zzg(zzdte);
        }

        /* access modifiers changed from: package-private */
        public final void zza(int i, zzdte zzdte, zzdua zzdua) throws IOException {
            zzaa(i, 2);
            zza(zzdte, zzdua);
        }

        public final void zzb(int i, zzdte zzdte) throws IOException {
            zzaa(1, 3);
            zzac(2, i);
            zza(3, zzdte);
            zzaa(1, 4);
        }

        public final void zzb(int i, zzdqk zzdqk) throws IOException {
            zzaa(1, 3);
            zzac(2, i);
            zza(3, zzdqk);
            zzaa(1, 4);
        }

        public final void zzg(zzdte zzdte) throws IOException {
            zzfw(zzdte.zzazu());
            zzdte.zzb(this);
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzdte zzdte, zzdua zzdua) throws IOException {
            zzdqa zzdqa = (zzdqa) zzdte;
            int zzaxl = zzdqa.zzaxl();
            if (zzaxl == -1) {
                zzaxl = zzdua.zzax(zzdqa);
                zzdqa.zzfa(zzaxl);
            }
            zzfw(zzaxl);
            zzdua.zza(zzdte, this.zzhiv);
        }

        public final void zzd(byte b) throws IOException {
            try {
                this.zzako.put(b);
            } catch (BufferOverflowException e) {
                throw new zzb(e);
            }
        }

        public final void zzbe(zzdqk zzdqk) throws IOException {
            zzfw(zzdqk.size());
            zzdqk.zza(this);
        }

        public final void zzk(byte[] bArr, int i, int i2) throws IOException {
            zzfw(i2);
            write(bArr, 0, i2);
        }

        public final void zzfv(int i) throws IOException {
            if (i >= 0) {
                zzfw(i);
            } else {
                zzfg((long) i);
            }
        }

        public final void zzfw(int i) throws IOException {
            while ((i & -128) != 0) {
                this.zzako.put((byte) ((i & Notifications.NOTIFICATION_TYPES_ALL) | 128));
                i >>>= 7;
            }
            try {
                this.zzako.put((byte) i);
            } catch (BufferOverflowException e) {
                throw new zzb(e);
            }
        }

        public final void zzfy(int i) throws IOException {
            try {
                this.zzako.putInt(i);
            } catch (BufferOverflowException e) {
                throw new zzb(e);
            }
        }

        public final void zzfg(long j) throws IOException {
            while ((-128 & j) != 0) {
                this.zzako.put((byte) ((((int) j) & Notifications.NOTIFICATION_TYPES_ALL) | 128));
                j >>>= 7;
            }
            try {
                this.zzako.put((byte) ((int) j));
            } catch (BufferOverflowException e) {
                throw new zzb(e);
            }
        }

        public final void zzfi(long j) throws IOException {
            try {
                this.zzako.putLong(j);
            } catch (BufferOverflowException e) {
                throw new zzb(e);
            }
        }

        public final void write(byte[] bArr, int i, int i2) throws IOException {
            try {
                this.zzako.put(bArr, i, i2);
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(e);
            } catch (BufferOverflowException e2) {
                throw new zzb(e2);
            }
        }

        public final void zzh(byte[] bArr, int i, int i2) throws IOException {
            write(bArr, i, i2);
        }

        public final void zzhg(String str) throws IOException {
            int position = this.zzako.position();
            try {
                int zzgb = zzgb(str.length() * 3);
                int zzgb2 = zzgb(str.length());
                if (zzgb2 == zzgb) {
                    int position2 = this.zzako.position() + zzgb2;
                    this.zzako.position(position2);
                    zzhi(str);
                    int position3 = this.zzako.position();
                    this.zzako.position(position);
                    zzfw(position3 - position2);
                    this.zzako.position(position3);
                    return;
                }
                zzfw(zzdva.zza(str));
                zzhi(str);
            } catch (zzdve e) {
                this.zzako.position(position);
                zza(str, e);
            } catch (IllegalArgumentException e2) {
                throw new zzb(e2);
            }
        }

        public final void flush() {
            this.zzhiz.position(this.zzako.position());
        }

        public final int zzazc() {
            return this.zzako.remaining();
        }

        private final void zzhi(String str) throws IOException {
            try {
                zzdva.zza(str, this.zzako);
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(e);
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static final class zzd extends zzdrb {
        private final ByteBuffer zzako;
        private long zzamw = this.zzhjb;
        private final ByteBuffer zzhiz;
        private final long zzhja;
        private final long zzhjb;
        private final long zzhjc;
        private final long zzhjd = (this.zzhjc - 10);

        zzd(ByteBuffer byteBuffer) {
            super();
            this.zzhiz = byteBuffer;
            this.zzako = byteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
            this.zzhja = zzduy.zzn(byteBuffer);
            this.zzhjb = this.zzhja + ((long) byteBuffer.position());
            this.zzhjc = this.zzhja + ((long) byteBuffer.limit());
        }

        public final void zzaa(int i, int i2) throws IOException {
            zzfw((i << 3) | i2);
        }

        public final void zzab(int i, int i2) throws IOException {
            zzaa(i, 0);
            zzfv(i2);
        }

        public final void zzac(int i, int i2) throws IOException {
            zzaa(i, 0);
            zzfw(i2);
        }

        public final void zzae(int i, int i2) throws IOException {
            zzaa(i, 5);
            zzfy(i2);
        }

        public final void zzg(int i, long j) throws IOException {
            zzaa(i, 0);
            zzfg(j);
        }

        public final void zzi(int i, long j) throws IOException {
            zzaa(i, 1);
            zzfi(j);
        }

        public final void zzh(int i, boolean z) throws IOException {
            zzaa(i, 0);
            zzd(z ? (byte) 1 : 0);
        }

        public final void zzf(int i, String str) throws IOException {
            zzaa(i, 2);
            zzhg(str);
        }

        public final void zza(int i, zzdqk zzdqk) throws IOException {
            zzaa(i, 2);
            zzbe(zzdqk);
        }

        public final void zza(int i, zzdte zzdte) throws IOException {
            zzaa(i, 2);
            zzg(zzdte);
        }

        /* access modifiers changed from: package-private */
        public final void zza(int i, zzdte zzdte, zzdua zzdua) throws IOException {
            zzaa(i, 2);
            zza(zzdte, zzdua);
        }

        public final void zzb(int i, zzdte zzdte) throws IOException {
            zzaa(1, 3);
            zzac(2, i);
            zza(3, zzdte);
            zzaa(1, 4);
        }

        public final void zzb(int i, zzdqk zzdqk) throws IOException {
            zzaa(1, 3);
            zzac(2, i);
            zza(3, zzdqk);
            zzaa(1, 4);
        }

        public final void zzg(zzdte zzdte) throws IOException {
            zzfw(zzdte.zzazu());
            zzdte.zzb(this);
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzdte zzdte, zzdua zzdua) throws IOException {
            zzdqa zzdqa = (zzdqa) zzdte;
            int zzaxl = zzdqa.zzaxl();
            if (zzaxl == -1) {
                zzaxl = zzdua.zzax(zzdqa);
                zzdqa.zzfa(zzaxl);
            }
            zzfw(zzaxl);
            zzdua.zza(zzdte, this.zzhiv);
        }

        public final void zzd(byte b) throws IOException {
            long j = this.zzamw;
            if (j < this.zzhjc) {
                this.zzamw = 1 + j;
                zzduy.zza(j, b);
                return;
            }
            throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(j), Long.valueOf(this.zzhjc), 1));
        }

        public final void zzbe(zzdqk zzdqk) throws IOException {
            zzfw(zzdqk.size());
            zzdqk.zza(this);
        }

        public final void zzk(byte[] bArr, int i, int i2) throws IOException {
            zzfw(i2);
            write(bArr, 0, i2);
        }

        public final void zzfv(int i) throws IOException {
            if (i >= 0) {
                zzfw(i);
            } else {
                zzfg((long) i);
            }
        }

        public final void zzfw(int i) throws IOException {
            if (this.zzamw <= this.zzhjd) {
                while ((i & -128) != 0) {
                    long j = this.zzamw;
                    this.zzamw = j + 1;
                    zzduy.zza(j, (byte) ((i & Notifications.NOTIFICATION_TYPES_ALL) | 128));
                    i >>>= 7;
                }
                long j2 = this.zzamw;
                this.zzamw = 1 + j2;
                zzduy.zza(j2, (byte) i);
                return;
            }
            while (true) {
                long j3 = this.zzamw;
                if (j3 >= this.zzhjc) {
                    throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(j3), Long.valueOf(this.zzhjc), 1));
                } else if ((i & -128) == 0) {
                    this.zzamw = 1 + j3;
                    zzduy.zza(j3, (byte) i);
                    return;
                } else {
                    this.zzamw = j3 + 1;
                    zzduy.zza(j3, (byte) ((i & Notifications.NOTIFICATION_TYPES_ALL) | 128));
                    i >>>= 7;
                }
            }
        }

        public final void zzfy(int i) throws IOException {
            this.zzako.putInt((int) (this.zzamw - this.zzhja), i);
            this.zzamw += 4;
        }

        public final void zzfg(long j) throws IOException {
            if (this.zzamw <= this.zzhjd) {
                while ((j & -128) != 0) {
                    long j2 = this.zzamw;
                    this.zzamw = j2 + 1;
                    zzduy.zza(j2, (byte) ((((int) j) & Notifications.NOTIFICATION_TYPES_ALL) | 128));
                    j >>>= 7;
                }
                long j3 = this.zzamw;
                this.zzamw = 1 + j3;
                zzduy.zza(j3, (byte) ((int) j));
                return;
            }
            while (true) {
                long j4 = this.zzamw;
                if (j4 >= this.zzhjc) {
                    throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(j4), Long.valueOf(this.zzhjc), 1));
                } else if ((j & -128) == 0) {
                    this.zzamw = 1 + j4;
                    zzduy.zza(j4, (byte) ((int) j));
                    return;
                } else {
                    this.zzamw = j4 + 1;
                    zzduy.zza(j4, (byte) ((((int) j) & Notifications.NOTIFICATION_TYPES_ALL) | 128));
                    j >>>= 7;
                }
            }
        }

        public final void zzfi(long j) throws IOException {
            this.zzako.putLong((int) (this.zzamw - this.zzhja), j);
            this.zzamw += 8;
        }

        public final void write(byte[] bArr, int i, int i2) throws IOException {
            if (bArr != null && i >= 0 && i2 >= 0 && bArr.length - i2 >= i) {
                long j = (long) i2;
                long j2 = this.zzamw;
                if (this.zzhjc - j >= j2) {
                    zzduy.zza(bArr, (long) i, j2, j);
                    this.zzamw += j;
                    return;
                }
            }
            if (bArr == null) {
                throw new NullPointerException("value");
            }
            throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(this.zzamw), Long.valueOf(this.zzhjc), Integer.valueOf(i2)));
        }

        public final void zzh(byte[] bArr, int i, int i2) throws IOException {
            write(bArr, i, i2);
        }

        public final void zzhg(String str) throws IOException {
            long j = this.zzamw;
            try {
                int zzgb = zzgb(str.length() * 3);
                int zzgb2 = zzgb(str.length());
                if (zzgb2 == zzgb) {
                    int i = ((int) (this.zzamw - this.zzhja)) + zzgb2;
                    this.zzako.position(i);
                    zzdva.zza(str, this.zzako);
                    int position = this.zzako.position() - i;
                    zzfw(position);
                    this.zzamw += (long) position;
                    return;
                }
                int zza = zzdva.zza(str);
                zzfw(zza);
                zzfp(this.zzamw);
                zzdva.zza(str, this.zzako);
                this.zzamw += (long) zza;
            } catch (zzdve e) {
                this.zzamw = j;
                zzfp(this.zzamw);
                zza(str, e);
            } catch (IllegalArgumentException e2) {
                throw new zzb(e2);
            } catch (IndexOutOfBoundsException e3) {
                throw new zzb(e3);
            }
        }

        public final void flush() {
            this.zzhiz.position((int) (this.zzamw - this.zzhja));
        }

        public final int zzazc() {
            return (int) (this.zzhjc - this.zzamw);
        }

        private final void zzfp(long j) {
            this.zzako.position((int) (j - this.zzhja));
        }
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static class zza extends zzdrb {
        private final byte[] buffer;
        private final int limit;
        private final int offset;
        private int position;

        zza(byte[] bArr, int i, int i2) {
            super();
            if (bArr != null) {
                int i3 = i + i2;
                if ((i | i2 | (bArr.length - i3)) >= 0) {
                    this.buffer = bArr;
                    this.offset = i;
                    this.position = i;
                    this.limit = i3;
                    return;
                }
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)));
            }
            throw new NullPointerException("buffer");
        }

        public void flush() {
        }

        public final void zzaa(int i, int i2) throws IOException {
            zzfw((i << 3) | i2);
        }

        public final void zzab(int i, int i2) throws IOException {
            zzaa(i, 0);
            zzfv(i2);
        }

        public final void zzac(int i, int i2) throws IOException {
            zzaa(i, 0);
            zzfw(i2);
        }

        public final void zzae(int i, int i2) throws IOException {
            zzaa(i, 5);
            zzfy(i2);
        }

        public final void zzg(int i, long j) throws IOException {
            zzaa(i, 0);
            zzfg(j);
        }

        public final void zzi(int i, long j) throws IOException {
            zzaa(i, 1);
            zzfi(j);
        }

        public final void zzh(int i, boolean z) throws IOException {
            zzaa(i, 0);
            zzd(z ? (byte) 1 : 0);
        }

        public final void zzf(int i, String str) throws IOException {
            zzaa(i, 2);
            zzhg(str);
        }

        public final void zza(int i, zzdqk zzdqk) throws IOException {
            zzaa(i, 2);
            zzbe(zzdqk);
        }

        public final void zzbe(zzdqk zzdqk) throws IOException {
            zzfw(zzdqk.size());
            zzdqk.zza(this);
        }

        public final void zzk(byte[] bArr, int i, int i2) throws IOException {
            zzfw(i2);
            write(bArr, 0, i2);
        }

        public final void zza(int i, zzdte zzdte) throws IOException {
            zzaa(i, 2);
            zzg(zzdte);
        }

        /* access modifiers changed from: package-private */
        public final void zza(int i, zzdte zzdte, zzdua zzdua) throws IOException {
            zzaa(i, 2);
            zzdqa zzdqa = (zzdqa) zzdte;
            int zzaxl = zzdqa.zzaxl();
            if (zzaxl == -1) {
                zzaxl = zzdua.zzax(zzdqa);
                zzdqa.zzfa(zzaxl);
            }
            zzfw(zzaxl);
            zzdua.zza(zzdte, this.zzhiv);
        }

        public final void zzb(int i, zzdte zzdte) throws IOException {
            zzaa(1, 3);
            zzac(2, i);
            zza(3, zzdte);
            zzaa(1, 4);
        }

        public final void zzb(int i, zzdqk zzdqk) throws IOException {
            zzaa(1, 3);
            zzac(2, i);
            zza(3, zzdqk);
            zzaa(1, 4);
        }

        public final void zzg(zzdte zzdte) throws IOException {
            zzfw(zzdte.zzazu());
            zzdte.zzb(this);
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzdte zzdte, zzdua zzdua) throws IOException {
            zzdqa zzdqa = (zzdqa) zzdte;
            int zzaxl = zzdqa.zzaxl();
            if (zzaxl == -1) {
                zzaxl = zzdua.zzax(zzdqa);
                zzdqa.zzfa(zzaxl);
            }
            zzfw(zzaxl);
            zzdua.zza(zzdte, this.zzhiv);
        }

        public final void zzd(byte b) throws IOException {
            try {
                byte[] bArr = this.buffer;
                int i = this.position;
                this.position = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
            }
        }

        public final void zzfv(int i) throws IOException {
            if (i >= 0) {
                zzfw(i);
            } else {
                zzfg((long) i);
            }
        }

        public final void zzfw(int i) throws IOException {
            if (!zzdrb.zzhiu || zzdqd.zzaxn() || zzazc() < 5) {
                while ((i & -128) != 0) {
                    byte[] bArr = this.buffer;
                    int i2 = this.position;
                    this.position = i2 + 1;
                    bArr[i2] = (byte) ((i & Notifications.NOTIFICATION_TYPES_ALL) | 128);
                    i >>>= 7;
                }
                try {
                    byte[] bArr2 = this.buffer;
                    int i3 = this.position;
                    this.position = i3 + 1;
                    bArr2[i3] = (byte) i;
                } catch (IndexOutOfBoundsException e) {
                    throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
                }
            } else if ((i & -128) == 0) {
                byte[] bArr3 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                zzduy.zza(bArr3, (long) i4, (byte) i);
            } else {
                byte[] bArr4 = this.buffer;
                int i5 = this.position;
                this.position = i5 + 1;
                zzduy.zza(bArr4, (long) i5, (byte) (i | 128));
                int i6 = i >>> 7;
                if ((i6 & -128) == 0) {
                    byte[] bArr5 = this.buffer;
                    int i7 = this.position;
                    this.position = i7 + 1;
                    zzduy.zza(bArr5, (long) i7, (byte) i6);
                    return;
                }
                byte[] bArr6 = this.buffer;
                int i8 = this.position;
                this.position = i8 + 1;
                zzduy.zza(bArr6, (long) i8, (byte) (i6 | 128));
                int i9 = i6 >>> 7;
                if ((i9 & -128) == 0) {
                    byte[] bArr7 = this.buffer;
                    int i10 = this.position;
                    this.position = i10 + 1;
                    zzduy.zza(bArr7, (long) i10, (byte) i9);
                    return;
                }
                byte[] bArr8 = this.buffer;
                int i11 = this.position;
                this.position = i11 + 1;
                zzduy.zza(bArr8, (long) i11, (byte) (i9 | 128));
                int i12 = i9 >>> 7;
                if ((i12 & -128) == 0) {
                    byte[] bArr9 = this.buffer;
                    int i13 = this.position;
                    this.position = i13 + 1;
                    zzduy.zza(bArr9, (long) i13, (byte) i12);
                    return;
                }
                byte[] bArr10 = this.buffer;
                int i14 = this.position;
                this.position = i14 + 1;
                zzduy.zza(bArr10, (long) i14, (byte) (i12 | 128));
                byte[] bArr11 = this.buffer;
                int i15 = this.position;
                this.position = i15 + 1;
                zzduy.zza(bArr11, (long) i15, (byte) (i12 >>> 7));
            }
        }

        public final void zzfy(int i) throws IOException {
            try {
                byte[] bArr = this.buffer;
                int i2 = this.position;
                this.position = i2 + 1;
                bArr[i2] = (byte) i;
                byte[] bArr2 = this.buffer;
                int i3 = this.position;
                this.position = i3 + 1;
                bArr2[i3] = (byte) (i >> 8);
                byte[] bArr3 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                bArr3[i4] = (byte) (i >> 16);
                byte[] bArr4 = this.buffer;
                int i5 = this.position;
                this.position = i5 + 1;
                bArr4[i5] = (byte) (i >>> 24);
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
            }
        }

        public final void zzfg(long j) throws IOException {
            if (!zzdrb.zzhiu || zzazc() < 10) {
                while ((j & -128) != 0) {
                    byte[] bArr = this.buffer;
                    int i = this.position;
                    this.position = i + 1;
                    bArr[i] = (byte) ((((int) j) & Notifications.NOTIFICATION_TYPES_ALL) | 128);
                    j >>>= 7;
                }
                try {
                    byte[] bArr2 = this.buffer;
                    int i2 = this.position;
                    this.position = i2 + 1;
                    bArr2[i2] = (byte) ((int) j);
                } catch (IndexOutOfBoundsException e) {
                    throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
                }
            } else {
                while ((j & -128) != 0) {
                    byte[] bArr3 = this.buffer;
                    int i3 = this.position;
                    this.position = i3 + 1;
                    zzduy.zza(bArr3, (long) i3, (byte) ((((int) j) & Notifications.NOTIFICATION_TYPES_ALL) | 128));
                    j >>>= 7;
                }
                byte[] bArr4 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                zzduy.zza(bArr4, (long) i4, (byte) ((int) j));
            }
        }

        public final void zzfi(long j) throws IOException {
            try {
                byte[] bArr = this.buffer;
                int i = this.position;
                this.position = i + 1;
                bArr[i] = (byte) ((int) j);
                byte[] bArr2 = this.buffer;
                int i2 = this.position;
                this.position = i2 + 1;
                bArr2[i2] = (byte) ((int) (j >> 8));
                byte[] bArr3 = this.buffer;
                int i3 = this.position;
                this.position = i3 + 1;
                bArr3[i3] = (byte) ((int) (j >> 16));
                byte[] bArr4 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                bArr4[i4] = (byte) ((int) (j >> 24));
                byte[] bArr5 = this.buffer;
                int i5 = this.position;
                this.position = i5 + 1;
                bArr5[i5] = (byte) ((int) (j >> 32));
                byte[] bArr6 = this.buffer;
                int i6 = this.position;
                this.position = i6 + 1;
                bArr6[i6] = (byte) ((int) (j >> 40));
                byte[] bArr7 = this.buffer;
                int i7 = this.position;
                this.position = i7 + 1;
                bArr7[i7] = (byte) ((int) (j >> 48));
                byte[] bArr8 = this.buffer;
                int i8 = this.position;
                this.position = i8 + 1;
                bArr8[i8] = (byte) ((int) (j >> 56));
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
            }
        }

        public final void write(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.buffer, this.position, i2);
                this.position += i2;
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), Integer.valueOf(i2)), e);
            }
        }

        public final void zzh(byte[] bArr, int i, int i2) throws IOException {
            write(bArr, i, i2);
        }

        public final void zzhg(String str) throws IOException {
            int i = this.position;
            try {
                int zzgb = zzgb(str.length() * 3);
                int zzgb2 = zzgb(str.length());
                if (zzgb2 == zzgb) {
                    this.position = i + zzgb2;
                    int zza = zzdva.zza(str, this.buffer, this.position, zzazc());
                    this.position = i;
                    zzfw((zza - i) - zzgb2);
                    this.position = zza;
                    return;
                }
                zzfw(zzdva.zza(str));
                this.position = zzdva.zza(str, this.buffer, this.position, zzazc());
            } catch (zzdve e) {
                this.position = i;
                zza(str, e);
            } catch (IndexOutOfBoundsException e2) {
                throw new zzb(e2);
            }
        }

        public final int zzazc() {
            return this.limit - this.position;
        }

        public final int zzazf() {
            return this.position - this.offset;
        }
    }

    private zzdrb() {
    }

    public final void zzad(int i, int i2) throws IOException {
        zzac(i, zzgg(i2));
    }

    public final void zzh(int i, long j) throws IOException {
        zzg(i, zzfo(j));
    }

    public final void zza(int i, float f) throws IOException {
        zzae(i, Float.floatToRawIntBits(f));
    }

    public final void zzb(int i, double d) throws IOException {
        zzi(i, Double.doubleToRawLongBits(d));
    }

    public final void zzfx(int i) throws IOException {
        zzfw(zzgg(i));
    }

    public final void zzfh(long j) throws IOException {
        zzfg(zzfo(j));
    }

    public final void zzf(float f) throws IOException {
        zzfy(Float.floatToRawIntBits(f));
    }

    public final void zzb(double d) throws IOException {
        zzfi(Double.doubleToRawLongBits(d));
    }

    public final void zzbn(boolean z) throws IOException {
        zzd(z ? (byte) 1 : 0);
    }

    public static int zzaf(int i, int i2) {
        return zzfz(i) + zzga(i2);
    }

    public static int zzag(int i, int i2) {
        return zzfz(i) + zzgb(i2);
    }

    public static int zzah(int i, int i2) {
        return zzfz(i) + zzgb(zzgg(i2));
    }

    public static int zzai(int i, int i2) {
        return zzfz(i) + 4;
    }

    public static int zzaj(int i, int i2) {
        return zzfz(i) + 4;
    }

    public static int zzj(int i, long j) {
        return zzfz(i) + zzfk(j);
    }

    public static int zzk(int i, long j) {
        return zzfz(i) + zzfk(j);
    }

    public static int zzl(int i, long j) {
        return zzfz(i) + zzfk(zzfo(j));
    }

    public static int zzm(int i, long j) {
        return zzfz(i) + 8;
    }

    public static int zzn(int i, long j) {
        return zzfz(i) + 8;
    }

    public static int zzb(int i, float f) {
        return zzfz(i) + 4;
    }

    public static int zzc(int i, double d) {
        return zzfz(i) + 8;
    }

    public static int zzi(int i, boolean z) {
        return zzfz(i) + 1;
    }

    public static int zzak(int i, int i2) {
        return zzfz(i) + zzga(i2);
    }

    public static int zzg(int i, String str) {
        return zzfz(i) + zzhh(str);
    }

    public static int zzc(int i, zzdqk zzdqk) {
        int zzfz = zzfz(i);
        int size = zzdqk.size();
        return zzfz + zzgb(size) + size;
    }

    public static int zza(int i, zzdsj zzdsj) {
        int zzfz = zzfz(i);
        int zzazu = zzdsj.zzazu();
        return zzfz + zzgb(zzazu) + zzazu;
    }

    public static int zzc(int i, zzdte zzdte) {
        return zzfz(i) + zzh(zzdte);
    }

    static int zzb(int i, zzdte zzdte, zzdua zzdua) {
        return zzfz(i) + zzb(zzdte, zzdua);
    }

    public static int zzd(int i, zzdte zzdte) {
        return (zzfz(1) << 1) + zzag(2, i) + zzc(3, zzdte);
    }

    public static int zzd(int i, zzdqk zzdqk) {
        return (zzfz(1) << 1) + zzag(2, i) + zzc(3, zzdqk);
    }

    public static int zzb(int i, zzdsj zzdsj) {
        return (zzfz(1) << 1) + zzag(2, i) + zza(3, zzdsj);
    }

    public static int zzfz(int i) {
        return zzgb(i << 3);
    }

    public static int zzga(int i) {
        if (i >= 0) {
            return zzgb(i);
        }
        return 10;
    }

    public static int zzgc(int i) {
        return zzgb(zzgg(i));
    }

    public static int zzfj(long j) {
        return zzfk(j);
    }

    public static int zzfl(long j) {
        return zzfk(zzfo(j));
    }

    public static int zzgf(int i) {
        return zzga(i);
    }

    public static int zzhh(String str) {
        int i;
        try {
            i = zzdva.zza(str);
        } catch (zzdve unused) {
            i = str.getBytes(zzdrv.UTF_8).length;
        }
        return zzgb(i) + i;
    }

    public static int zza(zzdsj zzdsj) {
        int zzazu = zzdsj.zzazu();
        return zzgb(zzazu) + zzazu;
    }

    public static int zzbf(zzdqk zzdqk) {
        int size = zzdqk.size();
        return zzgb(size) + size;
    }

    public static int zzx(byte[] bArr) {
        int length = bArr.length;
        return zzgb(length) + length;
    }

    public static int zzh(zzdte zzdte) {
        int zzazu = zzdte.zzazu();
        return zzgb(zzazu) + zzazu;
    }

    static int zzb(zzdte zzdte, zzdua zzdua) {
        zzdqa zzdqa = (zzdqa) zzdte;
        int zzaxl = zzdqa.zzaxl();
        if (zzaxl == -1) {
            zzaxl = zzdua.zzax(zzdqa);
            zzdqa.zzfa(zzaxl);
        }
        return zzgb(zzaxl) + zzaxl;
    }

    public final void zzazd() {
        if (zzazc() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, com.google.android.gms.internal.ads.zzdve]
     candidates:
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    public final void zza(String str, zzdve zzdve) throws IOException {
        logger.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) zzdve);
        byte[] bytes = str.getBytes(zzdrv.UTF_8);
        try {
            zzfw(bytes.length);
            zzh(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new zzb(e);
        } catch (zzb e2) {
            throw e2;
        }
    }

    @Deprecated
    static int zzc(int i, zzdte zzdte, zzdua zzdua) {
        int zzfz = zzfz(i) << 1;
        zzdqa zzdqa = (zzdqa) zzdte;
        int zzaxl = zzdqa.zzaxl();
        if (zzaxl == -1) {
            zzaxl = zzdua.zzax(zzdqa);
            zzdqa.zzfa(zzaxl);
        }
        return zzfz + zzaxl;
    }

    @Deprecated
    public static int zzi(zzdte zzdte) {
        return zzdte.zzazu();
    }

    @Deprecated
    public static int zzgh(int i) {
        return zzgb(i);
    }
}
