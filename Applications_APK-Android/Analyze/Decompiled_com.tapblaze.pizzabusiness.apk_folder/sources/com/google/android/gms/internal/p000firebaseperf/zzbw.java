package com.google.android.gms.internal.p000firebaseperf;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbw  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzbw implements Parcelable.Creator<zzbt> {
    zzbw() {
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzbt[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zzbt(parcel, null);
    }
}
