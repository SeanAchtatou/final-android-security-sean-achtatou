package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzln implements zzdb<zzlm> {
    private static zzln zza = new zzln();
    private final zzdb<zzlm> zzb;

    public static boolean zzb() {
        return ((zzlm) zza.zza()).zza();
    }

    public static boolean zzc() {
        return ((zzlm) zza.zza()).zzb();
    }

    private zzln(zzdb<zzlm> zzdb) {
        this.zzb = zzda.zza((zzdb) zzdb);
    }

    public zzln() {
        this(zzda.zza(new zzlp()));
    }

    public final /* synthetic */ Object zza() {
        return this.zzb.zza();
    }
}
