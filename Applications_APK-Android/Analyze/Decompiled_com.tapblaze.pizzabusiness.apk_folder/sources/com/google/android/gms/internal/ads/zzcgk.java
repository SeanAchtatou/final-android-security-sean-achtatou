package com.google.android.gms.internal.ads;

import android.os.Binder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;
import java.io.InputStream;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzcgk implements BaseGmsClient.BaseConnectionCallbacks, BaseGmsClient.BaseOnConnectionFailedListener {
    protected final Object mLock = new Object();
    protected final zzazl<InputStream> zzdbf = new zzazl<>();
    protected boolean zzfvq = false;
    protected boolean zzfvr = false;
    protected zzaqk zzfvs;
    protected zzaps zzfvt;

    /* access modifiers changed from: protected */
    public final void zzalu() {
        synchronized (this.mLock) {
            this.zzfvr = true;
            if (this.zzfvt.isConnected() || this.zzfvt.isConnecting()) {
                this.zzfvt.disconnect();
            }
            Binder.flushPendingCommands();
        }
    }

    public void onConnectionSuspended(int i) {
        zzavs.zzea("Cannot connect to remote service, fallback to local instance.");
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        zzavs.zzea("Disconnected from remote ad request service.");
        this.zzdbf.setException(new zzcgr(0));
    }
}
