package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.Clock;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbju implements zzqj {
    private final Clock zzbmq;
    private Runnable zzdro = null;
    private final ScheduledExecutorService zzfdi;
    private ScheduledFuture<?> zzfdj;
    private long zzfdk = -1;
    private long zzfdl = -1;
    private boolean zzfdm = false;

    public zzbju(ScheduledExecutorService scheduledExecutorService, Clock clock) {
        this.zzfdi = scheduledExecutorService;
        this.zzbmq = clock;
        zzq.zzkt().zza(this);
    }

    public final void zzp(boolean z) {
        if (z) {
            zzafs();
        } else {
            zzafr();
        }
    }

    public final synchronized void zza(int i, Runnable runnable) {
        this.zzdro = runnable;
        long j = (long) i;
        this.zzfdk = this.zzbmq.elapsedRealtime() + j;
        this.zzfdj = this.zzfdi.schedule(runnable, j, TimeUnit.MILLISECONDS);
    }

    private final synchronized void zzafr() {
        if (!this.zzfdm) {
            if (this.zzfdj == null || this.zzfdj.isDone()) {
                this.zzfdl = -1;
            } else {
                this.zzfdj.cancel(true);
                this.zzfdl = this.zzfdk - this.zzbmq.elapsedRealtime();
            }
            this.zzfdm = true;
        }
    }

    private final synchronized void zzafs() {
        if (this.zzfdm) {
            if (this.zzfdl > 0 && this.zzfdj != null && this.zzfdj.isCancelled()) {
                this.zzfdj = this.zzfdi.schedule(this.zzdro, this.zzfdl, TimeUnit.MILLISECONDS);
            }
            this.zzfdm = false;
        }
    }
}
