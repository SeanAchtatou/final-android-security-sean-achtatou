package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzckv implements zzdgf {
    static final zzdgf zzbkw = new zzckv();

    private zzckv() {
    }

    public final zzdhe zzf(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (jSONObject.optBoolean("success")) {
            return zzdgs.zzaj(jSONObject.getJSONObject("json").getJSONArray("ads"));
        }
        throw new zzajr("process json failed");
    }
}
