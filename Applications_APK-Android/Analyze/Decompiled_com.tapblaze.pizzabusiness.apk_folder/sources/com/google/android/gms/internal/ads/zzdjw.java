package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdjw extends zzdii<zzdns> {
    zzdjw() {
        super(zzdns.class, new zzdjv(zzdhx.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.REMOTE;
    }

    public final zzdih<zzdnt, zzdns> zzasg() {
        return new zzdjy(this, zzdnt.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdpo.zzx(((zzdns) zzdte).getVersion(), 0);
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdns.zzaz(zzdqk);
    }
}
