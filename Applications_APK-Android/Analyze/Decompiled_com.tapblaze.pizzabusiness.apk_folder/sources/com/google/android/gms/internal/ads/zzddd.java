package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzddd implements Runnable {
    private final String zzcyr;
    private final zzdda zzgrp;

    zzddd(zzdda zzdda, String str) {
        this.zzgrp = zzdda;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzgrp.zzgp(this.zzcyr);
    }
}
