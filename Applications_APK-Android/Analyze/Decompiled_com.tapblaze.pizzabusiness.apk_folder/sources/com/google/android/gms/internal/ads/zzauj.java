package com.google.android.gms.internal.ads;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzauj implements ThreadFactory {
    private final AtomicInteger zzye = new AtomicInteger(1);

    zzauj(zzatv zzatv) {
    }

    public final Thread newThread(Runnable runnable) {
        int andIncrement = this.zzye.getAndIncrement();
        StringBuilder sb = new StringBuilder(42);
        sb.append("AdWorker(SCION_TASK_EXECUTOR) #");
        sb.append(andIncrement);
        return new Thread(runnable, sb.toString());
    }
}
