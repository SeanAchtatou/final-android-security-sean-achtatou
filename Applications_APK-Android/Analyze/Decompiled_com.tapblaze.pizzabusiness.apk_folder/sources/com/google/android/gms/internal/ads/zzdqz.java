package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdqz implements zzdtu {
    private int tag;
    private final zzdqw zzhir;
    private int zzhis;
    private int zzhit = 0;

    public static zzdqz zza(zzdqw zzdqw) {
        if (zzdqw.zzhih != null) {
            return zzdqw.zzhih;
        }
        return new zzdqz(zzdqw);
    }

    private zzdqz(zzdqw zzdqw) {
        this.zzhir = (zzdqw) zzdrv.zza(zzdqw, "input");
        this.zzhir.zzhih = this;
    }

    public final int zzaza() throws IOException {
        int i = this.zzhit;
        if (i != 0) {
            this.tag = i;
            this.zzhit = 0;
        } else {
            this.tag = this.zzhir.zzayc();
        }
        int i2 = this.tag;
        if (i2 == 0 || i2 == this.zzhis) {
            return Integer.MAX_VALUE;
        }
        return i2 >>> 3;
    }

    public final int getTag() {
        return this.tag;
    }

    public final boolean zzazb() throws IOException {
        int i;
        if (this.zzhir.zzays() || (i = this.tag) == this.zzhis) {
            return false;
        }
        return this.zzhir.zzfi(i);
    }

    private final void zzfr(int i) throws IOException {
        if ((this.tag & 7) != i) {
            throw zzdse.zzbao();
        }
    }

    public final double readDouble() throws IOException {
        zzfr(1);
        return this.zzhir.readDouble();
    }

    public final float readFloat() throws IOException {
        zzfr(5);
        return this.zzhir.readFloat();
    }

    public final long zzayd() throws IOException {
        zzfr(0);
        return this.zzhir.zzayd();
    }

    public final long zzaye() throws IOException {
        zzfr(0);
        return this.zzhir.zzaye();
    }

    public final int zzayf() throws IOException {
        zzfr(0);
        return this.zzhir.zzayf();
    }

    public final long zzayg() throws IOException {
        zzfr(1);
        return this.zzhir.zzayg();
    }

    public final int zzayh() throws IOException {
        zzfr(5);
        return this.zzhir.zzayh();
    }

    public final boolean zzayi() throws IOException {
        zzfr(0);
        return this.zzhir.zzayi();
    }

    public final String readString() throws IOException {
        zzfr(2);
        return this.zzhir.readString();
    }

    public final String zzayj() throws IOException {
        zzfr(2);
        return this.zzhir.zzayj();
    }

    public final <T> T zza(zzdua<T> zzdua, zzdrg zzdrg) throws IOException {
        zzfr(2);
        return zzc(zzdua, zzdrg);
    }

    public final <T> T zzb(zzdua<T> zzdua, zzdrg zzdrg) throws IOException {
        zzfr(3);
        return zzd(zzdua, zzdrg);
    }

    private final <T> T zzc(zzdua<T> zzdua, zzdrg zzdrg) throws IOException {
        int zzayl = this.zzhir.zzayl();
        if (this.zzhir.zzhie < this.zzhir.zzhif) {
            int zzfj = this.zzhir.zzfj(zzayl);
            T newInstance = zzdua.newInstance();
            this.zzhir.zzhie++;
            zzdua.zza(newInstance, this, zzdrg);
            zzdua.zzan(newInstance);
            this.zzhir.zzfh(0);
            zzdqw zzdqw = this.zzhir;
            zzdqw.zzhie--;
            this.zzhir.zzfk(zzfj);
            return newInstance;
        }
        throw new zzdse("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    private final <T> T zzd(zzdua<T> zzdua, zzdrg zzdrg) throws IOException {
        int i = this.zzhis;
        this.zzhis = ((this.tag >>> 3) << 3) | 4;
        try {
            T newInstance = zzdua.newInstance();
            zzdua.zza(newInstance, this, zzdrg);
            zzdua.zzan(newInstance);
            if (this.tag == this.zzhis) {
                return newInstance;
            }
            throw zzdse.zzbaq();
        } finally {
            this.zzhis = i;
        }
    }

    public final zzdqk zzayk() throws IOException {
        zzfr(2);
        return this.zzhir.zzayk();
    }

    public final int zzayl() throws IOException {
        zzfr(0);
        return this.zzhir.zzayl();
    }

    public final int zzaym() throws IOException {
        zzfr(0);
        return this.zzhir.zzaym();
    }

    public final int zzayn() throws IOException {
        zzfr(5);
        return this.zzhir.zzayn();
    }

    public final long zzayo() throws IOException {
        zzfr(1);
        return this.zzhir.zzayo();
    }

    public final int zzayp() throws IOException {
        zzfr(0);
        return this.zzhir.zzayp();
    }

    public final long zzayq() throws IOException {
        zzfr(0);
        return this.zzhir.zzayq();
    }

    public final void zzi(List<Double> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdre) {
            zzdre zzdre = (zzdre) list;
            int i = this.tag & 7;
            if (i == 1) {
                do {
                    zzdre.zzd(this.zzhir.readDouble());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayl = this.zzhir.zzayl();
                zzfs(zzayl);
                int zzayt = this.zzhir.zzayt() + zzayl;
                do {
                    zzdre.zzd(this.zzhir.readDouble());
                } while (this.zzhir.zzayt() < zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 1) {
                do {
                    list.add(Double.valueOf(this.zzhir.readDouble()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayl2 = this.zzhir.zzayl();
                zzfs(zzayl2);
                int zzayt2 = this.zzhir.zzayt() + zzayl2;
                do {
                    list.add(Double.valueOf(this.zzhir.readDouble()));
                } while (this.zzhir.zzayt() < zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzj(List<Float> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdrs) {
            zzdrs zzdrs = (zzdrs) list;
            int i = this.tag & 7;
            if (i == 2) {
                int zzayl = this.zzhir.zzayl();
                zzft(zzayl);
                int zzayt = this.zzhir.zzayt() + zzayl;
                do {
                    zzdrs.zzh(this.zzhir.readFloat());
                } while (this.zzhir.zzayt() < zzayt);
            } else if (i == 5) {
                do {
                    zzdrs.zzh(this.zzhir.readFloat());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 2) {
                int zzayl2 = this.zzhir.zzayl();
                zzft(zzayl2);
                int zzayt2 = this.zzhir.zzayt() + zzayl2;
                do {
                    list.add(Float.valueOf(this.zzhir.readFloat()));
                } while (this.zzhir.zzayt() < zzayt2);
            } else if (i2 == 5) {
                do {
                    list.add(Float.valueOf(this.zzhir.readFloat()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzk(List<Long> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdss) {
            zzdss zzdss = (zzdss) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdss.zzfr(this.zzhir.zzayd());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayt = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    zzdss.zzfr(this.zzhir.zzayd());
                } while (this.zzhir.zzayt() < zzayt);
                zzfu(zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.zzhir.zzayd()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayt2 = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    list.add(Long.valueOf(this.zzhir.zzayd()));
                } while (this.zzhir.zzayt() < zzayt2);
                zzfu(zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzl(List<Long> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdss) {
            zzdss zzdss = (zzdss) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdss.zzfr(this.zzhir.zzaye());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayt = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    zzdss.zzfr(this.zzhir.zzaye());
                } while (this.zzhir.zzayt() < zzayt);
                zzfu(zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.zzhir.zzaye()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayt2 = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    list.add(Long.valueOf(this.zzhir.zzaye()));
                } while (this.zzhir.zzayt() < zzayt2);
                zzfu(zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzm(List<Integer> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdrw.zzgl(this.zzhir.zzayf());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayt = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    zzdrw.zzgl(this.zzhir.zzayf());
                } while (this.zzhir.zzayt() < zzayt);
                zzfu(zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayf()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayt2 = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayf()));
                } while (this.zzhir.zzayt() < zzayt2);
                zzfu(zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzn(List<Long> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdss) {
            zzdss zzdss = (zzdss) list;
            int i = this.tag & 7;
            if (i == 1) {
                do {
                    zzdss.zzfr(this.zzhir.zzayg());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayl = this.zzhir.zzayl();
                zzfs(zzayl);
                int zzayt = this.zzhir.zzayt() + zzayl;
                do {
                    zzdss.zzfr(this.zzhir.zzayg());
                } while (this.zzhir.zzayt() < zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.zzhir.zzayg()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayl2 = this.zzhir.zzayl();
                zzfs(zzayl2);
                int zzayt2 = this.zzhir.zzayt() + zzayl2;
                do {
                    list.add(Long.valueOf(this.zzhir.zzayg()));
                } while (this.zzhir.zzayt() < zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzo(List<Integer> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            int i = this.tag & 7;
            if (i == 2) {
                int zzayl = this.zzhir.zzayl();
                zzft(zzayl);
                int zzayt = this.zzhir.zzayt() + zzayl;
                do {
                    zzdrw.zzgl(this.zzhir.zzayh());
                } while (this.zzhir.zzayt() < zzayt);
            } else if (i == 5) {
                do {
                    zzdrw.zzgl(this.zzhir.zzayh());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 2) {
                int zzayl2 = this.zzhir.zzayl();
                zzft(zzayl2);
                int zzayt2 = this.zzhir.zzayt() + zzayl2;
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayh()));
                } while (this.zzhir.zzayt() < zzayt2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayh()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzp(List<Boolean> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdqi) {
            zzdqi zzdqi = (zzdqi) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdqi.addBoolean(this.zzhir.zzayi());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayt = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    zzdqi.addBoolean(this.zzhir.zzayi());
                } while (this.zzhir.zzayt() < zzayt);
                zzfu(zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Boolean.valueOf(this.zzhir.zzayi()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayt2 = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    list.add(Boolean.valueOf(this.zzhir.zzayi()));
                } while (this.zzhir.zzayt() < zzayt2);
                zzfu(zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.util.List<java.lang.String>, boolean):void
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(com.google.android.gms.internal.ads.zzdua, com.google.android.gms.internal.ads.zzdrg):T
      com.google.android.gms.internal.ads.zzdtu.zza(com.google.android.gms.internal.ads.zzdua, com.google.android.gms.internal.ads.zzdrg):T
      com.google.android.gms.internal.ads.zzdqz.zza(java.util.List<java.lang.String>, boolean):void */
    public final void readStringList(List<String> list) throws IOException {
        zza(list, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdqz.zza(java.util.List<java.lang.String>, boolean):void
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.internal.ads.zzdqz.zza(com.google.android.gms.internal.ads.zzdua, com.google.android.gms.internal.ads.zzdrg):T
      com.google.android.gms.internal.ads.zzdtu.zza(com.google.android.gms.internal.ads.zzdua, com.google.android.gms.internal.ads.zzdrg):T
      com.google.android.gms.internal.ads.zzdqz.zza(java.util.List<java.lang.String>, boolean):void */
    public final void zzq(List<String> list) throws IOException {
        zza(list, true);
    }

    private final void zza(List<String> list, boolean z) throws IOException {
        int zzayc;
        int zzayc2;
        if ((this.tag & 7) != 2) {
            throw zzdse.zzbao();
        } else if (!(list instanceof zzdsl) || z) {
            do {
                list.add(z ? zzayj() : readString());
                if (!this.zzhir.zzays()) {
                    zzayc = this.zzhir.zzayc();
                } else {
                    return;
                }
            } while (zzayc == this.tag);
            this.zzhit = zzayc;
        } else {
            zzdsl zzdsl = (zzdsl) list;
            do {
                zzdsl.zzbg(zzayk());
                if (!this.zzhir.zzays()) {
                    zzayc2 = this.zzhir.zzayc();
                } else {
                    return;
                }
            } while (zzayc2 == this.tag);
            this.zzhit = zzayc2;
        }
    }

    public final <T> void zza(List<T> list, zzdua<T> zzdua, zzdrg zzdrg) throws IOException {
        int zzayc;
        int i = this.tag;
        if ((i & 7) == 2) {
            do {
                list.add(zzc(zzdua, zzdrg));
                if (!this.zzhir.zzays() && this.zzhit == 0) {
                    zzayc = this.zzhir.zzayc();
                } else {
                    return;
                }
            } while (zzayc == i);
            this.zzhit = zzayc;
            return;
        }
        throw zzdse.zzbao();
    }

    public final <T> void zzb(List<T> list, zzdua<T> zzdua, zzdrg zzdrg) throws IOException {
        int zzayc;
        int i = this.tag;
        if ((i & 7) == 3) {
            do {
                list.add(zzd(zzdua, zzdrg));
                if (!this.zzhir.zzays() && this.zzhit == 0) {
                    zzayc = this.zzhir.zzayc();
                } else {
                    return;
                }
            } while (zzayc == i);
            this.zzhit = zzayc;
            return;
        }
        throw zzdse.zzbao();
    }

    public final void zzr(List<zzdqk> list) throws IOException {
        int zzayc;
        if ((this.tag & 7) == 2) {
            do {
                list.add(zzayk());
                if (!this.zzhir.zzays()) {
                    zzayc = this.zzhir.zzayc();
                } else {
                    return;
                }
            } while (zzayc == this.tag);
            this.zzhit = zzayc;
            return;
        }
        throw zzdse.zzbao();
    }

    public final void zzs(List<Integer> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdrw.zzgl(this.zzhir.zzayl());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayt = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    zzdrw.zzgl(this.zzhir.zzayl());
                } while (this.zzhir.zzayt() < zzayt);
                zzfu(zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayl()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayt2 = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayl()));
                } while (this.zzhir.zzayt() < zzayt2);
                zzfu(zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzt(List<Integer> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdrw.zzgl(this.zzhir.zzaym());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayt = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    zzdrw.zzgl(this.zzhir.zzaym());
                } while (this.zzhir.zzayt() < zzayt);
                zzfu(zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzhir.zzaym()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayt2 = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    list.add(Integer.valueOf(this.zzhir.zzaym()));
                } while (this.zzhir.zzayt() < zzayt2);
                zzfu(zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzu(List<Integer> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            int i = this.tag & 7;
            if (i == 2) {
                int zzayl = this.zzhir.zzayl();
                zzft(zzayl);
                int zzayt = this.zzhir.zzayt() + zzayl;
                do {
                    zzdrw.zzgl(this.zzhir.zzayn());
                } while (this.zzhir.zzayt() < zzayt);
            } else if (i == 5) {
                do {
                    zzdrw.zzgl(this.zzhir.zzayn());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 2) {
                int zzayl2 = this.zzhir.zzayl();
                zzft(zzayl2);
                int zzayt2 = this.zzhir.zzayt() + zzayl2;
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayn()));
                } while (this.zzhir.zzayt() < zzayt2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayn()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzv(List<Long> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdss) {
            zzdss zzdss = (zzdss) list;
            int i = this.tag & 7;
            if (i == 1) {
                do {
                    zzdss.zzfr(this.zzhir.zzayo());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayl = this.zzhir.zzayl();
                zzfs(zzayl);
                int zzayt = this.zzhir.zzayt() + zzayl;
                do {
                    zzdss.zzfr(this.zzhir.zzayo());
                } while (this.zzhir.zzayt() < zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.zzhir.zzayo()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayl2 = this.zzhir.zzayl();
                zzfs(zzayl2);
                int zzayt2 = this.zzhir.zzayt() + zzayl2;
                do {
                    list.add(Long.valueOf(this.zzhir.zzayo()));
                } while (this.zzhir.zzayt() < zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzw(List<Integer> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdrw) {
            zzdrw zzdrw = (zzdrw) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdrw.zzgl(this.zzhir.zzayp());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayt = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    zzdrw.zzgl(this.zzhir.zzayp());
                } while (this.zzhir.zzayt() < zzayt);
                zzfu(zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayp()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayt2 = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    list.add(Integer.valueOf(this.zzhir.zzayp()));
                } while (this.zzhir.zzayt() < zzayt2);
                zzfu(zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    public final void zzx(List<Long> list) throws IOException {
        int zzayc;
        int zzayc2;
        if (list instanceof zzdss) {
            zzdss zzdss = (zzdss) list;
            int i = this.tag & 7;
            if (i == 0) {
                do {
                    zzdss.zzfr(this.zzhir.zzayq());
                    if (!this.zzhir.zzays()) {
                        zzayc2 = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc2 == this.tag);
                this.zzhit = zzayc2;
            } else if (i == 2) {
                int zzayt = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    zzdss.zzfr(this.zzhir.zzayq());
                } while (this.zzhir.zzayt() < zzayt);
                zzfu(zzayt);
            } else {
                throw zzdse.zzbao();
            }
        } else {
            int i2 = this.tag & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.zzhir.zzayq()));
                    if (!this.zzhir.zzays()) {
                        zzayc = this.zzhir.zzayc();
                    } else {
                        return;
                    }
                } while (zzayc == this.tag);
                this.zzhit = zzayc;
            } else if (i2 == 2) {
                int zzayt2 = this.zzhir.zzayt() + this.zzhir.zzayl();
                do {
                    list.add(Long.valueOf(this.zzhir.zzayq()));
                } while (this.zzhir.zzayt() < zzayt2);
                zzfu(zzayt2);
            } else {
                throw zzdse.zzbao();
            }
        }
    }

    private static void zzfs(int i) throws IOException {
        if ((i & 7) != 0) {
            throw zzdse.zzbaq();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0052, code lost:
        if (zzazb() != false) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        throw new com.google.android.gms.internal.ads.zzdse("Unable to parse map entry.");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x004e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <K, V> void zza(java.util.Map<K, V> r8, com.google.android.gms.internal.ads.zzdsv<K, V> r9, com.google.android.gms.internal.ads.zzdrg r10) throws java.io.IOException {
        /*
            r7 = this;
            r0 = 2
            r7.zzfr(r0)
            com.google.android.gms.internal.ads.zzdqw r1 = r7.zzhir
            int r1 = r1.zzayl()
            com.google.android.gms.internal.ads.zzdqw r2 = r7.zzhir
            int r1 = r2.zzfj(r1)
            K r2 = r9.zzhor
            V r3 = r9.zzcfu
        L_0x0014:
            int r4 = r7.zzaza()     // Catch:{ all -> 0x0064 }
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r4 == r5) goto L_0x005b
            com.google.android.gms.internal.ads.zzdqw r5 = r7.zzhir     // Catch:{ all -> 0x0064 }
            boolean r5 = r5.zzays()     // Catch:{ all -> 0x0064 }
            if (r5 != 0) goto L_0x005b
            r5 = 1
            java.lang.String r6 = "Unable to parse map entry."
            if (r4 == r5) goto L_0x0046
            if (r4 == r0) goto L_0x0039
            boolean r4 = r7.zzazb()     // Catch:{ zzdsd -> 0x004e }
            if (r4 == 0) goto L_0x0033
            goto L_0x0014
        L_0x0033:
            com.google.android.gms.internal.ads.zzdse r4 = new com.google.android.gms.internal.ads.zzdse     // Catch:{ zzdsd -> 0x004e }
            r4.<init>(r6)     // Catch:{ zzdsd -> 0x004e }
            throw r4     // Catch:{ zzdsd -> 0x004e }
        L_0x0039:
            com.google.android.gms.internal.ads.zzdvf r4 = r9.zzhos     // Catch:{ zzdsd -> 0x004e }
            V r5 = r9.zzcfu     // Catch:{ zzdsd -> 0x004e }
            java.lang.Class r5 = r5.getClass()     // Catch:{ zzdsd -> 0x004e }
            java.lang.Object r3 = r7.zza(r4, r5, r10)     // Catch:{ zzdsd -> 0x004e }
            goto L_0x0014
        L_0x0046:
            com.google.android.gms.internal.ads.zzdvf r4 = r9.zzhoq     // Catch:{ zzdsd -> 0x004e }
            r5 = 0
            java.lang.Object r2 = r7.zza(r4, r5, r5)     // Catch:{ zzdsd -> 0x004e }
            goto L_0x0014
        L_0x004e:
            boolean r4 = r7.zzazb()     // Catch:{ all -> 0x0064 }
            if (r4 == 0) goto L_0x0055
            goto L_0x0014
        L_0x0055:
            com.google.android.gms.internal.ads.zzdse r8 = new com.google.android.gms.internal.ads.zzdse     // Catch:{ all -> 0x0064 }
            r8.<init>(r6)     // Catch:{ all -> 0x0064 }
            throw r8     // Catch:{ all -> 0x0064 }
        L_0x005b:
            r8.put(r2, r3)     // Catch:{ all -> 0x0064 }
            com.google.android.gms.internal.ads.zzdqw r8 = r7.zzhir
            r8.zzfk(r1)
            return
        L_0x0064:
            r8 = move-exception
            com.google.android.gms.internal.ads.zzdqw r9 = r7.zzhir
            r9.zzfk(r1)
            goto L_0x006c
        L_0x006b:
            throw r8
        L_0x006c:
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdqz.zza(java.util.Map, com.google.android.gms.internal.ads.zzdsv, com.google.android.gms.internal.ads.zzdrg):void");
    }

    private final Object zza(zzdvf zzdvf, Class<?> cls, zzdrg zzdrg) throws IOException {
        switch (zzdrc.zzhiw[zzdvf.ordinal()]) {
            case 1:
                return Boolean.valueOf(zzayi());
            case 2:
                return zzayk();
            case 3:
                return Double.valueOf(readDouble());
            case 4:
                return Integer.valueOf(zzaym());
            case 5:
                return Integer.valueOf(zzayh());
            case 6:
                return Long.valueOf(zzayg());
            case 7:
                return Float.valueOf(readFloat());
            case 8:
                return Integer.valueOf(zzayf());
            case 9:
                return Long.valueOf(zzaye());
            case 10:
                zzfr(2);
                return zzc(zzdtp.zzbbm().zzh(cls), zzdrg);
            case 11:
                return Integer.valueOf(zzayn());
            case 12:
                return Long.valueOf(zzayo());
            case 13:
                return Integer.valueOf(zzayp());
            case 14:
                return Long.valueOf(zzayq());
            case 15:
                return zzayj();
            case 16:
                return Integer.valueOf(zzayl());
            case 17:
                return Long.valueOf(zzayd());
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    private static void zzft(int i) throws IOException {
        if ((i & 3) != 0) {
            throw zzdse.zzbaq();
        }
    }

    private final void zzfu(int i) throws IOException {
        if (this.zzhir.zzayt() != i) {
            throw zzdse.zzbaj();
        }
    }
}
