package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdiy {
    public static final String zzgyr = new zzdjc().getKeyType();
    public static final String zzgys = new zzdjm().getKeyType();
    private static final String zzgyt = new zzdjh().getKeyType();
    private static final String zzgyu = new zzdjs().getKeyType();
    private static final String zzgyv = new zzdjw().getKeyType();
    private static final String zzgyw = new zzdjn().getKeyType();
    private static final String zzgyx = new zzdjx().getKeyType();
    @Deprecated
    private static final zzdny zzgyy;
    @Deprecated
    private static final zzdny zzgyz;
    @Deprecated
    private static final zzdny zzgza = zzgyy;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdjc, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class):com.google.android.gms.internal.ads.zzdid<P>
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdid, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdjh, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class):com.google.android.gms.internal.ads.zzdid<P>
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdid, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdjm, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class):com.google.android.gms.internal.ads.zzdid<P>
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdid, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdjn, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class):com.google.android.gms.internal.ads.zzdid<P>
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdid, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdjs, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class):com.google.android.gms.internal.ads.zzdid<P>
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdid, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdjw, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class):com.google.android.gms.internal.ads.zzdid<P>
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdid, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdjx, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class):com.google.android.gms.internal.ads.zzdid<P>
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdid, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void */
    public static void zzasq() throws GeneralSecurityException {
        zzdku.zzasq();
        zzdit.zza((zzdii) new zzdjc(), true);
        zzdit.zza((zzdii) new zzdjh(), true);
        zzdit.zza((zzdii) new zzdjm(), true);
        zzdit.zza((zzdii) new zzdjn(), true);
        zzdit.zza((zzdii) new zzdjs(), true);
        zzdit.zza((zzdii) new zzdjw(), true);
        zzdit.zza((zzdii) new zzdjx(), true);
        zzdit.zza(new zzdjb());
    }

    static {
        zzdny zzawv = zzdny.zzawv();
        zzgyy = zzawv;
        zzgyz = zzawv;
        try {
            zzasq();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }
}
