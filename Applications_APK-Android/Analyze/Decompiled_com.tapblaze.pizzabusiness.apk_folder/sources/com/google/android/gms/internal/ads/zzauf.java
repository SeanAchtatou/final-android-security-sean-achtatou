package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzauf implements zzaui {
    static final zzaui zzdpr = new zzauf();

    private zzauf() {
    }

    public final Object zzb(zzbfq zzbfq) {
        return zzbfq.getAppInstanceId();
    }
}
