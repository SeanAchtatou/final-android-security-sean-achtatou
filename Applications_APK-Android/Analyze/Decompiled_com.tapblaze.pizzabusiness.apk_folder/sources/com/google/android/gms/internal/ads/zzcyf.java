package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcyf implements zzcxo {
    private final String zzcyr;
    private final String zzdbl;
    private final zzare zzfhs;

    zzcyf(zzare zzare, String str, String str2) {
        this.zzfhs = zzare;
        this.zzcyr = str;
        this.zzdbl = str2;
    }

    public final void zzt(Object obj) {
        ((zzarj) obj).zza(this.zzfhs, this.zzcyr, this.zzdbl);
    }
}
