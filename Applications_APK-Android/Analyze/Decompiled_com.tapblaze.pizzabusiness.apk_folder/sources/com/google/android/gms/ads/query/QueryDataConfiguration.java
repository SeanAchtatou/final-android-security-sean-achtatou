package com.google.android.gms.ads.query;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class QueryDataConfiguration {
    private final String zzbqz;
    private final Context zzup;

    QueryDataConfiguration(Context context, String str) {
        this.zzup = context;
        this.zzbqz = str;
    }

    public Context getContext() {
        return this.zzup;
    }

    public String getAdUnitId() {
        return this.zzbqz;
    }
}
