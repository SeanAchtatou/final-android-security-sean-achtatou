package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdju extends zzdih<zzdnp, zzdno> {
    private final /* synthetic */ zzdjs zzgzk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdju(zzdjs zzdjs, Class cls) {
        super(cls);
        this.zzgzk = zzdjs;
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        return (zzdno) ((zzdrt) zzdno.zzawi().zzb((zzdnp) zzdte).zzeu(0).zzbaf());
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdnp.zzay(zzdqk);
    }

    public final /* bridge */ /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdnp zzdnp = (zzdnp) zzdte;
    }
}
