package com.google.android.gms.internal.ads;

import com.flurry.android.Constants;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzdqk implements Serializable, Iterable<Byte> {
    public static final zzdqk zzhhx = new zzdqu(zzdrv.zzhng);
    private static final zzdqq zzhhy = (zzdqd.zzaxn() ? new zzdqt(null) : new zzdqo(null));
    private static final Comparator<zzdqk> zzhhz = new zzdqm();
    private int zzhhd = 0;

    zzdqk() {
    }

    /* access modifiers changed from: private */
    public static int zzb(byte b) {
        return b & Constants.UNKNOWN;
    }

    public abstract boolean equals(Object obj);

    public abstract int size();

    /* access modifiers changed from: protected */
    public abstract String zza(Charset charset);

    /* access modifiers changed from: package-private */
    public abstract void zza(zzdqh zzdqh) throws IOException;

    public abstract boolean zzaxu();

    public abstract zzdqw zzaxv();

    /* access modifiers changed from: protected */
    public abstract int zzaxw();

    /* access modifiers changed from: protected */
    public abstract boolean zzaxx();

    /* access modifiers changed from: protected */
    public abstract void zzb(byte[] bArr, int i, int i2, int i3);

    public abstract byte zzfe(int i);

    /* access modifiers changed from: package-private */
    public abstract byte zzff(int i);

    /* access modifiers changed from: protected */
    public abstract int zzg(int i, int i2, int i3);

    /* access modifiers changed from: protected */
    public abstract int zzh(int i, int i2, int i3);

    public abstract zzdqk zzy(int i, int i2);

    /* renamed from: zzaxs */
    public zzdqp iterator() {
        return new zzdqj(this);
    }

    public final boolean isEmpty() {
        return size() == 0;
    }

    public static zzdqk zzi(byte[] bArr, int i, int i2) {
        zzi(i, i + i2, bArr.length);
        return new zzdqu(zzhhy.zzj(bArr, i, i2));
    }

    public static zzdqk zzu(byte[] bArr) {
        return zzi(bArr, 0, bArr.length);
    }

    static zzdqk zzv(byte[] bArr) {
        return new zzdqu(bArr);
    }

    public static zzdqk zzhf(String str) {
        return new zzdqu(str.getBytes(zzdrv.UTF_8));
    }

    public static zzdqk zzf(InputStream inputStream) throws IOException {
        zzdqk zzdqk;
        ArrayList arrayList = new ArrayList();
        int i = 256;
        while (true) {
            byte[] bArr = new byte[i];
            int i2 = 0;
            while (i2 < i) {
                int read = inputStream.read(bArr, i2, i - i2);
                if (read == -1) {
                    break;
                }
                i2 += read;
            }
            if (i2 == 0) {
                zzdqk = null;
            } else {
                zzdqk = zzi(bArr, 0, i2);
            }
            if (zzdqk == null) {
                break;
            }
            arrayList.add(zzdqk);
            i = Math.min(i << 1, 8192);
        }
        int size = arrayList.size();
        if (size == 0) {
            return zzhhx;
        }
        return zza(arrayList.iterator(), size);
    }

    private static zzdqk zza(Iterator<zzdqk> it, int i) {
        if (i <= 0) {
            throw new IllegalArgumentException(String.format("length (%s) must be >= 1", Integer.valueOf(i)));
        } else if (i == 1) {
            return it.next();
        } else {
            int i2 = i >>> 1;
            zzdqk zza = zza(it, i2);
            zzdqk zza2 = zza(it, i - i2);
            if (Integer.MAX_VALUE - zza.size() >= zza2.size()) {
                return zzdtt.zza(zza, zza2);
            }
            int size = zza.size();
            int size2 = zza2.size();
            StringBuilder sb = new StringBuilder(53);
            sb.append("ByteString would be too long: ");
            sb.append(size);
            sb.append("+");
            sb.append(size2);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    @Deprecated
    public final void zza(byte[] bArr, int i, int i2, int i3) {
        zzi(i, i + i3, size());
        zzi(i2, i2 + i3, bArr.length);
        if (i3 > 0) {
            zzb(bArr, i, i2, i3);
        }
    }

    public final byte[] toByteArray() {
        int size = size();
        if (size == 0) {
            return zzdrv.zzhng;
        }
        byte[] bArr = new byte[size];
        zzb(bArr, 0, 0, size);
        return bArr;
    }

    public final String zzaxt() {
        return size() == 0 ? "" : zza(zzdrv.UTF_8);
    }

    public final int hashCode() {
        int i = this.zzhhd;
        if (i == 0) {
            int size = size();
            i = zzh(size, 0, size);
            if (i == 0) {
                i = 1;
            }
            this.zzhhd = i;
        }
        return i;
    }

    static zzdqs zzfg(int i) {
        return new zzdqs(i, null);
    }

    /* access modifiers changed from: protected */
    public final int zzaxy() {
        return this.zzhhd;
    }

    static void zzz(int i, int i2) {
        if (((i2 - (i + 1)) | i) >= 0) {
            return;
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(22);
            sb.append("Index < 0: ");
            sb.append(i);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder(40);
        sb2.append("Index > length: ");
        sb2.append(i);
        sb2.append(", ");
        sb2.append(i2);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }

    static int zzi(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Beginning index: ");
            sb.append(i);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (i2 < i) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            throw new IndexOutOfBoundsException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder(37);
            sb3.append("End index: ");
            sb3.append(i2);
            sb3.append(" >= ");
            sb3.append(i3);
            throw new IndexOutOfBoundsException(sb3.toString());
        }
    }

    public final String toString() {
        Locale locale = Locale.ROOT;
        Object[] objArr = new Object[3];
        objArr[0] = Integer.toHexString(System.identityHashCode(this));
        objArr[1] = Integer.valueOf(size());
        objArr[2] = size() <= 50 ? zzduo.zzbj(this) : String.valueOf(zzduo.zzbj(zzy(0, 47))).concat("...");
        return String.format(locale, "<ByteString@%s size=%d contents=\"%s\">", objArr);
    }
}
