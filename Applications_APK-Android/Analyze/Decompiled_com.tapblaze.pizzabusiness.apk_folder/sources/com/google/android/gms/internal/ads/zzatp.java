package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzatp {
    public static void zzea(String str) {
        if (zzabf.zzcud.get().booleanValue()) {
            zzavs.zzea(str);
        }
    }

    public static boolean isEnabled() {
        return zzabf.zzcud.get().booleanValue();
    }
}
