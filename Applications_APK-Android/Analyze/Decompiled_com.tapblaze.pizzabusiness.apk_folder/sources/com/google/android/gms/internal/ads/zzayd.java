package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzayd<T> {
    private T zzduw;

    public final T get() {
        return this.zzduw;
    }

    public final void set(T t) {
        this.zzduw = t;
    }
}
