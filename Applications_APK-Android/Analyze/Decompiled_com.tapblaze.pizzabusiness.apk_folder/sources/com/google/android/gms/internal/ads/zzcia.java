package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcia implements zzdxg<zzchx> {
    private final zzdxp<zzcid> zzfxj;

    private zzcia(zzdxp<zzcid> zzdxp) {
        this.zzfxj = zzdxp;
    }

    public static zzcia zzad(zzdxp<zzcid> zzdxp) {
        return new zzcia(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzchx(this.zzfxj.get());
    }
}
