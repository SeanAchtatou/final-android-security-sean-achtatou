package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzavq {
    public static void zzan(Context context) {
        if (zzayo.zzbo(context) && !zzayo.zzxl()) {
            zzdhe<?> zzvr = new zzavt(context).zzvr();
            zzavs.zzey("Updating ad debug logging enablement.");
            zzazh.zza(zzvr, "AdDebugLogUpdater.updateEnablement");
        }
    }
}
