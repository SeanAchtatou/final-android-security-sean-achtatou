package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhs  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhs implements zzhr {
    private final /* synthetic */ zzeb zzuo;

    zzhs(zzeb zzeb) {
        this.zzuo = zzeb;
    }

    public final int size() {
        return this.zzuo.size();
    }

    public final byte zzq(int i) {
        return this.zzuo.zzq(i);
    }
}
