package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbvc implements zzbsn {
    private final zzbpg zzfjo;

    public zzbvc(zzbpg zzbpg) {
        this.zzfjo = zzbpg;
    }

    public final void zzahy() {
    }

    public final void onHide() {
        this.zzfjo.zzbx(null);
    }
}
