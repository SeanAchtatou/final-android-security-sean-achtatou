package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzha {
    void onStopped();

    void zza(zzhf[] zzhfArr, zzmr zzmr, zzng zzng);

    boolean zzc(long j, boolean z);

    boolean zzdt(long j);

    void zzer();

    void zzes();

    zznj zzet();
}
