package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbjx implements zzdxg<Set<zzbsu<zzps>>> {
    private final zzbjw zzfdn;
    private final zzdxp<zzbly> zzfdq;

    public zzbjx(zzbjw zzbjw, zzdxp<zzbly> zzdxp) {
        this.zzfdn = zzbjw;
        this.zzfdq = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(Collections.singleton(new zzbsu(this.zzfdq.get(), zzazd.zzdwj)), "Cannot return null from a non-@Nullable @Provides method");
    }
}
