package com.google.android.gms.internal.ads;

import android.location.Location;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcvq implements zzded {
    static final zzded zzdoq = new zzcvq();

    private zzcvq() {
    }

    public final Object apply(Object obj) {
        return new zzcvo((Location) obj);
    }
}
