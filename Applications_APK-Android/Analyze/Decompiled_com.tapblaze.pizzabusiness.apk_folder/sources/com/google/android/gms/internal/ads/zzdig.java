package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdte;
import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzdig<PrimitiveT, KeyProtoT extends zzdte> implements zzdid<PrimitiveT> {
    private final zzdii<KeyProtoT> zzgxt;
    private final Class<PrimitiveT> zzgxu;

    public zzdig(zzdii<KeyProtoT> zzdii, Class<PrimitiveT> cls) {
        if (zzdii.zzase().contains(cls) || Void.class.equals(cls)) {
            this.zzgxt = zzdii;
            this.zzgxu = cls;
            return;
        }
        throw new IllegalArgumentException(String.format("Given internalKeyMananger %s does not support primitive class %s", zzdii.toString(), cls.getName()));
    }

    public final PrimitiveT zzm(zzdqk zzdqk) throws GeneralSecurityException {
        try {
            return zzb(this.zzgxt.zzr(zzdqk));
        } catch (zzdse e) {
            String valueOf = String.valueOf(this.zzgxt.zzasc().getName());
            throw new GeneralSecurityException(valueOf.length() != 0 ? "Failures parsing proto of type ".concat(valueOf) : new String("Failures parsing proto of type "), e);
        }
    }

    public final PrimitiveT zza(zzdte zzdte) throws GeneralSecurityException {
        String valueOf = String.valueOf(this.zzgxt.zzasc().getName());
        String concat = valueOf.length() != 0 ? "Expected proto of type ".concat(valueOf) : new String("Expected proto of type ");
        if (this.zzgxt.zzasc().isInstance(zzdte)) {
            return zzb(zzdte);
        }
        throw new GeneralSecurityException(concat);
    }

    public final zzdte zzn(zzdqk zzdqk) throws GeneralSecurityException {
        try {
            return zzasa().zzp(zzdqk);
        } catch (zzdse e) {
            String valueOf = String.valueOf(this.zzgxt.zzasg().zzasb().getName());
            throw new GeneralSecurityException(valueOf.length() != 0 ? "Failures parsing proto of type ".concat(valueOf) : new String("Failures parsing proto of type "), e);
        }
    }

    public final String getKeyType() {
        return this.zzgxt.getKeyType();
    }

    public final zzdna zzo(zzdqk zzdqk) throws GeneralSecurityException {
        try {
            return (zzdna) ((zzdrt) zzdna.zzavl().zzhb(this.zzgxt.getKeyType()).zzaw(zzasa().zzp(zzdqk).zzaxk()).zzb(this.zzgxt.zzasd()).zzbaf());
        } catch (zzdse e) {
            throw new GeneralSecurityException("Unexpected proto", e);
        }
    }

    public final Class<PrimitiveT> zzarz() {
        return this.zzgxu;
    }

    private final PrimitiveT zzb(KeyProtoT keyprotot) throws GeneralSecurityException {
        if (!Void.class.equals(this.zzgxu)) {
            this.zzgxt.zze(keyprotot);
            return this.zzgxt.zza(keyprotot, this.zzgxu);
        }
        throw new GeneralSecurityException("Cannot create a primitive for Void");
    }

    private final zzdif<?, KeyProtoT> zzasa() {
        return new zzdif<>(this.zzgxt.zzasg());
    }
}
