package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdku {
    @Deprecated
    private static final zzdny zzgyy;
    @Deprecated
    private static final zzdny zzgyz;
    @Deprecated
    private static final zzdny zzgza = zzgyy;
    private static final String zzgzz = new zzdkq().getKeyType();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdkq, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class):com.google.android.gms.internal.ads.zzdid<P>
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdid, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdkp, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class):com.google.android.gms.internal.ads.zzdid<P>
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdid, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdii, boolean):void */
    public static void zzasq() throws GeneralSecurityException {
        zzdit.zza((zzdii) new zzdkq(), true);
        zzdit.zza((zzdii) new zzdkp(), true);
        zzdit.zza(new zzdkx());
    }

    static {
        zzdny zzawv = zzdny.zzawv();
        zzgyy = zzawv;
        zzgyz = zzawv;
        try {
            zzasq();
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }
}
