package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import java.util.Collections;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbqp extends zzbrl<zzbqt> {
    private final Clock zzbmq;
    private boolean zzfco = false;
    private final ScheduledExecutorService zzfdi;
    private long zzfdk = -1;
    private long zzfdl = -1;
    private ScheduledFuture<?> zzfhu;

    public zzbqp(ScheduledExecutorService scheduledExecutorService, Clock clock) {
        super(Collections.emptySet());
        this.zzfdi = scheduledExecutorService;
        this.zzbmq = clock;
    }

    public final synchronized void onPause() {
        if (!this.zzfco) {
            if (this.zzfhu == null || this.zzfhu.isCancelled()) {
                this.zzfdl = -1;
            } else {
                this.zzfhu.cancel(true);
                this.zzfdl = this.zzfdk - this.zzbmq.elapsedRealtime();
            }
            this.zzfco = true;
        }
    }

    public final synchronized void onResume() {
        if (this.zzfco) {
            if (this.zzfdl > 0 && this.zzfhu.isCancelled()) {
                zzfd(this.zzfdl);
            }
            this.zzfco = false;
        }
    }

    public final synchronized void zzahj() {
        this.zzfco = false;
        zzfd(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zzdg(int r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            if (r7 > 0) goto L_0x0005
            monitor-exit(r6)
            return
        L_0x0005:
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x0043 }
            long r1 = (long) r7     // Catch:{ all -> 0x0043 }
            long r0 = r0.toMillis(r1)     // Catch:{ all -> 0x0043 }
            boolean r7 = r6.zzfco     // Catch:{ all -> 0x0043 }
            if (r7 == 0) goto L_0x0025
            long r2 = r6.zzfdl     // Catch:{ all -> 0x0043 }
            r4 = 0
            int r7 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r7 <= 0) goto L_0x001f
            long r2 = r6.zzfdl     // Catch:{ all -> 0x0043 }
            int r7 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r7 >= 0) goto L_0x001f
            goto L_0x0021
        L_0x001f:
            long r0 = r6.zzfdl     // Catch:{ all -> 0x0043 }
        L_0x0021:
            r6.zzfdl = r0     // Catch:{ all -> 0x0043 }
            monitor-exit(r6)
            return
        L_0x0025:
            com.google.android.gms.common.util.Clock r7 = r6.zzbmq     // Catch:{ all -> 0x0043 }
            long r2 = r7.elapsedRealtime()     // Catch:{ all -> 0x0043 }
            long r4 = r6.zzfdk     // Catch:{ all -> 0x0043 }
            int r7 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r7 > 0) goto L_0x003e
            long r2 = r6.zzfdk     // Catch:{ all -> 0x0043 }
            com.google.android.gms.common.util.Clock r7 = r6.zzbmq     // Catch:{ all -> 0x0043 }
            long r4 = r7.elapsedRealtime()     // Catch:{ all -> 0x0043 }
            long r2 = r2 - r4
            int r7 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r7 <= 0) goto L_0x0041
        L_0x003e:
            r6.zzfd(r0)     // Catch:{ all -> 0x0043 }
        L_0x0041:
            monitor-exit(r6)
            return
        L_0x0043:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbqp.zzdg(int):void");
    }

    private final synchronized void zzfd(long j) {
        if (this.zzfhu != null && !this.zzfhu.isDone()) {
            this.zzfhu.cancel(true);
        }
        this.zzfdk = this.zzbmq.elapsedRealtime() + j;
        this.zzfhu = this.zzfdi.schedule(new zzbqq(this), j, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: private */
    public final void zzahk() {
        zza(zzbqo.zzfhp);
    }
}
