package com.google.android.gms.internal.ads;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzabu extends zzach {
    private final int height;
    private final Uri uri;
    private final int width;
    private final Drawable zzcvm;
    private final double zzcvn;

    public zzabu(Drawable drawable, Uri uri2, double d, int i, int i2) {
        this.zzcvm = drawable;
        this.uri = uri2;
        this.zzcvn = d;
        this.width = i;
        this.height = i2;
    }

    public final IObjectWrapper zzrc() throws RemoteException {
        return ObjectWrapper.wrap(this.zzcvm);
    }

    public final Uri getUri() throws RemoteException {
        return this.uri;
    }

    public final double getScale() {
        return this.zzcvn;
    }

    public final int getWidth() {
        return this.width;
    }

    public final int getHeight() {
        return this.height;
    }
}
