package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.doubleclick.AppEventListener;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcos implements AppEventListener {
    private zzwc zzgdh;

    public final synchronized void zzb(zzwc zzwc) {
        this.zzgdh = zzwc;
    }

    public final synchronized zzwc zzamq() {
        return this.zzgdh;
    }

    public final synchronized void onAppEvent(String str, String str2) {
        if (this.zzgdh != null) {
            try {
                this.zzgdh.onAppEvent(str, str2);
            } catch (RemoteException e) {
                zzavs.zzd("Remote Exception at onAppEvent.", e);
            }
        }
    }
}
