package com.google.android.gms.internal.p000firebaseperf;

import android.util.Log;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbh  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzbh {
    private static zzbh zzfi = null;
    private static String zzfj = "FirebasePerformance";

    public static synchronized zzbh zzcn() {
        zzbh zzbh;
        synchronized (zzbh.class) {
            if (zzfi == null) {
                zzfi = new zzbh();
            }
            zzbh = zzfi;
        }
        return zzbh;
    }

    static void zzm(String str) {
        Log.d(zzfj, str);
    }

    private zzbh() {
    }
}
