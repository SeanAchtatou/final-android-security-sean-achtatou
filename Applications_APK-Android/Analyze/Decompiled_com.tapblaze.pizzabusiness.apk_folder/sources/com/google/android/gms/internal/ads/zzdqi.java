package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdqi extends zzdqe<Boolean> implements zzdsb<Boolean>, zzdtq, RandomAccess {
    private static final zzdqi zzhhu;
    private int size;
    private boolean[] zzhhv;

    zzdqi() {
        this(new boolean[10], 0);
    }

    private zzdqi(boolean[] zArr, int i) {
        this.zzhhv = zArr;
        this.size = i;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zzaxr();
        if (i2 >= i) {
            boolean[] zArr = this.zzhhv;
            System.arraycopy(zArr, i2, zArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdqi)) {
            return super.equals(obj);
        }
        zzdqi zzdqi = (zzdqi) obj;
        if (this.size != zzdqi.size) {
            return false;
        }
        boolean[] zArr = zzdqi.zzhhv;
        for (int i = 0; i < this.size; i++) {
            if (this.zzhhv[i] != zArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + zzdrv.zzbp(this.zzhhv[i2]);
        }
        return i;
    }

    public final int size() {
        return this.size;
    }

    public final void addBoolean(boolean z) {
        zzaxr();
        int i = this.size;
        boolean[] zArr = this.zzhhv;
        if (i == zArr.length) {
            boolean[] zArr2 = new boolean[(((i * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            this.zzhhv = zArr2;
        }
        boolean[] zArr3 = this.zzhhv;
        int i2 = this.size;
        this.size = i2 + 1;
        zArr3[i2] = z;
    }

    public final boolean addAll(Collection<? extends Boolean> collection) {
        zzaxr();
        zzdrv.checkNotNull(collection);
        if (!(collection instanceof zzdqi)) {
            return super.addAll(collection);
        }
        zzdqi zzdqi = (zzdqi) collection;
        int i = zzdqi.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            boolean[] zArr = this.zzhhv;
            if (i3 > zArr.length) {
                this.zzhhv = Arrays.copyOf(zArr, i3);
            }
            System.arraycopy(zzdqi.zzhhv, 0, this.zzhhv, this.size, zzdqi.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean remove(Object obj) {
        zzaxr();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Boolean.valueOf(this.zzhhv[i]))) {
                boolean[] zArr = this.zzhhv;
                System.arraycopy(zArr, i + 1, zArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    private final void zzfb(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
    }

    private final String zzfc(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        zzaxr();
        zzfb(i);
        boolean[] zArr = this.zzhhv;
        boolean z = zArr[i];
        zArr[i] = booleanValue;
        return Boolean.valueOf(z);
    }

    public final /* synthetic */ Object remove(int i) {
        zzaxr();
        zzfb(i);
        boolean[] zArr = this.zzhhv;
        boolean z = zArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(zArr, i + 1, zArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Boolean.valueOf(z);
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        boolean booleanValue = ((Boolean) obj).booleanValue();
        zzaxr();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
        boolean[] zArr = this.zzhhv;
        if (i2 < zArr.length) {
            System.arraycopy(zArr, i, zArr, i + 1, i2 - i);
        } else {
            boolean[] zArr2 = new boolean[(((i2 * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            System.arraycopy(this.zzhhv, i, zArr2, i + 1, this.size - i);
            this.zzhhv = zArr2;
        }
        this.zzhhv[i] = booleanValue;
        this.size++;
        this.modCount++;
    }

    public final /* synthetic */ boolean add(Object obj) {
        addBoolean(((Boolean) obj).booleanValue());
        return true;
    }

    public final /* synthetic */ zzdsb zzfd(int i) {
        if (i >= this.size) {
            return new zzdqi(Arrays.copyOf(this.zzhhv, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ Object get(int i) {
        zzfb(i);
        return Boolean.valueOf(this.zzhhv[i]);
    }

    static {
        zzdqi zzdqi = new zzdqi(new boolean[0], 0);
        zzhhu = zzdqi;
        zzdqi.zzaxq();
    }
}
