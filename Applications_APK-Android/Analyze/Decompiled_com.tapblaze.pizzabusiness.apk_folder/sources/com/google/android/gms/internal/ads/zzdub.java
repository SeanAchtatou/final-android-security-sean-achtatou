package com.google.android.gms.internal.ads;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
class zzdub<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    private boolean zzhjq;
    private final int zzhql;
    /* access modifiers changed from: private */
    public List<zzduk> zzhqm;
    /* access modifiers changed from: private */
    public Map<K, V> zzhqn;
    private volatile zzdum zzhqo;
    /* access modifiers changed from: private */
    public Map<K, V> zzhqp;
    private volatile zzdug zzhqq;

    static <FieldDescriptorType extends zzdro<FieldDescriptorType>> zzdub<FieldDescriptorType, Object> zzgv(int i) {
        return new zzdue(i);
    }

    private zzdub(int i) {
        this.zzhql = i;
        this.zzhqm = Collections.emptyList();
        this.zzhqn = Collections.emptyMap();
        this.zzhqp = Collections.emptyMap();
    }

    public void zzaxq() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.zzhjq) {
            if (this.zzhqn.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.zzhqn);
            }
            this.zzhqn = map;
            if (this.zzhqp.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.zzhqp);
            }
            this.zzhqp = map2;
            this.zzhjq = true;
        }
    }

    public final boolean isImmutable() {
        return this.zzhjq;
    }

    public final int zzbbs() {
        return this.zzhqm.size();
    }

    public final Map.Entry<K, V> zzgw(int i) {
        return this.zzhqm.get(i);
    }

    public final Iterable<Map.Entry<K, V>> zzbbt() {
        if (this.zzhqn.isEmpty()) {
            return zzduf.zzbcd();
        }
        return this.zzhqn.entrySet();
    }

    public int size() {
        return this.zzhqm.size() + this.zzhqn.size();
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return zza(comparable) >= 0 || this.zzhqn.containsKey(comparable);
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzhqm.get(zza).getValue();
        }
        return this.zzhqn.get(comparable);
    }

    /* renamed from: zza */
    public final V put(Comparable comparable, Object obj) {
        zzbbv();
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzhqm.get(zza).setValue(obj);
        }
        zzbbv();
        if (this.zzhqm.isEmpty() && !(this.zzhqm instanceof ArrayList)) {
            this.zzhqm = new ArrayList(this.zzhql);
        }
        int i = -(zza + 1);
        if (i >= this.zzhql) {
            return zzbbw().put(comparable, obj);
        }
        int size = this.zzhqm.size();
        int i2 = this.zzhql;
        if (size == i2) {
            zzduk remove = this.zzhqm.remove(i2 - 1);
            zzbbw().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.zzhqm.add(i, new zzduk(this, comparable, obj));
        return null;
    }

    public void clear() {
        zzbbv();
        if (!this.zzhqm.isEmpty()) {
            this.zzhqm.clear();
        }
        if (!this.zzhqn.isEmpty()) {
            this.zzhqn.clear();
        }
    }

    public V remove(Object obj) {
        zzbbv();
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return zzgx(zza);
        }
        if (this.zzhqn.isEmpty()) {
            return null;
        }
        return this.zzhqn.remove(comparable);
    }

    /* access modifiers changed from: private */
    public final V zzgx(int i) {
        zzbbv();
        V value = this.zzhqm.remove(i).getValue();
        if (!this.zzhqn.isEmpty()) {
            Iterator it = zzbbw().entrySet().iterator();
            this.zzhqm.add(new zzduk(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final int zza(K r5) {
        /*
            r4 = this;
            java.util.List<com.google.android.gms.internal.ads.zzduk> r0 = r4.zzhqm
            int r0 = r0.size()
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x0025
            java.util.List<com.google.android.gms.internal.ads.zzduk> r1 = r4.zzhqm
            java.lang.Object r1 = r1.get(r0)
            com.google.android.gms.internal.ads.zzduk r1 = (com.google.android.gms.internal.ads.zzduk) r1
            java.lang.Object r1 = r1.getKey()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r1 = r5.compareTo(r1)
            if (r1 <= 0) goto L_0x0022
            int r0 = r0 + 2
            int r5 = -r0
            return r5
        L_0x0022:
            if (r1 != 0) goto L_0x0025
            return r0
        L_0x0025:
            r1 = 0
        L_0x0026:
            if (r1 > r0) goto L_0x0049
            int r2 = r1 + r0
            int r2 = r2 / 2
            java.util.List<com.google.android.gms.internal.ads.zzduk> r3 = r4.zzhqm
            java.lang.Object r3 = r3.get(r2)
            com.google.android.gms.internal.ads.zzduk r3 = (com.google.android.gms.internal.ads.zzduk) r3
            java.lang.Object r3 = r3.getKey()
            java.lang.Comparable r3 = (java.lang.Comparable) r3
            int r3 = r5.compareTo(r3)
            if (r3 >= 0) goto L_0x0043
            int r0 = r2 + -1
            goto L_0x0026
        L_0x0043:
            if (r3 <= 0) goto L_0x0048
            int r1 = r2 + 1
            goto L_0x0026
        L_0x0048:
            return r2
        L_0x0049:
            int r1 = r1 + 1
            int r5 = -r1
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdub.zza(java.lang.Comparable):int");
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.zzhqo == null) {
            this.zzhqo = new zzdum(this, null);
        }
        return this.zzhqo;
    }

    /* access modifiers changed from: package-private */
    public final Set<Map.Entry<K, V>> zzbbu() {
        if (this.zzhqq == null) {
            this.zzhqq = new zzdug(this, null);
        }
        return this.zzhqq;
    }

    /* access modifiers changed from: private */
    public final void zzbbv() {
        if (this.zzhjq) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> zzbbw() {
        zzbbv();
        if (this.zzhqn.isEmpty() && !(this.zzhqn instanceof TreeMap)) {
            this.zzhqn = new TreeMap();
            this.zzhqp = ((TreeMap) this.zzhqn).descendingMap();
        }
        return (SortedMap) this.zzhqn;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdub)) {
            return super.equals(obj);
        }
        zzdub zzdub = (zzdub) obj;
        int size = size();
        if (size != zzdub.size()) {
            return false;
        }
        int zzbbs = zzbbs();
        if (zzbbs != zzdub.zzbbs()) {
            return entrySet().equals(zzdub.entrySet());
        }
        for (int i = 0; i < zzbbs; i++) {
            if (!zzgw(i).equals(zzdub.zzgw(i))) {
                return false;
            }
        }
        if (zzbbs != size) {
            return this.zzhqn.equals(zzdub.zzhqn);
        }
        return true;
    }

    public int hashCode() {
        int zzbbs = zzbbs();
        int i = 0;
        for (int i2 = 0; i2 < zzbbs; i2++) {
            i += this.zzhqm.get(i2).hashCode();
        }
        return this.zzhqn.size() > 0 ? i + this.zzhqn.hashCode() : i;
    }

    /* synthetic */ zzdub(int i, zzdue zzdue) {
        this(i);
    }
}
