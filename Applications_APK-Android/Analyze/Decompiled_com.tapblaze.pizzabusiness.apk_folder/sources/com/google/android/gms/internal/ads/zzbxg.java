package com.google.android.gms.internal.ads;

import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbxg implements zzdxg<View> {
    private final zzbxe zzfmy;

    private zzbxg(zzbxe zzbxe) {
        this.zzfmy = zzbxe;
    }

    public final /* synthetic */ Object get() {
        return null;
    }

    public static zzbxg zzc(zzbxe zzbxe) {
        return new zzbxg(zzbxe);
    }
}
