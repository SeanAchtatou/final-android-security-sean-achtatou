package com.google.android.gms.internal.measurement;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class zzhb {
    private static final zzhb zza = new zzhb();
    private final zzhe zzb = new zzgd();
    private final ConcurrentMap<Class<?>, zzhf<?>> zzc = new ConcurrentHashMap();

    public static zzhb zza() {
        return zza;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzfh.zza(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.zzfh.zza(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.zzfh.zza(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzfh.zza(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.zzhf<T>, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.zzfh.zza(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.zzfh.zza(java.lang.Object, java.lang.String):T */
    public final <T> zzhf<T> zza(Class cls) {
        zzfh.zza((Object) cls, "messageType");
        zzhf<T> zzhf = this.zzc.get(cls);
        if (zzhf != null) {
            return zzhf;
        }
        zzhf<T> zza2 = this.zzb.zza(cls);
        zzfh.zza((Object) cls, "messageType");
        zzfh.zza((Object) zza2, "schema");
        zzhf<T> putIfAbsent = this.zzc.putIfAbsent(cls, zza2);
        return putIfAbsent != null ? putIfAbsent : zza2;
    }

    public final <T> zzhf<T> zza(Object obj) {
        return zza((Class) obj.getClass());
    }

    private zzhb() {
    }
}
