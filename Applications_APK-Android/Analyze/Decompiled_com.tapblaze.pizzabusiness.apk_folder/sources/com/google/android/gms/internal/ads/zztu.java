package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zztu extends zzdvq<zztu> {
    private Integer zzcap = null;
    public String zzcaq = null;
    private Integer zzcar = null;
    private zzte zzcas = null;
    private zztt zzcat = null;
    public long[] zzcau = zzdvy.zzhty;
    public zzts zzcav = null;
    private zztr zzcaw = null;
    private zzsy.zzh zzcax = null;
    public zztq zzcay = null;
    public zzsy.zzj zzcaz = null;
    public zzsy.zzw zzcba = null;
    public zzsy.zza zzcbb = null;

    public zztu() {
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    public final void zza(zzdvo zzdvo) throws IOException {
        String str = this.zzcaq;
        if (str != null) {
            zzdvo.zzf(10, str);
        }
        long[] jArr = this.zzcau;
        if (jArr != null && jArr.length > 0) {
            int i = 0;
            while (true) {
                long[] jArr2 = this.zzcau;
                if (i >= jArr2.length) {
                    break;
                }
                long j = jArr2[i];
                zzdvo.zzaa(14, 0);
                zzdvo.zzfs(j);
                i++;
            }
        }
        zzts zzts = this.zzcav;
        if (zzts != null) {
            zzdvo.zza(15, zzts);
        }
        zztq zztq = this.zzcay;
        if (zztq != null) {
            zzdvo.zza(18, zztq);
        }
        zzsy.zzj zzj = this.zzcaz;
        if (zzj != null) {
            zzdvo.zze(19, zzj);
        }
        zzsy.zzw zzw = this.zzcba;
        if (zzw != null) {
            zzdvo.zze(20, zzw);
        }
        zzsy.zza zza = this.zzcbb;
        if (zza != null) {
            zzdvo.zze(21, zza);
        }
        super.zza(zzdvo);
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        long[] jArr;
        int zzoi = super.zzoi();
        String str = this.zzcaq;
        if (str != null) {
            zzoi += zzdvo.zzg(10, str);
        }
        long[] jArr2 = this.zzcau;
        if (jArr2 != null && jArr2.length > 0) {
            int i = 0;
            int i2 = 0;
            while (true) {
                jArr = this.zzcau;
                if (i >= jArr.length) {
                    break;
                }
                long j = jArr[i];
                i2 += (-128 & j) == 0 ? 1 : (-16384 & j) == 0 ? 2 : (-2097152 & j) == 0 ? 3 : (-268435456 & j) == 0 ? 4 : (-34359738368L & j) == 0 ? 5 : (-4398046511104L & j) == 0 ? 6 : (-562949953421312L & j) == 0 ? 7 : (-72057594037927936L & j) == 0 ? 8 : (Long.MIN_VALUE & j) == 0 ? 9 : 10;
                i++;
            }
            zzoi = zzoi + i2 + (jArr.length * 1);
        }
        zzts zzts = this.zzcav;
        if (zzts != null) {
            zzoi += zzdvo.zzb(15, zzts);
        }
        zztq zztq = this.zzcay;
        if (zztq != null) {
            zzoi += zzdvo.zzb(18, zztq);
        }
        zzsy.zzj zzj = this.zzcaz;
        if (zzj != null) {
            zzoi += zzdrb.zzc(19, zzj);
        }
        zzsy.zzw zzw = this.zzcba;
        if (zzw != null) {
            zzoi += zzdrb.zzc(20, zzw);
        }
        zzsy.zza zza = this.zzcbb;
        return zza != null ? zzoi + zzdrb.zzc(21, zza) : zzoi;
    }
}
