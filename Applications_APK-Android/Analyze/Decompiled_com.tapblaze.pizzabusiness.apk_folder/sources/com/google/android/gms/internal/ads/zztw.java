package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zztw extends zzdvq<zztw> {
    private static volatile zztw[] zzcbc;
    private zzsy.zzr zzcbd = null;
    private zzsy.zzt zzcbe = null;
    private zzsy.zzu zzcbf = null;
    private zzsy.zzv zzcbg = null;
    private zzsy.zzp zzcbh = null;
    private zzsy.zzs zzcbi = null;
    private zztv zzcbj = null;
    private Integer zzcbk = null;
    private Integer zzcbl = null;
    private zzsy.zzo zzcbm = null;
    private Integer zzcbn = null;
    private Integer zzcbo = null;
    private Integer zzcbp = null;
    private Integer zzcbq = null;
    private Integer zzcbr = null;
    private Long zzcbs = null;

    public static zztw[] zzoj() {
        if (zzcbc == null) {
            synchronized (zzdvu.zzhtt) {
                if (zzcbc == null) {
                    zzcbc = new zztw[0];
                }
            }
        }
        return zzcbc;
    }

    public zztw() {
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        return super.zzoi();
    }
}
