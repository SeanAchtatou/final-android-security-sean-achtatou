package com.google.android.gms.internal.ads;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public enum zzdvm {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf((double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(zzdqk.zzhhx),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzhny;

    private zzdvm(Object obj) {
        this.zzhny = obj;
    }
}
