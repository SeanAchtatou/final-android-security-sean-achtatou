package com.google.android.gms.internal.p000firebaseperf;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzet  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class zzet {
    private static volatile boolean zznk = false;
    private static boolean zznl = true;
    private static volatile zzet zznm;
    private static final zzet zznn = new zzet(true);
    private final Map<Object, Object> zzno;

    public static zzet zzgv() {
        zzet zzet = zznm;
        if (zzet == null) {
            synchronized (zzet.class) {
                zzet = zznm;
                if (zzet == null) {
                    zzet = zznn;
                    zznm = zzet;
                }
            }
        }
        return zzet;
    }

    zzet() {
        this.zzno = new HashMap();
    }

    private zzet(boolean z) {
        this.zzno = Collections.emptyMap();
    }
}
