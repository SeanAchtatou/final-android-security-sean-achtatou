package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdto {
    private static final zzdtm zzhpo = zzbbl();
    private static final zzdtm zzhpp = new zzdtl();

    static zzdtm zzbbj() {
        return zzhpo;
    }

    static zzdtm zzbbk() {
        return zzhpp;
    }

    private static zzdtm zzbbl() {
        try {
            return (zzdtm) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
