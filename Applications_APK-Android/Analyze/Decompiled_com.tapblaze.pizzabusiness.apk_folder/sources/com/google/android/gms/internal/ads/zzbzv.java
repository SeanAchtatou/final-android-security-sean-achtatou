package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.Collections;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbzv extends zzahe implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener, zzabr {
    private boolean zzegh = false;
    private zzxb zzfma;
    private View zzfmf;
    private zzbwk zzfnf;
    private boolean zzfqa = false;

    public zzbzv(zzbwk zzbwk, zzbws zzbws) {
        this.zzfmf = zzbws.zzaje();
        this.zzfma = zzbws.getVideoController();
        this.zzfnf = zzbwk;
        if (zzbws.zzajf() != null) {
            zzbws.zzajf().zza(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void
     arg types: [android.view.View, com.google.android.gms.internal.ads.zzbzv]
     candidates:
      com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void
      com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void
     arg types: [android.view.View, com.google.android.gms.internal.ads.zzbzv]
     candidates:
      com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void
      com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void */
    public final void zza(IObjectWrapper iObjectWrapper, zzahg zzahg) throws RemoteException {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        if (this.zzegh) {
            zzavs.zzex("Instream ad can not be shown after destroy().");
            zza(zzahg, 2);
        } else if (this.zzfmf == null || this.zzfma == null) {
            String str = this.zzfmf == null ? "can not get video view." : "can not get video controller.";
            zzavs.zzex(str.length() != 0 ? "Instream internal error: ".concat(str) : new String("Instream internal error: "));
            zza(zzahg, 0);
        } else if (this.zzfqa) {
            zzavs.zzex("Instream ad should not be used again.");
            zza(zzahg, 1);
        } else {
            this.zzfqa = true;
            zzakn();
            ((ViewGroup) ObjectWrapper.unwrap(iObjectWrapper)).addView(this.zzfmf, new ViewGroup.LayoutParams(-1, -1));
            zzq.zzln();
            zzazt.zza(this.zzfmf, (ViewTreeObserver.OnGlobalLayoutListener) this);
            zzq.zzln();
            zzazt.zza(this.zzfmf, (ViewTreeObserver.OnScrollChangedListener) this);
            zzako();
            try {
                zzahg.zzrv();
            } catch (RemoteException e) {
                zzavs.zze("#007 Could not call remote method.", e);
            }
        }
    }

    public final void zzr(IObjectWrapper iObjectWrapper) throws RemoteException {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zza(iObjectWrapper, new zzbzx(this));
    }

    public final zzxb getVideoController() throws RemoteException {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        if (!this.zzegh) {
            return this.zzfma;
        }
        zzavs.zzex("getVideoController: Instream ad should not be used after destroyed");
        return null;
    }

    public final void destroy() throws RemoteException {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzakn();
        zzbwk zzbwk = this.zzfnf;
        if (zzbwk != null) {
            zzbwk.destroy();
        }
        this.zzfnf = null;
        this.zzfmf = null;
        this.zzfma = null;
        this.zzegh = true;
    }

    private final void zzakn() {
        View view = this.zzfmf;
        if (view != null) {
            ViewParent parent = view.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.zzfmf);
            }
        }
    }

    public final void onGlobalLayout() {
        zzako();
    }

    public final void onScrollChanged() {
        zzako();
    }

    public final void zzrb() {
        zzawb.zzdsr.post(new zzbzy(this));
    }

    private final void zzako() {
        View view;
        zzbwk zzbwk = this.zzfnf;
        if (zzbwk != null && (view = this.zzfmf) != null) {
            zzbwk.zzb(view, Collections.emptyMap(), Collections.emptyMap(), zzbwk.zzy(this.zzfmf));
        }
    }

    private static void zza(zzahg zzahg, int i) {
        try {
            zzahg.zzcn(i);
        } catch (RemoteException e) {
            zzavs.zze("#007 Could not call remote method.", e);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzakp() {
        try {
            destroy();
        } catch (RemoteException e) {
            zzavs.zze("#007 Could not call remote method.", e);
        }
    }
}
