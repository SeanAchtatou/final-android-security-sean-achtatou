package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdnt extends zzdrt<zzdnt, zza> implements zzdtg {
    private static volatile zzdtn<zzdnt> zzdz;
    /* access modifiers changed from: private */
    public static final zzdnt zzhei;
    private String zzheg = "";
    private zzdng zzheh;

    private zzdnt() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdnt, zza> implements zzdtg {
        private zza() {
            super(zzdnt.zzhei);
        }

        /* synthetic */ zza(zzdnu zzdnu) {
            this();
        }
    }

    public final String zzawq() {
        return this.zzheg;
    }

    public final zzdng zzawr() {
        zzdng zzdng = this.zzheh;
        return zzdng == null ? zzdng.zzavo() : zzdng;
    }

    public static zzdnt zzba(zzdqk zzdqk) throws zzdse {
        return (zzdnt) zzdrt.zza(zzhei, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnu.zzdk[i - 1]) {
            case 1:
                return new zzdnt();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhei, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001Ȉ\u0002\t", new Object[]{"zzheg", "zzheh"});
            case 4:
                return zzhei;
            case 5:
                zzdtn<zzdnt> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdnt.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhei);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdnt zzaws() {
        return zzhei;
    }

    static {
        zzdnt zzdnt = new zzdnt();
        zzhei = zzdnt;
        zzdrt.zza(zzdnt.class, zzdnt);
    }
}
