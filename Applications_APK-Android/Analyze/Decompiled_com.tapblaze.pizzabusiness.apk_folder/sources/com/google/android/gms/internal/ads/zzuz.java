package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzuz extends zzvb<zzacp> {
    private final /* synthetic */ zzup zzcdi;
    private final /* synthetic */ View zzcdk;
    private final /* synthetic */ HashMap zzcdl;
    private final /* synthetic */ HashMap zzcdm;

    zzuz(zzup zzup, View view, HashMap hashMap, HashMap hashMap2) {
        this.zzcdi = zzup;
        this.zzcdk = view;
        this.zzcdl = hashMap;
        this.zzcdm = hashMap2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzop() {
        zzup.zza(this.zzcdk.getContext(), "native_ad_view_holder_delegate");
        return new zzyk();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzcde.zzb(this.zzcdk, this.zzcdl, this.zzcdm);
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zza(ObjectWrapper.wrap(this.zzcdk), ObjectWrapper.wrap(this.zzcdl), ObjectWrapper.wrap(this.zzcdm));
    }
}
