package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbkg extends zzre {
    private final zzvu zzbqy;
    private final zzbke zzfdv;

    public zzbkg(zzbke zzbke, zzvu zzvu) {
        this.zzfdv = zzbke;
        this.zzbqy = zzvu;
    }

    public final zzvu zzdm() {
        return this.zzbqy;
    }

    public final void zza(zzrl zzrl) {
        this.zzfdv.zza(zzrl);
    }
}
