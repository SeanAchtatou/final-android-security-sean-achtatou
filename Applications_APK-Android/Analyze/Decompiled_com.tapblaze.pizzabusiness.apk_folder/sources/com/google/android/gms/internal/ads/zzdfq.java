package com.google.android.gms.internal.ads;

import java.lang.Throwable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdfq<V, X extends Throwable> extends zzdfr<V, X, zzdgf<? super X, ? extends V>, zzdhe<? extends V>> {
    zzdfq(zzdhe<? extends V> zzdhe, Class<X> cls, zzdgf<? super X, ? extends V> zzdgf) {
        super(zzdhe, cls, zzdgf);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void setResult(Object obj) {
        setFuture((zzdhe) obj);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zza(Object obj, Throwable th) throws Exception {
        zzdgf zzdgf = (zzdgf) obj;
        zzdhe zzf = zzdgf.zzf(th);
        zzdei.zza(zzf, "AsyncFunction.apply returned null instead of a Future. Did you mean to return immediateFuture(null)? %s", zzdgf);
        return zzf;
    }
}
