package com.google.android.gms.internal.ads;

import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzej extends zzcj<Integer, Long> {
    public Long zzya;
    public Long zzyb;

    public zzej() {
    }

    public zzej(String str) {
        zzap(str);
    }

    /* access modifiers changed from: protected */
    public final void zzap(String str) {
        HashMap zzaq = zzaq(str);
        if (zzaq != null) {
            this.zzya = (Long) zzaq.get(0);
            this.zzyb = (Long) zzaq.get(1);
        }
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Long> zzbk() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(0, this.zzya);
        hashMap.put(1, this.zzyb);
        return hashMap;
    }
}
