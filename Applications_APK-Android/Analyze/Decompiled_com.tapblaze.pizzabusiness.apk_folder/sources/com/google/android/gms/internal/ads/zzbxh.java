package com.google.android.gms.internal.ads;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdAssetNames;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbxh extends zzacl implements ViewTreeObserver.OnGlobalLayoutListener, ViewTreeObserver.OnScrollChangedListener, zzbxz {
    public static final String[] zzfna = {NativeAppInstallAd.ASSET_MEDIA_VIDEO, NativeContentAd.ASSET_MEDIA_VIDEO, UnifiedNativeAdAssetNames.ASSET_MEDIA_VIDEO};
    private FrameLayout zzbkf;
    private zzacd zzcwg;
    private final int zzdwa;
    private boolean zzegh = false;
    private final String zzfmz;
    private Map<String, WeakReference<View>> zzfnb = new HashMap();
    private FrameLayout zzfnc;
    private zzdhd zzfnd;
    private View zzfne;
    private zzbwk zzfnf;
    private zzpo zzfng;
    private IObjectWrapper zzfnh = null;
    private boolean zzfni;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void
     arg types: [android.widget.FrameLayout, com.google.android.gms.internal.ads.zzbxh]
     candidates:
      com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void
      com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void
     arg types: [android.widget.FrameLayout, com.google.android.gms.internal.ads.zzbxh]
     candidates:
      com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnGlobalLayoutListener):void
      com.google.android.gms.internal.ads.zzazt.zza(android.view.View, android.view.ViewTreeObserver$OnScrollChangedListener):void */
    public zzbxh(FrameLayout frameLayout, FrameLayout frameLayout2, int i) {
        String str;
        this.zzfnc = frameLayout;
        this.zzbkf = frameLayout2;
        this.zzdwa = i;
        String canonicalName = frameLayout.getClass().getCanonicalName();
        if ("com.google.android.gms.ads.formats.NativeContentAdView".equals(canonicalName)) {
            str = NativeContentAd.ASSET_ATTRIBUTION_ICON_IMAGE;
        } else if ("com.google.android.gms.ads.formats.NativeAppInstallAdView".equals(canonicalName)) {
            str = NativeAppInstallAd.ASSET_ATTRIBUTION_ICON_IMAGE;
        } else {
            "com.google.android.gms.ads.formats.UnifiedNativeAdView".equals(canonicalName);
            str = "3012";
        }
        this.zzfmz = str;
        zzq.zzln();
        zzazt.zza((View) frameLayout, (ViewTreeObserver.OnGlobalLayoutListener) this);
        zzq.zzln();
        zzazt.zza((View) frameLayout, (ViewTreeObserver.OnScrollChangedListener) this);
        this.zzfnd = zzazd.zzdwi;
        this.zzfng = new zzpo(this.zzfnc.getContext(), this.zzfnc);
        frameLayout.setOnTouchListener(this);
        frameLayout.setOnClickListener(this);
    }

    public final synchronized void zzb(String str, IObjectWrapper iObjectWrapper) {
        zza(str, (View) ObjectWrapper.unwrap(iObjectWrapper), true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0040, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zza(java.lang.String r2, android.view.View r3, boolean r4) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r4 = r1.zzegh     // Catch:{ all -> 0x0041 }
            if (r4 == 0) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            if (r3 != 0) goto L_0x0010
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r3 = r1.zzfnb     // Catch:{ all -> 0x0041 }
            r3.remove(r2)     // Catch:{ all -> 0x0041 }
            monitor-exit(r1)
            return
        L_0x0010:
            java.util.Map<java.lang.String, java.lang.ref.WeakReference<android.view.View>> r4 = r1.zzfnb     // Catch:{ all -> 0x0041 }
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0041 }
            r0.<init>(r3)     // Catch:{ all -> 0x0041 }
            r4.put(r2, r0)     // Catch:{ all -> 0x0041 }
            java.lang.String r4 = "1098"
            boolean r4 = r4.equals(r2)     // Catch:{ all -> 0x0041 }
            if (r4 != 0) goto L_0x003f
            java.lang.String r4 = "3011"
            boolean r2 = r4.equals(r2)     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x002b
            goto L_0x003f
        L_0x002b:
            int r2 = r1.zzdwa     // Catch:{ all -> 0x0041 }
            boolean r2 = com.google.android.gms.internal.ads.zzaxy.zzcs(r2)     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x0036
            r3.setOnTouchListener(r1)     // Catch:{ all -> 0x0041 }
        L_0x0036:
            r2 = 1
            r3.setClickable(r2)     // Catch:{ all -> 0x0041 }
            r3.setOnClickListener(r1)     // Catch:{ all -> 0x0041 }
            monitor-exit(r1)
            return
        L_0x003f:
            monitor-exit(r1)
            return
        L_0x0041:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbxh.zza(java.lang.String, android.view.View, boolean):void");
    }

    public final synchronized IObjectWrapper zzco(String str) {
        return ObjectWrapper.wrap(zzgb(str));
    }

    public final synchronized View zzgb(String str) {
        if (this.zzegh) {
            return null;
        }
        WeakReference weakReference = this.zzfnb.get(str);
        if (weakReference == null) {
            return null;
        }
        return (View) weakReference.get();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0049, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zza(com.google.android.gms.dynamic.IObjectWrapper r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r0 = r1.zzegh     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            java.lang.Object r2 = com.google.android.gms.dynamic.ObjectWrapper.unwrap(r2)     // Catch:{ all -> 0x004a }
            boolean r0 = r2 instanceof com.google.android.gms.internal.ads.zzbwk     // Catch:{ all -> 0x004a }
            if (r0 != 0) goto L_0x0016
            java.lang.String r2 = "Not an instance of native engine. This is most likely a transient error"
            com.google.android.gms.internal.ads.zzavs.zzez(r2)     // Catch:{ all -> 0x004a }
            monitor-exit(r1)
            return
        L_0x0016:
            com.google.android.gms.internal.ads.zzbwk r0 = r1.zzfnf     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x001f
            com.google.android.gms.internal.ads.zzbwk r0 = r1.zzfnf     // Catch:{ all -> 0x004a }
            r0.zzb(r1)     // Catch:{ all -> 0x004a }
        L_0x001f:
            r1.zzajy()     // Catch:{ all -> 0x004a }
            com.google.android.gms.internal.ads.zzbwk r2 = (com.google.android.gms.internal.ads.zzbwk) r2     // Catch:{ all -> 0x004a }
            r1.zzfnf = r2     // Catch:{ all -> 0x004a }
            com.google.android.gms.internal.ads.zzbwk r2 = r1.zzfnf     // Catch:{ all -> 0x004a }
            r2.zza(r1)     // Catch:{ all -> 0x004a }
            com.google.android.gms.internal.ads.zzbwk r2 = r1.zzfnf     // Catch:{ all -> 0x004a }
            android.widget.FrameLayout r0 = r1.zzfnc     // Catch:{ all -> 0x004a }
            r2.zzz(r0)     // Catch:{ all -> 0x004a }
            com.google.android.gms.internal.ads.zzbwk r2 = r1.zzfnf     // Catch:{ all -> 0x004a }
            android.widget.FrameLayout r0 = r1.zzbkf     // Catch:{ all -> 0x004a }
            r2.zzaa(r0)     // Catch:{ all -> 0x004a }
            boolean r2 = r1.zzfni     // Catch:{ all -> 0x004a }
            if (r2 == 0) goto L_0x0048
            com.google.android.gms.internal.ads.zzbwk r2 = r1.zzfnf     // Catch:{ all -> 0x004a }
            com.google.android.gms.internal.ads.zzbwq r2 = r2.zzaix()     // Catch:{ all -> 0x004a }
            com.google.android.gms.internal.ads.zzacd r0 = r1.zzcwg     // Catch:{ all -> 0x004a }
            r2.zza(r0)     // Catch:{ all -> 0x004a }
        L_0x0048:
            monitor-exit(r1)
            return
        L_0x004a:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbxh.zza(com.google.android.gms.dynamic.IObjectWrapper):void");
    }

    private final synchronized void zzajy() {
        this.zzfnd.execute(new zzbxk(this));
    }

    public final synchronized void destroy() {
        if (!this.zzegh) {
            if (this.zzfnf != null) {
                this.zzfnf.zzb(this);
                this.zzfnf = null;
            }
            this.zzfnb.clear();
            this.zzfnc.removeAllViews();
            this.zzbkf.removeAllViews();
            this.zzfnb = null;
            this.zzfnc = null;
            this.zzbkf = null;
            this.zzfne = null;
            this.zzfng = null;
            this.zzegh = true;
        }
    }

    public final synchronized void zzc(IObjectWrapper iObjectWrapper, int i) {
    }

    public final synchronized void onClick(View view) {
        if (this.zzfnf != null) {
            this.zzfnf.cancelUnconfirmedClick();
            this.zzfnf.zza(view, this.zzfnc, zzajz(), zzaka(), false);
        }
    }

    public final synchronized boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.zzfnf != null) {
            this.zzfnf.zza(view, motionEvent, this.zzfnc);
        }
        return false;
    }

    public final synchronized void onGlobalLayout() {
        if (this.zzfnf != null) {
            this.zzfnf.zzb(this.zzfnc, zzajz(), zzaka(), zzbwk.zzy(this.zzfnc));
        }
    }

    public final synchronized void onScrollChanged() {
        if (this.zzfnf != null) {
            this.zzfnf.zzb(this.zzfnc, zzajz(), zzaka(), zzbwk.zzy(this.zzfnc));
        }
    }

    public final synchronized Map<String, WeakReference<View>> zzajz() {
        return this.zzfnb;
    }

    public final synchronized Map<String, WeakReference<View>> zzaka() {
        return this.zzfnb;
    }

    public final synchronized Map<String, WeakReference<View>> zzakb() {
        return null;
    }

    public final synchronized String zzakc() {
        return this.zzfmz;
    }

    public final FrameLayout zzakd() {
        return this.zzbkf;
    }

    public final zzpo zzake() {
        return this.zzfng;
    }

    public final synchronized void zze(IObjectWrapper iObjectWrapper) {
        this.zzfnf.setClickConfirmingView((View) ObjectWrapper.unwrap(iObjectWrapper));
    }

    public final synchronized void zzg(IObjectWrapper iObjectWrapper) {
        if (!this.zzegh) {
            this.zzfnh = iObjectWrapper;
        }
    }

    public final IObjectWrapper zzakf() {
        return this.zzfnh;
    }

    public final void zzf(IObjectWrapper iObjectWrapper) {
        onTouch(this.zzfnc, (MotionEvent) ObjectWrapper.unwrap(iObjectWrapper));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zza(com.google.android.gms.internal.ads.zzacd r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r0 = r1.zzegh     // Catch:{ all -> 0x001b }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            r0 = 1
            r1.zzfni = r0     // Catch:{ all -> 0x001b }
            r1.zzcwg = r2     // Catch:{ all -> 0x001b }
            com.google.android.gms.internal.ads.zzbwk r0 = r1.zzfnf     // Catch:{ all -> 0x001b }
            if (r0 == 0) goto L_0x0019
            com.google.android.gms.internal.ads.zzbwk r0 = r1.zzfnf     // Catch:{ all -> 0x001b }
            com.google.android.gms.internal.ads.zzbwq r0 = r0.zzaix()     // Catch:{ all -> 0x001b }
            r0.zza(r2)     // Catch:{ all -> 0x001b }
        L_0x0019:
            monitor-exit(r1)
            return
        L_0x001b:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbxh.zza(com.google.android.gms.internal.ads.zzacd):void");
    }

    public final /* synthetic */ View zzaga() {
        return this.zzfnc;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzakg() {
        if (this.zzfne == null) {
            this.zzfne = new View(this.zzfnc.getContext());
            this.zzfne.setLayoutParams(new FrameLayout.LayoutParams(-1, 0));
        }
        if (this.zzfnc != this.zzfne.getParent()) {
            this.zzfnc.addView(this.zzfne);
        }
    }
}
