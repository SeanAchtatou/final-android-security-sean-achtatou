package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.PriorityQueue;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzqr {
    private final int zzbqg;
    private final int zzbqh;
    private final int zzbqi;
    private final zzqo zzbqj = new zzqv();

    public zzqr(int i) {
        this.zzbqh = i;
        this.zzbqg = 6;
        this.zzbqi = 0;
    }

    public final String zza(ArrayList<String> arrayList) {
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            sb.append(((String) obj).toLowerCase(Locale.US));
            sb.append(10);
        }
        return zzbw(sb.toString());
    }

    private final String zzbw(String str) {
        String[] split = str.split("\n");
        if (split.length == 0) {
            return "";
        }
        zzqt zzqt = new zzqt();
        PriorityQueue priorityQueue = new PriorityQueue(this.zzbqh, new zzqq(this));
        for (String zzd : split) {
            String[] zzd2 = zzqs.zzd(zzd, false);
            if (zzd2.length != 0) {
                zzqx.zza(zzd2, this.zzbqh, this.zzbqg, priorityQueue);
            }
        }
        Iterator it = priorityQueue.iterator();
        while (it.hasNext()) {
            try {
                zzqt.write(this.zzbqj.zzbv(((zzqw) it.next()).zzbqn));
            } catch (IOException e) {
                zzavs.zzc("Error while writing hash to byteStream", e);
            }
        }
        return zzqt.toString();
    }
}
