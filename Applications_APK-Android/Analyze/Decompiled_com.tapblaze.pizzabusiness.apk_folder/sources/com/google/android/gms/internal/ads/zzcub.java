package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzcty;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzcub<S extends zzcty<?>> {
    zzdhe<S> zzanc();
}
