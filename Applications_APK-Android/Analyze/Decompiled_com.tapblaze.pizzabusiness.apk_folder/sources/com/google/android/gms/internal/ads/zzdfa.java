package com.google.android.gms.internal.ads;

import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zzdfa<E> extends zzdes<E> {
    private int zzafx;
    @NullableDecl
    private Object[] zzgup;

    public zzdfa() {
        super(4);
    }

    zzdfa(int i) {
        super(i);
        this.zzgup = new Object[zzdfb.zzdx(i)];
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: com.google.android.gms.internal.ads.zzdfk} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: com.google.android.gms.internal.ads.zzdfb} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v18, resolved type: com.google.android.gms.internal.ads.zzdfk} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: com.google.android.gms.internal.ads.zzdfk} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzdfb<E> zzarh() {
        /*
            r8 = this;
            int r0 = r8.size
            if (r0 == 0) goto L_0x005b
            r1 = 1
            if (r0 == r1) goto L_0x0051
            java.lang.Object[] r0 = r8.zzgup
            if (r0 == 0) goto L_0x003d
            int r0 = r8.size
            int r0 = com.google.android.gms.internal.ads.zzdfb.zzdx(r0)
            java.lang.Object[] r2 = r8.zzgup
            int r2 = r2.length
            if (r0 != r2) goto L_0x003d
            int r0 = r8.size
            java.lang.Object[] r2 = r8.zzguf
            int r2 = r2.length
            boolean r0 = com.google.android.gms.internal.ads.zzdfb.zzv(r0, r2)
            if (r0 == 0) goto L_0x002a
            java.lang.Object[] r0 = r8.zzguf
            int r2 = r8.size
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r2)
            goto L_0x002c
        L_0x002a:
            java.lang.Object[] r0 = r8.zzguf
        L_0x002c:
            r3 = r0
            com.google.android.gms.internal.ads.zzdfk r0 = new com.google.android.gms.internal.ads.zzdfk
            int r4 = r8.zzafx
            java.lang.Object[] r5 = r8.zzgup
            int r2 = r5.length
            int r6 = r2 + -1
            int r7 = r8.size
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7)
            goto L_0x004b
        L_0x003d:
            int r0 = r8.size
            java.lang.Object[] r2 = r8.zzguf
            com.google.android.gms.internal.ads.zzdfb r0 = com.google.android.gms.internal.ads.zzdfb.zza(r0, r2)
            int r2 = r0.size()
            r8.size = r2
        L_0x004b:
            r8.zzgug = r1
            r1 = 0
            r8.zzgup = r1
            return r0
        L_0x0051:
            java.lang.Object[] r0 = r8.zzguf
            r1 = 0
            r0 = r0[r1]
            com.google.android.gms.internal.ads.zzdfb r0 = com.google.android.gms.internal.ads.zzdfb.zzag(r0)
            return r0
        L_0x005b:
            com.google.android.gms.internal.ads.zzdfk<java.lang.Object> r0 = com.google.android.gms.internal.ads.zzdfk.zzgvb
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdfa.zzarh():com.google.android.gms.internal.ads.zzdfb");
    }

    public final /* synthetic */ zzdev zze(Iterable iterable) {
        zzdei.checkNotNull(iterable);
        if (this.zzgup != null) {
            for (Object zzae : iterable) {
                zzae(zzae);
            }
        } else {
            super.zze(iterable);
        }
        return this;
    }

    public final /* synthetic */ zzdes zzad(Object obj) {
        return (zzdfa) zzae(obj);
    }

    public final /* synthetic */ zzdev zza(Iterator it) {
        zzdei.checkNotNull(it);
        while (it.hasNext()) {
            zzae(it.next());
        }
        return this;
    }

    public final /* synthetic */ zzdev zzae(Object obj) {
        zzdei.checkNotNull(obj);
        if (this.zzgup != null) {
            int zzdx = zzdfb.zzdx(this.size);
            Object[] objArr = this.zzgup;
            if (zzdx <= objArr.length) {
                int length = objArr.length - 1;
                int hashCode = obj.hashCode();
                int zzdv = zzdeq.zzdv(hashCode);
                while (true) {
                    int i = zzdv & length;
                    Object[] objArr2 = this.zzgup;
                    Object obj2 = objArr2[i];
                    if (obj2 != null) {
                        if (obj2.equals(obj)) {
                            break;
                        }
                        zzdv = i + 1;
                    } else {
                        objArr2[i] = obj;
                        this.zzafx += hashCode;
                        super.zzae(obj);
                        break;
                    }
                }
                return this;
            }
        }
        this.zzgup = null;
        super.zzae(obj);
        return this;
    }
}
