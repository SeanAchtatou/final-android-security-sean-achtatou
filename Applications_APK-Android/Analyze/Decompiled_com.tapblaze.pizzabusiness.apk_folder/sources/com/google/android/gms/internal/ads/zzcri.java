package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcri implements Callable {
    private final zzcrj zzgfl;

    zzcri(zzcrj zzcrj) {
        this.zzgfl = zzcrj;
    }

    public final Object call() {
        return this.zzgfl.zzang();
    }
}
