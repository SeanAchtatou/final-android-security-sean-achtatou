package com.google.android.gms.internal.p000firebaseperf;

import java.util.Iterator;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzia  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzia implements Iterator<String> {
    private final /* synthetic */ zzhy zzuu;
    private Iterator<String> zzvq = this.zzuu.zzuv.iterator();

    zzia(zzhy zzhy) {
        this.zzuu = zzhy;
    }

    public final boolean hasNext() {
        return this.zzvq.hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ Object next() {
        return this.zzvq.next();
    }
}
