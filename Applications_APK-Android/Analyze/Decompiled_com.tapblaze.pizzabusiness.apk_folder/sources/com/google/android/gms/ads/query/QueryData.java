package com.google.android.gms.ads.query;

import com.google.android.gms.internal.ads.zzapj;
import com.google.android.gms.internal.ads.zzxx;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class QueryData {
    private zzxx zzgrs;

    public QueryData(zzxx zzxx) {
        this.zzgrs = zzxx;
    }

    public String getQuery() {
        return this.zzgrs.getQuery();
    }

    public static void generate(QueryDataConfiguration queryDataConfiguration, QueryDataGenerationCallback queryDataGenerationCallback) {
        new zzapj(queryDataConfiguration).zza(queryDataGenerationCallback);
    }
}
