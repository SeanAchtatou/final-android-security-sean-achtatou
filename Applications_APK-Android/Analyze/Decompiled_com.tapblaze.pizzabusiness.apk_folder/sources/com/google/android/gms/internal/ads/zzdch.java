package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdch {
    private final E zzgpx;
    private final /* synthetic */ zzdcd zzgqd;

    private zzdch(zzdcd zzdcd, E e) {
        this.zzgqd = zzdcd;
        this.zzgpx = e;
    }

    public final <O> zzdcj<O> zzc(zzdhe zzdhe) {
        return new zzdcj(this.zzgqd, this.zzgpx, zzdcd.zzgqa, Collections.emptyList(), zzdhe);
    }

    public final <O> zzdcj<O> zzc(Callable callable) {
        return zza(callable, this.zzgqd.zzfov);
    }

    private final <O> zzdcj<O> zza(Callable callable, zzdhd zzdhd) {
        return new zzdcj(this.zzgqd, this.zzgpx, zzdcd.zzgqa, Collections.emptyList(), zzdhd.zzd(callable));
    }

    public final zzdcj<?> zza(zzdcb zzdcb, zzdhd zzdhd) {
        return zza(new zzdcg(zzdcb), zzdhd);
    }
}
