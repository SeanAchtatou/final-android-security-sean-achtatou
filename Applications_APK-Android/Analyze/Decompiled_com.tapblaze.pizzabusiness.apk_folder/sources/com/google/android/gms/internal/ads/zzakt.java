package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzakt implements Runnable {
    private final Context zzcey;
    private final zzaku zzdbk;
    private final String zzdbl;

    zzakt(zzaku zzaku, Context context, String str) {
        this.zzdbk = zzaku;
        this.zzcey = context;
        this.zzdbl = str;
    }

    public final void run() {
        zzaku.zzd(this.zzcey, this.zzdbl);
    }
}
