package com.google.android.gms.internal.p000firebaseperf;

import java.util.List;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhu  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzhu extends RuntimeException {
    private final List<String> zzup = null;

    public zzhu(zzgl zzgl) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
