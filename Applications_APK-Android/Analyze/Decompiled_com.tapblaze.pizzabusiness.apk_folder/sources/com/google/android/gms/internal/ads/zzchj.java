package com.google.android.gms.internal.ads;

import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.ads.internal.zzq;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzchj implements zzajv<zzchk> {
    zzchj() {
    }

    public final /* synthetic */ JSONObject zzj(Object obj) throws JSONException {
        zzchk zzchk = (zzchk) obj;
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        JSONObject jSONObject3 = new JSONObject();
        jSONObject2.put("base_url", zzchk.zzfwi.zzub());
        jSONObject2.put("signals", zzchk.zzfwj);
        jSONObject3.put("body", zzchk.zzfwl.zzdki);
        jSONObject3.put("headers", zzq.zzkq().zzi(zzchk.zzfwl.zzab));
        jSONObject3.put("response_code", zzchk.zzfwl.zzfws);
        jSONObject3.put("latency", zzchk.zzfwl.zzfwt);
        jSONObject.put(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jSONObject2);
        jSONObject.put(ServerResponseWrapper.RESPONSE_FIELD, jSONObject3);
        jSONObject.put("flags", zzchk.zzfwi.zzue());
        return jSONObject;
    }
}
