package com.google.android.gms.gass.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zzm extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzm> CREATOR = new zzl();
    private final int versionCode;
    private final String zzgsf;
    private final String zzgsg;
    private final int zzgsh;

    zzm(int i, int i2, String str, String str2) {
        this.versionCode = i;
        this.zzgsh = i2;
        this.zzgsf = str;
        this.zzgsg = str2;
    }

    public zzm(int i, String str, String str2) {
        this(1, i, str, str2);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.versionCode);
        SafeParcelWriter.writeInt(parcel, 2, this.zzgsh);
        SafeParcelWriter.writeString(parcel, 3, this.zzgsf, false);
        SafeParcelWriter.writeString(parcel, 4, this.zzgsg, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
