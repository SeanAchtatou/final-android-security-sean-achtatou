package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcai extends zzaef {
    private final String zzfge;
    private final zzbws zzfkc;
    private final zzbwk zzfnf;

    public zzcai(String str, zzbwk zzbwk, zzbws zzbws) {
        this.zzfge = str;
        this.zzfnf = zzbwk;
        this.zzfkc = zzbws;
    }

    public final IObjectWrapper zzrf() throws RemoteException {
        return ObjectWrapper.wrap(this.zzfnf);
    }

    public final String getHeadline() throws RemoteException {
        return this.zzfkc.getHeadline();
    }

    public final List<?> getImages() throws RemoteException {
        return this.zzfkc.getImages();
    }

    public final List<?> getMuteThisAdReasons() throws RemoteException {
        if (isCustomMuteThisAdEnabled()) {
            return this.zzfkc.getMuteThisAdReasons();
        }
        return Collections.emptyList();
    }

    public final boolean isCustomMuteThisAdEnabled() throws RemoteException {
        return !this.zzfkc.getMuteThisAdReasons().isEmpty() && this.zzfkc.zzajd() != null;
    }

    public final String getBody() throws RemoteException {
        return this.zzfkc.getBody();
    }

    public final zzaci zzrg() throws RemoteException {
        return this.zzfkc.zzrg();
    }

    public final String getCallToAction() throws RemoteException {
        return this.zzfkc.getCallToAction();
    }

    public final String getAdvertiser() throws RemoteException {
        return this.zzfkc.getAdvertiser();
    }

    public final double getStarRating() throws RemoteException {
        return this.zzfkc.getStarRating();
    }

    public final String getStore() throws RemoteException {
        return this.zzfkc.getStore();
    }

    public final String getPrice() throws RemoteException {
        return this.zzfkc.getPrice();
    }

    public final void destroy() throws RemoteException {
        this.zzfnf.destroy();
    }

    public final zzxb getVideoController() throws RemoteException {
        return this.zzfkc.getVideoController();
    }

    public final void performClick(Bundle bundle) throws RemoteException {
        this.zzfnf.zzf(bundle);
    }

    public final boolean recordImpression(Bundle bundle) throws RemoteException {
        return this.zzfnf.zzh(bundle);
    }

    public final void reportTouchEvent(Bundle bundle) throws RemoteException {
        this.zzfnf.zzg(bundle);
    }

    public final zzaca zzrh() throws RemoteException {
        return this.zzfkc.zzrh();
    }

    public final IObjectWrapper zzri() throws RemoteException {
        return this.zzfkc.zzri();
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        return this.zzfge;
    }

    public final Bundle getExtras() throws RemoteException {
        return this.zzfkc.getExtras();
    }

    public final void zza(zzaeb zzaeb) throws RemoteException {
        this.zzfnf.zza(zzaeb);
    }

    public final void zza(zzwr zzwr) throws RemoteException {
        this.zzfnf.zza(zzwr);
    }

    public final void zza(zzwn zzwn) throws RemoteException {
        this.zzfnf.zza(zzwn);
    }

    public final void zzrp() {
        this.zzfnf.zzrp();
    }

    public final void recordCustomClickGesture() {
        this.zzfnf.recordCustomClickGesture();
    }

    public final boolean isCustomClickGestureEnabled() {
        return this.zzfnf.isCustomClickGestureEnabled();
    }

    public final void cancelUnconfirmedClick() throws RemoteException {
        this.zzfnf.cancelUnconfirmedClick();
    }

    public final zzacd zzrq() throws RemoteException {
        return this.zzfnf.zzaix().zzrq();
    }

    public final zzxa zzkb() throws RemoteException {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcrf)).booleanValue()) {
            return null;
        }
        return this.zzfnf.zzags();
    }
}
