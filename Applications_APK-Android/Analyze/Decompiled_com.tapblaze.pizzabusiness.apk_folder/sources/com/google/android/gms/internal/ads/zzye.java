package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzye implements Runnable {
    private final /* synthetic */ zzyb zzcfk;

    zzye(zzyb zzyb) {
        this.zzcfk = zzyb;
    }

    public final void run() {
        if (this.zzcfk.zzcfj.zzblq != null) {
            try {
                this.zzcfk.zzcfj.zzblq.onAdFailedToLoad(1);
            } catch (RemoteException e) {
                zzayu.zzd("Could not notify onAdFailedToLoad event.", e);
            }
        }
    }
}
