package com.google.android.gms.internal.ads;

import android.os.Bundle;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzaew {
    void zza(String str, Bundle bundle);
}
