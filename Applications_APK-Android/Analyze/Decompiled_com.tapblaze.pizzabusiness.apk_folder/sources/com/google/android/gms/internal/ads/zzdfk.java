package com.google.android.gms.internal.ads;

import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzdfk<E> extends zzdfb<E> {
    static final zzdfk<Object> zzgvb = new zzdfk(new Object[0], 0, null, 0, 0);
    private final transient int mask;
    private final transient int size;
    private final transient int zzafx;
    private final transient Object[] zzgvc;
    private final transient Object[] zzgvd;

    zzdfk(Object[] objArr, int i, Object[] objArr2, int i2, int i3) {
        this.zzgvc = objArr;
        this.zzgvd = objArr2;
        this.mask = i2;
        this.zzafx = i;
        this.size = i3;
    }

    /* access modifiers changed from: package-private */
    public final int zzaqz() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzarc() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzari() {
        return true;
    }

    public final boolean contains(@NullableDecl Object obj) {
        int i;
        Object[] objArr = this.zzgvd;
        if (obj == null || objArr == null) {
            return false;
        }
        if (obj == null) {
            i = 0;
        } else {
            i = obj.hashCode();
        }
        int zzdv = zzdeq.zzdv(i);
        while (true) {
            int i2 = zzdv & this.mask;
            Object obj2 = objArr[i2];
            if (obj2 == null) {
                return false;
            }
            if (obj2.equals(obj)) {
                return true;
            }
            zzdv = i2 + 1;
        }
    }

    public final int size() {
        return this.size;
    }

    public final zzdfp<E> zzaqx() {
        return (zzdfp) zzarb().iterator();
    }

    /* access modifiers changed from: package-private */
    public final Object[] zzaqy() {
        return this.zzgvc;
    }

    /* access modifiers changed from: package-private */
    public final int zzara() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    public final int zza(Object[] objArr, int i) {
        System.arraycopy(this.zzgvc, 0, objArr, i, this.size);
        return i + this.size;
    }

    /* access modifiers changed from: package-private */
    public final zzdeu<E> zzarj() {
        return zzdeu.zzb(this.zzgvc, this.size);
    }

    public final int hashCode() {
        return this.zzafx;
    }

    public final /* synthetic */ Iterator iterator() {
        return iterator();
    }
}
