package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzau  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzau extends zzaz<Long> {
    private static zzau zzay;

    private zzau() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.TraceEventCountBackground";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_rl_trace_event_count_bg";
    }

    public static synchronized zzau zzaw() {
        zzau zzau;
        synchronized (zzau.class) {
            if (zzay == null) {
                zzay = new zzau();
            }
            zzau = zzay;
        }
        return zzau;
    }
}
