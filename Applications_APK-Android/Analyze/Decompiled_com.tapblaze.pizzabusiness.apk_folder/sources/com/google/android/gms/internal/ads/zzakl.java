package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzakl implements zzded {
    private final zzafn zzcyv;
    private final String zzcyz;

    zzakl(String str, zzafn zzafn) {
        this.zzcyz = str;
        this.zzcyv = zzafn;
    }

    public final Object apply(Object obj) {
        zzajq zzajq = (zzajq) obj;
        zzajq.zzb(this.zzcyz, this.zzcyv);
        return zzajq;
    }
}
