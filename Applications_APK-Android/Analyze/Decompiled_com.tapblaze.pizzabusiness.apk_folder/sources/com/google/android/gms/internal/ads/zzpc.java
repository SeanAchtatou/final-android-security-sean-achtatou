package com.google.android.gms.internal.ads;

import android.media.MediaCodec;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzpc implements MediaCodec.OnFrameRenderedListener {
    private final /* synthetic */ zzox zzbja;

    private zzpc(zzox zzox, MediaCodec mediaCodec) {
        this.zzbja = zzox;
        mediaCodec.setOnFrameRenderedListener(this, new zzddu());
    }

    public final void onFrameRendered(MediaCodec mediaCodec, long j, long j2) {
        if (this == this.zzbja.zzbil) {
            this.zzbja.zzjb();
        }
    }
}
