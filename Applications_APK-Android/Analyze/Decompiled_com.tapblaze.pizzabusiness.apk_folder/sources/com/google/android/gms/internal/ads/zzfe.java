package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.internal.ads.zzbs;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzfe extends zzfw {
    public zzfe(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 24);
    }

    public final Void zzcp() throws Exception {
        if (this.zzuv.isInitialized()) {
            return super.call();
        }
        if (!this.zzuv.zzcb()) {
            return null;
        }
        zzcq();
        return null;
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        if (this.zzuv.zzcb()) {
            zzcq();
            return;
        }
        synchronized (this.zzzt) {
            this.zzzt.zzan((String) this.zzaae.invoke(null, this.zzuv.getContext()));
        }
    }

    private final void zzcq() {
        AdvertisingIdClient zzcj = this.zzuv.zzcj();
        if (zzcj != null) {
            try {
                AdvertisingIdClient.Info info = zzcj.getInfo();
                String zzat = zzep.zzat(info.getId());
                if (zzat != null) {
                    synchronized (this.zzzt) {
                        this.zzzt.zzan(zzat);
                        this.zzzt.zzb(info.isLimitAdTrackingEnabled());
                        this.zzzt.zzb(zzbs.zza.zzc.DEVICE_IDENTIFIER_ANDROID_AD_ID);
                    }
                }
            } catch (IOException unused) {
            }
        }
    }

    public final /* synthetic */ Object call() throws Exception {
        return call();
    }
}
