package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzah  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzah extends zzaz<Boolean> {
    private static zzah zzak;

    private zzah() {
    }

    /* access modifiers changed from: protected */
    public final String zzaf() {
        return "firebase_performance_collection_deactivated";
    }

    protected static synchronized zzah zzah() {
        zzah zzah;
        synchronized (zzah.class) {
            if (zzak == null) {
                zzak = new zzah();
            }
            zzah = zzak;
        }
        return zzah;
    }
}
