package com.google.android.gms.internal.ads;

import java.util.Comparator;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdqm implements Comparator<zzdqk> {
    zzdqm() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        zzdqk zzdqk = (zzdqk) obj;
        zzdqk zzdqk2 = (zzdqk) obj2;
        zzdqp zzdqp = (zzdqp) zzdqk.iterator();
        zzdqp zzdqp2 = (zzdqp) zzdqk2.iterator();
        while (zzdqp.hasNext() && zzdqp2.hasNext()) {
            int compare = Integer.compare(zzdqk.zzb(zzdqp.nextByte()), zzdqk.zzb(zzdqp2.nextByte()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(zzdqk.size(), zzdqk2.size());
    }
}
