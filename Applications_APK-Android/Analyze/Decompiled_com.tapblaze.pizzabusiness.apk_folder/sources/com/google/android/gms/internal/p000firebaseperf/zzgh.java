package com.google.android.gms.internal.p000firebaseperf;

import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgh  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzgh implements zzgi {
    zzgh() {
    }

    public final zzgg<?, ?> zzj(Object obj) {
        return ((zzgd) obj).zzia();
    }

    public final Map<?, ?> zzk(Object obj) {
        return (zzgf) obj;
    }

    public final Object zzl(Object obj) {
        ((zzgf) obj).zzgg();
        return obj;
    }

    public final Object zzc(Object obj, Object obj2) {
        zzgf zzgf = (zzgf) obj;
        zzgf zzgf2 = (zzgf) obj2;
        if (!zzgf2.isEmpty()) {
            if (!zzgf.isMutable()) {
                zzgf = zzgf.zzic();
            }
            zzgf.zza(zzgf2);
        }
        return zzgf;
    }

    public final int zzb(int i, Object obj, Object obj2) {
        zzgf zzgf = (zzgf) obj;
        zzgd zzgd = (zzgd) obj2;
        int i2 = 0;
        if (zzgf.isEmpty()) {
            return 0;
        }
        for (Map.Entry entry : zzgf.entrySet()) {
            i2 += zzgd.zza(i, entry.getKey(), entry.getValue());
        }
        return i2;
    }
}
