package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.AdListener;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class zzvd extends AdListener {
    private final Object lock = new Object();
    private AdListener zzcdq;

    public final void zza(AdListener adListener) {
        synchronized (this.lock) {
            this.zzcdq = adListener;
        }
    }

    public void onAdClosed() {
        synchronized (this.lock) {
            if (this.zzcdq != null) {
                this.zzcdq.onAdClosed();
            }
        }
    }

    public void onAdFailedToLoad(int i) {
        synchronized (this.lock) {
            if (this.zzcdq != null) {
                this.zzcdq.onAdFailedToLoad(i);
            }
        }
    }

    public void onAdLeftApplication() {
        synchronized (this.lock) {
            if (this.zzcdq != null) {
                this.zzcdq.onAdLeftApplication();
            }
        }
    }

    public void onAdOpened() {
        synchronized (this.lock) {
            if (this.zzcdq != null) {
                this.zzcdq.onAdOpened();
            }
        }
    }

    public void onAdLoaded() {
        synchronized (this.lock) {
            if (this.zzcdq != null) {
                this.zzcdq.onAdLoaded();
            }
        }
    }
}
