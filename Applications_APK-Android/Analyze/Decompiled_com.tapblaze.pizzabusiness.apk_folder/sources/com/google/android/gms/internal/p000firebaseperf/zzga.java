package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzga  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzga extends zzfv {
    private zzga() {
        super();
    }

    /* access modifiers changed from: package-private */
    public final void zza(Object obj, long j) {
        zzc(obj, j).zzgg();
    }

    /* access modifiers changed from: package-private */
    public final <E> void zza(Object obj, Object obj2, long j) {
        zzfm zzc = zzc(obj, j);
        zzfm zzc2 = zzc(obj2, j);
        int size = zzc.size();
        int size2 = zzc2.size();
        if (size > 0 && size2 > 0) {
            if (!zzc.zzgf()) {
                zzc = zzc.zzao(size2 + size);
            }
            zzc.addAll(zzc2);
        }
        if (size > 0) {
            zzc2 = zzc;
        }
        zzhz.zza(obj, j, zzc2);
    }

    private static <E> zzfm<E> zzc(Object obj, long j) {
        return (zzfm) zzhz.zzo(obj, j);
    }
}
