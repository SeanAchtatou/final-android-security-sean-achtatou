package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import androidx.core.view.InputDeviceCompat;
import androidx.core.view.ViewCompat;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaoo;
import com.google.android.gms.internal.ads.zzaos;
import com.google.android.gms.internal.ads.zzavs;
import com.google.android.gms.internal.ads.zzawb;
import com.google.android.gms.internal.ads.zzawh;
import com.google.android.gms.internal.ads.zzbdi;
import com.google.android.gms.internal.ads.zzbdr;
import com.google.android.gms.internal.ads.zzbev;
import com.google.android.gms.internal.ads.zzsm;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;
import java.util.Collections;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzc extends zzaos implements zzy {
    private static final int zzdgm = Color.argb(0, 0, 0, 0);
    private boolean zzbkx = false;
    zzbdi zzcza;
    AdOverlayInfoParcel zzdgn;
    private zzi zzdgo;
    private zzq zzdgp;
    private boolean zzdgq = false;
    private FrameLayout zzdgr;
    private WebChromeClient.CustomViewCallback zzdgs;
    private boolean zzdgt = false;
    private zzj zzdgu;
    private boolean zzdgv = false;
    int zzdgw = 0;
    private final Object zzdgx = new Object();
    private Runnable zzdgy;
    private boolean zzdgz;
    private boolean zzdha;
    private boolean zzdhb = false;
    private boolean zzdhc = false;
    private boolean zzdhd = true;
    protected final Activity zzzk;

    public zzc(Activity activity) {
        this.zzzk = activity;
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
    }

    public final void onRestart() {
    }

    public final void close() {
        this.zzdgw = 2;
        this.zzzk.finish();
    }

    public final void zztk() {
        AdOverlayInfoParcel adOverlayInfoParcel = this.zzdgn;
        if (adOverlayInfoParcel != null && this.zzdgq) {
            setRequestedOrientation(adOverlayInfoParcel.orientation);
        }
        if (this.zzdgr != null) {
            this.zzzk.setContentView(this.zzdgu);
            this.zzdha = true;
            this.zzdgr.removeAllViews();
            this.zzdgr = null;
        }
        WebChromeClient.CustomViewCallback customViewCallback = this.zzdgs;
        if (customViewCallback != null) {
            customViewCallback.onCustomViewHidden();
            this.zzdgs = null;
        }
        this.zzdgq = false;
    }

    public final void zztl() {
        this.zzdgw = 1;
        this.zzzk.finish();
    }

    public final void onBackPressed() {
        this.zzdgw = 0;
    }

    public final boolean zztm() {
        this.zzdgw = 0;
        zzbdi zzbdi = this.zzcza;
        if (zzbdi == null) {
            return true;
        }
        boolean zzaah = zzbdi.zzaah();
        if (!zzaah) {
            this.zzcza.zza("onbackblocked", Collections.emptyMap());
        }
        return zzaah;
    }

    public void onCreate(Bundle bundle) {
        this.zzzk.requestWindowFeature(1);
        this.zzdgt = bundle != null && bundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false);
        try {
            this.zzdgn = AdOverlayInfoParcel.zzc(this.zzzk.getIntent());
            if (this.zzdgn != null) {
                if (this.zzdgn.zzbll.zzdwa > 7500000) {
                    this.zzdgw = 3;
                }
                if (this.zzzk.getIntent() != null) {
                    this.zzdhd = this.zzzk.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true);
                }
                if (this.zzdgn.zzdhx != null) {
                    this.zzbkx = this.zzdgn.zzdhx.zzbkx;
                } else {
                    this.zzbkx = false;
                }
                if (this.zzbkx && this.zzdgn.zzdhx.zzblc != -1) {
                    new zzl(this).zzvr();
                }
                if (bundle == null) {
                    if (this.zzdgn.zzdhq != null && this.zzdhd) {
                        this.zzdgn.zzdhq.zztf();
                    }
                    if (!(this.zzdgn.zzdhv == 1 || this.zzdgn.zzcbt == null)) {
                        this.zzdgn.zzcbt.onAdClicked();
                    }
                }
                this.zzdgu = new zzj(this.zzzk, this.zzdgn.zzdhw, this.zzdgn.zzbll.zzbma);
                this.zzdgu.setId(1000);
                zzq.zzks().zzg(this.zzzk);
                int i = this.zzdgn.zzdhv;
                if (i == 1) {
                    zzaj(false);
                } else if (i == 2) {
                    this.zzdgo = new zzi(this.zzdgn.zzcza);
                    zzaj(false);
                } else if (i == 3) {
                    zzaj(true);
                } else {
                    throw new zzg("Could not determine ad overlay type.");
                }
            } else {
                throw new zzg("Could not get info for ad overlay.");
            }
        } catch (zzg e) {
            zzavs.zzez(e.getMessage());
            this.zzdgw = 3;
            this.zzzk.finish();
        }
    }

    public final void onStart() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue()) {
            zzbdi zzbdi = this.zzcza;
            if (zzbdi == null || zzbdi.isDestroyed()) {
                zzavs.zzez("The webview does not exist. Ignoring action.");
                return;
            }
            zzq.zzks();
            zzawh.zzb(this.zzcza);
        }
    }

    public final void onResume() {
        if (this.zzdgn.zzdhq != null) {
            this.zzdgn.zzdhq.onResume();
        }
        zza(this.zzzk.getResources().getConfiguration());
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue()) {
            zzbdi zzbdi = this.zzcza;
            if (zzbdi == null || zzbdi.isDestroyed()) {
                zzavs.zzez("The webview does not exist. Ignoring action.");
                return;
            }
            zzq.zzks();
            zzawh.zzb(this.zzcza);
        }
    }

    public final void onPause() {
        zztk();
        if (this.zzdgn.zzdhq != null) {
            this.zzdgn.zzdhq.onPause();
        }
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue() && this.zzcza != null && (!this.zzzk.isFinishing() || this.zzdgo == null)) {
            zzq.zzks();
            zzawh.zza(this.zzcza);
        }
        zzto();
    }

    public final void zzad(IObjectWrapper iObjectWrapper) {
        zza((Configuration) ObjectWrapper.unwrap(iObjectWrapper));
    }

    public final void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.zzdgt);
    }

    public final void onStop() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue() && this.zzcza != null && (!this.zzzk.isFinishing() || this.zzdgo == null)) {
            zzq.zzks();
            zzawh.zza(this.zzcza);
        }
        zzto();
    }

    public final void onDestroy() {
        zzbdi zzbdi = this.zzcza;
        if (zzbdi != null) {
            try {
                this.zzdgu.removeView(zzbdi.getView());
            } catch (NullPointerException unused) {
            }
        }
        zzto();
    }

    private final void zzai(boolean z) {
        int intValue = ((Integer) zzve.zzoy().zzd(zzzn.zzcnv)).intValue();
        zzp zzp = new zzp();
        zzp.size = 50;
        zzp.paddingLeft = z ? intValue : 0;
        zzp.paddingRight = z ? 0 : intValue;
        zzp.paddingTop = 0;
        zzp.paddingBottom = intValue;
        this.zzdgp = new zzq(this.zzzk, zzp, this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(z ? 11 : 9);
        zza(z, this.zzdgn.zzdhs);
        this.zzdgu.addView(this.zzdgp, layoutParams);
    }

    public final void zzdf() {
        this.zzdha = true;
    }

    public final void zza(boolean z, boolean z2) {
        AdOverlayInfoParcel adOverlayInfoParcel;
        AdOverlayInfoParcel adOverlayInfoParcel2;
        boolean z3 = true;
        boolean z4 = ((Boolean) zzve.zzoy().zzd(zzzn.zzcjh)).booleanValue() && (adOverlayInfoParcel2 = this.zzdgn) != null && adOverlayInfoParcel2.zzdhx != null && this.zzdgn.zzdhx.zzble;
        boolean z5 = ((Boolean) zzve.zzoy().zzd(zzzn.zzcji)).booleanValue() && (adOverlayInfoParcel = this.zzdgn) != null && adOverlayInfoParcel.zzdhx != null && this.zzdgn.zzdhx.zzblf;
        if (z && z2 && z4 && !z5) {
            new zzaoo(this.zzcza, "useCustomClose").zzds("Custom close has been disabled for interstitial ads in this ad slot.");
        }
        zzq zzq = this.zzdgp;
        if (zzq != null) {
            if (!z5 && (!z2 || z4)) {
                z3 = false;
            }
            zzq.zzal(z3);
        }
    }

    public final void zztn() {
        this.zzdgu.removeView(this.zzdgp);
        zzai(true);
    }

    public final void setRequestedOrientation(int i) {
        if (this.zzzk.getApplicationInfo().targetSdkVersion >= ((Integer) zzve.zzoy().zzd(zzzn.zzcpr)).intValue()) {
            if (this.zzzk.getApplicationInfo().targetSdkVersion <= ((Integer) zzve.zzoy().zzd(zzzn.zzcps)).intValue()) {
                if (Build.VERSION.SDK_INT >= ((Integer) zzve.zzoy().zzd(zzzn.zzcpt)).intValue()) {
                    if (Build.VERSION.SDK_INT <= ((Integer) zzve.zzoy().zzd(zzzn.zzcpu)).intValue()) {
                        return;
                    }
                }
            }
        }
        try {
            this.zzzk.setRequestedOrientation(i);
        } catch (Throwable th) {
            zzq.zzku().zzb(th, "AdOverlay.setRequestedOrientation");
        }
    }

    public final void zza(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        this.zzdgr = new FrameLayout(this.zzzk);
        this.zzdgr.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.zzdgr.addView(view, -1, -1);
        this.zzzk.setContentView(this.zzdgr);
        this.zzdha = true;
        this.zzdgs = customViewCallback;
        this.zzdgq = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.ads.internal.overlay.zzc.zza(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.google.android.gms.ads.internal.overlay.zzc.zza(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.google.android.gms.ads.internal.overlay.zzc.zza(boolean, boolean):void */
    private final void zzaj(boolean z) throws zzg {
        if (!this.zzdha) {
            this.zzzk.requestWindowFeature(1);
        }
        Window window = this.zzzk.getWindow();
        if (window != null) {
            zzbev zzaaa = this.zzdgn.zzcza != null ? this.zzdgn.zzcza.zzaaa() : null;
            boolean z2 = false;
            boolean z3 = zzaaa != null && zzaaa.zzaat();
            this.zzdgv = false;
            if (z3) {
                int i = this.zzdgn.orientation;
                zzq.zzks();
                if (i == 6) {
                    if (this.zzzk.getResources().getConfiguration().orientation == 1) {
                        z2 = true;
                    }
                    this.zzdgv = z2;
                } else {
                    int i2 = this.zzdgn.orientation;
                    zzq.zzks();
                    if (i2 == 7) {
                        if (this.zzzk.getResources().getConfiguration().orientation == 2) {
                            z2 = true;
                        }
                        this.zzdgv = z2;
                    }
                }
            }
            boolean z4 = this.zzdgv;
            StringBuilder sb = new StringBuilder(46);
            sb.append("Delay onShow to next orientation change: ");
            sb.append(z4);
            zzavs.zzea(sb.toString());
            setRequestedOrientation(this.zzdgn.orientation);
            zzq.zzks();
            window.setFlags(16777216, 16777216);
            zzavs.zzea("Hardware acceleration on the AdActivity window enabled.");
            if (!this.zzbkx) {
                this.zzdgu.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            } else {
                this.zzdgu.setBackgroundColor(zzdgm);
            }
            this.zzzk.setContentView(this.zzdgu);
            this.zzdha = true;
            if (z) {
                try {
                    zzq.zzkr();
                    this.zzcza = zzbdr.zza(this.zzzk, this.zzdgn.zzcza != null ? this.zzdgn.zzcza.zzzy() : null, this.zzdgn.zzcza != null ? this.zzdgn.zzcza.zzzz() : null, true, z3, null, this.zzdgn.zzbll, null, null, this.zzdgn.zzcza != null ? this.zzdgn.zzcza.zzyo() : null, zzsm.zzmt(), null, false);
                    this.zzcza.zzaaa().zza(null, this.zzdgn.zzcwq, null, this.zzdgn.zzcws, this.zzdgn.zzdhu, true, null, this.zzdgn.zzcza != null ? this.zzdgn.zzcza.zzaaa().zzaas() : null, null, null);
                    this.zzcza.zzaaa().zza(new zzf(this));
                    if (this.zzdgn.url != null) {
                        this.zzcza.loadUrl(this.zzdgn.url);
                    } else if (this.zzdgn.zzdht != null) {
                        this.zzcza.loadDataWithBaseURL(this.zzdgn.zzdhr, this.zzdgn.zzdht, "text/html", "UTF-8", null);
                    } else {
                        throw new zzg("No URL or HTML to display in ad overlay.");
                    }
                    if (this.zzdgn.zzcza != null) {
                        this.zzdgn.zzcza.zzb(this);
                    }
                } catch (Exception e) {
                    zzavs.zzc("Error obtaining webview.", e);
                    throw new zzg("Could not obtain webview for the overlay.");
                }
            } else {
                this.zzcza = this.zzdgn.zzcza;
                this.zzcza.zzbr(this.zzzk);
            }
            this.zzcza.zza(this);
            if (this.zzdgn.zzcza != null) {
                zzc(this.zzdgn.zzcza.zzaae(), this.zzdgu);
            }
            ViewParent parent = this.zzcza.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ((ViewGroup) parent).removeView(this.zzcza.getView());
            }
            if (this.zzbkx) {
                this.zzcza.zzaam();
            }
            this.zzcza.zza((ViewGroup) null, this.zzzk, this.zzdgn.zzdhr, this.zzdgn.zzdht);
            this.zzdgu.addView(this.zzcza.getView(), -1, -1);
            if (!z && !this.zzdgv) {
                zztr();
            }
            zzai(z3);
            if (this.zzcza.zzaac()) {
                zza(z3, true);
                return;
            }
            return;
        }
        throw new zzg("Invalid activity, no window available.");
    }

    private final void zzto() {
        if (this.zzzk.isFinishing() && !this.zzdhb) {
            this.zzdhb = true;
            zzbdi zzbdi = this.zzcza;
            if (zzbdi != null) {
                zzbdi.zzde(this.zzdgw);
                synchronized (this.zzdgx) {
                    if (!this.zzdgz && this.zzcza.zzaai()) {
                        this.zzdgy = new zze(this);
                        zzawb.zzdsr.postDelayed(this.zzdgy, ((Long) zzve.zzoy().zzd(zzzn.zzcjg)).longValue());
                        return;
                    }
                }
            }
            zztp();
        }
    }

    /* access modifiers changed from: package-private */
    public final void zztp() {
        if (!this.zzdhc) {
            this.zzdhc = true;
            zzbdi zzbdi = this.zzcza;
            if (zzbdi != null) {
                this.zzdgu.removeView(zzbdi.getView());
                zzi zzi = this.zzdgo;
                if (zzi != null) {
                    this.zzcza.zzbr(zzi.zzup);
                    this.zzcza.zzax(false);
                    this.zzdgo.parent.addView(this.zzcza.getView(), this.zzdgo.index, this.zzdgo.zzdhj);
                    this.zzdgo = null;
                } else if (this.zzzk.getApplicationContext() != null) {
                    this.zzcza.zzbr(this.zzzk.getApplicationContext());
                }
                this.zzcza = null;
            }
            AdOverlayInfoParcel adOverlayInfoParcel = this.zzdgn;
            if (!(adOverlayInfoParcel == null || adOverlayInfoParcel.zzdhq == null)) {
                this.zzdgn.zzdhq.zzte();
            }
            AdOverlayInfoParcel adOverlayInfoParcel2 = this.zzdgn;
            if (adOverlayInfoParcel2 != null && adOverlayInfoParcel2.zzcza != null) {
                zzc(this.zzdgn.zzcza.zzaae(), this.zzdgn.zzcza.getView());
            }
        }
    }

    private static void zzc(IObjectWrapper iObjectWrapper, View view) {
        if (iObjectWrapper != null && view != null) {
            zzq.zzlf().zza(iObjectWrapper, view);
        }
    }

    public final void zztq() {
        if (this.zzdgv) {
            this.zzdgv = false;
            zztr();
        }
    }

    private final void zztr() {
        this.zzcza.zztr();
    }

    public final void zzts() {
        this.zzdgu.zzdhl = true;
    }

    public final void zztt() {
        synchronized (this.zzdgx) {
            this.zzdgz = true;
            if (this.zzdgy != null) {
                zzawb.zzdsr.removeCallbacks(this.zzdgy);
                zzawb.zzdsr.post(this.zzdgy);
            }
        }
    }

    private final void zza(Configuration configuration) {
        AdOverlayInfoParcel adOverlayInfoParcel;
        AdOverlayInfoParcel adOverlayInfoParcel2 = this.zzdgn;
        boolean z = true;
        boolean z2 = false;
        boolean z3 = (adOverlayInfoParcel2 == null || adOverlayInfoParcel2.zzdhx == null || !this.zzdgn.zzdhx.zzbky) ? false : true;
        boolean zza = zzq.zzks().zza(this.zzzk, configuration);
        if ((this.zzbkx && !z3) || zza) {
            z = false;
        } else if (Build.VERSION.SDK_INT >= 19 && (adOverlayInfoParcel = this.zzdgn) != null && adOverlayInfoParcel.zzdhx != null && this.zzdgn.zzdhx.zzbld) {
            z2 = true;
        }
        Window window = this.zzzk.getWindow();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcjj)).booleanValue() && Build.VERSION.SDK_INT >= 19) {
            View decorView = window.getDecorView();
            int i = 256;
            if (z) {
                i = 5380;
                if (z2) {
                    i = 5894;
                }
            }
            decorView.setSystemUiVisibility(i);
        } else if (z) {
            window.addFlags(1024);
            window.clearFlags(2048);
            if (Build.VERSION.SDK_INT >= 19 && z2) {
                window.getDecorView().setSystemUiVisibility(InputDeviceCompat.SOURCE_TOUCHSCREEN);
            }
        } else {
            window.addFlags(2048);
            window.clearFlags(1024);
        }
    }
}
