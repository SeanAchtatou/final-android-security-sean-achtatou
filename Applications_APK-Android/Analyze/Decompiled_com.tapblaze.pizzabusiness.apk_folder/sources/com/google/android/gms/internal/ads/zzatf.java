package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.internal.ads.zzdvx;
import com.vungle.warren.model.CookieDBAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javax.annotation.ParametersAreNonnullByDefault;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzatf implements zzato {
    /* access modifiers changed from: private */
    public static List<Future<Void>> zzdod = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public final Object lock = new Object();
    private final zzatn zzdld;
    /* access modifiers changed from: private */
    public final zzdwi zzdoe;
    private final LinkedHashMap<String, zzdwm> zzdof;
    private final List<String> zzdog = new ArrayList();
    private final List<String> zzdoh = new ArrayList();
    private final zzatq zzdoi;
    private boolean zzdoj;
    private final zzatt zzdok;
    private HashSet<String> zzdol = new HashSet<>();
    private boolean zzdom = false;
    private boolean zzdon = false;
    private boolean zzdoo = false;
    private final Context zzup;

    public zzatf(Context context, zzazb zzazb, zzatn zzatn, String str, zzatq zzatq) {
        Preconditions.checkNotNull(zzatn, "SafeBrowsing config is not present.");
        this.zzup = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        this.zzdof = new LinkedHashMap<>();
        this.zzdoi = zzatq;
        this.zzdld = zzatn;
        for (String lowerCase : this.zzdld.zzdow) {
            this.zzdol.add(lowerCase.toLowerCase(Locale.ENGLISH));
        }
        this.zzdol.remove(CookieDBAdapter.ookieColumns.TABLE_NAME.toLowerCase(Locale.ENGLISH));
        zzdwi zzdwi = new zzdwi();
        zzdwi.zzhxr = zzdvx.zzb.zzg.OCTAGON_AD;
        zzdwi.url = str;
        zzdwi.zzhxt = str;
        zzdvx.zzb.C0019zzb.zza zzbcw = zzdvx.zzb.C0019zzb.zzbcw();
        if (this.zzdld.zzdos != null) {
            zzbcw.zzhl(this.zzdld.zzdos);
        }
        zzdwi.zzhxv = (zzdvx.zzb.C0019zzb) ((zzdrt) zzbcw.zzbaf());
        zzdvx.zzb.zzi.zza zzbs = zzdvx.zzb.zzi.zzbdg().zzbs(Wrappers.packageManager(this.zzup).isCallerInstantApp());
        if (zzazb.zzbma != null) {
            zzbs.zzho(zzazb.zzbma);
        }
        long apkVersion = (long) GoogleApiAvailabilityLight.getInstance().getApkVersion(this.zzup);
        if (apkVersion > 0) {
            zzbs.zzfu(apkVersion);
        }
        zzdwi.zzhyf = (zzdvx.zzb.zzi) ((zzdrt) zzbs.zzbaf());
        this.zzdoe = zzdwi;
        this.zzdok = new zzatt(this.zzup, this.zzdld.zzdoz, this);
    }

    static final /* synthetic */ Void zzdz(String str) {
        return null;
    }

    public final zzatn zzuk() {
        return this.zzdld;
    }

    public final void zzdv(String str) {
        synchronized (this.lock) {
            this.zzdoe.zzhxx = str;
        }
    }

    public final boolean zzul() {
        return PlatformVersion.isAtLeastKitKat() && this.zzdld.zzdou && !this.zzdon;
    }

    public final void zzj(View view) {
        if (this.zzdld.zzdou && !this.zzdon) {
            zzq.zzkq();
            Bitmap zzl = zzawb.zzl(view);
            if (zzl == null) {
                zzatp.zzea("Failed to capture the webview bitmap.");
                return;
            }
            this.zzdon = true;
            zzawb.zzc(new zzatg(this, zzl));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(java.lang.String r7, java.util.Map<java.lang.String, java.lang.String> r8, int r9) {
        /*
            r6 = this;
            java.lang.Object r0 = r6.lock
            monitor-enter(r0)
            r1 = 3
            if (r9 != r1) goto L_0x0009
            r2 = 1
            r6.zzdoo = r2     // Catch:{ all -> 0x00c6 }
        L_0x0009:
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.ads.zzdwm> r2 = r6.zzdof     // Catch:{ all -> 0x00c6 }
            boolean r2 = r2.containsKey(r7)     // Catch:{ all -> 0x00c6 }
            if (r2 == 0) goto L_0x0023
            if (r9 != r1) goto L_0x0021
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.ads.zzdwm> r8 = r6.zzdof     // Catch:{ all -> 0x00c6 }
            java.lang.Object r7 = r8.get(r7)     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdwm r7 = (com.google.android.gms.internal.ads.zzdwm) r7     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdvx$zzb$zzh$zza r8 = com.google.android.gms.internal.ads.zzdvx.zzb.zzh.zza.zzhg(r9)     // Catch:{ all -> 0x00c6 }
            r7.zzhze = r8     // Catch:{ all -> 0x00c6 }
        L_0x0021:
            monitor-exit(r0)     // Catch:{ all -> 0x00c6 }
            return
        L_0x0023:
            com.google.android.gms.internal.ads.zzdwm r1 = new com.google.android.gms.internal.ads.zzdwm     // Catch:{ all -> 0x00c6 }
            r1.<init>()     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdvx$zzb$zzh$zza r9 = com.google.android.gms.internal.ads.zzdvx.zzb.zzh.zza.zzhg(r9)     // Catch:{ all -> 0x00c6 }
            r1.zzhze = r9     // Catch:{ all -> 0x00c6 }
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.ads.zzdwm> r9 = r6.zzdof     // Catch:{ all -> 0x00c6 }
            int r9 = r9.size()     // Catch:{ all -> 0x00c6 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x00c6 }
            r1.zzhyy = r9     // Catch:{ all -> 0x00c6 }
            r1.url = r7     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdwk r9 = new com.google.android.gms.internal.ads.zzdwk     // Catch:{ all -> 0x00c6 }
            r9.<init>()     // Catch:{ all -> 0x00c6 }
            r1.zzhyz = r9     // Catch:{ all -> 0x00c6 }
            java.util.HashSet<java.lang.String> r9 = r6.zzdol     // Catch:{ all -> 0x00c6 }
            int r9 = r9.size()     // Catch:{ all -> 0x00c6 }
            if (r9 <= 0) goto L_0x00bf
            if (r8 == 0) goto L_0x00bf
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ all -> 0x00c6 }
            r9.<init>()     // Catch:{ all -> 0x00c6 }
            java.util.Set r8 = r8.entrySet()     // Catch:{ all -> 0x00c6 }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ all -> 0x00c6 }
        L_0x005a:
            boolean r2 = r8.hasNext()     // Catch:{ all -> 0x00c6 }
            if (r2 == 0) goto L_0x00b2
            java.lang.Object r2 = r8.next()     // Catch:{ all -> 0x00c6 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x00c6 }
            java.lang.Object r3 = r2.getKey()     // Catch:{ all -> 0x00c6 }
            if (r3 == 0) goto L_0x0073
            java.lang.Object r3 = r2.getKey()     // Catch:{ all -> 0x00c6 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x00c6 }
            goto L_0x0075
        L_0x0073:
            java.lang.String r3 = ""
        L_0x0075:
            java.lang.Object r4 = r2.getValue()     // Catch:{ all -> 0x00c6 }
            if (r4 == 0) goto L_0x0082
            java.lang.Object r2 = r2.getValue()     // Catch:{ all -> 0x00c6 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x00c6 }
            goto L_0x0084
        L_0x0082:
            java.lang.String r2 = ""
        L_0x0084:
            java.util.Locale r4 = java.util.Locale.ENGLISH     // Catch:{ all -> 0x00c6 }
            java.lang.String r4 = r3.toLowerCase(r4)     // Catch:{ all -> 0x00c6 }
            java.util.HashSet<java.lang.String> r5 = r6.zzdol     // Catch:{ all -> 0x00c6 }
            boolean r4 = r5.contains(r4)     // Catch:{ all -> 0x00c6 }
            if (r4 == 0) goto L_0x005a
            com.google.android.gms.internal.ads.zzdvx$zzb$zzc$zza r4 = com.google.android.gms.internal.ads.zzdvx.zzb.zzc.zzbcy()     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdqk r3 = com.google.android.gms.internal.ads.zzdqk.zzhf(r3)     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdvx$zzb$zzc$zza r3 = r4.zzbk(r3)     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdqk r2 = com.google.android.gms.internal.ads.zzdqk.zzhf(r2)     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdvx$zzb$zzc$zza r2 = r3.zzbl(r2)     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdte r2 = r2.zzbaf()     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdrt r2 = (com.google.android.gms.internal.ads.zzdrt) r2     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdvx$zzb$zzc r2 = (com.google.android.gms.internal.ads.zzdvx.zzb.zzc) r2     // Catch:{ all -> 0x00c6 }
            r9.add(r2)     // Catch:{ all -> 0x00c6 }
            goto L_0x005a
        L_0x00b2:
            int r8 = r9.size()     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdvx$zzb$zzc[] r8 = new com.google.android.gms.internal.ads.zzdvx.zzb.zzc[r8]     // Catch:{ all -> 0x00c6 }
            r9.toArray(r8)     // Catch:{ all -> 0x00c6 }
            com.google.android.gms.internal.ads.zzdwk r9 = r1.zzhyz     // Catch:{ all -> 0x00c6 }
            r9.zzhyl = r8     // Catch:{ all -> 0x00c6 }
        L_0x00bf:
            java.util.LinkedHashMap<java.lang.String, com.google.android.gms.internal.ads.zzdwm> r8 = r6.zzdof     // Catch:{ all -> 0x00c6 }
            r8.put(r7, r1)     // Catch:{ all -> 0x00c6 }
            monitor-exit(r0)     // Catch:{ all -> 0x00c6 }
            return
        L_0x00c6:
            r7 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x00ca
        L_0x00c9:
            throw r7
        L_0x00ca:
            goto L_0x00c9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzatf.zza(java.lang.String, java.util.Map, int):void");
    }

    /* access modifiers changed from: package-private */
    public final void zzdw(String str) {
        synchronized (this.lock) {
            this.zzdog.add(str);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzdx(String str) {
        synchronized (this.lock) {
            this.zzdoh.add(str);
        }
    }

    public final String[] zza(String[] strArr) {
        return (String[]) this.zzdok.zzb(strArr).toArray(new String[0]);
    }

    public final void zzum() {
        this.zzdom = true;
    }

    private final zzdwm zzdy(String str) {
        zzdwm zzdwm;
        synchronized (this.lock) {
            zzdwm = this.zzdof.get(str);
        }
        return zzdwm;
    }

    public final void zzun() {
        synchronized (this.lock) {
            zzdhe zzb = zzdgs.zzb(this.zzdoi.zza(this.zzup, this.zzdof.keySet()), new zzate(this), zzazd.zzdwj);
            zzdhe zza = zzdgs.zza(zzb, 10, TimeUnit.SECONDS, zzazd.zzdwh);
            zzdgs.zza(zzb, new zzatj(this, zza), zzazd.zzdwj);
            zzdod.add(zza);
        }
    }

    private final zzdhe<Void> zzuo() {
        zzdhe<Void> zzb;
        if (!((this.zzdoj && this.zzdld.zzdoy) || (this.zzdoo && this.zzdld.zzdox) || (!this.zzdoj && this.zzdld.zzdov))) {
            return zzdgs.zzaj(null);
        }
        synchronized (this.lock) {
            this.zzdoe.zzhxw = new zzdwm[this.zzdof.size()];
            this.zzdof.values().toArray(this.zzdoe.zzhxw);
            this.zzdoe.zzhyg = (String[]) this.zzdog.toArray(new String[0]);
            this.zzdoe.zzhyh = (String[]) this.zzdoh.toArray(new String[0]);
            if (zzatp.isEnabled()) {
                String str = this.zzdoe.url;
                String str2 = this.zzdoe.zzhxx;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 53 + String.valueOf(str2).length());
                sb.append("Sending SB report\n  url: ");
                sb.append(str);
                sb.append("\n  clickUrl: ");
                sb.append(str2);
                sb.append("\n  resources: \n");
                StringBuilder sb2 = new StringBuilder(sb.toString());
                for (zzdwm zzdwm : this.zzdoe.zzhxw) {
                    sb2.append("    [");
                    sb2.append(zzdwm.zzhzf.length);
                    sb2.append("] ");
                    sb2.append(zzdwm.url);
                }
                zzatp.zzea(sb2.toString());
            }
            zzdhe<String> zza = new zzaxk(this.zzup).zza(1, this.zzdld.zzdot, null, zzdvt.zza(this.zzdoe));
            if (zzatp.isEnabled()) {
                zza.addListener(new zzati(this), zzazd.zzdwe);
            }
            zzb = zzdgs.zzb(zza, zzath.zzdoq, zzazd.zzdwj);
        }
        return zzb;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zzh(Map map) throws Exception {
        if (map != null) {
            try {
                for (String str : map.keySet()) {
                    JSONArray optJSONArray = new JSONObject((String) map.get(str)).optJSONArray("matches");
                    if (optJSONArray != null) {
                        synchronized (this.lock) {
                            int length = optJSONArray.length();
                            zzdwm zzdy = zzdy(str);
                            if (zzdy == null) {
                                String valueOf = String.valueOf(str);
                                zzatp.zzea(valueOf.length() != 0 ? "Cannot find the corresponding resource object for ".concat(valueOf) : new String("Cannot find the corresponding resource object for "));
                            } else {
                                zzdy.zzhzf = new String[length];
                                boolean z = false;
                                for (int i = 0; i < length; i++) {
                                    zzdy.zzhzf[i] = optJSONArray.getJSONObject(i).getString("threat_type");
                                }
                                boolean z2 = this.zzdoj;
                                if (length > 0) {
                                    z = true;
                                }
                                this.zzdoj = z | z2;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                if (zzabf.zzcud.get().booleanValue()) {
                    zzavs.zzb("Failed to get SafeBrowsing metadata", e);
                }
                return zzdgs.zzk(new Exception("Safebrowsing report transmission failed."));
            }
        }
        if (this.zzdoj) {
            synchronized (this.lock) {
                this.zzdoe.zzhxr = zzdvx.zzb.zzg.OCTAGON_AD_SB_MATCH;
            }
        }
        return zzuo();
    }
}
