package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdxe<T> implements zzdxg<T> {
    private zzdxp<T> zziab;

    public final T get() {
        zzdxp<T> zzdxp = this.zziab;
        if (zzdxp != null) {
            return zzdxp.get();
        }
        throw new IllegalStateException();
    }

    public static <T> void zzax(zzdxp<T> zzdxp, zzdxp<T> zzdxp2) {
        zzdxm.checkNotNull(zzdxp2);
        zzdxe zzdxe = (zzdxe) zzdxp;
        if (zzdxe.zziab == null) {
            zzdxe.zziab = zzdxp2;
            return;
        }
        throw new IllegalStateException();
    }
}
