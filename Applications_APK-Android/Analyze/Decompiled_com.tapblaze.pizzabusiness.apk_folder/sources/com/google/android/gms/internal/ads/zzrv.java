package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.internal.BaseGmsClient;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzrv implements BaseGmsClient.BaseConnectionCallbacks {
    private final /* synthetic */ zzrq zzbrh;

    zzrv(zzrq zzrq) {
        this.zzbrh = zzrq;
    }

    public final void onConnected(Bundle bundle) {
        synchronized (this.zzbrh.lock) {
            try {
                if (this.zzbrh.zzbrd != null) {
                    zzsd unused = this.zzbrh.zzbre = this.zzbrh.zzbrd.zzms();
                }
            } catch (DeadObjectException e) {
                zzavs.zzc("Unable to obtain a cache service instance.", e);
                this.zzbrh.disconnect();
            }
            this.zzbrh.lock.notifyAll();
        }
    }

    public final void onConnectionSuspended(int i) {
        synchronized (this.zzbrh.lock) {
            zzsd unused = this.zzbrh.zzbre = (zzsd) null;
            this.zzbrh.lock.notifyAll();
        }
    }
}
