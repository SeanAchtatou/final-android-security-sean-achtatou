package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcot {
    /* access modifiers changed from: private */
    public boolean zzadd = false;
    private final String zzbqz;
    private final zzcox<zzbmd> zzgdi;
    /* access modifiers changed from: private */
    public zzxa zzgdj;

    public zzcot(zzcox<zzbmd> zzcox, String str) {
        this.zzgdi = zzcox;
        this.zzbqz = str;
    }

    public final synchronized boolean isLoading() throws RemoteException {
        return this.zzgdi.isLoading();
    }

    public final synchronized void zza(zzug zzug, int i) throws RemoteException {
        this.zzgdj = null;
        this.zzadd = this.zzgdi.zza(zzug, this.zzbqz, new zzcpc(i), new zzcow(this));
    }

    public final synchronized String getMediationAdapterClassName() {
        try {
            if (this.zzgdj == null) {
                return null;
            }
            return this.zzgdj.getMediationAdapterClassName();
        } catch (RemoteException e) {
            zzavs.zze("#007 Could not call remote method.", e);
            return null;
        }
    }

    public final synchronized String zzka() {
        try {
            if (this.zzgdj == null) {
                return null;
            }
            return this.zzgdj.getMediationAdapterClassName();
        } catch (RemoteException e) {
            zzavs.zze("#007 Could not call remote method.", e);
            return null;
        }
    }
}
