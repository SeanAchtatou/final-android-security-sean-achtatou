package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzmi extends IOException {
    private final int zzbcg;

    public zzmi(int i) {
        this.zzbcg = i;
    }
}
