package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbng implements zzdxg<zzbsu<zzbpe>> {
    private final zzbnb zzfgs;
    private final zzdxp<zzbnk> zzfgt;

    private zzbng(zzbnb zzbnb, zzdxp<zzbnk> zzdxp) {
        this.zzfgs = zzbnb;
        this.zzfgt = zzdxp;
    }

    public static zzbng zzd(zzbnb zzbnb, zzdxp<zzbnk> zzdxp) {
        return new zzbng(zzbnb, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfgt.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
