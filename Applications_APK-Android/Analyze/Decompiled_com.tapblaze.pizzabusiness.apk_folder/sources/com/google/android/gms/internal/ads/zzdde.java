package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzdde extends zzgb implements zzddf {
    public static zzddf zzap(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.omid.IOmid");
        if (queryLocalInterface instanceof zzddf) {
            return (zzddf) queryLocalInterface;
        }
        return new zzddg(iBinder);
    }
}
