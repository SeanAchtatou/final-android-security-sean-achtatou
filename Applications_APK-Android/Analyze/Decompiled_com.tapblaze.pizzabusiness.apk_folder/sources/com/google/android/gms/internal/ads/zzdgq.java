package com.google.android.gms.internal.ads;

import java.util.concurrent.Future;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzdgq<V> extends zzdgr<V> {
    private final zzdhe<V> zzgwt;

    protected zzdgq(zzdhe<V> zzdhe) {
        this.zzgwt = (zzdhe) zzdei.checkNotNull(zzdhe);
    }

    /* access modifiers changed from: protected */
    public final zzdhe<V> zzarv() {
        return this.zzgwt;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Future zzaru() {
        return zzaru();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzaqw() {
        return zzaru();
    }
}
