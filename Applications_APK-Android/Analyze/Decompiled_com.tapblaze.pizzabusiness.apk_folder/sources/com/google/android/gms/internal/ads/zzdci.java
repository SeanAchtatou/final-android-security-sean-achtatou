package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzdci implements zzdgf {
    private final zzdby zzgqf;

    zzdci(zzdby zzdby) {
        this.zzgqf = zzdby;
    }

    public final zzdhe zzf(Object obj) {
        return zzdgs.zzaj(this.zzgqf.apply(obj));
    }
}
