package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzcq  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzcq extends zzfc<zzcq, zza> implements zzgn {
    private static volatile zzgv<zzcq> zzij;
    /* access modifiers changed from: private */
    public static final zzcq zzjy;
    private int zzie;
    private String zzjt = "";
    private zzcm zzju;
    private zzfm<zzch> zzjv = zzhh();
    private zzfm<zzcb> zzjw = zzhh();
    private zzfm<zzcu> zzjx = zzhh();

    private zzcq() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzcq$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzcq, zza> implements zzgn {
        private zza() {
            super(zzcq.zzjy);
        }

        public final zza zzad(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcq) this.zzqq).zzac(str);
            return this;
        }

        public final zza zzb(zzcm zzcm) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcq) this.zzqq).zza(zzcm);
            return this;
        }

        public final zza zzb(zzch zzch) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcq) this.zzqq).zza(zzch);
            return this;
        }

        public final zza zzb(zzcb zzcb) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcq) this.zzqq).zza(zzcb);
            return this;
        }

        /* synthetic */ zza(zzcp zzcp) {
            this();
        }
    }

    public final boolean zzdw() {
        return (this.zzie & 1) != 0;
    }

    public final String zzdx() {
        return this.zzjt;
    }

    /* access modifiers changed from: private */
    public final void zzac(String str) {
        str.getClass();
        this.zzie |= 1;
        this.zzjt = str;
    }

    public final boolean zzdy() {
        return (this.zzie & 2) != 0;
    }

    public final zzcm zzdz() {
        zzcm zzcm = this.zzju;
        return zzcm == null ? zzcm.zzdu() : zzcm;
    }

    /* access modifiers changed from: private */
    public final void zza(zzcm zzcm) {
        zzcm.getClass();
        this.zzju = zzcm;
        this.zzie |= 2;
    }

    public final int zzea() {
        return this.zzjv.size();
    }

    /* access modifiers changed from: private */
    public final void zza(zzch zzch) {
        zzch.getClass();
        if (!this.zzjv.zzgf()) {
            this.zzjv = zzfc.zza(this.zzjv);
        }
        this.zzjv.add(zzch);
    }

    public final int zzeb() {
        return this.zzjw.size();
    }

    /* access modifiers changed from: private */
    public final void zza(zzcb zzcb) {
        zzcb.getClass();
        if (!this.zzjw.zzgf()) {
            this.zzjw = zzfc.zza(this.zzjw);
        }
        this.zzjw.add(zzcb);
    }

    public static zza zzec() {
        return (zza) zzjy.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzcp.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzcq();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzjy, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0003\u0000\u0001\b\u0000\u0002\u001b\u0003\t\u0001\u0004\u001b\u0005\u001b", new Object[]{"zzie", "zzjt", "zzjv", zzch.class, "zzju", "zzjw", zzcb.class, "zzjx", zzcu.class});
            case 4:
                return zzjy;
            case 5:
                zzgv<zzcq> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzcq.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzjy);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzcq zzed() {
        return zzjy;
    }

    static {
        zzcq zzcq = new zzcq();
        zzjy = zzcq;
        zzfc.zza(zzcq.class, zzcq);
    }
}
