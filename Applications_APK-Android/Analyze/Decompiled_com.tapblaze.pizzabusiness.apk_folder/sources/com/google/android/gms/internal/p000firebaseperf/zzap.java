package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzap  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzap extends zzaz<Boolean> {
    private static zzap zzat;

    protected zzap() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.SdkEnabled";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_enabled";
    }

    protected static synchronized zzap zzar() {
        zzap zzap;
        synchronized (zzap.class) {
            if (zzat == null) {
                zzat = new zzap();
            }
            zzap = zzat;
        }
        return zzap;
    }
}
