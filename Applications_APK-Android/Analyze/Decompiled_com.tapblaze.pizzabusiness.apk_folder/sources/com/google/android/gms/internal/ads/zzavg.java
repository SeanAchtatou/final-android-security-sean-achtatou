package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzavg extends zzavo {
    private final /* synthetic */ zzave zzdqw;

    zzavg(zzave zzave) {
        this.zzdqw = zzave;
    }

    public final void zztu() {
        zzzr zzzr = new zzzr(this.zzdqw.zzup, this.zzdqw.zzbll.zzbma);
        synchronized (this.zzdqw.lock) {
            try {
                zzq.zzkz();
                zzzw.zza(this.zzdqw.zzdqj, zzzr);
            } catch (IllegalArgumentException e) {
                zzavs.zzd("Cannot config CSI reporter.", e);
            }
        }
    }
}
