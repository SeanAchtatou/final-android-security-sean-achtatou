package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbpl implements zzbrn {
    private final Context zzcri;

    zzbpl(Context context) {
        this.zzcri = context;
    }

    public final void zzp(Object obj) {
        ((zzbph) obj).zzbx(this.zzcri);
    }
}
