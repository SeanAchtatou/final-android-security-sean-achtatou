package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdrw extends zzdqe<Integer> implements zzdrz, zzdtq, RandomAccess {
    private static final zzdrw zzhnj;
    private int size;
    private int[] zzhnk;

    public static zzdrw zzbai() {
        return zzhnj;
    }

    zzdrw() {
        this(new int[10], 0);
    }

    private zzdrw(int[] iArr, int i) {
        this.zzhnk = iArr;
        this.size = i;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zzaxr();
        if (i2 >= i) {
            int[] iArr = this.zzhnk;
            System.arraycopy(iArr, i2, iArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdrw)) {
            return super.equals(obj);
        }
        zzdrw zzdrw = (zzdrw) obj;
        if (this.size != zzdrw.size) {
            return false;
        }
        int[] iArr = zzdrw.zzhnk;
        for (int i = 0; i < this.size; i++) {
            if (this.zzhnk[i] != iArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + this.zzhnk[i2];
        }
        return i;
    }

    /* renamed from: zzgk */
    public final zzdrz zzfd(int i) {
        if (i >= this.size) {
            return new zzdrw(Arrays.copyOf(this.zzhnk, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    public final int getInt(int i) {
        zzfb(i);
        return this.zzhnk[i];
    }

    public final int size() {
        return this.size;
    }

    public final void zzgl(int i) {
        zzaxr();
        int i2 = this.size;
        int[] iArr = this.zzhnk;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[(((i2 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.zzhnk = iArr2;
        }
        int[] iArr3 = this.zzhnk;
        int i3 = this.size;
        this.size = i3 + 1;
        iArr3[i3] = i;
    }

    public final boolean addAll(Collection<? extends Integer> collection) {
        zzaxr();
        zzdrv.checkNotNull(collection);
        if (!(collection instanceof zzdrw)) {
            return super.addAll(collection);
        }
        zzdrw zzdrw = (zzdrw) collection;
        int i = zzdrw.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.zzhnk;
            if (i3 > iArr.length) {
                this.zzhnk = Arrays.copyOf(iArr, i3);
            }
            System.arraycopy(zzdrw.zzhnk, 0, this.zzhnk, this.size, zzdrw.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean remove(Object obj) {
        zzaxr();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Integer.valueOf(this.zzhnk[i]))) {
                int[] iArr = this.zzhnk;
                System.arraycopy(iArr, i + 1, iArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    private final void zzfb(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
    }

    private final String zzfc(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        int intValue = ((Integer) obj).intValue();
        zzaxr();
        zzfb(i);
        int[] iArr = this.zzhnk;
        int i2 = iArr[i];
        iArr[i] = intValue;
        return Integer.valueOf(i2);
    }

    public final /* synthetic */ Object remove(int i) {
        zzaxr();
        zzfb(i);
        int[] iArr = this.zzhnk;
        int i2 = iArr[i];
        int i3 = this.size;
        if (i < i3 - 1) {
            System.arraycopy(iArr, i + 1, iArr, i, (i3 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Integer.valueOf(i2);
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        int intValue = ((Integer) obj).intValue();
        zzaxr();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
        int[] iArr = this.zzhnk;
        if (i2 < iArr.length) {
            System.arraycopy(iArr, i, iArr, i + 1, i2 - i);
        } else {
            int[] iArr2 = new int[(((i2 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i);
            System.arraycopy(this.zzhnk, i, iArr2, i + 1, this.size - i);
            this.zzhnk = iArr2;
        }
        this.zzhnk[i] = intValue;
        this.size++;
        this.modCount++;
    }

    public final /* synthetic */ boolean add(Object obj) {
        zzgl(((Integer) obj).intValue());
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        return Integer.valueOf(getInt(i));
    }

    static {
        zzdrw zzdrw = new zzdrw(new int[0], 0);
        zzhnj = zzdrw;
        zzdrw.zzaxq();
    }
}
