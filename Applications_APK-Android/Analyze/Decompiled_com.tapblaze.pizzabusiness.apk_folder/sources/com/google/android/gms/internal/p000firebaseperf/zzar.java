package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzar  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzar extends zzaz<Long> {
    private static zzar zzav;

    private zzar() {
    }

    /* access modifiers changed from: protected */
    public final String zzaf() {
        return "sessions_cpu_capture_frequency_fg_ms";
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.SessionsCpuCaptureFrequencyForegroundMs";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_session_gauge_cpu_capture_frequency_fg_ms";
    }

    public static synchronized zzar zzat() {
        zzar zzar;
        synchronized (zzar.class) {
            if (zzav == null) {
                zzav = new zzar();
            }
            zzar = zzav;
        }
        return zzar;
    }
}
