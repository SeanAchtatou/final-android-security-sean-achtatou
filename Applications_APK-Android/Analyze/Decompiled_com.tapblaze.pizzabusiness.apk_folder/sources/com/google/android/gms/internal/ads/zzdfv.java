package com.google.android.gms.internal.ads;

import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzdfv extends AbstractExecutorService implements zzdhd {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdhs.zza(java.lang.Runnable, java.lang.Object):com.google.android.gms.internal.ads.zzdhs<V>
     arg types: [java.lang.Runnable, T]
     candidates:
      com.google.android.gms.internal.ads.zzdgn.zza(com.google.android.gms.internal.ads.zzded, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzdgn<T>
      com.google.android.gms.internal.ads.zzdfs.zza(com.google.android.gms.internal.ads.zzdfs, com.google.android.gms.internal.ads.zzdfs$zzd):com.google.android.gms.internal.ads.zzdfs$zzd
      com.google.android.gms.internal.ads.zzdfs.zza(com.google.android.gms.internal.ads.zzdfs, com.google.android.gms.internal.ads.zzdfs$zzk):com.google.android.gms.internal.ads.zzdfs$zzk
      com.google.android.gms.internal.ads.zzdfs.zza(com.google.android.gms.internal.ads.zzdfs, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.ads.zzdfs.zza(java.lang.Runnable, java.util.concurrent.Executor):void
      com.google.android.gms.internal.ads.zzdhs.zza(java.lang.Runnable, java.lang.Object):com.google.android.gms.internal.ads.zzdhs<V> */
    /* access modifiers changed from: protected */
    public final <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return zzdhs.zza(runnable, (Object) t);
    }

    /* access modifiers changed from: protected */
    public final <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return zzdhs.zze(callable);
    }

    /* renamed from: zzf */
    public final zzdhe<?> submit(Runnable runnable) {
        return (zzdhe) super.submit(runnable);
    }

    /* renamed from: zzd */
    public final <T> zzdhe<T> submit(Callable<T> callable) {
        return (zzdhe) super.submit(callable);
    }

    public /* synthetic */ Future submit(Runnable runnable, @NullableDecl Object obj) {
        return (zzdhe) super.submit(runnable, obj);
    }
}
