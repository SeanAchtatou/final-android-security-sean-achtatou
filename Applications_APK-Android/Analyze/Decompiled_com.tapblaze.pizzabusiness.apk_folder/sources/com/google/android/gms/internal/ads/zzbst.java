package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbst extends zzbrl<zzps> implements zzps {
    private final zzczl zzffc;
    private Map<View, zzpo> zzfiq = new WeakHashMap(1);
    private final Context zzup;

    public zzbst(Context context, Set<zzbsu<zzps>> set, zzczl zzczl) {
        super(set);
        this.zzup = context;
        this.zzffc = zzczl;
    }

    public final synchronized void zza(zzpt zzpt) {
        zza(new zzbsw(zzpt));
    }

    public final synchronized void zzq(View view) {
        zzpo zzpo = this.zzfiq.get(view);
        if (zzpo == null) {
            zzpo = new zzpo(this.zzup, view);
            zzpo.zza(this);
            this.zzfiq.put(view, zzpo);
        }
        if (this.zzffc != null && this.zzffc.zzdlm) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzckj)).booleanValue()) {
                zzpo.zzen(((Long) zzve.zzoy().zzd(zzzn.zzcki)).longValue());
                return;
            }
        }
        zzpo.zzlp();
    }

    public final synchronized void zzr(View view) {
        if (this.zzfiq.containsKey(view)) {
            this.zzfiq.get(view).zzb(this);
            this.zzfiq.remove(view);
        }
    }
}
