package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.ArrayList;
import java.util.WeakHashMap;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzpp {
    private final Object lock = new Object();
    private final zzazb zzbll;
    private final WeakHashMap<Object, Object> zzbnt = new WeakHashMap<>();
    private final ArrayList<Object> zzbnu = new ArrayList<>();
    private final zzais zzbnv;
    private final Context zzyv;

    public zzpp(Context context, zzazb zzazb) {
        this.zzyv = context.getApplicationContext();
        this.zzbll = zzazb;
        this.zzbnv = new zzais(context.getApplicationContext(), zzazb, (String) zzve.zzoy().zzd(zzzn.zzcgi));
    }
}
