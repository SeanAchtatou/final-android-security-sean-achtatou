package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdmv extends zzdrt<zzdmv, zza> implements zzdtg {
    private static volatile zzdtn<zzdmv> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmv zzhco;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;
    private zzdmz zzhcn;

    private zzdmv() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdmv, zza> implements zzdtg {
        private zza() {
            super(zzdmv.zzhco);
        }

        public final zza zzen(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmv) this.zzhmp).setVersion(0);
            return this;
        }

        public final zza zzd(zzdmz zzdmz) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmv) this.zzhmp).zzc(zzdmz);
            return this;
        }

        public final zza zzau(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmv) this.zzhmp).zzs(zzdqk);
            return this;
        }

        /* synthetic */ zza(zzdmu zzdmu) {
            this();
        }
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public final zzdmz zzauz() {
        zzdmz zzdmz = this.zzhcn;
        return zzdmz == null ? zzdmz.zzavg() : zzdmz;
    }

    /* access modifiers changed from: private */
    public final void zzc(zzdmz zzdmz) {
        zzdmz.getClass();
        this.zzhcn = zzdmz;
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public static zzdmv zzas(zzdqk zzdqk) throws zzdse {
        return (zzdmv) zzdrt.zza(zzhco, zzdqk);
    }

    public static zza zzava() {
        return (zza) zzhco.zzazt();
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmu.zzdk[i - 1]) {
            case 1:
                return new zzdmv();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhco, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n", new Object[]{"zzhaa", "zzhcn", "zzhab"});
            case 4:
                return zzhco;
            case 5:
                zzdtn<zzdmv> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmv.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhco);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdmv zzavb() {
        return zzhco;
    }

    static {
        zzdmv zzdmv = new zzdmv();
        zzhco = zzdmv;
        zzdrt.zza(zzdmv.class, zzdmv);
    }
}
