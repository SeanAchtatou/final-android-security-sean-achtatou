package com.google.android.gms.internal.p000firebaseperf;

import java.util.Iterator;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhk  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhk {
    /* access modifiers changed from: private */
    public static final Iterator<Object> zzuk = new zzhj();
    private static final Iterable<Object> zzul = new zzhm();

    static <T> Iterable<T> zzjc() {
        return zzul;
    }
}
