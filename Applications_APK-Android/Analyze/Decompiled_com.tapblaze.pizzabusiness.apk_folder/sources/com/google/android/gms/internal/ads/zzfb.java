package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzfb extends zzfw {
    private long startTime;

    public zzfb(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, long j, int i, int i2) {
        super(zzei, str, str2, zzb, i, 25);
        this.startTime = j;
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        long longValue = ((Long) this.zzaae.invoke(null, new Object[0])).longValue();
        synchronized (this.zzzt) {
            this.zzzt.zzbr(longValue);
            if (this.startTime != 0) {
                this.zzzt.zzat(longValue - this.startTime);
                this.zzzt.zzaw(this.startTime);
            }
        }
    }
}
