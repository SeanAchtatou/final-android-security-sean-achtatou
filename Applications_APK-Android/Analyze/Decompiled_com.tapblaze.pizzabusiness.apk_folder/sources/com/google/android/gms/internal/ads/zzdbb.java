package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzdbb {
    zzdbi<?> zza(zzdbl zzdbl);

    zzdbl zza(zzug zzug, String str, zzuo zzuo);

    boolean zza(zzdbl zzdbl, zzdbi<?> zzdbi);

    boolean zzb(zzdbl zzdbl);
}
