package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzadb extends NativeContentAd {
    private final VideoController zzcel = new VideoController();
    private final List<NativeAd.Image> zzcvv = new ArrayList();
    private final NativeAd.AdChoicesInfo zzcvx;
    private final zzada zzcvy;
    private final zzacj zzcvz;

    /* JADX WARN: Type inference failed for: r3v3, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0078 A[Catch:{ RemoteException -> 0x0085 }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzadb(com.google.android.gms.internal.ads.zzada r6) {
        /*
            r5 = this;
            java.lang.String r0 = ""
            r5.<init>()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r5.zzcvv = r1
            com.google.android.gms.ads.VideoController r1 = new com.google.android.gms.ads.VideoController
            r1.<init>()
            r5.zzcel = r1
            r5.zzcvy = r6
            r6 = 0
            com.google.android.gms.internal.ads.zzada r1 = r5.zzcvy     // Catch:{ RemoteException -> 0x0057 }
            java.util.List r1 = r1.getImages()     // Catch:{ RemoteException -> 0x0057 }
            if (r1 == 0) goto L_0x005b
            java.util.Iterator r1 = r1.iterator()     // Catch:{ RemoteException -> 0x0057 }
        L_0x0022:
            boolean r2 = r1.hasNext()     // Catch:{ RemoteException -> 0x0057 }
            if (r2 == 0) goto L_0x005b
            java.lang.Object r2 = r1.next()     // Catch:{ RemoteException -> 0x0057 }
            boolean r3 = r2 instanceof android.os.IBinder     // Catch:{ RemoteException -> 0x0057 }
            if (r3 == 0) goto L_0x0049
            android.os.IBinder r2 = (android.os.IBinder) r2     // Catch:{ RemoteException -> 0x0057 }
            if (r2 == 0) goto L_0x0049
            java.lang.String r3 = "com.google.android.gms.ads.internal.formats.client.INativeAdImage"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)     // Catch:{ RemoteException -> 0x0057 }
            boolean r4 = r3 instanceof com.google.android.gms.internal.ads.zzaci     // Catch:{ RemoteException -> 0x0057 }
            if (r4 == 0) goto L_0x0042
            r2 = r3
            com.google.android.gms.internal.ads.zzaci r2 = (com.google.android.gms.internal.ads.zzaci) r2     // Catch:{ RemoteException -> 0x0057 }
            goto L_0x004a
        L_0x0042:
            com.google.android.gms.internal.ads.zzack r3 = new com.google.android.gms.internal.ads.zzack     // Catch:{ RemoteException -> 0x0057 }
            r3.<init>(r2)     // Catch:{ RemoteException -> 0x0057 }
            r2 = r3
            goto L_0x004a
        L_0x0049:
            r2 = r6
        L_0x004a:
            if (r2 == 0) goto L_0x0022
            java.util.List<com.google.android.gms.ads.formats.NativeAd$Image> r3 = r5.zzcvv     // Catch:{ RemoteException -> 0x0057 }
            com.google.android.gms.internal.ads.zzacj r4 = new com.google.android.gms.internal.ads.zzacj     // Catch:{ RemoteException -> 0x0057 }
            r4.<init>(r2)     // Catch:{ RemoteException -> 0x0057 }
            r3.add(r4)     // Catch:{ RemoteException -> 0x0057 }
            goto L_0x0022
        L_0x0057:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzayu.zzc(r0, r1)
        L_0x005b:
            com.google.android.gms.internal.ads.zzada r1 = r5.zzcvy     // Catch:{ RemoteException -> 0x0069 }
            com.google.android.gms.internal.ads.zzaci r1 = r1.zzrj()     // Catch:{ RemoteException -> 0x0069 }
            if (r1 == 0) goto L_0x006d
            com.google.android.gms.internal.ads.zzacj r2 = new com.google.android.gms.internal.ads.zzacj     // Catch:{ RemoteException -> 0x0069 }
            r2.<init>(r1)     // Catch:{ RemoteException -> 0x0069 }
            goto L_0x006e
        L_0x0069:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzayu.zzc(r0, r1)
        L_0x006d:
            r2 = r6
        L_0x006e:
            r5.zzcvz = r2
            com.google.android.gms.internal.ads.zzada r1 = r5.zzcvy     // Catch:{ RemoteException -> 0x0085 }
            com.google.android.gms.internal.ads.zzaca r1 = r1.zzrh()     // Catch:{ RemoteException -> 0x0085 }
            if (r1 == 0) goto L_0x0089
            com.google.android.gms.internal.ads.zzacb r1 = new com.google.android.gms.internal.ads.zzacb     // Catch:{ RemoteException -> 0x0085 }
            com.google.android.gms.internal.ads.zzada r2 = r5.zzcvy     // Catch:{ RemoteException -> 0x0085 }
            com.google.android.gms.internal.ads.zzaca r2 = r2.zzrh()     // Catch:{ RemoteException -> 0x0085 }
            r1.<init>(r2)     // Catch:{ RemoteException -> 0x0085 }
            r6 = r1
            goto L_0x0089
        L_0x0085:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzayu.zzc(r0, r1)
        L_0x0089:
            r5.zzcvx = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzadb.<init>(com.google.android.gms.internal.ads.zzada):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: zzrf */
    public final IObjectWrapper zzjj() {
        try {
            return this.zzcvy.zzrf();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final void performClick(Bundle bundle) {
        try {
            this.zzcvy.performClick(bundle);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final boolean recordImpression(Bundle bundle) {
        try {
            return this.zzcvy.recordImpression(bundle);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return false;
        }
    }

    public final void reportTouchEvent(Bundle bundle) {
        try {
            this.zzcvy.reportTouchEvent(bundle);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final CharSequence getHeadline() {
        try {
            return this.zzcvy.getHeadline();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final List<NativeAd.Image> getImages() {
        return this.zzcvv;
    }

    public final CharSequence getBody() {
        try {
            return this.zzcvy.getBody();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final NativeAd.Image getLogo() {
        return this.zzcvz;
    }

    public final CharSequence getCallToAction() {
        try {
            return this.zzcvy.getCallToAction();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final CharSequence getAdvertiser() {
        try {
            return this.zzcvy.getAdvertiser();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final VideoController getVideoController() {
        try {
            if (this.zzcvy.getVideoController() != null) {
                this.zzcel.zza(this.zzcvy.getVideoController());
            }
        } catch (RemoteException e) {
            zzayu.zzc("Exception occurred while getting video controller", e);
        }
        return this.zzcel;
    }

    public final Bundle getExtras() {
        try {
            return this.zzcvy.getExtras();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final NativeAd.AdChoicesInfo getAdChoicesInfo() {
        return this.zzcvx;
    }

    public final CharSequence getMediationAdapterClassName() {
        try {
            return this.zzcvy.getMediationAdapterClassName();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final void destroy() {
        try {
            this.zzcvy.destroy();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
