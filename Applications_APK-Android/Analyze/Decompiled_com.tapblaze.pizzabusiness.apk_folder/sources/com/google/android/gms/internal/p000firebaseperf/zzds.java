package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzds  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzds extends zzfc<zzds, zza> implements zzgn {
    private static volatile zzgv<zzds> zzij;
    /* access modifiers changed from: private */
    public static final zzds zzmn;
    private int zzie;
    private String zzig = "";
    private String zzmj = "";
    private int zzmk;
    private int zzml;
    private int zzmm;

    private zzds() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzds$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzds, zza> implements zzgn {
        private zza() {
            super(zzds.zzmn);
        }

        /* synthetic */ zza(zzdr zzdr) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzdr.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzds();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzmn, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001\u0003\f\u0002\u0004\f\u0003\u0005\f\u0004", new Object[]{"zzie", "zzig", "zzmj", "zzmk", zzdh.zzdp(), "zzml", zzdo.zzdp(), "zzmm", zzcl.zzdp()});
            case 4:
                return zzmn;
            case 5:
                zzgv<zzds> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzds.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzmn);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzds zzds = new zzds();
        zzmn = zzds;
        zzfc.zza(zzds.class, zzds);
    }
}
