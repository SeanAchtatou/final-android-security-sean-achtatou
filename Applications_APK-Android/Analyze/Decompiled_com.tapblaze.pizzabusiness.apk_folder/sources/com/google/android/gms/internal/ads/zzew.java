package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.view.View;
import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzew extends zzfw {
    private final Activity zzzk;
    private final View zzzl;

    public zzew(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2, View view, Activity activity) {
        super(zzei, str, str2, zzb, i, 62);
        this.zzzl = view;
        this.zzzk = activity;
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        if (this.zzzl != null) {
            boolean booleanValue = ((Boolean) zzve.zzoy().zzd(zzzn.zzclb)).booleanValue();
            Object[] objArr = (Object[]) this.zzaae.invoke(null, this.zzzl, this.zzzk, Boolean.valueOf(booleanValue));
            synchronized (this.zzzt) {
                this.zzzt.zzbp(((Long) objArr[0]).longValue());
                this.zzzt.zzbq(((Long) objArr[1]).longValue());
                if (booleanValue) {
                    this.zzzt.zzam((String) objArr[2]);
                }
            }
        }
    }
}
