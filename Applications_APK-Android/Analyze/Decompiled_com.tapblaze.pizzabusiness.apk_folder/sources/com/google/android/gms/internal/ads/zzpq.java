package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzpq extends BroadcastReceiver {
    private final /* synthetic */ zzpo zzbnw;

    zzpq(zzpo zzpo) {
        this.zzbnw = zzpo;
    }

    public final void onReceive(Context context, Intent intent) {
        this.zzbnw.zzbn(3);
    }
}
