package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfa  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
enum zzfa {
    SCALAR(false),
    VECTOR(true),
    PACKED_VECTOR(true),
    MAP(false);
    
    private final boolean zzqh;

    private zzfa(boolean z) {
        this.zzqh = z;
    }
}
