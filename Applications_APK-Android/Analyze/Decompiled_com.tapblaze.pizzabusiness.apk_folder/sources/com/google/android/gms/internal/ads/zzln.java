package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzln implements Parcelable.Creator<zzlo> {
    zzln() {
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzlo[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zzlo(parcel);
    }
}
