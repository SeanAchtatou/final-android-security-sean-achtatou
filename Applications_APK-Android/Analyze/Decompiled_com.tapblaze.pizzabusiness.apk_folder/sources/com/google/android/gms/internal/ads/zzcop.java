package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcop implements zzbov, zzbow, zzbpe, zzbqb, zzty {
    private zzvh zzgdc;

    public final void onRewardedVideoCompleted() {
    }

    public final void onRewardedVideoStarted() {
    }

    public final void zzb(zzare zzare, String str, String str2) {
    }

    public final synchronized void zzc(zzvh zzvh) {
        this.zzgdc = zzvh;
    }

    public final synchronized zzvh zzamo() {
        return this.zzgdc;
    }

    public final synchronized void onAdClosed() {
        if (this.zzgdc != null) {
            try {
                this.zzgdc.onAdClosed();
            } catch (RemoteException e) {
                zzavs.zzd("Remote Exception at onAdClosed.", e);
            }
        }
    }

    public final synchronized void onAdFailedToLoad(int i) {
        if (this.zzgdc != null) {
            try {
                this.zzgdc.onAdFailedToLoad(i);
            } catch (RemoteException e) {
                zzavs.zzd("Remote Exception at onAdFailedToLoad.", e);
            }
        }
    }

    public final synchronized void onAdLeftApplication() {
        if (this.zzgdc != null) {
            try {
                this.zzgdc.onAdLeftApplication();
            } catch (RemoteException e) {
                zzavs.zzd("Remote Exception at onAdLeftApplication.", e);
            }
        }
    }

    public final synchronized void onAdLoaded() {
        if (this.zzgdc != null) {
            try {
                this.zzgdc.onAdLoaded();
            } catch (RemoteException e) {
                zzavs.zzd("Remote Exception at onAdLoaded.", e);
            }
        }
    }

    public final synchronized void onAdOpened() {
        if (this.zzgdc != null) {
            try {
                this.zzgdc.onAdOpened();
            } catch (RemoteException e) {
                zzavs.zzd("Remote Exception at onAdOpened.", e);
            }
        }
    }

    public final synchronized void onAdClicked() {
        if (this.zzgdc != null) {
            try {
                this.zzgdc.onAdClicked();
            } catch (RemoteException e) {
                zzavs.zzd("Remote Exception at onAdClicked.", e);
            }
        }
    }

    public final synchronized void onAdImpression() {
        if (this.zzgdc != null) {
            try {
                this.zzgdc.onAdImpression();
            } catch (RemoteException e) {
                zzavs.zzd("Remote Exception at onAdImpression.", e);
            }
        }
    }
}
