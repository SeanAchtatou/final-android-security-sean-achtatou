package com.google.android.gms.internal.ads;

import android.content.Context;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzakc {
    private static final zzaxh<zzaif> zzdat = new zzakb();
    private static final zzaxh<zzaif> zzdau = new zzake();
    private final zzais zzdav;

    public zzakc(Context context, zzazb zzazb, String str) {
        this.zzdav = new zzais(context, zzazb, str, zzdat, zzdau);
    }

    public final <I, O> zzaju<I, O> zza(String str, zzajv<I> zzajv, zzajw<O> zzajw) {
        return new zzakd(this.zzdav, str, zzajv, zzajw);
    }

    public final zzakh zzsh() {
        return new zzakh(this.zzdav);
    }
}
