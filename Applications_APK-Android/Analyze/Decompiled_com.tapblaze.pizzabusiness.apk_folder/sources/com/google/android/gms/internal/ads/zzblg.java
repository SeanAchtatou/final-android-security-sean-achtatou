package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzblg {
    public abstract zzbkj zza(zzbmt zzbmt, zzbkn zzbkn);

    public abstract zzblp zza(zzbmt zzbmt, zzbls zzbls);

    public abstract zzbmz<zzbkk> zzadc();

    public abstract zzbou zzadd();

    public abstract zzbmi<zzbkk> zzaed();
}
