package com.google.android.gms.internal.ads;

import android.media.MediaCodecInfo;
import android.media.MediaCodecList;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzlf implements zzld {
    private final int zzazo;
    private MediaCodecInfo[] zzazp;

    public zzlf(boolean z) {
        this.zzazo = z ? 1 : 0;
    }

    public final boolean zzhd() {
        return true;
    }

    public final int getCodecCount() {
        zzhe();
        return this.zzazp.length;
    }

    public final MediaCodecInfo getCodecInfoAt(int i) {
        zzhe();
        return this.zzazp[i];
    }

    public final boolean zza(String str, MediaCodecInfo.CodecCapabilities codecCapabilities) {
        return codecCapabilities.isFeatureSupported("secure-playback");
    }

    private final void zzhe() {
        if (this.zzazp == null) {
            this.zzazp = new MediaCodecList(this.zzazo).getCodecInfos();
        }
    }
}
