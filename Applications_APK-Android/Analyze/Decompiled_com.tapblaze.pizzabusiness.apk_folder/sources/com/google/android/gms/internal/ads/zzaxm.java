package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaxm implements zzy {
    private final /* synthetic */ String zzdug;
    private final /* synthetic */ zzaxr zzduh;

    zzaxm(zzaxk zzaxk, String str, zzaxr zzaxr) {
        this.zzdug = str;
        this.zzduh = zzaxr;
    }

    public final void zzc(zzae zzae) {
        String str = this.zzdug;
        String zzae2 = zzae.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(zzae2).length());
        sb.append("Failed to load URL: ");
        sb.append(str);
        sb.append("\n");
        sb.append(zzae2);
        zzavs.zzez(sb.toString());
        this.zzduh.zzb(null);
    }
}
