package com.google.android.gms.internal.ads;

import android.webkit.JavascriptInterface;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzahy {
    private final zzaib zzcyt;

    private zzahy(zzaib zzaib) {
        this.zzcyt = zzaib;
    }

    @JavascriptInterface
    public final void notify(String str) {
        this.zzcyt.zzdc(str);
    }
}
