package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzcio<AdT> {
    boolean zza(zzczt zzczt, zzczl zzczl);

    zzdhe<AdT> zzb(zzczt zzczt, zzczl zzczl);
}
