package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzhf extends zzgm {
    void disable();

    int getState();

    int getTrackType();

    boolean isReady();

    void setIndex(int i);

    void start() throws zzgl;

    void stop() throws zzgl;

    void zza(zzhh zzhh, zzgw[] zzgwArr, zzmo zzmo, long j, boolean z, long j2) throws zzgl;

    void zza(zzgw[] zzgwArr, zzmo zzmo, long j) throws zzgl;

    void zzb(long j, long j2) throws zzgl;

    void zzdo(long j) throws zzgl;

    zzhe zzdp();

    zzog zzdq();

    zzmo zzdr();

    boolean zzds();

    void zzdt();

    boolean zzdu();

    void zzdv() throws IOException;

    boolean zzeu();
}
