package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzbww {
    protected final zzczl zzfmp;

    public zzbww(zzczl zzczl) {
        this.zzfmp = zzczl;
    }

    public JSONObject zzajl() {
        return null;
    }

    public boolean zzajm() {
        return false;
    }

    public boolean zzajn() {
        return this.zzfmp.zzdce;
    }

    public boolean zzajo() {
        return this.zzfmp.zzdcd;
    }

    public boolean zzaiw() {
        return this.zzfmp.zzdli;
    }
}
