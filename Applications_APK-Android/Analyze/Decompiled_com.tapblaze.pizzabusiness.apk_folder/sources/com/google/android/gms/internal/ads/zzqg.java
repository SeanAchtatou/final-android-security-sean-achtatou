package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzqg implements Runnable {
    private final /* synthetic */ zzqh zzbpd;

    zzqg(zzqh zzqh) {
        this.zzbpd = zzqh;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzqh.zza(com.google.android.gms.internal.ads.zzqh, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzqh, int]
     candidates:
      com.google.android.gms.internal.ads.zzqh.zza(android.app.Application, android.content.Context):void
      com.google.android.gms.internal.ads.zzqh.zza(com.google.android.gms.internal.ads.zzqh, boolean):boolean */
    public final void run() {
        synchronized (this.zzbpd.lock) {
            if (!this.zzbpd.foreground || !this.zzbpd.zzbpe) {
                zzavs.zzea("App is still foreground");
            } else {
                boolean unused = this.zzbpd.foreground = false;
                zzavs.zzea("App went background");
                for (zzqj zzp : this.zzbpd.zzbpf) {
                    try {
                        zzp.zzp(false);
                    } catch (Exception e) {
                        zzayu.zzc("", e);
                    }
                }
            }
        }
    }
}
