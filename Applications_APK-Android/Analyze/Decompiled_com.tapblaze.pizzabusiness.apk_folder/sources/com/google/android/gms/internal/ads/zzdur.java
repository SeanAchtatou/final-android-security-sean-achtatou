package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;
import com.ironsource.mediationsdk.logger.IronSourceError;
import java.io.IOException;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzdur {
    private static final zzdur zzhrd = new zzdur(0, new int[0], new Object[0], false);
    private int count;
    private boolean zzhhm;
    private int zzhml;
    private Object[] zzhoy;
    private int[] zzhre;

    public static zzdur zzbcf() {
        return zzhrd;
    }

    static zzdur zzbcg() {
        return new zzdur();
    }

    static zzdur zza(zzdur zzdur, zzdur zzdur2) {
        int i = zzdur.count + zzdur2.count;
        int[] copyOf = Arrays.copyOf(zzdur.zzhre, i);
        System.arraycopy(zzdur2.zzhre, 0, copyOf, zzdur.count, zzdur2.count);
        Object[] copyOf2 = Arrays.copyOf(zzdur.zzhoy, i);
        System.arraycopy(zzdur2.zzhoy, 0, copyOf2, zzdur.count, zzdur2.count);
        return new zzdur(i, copyOf, copyOf2, true);
    }

    private zzdur() {
        this(0, new int[8], new Object[8], true);
    }

    private zzdur(int i, int[] iArr, Object[] objArr, boolean z) {
        this.zzhml = -1;
        this.count = i;
        this.zzhre = iArr;
        this.zzhoy = objArr;
        this.zzhhm = z;
    }

    public final void zzaxq() {
        this.zzhhm = false;
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdvl zzdvl) throws IOException {
        if (zzdvl.zzazg() == zzdrt.zze.zzhne) {
            for (int i = this.count - 1; i >= 0; i--) {
                zzdvl.zzc(this.zzhre[i] >>> 3, this.zzhoy[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.count; i2++) {
            zzdvl.zzc(this.zzhre[i2] >>> 3, this.zzhoy[i2]);
        }
    }

    public final void zzb(zzdvl zzdvl) throws IOException {
        if (this.count != 0) {
            if (zzdvl.zzazg() == zzdrt.zze.zzhnd) {
                for (int i = 0; i < this.count; i++) {
                    zzb(this.zzhre[i], this.zzhoy[i], zzdvl);
                }
                return;
            }
            for (int i2 = this.count - 1; i2 >= 0; i2--) {
                zzb(this.zzhre[i2], this.zzhoy[i2], zzdvl);
            }
        }
    }

    private static void zzb(int i, Object obj, zzdvl zzdvl) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            zzdvl.zzo(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            zzdvl.zzi(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            zzdvl.zza(i2, (zzdqk) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                zzdvl.zzae(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(zzdse.zzbao());
        } else if (zzdvl.zzazg() == zzdrt.zze.zzhnd) {
            zzdvl.zzgi(i2);
            ((zzdur) obj).zzb(zzdvl);
            zzdvl.zzgj(i2);
        } else {
            zzdvl.zzgj(i2);
            ((zzdur) obj).zzb(zzdvl);
            zzdvl.zzgi(i2);
        }
    }

    public final int zzbch() {
        int i = this.zzhml;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.count; i3++) {
            i2 += zzdrb.zzd(this.zzhre[i3] >>> 3, (zzdqk) this.zzhoy[i3]);
        }
        this.zzhml = i2;
        return i2;
    }

    public final int zzazu() {
        int i;
        int i2 = this.zzhml;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.count; i4++) {
            int i5 = this.zzhre[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 == 0) {
                i = zzdrb.zzk(i6, ((Long) this.zzhoy[i4]).longValue());
            } else if (i7 == 1) {
                i = zzdrb.zzm(i6, ((Long) this.zzhoy[i4]).longValue());
            } else if (i7 == 2) {
                i = zzdrb.zzc(i6, (zzdqk) this.zzhoy[i4]);
            } else if (i7 == 3) {
                i = (zzdrb.zzfz(i6) << 1) + ((zzdur) this.zzhoy[i4]).zzazu();
            } else if (i7 == 5) {
                i = zzdrb.zzai(i6, ((Integer) this.zzhoy[i4]).intValue());
            } else {
                throw new IllegalStateException(zzdse.zzbao());
            }
            i3 += i;
        }
        this.zzhml = i3;
        return i3;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof zzdur)) {
            return false;
        }
        zzdur zzdur = (zzdur) obj;
        int i = this.count;
        if (i == zzdur.count) {
            int[] iArr = this.zzhre;
            int[] iArr2 = zzdur.zzhre;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.zzhoy;
                Object[] objArr2 = zzdur.zzhoy;
                int i3 = this.count;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                return z2;
            }
        }
    }

    public final int hashCode() {
        int i = this.count;
        int i2 = (i + IronSourceError.ERROR_NON_EXISTENT_INSTANCE) * 31;
        int[] iArr = this.zzhre;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.zzhoy;
        int i7 = this.count;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    /* access modifiers changed from: package-private */
    public final void zza(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.count; i2++) {
            zzdtf.zza(sb, i, String.valueOf(this.zzhre[i2] >>> 3), this.zzhoy[i2]);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzd(int i, Object obj) {
        if (this.zzhhm) {
            int i2 = this.count;
            if (i2 == this.zzhre.length) {
                int i3 = this.count + (i2 < 4 ? 8 : i2 >> 1);
                this.zzhre = Arrays.copyOf(this.zzhre, i3);
                this.zzhoy = Arrays.copyOf(this.zzhoy, i3);
            }
            int[] iArr = this.zzhre;
            int i4 = this.count;
            iArr[i4] = i;
            this.zzhoy[i4] = obj;
            this.count = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }
}
