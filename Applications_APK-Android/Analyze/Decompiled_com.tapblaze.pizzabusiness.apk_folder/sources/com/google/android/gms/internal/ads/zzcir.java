package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbpu;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzcir<AdT, AdapterT, ListenerT extends zzbpu> {
    void zza(zzczt zzczt, zzczl zzczl, zzcip<AdapterT, ListenerT> zzcip) throws zzdab;

    AdT zzb(zzczt zzczt, zzczl zzczl, zzcip<AdapterT, ListenerT> zzcip) throws zzdab, zzclr;
}
