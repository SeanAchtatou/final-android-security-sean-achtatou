package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdhg {
    public static Executor zzarw() {
        return zzdgl.INSTANCE;
    }

    public static zzdhd zza(ExecutorService executorService) {
        if (executorService instanceof zzdhd) {
            return (zzdhd) executorService;
        }
        if (executorService instanceof ScheduledExecutorService) {
            return new zzdhk((ScheduledExecutorService) executorService);
        }
        return new zzdhh(executorService);
    }

    static Executor zza(Executor executor, zzdfs<?> zzdfs) {
        zzdei.checkNotNull(executor);
        zzdei.checkNotNull(zzdfs);
        if (executor == zzdgl.INSTANCE) {
            return executor;
        }
        return new zzdhf(executor, zzdfs);
    }
}
