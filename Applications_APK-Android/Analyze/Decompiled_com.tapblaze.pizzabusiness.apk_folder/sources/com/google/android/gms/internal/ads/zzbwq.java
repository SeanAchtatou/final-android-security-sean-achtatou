package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbwq {
    private zzacd zzcwg;

    public zzbwq(zzbwi zzbwi) {
        this.zzcwg = zzbwi;
    }

    public final synchronized zzacd zzrq() {
        return this.zzcwg;
    }

    public final synchronized void zza(zzacd zzacd) {
        this.zzcwg = zzacd;
    }
}
