package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Notifications;
import com.google.android.gms.games.internal.zzg;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class zzbp implements Notifications {
    public final void clearAll(GoogleApiClient googleApiClient) {
        clear(googleApiClient, Notifications.NOTIFICATION_TYPES_ALL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.Games.zza(com.google.android.gms.common.api.GoogleApiClient, boolean):com.google.android.gms.games.internal.zzg
     arg types: [com.google.android.gms.common.api.GoogleApiClient, int]
     candidates:
      com.google.android.gms.games.Games.zza(com.google.android.gms.games.Games$GamesOptions, com.google.android.gms.auth.api.signin.GoogleSignInAccount):com.google.android.gms.games.Games$GamesOptions
      com.google.android.gms.games.Games.zza(com.google.android.gms.common.api.GoogleApiClient, boolean):com.google.android.gms.games.internal.zzg */
    public final void clear(GoogleApiClient googleApiClient, int i) {
        zzg zza = Games.zza(googleApiClient, false);
        if (zza != null) {
            zza.zzo(i);
        }
    }
}
