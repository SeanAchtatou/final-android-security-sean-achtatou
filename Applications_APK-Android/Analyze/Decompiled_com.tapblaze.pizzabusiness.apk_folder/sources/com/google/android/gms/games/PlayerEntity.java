package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.games.internal.player.MostRecentGameInfoEntity;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class PlayerEntity extends GamesDowngradeableSafeParcel implements Player {
    public static final Parcelable.Creator<PlayerEntity> CREATOR = new zza();
    private final String name;
    private String zzbw;
    private final long zzbx;
    private final int zzby;
    private final long zzbz;
    private final String zzca;
    private final MostRecentGameInfoEntity zzcb;
    private final PlayerLevelInfo zzcc;
    private final boolean zzcd;
    private final boolean zzce;
    private final String zzcf;
    private final Uri zzcg;
    private final String zzch;
    private final Uri zzci;
    private final String zzcj;
    private final int zzck;
    private final long zzcl;
    private final boolean zzcm;
    private final long zzcn;
    private final zzao zzco;
    private String zzj;
    private final Uri zzn;
    private final Uri zzo;
    private final String zzy;
    private final String zzz;

    /* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
    static final class zza extends zzal {
        zza() {
        }

        public final PlayerEntity zzc(Parcel parcel) {
            Uri uri;
            Uri uri2;
            if (PlayerEntity.zzb(PlayerEntity.getUnparcelClientVersion()) || PlayerEntity.canUnparcelSafely(PlayerEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            long readLong = parcel.readLong();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            if (readString3 == null) {
                uri = null;
            } else {
                uri = Uri.parse(readString3);
            }
            if (readString4 == null) {
                uri2 = null;
            } else {
                uri2 = Uri.parse(readString4);
            }
            return new PlayerEntity(readString, readString2, uri, uri2, readLong, -1, -1, null, null, null, null, null, true, false, readString5, readString6, null, null, null, null, -1, -1, false, -1, null);
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.google.android.gms.games.zzao} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public PlayerEntity(com.google.android.gms.games.Player r5) {
        /*
            r4 = this;
            r4.<init>()
            java.lang.String r0 = r5.getPlayerId()
            r4.zzbw = r0
            java.lang.String r0 = r5.getDisplayName()
            r4.zzj = r0
            android.net.Uri r0 = r5.getIconImageUri()
            r4.zzn = r0
            java.lang.String r0 = r5.getIconImageUrl()
            r4.zzy = r0
            android.net.Uri r0 = r5.getHiResImageUri()
            r4.zzo = r0
            java.lang.String r0 = r5.getHiResImageUrl()
            r4.zzz = r0
            long r0 = r5.getRetrievedTimestamp()
            r4.zzbx = r0
            int r0 = r5.zzj()
            r4.zzby = r0
            long r0 = r5.getLastPlayedWithTimestamp()
            r4.zzbz = r0
            java.lang.String r0 = r5.getTitle()
            r4.zzca = r0
            boolean r0 = r5.zzk()
            r4.zzcd = r0
            com.google.android.gms.games.internal.player.zza r0 = r5.zzl()
            r1 = 0
            if (r0 != 0) goto L_0x004e
            r2 = r1
            goto L_0x0053
        L_0x004e:
            com.google.android.gms.games.internal.player.MostRecentGameInfoEntity r2 = new com.google.android.gms.games.internal.player.MostRecentGameInfoEntity
            r2.<init>(r0)
        L_0x0053:
            r4.zzcb = r2
            com.google.android.gms.games.PlayerLevelInfo r0 = r5.getLevelInfo()
            r4.zzcc = r0
            boolean r0 = r5.zzi()
            r4.zzce = r0
            java.lang.String r0 = r5.zzh()
            r4.zzcf = r0
            java.lang.String r0 = r5.getName()
            r4.name = r0
            android.net.Uri r0 = r5.getBannerImageLandscapeUri()
            r4.zzcg = r0
            java.lang.String r0 = r5.getBannerImageLandscapeUrl()
            r4.zzch = r0
            android.net.Uri r0 = r5.getBannerImagePortraitUri()
            r4.zzci = r0
            java.lang.String r0 = r5.getBannerImagePortraitUrl()
            r4.zzcj = r0
            int r0 = r5.zzm()
            r4.zzck = r0
            long r2 = r5.zzn()
            r4.zzcl = r2
            boolean r0 = r5.isMuted()
            r4.zzcm = r0
            long r2 = r5.zzo()
            r4.zzcn = r2
            com.google.android.gms.games.zzap r5 = r5.zzp()
            if (r5 != 0) goto L_0x00a4
            goto L_0x00ab
        L_0x00a4:
            java.lang.Object r5 = r5.freeze()
            r1 = r5
            com.google.android.gms.games.zzao r1 = (com.google.android.gms.games.zzao) r1
        L_0x00ab:
            r4.zzco = r1
            java.lang.String r5 = r4.zzbw
            com.google.android.gms.common.internal.Asserts.checkNotNull(r5)
            java.lang.String r5 = r4.zzj
            com.google.android.gms.common.internal.Asserts.checkNotNull(r5)
            long r0 = r4.zzbx
            r2 = 0
            int r5 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r5 <= 0) goto L_0x00c1
            r5 = 1
            goto L_0x00c2
        L_0x00c1:
            r5 = 0
        L_0x00c2:
            com.google.android.gms.common.internal.Asserts.checkState(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.PlayerEntity.<init>(com.google.android.gms.games.Player):void");
    }

    public final Player freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    PlayerEntity(String str, String str2, Uri uri, Uri uri2, long j, int i, long j2, String str3, String str4, String str5, MostRecentGameInfoEntity mostRecentGameInfoEntity, PlayerLevelInfo playerLevelInfo, boolean z, boolean z2, String str6, String str7, Uri uri3, String str8, Uri uri4, String str9, int i2, long j3, boolean z3, long j4, zzao zzao) {
        this.zzbw = str;
        this.zzj = str2;
        this.zzn = uri;
        this.zzy = str3;
        this.zzo = uri2;
        this.zzz = str4;
        this.zzbx = j;
        this.zzby = i;
        this.zzbz = j2;
        this.zzca = str5;
        this.zzcd = z;
        this.zzcb = mostRecentGameInfoEntity;
        this.zzcc = playerLevelInfo;
        this.zzce = z2;
        this.zzcf = str6;
        this.name = str7;
        this.zzcg = uri3;
        this.zzch = str8;
        this.zzci = uri4;
        this.zzcj = str9;
        this.zzck = i2;
        this.zzcl = j3;
        this.zzcm = z3;
        this.zzcn = j4;
        this.zzco = zzao;
    }

    public final String getPlayerId() {
        return this.zzbw;
    }

    public final String getDisplayName() {
        return this.zzj;
    }

    public final void getDisplayName(CharArrayBuffer charArrayBuffer) {
        DataUtils.copyStringToBuffer(this.zzj, charArrayBuffer);
    }

    public final String zzh() {
        return this.zzcf;
    }

    public final String getName() {
        return this.name;
    }

    public final boolean zzi() {
        return this.zzce;
    }

    public final boolean hasIconImage() {
        return getIconImageUri() != null;
    }

    public final Uri getIconImageUri() {
        return this.zzn;
    }

    public final String getIconImageUrl() {
        return this.zzy;
    }

    public final boolean hasHiResImage() {
        return getHiResImageUri() != null;
    }

    public final Uri getHiResImageUri() {
        return this.zzo;
    }

    public final String getHiResImageUrl() {
        return this.zzz;
    }

    public final long getRetrievedTimestamp() {
        return this.zzbx;
    }

    public final long getLastPlayedWithTimestamp() {
        return this.zzbz;
    }

    public final int zzj() {
        return this.zzby;
    }

    public final boolean zzk() {
        return this.zzcd;
    }

    public final String getTitle() {
        return this.zzca;
    }

    public final void getTitle(CharArrayBuffer charArrayBuffer) {
        DataUtils.copyStringToBuffer(this.zzca, charArrayBuffer);
    }

    public final PlayerLevelInfo getLevelInfo() {
        return this.zzcc;
    }

    public final com.google.android.gms.games.internal.player.zza zzl() {
        return this.zzcb;
    }

    public final Uri getBannerImageLandscapeUri() {
        return this.zzcg;
    }

    public final String getBannerImageLandscapeUrl() {
        return this.zzch;
    }

    public final Uri getBannerImagePortraitUri() {
        return this.zzci;
    }

    public final String getBannerImagePortraitUrl() {
        return this.zzcj;
    }

    public final int zzm() {
        return this.zzck;
    }

    public final long zzn() {
        return this.zzcl;
    }

    public final boolean isMuted() {
        return this.zzcm;
    }

    public final long zzo() {
        return this.zzcn;
    }

    public final zzap zzp() {
        return this.zzco;
    }

    public final int hashCode() {
        return zza(this);
    }

    static int zza(Player player) {
        return Objects.hashCode(player.getPlayerId(), player.getDisplayName(), Boolean.valueOf(player.zzi()), player.getIconImageUri(), player.getHiResImageUri(), Long.valueOf(player.getRetrievedTimestamp()), player.getTitle(), player.getLevelInfo(), player.zzh(), player.getName(), player.getBannerImageLandscapeUri(), player.getBannerImagePortraitUri(), Integer.valueOf(player.zzm()), Long.valueOf(player.zzn()), Boolean.valueOf(player.isMuted()), Long.valueOf(player.zzo()), player.zzp());
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    static boolean zza(Player player, Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (player == obj) {
            return true;
        }
        Player player2 = (Player) obj;
        return Objects.equal(player2.getPlayerId(), player.getPlayerId()) && Objects.equal(player2.getDisplayName(), player.getDisplayName()) && Objects.equal(Boolean.valueOf(player2.zzi()), Boolean.valueOf(player.zzi())) && Objects.equal(player2.getIconImageUri(), player.getIconImageUri()) && Objects.equal(player2.getHiResImageUri(), player.getHiResImageUri()) && Objects.equal(Long.valueOf(player2.getRetrievedTimestamp()), Long.valueOf(player.getRetrievedTimestamp())) && Objects.equal(player2.getTitle(), player.getTitle()) && Objects.equal(player2.getLevelInfo(), player.getLevelInfo()) && Objects.equal(player2.zzh(), player.zzh()) && Objects.equal(player2.getName(), player.getName()) && Objects.equal(player2.getBannerImageLandscapeUri(), player.getBannerImageLandscapeUri()) && Objects.equal(player2.getBannerImagePortraitUri(), player.getBannerImagePortraitUri()) && Objects.equal(Integer.valueOf(player2.zzm()), Integer.valueOf(player.zzm())) && Objects.equal(Long.valueOf(player2.zzn()), Long.valueOf(player.zzn())) && Objects.equal(Boolean.valueOf(player2.isMuted()), Boolean.valueOf(player.isMuted())) && Objects.equal(Long.valueOf(player2.zzo()), Long.valueOf(player.zzo())) && Objects.equal(player2.zzp(), player.zzp());
    }

    public final String toString() {
        return zzb(this);
    }

    static String zzb(Player player) {
        Objects.ToStringHelper add = Objects.toStringHelper(player).add("PlayerId", player.getPlayerId()).add("DisplayName", player.getDisplayName()).add("HasDebugAccess", Boolean.valueOf(player.zzi())).add("IconImageUri", player.getIconImageUri()).add("IconImageUrl", player.getIconImageUrl()).add("HiResImageUri", player.getHiResImageUri()).add("HiResImageUrl", player.getHiResImageUrl()).add("RetrievedTimestamp", Long.valueOf(player.getRetrievedTimestamp())).add("Title", player.getTitle()).add("LevelInfo", player.getLevelInfo()).add("GamerTag", player.zzh()).add("Name", player.getName()).add("BannerImageLandscapeUri", player.getBannerImageLandscapeUri()).add("BannerImageLandscapeUrl", player.getBannerImageLandscapeUrl()).add("BannerImagePortraitUri", player.getBannerImagePortraitUri()).add("BannerImagePortraitUrl", player.getBannerImagePortraitUrl()).add("GamerFriendStatus", Integer.valueOf(player.zzm())).add("GamerFriendUpdateTimestamp", Long.valueOf(player.zzn())).add("IsMuted", Boolean.valueOf(player.isMuted())).add("totalUnlockedAchievement", Long.valueOf(player.zzo()));
        char[] cArr = {143, 171, 160, 184, 147, 174, 166, 164, 179, 167, 164, 177, 136, 173, 165, 174};
        for (int i = 0; i < 16; i++) {
            cArr[i] = (char) (cArr[i] - '?');
        }
        return add.add(new String(cArr), player.zzp()).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        if (!shouldDowngrade()) {
            int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
            SafeParcelWriter.writeString(parcel, 1, getPlayerId(), false);
            SafeParcelWriter.writeString(parcel, 2, getDisplayName(), false);
            SafeParcelWriter.writeParcelable(parcel, 3, getIconImageUri(), i, false);
            SafeParcelWriter.writeParcelable(parcel, 4, getHiResImageUri(), i, false);
            SafeParcelWriter.writeLong(parcel, 5, getRetrievedTimestamp());
            SafeParcelWriter.writeInt(parcel, 6, this.zzby);
            SafeParcelWriter.writeLong(parcel, 7, getLastPlayedWithTimestamp());
            SafeParcelWriter.writeString(parcel, 8, getIconImageUrl(), false);
            SafeParcelWriter.writeString(parcel, 9, getHiResImageUrl(), false);
            SafeParcelWriter.writeString(parcel, 14, getTitle(), false);
            SafeParcelWriter.writeParcelable(parcel, 15, this.zzcb, i, false);
            SafeParcelWriter.writeParcelable(parcel, 16, getLevelInfo(), i, false);
            SafeParcelWriter.writeBoolean(parcel, 18, this.zzcd);
            SafeParcelWriter.writeBoolean(parcel, 19, this.zzce);
            SafeParcelWriter.writeString(parcel, 20, this.zzcf, false);
            SafeParcelWriter.writeString(parcel, 21, this.name, false);
            SafeParcelWriter.writeParcelable(parcel, 22, getBannerImageLandscapeUri(), i, false);
            SafeParcelWriter.writeString(parcel, 23, getBannerImageLandscapeUrl(), false);
            SafeParcelWriter.writeParcelable(parcel, 24, getBannerImagePortraitUri(), i, false);
            SafeParcelWriter.writeString(parcel, 25, getBannerImagePortraitUrl(), false);
            SafeParcelWriter.writeInt(parcel, 26, this.zzck);
            SafeParcelWriter.writeLong(parcel, 27, this.zzcl);
            SafeParcelWriter.writeBoolean(parcel, 28, this.zzcm);
            SafeParcelWriter.writeLong(parcel, 29, this.zzcn);
            SafeParcelWriter.writeParcelable(parcel, 33, this.zzco, i, false);
            SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
            return;
        }
        parcel.writeString(this.zzbw);
        parcel.writeString(this.zzj);
        Uri uri = this.zzn;
        String str = null;
        parcel.writeString(uri == null ? null : uri.toString());
        Uri uri2 = this.zzo;
        if (uri2 != null) {
            str = uri2.toString();
        }
        parcel.writeString(str);
        parcel.writeLong(this.zzbx);
    }
}
