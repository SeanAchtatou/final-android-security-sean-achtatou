package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzch  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzch extends zzfc<zzch, zza> implements zzgn {
    private static volatile zzgv<zzch> zzij;
    /* access modifiers changed from: private */
    public static final zzch zzje;
    private int zzie;
    private long zzik;
    private long zzjc;
    private long zzjd;

    private zzch() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzch$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzch, zza> implements zzgn {
        private zza() {
            super(zzch.zzje);
        }

        public final zza zzy(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzch) this.zzqq).zzu(j);
            return this;
        }

        public final zza zzz(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzch) this.zzqq).zzw(j);
            return this;
        }

        public final zza zzaa(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzch) this.zzqq).zzx(j);
            return this;
        }

        /* synthetic */ zza(zzcj zzcj) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zzu(long j) {
        this.zzie |= 1;
        this.zzik = j;
    }

    /* access modifiers changed from: private */
    public final void zzw(long j) {
        this.zzie |= 2;
        this.zzjc = j;
    }

    /* access modifiers changed from: private */
    public final void zzx(long j) {
        this.zzie |= 4;
        this.zzjd = j;
    }

    public static zza zzdq() {
        return (zza) zzje.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzcj.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzch();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzje, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0002\u0000\u0002\u0002\u0001\u0003\u0002\u0002", new Object[]{"zzie", "zzik", "zzjc", "zzjd"});
            case 4:
                return zzje;
            case 5:
                zzgv<zzch> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzch.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzje);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzch zzch = new zzch();
        zzje = zzch;
        zzfc.zza(zzch.class, zzch);
    }
}
