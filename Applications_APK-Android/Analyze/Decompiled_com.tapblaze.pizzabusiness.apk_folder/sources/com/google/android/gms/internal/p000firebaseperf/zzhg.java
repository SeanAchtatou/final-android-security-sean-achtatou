package com.google.android.gms.internal.p000firebaseperf;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhg  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
class zzhg<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    private boolean zznu;
    private final int zzuc;
    /* access modifiers changed from: private */
    public List<zzhl> zzud;
    /* access modifiers changed from: private */
    public Map<K, V> zzue;
    private volatile zzhn zzuf;
    /* access modifiers changed from: private */
    public Map<K, V> zzug;
    private volatile zzhh zzuh;

    static <FieldDescriptorType extends zzez<FieldDescriptorType>> zzhg<FieldDescriptorType, Object> zzat(int i) {
        return new zzhf(i);
    }

    private zzhg(int i) {
        this.zzuc = i;
        this.zzud = Collections.emptyList();
        this.zzue = Collections.emptyMap();
        this.zzug = Collections.emptyMap();
    }

    public void zzgg() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.zznu) {
            if (this.zzue.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.zzue);
            }
            this.zzue = map;
            if (this.zzug.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.zzug);
            }
            this.zzug = map2;
            this.zznu = true;
        }
    }

    public final boolean isImmutable() {
        return this.zznu;
    }

    public final int zziw() {
        return this.zzud.size();
    }

    public final Map.Entry<K, V> zzau(int i) {
        return this.zzud.get(i);
    }

    public final Iterable<Map.Entry<K, V>> zzix() {
        if (this.zzue.isEmpty()) {
            return zzhk.zzjc();
        }
        return this.zzue.entrySet();
    }

    public int size() {
        return this.zzud.size() + this.zzue.size();
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return zza(comparable) >= 0 || this.zzue.containsKey(comparable);
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzud.get(zza).getValue();
        }
        return this.zzue.get(comparable);
    }

    /* renamed from: zza */
    public final V put(Comparable comparable, Object obj) {
        zziz();
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzud.get(zza).setValue(obj);
        }
        zziz();
        if (this.zzud.isEmpty() && !(this.zzud instanceof ArrayList)) {
            this.zzud = new ArrayList(this.zzuc);
        }
        int i = -(zza + 1);
        if (i >= this.zzuc) {
            return zzja().put(comparable, obj);
        }
        int size = this.zzud.size();
        int i2 = this.zzuc;
        if (size == i2) {
            zzhl remove = this.zzud.remove(i2 - 1);
            zzja().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.zzud.add(i, new zzhl(this, comparable, obj));
        return null;
    }

    public void clear() {
        zziz();
        if (!this.zzud.isEmpty()) {
            this.zzud.clear();
        }
        if (!this.zzue.isEmpty()) {
            this.zzue.clear();
        }
    }

    public V remove(Object obj) {
        zziz();
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return zzav(zza);
        }
        if (this.zzue.isEmpty()) {
            return null;
        }
        return this.zzue.remove(comparable);
    }

    /* access modifiers changed from: private */
    public final V zzav(int i) {
        zziz();
        V value = this.zzud.remove(i).getValue();
        if (!this.zzue.isEmpty()) {
            Iterator it = zzja().entrySet().iterator();
            this.zzud.add(new zzhl(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final int zza(K r5) {
        /*
            r4 = this;
            java.util.List<com.google.android.gms.internal.firebase-perf.zzhl> r0 = r4.zzud
            int r0 = r0.size()
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x0025
            java.util.List<com.google.android.gms.internal.firebase-perf.zzhl> r1 = r4.zzud
            java.lang.Object r1 = r1.get(r0)
            com.google.android.gms.internal.firebase-perf.zzhl r1 = (com.google.android.gms.internal.p000firebaseperf.zzhl) r1
            java.lang.Object r1 = r1.getKey()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r1 = r5.compareTo(r1)
            if (r1 <= 0) goto L_0x0022
            int r0 = r0 + 2
            int r5 = -r0
            return r5
        L_0x0022:
            if (r1 != 0) goto L_0x0025
            return r0
        L_0x0025:
            r1 = 0
        L_0x0026:
            if (r1 > r0) goto L_0x0049
            int r2 = r1 + r0
            int r2 = r2 / 2
            java.util.List<com.google.android.gms.internal.firebase-perf.zzhl> r3 = r4.zzud
            java.lang.Object r3 = r3.get(r2)
            com.google.android.gms.internal.firebase-perf.zzhl r3 = (com.google.android.gms.internal.p000firebaseperf.zzhl) r3
            java.lang.Object r3 = r3.getKey()
            java.lang.Comparable r3 = (java.lang.Comparable) r3
            int r3 = r5.compareTo(r3)
            if (r3 >= 0) goto L_0x0043
            int r0 = r2 + -1
            goto L_0x0026
        L_0x0043:
            if (r3 <= 0) goto L_0x0048
            int r1 = r2 + 1
            goto L_0x0026
        L_0x0048:
            return r2
        L_0x0049:
            int r1 = r1 + 1
            int r5 = -r1
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzhg.zza(java.lang.Comparable):int");
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.zzuf == null) {
            this.zzuf = new zzhn(this, null);
        }
        return this.zzuf;
    }

    /* access modifiers changed from: package-private */
    public final Set<Map.Entry<K, V>> zziy() {
        if (this.zzuh == null) {
            this.zzuh = new zzhh(this, null);
        }
        return this.zzuh;
    }

    /* access modifiers changed from: private */
    public final void zziz() {
        if (this.zznu) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> zzja() {
        zziz();
        if (this.zzue.isEmpty() && !(this.zzue instanceof TreeMap)) {
            this.zzue = new TreeMap();
            this.zzug = ((TreeMap) this.zzue).descendingMap();
        }
        return (SortedMap) this.zzue;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzhg)) {
            return super.equals(obj);
        }
        zzhg zzhg = (zzhg) obj;
        int size = size();
        if (size != zzhg.size()) {
            return false;
        }
        int zziw = zziw();
        if (zziw != zzhg.zziw()) {
            return entrySet().equals(zzhg.entrySet());
        }
        for (int i = 0; i < zziw; i++) {
            if (!zzau(i).equals(zzhg.zzau(i))) {
                return false;
            }
        }
        if (zziw != size) {
            return this.zzue.equals(zzhg.zzue);
        }
        return true;
    }

    public int hashCode() {
        int zziw = zziw();
        int i = 0;
        for (int i2 = 0; i2 < zziw; i2++) {
            i += this.zzud.get(i2).hashCode();
        }
        return this.zzue.size() > 0 ? i + this.zzue.hashCode() : i;
    }

    /* synthetic */ zzhg(int i, zzhf zzhf) {
        this(i);
    }
}
