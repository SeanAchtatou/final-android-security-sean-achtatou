package com.google.android.gms.internal.p000firebaseperf;

import java.util.List;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfw  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public interface zzfw extends List {
    Object getRaw(int i);

    void zzc(zzeb zzeb);

    List<?> zzhv();

    zzfw zzhw();
}
