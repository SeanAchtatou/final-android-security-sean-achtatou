package com.google.android.gms.internal.ads;

import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zzdfc {
    public static <E> ArrayList<E> zzdz(int i) {
        zzdeo.zze(i, "initialArraySize");
        return new ArrayList<>(i);
    }
}
