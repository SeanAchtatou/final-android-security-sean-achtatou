package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdro;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdrm<T extends zzdro<T>> {
    private static final zzdrm zzhjs = new zzdrm(true);
    final zzdub<T, Object> zzhjp;
    private boolean zzhjq;
    private boolean zzhjr;

    private zzdrm() {
        this.zzhjp = zzdub.zzgv(16);
    }

    private zzdrm(boolean z) {
        this(zzdub.zzgv(0));
        zzaxq();
    }

    private zzdrm(zzdub<T, Object> zzdub) {
        this.zzhjp = zzdub;
        zzaxq();
    }

    public static <T extends zzdro<T>> zzdrm<T> zzazm() {
        return zzhjs;
    }

    public final void zzaxq() {
        if (!this.zzhjq) {
            this.zzhjp.zzaxq();
            this.zzhjq = true;
        }
    }

    public final boolean isImmutable() {
        return this.zzhjq;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzdrm)) {
            return false;
        }
        return this.zzhjp.equals(((zzdrm) obj).zzhjp);
    }

    public final int hashCode() {
        return this.zzhjp.hashCode();
    }

    public final Iterator<Map.Entry<T, Object>> iterator() {
        if (this.zzhjr) {
            return new zzdsk(this.zzhjp.entrySet().iterator());
        }
        return this.zzhjp.entrySet().iterator();
    }

    /* access modifiers changed from: package-private */
    public final Iterator<Map.Entry<T, Object>> descendingIterator() {
        if (this.zzhjr) {
            return new zzdsk(this.zzhjp.zzbbu().iterator());
        }
        return this.zzhjp.zzbbu().iterator();
    }

    private final Object zza(T t) {
        Object obj = this.zzhjp.get(t);
        if (!(obj instanceof zzdsf)) {
            return obj;
        }
        zzdsf zzdsf = (zzdsf) obj;
        return zzdsf.zzbas();
    }

    private final void zza(T t, Object obj) {
        if (!t.zzazq()) {
            zza(t.zzazo(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList2.get(i);
                i++;
                zza(t.zzazo(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof zzdsf) {
            this.zzhjr = true;
        }
        this.zzhjp.put(t, obj);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        if ((r3 instanceof com.google.android.gms.internal.ads.zzdry) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001c, code lost:
        if ((r3 instanceof com.google.android.gms.internal.ads.zzdsf) == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void zza(com.google.android.gms.internal.ads.zzdvf r2, java.lang.Object r3) {
        /*
            com.google.android.gms.internal.ads.zzdrv.checkNotNull(r3)
            int[] r0 = com.google.android.gms.internal.ads.zzdrl.zzhjo
            com.google.android.gms.internal.ads.zzdvm r2 = r2.zzbcp()
            int r2 = r2.ordinal()
            r2 = r0[r2]
            r0 = 1
            r1 = 0
            switch(r2) {
                case 1: goto L_0x0040;
                case 2: goto L_0x003d;
                case 3: goto L_0x003a;
                case 4: goto L_0x0037;
                case 5: goto L_0x0034;
                case 6: goto L_0x0031;
                case 7: goto L_0x0028;
                case 8: goto L_0x001f;
                case 9: goto L_0x0016;
                default: goto L_0x0014;
            }
        L_0x0014:
            r0 = 0
            goto L_0x0042
        L_0x0016:
            boolean r2 = r3 instanceof com.google.android.gms.internal.ads.zzdte
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.google.android.gms.internal.ads.zzdsf
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x001f:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.google.android.gms.internal.ads.zzdry
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0028:
            boolean r2 = r3 instanceof com.google.android.gms.internal.ads.zzdqk
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0031:
            boolean r0 = r3 instanceof java.lang.String
            goto L_0x0042
        L_0x0034:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L_0x0042
        L_0x0037:
            boolean r0 = r3 instanceof java.lang.Double
            goto L_0x0042
        L_0x003a:
            boolean r0 = r3 instanceof java.lang.Float
            goto L_0x0042
        L_0x003d:
            boolean r0 = r3 instanceof java.lang.Long
            goto L_0x0042
        L_0x0040:
            boolean r0 = r3 instanceof java.lang.Integer
        L_0x0042:
            if (r0 == 0) goto L_0x0045
            return
        L_0x0045:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            goto L_0x004e
        L_0x004d:
            throw r2
        L_0x004e:
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdrm.zza(com.google.android.gms.internal.ads.zzdvf, java.lang.Object):void");
    }

    public final boolean isInitialized() {
        for (int i = 0; i < this.zzhjp.zzbbs(); i++) {
            if (!zzb(this.zzhjp.zzgw(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> zzb : this.zzhjp.zzbbt()) {
            if (!zzb(zzb)) {
                return false;
            }
        }
        return true;
    }

    private static <T extends zzdro<T>> boolean zzb(Map.Entry<T, Object> entry) {
        zzdro zzdro = (zzdro) entry.getKey();
        if (zzdro.zzazp() == zzdvm.MESSAGE) {
            if (zzdro.zzazq()) {
                for (zzdte isInitialized : (List) entry.getValue()) {
                    if (!isInitialized.isInitialized()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof zzdte) {
                    if (!((zzdte) value).isInitialized()) {
                        return false;
                    }
                } else if (value instanceof zzdsf) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public final void zza(zzdrm<T> zzdrm) {
        for (int i = 0; i < zzdrm.zzhjp.zzbbs(); i++) {
            zzc(zzdrm.zzhjp.zzgw(i));
        }
        for (Map.Entry<T, Object> zzc : zzdrm.zzhjp.zzbbt()) {
            zzc(zzc);
        }
    }

    private static Object zzao(Object obj) {
        if (obj instanceof zzdtj) {
            return ((zzdtj) obj).zzaxm();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    private final void zzc(Map.Entry<T, Object> entry) {
        Object obj;
        zzdro zzdro = (zzdro) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof zzdsf) {
            zzdsf zzdsf = (zzdsf) value;
            value = zzdsf.zzbas();
        }
        if (zzdro.zzazq()) {
            Object zza = zza(zzdro);
            if (zza == null) {
                zza = new ArrayList();
            }
            for (Object zzao : (List) value) {
                ((List) zza).add(zzao(zzao));
            }
            this.zzhjp.put(zzdro, zza);
        } else if (zzdro.zzazp() == zzdvm.MESSAGE) {
            Object zza2 = zza(zzdro);
            if (zza2 == null) {
                this.zzhjp.put(zzdro, zzao(value));
                return;
            }
            if (zza2 instanceof zzdtj) {
                obj = zzdro.zza((zzdtj) zza2, (zzdtj) value);
            } else {
                obj = zzdro.zza(((zzdte) zza2).zzazx(), (zzdte) value).zzbaf();
            }
            this.zzhjp.put(zzdro, obj);
        } else {
            this.zzhjp.put(zzdro, zzao(value));
        }
    }

    static void zza(zzdrb zzdrb, zzdvf zzdvf, int i, Object obj) throws IOException {
        if (zzdvf == zzdvf.GROUP) {
            zzdte zzdte = (zzdte) obj;
            zzdrv.zzk(zzdte);
            zzdrb.zzaa(i, 3);
            zzdte.zzb(zzdrb);
            zzdrb.zzaa(i, 4);
            return;
        }
        zzdrb.zzaa(i, zzdvf.zzbcq());
        switch (zzdrl.zzhiw[zzdvf.ordinal()]) {
            case 1:
                zzdrb.zzb(((Double) obj).doubleValue());
                return;
            case 2:
                zzdrb.zzf(((Float) obj).floatValue());
                return;
            case 3:
                zzdrb.zzfg(((Long) obj).longValue());
                return;
            case 4:
                zzdrb.zzfg(((Long) obj).longValue());
                return;
            case 5:
                zzdrb.zzfv(((Integer) obj).intValue());
                return;
            case 6:
                zzdrb.zzfi(((Long) obj).longValue());
                return;
            case 7:
                zzdrb.zzfy(((Integer) obj).intValue());
                return;
            case 8:
                zzdrb.zzbn(((Boolean) obj).booleanValue());
                return;
            case 9:
                ((zzdte) obj).zzb(zzdrb);
                return;
            case 10:
                zzdrb.zzg((zzdte) obj);
                return;
            case 11:
                if (obj instanceof zzdqk) {
                    zzdrb.zzbe((zzdqk) obj);
                    return;
                } else {
                    zzdrb.zzhg((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof zzdqk) {
                    zzdrb.zzbe((zzdqk) obj);
                    return;
                }
                byte[] bArr = (byte[]) obj;
                zzdrb.zzk(bArr, 0, bArr.length);
                return;
            case 13:
                zzdrb.zzfw(((Integer) obj).intValue());
                return;
            case 14:
                zzdrb.zzfy(((Integer) obj).intValue());
                return;
            case 15:
                zzdrb.zzfi(((Long) obj).longValue());
                return;
            case 16:
                zzdrb.zzfx(((Integer) obj).intValue());
                return;
            case 17:
                zzdrb.zzfh(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof zzdry) {
                    zzdrb.zzfv(((zzdry) obj).zzae());
                    return;
                } else {
                    zzdrb.zzfv(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    public final int zzazn() {
        int i = 0;
        for (int i2 = 0; i2 < this.zzhjp.zzbbs(); i2++) {
            i += zzd(this.zzhjp.zzgw(i2));
        }
        for (Map.Entry<T, Object> zzd : this.zzhjp.zzbbt()) {
            i += zzd(zzd);
        }
        return i;
    }

    private static int zzd(Map.Entry<T, Object> entry) {
        zzdro zzdro = (zzdro) entry.getKey();
        Object value = entry.getValue();
        if (zzdro.zzazp() != zzdvm.MESSAGE || zzdro.zzazq() || zzdro.zzazr()) {
            return zzb(zzdro, value);
        }
        if (value instanceof zzdsf) {
            return zzdrb.zzb(((zzdro) entry.getKey()).zzae(), (zzdsf) value);
        }
        return zzdrb.zzd(((zzdro) entry.getKey()).zzae(), (zzdte) value);
    }

    static int zza(zzdvf zzdvf, int i, Object obj) {
        int zzfz = zzdrb.zzfz(i);
        if (zzdvf == zzdvf.GROUP) {
            zzdrv.zzk((zzdte) obj);
            zzfz <<= 1;
        }
        return zzfz + zzb(zzdvf, obj);
    }

    private static int zzb(zzdvf zzdvf, Object obj) {
        switch (zzdrl.zzhiw[zzdvf.ordinal()]) {
            case 1:
                return zzdrb.zzc(((Double) obj).doubleValue());
            case 2:
                return zzdrb.zzg(((Float) obj).floatValue());
            case 3:
                return zzdrb.zzfj(((Long) obj).longValue());
            case 4:
                return zzdrb.zzfk(((Long) obj).longValue());
            case 5:
                return zzdrb.zzga(((Integer) obj).intValue());
            case 6:
                return zzdrb.zzfm(((Long) obj).longValue());
            case 7:
                return zzdrb.zzgd(((Integer) obj).intValue());
            case 8:
                return zzdrb.zzbo(((Boolean) obj).booleanValue());
            case 9:
                return zzdrb.zzi((zzdte) obj);
            case 10:
                if (obj instanceof zzdsf) {
                    return zzdrb.zza((zzdsf) obj);
                }
                return zzdrb.zzh((zzdte) obj);
            case 11:
                if (obj instanceof zzdqk) {
                    return zzdrb.zzbf((zzdqk) obj);
                }
                return zzdrb.zzhh((String) obj);
            case 12:
                if (obj instanceof zzdqk) {
                    return zzdrb.zzbf((zzdqk) obj);
                }
                return zzdrb.zzx((byte[]) obj);
            case 13:
                return zzdrb.zzgb(((Integer) obj).intValue());
            case 14:
                return zzdrb.zzge(((Integer) obj).intValue());
            case 15:
                return zzdrb.zzfn(((Long) obj).longValue());
            case 16:
                return zzdrb.zzgc(((Integer) obj).intValue());
            case 17:
                return zzdrb.zzfl(((Long) obj).longValue());
            case 18:
                if (obj instanceof zzdry) {
                    return zzdrb.zzgf(((zzdry) obj).zzae());
                }
                return zzdrb.zzgf(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int zzb(zzdro<?> zzdro, Object obj) {
        zzdvf zzazo = zzdro.zzazo();
        int zzae = zzdro.zzae();
        if (!zzdro.zzazq()) {
            return zza(zzazo, zzae, obj);
        }
        int i = 0;
        if (zzdro.zzazr()) {
            for (Object zzb : (List) obj) {
                i += zzb(zzazo, zzb);
            }
            return zzdrb.zzfz(zzae) + i + zzdrb.zzgh(i);
        }
        for (Object zza : (List) obj) {
            i += zza(zzazo, zzae, zza);
        }
        return i;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzdrm zzdrm = new zzdrm();
        for (int i = 0; i < this.zzhjp.zzbbs(); i++) {
            Map.Entry<T, Object> zzgw = this.zzhjp.zzgw(i);
            zzdrm.zza((zzdro) zzgw.getKey(), zzgw.getValue());
        }
        for (Map.Entry next : this.zzhjp.zzbbt()) {
            zzdrm.zza((zzdro) next.getKey(), next.getValue());
        }
        zzdrm.zzhjr = this.zzhjr;
        return zzdrm;
    }
}
