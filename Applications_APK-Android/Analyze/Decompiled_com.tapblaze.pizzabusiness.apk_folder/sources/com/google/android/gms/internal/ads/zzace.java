package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzace implements NativeCustomTemplateAd.DisplayOpenMeasurement {
    private final zzade zzcvr;

    public zzace(zzade zzade) {
        this.zzcvr = zzade;
        try {
            zzade.zzrn();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final boolean start() {
        try {
            return this.zzcvr.zzrm();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return false;
        }
    }

    public final void setView(View view) {
        try {
            this.zzcvr.zzq(ObjectWrapper.wrap(view));
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
