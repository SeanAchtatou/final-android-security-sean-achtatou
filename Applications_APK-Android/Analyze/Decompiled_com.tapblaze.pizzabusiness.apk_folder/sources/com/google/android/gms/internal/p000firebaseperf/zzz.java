package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzz  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
abstract class zzz {
    private static final Throwable[] zzs = new Throwable[0];

    zzz() {
    }

    public abstract void zza(Throwable th, Throwable th2);
}
