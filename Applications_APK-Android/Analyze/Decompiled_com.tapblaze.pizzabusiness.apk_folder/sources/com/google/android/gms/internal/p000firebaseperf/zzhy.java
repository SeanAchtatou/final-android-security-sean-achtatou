package com.google.android.gms.internal.p000firebaseperf;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhy  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzhy extends AbstractList<String> implements zzfw, RandomAccess {
    /* access modifiers changed from: private */
    public final zzfw zzuv;

    public zzhy(zzfw zzfw) {
        this.zzuv = zzfw;
    }

    public final zzfw zzhw() {
        return this;
    }

    public final Object getRaw(int i) {
        return this.zzuv.getRaw(i);
    }

    public final int size() {
        return this.zzuv.size();
    }

    public final void zzc(zzeb zzeb) {
        throw new UnsupportedOperationException();
    }

    public final ListIterator<String> listIterator(int i) {
        return new zzhx(this, i);
    }

    public final Iterator<String> iterator() {
        return new zzia(this);
    }

    public final List<?> zzhv() {
        return this.zzuv.zzhv();
    }

    public final /* synthetic */ Object get(int i) {
        return (String) this.zzuv.get(i);
    }
}
