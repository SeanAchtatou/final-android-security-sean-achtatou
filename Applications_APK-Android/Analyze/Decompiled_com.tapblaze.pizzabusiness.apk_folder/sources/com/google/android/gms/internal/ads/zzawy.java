package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.ads.internal.zzq;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzawy {
    private final Object lock = new Object();
    private String zzdtj = "";
    private String zzdtk = "";
    private boolean zzdtl = false;
    private String zzdtm = "";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzawy.zza(android.content.Context, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, java.lang.String, int, int]
     candidates:
      com.google.android.gms.internal.ads.zzawy.zza(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzawy.zza(android.content.Context, java.lang.String, boolean, boolean):void */
    public final void zze(Context context, String str, String str2) {
        if (!zzf(context, str, str2)) {
            zza(context, "In-app preview failed to load because of a system error. Please try again later.", true, true);
        } else if ("2".equals(this.zzdtm)) {
            zzavs.zzea("Creative is not pushed for this device.");
            zza(context, "There was no creative pushed from DFP to the device.", false, false);
        } else if ("1".equals(this.zzdtm)) {
            zzavs.zzea("The app is not linked for creative preview.");
            zzi(context, str, str2);
        } else if (AppEventsConstants.EVENT_PARAM_VALUE_NO.equals(this.zzdtm)) {
            zzavs.zzea("Device is linked for in app preview.");
            zza(context, "The device is successfully linked for creative preview.", false, true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzawy.zza(android.content.Context, java.lang.String, boolean, boolean):void
     arg types: [android.content.Context, java.lang.String, int, int]
     candidates:
      com.google.android.gms.internal.ads.zzawy.zza(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzawy.zza(android.content.Context, java.lang.String, boolean, boolean):void */
    public final void zza(Context context, String str, String str2, String str3) {
        boolean zzwy = zzwy();
        if (zzg(context, str, str2)) {
            if (!zzwy && !TextUtils.isEmpty(str3)) {
                zzc(context, str2, str3, str);
            }
            zzavs.zzea("Device is linked for debug signals.");
            zza(context, "The device is successfully linked for troubleshooting.", false, true);
            return;
        }
        zzi(context, str, str2);
    }

    private final boolean zzf(Context context, String str, String str2) {
        String zzh = zzh(context, zzd(context, (String) zzve.zzoy().zzd(zzzn.zzcnn), str, str2).toString(), str2);
        if (TextUtils.isEmpty(zzh)) {
            zzavs.zzea("Not linked for in app preview.");
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(zzh.trim());
            String optString = jSONObject.optString("gct");
            this.zzdtm = jSONObject.optString("status");
            synchronized (this.lock) {
                this.zzdtk = optString;
            }
            return true;
        } catch (JSONException e) {
            zzavs.zzd("Fail to get in app preview response json.", e);
            return false;
        }
    }

    private final boolean zzg(Context context, String str, String str2) {
        String zzh = zzh(context, zzd(context, (String) zzve.zzoy().zzd(zzzn.zzcno), str, str2).toString(), str2);
        if (TextUtils.isEmpty(zzh)) {
            zzavs.zzea("Not linked for debug signals.");
            return false;
        }
        try {
            boolean equals = "1".equals(new JSONObject(zzh.trim()).optString("debug_mode"));
            synchronized (this.lock) {
                this.zzdtl = equals;
            }
            return equals;
        } catch (JSONException e) {
            zzavs.zzd("Fail to get debug mode response json.", e);
            return false;
        }
    }

    private static String zzh(Context context, String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", zzq.zzkq().zzr(context, str2));
        zzdhe<String> zzc = new zzaxk(context).zzc(str, hashMap);
        try {
            return zzc.get((long) ((Integer) zzve.zzoy().zzd(zzzn.zzcnq)).intValue(), TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            String valueOf = String.valueOf(str);
            zzavs.zzc(valueOf.length() != 0 ? "Timeout while retriving a response from: ".concat(valueOf) : new String("Timeout while retriving a response from: "), e);
            zzc.cancel(true);
            return null;
        } catch (InterruptedException e2) {
            String valueOf2 = String.valueOf(str);
            zzavs.zzc(valueOf2.length() != 0 ? "Interrupted while retriving a response from: ".concat(valueOf2) : new String("Interrupted while retriving a response from: "), e2);
            zzc.cancel(true);
            return null;
        } catch (Exception e3) {
            String valueOf3 = String.valueOf(str);
            zzavs.zzc(valueOf3.length() != 0 ? "Error retriving a response from: ".concat(valueOf3) : new String("Error retriving a response from: "), e3);
            return null;
        }
    }

    private final void zzi(Context context, String str, String str2) {
        zzq.zzkq();
        zzawb.zza(context, zzd(context, (String) zzve.zzoy().zzd(zzzn.zzcnm), str, str2));
    }

    public final boolean zzb(Context context, String str, String str2, String str3) {
        if (TextUtils.isEmpty(str2) || !zzq.zzla().zzwy()) {
            return false;
        }
        zzavs.zzea("Sending troubleshooting signals to the server.");
        zzc(context, str, str2, str3);
        return true;
    }

    private final void zzc(Context context, String str, String str2, String str3) {
        Uri.Builder buildUpon = zzd(context, (String) zzve.zzoy().zzd(zzzn.zzcnp), str3, str).buildUpon();
        buildUpon.appendQueryParameter("debugData", str2);
        zzq.zzkq();
        zzawb.zzb(context, str, buildUpon.build().toString());
    }

    private final Uri zzd(Context context, String str, String str2, String str3) {
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        buildUpon.appendQueryParameter("linkedDeviceId", zzbf(context));
        buildUpon.appendQueryParameter("adSlotPath", str2);
        buildUpon.appendQueryParameter("afmaVersion", str3);
        return buildUpon.build();
    }

    private final String zzbf(Context context) {
        String str;
        synchronized (this.lock) {
            if (TextUtils.isEmpty(this.zzdtj)) {
                zzq.zzkq();
                this.zzdtj = zzawb.zzs(context, "debug_signals_id.txt");
                if (TextUtils.isEmpty(this.zzdtj)) {
                    zzq.zzkq();
                    this.zzdtj = zzawb.zzwk();
                    zzq.zzkq();
                    zzawb.zzc(context, "debug_signals_id.txt", this.zzdtj);
                }
            }
            str = this.zzdtj;
        }
        return str;
    }

    public final String zzwx() {
        String str;
        synchronized (this.lock) {
            str = this.zzdtk;
        }
        return str;
    }

    public final boolean zzwy() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzdtl;
        }
        return z;
    }

    private final void zza(Context context, String str, boolean z, boolean z2) {
        if (!(context instanceof Activity)) {
            zzavs.zzey("Can not create dialog without Activity Context");
        } else {
            zzawb.zzdsr.post(new zzaxb(this, context, str, z, z2));
        }
    }
}
