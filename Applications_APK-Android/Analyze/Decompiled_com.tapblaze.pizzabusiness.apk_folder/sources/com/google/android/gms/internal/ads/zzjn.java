package com.google.android.gms.internal.ads;

import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzjn {
    public final int zzanc = 1;
    public final byte[] zzand;

    public zzjn(int i, byte[] bArr) {
        this.zzand = bArr;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            zzjn zzjn = (zzjn) obj;
            return this.zzanc == zzjn.zzanc && Arrays.equals(this.zzand, zzjn.zzand);
        }
    }

    public final int hashCode() {
        return (this.zzanc * 31) + Arrays.hashCode(this.zzand);
    }
}
