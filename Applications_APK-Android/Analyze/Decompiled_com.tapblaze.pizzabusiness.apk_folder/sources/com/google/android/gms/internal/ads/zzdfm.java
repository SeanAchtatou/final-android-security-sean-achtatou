package com.google.android.gms.internal.ads;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzdfm<E> extends zzdfb<E> {
    private final transient E zzgve;
    private transient int zzgvf;

    zzdfm(E e) {
        this.zzgve = zzdei.checkNotNull(e);
    }

    public final int size() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzarc() {
        return false;
    }

    zzdfm(E e, int i) {
        this.zzgve = e;
        this.zzgvf = i;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final boolean contains(java.lang.Object r2) {
        /*
            r1 = this;
            E r0 = r1.zzgve
            boolean r2 = r0.equals(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdfm.contains(java.lang.Object):boolean");
    }

    public final zzdfp<E> zzaqx() {
        return new zzdfd(this.zzgve);
    }

    /* access modifiers changed from: package-private */
    public final zzdeu<E> zzarj() {
        return zzdeu.zzaf(this.zzgve);
    }

    /* access modifiers changed from: package-private */
    public final int zza(Object[] objArr, int i) {
        objArr[i] = this.zzgve;
        return i + 1;
    }

    public final int hashCode() {
        int i = this.zzgvf;
        if (i != 0) {
            return i;
        }
        int hashCode = this.zzgve.hashCode();
        this.zzgvf = hashCode;
        return hashCode;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzari() {
        return this.zzgvf != 0;
    }

    public final String toString() {
        String obj = this.zzgve.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 2);
        sb.append('[');
        sb.append(obj);
        sb.append(']');
        return sb.toString();
    }

    public final /* synthetic */ Iterator iterator() {
        return iterator();
    }
}
