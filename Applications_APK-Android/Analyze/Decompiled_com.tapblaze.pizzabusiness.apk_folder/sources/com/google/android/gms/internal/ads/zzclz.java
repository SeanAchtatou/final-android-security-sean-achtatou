package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzclz implements zzbuv {
    private final zzcip zzfyq;
    private final zzciq zzfzr;

    zzclz(zzcip zzcip, zzciq zzciq) {
        this.zzfyq = zzcip;
        this.zzfzr = zzciq;
    }

    public final void zza(boolean z, Context context) {
        zzcip zzcip = this.zzfyq;
        zzciq zzciq = this.zzfzr;
        try {
            if (((zzani) zzcip.zzddn).zzaa(ObjectWrapper.wrap(context))) {
                zzciq.zzamd();
            } else {
                zzavs.zzez("Can't show rewarded video.");
            }
        } catch (RemoteException e) {
            zzavs.zzd("Can't show rewarded video.", e);
        }
    }
}
