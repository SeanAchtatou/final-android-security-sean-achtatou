package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public class zzfv {
    private static final zzer zza = zzer.zza();
    private zzdw zzb;
    private volatile zzgm zzc;
    private volatile zzdw zzd;

    public int hashCode() {
        return 1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzfv)) {
            return false;
        }
        zzfv zzfv = (zzfv) obj;
        zzgm zzgm = this.zzc;
        zzgm zzgm2 = zzfv.zzc;
        if (zzgm == null && zzgm2 == null) {
            return zzc().equals(zzfv.zzc());
        }
        if (zzgm != null && zzgm2 != null) {
            return zzgm.equals(zzgm2);
        }
        if (zzgm != null) {
            return zzgm.equals(zzfv.zzb(zzgm.h_()));
        }
        return zzb(zzgm2.h_()).equals(zzgm2);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.measurement.zzgm zzb(com.google.android.gms.internal.measurement.zzgm r2) {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.zzgm r0 = r1.zzc
            if (r0 != 0) goto L_0x001d
            monitor-enter(r1)
            com.google.android.gms.internal.measurement.zzgm r0 = r1.zzc     // Catch:{ all -> 0x001a }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x000b:
            r1.zzc = r2     // Catch:{ zzfm -> 0x0012 }
            com.google.android.gms.internal.measurement.zzdw r0 = com.google.android.gms.internal.measurement.zzdw.zza     // Catch:{ zzfm -> 0x0012 }
            r1.zzd = r0     // Catch:{ zzfm -> 0x0012 }
            goto L_0x0018
        L_0x0012:
            r1.zzc = r2     // Catch:{ all -> 0x001a }
            com.google.android.gms.internal.measurement.zzdw r2 = com.google.android.gms.internal.measurement.zzdw.zza     // Catch:{ all -> 0x001a }
            r1.zzd = r2     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x001a:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            throw r2
        L_0x001d:
            com.google.android.gms.internal.measurement.zzgm r2 = r1.zzc
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzfv.zzb(com.google.android.gms.internal.measurement.zzgm):com.google.android.gms.internal.measurement.zzgm");
    }

    public final zzgm zza(zzgm zzgm) {
        zzgm zzgm2 = this.zzc;
        this.zzb = null;
        this.zzd = null;
        this.zzc = zzgm;
        return zzgm2;
    }

    public final int zzb() {
        if (this.zzd != null) {
            return this.zzd.zza();
        }
        if (this.zzc != null) {
            return this.zzc.zzbm();
        }
        return 0;
    }

    public final zzdw zzc() {
        if (this.zzd != null) {
            return this.zzd;
        }
        synchronized (this) {
            if (this.zzd != null) {
                zzdw zzdw = this.zzd;
                return zzdw;
            }
            if (this.zzc == null) {
                this.zzd = zzdw.zza;
            } else {
                this.zzd = this.zzc.zzbh();
            }
            zzdw zzdw2 = this.zzd;
            return zzdw2;
        }
    }
}
