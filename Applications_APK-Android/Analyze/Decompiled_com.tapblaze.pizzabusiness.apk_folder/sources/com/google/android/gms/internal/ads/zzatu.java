package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzatu implements zzaul {
    private final String zzcyz;

    zzatu(String str) {
        this.zzcyz = str;
    }

    public final void zza(zzbfq zzbfq) {
        zzbfq.beginAdUnitExposure(this.zzcyz);
    }
}
