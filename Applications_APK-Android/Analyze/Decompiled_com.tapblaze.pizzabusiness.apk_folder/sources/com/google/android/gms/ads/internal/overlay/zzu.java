package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzaos;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzu extends zzaos {
    private boolean zzdgt = false;
    private AdOverlayInfoParcel zzdia;
    private boolean zzdib = false;
    private Activity zzzk;

    public zzu(Activity activity, AdOverlayInfoParcel adOverlayInfoParcel) {
        this.zzdia = adOverlayInfoParcel;
        this.zzzk = activity;
    }

    public final void onActivityResult(int i, int i2, Intent intent) throws RemoteException {
    }

    public final void onBackPressed() throws RemoteException {
    }

    public final void onRestart() throws RemoteException {
    }

    public final void onStart() throws RemoteException {
    }

    public final void zzad(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final void zzdf() throws RemoteException {
    }

    public final boolean zztm() throws RemoteException {
        return false;
    }

    public final void onCreate(Bundle bundle) {
        boolean z = false;
        if (bundle != null && bundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false)) {
            z = true;
        }
        AdOverlayInfoParcel adOverlayInfoParcel = this.zzdia;
        if (adOverlayInfoParcel == null) {
            this.zzzk.finish();
        } else if (z) {
            this.zzzk.finish();
        } else {
            if (bundle == null) {
                if (adOverlayInfoParcel.zzcbt != null) {
                    this.zzdia.zzcbt.onAdClicked();
                }
                if (!(this.zzzk.getIntent() == null || !this.zzzk.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true) || this.zzdia.zzdhq == null)) {
                    this.zzdia.zzdhq.zztf();
                }
            }
            zzq.zzko();
            if (!zzb.zza(this.zzzk, this.zzdia.zzdhp, this.zzdia.zzdhu)) {
                this.zzzk.finish();
            }
        }
    }

    public final void onResume() throws RemoteException {
        if (this.zzdgt) {
            this.zzzk.finish();
            return;
        }
        this.zzdgt = true;
        if (this.zzdia.zzdhq != null) {
            this.zzdia.zzdhq.onResume();
        }
    }

    public final void onSaveInstanceState(Bundle bundle) throws RemoteException {
        bundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.zzdgt);
    }

    public final void onPause() throws RemoteException {
        if (this.zzdia.zzdhq != null) {
            this.zzdia.zzdhq.onPause();
        }
        if (this.zzzk.isFinishing()) {
            zztw();
        }
    }

    public final void onStop() throws RemoteException {
        if (this.zzzk.isFinishing()) {
            zztw();
        }
    }

    public final void onDestroy() throws RemoteException {
        if (this.zzzk.isFinishing()) {
            zztw();
        }
    }

    private final synchronized void zztw() {
        if (!this.zzdib) {
            if (this.zzdia.zzdhq != null) {
                this.zzdia.zzdhq.zzte();
            }
            this.zzdib = true;
        }
    }
}
