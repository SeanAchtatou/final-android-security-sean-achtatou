package com.google.android.gms.dynamite.descriptors.com.google.firebase.perf;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class ModuleDescriptor {
    public static final String MODULE_ID = "com.google.firebase.perf";
    public static final int MODULE_VERSION = 1;
}
