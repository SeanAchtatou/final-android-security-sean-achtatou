package com.google.android.gms.internal.p000firebaseperf;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhi  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhi implements Iterator<Map.Entry<K, V>> {
    private int pos;
    private final /* synthetic */ zzhg zzui;
    private Iterator<Map.Entry<K, V>> zzuj;

    private zzhi(zzhg zzhg) {
        this.zzui = zzhg;
        this.pos = this.zzui.zzud.size();
    }

    public final boolean hasNext() {
        int i = this.pos;
        return (i > 0 && i <= this.zzui.zzud.size()) || zzjb().hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    private final Iterator<Map.Entry<K, V>> zzjb() {
        if (this.zzuj == null) {
            this.zzuj = this.zzui.zzug.entrySet().iterator();
        }
        return this.zzuj;
    }

    public final /* synthetic */ Object next() {
        if (zzjb().hasNext()) {
            return (Map.Entry) zzjb().next();
        }
        List zzb = this.zzui.zzud;
        int i = this.pos - 1;
        this.pos = i;
        return (Map.Entry) zzb.get(i);
    }

    /* synthetic */ zzhi(zzhg zzhg, zzhf zzhf) {
        this(zzhg);
    }
}
