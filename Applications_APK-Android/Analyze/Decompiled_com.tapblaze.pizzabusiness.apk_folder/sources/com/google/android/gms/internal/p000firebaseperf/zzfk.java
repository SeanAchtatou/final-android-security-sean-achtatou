package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfk  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public interface zzfk extends zzfm<Integer> {
    int getInt(int i);

    zzfk zzak(int i);

    void zzal(int i);
}
