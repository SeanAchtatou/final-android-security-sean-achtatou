package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzao  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzao extends zzaz<Long> {
    private static zzao zzas;

    private zzao() {
    }

    /* access modifiers changed from: protected */
    public final String zzaf() {
        return "sessions_cpu_capture_frequency_bg_ms";
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.SessionsCpuCaptureFrequencyBackgroundMs";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_session_gauge_cpu_capture_frequency_bg_ms";
    }

    public static synchronized zzao zzaq() {
        zzao zzao;
        synchronized (zzao.class) {
            if (zzas == null) {
                zzas = new zzao();
            }
            zzao = zzas;
        }
        return zzao;
    }
}
