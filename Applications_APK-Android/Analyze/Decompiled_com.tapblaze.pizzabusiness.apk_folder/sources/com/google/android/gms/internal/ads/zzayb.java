package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzayb extends zzavo {
    private final String url;
    private final zzayy zzduv;

    public zzayb(Context context, String str, String str2) {
        this(str2, zzq.zzkq().zzr(context, str));
    }

    private zzayb(String str, String str2) {
        this.zzduv = new zzayy(str2);
        this.url = str;
    }

    public final void zztu() {
        this.zzduv.zzen(this.url);
    }
}
