package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcym implements zzcxo {
    static final zzcxo zzgjk = new zzcym();

    private zzcym() {
    }

    public final void zzt(Object obj) {
        ((zzaro) obj).onRewardedVideoAdClosed();
    }
}
