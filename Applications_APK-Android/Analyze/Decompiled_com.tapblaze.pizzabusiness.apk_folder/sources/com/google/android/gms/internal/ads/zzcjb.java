package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcjb implements Runnable {
    private final zzbdi zzehp;

    private zzcjb(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    static Runnable zzh(zzbdi zzbdi) {
        return new zzcjb(zzbdi);
    }

    public final void run() {
        this.zzehp.zzaan();
    }
}
