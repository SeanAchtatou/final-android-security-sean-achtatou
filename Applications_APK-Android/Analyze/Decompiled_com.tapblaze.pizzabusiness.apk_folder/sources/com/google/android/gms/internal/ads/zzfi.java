package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzfi extends zzfw {
    private long zzzw = -1;

    public zzfi(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 12);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        this.zzzt.zzap(-1);
        this.zzzt.zzap(((Long) this.zzaae.invoke(null, this.zzuv.getContext())).longValue());
    }
}
