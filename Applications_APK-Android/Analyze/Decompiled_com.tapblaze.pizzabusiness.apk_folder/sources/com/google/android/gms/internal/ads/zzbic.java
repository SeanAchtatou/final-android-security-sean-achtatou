package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbic implements zzdxg<zzayy> {
    private final zzdxp<String> zzfba;

    public zzbic(zzdxp<String> zzdxp) {
        this.zzfba = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzayy) zzdxm.zza(new zzayy(this.zzfba.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
