package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzc;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbdk implements Runnable {
    private final zzbdl zzeee;

    zzbdk(zzbdl zzbdl) {
        this.zzeee = zzbdl;
    }

    public final void run() {
        zzbdl zzbdl = this.zzeee;
        zzbdl.zzeef.zzaaj();
        zzc zzzw = zzbdl.zzeef.zzzw();
        if (zzzw != null) {
            zzzw.zztn();
        }
    }
}
