package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzzv {
    public static boolean zza(zzaae zzaae, zzaac zzaac, String... strArr) {
        if (zzaae == null || zzaac == null || !zzaae.zzcsb || zzaac == null) {
            return false;
        }
        return zzaae.zza(zzaac, zzq.zzkx().elapsedRealtime(), strArr);
    }

    public static zzaac zzb(zzaae zzaae) {
        if (zzaae == null) {
            return null;
        }
        return zzaae.zzex(zzq.zzkx().elapsedRealtime());
    }
}
