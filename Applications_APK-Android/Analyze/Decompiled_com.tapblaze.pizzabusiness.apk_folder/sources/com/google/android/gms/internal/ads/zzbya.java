package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbya implements zzdxg<zzbxr> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzbxa> zzeth;
    private final zzdxp<zzcaj> zzeuj;
    private final zzdxp<zzcbn> zzfog;
    private final zzdxp<zzbjq> zzfoh;

    public zzbya(zzdxp<Context> zzdxp, zzdxp<zzcbn> zzdxp2, zzdxp<zzcaj> zzdxp3, zzdxp<zzbjq> zzdxp4, zzdxp<zzbxa> zzdxp5) {
        this.zzejv = zzdxp;
        this.zzfog = zzdxp2;
        this.zzeuj = zzdxp3;
        this.zzfoh = zzdxp4;
        this.zzeth = zzdxp5;
    }

    public final /* synthetic */ Object get() {
        return new zzbxr(this.zzejv.get(), this.zzfog.get(), this.zzeuj.get(), this.zzfoh.get(), this.zzeth.get());
    }
}
