package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzazi<T> extends zzazl<T> {
    private final T zzdwn;

    public static <T> zzazi<T> zzl(T t) {
        return new zzazi<>(t);
    }

    private zzazi(T t) {
        this.zzdwn = t;
    }

    public final void zzxn() {
        set(this.zzdwn);
    }
}
