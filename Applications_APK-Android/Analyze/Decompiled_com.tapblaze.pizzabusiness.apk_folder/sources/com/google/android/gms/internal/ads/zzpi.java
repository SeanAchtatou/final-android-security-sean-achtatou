package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzpi implements Runnable {
    private final /* synthetic */ String zzahi;
    private final /* synthetic */ long zzahj;
    private final /* synthetic */ long zzahk;
    private final /* synthetic */ zzpg zzbjg;

    zzpi(zzpg zzpg, String str, long j, long j2) {
        this.zzbjg = zzpg;
        this.zzahi = str;
        this.zzahj = j;
        this.zzahk = j2;
    }

    public final void run() {
        this.zzbjg.zzbjh.zzd(this.zzahi, this.zzahj, this.zzahk);
    }
}
