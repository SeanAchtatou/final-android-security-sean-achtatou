package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdkf {
    @Deprecated
    public static final zzdny zzgyy = zzdny.zzawv();
    @Deprecated
    private static final zzdny zzgyz = zzdny.zzawv();
    @Deprecated
    private static final zzdny zzgza = zzdny.zzawv();
    private static final String zzgzo = new zzdkd().getKeyType();
    private static final String zzgzp = new zzdkc().getKeyType();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdiu, com.google.android.gms.internal.ads.zzdii, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzdkc, com.google.android.gms.internal.ads.zzdkd, int]
     candidates:
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdij, com.google.android.gms.internal.ads.zzdid, java.lang.Class):com.google.android.gms.internal.ads.zzdiq<P>
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, com.google.android.gms.internal.ads.zzdqk, java.lang.Class):P
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, com.google.android.gms.internal.ads.zzdte, java.lang.Class):P
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, byte[], java.lang.Class):P
      com.google.android.gms.internal.ads.zzdit.zza(java.lang.String, java.lang.Class<?>, boolean):void
      com.google.android.gms.internal.ads.zzdit.zza(com.google.android.gms.internal.ads.zzdiu, com.google.android.gms.internal.ads.zzdii, boolean):void */
    static {
        try {
            zzdiy.zzasq();
            zzdit.zza((zzdiu) new zzdkc(), (zzdii) new zzdkd(), true);
            zzdit.zza(new zzdkh());
            zzdit.zza(new zzdki());
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }
}
