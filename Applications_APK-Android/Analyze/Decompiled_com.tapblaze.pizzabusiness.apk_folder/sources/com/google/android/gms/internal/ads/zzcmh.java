package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcmh implements zzdgf {
    private final zzczl zzfzk;
    private final zzccd zzfzl;
    private final zzczt zzfzm;
    private final zzcmi zzgaq;

    zzcmh(zzcmi zzcmi, zzczl zzczl, zzccd zzccd, zzczt zzczt) {
        this.zzgaq = zzcmi;
        this.zzfzk = zzczl;
        this.zzfzl = zzccd;
        this.zzfzm = zzczt;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzgaq.zzb(this.zzfzk, this.zzfzl, this.zzfzm, obj);
    }
}
