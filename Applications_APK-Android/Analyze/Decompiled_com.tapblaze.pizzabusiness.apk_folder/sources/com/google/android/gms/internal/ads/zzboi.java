package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzboi implements zzdxg<String> {
    private final zzdxp<zzbnk> zzema;
    private final zzbod zzfhi;

    private zzboi(zzbod zzbod, zzdxp<zzbnk> zzdxp) {
        this.zzfhi = zzbod;
        this.zzema = zzdxp;
    }

    public static zzboi zzb(zzbod zzbod, zzdxp<zzbnk> zzdxp) {
        return new zzboi(zzbod, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza(this.zzema.get().zzuy(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
