package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zztf implements zzdsa {
    static final zzdsa zzew = new zztf();

    private zztf() {
    }

    public final boolean zzf(int i) {
        return zzte.zzbx(i) != null;
    }
}
