package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzddh;
import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zzddn extends zzdrt<zzddn, zzb> implements zzdtg {
    private static volatile zzdtn<zzddn> zzdz;
    /* access modifiers changed from: private */
    public static final zzddn zzgsw;
    private int zzdl;
    private String zzdm = "";
    private int zzgst;
    private String zzgsu = "";
    private zzddh zzgsv;

    /* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
    public enum zza implements zzdry {
        EVENT_TYPE_UNKNOWN(0),
        BLOCKED_IMPRESSION(1);
        
        private static final zzdrx<zza> zzen = new zzddp();
        private final int value;

        public final int zzae() {
            return this.value;
        }

        public static zza zzdq(int i) {
            if (i == 0) {
                return EVENT_TYPE_UNKNOWN;
            }
            if (i != 1) {
                return null;
            }
            return BLOCKED_IMPRESSION;
        }

        public static zzdsa zzaf() {
            return zzddo.zzew;
        }

        public final String toString() {
            return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
        }

        private zza(int i) {
            this.value = i;
        }
    }

    private zzddn() {
    }

    /* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
    public static final class zzb extends zzdrt.zzb<zzddn, zzb> implements zzdtg {
        private zzb() {
            super(zzddn.zzgsw);
        }

        public final zzb zzb(zza zza) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzddn) this.zzhmp).zza(zza);
            return this;
        }

        public final zzb zzgt(String str) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzddn) this.zzhmp).zzn(str);
            return this;
        }

        public final zzb zza(zzddh.zzb zzb) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzddn) this.zzhmp).zza((zzddh) ((zzdrt) zzb.zzbaf()));
            return this;
        }

        /* synthetic */ zzb(zzddm zzddm) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zza(zza zza2) {
        this.zzgst = zza2.zzae();
        this.zzdl |= 1;
    }

    /* access modifiers changed from: private */
    public final void zzn(String str) {
        str.getClass();
        this.zzdl |= 2;
        this.zzdm = str;
    }

    /* access modifiers changed from: private */
    public final void zza(zzddh zzddh) {
        zzddh.getClass();
        this.zzgsv = zzddh;
        this.zzdl |= 8;
    }

    public static zzb zzaqn() {
        return (zzb) zzgsw.zzazt();
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzddm.zzdk[i - 1]) {
            case 1:
                return new zzddn();
            case 2:
                return new zzb(null);
            case 3:
                return zza(zzgsw, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\f\u0000\u0002\b\u0001\u0003\b\u0002\u0004\t\u0003", new Object[]{"zzdl", "zzgst", zza.zzaf(), "zzdm", "zzgsu", "zzgsv"});
            case 4:
                return zzgsw;
            case 5:
                zzdtn<zzddn> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzddn.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzgsw);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzddn zzddn = new zzddn();
        zzgsw = zzddn;
        zzdrt.zza(zzddn.class, zzddn);
    }
}
