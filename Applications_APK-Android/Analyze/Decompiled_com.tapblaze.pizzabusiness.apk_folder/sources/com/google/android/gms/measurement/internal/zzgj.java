package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;

/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class zzgj implements Runnable {
    private final /* synthetic */ zzm zza;
    private final /* synthetic */ zzgk zzb;

    zzgj(zzgk zzgk, zzm zzm) {
        this.zzb = zzgk;
        this.zza = zzm;
    }

    public final void run() {
        this.zzb.zza.zzo();
        zzkj zza2 = this.zzb.zza;
        zzm zzm = this.zza;
        zza2.zzq().zzd();
        zza2.zzk();
        Preconditions.checkNotEmpty(zzm.zza);
        zza2.zzc(zzm);
    }
}
