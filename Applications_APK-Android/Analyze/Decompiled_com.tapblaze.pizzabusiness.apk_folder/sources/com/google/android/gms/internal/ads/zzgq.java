package com.google.android.gms.internal.ads;

import android.os.Looper;
import android.os.Message;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzgq extends zzddu {
    private final /* synthetic */ zzgr zzacp;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzgq(zzgr zzgr, Looper looper) {
        super(looper);
        this.zzacp = zzgr;
    }

    public final void handleMessage(Message message) {
        this.zzacp.zza(message);
    }
}
