package com.google.android.gms.internal.ads;

import cz.msebera.android.httpclient.protocol.HTTP;
import java.nio.charset.Charset;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzdeb {
    private static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    private static final Charset US_ASCII = Charset.forName("US-ASCII");
    private static final Charset UTF_16 = Charset.forName(HTTP.UTF_16);
    private static final Charset UTF_16BE = Charset.forName("UTF-16BE");
    private static final Charset UTF_16LE = Charset.forName("UTF-16LE");
    public static final Charset UTF_8 = Charset.forName("UTF-8");
}
