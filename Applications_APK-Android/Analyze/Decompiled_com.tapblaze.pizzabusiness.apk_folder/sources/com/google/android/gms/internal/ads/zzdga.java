package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdga implements Runnable {
    private final /* synthetic */ zzdfz zzgwa;
    private final /* synthetic */ zzdet zzgwh;

    zzdga(zzdfz zzdfz, zzdet zzdet) {
        this.zzgwa = zzdfz;
        this.zzgwh = zzdet;
    }

    public final void run() {
        this.zzgwa.zza(this.zzgwh);
    }
}
