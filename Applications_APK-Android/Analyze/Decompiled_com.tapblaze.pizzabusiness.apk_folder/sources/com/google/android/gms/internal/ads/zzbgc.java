package com.google.android.gms.internal.ads;

import android.content.Context;
import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbgc implements zzdxg<WeakReference<Context>> {
    private final zzbga zzejr;

    public zzbgc(zzbga zzbga) {
        this.zzejr = zzbga;
    }

    public final /* synthetic */ Object get() {
        return (WeakReference) zzdxm.zza(this.zzejr.zzacq(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
