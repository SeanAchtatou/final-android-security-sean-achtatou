package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbgl implements zzdxg<zzazb> {
    private final zzbga zzejr;

    public zzbgl(zzbga zzbga) {
        this.zzejr = zzbga;
    }

    public static zzazb zzb(zzbga zzbga) {
        return (zzazb) zzdxm.zza(zzbga.zzacr(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzb(this.zzejr);
    }
}
