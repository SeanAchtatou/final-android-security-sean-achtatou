package com.google.android.gms.internal.ads;

import java.io.Serializable;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzdej<T> implements Serializable {
    @NullableDecl
    public abstract T zzaqt();

    public static <T> zzdej<T> zzab(@NullableDecl T t) {
        if (t == null) {
            return zzddy.zzgtx;
        }
        return new zzdel(t);
    }

    zzdej() {
    }
}
