package com.google.android.gms.internal.ads;

import com.ironsource.mediationsdk.logger.IronSourceError;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzms {
    public final int length;
    private int zzafx;
    private final zzgw[] zzbbx;

    public zzms(zzgw... zzgwArr) {
        zzoc.checkState(zzgwArr.length > 0);
        this.zzbbx = zzgwArr;
        this.length = zzgwArr.length;
    }

    public final zzgw zzaw(int i) {
        return this.zzbbx[i];
    }

    public final int zzh(zzgw zzgw) {
        int i = 0;
        while (true) {
            zzgw[] zzgwArr = this.zzbbx;
            if (i >= zzgwArr.length) {
                return -1;
            }
            if (zzgw == zzgwArr[i]) {
                return i;
            }
            i++;
        }
    }

    public final int hashCode() {
        if (this.zzafx == 0) {
            this.zzafx = Arrays.hashCode(this.zzbbx) + IronSourceError.ERROR_NON_EXISTENT_INSTANCE;
        }
        return this.zzafx;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            zzms zzms = (zzms) obj;
            return this.length == zzms.length && Arrays.equals(this.zzbbx, zzms.zzbbx);
        }
    }
}
