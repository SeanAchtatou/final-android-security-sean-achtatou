package com.google.android.gms.internal.ads;

import android.os.IBinder;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzapg extends zzgc implements zzape {
    zzapg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.purchase.client.IPlayStorePurchaseListener");
    }
}
