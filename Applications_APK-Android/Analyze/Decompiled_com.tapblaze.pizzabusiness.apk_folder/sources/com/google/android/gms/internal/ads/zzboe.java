package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzboe<RequestComponentT> {
    zzboe<RequestComponentT> zza(zzcxw zzcxw);

    zzboe<RequestComponentT> zza(zzczt zzczt);

    RequestComponentT zzadg();
}
