package com.google.android.gms.internal.p000firebaseperf;

import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzho  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzho implements Iterator<Map.Entry<K, V>> {
    private int pos;
    private final /* synthetic */ zzhg zzui;
    private Iterator<Map.Entry<K, V>> zzuj;
    private boolean zzun;

    private zzho(zzhg zzhg) {
        this.zzui = zzhg;
        this.pos = -1;
    }

    public final boolean hasNext() {
        if (this.pos + 1 < this.zzui.zzud.size() || (!this.zzui.zzue.isEmpty() && zzjb().hasNext())) {
            return true;
        }
        return false;
    }

    public final void remove() {
        if (this.zzun) {
            this.zzun = false;
            this.zzui.zziz();
            if (this.pos < this.zzui.zzud.size()) {
                zzhg zzhg = this.zzui;
                int i = this.pos;
                this.pos = i - 1;
                Object unused = zzhg.zzav(i);
                return;
            }
            zzjb().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }

    private final Iterator<Map.Entry<K, V>> zzjb() {
        if (this.zzuj == null) {
            this.zzuj = this.zzui.zzue.entrySet().iterator();
        }
        return this.zzuj;
    }

    public final /* synthetic */ Object next() {
        this.zzun = true;
        int i = this.pos + 1;
        this.pos = i;
        if (i < this.zzui.zzud.size()) {
            return (Map.Entry) this.zzui.zzud.get(this.pos);
        }
        return (Map.Entry) zzjb().next();
    }

    /* synthetic */ zzho(zzhg zzhg, zzhf zzhf) {
        this(zzhg);
    }
}
