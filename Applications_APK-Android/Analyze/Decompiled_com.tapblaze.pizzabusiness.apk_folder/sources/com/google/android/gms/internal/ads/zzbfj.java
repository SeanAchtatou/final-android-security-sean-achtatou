package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbfj implements zzsp {
    private final int zzdtf;
    private final boolean zzeft;

    zzbfj(boolean z, int i) {
        this.zzeft = z;
        this.zzdtf = i;
    }

    public final void zza(zztu zztu) {
        zzbfg.zza(this.zzeft, this.zzdtf, zztu);
    }
}
