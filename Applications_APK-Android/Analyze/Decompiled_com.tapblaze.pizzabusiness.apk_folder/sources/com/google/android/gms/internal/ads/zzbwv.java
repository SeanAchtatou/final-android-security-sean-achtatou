package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbwv extends zzbww {
    private final boolean zzdcd;
    private final boolean zzdce;
    private final boolean zzdli;
    private final JSONObject zzfmn;
    private final boolean zzfmo;

    public zzbwv(zzczl zzczl, JSONObject jSONObject) {
        super(zzczl);
        boolean z = false;
        this.zzfmn = zzaxs.zza(jSONObject, "tracking_urls_and_actions", "active_view");
        this.zzdce = zzaxs.zza(false, jSONObject, "allow_pub_owned_ad_view");
        this.zzdcd = zzaxs.zza(false, jSONObject, "attribution", "allow_pub_rendering");
        this.zzdli = zzaxs.zza(false, jSONObject, "enable_omid");
        if (!(jSONObject == null || jSONObject.optJSONObject("overlay") == null)) {
            z = true;
        }
        this.zzfmo = z;
    }

    public final JSONObject zzajl() {
        JSONObject jSONObject = this.zzfmn;
        if (jSONObject != null) {
            return jSONObject;
        }
        try {
            return new JSONObject(this.zzfmp.zzdks);
        } catch (JSONException unused) {
            return null;
        }
    }

    public final boolean zzajm() {
        return this.zzfmo;
    }

    public final boolean zzajn() {
        return this.zzdce;
    }

    public final boolean zzaiw() {
        return this.zzdli;
    }

    public final boolean zzajo() {
        return this.zzdcd;
    }
}
