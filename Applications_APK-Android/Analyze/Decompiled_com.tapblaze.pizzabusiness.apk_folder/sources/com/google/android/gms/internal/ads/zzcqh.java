package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcqh implements zzcub<zzcqe> {
    private final zzave zzbmm;
    private final zzczu zzfgl;
    private final zzcub<zzcue> zzget;
    private final Context zzup;

    public zzcqh(zzcrk<zzcue> zzcrk, zzczu zzczu, Context context, zzave zzave) {
        this.zzget = zzcrk;
        this.zzfgl = zzczu;
        this.zzup = context;
        this.zzbmm = zzave;
    }

    public final zzdhe<zzcqe> zzanc() {
        return zzdgs.zzb(this.zzget.zzanc(), new zzcqg(this), zzazd.zzdwj);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcqe zza(zzcue zzcue) {
        boolean z;
        String str;
        int i;
        int i2;
        float f;
        String str2;
        int i3;
        int i4;
        int i5;
        DisplayMetrics displayMetrics;
        zzuj zzuj = this.zzfgl.zzblm;
        if (zzuj.zzccp != null) {
            str = null;
            boolean z2 = false;
            boolean z3 = false;
            z = false;
            for (zzuj zzuj2 : zzuj.zzccp) {
                if (!zzuj2.zzccq && !z2) {
                    str = zzuj2.zzabg;
                    z2 = true;
                }
                if (zzuj2.zzccq && !z3) {
                    z3 = true;
                    z = true;
                }
                if (z2 && z3) {
                    break;
                }
            }
        } else {
            str = zzuj.zzabg;
            z = zzuj.zzccq;
        }
        Resources resources = this.zzup.getResources();
        if (resources == null || (displayMetrics = resources.getDisplayMetrics()) == null) {
            str2 = null;
            f = 0.0f;
            i2 = 0;
            i = 0;
        } else {
            float f2 = displayMetrics.density;
            int i6 = displayMetrics.widthPixels;
            i = displayMetrics.heightPixels;
            str2 = this.zzbmm.zzvf().zzwg();
            i2 = i6;
            f = f2;
        }
        StringBuilder sb = new StringBuilder();
        if (zzuj.zzccp != null) {
            boolean z4 = false;
            for (zzuj zzuj3 : zzuj.zzccp) {
                if (zzuj3.zzccq) {
                    z4 = true;
                } else {
                    if (sb.length() != 0) {
                        sb.append("|");
                    }
                    if (zzuj3.width != -1 || f == 0.0f) {
                        i4 = zzuj3.width;
                    } else {
                        i4 = (int) (((float) zzuj3.widthPixels) / f);
                    }
                    sb.append(i4);
                    sb.append("x");
                    if (zzuj3.height == -2) {
                        if (f != 0.0f) {
                            i5 = (int) (((float) zzuj3.heightPixels) / f);
                            sb.append(i5);
                        }
                    }
                    i5 = zzuj3.height;
                    sb.append(i5);
                }
            }
            if (z4) {
                if (sb.length() != 0) {
                    i3 = 0;
                    sb.insert(0, "|");
                } else {
                    i3 = 0;
                }
                sb.insert(i3, "320x50");
            }
        }
        return new zzcqe(zzuj, str, z, sb.toString(), f, i2, i, str2);
    }
}
