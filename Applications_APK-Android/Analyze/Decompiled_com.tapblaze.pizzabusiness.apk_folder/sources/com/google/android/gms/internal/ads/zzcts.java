package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcts implements zzcub<zzctt> {
    private final zzazb zzbll;
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcts(zzdhd zzdhd, Context context, zzazb zzazb) {
        this.zzfov = zzdhd;
        this.zzup = context;
        this.zzbll = zzazb;
    }

    public final zzdhe<zzctt> zzanc() {
        return this.zzfov.zzd(new zzctv(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzctt zzanp() throws Exception {
        boolean isCallerInstantApp = Wrappers.packageManager(this.zzup).isCallerInstantApp();
        zzq.zzkq();
        boolean zzay = zzawb.zzay(this.zzup);
        String str = this.zzbll.zzbma;
        zzq.zzks();
        boolean zzwq = zzawh.zzwq();
        zzq.zzkq();
        return new zzctt(isCallerInstantApp, zzay, str, zzwq, zzawb.zzav(this.zzup), DynamiteModule.getRemoteVersion(this.zzup, ModuleDescriptor.MODULE_ID), DynamiteModule.getLocalVersion(this.zzup, ModuleDescriptor.MODULE_ID));
    }
}
