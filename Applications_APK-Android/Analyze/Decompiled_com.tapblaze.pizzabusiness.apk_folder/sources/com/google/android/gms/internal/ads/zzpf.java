package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzpf implements Runnable {
    private final /* synthetic */ zzit zzahe;
    private final /* synthetic */ zzpg zzbjg;

    zzpf(zzpg zzpg, zzit zzit) {
        this.zzbjg = zzpg;
        this.zzahe = zzit;
    }

    public final void run() {
        this.zzbjg.zzbjh.zze(this.zzahe);
    }
}
