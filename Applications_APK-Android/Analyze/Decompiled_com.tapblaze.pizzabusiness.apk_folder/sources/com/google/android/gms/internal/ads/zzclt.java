package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzclt implements zzdgf {
    private final zzczl zzfzk;
    private final zzczt zzfzm;
    private final zzclu zzgah;
    private final zzcio zzgai;

    zzclt(zzclu zzclu, zzczl zzczl, zzcio zzcio, zzczt zzczt) {
        this.zzgah = zzclu;
        this.zzfzk = zzczl;
        this.zzgai = zzcio;
        this.zzfzm = zzczt;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzgah.zza(this.zzfzk, this.zzgai, this.zzfzm, (Throwable) obj);
    }
}
