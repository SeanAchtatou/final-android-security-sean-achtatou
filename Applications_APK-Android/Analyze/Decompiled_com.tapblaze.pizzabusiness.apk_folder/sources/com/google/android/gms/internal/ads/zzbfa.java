package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzc;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbfa implements Runnable {
    private final zzbfb zzeht;

    zzbfa(zzbfb zzbfb) {
        this.zzeht = zzbfb;
    }

    public final void run() {
        zzbfb zzbfb = this.zzeht;
        zzbfb.zzeef.zzaaj();
        zzc zzzw = zzbfb.zzeef.zzzw();
        if (zzzw != null) {
            zzzw.zztn();
        }
    }
}
