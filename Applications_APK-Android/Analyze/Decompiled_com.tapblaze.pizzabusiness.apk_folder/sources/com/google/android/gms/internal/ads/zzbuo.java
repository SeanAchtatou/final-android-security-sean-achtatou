package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.gass.zzf;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbuo implements zzdxg<zzbtu> {
    public static zzbtu zza(zzbmg zzbmg, Context context, zzbdi zzbdi, zzbsk zzbsk, zzbuv zzbuv, zzbmx zzbmx, zzf zzf) {
        return new zzbtu(zzbmg, context, zzbdi, zzbsk, zzbuv, zzbmx, zzf);
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
