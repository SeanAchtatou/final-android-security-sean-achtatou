package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzcbi implements zzbob<zzcbb> {
    public abstract zzcbd zza(zzbmt zzbmt, zzcbg zzcbg);

    public abstract zzbmz<zzcbb> zzadc();

    public abstract zzbou zzadd();
}
