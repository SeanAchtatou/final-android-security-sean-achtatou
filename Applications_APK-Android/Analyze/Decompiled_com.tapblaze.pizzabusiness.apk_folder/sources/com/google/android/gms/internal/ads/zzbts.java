package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbts implements zzro {
    private zzddz zzfiw;

    public final void zza(Activity activity, WebView webView) {
        try {
            this.zzfiw = new zzddz(activity, webView);
        } catch (RuntimeException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 46);
            sb.append(" Failed to initialize the internal ArWebView: ");
            sb.append(valueOf);
            zzavs.zzex(sb.toString());
        }
    }

    public final void zzc(String str, String str2) {
        if (this.zzfiw == null) {
            zzavs.zzex("ArWebView is not initialized.");
        } else {
            zzddz.getWebView().loadDataWithBaseURL(str, str2, "text/html", "UTF-8", null);
        }
    }

    public final WebView getWebView() {
        if (this.zzfiw == null) {
            return null;
        }
        return zzddz.getWebView();
    }

    public final View getView() {
        return this.zzfiw;
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this.zzfiw != null) {
            zzddz.onPageStarted(webView, str, bitmap);
        }
    }
}
