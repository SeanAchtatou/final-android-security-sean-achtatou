package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaqm extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaqm> CREATOR = new zzaqp();
    String zzdlz;

    public zzaqm(String str) {
        this.zzdlz = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.zzdlz, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
