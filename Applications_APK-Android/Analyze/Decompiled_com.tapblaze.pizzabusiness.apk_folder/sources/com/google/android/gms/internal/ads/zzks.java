package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzks {
    public final int id;
    public final int type;
    public final zzgw zzafz;
    public final long zzagj;
    public final int zzaqu;
    public final long zzaxi;
    public final int zzaxj;
    private final zzkr[] zzaxk;
    public final long[] zzaxl;
    public final long[] zzaxm;
    public final long zzcv;

    public zzks(int i, int i2, long j, long j2, long j3, zzgw zzgw, int i3, zzkr[] zzkrArr, int i4, long[] jArr, long[] jArr2) {
        this.id = i;
        this.type = i2;
        this.zzcv = j;
        this.zzaxi = j2;
        this.zzagj = j3;
        this.zzafz = zzgw;
        this.zzaxj = i3;
        this.zzaxk = zzkrArr;
        this.zzaqu = i4;
        this.zzaxl = jArr;
        this.zzaxm = jArr2;
    }
}
