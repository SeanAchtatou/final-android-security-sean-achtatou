package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zztx extends zzvf {
    private final zzty zzcbt;

    public zztx(zzty zzty) {
        this.zzcbt = zzty;
    }

    public final void onAdClicked() {
        this.zzcbt.onAdClicked();
    }
}
