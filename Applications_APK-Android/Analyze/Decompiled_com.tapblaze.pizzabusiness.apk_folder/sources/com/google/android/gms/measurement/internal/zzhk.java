package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.collection.ArrayMap;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.common.api.internal.GoogleServices;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.internal.measurement.zzkj;
import com.google.android.gms.internal.measurement.zzlc;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzhk extends zze {
    protected zzid zza;
    protected boolean zzb = true;
    private zzhf zzc;
    private final Set<zzhi> zzd = new CopyOnWriteArraySet();
    private boolean zze;
    private final AtomicReference<String> zzf = new AtomicReference<>();

    protected zzhk(zzgf zzgf) {
        super(zzgf);
    }

    /* access modifiers changed from: protected */
    public final boolean zzz() {
        return false;
    }

    public final void zzab() {
        if (zzn().getApplicationContext() instanceof Application) {
            ((Application) zzn().getApplicationContext()).unregisterActivityLifecycleCallbacks(this.zza);
        }
    }

    public final Boolean zzac() {
        AtomicReference atomicReference = new AtomicReference();
        return (Boolean) zzq().zza(atomicReference, 15000, "boolean test flag value", new zzhm(this, atomicReference));
    }

    public final String zzad() {
        AtomicReference atomicReference = new AtomicReference();
        return (String) zzq().zza(atomicReference, 15000, "String test flag value", new zzhw(this, atomicReference));
    }

    public final Long zzae() {
        AtomicReference atomicReference = new AtomicReference();
        return (Long) zzq().zza(atomicReference, 15000, "long test flag value", new zzhy(this, atomicReference));
    }

    public final Integer zzaf() {
        AtomicReference atomicReference = new AtomicReference();
        return (Integer) zzq().zza(atomicReference, 15000, "int test flag value", new zzhx(this, atomicReference));
    }

    public final Double zzag() {
        AtomicReference atomicReference = new AtomicReference();
        return (Double) zzq().zza(atomicReference, 15000, "double test flag value", new zzia(this, atomicReference));
    }

    public final void zza(boolean z) {
        zzw();
        zzb();
        zzq().zza(new zzhz(this, z));
    }

    public final void zzb(boolean z) {
        zzw();
        zzb();
        zzq().zza(new zzic(this, z));
    }

    /* access modifiers changed from: private */
    public final void zzd(boolean z) {
        zzd();
        zzb();
        zzw();
        zzr().zzw().zza("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        zzs().zzb(z);
        zzam();
    }

    /* access modifiers changed from: private */
    public final void zzam() {
        if (zzt().zza(zzap.zzba)) {
            zzd();
            String zza2 = zzs().zzn.zza();
            if (zza2 != null) {
                if ("unset".equals(zza2)) {
                    zza(SettingsJsonConstants.APP_KEY, "_npa", (Object) null, zzm().currentTimeMillis());
                } else {
                    zza(SettingsJsonConstants.APP_KEY, "_npa", Long.valueOf(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(zza2) ? 1 : 0), zzm().currentTimeMillis());
                }
            }
        }
        if (!this.zzx.zzab() || !this.zzb) {
            zzr().zzw().zza("Updating Scion state (FE)");
            zzh().zzac();
            return;
        }
        zzr().zzw().zza("Recording app launch after enabling measurement for the first time (FE)");
        zzai();
        if (zzlc.zzb() && zzt().zza(zzap.zzcp)) {
            zzk().zza.zza();
        }
        if (zzkj.zzb() && zzt().zza(zzap.zzcv)) {
            if (!(this.zzx.zzf().zza.zzc().zzi.zza() > 0)) {
                this.zzx.zzf().zza();
            }
        }
    }

    public final void zza(long j) {
        zzb();
        zzq().zza(new zzib(this, j));
    }

    public final void zzb(long j) {
        zzb();
        zzq().zza(new zzie(this, j));
    }

    public final void zza(String str, String str2, Bundle bundle, boolean z) {
        zza(str, str2, bundle, false, true, zzm().currentTimeMillis());
    }

    public final void zza(String str, String str2, Bundle bundle) {
        zza(str, str2, bundle, true, true, zzm().currentTimeMillis());
    }

    /* access modifiers changed from: package-private */
    public final void zzb(String str, String str2, Bundle bundle) {
        zzb();
        zzd();
        zza(str, str2, zzm().currentTimeMillis(), bundle);
    }

    /* access modifiers changed from: package-private */
    public final void zza(String str, String str2, long j, Bundle bundle) {
        zzb();
        zzd();
        zza(str, str2, j, bundle, true, this.zzc == null || zzkv.zze(str2), false, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.zzkv.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzkv.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzin.zza(com.google.android.gms.measurement.internal.zzio, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzio, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.zzin.zza(android.app.Activity, com.google.android.gms.measurement.internal.zzio, boolean):void
      com.google.android.gms.measurement.internal.zzin.zza(com.google.android.gms.measurement.internal.zzio, boolean, long):void
      com.google.android.gms.measurement.internal.zzin.zza(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.zzin.zza(com.google.android.gms.measurement.internal.zzio, android.os.Bundle, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x0423  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x04d9  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04ea  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0115  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(java.lang.String r27, java.lang.String r28, long r29, android.os.Bundle r31, boolean r32, boolean r33, boolean r34, java.lang.String r35) {
        /*
            r26 = this;
            r7 = r26
            r8 = r27
            r15 = r28
            r13 = r29
            r12 = r31
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r27)
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r31)
            r26.zzd()
            r26.zzw()
            com.google.android.gms.measurement.internal.zzgf r0 = r7.zzx
            boolean r0 = r0.zzab()
            if (r0 != 0) goto L_0x002c
            com.google.android.gms.measurement.internal.zzfb r0 = r26.zzr()
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzw()
            java.lang.String r1 = "Event not sent since app measurement is disabled"
            r0.zza(r1)
            return
        L_0x002c:
            com.google.android.gms.measurement.internal.zzx r0 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.zzap.zzbj
            boolean r0 = r0.zza(r1)
            if (r0 == 0) goto L_0x0056
            com.google.android.gms.measurement.internal.zzey r0 = r26.zzg()
            java.util.List r0 = r0.zzah()
            if (r0 == 0) goto L_0x0056
            boolean r0 = r0.contains(r15)
            if (r0 != 0) goto L_0x0056
            com.google.android.gms.measurement.internal.zzfb r0 = r26.zzr()
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzw()
            java.lang.String r1 = "Dropping non-safelisted event. event name, origin"
            r0.zza(r1, r15, r8)
            return
        L_0x0056:
            boolean r0 = r7.zze
            r11 = 0
            r16 = 0
            r10 = 1
            if (r0 != 0) goto L_0x00af
            r7.zze = r10
            com.google.android.gms.measurement.internal.zzgf r0 = r7.zzx     // Catch:{ ClassNotFoundException -> 0x00a2 }
            boolean r0 = r0.zzt()     // Catch:{ ClassNotFoundException -> 0x00a2 }
            java.lang.String r1 = "com.google.android.gms.tagmanager.TagManagerService"
            if (r0 != 0) goto L_0x0077
            android.content.Context r0 = r26.zzn()     // Catch:{ ClassNotFoundException -> 0x00a2 }
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x00a2 }
            java.lang.Class r0 = java.lang.Class.forName(r1, r10, r0)     // Catch:{ ClassNotFoundException -> 0x00a2 }
            goto L_0x007b
        L_0x0077:
            java.lang.Class r0 = java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x00a2 }
        L_0x007b:
            java.lang.String r1 = "initialize"
            java.lang.Class[] r2 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x0093 }
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            r2[r16] = r3     // Catch:{ Exception -> 0x0093 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x0093 }
            java.lang.Object[] r1 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x0093 }
            android.content.Context r2 = r26.zzn()     // Catch:{ Exception -> 0x0093 }
            r1[r16] = r2     // Catch:{ Exception -> 0x0093 }
            r0.invoke(r11, r1)     // Catch:{ Exception -> 0x0093 }
            goto L_0x00af
        L_0x0093:
            r0 = move-exception
            com.google.android.gms.measurement.internal.zzfb r1 = r26.zzr()     // Catch:{ ClassNotFoundException -> 0x00a2 }
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzi()     // Catch:{ ClassNotFoundException -> 0x00a2 }
            java.lang.String r2 = "Failed to invoke Tag Manager's initialize() method"
            r1.zza(r2, r0)     // Catch:{ ClassNotFoundException -> 0x00a2 }
            goto L_0x00af
        L_0x00a2:
            com.google.android.gms.measurement.internal.zzfb r0 = r26.zzr()
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzv()
            java.lang.String r1 = "Tag Manager is not found and thus will not be used"
            r0.zza(r1)
        L_0x00af:
            com.google.android.gms.measurement.internal.zzx r0 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.zzap.zzby
            boolean r0 = r0.zza(r1)
            if (r0 == 0) goto L_0x00e0
            java.lang.String r0 = "_cmp"
            boolean r0 = r0.equals(r15)
            if (r0 == 0) goto L_0x00e0
            java.lang.String r0 = "gclid"
            boolean r1 = r12.containsKey(r0)
            if (r1 == 0) goto L_0x00e0
            java.lang.String r4 = r12.getString(r0)
            com.google.android.gms.common.util.Clock r0 = r26.zzm()
            long r5 = r0.currentTimeMillis()
            java.lang.String r2 = "auto"
            java.lang.String r3 = "_lgclid"
            r1 = r26
            r1.zza(r2, r3, r4, r5)
        L_0x00e0:
            r0 = 40
            r1 = 2
            if (r34 == 0) goto L_0x0149
            r26.zzu()
            java.lang.String r2 = "_iap"
            boolean r2 = r2.equals(r15)
            if (r2 != 0) goto L_0x0149
            com.google.android.gms.measurement.internal.zzgf r2 = r7.zzx
            com.google.android.gms.measurement.internal.zzkv r2 = r2.zzi()
            java.lang.String r3 = "event"
            boolean r4 = r2.zza(r3, r15)
            if (r4 != 0) goto L_0x0100
        L_0x00fe:
            r2 = 2
            goto L_0x0113
        L_0x0100:
            java.lang.String[] r4 = com.google.android.gms.measurement.internal.zzhe.zza
            boolean r4 = r2.zza(r3, r4, r15)
            if (r4 != 0) goto L_0x010b
            r2 = 13
            goto L_0x0113
        L_0x010b:
            boolean r2 = r2.zza(r3, r0, r15)
            if (r2 != 0) goto L_0x0112
            goto L_0x00fe
        L_0x0112:
            r2 = 0
        L_0x0113:
            if (r2 == 0) goto L_0x0149
            com.google.android.gms.measurement.internal.zzfb r1 = r26.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzh()
            com.google.android.gms.measurement.internal.zzez r3 = r26.zzo()
            java.lang.String r3 = r3.zza(r15)
            java.lang.String r4 = "Invalid public event name. Event will not be logged (FE)"
            r1.zza(r4, r3)
            com.google.android.gms.measurement.internal.zzgf r1 = r7.zzx
            r1.zzi()
            java.lang.String r0 = com.google.android.gms.measurement.internal.zzkv.zza(r15, r0, r10)
            if (r15 == 0) goto L_0x013c
            int r16 = r28.length()
            r1 = r16
            goto L_0x013d
        L_0x013c:
            r1 = 0
        L_0x013d:
            com.google.android.gms.measurement.internal.zzgf r3 = r7.zzx
            com.google.android.gms.measurement.internal.zzkv r3 = r3.zzi()
            java.lang.String r4 = "_ev"
            r3.zza(r2, r4, r0, r1)
            return
        L_0x0149:
            r26.zzu()
            com.google.android.gms.measurement.internal.zzin r2 = r26.zzi()
            com.google.android.gms.measurement.internal.zzio r2 = r2.zzab()
            java.lang.String r3 = "_sc"
            if (r2 == 0) goto L_0x0160
            boolean r4 = r12.containsKey(r3)
            if (r4 != 0) goto L_0x0160
            r2.zzd = r10
        L_0x0160:
            if (r32 == 0) goto L_0x0166
            if (r34 == 0) goto L_0x0166
            r4 = 1
            goto L_0x0167
        L_0x0166:
            r4 = 0
        L_0x0167:
            com.google.android.gms.measurement.internal.zzin.zza(r2, r12, r4)
            java.lang.String r4 = "am"
            boolean r17 = r4.equals(r8)
            boolean r4 = com.google.android.gms.measurement.internal.zzkv.zze(r28)
            if (r32 == 0) goto L_0x01a9
            com.google.android.gms.measurement.internal.zzhf r5 = r7.zzc
            if (r5 == 0) goto L_0x01a9
            if (r4 != 0) goto L_0x01a9
            if (r17 != 0) goto L_0x01a9
            com.google.android.gms.measurement.internal.zzfb r0 = r26.zzr()
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzw()
            com.google.android.gms.measurement.internal.zzez r1 = r26.zzo()
            java.lang.String r1 = r1.zza(r15)
            com.google.android.gms.measurement.internal.zzez r2 = r26.zzo()
            java.lang.String r2 = r2.zza(r12)
            java.lang.String r3 = "Passing event to registered event handler (FE)"
            r0.zza(r3, r1, r2)
            com.google.android.gms.measurement.internal.zzhf r1 = r7.zzc
            r2 = r27
            r3 = r28
            r4 = r31
            r5 = r29
            r1.interceptEvent(r2, r3, r4, r5)
            return
        L_0x01a9:
            com.google.android.gms.measurement.internal.zzgf r4 = r7.zzx
            boolean r4 = r4.zzah()
            if (r4 != 0) goto L_0x01b2
            return
        L_0x01b2:
            com.google.android.gms.measurement.internal.zzkv r4 = r26.zzp()
            int r4 = r4.zzb(r15)
            if (r4 == 0) goto L_0x01f6
            com.google.android.gms.measurement.internal.zzfb r1 = r26.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzh()
            com.google.android.gms.measurement.internal.zzez r2 = r26.zzo()
            java.lang.String r2 = r2.zza(r15)
            java.lang.String r3 = "Invalid event name. Event will not be logged (FE)"
            r1.zza(r3, r2)
            r26.zzp()
            java.lang.String r0 = com.google.android.gms.measurement.internal.zzkv.zza(r15, r0, r10)
            if (r15 == 0) goto L_0x01de
            int r16 = r28.length()
        L_0x01de:
            com.google.android.gms.measurement.internal.zzgf r1 = r7.zzx
            com.google.android.gms.measurement.internal.zzkv r1 = r1.zzi()
            java.lang.String r2 = "_ev"
            r27 = r1
            r28 = r35
            r29 = r4
            r30 = r2
            r31 = r0
            r32 = r16
            r27.zza(r28, r29, r30, r31, r32)
            return
        L_0x01f6:
            r0 = 4
            java.lang.String[] r0 = new java.lang.String[r0]
            java.lang.String r5 = "_o"
            r0[r16] = r5
            java.lang.String r4 = "_sn"
            r0[r10] = r4
            r0[r1] = r3
            r1 = 3
            java.lang.String r6 = "_si"
            r0[r1] = r6
            java.util.List r0 = com.google.android.gms.common.util.CollectionUtils.listOf(r0)
            com.google.android.gms.measurement.internal.zzkv r9 = r26.zzp()
            r1 = 1
            r10 = r35
            r19 = r11
            r11 = r28
            r12 = r31
            r13 = r0
            r14 = r34
            r7 = r15
            r15 = r1
            android.os.Bundle r15 = r9.zza(r10, r11, r12, r13, r14, r15)
            if (r15 == 0) goto L_0x024b
            boolean r1 = r15.containsKey(r3)
            if (r1 == 0) goto L_0x024b
            boolean r1 = r15.containsKey(r6)
            if (r1 != 0) goto L_0x0231
            goto L_0x024b
        L_0x0231:
            java.lang.String r1 = r15.getString(r4)
            java.lang.String r3 = r15.getString(r3)
            long r9 = r15.getLong(r6)
            java.lang.Long r4 = java.lang.Long.valueOf(r9)
            com.google.android.gms.measurement.internal.zzio r11 = new com.google.android.gms.measurement.internal.zzio
            long r9 = r4.longValue()
            r11.<init>(r1, r3, r9)
            goto L_0x024d
        L_0x024b:
            r11 = r19
        L_0x024d:
            if (r11 != 0) goto L_0x0251
            r14 = r2
            goto L_0x0252
        L_0x0251:
            r14 = r11
        L_0x0252:
            com.google.android.gms.measurement.internal.zzx r1 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzap.zzax
            boolean r1 = r1.zza(r2)
            r9 = 0
            java.lang.String r13 = "_ae"
            if (r1 == 0) goto L_0x028a
            r26.zzu()
            com.google.android.gms.measurement.internal.zzin r1 = r26.zzi()
            com.google.android.gms.measurement.internal.zzio r1 = r1.zzab()
            if (r1 == 0) goto L_0x028a
            boolean r1 = r13.equals(r7)
            if (r1 == 0) goto L_0x028a
            com.google.android.gms.measurement.internal.zzjt r1 = r26.zzk()
            com.google.android.gms.measurement.internal.zzkb r1 = r1.zzb
            long r1 = r1.zzb()
            int r3 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r3 <= 0) goto L_0x028a
            com.google.android.gms.measurement.internal.zzkv r3 = r26.zzp()
            r3.zza(r15, r1)
        L_0x028a:
            boolean r1 = com.google.android.gms.internal.measurement.zzjy.zzb()
            if (r1 == 0) goto L_0x030c
            com.google.android.gms.measurement.internal.zzx r1 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzap.zzco
            boolean r1 = r1.zza(r2)
            if (r1 == 0) goto L_0x030c
            java.lang.String r1 = "auto"
            boolean r1 = r1.equals(r8)
            java.lang.String r2 = "_ffr"
            if (r1 != 0) goto L_0x02ef
            java.lang.String r1 = "_ssr"
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L_0x02ef
            com.google.android.gms.measurement.internal.zzkv r1 = r26.zzp()
            java.lang.String r2 = r15.getString(r2)
            boolean r3 = com.google.android.gms.common.util.Strings.isEmptyOrWhitespace(r2)
            if (r3 == 0) goto L_0x02bf
            r11 = r19
            goto L_0x02c3
        L_0x02bf:
            java.lang.String r11 = r2.trim()
        L_0x02c3:
            com.google.android.gms.measurement.internal.zzfo r2 = r1.zzs()
            com.google.android.gms.measurement.internal.zzfr r2 = r2.zzw
            java.lang.String r2 = r2.zza()
            boolean r2 = com.google.android.gms.measurement.internal.zzkv.zzc(r11, r2)
            if (r2 == 0) goto L_0x02e2
            com.google.android.gms.measurement.internal.zzfb r1 = r1.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzw()
            java.lang.String r2 = "Not logging duplicate session_start_with_rollout event"
            r1.zza(r2)
            r1 = 0
            goto L_0x02ec
        L_0x02e2:
            com.google.android.gms.measurement.internal.zzfo r1 = r1.zzs()
            com.google.android.gms.measurement.internal.zzfr r1 = r1.zzw
            r1.zza(r11)
            r1 = 1
        L_0x02ec:
            if (r1 != 0) goto L_0x030c
            return
        L_0x02ef:
            boolean r1 = r13.equals(r7)
            if (r1 == 0) goto L_0x030c
            com.google.android.gms.measurement.internal.zzkv r1 = r26.zzp()
            com.google.android.gms.measurement.internal.zzfo r1 = r1.zzs()
            com.google.android.gms.measurement.internal.zzfr r1 = r1.zzw
            java.lang.String r1 = r1.zza()
            boolean r3 = android.text.TextUtils.isEmpty(r1)
            if (r3 != 0) goto L_0x030c
            r15.putString(r2, r1)
        L_0x030c:
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
            r12.add(r15)
            com.google.android.gms.measurement.internal.zzkv r1 = r26.zzp()
            java.security.SecureRandom r1 = r1.zzh()
            long r3 = r1.nextLong()
            com.google.android.gms.measurement.internal.zzx r1 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzap.zzar
            boolean r1 = r1.zza(r2)
            if (r1 == 0) goto L_0x03c9
            com.google.android.gms.measurement.internal.zzfo r1 = r26.zzs()
            com.google.android.gms.measurement.internal.zzfp r1 = r1.zzq
            long r1 = r1.zza()
            int r6 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r6 <= 0) goto L_0x03c9
            com.google.android.gms.measurement.internal.zzfo r1 = r26.zzs()
            r9 = r29
            boolean r1 = r1.zza(r9)
            if (r1 == 0) goto L_0x03cb
            com.google.android.gms.measurement.internal.zzfo r1 = r26.zzs()
            com.google.android.gms.measurement.internal.zzfq r1 = r1.zzt
            boolean r1 = r1.zza()
            if (r1 == 0) goto L_0x03cb
            com.google.android.gms.measurement.internal.zzfb r1 = r26.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzx()
            java.lang.String r2 = "Current session is expired, remove the session number, ID, and engagement time"
            r1.zza(r2)
            com.google.android.gms.measurement.internal.zzx r1 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzap.zzao
            boolean r1 = r1.zza(r2)
            if (r1 == 0) goto L_0x0384
            r6 = 0
            com.google.android.gms.common.util.Clock r1 = r26.zzm()
            long r21 = r1.currentTimeMillis()
            java.lang.String r2 = "auto"
            java.lang.String r11 = "_sid"
            r1 = r26
            r7 = r3
            r3 = r11
            r4 = r6
            r11 = r5
            r5 = r21
            r1.zza(r2, r3, r4, r5)
            goto L_0x0386
        L_0x0384:
            r7 = r3
            r11 = r5
        L_0x0386:
            com.google.android.gms.measurement.internal.zzx r1 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzap.zzap
            boolean r1 = r1.zza(r2)
            if (r1 == 0) goto L_0x03a4
            r4 = 0
            com.google.android.gms.common.util.Clock r1 = r26.zzm()
            long r5 = r1.currentTimeMillis()
            java.lang.String r2 = "auto"
            java.lang.String r3 = "_sno"
            r1 = r26
            r1.zza(r2, r3, r4, r5)
        L_0x03a4:
            boolean r1 = com.google.android.gms.internal.measurement.zzms.zzb()
            if (r1 == 0) goto L_0x03cd
            com.google.android.gms.measurement.internal.zzx r1 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzap.zzbo
            boolean r1 = r1.zza(r2)
            if (r1 == 0) goto L_0x03cd
            r4 = 0
            com.google.android.gms.common.util.Clock r1 = r26.zzm()
            long r5 = r1.currentTimeMillis()
            java.lang.String r2 = "auto"
            java.lang.String r3 = "_se"
            r1 = r26
            r1.zza(r2, r3, r4, r5)
            goto L_0x03cd
        L_0x03c9:
            r9 = r29
        L_0x03cb:
            r7 = r3
            r11 = r5
        L_0x03cd:
            com.google.android.gms.measurement.internal.zzx r1 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzap.zzaq
            boolean r1 = r1.zza(r2)
            if (r1 == 0) goto L_0x0405
            java.lang.String r1 = "extend_session"
            r2 = 0
            long r1 = r15.getLong(r1, r2)
            r3 = 1
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x0405
            com.google.android.gms.measurement.internal.zzfb r1 = r26.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzx()
            java.lang.String r2 = "EXTEND_SESSION param attached: initiate a new session or extend the current active session"
            r1.zza(r2)
            r5 = r26
            r6 = r28
            com.google.android.gms.measurement.internal.zzgf r1 = r5.zzx
            com.google.android.gms.measurement.internal.zzjt r1 = r1.zze()
            com.google.android.gms.measurement.internal.zzkd r1 = r1.zza
            r4 = 1
            r1.zza(r9, r4)
            goto L_0x040a
        L_0x0405:
            r4 = 1
            r5 = r26
            r6 = r28
        L_0x040a:
            java.util.Set r1 = r15.keySet()
            int r2 = r31.size()
            java.lang.String[] r2 = new java.lang.String[r2]
            java.lang.Object[] r1 = r1.toArray(r2)
            java.lang.String[] r1 = (java.lang.String[]) r1
            java.util.Arrays.sort(r1)
            int r2 = r1.length
            r3 = 0
            r23 = 0
        L_0x0421:
            if (r3 >= r2) goto L_0x04ce
            r4 = r1[r3]
            java.lang.Object r19 = r15.get(r4)
            r26.zzp()
            r31 = r1
            android.os.Bundle[] r1 = com.google.android.gms.measurement.internal.zzkv.zza(r19)
            if (r1 == 0) goto L_0x04a9
            r19 = r2
            int r2 = r1.length
            r15.putInt(r4, r2)
            r2 = 0
        L_0x043b:
            int r5 = r1.length
            if (r2 >= r5) goto L_0x0498
            r5 = r1[r2]
            r32 = r15
            r15 = 1
            com.google.android.gms.measurement.internal.zzin.zza(r14, r5, r15)
            com.google.android.gms.measurement.internal.zzkv r18 = r26.zzp()
            r20 = 0
            java.lang.String r21 = "_ep"
            r9 = r18
            r10 = r35
            r24 = r11
            r11 = r21
            r25 = r12
            r12 = r5
            r5 = r13
            r13 = r0
            r18 = r14
            r14 = r34
            r21 = r0
            r22 = 1
            r0 = r32
            r15 = r20
            android.os.Bundle r9 = r9.zza(r10, r11, r12, r13, r14, r15)
            java.lang.String r10 = "_en"
            r9.putString(r10, r6)
            java.lang.String r10 = "_eid"
            r9.putLong(r10, r7)
            java.lang.String r10 = "_gn"
            r9.putString(r10, r4)
            int r10 = r1.length
            java.lang.String r11 = "_ll"
            r9.putInt(r11, r10)
            java.lang.String r10 = "_i"
            r9.putInt(r10, r2)
            r10 = r25
            r10.add(r9)
            int r2 = r2 + 1
            r15 = r0
            r13 = r5
            r12 = r10
            r14 = r18
            r0 = r21
            r11 = r24
            r9 = r29
            goto L_0x043b
        L_0x0498:
            r21 = r0
            r24 = r11
            r10 = r12
            r5 = r13
            r18 = r14
            r0 = r15
            r22 = 1
            int r1 = r1.length
            r2 = r23
            int r23 = r2 + r1
            goto L_0x04b8
        L_0x04a9:
            r21 = r0
            r19 = r2
            r24 = r11
            r10 = r12
            r5 = r13
            r18 = r14
            r0 = r15
            r2 = r23
            r22 = 1
        L_0x04b8:
            int r3 = r3 + 1
            r1 = r31
            r15 = r0
            r13 = r5
            r12 = r10
            r14 = r18
            r2 = r19
            r0 = r21
            r11 = r24
            r4 = 1
            r5 = r26
            r9 = r29
            goto L_0x0421
        L_0x04ce:
            r24 = r11
            r10 = r12
            r5 = r13
            r0 = r15
            r2 = r23
            r22 = 1
            if (r2 == 0) goto L_0x04e3
            java.lang.String r1 = "_eid"
            r0.putLong(r1, r7)
            java.lang.String r1 = "_epc"
            r0.putInt(r1, r2)
        L_0x04e3:
            r0 = 0
        L_0x04e4:
            int r1 = r10.size()
            if (r0 >= r1) goto L_0x058a
            java.lang.Object r1 = r10.get(r0)
            android.os.Bundle r1 = (android.os.Bundle) r1
            if (r0 == 0) goto L_0x04f4
            r2 = 1
            goto L_0x04f5
        L_0x04f4:
            r2 = 0
        L_0x04f5:
            if (r2 == 0) goto L_0x04fc
            java.lang.String r2 = "_ep"
            r7 = r27
            goto L_0x04ff
        L_0x04fc:
            r7 = r27
            r2 = r6
        L_0x04ff:
            r8 = r24
            r1.putString(r8, r7)
            if (r33 == 0) goto L_0x050e
            com.google.android.gms.measurement.internal.zzkv r3 = r26.zzp()
            android.os.Bundle r1 = r3.zza(r1)
        L_0x050e:
            r9 = r1
            boolean r1 = com.google.android.gms.internal.measurement.zzkv.zzb()
            if (r1 == 0) goto L_0x0521
            com.google.android.gms.measurement.internal.zzx r1 = r26.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.zzap.zzcw
            boolean r1 = r1.zza(r3)
            if (r1 != 0) goto L_0x053e
        L_0x0521:
            com.google.android.gms.measurement.internal.zzfb r1 = r26.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzw()
            com.google.android.gms.measurement.internal.zzez r3 = r26.zzo()
            java.lang.String r3 = r3.zza(r6)
            com.google.android.gms.measurement.internal.zzez r4 = r26.zzo()
            java.lang.String r4 = r4.zza(r9)
            java.lang.String r11 = "Logging event (FE)"
            r1.zza(r11, r3, r4)
        L_0x053e:
            com.google.android.gms.measurement.internal.zzan r11 = new com.google.android.gms.measurement.internal.zzan
            com.google.android.gms.measurement.internal.zzam r3 = new com.google.android.gms.measurement.internal.zzam
            r3.<init>(r9)
            r1 = r11
            r12 = 1
            r4 = r27
            r13 = r26
            r15 = r5
            r14 = r6
            r5 = r29
            r1.<init>(r2, r3, r4, r5)
            com.google.android.gms.measurement.internal.zzis r1 = r26.zzh()
            r5 = r35
            r1.zza(r11, r5)
            if (r17 != 0) goto L_0x0580
            java.util.Set<com.google.android.gms.measurement.internal.zzhi> r1 = r13.zzd
            java.util.Iterator r11 = r1.iterator()
        L_0x0563:
            boolean r1 = r11.hasNext()
            if (r1 == 0) goto L_0x0580
            java.lang.Object r1 = r11.next()
            com.google.android.gms.measurement.internal.zzhi r1 = (com.google.android.gms.measurement.internal.zzhi) r1
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>(r9)
            r2 = r27
            r3 = r28
            r5 = r29
            r1.onEvent(r2, r3, r4, r5)
            r5 = r35
            goto L_0x0563
        L_0x0580:
            int r0 = r0 + 1
            r24 = r8
            r6 = r14
            r5 = r15
            r22 = 1
            goto L_0x04e4
        L_0x058a:
            r12 = 1
            r13 = r26
            r15 = r5
            r14 = r6
            r26.zzu()
            com.google.android.gms.measurement.internal.zzin r0 = r26.zzi()
            com.google.android.gms.measurement.internal.zzio r0 = r0.zzab()
            if (r0 == 0) goto L_0x05b1
            boolean r0 = r15.equals(r14)
            if (r0 == 0) goto L_0x05b1
            com.google.android.gms.measurement.internal.zzjt r0 = r26.zzk()
            com.google.android.gms.common.util.Clock r1 = r26.zzm()
            long r1 = r1.elapsedRealtime()
            r0.zza(r12, r12, r1)
        L_0x05b1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzhk.zza(java.lang.String, java.lang.String, long, android.os.Bundle, boolean, boolean, boolean, java.lang.String):void");
    }

    public final void zza(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        zzb();
        zzb(str == null ? SettingsJsonConstants.APP_KEY : str, str2, j, bundle == null ? new Bundle() : bundle, z2, !z2 || this.zzc == null || zzkv.zze(str2), !z, null);
    }

    private final void zzb(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        zzq().zza(new zzhl(this, str, str2, j, zzkv.zzb(bundle), z, z2, z3, str3));
    }

    public final void zza(String str, String str2, Object obj, boolean z) {
        zza(str, str2, obj, z, zzm().currentTimeMillis());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.zzkv.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzkv.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, boolean):java.lang.String */
    public final void zza(String str, String str2, Object obj, boolean z, long j) {
        if (str == null) {
            str = SettingsJsonConstants.APP_KEY;
        }
        String str3 = str;
        int i = 6;
        int i2 = 0;
        if (z) {
            i = zzp().zzc(str2);
        } else {
            zzkv zzp = zzp();
            if (zzp.zza("user property", str2)) {
                if (!zzp.zza("user property", zzhg.zza, str2)) {
                    i = 15;
                } else if (zzp.zza("user property", 24, str2)) {
                    i = 0;
                }
            }
        }
        if (i != 0) {
            zzp();
            String zza2 = zzkv.zza(str2, 24, true);
            if (str2 != null) {
                i2 = str2.length();
            }
            this.zzx.zzi().zza(i, "_ev", zza2, i2);
        } else if (obj != null) {
            int zzb2 = zzp().zzb(str2, obj);
            if (zzb2 != 0) {
                zzp();
                String zza3 = zzkv.zza(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i2 = String.valueOf(obj).length();
                }
                this.zzx.zzi().zza(zzb2, "_ev", zza3, i2);
                return;
            }
            Object zzc2 = zzp().zzc(str2, obj);
            if (zzc2 != null) {
                zza(str3, str2, j, zzc2);
            }
        } else {
            zza(str3, str2, j, (Object) null);
        }
    }

    private final void zza(String str, String str2, long j, Object obj) {
        zzq().zza(new zzho(this, str, str2, obj, j));
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(java.lang.String r9, java.lang.String r10, java.lang.Object r11, long r12) {
        /*
            r8 = this;
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r9)
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r10)
            r8.zzd()
            r8.zzb()
            r8.zzw()
            com.google.android.gms.measurement.internal.zzx r0 = r8.zzt()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.zzap.zzba
            boolean r0 = r0.zza(r1)
            java.lang.String r1 = "_npa"
            if (r0 == 0) goto L_0x0072
            java.lang.String r0 = "allow_personalized_ads"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L_0x0072
            boolean r0 = r11 instanceof java.lang.String
            if (r0 == 0) goto L_0x0062
            r0 = r11
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0062
            java.util.Locale r10 = java.util.Locale.ENGLISH
            java.lang.String r10 = r0.toLowerCase(r10)
            java.lang.String r11 = "false"
            boolean r10 = r11.equals(r10)
            r2 = 1
            if (r10 == 0) goto L_0x0044
            r4 = r2
            goto L_0x0046
        L_0x0044:
            r4 = 0
        L_0x0046:
            java.lang.Long r10 = java.lang.Long.valueOf(r4)
            com.google.android.gms.measurement.internal.zzfo r0 = r8.zzs()
            com.google.android.gms.measurement.internal.zzfr r0 = r0.zzn
            r4 = r10
            java.lang.Long r4 = (java.lang.Long) r4
            long r4 = r4.longValue()
            int r6 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r6 != 0) goto L_0x005d
            java.lang.String r11 = "true"
        L_0x005d:
            r0.zza(r11)
            r6 = r10
            goto L_0x0070
        L_0x0062:
            if (r11 != 0) goto L_0x0072
            com.google.android.gms.measurement.internal.zzfo r10 = r8.zzs()
            com.google.android.gms.measurement.internal.zzfr r10 = r10.zzn
            java.lang.String r0 = "unset"
            r10.zza(r0)
            r6 = r11
        L_0x0070:
            r3 = r1
            goto L_0x0074
        L_0x0072:
            r3 = r10
            r6 = r11
        L_0x0074:
            com.google.android.gms.measurement.internal.zzgf r10 = r8.zzx
            boolean r10 = r10.zzab()
            if (r10 != 0) goto L_0x008a
            com.google.android.gms.measurement.internal.zzfb r9 = r8.zzr()
            com.google.android.gms.measurement.internal.zzfd r9 = r9.zzx()
            java.lang.String r10 = "User property not set since app measurement is disabled"
            r9.zza(r10)
            return
        L_0x008a:
            com.google.android.gms.measurement.internal.zzgf r10 = r8.zzx
            boolean r10 = r10.zzah()
            if (r10 != 0) goto L_0x0093
            return
        L_0x0093:
            com.google.android.gms.measurement.internal.zzkq r10 = new com.google.android.gms.measurement.internal.zzkq
            r2 = r10
            r4 = r12
            r7 = r9
            r2.<init>(r3, r4, r6, r7)
            com.google.android.gms.measurement.internal.zzis r9 = r8.zzh()
            r9.zza(r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzhk.zza(java.lang.String, java.lang.String, java.lang.Object, long):void");
    }

    public final List<zzkq> zzc(boolean z) {
        zzb();
        zzw();
        zzr().zzx().zza("Getting user properties (FE)");
        if (zzq().zzg()) {
            zzr().zzf().zza("Cannot get all user properties from analytics worker thread");
            return Collections.emptyList();
        } else if (zzw.zza()) {
            zzr().zzf().zza("Cannot get all user properties from main thread");
            return Collections.emptyList();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            this.zzx.zzq().zza(atomicReference, 5000, "get user properties", new zzhn(this, atomicReference, z));
            List<zzkq> list = (List) atomicReference.get();
            if (list != null) {
                return list;
            }
            zzr().zzf().zza("Timed out waiting for get user properties, includeInternal", Boolean.valueOf(z));
            return Collections.emptyList();
        }
    }

    public final String zzah() {
        zzb();
        return this.zzf.get();
    }

    public final String zzc(long j) {
        if (zzq().zzg()) {
            zzr().zzf().zza("Cannot retrieve app instance id from analytics worker thread");
            return null;
        } else if (zzw.zza()) {
            zzr().zzf().zza("Cannot retrieve app instance id from main thread");
            return null;
        } else {
            long elapsedRealtime = zzm().elapsedRealtime();
            String zze2 = zze(120000);
            long elapsedRealtime2 = zzm().elapsedRealtime() - elapsedRealtime;
            return (zze2 != null || elapsedRealtime2 >= 120000) ? zze2 : zze(120000 - elapsedRealtime2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(String str) {
        this.zzf.set(str);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:10|11|12|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        zzr().zzi().zza("Interrupted waiting for app instance id");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        return null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x001d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.lang.String zze(long r4) {
        /*
            r3 = this;
            java.util.concurrent.atomic.AtomicReference r0 = new java.util.concurrent.atomic.AtomicReference
            r0.<init>()
            monitor-enter(r0)
            com.google.android.gms.measurement.internal.zzgc r1 = r3.zzq()     // Catch:{ all -> 0x002d }
            com.google.android.gms.measurement.internal.zzhq r2 = new com.google.android.gms.measurement.internal.zzhq     // Catch:{ all -> 0x002d }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x002d }
            r1.zza(r2)     // Catch:{ all -> 0x002d }
            r0.wait(r4)     // Catch:{ InterruptedException -> 0x001d }
            monitor-exit(r0)     // Catch:{ all -> 0x002d }
            java.lang.Object r4 = r0.get()
            java.lang.String r4 = (java.lang.String) r4
            return r4
        L_0x001d:
            com.google.android.gms.measurement.internal.zzfb r4 = r3.zzr()     // Catch:{ all -> 0x002d }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzi()     // Catch:{ all -> 0x002d }
            java.lang.String r5 = "Interrupted waiting for app instance id"
            r4.zza(r5)     // Catch:{ all -> 0x002d }
            r4 = 0
            monitor-exit(r0)     // Catch:{ all -> 0x002d }
            return r4
        L_0x002d:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002d }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzhk.zze(long):java.lang.String");
    }

    public final void zzd(long j) {
        zza((String) null);
        zzq().zza(new zzhp(this, j));
    }

    public final void zzai() {
        zzd();
        zzb();
        zzw();
        if (this.zzx.zzah()) {
            if (zzt().zza(zzap.zzbw)) {
                zzx zzt = zzt();
                zzt.zzu();
                Boolean zzb2 = zzt.zzb("google_analytics_deferred_deep_link_enabled");
                if (zzb2 != null && zzb2.booleanValue()) {
                    zzr().zzw().zza("Deferred Deep Link feature enabled.");
                    zzq().zza(new zzhj(this));
                }
            }
            zzh().zzae();
            this.zzb = false;
            String zzw = zzs().zzw();
            if (!TextUtils.isEmpty(zzw)) {
                zzl().zzaa();
                if (!zzw.equals(Build.VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", zzw);
                    zza("auto", "_ou", bundle);
                }
            }
        }
    }

    public final void zza(zzhf zzhf) {
        zzhf zzhf2;
        zzd();
        zzb();
        zzw();
        if (!(zzhf == null || zzhf == (zzhf2 = this.zzc))) {
            Preconditions.checkState(zzhf2 == null, "EventInterceptor already set.");
        }
        this.zzc = zzhf;
    }

    public final void zza(zzhi zzhi) {
        zzb();
        zzw();
        Preconditions.checkNotNull(zzhi);
        if (!this.zzd.add(zzhi)) {
            zzr().zzi().zza("OnEventListener already registered");
        }
    }

    public final void zzb(zzhi zzhi) {
        zzb();
        zzw();
        Preconditions.checkNotNull(zzhi);
        if (!this.zzd.remove(zzhi)) {
            zzr().zzi().zza("OnEventListener had not been registered");
        }
    }

    public final void zza(Bundle bundle) {
        zza(bundle, zzm().currentTimeMillis());
    }

    public final void zza(Bundle bundle, long j) {
        Preconditions.checkNotNull(bundle);
        zzb();
        Bundle bundle2 = new Bundle(bundle);
        if (!TextUtils.isEmpty(bundle2.getString("app_id"))) {
            zzr().zzi().zza("Package name should be null when calling setConditionalUserProperty");
        }
        bundle2.remove("app_id");
        zzb(bundle2, j);
    }

    public final void zzb(Bundle bundle) {
        Preconditions.checkNotNull(bundle);
        Preconditions.checkNotEmpty(bundle.getString("app_id"));
        zza();
        zzb(new Bundle(bundle), zzm().currentTimeMillis());
    }

    private final void zzb(Bundle bundle, long j) {
        Preconditions.checkNotNull(bundle);
        zzhb.zza(bundle, "app_id", String.class, null);
        zzhb.zza(bundle, "origin", String.class, null);
        zzhb.zza(bundle, "name", String.class, null);
        zzhb.zza(bundle, "value", Object.class, null);
        zzhb.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TRIGGER_EVENT_NAME, String.class, null);
        zzhb.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TRIGGER_TIMEOUT, Long.class, 0L);
        zzhb.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TIMED_OUT_EVENT_NAME, String.class, null);
        zzhb.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TIMED_OUT_EVENT_PARAMS, Bundle.class, null);
        zzhb.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_EVENT_NAME, String.class, null);
        zzhb.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_EVENT_PARAMS, Bundle.class, null);
        zzhb.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.TIME_TO_LIVE, Long.class, 0L);
        zzhb.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_NAME, String.class, null);
        zzhb.zza(bundle, AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_PARAMS, Bundle.class, null);
        Preconditions.checkNotEmpty(bundle.getString("name"));
        Preconditions.checkNotEmpty(bundle.getString("origin"));
        Preconditions.checkNotNull(bundle.get("value"));
        bundle.putLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP, j);
        String string = bundle.getString("name");
        Object obj = bundle.get("value");
        if (zzp().zzc(string) != 0) {
            zzr().zzf().zza("Invalid conditional user property name", zzo().zzc(string));
        } else if (zzp().zzb(string, obj) != 0) {
            zzr().zzf().zza("Invalid conditional user property value", zzo().zzc(string), obj);
        } else {
            Object zzc2 = zzp().zzc(string, obj);
            if (zzc2 == null) {
                zzr().zzf().zza("Unable to normalize conditional user property value", zzo().zzc(string), obj);
                return;
            }
            zzhb.zza(bundle, zzc2);
            long j2 = bundle.getLong(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_TIMEOUT);
            if (TextUtils.isEmpty(bundle.getString(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_EVENT_NAME)) || (j2 <= 15552000000L && j2 >= 1)) {
                long j3 = bundle.getLong(AppMeasurementSdk.ConditionalUserProperty.TIME_TO_LIVE);
                if (j3 > 15552000000L || j3 < 1) {
                    zzr().zzf().zza("Invalid conditional user property time to live", zzo().zzc(string), Long.valueOf(j3));
                } else {
                    zzq().zza(new zzhr(this, bundle));
                }
            } else {
                zzr().zzf().zza("Invalid conditional user property timeout", zzo().zzc(string), Long.valueOf(j2));
            }
        }
    }

    public final void zzc(String str, String str2, Bundle bundle) {
        zzb();
        zzb((String) null, str, str2, bundle);
    }

    public final void zza(String str, String str2, String str3, Bundle bundle) {
        Preconditions.checkNotEmpty(str);
        zza();
        zzb(str, str2, str3, bundle);
    }

    private final void zzb(String str, String str2, String str3, Bundle bundle) {
        long currentTimeMillis = zzm().currentTimeMillis();
        Preconditions.checkNotEmpty(str2);
        Bundle bundle2 = new Bundle();
        if (str != null) {
            bundle2.putString("app_id", str);
        }
        bundle2.putString("name", str2);
        bundle2.putLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP, currentTimeMillis);
        if (str3 != null) {
            bundle2.putString(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_NAME, str3);
            bundle2.putBundle(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_PARAMS, bundle);
        }
        zzq().zza(new zzhu(this, bundle2));
    }

    /* access modifiers changed from: private */
    public final void zzc(Bundle bundle) {
        Bundle bundle2 = bundle;
        zzd();
        zzw();
        Preconditions.checkNotNull(bundle);
        Preconditions.checkNotEmpty(bundle2.getString("name"));
        Preconditions.checkNotEmpty(bundle2.getString("origin"));
        Preconditions.checkNotNull(bundle2.get("value"));
        if (!this.zzx.zzab()) {
            zzr().zzx().zza("Conditional property not set since app measurement is disabled");
            return;
        }
        zzkq zzkq = new zzkq(bundle2.getString("name"), bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_TIMESTAMP), bundle2.get("value"), bundle2.getString("origin"));
        try {
            zzan zza2 = zzp().zza(bundle2.getString("app_id"), bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_EVENT_NAME), bundle2.getBundle(AppMeasurementSdk.ConditionalUserProperty.TRIGGERED_EVENT_PARAMS), bundle2.getString("origin"), 0, true, false);
            zzh().zza(new zzv(bundle2.getString("app_id"), bundle2.getString("origin"), zzkq, bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP), false, bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_EVENT_NAME), zzp().zza(bundle2.getString("app_id"), bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.TIMED_OUT_EVENT_NAME), bundle2.getBundle(AppMeasurementSdk.ConditionalUserProperty.TIMED_OUT_EVENT_PARAMS), bundle2.getString("origin"), 0, true, false), bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_TIMEOUT), zza2, bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TIME_TO_LIVE), zzp().zza(bundle2.getString("app_id"), bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_NAME), bundle2.getBundle(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_PARAMS), bundle2.getString("origin"), 0, true, false)));
        } catch (IllegalArgumentException unused) {
        }
    }

    /* access modifiers changed from: private */
    public final void zzd(Bundle bundle) {
        Bundle bundle2 = bundle;
        zzd();
        zzw();
        Preconditions.checkNotNull(bundle);
        Preconditions.checkNotEmpty(bundle2.getString("name"));
        if (!this.zzx.zzab()) {
            zzr().zzx().zza("Conditional property not cleared since app measurement is disabled");
            return;
        }
        zzkq zzkq = new zzkq(bundle2.getString("name"), 0, null, null);
        try {
            zzan zza2 = zzp().zza(bundle2.getString("app_id"), bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_NAME), bundle2.getBundle(AppMeasurementSdk.ConditionalUserProperty.EXPIRED_EVENT_PARAMS), bundle2.getString("origin"), bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP), true, false);
            zzkq zzkq2 = zzkq;
            zzh().zza(new zzv(bundle2.getString("app_id"), bundle2.getString("origin"), zzkq2, bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.CREATION_TIMESTAMP), bundle2.getBoolean(AppMeasurementSdk.ConditionalUserProperty.ACTIVE), bundle2.getString(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_EVENT_NAME), null, bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TRIGGER_TIMEOUT), null, bundle2.getLong(AppMeasurementSdk.ConditionalUserProperty.TIME_TO_LIVE), zza2));
        } catch (IllegalArgumentException unused) {
        }
    }

    public final ArrayList<Bundle> zza(String str, String str2) {
        zzb();
        return zzb((String) null, str, str2);
    }

    public final ArrayList<Bundle> zza(String str, String str2, String str3) {
        Preconditions.checkNotEmpty(str);
        zza();
        return zzb(str, str2, str3);
    }

    private final ArrayList<Bundle> zzb(String str, String str2, String str3) {
        if (zzq().zzg()) {
            zzr().zzf().zza("Cannot get conditional user properties from analytics worker thread");
            return new ArrayList<>(0);
        } else if (zzw.zza()) {
            zzr().zzf().zza("Cannot get conditional user properties from main thread");
            return new ArrayList<>(0);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            this.zzx.zzq().zza(atomicReference, 5000, "get conditional user properties", new zzht(this, atomicReference, str, str2, str3));
            List list = (List) atomicReference.get();
            if (list != null) {
                return zzkv.zzb(list);
            }
            zzr().zzf().zza("Timed out waiting for get conditional user properties", str);
            return new ArrayList<>();
        }
    }

    public final Map<String, Object> zza(String str, String str2, boolean z) {
        zzb();
        return zzb((String) null, str, str2, z);
    }

    public final Map<String, Object> zza(String str, String str2, String str3, boolean z) {
        Preconditions.checkNotEmpty(str);
        zza();
        return zzb(str, str2, str3, z);
    }

    private final Map<String, Object> zzb(String str, String str2, String str3, boolean z) {
        if (zzq().zzg()) {
            zzr().zzf().zza("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        } else if (zzw.zza()) {
            zzr().zzf().zza("Cannot get user properties from main thread");
            return Collections.emptyMap();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            this.zzx.zzq().zza(atomicReference, 5000, "get user properties", new zzhv(this, atomicReference, str, str2, str3, z));
            List<zzkq> list = (List) atomicReference.get();
            if (list == null) {
                zzr().zzf().zza("Timed out waiting for handle get user properties, includeInternal", Boolean.valueOf(z));
                return Collections.emptyMap();
            }
            ArrayMap arrayMap = new ArrayMap(list.size());
            for (zzkq zzkq : list) {
                arrayMap.put(zzkq.zza, zzkq.zza());
            }
            return arrayMap;
        }
    }

    public final String zzaj() {
        zzio zzac = this.zzx.zzv().zzac();
        if (zzac != null) {
            return zzac.zza;
        }
        return null;
    }

    public final String zzak() {
        zzio zzac = this.zzx.zzv().zzac();
        if (zzac != null) {
            return zzac.zzb;
        }
        return null;
    }

    public final String zzal() {
        if (this.zzx.zzo() != null) {
            return this.zzx.zzo();
        }
        try {
            return GoogleServices.getGoogleAppId();
        } catch (IllegalStateException e) {
            this.zzx.zzr().zzf().zza("getGoogleAppId failed with exception", e);
            return null;
        }
    }

    public final /* bridge */ /* synthetic */ void zza() {
        super.zza();
    }

    public final /* bridge */ /* synthetic */ void zzb() {
        super.zzb();
    }

    public final /* bridge */ /* synthetic */ void zzc() {
        super.zzc();
    }

    public final /* bridge */ /* synthetic */ void zzd() {
        super.zzd();
    }

    public final /* bridge */ /* synthetic */ zzb zze() {
        return super.zze();
    }

    public final /* bridge */ /* synthetic */ zzhk zzf() {
        return super.zzf();
    }

    public final /* bridge */ /* synthetic */ zzey zzg() {
        return super.zzg();
    }

    public final /* bridge */ /* synthetic */ zzis zzh() {
        return super.zzh();
    }

    public final /* bridge */ /* synthetic */ zzin zzi() {
        return super.zzi();
    }

    public final /* bridge */ /* synthetic */ zzex zzj() {
        return super.zzj();
    }

    public final /* bridge */ /* synthetic */ zzjt zzk() {
        return super.zzk();
    }

    public final /* bridge */ /* synthetic */ zzah zzl() {
        return super.zzl();
    }

    public final /* bridge */ /* synthetic */ Clock zzm() {
        return super.zzm();
    }

    public final /* bridge */ /* synthetic */ Context zzn() {
        return super.zzn();
    }

    public final /* bridge */ /* synthetic */ zzez zzo() {
        return super.zzo();
    }

    public final /* bridge */ /* synthetic */ zzkv zzp() {
        return super.zzp();
    }

    public final /* bridge */ /* synthetic */ zzgc zzq() {
        return super.zzq();
    }

    public final /* bridge */ /* synthetic */ zzfb zzr() {
        return super.zzr();
    }

    public final /* bridge */ /* synthetic */ zzfo zzs() {
        return super.zzs();
    }

    public final /* bridge */ /* synthetic */ zzx zzt() {
        return super.zzt();
    }

    public final /* bridge */ /* synthetic */ zzw zzu() {
        return super.zzu();
    }
}
