package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import javax.annotation.ParametersAreNonnullByDefault;
import org.json.JSONException;
import org.json.JSONObject;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbvr implements zzbxa {
    private final zzazb zzbll;
    private final Clock zzbmq;
    private final zzdq zzefv;
    private final zzdda zzfbm;
    private final zzbwz zzfea;
    private final zzczl zzffc;
    private final zzczu zzfgl;
    private final JSONObject zzfka;
    private final zzcaj zzfkb;
    private final zzbws zzfkc;
    /* access modifiers changed from: private */
    public final zzbpd zzfkd;
    /* access modifiers changed from: private */
    public final zzboq zzfke;
    private final zzbjd zzfkf;
    private final zzbxq zzfkg;
    private final zzbst zzfkh;
    private boolean zzfki = false;
    private boolean zzfkj;
    private boolean zzfkk = false;
    private boolean zzfkl = false;
    private Point zzfkm = new Point();
    private Point zzfkn = new Point();
    private long zzfko = 0;
    private long zzfkp = 0;
    private zzwn zzfkq;
    private final Context zzup;

    public zzbvr(Context context, zzbwz zzbwz, JSONObject jSONObject, zzcaj zzcaj, zzbws zzbws, zzdq zzdq, zzbpd zzbpd, zzboq zzboq, zzczl zzczl, zzazb zzazb, zzczu zzczu, zzbjd zzbjd, zzbxq zzbxq, Clock clock, zzbst zzbst, zzdda zzdda) {
        this.zzup = context;
        this.zzfea = zzbwz;
        this.zzfka = jSONObject;
        this.zzfkb = zzcaj;
        this.zzfkc = zzbws;
        this.zzefv = zzdq;
        this.zzfkd = zzbpd;
        this.zzfke = zzboq;
        this.zzffc = zzczl;
        this.zzbll = zzazb;
        this.zzfgl = zzczu;
        this.zzfkf = zzbjd;
        this.zzfkg = zzbxq;
        this.zzbmq = clock;
        this.zzfkh = zzbst;
        this.zzfbm = zzdda;
    }

    public final void zza(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, View.OnTouchListener onTouchListener, View.OnClickListener onClickListener) {
        this.zzfkm = new Point();
        this.zzfkn = new Point();
        if (!this.zzfkj) {
            this.zzfkh.zzq(view);
            this.zzfkj = true;
        }
        view.setOnTouchListener(onTouchListener);
        view.setClickable(true);
        view.setOnClickListener(onClickListener);
        this.zzfkf.zzo(this);
        boolean zzcs = zzaxy.zzcs(this.zzbll.zzdwa);
        if (map != null) {
            for (Map.Entry<String, WeakReference<View>> value : map.entrySet()) {
                View view2 = (View) ((WeakReference) value.getValue()).get();
                if (view2 != null) {
                    if (zzcs) {
                        view2.setOnTouchListener(onTouchListener);
                    }
                    view2.setClickable(true);
                    view2.setOnClickListener(onClickListener);
                }
            }
        }
        if (map2 != null) {
            for (Map.Entry<String, WeakReference<View>> value2 : map2.entrySet()) {
                View view3 = (View) ((WeakReference) value2.getValue()).get();
                if (view3 != null) {
                    if (zzcs) {
                        view3.setOnTouchListener(onTouchListener);
                    }
                    view3.setClickable(false);
                }
            }
        }
    }

    public final void zza(View view, Map<String, WeakReference<View>> map) {
        this.zzfkm = new Point();
        this.zzfkn = new Point();
        this.zzfkh.zzr(view);
        this.zzfkj = false;
    }

    private final boolean zzft(String str) {
        JSONObject optJSONObject = this.zzfka.optJSONObject("allow_pub_event_reporting");
        if (optJSONObject == null || !optJSONObject.optBoolean(str, false)) {
            return false;
        }
        return true;
    }

    public final void zza(View view, View view2, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, boolean z) {
        JSONObject zza = zza(map, map2, view2);
        JSONObject zzs = zzs(view2);
        JSONObject zzt = zzt(view2);
        JSONObject zzu = zzu(view2);
        String zzb = zzb(view, map);
        zza(view, zzs, zza, zzt, zzu, zzb, zzfv(zzb), null, z, false);
    }

    private final String zzb(View view, Map<String, WeakReference<View>> map) {
        if (!(map == null || view == null)) {
            for (Map.Entry next : map.entrySet()) {
                if (view.equals((View) ((WeakReference) next.getValue()).get())) {
                    return (String) next.getKey();
                }
            }
        }
        int zzaja = this.zzfkc.zzaja();
        if (zzaja == 1) {
            return "1099";
        }
        if (zzaja == 2) {
            return "2099";
        }
        if (zzaja == 3 || zzaja != 6) {
            return null;
        }
        return "3099";
    }

    public final void zzfu(String str) {
        zza(null, null, null, null, null, str, null, null, false, false);
    }

    public final void zzf(Bundle bundle) {
        if (bundle == null) {
            zzavs.zzea("Click data is null. No click is reported.");
        } else if (!zzft("click_reporting")) {
            zzavs.zzex("The ad slot cannot handle external click events. You must be whitelisted to be able to report your click events.");
        } else {
            Bundle bundle2 = bundle.getBundle("click_signal");
            zza(null, null, null, null, null, bundle2 != null ? bundle2.getString("asset_id") : null, null, zzq.zzkq().zza(bundle, (JSONObject) null), false, false);
        }
    }

    public final void zza(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, boolean z) {
        if (!this.zzfkl) {
            zzavs.zzea("Custom click reporting failed. enableCustomClickGesture is not set.");
        } else if (!zzain()) {
            zzavs.zzea("Custom click reporting failed. Ad unit id not whitelisted.");
        } else {
            JSONObject zza = zza(map, map2, view);
            JSONObject zzs = zzs(view);
            JSONObject zzt = zzt(view);
            JSONObject zzu = zzu(view);
            String zzb = zzb(null, map);
            zza(view, zzs, zza, zzt, zzu, zzb, zzfv(zzb), null, z, true);
        }
    }

    private final boolean zzain() {
        return this.zzfka.optBoolean("allow_custom_click_gesture", false);
    }

    public final void zzrp() {
        this.zzfkl = true;
    }

    public final boolean isCustomClickGestureEnabled() {
        return zzain();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private final void zza(View view, JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, JSONObject jSONObject4, String str, JSONObject jSONObject5, JSONObject jSONObject6, boolean z, boolean z2) {
        Preconditions.checkMainThread("performClick must be called on the main UI thread.");
        try {
            JSONObject jSONObject7 = new JSONObject();
            jSONObject7.put("ad", this.zzfka);
            jSONObject7.put("asset_view_signal", jSONObject2);
            jSONObject7.put("ad_view_signal", jSONObject);
            jSONObject7.put("click_signal", jSONObject5);
            jSONObject7.put("scroll_view_signal", jSONObject3);
            jSONObject7.put("lock_screen_signal", jSONObject4);
            boolean z3 = false;
            jSONObject7.put("has_custom_click_handler", this.zzfea.zzga(this.zzfkc.getCustomTemplateId()) != null);
            jSONObject7.put("provided_signals", jSONObject6);
            JSONObject jSONObject8 = new JSONObject();
            jSONObject8.put("asset_id", str);
            jSONObject8.put("template", this.zzfkc.zzaja());
            jSONObject8.put("view_aware_api_used", z);
            jSONObject8.put("custom_mute_requested", this.zzfgl.zzddz != null && this.zzfgl.zzddz.zzbkb);
            jSONObject8.put("custom_mute_enabled", !this.zzfkc.getMuteThisAdReasons().isEmpty() && this.zzfkc.zzajd() != null);
            if (this.zzfkg.zzakh() != null && this.zzfka.optBoolean("custom_one_point_five_click_enabled", false)) {
                jSONObject8.put("custom_one_point_five_click_eligible", true);
            }
            jSONObject8.put("timestamp", this.zzbmq.currentTimeMillis());
            if (this.zzfkl && zzain()) {
                jSONObject8.put("custom_click_gesture_eligible", true);
            }
            if (z2) {
                jSONObject8.put("is_custom_click_gesture", true);
            }
            if (this.zzfea.zzga(this.zzfkc.getCustomTemplateId()) != null) {
                z3 = true;
            }
            jSONObject8.put("has_custom_click_handler", z3);
            jSONObject8.put("click_signals", zzv(view));
            jSONObject7.put("click", jSONObject8);
            JSONObject jSONObject9 = new JSONObject();
            long currentTimeMillis = this.zzbmq.currentTimeMillis();
            jSONObject9.put("time_from_last_touch_down", currentTimeMillis - this.zzfko);
            jSONObject9.put("time_from_last_touch", currentTimeMillis - this.zzfkp);
            jSONObject7.put("touch_signal", jSONObject9);
            zzazh.zza(this.zzfkb.zzc("google.afma.nativeAds.handleClick", jSONObject7), "Error during performing handleClick");
        } catch (JSONException e) {
            zzavs.zzc("Unable to create click JSON.", e);
        }
    }

    public final void zza(View view, MotionEvent motionEvent, View view2) {
        int[] zzx = zzx(view2);
        this.zzfkm = new Point(((int) motionEvent.getRawX()) - zzx[0], ((int) motionEvent.getRawY()) - zzx[1]);
        long currentTimeMillis = this.zzbmq.currentTimeMillis();
        this.zzfkp = currentTimeMillis;
        if (motionEvent.getAction() == 0) {
            this.zzfko = currentTimeMillis;
            this.zzfkn = this.zzfkm;
        }
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        obtain.setLocation((float) this.zzfkm.x, (float) this.zzfkm.y);
        this.zzefv.zza(obtain);
        obtain.recycle();
    }

    public final void zzg(Bundle bundle) {
        if (bundle == null) {
            zzavs.zzea("Touch event data is null. No touch event is reported.");
        } else if (!zzft("touch_reporting")) {
            zzavs.zzex("The ad slot cannot handle external touch events. You must be whitelisted to be able to report your touch events.");
        } else {
            int i = bundle.getInt("duration_ms");
            this.zzefv.zzbw().zza((int) bundle.getFloat("x"), (int) bundle.getFloat("y"), i);
        }
    }

    public final void zzaio() {
        zza(null, null, null, null, null, null);
    }

    public final void setClickConfirmingView(View view) {
        if (!this.zzfka.optBoolean("custom_one_point_five_click_enabled", false)) {
            zzavs.zzez("setClickConfirmingView: Your account need to be whitelisted to use this feature.\nContact your account manager for more information.");
            return;
        }
        zzbxq zzbxq = this.zzfkg;
        if (view != null) {
            view.setOnClickListener(zzbxq);
            view.setClickable(true);
            zzbxq.zzfoc = new WeakReference<>(view);
        }
    }

    public final void zza(zzaeb zzaeb) {
        if (!this.zzfka.optBoolean("custom_one_point_five_click_enabled", false)) {
            zzavs.zzez("setUnconfirmedClickListener: Your account need to be whitelisted to use this feature.\nContact your account manager for more information.");
        } else {
            this.zzfkg.zza(zzaeb);
        }
    }

    public final void cancelUnconfirmedClick() {
        if (this.zzfka.optBoolean("custom_one_point_five_click_enabled", false)) {
            this.zzfkg.cancelUnconfirmedClick();
        }
    }

    public final void zza(zzwr zzwr) {
        try {
            if (!this.zzfkk) {
                if (zzwr != null || this.zzfkc.zzajd() == null) {
                    this.zzfkk = true;
                    this.zzfbm.zzen(zzwr.zzph());
                    zzaip();
                    return;
                }
                this.zzfkk = true;
                this.zzfbm.zzen(this.zzfkc.zzajd().zzph());
                zzaip();
            }
        } catch (RemoteException e) {
            zzavs.zze("#007 Could not call remote method.", e);
        }
    }

    public final void zza(zzwn zzwn) {
        this.zzfkq = zzwn;
    }

    public final void zzaip() {
        try {
            if (this.zzfkq != null) {
                this.zzfkq.onAdMuted();
            }
        } catch (RemoteException e) {
            zzavs.zze("#007 Could not call remote method.", e);
        }
    }

    public final void zza(View view, Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2) {
        zza(zzs(view), zza(map, map2, view), zzt(view), zzu(view), zzw(view), null);
    }

    public final void zzaiq() {
        Preconditions.checkMainThread("recordDownloadedImpression must be called on the main UI thread.");
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("ad", this.zzfka);
            zzazh.zza(this.zzfkb.zzc("google.afma.nativeAds.handleDownloadedImpression", jSONObject), "Error during performing handleDownloadedImpression");
        } catch (JSONException e) {
            zzayu.zzc("", e);
        }
    }

    public final boolean zzh(Bundle bundle) {
        if (zzft("impression_reporting")) {
            return zza(null, null, null, null, null, zzq.zzkq().zza(bundle, (JSONObject) null));
        }
        zzavs.zzex("The ad slot cannot handle external impression events. You must be whitelisted to whitelisted to be able to report your impression events.");
        return false;
    }

    private final boolean zza(JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, JSONObject jSONObject4, String str, JSONObject jSONObject5) {
        Preconditions.checkMainThread("recordImpression must be called on the main UI thread.");
        try {
            JSONObject jSONObject6 = new JSONObject();
            jSONObject6.put("ad", this.zzfka);
            jSONObject6.put("asset_view_signal", jSONObject2);
            jSONObject6.put("ad_view_signal", jSONObject);
            jSONObject6.put("scroll_view_signal", jSONObject3);
            jSONObject6.put("lock_screen_signal", jSONObject4);
            jSONObject6.put("provided_signals", jSONObject5);
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcls)).booleanValue()) {
                jSONObject6.put("view_signals", str);
            }
            this.zzfkb.zza("/logScionEvent", new zzbvt(this));
            this.zzfkb.zza("/nativeImpression", new zzbvw(this));
            zzazh.zza(this.zzfkb.zzc("google.afma.nativeAds.handleImpression", jSONObject6), "Error during performing handleImpression");
            if (this.zzfki || this.zzffc.zzglt == null) {
                return true;
            }
            this.zzfki |= zzq.zzla().zzb(this.zzup, this.zzbll.zzbma, this.zzffc.zzglt.toString(), this.zzfgl.zzgmm);
            return true;
        } catch (JSONException e) {
            zzavs.zzc("Unable to create impression JSON.", e);
            return false;
        }
    }

    private final JSONObject zzb(Rect rect) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("width", zzdi(rect.right - rect.left));
        jSONObject.put("height", zzdi(rect.bottom - rect.top));
        jSONObject.put("x", zzdi(rect.left));
        jSONObject.put("y", zzdi(rect.top));
        jSONObject.put("relative_to", "self");
        return jSONObject;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d4 A[Catch:{ JSONException -> 0x0100 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e4 A[Catch:{ JSONException -> 0x0100 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00fc A[Catch:{ JSONException -> 0x0100 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final org.json.JSONObject zzs(android.view.View r14) {
        /*
            r13 = this;
            java.lang.String r0 = "window"
            java.lang.String r1 = "relative_to"
            java.lang.String r2 = "y"
            java.lang.String r3 = "x"
            java.lang.String r4 = "height"
            java.lang.String r5 = "width"
            java.lang.String r6 = "Cannot access method getTemplateTypeName: "
            org.json.JSONObject r7 = new org.json.JSONObject
            r7.<init>()
            if (r14 != 0) goto L_0x0016
            return r7
        L_0x0016:
            r8 = 1
            r9 = 0
            int[] r10 = zzx(r14)     // Catch:{ Exception -> 0x0088 }
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ Exception -> 0x0088 }
            r11.<init>()     // Catch:{ Exception -> 0x0088 }
            int r12 = r14.getMeasuredWidth()     // Catch:{ Exception -> 0x0088 }
            int r12 = r13.zzdi(r12)     // Catch:{ Exception -> 0x0088 }
            r11.put(r5, r12)     // Catch:{ Exception -> 0x0088 }
            int r12 = r14.getMeasuredHeight()     // Catch:{ Exception -> 0x0088 }
            int r12 = r13.zzdi(r12)     // Catch:{ Exception -> 0x0088 }
            r11.put(r4, r12)     // Catch:{ Exception -> 0x0088 }
            r12 = r10[r9]     // Catch:{ Exception -> 0x0088 }
            int r12 = r13.zzdi(r12)     // Catch:{ Exception -> 0x0088 }
            r11.put(r3, r12)     // Catch:{ Exception -> 0x0088 }
            r12 = r10[r8]     // Catch:{ Exception -> 0x0088 }
            int r12 = r13.zzdi(r12)     // Catch:{ Exception -> 0x0088 }
            r11.put(r2, r12)     // Catch:{ Exception -> 0x0088 }
            r11.put(r1, r0)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r12 = "frame"
            r7.put(r12, r11)     // Catch:{ Exception -> 0x0088 }
            android.graphics.Rect r11 = new android.graphics.Rect     // Catch:{ Exception -> 0x0088 }
            r11.<init>()     // Catch:{ Exception -> 0x0088 }
            boolean r12 = r14.getGlobalVisibleRect(r11)     // Catch:{ Exception -> 0x0088 }
            if (r12 == 0) goto L_0x0061
            org.json.JSONObject r0 = r13.zzb(r11)     // Catch:{ Exception -> 0x0088 }
            goto L_0x0082
        L_0x0061:
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ Exception -> 0x0088 }
            r11.<init>()     // Catch:{ Exception -> 0x0088 }
            r11.put(r5, r9)     // Catch:{ Exception -> 0x0088 }
            r11.put(r4, r9)     // Catch:{ Exception -> 0x0088 }
            r4 = r10[r9]     // Catch:{ Exception -> 0x0088 }
            int r4 = r13.zzdi(r4)     // Catch:{ Exception -> 0x0088 }
            r11.put(r3, r4)     // Catch:{ Exception -> 0x0088 }
            r3 = r10[r8]     // Catch:{ Exception -> 0x0088 }
            int r3 = r13.zzdi(r3)     // Catch:{ Exception -> 0x0088 }
            r11.put(r2, r3)     // Catch:{ Exception -> 0x0088 }
            r11.put(r1, r0)     // Catch:{ Exception -> 0x0088 }
            r0 = r11
        L_0x0082:
            java.lang.String r1 = "visible_bounds"
            r7.put(r1, r0)     // Catch:{ Exception -> 0x0088 }
            goto L_0x008d
        L_0x0088:
            java.lang.String r0 = "Unable to get native ad view bounding box"
            com.google.android.gms.internal.ads.zzavs.zzez(r0)
        L_0x008d:
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzzn.zzcqh
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r0 = r1.zzd(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0106
            android.view.ViewParent r14 = r14.getParent()
            if (r14 == 0) goto L_0x00c8
            java.lang.Class r0 = r14.getClass()     // Catch:{ NoSuchMethodException -> 0x00c8, SecurityException -> 0x00c4, IllegalAccessException -> 0x00bf, InvocationTargetException -> 0x00ba }
            java.lang.String r1 = "getTemplateTypeName"
            java.lang.Class[] r2 = new java.lang.Class[r9]     // Catch:{ NoSuchMethodException -> 0x00c8, SecurityException -> 0x00c4, IllegalAccessException -> 0x00bf, InvocationTargetException -> 0x00ba }
            java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ NoSuchMethodException -> 0x00c8, SecurityException -> 0x00c4, IllegalAccessException -> 0x00bf, InvocationTargetException -> 0x00ba }
            java.lang.Object[] r1 = new java.lang.Object[r9]     // Catch:{ NoSuchMethodException -> 0x00c8, SecurityException -> 0x00c4, IllegalAccessException -> 0x00bf, InvocationTargetException -> 0x00ba }
            java.lang.Object r14 = r0.invoke(r14, r1)     // Catch:{ NoSuchMethodException -> 0x00c8, SecurityException -> 0x00c4, IllegalAccessException -> 0x00bf, InvocationTargetException -> 0x00ba }
            java.lang.String r14 = (java.lang.String) r14     // Catch:{ NoSuchMethodException -> 0x00c8, SecurityException -> 0x00c4, IllegalAccessException -> 0x00bf, InvocationTargetException -> 0x00ba }
            goto L_0x00ca
        L_0x00ba:
            r14 = move-exception
            com.google.android.gms.internal.ads.zzavs.zzc(r6, r14)
            goto L_0x00c8
        L_0x00bf:
            r14 = move-exception
            com.google.android.gms.internal.ads.zzavs.zzc(r6, r14)
            goto L_0x00c8
        L_0x00c4:
            r14 = move-exception
            com.google.android.gms.internal.ads.zzavs.zzc(r6, r14)
        L_0x00c8:
            java.lang.String r14 = ""
        L_0x00ca:
            r0 = -1
            int r1 = r14.hashCode()     // Catch:{ JSONException -> 0x0100 }
            r2 = -2066603854(0xffffffff84d220b2, float:-4.940079E-36)
            if (r1 == r2) goto L_0x00e4
            r2 = 2019754500(0x78630204, float:1.8417067E34)
            if (r1 == r2) goto L_0x00da
            goto L_0x00ed
        L_0x00da:
            java.lang.String r1 = "medium_template"
            boolean r14 = r14.equals(r1)     // Catch:{ JSONException -> 0x0100 }
            if (r14 == 0) goto L_0x00ed
            r0 = 1
            goto L_0x00ed
        L_0x00e4:
            java.lang.String r1 = "small_template"
            boolean r14 = r14.equals(r1)     // Catch:{ JSONException -> 0x0100 }
            if (r14 == 0) goto L_0x00ed
            r0 = 0
        L_0x00ed:
            java.lang.String r14 = "native_template_type"
            if (r0 == 0) goto L_0x00fc
            if (r0 == r8) goto L_0x00f7
            r7.put(r14, r9)     // Catch:{ JSONException -> 0x0100 }
            goto L_0x0106
        L_0x00f7:
            r0 = 2
            r7.put(r14, r0)     // Catch:{ JSONException -> 0x0100 }
            goto L_0x0106
        L_0x00fc:
            r7.put(r14, r8)     // Catch:{ JSONException -> 0x0100 }
            goto L_0x0106
        L_0x0100:
            r14 = move-exception
            java.lang.String r0 = "Could not log native template signal to JSON"
            com.google.android.gms.internal.ads.zzavs.zzc(r0, r14)
        L_0x0106:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbvr.zzs(android.view.View):org.json.JSONObject");
    }

    private static JSONObject zzt(View view) {
        JSONObject jSONObject = new JSONObject();
        if (view == null) {
            return jSONObject;
        }
        try {
            zzq.zzkq();
            jSONObject.put("contained_in_scroll_view", zzawb.zzp(view) != -1);
        } catch (Exception unused) {
        }
        return jSONObject;
    }

    private final JSONObject zzu(View view) {
        JSONObject jSONObject = new JSONObject();
        if (view == null) {
            return jSONObject;
        }
        try {
            zzq.zzkq();
            jSONObject.put("can_show_on_lock_screen", zzawb.zzo(view));
            zzq.zzkq();
            jSONObject.put("is_keyguard_locked", zzawb.zzax(this.zzup));
        } catch (JSONException unused) {
            zzavs.zzez("Unable to get lock screen information");
        }
        return jSONObject;
    }

    private final JSONObject zza(Map<String, WeakReference<View>> map, Map<String, WeakReference<View>> map2, View view) {
        String str;
        String str2;
        JSONObject jSONObject;
        Map<String, WeakReference<View>> map3 = map2;
        String str3 = "ad_view";
        String str4 = "relative_to";
        JSONObject jSONObject2 = new JSONObject();
        if (!(map == null || view == null)) {
            int[] zzx = zzx(view);
            Iterator<Map.Entry<String, WeakReference<View>>> it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry next = it.next();
                View view2 = (View) ((WeakReference) next.getValue()).get();
                if (view2 != null) {
                    int[] zzx2 = zzx(view2);
                    JSONObject jSONObject3 = new JSONObject();
                    JSONObject jSONObject4 = new JSONObject();
                    Iterator<Map.Entry<String, WeakReference<View>>> it2 = it;
                    try {
                        jSONObject4.put("width", zzdi(view2.getMeasuredWidth()));
                        jSONObject4.put("height", zzdi(view2.getMeasuredHeight()));
                        jSONObject4.put("x", zzdi(zzx2[0] - zzx[0]));
                        jSONObject4.put("y", zzdi(zzx2[1] - zzx[1]));
                        jSONObject4.put(str4, str3);
                        jSONObject3.put("frame", jSONObject4);
                        Rect rect = new Rect();
                        if (view2.getLocalVisibleRect(rect)) {
                            jSONObject = zzb(rect);
                        } else {
                            jSONObject = new JSONObject();
                            jSONObject.put("width", 0);
                            jSONObject.put("height", 0);
                            jSONObject.put("x", zzdi(zzx2[0] - zzx[0]));
                            jSONObject.put("y", zzdi(zzx2[1] - zzx[1]));
                            jSONObject.put(str4, str3);
                        }
                        jSONObject3.put("visible_bounds", jSONObject);
                        if (view2 instanceof TextView) {
                            TextView textView = (TextView) view2;
                            jSONObject3.put("text_color", textView.getCurrentTextColor());
                            str2 = str3;
                            str = str4;
                            try {
                                jSONObject3.put(ViewHierarchyConstants.TEXT_SIZE, (double) textView.getTextSize());
                                jSONObject3.put(ViewHierarchyConstants.TEXT_KEY, textView.getText());
                            } catch (JSONException unused) {
                                zzavs.zzez("Unable to get asset views information");
                                it = it2;
                                str3 = str2;
                                str4 = str;
                            }
                        } else {
                            str2 = str3;
                            str = str4;
                        }
                        jSONObject3.put("is_clickable", map3 != null && map3.containsKey(next.getKey()) && view2.isClickable());
                        jSONObject2.put((String) next.getKey(), jSONObject3);
                    } catch (JSONException unused2) {
                        str2 = str3;
                        str = str4;
                        zzavs.zzez("Unable to get asset views information");
                        it = it2;
                        str3 = str2;
                        str4 = str;
                    }
                    it = it2;
                    str3 = str2;
                    str4 = str;
                }
            }
        }
        return jSONObject2;
    }

    private final String zzv(View view) {
        try {
            JSONObject optJSONObject = this.zzfka.optJSONObject("tracking_urls_and_actions");
            if (optJSONObject == null) {
                optJSONObject = new JSONObject();
            }
            return this.zzefv.zzbw().zza(this.zzup, optJSONObject.optString("click_string"), view);
        } catch (Exception e) {
            zzavs.zzc("Exception obtaining click signals", e);
            return null;
        }
    }

    private final String zzw(View view) {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcls)).booleanValue()) {
            return null;
        }
        try {
            return this.zzefv.zzbw().zza(this.zzup, view, (Activity) null);
        } catch (Exception unused) {
            zzavs.zzex("Exception getting data.");
            return null;
        }
    }

    private final JSONObject zzfv(String str) {
        JSONObject jSONObject;
        try {
            jSONObject = new JSONObject();
            try {
                jSONObject.put("click_point", zzair());
                jSONObject.put("asset_id", str);
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            jSONObject = null;
            zzavs.zzc("Error occurred while grabbing click signals.", e);
            return jSONObject;
        }
        return jSONObject;
    }

    private final JSONObject zzair() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("x", zzdi(this.zzfkm.x));
            jSONObject.put("y", zzdi(this.zzfkm.y));
            jSONObject.put("start_x", zzdi(this.zzfkn.x));
            jSONObject.put("start_y", zzdi(this.zzfkn.y));
            return jSONObject;
        } catch (JSONException e) {
            zzavs.zzc("Error occurred while putting signals into JSON object.", e);
            return null;
        }
    }

    private static int[] zzx(View view) {
        int[] iArr = new int[2];
        if (view != null) {
            view.getLocationOnScreen(iArr);
        }
        return iArr;
    }

    private final int zzdi(int i) {
        return zzve.zzou().zzb(this.zzup, i);
    }

    public final void destroy() {
        this.zzfkb.destroy();
    }
}
