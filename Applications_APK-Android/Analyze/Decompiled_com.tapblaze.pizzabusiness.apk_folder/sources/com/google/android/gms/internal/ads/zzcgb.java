package com.google.android.gms.internal.ads;

import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcgb implements zzdxg<zzcfx> {
    public static zzcfx zza(zzbqs zzbqs, zzczu zzczu, zzcfe zzcfe, zzdhd zzdhd, ScheduledExecutorService scheduledExecutorService, zzchz zzchz) {
        return new zzcfx(zzbqs, zzczu, zzcfe, zzdhd, scheduledExecutorService, zzchz);
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
