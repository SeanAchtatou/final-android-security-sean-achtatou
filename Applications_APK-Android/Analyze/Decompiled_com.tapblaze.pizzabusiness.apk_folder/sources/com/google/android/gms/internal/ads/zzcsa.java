package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcsa implements Callable {
    static final Callable zzgfx = new zzcsa();

    private zzcsa() {
    }

    public final Object call() {
        return new zzcry(zzq.zzla().zzwx(), zzq.zzla().zzwy());
    }
}
