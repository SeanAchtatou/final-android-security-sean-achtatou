package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;
import java.io.IOException;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzev  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzev extends zzes<zzfc.zzc> {
    zzev() {
    }

    /* access modifiers changed from: package-private */
    public final boolean zze(zzgl zzgl) {
        return zzgl instanceof zzfc.zzd;
    }

    /* access modifiers changed from: package-private */
    public final zzex<zzfc.zzc> zzd(Object obj) {
        return ((zzfc.zzd) obj).zzqs;
    }

    /* access modifiers changed from: package-private */
    public final zzex<zzfc.zzc> zze(Object obj) {
        zzfc.zzd zzd = (zzfc.zzd) obj;
        if (zzd.zzqs.isImmutable()) {
            zzd.zzqs = (zzex) zzd.zzqs.clone();
        }
        return zzd.zzqs;
    }

    /* access modifiers changed from: package-private */
    public final void zzf(Object obj) {
        zzd(obj).zzgg();
    }

    /* access modifiers changed from: package-private */
    public final int zzb(Map.Entry<?, ?> entry) {
        zzfc.zzc zzc = (zzfc.zzc) entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzin zzin, Map.Entry<?, ?> entry) throws IOException {
        zzfc.zzc zzc = (zzfc.zzc) entry.getKey();
        throw new NoSuchMethodError();
    }
}
