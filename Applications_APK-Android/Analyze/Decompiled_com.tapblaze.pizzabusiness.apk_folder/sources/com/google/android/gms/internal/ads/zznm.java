package com.google.android.gms.internal.ads;

import android.net.Uri;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zznm implements zznl {
    private final byte[] data;
    private Uri uri;
    private int zzbej;
    private int zzbek;

    public zznm(byte[] bArr) {
        zzoc.checkNotNull(bArr);
        zzoc.checkArgument(bArr.length > 0);
        this.data = bArr;
    }

    public final long zza(zznq zznq) throws IOException {
        this.uri = zznq.uri;
        this.zzbej = (int) zznq.zzamw;
        this.zzbek = (int) (zznq.zzce == -1 ? ((long) this.data.length) - zznq.zzamw : zznq.zzce);
        int i = this.zzbek;
        if (i > 0 && this.zzbej + i <= this.data.length) {
            return (long) i;
        }
        int i2 = this.zzbej;
        long j = zznq.zzce;
        int length = this.data.length;
        StringBuilder sb = new StringBuilder(77);
        sb.append("Unsatisfiable range: [");
        sb.append(i2);
        sb.append(", ");
        sb.append(j);
        sb.append("], length: ");
        sb.append(length);
        throw new IOException(sb.toString());
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        if (i2 == 0) {
            return 0;
        }
        int i3 = this.zzbek;
        if (i3 == 0) {
            return -1;
        }
        int min = Math.min(i2, i3);
        System.arraycopy(this.data, this.zzbej, bArr, i, min);
        this.zzbej += min;
        this.zzbek -= min;
        return min;
    }

    public final Uri getUri() {
        return this.uri;
    }

    public final void close() throws IOException {
        this.uri = null;
    }
}
