package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzdkn {
    static final /* synthetic */ int[] zzgzw = new int[zzdmt.values().length];
    static final /* synthetic */ int[] zzgzx = new int[zzdmr.values().length];
    static final /* synthetic */ int[] zzgzy = new int[zzdmd.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(21:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|19|21|22|23|24|25|26|28) */
    /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|19|21|22|23|24|25|26|28) */
    /* JADX WARNING: Can't wrap try/catch for region: R(23:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|21|22|23|24|25|26|28) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x003d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0064 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x006e */
    static {
        /*
            com.google.android.gms.internal.ads.zzdmd[] r0 = com.google.android.gms.internal.ads.zzdmd.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.android.gms.internal.ads.zzdkn.zzgzy = r0
            r0 = 1
            int[] r1 = com.google.android.gms.internal.ads.zzdkn.zzgzy     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.google.android.gms.internal.ads.zzdmd r2 = com.google.android.gms.internal.ads.zzdmd.UNCOMPRESSED     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.google.android.gms.internal.ads.zzdkn.zzgzy     // Catch:{ NoSuchFieldError -> 0x001f }
            com.google.android.gms.internal.ads.zzdmd r3 = com.google.android.gms.internal.ads.zzdmd.DO_NOT_USE_CRUNCHY_UNCOMPRESSED     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            r2 = 3
            int[] r3 = com.google.android.gms.internal.ads.zzdkn.zzgzy     // Catch:{ NoSuchFieldError -> 0x002a }
            com.google.android.gms.internal.ads.zzdmd r4 = com.google.android.gms.internal.ads.zzdmd.COMPRESSED     // Catch:{ NoSuchFieldError -> 0x002a }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            com.google.android.gms.internal.ads.zzdmr[] r3 = com.google.android.gms.internal.ads.zzdmr.values()
            int r3 = r3.length
            int[] r3 = new int[r3]
            com.google.android.gms.internal.ads.zzdkn.zzgzx = r3
            int[] r3 = com.google.android.gms.internal.ads.zzdkn.zzgzx     // Catch:{ NoSuchFieldError -> 0x003d }
            com.google.android.gms.internal.ads.zzdmr r4 = com.google.android.gms.internal.ads.zzdmr.NIST_P256     // Catch:{ NoSuchFieldError -> 0x003d }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x003d }
            r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x003d }
        L_0x003d:
            int[] r3 = com.google.android.gms.internal.ads.zzdkn.zzgzx     // Catch:{ NoSuchFieldError -> 0x0047 }
            com.google.android.gms.internal.ads.zzdmr r4 = com.google.android.gms.internal.ads.zzdmr.NIST_P384     // Catch:{ NoSuchFieldError -> 0x0047 }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
            r3[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0047 }
        L_0x0047:
            int[] r3 = com.google.android.gms.internal.ads.zzdkn.zzgzx     // Catch:{ NoSuchFieldError -> 0x0051 }
            com.google.android.gms.internal.ads.zzdmr r4 = com.google.android.gms.internal.ads.zzdmr.NIST_P521     // Catch:{ NoSuchFieldError -> 0x0051 }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0051 }
            r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0051 }
        L_0x0051:
            com.google.android.gms.internal.ads.zzdmt[] r3 = com.google.android.gms.internal.ads.zzdmt.values()
            int r3 = r3.length
            int[] r3 = new int[r3]
            com.google.android.gms.internal.ads.zzdkn.zzgzw = r3
            int[] r3 = com.google.android.gms.internal.ads.zzdkn.zzgzw     // Catch:{ NoSuchFieldError -> 0x0064 }
            com.google.android.gms.internal.ads.zzdmt r4 = com.google.android.gms.internal.ads.zzdmt.SHA1     // Catch:{ NoSuchFieldError -> 0x0064 }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0064 }
            r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0064 }
        L_0x0064:
            int[] r0 = com.google.android.gms.internal.ads.zzdkn.zzgzw     // Catch:{ NoSuchFieldError -> 0x006e }
            com.google.android.gms.internal.ads.zzdmt r3 = com.google.android.gms.internal.ads.zzdmt.SHA256     // Catch:{ NoSuchFieldError -> 0x006e }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
            r0[r3] = r1     // Catch:{ NoSuchFieldError -> 0x006e }
        L_0x006e:
            int[] r0 = com.google.android.gms.internal.ads.zzdkn.zzgzw     // Catch:{ NoSuchFieldError -> 0x0078 }
            com.google.android.gms.internal.ads.zzdmt r1 = com.google.android.gms.internal.ads.zzdmt.SHA512     // Catch:{ NoSuchFieldError -> 0x0078 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0078 }
        L_0x0078:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdkn.<clinit>():void");
    }
}
