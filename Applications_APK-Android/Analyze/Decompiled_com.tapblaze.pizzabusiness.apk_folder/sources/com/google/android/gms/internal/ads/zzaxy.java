package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaxy {
    public static boolean zzcs(int i) {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzclq)).booleanValue()) {
            return true;
        }
        return ((Boolean) zzve.zzoy().zzd(zzzn.zzclr)).booleanValue() || i <= 15299999;
    }
}
