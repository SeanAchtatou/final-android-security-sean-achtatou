package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbe  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final /* synthetic */ class zzbe implements Runnable {
    private final zzbt zzbf;
    private final zzbc zzbu;

    zzbe(zzbc zzbc, zzbt zzbt) {
        this.zzbu = zzbc;
        this.zzbf = zzbt;
    }

    public final void run() {
        this.zzbu.zzh(this.zzbf);
    }
}
