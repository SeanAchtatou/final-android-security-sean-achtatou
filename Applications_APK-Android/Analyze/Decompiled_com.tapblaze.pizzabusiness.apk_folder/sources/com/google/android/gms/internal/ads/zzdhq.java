package com.google.android.gms.internal.ads;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdhq<V> implements Runnable {
    @NullableDecl
    private zzdho<V> zzgxm;

    zzdhq(zzdho<V> zzdho) {
        this.zzgxm = zzdho;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdho.zza(com.google.android.gms.internal.ads.zzdho, java.util.concurrent.ScheduledFuture):java.util.concurrent.ScheduledFuture
     arg types: [com.google.android.gms.internal.ads.zzdho<V>, ?[OBJECT, ARRAY]]
     candidates:
      com.google.android.gms.internal.ads.zzdgn.zza(com.google.android.gms.internal.ads.zzded, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzdgn<T>
      com.google.android.gms.internal.ads.zzdfs.zza(com.google.android.gms.internal.ads.zzdfs, com.google.android.gms.internal.ads.zzdfs$zzd):com.google.android.gms.internal.ads.zzdfs$zzd
      com.google.android.gms.internal.ads.zzdfs.zza(com.google.android.gms.internal.ads.zzdfs, com.google.android.gms.internal.ads.zzdfs$zzk):com.google.android.gms.internal.ads.zzdfs$zzk
      com.google.android.gms.internal.ads.zzdfs.zza(com.google.android.gms.internal.ads.zzdfs, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.ads.zzdfs.zza(java.lang.Runnable, java.util.concurrent.Executor):void
      com.google.android.gms.internal.ads.zzdho.zza(com.google.android.gms.internal.ads.zzdho, java.util.concurrent.ScheduledFuture):java.util.concurrent.ScheduledFuture */
    public final void run() {
        zzdhe zza;
        String str;
        zzdho<V> zzdho = this.zzgxm;
        if (zzdho != null && (zza = ((zzdho) zzdho).zzgxk) != null) {
            this.zzgxm = null;
            if (zza.isDone()) {
                zzdho.setFuture(zza);
                return;
            }
            try {
                ScheduledFuture zzb = ((zzdho) zzdho).zzgxl;
                ScheduledFuture unused = ((zzdho) zzdho).zzgxl = null;
                str = "Timed out";
                if (zzb != null) {
                    long abs = Math.abs(zzb.getDelay(TimeUnit.MILLISECONDS));
                    if (abs > 10) {
                        StringBuilder sb = new StringBuilder(str.length() + 66);
                        sb.append(str);
                        sb.append(" (timeout delayed by ");
                        sb.append(abs);
                        sb.append(" ms after scheduled time)");
                        str = sb.toString();
                    }
                }
                String valueOf = String.valueOf(str);
                String valueOf2 = String.valueOf(zza);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 2 + String.valueOf(valueOf2).length());
                sb2.append(valueOf);
                sb2.append(": ");
                sb2.append(valueOf2);
                zzdho.setException(new zzdhp(sb2.toString()));
                zza.cancel(true);
            } catch (Throwable th) {
                zza.cancel(true);
                throw th;
            }
        }
    }
}
