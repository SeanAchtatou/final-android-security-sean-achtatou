package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzaa {
    void zza(zzq<?> zzq, zzae zzae);

    void zza(zzq<?> zzq, zzz<?> zzz, Runnable runnable);

    void zzb(zzq<?> zzq, zzz<?> zzz);
}
