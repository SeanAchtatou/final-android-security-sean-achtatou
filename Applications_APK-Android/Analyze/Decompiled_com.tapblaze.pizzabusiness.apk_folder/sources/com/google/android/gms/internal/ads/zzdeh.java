package com.google.android.gms.internal.ads;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdeh {
    @NullableDecl
    Object value;
    @NullableDecl
    zzdeh zzgub;

    private zzdeh() {
    }
}
