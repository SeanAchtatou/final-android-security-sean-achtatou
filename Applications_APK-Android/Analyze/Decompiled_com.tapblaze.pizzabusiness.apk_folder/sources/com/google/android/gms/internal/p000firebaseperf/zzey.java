package com.google.android.gms.internal.p000firebaseperf;

import java.lang.reflect.Type;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzey  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public enum zzey {
    DOUBLE(0, zzfa.SCALAR, zzfn.DOUBLE),
    FLOAT(1, zzfa.SCALAR, zzfn.FLOAT),
    INT64(2, zzfa.SCALAR, zzfn.LONG),
    UINT64(3, zzfa.SCALAR, zzfn.LONG),
    INT32(4, zzfa.SCALAR, zzfn.INT),
    FIXED64(5, zzfa.SCALAR, zzfn.LONG),
    FIXED32(6, zzfa.SCALAR, zzfn.INT),
    BOOL(7, zzfa.SCALAR, zzfn.BOOLEAN),
    STRING(8, zzfa.SCALAR, zzfn.STRING),
    MESSAGE(9, zzfa.SCALAR, zzfn.MESSAGE),
    BYTES(10, zzfa.SCALAR, zzfn.BYTE_STRING),
    UINT32(11, zzfa.SCALAR, zzfn.INT),
    ENUM(12, zzfa.SCALAR, zzfn.ENUM),
    SFIXED32(13, zzfa.SCALAR, zzfn.INT),
    SFIXED64(14, zzfa.SCALAR, zzfn.LONG),
    SINT32(15, zzfa.SCALAR, zzfn.INT),
    SINT64(16, zzfa.SCALAR, zzfn.LONG),
    GROUP(17, zzfa.SCALAR, zzfn.MESSAGE),
    DOUBLE_LIST(18, zzfa.VECTOR, zzfn.DOUBLE),
    FLOAT_LIST(19, zzfa.VECTOR, zzfn.FLOAT),
    INT64_LIST(20, zzfa.VECTOR, zzfn.LONG),
    UINT64_LIST(21, zzfa.VECTOR, zzfn.LONG),
    INT32_LIST(22, zzfa.VECTOR, zzfn.INT),
    FIXED64_LIST(23, zzfa.VECTOR, zzfn.LONG),
    FIXED32_LIST(24, zzfa.VECTOR, zzfn.INT),
    BOOL_LIST(25, zzfa.VECTOR, zzfn.BOOLEAN),
    STRING_LIST(26, zzfa.VECTOR, zzfn.STRING),
    MESSAGE_LIST(27, zzfa.VECTOR, zzfn.MESSAGE),
    BYTES_LIST(28, zzfa.VECTOR, zzfn.BYTE_STRING),
    UINT32_LIST(29, zzfa.VECTOR, zzfn.INT),
    ENUM_LIST(30, zzfa.VECTOR, zzfn.ENUM),
    SFIXED32_LIST(31, zzfa.VECTOR, zzfn.INT),
    SFIXED64_LIST(32, zzfa.VECTOR, zzfn.LONG),
    SINT32_LIST(33, zzfa.VECTOR, zzfn.INT),
    SINT64_LIST(34, zzfa.VECTOR, zzfn.LONG),
    DOUBLE_LIST_PACKED(35, zzfa.PACKED_VECTOR, zzfn.DOUBLE),
    FLOAT_LIST_PACKED(36, zzfa.PACKED_VECTOR, zzfn.FLOAT),
    INT64_LIST_PACKED(37, zzfa.PACKED_VECTOR, zzfn.LONG),
    UINT64_LIST_PACKED(38, zzfa.PACKED_VECTOR, zzfn.LONG),
    INT32_LIST_PACKED(39, zzfa.PACKED_VECTOR, zzfn.INT),
    FIXED64_LIST_PACKED(40, zzfa.PACKED_VECTOR, zzfn.LONG),
    FIXED32_LIST_PACKED(41, zzfa.PACKED_VECTOR, zzfn.INT),
    BOOL_LIST_PACKED(42, zzfa.PACKED_VECTOR, zzfn.BOOLEAN),
    UINT32_LIST_PACKED(43, zzfa.PACKED_VECTOR, zzfn.INT),
    ENUM_LIST_PACKED(44, zzfa.PACKED_VECTOR, zzfn.ENUM),
    SFIXED32_LIST_PACKED(45, zzfa.PACKED_VECTOR, zzfn.INT),
    SFIXED64_LIST_PACKED(46, zzfa.PACKED_VECTOR, zzfn.LONG),
    SINT32_LIST_PACKED(47, zzfa.PACKED_VECTOR, zzfn.INT),
    SINT64_LIST_PACKED(48, zzfa.PACKED_VECTOR, zzfn.LONG),
    GROUP_LIST(49, zzfa.VECTOR, zzfn.MESSAGE),
    MAP(50, zzfa.MAP, zzfn.VOID);
    
    private static final zzey[] zzqa;
    private static final Type[] zzqb = new Type[0];
    private final int id;
    private final zzfn zzpw;
    private final zzfa zzpx;
    private final Class<?> zzpy;
    private final boolean zzpz;

    private zzey(int i, zzfa zzfa, zzfn zzfn) {
        int i2;
        this.id = i;
        this.zzpx = zzfa;
        this.zzpw = zzfn;
        int i3 = zzfb.zzqj[zzfa.ordinal()];
        if (i3 == 1) {
            this.zzpy = zzfn.zzhs();
        } else if (i3 != 2) {
            this.zzpy = null;
        } else {
            this.zzpy = zzfn.zzhs();
        }
        boolean z = false;
        if (!(zzfa != zzfa.SCALAR || (i2 = zzfb.zzqk[zzfn.ordinal()]) == 1 || i2 == 2 || i2 == 3)) {
            z = true;
        }
        this.zzpz = z;
    }

    public final int id() {
        return this.id;
    }

    static {
        zzey[] values = values();
        zzqa = new zzey[values.length];
        for (zzey zzey : values) {
            zzqa[zzey.id] = zzey;
        }
    }
}
