package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdlc extends zzdrt<zzdlc, zza> implements zzdtg {
    private static volatile zzdtn<zzdlc> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlc zzhai;
    private int zzhah;

    private zzdlc() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdlc, zza> implements zzdtg {
        private zza() {
            super(zzdlc.zzhai);
        }

        /* synthetic */ zza(zzdld zzdld) {
            this();
        }
    }

    public final int zzasx() {
        return this.zzhah;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdld.zzdk[i - 1]) {
            case 1:
                return new zzdlc();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhai, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u000b", new Object[]{"zzhah"});
            case 4:
                return zzhai;
            case 5:
                zzdtn<zzdlc> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlc.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhai);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdlc zzasy() {
        return zzhai;
    }

    static {
        zzdlc zzdlc = new zzdlc();
        zzhai = zzdlc;
        zzdrt.zza(zzdlc.class, zzdlc);
    }
}
