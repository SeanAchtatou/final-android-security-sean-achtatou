package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcfa implements Runnable {
    private final String zzcyr;
    private final zzcex zzfue;

    zzcfa(zzcex zzcex, String str) {
        this.zzfue = zzcex;
        this.zzcyr = str;
    }

    public final void run() {
        zzcex zzcex = this.zzfue;
        zzcex.zzftz.zzgf(this.zzcyr);
    }
}
