package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbiu implements zzbph {
    private final zzdac zzfbn;

    public zzbiu(zzdac zzdac) {
        this.zzfbn = zzdac;
    }

    public final void zzbv(Context context) {
        try {
            this.zzfbn.pause();
        } catch (zzdab e) {
            zzavs.zzd("Cannot invoke onPause for the mediation adapter.", e);
        }
    }

    public final void zzbw(Context context) {
        try {
            this.zzfbn.resume();
            if (context != null) {
                this.zzfbn.onContextChanged(context);
            }
        } catch (zzdab e) {
            zzavs.zzd("Cannot invoke onResume for the mediation adapter.", e);
        }
    }

    public final void zzbx(Context context) {
        try {
            this.zzfbn.destroy();
        } catch (zzdab e) {
            zzavs.zzd("Cannot invoke onDestroy for the mediation adapter.", e);
        }
    }
}
