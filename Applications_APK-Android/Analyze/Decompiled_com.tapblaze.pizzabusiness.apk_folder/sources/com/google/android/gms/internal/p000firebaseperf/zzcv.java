package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzcv  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzcv extends zzfc<zzcv, zza> implements zzgn {
    private static volatile zzgv<zzcv> zzij;
    /* access modifiers changed from: private */
    public static final zzcv zzks;
    private int zzie;
    private zzgf<String, String> zzit = zzgf.zzib();
    private String zzkg = "";
    private int zzkh;
    private long zzki;
    private long zzkj;
    private int zzkk;
    private int zzkl;
    private String zzkm = "";
    private long zzkn;
    private long zzko;
    private long zzkp;
    private long zzkq;
    private zzfm<zzde> zzkr = zzhh();

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzcv$zzb */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public enum zzb implements zzff {
        HTTP_METHOD_UNKNOWN(0),
        GET(1),
        PUT(2),
        POST(3),
        DELETE(4),
        HEAD(5),
        PATCH(6),
        OPTIONS(7),
        TRACE(8),
        CONNECT(9);
        
        private static final zzfi<zzb> zzja = new zzcy();
        private final int value;

        public final int getNumber() {
            return this.value;
        }

        public static zzb zzm(int i) {
            switch (i) {
                case 0:
                    return HTTP_METHOD_UNKNOWN;
                case 1:
                    return GET;
                case 2:
                    return PUT;
                case 3:
                    return POST;
                case 4:
                    return DELETE;
                case 5:
                    return HEAD;
                case 6:
                    return PATCH;
                case 7:
                    return OPTIONS;
                case 8:
                    return TRACE;
                case 9:
                    return CONNECT;
                default:
                    return null;
            }
        }

        public static zzfh zzdp() {
            return zzcx.zzjf;
        }

        public final String toString() {
            return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + getNumber() + " name=" + name() + '>';
        }

        private zzb(int i) {
            this.value = i;
        }
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzcv$zzc */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static final class zzc {
        static final zzgd<String, String> zziv = zzgd.zza(zzih.STRING, "", zzih.STRING, "");
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzcv$zzd */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public enum zzd implements zzff {
        NETWORK_CLIENT_ERROR_REASON_UNKNOWN(0),
        GENERIC_CLIENT_ERROR(1);
        
        private static final zzfi<zzd> zzja = new zzcz();
        private final int value;

        public final int getNumber() {
            return this.value;
        }

        public static zzfh zzdp() {
            return zzdb.zzjf;
        }

        public final String toString() {
            return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + getNumber() + " name=" + name() + '>';
        }

        private zzd(int i) {
            this.value = i;
        }
    }

    private zzcv() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzcv$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzcv, zza> implements zzgn {
        private zza() {
            super(zzcv.zzks);
        }

        public final zza zzae(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).setUrl(str);
            return this;
        }

        public final zza zzb(zzb zzb) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zza(zzb);
            return this;
        }

        public final zza zzah(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zzab(j);
            return this;
        }

        public final zza zzai(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zzac(j);
            return this;
        }

        public final zza zzb(zzd zzd) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zza(zzd);
            return this;
        }

        public final boolean zzbl() {
            return ((zzcv) this.zzqq).zzbl();
        }

        public final zza zzl(int i) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).setHttpResponseCode(i);
            return this;
        }

        public final zza zzaf(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).setResponseContentType(str);
            return this;
        }

        public final zza zzfb() {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zzeo();
            return this;
        }

        public final boolean zzep() {
            return ((zzcv) this.zzqq).zzep();
        }

        public final zza zzaj(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zzad(j);
            return this;
        }

        public final zza zzak(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zzae(j);
            return this;
        }

        public final long zzeu() {
            return ((zzcv) this.zzqq).zzeu();
        }

        public final zza zzal(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zzaf(j);
            return this;
        }

        public final boolean zzev() {
            return ((zzcv) this.zzqq).zzev();
        }

        public final zza zzam(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zzag(j);
            return this;
        }

        public final zza zzfc() {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zzdl().clear();
            return this;
        }

        public final zza zzc(Map<String, String> map) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zzdl().putAll(map);
            return this;
        }

        public final zza zzb(Iterable<? extends zzde> iterable) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcv) this.zzqq).zza(iterable);
            return this;
        }

        /* synthetic */ zza(zzcw zzcw) {
            this();
        }
    }

    public final String getUrl() {
        return this.zzkg;
    }

    /* access modifiers changed from: private */
    public final void setUrl(String str) {
        str.getClass();
        this.zzie |= 1;
        this.zzkg = str;
    }

    public final boolean zzeh() {
        return (this.zzie & 2) != 0;
    }

    public final zzb zzei() {
        zzb zzm = zzb.zzm(this.zzkh);
        return zzm == null ? zzb.HTTP_METHOD_UNKNOWN : zzm;
    }

    /* access modifiers changed from: private */
    public final void zza(zzb zzb2) {
        this.zzkh = zzb2.getNumber();
        this.zzie |= 2;
    }

    public final boolean zzej() {
        return (this.zzie & 4) != 0;
    }

    public final long zzek() {
        return this.zzki;
    }

    /* access modifiers changed from: private */
    public final void zzab(long j) {
        this.zzie |= 4;
        this.zzki = j;
    }

    public final boolean zzel() {
        return (this.zzie & 8) != 0;
    }

    public final long zzem() {
        return this.zzkj;
    }

    /* access modifiers changed from: private */
    public final void zzac(long j) {
        this.zzie |= 8;
        this.zzkj = j;
    }

    /* access modifiers changed from: private */
    public final void zza(zzd zzd2) {
        this.zzkk = zzd2.getNumber();
        this.zzie |= 16;
    }

    public final boolean zzbl() {
        return (this.zzie & 32) != 0;
    }

    public final int zzen() {
        return this.zzkl;
    }

    /* access modifiers changed from: private */
    public final void setHttpResponseCode(int i) {
        this.zzie |= 32;
        this.zzkl = i;
    }

    /* access modifiers changed from: private */
    public final void setResponseContentType(String str) {
        str.getClass();
        this.zzie |= 64;
        this.zzkm = str;
    }

    /* access modifiers changed from: private */
    public final void zzeo() {
        this.zzie &= -65;
        this.zzkm = zzks.zzkm;
    }

    public final boolean zzep() {
        return (this.zzie & 128) != 0;
    }

    public final long zzeq() {
        return this.zzkn;
    }

    /* access modifiers changed from: private */
    public final void zzad(long j) {
        this.zzie |= 128;
        this.zzkn = j;
    }

    public final boolean zzer() {
        return (this.zzie & 256) != 0;
    }

    public final long zzes() {
        return this.zzko;
    }

    /* access modifiers changed from: private */
    public final void zzae(long j) {
        this.zzie |= 256;
        this.zzko = j;
    }

    public final boolean zzet() {
        return (this.zzie & 512) != 0;
    }

    public final long zzeu() {
        return this.zzkp;
    }

    /* access modifiers changed from: private */
    public final void zzaf(long j) {
        this.zzie |= 512;
        this.zzkp = j;
    }

    public final boolean zzev() {
        return (this.zzie & 1024) != 0;
    }

    public final long zzew() {
        return this.zzkq;
    }

    /* access modifiers changed from: private */
    public final void zzag(long j) {
        this.zzie |= 1024;
        this.zzkq = j;
    }

    /* access modifiers changed from: private */
    public final Map<String, String> zzdl() {
        if (!this.zzit.isMutable()) {
            this.zzit = this.zzit.zzic();
        }
        return this.zzit;
    }

    public final List<zzde> zzex() {
        return this.zzkr;
    }

    /* access modifiers changed from: private */
    public final void zza(Iterable<? extends zzde> iterable) {
        if (!this.zzkr.zzgf()) {
            this.zzkr = zzfc.zza(this.zzkr);
        }
        zzdt.zza(iterable, this.zzkr);
    }

    public static zza zzey() {
        return (zza) zzks.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzcw.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzcv();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzks, "\u0001\r\u0000\u0001\u0001\r\r\u0001\u0001\u0000\u0001\b\u0000\u0002\f\u0001\u0003\u0002\u0002\u0004\u0002\u0003\u0005\u0004\u0005\u0006\b\u0006\u0007\u0002\u0007\b\u0002\b\t\u0002\t\n\u0002\n\u000b\f\u0004\f2\r\u001b", new Object[]{"zzie", "zzkg", "zzkh", zzb.zzdp(), "zzki", "zzkj", "zzkl", "zzkm", "zzkn", "zzko", "zzkp", "zzkq", "zzkk", zzd.zzdp(), "zzit", zzc.zziv, "zzkr", zzde.class});
            case 4:
                return zzks;
            case 5:
                zzgv<zzcv> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzcv.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzks);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzcv zzez() {
        return zzks;
    }

    static {
        zzcv zzcv = new zzcv();
        zzks = zzcv;
        zzfc.zza(zzcv.class, zzcv);
    }
}
