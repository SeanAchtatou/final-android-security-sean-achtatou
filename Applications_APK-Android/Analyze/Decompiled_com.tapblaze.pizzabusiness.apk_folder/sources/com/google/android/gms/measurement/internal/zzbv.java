package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzmg;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class zzbv implements zzes {
    static final zzes zza = new zzbv();

    private zzbv() {
    }

    public final Object zza() {
        return Long.valueOf(zzmg.zze());
    }
}
