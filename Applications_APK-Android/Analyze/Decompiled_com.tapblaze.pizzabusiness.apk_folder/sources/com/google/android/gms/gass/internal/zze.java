package com.google.android.gms.gass.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.ads.zzbs;
import com.google.android.gms.internal.ads.zzdrg;
import com.google.android.gms.internal.ads.zzdse;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zze extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zze> CREATOR = new zzh();
    private final int versionCode;
    private zzbs.zza zzgtc = null;
    private byte[] zzgtd;

    zze(int i, byte[] bArr) {
        this.versionCode = i;
        this.zzgtd = bArr;
        zzaqr();
    }

    public final zzbs.zza zzaqq() {
        if (!(this.zzgtc != null)) {
            try {
                this.zzgtc = zzbs.zza.zza(this.zzgtd, zzdrg.zzazi());
                this.zzgtd = null;
            } catch (zzdse e) {
                throw new IllegalStateException(e);
            }
        }
        zzaqr();
        return this.zzgtc;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.versionCode);
        byte[] bArr = this.zzgtd;
        if (bArr == null) {
            bArr = this.zzgtc.toByteArray();
        }
        SafeParcelWriter.writeByteArray(parcel, 2, bArr, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    private final void zzaqr() {
        if (this.zzgtc == null && this.zzgtd != null) {
            return;
        }
        if (this.zzgtc != null && this.zzgtd == null) {
            return;
        }
        if (this.zzgtc != null && this.zzgtd != null) {
            throw new IllegalStateException("Invalid internal representation - full");
        } else if (this.zzgtc == null && this.zzgtd == null) {
            throw new IllegalStateException("Invalid internal representation - empty");
        } else {
            throw new IllegalStateException("Impossible");
        }
    }
}
