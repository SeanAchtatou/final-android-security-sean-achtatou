package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcoa implements zzdxg<zzcnw> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzbup> zzfyl;

    public zzcoa(zzdxp<Context> zzdxp, zzdxp<zzbup> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfyl = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzcnw(this.zzejv.get(), this.zzfyl.get());
    }
}
