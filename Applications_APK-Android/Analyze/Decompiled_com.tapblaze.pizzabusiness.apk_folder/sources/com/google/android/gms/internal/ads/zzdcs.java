package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzdcs implements zzbrn {
    private final zzdca zzgrk;

    zzdcs(zzdca zzdca) {
        this.zzgrk = zzdca;
    }

    public final void zzp(Object obj) {
        zzdca zzdca = this.zzgrk;
        ((zzdcx) obj).zzb((zzdco) zzdca.zzaqd(), zzdca.zzaqe());
    }
}
