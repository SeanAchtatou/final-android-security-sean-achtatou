package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.search.SearchAdRequest;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzys extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzys> CREATOR = new zzyr();
    public final String zzblv;

    public zzys(SearchAdRequest searchAdRequest) {
        this.zzblv = searchAdRequest.getQuery();
    }

    zzys(String str) {
        this.zzblv = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 15, this.zzblv, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
