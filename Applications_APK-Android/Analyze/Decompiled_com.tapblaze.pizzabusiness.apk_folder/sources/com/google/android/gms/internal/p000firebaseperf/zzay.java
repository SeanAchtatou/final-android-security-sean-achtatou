package com.google.android.gms.internal.p000firebaseperf;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.firebase.FirebaseApp;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzay  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzay {
    private static zzay zzbc;
    private zzbi zzai = zzbi.zzco();
    private SharedPreferences zzbd;

    private zzay() {
    }

    public static synchronized zzay zzba() {
        zzay zzay;
        synchronized (zzay.class) {
            if (zzbc == null) {
                zzbc = new zzay();
            }
            zzay = zzbc;
        }
        return zzay;
    }

    public final synchronized void zzd(Context context) {
        if (this.zzbd == null && context != null) {
            this.zzbd = context.getSharedPreferences("FirebasePerfSharedPrefs", 0);
        }
    }

    public final zzbo<Boolean> zzb(String str) {
        if (str == null) {
            this.zzai.zzm("Key is null when getting boolean value on device cache.");
            return zzbo.zzcy();
        }
        if (this.zzbd == null) {
            zzd(zzbb());
            if (this.zzbd == null) {
                return zzbo.zzcy();
            }
        }
        if (!this.zzbd.contains(str)) {
            return zzbo.zzcy();
        }
        try {
            return zzbo.zzb(Boolean.valueOf(this.zzbd.getBoolean(str, false)));
        } catch (ClassCastException e) {
            this.zzai.zzm(String.format("Key %s from sharedPreferences has type other than long: %s", str, e.getMessage()));
            return zzbo.zzcy();
        }
    }

    public final boolean zza(String str, boolean z) {
        if (str == null) {
            this.zzai.zzm("Key is null when setting boolean value on device cache.");
            return false;
        }
        if (this.zzbd == null) {
            zzd(zzbb());
            if (this.zzbd == null) {
                return false;
            }
        }
        this.zzbd.edit().putBoolean(str, z).apply();
        return true;
    }

    public final zzbo<String> zzc(String str) {
        if (str == null) {
            this.zzai.zzm("Key is null when getting String value on device cache.");
            return zzbo.zzcy();
        }
        if (this.zzbd == null) {
            zzd(zzbb());
            if (this.zzbd == null) {
                return zzbo.zzcy();
            }
        }
        if (!this.zzbd.contains(str)) {
            return zzbo.zzcy();
        }
        try {
            return zzbo.zzb(this.zzbd.getString(str, ""));
        } catch (ClassCastException e) {
            this.zzai.zzm(String.format("Key %s from sharedPreferences has type other than String: %s", str, e.getMessage()));
            return zzbo.zzcy();
        }
    }

    public final boolean zza(String str, String str2) {
        if (str == null) {
            this.zzai.zzm("Key is null when setting String value on device cache.");
            return false;
        }
        if (this.zzbd == null) {
            zzd(zzbb());
            if (this.zzbd == null) {
                return false;
            }
        }
        if (str2 == null) {
            this.zzbd.edit().remove(str).apply();
            return true;
        }
        this.zzbd.edit().putString(str, str2).apply();
        return true;
    }

    public final zzbo<Float> zzd(String str) {
        if (str == null) {
            this.zzai.zzm("Key is null when getting float value on device cache.");
            return zzbo.zzcy();
        }
        if (this.zzbd == null) {
            zzd(zzbb());
            if (this.zzbd == null) {
                return zzbo.zzcy();
            }
        }
        if (!this.zzbd.contains(str)) {
            return zzbo.zzcy();
        }
        try {
            return zzbo.zzb(Float.valueOf(this.zzbd.getFloat(str, 0.0f)));
        } catch (ClassCastException e) {
            this.zzai.zzm(String.format("Key %s from sharedPreferences has type other than float: %s", str, e.getMessage()));
            return zzbo.zzcy();
        }
    }

    public final boolean zza(String str, float f) {
        if (str == null) {
            this.zzai.zzm("Key is null when setting float value on device cache.");
            return false;
        }
        if (this.zzbd == null) {
            zzd(zzbb());
            if (this.zzbd == null) {
                return false;
            }
        }
        this.zzbd.edit().putFloat(str, f).apply();
        return true;
    }

    public final zzbo<Long> zze(String str) {
        if (str == null) {
            this.zzai.zzm("Key is null when getting long value on device cache.");
            return zzbo.zzcy();
        }
        if (this.zzbd == null) {
            zzd(zzbb());
            if (this.zzbd == null) {
                return zzbo.zzcy();
            }
        }
        if (!this.zzbd.contains(str)) {
            return zzbo.zzcy();
        }
        try {
            return zzbo.zzb(Long.valueOf(this.zzbd.getLong(str, 0)));
        } catch (ClassCastException e) {
            this.zzai.zzm(String.format("Key %s from sharedPreferences has type other than long: %s", str, e.getMessage()));
            return zzbo.zzcy();
        }
    }

    public final boolean zza(String str, long j) {
        if (str == null) {
            this.zzai.zzm("Key is null when setting long value on device cache.");
            return false;
        }
        if (this.zzbd == null) {
            zzd(zzbb());
            if (this.zzbd == null) {
                return false;
            }
        }
        this.zzbd.edit().putLong(str, j).apply();
        return true;
    }

    private static Context zzbb() {
        try {
            FirebaseApp.getInstance();
            return FirebaseApp.getInstance().getApplicationContext();
        } catch (IllegalStateException unused) {
            return null;
        }
    }
}
