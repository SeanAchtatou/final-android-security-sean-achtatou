package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.lang.reflect.InvocationTargetException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzfs extends zzfw {
    public zzfs(zzei zzei, String str, String str2, zzbs.zza.zzb zzb, int i, int i2) {
        super(zzei, str, str2, zzb, i, 48);
    }

    /* access modifiers changed from: protected */
    public final void zzcn() throws IllegalAccessException, InvocationTargetException {
        this.zzzt.zze(zzcd.ENUM_FAILURE);
        boolean booleanValue = ((Boolean) this.zzaae.invoke(null, this.zzuv.getContext())).booleanValue();
        synchronized (this.zzzt) {
            if (booleanValue) {
                this.zzzt.zze(zzcd.ENUM_TRUE);
            } else {
                this.zzzt.zze(zzcd.ENUM_FALSE);
            }
        }
    }
}
