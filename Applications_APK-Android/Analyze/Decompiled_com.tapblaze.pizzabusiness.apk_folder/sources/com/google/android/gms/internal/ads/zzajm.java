package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzajm implements zzazp<zzaif> {
    private final /* synthetic */ zzajf zzdag;

    zzajm(zzajj zzajj, zzajf zzajf) {
        this.zzdag = zzajf;
    }

    public final /* synthetic */ void zzh(Object obj) {
        zzavs.zzed("Getting a new session for JS Engine.");
        this.zzdag.zzm(((zzaif) obj).zzrz());
    }
}
