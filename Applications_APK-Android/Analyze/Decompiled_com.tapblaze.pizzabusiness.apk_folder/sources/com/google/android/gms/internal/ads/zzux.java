package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzux extends zzvb<zzwk> {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ zzup zzcdi;

    zzux(zzup zzup, Context context) {
        this.zzcdi = zzup;
        this.val$context = context;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzop() {
        zzup.zza(this.val$context, "mobile_ads_settings");
        return new zzyf();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzccz.zzi(this.val$context);
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zza(ObjectWrapper.wrap(this.val$context), 19649000);
    }
}
