package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzsd extends IInterface {
    zzrx zza(zzry zzry) throws RemoteException;
}
