package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzce;
import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzda  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzda extends zzfc<zzda, zza> implements zzgn {
    private static volatile zzgv<zzda> zzij;
    /* access modifiers changed from: private */
    public static final zzda zzll;
    private int zzie;
    private zzce zzlh;
    private zzdn zzli;
    private zzcv zzlj;
    private zzcq zzlk;

    private zzda() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzda$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzda, zza> implements zzgn {
        private zza() {
            super(zzda.zzll);
        }

        public final zza zza(zzce.zzb zzb) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzda) this.zzqq).zzb((zzce) ((zzfc) zzb.zzhp()));
            return this;
        }

        public final zza zzb(zzdn zzdn) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzda) this.zzqq).zza(zzdn);
            return this;
        }

        public final zza zzd(zzcv zzcv) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzda) this.zzqq).zzc(zzcv);
            return this;
        }

        public final zza zzb(zzcq zzcq) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzda) this.zzqq).zza(zzcq);
            return this;
        }
    }

    public final boolean zzfd() {
        return (this.zzie & 1) != 0;
    }

    public final zzce zzfe() {
        zzce zzce = this.zzlh;
        return zzce == null ? zzce.zzdn() : zzce;
    }

    /* access modifiers changed from: private */
    public final void zzb(zzce zzce) {
        zzce.getClass();
        this.zzlh = zzce;
        this.zzie |= 1;
    }

    public final boolean zzff() {
        return (this.zzie & 2) != 0;
    }

    public final zzdn zzfg() {
        zzdn zzdn = this.zzli;
        return zzdn == null ? zzdn.zzfy() : zzdn;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdn zzdn) {
        zzdn.getClass();
        this.zzli = zzdn;
        this.zzie |= 2;
    }

    public final boolean zzfh() {
        return (this.zzie & 4) != 0;
    }

    public final zzcv zzfi() {
        zzcv zzcv = this.zzlj;
        return zzcv == null ? zzcv.zzez() : zzcv;
    }

    /* access modifiers changed from: private */
    public final void zzc(zzcv zzcv) {
        zzcv.getClass();
        this.zzlj = zzcv;
        this.zzie |= 4;
    }

    public final boolean zzfj() {
        return (this.zzie & 8) != 0;
    }

    public final zzcq zzfk() {
        zzcq zzcq = this.zzlk;
        return zzcq == null ? zzcq.zzed() : zzcq;
    }

    /* access modifiers changed from: private */
    public final void zza(zzcq zzcq) {
        zzcq.getClass();
        this.zzlk = zzcq;
        this.zzie |= 8;
    }

    public static zza zzfl() {
        return (zza) zzll.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zze) {
            case NEW_MUTABLE_INSTANCE:
                return new zzda();
            case NEW_BUILDER:
                return new zza();
            case BUILD_MESSAGE_INFO:
                return zza(zzll, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0000\u0002\t\u0001\u0003\t\u0002\u0004\t\u0003", new Object[]{"zzie", "zzlh", "zzli", "zzlj", "zzlk"});
            case GET_DEFAULT_INSTANCE:
                return zzll;
            case GET_PARSER:
                zzgv<zzda> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzda.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzll);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case GET_MEMOIZED_IS_INITIALIZED:
                return (byte) 1;
            case SET_MEMOIZED_IS_INITIALIZED:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzda zzda = new zzda();
        zzll = zzda;
        zzfc.zza(zzda.class, zzda);
    }
}
