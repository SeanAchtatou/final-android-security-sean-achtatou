package com.google.android.gms.internal.ads;

import cz.msebera.android.httpclient.message.TokenParser;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzduo {
    static String zzbj(zzdqk zzdqk) {
        zzdun zzdun = new zzdun(zzdqk);
        StringBuilder sb = new StringBuilder(zzdun.size());
        for (int i = 0; i < zzdun.size(); i++) {
            byte zzfe = zzdun.zzfe(i);
            if (zzfe == 34) {
                sb.append("\\\"");
            } else if (zzfe == 39) {
                sb.append("\\'");
            } else if (zzfe != 92) {
                switch (zzfe) {
                    case 7:
                        sb.append("\\a");
                        continue;
                    case 8:
                        sb.append("\\b");
                        continue;
                    case 9:
                        sb.append("\\t");
                        continue;
                    case 10:
                        sb.append("\\n");
                        continue;
                    case 11:
                        sb.append("\\v");
                        continue;
                    case 12:
                        sb.append("\\f");
                        continue;
                    case 13:
                        sb.append("\\r");
                        continue;
                    default:
                        if (zzfe < 32 || zzfe > 126) {
                            sb.append((char) TokenParser.ESCAPE);
                            sb.append((char) (((zzfe >>> 6) & 3) + 48));
                            sb.append((char) (((zzfe >>> 3) & 7) + 48));
                            sb.append((char) ((zzfe & 7) + 48));
                            break;
                        } else {
                            sb.append((char) zzfe);
                            continue;
                        }
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }
}
