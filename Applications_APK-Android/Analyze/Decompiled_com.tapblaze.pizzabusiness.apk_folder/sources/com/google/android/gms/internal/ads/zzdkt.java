package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdkt extends zzdik<zzdio, zzdmv> {
    zzdkt(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        zzdmv zzdmv = (zzdmv) obj;
        zzdmt zzavf = zzdmv.zzauz().zzavf();
        SecretKeySpec secretKeySpec = new SecretKeySpec(zzdmv.zzass().toByteArray(), "HMAC");
        int zzasx = zzdmv.zzauz().zzasx();
        int i = zzdkv.zzgzw[zzavf.ordinal()];
        if (i == 1) {
            return new zzdpl("HMACSHA1", secretKeySpec, zzasx);
        }
        if (i == 2) {
            return new zzdpl("HMACSHA256", secretKeySpec, zzasx);
        }
        if (i == 3) {
            return new zzdpl("HMACSHA512", secretKeySpec, zzasx);
        }
        throw new GeneralSecurityException("unknown hash");
    }
}
