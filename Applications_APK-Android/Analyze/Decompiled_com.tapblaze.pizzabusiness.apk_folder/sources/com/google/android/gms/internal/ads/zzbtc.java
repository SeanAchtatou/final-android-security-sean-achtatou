package com.google.android.gms.internal.ads;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbtc implements zzbsz {
    private final List<String> zzdkm;
    private final zzdda zzfbm;
    private boolean zzfit;

    public zzbtc(zzczl zzczl, zzdda zzdda) {
        this.zzdkm = zzczl.zzdkm;
        this.zzfbm = zzdda;
    }

    public final void zzaia() {
        if (!this.zzfit) {
            this.zzfbm.zzg(this.zzdkm);
            this.zzfit = true;
        }
    }
}
