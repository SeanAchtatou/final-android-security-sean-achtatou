package com.google.android.gms.internal.ads;

import java.util.ListIterator;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzduw implements ListIterator<String> {
    private final /* synthetic */ int zzgvz;
    private final /* synthetic */ zzdut zzhrh;
    private ListIterator<String> zzhri = this.zzhrh.zzhrf.listIterator(this.zzgvz);

    zzduw(zzdut zzdut, int i) {
        this.zzhrh = zzdut;
        this.zzgvz = i;
    }

    public final boolean hasNext() {
        return this.zzhri.hasNext();
    }

    public final boolean hasPrevious() {
        return this.zzhri.hasPrevious();
    }

    public final int nextIndex() {
        return this.zzhri.nextIndex();
    }

    public final int previousIndex() {
        return this.zzhri.previousIndex();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ void add(Object obj) {
        String str = (String) obj;
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ void set(Object obj) {
        String str = (String) obj;
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ Object previous() {
        return this.zzhri.previous();
    }

    public final /* synthetic */ Object next() {
        return this.zzhri.next();
    }
}
