package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbbx implements zzno {
    private final String zzcyr;
    private final zzbbs zzeco;

    zzbbx(zzbbs zzbbs, String str) {
        this.zzeco = zzbbs;
        this.zzcyr = str;
    }

    public final zznl zzih() {
        return this.zzeco.zzfg(this.zzcyr);
    }
}
