package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzer  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzer implements zzin {
    private final zzeo zzna;

    public static zzer zza(zzeo zzeo) {
        if (zzeo.zzng != null) {
            return zzeo.zzng;
        }
        return new zzer(zzeo);
    }

    private zzer(zzeo zzeo) {
        this.zzna = (zzeo) zzfg.checkNotNull(zzeo, "output");
        this.zzna.zzng = this;
    }

    public final int zzgu() {
        return zzgx.zztr;
    }

    public final void zzp(int i, int i2) throws IOException {
        this.zzna.zzi(i, i2);
    }

    public final void zzi(int i, long j) throws IOException {
        this.zzna.zza(i, j);
    }

    public final void zzj(int i, long j) throws IOException {
        this.zzna.zzc(i, j);
    }

    public final void zza(int i, float f) throws IOException {
        this.zzna.zza(i, f);
    }

    public final void zza(int i, double d) throws IOException {
        this.zzna.zza(i, d);
    }

    public final void zzq(int i, int i2) throws IOException {
        this.zzna.zzf(i, i2);
    }

    public final void zza(int i, long j) throws IOException {
        this.zzna.zza(i, j);
    }

    public final void zzf(int i, int i2) throws IOException {
        this.zzna.zzf(i, i2);
    }

    public final void zzc(int i, long j) throws IOException {
        this.zzna.zzc(i, j);
    }

    public final void zzi(int i, int i2) throws IOException {
        this.zzna.zzi(i, i2);
    }

    public final void zza(int i, boolean z) throws IOException {
        this.zzna.zza(i, z);
    }

    public final void zza(int i, String str) throws IOException {
        this.zzna.zza(i, str);
    }

    public final void zza(int i, zzeb zzeb) throws IOException {
        this.zzna.zza(i, zzeb);
    }

    public final void zzg(int i, int i2) throws IOException {
        this.zzna.zzg(i, i2);
    }

    public final void zzh(int i, int i2) throws IOException {
        this.zzna.zzh(i, i2);
    }

    public final void zzb(int i, long j) throws IOException {
        this.zzna.zzb(i, j);
    }

    public final void zza(int i, Object obj, zzhb zzhb) throws IOException {
        this.zzna.zza(i, (zzgl) obj, zzhb);
    }

    public final void zzb(int i, Object obj, zzhb zzhb) throws IOException {
        zzeo zzeo = this.zzna;
        zzeo.zze(i, 3);
        zzhb.zza((zzgl) obj, zzeo.zzng);
        zzeo.zze(i, 4);
    }

    public final void zzai(int i) throws IOException {
        this.zzna.zze(i, 3);
    }

    public final void zzaj(int i) throws IOException {
        this.zzna.zze(i, 4);
    }

    public final void zza(int i, Object obj) throws IOException {
        if (obj instanceof zzeb) {
            this.zzna.zzb(i, (zzeb) obj);
        } else {
            this.zzna.zza(i, (zzgl) obj);
        }
    }

    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzz(list.get(i4).intValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzu(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zzf(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzac(list.get(i4).intValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzx(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zzi(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzat(list.get(i4).longValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzaq(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zza(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzau(list.get(i4).longValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzaq(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zza(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzaw(list.get(i4).longValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzas(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zzc(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzc(list.get(i4).floatValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzb(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zza(i, list.get(i2).floatValue());
            i2++;
        }
    }

    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzb(list.get(i4).doubleValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zza(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zza(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzae(list.get(i4).intValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzu(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zzf(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzg(list.get(i4).booleanValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzf(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zza(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof zzfw) {
            zzfw zzfw = (zzfw) list;
            while (i2 < list.size()) {
                Object raw = zzfw.getRaw(i2);
                if (raw instanceof String) {
                    this.zzna.zza(i, (String) raw);
                } else {
                    this.zzna.zza(i, (zzeb) raw);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zza(i, list.get(i2));
            i2++;
        }
    }

    public final void zzb(int i, List<zzeb> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.zzna.zza(i, list.get(i2));
        }
    }

    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzaa(list.get(i4).intValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzv(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zzg(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzad(list.get(i4).intValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzx(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zzi(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzax(list.get(i4).longValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzas(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zzc(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzab(list.get(i4).intValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzw(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zzh(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.zzna.zze(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zzeo.zzav(list.get(i4).longValue());
            }
            this.zzna.zzv(i3);
            while (i2 < list.size()) {
                this.zzna.zzar(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.zzna.zzb(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void zza(int i, List<?> list, zzhb zzhb) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            zza(i, list.get(i2), zzhb);
        }
    }

    public final void zzb(int i, List<?> list, zzhb zzhb) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            zzb(i, list.get(i2), zzhb);
        }
    }

    public final <K, V> void zza(int i, zzgg<K, V> zzgg, Map<K, V> map) throws IOException {
        for (Map.Entry next : map.entrySet()) {
            this.zzna.zze(i, 2);
            this.zzna.zzv(zzgd.zza(zzgg, next.getKey(), next.getValue()));
            zzeo zzeo = this.zzna;
            Object key = next.getKey();
            Object value = next.getValue();
            zzex.zza(zzeo, zzgg.zzsn, 1, key);
            zzex.zza(zzeo, zzgg.zzsp, 2, value);
        }
    }
}
