package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzafh implements zzafn<zzbdi> {
    zzafh() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzabw zzaal = ((zzbdi) obj).zzaal();
        if (zzaal != null) {
            zzaal.zzrd();
        }
    }
}
