package com.google.android.gms.ads.query;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class AdData {
    private final QueryData zzgrq;
    private final String zzgrr;

    public static String getRequestId(String str) {
        return "";
    }

    public AdData(QueryData queryData, String str) {
        this.zzgrq = queryData;
        this.zzgrr = str;
    }

    public QueryData getQueryData() {
        return this.zzgrq;
    }

    public String getAdString() {
        return this.zzgrr;
    }
}
