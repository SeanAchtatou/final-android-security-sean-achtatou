package com.google.android.gms.internal.ads;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdty implements Iterator<zzdqr> {
    private final ArrayDeque<zzdtt> zzhqj;
    private zzdqr zzhqk;

    private zzdty(zzdqk zzdqk) {
        if (zzdqk instanceof zzdtt) {
            zzdtt zzdtt = (zzdtt) zzdqk;
            this.zzhqj = new ArrayDeque<>(zzdtt.zzaxw());
            this.zzhqj.push(zzdtt);
            this.zzhqk = zzbi(zzdtt.zzhpw);
            return;
        }
        this.zzhqj = null;
        this.zzhqk = (zzdqr) zzdqk;
    }

    private final zzdqr zzbi(zzdqk zzdqk) {
        while (zzdqk instanceof zzdtt) {
            zzdtt zzdtt = (zzdtt) zzdqk;
            this.zzhqj.push(zzdtt);
            zzdqk = zzdtt.zzhpw;
        }
        return (zzdqr) zzdqk;
    }

    public final boolean hasNext() {
        return this.zzhqk != null;
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ Object next() {
        zzdqr zzdqr;
        zzdqr zzdqr2 = this.zzhqk;
        if (zzdqr2 != null) {
            while (true) {
                ArrayDeque<zzdtt> arrayDeque = this.zzhqj;
                if (arrayDeque != null && !arrayDeque.isEmpty()) {
                    zzdqr = zzbi(this.zzhqj.pop().zzhpx);
                    if (!zzdqr.isEmpty()) {
                        break;
                    }
                } else {
                    zzdqr = null;
                }
            }
            zzdqr = null;
            this.zzhqk = zzdqr;
            return zzdqr2;
        }
        throw new NoSuchElementException();
    }

    /* synthetic */ zzdty(zzdqk zzdqk, zzdtw zzdtw) {
        this(zzdqk);
    }
}
