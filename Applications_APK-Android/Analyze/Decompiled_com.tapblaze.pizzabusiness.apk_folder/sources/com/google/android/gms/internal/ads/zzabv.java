package com.google.android.gms.internal.ads;

import android.widget.ImageView;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzabv {
    void setImageScaleType(ImageView.ScaleType scaleType);
}
