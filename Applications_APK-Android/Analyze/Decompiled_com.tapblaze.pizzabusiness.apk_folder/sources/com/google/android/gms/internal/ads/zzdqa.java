package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdpz;
import com.google.android.gms.internal.ads.zzdqa;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzdqa<MessageType extends zzdqa<MessageType, BuilderType>, BuilderType extends zzdpz<MessageType, BuilderType>> implements zzdte {
    protected int zzhhk = 0;

    public final zzdqk zzaxk() {
        try {
            zzdqs zzfg = zzdqk.zzfg(zzazu());
            zzb(zzfg.zzayb());
            return zzfg.zzaya();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    public final byte[] toByteArray() {
        try {
            byte[] bArr = new byte[zzazu()];
            zzdrb zzw = zzdrb.zzw(bArr);
            zzb(zzw);
            zzw.zzazd();
            return bArr;
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "byte array".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("byte array");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    /* access modifiers changed from: package-private */
    public int zzaxl() {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    public void zzfa(int i) {
        throw new UnsupportedOperationException();
    }

    protected static <T> void zza(Iterable iterable, List list) {
        zzdrv.checkNotNull(iterable);
        if (iterable instanceof zzdsl) {
            List<?> zzbav = ((zzdsl) iterable).zzbav();
            zzdsl zzdsl = (zzdsl) list;
            int size = list.size();
            for (Object next : zzbav) {
                if (next == null) {
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(zzdsl.size() - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    for (int size2 = zzdsl.size() - 1; size2 >= size; size2--) {
                        zzdsl.remove(size2);
                    }
                    throw new NullPointerException(sb2);
                } else if (next instanceof zzdqk) {
                    zzdsl.zzbg((zzdqk) next);
                } else {
                    zzdsl.add((String) next);
                }
            }
        } else if (iterable instanceof zzdtq) {
            list.addAll((Collection) iterable);
        } else {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(list.size() + ((Collection) iterable).size());
            }
            int size3 = list.size();
            for (Object next2 : iterable) {
                if (next2 == null) {
                    StringBuilder sb3 = new StringBuilder(37);
                    sb3.append("Element at index ");
                    sb3.append(list.size() - size3);
                    sb3.append(" is null.");
                    String sb4 = sb3.toString();
                    for (int size4 = list.size() - 1; size4 >= size3; size4--) {
                        list.remove(size4);
                    }
                    throw new NullPointerException(sb4);
                }
                list.add(next2);
            }
        }
    }
}
