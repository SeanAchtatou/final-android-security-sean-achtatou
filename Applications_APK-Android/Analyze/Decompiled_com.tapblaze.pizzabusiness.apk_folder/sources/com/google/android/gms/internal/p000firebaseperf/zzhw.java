package com.google.android.gms.internal.p000firebaseperf;

import com.ironsource.mediationsdk.logger.IronSourceError;
import java.io.IOException;
import java.util.Arrays;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhw  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzhw {
    private static final zzhw zzuq = new zzhw(0, new int[0], new Object[0], false);
    private int count;
    private boolean zzmp;
    private int zzqm;
    private Object[] zzsw;
    private int[] zzur;

    public static zzhw zzje() {
        return zzuq;
    }

    static zzhw zza(zzhw zzhw, zzhw zzhw2) {
        int i = zzhw.count + zzhw2.count;
        int[] copyOf = Arrays.copyOf(zzhw.zzur, i);
        System.arraycopy(zzhw2.zzur, 0, copyOf, zzhw.count, zzhw2.count);
        Object[] copyOf2 = Arrays.copyOf(zzhw.zzsw, i);
        System.arraycopy(zzhw2.zzsw, 0, copyOf2, zzhw.count, zzhw2.count);
        return new zzhw(i, copyOf, copyOf2, true);
    }

    private zzhw() {
        this(0, new int[8], new Object[8], true);
    }

    private zzhw(int i, int[] iArr, Object[] objArr, boolean z) {
        this.zzqm = -1;
        this.count = i;
        this.zzur = iArr;
        this.zzsw = objArr;
        this.zzmp = z;
    }

    public final void zzgg() {
        this.zzmp = false;
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzin zzin) throws IOException {
        if (zzin.zzgu() == zzgx.zzts) {
            for (int i = this.count - 1; i >= 0; i--) {
                zzin.zza(this.zzur[i] >>> 3, this.zzsw[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.count; i2++) {
            zzin.zza(this.zzur[i2] >>> 3, this.zzsw[i2]);
        }
    }

    public final void zzb(zzin zzin) throws IOException {
        if (this.count != 0) {
            if (zzin.zzgu() == zzgx.zztr) {
                for (int i = 0; i < this.count; i++) {
                    zzb(this.zzur[i], this.zzsw[i], zzin);
                }
                return;
            }
            for (int i2 = this.count - 1; i2 >= 0; i2--) {
                zzb(this.zzur[i2], this.zzsw[i2], zzin);
            }
        }
    }

    private static void zzb(int i, Object obj, zzin zzin) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            zzin.zzi(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            zzin.zzc(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            zzin.zza(i2, (zzeb) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                zzin.zzi(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(zzfl.zzhr());
        } else if (zzin.zzgu() == zzgx.zztr) {
            zzin.zzai(i2);
            ((zzhw) obj).zzb(zzin);
            zzin.zzaj(i2);
        } else {
            zzin.zzaj(i2);
            ((zzhw) obj).zzb(zzin);
            zzin.zzai(i2);
        }
    }

    public final int zzjf() {
        int i = this.zzqm;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.count; i3++) {
            i2 += zzeo.zzd(this.zzur[i3] >>> 3, (zzeb) this.zzsw[i3]);
        }
        this.zzqm = i2;
        return i2;
    }

    public final int getSerializedSize() {
        int i;
        int i2 = this.zzqm;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.count; i4++) {
            int i5 = this.zzur[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 == 0) {
                i = zzeo.zze(i6, ((Long) this.zzsw[i4]).longValue());
            } else if (i7 == 1) {
                i = zzeo.zzg(i6, ((Long) this.zzsw[i4]).longValue());
            } else if (i7 == 2) {
                i = zzeo.zzc(i6, (zzeb) this.zzsw[i4]);
            } else if (i7 == 3) {
                i = (zzeo.zzy(i6) << 1) + ((zzhw) this.zzsw[i4]).getSerializedSize();
            } else if (i7 == 5) {
                i = zzeo.zzm(i6, ((Integer) this.zzsw[i4]).intValue());
            } else {
                throw new IllegalStateException(zzfl.zzhr());
            }
            i3 += i;
        }
        this.zzqm = i3;
        return i3;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof zzhw)) {
            return false;
        }
        zzhw zzhw = (zzhw) obj;
        int i = this.count;
        if (i == zzhw.count) {
            int[] iArr = this.zzur;
            int[] iArr2 = zzhw.zzur;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.zzsw;
                Object[] objArr2 = zzhw.zzsw;
                int i3 = this.count;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                return z2;
            }
        }
    }

    public final int hashCode() {
        int i = this.count;
        int i2 = (i + IronSourceError.ERROR_NON_EXISTENT_INSTANCE) * 31;
        int[] iArr = this.zzur;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.zzsw;
        int i7 = this.count;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    /* access modifiers changed from: package-private */
    public final void zza(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.count; i2++) {
            zzgq.zza(sb, i, String.valueOf(this.zzur[i2] >>> 3), this.zzsw[i2]);
        }
    }
}
