package com.google.android.gms.internal.p000firebaseperf;

import java.util.Iterator;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhm  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhm implements Iterable<Object> {
    zzhm() {
    }

    public final Iterator<Object> iterator() {
        return zzhk.zzuk;
    }
}
