package com.google.android.gms.internal.ads;

import com.ironsource.sdk.constants.Constants;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
abstract class zzdfz<InputT, OutputT> extends zzdgc<OutputT> {
    private static final Logger logger = Logger.getLogger(zzdfz.class.getName());
    /* access modifiers changed from: private */
    @NullableDecl
    public zzdet<? extends zzdhe<? extends InputT>> zzgwb;
    private final boolean zzgwc;
    private final boolean zzgwd;

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    enum zza {
        OUTPUT_FUTURE_DONE,
        ALL_INPUT_FUTURES_PROCESSED
    }

    zzdfz(zzdet<? extends zzdhe<? extends InputT>> zzdet, boolean z, boolean z2) {
        super(zzdet.size());
        this.zzgwb = (zzdet) zzdei.checkNotNull(zzdet);
        this.zzgwc = z;
        this.zzgwd = z2;
    }

    /* access modifiers changed from: package-private */
    public abstract void zzaro();

    /* access modifiers changed from: package-private */
    public abstract void zzb(int i, @NullableDecl InputT inputt);

    /* access modifiers changed from: protected */
    public final void afterDone() {
        super.afterDone();
        zzdet<? extends zzdhe<? extends InputT>> zzdet = this.zzgwb;
        zza(zza.OUTPUT_FUTURE_DONE);
        if (isCancelled() && (zzdet != null)) {
            boolean wasInterrupted = wasInterrupted();
            zzdfp zzdfp = (zzdfp) zzdet.iterator();
            while (zzdfp.hasNext()) {
                ((Future) zzdfp.next()).cancel(wasInterrupted);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final String pendingToString() {
        zzdet<? extends zzdhe<? extends InputT>> zzdet = this.zzgwb;
        if (zzdet == null) {
            return null;
        }
        String valueOf = String.valueOf(zzdet);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 10);
        sb.append("futures=[");
        sb.append(valueOf);
        sb.append(Constants.RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final void zzarn() {
        if (this.zzgwb.isEmpty()) {
            zzaro();
        } else if (this.zzgwc) {
            int i = 0;
            zzdfp zzdfp = (zzdfp) this.zzgwb.iterator();
            while (zzdfp.hasNext()) {
                zzdhe zzdhe = (zzdhe) zzdfp.next();
                zzdhe.addListener(new zzdfy(this, zzdhe, i), zzdgl.INSTANCE);
                i++;
            }
        } else {
            zzdga zzdga = new zzdga(this, this.zzgwd ? this.zzgwb : null);
            zzdfp zzdfp2 = (zzdfp) this.zzgwb.iterator();
            while (zzdfp2.hasNext()) {
                ((zzdhe) zzdfp2.next()).addListener(zzdga, zzdgl.INSTANCE);
            }
        }
    }

    private final void zzi(Throwable th) {
        zzdei.checkNotNull(th);
        if (this.zzgwc && !setException(th) && zza(zzarp(), th)) {
            zzj(th);
        } else if (th instanceof Error) {
            zzj(th);
        }
    }

    private static void zzj(Throwable th) {
        logger.logp(Level.SEVERE, "com.google.common.util.concurrent.AggregateFuture", "log", th instanceof Error ? "Input Future failed with Error" : "Got more than one input Future failure. Logging failures after the first", th);
    }

    /* access modifiers changed from: package-private */
    public final void zzg(Set<Throwable> set) {
        zzdei.checkNotNull(set);
        if (!isCancelled()) {
            zza(set, zzark());
        }
    }

    /* access modifiers changed from: private */
    public final void zza(int i, Future<? extends InputT> future) {
        try {
            zzb(i, zzdgs.zzb(future));
        } catch (ExecutionException e) {
            zzi(e.getCause());
        } catch (Throwable th) {
            zzi(th);
        }
    }

    /* access modifiers changed from: private */
    public final void zza(@NullableDecl zzdet<? extends Future<? extends InputT>> zzdet) {
        int zzarq = zzarq();
        int i = 0;
        if (!(zzarq >= 0)) {
            throw new IllegalStateException("Less than 0 remaining futures");
        } else if (zzarq == 0) {
            if (zzdet != null) {
                zzdfp zzdfp = (zzdfp) zzdet.iterator();
                while (zzdfp.hasNext()) {
                    Future future = (Future) zzdfp.next();
                    if (!future.isCancelled()) {
                        zza(i, future);
                    }
                    i++;
                }
            }
            zzarr();
            zzaro();
            zza(zza.ALL_INPUT_FUTURES_PROCESSED);
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zza zza2) {
        zzdei.checkNotNull(zza2);
        this.zzgwb = null;
    }

    private static boolean zza(Set<Throwable> set, Throwable th) {
        while (th != null) {
            if (!set.add(th)) {
                return false;
            }
            th = th.getCause();
        }
        return true;
    }
}
