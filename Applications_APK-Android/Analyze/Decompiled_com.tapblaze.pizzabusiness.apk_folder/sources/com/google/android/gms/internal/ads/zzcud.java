package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcud implements Runnable {
    private final zzcub zzgha;
    private final long zzghb;

    zzcud(zzcub zzcub, long j) {
        this.zzgha = zzcub;
        this.zzghb = j;
    }

    public final void run() {
        zzcub zzcub = this.zzgha;
        long j = this.zzghb;
        String canonicalName = zzcub.getClass().getCanonicalName();
        long elapsedRealtime = zzq.zzkx().elapsedRealtime() - j;
        StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 40);
        sb.append("Signal runtime : ");
        sb.append(canonicalName);
        sb.append(" = ");
        sb.append(elapsedRealtime);
        zzavs.zzed(sb.toString());
    }
}
