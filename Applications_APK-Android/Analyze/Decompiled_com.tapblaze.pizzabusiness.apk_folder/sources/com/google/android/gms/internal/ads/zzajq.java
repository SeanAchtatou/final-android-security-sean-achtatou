package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzajq extends zzahs, zzaip {
    void zza(String str, zzafn<? super zzajq> zzafn);

    void zzb(String str, zzafn<? super zzajq> zzafn);
}
