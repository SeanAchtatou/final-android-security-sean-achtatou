package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzddq {
    private static final zzddr zzgtr;
    private static volatile zzddr zzgts;

    public static zzddr zzaqs() {
        return zzgts;
    }

    static {
        zzdds zzdds = new zzdds();
        zzgtr = zzdds;
        zzgts = zzdds;
    }
}
