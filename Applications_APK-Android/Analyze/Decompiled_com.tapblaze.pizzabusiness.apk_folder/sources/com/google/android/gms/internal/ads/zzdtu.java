package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
interface zzdtu {
    int getTag();

    double readDouble() throws IOException;

    float readFloat() throws IOException;

    String readString() throws IOException;

    void readStringList(List<String> list) throws IOException;

    <T> T zza(zzdua zzdua, zzdrg zzdrg) throws IOException;

    <T> void zza(List list, zzdua zzdua, zzdrg zzdrg) throws IOException;

    <K, V> void zza(Map map, zzdsv zzdsv, zzdrg zzdrg) throws IOException;

    long zzayd() throws IOException;

    long zzaye() throws IOException;

    int zzayf() throws IOException;

    long zzayg() throws IOException;

    int zzayh() throws IOException;

    boolean zzayi() throws IOException;

    String zzayj() throws IOException;

    zzdqk zzayk() throws IOException;

    int zzayl() throws IOException;

    int zzaym() throws IOException;

    int zzayn() throws IOException;

    long zzayo() throws IOException;

    int zzayp() throws IOException;

    long zzayq() throws IOException;

    int zzaza() throws IOException;

    boolean zzazb() throws IOException;

    @Deprecated
    <T> T zzb(zzdua<T> zzdua, zzdrg zzdrg) throws IOException;

    @Deprecated
    <T> void zzb(List<T> list, zzdua<T> zzdua, zzdrg zzdrg) throws IOException;

    void zzi(List<Double> list) throws IOException;

    void zzj(List<Float> list) throws IOException;

    void zzk(List<Long> list) throws IOException;

    void zzl(List<Long> list) throws IOException;

    void zzm(List<Integer> list) throws IOException;

    void zzn(List<Long> list) throws IOException;

    void zzo(List<Integer> list) throws IOException;

    void zzp(List<Boolean> list) throws IOException;

    void zzq(List<String> list) throws IOException;

    void zzr(List<zzdqk> list) throws IOException;

    void zzs(List<Integer> list) throws IOException;

    void zzt(List<Integer> list) throws IOException;

    void zzu(List<Integer> list) throws IOException;

    void zzv(List<Long> list) throws IOException;

    void zzw(List<Integer> list) throws IOException;

    void zzx(List<Long> list) throws IOException;
}
