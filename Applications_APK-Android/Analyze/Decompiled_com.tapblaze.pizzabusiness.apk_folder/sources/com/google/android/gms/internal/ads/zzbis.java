package com.google.android.gms.internal.ads;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.CookieManager;
import com.google.android.gms.ads.internal.zzq;
import com.vungle.warren.model.CookieDBAdapter;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbis implements zzbil {
    private final Context zzup;

    public zzbis(Context context) {
        this.zzup = context;
    }

    public final void zzk(Map<String, String> map) {
        CookieManager zzbd;
        String str = map.get(CookieDBAdapter.ookieColumns.TABLE_NAME);
        if (!TextUtils.isEmpty(str) && (zzbd = zzq.zzks().zzbd(this.zzup)) != null) {
            zzbd.setCookie("googleads.g.doubleclick.net", str);
        }
    }
}
