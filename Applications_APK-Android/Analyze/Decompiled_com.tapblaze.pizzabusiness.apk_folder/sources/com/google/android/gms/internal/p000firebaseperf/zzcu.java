package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzcu  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzcu extends zzfc<zzcu, zza> implements zzgn {
    private static volatile zzgv<zzcu> zzij;
    /* access modifiers changed from: private */
    public static final zzcu zzkf;
    private int zzie;
    private long zzik;
    private int zzkd;
    private int zzke;

    private zzcu() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzcu$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzcu, zza> implements zzgn {
        private zza() {
            super(zzcu.zzkf);
        }

        /* synthetic */ zza(zzct zzct) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzct.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzcu();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzkf, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0002\u0000\u0002\u0004\u0001\u0003\u0004\u0002", new Object[]{"zzie", "zzik", "zzkd", "zzke"});
            case 4:
                return zzkf;
            case 5:
                zzgv<zzcu> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzcu.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzkf);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzcu zzcu = new zzcu();
        zzkf = zzcu;
        zzfc.zza(zzcu.class, zzcu);
    }
}
