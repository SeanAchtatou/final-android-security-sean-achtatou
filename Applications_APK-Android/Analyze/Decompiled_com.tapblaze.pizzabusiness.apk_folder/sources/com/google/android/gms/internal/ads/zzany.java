package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.browser.customtabs.CustomTabsIntent;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.common.util.PlatformVersion;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzany implements MediationInterstitialAdapter {
    private Uri uri;
    /* access modifiers changed from: private */
    public Activity zzdex;
    /* access modifiers changed from: private */
    public MediationInterstitialListener zzdey;

    public final void requestInterstitialAd(Context context, MediationInterstitialListener mediationInterstitialListener, Bundle bundle, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.zzdey = mediationInterstitialListener;
        if (this.zzdey == null) {
            zzayu.zzez("Listener not set for mediation. Returning.");
        } else if (!(context instanceof Activity)) {
            zzayu.zzez("AdMobCustomTabs can only work with Activity context. Bailing out.");
            this.zzdey.onAdFailedToLoad(this, 0);
        } else {
            if (!(PlatformVersion.isAtLeastIceCreamSandwichMR1() && zzaao.zzk(context))) {
                zzayu.zzez("Default browser does not support custom tabs. Bailing out.");
                this.zzdey.onAdFailedToLoad(this, 0);
                return;
            }
            String string = bundle.getString("tab_url");
            if (TextUtils.isEmpty(string)) {
                zzayu.zzez("The tab_url retrieved from mediation metadata is empty. Bailing out.");
                this.zzdey.onAdFailedToLoad(this, 0);
                return;
            }
            this.zzdex = (Activity) context;
            this.uri = Uri.parse(string);
            this.zzdey.onAdLoaded(this);
        }
    }

    public final void showInterstitial() {
        CustomTabsIntent build = new CustomTabsIntent.Builder().build();
        build.intent.setData(this.uri);
        zzawb.zzdsr.post(new zzaoa(this, new AdOverlayInfoParcel(new zzd(build.intent), null, new zzanx(this), null, new zzazb(0, 0, false))));
        zzq.zzku().zzvb();
    }

    public final void onDestroy() {
        zzayu.zzea("Destroying AdMobCustomTabsAdapter adapter.");
    }

    public final void onPause() {
        zzayu.zzea("Pausing AdMobCustomTabsAdapter adapter.");
    }

    public final void onResume() {
        zzayu.zzea("Resuming AdMobCustomTabsAdapter adapter.");
    }
}
