package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.drive.query.Filter;

public class FilterHolder extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<FilterHolder> CREATOR = new zzh();
    private final Filter zzbc;
    private final zzb<?> zzmd;
    private final zzd zzme;
    private final zzr zzmf;
    private final zzv zzmg;
    private final zzp<?> zzmh;
    private final zzt zzmi;
    private final zzn zzmj;
    private final zzl zzmk;
    private final zzz zzml;

    FilterHolder(zzb<?> zzb, zzd zzd, zzr zzr, zzv zzv, zzp<?> zzp, zzt zzt, zzn<?> zzn, zzl zzl, zzz zzz) {
        this.zzmd = zzb;
        this.zzme = zzd;
        this.zzmf = zzr;
        this.zzmg = zzv;
        this.zzmh = zzp;
        this.zzmi = zzt;
        this.zzmj = zzn;
        this.zzmk = zzl;
        this.zzml = zzz;
        zzb<?> zzb2 = this.zzmd;
        if (zzb2 != null) {
            this.zzbc = zzb2;
            return;
        }
        zzd zzd2 = this.zzme;
        if (zzd2 != null) {
            this.zzbc = zzd2;
            return;
        }
        zzr zzr2 = this.zzmf;
        if (zzr2 != null) {
            this.zzbc = zzr2;
            return;
        }
        zzv zzv2 = this.zzmg;
        if (zzv2 != null) {
            this.zzbc = zzv2;
            return;
        }
        zzp<?> zzp2 = this.zzmh;
        if (zzp2 != null) {
            this.zzbc = zzp2;
            return;
        }
        zzt zzt2 = this.zzmi;
        if (zzt2 != null) {
            this.zzbc = zzt2;
            return;
        }
        zzn zzn2 = this.zzmj;
        if (zzn2 != null) {
            this.zzbc = zzn2;
            return;
        }
        zzl zzl2 = this.zzmk;
        if (zzl2 != null) {
            this.zzbc = zzl2;
            return;
        }
        zzz zzz2 = this.zzml;
        if (zzz2 != null) {
            this.zzbc = zzz2;
            return;
        }
        throw new IllegalArgumentException("At least one filter must be set.");
    }

    public FilterHolder(Filter filter) {
        Preconditions.checkNotNull(filter, "Null filter.");
        zzz zzz = null;
        this.zzmd = filter instanceof zzb ? (zzb) filter : null;
        this.zzme = filter instanceof zzd ? (zzd) filter : null;
        this.zzmf = filter instanceof zzr ? (zzr) filter : null;
        this.zzmg = filter instanceof zzv ? (zzv) filter : null;
        this.zzmh = filter instanceof zzp ? (zzp) filter : null;
        this.zzmi = filter instanceof zzt ? (zzt) filter : null;
        this.zzmj = filter instanceof zzn ? (zzn) filter : null;
        this.zzmk = filter instanceof zzl ? (zzl) filter : null;
        this.zzml = filter instanceof zzz ? (zzz) filter : zzz;
        if (this.zzmd == null && this.zzme == null && this.zzmf == null && this.zzmg == null && this.zzmh == null && this.zzmi == null && this.zzmj == null && this.zzmk == null && this.zzml == null) {
            throw new IllegalArgumentException("Invalid filter type.");
        }
        this.zzbc = filter;
    }

    public final Filter getFilter() {
        return this.zzbc;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, this.zzmd, i, false);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzme, i, false);
        SafeParcelWriter.writeParcelable(parcel, 3, this.zzmf, i, false);
        SafeParcelWriter.writeParcelable(parcel, 4, this.zzmg, i, false);
        SafeParcelWriter.writeParcelable(parcel, 5, this.zzmh, i, false);
        SafeParcelWriter.writeParcelable(parcel, 6, this.zzmi, i, false);
        SafeParcelWriter.writeParcelable(parcel, 7, this.zzmj, i, false);
        SafeParcelWriter.writeParcelable(parcel, 8, this.zzmk, i, false);
        SafeParcelWriter.writeParcelable(parcel, 9, this.zzml, i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
