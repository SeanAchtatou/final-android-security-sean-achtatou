package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzdh  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public enum zzdh implements zzff {
    SERVICE_WORKER_STATUS_UNKNOWN(0),
    UNSUPPORTED(1),
    CONTROLLED(2),
    UNCONTROLLED(3);
    
    private static final zzfi<zzdh> zzja = new zzdg();
    private final int value;

    public final int getNumber() {
        return this.value;
    }

    public static zzfh zzdp() {
        return zzdj.zzjf;
    }

    public final String toString() {
        return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + getNumber() + " name=" + name() + '>';
    }

    private zzdh(int i) {
        this.value = i;
    }
}
