package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.ironsource.sdk.constants.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdbk implements zzdbl {
    private final Object[] zzgpf;

    public zzdbk(zzug zzug, String str, int i, String str2, zzuo zzuo) {
        HashSet hashSet = new HashSet(Arrays.asList(str2.split(",")));
        ArrayList arrayList = new ArrayList();
        arrayList.add(str2);
        arrayList.add(str);
        if (hashSet.contains("networkType")) {
            arrayList.add(Integer.valueOf(i));
        }
        if (hashSet.contains("birthday")) {
            arrayList.add(Long.valueOf(zzug.zzcby));
        }
        if (hashSet.contains("extras")) {
            arrayList.add(zzp(zzug.extras));
        } else if (hashSet.contains("npa")) {
            arrayList.add(zzug.extras.getString("npa"));
        }
        if (hashSet.contains("gender")) {
            arrayList.add(Integer.valueOf(zzug.zzcbz));
        }
        if (hashSet.contains("keywords")) {
            if (zzug.zzcca != null) {
                arrayList.add(zzug.zzcca.toString());
            } else {
                arrayList.add(null);
            }
        }
        if (hashSet.contains("isTestDevice")) {
            arrayList.add(Boolean.valueOf(zzug.zzccb));
        }
        if (hashSet.contains("tagForChildDirectedTreatment")) {
            arrayList.add(Integer.valueOf(zzug.zzabo));
        }
        if (hashSet.contains("manualImpressionsEnabled")) {
            arrayList.add(Boolean.valueOf(zzug.zzbkh));
        }
        if (hashSet.contains("publisherProvidedId")) {
            arrayList.add(zzug.zzccc);
        }
        if (hashSet.contains("location")) {
            if (zzug.zzmi != null) {
                arrayList.add(zzug.zzmi.toString());
            } else {
                arrayList.add(null);
            }
        }
        if (hashSet.contains("contentUrl")) {
            arrayList.add(zzug.zzcce);
        }
        if (hashSet.contains("networkExtras")) {
            arrayList.add(zzp(zzug.zzccf));
        }
        if (hashSet.contains("customTargeting")) {
            arrayList.add(zzp(zzug.zzccg));
        }
        if (hashSet.contains("categoryExclusions")) {
            if (zzug.zzcch != null) {
                arrayList.add(zzug.zzcch.toString());
            } else {
                arrayList.add(null);
            }
        }
        if (hashSet.contains("requestAgent")) {
            arrayList.add(zzug.zzcci);
        }
        if (hashSet.contains("requestPackage")) {
            arrayList.add(zzug.zzccj);
        }
        if (hashSet.contains("isDesignedForFamilies")) {
            arrayList.add(Boolean.valueOf(zzug.zzcck));
        }
        if (hashSet.contains("tagForUnderAgeOfConsent")) {
            arrayList.add(Integer.valueOf(zzug.zzabp));
        }
        if (hashSet.contains("maxAdContentRating")) {
            arrayList.add(zzug.zzabq);
        }
        if (hashSet.contains("orientation")) {
            if (zzuo != null) {
                arrayList.add(Integer.valueOf(zzuo.orientation));
            } else {
                arrayList.add(null);
            }
        }
        this.zzgpf = arrayList.toArray();
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzdbk)) {
            return false;
        }
        return Arrays.equals(this.zzgpf, ((zzdbk) obj).zzgpf);
    }

    public final int hashCode() {
        return Arrays.hashCode(this.zzgpf);
    }

    public final String toString() {
        int hashCode = hashCode();
        String arrays = Arrays.toString(this.zzgpf);
        StringBuilder sb = new StringBuilder(String.valueOf(arrays).length() + 22);
        sb.append("[PoolKey#");
        sb.append(hashCode);
        sb.append(" ");
        sb.append(arrays);
        sb.append(Constants.RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    private static String zzp(Bundle bundle) {
        String str;
        if (bundle == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = new TreeSet(bundle.keySet()).iterator();
        while (it.hasNext()) {
            Object obj = bundle.get((String) it.next());
            if (obj == null) {
                str = "null";
            } else if (obj instanceof Bundle) {
                str = zzp((Bundle) obj);
            } else {
                str = obj.toString();
            }
            sb.append(str);
        }
        return sb.toString();
    }
}
