package com.google.android.gms.internal.ads;

import android.util.JsonWriter;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
interface zzayv {
    void zzb(JsonWriter jsonWriter) throws IOException;
}
