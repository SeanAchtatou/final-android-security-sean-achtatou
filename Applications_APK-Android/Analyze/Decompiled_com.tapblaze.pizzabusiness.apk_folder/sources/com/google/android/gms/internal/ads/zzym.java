package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzym implements Runnable {
    private final zzaso zzcfp;

    zzym(zzaso zzaso) {
        this.zzcfp = zzaso;
    }

    public final void run() {
        zzaso zzaso = this.zzcfp;
        if (zzaso != null) {
            try {
                zzaso.onRewardedAdFailedToLoad(1);
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
    }
}
