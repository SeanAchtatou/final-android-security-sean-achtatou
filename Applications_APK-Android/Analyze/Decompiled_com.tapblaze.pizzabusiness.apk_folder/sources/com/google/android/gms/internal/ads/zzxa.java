package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzxa extends IInterface {
    String getMediationAdapterClassName() throws RemoteException;

    String zzpj() throws RemoteException;
}
