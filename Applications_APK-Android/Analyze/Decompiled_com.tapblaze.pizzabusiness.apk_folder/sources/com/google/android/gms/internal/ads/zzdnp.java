package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdnp extends zzdrt<zzdnp, zza> implements zzdtg {
    private static volatile zzdtn<zzdnp> zzdz;
    /* access modifiers changed from: private */
    public static final zzdnp zzhed;
    private String zzhec = "";

    private zzdnp() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdnp, zza> implements zzdtg {
        private zza() {
            super(zzdnp.zzhed);
        }

        /* synthetic */ zza(zzdnq zzdnq) {
            this();
        }
    }

    public final String zzawk() {
        return this.zzhec;
    }

    public static zzdnp zzay(zzdqk zzdqk) throws zzdse {
        return (zzdnp) zzdrt.zza(zzhed, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnq.zzdk[i - 1]) {
            case 1:
                return new zzdnp();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhed, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001Ȉ", new Object[]{"zzhec"});
            case 4:
                return zzhed;
            case 5:
                zzdtn<zzdnp> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdnp.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhed);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdnp zzawl() {
        return zzhed;
    }

    static {
        zzdnp zzdnp = new zzdnp();
        zzhed = zzdnp;
        zzdrt.zza(zzdnp.class, zzdnp);
    }
}
