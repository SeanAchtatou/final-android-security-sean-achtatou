package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbjo implements zzdxg<Set<zzbsu<zzps>>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbjd> zzfdd;
    private final zzdxp<JSONObject> zzfde;

    private zzbjo(zzdxp<zzbjd> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<JSONObject> zzdxp3) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzfde = zzdxp3;
    }

    public static zzbjo zze(zzdxp<zzbjd> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<JSONObject> zzdxp3) {
        return new zzbjo(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        Set set;
        zzbjd zzbjd = this.zzfdd.get();
        Executor executor = this.zzfcv.get();
        if (this.zzfde.get() == null) {
            set = Collections.emptySet();
        } else {
            set = Collections.singleton(new zzbsu(zzbjd, executor));
        }
        return (Set) zzdxm.zza(set, "Cannot return null from a non-@Nullable @Provides method");
    }
}
