package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class zzdse extends IOException {
    private zzdte zzhnl = null;

    public zzdse(String str) {
        super(str);
    }

    public final zzdse zzl(zzdte zzdte) {
        this.zzhnl = zzdte;
        return this;
    }

    static zzdse zzbaj() {
        return new zzdse("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    static zzdse zzbak() {
        return new zzdse("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    static zzdse zzbal() {
        return new zzdse("CodedInputStream encountered a malformed varint.");
    }

    static zzdse zzbam() {
        return new zzdse("Protocol message contained an invalid tag (zero).");
    }

    static zzdse zzban() {
        return new zzdse("Protocol message end-group tag did not match expected tag.");
    }

    static zzdsd zzbao() {
        return new zzdsd("Protocol message tag had invalid wire type.");
    }

    static zzdse zzbap() {
        return new zzdse("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
    }

    static zzdse zzbaq() {
        return new zzdse("Failed to parse the message.");
    }

    static zzdse zzbar() {
        return new zzdse("Protocol message had invalid UTF-8.");
    }
}
