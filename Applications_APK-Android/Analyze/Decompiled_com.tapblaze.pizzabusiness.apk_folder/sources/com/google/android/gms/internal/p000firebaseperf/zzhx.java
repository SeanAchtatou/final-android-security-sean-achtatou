package com.google.android.gms.internal.p000firebaseperf;

import java.util.ListIterator;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhx  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhx implements ListIterator<String> {
    private ListIterator<String> zzus = this.zzuu.zzuv.listIterator(this.zzut);
    private final /* synthetic */ int zzut;
    private final /* synthetic */ zzhy zzuu;

    zzhx(zzhy zzhy, int i) {
        this.zzuu = zzhy;
        this.zzut = i;
    }

    public final boolean hasNext() {
        return this.zzus.hasNext();
    }

    public final boolean hasPrevious() {
        return this.zzus.hasPrevious();
    }

    public final int nextIndex() {
        return this.zzus.nextIndex();
    }

    public final int previousIndex() {
        return this.zzus.previousIndex();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ void add(Object obj) {
        String str = (String) obj;
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ void set(Object obj) {
        String str = (String) obj;
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ Object previous() {
        return this.zzus.previous();
    }

    public final /* synthetic */ Object next() {
        return this.zzus.next();
    }
}
