package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzani extends IInterface {
    zzxb getVideoController() throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, String str, Bundle bundle, Bundle bundle2, zzuj zzuj, zzanj zzanj) throws RemoteException;

    void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzamw zzamw, zzali zzali, zzuj zzuj) throws RemoteException;

    void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzamx zzamx, zzali zzali) throws RemoteException;

    void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzanc zzanc, zzali zzali) throws RemoteException;

    void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzand zzand, zzali zzali) throws RemoteException;

    void zza(String[] strArr, Bundle[] bundleArr) throws RemoteException;

    boolean zzaa(IObjectWrapper iObjectWrapper) throws RemoteException;

    void zzdm(String str) throws RemoteException;

    zzanw zztc() throws RemoteException;

    zzanw zztd() throws RemoteException;

    void zzy(IObjectWrapper iObjectWrapper) throws RemoteException;

    boolean zzz(IObjectWrapper iObjectWrapper) throws RemoteException;
}
