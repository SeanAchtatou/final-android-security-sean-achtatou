package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzak  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzak extends zzaz<Float> {
    private static zzak zzao;

    private zzak() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.NetworkRequestSamplingRate";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_vc_network_request_sampling_rate";
    }

    protected static synchronized zzak zzam() {
        zzak zzak;
        synchronized (zzak.class) {
            if (zzao == null) {
                zzao = new zzak();
            }
            zzak = zzao;
        }
        return zzak;
    }
}
