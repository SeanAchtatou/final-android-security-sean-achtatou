package com.google.android.gms.internal.ads;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzdup extends RuntimeException {
    private final List<String> zzhrc = null;

    public zzdup(zzdte zzdte) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
