package com.google.android.gms.internal.ads;

import android.content.Context;
import java.io.InputStream;
import java.util.concurrent.Future;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzsn {
    public static Future<InputStream> zza(Context context, zzry zzry) {
        return new zzse(context).zzb(zzry);
    }
}
