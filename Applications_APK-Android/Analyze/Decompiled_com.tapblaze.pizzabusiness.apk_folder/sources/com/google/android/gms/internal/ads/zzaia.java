package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzaia extends zzahs, zzaip {
    void zzb(String str, JSONObject jSONObject);

    void zzcy(String str);

    void zzj(String str, String str2);
}
