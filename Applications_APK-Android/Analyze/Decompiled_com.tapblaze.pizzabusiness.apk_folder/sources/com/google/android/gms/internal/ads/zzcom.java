package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcom extends zzvq {
    private zzvh zzblq;
    private final zzbfx zzfzz;
    private final Context zzgcr;
    private final zzczw zzgcs = new zzczw();
    private final zzbxb zzgct = new zzbxb();

    public zzcom(zzbfx zzbfx, Context context, String str) {
        this.zzfzz = zzbfx;
        this.zzgcs.zzgk(str);
        this.zzgcr = context;
    }

    public final zzvm zzpd() {
        zzbwz zzajw = this.zzgct.zzajw();
        this.zzgcs.zzb(zzajw.zzaju());
        this.zzgcs.zzc(zzajw.zzajv());
        zzczw zzczw = this.zzgcs;
        if (zzczw.zzjz() == null) {
            zzczw.zzd(zzuj.zzg(this.zzgcr));
        }
        return new zzcol(this.zzgcr, this.zzfzz, this.zzgcs, zzajw, this.zzblq);
    }

    public final void zzb(zzvh zzvh) {
        this.zzblq = zzvh;
    }

    public final void zza(zzadi zzadi) {
        this.zzgct.zzb(zzadi);
    }

    public final void zza(zzadv zzadv) {
        this.zzgct.zzb(zzadv);
    }

    public final void zza(zzadj zzadj) {
        this.zzgct.zzb(zzadj);
    }

    public final void zza(String str, zzadp zzadp, zzado zzado) {
        this.zzgct.zzb(str, zzadp, zzado);
    }

    public final void zza(zzaby zzaby) {
        this.zzgcs.zzb(zzaby);
    }

    public final void zza(zzagz zzagz) {
        this.zzgcs.zzb(zzagz);
    }

    public final void zza(zzahh zzahh) {
        this.zzgct.zzb(zzahh);
    }

    public final void zzb(zzwi zzwi) {
        this.zzgcs.zzc(zzwi);
    }

    public final void zza(zzadu zzadu, zzuj zzuj) {
        this.zzgct.zza(zzadu);
        this.zzgcs.zzd(zzuj);
    }

    public final void zza(PublisherAdViewOptions publisherAdViewOptions) {
        this.zzgcs.zzb(publisherAdViewOptions);
    }
}
