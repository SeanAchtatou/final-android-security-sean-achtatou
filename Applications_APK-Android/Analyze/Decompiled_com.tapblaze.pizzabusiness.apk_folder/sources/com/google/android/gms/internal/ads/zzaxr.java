package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaxr<T> extends zzazl<T> implements zzab<T> {
    private zzaxr() {
    }

    public final void zzb(T t) {
        super.set(t);
    }

    /* synthetic */ zzaxr(zzaxn zzaxn) {
        this();
    }
}
