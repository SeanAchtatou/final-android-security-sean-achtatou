package com.google.android.gms.internal.ads;

import android.media.AudioTimestamp;
import android.media.AudioTrack;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzib extends zzhy {
    private final AudioTimestamp zzakb = new AudioTimestamp();
    private long zzakc;
    private long zzakd;
    private long zzake;

    public zzib() {
        super(null);
    }

    public final void zza(AudioTrack audioTrack, boolean z) {
        super.zza(audioTrack, z);
        this.zzakc = 0;
        this.zzakd = 0;
        this.zzake = 0;
    }

    public final boolean zzfq() {
        boolean timestamp = this.zzaia.getTimestamp(this.zzakb);
        if (timestamp) {
            long j = this.zzakb.framePosition;
            if (this.zzakd > j) {
                this.zzakc++;
            }
            this.zzakd = j;
            this.zzake = j + (this.zzakc << 32);
        }
        return timestamp;
    }

    public final long zzfr() {
        return this.zzakb.nanoTime;
    }

    public final long zzfs() {
        return this.zzake;
    }
}
