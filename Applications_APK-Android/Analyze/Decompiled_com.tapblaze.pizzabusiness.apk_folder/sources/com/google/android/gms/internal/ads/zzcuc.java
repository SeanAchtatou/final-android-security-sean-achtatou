package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcuc implements Callable {
    private final Object zzdbh;
    private final List zzggn;

    zzcuc(List list, Object obj) {
        this.zzggn = list;
        this.zzdbh = obj;
    }

    public final Object call() {
        List<zzdhe> list = this.zzggn;
        Object obj = this.zzdbh;
        for (zzdhe zzdhe : list) {
            zzcty zzcty = (zzcty) zzdhe.get();
            if (zzcty != null) {
                zzcty.zzr(obj);
            }
        }
        return obj;
    }
}
