package com.google.android.gms.ads;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzty;
import com.google.android.gms.internal.ads.zzxl;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
class BaseAdView extends ViewGroup {
    protected final zzxl zzabk;

    public BaseAdView(Context context, int i) {
        super(context);
        this.zzabk = new zzxl(this, i);
    }

    public BaseAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        this.zzabk = new zzxl(this, attributeSet, false, i);
    }

    public BaseAdView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i);
        this.zzabk = new zzxl(this, attributeSet, false, i2);
    }

    public void destroy() {
        this.zzabk.destroy();
    }

    public AdListener getAdListener() {
        return this.zzabk.getAdListener();
    }

    public AdSize getAdSize() {
        return this.zzabk.getAdSize();
    }

    public String getAdUnitId() {
        return this.zzabk.getAdUnitId();
    }

    public void loadAd(AdRequest adRequest) {
        this.zzabk.zza(adRequest.zzdg());
    }

    public void pause() {
        this.zzabk.pause();
    }

    public void resume() {
        this.zzabk.resume();
    }

    public boolean isLoading() {
        return this.zzabk.isLoading();
    }

    public void setAdListener(AdListener adListener) {
        this.zzabk.setAdListener(adListener);
        if (adListener == null) {
            this.zzabk.zza((zzty) null);
            this.zzabk.setAppEventListener(null);
            return;
        }
        if (adListener instanceof zzty) {
            this.zzabk.zza((zzty) adListener);
        }
        if (adListener instanceof AppEventListener) {
            this.zzabk.setAppEventListener((AppEventListener) adListener);
        }
    }

    public void setAdSize(AdSize adSize) {
        this.zzabk.setAdSizes(adSize);
    }

    public void setAdUnitId(String str) {
        this.zzabk.setAdUnitId(str);
    }

    public String getMediationAdapterClassName() {
        return this.zzabk.getMediationAdapterClassName();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = ((i3 - i) - measuredWidth) / 2;
            int i6 = ((i4 - i2) - measuredHeight) / 2;
            childAt.layout(i5, i6, measuredWidth + i5, measuredHeight + i6);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4 = 0;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e) {
                zzayu.zzc("Unable to retrieve ad size.", e);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i3 = adSize.getHeightInPixels(context);
                i4 = widthInPixels;
            } else {
                i3 = 0;
            }
        } else {
            measureChild(childAt, i, i2);
            i4 = childAt.getMeasuredWidth();
            i3 = childAt.getMeasuredHeight();
        }
        setMeasuredDimension(View.resolveSize(Math.max(i4, getSuggestedMinimumWidth()), i), View.resolveSize(Math.max(i3, getSuggestedMinimumHeight()), i2));
    }
}
