package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzfe;

/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final /* synthetic */ class zzbj {
    static final /* synthetic */ int[] zza = new int[zzfe.zzf.zza().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0031 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0039 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0019 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0021 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0029 */
    static {
        /*
            int[] r0 = com.google.android.gms.internal.measurement.zzfe.zzf.zza()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.android.gms.internal.measurement.zzbj.zza = r0
            r0 = 1
            int[] r1 = com.google.android.gms.internal.measurement.zzbj.zza     // Catch:{ NoSuchFieldError -> 0x0011 }
            int r2 = com.google.android.gms.internal.measurement.zzfe.zzf.zzd     // Catch:{ NoSuchFieldError -> 0x0011 }
            int r2 = r2 - r0
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0011 }
        L_0x0011:
            int[] r1 = com.google.android.gms.internal.measurement.zzbj.zza     // Catch:{ NoSuchFieldError -> 0x0019 }
            int r2 = com.google.android.gms.internal.measurement.zzfe.zzf.zze     // Catch:{ NoSuchFieldError -> 0x0019 }
            int r2 = r2 - r0
            r3 = 2
            r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x0019 }
        L_0x0019:
            int[] r1 = com.google.android.gms.internal.measurement.zzbj.zza     // Catch:{ NoSuchFieldError -> 0x0021 }
            int r2 = com.google.android.gms.internal.measurement.zzfe.zzf.zzc     // Catch:{ NoSuchFieldError -> 0x0021 }
            int r2 = r2 - r0
            r3 = 3
            r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x0021 }
        L_0x0021:
            int[] r1 = com.google.android.gms.internal.measurement.zzbj.zza     // Catch:{ NoSuchFieldError -> 0x0029 }
            int r2 = com.google.android.gms.internal.measurement.zzfe.zzf.zzf     // Catch:{ NoSuchFieldError -> 0x0029 }
            int r2 = r2 - r0
            r3 = 4
            r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x0029 }
        L_0x0029:
            int[] r1 = com.google.android.gms.internal.measurement.zzbj.zza     // Catch:{ NoSuchFieldError -> 0x0031 }
            int r2 = com.google.android.gms.internal.measurement.zzfe.zzf.zzg     // Catch:{ NoSuchFieldError -> 0x0031 }
            int r2 = r2 - r0
            r3 = 5
            r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x0031 }
        L_0x0031:
            int[] r1 = com.google.android.gms.internal.measurement.zzbj.zza     // Catch:{ NoSuchFieldError -> 0x0039 }
            int r2 = com.google.android.gms.internal.measurement.zzfe.zzf.zza     // Catch:{ NoSuchFieldError -> 0x0039 }
            int r2 = r2 - r0
            r3 = 6
            r1[r2] = r3     // Catch:{ NoSuchFieldError -> 0x0039 }
        L_0x0039:
            int[] r1 = com.google.android.gms.internal.measurement.zzbj.zza     // Catch:{ NoSuchFieldError -> 0x0041 }
            int r2 = com.google.android.gms.internal.measurement.zzfe.zzf.zzb     // Catch:{ NoSuchFieldError -> 0x0041 }
            int r2 = r2 - r0
            r0 = 7
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0041 }
        L_0x0041:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzbj.<clinit>():void");
    }
}
