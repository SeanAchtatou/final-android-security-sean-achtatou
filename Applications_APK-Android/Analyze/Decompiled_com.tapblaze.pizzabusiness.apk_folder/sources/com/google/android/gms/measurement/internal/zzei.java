package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzkq;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class zzei implements zzes {
    static final zzes zza = new zzei();

    private zzei() {
    }

    public final Object zza() {
        return Boolean.valueOf(zzkq.zzc());
    }
}
