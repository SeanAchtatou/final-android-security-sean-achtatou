package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzdte extends zzdtg {
    byte[] toByteArray();

    zzdqk zzaxk();

    int zzazu();

    zzdtd zzazx();

    zzdtd zzazy();

    void zzb(zzdrb zzdrb) throws IOException;
}
