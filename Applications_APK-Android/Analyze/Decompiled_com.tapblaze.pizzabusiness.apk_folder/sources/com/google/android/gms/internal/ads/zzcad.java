package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import androidx.collection.SimpleArrayMap;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.Arrays;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcad extends zzadd {
    private final zzbws zzfkc;
    /* access modifiers changed from: private */
    public final zzbwk zzfnf;
    private final zzbxj zzfqd;
    private final Context zzyv;

    public zzcad(Context context, zzbws zzbws, zzbxj zzbxj, zzbwk zzbwk) {
        this.zzyv = context;
        this.zzfkc = zzbws;
        this.zzfqd = zzbxj;
        this.zzfnf = zzbwk;
    }

    public final IObjectWrapper zzrf() {
        return null;
    }

    public final String zzct(String str) {
        return this.zzfkc.zzajk().get(str);
    }

    public final zzaci zzcu(String str) {
        return this.zzfkc.zzaji().get(str);
    }

    public final List<String> getAvailableAssetNames() {
        SimpleArrayMap<String, zzabu> zzaji = this.zzfkc.zzaji();
        SimpleArrayMap<String, String> zzajk = this.zzfkc.zzajk();
        String[] strArr = new String[(zzaji.size() + zzajk.size())];
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i2 < zzaji.size()) {
            strArr[i3] = zzaji.keyAt(i2);
            i2++;
            i3++;
        }
        while (i < zzajk.size()) {
            strArr[i3] = zzajk.keyAt(i);
            i++;
            i3++;
        }
        return Arrays.asList(strArr);
    }

    public final String getCustomTemplateId() {
        return this.zzfkc.getCustomTemplateId();
    }

    public final void performClick(String str) {
        this.zzfnf.zzfu(str);
    }

    public final void recordImpression() {
        this.zzfnf.zzaio();
    }

    public final zzxb getVideoController() {
        return this.zzfkc.getVideoController();
    }

    public final void destroy() {
        this.zzfnf.destroy();
    }

    public final IObjectWrapper zzrk() {
        return ObjectWrapper.wrap(this.zzyv);
    }

    public final boolean zzp(IObjectWrapper iObjectWrapper) {
        Object unwrap = ObjectWrapper.unwrap(iObjectWrapper);
        if (!(unwrap instanceof ViewGroup) || !this.zzfqd.zza((ViewGroup) unwrap)) {
            return false;
        }
        this.zzfkc.zzajf().zza(new zzcag(this));
        return true;
    }

    public final boolean zzrl() {
        if (this.zzfnf.zzaiw() && this.zzfkc.zzajg() != null && this.zzfkc.zzajf() == null) {
            return true;
        }
        return false;
    }

    public final boolean zzrm() {
        IObjectWrapper zzajh = this.zzfkc.zzajh();
        if (zzajh != null) {
            zzq.zzlf().zzab(zzajh);
            return true;
        }
        zzavs.zzez("Trying to start OMID session before creation.");
        return false;
    }

    public final void zzq(IObjectWrapper iObjectWrapper) {
        Object unwrap = ObjectWrapper.unwrap(iObjectWrapper);
        if ((unwrap instanceof View) && this.zzfkc.zzajh() != null) {
            this.zzfnf.zzz((View) unwrap);
        }
    }

    public final void zzrn() {
        String zzajj = this.zzfkc.zzajj();
        if ("Google".equals(zzajj)) {
            zzavs.zzez("Illegal argument specified for omid partner name.");
        } else {
            this.zzfnf.zzg(zzajj, false);
        }
    }
}
