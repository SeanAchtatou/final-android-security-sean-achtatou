package com.google.android.gms.internal.p000firebaseperf;

import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfr  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzfr<K> implements Iterator<Map.Entry<K, Object>> {
    private Iterator<Map.Entry<K, Object>> zzru;

    public zzfr(Iterator<Map.Entry<K, Object>> it) {
        this.zzru = it;
    }

    public final boolean hasNext() {
        return this.zzru.hasNext();
    }

    public final void remove() {
        this.zzru.remove();
    }

    public final /* synthetic */ Object next() {
        Map.Entry next = this.zzru.next();
        return next.getValue() instanceof zzfq ? new zzfs(next) : next;
    }
}
