package com.google.android.gms.internal.p000firebaseperf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzft  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzft extends zzdw<String> implements zzfw, RandomAccess {
    private static final zzft zzrw;
    private static final zzfw zzrx = zzrw;
    private final List<Object> zzry;

    public zzft() {
        this(10);
    }

    public zzft(int i) {
        this(new ArrayList(i));
    }

    private zzft(ArrayList<Object> arrayList) {
        this.zzry = arrayList;
    }

    public final int size() {
        return this.zzry.size();
    }

    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    public final boolean addAll(int i, Collection<? extends String> collection) {
        zzgh();
        if (collection instanceof zzfw) {
            collection = ((zzfw) collection).zzhv();
        }
        boolean addAll = this.zzry.addAll(i, collection);
        this.modCount++;
        return addAll;
    }

    public final void clear() {
        zzgh();
        this.zzry.clear();
        this.modCount++;
    }

    public final void zzc(zzeb zzeb) {
        zzgh();
        this.zzry.add(zzeb);
        this.modCount++;
    }

    public final Object getRaw(int i) {
        return this.zzry.get(i);
    }

    private static String zzh(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzeb) {
            return ((zzeb) obj).zzgk();
        }
        return zzfg.zzd((byte[]) obj);
    }

    public final List<?> zzhv() {
        return Collections.unmodifiableList(this.zzry);
    }

    public final zzfw zzhw() {
        return zzgf() ? new zzhy(this) : this;
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        zzgh();
        return zzh(this.zzry.set(i, (String) obj));
    }

    public final /* bridge */ /* synthetic */ boolean retainAll(Collection collection) {
        return super.retainAll(collection);
    }

    public final /* bridge */ /* synthetic */ boolean removeAll(Collection collection) {
        return super.removeAll(collection);
    }

    public final /* bridge */ /* synthetic */ boolean remove(Object obj) {
        return super.remove(obj);
    }

    public final /* synthetic */ Object remove(int i) {
        zzgh();
        Object remove = this.zzry.remove(i);
        this.modCount++;
        return zzh(remove);
    }

    public final /* bridge */ /* synthetic */ boolean zzgf() {
        return super.zzgf();
    }

    public final /* synthetic */ void add(int i, Object obj) {
        zzgh();
        this.zzry.add(i, (String) obj);
        this.modCount++;
    }

    public final /* bridge */ /* synthetic */ boolean add(Object obj) {
        return super.add(obj);
    }

    public final /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    public final /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    public final /* synthetic */ zzfm zzao(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.zzry);
            return new zzft(arrayList);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ Object get(int i) {
        Object obj = this.zzry.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzeb) {
            zzeb zzeb = (zzeb) obj;
            String zzgk = zzeb.zzgk();
            if (zzeb.zzgl()) {
                this.zzry.set(i, zzgk);
            }
            return zzgk;
        }
        byte[] bArr = (byte[]) obj;
        String zzd = zzfg.zzd(bArr);
        if (zzfg.zzc(bArr)) {
            this.zzry.set(i, zzd);
        }
        return zzd;
    }

    static {
        zzft zzft = new zzft();
        zzrw = zzft;
        zzft.zzgg();
    }
}
