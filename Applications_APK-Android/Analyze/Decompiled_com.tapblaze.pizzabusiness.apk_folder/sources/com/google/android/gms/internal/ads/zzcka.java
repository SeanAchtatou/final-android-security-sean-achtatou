package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcka {
    private final AtomicReference<zzalc> zzfzi = new AtomicReference<>();

    zzcka() {
    }

    public final void zzb(zzalc zzalc) {
        this.zzfzi.compareAndSet(null, zzalc);
    }

    public final zzdac zze(String str, JSONObject jSONObject) throws zzdab {
        zzald zzald;
        try {
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(str)) {
                zzald = new zzaly(new AdMobAdapter());
            } else if ("com.google.ads.mediation.AdUrlAdapter".equals(str)) {
                zzald = new zzaly(new AdUrlAdapter());
            } else if ("com.google.ads.mediation.admob.AdMobCustomTabsAdapter".equals(str)) {
                zzald = new zzaly(new zzany());
            } else {
                zzald = zzf(str, jSONObject);
            }
            return new zzdac(zzald);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final zzani zzdi(String str) throws RemoteException {
        return zzamg().zzdi(str);
    }

    public final boolean zzamf() {
        return this.zzfzi.get() != null;
    }

    private final zzald zzf(String str, JSONObject jSONObject) throws RemoteException {
        zzalc zzamg = zzamg();
        if ("com.google.ads.mediation.customevent.CustomEventAdapter".equals(str) || "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str)) {
            try {
                if (zzamg.zzdf(jSONObject.getString("class_name"))) {
                    return zzamg.zzde("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter");
                }
                return zzamg.zzde("com.google.ads.mediation.customevent.CustomEventAdapter");
            } catch (JSONException e) {
                zzavs.zzc("Invalid custom event.", e);
            }
        }
        return zzamg.zzde(str);
    }

    private final zzalc zzamg() throws RemoteException {
        zzalc zzalc = this.zzfzi.get();
        if (zzalc != null) {
            return zzalc;
        }
        zzavs.zzez("Unexpected call to adapter creator.");
        throw new RemoteException();
    }
}
