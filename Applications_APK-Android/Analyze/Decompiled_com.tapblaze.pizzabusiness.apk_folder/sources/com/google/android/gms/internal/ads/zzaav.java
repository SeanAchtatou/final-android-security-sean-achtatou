package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaav {
    public static zzaan<String> zzcsv = zzaan.zzi("gads:sdk_csi_server", "https://csi.gstatic.com/csi");
    public static zzaan<Boolean> zzcsw = zzaan.zzf("gads:enabled_sdk_csi", false);
}
