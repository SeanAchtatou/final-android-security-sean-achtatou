package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.BaseGmsClient;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzrq {
    /* access modifiers changed from: private */
    public final Object lock = new Object();
    private final Runnable zzbrc = new zzrt(this);
    /* access modifiers changed from: private */
    public zzrz zzbrd;
    /* access modifiers changed from: private */
    public zzsd zzbre;
    private Context zzup;

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0047, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void initialize(android.content.Context r3) {
        /*
            r2 = this;
            if (r3 != 0) goto L_0x0003
            return
        L_0x0003:
            java.lang.Object r0 = r2.lock
            monitor-enter(r0)
            android.content.Context r1 = r2.zzup     // Catch:{ all -> 0x0048 }
            if (r1 == 0) goto L_0x000c
            monitor-exit(r0)     // Catch:{ all -> 0x0048 }
            return
        L_0x000c:
            android.content.Context r3 = r3.getApplicationContext()     // Catch:{ all -> 0x0048 }
            r2.zzup = r3     // Catch:{ all -> 0x0048 }
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r3 = com.google.android.gms.internal.ads.zzzn.zzcmz     // Catch:{ all -> 0x0048 }
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ all -> 0x0048 }
            java.lang.Object r3 = r1.zzd(r3)     // Catch:{ all -> 0x0048 }
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ all -> 0x0048 }
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x0048 }
            if (r3 == 0) goto L_0x0028
            r2.connect()     // Catch:{ all -> 0x0048 }
            goto L_0x0046
        L_0x0028:
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r3 = com.google.android.gms.internal.ads.zzzn.zzcmy     // Catch:{ all -> 0x0048 }
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ all -> 0x0048 }
            java.lang.Object r3 = r1.zzd(r3)     // Catch:{ all -> 0x0048 }
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ all -> 0x0048 }
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x0048 }
            if (r3 == 0) goto L_0x0046
            com.google.android.gms.internal.ads.zzrs r3 = new com.google.android.gms.internal.ads.zzrs     // Catch:{ all -> 0x0048 }
            r3.<init>(r2)     // Catch:{ all -> 0x0048 }
            com.google.android.gms.internal.ads.zzqe r1 = com.google.android.gms.ads.internal.zzq.zzkt()     // Catch:{ all -> 0x0048 }
            r1.zza(r3)     // Catch:{ all -> 0x0048 }
        L_0x0046:
            monitor-exit(r0)     // Catch:{ all -> 0x0048 }
            return
        L_0x0048:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0048 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzrq.initialize(android.content.Context):void");
    }

    public final void zzmo() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcna)).booleanValue()) {
            synchronized (this.lock) {
                connect();
                zzq.zzkq();
                zzawb.zzdsr.removeCallbacks(this.zzbrc);
                zzq.zzkq();
                zzawb.zzdsr.postDelayed(this.zzbrc, ((Long) zzve.zzoy().zzd(zzzn.zzcnb)).longValue());
            }
        }
    }

    public final zzrx zza(zzry zzry) {
        synchronized (this.lock) {
            if (this.zzbre == null) {
                zzrx zzrx = new zzrx();
                return zzrx;
            }
            try {
                zzrx zza = this.zzbre.zza(zzry);
                return zza;
            } catch (RemoteException e) {
                zzavs.zzc("Unable to call into cache service.", e);
                return new zzrx();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void connect() {
        /*
            r3 = this;
            java.lang.Object r0 = r3.lock
            monitor-enter(r0)
            android.content.Context r1 = r3.zzup     // Catch:{ all -> 0x0025 }
            if (r1 == 0) goto L_0x0023
            com.google.android.gms.internal.ads.zzrz r1 = r3.zzbrd     // Catch:{ all -> 0x0025 }
            if (r1 == 0) goto L_0x000c
            goto L_0x0023
        L_0x000c:
            com.google.android.gms.internal.ads.zzrv r1 = new com.google.android.gms.internal.ads.zzrv     // Catch:{ all -> 0x0025 }
            r1.<init>(r3)     // Catch:{ all -> 0x0025 }
            com.google.android.gms.internal.ads.zzru r2 = new com.google.android.gms.internal.ads.zzru     // Catch:{ all -> 0x0025 }
            r2.<init>(r3)     // Catch:{ all -> 0x0025 }
            com.google.android.gms.internal.ads.zzrz r1 = r3.zza(r1, r2)     // Catch:{ all -> 0x0025 }
            r3.zzbrd = r1     // Catch:{ all -> 0x0025 }
            com.google.android.gms.internal.ads.zzrz r1 = r3.zzbrd     // Catch:{ all -> 0x0025 }
            r1.checkAvailabilityAndConnect()     // Catch:{ all -> 0x0025 }
            monitor-exit(r0)     // Catch:{ all -> 0x0025 }
            return
        L_0x0023:
            monitor-exit(r0)     // Catch:{ all -> 0x0025 }
            return
        L_0x0025:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0025 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzrq.connect():void");
    }

    private final synchronized zzrz zza(BaseGmsClient.BaseConnectionCallbacks baseConnectionCallbacks, BaseGmsClient.BaseOnConnectionFailedListener baseOnConnectionFailedListener) {
        return new zzrz(this.zzup, zzq.zzle().zzxb(), baseConnectionCallbacks, baseOnConnectionFailedListener);
    }

    /* access modifiers changed from: private */
    public final void disconnect() {
        synchronized (this.lock) {
            if (this.zzbrd != null) {
                if (this.zzbrd.isConnected() || this.zzbrd.isConnecting()) {
                    this.zzbrd.disconnect();
                }
                this.zzbrd = null;
                this.zzbre = null;
                Binder.flushPendingCommands();
            }
        }
    }
}
