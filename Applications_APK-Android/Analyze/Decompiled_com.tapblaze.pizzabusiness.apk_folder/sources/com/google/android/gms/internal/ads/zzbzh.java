package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbzh {
    private final Executor zzfci;
    private final zzczu zzfgl;
    private final zzcbn zzfod;
    private final Context zzup;

    public zzbzh(Context context, zzczu zzczu, Executor executor, zzcbn zzcbn) {
        this.zzup = context;
        this.zzfgl = zzczu;
        this.zzfci = executor;
        this.zzfod = zzcbn;
    }

    public final zzdhe<zzbdi> zzm(JSONObject jSONObject) {
        return zzdgs.zzb(zzdgs.zzb(zzdgs.zzaj(null), new zzbzm(this), this.zzfci), new zzbzk(this, jSONObject), this.zzfci);
    }

    public final zzdhe<zzbdi> zzo(String str, String str2) {
        return zzdgs.zzb(zzdgs.zzaj(null), new zzbzj(this, str, str2), this.zzfci);
    }

    private final void zzk(zzbdi zzbdi) {
        zzbdi.zza("/video", zzafa.zzcxf);
        zzbdi.zza("/videoMeta", zzafa.zzcxg);
        zzbdi.zza("/precache", new zzbcs());
        zzbdi.zza("/delayPageLoaded", zzafa.zzcxj);
        zzbdi.zza("/instrument", zzafa.zzcxh);
        zzbdi.zza("/log", zzafa.zzcxa);
        zzbdi.zza("/videoClicked", zzafa.zzcxb);
        zzbdi.zzaaa().zzbb(true);
        zzbdi.zza("/click", zzafa.zzcww);
        if (this.zzfgl.zzdkf != null) {
            zzbdi.zzaaa().zzbc(true);
            zzbdi.zza("/open", new zzafr(null, null));
            return;
        }
        zzbdi.zzaaa().zzbc(false);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zzq(Object obj) throws Exception {
        zzbdi zza = this.zzfod.zza(zzuj.zzg(this.zzup), false);
        zzazi zzl = zzazi.zzl(zza);
        zzk(zza);
        zza.zzaaa().zza(new zzbzl(zzl));
        zza.loadUrl((String) zzve.zzoy().zzd(zzzn.zzclz));
        return zzl;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(String str, String str2, Object obj) throws Exception {
        zzbdi zza = this.zzfod.zza(zzuj.zzg(this.zzup), false);
        zzazi zzl = zzazi.zzl(zza);
        zzk(zza);
        if (this.zzfgl.zzdkf != null) {
            zza.zza(zzbey.zzabs());
        } else {
            zza.zza(zzbey.zzabr());
        }
        zza.zzaaa().zza(new zzbzo(this, zza, zzl));
        zza.zzb(str, str2, null);
        return zzl;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzbdi zzbdi, zzazi zzazi, boolean z) {
        if (!(this.zzfgl.zzgmk == null || zzbdi.zzyl() == null)) {
            zzbdi.zzyl().zzb(this.zzfgl.zzgmk);
        }
        zzazi.zzxn();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(JSONObject jSONObject, zzbdi zzbdi) throws Exception {
        zzazi zzl = zzazi.zzl(zzbdi);
        if (this.zzfgl.zzdkf != null) {
            zzbdi.zza(zzbey.zzabs());
        } else {
            zzbdi.zza(zzbey.zzabr());
        }
        zzbdi.zzaaa().zza(new zzbzn(this, zzbdi, zzl));
        zzbdi.zza("google.afma.nativeAds.renderVideo", jSONObject);
        return zzl;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(zzbdi zzbdi, zzazi zzazi, boolean z) {
        if (!(this.zzfgl.zzgmk == null || zzbdi.zzyl() == null)) {
            zzbdi.zzyl().zzb(this.zzfgl.zzgmk);
        }
        zzazi.zzxn();
    }
}
