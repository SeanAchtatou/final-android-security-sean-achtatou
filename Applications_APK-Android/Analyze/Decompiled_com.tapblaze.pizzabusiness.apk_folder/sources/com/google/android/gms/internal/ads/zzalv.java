package com.google.android.gms.internal.ads;

import android.location.Location;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import java.util.Date;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzalv implements MediationAdRequest {
    private final String zzabq;
    private final int zzcbz;
    private final boolean zzcck;
    private final int zzddh;
    private final int zzddi;
    private final Date zzme;
    private final Set<String> zzmg;
    private final boolean zzmh;
    private final Location zzmi;

    public zzalv(Date date, int i, Set<String> set, Location location, boolean z, int i2, boolean z2, int i3, String str) {
        this.zzme = date;
        this.zzcbz = i;
        this.zzmg = set;
        this.zzmi = location;
        this.zzmh = z;
        this.zzddh = i2;
        this.zzcck = z2;
        this.zzddi = i3;
        this.zzabq = str;
    }

    @Deprecated
    public final Date getBirthday() {
        return this.zzme;
    }

    @Deprecated
    public final int getGender() {
        return this.zzcbz;
    }

    public final Set<String> getKeywords() {
        return this.zzmg;
    }

    public final Location getLocation() {
        return this.zzmi;
    }

    public final boolean isTesting() {
        return this.zzmh;
    }

    public final int taggedForChildDirectedTreatment() {
        return this.zzddh;
    }

    @Deprecated
    public final boolean isDesignedForFamilies() {
        return this.zzcck;
    }
}
