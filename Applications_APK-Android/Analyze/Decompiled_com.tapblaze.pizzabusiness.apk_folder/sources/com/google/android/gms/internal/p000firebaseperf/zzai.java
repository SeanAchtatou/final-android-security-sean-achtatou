package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzai  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzai extends zzaz<Long> {
    private static zzai zzal;

    private zzai() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.NetworkEventCountBackground";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_rl_network_event_count_bg";
    }

    public static synchronized zzai zzai() {
        zzai zzai;
        synchronized (zzai.class) {
            if (zzal == null) {
                zzal = new zzai();
            }
            zzai = zzal;
        }
        return zzai;
    }
}
