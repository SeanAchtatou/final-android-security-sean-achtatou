package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcbo implements zzdxg<zzcio<zzcbb>> {
    private final zzdxp<zzdhd> zzfei;
    private final zzdxp<zzdcr> zzfet;
    private final zzdxp<zzcma> zzfeu;
    private final zzdxp<zzcmy> zzfev;

    public zzcbo(zzdxp<zzdcr> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<zzcma> zzdxp3, zzdxp<zzcmy> zzdxp4) {
        this.zzfet = zzdxp;
        this.zzfei = zzdxp2;
        this.zzfeu = zzdxp3;
        this.zzfev = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return (zzcio) zzdxm.zza(new zzcna(this.zzfet.get(), this.zzfei.get(), this.zzfev.get(), this.zzfeu.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
