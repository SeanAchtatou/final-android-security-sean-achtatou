package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzem implements Runnable {
    private final /* synthetic */ zzei zzyc;

    zzem(zzei zzei) {
        this.zzyc = zzei;
    }

    public final void run() {
        zzzn.initialize(this.zzyc.zzup);
    }
}
