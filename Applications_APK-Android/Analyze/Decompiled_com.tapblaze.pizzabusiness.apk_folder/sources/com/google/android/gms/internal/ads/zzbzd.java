package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbzd implements zzded {
    static final zzded zzdoq = new zzbzd();

    private zzbzd() {
    }

    public final Object apply(Object obj) {
        ArrayList arrayList = new ArrayList();
        for (zzbzf zzbzf : (List) obj) {
            if (zzbzf != null) {
                arrayList.add(zzbzf);
            }
        }
        return arrayList;
    }
}
