package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
abstract class zzdql implements zzdqp {
    zzdql() {
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public /* synthetic */ Object next() {
        return Byte.valueOf(nextByte());
    }
}
