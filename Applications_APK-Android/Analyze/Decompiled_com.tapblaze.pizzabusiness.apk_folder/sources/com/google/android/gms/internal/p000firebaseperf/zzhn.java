package com.google.android.gms.internal.p000firebaseperf;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhn  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
class zzhn extends AbstractSet<Map.Entry<K, V>> {
    private final /* synthetic */ zzhg zzui;

    private zzhn(zzhg zzhg) {
        this.zzui = zzhg;
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        return new zzho(this.zzui, null);
    }

    public int size() {
        return this.zzui.size();
    }

    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.zzui.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.zzui.remove(entry.getKey());
        return true;
    }

    public void clear() {
        this.zzui.clear();
    }

    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.zzui.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    /* synthetic */ zzhn(zzhg zzhg, zzhf zzhf) {
        this(zzhg);
    }
}
