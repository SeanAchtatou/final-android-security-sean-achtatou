package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdji extends zzdih<zzdlk, zzdlj> {
    private final /* synthetic */ zzdjg zzgzd;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdji(zzdjg zzdjg, Class cls) {
        super(cls);
        this.zzgzd = zzdjg;
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        zzdlk zzdlk = (zzdlk) zzdte;
        return (zzdlj) ((zzdrt) zzdlj.zzati().zzc(zzdlk.zzath()).zzaa(zzdqk.zzu(zzdpn.zzey(zzdlk.getKeySize()))).zzee(0).zzbaf());
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdlk.zzz(zzdqk);
    }

    public final /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdlk zzdlk = (zzdlk) zzdte;
        zzdpo.zzez(zzdlk.getKeySize());
        zzdjg.zza(zzdlk.zzath());
    }
}
