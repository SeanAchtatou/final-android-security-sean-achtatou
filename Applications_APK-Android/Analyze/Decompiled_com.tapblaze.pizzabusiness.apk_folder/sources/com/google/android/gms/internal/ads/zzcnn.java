package com.google.android.gms.internal.ads;

import android.view.View;
import com.google.android.gms.ads.internal.zze;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcnn implements zze {
    private final /* synthetic */ zzczt zzgav;
    private final /* synthetic */ zzczl zzgaw;
    private final /* synthetic */ zzazl zzgbu;
    private final /* synthetic */ zzcnt zzgbv;
    private final /* synthetic */ zzcnl zzgbw;

    zzcnn(zzcnl zzcnl, zzazl zzazl, zzczt zzczt, zzczl zzczl, zzcnt zzcnt) {
        this.zzgbw = zzcnl;
        this.zzgbu = zzazl;
        this.zzgav = zzczt;
        this.zzgaw = zzczl;
        this.zzgbv = zzcnt;
    }

    public final void zzjr() {
    }

    public final void zzjs() {
    }

    public final void zzg(View view) {
        this.zzgbu.set(this.zzgbw.zzgbr.zza(this.zzgav, this.zzgaw, view, this.zzgbv));
    }
}
