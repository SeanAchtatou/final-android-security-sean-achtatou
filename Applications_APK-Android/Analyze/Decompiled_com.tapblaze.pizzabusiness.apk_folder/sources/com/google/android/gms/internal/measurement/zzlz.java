package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzlz implements zzdb<zzly> {
    private static zzlz zza = new zzlz();
    private final zzdb<zzly> zzb;

    public static boolean zzb() {
        return ((zzly) zza.zza()).zza();
    }

    public static boolean zzc() {
        return ((zzly) zza.zza()).zzb();
    }

    private zzlz(zzdb<zzly> zzdb) {
        this.zzb = zzda.zza((zzdb) zzdb);
    }

    public zzlz() {
        this(zzda.zza(new zzmb()));
    }

    public final /* synthetic */ Object zza() {
        return this.zzb.zza();
    }
}
