package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaue implements Callable {
    private final zzatv zzdps;

    zzaue(zzatv zzatv) {
        this.zzdps = zzatv;
    }

    public final Object call() {
        return this.zzdps.zzur();
    }
}
