package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaas<T> extends zzaan<T> {
    public static zzaan<Boolean> zzf(String str, boolean z) {
        return new zzaas(str, true, zzaap.zzcsh);
    }

    protected zzaas(String str, T t, Integer num) {
        super(str, t, num);
    }

    public final T get() {
        if (zzabn.zzcuz.get()) {
            return super.get();
        }
        throw new IllegalStateException("Striped code is accessed: 54c42518-856a-44fb-aae0-cd6676d514e5");
    }
}
