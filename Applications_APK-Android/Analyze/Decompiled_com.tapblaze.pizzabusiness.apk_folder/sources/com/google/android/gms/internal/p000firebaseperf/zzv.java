package com.google.android.gms.internal.p000firebaseperf;

import java.util.Iterator;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzv  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public abstract class zzv<E> implements Iterator<E> {
    protected zzv() {
    }

    @Deprecated
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
