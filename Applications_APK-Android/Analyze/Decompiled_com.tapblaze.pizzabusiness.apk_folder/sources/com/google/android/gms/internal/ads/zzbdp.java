package com.google.android.gms.internal.ads;

import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbdp implements View.OnAttachStateChangeListener {
    private final /* synthetic */ zzato zzeev;
    private final /* synthetic */ zzbdl zzeex;

    zzbdp(zzbdl zzbdl, zzato zzato) {
        this.zzeex = zzbdl;
        this.zzeev = zzato;
    }

    public final void onViewDetachedFromWindow(View view) {
    }

    public final void onViewAttachedToWindow(View view) {
        this.zzeex.zza(view, this.zzeev, 10);
    }
}
