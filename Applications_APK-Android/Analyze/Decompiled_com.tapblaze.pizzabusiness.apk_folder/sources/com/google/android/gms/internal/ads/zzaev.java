package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaev implements zzafn<Object> {
    private final zzaey zzcws;

    public zzaev(zzaey zzaey) {
        this.zzcws = zzaey;
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = map.get("name");
        if (str == null) {
            zzavs.zzez("App event with no name parameter.");
        } else {
            this.zzcws.onAppEvent(str, map.get("info"));
        }
    }
}
