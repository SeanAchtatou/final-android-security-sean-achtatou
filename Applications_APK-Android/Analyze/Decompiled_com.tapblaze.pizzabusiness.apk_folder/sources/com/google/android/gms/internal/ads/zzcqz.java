package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcqz implements zzcub<zzcqw> {
    private final Executor executor;
    private final ScheduledExecutorService zzffx;
    private final Context zzup;

    public zzcqz(Context context, ScheduledExecutorService scheduledExecutorService, Executor executor2) {
        this.zzup = context;
        this.zzffx = scheduledExecutorService;
        this.executor = executor2;
    }

    public final zzdhe<zzcqw> zzanc() {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcqj)).booleanValue()) {
            return zzdgs.zzaj(null);
        }
        zzazl zzazl = new zzazl();
        try {
            new zzcqy(zzazl).zzbk(false);
        } catch (Throwable unused) {
            zzavs.zzex("ArCoreApk is not ready.");
            zzazl.set(false);
        }
        return zzdgs.zzb(zzdgs.zzb(zzdgs.zza(zzazl, 200, TimeUnit.MILLISECONDS, this.zzffx), new zzcrb(this), this.executor), Throwable.class, zzcra.zzbkw, this.executor);
    }
}
