package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaxc extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaxc> CREATOR = new zzaxe();
    public final int errorCode;
    public final String zzdtr;

    public static zzaxc zza(Throwable th, int i) {
        return new zzaxc(th.getMessage(), i);
    }

    zzaxc(String str, int i) {
        this.zzdtr = str == null ? "" : str;
        this.errorCode = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zzdtr, false);
        SafeParcelWriter.writeInt(parcel, 2, this.errorCode);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
