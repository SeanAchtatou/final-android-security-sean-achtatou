package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbmk<AdT> implements zzdxg<zzbmh<AdT>> {
    private final zzdxp<Map<String, zzcio<AdT>>> zzffv;

    private zzbmk(zzdxp<Map<String, zzcio<AdT>>> zzdxp) {
        this.zzffv = zzdxp;
    }

    public static <AdT> zzbmk<AdT> zzd(zzdxp<Map<String, zzcio<AdT>>> zzdxp) {
        return new zzbmk<>(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbmh(this.zzffv.get());
    }
}
