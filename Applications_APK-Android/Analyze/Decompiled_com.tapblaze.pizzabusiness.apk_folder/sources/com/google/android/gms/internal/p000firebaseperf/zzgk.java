package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgk  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzgk {
    private static final zzgi zzsr = zzij();
    private static final zzgi zzss = new zzgh();

    static zzgi zzih() {
        return zzsr;
    }

    static zzgi zzii() {
        return zzss;
    }

    private static zzgi zzij() {
        try {
            return (zzgi) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
