package com.google.android.gms.internal.ads;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zzdex<E> extends zzdes<E> {
    public zzdex() {
        this(4);
    }

    private zzdex(int i) {
        super(4);
    }

    public final /* synthetic */ zzdev zze(Iterable iterable) {
        super.zze(iterable);
        return this;
    }

    public final /* synthetic */ zzdes zzad(Object obj) {
        return (zzdex) zzae(obj);
    }

    public final /* synthetic */ zzdev zza(Iterator it) {
        super.zza(it);
        return this;
    }

    public final /* synthetic */ zzdev zzae(Object obj) {
        super.zzae(obj);
        return this;
    }
}
