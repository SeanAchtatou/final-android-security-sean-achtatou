package com.google.android.gms.internal.ads;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzduf {
    /* access modifiers changed from: private */
    public static final Iterator<Object> zzhqx = new zzdui();
    private static final Iterable<Object> zzhqy = new zzduh();

    static <T> Iterable<T> zzbcd() {
        return zzhqy;
    }
}
