package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzew;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class zzeu<T extends zzew<T>> {
    private static final zzeu zzd = new zzeu(true);
    final zzhg<T, Object> zza;
    private boolean zzb;
    private boolean zzc;

    private zzeu() {
        this.zza = zzhg.zza(16);
    }

    private zzeu(boolean z) {
        this(zzhg.zza(0));
        zzb();
    }

    private zzeu(zzhg<T, Object> zzhg) {
        this.zza = zzhg;
        zzb();
    }

    public static <T extends zzew<T>> zzeu<T> zza() {
        return zzd;
    }

    public final void zzb() {
        if (!this.zzb) {
            this.zza.zza();
            this.zzb = true;
        }
    }

    public final boolean zzc() {
        return this.zzb;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzeu)) {
            return false;
        }
        return this.zza.equals(((zzeu) obj).zza);
    }

    public final int hashCode() {
        return this.zza.hashCode();
    }

    public final Iterator<Map.Entry<T, Object>> zzd() {
        if (this.zzc) {
            return new zzfs(this.zza.entrySet().iterator());
        }
        return this.zza.entrySet().iterator();
    }

    /* access modifiers changed from: package-private */
    public final Iterator<Map.Entry<T, Object>> zze() {
        if (this.zzc) {
            return new zzfs(this.zza.zze().iterator());
        }
        return this.zza.zze().iterator();
    }

    private final Object zza(T t) {
        Object obj = this.zza.get(t);
        if (!(obj instanceof zzfr)) {
            return obj;
        }
        zzfr zzfr = (zzfr) obj;
        return zzfr.zza();
    }

    private final void zzb(T t, Object obj) {
        if (!t.zzd()) {
            zza(t.zzb(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList2.get(i);
                i++;
                zza(t.zzb(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof zzfr) {
            this.zzc = true;
        }
        this.zza.put(t, obj);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        if ((r3 instanceof com.google.android.gms.internal.measurement.zzfg) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001c, code lost:
        if ((r3 instanceof com.google.android.gms.internal.measurement.zzfr) == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void zza(com.google.android.gms.internal.measurement.zzik r2, java.lang.Object r3) {
        /*
            com.google.android.gms.internal.measurement.zzfh.zza(r3)
            int[] r0 = com.google.android.gms.internal.measurement.zzex.zza
            com.google.android.gms.internal.measurement.zzir r2 = r2.zza()
            int r2 = r2.ordinal()
            r2 = r0[r2]
            r0 = 1
            r1 = 0
            switch(r2) {
                case 1: goto L_0x0040;
                case 2: goto L_0x003d;
                case 3: goto L_0x003a;
                case 4: goto L_0x0037;
                case 5: goto L_0x0034;
                case 6: goto L_0x0031;
                case 7: goto L_0x0028;
                case 8: goto L_0x001f;
                case 9: goto L_0x0016;
                default: goto L_0x0014;
            }
        L_0x0014:
            r0 = 0
            goto L_0x0042
        L_0x0016:
            boolean r2 = r3 instanceof com.google.android.gms.internal.measurement.zzgm
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.google.android.gms.internal.measurement.zzfr
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x001f:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.google.android.gms.internal.measurement.zzfg
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0028:
            boolean r2 = r3 instanceof com.google.android.gms.internal.measurement.zzdw
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0031:
            boolean r0 = r3 instanceof java.lang.String
            goto L_0x0042
        L_0x0034:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L_0x0042
        L_0x0037:
            boolean r0 = r3 instanceof java.lang.Double
            goto L_0x0042
        L_0x003a:
            boolean r0 = r3 instanceof java.lang.Float
            goto L_0x0042
        L_0x003d:
            boolean r0 = r3 instanceof java.lang.Long
            goto L_0x0042
        L_0x0040:
            boolean r0 = r3 instanceof java.lang.Integer
        L_0x0042:
            if (r0 == 0) goto L_0x0045
            return
        L_0x0045:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            goto L_0x004e
        L_0x004d:
            throw r2
        L_0x004e:
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzeu.zza(com.google.android.gms.internal.measurement.zzik, java.lang.Object):void");
    }

    public final boolean zzf() {
        for (int i = 0; i < this.zza.zzc(); i++) {
            if (!zza((Map.Entry) this.zza.zzb(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> entry : this.zza.zzd()) {
            if (!zza((Map.Entry) entry)) {
                return false;
            }
        }
        return true;
    }

    private static <T extends zzew<T>> boolean zza(Map.Entry<T, Object> entry) {
        zzew zzew = (zzew) entry.getKey();
        if (zzew.zzc() == zzir.MESSAGE) {
            if (zzew.zzd()) {
                for (zzgm g_ : (List) entry.getValue()) {
                    if (!g_.g_()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof zzgm) {
                    if (!((zzgm) value).g_()) {
                        return false;
                    }
                } else if (value instanceof zzfr) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public final void zza(zzeu<T> zzeu) {
        for (int i = 0; i < zzeu.zza.zzc(); i++) {
            zzb(zzeu.zza.zzb(i));
        }
        for (Map.Entry<T, Object> zzb2 : zzeu.zza.zzd()) {
            zzb(zzb2);
        }
    }

    private static Object zza(Object obj) {
        if (obj instanceof zzgv) {
            return ((zzgv) obj).zza();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    private final void zzb(Map.Entry<T, Object> entry) {
        Object obj;
        zzew zzew = (zzew) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof zzfr) {
            zzfr zzfr = (zzfr) value;
            value = zzfr.zza();
        }
        if (zzew.zzd()) {
            Object zza2 = zza(zzew);
            if (zza2 == null) {
                zza2 = new ArrayList();
            }
            for (Object zza3 : (List) value) {
                ((List) zza2).add(zza(zza3));
            }
            this.zza.put(zzew, zza2);
        } else if (zzew.zzc() == zzir.MESSAGE) {
            Object zza4 = zza(zzew);
            if (zza4 == null) {
                this.zza.put(zzew, zza(value));
                return;
            }
            if (zza4 instanceof zzgv) {
                obj = zzew.zza((zzgv) zza4, (zzgv) value);
            } else {
                obj = zzew.zza(((zzgm) zza4).zzbq(), (zzgm) value).zzv();
            }
            this.zza.put(zzew, obj);
        } else {
            this.zza.put(zzew, zza(value));
        }
    }

    static void zza(zzel zzel, zzik zzik, int i, Object obj) throws IOException {
        if (zzik == zzik.GROUP) {
            zzgm zzgm = (zzgm) obj;
            zzfh.zza(zzgm);
            zzel.zza(i, 3);
            zzgm.zza(zzel);
            zzel.zza(i, 4);
            return;
        }
        zzel.zza(i, zzik.zzb());
        switch (zzex.zzb[zzik.ordinal()]) {
            case 1:
                zzel.zza(((Double) obj).doubleValue());
                return;
            case 2:
                zzel.zza(((Float) obj).floatValue());
                return;
            case 3:
                zzel.zza(((Long) obj).longValue());
                return;
            case 4:
                zzel.zza(((Long) obj).longValue());
                return;
            case 5:
                zzel.zza(((Integer) obj).intValue());
                return;
            case 6:
                zzel.zzc(((Long) obj).longValue());
                return;
            case 7:
                zzel.zzd(((Integer) obj).intValue());
                return;
            case 8:
                zzel.zza(((Boolean) obj).booleanValue());
                return;
            case 9:
                ((zzgm) obj).zza(zzel);
                return;
            case 10:
                zzel.zza((zzgm) obj);
                return;
            case 11:
                if (obj instanceof zzdw) {
                    zzel.zza((zzdw) obj);
                    return;
                } else {
                    zzel.zza((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof zzdw) {
                    zzel.zza((zzdw) obj);
                    return;
                }
                byte[] bArr = (byte[]) obj;
                zzel.zzb(bArr, 0, bArr.length);
                return;
            case 13:
                zzel.zzb(((Integer) obj).intValue());
                return;
            case 14:
                zzel.zzd(((Integer) obj).intValue());
                return;
            case 15:
                zzel.zzc(((Long) obj).longValue());
                return;
            case 16:
                zzel.zzc(((Integer) obj).intValue());
                return;
            case 17:
                zzel.zzb(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof zzfg) {
                    zzel.zza(((zzfg) obj).zza());
                    return;
                } else {
                    zzel.zza(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    public final int zzg() {
        int i = 0;
        for (int i2 = 0; i2 < this.zza.zzc(); i2++) {
            i += zzc(this.zza.zzb(i2));
        }
        for (Map.Entry<T, Object> zzc2 : this.zza.zzd()) {
            i += zzc(zzc2);
        }
        return i;
    }

    private static int zzc(Map.Entry<T, Object> entry) {
        zzew zzew = (zzew) entry.getKey();
        Object value = entry.getValue();
        if (zzew.zzc() != zzir.MESSAGE || zzew.zzd() || zzew.zze()) {
            return zza(zzew, value);
        }
        if (value instanceof zzfr) {
            return zzel.zzb(((zzew) entry.getKey()).zza(), (zzfr) value);
        }
        return zzel.zzb(((zzew) entry.getKey()).zza(), (zzgm) value);
    }

    static int zza(zzik zzik, int i, Object obj) {
        int zze = zzel.zze(i);
        if (zzik == zzik.GROUP) {
            zzfh.zza((zzgm) obj);
            zze <<= 1;
        }
        return zze + zzb(zzik, obj);
    }

    private static int zzb(zzik zzik, Object obj) {
        switch (zzex.zzb[zzik.ordinal()]) {
            case 1:
                return zzel.zzb(((Double) obj).doubleValue());
            case 2:
                return zzel.zzb(((Float) obj).floatValue());
            case 3:
                return zzel.zzd(((Long) obj).longValue());
            case 4:
                return zzel.zze(((Long) obj).longValue());
            case 5:
                return zzel.zzf(((Integer) obj).intValue());
            case 6:
                return zzel.zzg(((Long) obj).longValue());
            case 7:
                return zzel.zzi(((Integer) obj).intValue());
            case 8:
                return zzel.zzb(((Boolean) obj).booleanValue());
            case 9:
                return zzel.zzc((zzgm) obj);
            case 10:
                if (obj instanceof zzfr) {
                    return zzel.zza((zzfr) obj);
                }
                return zzel.zzb((zzgm) obj);
            case 11:
                if (obj instanceof zzdw) {
                    return zzel.zzb((zzdw) obj);
                }
                return zzel.zzb((String) obj);
            case 12:
                if (obj instanceof zzdw) {
                    return zzel.zzb((zzdw) obj);
                }
                return zzel.zzb((byte[]) obj);
            case 13:
                return zzel.zzg(((Integer) obj).intValue());
            case 14:
                return zzel.zzj(((Integer) obj).intValue());
            case 15:
                return zzel.zzh(((Long) obj).longValue());
            case 16:
                return zzel.zzh(((Integer) obj).intValue());
            case 17:
                return zzel.zzf(((Long) obj).longValue());
            case 18:
                if (obj instanceof zzfg) {
                    return zzel.zzk(((zzfg) obj).zza());
                }
                return zzel.zzk(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int zza(zzew<?> zzew, Object obj) {
        zzik zzb2 = zzew.zzb();
        int zza2 = zzew.zza();
        if (!zzew.zzd()) {
            return zza(zzb2, zza2, obj);
        }
        int i = 0;
        if (zzew.zze()) {
            for (Object zzb3 : (List) obj) {
                i += zzb(zzb2, zzb3);
            }
            return zzel.zze(zza2) + i + zzel.zzl(i);
        }
        for (Object zza3 : (List) obj) {
            i += zza(zzb2, zza2, zza3);
        }
        return i;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzeu zzeu = new zzeu();
        for (int i = 0; i < this.zza.zzc(); i++) {
            Map.Entry<T, Object> zzb2 = this.zza.zzb(i);
            zzeu.zzb((zzew) zzb2.getKey(), zzb2.getValue());
        }
        for (Map.Entry next : this.zza.zzd()) {
            zzeu.zzb((zzew) next.getKey(), next.getValue());
        }
        zzeu.zzc = this.zzc;
        return zzeu;
    }
}
