package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzctd implements Callable {
    private final zzcta zzggg;

    zzctd(zzcta zzcta) {
        this.zzggg = zzcta;
    }

    public final Object call() {
        return this.zzggg.zzanl();
    }
}
