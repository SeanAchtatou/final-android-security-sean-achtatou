package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzin extends zze {
    protected zzio zza;
    private volatile zzio zzb;
    private zzio zzc;
    private final Map<Activity, zzio> zzd = new ArrayMap();
    private zzio zze;
    private String zzf;

    public zzin(zzgf zzgf) {
        super(zzgf);
    }

    /* access modifiers changed from: protected */
    public final boolean zzz() {
        return false;
    }

    public final zzio zzab() {
        zzw();
        zzd();
        return this.zza;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzin.zza(android.app.Activity, com.google.android.gms.measurement.internal.zzio, boolean):void
     arg types: [android.app.Activity, com.google.android.gms.measurement.internal.zzio, int]
     candidates:
      com.google.android.gms.measurement.internal.zzin.zza(com.google.android.gms.measurement.internal.zzio, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.zzin.zza(com.google.android.gms.measurement.internal.zzio, boolean, long):void
      com.google.android.gms.measurement.internal.zzin.zza(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.zzin.zza(android.app.Activity, com.google.android.gms.measurement.internal.zzio, boolean):void */
    public final void zza(Activity activity, String str, String str2) {
        if (this.zzb == null) {
            zzr().zzk().zza("setCurrentScreen cannot be called while no activity active");
        } else if (this.zzd.get(activity) == null) {
            zzr().zzk().zza("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = zza(activity.getClass().getCanonicalName());
            }
            boolean equals = this.zzb.zzb.equals(str2);
            boolean zzc2 = zzkv.zzc(this.zzb.zza, str);
            if (equals && zzc2) {
                zzr().zzk().zza("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                zzr().zzk().zza("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                zzr().zzx().zza("Setting current screen to name, class", str == null ? "null" : str, str2);
                zzio zzio = new zzio(str, str2, zzp().zzg());
                this.zzd.put(activity, zzio);
                zza(activity, zzio, true);
            } else {
                zzr().zzk().zza("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    public final zzio zzac() {
        zzb();
        return this.zzb;
    }

    private final void zza(Activity activity, zzio zzio, boolean z) {
        zzio zzio2 = this.zzb == null ? this.zzc : this.zzb;
        zzio zzio3 = zzio.zzb == null ? new zzio(zzio.zza, zza(activity.getClass().getCanonicalName()), zzio.zzc) : zzio;
        this.zzc = this.zzb;
        this.zzb = zzio3;
        zzq().zza(new zziq(this, z, zzm().elapsedRealtime(), zzio2, zzio3));
    }

    /* access modifiers changed from: private */
    public final void zza(zzio zzio, boolean z, long j) {
        zze().zza(zzm().elapsedRealtime());
        if (zzk().zza(zzio.zzd, z, j)) {
            zzio.zzd = false;
        }
    }

    public static void zza(zzio zzio, Bundle bundle, boolean z) {
        if (bundle != null && zzio != null && (!bundle.containsKey("_sc") || z)) {
            if (zzio.zza != null) {
                bundle.putString("_sn", zzio.zza);
            } else {
                bundle.remove("_sn");
            }
            bundle.putString("_sc", zzio.zzb);
            bundle.putLong("_si", zzio.zzc);
        } else if (bundle != null && zzio == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    public final void zza(String str, zzio zzio) {
        zzd();
        synchronized (this) {
            if (this.zzf == null || this.zzf.equals(str) || zzio != null) {
                this.zzf = str;
                this.zze = zzio;
            }
        }
    }

    private static String zza(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    private final zzio zzd(Activity activity) {
        Preconditions.checkNotNull(activity);
        zzio zzio = this.zzd.get(activity);
        if (zzio != null) {
            return zzio;
        }
        zzio zzio2 = new zzio(null, zza(activity.getClass().getCanonicalName()), zzp().zzg());
        this.zzd.put(activity, zzio2);
        return zzio2;
    }

    public final void zza(Activity activity, Bundle bundle) {
        Bundle bundle2;
        if (bundle != null && (bundle2 = bundle.getBundle("com.google.app_measurement.screen_service")) != null) {
            this.zzd.put(activity, new zzio(bundle2.getString("name"), bundle2.getString("referrer_name"), bundle2.getLong("id")));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzin.zza(android.app.Activity, com.google.android.gms.measurement.internal.zzio, boolean):void
     arg types: [android.app.Activity, com.google.android.gms.measurement.internal.zzio, int]
     candidates:
      com.google.android.gms.measurement.internal.zzin.zza(com.google.android.gms.measurement.internal.zzio, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.zzin.zza(com.google.android.gms.measurement.internal.zzio, boolean, long):void
      com.google.android.gms.measurement.internal.zzin.zza(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.zzin.zza(android.app.Activity, com.google.android.gms.measurement.internal.zzio, boolean):void */
    public final void zza(Activity activity) {
        zza(activity, zzd(activity), false);
        zzb zze2 = zze();
        zze2.zzq().zza(new zzc(zze2, zze2.zzm().elapsedRealtime()));
    }

    public final void zzb(Activity activity) {
        zzio zzd2 = zzd(activity);
        this.zzc = this.zzb;
        this.zzb = null;
        zzq().zza(new zzip(this, zzd2, zzm().elapsedRealtime()));
    }

    public final void zzb(Activity activity, Bundle bundle) {
        zzio zzio;
        if (bundle != null && (zzio = this.zzd.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("id", zzio.zzc);
            bundle2.putString("name", zzio.zza);
            bundle2.putString("referrer_name", zzio.zzb);
            bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
        }
    }

    public final void zzc(Activity activity) {
        this.zzd.remove(activity);
    }

    public final /* bridge */ /* synthetic */ void zza() {
        super.zza();
    }

    public final /* bridge */ /* synthetic */ void zzb() {
        super.zzb();
    }

    public final /* bridge */ /* synthetic */ void zzc() {
        super.zzc();
    }

    public final /* bridge */ /* synthetic */ void zzd() {
        super.zzd();
    }

    public final /* bridge */ /* synthetic */ zzb zze() {
        return super.zze();
    }

    public final /* bridge */ /* synthetic */ zzhk zzf() {
        return super.zzf();
    }

    public final /* bridge */ /* synthetic */ zzey zzg() {
        return super.zzg();
    }

    public final /* bridge */ /* synthetic */ zzis zzh() {
        return super.zzh();
    }

    public final /* bridge */ /* synthetic */ zzin zzi() {
        return super.zzi();
    }

    public final /* bridge */ /* synthetic */ zzex zzj() {
        return super.zzj();
    }

    public final /* bridge */ /* synthetic */ zzjt zzk() {
        return super.zzk();
    }

    public final /* bridge */ /* synthetic */ zzah zzl() {
        return super.zzl();
    }

    public final /* bridge */ /* synthetic */ Clock zzm() {
        return super.zzm();
    }

    public final /* bridge */ /* synthetic */ Context zzn() {
        return super.zzn();
    }

    public final /* bridge */ /* synthetic */ zzez zzo() {
        return super.zzo();
    }

    public final /* bridge */ /* synthetic */ zzkv zzp() {
        return super.zzp();
    }

    public final /* bridge */ /* synthetic */ zzgc zzq() {
        return super.zzq();
    }

    public final /* bridge */ /* synthetic */ zzfb zzr() {
        return super.zzr();
    }

    public final /* bridge */ /* synthetic */ zzfo zzs() {
        return super.zzs();
    }

    public final /* bridge */ /* synthetic */ zzx zzt() {
        return super.zzt();
    }

    public final /* bridge */ /* synthetic */ zzw zzu() {
        return super.zzu();
    }
}
