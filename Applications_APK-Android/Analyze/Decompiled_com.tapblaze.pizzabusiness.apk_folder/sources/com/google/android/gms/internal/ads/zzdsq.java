package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdsq extends zzdso {
    private static final Class<?> zzhok = Collections.unmodifiableList(Collections.emptyList()).getClass();

    private zzdsq() {
        super();
    }

    /* access modifiers changed from: package-private */
    public final <L> List<L> zza(Object obj, long j) {
        return zza(obj, j, 10);
    }

    /* access modifiers changed from: package-private */
    public final void zzb(Object obj, long j) {
        Object obj2;
        List list = (List) zzduy.zzp(obj, j);
        if (list instanceof zzdsl) {
            obj2 = ((zzdsl) list).zzbaw();
        } else if (!zzhok.isAssignableFrom(list.getClass())) {
            if (!(list instanceof zzdtq) || !(list instanceof zzdsb)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                zzdsb zzdsb = (zzdsb) list;
                if (zzdsb.zzaxp()) {
                    zzdsb.zzaxq();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        zzduy.zza(obj, j, obj2);
    }

    private static <L> List<L> zza(Object obj, long j, int i) {
        List<L> zzdsm;
        List<L> list;
        List<L> zzd = zzd(obj, j);
        if (zzd.isEmpty()) {
            if (zzd instanceof zzdsl) {
                list = new zzdsm(i);
            } else if (!(zzd instanceof zzdtq) || !(zzd instanceof zzdsb)) {
                list = new ArrayList<>(i);
            } else {
                list = ((zzdsb) zzd).zzfd(i);
            }
            zzduy.zza(obj, j, list);
            return list;
        }
        if (zzhok.isAssignableFrom(zzd.getClass())) {
            zzdsm = new ArrayList<>(zzd.size() + i);
            zzdsm.addAll(zzd);
            zzduy.zza(obj, j, zzdsm);
        } else if (zzd instanceof zzdut) {
            zzdsm = new zzdsm(zzd.size() + i);
            zzdsm.addAll((zzdut) zzd);
            zzduy.zza(obj, j, zzdsm);
        } else if (!(zzd instanceof zzdtq) || !(zzd instanceof zzdsb)) {
            return zzd;
        } else {
            zzdsb zzdsb = (zzdsb) zzd;
            if (zzdsb.zzaxp()) {
                return zzd;
            }
            zzdsb zzfd = zzdsb.zzfd(zzd.size() + i);
            zzduy.zza(obj, j, zzfd);
            return zzfd;
        }
        return zzdsm;
    }

    /* access modifiers changed from: package-private */
    public final <E> void zza(Object obj, Object obj2, long j) {
        List zzd = zzd(obj2, j);
        List zza = zza(obj, j, zzd.size());
        int size = zza.size();
        int size2 = zzd.size();
        if (size > 0 && size2 > 0) {
            zza.addAll(zzd);
        }
        if (size > 0) {
            zzd = zza;
        }
        zzduy.zza(obj, j, zzd);
    }

    private static <E> List<E> zzd(Object obj, long j) {
        return (List) zzduy.zzp(obj, j);
    }
}
