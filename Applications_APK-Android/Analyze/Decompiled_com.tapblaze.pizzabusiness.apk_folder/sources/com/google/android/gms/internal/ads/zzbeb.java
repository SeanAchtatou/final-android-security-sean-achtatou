package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbeb implements zzafn<zzbdi> {
    private final /* synthetic */ zzbdz zzehd;

    zzbeb(zzbdz zzbdz) {
        this.zzehd = zzbdz;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        if (map != null) {
            String str = (String) map.get("height");
            if (!TextUtils.isEmpty(str)) {
                try {
                    int parseInt = Integer.parseInt(str);
                    synchronized (this.zzehd) {
                        if (this.zzehd.zzegu != parseInt) {
                            int unused = this.zzehd.zzegu = parseInt;
                            this.zzehd.requestLayout();
                        }
                    }
                } catch (Exception e) {
                    zzavs.zzd("Exception occurred while getting webview content height", e);
                }
            }
        }
    }
}
