package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdlj extends zzdrt<zzdlj, zza> implements zzdtg {
    private static volatile zzdtn<zzdlj> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlj zzhaq;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;
    private zzdln zzhap;

    private zzdlj() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdlj, zza> implements zzdtg {
        private zza() {
            super(zzdlj.zzhaq);
        }

        public final zza zzee(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlj) this.zzhmp).setVersion(0);
            return this;
        }

        public final zza zzc(zzdln zzdln) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlj) this.zzhmp).zzb(zzdln);
            return this;
        }

        public final zza zzaa(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlj) this.zzhmp).zzs(zzdqk);
            return this;
        }

        /* synthetic */ zza(zzdli zzdli) {
            this();
        }
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public final zzdln zzath() {
        zzdln zzdln = this.zzhap;
        return zzdln == null ? zzdln.zzato() : zzdln;
    }

    /* access modifiers changed from: private */
    public final void zzb(zzdln zzdln) {
        zzdln.getClass();
        this.zzhap = zzdln;
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public static zzdlj zzy(zzdqk zzdqk) throws zzdse {
        return (zzdlj) zzdrt.zza(zzhaq, zzdqk);
    }

    public static zza zzati() {
        return (zza) zzhaq.zzazt();
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdli.zzdk[i - 1]) {
            case 1:
                return new zzdlj();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhaq, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n", new Object[]{"zzhaa", "zzhap", "zzhab"});
            case 4:
                return zzhaq;
            case 5:
                zzdtn<zzdlj> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlj.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhaq);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdlj zzatj() {
        return zzhaq;
    }

    static {
        zzdlj zzdlj = new zzdlj();
        zzhaq = zzdlj;
        zzdrt.zza(zzdlj.class, zzdlj);
    }
}
