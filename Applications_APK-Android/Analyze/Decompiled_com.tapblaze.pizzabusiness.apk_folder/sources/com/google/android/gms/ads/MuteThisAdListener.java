package com.google.android.gms.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface MuteThisAdListener {
    void onAdMuted();
}
