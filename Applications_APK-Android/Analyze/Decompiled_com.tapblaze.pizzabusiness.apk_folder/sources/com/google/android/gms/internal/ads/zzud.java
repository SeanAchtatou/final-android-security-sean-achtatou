package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.reward.AdMetadataListener;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzud extends zzwa {
    private final AdMetadataListener zzcbx;

    public zzud(AdMetadataListener adMetadataListener) {
        this.zzcbx = adMetadataListener;
    }

    public final void onAdMetadataChanged() {
        AdMetadataListener adMetadataListener = this.zzcbx;
        if (adMetadataListener != null) {
            adMetadataListener.onAdMetadataChanged();
        }
    }
}
