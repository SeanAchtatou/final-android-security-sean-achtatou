package com.google.android.gms.internal.ads;

import com.facebook.appevents.AppEventsConstants;
import com.ironsource.sdk.ISNAdView.ISNAdViewConstants;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbzs implements zzps {
    private final zzbdi zzehp;

    zzbzs(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void zza(zzpt zzpt) {
        zzbdi zzbdi = this.zzehp;
        HashMap hashMap = new HashMap();
        hashMap.put(ISNAdViewConstants.IS_VISIBLE_KEY, zzpt.zzbnq ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        zzbdi.zza("onAdVisibilityChanged", hashMap);
    }
}
