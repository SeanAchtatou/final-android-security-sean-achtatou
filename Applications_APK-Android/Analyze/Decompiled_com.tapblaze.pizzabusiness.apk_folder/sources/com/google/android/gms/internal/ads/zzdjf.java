package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdjf extends zzdik<zzdpi, zzdlj> {
    zzdjf(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        zzdlj zzdlj = (zzdlj) obj;
        return new zzdof(zzdlj.zzass().toByteArray(), zzdlj.zzath().zzatn());
    }
}
