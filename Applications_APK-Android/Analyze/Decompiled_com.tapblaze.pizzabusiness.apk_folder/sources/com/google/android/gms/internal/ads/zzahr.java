package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import java.util.Map;
import javax.annotation.ParametersAreNonnullByDefault;
import org.json.JSONObject;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzahr extends zzaie<zzajq> implements zzaia, zzaif {
    private final zzbfm zzcyo;
    /* access modifiers changed from: private */
    public zzaii zzcyp;

    public zzahr(Context context, zzazb zzazb) throws zzbdv {
        try {
            this.zzcyo = new zzbfm(context, new zzahx(this));
            this.zzcyo.setWillNotDraw(true);
            this.zzcyo.addJavascriptInterface(new zzahy(this), "GoogleJsInterface");
            zzq.zzkq().zza(context, zzazb.zzbma, this.zzcyo.getSettings());
            super.zzg(this);
        } catch (Throwable th) {
            throw new zzbdv("Init failed.", th);
        }
    }

    public final void zza(String str, Map map) {
        zzahz.zza(this, str, map);
    }

    public final void zza(String str, JSONObject jSONObject) {
        zzahz.zza(this, str, jSONObject);
    }

    public final void zzb(String str, JSONObject jSONObject) {
        zzahz.zzb(this, str, jSONObject);
    }

    public final void zzj(String str, String str2) {
        zzahz.zza(this, str, str2);
    }

    public final void zzcv(String str) {
        zzcw(String.format("<!DOCTYPE html><html><head><script src=\"%s\"></script></head></html>", str));
    }

    public final void zzcw(String str) {
        zzazd.zzdwi.execute(new zzahu(this, str));
    }

    public final void zzcx(String str) {
        zzazd.zzdwi.execute(new zzaht(this, str));
    }

    public final void zza(zzaii zzaii) {
        this.zzcyp = zzaii;
    }

    public final void destroy() {
        this.zzcyo.destroy();
    }

    public final boolean isDestroyed() {
        return this.zzcyo.isDestroyed();
    }

    public final zzajp zzrz() {
        return new zzajs(this);
    }

    public final void zzcy(String str) {
        zzazd.zzdwi.execute(new zzahw(this, str));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzcz(String str) {
        this.zzcyo.zzcy(str);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzda(String str) {
        this.zzcyo.loadUrl(str);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzdb(String str) {
        this.zzcyo.loadData(str, "text/html", "UTF-8");
    }
}
