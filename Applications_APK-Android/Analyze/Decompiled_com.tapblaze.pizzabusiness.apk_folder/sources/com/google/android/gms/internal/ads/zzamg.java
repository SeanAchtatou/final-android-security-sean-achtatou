package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.zzb;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzamg<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> extends zzalg {
    private final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> zzded;
    private final NETWORK_EXTRAS zzdee;

    public zzamg(MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter, NETWORK_EXTRAS network_extras) {
        this.zzded = mediationAdapter;
        this.zzdee = network_extras;
    }

    public final zzxb getVideoController() {
        return null;
    }

    public final boolean isInitialized() {
        return true;
    }

    public final void setImmersiveMode(boolean z) {
    }

    public final void showVideo() {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzagp zzagp, List<zzagx> list) throws RemoteException {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzarz zzarz, List<String> list) {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzarz zzarz, String str2) throws RemoteException {
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, String str2, zzali zzali, zzaby zzaby, List<String> list) {
    }

    public final void zza(zzug zzug, String str) {
    }

    public final void zza(zzug zzug, String str, String str2) {
    }

    public final void zzb(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzali zzali) throws RemoteException {
    }

    public final void zzs(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final zzall zzsl() {
        return null;
    }

    public final zzalq zzsm() {
        return null;
    }

    public final boolean zzsp() {
        return false;
    }

    public final zzade zzsq() {
        return null;
    }

    public final zzalr zzsr() {
        return null;
    }

    public final void zzt(IObjectWrapper iObjectWrapper) throws RemoteException {
    }

    public final IObjectWrapper zzsk() throws RemoteException {
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzded;
        if (!(mediationAdapter instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzayu.zzez(valueOf.length() != 0 ? "Not a MediationBannerAdapter: ".concat(valueOf) : new String("Not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        try {
            return ObjectWrapper.wrap(((MediationBannerAdapter) mediationAdapter).getBannerView());
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzuj zzuj, zzug zzug, String str, zzali zzali) throws RemoteException {
        zza(iObjectWrapper, zzuj, zzug, str, null, zzali);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzuj zzuj, zzug zzug, String str, String str2, zzali zzali) throws RemoteException {
        AdSize adSize;
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzded;
        if (!(mediationAdapter instanceof MediationBannerAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzayu.zzez(valueOf.length() != 0 ? "Not a MediationBannerAdapter: ".concat(valueOf) : new String("Not a MediationBannerAdapter: "));
            throw new RemoteException();
        }
        zzayu.zzea("Requesting banner ad from adapter.");
        try {
            MediationBannerAdapter mediationBannerAdapter = (MediationBannerAdapter) this.zzded;
            zzamf zzamf = new zzamf(zzali);
            Activity activity = (Activity) ObjectWrapper.unwrap(iObjectWrapper);
            MediationServerParameters zzdk = zzdk(str);
            int i = 0;
            AdSize[] adSizeArr = {AdSize.SMART_BANNER, AdSize.BANNER, AdSize.IAB_MRECT, AdSize.IAB_BANNER, AdSize.IAB_LEADERBOARD, AdSize.IAB_WIDE_SKYSCRAPER};
            while (true) {
                if (i < 6) {
                    if (adSizeArr[i].getWidth() == zzuj.width && adSizeArr[i].getHeight() == zzuj.height) {
                        adSize = adSizeArr[i];
                        break;
                    }
                    i++;
                } else {
                    adSize = new AdSize(zzb.zza(zzuj.width, zzuj.height, zzuj.zzabg));
                    break;
                }
            }
            mediationBannerAdapter.requestBannerAd(zzamf, activity, zzdk, adSize, zzamr.zza(zzug, zzc(zzug)), this.zzdee);
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    public final Bundle zzsn() {
        return new Bundle();
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzali zzali) throws RemoteException {
        zza(iObjectWrapper, zzug, str, (String) null, zzali);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, String str2, zzali zzali) throws RemoteException {
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzded;
        if (!(mediationAdapter instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzayu.zzez(valueOf.length() != 0 ? "Not a MediationInterstitialAdapter: ".concat(valueOf) : new String("Not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzayu.zzea("Requesting interstitial ad from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzded).requestInterstitialAd(new zzamf(zzali), (Activity) ObjectWrapper.unwrap(iObjectWrapper), zzdk(str), zzamr.zza(zzug, zzc(zzug)), this.zzdee);
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    public final Bundle getInterstitialAdapterInfo() {
        return new Bundle();
    }

    public final void showInterstitial() throws RemoteException {
        MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter = this.zzded;
        if (!(mediationAdapter instanceof MediationInterstitialAdapter)) {
            String valueOf = String.valueOf(mediationAdapter.getClass().getCanonicalName());
            zzayu.zzez(valueOf.length() != 0 ? "Not a MediationInterstitialAdapter: ".concat(valueOf) : new String("Not a MediationInterstitialAdapter: "));
            throw new RemoteException();
        }
        zzayu.zzea("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzded).showInterstitial();
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    public final Bundle zzso() {
        return new Bundle();
    }

    public final void destroy() throws RemoteException {
        try {
            this.zzded.destroy();
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    public final void pause() throws RemoteException {
        throw new RemoteException();
    }

    public final void resume() throws RemoteException {
        throw new RemoteException();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: SERVER_PARAMETERS
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final SERVER_PARAMETERS zzdk(java.lang.String r5) throws android.os.RemoteException {
        /*
            r4 = this;
            if (r5 == 0) goto L_0x0028
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ all -> 0x0042 }
            r0.<init>(r5)     // Catch:{ all -> 0x0042 }
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ all -> 0x0042 }
            int r1 = r0.length()     // Catch:{ all -> 0x0042 }
            r5.<init>(r1)     // Catch:{ all -> 0x0042 }
            java.util.Iterator r1 = r0.keys()     // Catch:{ all -> 0x0042 }
        L_0x0014:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x0042 }
            if (r2 == 0) goto L_0x002e
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x0042 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0042 }
            java.lang.String r3 = r0.getString(r2)     // Catch:{ all -> 0x0042 }
            r5.put(r2, r3)     // Catch:{ all -> 0x0042 }
            goto L_0x0014
        L_0x0028:
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ all -> 0x0042 }
            r0 = 0
            r5.<init>(r0)     // Catch:{ all -> 0x0042 }
        L_0x002e:
            com.google.ads.mediation.MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> r0 = r4.zzded     // Catch:{ all -> 0x0042 }
            java.lang.Class r0 = r0.getServerParametersType()     // Catch:{ all -> 0x0042 }
            r1 = 0
            if (r0 == 0) goto L_0x0041
            java.lang.Object r0 = r0.newInstance()     // Catch:{ all -> 0x0042 }
            r1 = r0
            com.google.ads.mediation.MediationServerParameters r1 = (com.google.ads.mediation.MediationServerParameters) r1     // Catch:{ all -> 0x0042 }
            r1.load(r5)     // Catch:{ all -> 0x0042 }
        L_0x0041:
            return r1
        L_0x0042:
            r5 = move-exception
            java.lang.String r0 = ""
            com.google.android.gms.internal.ads.zzayu.zzc(r0, r5)
            android.os.RemoteException r5 = new android.os.RemoteException
            r5.<init>()
            goto L_0x004f
        L_0x004e:
            throw r5
        L_0x004f:
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzamg.zzdk(java.lang.String):com.google.ads.mediation.MediationServerParameters");
    }

    private static boolean zzc(zzug zzug) {
        if (zzug.zzccb) {
            return true;
        }
        zzve.zzou();
        return zzayk.zzxd();
    }
}
