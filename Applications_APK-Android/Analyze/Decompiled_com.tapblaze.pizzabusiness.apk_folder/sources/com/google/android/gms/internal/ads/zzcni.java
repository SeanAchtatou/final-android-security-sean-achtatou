package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcni implements zzdcb {
    private final zzcng zzgbo;
    private final zzaad zzgbp;

    zzcni(zzcng zzcng, zzaad zzaad) {
        this.zzgbo = zzcng;
        this.zzgbp = zzaad;
    }

    public final void run() {
        this.zzgbo.zza(this.zzgbp);
    }
}
