package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.drive.DriveFile;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import cz.msebera.android.httpclient.message.TokenParser;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgp  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzgp<T> implements zzhb<T> {
    private static final int[] zzst = new int[0];
    private static final Unsafe zzsu = zzhz.zzji();
    private final int[] zzsv;
    private final Object[] zzsw;
    private final int zzsx;
    private final int zzsy;
    private final zzgl zzsz;
    private final boolean zzta;
    private final boolean zztb;
    private final boolean zztc;
    private final boolean zztd;
    private final int[] zzte;
    private final int zztf;
    private final int zztg;
    private final zzgu zzth;
    private final zzfv zzti;
    private final zzht<?, ?> zztj;
    private final zzes<?> zztk;
    private final zzgi zztl;

    private zzgp(int[] iArr, Object[] objArr, int i, int i2, zzgl zzgl, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzgu zzgu, zzfv zzfv, zzht<?, ?> zzht, zzes<?> zzes, zzgi zzgi) {
        this.zzsv = iArr;
        this.zzsw = objArr;
        this.zzsx = i;
        this.zzsy = i2;
        this.zztb = zzgl instanceof zzfc;
        this.zztc = z;
        this.zzta = zzes != null && zzes.zze(zzgl);
        this.zztd = false;
        this.zzte = iArr2;
        this.zztf = i3;
        this.zztg = i4;
        this.zzth = zzgu;
        this.zzti = zzfv;
        this.zztj = zzht;
        this.zztk = zzes;
        this.zzsz = zzgl;
        this.zztl = zzgi;
    }

    static <T> zzgp<T> zza(Class<T> cls, zzgj zzgj, zzgu zzgu, zzfv zzfv, zzht<?, ?> zzht, zzes<?> zzes, zzgi zzgi) {
        int i;
        int i2;
        char c;
        int[] iArr;
        char c2;
        char c3;
        int i3;
        char c4;
        char c5;
        int i4;
        int i5;
        String str;
        char c6;
        int i6;
        char c7;
        int i7;
        int i8;
        int i9;
        int i10;
        Class<?> cls2;
        int i11;
        int i12;
        Field field;
        int i13;
        char charAt;
        int i14;
        char c8;
        Field field2;
        Field field3;
        int i15;
        char charAt2;
        int i16;
        char charAt3;
        int i17;
        char charAt4;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        char charAt5;
        int i24;
        char charAt6;
        int i25;
        char charAt7;
        int i26;
        char charAt8;
        char charAt9;
        char charAt10;
        char charAt11;
        char charAt12;
        char charAt13;
        char charAt14;
        zzgj zzgj2 = zzgj;
        if (zzgj2 instanceof zzhc) {
            zzhc zzhc = (zzhc) zzgj2;
            char c9 = 0;
            boolean z = zzhc.zzie() == zzgx.zztp;
            String zzip = zzhc.zzip();
            int length = zzip.length();
            char charAt15 = zzip.charAt(0);
            if (charAt15 >= 55296) {
                char c10 = charAt15 & 8191;
                int i27 = 1;
                int i28 = 13;
                while (true) {
                    i = i27 + 1;
                    charAt14 = zzip.charAt(i27);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c10 |= (charAt14 & 8191) << i28;
                    i28 += 13;
                    i27 = i;
                }
                charAt15 = (charAt14 << i28) | c10;
            } else {
                i = 1;
            }
            int i29 = i + 1;
            char charAt16 = zzip.charAt(i);
            if (charAt16 >= 55296) {
                char c11 = charAt16 & 8191;
                int i30 = 13;
                while (true) {
                    i2 = i29 + 1;
                    charAt13 = zzip.charAt(i29);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c11 |= (charAt13 & 8191) << i30;
                    i30 += 13;
                    i29 = i2;
                }
                charAt16 = c11 | (charAt13 << i30);
            } else {
                i2 = i29;
            }
            if (charAt16 == 0) {
                iArr = zzst;
                c5 = 0;
                c4 = 0;
                i3 = 0;
                c3 = 0;
                c2 = 0;
                c = 0;
            } else {
                int i31 = i2 + 1;
                char charAt17 = zzip.charAt(i2);
                if (charAt17 >= 55296) {
                    char c12 = charAt17 & 8191;
                    int i32 = 13;
                    while (true) {
                        i18 = i31 + 1;
                        charAt12 = zzip.charAt(i31);
                        if (charAt12 < 55296) {
                            break;
                        }
                        c12 |= (charAt12 & 8191) << i32;
                        i32 += 13;
                        i31 = i18;
                    }
                    charAt17 = (charAt12 << i32) | c12;
                } else {
                    i18 = i31;
                }
                int i33 = i18 + 1;
                char charAt18 = zzip.charAt(i18);
                if (charAt18 >= 55296) {
                    char c13 = charAt18 & 8191;
                    int i34 = 13;
                    while (true) {
                        i19 = i33 + 1;
                        charAt11 = zzip.charAt(i33);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c13 |= (charAt11 & 8191) << i34;
                        i34 += 13;
                        i33 = i19;
                    }
                    charAt18 = c13 | (charAt11 << i34);
                } else {
                    i19 = i33;
                }
                int i35 = i19 + 1;
                char charAt19 = zzip.charAt(i19);
                if (charAt19 >= 55296) {
                    char c14 = charAt19 & 8191;
                    int i36 = 13;
                    while (true) {
                        i20 = i35 + 1;
                        charAt10 = zzip.charAt(i35);
                        if (charAt10 < 55296) {
                            break;
                        }
                        c14 |= (charAt10 & 8191) << i36;
                        i36 += 13;
                        i35 = i20;
                    }
                    charAt19 = (charAt10 << i36) | c14;
                } else {
                    i20 = i35;
                }
                int i37 = i20 + 1;
                c3 = zzip.charAt(i20);
                if (c3 >= 55296) {
                    char c15 = c3 & 8191;
                    int i38 = 13;
                    while (true) {
                        i21 = i37 + 1;
                        charAt9 = zzip.charAt(i37);
                        if (charAt9 < 55296) {
                            break;
                        }
                        c15 |= (charAt9 & 8191) << i38;
                        i38 += 13;
                        i37 = i21;
                    }
                    c3 = (charAt9 << i38) | c15;
                } else {
                    i21 = i37;
                }
                int i39 = i21 + 1;
                c2 = zzip.charAt(i21);
                if (c2 >= 55296) {
                    char c16 = c2 & 8191;
                    int i40 = 13;
                    while (true) {
                        i26 = i39 + 1;
                        charAt8 = zzip.charAt(i39);
                        if (charAt8 < 55296) {
                            break;
                        }
                        c16 |= (charAt8 & 8191) << i40;
                        i40 += 13;
                        i39 = i26;
                    }
                    c2 = (charAt8 << i40) | c16;
                    i39 = i26;
                }
                int i41 = i39 + 1;
                c5 = zzip.charAt(i39);
                if (c5 >= 55296) {
                    char c17 = c5 & 8191;
                    int i42 = 13;
                    while (true) {
                        i25 = i41 + 1;
                        charAt7 = zzip.charAt(i41);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c17 |= (charAt7 & 8191) << i42;
                        i42 += 13;
                        i41 = i25;
                    }
                    c5 = c17 | (charAt7 << i42);
                    i41 = i25;
                }
                int i43 = i41 + 1;
                char charAt20 = zzip.charAt(i41);
                if (charAt20 >= 55296) {
                    int i44 = 13;
                    int i45 = i43;
                    char c18 = charAt20 & 8191;
                    int i46 = i45;
                    while (true) {
                        i24 = i46 + 1;
                        charAt6 = zzip.charAt(i46);
                        if (charAt6 < 55296) {
                            break;
                        }
                        c18 |= (charAt6 & 8191) << i44;
                        i44 += 13;
                        i46 = i24;
                    }
                    charAt20 = c18 | (charAt6 << i44);
                    i22 = i24;
                } else {
                    i22 = i43;
                }
                int i47 = i22 + 1;
                c9 = zzip.charAt(i22);
                if (c9 >= 55296) {
                    int i48 = 13;
                    int i49 = i47;
                    char c19 = c9 & 8191;
                    int i50 = i49;
                    while (true) {
                        i23 = i50 + 1;
                        charAt5 = zzip.charAt(i50);
                        if (charAt5 < 55296) {
                            break;
                        }
                        c19 |= (charAt5 & 8191) << i48;
                        i48 += 13;
                        i50 = i23;
                    }
                    c9 = c19 | (charAt5 << i48);
                    i47 = i23;
                }
                iArr = new int[(c9 + c5 + charAt20)];
                i3 = (charAt17 << 1) + charAt18;
                int i51 = i47;
                c = charAt17;
                c4 = charAt19;
                i2 = i51;
            }
            Unsafe unsafe = zzsu;
            Object[] zziq = zzhc.zziq();
            Class<?> cls3 = zzhc.zzig().getClass();
            int i52 = i3;
            int[] iArr2 = new int[(c2 * 3)];
            Object[] objArr = new Object[(c2 << 1)];
            int i53 = c9 + c5;
            char c20 = c9;
            int i54 = i53;
            int i55 = 0;
            int i56 = 0;
            while (i2 < length) {
                int i57 = i2 + 1;
                char charAt21 = zzip.charAt(i2);
                char c21 = 55296;
                if (charAt21 >= 55296) {
                    int i58 = 13;
                    int i59 = i57;
                    char c22 = charAt21 & 8191;
                    int i60 = i59;
                    while (true) {
                        i17 = i60 + 1;
                        charAt4 = zzip.charAt(i60);
                        if (charAt4 < c21) {
                            break;
                        }
                        c22 |= (charAt4 & 8191) << i58;
                        i58 += 13;
                        i60 = i17;
                        c21 = 55296;
                    }
                    charAt21 = c22 | (charAt4 << i58);
                    i4 = i17;
                } else {
                    i4 = i57;
                }
                int i61 = i4 + 1;
                char charAt22 = zzip.charAt(i4);
                int i62 = length;
                char c23 = 55296;
                if (charAt22 >= 55296) {
                    int i63 = 13;
                    int i64 = i61;
                    char c24 = charAt22 & 8191;
                    int i65 = i64;
                    while (true) {
                        i16 = i65 + 1;
                        charAt3 = zzip.charAt(i65);
                        if (charAt3 < c23) {
                            break;
                        }
                        c24 |= (charAt3 & 8191) << i63;
                        i63 += 13;
                        i65 = i16;
                        c23 = 55296;
                    }
                    charAt22 = c24 | (charAt3 << i63);
                    i5 = i16;
                } else {
                    i5 = i61;
                }
                char c25 = c9;
                char c26 = charAt22 & 255;
                boolean z2 = z;
                if ((charAt22 & 1024) != 0) {
                    iArr[i55] = i56;
                    i55++;
                }
                int i66 = i55;
                if (c26 >= '3') {
                    int i67 = i5 + 1;
                    char charAt23 = zzip.charAt(i5);
                    char c27 = 55296;
                    if (charAt23 >= 55296) {
                        char c28 = charAt23 & 8191;
                        int i68 = 13;
                        while (true) {
                            i15 = i67 + 1;
                            charAt2 = zzip.charAt(i67);
                            if (charAt2 < c27) {
                                break;
                            }
                            c28 |= (charAt2 & 8191) << i68;
                            i68 += 13;
                            i67 = i15;
                            c27 = 55296;
                        }
                        charAt23 = c28 | (charAt2 << i68);
                        i67 = i15;
                    }
                    int i69 = c26 - '3';
                    int i70 = i67;
                    if (i69 == 9 || i69 == 17) {
                        c8 = 1;
                        objArr[((i56 / 3) << 1) + 1] = zziq[i52];
                        i52++;
                    } else {
                        if (i69 == 12 && (charAt15 & 1) == 1) {
                            objArr[((i56 / 3) << 1) + 1] = zziq[i52];
                            i52++;
                        }
                        c8 = 1;
                    }
                    int i71 = charAt23 << c8;
                    Object obj = zziq[i71];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = zza(cls3, (String) obj);
                        zziq[i71] = field2;
                    }
                    char c29 = c4;
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i72 = i71 + 1;
                    Object obj2 = zziq[i72];
                    int i73 = objectFieldOffset;
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = zza(cls3, (String) obj2);
                        zziq[i72] = field3;
                    }
                    str = zzip;
                    i9 = (int) unsafe.objectFieldOffset(field3);
                    cls2 = cls3;
                    i6 = i52;
                    i8 = i73;
                    i10 = 0;
                    c6 = c29;
                    c7 = c3;
                    i7 = charAt21;
                    i12 = i70;
                } else {
                    char c30 = c4;
                    int i74 = i52 + 1;
                    Field zza = zza(cls3, (String) zziq[i52]);
                    c7 = c3;
                    if (c26 == 9 || c26 == 17) {
                        c6 = c30;
                        objArr[((i56 / 3) << 1) + 1] = zza.getType();
                    } else {
                        if (c26 == 27 || c26 == '1') {
                            c6 = c30;
                            i14 = i74 + 1;
                            objArr[((i56 / 3) << 1) + 1] = zziq[i74];
                        } else if (c26 == 12 || c26 == 30 || c26 == ',') {
                            c6 = c30;
                            if ((charAt15 & 1) == 1) {
                                i14 = i74 + 1;
                                objArr[((i56 / 3) << 1) + 1] = zziq[i74];
                            }
                        } else if (c26 == '2') {
                            int i75 = c20 + 1;
                            iArr[c20] = i56;
                            int i76 = (i56 / 3) << 1;
                            int i77 = i74 + 1;
                            objArr[i76] = zziq[i74];
                            if ((charAt22 & 2048) != 0) {
                                i74 = i77 + 1;
                                objArr[i76 + 1] = zziq[i77];
                                c6 = c30;
                                c20 = i75;
                            } else {
                                c20 = i75;
                                i74 = i77;
                                c6 = c30;
                            }
                        } else {
                            c6 = c30;
                        }
                        i7 = charAt21;
                        i74 = i14;
                        i8 = (int) unsafe.objectFieldOffset(zza);
                        if ((charAt15 & 1) == 1 || c26 > 17) {
                            str = zzip;
                            cls2 = cls3;
                            i6 = i74;
                            i11 = i5;
                            i10 = 0;
                            i9 = 0;
                        } else {
                            i11 = i5 + 1;
                            char charAt24 = zzip.charAt(i5);
                            if (charAt24 >= 55296) {
                                char c31 = charAt24 & 8191;
                                int i78 = 13;
                                while (true) {
                                    i13 = i11 + 1;
                                    charAt = zzip.charAt(i11);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    c31 |= (charAt & 8191) << i78;
                                    i78 += 13;
                                    i11 = i13;
                                }
                                charAt24 = c31 | (charAt << i78);
                                i11 = i13;
                            }
                            int i79 = (c << 1) + (charAt24 / TokenParser.SP);
                            Object obj3 = zziq[i79];
                            str = zzip;
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = zza(cls3, (String) obj3);
                                zziq[i79] = field;
                            }
                            cls2 = cls3;
                            i6 = i74;
                            i9 = (int) unsafe.objectFieldOffset(field);
                            i10 = charAt24 % TokenParser.SP;
                        }
                        if (c26 >= 18 && c26 <= '1') {
                            iArr[i54] = i8;
                            i54++;
                        }
                        i12 = i11;
                    }
                    i7 = charAt21;
                    i8 = (int) unsafe.objectFieldOffset(zza);
                    if ((charAt15 & 1) == 1) {
                    }
                    str = zzip;
                    cls2 = cls3;
                    i6 = i74;
                    i11 = i5;
                    i10 = 0;
                    i9 = 0;
                    iArr[i54] = i8;
                    i54++;
                    i12 = i11;
                }
                int i80 = i56 + 1;
                iArr2[i56] = i7;
                int i81 = i80 + 1;
                iArr2[i80] = (c26 << 20) | ((charAt22 & 256) != 0 ? DriveFile.MODE_READ_ONLY : 0) | ((charAt22 & 512) != 0 ? DriveFile.MODE_WRITE_ONLY : 0) | i8;
                i56 = i81 + 1;
                iArr2[i81] = (i10 << 20) | i9;
                cls3 = cls2;
                c3 = c7;
                c9 = c25;
                i52 = i6;
                length = i62;
                z = z2;
                c4 = c6;
                i55 = i66;
                zzip = str;
            }
            return new zzgp(iArr2, objArr, c4, c3, zzhc.zzig(), z, false, iArr, c9, i53, zzgu, zzfv, zzht, zzes, zzgi);
        }
        int zzie = ((zzhq) zzgj2).zzie();
        int i82 = zzgx.zztp;
        throw new NoSuchMethodError();
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhd.zze(com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6), com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhd.zze(com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6), com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhd.zze(com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6), com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhd.zze(com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6), com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzl(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzl(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6) == com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.p000firebaseperf.zzhz.zzm(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.p000firebaseperf.zzhz.zzm(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.p000firebaseperf.zzhz.zzn(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.p000firebaseperf.zzhz.zzn(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.p000firebaseperf.zzhd.zze(com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6), com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)) != false) goto L_0x01c2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(T r10, T r11) {
        /*
            r9 = this;
            int[] r0 = r9.zzsv
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            r3 = 1
            if (r2 >= r0) goto L_0x01c9
            int r4 = r9.zzar(r2)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r4 & r5
            long r6 = (long) r6
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r8
            int r4 = r4 >>> 20
            switch(r4) {
                case 0: goto L_0x01a7;
                case 1: goto L_0x018e;
                case 2: goto L_0x017b;
                case 3: goto L_0x0168;
                case 4: goto L_0x0157;
                case 5: goto L_0x0144;
                case 6: goto L_0x0132;
                case 7: goto L_0x0120;
                case 8: goto L_0x010a;
                case 9: goto L_0x00f4;
                case 10: goto L_0x00de;
                case 11: goto L_0x00cc;
                case 12: goto L_0x00ba;
                case 13: goto L_0x00a8;
                case 14: goto L_0x0094;
                case 15: goto L_0x0082;
                case 16: goto L_0x006e;
                case 17: goto L_0x0058;
                case 18: goto L_0x004a;
                case 19: goto L_0x004a;
                case 20: goto L_0x004a;
                case 21: goto L_0x004a;
                case 22: goto L_0x004a;
                case 23: goto L_0x004a;
                case 24: goto L_0x004a;
                case 25: goto L_0x004a;
                case 26: goto L_0x004a;
                case 27: goto L_0x004a;
                case 28: goto L_0x004a;
                case 29: goto L_0x004a;
                case 30: goto L_0x004a;
                case 31: goto L_0x004a;
                case 32: goto L_0x004a;
                case 33: goto L_0x004a;
                case 34: goto L_0x004a;
                case 35: goto L_0x004a;
                case 36: goto L_0x004a;
                case 37: goto L_0x004a;
                case 38: goto L_0x004a;
                case 39: goto L_0x004a;
                case 40: goto L_0x004a;
                case 41: goto L_0x004a;
                case 42: goto L_0x004a;
                case 43: goto L_0x004a;
                case 44: goto L_0x004a;
                case 45: goto L_0x004a;
                case 46: goto L_0x004a;
                case 47: goto L_0x004a;
                case 48: goto L_0x004a;
                case 49: goto L_0x004a;
                case 50: goto L_0x003c;
                case 51: goto L_0x001c;
                case 52: goto L_0x001c;
                case 53: goto L_0x001c;
                case 54: goto L_0x001c;
                case 55: goto L_0x001c;
                case 56: goto L_0x001c;
                case 57: goto L_0x001c;
                case 58: goto L_0x001c;
                case 59: goto L_0x001c;
                case 60: goto L_0x001c;
                case 61: goto L_0x001c;
                case 62: goto L_0x001c;
                case 63: goto L_0x001c;
                case 64: goto L_0x001c;
                case 65: goto L_0x001c;
                case 66: goto L_0x001c;
                case 67: goto L_0x001c;
                case 68: goto L_0x001c;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x01c2
        L_0x001c:
            int r4 = r9.zzas(r2)
            r4 = r4 & r5
            long r4 = (long) r4
            int r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r4)
            int r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r4)
            if (r8 != r4) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)
            boolean r4 = com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x003c:
            java.lang.Object r3 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)
            boolean r3 = com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r3, r4)
            goto L_0x01c2
        L_0x004a:
            java.lang.Object r3 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)
            boolean r3 = com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r3, r4)
            goto L_0x01c2
        L_0x0058:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)
            boolean r4 = com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x006e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6)
            long r6 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0082:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6)
            int r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0094:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6)
            long r6 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00a8:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6)
            int r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00ba:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6)
            int r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00cc:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6)
            int r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00de:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)
            boolean r4 = com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00f4:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)
            boolean r4 = com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x010a:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r11, r6)
            boolean r4 = com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0120:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            boolean r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzl(r10, r6)
            boolean r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzl(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0132:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6)
            int r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0144:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6)
            long r6 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0157:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r10, r6)
            int r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0168:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6)
            long r6 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x017b:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r10, r6)
            long r6 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x018e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            float r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzm(r10, r6)
            int r4 = java.lang.Float.floatToIntBits(r4)
            float r5 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzm(r11, r6)
            int r5 = java.lang.Float.floatToIntBits(r5)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x01a7:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            double r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzn(r10, r6)
            long r4 = java.lang.Double.doubleToLongBits(r4)
            double r6 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzn(r11, r6)
            long r6 = java.lang.Double.doubleToLongBits(r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
        L_0x01c1:
            r3 = 0
        L_0x01c2:
            if (r3 != 0) goto L_0x01c5
            return r1
        L_0x01c5:
            int r2 = r2 + 3
            goto L_0x0005
        L_0x01c9:
            com.google.android.gms.internal.firebase-perf.zzht<?, ?> r0 = r9.zztj
            java.lang.Object r0 = r0.zzp(r10)
            com.google.android.gms.internal.firebase-perf.zzht<?, ?> r2 = r9.zztj
            java.lang.Object r2 = r2.zzp(r11)
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x01dc
            return r1
        L_0x01dc:
            boolean r0 = r9.zzta
            if (r0 == 0) goto L_0x01f1
            com.google.android.gms.internal.firebase-perf.zzes<?> r0 = r9.zztk
            com.google.android.gms.internal.firebase-perf.zzex r10 = r0.zzd(r10)
            com.google.android.gms.internal.firebase-perf.zzes<?> r0 = r9.zztk
            com.google.android.gms.internal.firebase-perf.zzex r11 = r0.zzd(r11)
            boolean r10 = r10.equals(r11)
            return r10
        L_0x01f1:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzgp.equals(java.lang.Object, java.lang.Object):boolean");
    }

    public final int hashCode(T t) {
        int i;
        int i2;
        int length = this.zzsv.length;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4 += 3) {
            int zzar = zzar(i4);
            int i5 = this.zzsv[i4];
            long j = (long) (1048575 & zzar);
            int i6 = 37;
            switch ((zzar & 267386880) >>> 20) {
                case 0:
                    i2 = i3 * 53;
                    i = zzfg.zzaz(Double.doubleToLongBits(zzhz.zzn(t, j)));
                    i3 = i2 + i;
                    break;
                case 1:
                    i2 = i3 * 53;
                    i = Float.floatToIntBits(zzhz.zzm(t, j));
                    i3 = i2 + i;
                    break;
                case 2:
                    i2 = i3 * 53;
                    i = zzfg.zzaz(zzhz.zzk(t, j));
                    i3 = i2 + i;
                    break;
                case 3:
                    i2 = i3 * 53;
                    i = zzfg.zzaz(zzhz.zzk(t, j));
                    i3 = i2 + i;
                    break;
                case 4:
                    i2 = i3 * 53;
                    i = zzhz.zzj(t, j);
                    i3 = i2 + i;
                    break;
                case 5:
                    i2 = i3 * 53;
                    i = zzfg.zzaz(zzhz.zzk(t, j));
                    i3 = i2 + i;
                    break;
                case 6:
                    i2 = i3 * 53;
                    i = zzhz.zzj(t, j);
                    i3 = i2 + i;
                    break;
                case 7:
                    i2 = i3 * 53;
                    i = zzfg.zzh(zzhz.zzl(t, j));
                    i3 = i2 + i;
                    break;
                case 8:
                    i2 = i3 * 53;
                    i = ((String) zzhz.zzo(t, j)).hashCode();
                    i3 = i2 + i;
                    break;
                case 9:
                    Object zzo = zzhz.zzo(t, j);
                    if (zzo != null) {
                        i6 = zzo.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 10:
                    i2 = i3 * 53;
                    i = zzhz.zzo(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 11:
                    i2 = i3 * 53;
                    i = zzhz.zzj(t, j);
                    i3 = i2 + i;
                    break;
                case 12:
                    i2 = i3 * 53;
                    i = zzhz.zzj(t, j);
                    i3 = i2 + i;
                    break;
                case 13:
                    i2 = i3 * 53;
                    i = zzhz.zzj(t, j);
                    i3 = i2 + i;
                    break;
                case 14:
                    i2 = i3 * 53;
                    i = zzfg.zzaz(zzhz.zzk(t, j));
                    i3 = i2 + i;
                    break;
                case 15:
                    i2 = i3 * 53;
                    i = zzhz.zzj(t, j);
                    i3 = i2 + i;
                    break;
                case 16:
                    i2 = i3 * 53;
                    i = zzfg.zzaz(zzhz.zzk(t, j));
                    i3 = i2 + i;
                    break;
                case 17:
                    Object zzo2 = zzhz.zzo(t, j);
                    if (zzo2 != null) {
                        i6 = zzo2.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i2 = i3 * 53;
                    i = zzhz.zzo(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 50:
                    i2 = i3 * 53;
                    i = zzhz.zzo(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 51:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfg.zzaz(Double.doubleToLongBits(zze(t, j)));
                        i3 = i2 + i;
                        break;
                    }
                case 52:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = Float.floatToIntBits(zzf(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 53:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfg.zzaz(zzh(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 54:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfg.zzaz(zzh(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 55:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzg(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 56:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfg.zzaz(zzh(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 57:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzg(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 58:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfg.zzh(zzi(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 59:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = ((String) zzhz.zzo(t, j)).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 60:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzhz.zzo(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 61:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzhz.zzo(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 62:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzg(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 63:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzg(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 64:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzg(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 65:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfg.zzaz(zzh(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 66:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzg(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 67:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfg.zzaz(zzh(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 68:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzhz.zzo(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
            }
        }
        int hashCode = (i3 * 53) + this.zztj.zzp(t).hashCode();
        return this.zzta ? (hashCode * 53) + this.zztk.zzd(t).hashCode() : hashCode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, long):void
     arg types: [T, long, long]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(byte[], long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(byte[], long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(byte[], long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(byte[], long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, float):void */
    public final void zzd(T t, T t2) {
        if (t2 != null) {
            for (int i = 0; i < this.zzsv.length; i += 3) {
                int zzar = zzar(i);
                long j = (long) (1048575 & zzar);
                int i2 = this.zzsv[i];
                switch ((zzar & 267386880) >>> 20) {
                    case 0:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza(t, j, zzhz.zzn(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 1:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzm(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 2:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 3:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 4:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzj(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 5:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 6:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzj(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 7:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzl(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 8:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza(t, j, zzhz.zzo(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 9:
                        zza(t, t2, i);
                        break;
                    case 10:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza(t, j, zzhz.zzo(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 11:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzj(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 12:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzj(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 13:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzj(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 14:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 15:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzj(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 16:
                        if (!zza(t2, i)) {
                            break;
                        } else {
                            zzhz.zza((Object) t, j, zzhz.zzk(t2, j));
                            zzb(t, i);
                            break;
                        }
                    case 17:
                        zza(t, t2, i);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.zzti.zza(t, t2, j);
                        break;
                    case 50:
                        zzhd.zza(this.zztl, t, t2, j);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzhz.zza(t, j, zzhz.zzo(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 60:
                        zzb(t, t2, i);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzhz.zza(t, j, zzhz.zzo(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 68:
                        zzb(t, t2, i);
                        break;
                }
            }
            zzhd.zza(this.zztj, t, t2);
            if (this.zzta) {
                zzhd.zza(this.zztk, t, t2);
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    private final void zza(T t, T t2, int i) {
        long zzar = (long) (zzar(i) & 1048575);
        if (zza(t2, i)) {
            Object zzo = zzhz.zzo(t, zzar);
            Object zzo2 = zzhz.zzo(t2, zzar);
            if (zzo != null && zzo2 != null) {
                zzhz.zza(t, zzar, zzfg.zzb(zzo, zzo2));
                zzb(t, i);
            } else if (zzo2 != null) {
                zzhz.zza(t, zzar, zzo2);
                zzb(t, i);
            }
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzar = zzar(i);
        int i2 = this.zzsv[i];
        long j = (long) (zzar & 1048575);
        if (zza(t2, i2, i)) {
            Object zzo = zzhz.zzo(t, j);
            Object zzo2 = zzhz.zzo(t2, j);
            if (zzo != null && zzo2 != null) {
                zzhz.zza(t, j, zzfg.zzb(zzo, zzo2));
                zzb(t, i2, i);
            } else if (zzo2 != null) {
                zzhz.zza(t, j, zzo2);
                zzb(t, i2, i);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzeo.zzh(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzeo.zzh(int, int):void
      com.google.android.gms.internal.firebase-perf.zzeo.zzh(int, long):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, boolean):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, double):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, float):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, com.google.android.gms.internal.firebase-perf.zzfu):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, com.google.android.gms.internal.firebase-perf.zzgl):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, java.lang.String):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, long):void
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, com.google.android.gms.internal.firebase-perf.zzeb):void
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzeo.zzg(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzeo.zzg(int, int):void
      com.google.android.gms.internal.firebase-perf.zzeo.zzg(int, long):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, float):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, double):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, com.google.android.gms.internal.firebase-perf.zzfu):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, com.google.android.gms.internal.firebase-perf.zzgl):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, java.lang.String):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, boolean):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, long):void
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, com.google.android.gms.internal.firebase-perf.zzeb):void
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, float):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzgp.zza(com.google.android.gms.internal.firebase-perf.zzht, java.lang.Object):int
     arg types: [com.google.android.gms.internal.firebase-perf.zzht<?, ?>, T]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzgp.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.firebase-perf.zzgp.zza(java.lang.Object, int):boolean
      com.google.android.gms.internal.firebase-perf.zzgp.zza(java.lang.Object, com.google.android.gms.internal.firebase-perf.zzin):void
      com.google.android.gms.internal.firebase-perf.zzhb.zza(java.lang.Object, com.google.android.gms.internal.firebase-perf.zzin):void
      com.google.android.gms.internal.firebase-perf.zzgp.zza(com.google.android.gms.internal.firebase-perf.zzht, java.lang.Object):int */
    public final int zzm(T t) {
        int i;
        int i2;
        long j;
        int i3;
        int zzb;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int zzb2;
        int i9;
        int i10;
        int i11;
        T t2 = t;
        int i12 = 267386880;
        int i13 = 1048575;
        int i14 = 1;
        if (this.zztc) {
            Unsafe unsafe = zzsu;
            int i15 = 0;
            int i16 = 0;
            while (i15 < this.zzsv.length) {
                int zzar = zzar(i15);
                int i17 = (zzar & i12) >>> 20;
                int i18 = this.zzsv[i15];
                long j2 = (long) (zzar & 1048575);
                int i19 = (i17 < zzey.DOUBLE_LIST_PACKED.id() || i17 > zzey.SINT64_LIST_PACKED.id()) ? 0 : this.zzsv[i15 + 2] & 1048575;
                switch (i17) {
                    case 0:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzb(i18, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 1:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzb(i18, 0.0f);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 2:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzd(i18, zzhz.zzk(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 3:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zze(i18, zzhz.zzk(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 4:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzj(i18, zzhz.zzj(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 5:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzg(i18, 0L);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 6:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzm(i18, 0);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 7:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzb(i18, true);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 8:
                        if (zza(t2, i15)) {
                            Object zzo = zzhz.zzo(t2, j2);
                            if (!(zzo instanceof zzeb)) {
                                zzb2 = zzeo.zzb(i18, (String) zzo);
                                break;
                            } else {
                                zzb2 = zzeo.zzc(i18, (zzeb) zzo);
                                break;
                            }
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 9:
                        if (zza(t2, i15)) {
                            zzb2 = zzhd.zzc(i18, zzhz.zzo(t2, j2), zzap(i15));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 10:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzc(i18, (zzeb) zzhz.zzo(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 11:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzk(i18, zzhz.zzj(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 12:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzo(i18, zzhz.zzj(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 13:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzn(i18, 0);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 14:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzh(i18, 0L);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 15:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzl(i18, zzhz.zzj(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 16:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzf(i18, zzhz.zzk(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 17:
                        if (zza(t2, i15)) {
                            zzb2 = zzeo.zzc(i18, (zzgl) zzhz.zzo(t2, j2), zzap(i15));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 18:
                        zzb2 = zzhd.zzw(i18, zzd(t2, j2), false);
                        break;
                    case 19:
                        zzb2 = zzhd.zzv(i18, zzd(t2, j2), false);
                        break;
                    case 20:
                        zzb2 = zzhd.zzo(i18, zzd(t2, j2), false);
                        break;
                    case 21:
                        zzb2 = zzhd.zzp(i18, zzd(t2, j2), false);
                        break;
                    case 22:
                        zzb2 = zzhd.zzs(i18, zzd(t2, j2), false);
                        break;
                    case 23:
                        zzb2 = zzhd.zzw(i18, zzd(t2, j2), false);
                        break;
                    case 24:
                        zzb2 = zzhd.zzv(i18, zzd(t2, j2), false);
                        break;
                    case 25:
                        zzb2 = zzhd.zzx(i18, zzd(t2, j2), false);
                        break;
                    case 26:
                        zzb2 = zzhd.zzc(i18, zzd(t2, j2));
                        break;
                    case 27:
                        zzb2 = zzhd.zzc(i18, zzd(t2, j2), zzap(i15));
                        break;
                    case 28:
                        zzb2 = zzhd.zzd(i18, zzd(t2, j2));
                        break;
                    case 29:
                        zzb2 = zzhd.zzt(i18, zzd(t2, j2), false);
                        break;
                    case 30:
                        zzb2 = zzhd.zzr(i18, zzd(t2, j2), false);
                        break;
                    case 31:
                        zzb2 = zzhd.zzv(i18, zzd(t2, j2), false);
                        break;
                    case 32:
                        zzb2 = zzhd.zzw(i18, zzd(t2, j2), false);
                        break;
                    case 33:
                        zzb2 = zzhd.zzu(i18, zzd(t2, j2), false);
                        break;
                    case 34:
                        zzb2 = zzhd.zzq(i18, zzd(t2, j2), false);
                        break;
                    case 35:
                        i10 = zzhd.zzk((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 36:
                        i10 = zzhd.zzj((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 37:
                        i10 = zzhd.zzc((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 38:
                        i10 = zzhd.zzd((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 39:
                        i10 = zzhd.zzg((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 40:
                        i10 = zzhd.zzk((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 41:
                        i10 = zzhd.zzj((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 42:
                        i10 = zzhd.zzl((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 43:
                        i10 = zzhd.zzh((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 44:
                        i10 = zzhd.zzf((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 45:
                        i10 = zzhd.zzj((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 46:
                        i10 = zzhd.zzk((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 47:
                        i10 = zzhd.zzi((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 48:
                        i10 = zzhd.zze((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zztd) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzeo.zzy(i18);
                            i9 = zzeo.zzaa(i10);
                            zzb2 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 49:
                        zzb2 = zzhd.zzd(i18, zzd(t2, j2), zzap(i15));
                        break;
                    case 50:
                        zzb2 = this.zztl.zzb(i18, zzhz.zzo(t2, j2), zzaq(i15));
                        break;
                    case 51:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzb(i18, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 52:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzb(i18, 0.0f);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 53:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzd(i18, zzh(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 54:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zze(i18, zzh(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 55:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzj(i18, zzg(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 56:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzg(i18, 0L);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 57:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzm(i18, 0);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 58:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzb(i18, true);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 59:
                        if (zza(t2, i18, i15)) {
                            Object zzo2 = zzhz.zzo(t2, j2);
                            if (!(zzo2 instanceof zzeb)) {
                                zzb2 = zzeo.zzb(i18, (String) zzo2);
                                break;
                            } else {
                                zzb2 = zzeo.zzc(i18, (zzeb) zzo2);
                                break;
                            }
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 60:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzhd.zzc(i18, zzhz.zzo(t2, j2), zzap(i15));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 61:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzc(i18, (zzeb) zzhz.zzo(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 62:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzk(i18, zzg(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 63:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzo(i18, zzg(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 64:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzn(i18, 0);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 65:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzh(i18, 0L);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 66:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzl(i18, zzg(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 67:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzf(i18, zzh(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 68:
                        if (zza(t2, i18, i15)) {
                            zzb2 = zzeo.zzc(i18, (zzgl) zzhz.zzo(t2, j2), zzap(i15));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    default:
                        i15 += 3;
                        i12 = 267386880;
                }
                i16 += zzb2;
                i15 += 3;
                i12 = 267386880;
            }
            return i16 + zza((zzht) this.zztj, (Object) t2);
        }
        Unsafe unsafe2 = zzsu;
        int i20 = 0;
        int i21 = 0;
        int i22 = -1;
        int i23 = 0;
        while (i20 < this.zzsv.length) {
            int zzar2 = zzar(i20);
            int[] iArr = this.zzsv;
            int i24 = iArr[i20];
            int i25 = (zzar2 & 267386880) >>> 20;
            if (i25 <= 17) {
                i2 = iArr[i20 + 2];
                int i26 = i2 & i13;
                i = i14 << (i2 >>> 20);
                if (i26 != i22) {
                    i23 = unsafe2.getInt(t2, (long) i26);
                } else {
                    i26 = i22;
                }
                i22 = i26;
            } else {
                i2 = (!this.zztd || i25 < zzey.DOUBLE_LIST_PACKED.id() || i25 > zzey.SINT64_LIST_PACKED.id()) ? 0 : this.zzsv[i20 + 2] & i13;
                i = 0;
            }
            long j3 = (long) (zzar2 & i13);
            switch (i25) {
                case 0:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i21 += zzeo.zzb(i24, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
                        continue;
                        i20 += 3;
                        i13 = 1048575;
                        i14 = 1;
                    }
                    break;
                case 1:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i21 += zzeo.zzb(i24, 0.0f);
                        break;
                    }
                    break;
                case 2:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i3 = zzeo.zzd(i24, unsafe2.getLong(t2, j3));
                        i21 += i3;
                        break;
                    }
                    break;
                case 3:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i3 = zzeo.zze(i24, unsafe2.getLong(t2, j3));
                        i21 += i3;
                        break;
                    }
                    break;
                case 4:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i3 = zzeo.zzj(i24, unsafe2.getInt(t2, j3));
                        i21 += i3;
                        break;
                    }
                    break;
                case 5:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i3 = zzeo.zzg(i24, 0L);
                        i21 += i3;
                        break;
                    }
                    break;
                case 6:
                    if ((i23 & i) != 0) {
                        i21 += zzeo.zzm(i24, 0);
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 7:
                    if ((i23 & i) != 0) {
                        i21 += zzeo.zzb(i24, true);
                        j = 0;
                        i20 += 3;
                        i13 = 1048575;
                        i14 = 1;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 8:
                    if ((i23 & i) != 0) {
                        Object object = unsafe2.getObject(t2, j3);
                        if (object instanceof zzeb) {
                            zzb = zzeo.zzc(i24, (zzeb) object);
                        } else {
                            zzb = zzeo.zzb(i24, (String) object);
                        }
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 9:
                    if ((i23 & i) != 0) {
                        zzb = zzhd.zzc(i24, unsafe2.getObject(t2, j3), zzap(i20));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 10:
                    if ((i23 & i) != 0) {
                        zzb = zzeo.zzc(i24, (zzeb) unsafe2.getObject(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 11:
                    if ((i23 & i) != 0) {
                        zzb = zzeo.zzk(i24, unsafe2.getInt(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 12:
                    if ((i23 & i) != 0) {
                        zzb = zzeo.zzo(i24, unsafe2.getInt(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 13:
                    if ((i23 & i) != 0) {
                        i4 = zzeo.zzn(i24, 0);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 14:
                    if ((i23 & i) != 0) {
                        zzb = zzeo.zzh(i24, 0L);
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 15:
                    if ((i23 & i) != 0) {
                        zzb = zzeo.zzl(i24, unsafe2.getInt(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 16:
                    if ((i23 & i) != 0) {
                        zzb = zzeo.zzf(i24, unsafe2.getLong(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 17:
                    if ((i23 & i) != 0) {
                        zzb = zzeo.zzc(i24, (zzgl) unsafe2.getObject(t2, j3), zzap(i20));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 18:
                    zzb = zzhd.zzw(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += zzb;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 19:
                    i5 = zzhd.zzv(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 20:
                    i5 = zzhd.zzo(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 21:
                    i5 = zzhd.zzp(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 22:
                    i5 = zzhd.zzs(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 23:
                    i5 = zzhd.zzw(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 24:
                    i5 = zzhd.zzv(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 25:
                    i5 = zzhd.zzx(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 26:
                    zzb = zzhd.zzc(i24, (List) unsafe2.getObject(t2, j3));
                    i21 += zzb;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 27:
                    zzb = zzhd.zzc(i24, (List<?>) ((List) unsafe2.getObject(t2, j3)), zzap(i20));
                    i21 += zzb;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 28:
                    zzb = zzhd.zzd(i24, (List) unsafe2.getObject(t2, j3));
                    i21 += zzb;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 29:
                    zzb = zzhd.zzt(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += zzb;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 30:
                    i5 = zzhd.zzr(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 31:
                    i5 = zzhd.zzv(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 32:
                    i5 = zzhd.zzw(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 33:
                    i5 = zzhd.zzu(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 34:
                    i5 = zzhd.zzq(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 35:
                    i8 = zzhd.zzk((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 36:
                    i8 = zzhd.zzj((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 37:
                    i8 = zzhd.zzc((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 38:
                    i8 = zzhd.zzd((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 39:
                    i8 = zzhd.zzg((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 40:
                    i8 = zzhd.zzk((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 41:
                    i8 = zzhd.zzj((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 42:
                    i8 = zzhd.zzl((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 43:
                    i8 = zzhd.zzh((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 44:
                    i8 = zzhd.zzf((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 45:
                    i8 = zzhd.zzj((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 46:
                    i8 = zzhd.zzk((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 47:
                    i8 = zzhd.zzi((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 48:
                    i8 = zzhd.zze((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zztd) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzeo.zzy(i24);
                        i6 = zzeo.zzaa(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 49:
                    zzb = zzhd.zzd(i24, (List) unsafe2.getObject(t2, j3), zzap(i20));
                    i21 += zzb;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 50:
                    zzb = this.zztl.zzb(i24, unsafe2.getObject(t2, j3), zzaq(i20));
                    i21 += zzb;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 51:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzb(i24, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 52:
                    if (zza(t2, i24, i20)) {
                        i4 = zzeo.zzb(i24, 0.0f);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 53:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzd(i24, zzh(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 54:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zze(i24, zzh(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 55:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzj(i24, zzg(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 56:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzg(i24, 0L);
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 57:
                    if (zza(t2, i24, i20)) {
                        i4 = zzeo.zzm(i24, 0);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 58:
                    if (zza(t2, i24, i20)) {
                        i4 = zzeo.zzb(i24, true);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 59:
                    if (zza(t2, i24, i20)) {
                        Object object2 = unsafe2.getObject(t2, j3);
                        if (object2 instanceof zzeb) {
                            zzb = zzeo.zzc(i24, (zzeb) object2);
                        } else {
                            zzb = zzeo.zzb(i24, (String) object2);
                        }
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 60:
                    if (zza(t2, i24, i20)) {
                        zzb = zzhd.zzc(i24, unsafe2.getObject(t2, j3), zzap(i20));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 61:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzc(i24, (zzeb) unsafe2.getObject(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 62:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzk(i24, zzg(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 63:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzo(i24, zzg(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 64:
                    if (zza(t2, i24, i20)) {
                        i4 = zzeo.zzn(i24, 0);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 65:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzh(i24, 0L);
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 66:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzl(i24, zzg(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 67:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzf(i24, zzh(t2, j3));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 68:
                    if (zza(t2, i24, i20)) {
                        zzb = zzeo.zzc(i24, (zzgl) unsafe2.getObject(t2, j3), zzap(i20));
                        i21 += zzb;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                default:
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
            }
            i20 += 3;
            i13 = 1048575;
            i14 = 1;
        }
        int zza = i21 + zza((zzht) this.zztj, (Object) t2);
        if (!this.zzta) {
            return zza;
        }
        zzex<?> zzd = this.zztk.zzd(t2);
        int i27 = 0;
        for (int i28 = 0; i28 < zzd.zznt.zziw(); i28++) {
            Map.Entry<T, Object> zzau = zzd.zznt.zzau(i28);
            i27 += zzex.zzb((zzez) zzau.getKey(), zzau.getValue());
        }
        for (Map.Entry next : zzd.zznt.zzix()) {
            i27 += zzex.zzb((zzez) next.getKey(), next.getValue());
        }
        return zza + i27;
    }

    private static <UT, UB> int zza(zzht<UT, UB> zzht, T t) {
        return zzht.zzm(zzht.zzp(t));
    }

    private static List<?> zzd(Object obj, long j) {
        return (List) zzhz.zzo(obj, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhd.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.firebase-perf.zzin, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.firebase-perf.zzin, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhd.zzb(int, java.util.List<?>, com.google.android.gms.internal.firebase-perf.zzin, com.google.android.gms.internal.firebase-perf.zzhb):void
      com.google.android.gms.internal.firebase-perf.zzhd.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.firebase-perf.zzin, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhd.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.firebase-perf.zzin, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.firebase-perf.zzin, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhd.zza(int, java.util.List<?>, com.google.android.gms.internal.firebase-perf.zzin, com.google.android.gms.internal.firebase-perf.zzhb):void
      com.google.android.gms.internal.firebase-perf.zzhd.zza(com.google.android.gms.internal.firebase-perf.zzgi, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.firebase-perf.zzhd.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.firebase-perf.zzin, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0553  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a2b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r14, com.google.android.gms.internal.p000firebaseperf.zzin r15) throws java.io.IOException {
        /*
            r13 = this;
            int r0 = r15.zzgu()
            int r1 = com.google.android.gms.internal.p000firebaseperf.zzgx.zzts
            r2 = 267386880(0xff00000, float:2.3665827E-29)
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
            if (r0 != r1) goto L_0x0529
            com.google.android.gms.internal.firebase-perf.zzht<?, ?> r0 = r13.zztj
            zza(r0, r14, r15)
            boolean r0 = r13.zzta
            if (r0 == 0) goto L_0x0032
            com.google.android.gms.internal.firebase-perf.zzes<?> r0 = r13.zztk
            com.google.android.gms.internal.firebase-perf.zzex r0 = r0.zzd(r14)
            com.google.android.gms.internal.firebase-perf.zzhg<T, java.lang.Object> r1 = r0.zznt
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0032
            java.util.Iterator r0 = r0.descendingIterator()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0034
        L_0x0032:
            r0 = r3
            r1 = r0
        L_0x0034:
            int[] r7 = r13.zzsv
            int r7 = r7.length
            int r7 = r7 + -3
        L_0x0039:
            if (r7 < 0) goto L_0x0511
            int r8 = r13.zzar(r7)
            int[] r9 = r13.zzsv
            r9 = r9[r7]
        L_0x0043:
            if (r1 == 0) goto L_0x0061
            com.google.android.gms.internal.firebase-perf.zzes<?> r10 = r13.zztk
            int r10 = r10.zzb(r1)
            if (r10 <= r9) goto L_0x0061
            com.google.android.gms.internal.firebase-perf.zzes<?> r10 = r13.zztk
            r10.zza(r15, r1)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x005f
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0043
        L_0x005f:
            r1 = r3
            goto L_0x0043
        L_0x0061:
            r10 = r8 & r2
            int r10 = r10 >>> 20
            switch(r10) {
                case 0: goto L_0x04fe;
                case 1: goto L_0x04ee;
                case 2: goto L_0x04de;
                case 3: goto L_0x04ce;
                case 4: goto L_0x04be;
                case 5: goto L_0x04ae;
                case 6: goto L_0x049e;
                case 7: goto L_0x048d;
                case 8: goto L_0x047c;
                case 9: goto L_0x0467;
                case 10: goto L_0x0454;
                case 11: goto L_0x0443;
                case 12: goto L_0x0432;
                case 13: goto L_0x0421;
                case 14: goto L_0x0410;
                case 15: goto L_0x03ff;
                case 16: goto L_0x03ee;
                case 17: goto L_0x03d9;
                case 18: goto L_0x03c8;
                case 19: goto L_0x03b7;
                case 20: goto L_0x03a6;
                case 21: goto L_0x0395;
                case 22: goto L_0x0384;
                case 23: goto L_0x0373;
                case 24: goto L_0x0362;
                case 25: goto L_0x0351;
                case 26: goto L_0x0340;
                case 27: goto L_0x032b;
                case 28: goto L_0x031a;
                case 29: goto L_0x0309;
                case 30: goto L_0x02f8;
                case 31: goto L_0x02e7;
                case 32: goto L_0x02d6;
                case 33: goto L_0x02c5;
                case 34: goto L_0x02b4;
                case 35: goto L_0x02a3;
                case 36: goto L_0x0292;
                case 37: goto L_0x0281;
                case 38: goto L_0x0270;
                case 39: goto L_0x025f;
                case 40: goto L_0x024e;
                case 41: goto L_0x023d;
                case 42: goto L_0x022c;
                case 43: goto L_0x021b;
                case 44: goto L_0x020a;
                case 45: goto L_0x01f9;
                case 46: goto L_0x01e8;
                case 47: goto L_0x01d7;
                case 48: goto L_0x01c6;
                case 49: goto L_0x01b1;
                case 50: goto L_0x01a6;
                case 51: goto L_0x0195;
                case 52: goto L_0x0184;
                case 53: goto L_0x0173;
                case 54: goto L_0x0162;
                case 55: goto L_0x0151;
                case 56: goto L_0x0140;
                case 57: goto L_0x012f;
                case 58: goto L_0x011e;
                case 59: goto L_0x010d;
                case 60: goto L_0x00f8;
                case 61: goto L_0x00e5;
                case 62: goto L_0x00d4;
                case 63: goto L_0x00c3;
                case 64: goto L_0x00b2;
                case 65: goto L_0x00a1;
                case 66: goto L_0x0090;
                case 67: goto L_0x007f;
                case 68: goto L_0x006a;
                default: goto L_0x0068;
            }
        L_0x0068:
            goto L_0x050d
        L_0x006a:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            com.google.android.gms.internal.firebase-perf.zzhb r10 = r13.zzap(r7)
            r15.zzb(r9, r8, r10)
            goto L_0x050d
        L_0x007f:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzh(r14, r10)
            r15.zzb(r9, r10)
            goto L_0x050d
        L_0x0090:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzg(r14, r10)
            r15.zzh(r9, r8)
            goto L_0x050d
        L_0x00a1:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzh(r14, r10)
            r15.zzj(r9, r10)
            goto L_0x050d
        L_0x00b2:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzg(r14, r10)
            r15.zzp(r9, r8)
            goto L_0x050d
        L_0x00c3:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzg(r14, r10)
            r15.zzq(r9, r8)
            goto L_0x050d
        L_0x00d4:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzg(r14, r10)
            r15.zzg(r9, r8)
            goto L_0x050d
        L_0x00e5:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            com.google.android.gms.internal.firebase-perf.zzeb r8 = (com.google.android.gms.internal.p000firebaseperf.zzeb) r8
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x00f8:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            com.google.android.gms.internal.firebase-perf.zzhb r10 = r13.zzap(r7)
            r15.zza(r9, r8, r10)
            goto L_0x050d
        L_0x010d:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            zza(r9, r8, r15)
            goto L_0x050d
        L_0x011e:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = zzi(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x012f:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzg(r14, r10)
            r15.zzi(r9, r8)
            goto L_0x050d
        L_0x0140:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzh(r14, r10)
            r15.zzc(r9, r10)
            goto L_0x050d
        L_0x0151:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzg(r14, r10)
            r15.zzf(r9, r8)
            goto L_0x050d
        L_0x0162:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzh(r14, r10)
            r15.zza(r9, r10)
            goto L_0x050d
        L_0x0173:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zzh(r14, r10)
            r15.zzi(r9, r10)
            goto L_0x050d
        L_0x0184:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = zzf(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x0195:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = zze(r14, r10)
            r15.zza(r9, r10)
            goto L_0x050d
        L_0x01a6:
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            r13.zza(r15, r9, r8, r7)
            goto L_0x050d
        L_0x01b1:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase-perf.zzhb r10 = r13.zzap(r7)
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r9, r8, r15, r10)
            goto L_0x050d
        L_0x01c6:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01d7:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzj(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01e8:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzg(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01f9:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzl(r9, r8, r15, r4)
            goto L_0x050d
        L_0x020a:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzm(r9, r8, r15, r4)
            goto L_0x050d
        L_0x021b:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzi(r9, r8, r15, r4)
            goto L_0x050d
        L_0x022c:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzn(r9, r8, r15, r4)
            goto L_0x050d
        L_0x023d:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzk(r9, r8, r15, r4)
            goto L_0x050d
        L_0x024e:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzf(r9, r8, r15, r4)
            goto L_0x050d
        L_0x025f:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzh(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0270:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzd(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0281:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzc(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0292:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r9, r8, r15, r4)
            goto L_0x050d
        L_0x02a3:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r9, r8, r15, r4)
            goto L_0x050d
        L_0x02b4:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02c5:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzj(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02d6:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzg(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02e7:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzl(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02f8:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzm(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0309:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzi(r9, r8, r15, r5)
            goto L_0x050d
        L_0x031a:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r9, r8, r15)
            goto L_0x050d
        L_0x032b:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.firebase-perf.zzhb r10 = r13.zzap(r7)
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r9, r8, r15, r10)
            goto L_0x050d
        L_0x0340:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r9, r8, r15)
            goto L_0x050d
        L_0x0351:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzn(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0362:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzk(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0373:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzf(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0384:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzh(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0395:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzd(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03a6:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzc(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03b7:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03c8:
            int[] r9 = r13.zzsv
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03d9:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            com.google.android.gms.internal.firebase-perf.zzhb r10 = r13.zzap(r7)
            r15.zzb(r9, r8, r10)
            goto L_0x050d
        L_0x03ee:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r10)
            r15.zzb(r9, r10)
            goto L_0x050d
        L_0x03ff:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r10)
            r15.zzh(r9, r8)
            goto L_0x050d
        L_0x0410:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r10)
            r15.zzj(r9, r10)
            goto L_0x050d
        L_0x0421:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r10)
            r15.zzp(r9, r8)
            goto L_0x050d
        L_0x0432:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r10)
            r15.zzq(r9, r8)
            goto L_0x050d
        L_0x0443:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r10)
            r15.zzg(r9, r8)
            goto L_0x050d
        L_0x0454:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            com.google.android.gms.internal.firebase-perf.zzeb r8 = (com.google.android.gms.internal.p000firebaseperf.zzeb) r8
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x0467:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            com.google.android.gms.internal.firebase-perf.zzhb r10 = r13.zzap(r7)
            r15.zza(r9, r8, r10)
            goto L_0x050d
        L_0x047c:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r10)
            zza(r9, r8, r15)
            goto L_0x050d
        L_0x048d:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzl(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x049e:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r10)
            r15.zzi(r9, r8)
            goto L_0x050d
        L_0x04ae:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r10)
            r15.zzc(r9, r10)
            goto L_0x050d
        L_0x04be:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r10)
            r15.zzf(r9, r8)
            goto L_0x050d
        L_0x04ce:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r10)
            r15.zza(r9, r10)
            goto L_0x050d
        L_0x04de:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r10)
            r15.zzi(r9, r10)
            goto L_0x050d
        L_0x04ee:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzm(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x04fe:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzn(r14, r10)
            r15.zza(r9, r10)
        L_0x050d:
            int r7 = r7 + -3
            goto L_0x0039
        L_0x0511:
            if (r1 == 0) goto L_0x0528
            com.google.android.gms.internal.firebase-perf.zzes<?> r14 = r13.zztk
            r14.zza(r15, r1)
            boolean r14 = r0.hasNext()
            if (r14 == 0) goto L_0x0526
            java.lang.Object r14 = r0.next()
            java.util.Map$Entry r14 = (java.util.Map.Entry) r14
            r1 = r14
            goto L_0x0511
        L_0x0526:
            r1 = r3
            goto L_0x0511
        L_0x0528:
            return
        L_0x0529:
            boolean r0 = r13.zztc
            if (r0 == 0) goto L_0x0a46
            boolean r0 = r13.zzta
            if (r0 == 0) goto L_0x054a
            com.google.android.gms.internal.firebase-perf.zzes<?> r0 = r13.zztk
            com.google.android.gms.internal.firebase-perf.zzex r0 = r0.zzd(r14)
            com.google.android.gms.internal.firebase-perf.zzhg<T, java.lang.Object> r1 = r0.zznt
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x054a
            java.util.Iterator r0 = r0.iterator()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x054c
        L_0x054a:
            r0 = r3
            r1 = r0
        L_0x054c:
            int[] r7 = r13.zzsv
            int r7 = r7.length
            r8 = r1
            r1 = 0
        L_0x0551:
            if (r1 >= r7) goto L_0x0a29
            int r9 = r13.zzar(r1)
            int[] r10 = r13.zzsv
            r10 = r10[r1]
        L_0x055b:
            if (r8 == 0) goto L_0x0579
            com.google.android.gms.internal.firebase-perf.zzes<?> r11 = r13.zztk
            int r11 = r11.zzb(r8)
            if (r11 > r10) goto L_0x0579
            com.google.android.gms.internal.firebase-perf.zzes<?> r11 = r13.zztk
            r11.zza(r15, r8)
            boolean r8 = r0.hasNext()
            if (r8 == 0) goto L_0x0577
            java.lang.Object r8 = r0.next()
            java.util.Map$Entry r8 = (java.util.Map.Entry) r8
            goto L_0x055b
        L_0x0577:
            r8 = r3
            goto L_0x055b
        L_0x0579:
            r11 = r9 & r2
            int r11 = r11 >>> 20
            switch(r11) {
                case 0: goto L_0x0a16;
                case 1: goto L_0x0a06;
                case 2: goto L_0x09f6;
                case 3: goto L_0x09e6;
                case 4: goto L_0x09d6;
                case 5: goto L_0x09c6;
                case 6: goto L_0x09b6;
                case 7: goto L_0x09a5;
                case 8: goto L_0x0994;
                case 9: goto L_0x097f;
                case 10: goto L_0x096c;
                case 11: goto L_0x095b;
                case 12: goto L_0x094a;
                case 13: goto L_0x0939;
                case 14: goto L_0x0928;
                case 15: goto L_0x0917;
                case 16: goto L_0x0906;
                case 17: goto L_0x08f1;
                case 18: goto L_0x08e0;
                case 19: goto L_0x08cf;
                case 20: goto L_0x08be;
                case 21: goto L_0x08ad;
                case 22: goto L_0x089c;
                case 23: goto L_0x088b;
                case 24: goto L_0x087a;
                case 25: goto L_0x0869;
                case 26: goto L_0x0858;
                case 27: goto L_0x0843;
                case 28: goto L_0x0832;
                case 29: goto L_0x0821;
                case 30: goto L_0x0810;
                case 31: goto L_0x07ff;
                case 32: goto L_0x07ee;
                case 33: goto L_0x07dd;
                case 34: goto L_0x07cc;
                case 35: goto L_0x07bb;
                case 36: goto L_0x07aa;
                case 37: goto L_0x0799;
                case 38: goto L_0x0788;
                case 39: goto L_0x0777;
                case 40: goto L_0x0766;
                case 41: goto L_0x0755;
                case 42: goto L_0x0744;
                case 43: goto L_0x0733;
                case 44: goto L_0x0722;
                case 45: goto L_0x0711;
                case 46: goto L_0x0700;
                case 47: goto L_0x06ef;
                case 48: goto L_0x06de;
                case 49: goto L_0x06c9;
                case 50: goto L_0x06be;
                case 51: goto L_0x06ad;
                case 52: goto L_0x069c;
                case 53: goto L_0x068b;
                case 54: goto L_0x067a;
                case 55: goto L_0x0669;
                case 56: goto L_0x0658;
                case 57: goto L_0x0647;
                case 58: goto L_0x0636;
                case 59: goto L_0x0625;
                case 60: goto L_0x0610;
                case 61: goto L_0x05fd;
                case 62: goto L_0x05ec;
                case 63: goto L_0x05db;
                case 64: goto L_0x05ca;
                case 65: goto L_0x05b9;
                case 66: goto L_0x05a8;
                case 67: goto L_0x0597;
                case 68: goto L_0x0582;
                default: goto L_0x0580;
            }
        L_0x0580:
            goto L_0x0a25
        L_0x0582:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            com.google.android.gms.internal.firebase-perf.zzhb r11 = r13.zzap(r1)
            r15.zzb(r10, r9, r11)
            goto L_0x0a25
        L_0x0597:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzh(r14, r11)
            r15.zzb(r10, r11)
            goto L_0x0a25
        L_0x05a8:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzg(r14, r11)
            r15.zzh(r10, r9)
            goto L_0x0a25
        L_0x05b9:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzh(r14, r11)
            r15.zzj(r10, r11)
            goto L_0x0a25
        L_0x05ca:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzg(r14, r11)
            r15.zzp(r10, r9)
            goto L_0x0a25
        L_0x05db:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzg(r14, r11)
            r15.zzq(r10, r9)
            goto L_0x0a25
        L_0x05ec:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzg(r14, r11)
            r15.zzg(r10, r9)
            goto L_0x0a25
        L_0x05fd:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            com.google.android.gms.internal.firebase-perf.zzeb r9 = (com.google.android.gms.internal.p000firebaseperf.zzeb) r9
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x0610:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            com.google.android.gms.internal.firebase-perf.zzhb r11 = r13.zzap(r1)
            r15.zza(r10, r9, r11)
            goto L_0x0a25
        L_0x0625:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            zza(r10, r9, r15)
            goto L_0x0a25
        L_0x0636:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = zzi(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x0647:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzg(r14, r11)
            r15.zzi(r10, r9)
            goto L_0x0a25
        L_0x0658:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzh(r14, r11)
            r15.zzc(r10, r11)
            goto L_0x0a25
        L_0x0669:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzg(r14, r11)
            r15.zzf(r10, r9)
            goto L_0x0a25
        L_0x067a:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzh(r14, r11)
            r15.zza(r10, r11)
            goto L_0x0a25
        L_0x068b:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zzh(r14, r11)
            r15.zzi(r10, r11)
            goto L_0x0a25
        L_0x069c:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = zzf(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x06ad:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = zze(r14, r11)
            r15.zza(r10, r11)
            goto L_0x0a25
        L_0x06be:
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            r13.zza(r15, r10, r9, r1)
            goto L_0x0a25
        L_0x06c9:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase-perf.zzhb r11 = r13.zzap(r1)
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r10, r9, r15, r11)
            goto L_0x0a25
        L_0x06de:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x06ef:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzj(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0700:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzg(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0711:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzl(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0722:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzm(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0733:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzi(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0744:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzn(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0755:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzk(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0766:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzf(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0777:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzh(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0788:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzd(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0799:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzc(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x07aa:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x07bb:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x07cc:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x07dd:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzj(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x07ee:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzg(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x07ff:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzl(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x0810:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzm(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x0821:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzi(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x0832:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r10, r9, r15)
            goto L_0x0a25
        L_0x0843:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase-perf.zzhb r11 = r13.zzap(r1)
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r10, r9, r15, r11)
            goto L_0x0a25
        L_0x0858:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r10, r9, r15)
            goto L_0x0a25
        L_0x0869:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzn(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x087a:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzk(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x088b:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzf(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x089c:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzh(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08ad:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzd(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08be:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzc(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08cf:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08e0:
            int[] r10 = r13.zzsv
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08f1:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            com.google.android.gms.internal.firebase-perf.zzhb r11 = r13.zzap(r1)
            r15.zzb(r10, r9, r11)
            goto L_0x0a25
        L_0x0906:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r11)
            r15.zzb(r10, r11)
            goto L_0x0a25
        L_0x0917:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r11)
            r15.zzh(r10, r9)
            goto L_0x0a25
        L_0x0928:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r11)
            r15.zzj(r10, r11)
            goto L_0x0a25
        L_0x0939:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r11)
            r15.zzp(r10, r9)
            goto L_0x0a25
        L_0x094a:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r11)
            r15.zzq(r10, r9)
            goto L_0x0a25
        L_0x095b:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r11)
            r15.zzg(r10, r9)
            goto L_0x0a25
        L_0x096c:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            com.google.android.gms.internal.firebase-perf.zzeb r9 = (com.google.android.gms.internal.p000firebaseperf.zzeb) r9
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x097f:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            com.google.android.gms.internal.firebase-perf.zzhb r11 = r13.zzap(r1)
            r15.zza(r10, r9, r11)
            goto L_0x0a25
        L_0x0994:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzo(r14, r11)
            zza(r10, r9, r15)
            goto L_0x0a25
        L_0x09a5:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzl(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x09b6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r11)
            r15.zzi(r10, r9)
            goto L_0x0a25
        L_0x09c6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r11)
            r15.zzc(r10, r11)
            goto L_0x0a25
        L_0x09d6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzj(r14, r11)
            r15.zzf(r10, r9)
            goto L_0x0a25
        L_0x09e6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r11)
            r15.zza(r10, r11)
            goto L_0x0a25
        L_0x09f6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzk(r14, r11)
            r15.zzi(r10, r11)
            goto L_0x0a25
        L_0x0a06:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzm(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x0a16:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzn(r14, r11)
            r15.zza(r10, r11)
        L_0x0a25:
            int r1 = r1 + 3
            goto L_0x0551
        L_0x0a29:
            if (r8 == 0) goto L_0x0a40
            com.google.android.gms.internal.firebase-perf.zzes<?> r1 = r13.zztk
            r1.zza(r15, r8)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0a3e
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            r8 = r1
            goto L_0x0a29
        L_0x0a3e:
            r8 = r3
            goto L_0x0a29
        L_0x0a40:
            com.google.android.gms.internal.firebase-perf.zzht<?, ?> r0 = r13.zztj
            zza(r0, r14, r15)
            return
        L_0x0a46:
            r13.zzb(r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzgp.zza(java.lang.Object, com.google.android.gms.internal.firebase-perf.zzin):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhd.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.firebase-perf.zzin, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.firebase-perf.zzin, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhd.zzb(int, java.util.List<?>, com.google.android.gms.internal.firebase-perf.zzin, com.google.android.gms.internal.firebase-perf.zzhb):void
      com.google.android.gms.internal.firebase-perf.zzhd.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.firebase-perf.zzin, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhd.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.firebase-perf.zzin, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.firebase-perf.zzin, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhd.zza(int, java.util.List<?>, com.google.android.gms.internal.firebase-perf.zzin, com.google.android.gms.internal.firebase-perf.zzhb):void
      com.google.android.gms.internal.firebase-perf.zzhd.zza(com.google.android.gms.internal.firebase-perf.zzgi, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.firebase-perf.zzhd.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.firebase-perf.zzin, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04b3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzb(T r19, com.google.android.gms.internal.p000firebaseperf.zzin r20) throws java.io.IOException {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            boolean r3 = r0.zzta
            if (r3 == 0) goto L_0x0023
            com.google.android.gms.internal.firebase-perf.zzes<?> r3 = r0.zztk
            com.google.android.gms.internal.firebase-perf.zzex r3 = r3.zzd(r1)
            com.google.android.gms.internal.firebase-perf.zzhg<T, java.lang.Object> r5 = r3.zznt
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x0023
            java.util.Iterator r3 = r3.iterator()
            java.lang.Object r5 = r3.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            goto L_0x0025
        L_0x0023:
            r3 = 0
            r5 = 0
        L_0x0025:
            r6 = -1
            int[] r7 = r0.zzsv
            int r7 = r7.length
            sun.misc.Unsafe r8 = com.google.android.gms.internal.p000firebaseperf.zzgp.zzsu
            r10 = r5
            r5 = 0
            r11 = 0
        L_0x002e:
            if (r5 >= r7) goto L_0x04ad
            int r12 = r0.zzar(r5)
            int[] r13 = r0.zzsv
            r14 = r13[r5]
            r15 = 267386880(0xff00000, float:2.3665827E-29)
            r15 = r15 & r12
            int r15 = r15 >>> 20
            boolean r4 = r0.zztc
            r16 = 1048575(0xfffff, float:1.469367E-39)
            if (r4 != 0) goto L_0x0062
            r4 = 17
            if (r15 > r4) goto L_0x0062
            int r4 = r5 + 2
            r4 = r13[r4]
            r13 = r4 & r16
            r17 = r10
            if (r13 == r6) goto L_0x0058
            long r9 = (long) r13
            int r11 = r8.getInt(r1, r9)
            goto L_0x0059
        L_0x0058:
            r13 = r6
        L_0x0059:
            int r4 = r4 >>> 20
            r6 = 1
            int r9 = r6 << r4
            r6 = r13
            r10 = r17
            goto L_0x0067
        L_0x0062:
            r17 = r10
            r10 = r17
            r9 = 0
        L_0x0067:
            if (r10 == 0) goto L_0x0086
            com.google.android.gms.internal.firebase-perf.zzes<?> r4 = r0.zztk
            int r4 = r4.zzb(r10)
            if (r4 > r14) goto L_0x0086
            com.google.android.gms.internal.firebase-perf.zzes<?> r4 = r0.zztk
            r4.zza(r2, r10)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0084
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            r10 = r4
            goto L_0x0067
        L_0x0084:
            r10 = 0
            goto L_0x0067
        L_0x0086:
            r4 = r12 & r16
            long r12 = (long) r4
            switch(r15) {
                case 0: goto L_0x049d;
                case 1: goto L_0x0490;
                case 2: goto L_0x0483;
                case 3: goto L_0x0476;
                case 4: goto L_0x0469;
                case 5: goto L_0x045c;
                case 6: goto L_0x044f;
                case 7: goto L_0x0442;
                case 8: goto L_0x0434;
                case 9: goto L_0x0422;
                case 10: goto L_0x0412;
                case 11: goto L_0x0404;
                case 12: goto L_0x03f6;
                case 13: goto L_0x03e8;
                case 14: goto L_0x03da;
                case 15: goto L_0x03cc;
                case 16: goto L_0x03be;
                case 17: goto L_0x03ac;
                case 18: goto L_0x039c;
                case 19: goto L_0x038c;
                case 20: goto L_0x037c;
                case 21: goto L_0x036c;
                case 22: goto L_0x035c;
                case 23: goto L_0x034c;
                case 24: goto L_0x033c;
                case 25: goto L_0x032c;
                case 26: goto L_0x031d;
                case 27: goto L_0x030a;
                case 28: goto L_0x02fb;
                case 29: goto L_0x02eb;
                case 30: goto L_0x02db;
                case 31: goto L_0x02cb;
                case 32: goto L_0x02bb;
                case 33: goto L_0x02ab;
                case 34: goto L_0x029b;
                case 35: goto L_0x028b;
                case 36: goto L_0x027b;
                case 37: goto L_0x026b;
                case 38: goto L_0x025b;
                case 39: goto L_0x024b;
                case 40: goto L_0x023b;
                case 41: goto L_0x022b;
                case 42: goto L_0x021b;
                case 43: goto L_0x020b;
                case 44: goto L_0x01fb;
                case 45: goto L_0x01eb;
                case 46: goto L_0x01db;
                case 47: goto L_0x01cb;
                case 48: goto L_0x01bb;
                case 49: goto L_0x01a8;
                case 50: goto L_0x019f;
                case 51: goto L_0x0190;
                case 52: goto L_0x0181;
                case 53: goto L_0x0172;
                case 54: goto L_0x0163;
                case 55: goto L_0x0154;
                case 56: goto L_0x0145;
                case 57: goto L_0x0136;
                case 58: goto L_0x0127;
                case 59: goto L_0x0118;
                case 60: goto L_0x0105;
                case 61: goto L_0x00f5;
                case 62: goto L_0x00e7;
                case 63: goto L_0x00d9;
                case 64: goto L_0x00cb;
                case 65: goto L_0x00bd;
                case 66: goto L_0x00af;
                case 67: goto L_0x00a1;
                case 68: goto L_0x008f;
                default: goto L_0x008c;
            }
        L_0x008c:
            r15 = 0
            goto L_0x04a9
        L_0x008f:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.firebase-perf.zzhb r9 = r0.zzap(r5)
            r2.zzb(r14, r4, r9)
            goto L_0x008c
        L_0x00a1:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzh(r1, r12)
            r2.zzb(r14, r12)
            goto L_0x008c
        L_0x00af:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzg(r1, r12)
            r2.zzh(r14, r4)
            goto L_0x008c
        L_0x00bd:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzh(r1, r12)
            r2.zzj(r14, r12)
            goto L_0x008c
        L_0x00cb:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzg(r1, r12)
            r2.zzp(r14, r4)
            goto L_0x008c
        L_0x00d9:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzg(r1, r12)
            r2.zzq(r14, r4)
            goto L_0x008c
        L_0x00e7:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzg(r1, r12)
            r2.zzg(r14, r4)
            goto L_0x008c
        L_0x00f5:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.firebase-perf.zzeb r4 = (com.google.android.gms.internal.p000firebaseperf.zzeb) r4
            r2.zza(r14, r4)
            goto L_0x008c
        L_0x0105:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.firebase-perf.zzhb r9 = r0.zzap(r5)
            r2.zza(r14, r4, r9)
            goto L_0x008c
        L_0x0118:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            zza(r14, r4, r2)
            goto L_0x008c
        L_0x0127:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            boolean r4 = zzi(r1, r12)
            r2.zza(r14, r4)
            goto L_0x008c
        L_0x0136:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzg(r1, r12)
            r2.zzi(r14, r4)
            goto L_0x008c
        L_0x0145:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzh(r1, r12)
            r2.zzc(r14, r12)
            goto L_0x008c
        L_0x0154:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzg(r1, r12)
            r2.zzf(r14, r4)
            goto L_0x008c
        L_0x0163:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzh(r1, r12)
            r2.zza(r14, r12)
            goto L_0x008c
        L_0x0172:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zzh(r1, r12)
            r2.zzi(r14, r12)
            goto L_0x008c
        L_0x0181:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            float r4 = zzf(r1, r12)
            r2.zza(r14, r4)
            goto L_0x008c
        L_0x0190:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            double r12 = zze(r1, r12)
            r2.zza(r14, r12)
            goto L_0x008c
        L_0x019f:
            java.lang.Object r4 = r8.getObject(r1, r12)
            r0.zza(r2, r14, r4, r5)
            goto L_0x008c
        L_0x01a8:
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase-perf.zzhb r12 = r0.zzap(r5)
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r4, r9, r2, r12)
            goto L_0x008c
        L_0x01bb:
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r14 = 1
            com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01cb:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzj(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01db:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzg(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01eb:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzl(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01fb:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzm(r4, r9, r2, r14)
            goto L_0x008c
        L_0x020b:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzi(r4, r9, r2, r14)
            goto L_0x008c
        L_0x021b:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzn(r4, r9, r2, r14)
            goto L_0x008c
        L_0x022b:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzk(r4, r9, r2, r14)
            goto L_0x008c
        L_0x023b:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzf(r4, r9, r2, r14)
            goto L_0x008c
        L_0x024b:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzh(r4, r9, r2, r14)
            goto L_0x008c
        L_0x025b:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzd(r4, r9, r2, r14)
            goto L_0x008c
        L_0x026b:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzc(r4, r9, r2, r14)
            goto L_0x008c
        L_0x027b:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r4, r9, r2, r14)
            goto L_0x008c
        L_0x028b:
            r14 = 1
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r4, r9, r2, r14)
            goto L_0x008c
        L_0x029b:
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r14 = 0
            com.google.android.gms.internal.p000firebaseperf.zzhd.zze(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02ab:
            r14 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzj(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02bb:
            r14 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzg(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02cb:
            r14 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzl(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02db:
            r14 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzm(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02eb:
            r14 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzi(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02fb:
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r4, r9, r2)
            goto L_0x008c
        L_0x030a:
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.firebase-perf.zzhb r12 = r0.zzap(r5)
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r4, r9, r2, r12)
            goto L_0x008c
        L_0x031d:
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r4, r9, r2)
            goto L_0x008c
        L_0x032c:
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r15 = 0
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzn(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x033c:
            r15 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzk(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x034c:
            r15 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzf(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x035c:
            r15 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzh(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x036c:
            r15 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzd(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x037c:
            r15 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzc(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x038c:
            r15 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zzb(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x039c:
            r15 = 0
            int[] r4 = r0.zzsv
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.p000firebaseperf.zzhd.zza(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x03ac:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.firebase-perf.zzhb r9 = r0.zzap(r5)
            r2.zzb(r14, r4, r9)
            goto L_0x04a9
        L_0x03be:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzb(r14, r12)
            goto L_0x04a9
        L_0x03cc:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzh(r14, r4)
            goto L_0x04a9
        L_0x03da:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzj(r14, r12)
            goto L_0x04a9
        L_0x03e8:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzp(r14, r4)
            goto L_0x04a9
        L_0x03f6:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzq(r14, r4)
            goto L_0x04a9
        L_0x0404:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzg(r14, r4)
            goto L_0x04a9
        L_0x0412:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.firebase-perf.zzeb r4 = (com.google.android.gms.internal.p000firebaseperf.zzeb) r4
            r2.zza(r14, r4)
            goto L_0x04a9
        L_0x0422:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.firebase-perf.zzhb r9 = r0.zzap(r5)
            r2.zza(r14, r4, r9)
            goto L_0x04a9
        L_0x0434:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            zza(r14, r4, r2)
            goto L_0x04a9
        L_0x0442:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            boolean r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzl(r1, r12)
            r2.zza(r14, r4)
            goto L_0x04a9
        L_0x044f:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzi(r14, r4)
            goto L_0x04a9
        L_0x045c:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzc(r14, r12)
            goto L_0x04a9
        L_0x0469:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzf(r14, r4)
            goto L_0x04a9
        L_0x0476:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zza(r14, r12)
            goto L_0x04a9
        L_0x0483:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzi(r14, r12)
            goto L_0x04a9
        L_0x0490:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            float r4 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzm(r1, r12)
            r2.zza(r14, r4)
            goto L_0x04a9
        L_0x049d:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            double r12 = com.google.android.gms.internal.p000firebaseperf.zzhz.zzn(r1, r12)
            r2.zza(r14, r12)
        L_0x04a9:
            int r5 = r5 + 3
            goto L_0x002e
        L_0x04ad:
            r17 = r10
            r4 = r17
        L_0x04b1:
            if (r4 == 0) goto L_0x04c7
            com.google.android.gms.internal.firebase-perf.zzes<?> r5 = r0.zztk
            r5.zza(r2, r4)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x04c5
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            goto L_0x04b1
        L_0x04c5:
            r4 = 0
            goto L_0x04b1
        L_0x04c7:
            com.google.android.gms.internal.firebase-perf.zzht<?, ?> r3 = r0.zztj
            zza(r3, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzgp.zzb(java.lang.Object, com.google.android.gms.internal.firebase-perf.zzin):void");
    }

    private final <K, V> void zza(zzin zzin, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzin.zza(i, this.zztl.zzj(zzaq(i2)), this.zztl.zzk(obj));
        }
    }

    private static <UT, UB> void zza(zzht<UT, UB> zzht, T t, zzin zzin) throws IOException {
        zzht.zza(zzht.zzp(t), zzin);
    }

    private final zzhb zzap(int i) {
        int i2 = (i / 3) << 1;
        zzhb zzhb = (zzhb) this.zzsw[i2];
        if (zzhb != null) {
            return zzhb;
        }
        zzhb zze = zzha.zzio().zze((Class) this.zzsw[i2 + 1]);
        this.zzsw[i2] = zze;
        return zze;
    }

    private final Object zzaq(int i) {
        return this.zzsw[(i / 3) << 1];
    }

    public final void zzf(T t) {
        int i;
        int i2 = this.zztf;
        while (true) {
            i = this.zztg;
            if (i2 >= i) {
                break;
            }
            long zzar = (long) (zzar(this.zzte[i2]) & 1048575);
            Object zzo = zzhz.zzo(t, zzar);
            if (zzo != null) {
                zzhz.zza(t, zzar, this.zztl.zzl(zzo));
            }
            i2++;
        }
        int length = this.zzte.length;
        while (i < length) {
            this.zzti.zza(t, (long) this.zzte[i]);
            i++;
        }
        this.zztj.zzf(t);
        if (this.zzta) {
            this.zztk.zzf(t);
        }
    }

    public final boolean zzn(T t) {
        int i;
        int i2 = 0;
        int i3 = -1;
        int i4 = 0;
        while (true) {
            boolean z = true;
            if (i2 >= this.zztf) {
                return !this.zzta || this.zztk.zzd(t).isInitialized();
            }
            int i5 = this.zzte[i2];
            int i6 = this.zzsv[i5];
            int zzar = zzar(i5);
            if (!this.zztc) {
                int i7 = this.zzsv[i5 + 2];
                int i8 = i7 & 1048575;
                i = 1 << (i7 >>> 20);
                if (i8 != i3) {
                    i4 = zzsu.getInt(t, (long) i8);
                    i3 = i8;
                }
            } else {
                i = 0;
            }
            if (((268435456 & zzar) != 0) && !zza(t, i5, i4, i)) {
                return false;
            }
            int i9 = (267386880 & zzar) >>> 20;
            if (i9 != 9 && i9 != 17) {
                if (i9 != 27) {
                    if (i9 == 60 || i9 == 68) {
                        if (zza(t, i6, i5) && !zza(t, zzar, zzap(i5))) {
                            return false;
                        }
                    } else if (i9 != 49) {
                        if (i9 != 50) {
                            continue;
                        } else {
                            Map<?, ?> zzk = this.zztl.zzk(zzhz.zzo(t, (long) (zzar & 1048575)));
                            if (!zzk.isEmpty()) {
                                if (this.zztl.zzj(zzaq(i5)).zzsp.zzjm() == zzio.MESSAGE) {
                                    zzhb zzhb = null;
                                    Iterator<?> it = zzk.values().iterator();
                                    while (true) {
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        Object next = it.next();
                                        if (zzhb == null) {
                                            zzhb = zzha.zzio().zze(next.getClass());
                                        }
                                        if (!zzhb.zzn(next)) {
                                            z = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!z) {
                                return false;
                            }
                        }
                    }
                }
                List list = (List) zzhz.zzo(t, (long) (zzar & 1048575));
                if (!list.isEmpty()) {
                    zzhb zzap = zzap(i5);
                    int i10 = 0;
                    while (true) {
                        if (i10 >= list.size()) {
                            break;
                        } else if (!zzap.zzn(list.get(i10))) {
                            z = false;
                            break;
                        } else {
                            i10++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (zza(t, i5, i4, i) && !zza(t, zzar, zzap(i5))) {
                return false;
            }
            i2++;
        }
    }

    private static boolean zza(Object obj, int i, zzhb zzhb) {
        return zzhb.zzn(zzhz.zzo(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zzin zzin) throws IOException {
        if (obj instanceof String) {
            zzin.zza(i, (String) obj);
        } else {
            zzin.zza(i, (zzeb) obj);
        }
    }

    private final int zzar(int i) {
        return this.zzsv[i + 1];
    }

    private final int zzas(int i) {
        return this.zzsv[i + 2];
    }

    private static <T> double zze(T t, long j) {
        return ((Double) zzhz.zzo(t, j)).doubleValue();
    }

    private static <T> float zzf(T t, long j) {
        return ((Float) zzhz.zzo(t, j)).floatValue();
    }

    private static <T> int zzg(T t, long j) {
        return ((Integer) zzhz.zzo(t, j)).intValue();
    }

    private static <T> long zzh(T t, long j) {
        return ((Long) zzhz.zzo(t, j)).longValue();
    }

    private static <T> boolean zzi(T t, long j) {
        return ((Boolean) zzhz.zzo(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zza(t, i) == zza(t2, i);
    }

    private final boolean zza(T t, int i, int i2, int i3) {
        if (this.zztc) {
            return zza(t, i);
        }
        return (i2 & i3) != 0;
    }

    private final boolean zza(T t, int i) {
        if (this.zztc) {
            int zzar = zzar(i);
            long j = (long) (zzar & 1048575);
            switch ((zzar & 267386880) >>> 20) {
                case 0:
                    return zzhz.zzn(t, j) != FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
                case 1:
                    return zzhz.zzm(t, j) != 0.0f;
                case 2:
                    return zzhz.zzk(t, j) != 0;
                case 3:
                    return zzhz.zzk(t, j) != 0;
                case 4:
                    return zzhz.zzj(t, j) != 0;
                case 5:
                    return zzhz.zzk(t, j) != 0;
                case 6:
                    return zzhz.zzj(t, j) != 0;
                case 7:
                    return zzhz.zzl(t, j);
                case 8:
                    Object zzo = zzhz.zzo(t, j);
                    if (zzo instanceof String) {
                        return !((String) zzo).isEmpty();
                    }
                    if (zzo instanceof zzeb) {
                        return !zzeb.zzmv.equals(zzo);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzhz.zzo(t, j) != null;
                case 10:
                    return !zzeb.zzmv.equals(zzhz.zzo(t, j));
                case 11:
                    return zzhz.zzj(t, j) != 0;
                case 12:
                    return zzhz.zzj(t, j) != 0;
                case 13:
                    return zzhz.zzj(t, j) != 0;
                case 14:
                    return zzhz.zzk(t, j) != 0;
                case 15:
                    return zzhz.zzj(t, j) != 0;
                case 16:
                    return zzhz.zzk(t, j) != 0;
                case 17:
                    return zzhz.zzo(t, j) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int zzas = zzas(i);
            return (zzhz.zzj(t, (long) (zzas & 1048575)) & (1 << (zzas >>> 20))) != 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(byte[], long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, int):void */
    private final void zzb(T t, int i) {
        if (!this.zztc) {
            int zzas = zzas(i);
            long j = (long) (zzas & 1048575);
            zzhz.zza((Object) t, j, zzhz.zzj(t, j) | (1 << (zzas >>> 20)));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzhz.zzj(t, (long) (zzas(i2) & 1048575)) == i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(byte[], long, byte):void
      com.google.android.gms.internal.firebase-perf.zzhz.zza(java.lang.Object, long, int):void */
    private final void zzb(T t, int i, int i2) {
        zzhz.zza((Object) t, (long) (zzas(i2) & 1048575), i);
    }
}
