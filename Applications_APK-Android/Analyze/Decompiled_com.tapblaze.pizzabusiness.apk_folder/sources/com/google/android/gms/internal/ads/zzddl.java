package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzddh;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzddl implements zzdsa {
    static final zzdsa zzew = new zzddl();

    private zzddl() {
    }

    public final boolean zzf(int i) {
        return zzddh.zza.zzdp(i) != null;
    }
}
