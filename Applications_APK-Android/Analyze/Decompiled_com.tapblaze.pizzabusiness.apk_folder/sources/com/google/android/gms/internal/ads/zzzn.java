package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants;
import com.vungle.warren.AdLoader;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzzn {
    public static final zzzc<String> zzcgh = zzzc.zza(1, "gads:sdk_core_location:client:html", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/production/sdk-core-v40-impl.html");
    public static final zzzc<String> zzcgi = zzzc.zza(1, "gads:active_view_location:html", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/production/sdk-core-v40-impl.html");
    private static final zzaan<Boolean> zzcgj = zzabe.zzcgj;
    private static final zzzc<Integer> zzcgk = zzzc.zza(1, "gads:http_url_connection_factory:timeout_millis", 10000);
    public static final zzzc<String> zzcgl = zzzc.zza(1, "gads:video_exo_player:version", "3");
    public static final zzzc<Integer> zzcgm = zzzc.zza(1, "gads:video_exo_player:connect_timeout", 8000);
    public static final zzzc<Integer> zzcgn = zzzc.zza(1, "gads:video_exo_player:read_timeout", 8000);
    public static final zzzc<Integer> zzcgo = zzzc.zza(1, "gads:video_exo_player:loading_check_interval", 1048576);
    public static final zzzc<Integer> zzcgp = zzzc.zza(1, "gads:video_exo_player:exo_player_precache_limit", Integer.MAX_VALUE);
    public static final zzzc<Integer> zzcgq = zzzc.zza(1, "gads:video_exo_player:byte_buffer_precache_limit", Integer.MAX_VALUE);
    public static final zzzc<Integer> zzcgr = zzzc.zza(1, "gads:video_exo_player_socket_receive_buffer_size", 0);
    public static final zzzc<String> zzcgs = zzzc.zza(1, "gads:video_exo_player:precache_exceptions", "");
    public static final zzzc<Integer> zzcgt = zzzc.zza(1, "gads:video_exo_player:min_retry_count", -1);
    public static final zzzc<Integer> zzcgu = zzzc.zza(1, "gads:video_stream_cache:limit_count", 5);
    public static final zzzc<Integer> zzcgv = zzzc.zza(1, "gads:video_stream_cache:limit_space", 8388608);
    public static final zzzc<Integer> zzcgw = zzzc.zza(1, "gads:video_stream_exo_cache:buffer_size", 8388608);
    public static final zzzc<Long> zzcgx = zzzc.zza(1, "gads:video_stream_cache:limit_time_sec", 300L);
    public static final zzzc<Long> zzcgy = zzzc.zza(1, "gads:video_stream_cache:notify_interval_millis", 125L);
    public static final zzzc<Integer> zzcgz = zzzc.zza(1, "gads:video_stream_cache:connect_timeout_millis", 10000);
    public static final zzzc<String> zzcha = zzzc.zza(1, "gads:video:metric_frame_hash_times", "");
    public static final zzzc<Long> zzchb = zzzc.zza(1, "gads:video:metric_frame_hash_time_leniency", 500L);
    public static final zzzc<Boolean> zzchc = zzzc.zza(1, "gads:video:force_watermark", (Boolean) false);
    public static final zzzc<Long> zzchd = zzzc.zza(1, "gads:video:surface_update_min_spacing_ms", 1000L);
    public static final zzzc<Boolean> zzche = zzzc.zza(1, "gads:video:spinner:enabled", (Boolean) false);
    public static final zzzc<Integer> zzchf = zzzc.zza(1, "gads:video:spinner:scale", 4);
    public static final zzzc<Long> zzchg = zzzc.zza(1, "gads:video:spinner:jank_threshold_ms", 50L);
    public static final zzzc<Boolean> zzchh = zzzc.zza(1, "gads:video:aggressive_media_codec_release", (Boolean) false);
    public static final zzzc<Boolean> zzchi = zzzc.zza(1, "gads:memory_bundle:debug_info", (Boolean) false);
    public static final zzzc<String> zzchj = zzzc.zza(1, "gads:video:codec_query_mime_types", "");
    public static final zzzc<Integer> zzchk = zzzc.zza(1, "gads:video:codec_query_minimum_version", 16);
    public static final zzzc<String> zzchl = zzzc.zza(1, "gad:mraid:url_banner", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/production/mraid/v3/mraid_app_banner.js");
    public static final zzzc<String> zzchm = zzzc.zza(1, "gad:mraid:url_expanded_banner", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/production/mraid/v3/mraid_app_expanded_banner.js");
    public static final zzzc<String> zzchn = zzzc.zza(1, "gad:mraid:url_interstitial", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/production/mraid/v3/mraid_app_interstitial.js");
    public static final zzzc<String> zzcho = zzzc.zza(1, "gad:mraid:version", "3.0");
    public static final zzzc<Boolean> zzchp = zzzc.zza(1, "gads:mraid:expanded_interstitial_fix", (Boolean) false);
    public static final zzzc<Boolean> zzchq = zzzc.zza(1, "gads:mraid:initial_size_fallback", (Boolean) false);
    public static final zzzc<Integer> zzchr = zzzc.zza(1, "gads:content_vertical_fingerprint_number", 100);
    public static final zzzc<Integer> zzchs = zzzc.zza(1, "gads:content_vertical_fingerprint_bits", 23);
    public static final zzzc<Integer> zzcht = zzzc.zza(1, "gads:content_vertical_fingerprint_ngram", 3);
    public static final zzzc<String> zzchu = zzzc.zza(1, "gads:content_fetch_view_tag_id", "googlebot");
    public static final zzzc<String> zzchv = zzzc.zza(1, "gads:content_fetch_exclude_view_tag", Constants.ParametersKeys.ORIENTATION_NONE);
    public static final zzzc<Boolean> zzchw = zzzc.zza(1, "gads:content_fetch_disable_get_title_from_webview", (Boolean) false);
    public static final zzzc<Boolean> zzchx = zzzc.zza(1, "gads:content_fetch_enable_new_content_score", (Boolean) false);
    public static final zzzc<Boolean> zzchy = zzzc.zza(1, "gads:content_fetch_enable_serve_once", (Boolean) false);
    public static final zzzc<Boolean> zzchz = zzzc.zza(1, "gads:sai:enabled", (Boolean) true);
    public static final zzzc<String> zzcia = zzzc.zza(1, "gads:sai:click_ping_schema_v2", "^[^?]*(/aclk\\?|/pcs/click\\?).*");
    public static final zzzc<String> zzcib = zzzc.zza(1, "gads:sai:impression_ping_schema_v2", "^[^?]*/adview.*");
    public static final zzzc<Boolean> zzcic = zzzc.zza(1, "gads:sai:using_macro:enabled", (Boolean) false);
    public static final zzzc<String> zzcid = zzzc.zza(1, "gads:sai:ad_event_id_macro_name", "[gw_fbsaeid]");
    public static final zzzc<Long> zzcie = zzzc.zza(1, "gads:sai:timeout_ms", -1L);
    public static final zzzc<Integer> zzcif = zzzc.zza(1, "gads:sai:scion_thread_pool_size", 5);
    public static final zzzc<Boolean> zzcig = zzzc.zza(1, "gads:sai:app_measurement_enabled3", (Boolean) false);
    public static final zzzc<Boolean> zzcih = zzzc.zza(2, "app_measurement_enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcii = zzzc.zza(1, "gads:sai:force_through_reflection", (Boolean) true);
    public static final zzzc<Boolean> zzcij = zzzc.zza(1, "gads:sai:gmscore_availability_check_disabled", (Boolean) false);
    public static final zzzc<Boolean> zzcik = zzzc.zza(1, "gads:sai:logging_disabled_for_drx", (Boolean) false);
    public static final zzzc<Boolean> zzcil = zzzc.zza(1, "gads:sai:inject_firebase_proxy", (Boolean) false);
    public static final zzzc<Boolean> zzcim = zzzc.zza(1, "gads:sai:app_measurement_npa_enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcin = zzzc.zza(1, "gads:interstitial:app_must_be_foreground:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcio = zzzc.zza(1, "gads:interstitial:foreground_report:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcip = zzzc.zza(1, "gads:webview:error_web_response:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzciq = zzzc.zza(1, "gads:webview:pause_interstitial:enabled", (Boolean) true);
    private static final zzzc<Boolean> zzcir = zzzc.zza(1, "gads:rsku:webviewgone:kill_process:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcis = zzzc.zza(1, "gads:webviewgone:kill_process:enabled", (Boolean) false);
    private static final zzzc<Boolean> zzcit = zzzc.zza(1, "gads:rsku:webviewgone:new_onshow:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzciu = zzzc.zza(1, "gads:webviewgone:new_onshow:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzciv = zzzc.zza(1, "gads:webview:pause_resume:enabled", (Boolean) true);
    private static final zzzc<Boolean> zzciw = zzzc.zza(1, "gads:new_rewarded_ad:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcix = zzzc.zza(1, "gads:rewarded:adapter_initialization_enabled", (Boolean) false);
    private static final zzzc<Long> zzciy = zzzc.zza(1, "gads:rewarded:adapter_timeout_ms", 20000L);
    public static final zzzc<Boolean> zzciz = zzzc.zza(1, "gads:rewarded:ad_metadata_enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcja = zzzc.zza(1, "gads:rewarded:ssv_custom_data_enabled", (Boolean) true);
    public static final zzzc<Long> zzcjb = zzzc.zza(1, "gads:app_activity_tracker:notify_background_listeners_delay_ms", 500L);
    public static final zzzc<Long> zzcjc = zzzc.zza(1, "gads:app_activity_tracker:app_session_timeout_ms", TimeUnit.MINUTES.toMillis(5));
    public static final zzzc<Boolean> zzcjd = zzzc.zza(1, "gads:adid_values_in_adrequest:enabled", (Boolean) false);
    public static final zzzc<Long> zzcje = zzzc.zza(1, "gads:adid_values_in_adrequest:timeout", (long) AdLoader.RETRY_DELAY);
    public static final zzzc<Boolean> zzcjf = zzzc.zza(1, "gads:disable_adid_values_in_ms", (Boolean) false);
    public static final zzzc<Long> zzcjg = zzzc.zza(1, "gads:ad_overlay:delay_page_close_timeout_ms", 5000L);
    public static final zzzc<Boolean> zzcjh = zzzc.zza(1, "gads:custom_close_blocking:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcji = zzzc.zza(1, "gads:disabling_closable_area:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcjj = zzzc.zza(1, "gads:use_system_ui_for_fullscreen:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcjk = zzzc.zza(1, "gads:ad_overlay:collect_cutout_info:enabled", (Boolean) false);
    private static final zzzc<Boolean> zzcjl = zzzc.zza(1, "gads:impression_optimization_enabled", (Boolean) false);
    private static final zzzc<String> zzcjm = zzzc.zza(1, "gads:banner_ad_pool:schema", "customTargeting");
    private static final zzzc<Integer> zzcjn = zzzc.zza(1, "gads:banner_ad_pool:max_queues", 3);
    private static final zzzc<Integer> zzcjo = zzzc.zza(1, "gads:banner_ad_pool:max_pools", 3);
    public static final zzzc<Boolean> zzcjp = zzzc.zza(1, "gads:delay_banner_renderer:enabled", (Boolean) true);
    private static final zzzc<Boolean> zzcjq = zzzc.zza(1, "gads:interstitial_ad_pool:enabled", (Boolean) false);
    private static final zzzc<Boolean> zzcjr = zzzc.zza(1, "gads:interstitial_ad_pool:enabled_for_rewarded", (Boolean) false);
    private static final zzzc<String> zzcjs = zzzc.zza(1, "gads:interstitial_ad_pool:schema", "customTargeting");
    private static final zzzc<String> zzcjt = zzzc.zza(1, "gads:interstitial_ad_pool:request_exclusions", "com.google.ads.mediation.admob.AdMobAdapter/_ad");
    private static final zzzc<Integer> zzcju = zzzc.zza(1, "gads:interstitial_ad_pool:max_pools", 3);
    private static final zzzc<Integer> zzcjv = zzzc.zza(1, "gads:interstitial_ad_pool:max_pool_depth", 2);
    private static final zzzc<Integer> zzcjw = zzzc.zza(1, "gads:interstitial_ad_pool:time_limit_sec", (int) IronSourceConstants.RV_INSTANCE_LOAD_FAILED);
    private static final zzzc<String> zzcjx = zzzc.zza(1, "gads:interstitial_ad_pool:ad_unit_exclusions", "(?!)");
    private static final zzzc<Integer> zzcjy = zzzc.zza(1, "gads:interstitial_ad_pool:top_off_latency_min_millis", 0);
    private static final zzzc<Integer> zzcjz = zzzc.zza(1, "gads:interstitial_ad_pool:top_off_latency_range_millis", 0);
    private static final zzzc<Long> zzcka = zzzc.zza(1, "gads:interstitial_ad_pool:discard_thresholds", 0L);
    private static final zzzc<Long> zzckb = zzzc.zza(1, "gads:interstitial_ad_pool:miss_thresholds", 0L);
    private static final zzzc<Float> zzckc = zzzc.zza(1, "gads:interstitial_ad_pool:discard_asymptote", 0.0f);
    private static final zzzc<Float> zzckd = zzzc.zza(1, "gads:interstitial_ad_pool:miss_asymptote", 0.0f);
    public static final zzzc<String> zzcke = zzzc.zza(1, "gads:spherical_video:vertex_shader", "");
    public static final zzzc<String> zzckf = zzzc.zza(1, "gads:spherical_video:fragment_shader", "");
    public static final zzzc<Boolean> zzckg = zzzc.zza(1, "gads:include_local_global_rectangles", (Boolean) false);
    public static final zzzc<Long> zzckh = zzzc.zza(1, "gads:position_watcher:throttle_ms", 200L);
    public static final zzzc<Long> zzcki = zzzc.zza(1, "gads:position_watcher:scroll_aware_throttle_ms", 33L);
    public static final zzzc<Boolean> zzckj = zzzc.zza(1, "gads:position_watcher:enable_scroll_aware_ads", (Boolean) false);
    public static final zzzc<Boolean> zzckk = zzzc.zza(1, "gads:position_watcher:send_scroll_data", (Boolean) false);
    private static final zzzc<Boolean> zzckl = zzzc.zza(1, "gads:gen204_signals:enabled", (Boolean) false);
    public static final zzzc<Long> zzckm = zzzc.zza(1, "gads:rtb_v1_1:signal_timeout_ms", 1000L);
    public static final zzzc<Boolean> zzckn = zzzc.zza(1, "gads:rtb_signal:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcko = zzzc.zza(1, "gads:rtb_v1_1:cld:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzckp = zzzc.zza(1, "gads:rtb_v1_1:cld:check_regex:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzckq = zzzc.zza(1, "gads:v1_1:adapter_initialization:enabled", (Boolean) true);
    public static final zzzc<Integer> zzckr = zzzc.zza(1, "gads:adapter_initialization:min_sdk_version", 15301000);
    public static final zzzc<Long> zzcks = zzzc.zza(1, "gads:adapter_initialization:timeout", 30L);
    public static final zzzc<Long> zzckt = zzzc.zza(1, "gads:adapter_initialization:cld_timeout", 10L);
    public static final zzzc<Boolean> zzcku = zzzc.zza(1, "gads:initialization_csi:enabled", (Boolean) false);
    private static final zzzc<Boolean> zzckv = zzzc.zza(1, "gads:initialization_csi_control:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzckw = zzzc.zza(1, "gads:gestures:clearTd:enabled", (Boolean) false);
    public static final zzaan<Boolean> zzckx = zzabb.zzctw;
    public static final zzzc<Boolean> zzcky = zzzc.zza(1, "gads:gestures:errorlogging:enabled", (Boolean) false);
    public static final zzzc<Long> zzckz = zzzc.zza(1, "gads:gestures:task_timeout", (long) AdLoader.RETRY_DELAY);
    public static final zzzc<Boolean> zzcla = zzzc.zza(1, "gads:gestures:asig:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzclb = zzzc.zza(1, "gads:gestures:ans:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzclc = zzzc.zza(1, "gads:gestures:tos:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcld = zzzc.zza(1, "gads:gestures:brt:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcle = zzzc.zza(1, "gads:gestures:fpi:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzclf = zzzc.zza(1, "gads:signal:app_permissions:disabled", (Boolean) false);
    public static final zzzc<Boolean> zzclg = zzzc.zza(1, "gads:gestures:inthex:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzclh = zzzc.zza(1, "gads:gestures:hpk:enabled", (Boolean) true);
    public static final zzzc<String> zzcli = zzzc.zza(1, "gads:gestures:pk", "");
    public static final zzzc<Boolean> zzclj = zzzc.zza(1, "gads:gestures:bs:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzclk = zzzc.zza(1, "gads:gestures:check_initialization_thread:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcll = zzzc.zza(1, "gads:gestures:get_query_in_non_ui_thread:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzclm = zzzc.zza(1, "gads:gestures:init_new_thread:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcln = zzzc.zza(1, "gads:gestures:pds:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzclo = zzzc.zza(1, "gads:gestures:ns:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzclp = zzzc.zza(1, "gads:gestures:vdd:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzclq = zzzc.zza(1, "gads:native:asset_view_touch_events", (Boolean) false);
    public static final zzzc<Boolean> zzclr = zzzc.zza(1, "gads:native:set_touch_listener_on_asset_views", (Boolean) true);
    public static final zzzc<Boolean> zzcls = zzzc.zza(1, "gads:ais:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzclt = zzzc.zza(1, "gads:send_available_disk_space:enabled", (Boolean) false);
    private static final zzzc<String> zzclu;
    public static final zzzc<String> zzclv = zzzc.zza(1, "gads:sdk_core_constants:caps", "");
    private static final zzzc<Boolean> zzclw = zzzc.zza(1, "gads:js_flags:disable_phenotype", (Boolean) false);
    public static final zzzc<String> zzclx = zzzc.zza(1, "gads:native:engine_url_with_protocol", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/native_ads.html");
    private static final zzzc<String> zzcly = zzzc.zza(1, "gads:native:video_url", "//imasdk.googleapis.com/admob/sdkloader/native_video.html");
    public static final zzzc<String> zzclz = zzzc.zza(1, "gads:native:video_url_with_protocol", "https://imasdk.googleapis.com/admob/sdkloader/native_video.html");
    public static final zzzc<Integer> zzcma = zzzc.zza(1, "gads:native_video_load_timeout", 10);
    private static final zzzc<Integer> zzcmb = zzzc.zza(1, "gads:omid:native_webview_load_timeout", 2000);
    public static final zzzc<Boolean> zzcmc = zzzc.zza(1, "gads:enable_native_media_aspect_ratio", (Boolean) true);
    public static final zzzc<Boolean> zzcmd = zzzc.zza(1, "gads:native:media_content_main_image:enabled", (Boolean) true);
    public static final zzzc<String> zzcme = zzzc.zza(1, "gads:ad_choices_content_description", "Ad Choices Icon");
    private static final zzzc<Boolean> zzcmf = zzzc.zza(1, "gads:enable_store_active_view_state", (Boolean) false);
    public static final zzzc<Boolean> zzcmg = zzzc.zza(1, "gads:enable_singleton_broadcast_receiver", (Boolean) false);
    public static final zzzc<Boolean> zzcmh = zzzc.zza(1, "gads:native:media_view_match_parent:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcmi = zzzc.zza(1, "gads:native:count_impression_for_assets", (Boolean) false);
    private static final zzzc<Boolean> zzcmj = zzzc.zza(1, "gads:instream_ad:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcmk = zzzc.zza(1, "gads:fluid_ad:use_wrap_content_height", (Boolean) false);
    private static final zzzc<Boolean> zzcml = zzzc.zza(1, "gads:auto_location_for_coarse_permission", (Boolean) false);
    private static final zzzc<String> zzcmm = zzzc.zzb(1, "gads:auto_location_for_coarse_permission:experiment_id");
    public static final zzzc<Long> zzcmn = zzzc.zza(1, "gads:auto_location_timeout", (long) AdLoader.RETRY_DELAY);
    private static final zzzc<String> zzcmo = zzzc.zzb(1, "gads:auto_location_timeout:experiment_id");
    private static final zzzc<Long> zzcmp = zzzc.zza(1, "gads:auto_location_interval", -1L);
    private static final zzzc<String> zzcmq = zzzc.zzb(1, "gads:auto_location_interval:experiment_id");
    public static final zzzc<Boolean> zzcmr = zzzc.zza(1, "gads:rtb_v1_1:fetch_app_settings_using_cld:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcms = zzzc.zza(1, "gads:rtb_v1_1:use_manifest_appid_cld:enabled", (Boolean) true);
    public static final zzzc<Long> zzcmt = zzzc.zza(1, "gads:fetch_app_settings_using_cld:refresh_interval_ms", 7200000L);
    public static final zzzc<Long> zzcmu = zzzc.zza(1, "gads:parental_controls:timeout", (long) AdLoader.RETRY_DELAY);
    private static final zzzc<Integer> zzcmv = zzzc.zza(1, "gads:cache:ad_request_timeout_millis", (int) IronSourceConstants.INTERSTITIAL_DAILY_CAPPED);
    private static final zzzc<Integer> zzcmw = zzzc.zza(1, "gads:cache:max_concurrent_downloads", 10);
    private static final zzzc<Long> zzcmx = zzzc.zza(1, "gads:cache:javascript_timeout_millis", 5000L);
    public static final zzzc<Boolean> zzcmy = zzzc.zza(1, "gads:cache:bind_on_foreground", (Boolean) false);
    public static final zzzc<Boolean> zzcmz = zzzc.zza(1, "gads:cache:bind_on_init", (Boolean) false);
    public static final zzzc<Boolean> zzcna = zzzc.zza(1, "gads:cache:bind_on_request", (Boolean) false);
    public static final zzzc<Long> zzcnb = zzzc.zza(1, "gads:cache:bind_on_request_keep_alive", TimeUnit.SECONDS.toMillis(30));
    public static final zzzc<Boolean> zzcnc = zzzc.zza(1, "gads:cache:use_cache_data_source", (Boolean) false);
    public static final zzzc<Boolean> zzcnd = zzzc.zza(1, "gads:cache:connection_per_read", (Boolean) false);
    public static final zzzc<Long> zzcne = zzzc.zza(1, "gads:cache:connection_timeout", 5000L);
    public static final zzzc<Long> zzcnf = zzzc.zza(1, "gads:cache:read_only_connection_timeout", 5000L);
    public static final zzzc<Boolean> zzcng = zzzc.zza(1, "gads:http_assets_cache:enabled", (Boolean) false);
    public static final zzzc<String> zzcnh = zzzc.zza(1, "gads:http_assets_cache:regex", "(?i)https:\\/\\/(tpc\\.googlesyndication\\.com\\/(.*)|lh\\d+\\.googleusercontent\\.com\\/(.*))");
    public static final zzzc<Integer> zzcni = zzzc.zza(1, "gads:http_assets_cache:time_out", 100);
    public static final zzzc<Boolean> zzcnj = zzzc.zza(1, "gads:chrome_custom_tabs_browser:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcnk = zzzc.zza(1, "gads:chrome_custom_tabs:disabled", (Boolean) false);
    public static final zzzc<Long> zzcnl = zzzc.zza(1, "gads:debug_hold_gesture:time_millis", (long) AdLoader.RETRY_DELAY);
    public static final zzzc<String> zzcnm = zzzc.zza(1, "gads:drx_debug:debug_device_linking_url", "https://www.google.com/dfp/linkDevice");
    public static final zzzc<String> zzcnn = zzzc.zza(1, "gads:drx_debug:in_app_preview_status_url", "https://www.google.com/dfp/inAppPreview");
    public static final zzzc<String> zzcno = zzzc.zza(1, "gads:drx_debug:debug_signal_status_url", "https://www.google.com/dfp/debugSignals");
    public static final zzzc<String> zzcnp = zzzc.zza(1, "gads:drx_debug:send_debug_data_url", "https://www.google.com/dfp/sendDebugData");
    public static final zzzc<Integer> zzcnq = zzzc.zza(1, "gads:drx_debug:timeout_ms", 5000);
    public static final zzzc<Integer> zzcnr = zzzc.zza(1, "gad:pixel_dp_comparision_multiplier", 1);
    public static final zzzc<Boolean> zzcns = zzzc.zza(1, "gad:interstitial_for_multi_window", (Boolean) false);
    public static final zzzc<Boolean> zzcnt = zzzc.zza(1, "gad:interstitial_ad_stay_active_in_multi_window", (Boolean) false);
    public static final zzzc<Boolean> zzcnu = zzzc.zza(1, "gad:interstitial_multi_window_method", (Boolean) false);
    public static final zzzc<Integer> zzcnv = zzzc.zza(1, "gad:interstitial:close_button_padding_dip", 0);
    public static final zzzc<Boolean> zzcnw = zzzc.zza(1, "gads:clearcut_logging:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcnx = zzzc.zza(1, "gads:clearcut_logging:write_to_file", (Boolean) false);
    public static final zzzc<Boolean> zzcny = zzzc.zza(1, "gad:publisher_testing:force_local_request:enabled", (Boolean) true);
    public static final zzzc<String> zzcnz = zzzc.zza(1, "gad:publisher_testing:force_local_request:enabled_list", "");
    public static final zzzc<String> zzcoa = zzzc.zza(1, "gad:publisher_testing:force_local_request:disabled_list", "");
    public static final zzzc<Integer> zzcob = zzzc.zza(1, "gad:http_redirect_max_count:times", 8);
    public static final zzzc<Boolean> zzcoc = zzzc.zza(1, "gads:omid:enabled", (Boolean) true);
    public static final zzzc<Integer> zzcod = zzzc.zza(1, "gads:omid:destroy_webview_delay", 1000);
    private static final zzzc<Boolean> zzcoe = zzzc.zza(1, "gads:nonagon:banner:enabled", (Boolean) false);
    private static final zzzc<String> zzcof = zzzc.zza(1, "gads:nonagon:banner:ad_unit_exclusions", "(?!)");
    private static final zzzc<Boolean> zzcog = zzzc.zza(1, "gads:nonagon:app_open:enabled", (Boolean) true);
    public static final zzzc<Integer> zzcoh = zzzc.zza(1, "gads:app_open_beta:min_version", 99999999);
    private static final zzzc<String> zzcoi = zzzc.zza(1, "gads:nonagon:app_open:ad_unit_exclusions", "(?!)");
    private static final zzzc<Boolean> zzcoj = zzzc.zza(1, "gads:nonagon:interstitial:enabled", (Boolean) false);
    private static final zzzc<String> zzcok = zzzc.zza(1, "gads:nonagon:interstitial:ad_unit_exclusions", "(?!)");
    private static final zzzc<Boolean> zzcol = zzzc.zza(1, "gads:nonagon:rewardedvideo:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcom = zzzc.zza(1, "gads:nonagon:mobile_ads_setting_manager:enabled", (Boolean) false);
    public static final zzzc<String> zzcon = zzzc.zza(1, "gads:nonagon:rewardedvideo:ad_unit_exclusions", "(?!)");
    private static final zzzc<Boolean> zzcoo = zzzc.zza(1, "gads:nonagon:nativead:enabled", (Boolean) false);
    private static final zzzc<String> zzcop = zzzc.zza(1, "gads:nonagon:nativead:app_name", "(?!)");
    public static final zzzc<Boolean> zzcoq = zzzc.zza(1, "gads:nonagon:banner:check_dp_size", (Boolean) true);
    public static final zzzc<Boolean> zzcor = zzzc.zza(1, "gads:nonagon:rewarded:load_multiple_ads", (Boolean) true);
    private static final zzzc<Boolean> zzcos = zzzc.zza(1, "gads:nonagon:return_last_error_code", (Boolean) false);
    public static final zzzc<Boolean> zzcot = zzzc.zza(1, "gads:nonagon:return_no_fill_error_code", (Boolean) false);
    public static final zzzc<Boolean> zzcou = zzzc.zza(1, "gads:nonagon:continue_on_no_fill", (Boolean) false);
    private static final zzzc<Boolean> zzcov = zzzc.zza(1, "gads:nonagon:open_not_loaded_interstitial", (Boolean) true);
    public static final zzzc<Boolean> zzcow = zzzc.zza(1, "gads:nonagon:separate_timeout:enabled", (Boolean) true);
    public static final zzzc<Integer> zzcox = zzzc.zza(1, "gads:nonagon:request_timeout:seconds", 60);
    public static final zzzc<Boolean> zzcoy = zzzc.zza(1, "gads:nonagon:banner_recursive_renderer", (Boolean) false);
    public static final zzzc<Boolean> zzcoz = zzzc.zza(1, "gads:nonagon:app_stats_lock:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpa = zzzc.zza(1, "gads:nonagon:app_stats_main_thread:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpb = zzzc.zza(1, "gads:nonagon:active_view_gmsg_background_thread:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcpc = zzzc.zza(1, "gads:active_view_gmsg_separate_pool:enabled", (Boolean) true);
    private static final zzzc<Boolean> zzcpd = zzzc.zza(1, "gads:nonagon:service:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcpe = zzzc.zza(1, "gads:nonagon:dpl_cancel_destroy_webview:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpf = zzzc.zza(1, "gads:signals:ad_id_info:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpg = zzzc.zza(1, "gads:signals:app_index:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcph = zzzc.zza(1, "gads:signals:cache:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpi = zzzc.zza(1, "gads:signals:doritos:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpj = zzzc.zza(1, "gads:signals:doritos:v1:enabled", (Boolean) false);
    private static final zzzc<Boolean> zzcpk = zzzc.zza(1, "gads:signals:doritos:v2:immediate:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpl = zzzc.zza(1, "gads:signals:location:enabled", (Boolean) false);
    private static final zzzc<Boolean> zzcpm = zzzc.zza(1, "gads:signals:network_prediction:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpn = zzzc.zza(1, "gads:signals:parental_control:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpo = zzzc.zza(1, "gads:signals:video_decoder:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcpp = zzzc.zza(1, "gads:signals:signals_on_service:enabled", (Boolean) false);
    public static final zzzc<Long> zzcpq = zzzc.zza(1, "gads:mobius_linking:sdk_side_cooldown_time_threshold:ms", 3600000L);
    public static final zzzc<Integer> zzcpr = zzzc.zza(1, "gads:adoverlay:b68684796:targeting_sdk:lower_bound", 27);
    public static final zzzc<Integer> zzcps = zzzc.zza(1, "gads:adoverlay:b68684796:targeting_sdk:upper_bound", 26);
    public static final zzzc<Integer> zzcpt = zzzc.zza(1, "gads:adoverlay:b68684796:sdk_int:lower_bound", 27);
    public static final zzzc<Integer> zzcpu = zzzc.zza(1, "gads:adoverlay:b68684796:sdk_int:upper_bound", 26);
    private static final zzaan<Boolean> zzcpv = zzabg.zzcue;
    public static final zzzc<Boolean> zzcpw = zzzc.zza(1, "gads:consent:shared_preference_reading:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcpx = zzzc.zza(1, "gads:consent:iab_consent_info:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcpy = zzzc.zza(1, "gads:fc_consent:shared_preference_reading:enabled", (Boolean) true);
    private static final zzzc<String> zzcpz = zzzc.zza(1, "gads:sp:json_string", "");
    public static final zzzc<Boolean> zzcqa = zzzc.zza(1, "gads:nativeads:image:sample:enabled", (Boolean) true);
    public static final zzzc<Integer> zzcqb = zzzc.zza(1, "gads:nativeads:image:sample:pixels", 1048576);
    public static final zzzc<Boolean> zzcqc = zzzc.zza(1, "gads:nativeads:overlay_webview:onclick", (Boolean) false);
    public static final zzzc<Boolean> zzcqd = zzzc.zza(1, "gads:nativeads:create_overlay_webview:onjson", (Boolean) false);
    public static final zzzc<Boolean> zzcqe = zzzc.zza(1, "gads:nativeads:pub_image_scale_type:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcqf = zzzc.zza(1, "gads:offline_signaling:enabled", (Boolean) false);
    public static final zzzc<Integer> zzcqg = zzzc.zza(1, "gads:offline_signaling:log_maximum", 100);
    public static final zzzc<Boolean> zzcqh = zzzc.zza(1, "gads:nativeads:template_signal:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcqi = zzzc.zza(1, "gads:nativeads:media_content_aspect_ratio:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcqj = zzzc.zza(1, "gads:ar_ads:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcqk = zzzc.zza(1, "gads:precache_pool:verbose_logging", (Boolean) false);
    public static final zzzc<Integer> zzcql = zzzc.zza(1, "gads:rewarded_precache_pool:count", 0);
    public static final zzzc<Integer> zzcqm = zzzc.zza(1, "gads:interstitial_precache_pool:count", 0);
    public static final zzzc<String> zzcqn = zzzc.zza(1, "gads:rewarded_precache_pool:discard_strategy", "lru");
    public static final zzzc<String> zzcqo = zzzc.zza(1, "gads:interstitial_precache_pool:discard_strategy", "lru");
    public static final zzzc<String> zzcqp = zzzc.zza(1, "gads:rewarded_precache_pool:cache_start_trigger", "onAdClosed");
    public static final zzzc<String> zzcqq = zzzc.zza(1, "gads:interstitial_precache_pool:cache_start_trigger", "onAdClosed");
    public static final zzzc<Integer> zzcqr = zzzc.zza(1, "gads:rewarded_precache_pool:size", 1);
    public static final zzzc<Integer> zzcqs = zzzc.zza(1, "gads:interstitial_precache_pool:size", 1);
    public static final zzzc<Integer> zzcqt = zzzc.zza(1, "gads:rewarded_precache_pool:ad_time_limit", (int) IronSourceConstants.RV_INSTANCE_LOAD_FAILED);
    public static final zzzc<Integer> zzcqu = zzzc.zza(1, "gads:interstitial_precache_pool:ad_time_limit", (int) IronSourceConstants.RV_INSTANCE_LOAD_FAILED);
    public static final zzzc<String> zzcqv = zzzc.zza(1, "gads:rewarded_precache_pool:schema", "customTargeting,npa,tagForChildDirectedTreatment,tagForUnderAgeOfConsent,maxAdContentRating");
    public static final zzzc<String> zzcqw = zzzc.zza(1, "gads:interstitial_precache_pool:schema", "customTargeting,npa,tagForChildDirectedTreatment,tagForUnderAgeOfConsent,maxAdContentRating");
    public static final zzzc<String> zzcqx = zzzc.zza(1, "gads:app_open_precache_pool:schema", "orientation,npa,tagForChildDirectedTreatment,tagForUnderAgeOfConsent,maxAdContentRating");
    public static final zzzc<String> zzcqy = zzzc.zza(1, "gads:app_open_precache_pool:discard_strategy", "oldest");
    public static final zzzc<Integer> zzcqz = zzzc.zza(1, "gads:app_open_precache_pool:count", 0);
    public static final zzzc<String> zzcra = zzzc.zza(1, "gads:app_open_precache_pool:cache_start_trigger", "onAdClosed");
    public static final zzzc<Integer> zzcrb = zzzc.zza(1, "gads:app_open_precache_pool:size", 1);
    public static final zzzc<Integer> zzcrc = zzzc.zza(1, "gads:app_open_precache_pool:ad_time_limit", 14400);
    public static final zzzc<Boolean> zzcrd = zzzc.zza(1, "gads:memory_leak:b129558083", (Boolean) false);
    public static final zzzc<Boolean> zzcre = zzzc.zza(1, "gads:unhandled_event_reporting:enabled", (Boolean) false);
    public static final zzzc<Boolean> zzcrf = zzzc.zza(1, "gads:response_info:enabled", (Boolean) true);
    public static final zzzc<Boolean> zzcrg = zzzc.zza(1, "gads:msa:experiments:enabled", (Boolean) false);

    public static void initialize(Context context) {
        zzayc.zza(new zzzq(context));
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [com.google.android.gms.internal.ads.zzabq, com.google.android.gms.internal.ads.zzzh] */
    public static void zza(Context context, int i, JSONObject jSONObject) {
        zzve.zzow();
        SharedPreferences.Editor edit = context.getSharedPreferences("google_ads_flags", 0).edit();
        zzaat.zza(new zzzh(zzve.zzox(), edit, jSONObject));
        zzve.zzox().zza(edit, 1, jSONObject);
        zzve.zzow();
        edit.commit();
    }

    public static List<String> zzqg() {
        return zzve.zzox().zzqg();
    }

    public static List<String> zzqh() {
        return zzve.zzox().zzqh();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, long):com.google.android.gms.internal.ads.zzzc<java.lang.Long>
     arg types: [int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, float):com.google.android.gms.internal.ads.zzzc<java.lang.Float>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, int):com.google.android.gms.internal.ads.zzzc<java.lang.Integer>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, java.lang.Boolean):com.google.android.gms.internal.ads.zzzc<java.lang.Boolean>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, java.lang.String):com.google.android.gms.internal.ads.zzzc<java.lang.String>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, long):com.google.android.gms.internal.ads.zzzc<java.lang.Long> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, java.lang.Boolean):com.google.android.gms.internal.ads.zzzc<java.lang.Boolean>
     arg types: [int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, float):com.google.android.gms.internal.ads.zzzc<java.lang.Float>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, int):com.google.android.gms.internal.ads.zzzc<java.lang.Integer>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, long):com.google.android.gms.internal.ads.zzzc<java.lang.Long>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, java.lang.String):com.google.android.gms.internal.ads.zzzc<java.lang.String>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, java.lang.Boolean):com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, float):com.google.android.gms.internal.ads.zzzc<java.lang.Float>
     arg types: [int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, int):com.google.android.gms.internal.ads.zzzc<java.lang.Integer>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, long):com.google.android.gms.internal.ads.zzzc<java.lang.Long>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, java.lang.Boolean):com.google.android.gms.internal.ads.zzzc<java.lang.Boolean>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, java.lang.String):com.google.android.gms.internal.ads.zzzc<java.lang.String>
      com.google.android.gms.internal.ads.zzzc.zza(int, java.lang.String, float):com.google.android.gms.internal.ads.zzzc<java.lang.Float> */
    static {
        zzzc<String> zza = zzzc.zza(1, "gads:sdk_core_constants:experiment_id", (String) null);
        zzve.zzox().zzb(zza);
        zzclu = zza;
    }
}
