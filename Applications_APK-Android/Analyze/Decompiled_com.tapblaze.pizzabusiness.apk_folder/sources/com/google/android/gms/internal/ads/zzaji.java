package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaji implements zzazp<zzajq> {
    zzaji(zzajf zzajf) {
    }

    public final /* synthetic */ void zzh(Object obj) {
        zzavs.zzed("Ending javascript session.");
        ((zzajp) ((zzajq) obj)).zzsg();
    }
}
