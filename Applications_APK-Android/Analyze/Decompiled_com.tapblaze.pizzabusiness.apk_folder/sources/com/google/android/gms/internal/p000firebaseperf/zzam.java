package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzam  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzam extends zzaz<String> {
    private static zzam zzaq;

    protected zzam() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.SdkDisabledVersions";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_disabled_android_versions";
    }

    protected static synchronized zzam zzao() {
        zzam zzam;
        synchronized (zzam.class) {
            if (zzaq == null) {
                zzaq = new zzam();
            }
            zzam = zzaq;
        }
        return zzam;
    }
}
