package com.google.android.gms.internal.ads;

import com.flurry.android.Constants;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzom {
    private byte[] data;
    private int zzbgp;
    private int zzbgq = 0;
    private int zzbgr;

    public zzom(byte[] bArr, int i, int i2) {
        this.data = bArr;
        this.zzbgp = i;
        this.zzbgr = i2;
        zziy();
    }

    public final void zzbi(int i) {
        int i2 = this.zzbgp;
        this.zzbgp = (i / 8) + i2;
        this.zzbgq += i % 8;
        int i3 = this.zzbgq;
        if (i3 > 7) {
            this.zzbgp++;
            this.zzbgq = i3 - 8;
        }
        while (true) {
            i2++;
            if (i2 > this.zzbgp) {
                zziy();
                return;
            } else if (zzbj(i2)) {
                this.zzbgp++;
                i2 += 2;
            }
        }
    }

    public final boolean zziu() {
        return zzbh(1) == 1;
    }

    public final int zzbh(int i) {
        byte b;
        byte b2;
        if (i == 0) {
            return 0;
        }
        int i2 = i / 8;
        byte b3 = 0;
        for (int i3 = 0; i3 < i2; i3++) {
            int i4 = zzbj(this.zzbgp + 1) ? this.zzbgp + 2 : this.zzbgp + 1;
            int i5 = this.zzbgq;
            if (i5 != 0) {
                byte[] bArr = this.data;
                b2 = ((bArr[i4] & Constants.UNKNOWN) >>> (8 - i5)) | ((bArr[this.zzbgp] & Constants.UNKNOWN) << i5);
            } else {
                b2 = this.data[this.zzbgp];
            }
            i -= 8;
            b3 |= (255 & b2) << i;
            this.zzbgp = i4;
        }
        if (i > 0) {
            int i6 = this.zzbgq + i;
            byte b4 = (byte) (255 >> (8 - i));
            int i7 = zzbj(this.zzbgp + 1) ? this.zzbgp + 2 : this.zzbgp + 1;
            if (i6 > 8) {
                byte[] bArr2 = this.data;
                b = (b4 & (((255 & bArr2[i7]) >> (16 - i6)) | ((bArr2[this.zzbgp] & Constants.UNKNOWN) << (i6 - 8)))) | b3;
                this.zzbgp = i7;
            } else {
                b = (b4 & ((255 & this.data[this.zzbgp]) >> (8 - i6))) | b3;
                if (i6 == 8) {
                    this.zzbgp = i7;
                }
            }
            b3 = b;
            this.zzbgq = i6 % 8;
        }
        zziy();
        return b3;
    }

    public final int zziv() {
        return zzix();
    }

    public final int zziw() {
        int zzix = zzix();
        return (zzix % 2 == 0 ? -1 : 1) * ((zzix + 1) / 2);
    }

    private final int zzix() {
        int i = 0;
        int i2 = 0;
        while (!zziu()) {
            i2++;
        }
        int i3 = (1 << i2) - 1;
        if (i2 > 0) {
            i = zzbh(i2);
        }
        return i3 + i;
    }

    private final boolean zzbj(int i) {
        if (2 > i || i >= this.zzbgr) {
            return false;
        }
        byte[] bArr = this.data;
        return bArr[i] == 3 && bArr[i + -2] == 0 && bArr[i - 1] == 0;
    }

    private final void zziy() {
        int i;
        int i2;
        int i3 = this.zzbgp;
        zzoc.checkState(i3 >= 0 && (i = this.zzbgq) >= 0 && i < 8 && (i3 < (i2 = this.zzbgr) || (i3 == i2 && i == 0)));
    }
}
