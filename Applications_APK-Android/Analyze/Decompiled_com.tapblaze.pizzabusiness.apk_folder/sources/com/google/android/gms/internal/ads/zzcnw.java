package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcnw implements zzcnq<zzbtu> {
    private final zzbup zzfyt;
    private final Context zzup;

    public zzcnw(Context context, zzbup zzbup) {
        this.zzup = context;
        this.zzfyt = zzbup;
    }

    public final /* synthetic */ Object zza(zzczt zzczt, zzczl zzczl, View view, zzcnt zzcnt) {
        zzbtw zza = this.zzfyt.zza(new zzbmt(zzczt, zzczl, null), new zzcny(this, zzcnv.zzgcc));
        zzcnt.zza(new zzcnx(this, zza));
        return zza.zzaem();
    }
}
