package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzlz extends zzmn {
    long zza(zzne[] zzneArr, boolean[] zArr, zzmo[] zzmoArr, boolean[] zArr2, long j);

    void zza(zzmc zzmc, long j);

    void zzee(long j);

    boolean zzef(long j);

    long zzeg(long j);

    void zzhf() throws IOException;

    zzmr zzhg();

    long zzhh();

    long zzhi();

    long zzhj();
}
