package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcho {
    /* access modifiers changed from: private */
    public final zzaqq zzfwi;
    /* access modifiers changed from: private */
    public final JSONObject zzfwj;

    public zzcho(JSONObject jSONObject, zzaqq zzaqq) {
        this.zzfwj = jSONObject;
        this.zzfwi = zzaqq;
    }
}
