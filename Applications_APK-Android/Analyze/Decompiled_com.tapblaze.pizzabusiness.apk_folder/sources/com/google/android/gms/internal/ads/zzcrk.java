package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import com.google.android.gms.internal.ads.zzcty;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcrk<S extends zzcty<?>> implements zzcub<S> {
    private final Clock zzbmq;
    private final AtomicReference<zzcrn<S>> zzgfm = new AtomicReference<>();
    private final zzcub<S> zzgfn;
    private final long zzgfo;

    public zzcrk(zzcub<S> zzcub, long j, Clock clock) {
        this.zzbmq = clock;
        this.zzgfn = zzcub;
        this.zzgfo = j;
    }

    public final zzdhe<S> zzanc() {
        zzcrn zzcrn = this.zzgfm.get();
        if (zzcrn == null || zzcrn.hasExpired()) {
            zzcrn = new zzcrn(this.zzgfn.zzanc(), this.zzgfo, this.zzbmq);
            this.zzgfm.set(zzcrn);
        }
        return zzcrn.zzgfq;
    }
}
