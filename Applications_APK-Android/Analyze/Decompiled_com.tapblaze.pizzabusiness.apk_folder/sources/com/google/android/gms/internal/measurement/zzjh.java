package com.google.android.gms.internal.measurement;

import android.support.v4.media.session.PlaybackStateCompat;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzjh implements zzje {
    private static final zzcn<Long> zza;
    private static final zzcn<Long> zzaa;
    private static final zzcn<Long> zzab;
    private static final zzcn<Long> zzac;
    private static final zzcn<Long> zzad;
    private static final zzcn<Long> zzae;
    private static final zzcn<Long> zzaf;
    private static final zzcn<Long> zzag;
    private static final zzcn<Long> zzah;
    private static final zzcn<String> zzai;
    private static final zzcn<Long> zzaj;
    private static final zzcn<Long> zzb;
    private static final zzcn<String> zzc;
    private static final zzcn<String> zzd;
    private static final zzcn<String> zze;
    private static final zzcn<Long> zzf;
    private static final zzcn<Long> zzg;
    private static final zzcn<Long> zzh;
    private static final zzcn<Long> zzi;
    private static final zzcn<Long> zzj;
    private static final zzcn<Long> zzk;
    private static final zzcn<Long> zzl;
    private static final zzcn<Long> zzm;
    private static final zzcn<Long> zzn;
    private static final zzcn<Long> zzo;
    private static final zzcn<Long> zzp;
    private static final zzcn<Long> zzq;
    private static final zzcn<String> zzr;
    private static final zzcn<Long> zzs;
    private static final zzcn<Long> zzt;
    private static final zzcn<Long> zzu;
    private static final zzcn<Long> zzv;
    private static final zzcn<Long> zzw;
    private static final zzcn<Long> zzx;
    private static final zzcn<Long> zzy;
    private static final zzcn<Long> zzz;

    public final long zza() {
        return zza.zzc().longValue();
    }

    public final long zzb() {
        return zzb.zzc().longValue();
    }

    public final String zzc() {
        return zzd.zzc();
    }

    public final String zzd() {
        return zze.zzc();
    }

    public final long zze() {
        return zzf.zzc().longValue();
    }

    public final long zzf() {
        return zzg.zzc().longValue();
    }

    public final long zzg() {
        return zzh.zzc().longValue();
    }

    public final long zzh() {
        return zzi.zzc().longValue();
    }

    public final long zzi() {
        return zzj.zzc().longValue();
    }

    public final long zzj() {
        return zzk.zzc().longValue();
    }

    public final long zzk() {
        return zzl.zzc().longValue();
    }

    public final long zzl() {
        return zzm.zzc().longValue();
    }

    public final long zzm() {
        return zzn.zzc().longValue();
    }

    public final long zzn() {
        return zzo.zzc().longValue();
    }

    public final long zzo() {
        return zzq.zzc().longValue();
    }

    public final long zzp() {
        return zzs.zzc().longValue();
    }

    public final long zzq() {
        return zzt.zzc().longValue();
    }

    public final long zzr() {
        return zzu.zzc().longValue();
    }

    public final long zzs() {
        return zzv.zzc().longValue();
    }

    public final long zzt() {
        return zzw.zzc().longValue();
    }

    public final long zzu() {
        return zzx.zzc().longValue();
    }

    public final long zzv() {
        return zzy.zzc().longValue();
    }

    public final long zzw() {
        return zzz.zzc().longValue();
    }

    public final long zzx() {
        return zzaa.zzc().longValue();
    }

    public final long zzy() {
        return zzab.zzc().longValue();
    }

    public final long zzz() {
        return zzac.zzc().longValue();
    }

    public final long zzaa() {
        return zzad.zzc().longValue();
    }

    public final long zzab() {
        return zzae.zzc().longValue();
    }

    public final long zzac() {
        return zzaf.zzc().longValue();
    }

    public final long zzad() {
        return zzag.zzc().longValue();
    }

    public final long zzae() {
        return zzah.zzc().longValue();
    }

    public final String zzaf() {
        return zzai.zzc();
    }

    public final long zzag() {
        return zzaj.zzc().longValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, long):com.google.android.gms.internal.measurement.zzcn<java.lang.Long>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, double):com.google.android.gms.internal.measurement.zzcn<java.lang.Double>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.zzcn<java.lang.String>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, boolean):com.google.android.gms.internal.measurement.zzcn<java.lang.Boolean>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, long):com.google.android.gms.internal.measurement.zzcn<java.lang.Long> */
    static {
        zzct zzct = new zzct(zzck.zza("com.google.android.gms.measurement"));
        zza = zzct.zza("measurement.ad_id_cache_time", 10000L);
        zzb = zzct.zza("measurement.config.cache_time", 86400000L);
        zzc = zzct.zza("measurement.log_tag", "FA");
        zzd = zzct.zza("measurement.config.url_authority", "app-measurement.com");
        zze = zzct.zza("measurement.config.url_scheme", "https");
        zzf = zzct.zza("measurement.upload.debug_upload_interval", 1000L);
        zzg = zzct.zza("measurement.lifetimevalue.max_currency_tracked", 4L);
        zzh = zzct.zza("measurement.store.max_stored_events_per_app", 100000L);
        zzi = zzct.zza("measurement.experiment.max_ids", 50L);
        zzj = zzct.zza("measurement.audience.filter_result_max_count", 200L);
        zzk = zzct.zza("measurement.alarm_manager.minimum_interval", 60000L);
        zzl = zzct.zza("measurement.upload.minimum_delay", 500L);
        zzm = zzct.zza("measurement.monitoring.sample_period_millis", 86400000L);
        zzn = zzct.zza("measurement.upload.realtime_upload_interval", 10000L);
        zzo = zzct.zza("measurement.upload.refresh_blacklisted_config_interval", 604800000L);
        zzp = zzct.zza("measurement.config.cache_time.service", 3600000L);
        zzq = zzct.zza("measurement.service_client.idle_disconnect_millis", 5000L);
        zzr = zzct.zza("measurement.log_tag.service", "FA-SVC");
        zzs = zzct.zza("measurement.upload.stale_data_deletion_interval", 86400000L);
        zzt = zzct.zza("measurement.upload.backoff_period", 43200000L);
        zzu = zzct.zza("measurement.upload.initial_upload_delay_time", 15000L);
        zzv = zzct.zza("measurement.upload.interval", 3600000L);
        zzw = zzct.zza("measurement.upload.max_bundle_size", (long) PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH);
        zzx = zzct.zza("measurement.upload.max_bundles", 100L);
        zzy = zzct.zza("measurement.upload.max_conversions_per_day", 500L);
        zzz = zzct.zza("measurement.upload.max_error_events_per_day", 1000L);
        zzaa = zzct.zza("measurement.upload.max_events_per_bundle", 1000L);
        zzab = zzct.zza("measurement.upload.max_events_per_day", 100000L);
        zzac = zzct.zza("measurement.upload.max_public_events_per_day", 50000L);
        zzad = zzct.zza("measurement.upload.max_queue_time", 2419200000L);
        zzae = zzct.zza("measurement.upload.max_realtime_events_per_day", 10L);
        zzaf = zzct.zza("measurement.upload.max_batch_size", (long) PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH);
        zzag = zzct.zza("measurement.upload.retry_count", 6L);
        zzah = zzct.zza("measurement.upload.retry_time", 1800000L);
        zzai = zzct.zza("measurement.upload.url", "https://app-measurement.com/a");
        zzaj = zzct.zza("measurement.upload.window_interval", 3600000L);
    }
}
