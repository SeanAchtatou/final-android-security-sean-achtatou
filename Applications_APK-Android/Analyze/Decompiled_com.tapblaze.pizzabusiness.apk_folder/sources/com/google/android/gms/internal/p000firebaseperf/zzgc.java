package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgc  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzgc implements zzhe {
    private static final zzgm zzsi = new zzgb();
    private final zzgm zzsh;

    public zzgc() {
        this(new zzge(zzfd.zzhk(), zzhz()));
    }

    private zzgc(zzgm zzgm) {
        this.zzsh = (zzgm) zzfg.checkNotNull(zzgm, "messageInfoFactory");
    }

    public final <T> zzhb<T> zzd(Class<T> cls) {
        zzhd.zzf((Class<?>) cls);
        zzgj zzc = this.zzsh.zzc(cls);
        if (zzc.zzif()) {
            if (zzfc.class.isAssignableFrom(cls)) {
                return zzgs.zza(zzhd.zzit(), zzeu.zzgx(), zzc.zzig());
            }
            return zzgs.zza(zzhd.zzir(), zzeu.zzgy(), zzc.zzig());
        } else if (zzfc.class.isAssignableFrom(cls)) {
            if (zza(zzc)) {
                return zzgp.zza(cls, zzc, zzgw.zzil(), zzfv.zzhy(), zzhd.zzit(), zzeu.zzgx(), zzgk.zzii());
            }
            return zzgp.zza(cls, zzc, zzgw.zzil(), zzfv.zzhy(), zzhd.zzit(), null, zzgk.zzii());
        } else if (zza(zzc)) {
            return zzgp.zza(cls, zzc, zzgw.zzik(), zzfv.zzhx(), zzhd.zzir(), zzeu.zzgy(), zzgk.zzih());
        } else {
            return zzgp.zza(cls, zzc, zzgw.zzik(), zzfv.zzhx(), zzhd.zzis(), null, zzgk.zzih());
        }
    }

    private static boolean zza(zzgj zzgj) {
        return zzgj.zzie() == zzgx.zzto;
    }

    private static zzgm zzhz() {
        try {
            return (zzgm) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return zzsi;
        }
    }
}
