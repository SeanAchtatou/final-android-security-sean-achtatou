package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzxs implements Runnable {
    private final zzxq zzcex;
    private final OnInitializationCompleteListener zzcfe;

    zzxs(zzxq zzxq, OnInitializationCompleteListener onInitializationCompleteListener) {
        this.zzcex = zzxq;
        this.zzcfe = onInitializationCompleteListener;
    }

    public final void run() {
        this.zzcex.zza(this.zzcfe);
    }
}
