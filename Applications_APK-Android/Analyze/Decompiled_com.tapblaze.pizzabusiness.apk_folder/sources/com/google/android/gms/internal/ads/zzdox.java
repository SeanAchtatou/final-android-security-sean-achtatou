package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public enum zzdox {
    NIST_P256,
    NIST_P384,
    NIST_P521
}
