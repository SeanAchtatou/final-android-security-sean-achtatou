package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbwp implements zzdxg<zzbwq> {
    private final zzdxp<zzbwi> zzfly;

    private zzbwp(zzdxp<zzbwi> zzdxp) {
        this.zzfly = zzdxp;
    }

    public static zzbwp zzx(zzdxp<zzbwi> zzdxp) {
        return new zzbwp(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbwq(this.zzfly.get());
    }
}
