package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaik implements Runnable {
    private final String zzcyr;
    private final zzaih zzczc;

    zzaik(zzaih zzaih, String str) {
        this.zzczc = zzaih;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzczc.zzdd(this.zzcyr);
    }
}
