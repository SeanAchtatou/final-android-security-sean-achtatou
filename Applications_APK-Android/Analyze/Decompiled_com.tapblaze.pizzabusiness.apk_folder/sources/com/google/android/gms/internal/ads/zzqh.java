package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.internal.zzq;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzqh implements Application.ActivityLifecycleCallbacks {
    /* access modifiers changed from: private */
    public boolean foreground = true;
    /* access modifiers changed from: private */
    public final Object lock = new Object();
    /* access modifiers changed from: private */
    public boolean zzbpe = false;
    /* access modifiers changed from: private */
    public final List<zzqj> zzbpf = new ArrayList();
    private final List<zzqu> zzbpg = new ArrayList();
    private Runnable zzbph;
    private long zzbpi;
    private Context zzup;
    private boolean zzxx = false;
    private Activity zzzk;

    zzqh() {
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    public final void zza(Application application, Context context) {
        if (!this.zzxx) {
            application.registerActivityLifecycleCallbacks(this);
            if (context instanceof Activity) {
                setActivity((Activity) context);
            }
            this.zzup = application;
            this.zzbpi = ((Long) zzve.zzoy().zzd(zzzn.zzcjb)).longValue();
            this.zzxx = true;
        }
    }

    public final void zza(zzqj zzqj) {
        synchronized (this.lock) {
            this.zzbpf.add(zzqj);
        }
    }

    public final void zzb(zzqj zzqj) {
        synchronized (this.lock) {
            this.zzbpf.remove(zzqj);
        }
    }

    public final Activity getActivity() {
        return this.zzzk;
    }

    public final Context getContext() {
        return this.zzup;
    }

    public final void onActivityStarted(Activity activity) {
        setActivity(activity);
    }

    public final void onActivityResumed(Activity activity) {
        setActivity(activity);
        this.zzbpe = false;
        boolean z = !this.foreground;
        this.foreground = true;
        if (this.zzbph != null) {
            zzawb.zzdsr.removeCallbacks(this.zzbph);
        }
        synchronized (this.lock) {
            for (zzqu onActivityResumed : this.zzbpg) {
                try {
                    onActivityResumed.onActivityResumed(activity);
                } catch (Exception e) {
                    zzq.zzku().zza(e, "AppActivityTracker.ActivityListener.onActivityResumed");
                    zzayu.zzc("", e);
                }
            }
            if (z) {
                for (zzqj zzp : this.zzbpf) {
                    try {
                        zzp.zzp(true);
                    } catch (Exception e2) {
                        zzayu.zzc("", e2);
                    }
                }
            } else {
                zzavs.zzea("App is still foreground.");
            }
        }
    }

    public final void onActivityPaused(Activity activity) {
        setActivity(activity);
        synchronized (this.lock) {
            for (zzqu onActivityPaused : this.zzbpg) {
                try {
                    onActivityPaused.onActivityPaused(activity);
                } catch (Exception e) {
                    zzq.zzku().zza(e, "AppActivityTracker.ActivityListener.onActivityPaused");
                    zzayu.zzc("", e);
                }
            }
        }
        this.zzbpe = true;
        if (this.zzbph != null) {
            zzawb.zzdsr.removeCallbacks(this.zzbph);
        }
        zzddu zzddu = zzawb.zzdsr;
        zzqg zzqg = new zzqg(this);
        this.zzbph = zzqg;
        zzddu.postDelayed(zzqg, this.zzbpi);
    }

    public final void onActivityDestroyed(Activity activity) {
        synchronized (this.lock) {
            if (this.zzzk != null) {
                if (this.zzzk.equals(activity)) {
                    this.zzzk = null;
                }
                Iterator<zzqu> it = this.zzbpg.iterator();
                while (it.hasNext()) {
                    try {
                        if (it.next().zza(activity)) {
                            it.remove();
                        }
                    } catch (Exception e) {
                        zzq.zzku().zza(e, "AppActivityTracker.ActivityListener.onActivityDestroyed");
                        zzayu.zzc("", e);
                    }
                }
            }
        }
    }

    private final void setActivity(Activity activity) {
        synchronized (this.lock) {
            if (!activity.getClass().getName().startsWith("com.google.android.gms.ads")) {
                this.zzzk = activity;
            }
        }
    }
}
