package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzdhd extends ExecutorService {
    <T> zzdhe<T> zzd(Callable<T> callable);

    zzdhe<?> zzf(Runnable runnable);
}
