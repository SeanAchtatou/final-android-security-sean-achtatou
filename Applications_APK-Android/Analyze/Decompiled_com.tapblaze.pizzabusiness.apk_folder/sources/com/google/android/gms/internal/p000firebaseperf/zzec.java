package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzec  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
abstract class zzec implements zzeg {
    zzec() {
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public /* synthetic */ Object next() {
        return Byte.valueOf(nextByte());
    }
}
