package com.google.android.gms.base;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int buttonSize = 2130837571;
        public static final int circleCrop = 2130837585;
        public static final int colorScheme = 2130837600;
        public static final int imageAspectRatio = 2130837670;
        public static final int imageAspectRatioAdjust = 2130837671;
        public static final int scopeUris = 2130837727;

        private attr() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2130968639;
        public static final int common_google_signin_btn_text_dark_default = 2130968640;
        public static final int common_google_signin_btn_text_dark_disabled = 2130968641;
        public static final int common_google_signin_btn_text_dark_focused = 2130968642;
        public static final int common_google_signin_btn_text_dark_pressed = 2130968643;
        public static final int common_google_signin_btn_text_light = 2130968644;
        public static final int common_google_signin_btn_text_light_default = 2130968645;
        public static final int common_google_signin_btn_text_light_disabled = 2130968646;
        public static final int common_google_signin_btn_text_light_focused = 2130968647;
        public static final int common_google_signin_btn_text_light_pressed = 2130968648;
        public static final int common_google_signin_btn_tint = 2130968649;

        private color() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131099755;
        public static final int common_google_signin_btn_icon_dark = 2131099756;
        public static final int common_google_signin_btn_icon_dark_focused = 2131099757;
        public static final int common_google_signin_btn_icon_dark_normal = 2131099758;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131099759;
        public static final int common_google_signin_btn_icon_disabled = 2131099760;
        public static final int common_google_signin_btn_icon_light = 2131099761;
        public static final int common_google_signin_btn_icon_light_focused = 2131099762;
        public static final int common_google_signin_btn_icon_light_normal = 2131099763;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131099764;
        public static final int common_google_signin_btn_text_dark = 2131099765;
        public static final int common_google_signin_btn_text_dark_focused = 2131099766;
        public static final int common_google_signin_btn_text_dark_normal = 2131099767;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131099768;
        public static final int common_google_signin_btn_text_disabled = 2131099769;
        public static final int common_google_signin_btn_text_light = 2131099770;
        public static final int common_google_signin_btn_text_light_focused = 2131099771;
        public static final int common_google_signin_btn_text_light_normal = 2131099772;
        public static final int common_google_signin_btn_text_light_normal_background = 2131099773;
        public static final int googleg_disabled_color_18 = 2131099774;
        public static final int googleg_standard_color_18 = 2131099775;

        private drawable() {
        }
    }

    public static final class id {
        public static final int adjust_height = 2131165211;
        public static final int adjust_width = 2131165212;
        public static final int auto = 2131165217;
        public static final int dark = 2131165255;
        public static final int icon_only = 2131165274;
        public static final int light = 2131165282;
        public static final int none = 2131165294;
        public static final int standard = 2131165334;
        public static final int wide = 2131165357;

        private id() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131427407;
        public static final int common_google_play_services_enable_text = 2131427408;
        public static final int common_google_play_services_enable_title = 2131427409;
        public static final int common_google_play_services_install_button = 2131427410;
        public static final int common_google_play_services_install_text = 2131427411;
        public static final int common_google_play_services_install_title = 2131427412;
        public static final int common_google_play_services_notification_channel_name = 2131427413;
        public static final int common_google_play_services_notification_ticker = 2131427414;
        public static final int common_google_play_services_unsupported_text = 2131427416;
        public static final int common_google_play_services_update_button = 2131427417;
        public static final int common_google_play_services_update_text = 2131427418;
        public static final int common_google_play_services_update_title = 2131427419;
        public static final int common_google_play_services_updating_text = 2131427420;
        public static final int common_google_play_services_wear_update_text = 2131427421;
        public static final int common_open_on_phone = 2131427422;
        public static final int common_signin_button_text = 2131427423;
        public static final int common_signin_button_text_long = 2131427424;

        private string() {
        }
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {com.tapblaze.pizzabusiness.R.attr.circleCrop, com.tapblaze.pizzabusiness.R.attr.imageAspectRatio, com.tapblaze.pizzabusiness.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.tapblaze.pizzabusiness.R.attr.buttonSize, com.tapblaze.pizzabusiness.R.attr.colorScheme, com.tapblaze.pizzabusiness.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }
}
