package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcjt implements Runnable {
    private final zzczl zzfel;
    private final zzczt zzfot;
    private final zzcjr zzfzb;

    zzcjt(zzcjr zzcjr, zzczt zzczt, zzczl zzczl) {
        this.zzfzb = zzcjr;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
    }

    public final void run() {
        this.zzfzb.zzd(this.zzfot, this.zzfel);
    }
}
