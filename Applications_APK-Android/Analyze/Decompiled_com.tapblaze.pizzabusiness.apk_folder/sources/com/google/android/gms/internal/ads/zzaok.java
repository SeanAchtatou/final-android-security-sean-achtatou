package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaok {
    private final boolean zzdfz;
    private final boolean zzdga;
    private final boolean zzdgb;
    private final boolean zzdgc;
    private final boolean zzdgd;

    private zzaok(zzaom zzaom) {
        this.zzdfz = zzaom.zzdfz;
        this.zzdga = zzaom.zzdga;
        this.zzdgb = zzaom.zzdgb;
        this.zzdgc = zzaom.zzdgc;
        this.zzdgd = zzaom.zzdgd;
    }

    public final JSONObject zzth() {
        try {
            return new JSONObject().put("sms", this.zzdfz).put("tel", this.zzdga).put("calendar", this.zzdgb).put("storePicture", this.zzdgc).put("inlineVideo", this.zzdgd);
        } catch (JSONException e) {
            zzavs.zzc("Error occured while obtaining the MRAID capabilities.", e);
            return null;
        }
    }
}
