package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzuw extends zzvb<zzvu> {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ String zzcdg;
    private final /* synthetic */ zzup zzcdi;
    private final /* synthetic */ zzuj zzcdj;

    zzuw(zzup zzup, Context context, zzuj zzuj, String str) {
        this.zzcdi = zzup;
        this.val$context = context;
        this.zzcdj = zzuj;
        this.zzcdg = str;
    }

    public final /* synthetic */ Object zzop() {
        zzup.zza(this.val$context, "search");
        return new zzyd();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzccx.zza(this.val$context, this.zzcdj, this.zzcdg, null, 3);
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zza(ObjectWrapper.wrap(this.val$context), this.zzcdj, this.zzcdg, 19649000);
    }
}
