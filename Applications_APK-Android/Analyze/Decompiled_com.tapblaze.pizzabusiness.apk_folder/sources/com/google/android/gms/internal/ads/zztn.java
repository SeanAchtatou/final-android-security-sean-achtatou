package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zztn implements zzdsa {
    static final zzdsa zzew = new zztn();

    private zztn() {
    }

    public final boolean zzf(int i) {
        return zzsy.zzq.zza.zzch(i) != null;
    }
}
