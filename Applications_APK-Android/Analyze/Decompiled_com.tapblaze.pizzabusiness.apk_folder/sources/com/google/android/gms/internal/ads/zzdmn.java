package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdmn extends zzdrt<zzdmn, zza> implements zzdtg {
    private static volatile zzdtn<zzdmn> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmn zzhbu;
    private int zzhaa;
    private zzdmj zzhbk;
    private zzdqk zzhbs = zzdqk.zzhhx;
    private zzdqk zzhbt = zzdqk.zzhhx;

    private zzdmn() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdmn, zza> implements zzdtg {
        private zza() {
            super(zzdmn.zzhbu);
        }

        public final zza zzek(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmn) this.zzhmp).setVersion(0);
            return this;
        }

        public final zza zzc(zzdmj zzdmj) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmn) this.zzhmp).zzb(zzdmj);
            return this;
        }

        public final zza zzaq(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmn) this.zzhmp).zzan(zzdqk);
            return this;
        }

        public final zza zzar(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmn) this.zzhmp).zzao(zzdqk);
            return this;
        }

        /* synthetic */ zza(zzdmm zzdmm) {
            this();
        }
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public final zzdmj zzauf() {
        zzdmj zzdmj = this.zzhbk;
        return zzdmj == null ? zzdmj.zzauk() : zzdmj;
    }

    /* access modifiers changed from: private */
    public final void zzb(zzdmj zzdmj) {
        zzdmj.getClass();
        this.zzhbk = zzdmj;
    }

    public final zzdqk zzaup() {
        return this.zzhbs;
    }

    /* access modifiers changed from: private */
    public final void zzan(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhbs = zzdqk;
    }

    public final zzdqk zzauq() {
        return this.zzhbt;
    }

    /* access modifiers changed from: private */
    public final void zzao(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhbt = zzdqk;
    }

    public static zzdmn zzap(zzdqk zzdqk) throws zzdse {
        return (zzdmn) zzdrt.zza(zzhbu, zzdqk);
    }

    public static zza zzaur() {
        return (zza) zzhbu.zzazt();
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmm.zzdk[i - 1]) {
            case 1:
                return new zzdmn();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhbu, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n\u0004\n", new Object[]{"zzhaa", "zzhbk", "zzhbs", "zzhbt"});
            case 4:
                return zzhbu;
            case 5:
                zzdtn<zzdmn> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmn.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhbu);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdmn zzaus() {
        return zzhbu;
    }

    static {
        zzdmn zzdmn = new zzdmn();
        zzhbu = zzdmn;
        zzdrt.zza(zzdmn.class, zzdmn);
    }
}
