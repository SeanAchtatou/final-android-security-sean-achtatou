package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zztm implements zzdsa {
    static final zzdsa zzew = new zztm();

    private zztm() {
    }

    public final boolean zzf(int i) {
        return zzsy.zzj.zzc.zzcg(i) != null;
    }
}
