package com.google.android.gms.internal.ads;

import android.content.Context;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzajt {
    private final Object zzdam = new Object();
    private final Object zzdan = new Object();
    private zzakc zzdao;
    private zzakc zzdap;

    public final zzakc zza(Context context, zzazb zzazb) {
        zzakc zzakc;
        synchronized (this.zzdan) {
            if (this.zzdap == null) {
                this.zzdap = new zzakc(zzl(context), zzazb, zzabh.zzcuh.get());
            }
            zzakc = this.zzdap;
        }
        return zzakc;
    }

    public final zzakc zzb(Context context, zzazb zzazb) {
        zzakc zzakc;
        synchronized (this.zzdam) {
            if (this.zzdao == null) {
                this.zzdao = new zzakc(zzl(context), zzazb, (String) zzve.zzoy().zzd(zzzn.zzcgh));
            }
            zzakc = this.zzdao;
        }
        return zzakc;
    }

    private static Context zzl(Context context) {
        Context applicationContext = context.getApplicationContext();
        return applicationContext == null ? context : applicationContext;
    }
}
