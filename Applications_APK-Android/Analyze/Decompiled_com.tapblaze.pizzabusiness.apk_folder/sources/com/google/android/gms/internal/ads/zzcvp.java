package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcvp implements Callable {
    private final zzcvm zzgic;

    zzcvp(zzcvm zzcvm) {
        this.zzgic = zzcvm;
    }

    public final Object call() {
        zzcvm zzcvm = this.zzgic;
        return new zzcvn(zzcvm.zzgib.zzd(zzcvm.zzdiz));
    }
}
