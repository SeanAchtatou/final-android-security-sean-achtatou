package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcoi {
    private final String zzcyg;
    public final Bundle zzeig;
    public final String zzfge;

    public zzcoi(String str, String str2, Bundle bundle) {
        this.zzfge = str;
        this.zzcyg = str2;
        this.zzeig = bundle;
    }
}
