package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdqd {
    private static final Class<?> zzhho = zzhe("libcore.io.Memory");
    private static final boolean zzhhp = (zzhe("org.robolectric.Robolectric") != null);

    static boolean zzaxn() {
        return zzhho != null && !zzhhp;
    }

    static Class<?> zzaxo() {
        return zzhho;
    }

    private static <T> Class<T> zzhe(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable unused) {
            return null;
        }
    }
}
