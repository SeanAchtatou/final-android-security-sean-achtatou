package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbac implements Runnable {
    private final /* synthetic */ zzazx zzdxn;

    zzbac(zzazx zzazx) {
        this.zzdxn = zzazx;
    }

    public final void run() {
        if (this.zzdxn.zzdxm != null) {
            this.zzdxn.zzdxm.onPaused();
            this.zzdxn.zzdxm.zzxw();
        }
    }
}
