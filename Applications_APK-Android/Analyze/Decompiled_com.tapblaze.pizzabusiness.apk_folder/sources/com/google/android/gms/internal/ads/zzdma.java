package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdma extends zzdrt<zzdma, zza> implements zzdtg {
    private static volatile zzdtn<zzdma> zzdz;
    /* access modifiers changed from: private */
    public static final zzdma zzhbb;

    private zzdma() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdma, zza> implements zzdtg {
        private zza() {
            super(zzdma.zzhbb);
        }

        /* synthetic */ zza(zzdmb zzdmb) {
            this();
        }
    }

    public static zzdma zzai(zzdqk zzdqk) throws zzdse {
        return (zzdma) zzdrt.zza(zzhbb, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmb.zzdk[i - 1]) {
            case 1:
                return new zzdma();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhbb, "\u0000\u0000", (Object[]) null);
            case 4:
                return zzhbb;
            case 5:
                zzdtn<zzdma> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdma.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhbb);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzdma zzdma = new zzdma();
        zzhbb = zzdma;
        zzdrt.zza(zzdma.class, zzdma);
    }
}
