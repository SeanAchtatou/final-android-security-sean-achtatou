package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcsj implements zzcub<zzcsg> {
    private final zzczj zzfbj;
    private final zzdhd zzfov;

    public zzcsj(zzdhd zzdhd, zzczj zzczj) {
        this.zzfov = zzdhd;
        this.zzfbj = zzczj;
    }

    public final zzdhe<zzcsg> zzanc() {
        return this.zzfov.zzd(new zzcsi(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcsg zzani() throws Exception {
        return new zzcsg(this.zzfbj);
    }
}
