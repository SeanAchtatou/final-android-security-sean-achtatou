package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzin  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
interface zzin {
    void zza(int i, double d) throws IOException;

    void zza(int i, float f) throws IOException;

    void zza(int i, long j) throws IOException;

    void zza(int i, zzeb zzeb) throws IOException;

    <K, V> void zza(int i, zzgg zzgg, Map map) throws IOException;

    void zza(int i, Object obj) throws IOException;

    void zza(int i, Object obj, zzhb zzhb) throws IOException;

    void zza(int i, String str) throws IOException;

    void zza(int i, List<String> list) throws IOException;

    void zza(int i, List<?> list, zzhb zzhb) throws IOException;

    void zza(int i, List<Integer> list, boolean z) throws IOException;

    void zza(int i, boolean z) throws IOException;

    @Deprecated
    void zzai(int i) throws IOException;

    @Deprecated
    void zzaj(int i) throws IOException;

    void zzb(int i, long j) throws IOException;

    @Deprecated
    void zzb(int i, Object obj, zzhb zzhb) throws IOException;

    void zzb(int i, List<zzeb> list) throws IOException;

    @Deprecated
    void zzb(int i, List<?> list, zzhb zzhb) throws IOException;

    void zzb(int i, List<Integer> list, boolean z) throws IOException;

    void zzc(int i, long j) throws IOException;

    void zzc(int i, List<Long> list, boolean z) throws IOException;

    void zzd(int i, List<Long> list, boolean z) throws IOException;

    void zze(int i, List<Long> list, boolean z) throws IOException;

    void zzf(int i, int i2) throws IOException;

    void zzf(int i, List<Float> list, boolean z) throws IOException;

    void zzg(int i, int i2) throws IOException;

    void zzg(int i, List<Double> list, boolean z) throws IOException;

    int zzgu();

    void zzh(int i, int i2) throws IOException;

    void zzh(int i, List<Integer> list, boolean z) throws IOException;

    void zzi(int i, int i2) throws IOException;

    void zzi(int i, long j) throws IOException;

    void zzi(int i, List<Boolean> list, boolean z) throws IOException;

    void zzj(int i, long j) throws IOException;

    void zzj(int i, List<Integer> list, boolean z) throws IOException;

    void zzk(int i, List<Integer> list, boolean z) throws IOException;

    void zzl(int i, List<Long> list, boolean z) throws IOException;

    void zzm(int i, List<Integer> list, boolean z) throws IOException;

    void zzn(int i, List<Long> list, boolean z) throws IOException;

    void zzp(int i, int i2) throws IOException;

    void zzq(int i, int i2) throws IOException;
}
