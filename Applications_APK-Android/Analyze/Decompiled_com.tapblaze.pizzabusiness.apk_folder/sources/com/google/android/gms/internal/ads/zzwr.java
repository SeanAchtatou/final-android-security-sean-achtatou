package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzwr extends IInterface {
    String getDescription() throws RemoteException;

    String zzph() throws RemoteException;
}
