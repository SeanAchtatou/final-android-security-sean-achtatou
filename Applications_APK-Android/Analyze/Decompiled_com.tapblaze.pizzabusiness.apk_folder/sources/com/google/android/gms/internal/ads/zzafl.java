package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzafl implements zzafn<zzbdi> {
    zzafl() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        if (map.keySet().contains("start")) {
            zzbdi.zzaz(true);
        }
        if (map.keySet().contains("stop")) {
            zzbdi.zzaz(false);
        }
    }
}
