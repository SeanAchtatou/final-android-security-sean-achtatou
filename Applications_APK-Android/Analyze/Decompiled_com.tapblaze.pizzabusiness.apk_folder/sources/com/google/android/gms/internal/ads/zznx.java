package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zznx {
    void cancelLoad();

    boolean zzhp();

    void zzhq() throws IOException, InterruptedException;
}
