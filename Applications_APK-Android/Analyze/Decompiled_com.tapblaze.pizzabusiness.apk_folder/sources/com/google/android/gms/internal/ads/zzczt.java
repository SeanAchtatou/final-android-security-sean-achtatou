package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzczt {
    public final zzczo zzgmh;
    public final zzczr zzgmi;

    public zzczt(zzczo zzczo, zzczr zzczr) {
        this.zzgmh = zzczo;
        this.zzgmi = zzczr;
    }
}
