package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzlb extends Exception {
    private zzlb(Throwable th) {
        super("Failed to query underlying media codecs", th);
    }
}
