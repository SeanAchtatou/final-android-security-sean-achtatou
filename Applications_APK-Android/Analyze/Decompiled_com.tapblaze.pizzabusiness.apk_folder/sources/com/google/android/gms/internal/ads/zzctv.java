package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzctv implements Callable {
    private final zzcts zzggw;

    zzctv(zzcts zzcts) {
        this.zzggw = zzcts;
    }

    public final Object call() {
        return this.zzggw.zzanp();
    }
}
