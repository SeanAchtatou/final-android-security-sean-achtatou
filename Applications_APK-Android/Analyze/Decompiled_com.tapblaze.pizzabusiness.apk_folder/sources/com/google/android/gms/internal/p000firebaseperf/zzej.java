package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzej  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzej {
    private final byte[] buffer;
    private final zzeo zzna;

    private zzej(int i) {
        this.buffer = new byte[i];
        this.zzna = zzeo.zza(this.buffer);
    }

    public final zzeb zzgo() {
        this.zzna.zzgs();
        return new zzel(this.buffer);
    }

    public final zzeo zzgp() {
        return this.zzna;
    }

    /* synthetic */ zzej(int i, zzea zzea) {
        this(i);
    }
}
