package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzbk;

/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final /* synthetic */ class zzq {
    static final /* synthetic */ int[] zza = new int[zzbk.zzf.zza.values().length];
    static final /* synthetic */ int[] zzb = new int[zzbk.zzd.zzb.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|(3:27|28|30)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|(3:27|28|30)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(23:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|(3:27|28|30)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
    static {
        /*
            com.google.android.gms.internal.measurement.zzbk$zzd$zzb[] r0 = com.google.android.gms.internal.measurement.zzbk.zzd.zzb.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.android.gms.measurement.internal.zzq.zzb = r0
            r0 = 1
            int[] r1 = com.google.android.gms.measurement.internal.zzq.zzb     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.google.android.gms.internal.measurement.zzbk$zzd$zzb r2 = com.google.android.gms.internal.measurement.zzbk.zzd.zzb.LESS_THAN     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.google.android.gms.measurement.internal.zzq.zzb     // Catch:{ NoSuchFieldError -> 0x001f }
            com.google.android.gms.internal.measurement.zzbk$zzd$zzb r3 = com.google.android.gms.internal.measurement.zzbk.zzd.zzb.GREATER_THAN     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            r2 = 3
            int[] r3 = com.google.android.gms.measurement.internal.zzq.zzb     // Catch:{ NoSuchFieldError -> 0x002a }
            com.google.android.gms.internal.measurement.zzbk$zzd$zzb r4 = com.google.android.gms.internal.measurement.zzbk.zzd.zzb.EQUAL     // Catch:{ NoSuchFieldError -> 0x002a }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            r3 = 4
            int[] r4 = com.google.android.gms.measurement.internal.zzq.zzb     // Catch:{ NoSuchFieldError -> 0x0035 }
            com.google.android.gms.internal.measurement.zzbk$zzd$zzb r5 = com.google.android.gms.internal.measurement.zzbk.zzd.zzb.BETWEEN     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            com.google.android.gms.internal.measurement.zzbk$zzf$zza[] r4 = com.google.android.gms.internal.measurement.zzbk.zzf.zza.values()
            int r4 = r4.length
            int[] r4 = new int[r4]
            com.google.android.gms.measurement.internal.zzq.zza = r4
            int[] r4 = com.google.android.gms.measurement.internal.zzq.zza     // Catch:{ NoSuchFieldError -> 0x0048 }
            com.google.android.gms.internal.measurement.zzbk$zzf$zza r5 = com.google.android.gms.internal.measurement.zzbk.zzf.zza.REGEXP     // Catch:{ NoSuchFieldError -> 0x0048 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0048 }
            r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0048 }
        L_0x0048:
            int[] r0 = com.google.android.gms.measurement.internal.zzq.zza     // Catch:{ NoSuchFieldError -> 0x0052 }
            com.google.android.gms.internal.measurement.zzbk$zzf$zza r4 = com.google.android.gms.internal.measurement.zzbk.zzf.zza.BEGINS_WITH     // Catch:{ NoSuchFieldError -> 0x0052 }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
            r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0052 }
        L_0x0052:
            int[] r0 = com.google.android.gms.measurement.internal.zzq.zza     // Catch:{ NoSuchFieldError -> 0x005c }
            com.google.android.gms.internal.measurement.zzbk$zzf$zza r1 = com.google.android.gms.internal.measurement.zzbk.zzf.zza.ENDS_WITH     // Catch:{ NoSuchFieldError -> 0x005c }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005c }
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005c }
        L_0x005c:
            int[] r0 = com.google.android.gms.measurement.internal.zzq.zza     // Catch:{ NoSuchFieldError -> 0x0066 }
            com.google.android.gms.internal.measurement.zzbk$zzf$zza r1 = com.google.android.gms.internal.measurement.zzbk.zzf.zza.PARTIAL     // Catch:{ NoSuchFieldError -> 0x0066 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0066 }
            r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0066 }
        L_0x0066:
            int[] r0 = com.google.android.gms.measurement.internal.zzq.zza     // Catch:{ NoSuchFieldError -> 0x0071 }
            com.google.android.gms.internal.measurement.zzbk$zzf$zza r1 = com.google.android.gms.internal.measurement.zzbk.zzf.zza.EXACT     // Catch:{ NoSuchFieldError -> 0x0071 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
            r2 = 5
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0071 }
        L_0x0071:
            int[] r0 = com.google.android.gms.measurement.internal.zzq.zza     // Catch:{ NoSuchFieldError -> 0x007c }
            com.google.android.gms.internal.measurement.zzbk$zzf$zza r1 = com.google.android.gms.internal.measurement.zzbk.zzf.zza.IN_LIST     // Catch:{ NoSuchFieldError -> 0x007c }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007c }
            r2 = 6
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007c }
        L_0x007c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzq.<clinit>():void");
    }
}
