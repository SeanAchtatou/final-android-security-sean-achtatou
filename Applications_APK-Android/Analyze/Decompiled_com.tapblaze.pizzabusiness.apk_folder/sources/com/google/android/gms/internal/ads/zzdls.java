package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdls extends zzdrt<zzdls, zza> implements zzdtg {
    private static volatile zzdtn<zzdls> zzdz;
    /* access modifiers changed from: private */
    public static final zzdls zzhax;
    private int zzhas;

    private zzdls() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdls, zza> implements zzdtg {
        private zza() {
            super(zzdls.zzhax);
        }

        /* synthetic */ zza(zzdlt zzdlt) {
            this();
        }
    }

    public final int zzatn() {
        return this.zzhas;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdlt.zzdk[i - 1]) {
            case 1:
                return new zzdls();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhax, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u000b", new Object[]{"zzhas"});
            case 4:
                return zzhax;
            case 5:
                zzdtn<zzdls> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdls.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhax);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdls zzatu() {
        return zzhax;
    }

    static {
        zzdls zzdls = new zzdls();
        zzhax = zzdls;
        zzdrt.zza(zzdls.class, zzdls);
    }
}
