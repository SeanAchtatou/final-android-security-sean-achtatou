package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationExtrasReceiver;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.query.AdData;
import com.google.android.gms.ads.search.SearchAdRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzxj {
    private final int zzabo;
    private final int zzabp;
    private final String zzabq;
    private final boolean zzbkh;
    private final int zzcbz;
    private final String zzccc;
    private final String zzcce;
    private final Bundle zzccg;
    private final String zzcci;
    private final boolean zzcck;
    private final List<String> zzccl;
    private final Bundle zzcee;
    private final Map<Class<? extends NetworkExtras>, NetworkExtras> zzcef;
    private final SearchAdRequest zzceg;
    private final Set<String> zzceh;
    private final Set<String> zzcei;
    private final AdData zzcej;
    private final Date zzme;
    private final Set<String> zzmg;
    private final Location zzmi;

    public zzxj(zzxm zzxm) {
        this(zzxm, null);
    }

    public zzxj(zzxm zzxm, SearchAdRequest searchAdRequest) {
        this.zzme = zzxm.zzme;
        this.zzcce = zzxm.zzcce;
        this.zzccl = zzxm.zzccl;
        this.zzcbz = zzxm.zzcbz;
        this.zzmg = Collections.unmodifiableSet(zzxm.zzceq);
        this.zzmi = zzxm.zzmi;
        this.zzbkh = zzxm.zzbkh;
        this.zzcee = zzxm.zzcee;
        this.zzcef = Collections.unmodifiableMap(zzxm.zzcer);
        this.zzccc = zzxm.zzccc;
        this.zzcci = zzxm.zzcci;
        this.zzceg = searchAdRequest;
        this.zzabo = zzxm.zzabo;
        this.zzceh = Collections.unmodifiableSet(zzxm.zzces);
        this.zzccg = zzxm.zzccg;
        this.zzcei = Collections.unmodifiableSet(zzxm.zzcet);
        this.zzcck = zzxm.zzcck;
        this.zzcej = zzxm.zzcej;
        this.zzabp = zzxm.zzabp;
        this.zzabq = zzxm.zzabq;
    }

    @Deprecated
    public final Date getBirthday() {
        return this.zzme;
    }

    public final String getContentUrl() {
        return this.zzcce;
    }

    public final List<String> zzpn() {
        return new ArrayList(this.zzccl);
    }

    @Deprecated
    public final int getGender() {
        return this.zzcbz;
    }

    public final Set<String> getKeywords() {
        return this.zzmg;
    }

    public final Location getLocation() {
        return this.zzmi;
    }

    public final boolean getManualImpressionsEnabled() {
        return this.zzbkh;
    }

    @Deprecated
    public final <T extends NetworkExtras> T getNetworkExtras(Class<T> cls) {
        return (NetworkExtras) this.zzcef.get(cls);
    }

    public final Bundle getNetworkExtrasBundle(Class<? extends MediationExtrasReceiver> cls) {
        return this.zzcee.getBundle(cls.getName());
    }

    public final Bundle getCustomEventExtrasBundle(Class<? extends CustomEvent> cls) {
        Bundle bundle = this.zzcee.getBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter");
        if (bundle != null) {
            return bundle.getBundle(cls.getName());
        }
        return null;
    }

    public final String getPublisherProvidedId() {
        return this.zzccc;
    }

    public final String zzpo() {
        return this.zzcci;
    }

    public final SearchAdRequest zzpp() {
        return this.zzceg;
    }

    public final boolean isTestDevice(Context context) {
        Set<String> set = this.zzceh;
        zzve.zzou();
        return set.contains(zzayk.zzbi(context));
    }

    public final Map<Class<? extends NetworkExtras>, NetworkExtras> zzpq() {
        return this.zzcef;
    }

    public final Bundle zzpr() {
        return this.zzcee;
    }

    public final int zzps() {
        return this.zzabo;
    }

    public final Bundle getCustomTargeting() {
        return this.zzccg;
    }

    public final Set<String> zzpt() {
        return this.zzcei;
    }

    @Deprecated
    public final boolean isDesignedForFamilies() {
        return this.zzcck;
    }

    public final AdData zzpu() {
        return this.zzcej;
    }

    public final int zzpv() {
        return this.zzabp;
    }

    public final String getMaxAdContentRating() {
        return this.zzabq;
    }
}
