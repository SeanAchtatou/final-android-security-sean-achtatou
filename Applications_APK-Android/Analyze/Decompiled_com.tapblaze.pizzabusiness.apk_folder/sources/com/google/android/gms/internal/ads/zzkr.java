package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzkr {
    private final boolean zzaxf;
    private final int zzaxg;
    private final byte[] zzaxh;

    public zzkr(boolean z, int i, byte[] bArr) {
        this.zzaxf = z;
        this.zzaxg = i;
        this.zzaxh = bArr;
    }
}
