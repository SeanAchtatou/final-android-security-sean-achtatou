package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzdqc implements zzdtj {
    private boolean zzhhm = true;
    private int zzhhn = -1;

    /* renamed from: zzaxm */
    public final zzdtj clone() {
        throw new UnsupportedOperationException("clone() should be implemented by subclasses.");
    }
}
