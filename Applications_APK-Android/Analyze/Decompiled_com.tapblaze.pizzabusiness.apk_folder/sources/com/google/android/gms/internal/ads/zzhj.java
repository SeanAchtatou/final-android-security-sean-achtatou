package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzhj extends zzhg {
    zzhj() {
    }

    public final int zzc(Object obj) {
        return -1;
    }

    public final int zzev() {
        return 0;
    }

    public final int zzew() {
        return 0;
    }

    public final zzhl zza(int i, zzhl zzhl, boolean z, long j) {
        throw new IndexOutOfBoundsException();
    }

    public final zzhi zza(int i, zzhi zzhi, boolean z) {
        throw new IndexOutOfBoundsException();
    }
}
