package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.Preconditions;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
class zzf extends zzha implements zzhc {
    zzf(zzgf zzgf) {
        super(zzgf);
        Preconditions.checkNotNull(zzgf);
    }

    public zzjt zzk() {
        return this.zzx.zze();
    }

    public zzex zzj() {
        return this.zzx.zzk();
    }

    public zzin zzi() {
        return this.zzx.zzv();
    }

    public zzis zzh() {
        return this.zzx.zzw();
    }

    public zzey zzg() {
        return this.zzx.zzy();
    }

    public zzhk zzf() {
        return this.zzx.zzh();
    }

    public zzb zze() {
        return this.zzx.zzz();
    }

    public void zzd() {
        this.zzx.zzq().zzd();
    }

    public void zzc() {
        this.zzx.zzq().zzc();
    }

    public void zzb() {
        this.zzx.zzae();
    }

    public void zza() {
        this.zzx.zzaf();
    }
}
