package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcxh implements zzded {
    private final zzdbi zzgjz;

    zzcxh(zzdbi zzdbi) {
        this.zzgjz = zzdbi;
    }

    public final Object apply(Object obj) {
        zzdbi zzdbi = this.zzgjz;
        zzdbi.zzgpd = (zzbmd) obj;
        return zzdbi;
    }
}
