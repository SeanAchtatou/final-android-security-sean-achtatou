package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdxk<K, V> extends zzdxc<K, V, V> {
    private zzdxk(int i) {
        super(i);
    }

    public final zzdxi<K, V> zzbdo() {
        return new zzdxi<>(this.zzhzy);
    }

    public final /* synthetic */ zzdxc zza(Object obj, zzdxp zzdxp) {
        super.zza(obj, zzdxp);
        return this;
    }
}
