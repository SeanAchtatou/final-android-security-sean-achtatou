package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdwg implements zzdsa {
    static final zzdsa zzew = new zzdwg();

    private zzdwg() {
    }

    public final boolean zzf(int i) {
        return zzdvx.zzb.zzh.zza.zzhg(i) != null;
    }
}
