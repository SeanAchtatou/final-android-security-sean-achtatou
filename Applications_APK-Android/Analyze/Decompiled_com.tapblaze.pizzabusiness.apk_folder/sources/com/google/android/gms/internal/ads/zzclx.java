package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzclx implements zzdxg<zzclv> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzcbi> zzfyl;

    public zzclx(zzdxp<Context> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzcbi> zzdxp3) {
        this.zzejv = zzdxp;
        this.zzfei = zzdxp2;
        this.zzfyl = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzclv(this.zzejv.get(), this.zzfei.get(), this.zzfyl.get());
    }
}
