package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzlq implements zzmn {
    private final zzmn[] zzbas;

    public zzlq(zzmn[] zzmnArr) {
        this.zzbas = zzmnArr;
    }

    public final long zzhh() {
        long j = Long.MAX_VALUE;
        for (zzmn zzhh : this.zzbas) {
            long zzhh2 = zzhh.zzhh();
            if (zzhh2 != Long.MIN_VALUE) {
                j = Math.min(j, zzhh2);
            }
        }
        if (j == Long.MAX_VALUE) {
            return Long.MIN_VALUE;
        }
        return j;
    }

    public final boolean zzef(long j) {
        boolean z;
        boolean z2 = false;
        do {
            long zzhh = zzhh();
            if (zzhh == Long.MIN_VALUE) {
                break;
            }
            z = false;
            for (zzmn zzmn : this.zzbas) {
                if (zzmn.zzhh() == zzhh) {
                    z |= zzmn.zzef(j);
                }
            }
            z2 |= z;
        } while (z);
        return z2;
    }
}
