package com.google.android.gms.internal.ads;

import com.facebook.internal.AnalyticsEvents;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbvq implements zzdxg<String> {
    private static final zzbvq zzfjz = new zzbvq();

    public static zzbvq zzaim() {
        return zzfjz;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza(AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_NATIVE, "Cannot return null from a non-@Nullable @Provides method");
    }
}
