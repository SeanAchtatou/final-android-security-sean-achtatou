package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzbs;

/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
interface zzae {
    void zza(zzbs.zzg zzg);

    boolean zza(long j, zzbs.zzc zzc);
}
