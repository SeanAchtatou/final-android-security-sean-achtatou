package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgd  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzgd<K, V> {
    private final V value;
    private final zzgg<K, V> zzsj;
    private final K zzsk;

    private zzgd(zzih zzih, K k, zzih zzih2, V v) {
        this.zzsj = new zzgg<>(zzih, k, zzih2, v);
        this.zzsk = k;
        this.value = v;
    }

    public static <K, V> zzgd<K, V> zza(zzih zzih, K k, zzih zzih2, V v) {
        return new zzgd<>(zzih, k, zzih2, v);
    }

    static <K, V> int zza(zzgg<K, V> zzgg, K k, V v) {
        return zzex.zza(zzgg.zzsn, 1, k) + zzex.zza(zzgg.zzsp, 2, v);
    }

    public final int zza(int i, K k, V v) {
        return zzeo.zzy(i) + zzeo.zzaf(zza(this.zzsj, k, v));
    }

    /* access modifiers changed from: package-private */
    public final zzgg<K, V> zzia() {
        return this.zzsj;
    }
}
