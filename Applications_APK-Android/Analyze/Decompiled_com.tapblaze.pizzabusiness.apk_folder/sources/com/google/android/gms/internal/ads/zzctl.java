package com.google.android.gms.internal.ads;

import android.os.Bundle;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzctl implements zzdgd {
    private final String zzcyr;
    private final zzctj zzggj;
    private final List zzggl;
    private final Bundle zzggm;

    zzctl(zzctj zzctj, String str, List list, Bundle bundle) {
        this.zzggj = zzctj;
        this.zzcyr = str;
        this.zzggl = list;
        this.zzggm = bundle;
    }

    public final zzdhe zzanm() {
        return this.zzggj.zza(this.zzcyr, this.zzggl, this.zzggm);
    }
}
