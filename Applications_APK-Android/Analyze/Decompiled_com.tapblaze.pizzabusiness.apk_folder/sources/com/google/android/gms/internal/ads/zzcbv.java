package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcbv implements zzps {
    private final zzbdi zzehp;

    zzcbv(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void zza(zzpt zzpt) {
        this.zzehp.zzaaa().zza(zzpt.zzbob.left, zzpt.zzbob.top, false);
    }
}
