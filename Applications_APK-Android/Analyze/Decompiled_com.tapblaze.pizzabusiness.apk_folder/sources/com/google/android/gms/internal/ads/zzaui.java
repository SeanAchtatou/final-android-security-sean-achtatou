package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
interface zzaui<T> {
    T zzb(zzbfq zzbfq) throws RemoteException;
}
