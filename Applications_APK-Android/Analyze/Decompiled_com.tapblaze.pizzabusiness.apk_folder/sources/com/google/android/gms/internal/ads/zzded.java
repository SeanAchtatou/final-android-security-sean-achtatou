package com.google.android.gms.internal.ads;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzded<F, T> {
    @NullableDecl
    T apply(@NullableDecl F f);
}
