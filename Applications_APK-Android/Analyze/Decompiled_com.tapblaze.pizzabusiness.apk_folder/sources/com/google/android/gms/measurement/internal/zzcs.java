package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzjl;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class zzcs implements zzes {
    static final zzes zza = new zzcs();

    private zzcs() {
    }

    public final Object zza() {
        return Boolean.valueOf(zzjl.zzb());
    }
}
