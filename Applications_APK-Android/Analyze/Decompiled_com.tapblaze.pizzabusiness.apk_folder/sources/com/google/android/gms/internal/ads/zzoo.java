package com.google.android.gms.internal.ads;

import android.os.SystemClock;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzoo implements zzog {
    private boolean started;
    private zzhc zzadi = zzhc.zzagb;
    private long zzbgs;
    private long zzbgt;

    public final void start() {
        if (!this.started) {
            this.zzbgt = SystemClock.elapsedRealtime();
            this.started = true;
        }
    }

    public final void stop() {
        if (this.started) {
            zzel(zzfp());
            this.started = false;
        }
    }

    public final void zzel(long j) {
        this.zzbgs = j;
        if (this.started) {
            this.zzbgt = SystemClock.elapsedRealtime();
        }
    }

    public final void zza(zzog zzog) {
        zzel(zzog.zzfp());
        this.zzadi = zzog.zzfi();
    }

    public final long zzfp() {
        long j;
        long j2 = this.zzbgs;
        if (!this.started) {
            return j2;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.zzbgt;
        if (this.zzadi.zzagc == 1.0f) {
            j = zzgi.zzdn(elapsedRealtime);
        } else {
            j = this.zzadi.zzdu(elapsedRealtime);
        }
        return j2 + j;
    }

    public final zzhc zzb(zzhc zzhc) {
        if (this.started) {
            zzel(zzfp());
        }
        this.zzadi = zzhc;
        return zzhc;
    }

    public final zzhc zzfi() {
        return this.zzadi;
    }
}
