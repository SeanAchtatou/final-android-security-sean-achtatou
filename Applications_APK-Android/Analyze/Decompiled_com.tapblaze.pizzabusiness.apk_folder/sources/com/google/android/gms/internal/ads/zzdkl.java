package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdkl implements zzdie {
    private final zzdiq<zzdie> zzgzq;

    public zzdkl(zzdiq<zzdie> zzdiq) {
        this.zzgzq = zzdiq;
    }

    public final byte[] zzc(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
        return zzdoi.zza(this.zzgzq.zzasm().zzasl(), this.zzgzq.zzasm().zzasi().zzc(bArr, bArr2));
    }
}
