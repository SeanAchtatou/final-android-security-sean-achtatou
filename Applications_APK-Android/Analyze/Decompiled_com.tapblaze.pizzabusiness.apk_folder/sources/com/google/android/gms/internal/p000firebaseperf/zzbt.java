package com.google.android.gms.internal.p000firebaseperf;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbt  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class zzbt implements Parcelable {
    public static final Parcelable.Creator<zzbt> CREATOR = new zzbw();
    private long zzhz;
    private long zzia;

    public zzbt() {
        this.zzhz = TimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis());
        this.zzia = System.nanoTime();
    }

    public int describeContents() {
        return 0;
    }

    private zzbt(Parcel parcel) {
        this.zzhz = parcel.readLong();
        this.zzia = parcel.readLong();
    }

    public final void reset() {
        this.zzhz = TimeUnit.MILLISECONDS.toMicros(System.currentTimeMillis());
        this.zzia = System.nanoTime();
    }

    public final long zzcz() {
        return this.zzhz;
    }

    public final long zzda() {
        return TimeUnit.NANOSECONDS.toMicros(System.nanoTime() - this.zzia);
    }

    public final long zzk(zzbt zzbt) {
        return TimeUnit.NANOSECONDS.toMicros(zzbt.zzia - this.zzia);
    }

    public final long zzdb() {
        return this.zzhz + zzda();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.zzhz);
        parcel.writeLong(this.zzia);
    }

    /* synthetic */ zzbt(Parcel parcel, zzbw zzbw) {
        this(parcel);
    }
}
