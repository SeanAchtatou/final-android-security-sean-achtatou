package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
final class zzam implements Events.LoadEventsResult {
    private final /* synthetic */ Status zzba;

    zzam(zzaj zzaj, Status status) {
        this.zzba = status;
    }

    public final void release() {
    }

    public final Status getStatus() {
        return this.zzba;
    }

    public final EventBuffer getEvents() {
        return new EventBuffer(DataHolder.empty(14));
    }
}
