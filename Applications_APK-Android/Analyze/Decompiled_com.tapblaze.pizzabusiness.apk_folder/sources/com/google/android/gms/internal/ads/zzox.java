package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Point;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import com.ironsource.sdk.constants.Constants;
import java.nio.ByteBuffer;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzox extends zzkw {
    private static final int[] zzbhk = {1920, 1600, 1440, 1280, 960, 854, 640, 540, 480};
    private int zzagh;
    private boolean zzajo;
    private final zzpb zzbhl;
    private final zzpg zzbhm;
    private final long zzbhn;
    private final int zzbho;
    private final boolean zzbhp;
    private final long[] zzbhq;
    private zzgw[] zzbhr;
    private zzoz zzbhs;
    private Surface zzbht;
    private Surface zzbhu;
    private int zzbhv;
    private boolean zzbhw;
    private long zzbhx;
    private long zzbhy;
    private int zzbhz;
    private int zzbia;
    private int zzbib;
    private float zzbic;
    private int zzbid;
    private int zzbie;
    private int zzbif;
    private float zzbig;
    private int zzbih;
    private int zzbii;
    private int zzbij;
    private float zzbik;
    zzpc zzbil;
    private long zzbim;
    private int zzbin;
    private final Context zzup;

    public zzox(Context context, zzky zzky, long j, zzddu zzddu, zzpd zzpd, int i) {
        this(context, zzky, 0, null, false, zzddu, zzpd, -1);
    }

    private static boolean zzem(long j) {
        return j < -30000;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private zzox(Context context, zzky zzky, long j, zzja<zzjc> zzja, boolean z, zzddu zzddu, zzpd zzpd, int i) {
        super(2, zzky, null, false);
        boolean z2 = false;
        this.zzbhn = 0;
        this.zzbho = -1;
        this.zzup = context.getApplicationContext();
        this.zzbhl = new zzpb(context);
        this.zzbhm = new zzpg(zzddu, zzpd);
        if (zzoq.SDK_INT <= 22 && "foster".equals(zzoq.DEVICE) && "NVIDIA".equals(zzoq.MANUFACTURER)) {
            z2 = true;
        }
        this.zzbhp = z2;
        this.zzbhq = new long[10];
        this.zzbim = -9223372036854775807L;
        this.zzbhx = -9223372036854775807L;
        this.zzbid = -1;
        this.zzbie = -1;
        this.zzbig = -1.0f;
        this.zzbic = -1.0f;
        this.zzbhv = 1;
        zzjc();
    }

    /* access modifiers changed from: protected */
    public final int zza(zzky zzky, zzgw zzgw) throws zzlb {
        boolean z;
        String str = zzgw.zzafe;
        int i = 0;
        if (!zzof.zzbi(str)) {
            return 0;
        }
        zziv zziv = zzgw.zzafh;
        if (zziv != null) {
            z = false;
            for (int i2 = 0; i2 < zziv.zzamn; i2++) {
                z |= zziv.zzaa(i2).zzamo;
            }
        } else {
            z = false;
        }
        zzkt zzb = zzky.zzb(str, z);
        if (zzb == null) {
            return 1;
        }
        boolean zzbe = zzb.zzbe(zzgw.zzafb);
        if (zzbe && zzgw.width > 0 && zzgw.height > 0) {
            if (zzoq.SDK_INT >= 21) {
                zzbe = zzb.zza(zzgw.width, zzgw.height, (double) zzgw.zzafi);
            } else {
                zzbe = zzgw.width * zzgw.height <= zzla.zzhc();
                if (!zzbe) {
                    int i3 = zzgw.width;
                    int i4 = zzgw.height;
                    String str2 = zzoq.zzbgv;
                    StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 56);
                    sb.append("FalseCheck [legacyFrameSize, ");
                    sb.append(i3);
                    sb.append("x");
                    sb.append(i4);
                    sb.append("] [");
                    sb.append(str2);
                    sb.append(Constants.RequestParameters.RIGHT_BRACKETS);
                    Log.d("MediaCodecVideoRenderer", sb.toString());
                }
            }
        }
        int i5 = zzb.zzaxn ? 8 : 4;
        if (zzb.zzajo) {
            i = 16;
        }
        return (zzbe ? 3 : 2) | i5 | i;
    }

    /* access modifiers changed from: protected */
    public final void zze(boolean z) throws zzgl {
        super.zze(z);
        this.zzagh = zzdy().zzagh;
        this.zzajo = this.zzagh != 0;
        this.zzbhm.zzc(this.zzaze);
        this.zzbhl.enable();
    }

    /* access modifiers changed from: protected */
    public final void zza(zzgw[] zzgwArr, long j) throws zzgl {
        this.zzbhr = zzgwArr;
        if (this.zzbim == -9223372036854775807L) {
            this.zzbim = j;
        } else {
            int i = this.zzbin;
            long[] jArr = this.zzbhq;
            if (i == jArr.length) {
                long j2 = jArr[i - 1];
                StringBuilder sb = new StringBuilder(65);
                sb.append("Too many stream changes, so dropping offset: ");
                sb.append(j2);
                Log.w("MediaCodecVideoRenderer", sb.toString());
            } else {
                this.zzbin = i + 1;
            }
            this.zzbhq[this.zzbin - 1] = j;
        }
        super.zza(zzgwArr, j);
    }

    /* access modifiers changed from: protected */
    public final void zza(long j, boolean z) throws zzgl {
        super.zza(j, z);
        zzja();
        this.zzbia = 0;
        int i = this.zzbin;
        if (i != 0) {
            this.zzbim = this.zzbhq[i - 1];
            this.zzbin = 0;
        }
        if (z) {
            zziz();
        } else {
            this.zzbhx = -9223372036854775807L;
        }
    }

    public final boolean isReady() {
        Surface surface;
        if (super.isReady() && (this.zzbhw || (((surface = this.zzbhu) != null && this.zzbht == surface) || zzgw() == null))) {
            this.zzbhx = -9223372036854775807L;
            return true;
        } else if (this.zzbhx == -9223372036854775807L) {
            return false;
        } else {
            if (SystemClock.elapsedRealtime() < this.zzbhx) {
                return true;
            }
            this.zzbhx = -9223372036854775807L;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void onStarted() {
        super.onStarted();
        this.zzbhz = 0;
        this.zzbhy = SystemClock.elapsedRealtime();
        this.zzbhx = -9223372036854775807L;
    }

    /* access modifiers changed from: protected */
    public final void onStopped() {
        zzjf();
        super.onStopped();
    }

    /* access modifiers changed from: protected */
    public final void zzdx() {
        this.zzbid = -1;
        this.zzbie = -1;
        this.zzbig = -1.0f;
        this.zzbic = -1.0f;
        this.zzbim = -9223372036854775807L;
        this.zzbin = 0;
        zzjc();
        zzja();
        this.zzbhl.disable();
        this.zzbil = null;
        this.zzajo = false;
        try {
            super.zzdx();
        } finally {
            this.zzaze.zzge();
            this.zzbhm.zzd(this.zzaze);
        }
    }

    public final void zza(int i, Object obj) throws zzgl {
        if (i == 1) {
            Surface surface = (Surface) obj;
            if (surface == null) {
                Surface surface2 = this.zzbhu;
                if (surface2 != null) {
                    surface = surface2;
                } else {
                    zzkt zzgx = zzgx();
                    if (zzgx != null && zzn(zzgx.zzaxo)) {
                        this.zzbhu = zzot.zzc(this.zzup, zzgx.zzaxo);
                        surface = this.zzbhu;
                    }
                }
            }
            if (this.zzbht != surface) {
                this.zzbht = surface;
                int state = getState();
                if (state == 1 || state == 2) {
                    MediaCodec zzgw = zzgw();
                    if (zzoq.SDK_INT < 23 || zzgw == null || surface == null) {
                        zzgy();
                        zzgv();
                    } else {
                        zzgw.setOutputSurface(surface);
                    }
                }
                if (surface == null || surface == this.zzbhu) {
                    zzjc();
                    zzja();
                    return;
                }
                zzje();
                zzja();
                if (state == 2) {
                    zziz();
                }
            } else if (surface != null && surface != this.zzbhu) {
                zzje();
                if (this.zzbhw) {
                    this.zzbhm.zzb(this.zzbht);
                }
            }
        } else if (i == 4) {
            this.zzbhv = ((Integer) obj).intValue();
            MediaCodec zzgw2 = zzgw();
            if (zzgw2 != null) {
                zzgw2.setVideoScalingMode(this.zzbhv);
            }
        } else {
            super.zza(i, obj);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean zza(zzkt zzkt) {
        return this.zzbht != null || zzn(zzkt.zzaxo);
    }

    /* access modifiers changed from: protected */
    public final void zza(zzkt zzkt, MediaCodec mediaCodec, zzgw zzgw, MediaCrypto mediaCrypto) throws zzlb {
        zzoz zzoz;
        int i;
        Point point;
        float f;
        zzkt zzkt2 = zzkt;
        MediaCodec mediaCodec2 = mediaCodec;
        zzgw zzgw2 = zzgw;
        zzgw[] zzgwArr = this.zzbhr;
        int i2 = zzgw2.width;
        int i3 = zzgw2.height;
        int zzi = zzi(zzgw);
        if (zzgwArr.length == 1) {
            zzoz = new zzoz(i2, i3, zzi);
        } else {
            int i4 = i3;
            int i5 = zzi;
            boolean z = false;
            int i6 = i2;
            for (zzgw zzgw3 : zzgwArr) {
                if (zza(zzkt2.zzaxn, zzgw2, zzgw3)) {
                    z |= zzgw3.width == -1 || zzgw3.height == -1;
                    i6 = Math.max(i6, zzgw3.width);
                    int max = Math.max(i4, zzgw3.height);
                    i5 = Math.max(i5, zzi(zzgw3));
                    i4 = max;
                }
            }
            if (z) {
                StringBuilder sb = new StringBuilder(66);
                sb.append("Resolutions unknown. Codec max resolution: ");
                sb.append(i6);
                sb.append("x");
                sb.append(i4);
                Log.w("MediaCodecVideoRenderer", sb.toString());
                boolean z2 = zzgw2.height > zzgw2.width;
                int i7 = z2 ? zzgw2.height : zzgw2.width;
                int i8 = z2 ? zzgw2.width : zzgw2.height;
                float f2 = ((float) i8) / ((float) i7);
                int[] iArr = zzbhk;
                int length = iArr.length;
                int i9 = 0;
                while (true) {
                    if (i9 >= length) {
                        break;
                    }
                    int i10 = length;
                    int i11 = iArr[i9];
                    int[] iArr2 = iArr;
                    int i12 = (int) (((float) i11) * f2);
                    if (i11 <= i7 || i12 <= i8) {
                        break;
                    }
                    int i13 = i7;
                    int i14 = i8;
                    if (zzoq.SDK_INT >= 21) {
                        int i15 = z2 ? i12 : i11;
                        if (z2) {
                            i12 = i11;
                        }
                        Point zzd = zzkt2.zzd(i15, i12);
                        i = i5;
                        f = f2;
                        if (zzkt2.zza(zzd.x, zzd.y, (double) zzgw2.zzafi)) {
                            point = zzd;
                            break;
                        }
                    } else {
                        i = i5;
                        f = f2;
                        int zzf = zzoq.zzf(i11, 16) << 4;
                        int zzf2 = zzoq.zzf(i12, 16) << 4;
                        if (zzf * zzf2 <= zzla.zzhc()) {
                            int i16 = z2 ? zzf2 : zzf;
                            if (z2) {
                                zzf2 = zzf;
                            }
                            point = new Point(i16, zzf2);
                        }
                    }
                    i9++;
                    length = i10;
                    iArr = iArr2;
                    i7 = i13;
                    i8 = i14;
                    i5 = i;
                    f2 = f;
                }
                i = i5;
                point = null;
                if (point != null) {
                    i6 = Math.max(i6, point.x);
                    i4 = Math.max(i4, point.y);
                    i5 = Math.max(i, zza(zzgw2.zzafe, i6, i4));
                    StringBuilder sb2 = new StringBuilder(57);
                    sb2.append("Codec max resolution adjusted to: ");
                    sb2.append(i6);
                    sb2.append("x");
                    sb2.append(i4);
                    Log.w("MediaCodecVideoRenderer", sb2.toString());
                } else {
                    i5 = i;
                }
            }
            zzoz = new zzoz(i6, i4, i5);
        }
        this.zzbhs = zzoz;
        zzoz zzoz2 = this.zzbhs;
        boolean z3 = this.zzbhp;
        int i17 = this.zzagh;
        MediaFormat zzeq = zzgw.zzeq();
        zzeq.setInteger("max-width", zzoz2.width);
        zzeq.setInteger("max-height", zzoz2.height);
        if (zzoz2.zzbio != -1) {
            zzeq.setInteger("max-input-size", zzoz2.zzbio);
        }
        if (z3) {
            zzeq.setInteger("auto-frc", 0);
        }
        if (i17 != 0) {
            zzeq.setFeatureEnabled("tunneled-playback", true);
            zzeq.setInteger("audio-session-id", i17);
        }
        if (this.zzbht == null) {
            zzoc.checkState(zzn(zzkt2.zzaxo));
            if (this.zzbhu == null) {
                this.zzbhu = zzot.zzc(this.zzup, zzkt2.zzaxo);
            }
            this.zzbht = this.zzbhu;
        }
        mediaCodec2.configure(zzeq, this.zzbht, (MediaCrypto) null, 0);
        if (zzoq.SDK_INT >= 23 && this.zzajo) {
            this.zzbil = new zzpc(this, mediaCodec2);
        }
    }

    /* access modifiers changed from: protected */
    public final void zzgy() {
        try {
            super.zzgy();
        } finally {
            Surface surface = this.zzbhu;
            if (surface != null) {
                if (this.zzbht == surface) {
                    this.zzbht = null;
                }
                this.zzbhu.release();
                this.zzbhu = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void zzc(String str, long j, long j2) {
        this.zzbhm.zzb(str, j, j2);
    }

    /* access modifiers changed from: protected */
    public final void zzd(zzgw zzgw) throws zzgl {
        super.zzd(zzgw);
        this.zzbhm.zzc(zzgw);
        this.zzbic = zzgw.zzafk == -1.0f ? 1.0f : zzgw.zzafk;
        this.zzbib = zzj(zzgw);
    }

    /* access modifiers changed from: protected */
    public final void zza(zzis zzis) {
        if (zzoq.SDK_INT < 23 && this.zzajo) {
            zzjb();
        }
    }

    /* access modifiers changed from: protected */
    public final void onOutputFormatChanged(MediaCodec mediaCodec, MediaFormat mediaFormat) {
        int i;
        int i2;
        boolean z = mediaFormat.containsKey("crop-right") && mediaFormat.containsKey("crop-left") && mediaFormat.containsKey("crop-bottom") && mediaFormat.containsKey("crop-top");
        if (z) {
            i = (mediaFormat.getInteger("crop-right") - mediaFormat.getInteger("crop-left")) + 1;
        } else {
            i = mediaFormat.getInteger("width");
        }
        this.zzbid = i;
        if (z) {
            i2 = (mediaFormat.getInteger("crop-bottom") - mediaFormat.getInteger("crop-top")) + 1;
        } else {
            i2 = mediaFormat.getInteger("height");
        }
        this.zzbie = i2;
        this.zzbig = this.zzbic;
        if (zzoq.SDK_INT >= 21) {
            int i3 = this.zzbib;
            if (i3 == 90 || i3 == 270) {
                int i4 = this.zzbid;
                this.zzbid = this.zzbie;
                this.zzbie = i4;
                this.zzbig = 1.0f / this.zzbig;
            }
        } else {
            this.zzbif = this.zzbib;
        }
        mediaCodec.setVideoScalingMode(this.zzbhv);
    }

    /* access modifiers changed from: protected */
    public final boolean zza(MediaCodec mediaCodec, boolean z, zzgw zzgw, zzgw zzgw2) {
        return zza(z, zzgw, zzgw2) && zzgw2.width <= this.zzbhs.width && zzgw2.height <= this.zzbhs.height && zzgw2.zzaff <= this.zzbhs.zzbio;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.MediaCodec.releaseOutputBuffer(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{android.media.MediaCodec.releaseOutputBuffer(int, long):void}
      ClspMth{android.media.MediaCodec.releaseOutputBuffer(int, boolean):void} */
    /* access modifiers changed from: protected */
    public final boolean zza(long j, long j2, MediaCodec mediaCodec, ByteBuffer byteBuffer, int i, int i2, long j3, boolean z) {
        MediaCodec mediaCodec2 = mediaCodec;
        int i3 = i;
        long j4 = j3;
        while (true) {
            int i4 = this.zzbin;
            if (i4 == 0) {
                break;
            }
            long[] jArr = this.zzbhq;
            if (j4 < jArr[0]) {
                break;
            }
            this.zzbim = jArr[0];
            this.zzbin = i4 - 1;
            System.arraycopy(jArr, 1, jArr, 0, this.zzbin);
        }
        long j5 = j4 - this.zzbim;
        if (z) {
            zza(mediaCodec2, i3, j5);
            return true;
        }
        long j6 = j4 - j;
        if (this.zzbht == this.zzbhu) {
            if (!zzem(j6)) {
                return false;
            }
            zza(mediaCodec2, i3, j5);
            return true;
        } else if (!this.zzbhw) {
            if (zzoq.SDK_INT >= 21) {
                zza(mediaCodec, i, j5, System.nanoTime());
            } else {
                zzb(mediaCodec2, i3, j5);
            }
            return true;
        } else if (getState() != 2) {
            return false;
        } else {
            long elapsedRealtime = j6 - ((SystemClock.elapsedRealtime() * 1000) - j2);
            long nanoTime = System.nanoTime();
            long zzf = this.zzbhl.zzf(j4, (elapsedRealtime * 1000) + nanoTime);
            long j7 = (zzf - nanoTime) / 1000;
            if (zzem(j7)) {
                zzon.beginSection("dropVideoBuffer");
                mediaCodec2.releaseOutputBuffer(i3, false);
                zzon.endSection();
                this.zzaze.zzamk++;
                this.zzbhz++;
                this.zzbia++;
                this.zzaze.zzaml = Math.max(this.zzbia, this.zzaze.zzaml);
                if (this.zzbhz == this.zzbho) {
                    zzjf();
                }
                return true;
            }
            if (zzoq.SDK_INT >= 21) {
                if (j7 < 50000) {
                    zza(mediaCodec, i, j5, zzf);
                    return true;
                }
            } else if (j7 < 30000) {
                if (j7 > 11000) {
                    try {
                        Thread.sleep((j7 - 10000) / 1000);
                    } catch (InterruptedException unused) {
                        Thread.currentThread().interrupt();
                    }
                }
                zzb(mediaCodec2, i3, j5);
                return true;
            }
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.MediaCodec.releaseOutputBuffer(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{android.media.MediaCodec.releaseOutputBuffer(int, long):void}
      ClspMth{android.media.MediaCodec.releaseOutputBuffer(int, boolean):void} */
    private final void zza(MediaCodec mediaCodec, int i, long j) {
        zzon.beginSection("skipVideoBuffer");
        mediaCodec.releaseOutputBuffer(i, false);
        zzon.endSection();
        this.zzaze.zzamj++;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.MediaCodec.releaseOutputBuffer(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{android.media.MediaCodec.releaseOutputBuffer(int, long):void}
      ClspMth{android.media.MediaCodec.releaseOutputBuffer(int, boolean):void} */
    private final void zzb(MediaCodec mediaCodec, int i, long j) {
        zzjd();
        zzon.beginSection("releaseOutputBuffer");
        mediaCodec.releaseOutputBuffer(i, true);
        zzon.endSection();
        this.zzaze.zzami++;
        this.zzbia = 0;
        zzjb();
    }

    private final void zza(MediaCodec mediaCodec, int i, long j, long j2) {
        zzjd();
        zzon.beginSection("releaseOutputBuffer");
        mediaCodec.releaseOutputBuffer(i, j2);
        zzon.endSection();
        this.zzaze.zzami++;
        this.zzbia = 0;
        zzjb();
    }

    private final boolean zzn(boolean z) {
        if (zzoq.SDK_INT < 23 || this.zzajo) {
            return false;
        }
        return !z || zzot.zzc(this.zzup);
    }

    private final void zziz() {
        this.zzbhx = this.zzbhn > 0 ? SystemClock.elapsedRealtime() + this.zzbhn : -9223372036854775807L;
    }

    private final void zzja() {
        MediaCodec zzgw;
        this.zzbhw = false;
        if (zzoq.SDK_INT >= 23 && this.zzajo && (zzgw = zzgw()) != null) {
            this.zzbil = new zzpc(this, zzgw);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzjb() {
        if (!this.zzbhw) {
            this.zzbhw = true;
            this.zzbhm.zzb(this.zzbht);
        }
    }

    private final void zzjc() {
        this.zzbih = -1;
        this.zzbii = -1;
        this.zzbik = -1.0f;
        this.zzbij = -1;
    }

    private final void zzjd() {
        if (this.zzbih != this.zzbid || this.zzbii != this.zzbie || this.zzbij != this.zzbif || this.zzbik != this.zzbig) {
            this.zzbhm.zzb(this.zzbid, this.zzbie, this.zzbif, this.zzbig);
            this.zzbih = this.zzbid;
            this.zzbii = this.zzbie;
            this.zzbij = this.zzbif;
            this.zzbik = this.zzbig;
        }
    }

    private final void zzje() {
        if (this.zzbih != -1 || this.zzbii != -1) {
            this.zzbhm.zzb(this.zzbid, this.zzbie, this.zzbif, this.zzbig);
        }
    }

    private final void zzjf() {
        if (this.zzbhz > 0) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.zzbhm.zzf(this.zzbhz, elapsedRealtime - this.zzbhy);
            this.zzbhz = 0;
            this.zzbhy = elapsedRealtime;
        }
    }

    private static int zzi(zzgw zzgw) {
        if (zzgw.zzaff != -1) {
            return zzgw.zzaff;
        }
        return zza(zzgw.zzafe, zzgw.width, zzgw.height);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static int zza(String str, int i, int i2) {
        char c;
        int i3;
        if (i == -1 || i2 == -1) {
            return -1;
        }
        int i4 = 4;
        switch (str.hashCode()) {
            case -1664118616:
                if (str.equals("video/3gpp")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case -1662541442:
                if (str.equals("video/hevc")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case 1187890754:
                if (str.equals("video/mp4v-es")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 1331836730:
                if (str.equals("video/avc")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 1599127256:
                if (str.equals("video/x-vnd.on2.vp8")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 1599127257:
                if (str.equals("video/x-vnd.on2.vp9")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        if (!(c == 0 || c == 1)) {
            if (c != 2) {
                if (c != 3) {
                    if (c != 4 && c != 5) {
                        return -1;
                    }
                    i3 = i * i2;
                    return (i3 * 3) / (i4 * 2);
                }
            } else if ("BRAVIA 4K 2015".equals(zzoq.MODEL)) {
                return -1;
            } else {
                i3 = ((zzoq.zzf(i, 16) * zzoq.zzf(i2, 16)) << 4) << 4;
                i4 = 2;
                return (i3 * 3) / (i4 * 2);
            }
        }
        i3 = i * i2;
        i4 = 2;
        return (i3 * 3) / (i4 * 2);
    }

    private static boolean zza(boolean z, zzgw zzgw, zzgw zzgw2) {
        if (!zzgw.zzafe.equals(zzgw2.zzafe) || zzj(zzgw) != zzj(zzgw2)) {
            return false;
        }
        if (!z) {
            return zzgw.width == zzgw2.width && zzgw.height == zzgw2.height;
        }
        return true;
    }

    private static int zzj(zzgw zzgw) {
        if (zzgw.zzafj == -1) {
            return 0;
        }
        return zzgw.zzafj;
    }
}
