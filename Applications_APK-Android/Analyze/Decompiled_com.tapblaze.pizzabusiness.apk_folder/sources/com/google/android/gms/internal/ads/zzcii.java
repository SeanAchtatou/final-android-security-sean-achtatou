package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcii implements zzdxg<zzcid> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzchz> zzems;
    private final zzdxp<zzcht> zzepb;
    private final zzdxp<zzdhe<Bundle>> zzfxy;

    private zzcii(zzdxp<Context> zzdxp, zzdxp<zzdhe<Bundle>> zzdxp2, zzdxp<zzchz> zzdxp3, zzdxp<zzcht> zzdxp4) {
        this.zzejv = zzdxp;
        this.zzfxy = zzdxp2;
        this.zzems = zzdxp3;
        this.zzepb = zzdxp4;
    }

    public static zzcii zzc(zzdxp<Context> zzdxp, zzdxp<zzdhe<Bundle>> zzdxp2, zzdxp<zzchz> zzdxp3, zzdxp<zzcht> zzdxp4) {
        return new zzcii(zzdxp, zzdxp2, zzdxp3, zzdxp4);
    }

    public final /* synthetic */ Object get() {
        return new zzcid(this.zzejv.get(), this.zzfxy.get(), this.zzems.get(), this.zzepb.get());
    }
}
