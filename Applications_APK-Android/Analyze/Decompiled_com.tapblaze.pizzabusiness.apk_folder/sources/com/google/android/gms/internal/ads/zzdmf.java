package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdmf extends zzdrt<zzdmf, zza> implements zzdtg {
    private static volatile zzdtn<zzdmf> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmf zzhbj;
    private zzdng zzhbi;

    private zzdmf() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdmf, zza> implements zzdtg {
        private zza() {
            super(zzdmf.zzhbj);
        }

        /* synthetic */ zza(zzdme zzdme) {
            this();
        }
    }

    public final zzdng zzauc() {
        zzdng zzdng = this.zzhbi;
        return zzdng == null ? zzdng.zzavo() : zzdng;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdme.zzdk[i - 1]) {
            case 1:
                return new zzdmf();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhbj, "\u0000\u0001\u0000\u0000\u0002\u0002\u0001\u0000\u0000\u0000\u0002\t", new Object[]{"zzhbi"});
            case 4:
                return zzhbj;
            case 5:
                zzdtn<zzdmf> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmf.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhbj);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdmf zzaud() {
        return zzhbj;
    }

    static {
        zzdmf zzdmf = new zzdmf();
        zzhbj = zzdmf;
        zzdrt.zza(zzdmf.class, zzdmf);
    }
}
