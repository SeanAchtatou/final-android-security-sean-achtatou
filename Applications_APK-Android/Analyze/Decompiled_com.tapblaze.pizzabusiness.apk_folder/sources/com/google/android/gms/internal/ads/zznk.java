package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zznk {
    public final byte[] data;
    private final int offset = 0;

    public zznk(byte[] bArr, int i) {
        this.data = bArr;
    }

    public final int zzaz(int i) {
        return this.offset + i;
    }
}
