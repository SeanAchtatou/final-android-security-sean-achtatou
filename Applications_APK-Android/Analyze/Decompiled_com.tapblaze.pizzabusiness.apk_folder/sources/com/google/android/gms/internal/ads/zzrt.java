package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzrt implements Runnable {
    private final /* synthetic */ zzrq zzbrh;

    zzrt(zzrq zzrq) {
        this.zzbrh = zzrq;
    }

    public final void run() {
        this.zzbrh.disconnect();
    }
}
