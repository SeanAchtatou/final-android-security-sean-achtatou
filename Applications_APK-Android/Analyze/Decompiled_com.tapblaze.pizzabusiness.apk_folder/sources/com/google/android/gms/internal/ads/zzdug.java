package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdug extends zzdum {
    private final /* synthetic */ zzdub zzhqw;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private zzdug(zzdub zzdub) {
        super(zzdub, null);
        this.zzhqw = zzdub;
    }

    public final Iterator<Map.Entry<K, V>> iterator() {
        return new zzdud(this.zzhqw, null);
    }

    /* synthetic */ zzdug(zzdub zzdub, zzdue zzdue) {
        this(zzdub);
    }
}
