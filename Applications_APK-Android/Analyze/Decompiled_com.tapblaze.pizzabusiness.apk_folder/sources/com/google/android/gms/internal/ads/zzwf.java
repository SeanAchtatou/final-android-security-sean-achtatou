package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzwf extends zzgc implements zzwd {
    zzwf(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.client.IClientApi");
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzvu zza(com.google.android.gms.dynamic.IObjectWrapper r2, com.google.android.gms.internal.ads.zzuj r3, java.lang.String r4, com.google.android.gms.internal.ads.zzalc r5, int r6) throws android.os.RemoteException {
        /*
            r1 = this;
            android.os.Parcel r0 = r1.obtainAndWriteInterfaceToken()
            com.google.android.gms.internal.ads.zzge.zza(r0, r2)
            com.google.android.gms.internal.ads.zzge.zza(r0, r3)
            r0.writeString(r4)
            com.google.android.gms.internal.ads.zzge.zza(r0, r5)
            r0.writeInt(r6)
            r2 = 1
            android.os.Parcel r2 = r1.transactAndReadException(r2, r0)
            android.os.IBinder r3 = r2.readStrongBinder()
            if (r3 != 0) goto L_0x0020
            r3 = 0
            goto L_0x0034
        L_0x0020:
            java.lang.String r4 = "com.google.android.gms.ads.internal.client.IAdManager"
            android.os.IInterface r4 = r3.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.ads.zzvu
            if (r5 == 0) goto L_0x002e
            r3 = r4
            com.google.android.gms.internal.ads.zzvu r3 = (com.google.android.gms.internal.ads.zzvu) r3
            goto L_0x0034
        L_0x002e:
            com.google.android.gms.internal.ads.zzvw r4 = new com.google.android.gms.internal.ads.zzvw
            r4.<init>(r3)
            r3 = r4
        L_0x0034:
            r2.recycle()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzwf.zza(com.google.android.gms.dynamic.IObjectWrapper, com.google.android.gms.internal.ads.zzuj, java.lang.String, com.google.android.gms.internal.ads.zzalc, int):com.google.android.gms.internal.ads.zzvu");
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzvu zzb(com.google.android.gms.dynamic.IObjectWrapper r2, com.google.android.gms.internal.ads.zzuj r3, java.lang.String r4, com.google.android.gms.internal.ads.zzalc r5, int r6) throws android.os.RemoteException {
        /*
            r1 = this;
            android.os.Parcel r0 = r1.obtainAndWriteInterfaceToken()
            com.google.android.gms.internal.ads.zzge.zza(r0, r2)
            com.google.android.gms.internal.ads.zzge.zza(r0, r3)
            r0.writeString(r4)
            com.google.android.gms.internal.ads.zzge.zza(r0, r5)
            r0.writeInt(r6)
            r2 = 2
            android.os.Parcel r2 = r1.transactAndReadException(r2, r0)
            android.os.IBinder r3 = r2.readStrongBinder()
            if (r3 != 0) goto L_0x0020
            r3 = 0
            goto L_0x0034
        L_0x0020:
            java.lang.String r4 = "com.google.android.gms.ads.internal.client.IAdManager"
            android.os.IInterface r4 = r3.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.ads.zzvu
            if (r5 == 0) goto L_0x002e
            r3 = r4
            com.google.android.gms.internal.ads.zzvu r3 = (com.google.android.gms.internal.ads.zzvu) r3
            goto L_0x0034
        L_0x002e:
            com.google.android.gms.internal.ads.zzvw r4 = new com.google.android.gms.internal.ads.zzvw
            r4.<init>(r3)
            r3 = r4
        L_0x0034:
            r2.recycle()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzwf.zzb(com.google.android.gms.dynamic.IObjectWrapper, com.google.android.gms.internal.ads.zzuj, java.lang.String, com.google.android.gms.internal.ads.zzalc, int):com.google.android.gms.internal.ads.zzvu");
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzvn zza(com.google.android.gms.dynamic.IObjectWrapper r2, java.lang.String r3, com.google.android.gms.internal.ads.zzalc r4, int r5) throws android.os.RemoteException {
        /*
            r1 = this;
            android.os.Parcel r0 = r1.obtainAndWriteInterfaceToken()
            com.google.android.gms.internal.ads.zzge.zza(r0, r2)
            r0.writeString(r3)
            com.google.android.gms.internal.ads.zzge.zza(r0, r4)
            r0.writeInt(r5)
            r2 = 3
            android.os.Parcel r2 = r1.transactAndReadException(r2, r0)
            android.os.IBinder r3 = r2.readStrongBinder()
            if (r3 != 0) goto L_0x001d
            r3 = 0
            goto L_0x0031
        L_0x001d:
            java.lang.String r4 = "com.google.android.gms.ads.internal.client.IAdLoaderBuilder"
            android.os.IInterface r4 = r3.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.ads.zzvn
            if (r5 == 0) goto L_0x002b
            r3 = r4
            com.google.android.gms.internal.ads.zzvn r3 = (com.google.android.gms.internal.ads.zzvn) r3
            goto L_0x0031
        L_0x002b:
            com.google.android.gms.internal.ads.zzvp r4 = new com.google.android.gms.internal.ads.zzvp
            r4.<init>(r3)
            r3 = r4
        L_0x0031:
            r2.recycle()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzwf.zza(com.google.android.gms.dynamic.IObjectWrapper, java.lang.String, com.google.android.gms.internal.ads.zzalc, int):com.google.android.gms.internal.ads.zzvn");
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzwk zzc(com.google.android.gms.dynamic.IObjectWrapper r4) throws android.os.RemoteException {
        /*
            r3 = this;
            android.os.Parcel r0 = r3.obtainAndWriteInterfaceToken()
            com.google.android.gms.internal.ads.zzge.zza(r0, r4)
            r4 = 4
            android.os.Parcel r4 = r3.transactAndReadException(r4, r0)
            android.os.IBinder r0 = r4.readStrongBinder()
            if (r0 != 0) goto L_0x0014
            r0 = 0
            goto L_0x0028
        L_0x0014:
            java.lang.String r1 = "com.google.android.gms.ads.internal.client.IMobileAdsSettingManager"
            android.os.IInterface r1 = r0.queryLocalInterface(r1)
            boolean r2 = r1 instanceof com.google.android.gms.internal.ads.zzwk
            if (r2 == 0) goto L_0x0022
            r0 = r1
            com.google.android.gms.internal.ads.zzwk r0 = (com.google.android.gms.internal.ads.zzwk) r0
            goto L_0x0028
        L_0x0022:
            com.google.android.gms.internal.ads.zzwm r1 = new com.google.android.gms.internal.ads.zzwm
            r1.<init>(r0)
            r0 = r1
        L_0x0028:
            r4.recycle()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzwf.zzc(com.google.android.gms.dynamic.IObjectWrapper):com.google.android.gms.internal.ads.zzwk");
    }

    public final zzacm zza(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper2);
        Parcel transactAndReadException = transactAndReadException(5, obtainAndWriteInterfaceToken);
        zzacm zzn = zzacl.zzn(transactAndReadException.readStrongBinder());
        transactAndReadException.recycle();
        return zzn;
    }

    public final zzarl zza(IObjectWrapper iObjectWrapper, zzalc zzalc, int i) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, zzalc);
        obtainAndWriteInterfaceToken.writeInt(i);
        Parcel transactAndReadException = transactAndReadException(6, obtainAndWriteInterfaceToken);
        zzarl zzai = zzark.zzai(transactAndReadException.readStrongBinder());
        transactAndReadException.recycle();
        return zzai;
    }

    public final zzapd zzd(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        Parcel transactAndReadException = transactAndReadException(7, obtainAndWriteInterfaceToken);
        zzapd zzag = zzapc.zzag(transactAndReadException.readStrongBinder());
        transactAndReadException.recycle();
        return zzag;
    }

    public final zzaot zzb(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        Parcel transactAndReadException = transactAndReadException(8, obtainAndWriteInterfaceToken);
        zzaot zzae = zzaos.zzae(transactAndReadException.readStrongBinder());
        transactAndReadException.recycle();
        return zzae;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzwk zza(com.google.android.gms.dynamic.IObjectWrapper r3, int r4) throws android.os.RemoteException {
        /*
            r2 = this;
            android.os.Parcel r0 = r2.obtainAndWriteInterfaceToken()
            com.google.android.gms.internal.ads.zzge.zza(r0, r3)
            r0.writeInt(r4)
            r3 = 9
            android.os.Parcel r3 = r2.transactAndReadException(r3, r0)
            android.os.IBinder r4 = r3.readStrongBinder()
            if (r4 != 0) goto L_0x0018
            r4 = 0
            goto L_0x002c
        L_0x0018:
            java.lang.String r0 = "com.google.android.gms.ads.internal.client.IMobileAdsSettingManager"
            android.os.IInterface r0 = r4.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.internal.ads.zzwk
            if (r1 == 0) goto L_0x0026
            r4 = r0
            com.google.android.gms.internal.ads.zzwk r4 = (com.google.android.gms.internal.ads.zzwk) r4
            goto L_0x002c
        L_0x0026:
            com.google.android.gms.internal.ads.zzwm r0 = new com.google.android.gms.internal.ads.zzwm
            r0.<init>(r4)
            r4 = r0
        L_0x002c:
            r3.recycle()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzwf.zza(com.google.android.gms.dynamic.IObjectWrapper, int):com.google.android.gms.internal.ads.zzwk");
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzvu zza(com.google.android.gms.dynamic.IObjectWrapper r2, com.google.android.gms.internal.ads.zzuj r3, java.lang.String r4, int r5) throws android.os.RemoteException {
        /*
            r1 = this;
            android.os.Parcel r0 = r1.obtainAndWriteInterfaceToken()
            com.google.android.gms.internal.ads.zzge.zza(r0, r2)
            com.google.android.gms.internal.ads.zzge.zza(r0, r3)
            r0.writeString(r4)
            r0.writeInt(r5)
            r2 = 10
            android.os.Parcel r2 = r1.transactAndReadException(r2, r0)
            android.os.IBinder r3 = r2.readStrongBinder()
            if (r3 != 0) goto L_0x001e
            r3 = 0
            goto L_0x0032
        L_0x001e:
            java.lang.String r4 = "com.google.android.gms.ads.internal.client.IAdManager"
            android.os.IInterface r4 = r3.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.ads.zzvu
            if (r5 == 0) goto L_0x002c
            r3 = r4
            com.google.android.gms.internal.ads.zzvu r3 = (com.google.android.gms.internal.ads.zzvu) r3
            goto L_0x0032
        L_0x002c:
            com.google.android.gms.internal.ads.zzvw r4 = new com.google.android.gms.internal.ads.zzvw
            r4.<init>(r3)
            r3 = r4
        L_0x0032:
            r2.recycle()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzwf.zza(com.google.android.gms.dynamic.IObjectWrapper, com.google.android.gms.internal.ads.zzuj, java.lang.String, int):com.google.android.gms.internal.ads.zzvu");
    }

    public final zzacp zza(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper2);
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper3);
        Parcel transactAndReadException = transactAndReadException(11, obtainAndWriteInterfaceToken);
        zzacp zzo = zzacs.zzo(transactAndReadException.readStrongBinder());
        transactAndReadException.recycle();
        return zzo;
    }

    public final zzasg zzb(IObjectWrapper iObjectWrapper, String str, zzalc zzalc, int i) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        obtainAndWriteInterfaceToken.writeString(str);
        zzge.zza(obtainAndWriteInterfaceToken, zzalc);
        obtainAndWriteInterfaceToken.writeInt(i);
        Parcel transactAndReadException = transactAndReadException(12, obtainAndWriteInterfaceToken);
        zzasg zzam = zzasj.zzam(transactAndReadException.readStrongBinder());
        transactAndReadException.recycle();
        return zzam;
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzvu zzc(com.google.android.gms.dynamic.IObjectWrapper r2, com.google.android.gms.internal.ads.zzuj r3, java.lang.String r4, com.google.android.gms.internal.ads.zzalc r5, int r6) throws android.os.RemoteException {
        /*
            r1 = this;
            android.os.Parcel r0 = r1.obtainAndWriteInterfaceToken()
            com.google.android.gms.internal.ads.zzge.zza(r0, r2)
            com.google.android.gms.internal.ads.zzge.zza(r0, r3)
            r0.writeString(r4)
            com.google.android.gms.internal.ads.zzge.zza(r0, r5)
            r0.writeInt(r6)
            r2 = 13
            android.os.Parcel r2 = r1.transactAndReadException(r2, r0)
            android.os.IBinder r3 = r2.readStrongBinder()
            if (r3 != 0) goto L_0x0021
            r3 = 0
            goto L_0x0035
        L_0x0021:
            java.lang.String r4 = "com.google.android.gms.ads.internal.client.IAdManager"
            android.os.IInterface r4 = r3.queryLocalInterface(r4)
            boolean r5 = r4 instanceof com.google.android.gms.internal.ads.zzvu
            if (r5 == 0) goto L_0x002f
            r3 = r4
            com.google.android.gms.internal.ads.zzvu r3 = (com.google.android.gms.internal.ads.zzvu) r3
            goto L_0x0035
        L_0x002f:
            com.google.android.gms.internal.ads.zzvw r4 = new com.google.android.gms.internal.ads.zzvw
            r4.<init>(r3)
            r3 = r4
        L_0x0035:
            r2.recycle()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzwf.zzc(com.google.android.gms.dynamic.IObjectWrapper, com.google.android.gms.internal.ads.zzuj, java.lang.String, com.google.android.gms.internal.ads.zzalc, int):com.google.android.gms.internal.ads.zzvu");
    }
}
