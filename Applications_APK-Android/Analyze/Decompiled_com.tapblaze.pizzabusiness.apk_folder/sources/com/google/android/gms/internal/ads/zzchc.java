package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzchc implements zzajw {
    static final zzajw zzdas = new zzchc();

    private zzchc() {
    }

    public final Object zzd(JSONObject jSONObject) {
        return new zzaqq(jSONObject);
    }
}
