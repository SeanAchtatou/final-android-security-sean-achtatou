package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.widget.RelativeLayout;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzddz extends RelativeLayout {
    public zzddz(Context context, WebView webView) {
        super(context);
    }

    public static WebView getWebView() {
        return null;
    }

    public static void onPageStarted(WebView webView, String str, Bitmap bitmap) {
    }
}
