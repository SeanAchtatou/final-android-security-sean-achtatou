package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbmd;
import com.google.android.gms.internal.ads.zzbob;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcxf<R extends zzbob<AdT>, AdT extends zzbmd> implements zzcxt<R, zzdbi<AdT>> {
    private final Executor executor = zzdhg.zzarw();
    private R zzgjp;

    zzcxf() {
    }

    public final zzdhe<zzdbi<AdT>> zza(zzcxs zzcxs, zzcxv<R> zzcxv) {
        zzboe<R> zzc = zzcxv.zzc(zzcxs);
        zzc.zza(new zzcxw(true));
        this.zzgjp = (zzbob) zzc.zzadg();
        zzbmz zzadc = this.zzgjp.zzadc();
        zzdbi zzdbi = new zzdbi();
        return ((zzdgn) zzdgs.zzb(zzdgn.zze(zzadc.zzagz()), new zzcxe(this, zzdbi, zzadc), this.executor)).zza(new zzcxh(zzdbi), this.executor);
    }

    public final /* synthetic */ Object zzaog() {
        return this.zzgjp;
    }
}
