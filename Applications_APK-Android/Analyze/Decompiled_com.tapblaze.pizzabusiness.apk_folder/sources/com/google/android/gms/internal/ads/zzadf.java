package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.List;
import java.util.WeakHashMap;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzadf implements NativeCustomTemplateAd {
    private static WeakHashMap<IBinder, zzadf> zzcwa = new WeakHashMap<>();
    private final VideoController zzcel = new VideoController();
    private final zzade zzcwb;
    private final MediaView zzcwc;
    private NativeCustomTemplateAd.DisplayOpenMeasurement zzcwd;

    private zzadf(zzade zzade) {
        Context context;
        this.zzcwb = zzade;
        MediaView mediaView = null;
        try {
            context = (Context) ObjectWrapper.unwrap(zzade.zzrk());
        } catch (RemoteException | NullPointerException e) {
            zzayu.zzc("", e);
            context = null;
        }
        if (context != null) {
            MediaView mediaView2 = new MediaView(context);
            try {
                if (this.zzcwb.zzp(ObjectWrapper.wrap(mediaView2))) {
                    mediaView = mediaView2;
                }
            } catch (RemoteException e2) {
                zzayu.zzc("", e2);
            }
        }
        this.zzcwc = mediaView;
    }

    public static zzadf zza(zzade zzade) {
        synchronized (zzcwa) {
            zzadf zzadf = zzcwa.get(zzade.asBinder());
            if (zzadf != null) {
                return zzadf;
            }
            zzadf zzadf2 = new zzadf(zzade);
            zzcwa.put(zzade.asBinder(), zzadf2);
            return zzadf2;
        }
    }

    public final zzade zzro() {
        return this.zzcwb;
    }

    public final CharSequence getText(String str) {
        try {
            return this.zzcwb.zzct(str);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final NativeAd.Image getImage(String str) {
        try {
            zzaci zzcu = this.zzcwb.zzcu(str);
            if (zzcu != null) {
                return new zzacj(zzcu);
            }
            return null;
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final VideoController getVideoController() {
        try {
            zzxb videoController = this.zzcwb.getVideoController();
            if (videoController != null) {
                this.zzcel.zza(videoController);
            }
        } catch (RemoteException e) {
            zzayu.zzc("Exception occurred while getting video controller", e);
        }
        return this.zzcel;
    }

    public final MediaView getVideoMediaView() {
        return this.zzcwc;
    }

    public final List<String> getAvailableAssetNames() {
        try {
            return this.zzcwb.getAvailableAssetNames();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final String getCustomTemplateId() {
        try {
            return this.zzcwb.getCustomTemplateId();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    public final void performClick(String str) {
        try {
            this.zzcwb.performClick(str);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final void recordImpression() {
        try {
            this.zzcwb.recordImpression();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final NativeCustomTemplateAd.DisplayOpenMeasurement getDisplayOpenMeasurement() {
        try {
            if (this.zzcwd == null && this.zzcwb.zzrl()) {
                this.zzcwd = new zzace(this.zzcwb);
            }
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
        return this.zzcwd;
    }

    public final void destroy() {
        try {
            this.zzcwb.destroy();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
