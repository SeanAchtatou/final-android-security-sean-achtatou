package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzdvt {
    protected volatile int zzhhn = -1;

    public void zza(zzdvo zzdvo) throws IOException {
    }

    /* access modifiers changed from: protected */
    public int zzoi() {
        return 0;
    }

    public final int zzazu() {
        int zzoi = zzoi();
        this.zzhhn = zzoi;
        return zzoi;
    }

    public static final byte[] zza(zzdvt zzdvt) {
        byte[] bArr = new byte[zzdvt.zzazu()];
        try {
            zzdvo zzq = zzdvo.zzq(bArr, 0, bArr.length);
            zzdvt.zza(zzq);
            zzq.zzazd();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public String toString() {
        return zzdvw.zzb(this);
    }

    /* renamed from: zzbcr */
    public zzdvt clone() throws CloneNotSupportedException {
        return (zzdvt) super.clone();
    }
}
