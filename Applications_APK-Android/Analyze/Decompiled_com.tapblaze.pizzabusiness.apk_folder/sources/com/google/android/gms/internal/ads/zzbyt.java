package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbyt implements zzded {
    static final zzded zzdoq = new zzbyt();

    private zzbyt() {
    }

    public final Object apply(Object obj) {
        ArrayList arrayList = new ArrayList();
        for (zzabu zzabu : (List) obj) {
            if (zzabu != null) {
                arrayList.add(zzabu);
            }
        }
        return arrayList;
    }
}
