package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzhe {
    int getTrackType();

    int zza(zzgw zzgw) throws zzgl;

    int zzdw() throws zzgl;
}
