package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzdfe<E> extends zzdeu<E> {
    static final zzdeu<Object> zzgut = new zzdfe(new Object[0], 0);
    private final transient int size;
    private final transient Object[] zzguu;

    zzdfe(Object[] objArr, int i) {
        this.zzguu = objArr;
        this.size = i;
    }

    /* access modifiers changed from: package-private */
    public final int zzaqz() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzarc() {
        return false;
    }

    public final int size() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    public final Object[] zzaqy() {
        return this.zzguu;
    }

    /* access modifiers changed from: package-private */
    public final int zzara() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    public final int zza(Object[] objArr, int i) {
        System.arraycopy(this.zzguu, 0, objArr, i, this.size);
        return i + this.size;
    }

    public final E get(int i) {
        zzdei.zzs(i, this.size);
        return this.zzguu[i];
    }
}
