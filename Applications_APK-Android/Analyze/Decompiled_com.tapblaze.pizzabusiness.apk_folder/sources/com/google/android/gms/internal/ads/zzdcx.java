package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzdcx {
    void zza(zzdco zzdco, String str);

    void zza(zzdco zzdco, String str, Throwable th);

    void zzb(zzdco zzdco, String str);

    void zzc(zzdco zzdco, String str);
}
