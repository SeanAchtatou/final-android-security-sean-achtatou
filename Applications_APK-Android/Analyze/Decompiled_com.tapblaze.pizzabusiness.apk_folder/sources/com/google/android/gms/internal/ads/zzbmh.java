package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbmh<AdT> implements zzbmi<AdT> {
    private final Map<String, zzcio<AdT>> zzfft;

    zzbmh(Map<String, zzcio<AdT>> map) {
        this.zzfft = map;
    }

    public final zzcio<AdT> zzd(int i, String str) {
        return this.zzfft.get(str);
    }
}
