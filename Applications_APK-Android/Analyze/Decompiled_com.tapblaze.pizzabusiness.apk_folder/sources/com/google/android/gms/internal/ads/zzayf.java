package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzayf {
    private static zzayf zzdux;
    String zzduy;

    public static zzayf zzxc() {
        if (zzdux == null) {
            zzdux = new zzayf();
        }
        return zzdux;
    }

    private zzayf() {
    }
}
