package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutorService;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzddr {
    ExecutorService zzdt(int i);

    ExecutorService zzdu(int i);
}
