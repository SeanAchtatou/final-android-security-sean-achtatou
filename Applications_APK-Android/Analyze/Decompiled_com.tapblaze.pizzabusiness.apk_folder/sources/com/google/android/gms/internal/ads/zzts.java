package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzts extends zzdvq<zzts> {
    public String zzcae = null;
    private zzsy.zzo zzcaf = null;
    private Integer zzcag = null;
    public zztt zzcah = null;
    private Integer zzcai = null;
    private zzte zzcaj = null;
    private zzte zzcak = null;
    private zzte zzcal = null;

    public zzts() {
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    public final void zza(zzdvo zzdvo) throws IOException {
        String str = this.zzcae;
        if (str != null) {
            zzdvo.zzf(1, str);
        }
        zztt zztt = this.zzcah;
        if (zztt != null) {
            zzdvo.zza(4, zztt);
        }
        super.zza(zzdvo);
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        int zzoi = super.zzoi();
        String str = this.zzcae;
        if (str != null) {
            zzoi += zzdvo.zzg(1, str);
        }
        zztt zztt = this.zzcah;
        return zztt != null ? zzoi + zzdvo.zzb(4, zztt) : zzoi;
    }
}
