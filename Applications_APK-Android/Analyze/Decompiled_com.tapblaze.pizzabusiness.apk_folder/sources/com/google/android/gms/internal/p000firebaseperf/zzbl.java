package com.google.android.gms.internal.p000firebaseperf;

import android.os.Bundle;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbl  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzbl {
    private zzbi zzai;
    private final Bundle zzhj;

    public zzbl() {
        this(new Bundle());
    }

    public zzbl(Bundle bundle) {
        this.zzhj = (Bundle) bundle.clone();
        this.zzai = zzbi.zzco();
    }

    private final boolean containsKey(String str) {
        return str != null && this.zzhj.containsKey(str);
    }

    public final zzbo<Boolean> zzb(String str) {
        if (!containsKey(str)) {
            return zzbo.zzcy();
        }
        try {
            return zzbo.zzc((Boolean) this.zzhj.get(str));
        } catch (ClassCastException e) {
            this.zzai.zzm(String.format("Metadata key %s contains type other than boolean: %s", str, e.getMessage()));
            return zzbo.zzcy();
        }
    }

    public final zzbo<Float> zzd(String str) {
        if (!containsKey(str)) {
            return zzbo.zzcy();
        }
        try {
            return zzbo.zzc((Float) this.zzhj.get(str));
        } catch (ClassCastException e) {
            this.zzai.zzm(String.format("Metadata key %s contains type other than float: %s", str, e.getMessage()));
            return zzbo.zzcy();
        }
    }

    public final zzbo<Long> zze(String str) {
        zzbo<Integer> zzp = zzp(str);
        if (zzp.isPresent()) {
            return zzbo.zzb(Long.valueOf((long) zzp.get().intValue()));
        }
        return zzbo.zzcy();
    }

    private final zzbo<Integer> zzp(String str) {
        if (!containsKey(str)) {
            return zzbo.zzcy();
        }
        try {
            return zzbo.zzc((Integer) this.zzhj.get(str));
        } catch (ClassCastException e) {
            this.zzai.zzm(String.format("Metadata key %s contains type other than int: %s", str, e.getMessage()));
            return zzbo.zzcy();
        }
    }
}
