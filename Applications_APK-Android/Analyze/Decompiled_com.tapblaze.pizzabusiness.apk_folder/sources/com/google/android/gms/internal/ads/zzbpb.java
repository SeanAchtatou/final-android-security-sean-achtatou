package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbpb implements zzdxg<zzboz> {
    private final zzdxp<Set<zzbsu<zzbpa>>> zzfeo;

    public zzbpb(zzdxp<Set<zzbsu<zzbpa>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzboz(this.zzfeo.get());
    }
}
