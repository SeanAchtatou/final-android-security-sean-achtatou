package com.google.android.gms.internal.ads;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzbrl<ListenerT> {
    private final Map<ListenerT, Executor> zzfia = new HashMap();

    protected zzbrl(Set<zzbsu<ListenerT>> set) {
        zzb(set);
    }

    public final synchronized void zza(zzbsu zzbsu) {
        zza(zzbsu.zzfir, zzbsu.executor);
    }

    public final synchronized void zza(Object obj, Executor executor) {
        this.zzfia.put(obj, executor);
    }

    private final synchronized void zzb(Set<zzbsu<ListenerT>> set) {
        for (zzbsu<ListenerT> zza : set) {
            zza(zza);
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void zza(zzbrn zzbrn) {
        for (Map.Entry next : this.zzfia.entrySet()) {
            ((Executor) next.getValue()).execute(new zzbrk(zzbrn, next.getKey()));
        }
    }
}
