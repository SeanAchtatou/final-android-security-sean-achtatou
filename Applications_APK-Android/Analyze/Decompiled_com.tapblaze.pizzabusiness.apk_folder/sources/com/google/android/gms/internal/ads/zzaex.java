package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaex implements zzafn<zzbdi> {
    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        String str = (String) map.get("action");
        if ("tick".equals(str)) {
            String str2 = (String) map.get("label");
            String str3 = (String) map.get("start_label");
            String str4 = (String) map.get("timestamp");
            if (TextUtils.isEmpty(str2)) {
                zzavs.zzez("No label given for CSI tick.");
            } else if (TextUtils.isEmpty(str4)) {
                zzavs.zzez("No timestamp given for CSI tick.");
            } else {
                try {
                    long elapsedRealtime = zzq.zzkx().elapsedRealtime() + (Long.parseLong(str4) - zzq.zzkx().currentTimeMillis());
                    if (TextUtils.isEmpty(str3)) {
                        str3 = "native:view_load";
                    }
                    zzbdi.zzyq().zza(str2, str3, elapsedRealtime);
                } catch (NumberFormatException e) {
                    zzavs.zzd("Malformed timestamp for CSI tick.", e);
                }
            }
        } else if ("experiment".equals(str)) {
            String str5 = (String) map.get("value");
            if (TextUtils.isEmpty(str5)) {
                zzavs.zzez("No value given for CSI experiment.");
                return;
            }
            zzaae zzqp = zzbdi.zzyq().zzqp();
            if (zzqp == null) {
                zzavs.zzez("No ticker for WebView, dropping experiment ID.");
            } else {
                zzqp.zzh("e", str5);
            }
        } else if ("extra".equals(str)) {
            String str6 = (String) map.get("name");
            String str7 = (String) map.get("value");
            if (TextUtils.isEmpty(str7)) {
                zzavs.zzez("No value given for CSI extra.");
            } else if (TextUtils.isEmpty(str6)) {
                zzavs.zzez("No name given for CSI extra.");
            } else {
                zzaae zzqp2 = zzbdi.zzyq().zzqp();
                if (zzqp2 == null) {
                    zzavs.zzez("No ticker for WebView, dropping extra parameter.");
                } else {
                    zzqp2.zzh(str6, str7);
                }
            }
        }
    }
}
