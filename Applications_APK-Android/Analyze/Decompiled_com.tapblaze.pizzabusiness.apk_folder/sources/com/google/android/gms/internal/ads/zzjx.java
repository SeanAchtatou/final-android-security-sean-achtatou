package com.google.android.gms.internal.ads;

import android.support.v4.media.session.PlaybackStateCompat;
import com.flurry.android.Constants;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzjx {
    private final zzoj zzanv = new zzoj(8);
    private int zzapo;

    public final boolean zza(zzjg zzjg) throws IOException, InterruptedException {
        zzjg zzjg2 = zzjg;
        long length = zzjg.getLength();
        long j = PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
        if (length != -1 && length <= PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
            j = length;
        }
        int i = (int) j;
        zzjg2.zza(this.zzanv.data, 0, 4);
        this.zzapo = 4;
        for (long zzip = this.zzanv.zzip(); zzip != 440786851; zzip = ((zzip << 8) & -256) | ((long) (this.zzanv.data[0] & Constants.UNKNOWN))) {
            int i2 = this.zzapo + 1;
            this.zzapo = i2;
            if (i2 == i) {
                return false;
            }
            zzjg2.zza(this.zzanv.data, 0, 1);
        }
        long zzc = zzc(zzjg);
        long j2 = (long) this.zzapo;
        if (zzc != Long.MIN_VALUE && (length == -1 || j2 + zzc < length)) {
            while (true) {
                int i3 = this.zzapo;
                long j3 = j2 + zzc;
                if (((long) i3) < j3) {
                    if (zzc(zzjg) == Long.MIN_VALUE) {
                        return false;
                    }
                    long zzc2 = zzc(zzjg);
                    if (zzc2 < 0 || zzc2 > 2147483647L) {
                        return false;
                    }
                    if (zzc2 != 0) {
                        zzjg2.zzad((int) zzc2);
                        this.zzapo = (int) (((long) this.zzapo) + zzc2);
                    }
                } else if (((long) i3) == j3) {
                    return true;
                }
            }
        }
        return false;
    }

    private final long zzc(zzjg zzjg) throws IOException, InterruptedException {
        int i = 0;
        zzjg.zza(this.zzanv.data, 0, 1);
        byte b = this.zzanv.data[0] & Constants.UNKNOWN;
        if (b == 0) {
            return Long.MIN_VALUE;
        }
        int i2 = 128;
        int i3 = 0;
        while ((b & i2) == 0) {
            i2 >>= 1;
            i3++;
        }
        int i4 = b & (i2 ^ -1);
        zzjg.zza(this.zzanv.data, 1, i3);
        while (i < i3) {
            i++;
            i4 = (this.zzanv.data[i] & Constants.UNKNOWN) + (i4 << 8);
        }
        this.zzapo += i3 + 1;
        return (long) i4;
    }
}
