package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcyp implements zzcxo {
    static final zzcxo zzgjk = new zzcyp();

    private zzcyp() {
    }

    public final void zzt(Object obj) {
        ((zzaro) obj).onRewardedVideoAdLeftApplication();
    }
}
