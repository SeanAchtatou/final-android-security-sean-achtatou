package com.google.android.gms.internal.ads;

import java.util.Comparator;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzuk implements Comparator {
    static final Comparator zzccw = new zzuk();

    private zzuk() {
    }

    public final int compare(Object obj, Object obj2) {
        return zzuh.zzd((String) obj, (String) obj2);
    }
}
