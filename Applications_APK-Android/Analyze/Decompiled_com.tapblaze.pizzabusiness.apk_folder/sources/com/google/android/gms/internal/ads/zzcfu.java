package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcfu implements zzdxg<PackageInfo> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<ApplicationInfo> zzfvc;

    private zzcfu(zzdxp<Context> zzdxp, zzdxp<ApplicationInfo> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfvc = zzdxp2;
    }

    public static zzcfu zzae(zzdxp<Context> zzdxp, zzdxp<ApplicationInfo> zzdxp2) {
        return new zzcfu(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return zzcfi.zza(this.zzejv.get(), this.zzfvc.get());
    }
}
