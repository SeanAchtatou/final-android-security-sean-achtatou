package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import com.google.android.gms.internal.ads.zzayk;
import com.google.android.gms.internal.ads.zzve;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzq extends FrameLayout implements View.OnClickListener {
    private final ImageButton zzdhy;
    private final zzy zzdhz;

    public zzq(Context context, zzp zzp, zzy zzy) {
        super(context);
        this.zzdhz = zzy;
        setOnClickListener(this);
        this.zzdhy = new ImageButton(context);
        this.zzdhy.setImageResource(17301527);
        this.zzdhy.setBackgroundColor(0);
        this.zzdhy.setOnClickListener(this);
        ImageButton imageButton = this.zzdhy;
        zzve.zzou();
        int zza = zzayk.zza(context, zzp.paddingLeft);
        zzve.zzou();
        int zza2 = zzayk.zza(context, 0);
        zzve.zzou();
        int zza3 = zzayk.zza(context, zzp.paddingRight);
        zzve.zzou();
        imageButton.setPadding(zza, zza2, zza3, zzayk.zza(context, zzp.paddingBottom));
        this.zzdhy.setContentDescription("Interstitial close button");
        ImageButton imageButton2 = this.zzdhy;
        zzve.zzou();
        int zza4 = zzayk.zza(context, zzp.size + zzp.paddingLeft + zzp.paddingRight);
        zzve.zzou();
        addView(imageButton2, new FrameLayout.LayoutParams(zza4, zzayk.zza(context, zzp.size + zzp.paddingBottom), 17));
    }

    public final void onClick(View view) {
        zzy zzy = this.zzdhz;
        if (zzy != null) {
            zzy.zztl();
        }
    }

    public final void zzal(boolean z) {
        if (z) {
            this.zzdhy.setVisibility(8);
        } else {
            this.zzdhy.setVisibility(0);
        }
    }
}
