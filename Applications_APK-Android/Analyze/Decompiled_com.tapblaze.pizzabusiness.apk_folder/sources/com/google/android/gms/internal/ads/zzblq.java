package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzblq implements Runnable {
    private final Runnable zzfau;
    private final zzblo zzfez;

    zzblq(zzblo zzblo, Runnable runnable) {
        this.zzfez = zzblo;
        this.zzfau = runnable;
    }

    public final void run() {
        this.zzfez.zze(this.zzfau);
    }
}
