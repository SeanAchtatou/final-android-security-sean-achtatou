package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzatr;
import com.google.android.gms.internal.ads.zzbaj;
import com.google.android.gms.internal.ads.zzbcv;
import com.google.android.gms.internal.ads.zzsr;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zza {
    public final zzbcv zzbko;
    public final zzbaj zzbkp;
    private final zzatr zzbkq;
    private final zzsr zzbkr;

    public zza(zzbcv zzbcv, zzbaj zzbaj, zzatr zzatr, zzsr zzsr) {
        this.zzbko = zzbcv;
        this.zzbkp = zzbaj;
        this.zzbkq = zzatr;
        this.zzbkr = zzsr;
    }
}
