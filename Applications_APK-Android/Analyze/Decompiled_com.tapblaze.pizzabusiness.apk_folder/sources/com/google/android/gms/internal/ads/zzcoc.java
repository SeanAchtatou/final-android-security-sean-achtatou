package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcoc implements zzdxg<zzcnz> {
    private final zzdxp<zzcka> zzgbd;

    private zzcoc(zzdxp<zzcka> zzdxp) {
        this.zzgbd = zzdxp;
    }

    public static zzcoc zzag(zzdxp<zzcka> zzdxp) {
        return new zzcoc(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcnz(this.zzgbd.get());
    }
}
