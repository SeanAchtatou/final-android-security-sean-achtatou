package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzdd  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzdd implements zzfj<Integer, zzdi> {
    zzdd() {
    }

    public final /* synthetic */ Object convert(Object obj) {
        zzdi zzo = zzdi.zzo(((Integer) obj).intValue());
        return zzo == null ? zzdi.SESSION_VERBOSITY_NONE : zzo;
    }
}
