package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class zzkl implements zzfh {
    private final /* synthetic */ String zza;
    private final /* synthetic */ zzkj zzb;

    zzkl(zzkj zzkj, String str) {
        this.zzb = zzkj;
        this.zza = str;
    }

    public final void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.zzb.zza(i, th, bArr, this.zza);
    }
}
