package com.google.android.gms.internal.ads;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.ads.zzso;
import com.google.android.gms.internal.ads.zzsy;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcik {
    private zzazb zzdij;
    private zzcht zzfxo;
    private zzsm zzfya;
    private Context zzup;

    public zzcik(Context context, zzazb zzazb, zzsm zzsm, zzcht zzcht) {
        this.zzup = context;
        this.zzdij = zzazb;
        this.zzfya = zzsm;
        this.zzfxo = zzcht;
    }

    public final void zzamc() {
        try {
            this.zzfxo.zza(new zzcij(this));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.getMessage());
            zzavs.zzex(valueOf.length() != 0 ? "Error in offline signals database startup: ".concat(valueOf) : new String("Error in offline signals database startup: "));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ Void zzb(SQLiteDatabase sQLiteDatabase) throws Exception {
        SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
        ArrayList<zzsy.zzj.zza> zza = zzcih.zza(sQLiteDatabase);
        zzsy.zzj zzj = (zzsy.zzj) ((zzdrt) zzsy.zzj.zzno().zzca(this.zzup.getPackageName()).zzcb(Build.MODEL).zzcd(zzcih.zza(sQLiteDatabase2, 0)).zzc(zza).zzce(zzcih.zza(sQLiteDatabase2, 1)).zzes(zzq.zzkx().currentTimeMillis()).zzet(zzcih.zzb(sQLiteDatabase2, 2)).zzbaf());
        ArrayList arrayList = zza;
        int size = arrayList.size();
        long j = 0;
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            zzsy.zzj.zza zza2 = (zzsy.zzj.zza) obj;
            if (zza2.zznq() == zzte.ENUM_TRUE && zza2.getTimestamp() > j) {
                j = zza2.getTimestamp();
            }
        }
        if (j != 0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("value", Long.valueOf(j));
            sQLiteDatabase2.update("offline_signal_statistics", contentValues, "statistic_name = 'last_successful_request_time'", null);
        }
        this.zzfya.zza(new zzcim(zzj));
        zztt zztt = new zztt();
        zztt.zzcam = Integer.valueOf(this.zzdij.zzdvz);
        zztt.zzcan = Integer.valueOf(this.zzdij.zzdwa);
        zztt.zzcao = Integer.valueOf(this.zzdij.zzdwb ? 0 : 2);
        this.zzfya.zza(new zzcil(zztt));
        this.zzfya.zza(zzso.zza.C0024zza.OFFLINE_UPLOAD);
        sQLiteDatabase2.delete("offline_signal_contents", null, null);
        ContentValues contentValues2 = new ContentValues();
        contentValues2.put("value", (Integer) 0);
        sQLiteDatabase2.update("offline_signal_statistics", contentValues2, "statistic_name = ?", new String[]{"failed_requests"});
        ContentValues contentValues3 = new ContentValues();
        contentValues3.put("value", (Integer) 0);
        sQLiteDatabase2.update("offline_signal_statistics", contentValues3, "statistic_name = ?", new String[]{"total_requests"});
        return null;
    }
}
