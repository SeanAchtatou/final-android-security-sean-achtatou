package com.google.android.gms.internal.ads;

import android.content.Context;
import java.io.File;
import java.util.regex.Pattern;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaxd extends zzak {
    private final Context zzup;

    public static zzu zzbg(Context context) {
        zzu zzu = new zzu(new zzal(new File(context.getCacheDir(), "admob_volley"), 20971520), new zzaxd(context, new zzat()));
        zzu.start();
        return zzu;
    }

    private zzaxd(Context context, zzah zzah) {
        super(zzah);
        this.zzup = context;
    }

    public final zzo zzc(zzq<?> zzq) throws zzae {
        if (zzq.zzg() && zzq.getMethod() == 0) {
            if (Pattern.matches((String) zzve.zzoy().zzd(zzzn.zzcnh), zzq.getUrl())) {
                zzve.zzou();
                if (zzayk.zzc(this.zzup, 13400000)) {
                    zzo zzc = new zzagh(this.zzup).zzc(zzq);
                    if (zzc != null) {
                        String valueOf = String.valueOf(zzq.getUrl());
                        zzavs.zzed(valueOf.length() != 0 ? "Got gmscore asset response: ".concat(valueOf) : new String("Got gmscore asset response: "));
                        return zzc;
                    }
                    String valueOf2 = String.valueOf(zzq.getUrl());
                    zzavs.zzed(valueOf2.length() != 0 ? "Failed to get gmscore asset response: ".concat(valueOf2) : new String("Failed to get gmscore asset response: "));
                }
            }
        }
        return super.zzc(zzq);
    }
}
