package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcmm implements zzded {
    private final zzczl zzfel;
    private final zzbdi zzfpv;
    private final zzcmi zzgaq;
    private final zzcbd zzgar;

    zzcmm(zzcmi zzcmi, zzbdi zzbdi, zzczl zzczl, zzcbd zzcbd) {
        this.zzgaq = zzcmi;
        this.zzfpv = zzbdi;
        this.zzfel = zzczl;
        this.zzgar = zzcbd;
    }

    public final Object apply(Object obj) {
        zzbdi zzbdi = this.zzfpv;
        zzczl zzczl = this.zzfel;
        zzcbd zzcbd = this.zzgar;
        if (zzczl.zzdmf) {
            zzbdi.zzaan();
        }
        zzbdi.zzzu();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzciq)).booleanValue()) {
            zzq.zzks();
            zzawh.zza(zzbdi);
        }
        return zzcbd.zzaeu();
    }
}
