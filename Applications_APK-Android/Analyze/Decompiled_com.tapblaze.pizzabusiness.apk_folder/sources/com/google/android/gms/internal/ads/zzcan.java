package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcan implements zzdgt<zzbdi> {
    private final /* synthetic */ String zzfql;
    private final /* synthetic */ zzafn zzfqm;

    zzcan(zzcaj zzcaj, String str, zzafn zzafn) {
        this.zzfql = str;
        this.zzfqm = zzafn;
    }

    public final void zzb(Throwable th) {
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        ((zzbdi) obj).zza(this.zzfql, this.zzfqm);
    }
}
