package com.google.android.gms.internal.p000firebaseperf;

import com.ironsource.sdk.constants.Constants;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhl  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhl implements Comparable<zzhl>, Map.Entry<K, V> {
    private V value;
    private final /* synthetic */ zzhg zzui;
    private final K zzum;

    zzhl(zzhg zzhg, Map.Entry<K, V> entry) {
        this(zzhg, (Comparable) entry.getKey(), entry.getValue());
    }

    zzhl(zzhg zzhg, K k, V v) {
        this.zzui = zzhg;
        this.zzum = k;
        this.value = v;
    }

    public final V getValue() {
        return this.value;
    }

    public final V setValue(V v) {
        this.zzui.zziz();
        V v2 = this.value;
        this.value = v;
        return v2;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return equals(this.zzum, entry.getKey()) && equals(this.value, entry.getValue());
    }

    public final int hashCode() {
        K k = this.zzum;
        int i = 0;
        int hashCode = k == null ? 0 : k.hashCode();
        V v = this.value;
        if (v != null) {
            i = v.hashCode();
        }
        return hashCode ^ i;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.zzum);
        String valueOf2 = String.valueOf(this.value);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append(Constants.RequestParameters.EQUAL);
        sb.append(valueOf2);
        return sb.toString();
    }

    private static boolean equals(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    public final /* synthetic */ Object getKey() {
        return this.zzum;
    }

    public final /* synthetic */ int compareTo(Object obj) {
        return ((Comparable) getKey()).compareTo((Comparable) ((zzhl) obj).getKey());
    }
}
