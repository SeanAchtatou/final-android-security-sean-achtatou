package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzaqe extends IInterface {
    void zza(zzaxc zzaxc) throws RemoteException;

    void zzb(ParcelFileDescriptor parcelFileDescriptor) throws RemoteException;
}
