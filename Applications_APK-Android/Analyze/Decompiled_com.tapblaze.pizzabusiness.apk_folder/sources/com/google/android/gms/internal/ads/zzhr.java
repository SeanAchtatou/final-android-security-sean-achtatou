package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzhr {
    private final zzddu zzaee;
    /* access modifiers changed from: private */
    public final zzho zzahg;

    public zzhr(zzddu zzddu, zzho zzho) {
        this.zzaee = zzho != null ? (zzddu) zzoc.checkNotNull(zzddu) : null;
        this.zzahg = zzho;
    }

    public final void zzc(zzit zzit) {
        if (this.zzahg != null) {
            this.zzaee.post(new zzhq(this, zzit));
        }
    }

    public final void zzb(String str, long j, long j2) {
        if (this.zzahg != null) {
            this.zzaee.post(new zzht(this, str, j, j2));
        }
    }

    public final void zzc(zzgw zzgw) {
        if (this.zzahg != null) {
            this.zzaee.post(new zzhs(this, zzgw));
        }
    }

    public final void zzb(int i, long j, long j2) {
        if (this.zzahg != null) {
            this.zzaee.post(new zzhv(this, i, j, j2));
        }
    }

    public final void zzd(zzit zzit) {
        if (this.zzahg != null) {
            this.zzaee.post(new zzhu(this, zzit));
        }
    }

    public final void zzs(int i) {
        if (this.zzahg != null) {
            this.zzaee.post(new zzhx(this, i));
        }
    }
}
