package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdjp extends zzdih<zzdma, zzdlz> {
    private final /* synthetic */ zzdjn zzgzg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdjp(zzdjn zzdjn, Class cls) {
        super(cls);
        this.zzgzg = zzdjn;
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        zzdma zzdma = (zzdma) zzdte;
        return (zzdlz) ((zzdrt) zzdlz.zzatz().zzeh(0).zzaj(zzdqk.zzu(zzdpn.zzey(32))).zzbaf());
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdma.zzai(zzdqk);
    }

    public final /* bridge */ /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdma zzdma = (zzdma) zzdte;
    }
}
