package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.games.Notifications;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzeo  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public abstract class zzeo extends zzdy {
    private static final Logger logger = Logger.getLogger(zzeo.class.getName());
    /* access modifiers changed from: private */
    public static final boolean zznf = zzhz.zzjg();
    zzer zzng;

    public static zzeo zza(byte[] bArr) {
        return new zza(bArr, 0, bArr.length);
    }

    public static int zzaa(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    public static int zzac(int i) {
        return 4;
    }

    public static int zzad(int i) {
        return 4;
    }

    private static int zzag(int i) {
        return (i >> 31) ^ (i << 1);
    }

    public static int zzau(long j) {
        int i;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    public static int zzaw(long j) {
        return 8;
    }

    public static int zzax(long j) {
        return 8;
    }

    private static long zzay(long j) {
        return (j >> 63) ^ (j << 1);
    }

    public static int zzb(double d) {
        return 8;
    }

    public static int zzc(float f) {
        return 4;
    }

    public static int zzg(boolean z) {
        return 1;
    }

    public abstract void zza(int i, long j) throws IOException;

    public abstract void zza(int i, zzeb zzeb) throws IOException;

    public abstract void zza(int i, zzgl zzgl) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zza(int i, zzgl zzgl, zzhb zzhb) throws IOException;

    public abstract void zza(int i, String str) throws IOException;

    public abstract void zza(int i, boolean z) throws IOException;

    public abstract void zza(zzeb zzeb) throws IOException;

    public abstract void zzak(String str) throws IOException;

    public abstract void zzaq(long j) throws IOException;

    public abstract void zzas(long j) throws IOException;

    public abstract void zzb(int i, zzeb zzeb) throws IOException;

    public abstract void zzb(zzgl zzgl) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zzb(byte[] bArr, int i, int i2) throws IOException;

    public abstract void zzc(byte b) throws IOException;

    public abstract void zzc(int i, long j) throws IOException;

    public abstract void zze(int i, int i2) throws IOException;

    public abstract void zzf(int i, int i2) throws IOException;

    public abstract void zzg(int i, int i2) throws IOException;

    public abstract int zzgr();

    public abstract void zzi(int i, int i2) throws IOException;

    public abstract void zzu(int i) throws IOException;

    public abstract void zzv(int i) throws IOException;

    public abstract void zzx(int i) throws IOException;

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzeo$zzb */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static class zzb extends IOException {
        zzb() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        zzb(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        zzb(java.lang.String r3, java.lang.Throwable r4) {
            /*
                r2 = this;
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r0 = r3.length()
                java.lang.String r1 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                if (r0 == 0) goto L_0x0011
                java.lang.String r3 = r1.concat(r3)
                goto L_0x0016
            L_0x0011:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r1)
            L_0x0016:
                r2.<init>(r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzeo.zzb.<init>(java.lang.String, java.lang.Throwable):void");
        }
    }

    private zzeo() {
    }

    public final void zzh(int i, int i2) throws IOException {
        zzg(i, zzag(i2));
    }

    public final void zzb(int i, long j) throws IOException {
        zza(i, zzay(j));
    }

    public final void zza(int i, float f) throws IOException {
        zzi(i, Float.floatToRawIntBits(f));
    }

    public final void zza(int i, double d) throws IOException {
        zzc(i, Double.doubleToRawLongBits(d));
    }

    public final void zzw(int i) throws IOException {
        zzv(zzag(i));
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzeo$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static class zza extends zzeo {
        private final byte[] buffer;
        private final int limit;
        private final int offset;
        private int position;

        zza(byte[] bArr, int i, int i2) {
            super();
            if (bArr != null) {
                int i3 = i2 + 0;
                if ((i2 | 0 | (bArr.length - i3)) >= 0) {
                    this.buffer = bArr;
                    this.offset = 0;
                    this.position = 0;
                    this.limit = i3;
                    return;
                }
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), 0, Integer.valueOf(i2)));
            }
            throw new NullPointerException("buffer");
        }

        public final void zze(int i, int i2) throws IOException {
            zzv((i << 3) | i2);
        }

        public final void zzf(int i, int i2) throws IOException {
            zze(i, 0);
            zzu(i2);
        }

        public final void zzg(int i, int i2) throws IOException {
            zze(i, 0);
            zzv(i2);
        }

        public final void zzi(int i, int i2) throws IOException {
            zze(i, 5);
            zzx(i2);
        }

        public final void zza(int i, long j) throws IOException {
            zze(i, 0);
            zzaq(j);
        }

        public final void zzc(int i, long j) throws IOException {
            zze(i, 1);
            zzas(j);
        }

        public final void zza(int i, boolean z) throws IOException {
            zze(i, 0);
            zzc(z ? (byte) 1 : 0);
        }

        public final void zza(int i, String str) throws IOException {
            zze(i, 2);
            zzak(str);
        }

        public final void zza(int i, zzeb zzeb) throws IOException {
            zze(i, 2);
            zza(zzeb);
        }

        public final void zza(zzeb zzeb) throws IOException {
            zzv(zzeb.size());
            zzeb.zza(this);
        }

        public final void zzb(byte[] bArr, int i, int i2) throws IOException {
            zzv(i2);
            write(bArr, 0, i2);
        }

        /* access modifiers changed from: package-private */
        public final void zza(int i, zzgl zzgl, zzhb zzhb) throws IOException {
            zze(i, 2);
            zzdt zzdt = (zzdt) zzgl;
            int zzgc = zzdt.zzgc();
            if (zzgc == -1) {
                zzgc = zzhb.zzm(zzdt);
                zzdt.zzp(zzgc);
            }
            zzv(zzgc);
            zzhb.zza(zzgl, this.zzng);
        }

        public final void zza(int i, zzgl zzgl) throws IOException {
            zze(1, 3);
            zzg(2, i);
            zze(3, 2);
            zzb(zzgl);
            zze(1, 4);
        }

        public final void zzb(int i, zzeb zzeb) throws IOException {
            zze(1, 3);
            zzg(2, i);
            zza(3, zzeb);
            zze(1, 4);
        }

        public final void zzb(zzgl zzgl) throws IOException {
            zzv(zzgl.getSerializedSize());
            zzgl.writeTo(this);
        }

        public final void zzc(byte b) throws IOException {
            try {
                byte[] bArr = this.buffer;
                int i = this.position;
                this.position = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
            }
        }

        public final void zzu(int i) throws IOException {
            if (i >= 0) {
                zzv(i);
            } else {
                zzaq((long) i);
            }
        }

        public final void zzv(int i) throws IOException {
            if (!zzeo.zznf || zzdz.zzgi() || zzgr() < 5) {
                while ((i & -128) != 0) {
                    byte[] bArr = this.buffer;
                    int i2 = this.position;
                    this.position = i2 + 1;
                    bArr[i2] = (byte) ((i & Notifications.NOTIFICATION_TYPES_ALL) | 128);
                    i >>>= 7;
                }
                try {
                    byte[] bArr2 = this.buffer;
                    int i3 = this.position;
                    this.position = i3 + 1;
                    bArr2[i3] = (byte) i;
                } catch (IndexOutOfBoundsException e) {
                    throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
                }
            } else if ((i & -128) == 0) {
                byte[] bArr3 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                zzhz.zza(bArr3, (long) i4, (byte) i);
            } else {
                byte[] bArr4 = this.buffer;
                int i5 = this.position;
                this.position = i5 + 1;
                zzhz.zza(bArr4, (long) i5, (byte) (i | 128));
                int i6 = i >>> 7;
                if ((i6 & -128) == 0) {
                    byte[] bArr5 = this.buffer;
                    int i7 = this.position;
                    this.position = i7 + 1;
                    zzhz.zza(bArr5, (long) i7, (byte) i6);
                    return;
                }
                byte[] bArr6 = this.buffer;
                int i8 = this.position;
                this.position = i8 + 1;
                zzhz.zza(bArr6, (long) i8, (byte) (i6 | 128));
                int i9 = i6 >>> 7;
                if ((i9 & -128) == 0) {
                    byte[] bArr7 = this.buffer;
                    int i10 = this.position;
                    this.position = i10 + 1;
                    zzhz.zza(bArr7, (long) i10, (byte) i9);
                    return;
                }
                byte[] bArr8 = this.buffer;
                int i11 = this.position;
                this.position = i11 + 1;
                zzhz.zza(bArr8, (long) i11, (byte) (i9 | 128));
                int i12 = i9 >>> 7;
                if ((i12 & -128) == 0) {
                    byte[] bArr9 = this.buffer;
                    int i13 = this.position;
                    this.position = i13 + 1;
                    zzhz.zza(bArr9, (long) i13, (byte) i12);
                    return;
                }
                byte[] bArr10 = this.buffer;
                int i14 = this.position;
                this.position = i14 + 1;
                zzhz.zza(bArr10, (long) i14, (byte) (i12 | 128));
                byte[] bArr11 = this.buffer;
                int i15 = this.position;
                this.position = i15 + 1;
                zzhz.zza(bArr11, (long) i15, (byte) (i12 >>> 7));
            }
        }

        public final void zzx(int i) throws IOException {
            try {
                byte[] bArr = this.buffer;
                int i2 = this.position;
                this.position = i2 + 1;
                bArr[i2] = (byte) i;
                byte[] bArr2 = this.buffer;
                int i3 = this.position;
                this.position = i3 + 1;
                bArr2[i3] = (byte) (i >> 8);
                byte[] bArr3 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                bArr3[i4] = (byte) (i >> 16);
                byte[] bArr4 = this.buffer;
                int i5 = this.position;
                this.position = i5 + 1;
                bArr4[i5] = (byte) (i >>> 24);
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
            }
        }

        public final void zzaq(long j) throws IOException {
            if (!zzeo.zznf || zzgr() < 10) {
                while ((j & -128) != 0) {
                    byte[] bArr = this.buffer;
                    int i = this.position;
                    this.position = i + 1;
                    bArr[i] = (byte) ((((int) j) & Notifications.NOTIFICATION_TYPES_ALL) | 128);
                    j >>>= 7;
                }
                try {
                    byte[] bArr2 = this.buffer;
                    int i2 = this.position;
                    this.position = i2 + 1;
                    bArr2[i2] = (byte) ((int) j);
                } catch (IndexOutOfBoundsException e) {
                    throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
                }
            } else {
                while ((j & -128) != 0) {
                    byte[] bArr3 = this.buffer;
                    int i3 = this.position;
                    this.position = i3 + 1;
                    zzhz.zza(bArr3, (long) i3, (byte) ((((int) j) & Notifications.NOTIFICATION_TYPES_ALL) | 128));
                    j >>>= 7;
                }
                byte[] bArr4 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                zzhz.zza(bArr4, (long) i4, (byte) ((int) j));
            }
        }

        public final void zzas(long j) throws IOException {
            try {
                byte[] bArr = this.buffer;
                int i = this.position;
                this.position = i + 1;
                bArr[i] = (byte) ((int) j);
                byte[] bArr2 = this.buffer;
                int i2 = this.position;
                this.position = i2 + 1;
                bArr2[i2] = (byte) ((int) (j >> 8));
                byte[] bArr3 = this.buffer;
                int i3 = this.position;
                this.position = i3 + 1;
                bArr3[i3] = (byte) ((int) (j >> 16));
                byte[] bArr4 = this.buffer;
                int i4 = this.position;
                this.position = i4 + 1;
                bArr4[i4] = (byte) ((int) (j >> 24));
                byte[] bArr5 = this.buffer;
                int i5 = this.position;
                this.position = i5 + 1;
                bArr5[i5] = (byte) ((int) (j >> 32));
                byte[] bArr6 = this.buffer;
                int i6 = this.position;
                this.position = i6 + 1;
                bArr6[i6] = (byte) ((int) (j >> 40));
                byte[] bArr7 = this.buffer;
                int i7 = this.position;
                this.position = i7 + 1;
                bArr7[i7] = (byte) ((int) (j >> 48));
                byte[] bArr8 = this.buffer;
                int i8 = this.position;
                this.position = i8 + 1;
                bArr8[i8] = (byte) ((int) (j >> 56));
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), 1), e);
            }
        }

        private final void write(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.buffer, this.position, i2);
                this.position += i2;
            } catch (IndexOutOfBoundsException e) {
                throw new zzb(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.position), Integer.valueOf(this.limit), Integer.valueOf(i2)), e);
            }
        }

        public final void zza(byte[] bArr, int i, int i2) throws IOException {
            write(bArr, i, i2);
        }

        public final void zzak(String str) throws IOException {
            int i = this.position;
            try {
                int zzaa = zzaa(str.length() * 3);
                int zzaa2 = zzaa(str.length());
                if (zzaa2 == zzaa) {
                    this.position = i + zzaa2;
                    int zza = zzic.zza(str, this.buffer, this.position, zzgr());
                    this.position = i;
                    zzv((zza - i) - zzaa2);
                    this.position = zza;
                    return;
                }
                zzv(zzic.zza(str));
                this.position = zzic.zza(str, this.buffer, this.position, zzgr());
            } catch (zzig e) {
                this.position = i;
                zza(str, e);
            } catch (IndexOutOfBoundsException e2) {
                throw new zzb(e2);
            }
        }

        public final int zzgr() {
            return this.limit - this.position;
        }
    }

    public final void zzar(long j) throws IOException {
        zzaq(zzay(j));
    }

    public final void zzb(float f) throws IOException {
        zzx(Float.floatToRawIntBits(f));
    }

    public final void zza(double d) throws IOException {
        zzas(Double.doubleToRawLongBits(d));
    }

    public final void zzf(boolean z) throws IOException {
        zzc(z ? (byte) 1 : 0);
    }

    public static int zzj(int i, int i2) {
        return zzy(i) + zzz(i2);
    }

    public static int zzk(int i, int i2) {
        return zzy(i) + zzaa(i2);
    }

    public static int zzl(int i, int i2) {
        return zzy(i) + zzaa(zzag(i2));
    }

    public static int zzm(int i, int i2) {
        return zzy(i) + 4;
    }

    public static int zzn(int i, int i2) {
        return zzy(i) + 4;
    }

    public static int zzd(int i, long j) {
        return zzy(i) + zzau(j);
    }

    public static int zze(int i, long j) {
        return zzy(i) + zzau(j);
    }

    public static int zzf(int i, long j) {
        return zzy(i) + zzau(zzay(j));
    }

    public static int zzg(int i, long j) {
        return zzy(i) + 8;
    }

    public static int zzh(int i, long j) {
        return zzy(i) + 8;
    }

    public static int zzb(int i, float f) {
        return zzy(i) + 4;
    }

    public static int zzb(int i, double d) {
        return zzy(i) + 8;
    }

    public static int zzb(int i, boolean z) {
        return zzy(i) + 1;
    }

    public static int zzo(int i, int i2) {
        return zzy(i) + zzz(i2);
    }

    public static int zzb(int i, String str) {
        return zzy(i) + zzal(str);
    }

    public static int zzc(int i, zzeb zzeb) {
        int zzy = zzy(i);
        int size = zzeb.size();
        return zzy + zzaa(size) + size;
    }

    public static int zza(int i, zzfu zzfu) {
        int zzy = zzy(i);
        int serializedSize = zzfu.getSerializedSize();
        return zzy + zzaa(serializedSize) + serializedSize;
    }

    static int zzb(int i, zzgl zzgl, zzhb zzhb) {
        return zzy(i) + zza(zzgl, zzhb);
    }

    public static int zzb(int i, zzgl zzgl) {
        return (zzy(1) << 1) + zzk(2, i) + zzy(3) + zzc(zzgl);
    }

    public static int zzd(int i, zzeb zzeb) {
        return (zzy(1) << 1) + zzk(2, i) + zzc(3, zzeb);
    }

    public static int zzb(int i, zzfu zzfu) {
        return (zzy(1) << 1) + zzk(2, i) + zza(3, zzfu);
    }

    public static int zzy(int i) {
        return zzaa(i << 3);
    }

    public static int zzz(int i) {
        if (i >= 0) {
            return zzaa(i);
        }
        return 10;
    }

    public static int zzab(int i) {
        return zzaa(zzag(i));
    }

    public static int zzat(long j) {
        return zzau(j);
    }

    public static int zzav(long j) {
        return zzau(zzay(j));
    }

    public static int zzae(int i) {
        return zzz(i);
    }

    public static int zzal(String str) {
        int i;
        try {
            i = zzic.zza(str);
        } catch (zzig unused) {
            i = str.getBytes(zzfg.UTF_8).length;
        }
        return zzaa(i) + i;
    }

    public static int zza(zzfu zzfu) {
        int serializedSize = zzfu.getSerializedSize();
        return zzaa(serializedSize) + serializedSize;
    }

    public static int zzb(zzeb zzeb) {
        int size = zzeb.size();
        return zzaa(size) + size;
    }

    public static int zzb(byte[] bArr) {
        int length = bArr.length;
        return zzaa(length) + length;
    }

    public static int zzc(zzgl zzgl) {
        int serializedSize = zzgl.getSerializedSize();
        return zzaa(serializedSize) + serializedSize;
    }

    static int zza(zzgl zzgl, zzhb zzhb) {
        zzdt zzdt = (zzdt) zzgl;
        int zzgc = zzdt.zzgc();
        if (zzgc == -1) {
            zzgc = zzhb.zzm(zzdt);
            zzdt.zzp(zzgc);
        }
        return zzaa(zzgc) + zzgc;
    }

    static int zzaf(int i) {
        return zzaa(i) + i;
    }

    public final void zzgs() {
        if (zzgr() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, com.google.android.gms.internal.firebase-perf.zzig]
     candidates:
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    public final void zza(String str, zzig zzig) throws IOException {
        logger.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) zzig);
        byte[] bytes = str.getBytes(zzfg.UTF_8);
        try {
            zzv(bytes.length);
            zza(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new zzb(e);
        } catch (zzb e2) {
            throw e2;
        }
    }

    @Deprecated
    static int zzc(int i, zzgl zzgl, zzhb zzhb) {
        int zzy = zzy(i) << 1;
        zzdt zzdt = (zzdt) zzgl;
        int zzgc = zzdt.zzgc();
        if (zzgc == -1) {
            zzgc = zzhb.zzm(zzdt);
            zzdt.zzp(zzgc);
        }
        return zzy + zzgc;
    }

    @Deprecated
    public static int zzd(zzgl zzgl) {
        return zzgl.getSerializedSize();
    }

    @Deprecated
    public static int zzah(int i) {
        return zzaa(i);
    }
}
