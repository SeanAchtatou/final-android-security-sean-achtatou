package com.google.android.gms.internal.p000firebaseperf;

import java.util.List;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfm  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public interface zzfm<E> extends List<E>, RandomAccess {
    zzfm<E> zzao(int i);

    boolean zzgf();

    void zzgg();
}
