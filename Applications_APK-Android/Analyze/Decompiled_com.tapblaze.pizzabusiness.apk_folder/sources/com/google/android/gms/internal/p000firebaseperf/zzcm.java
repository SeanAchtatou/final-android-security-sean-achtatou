package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzcm  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzcm extends zzfc<zzcm, zza> implements zzgn {
    private static volatile zzgv<zzcm> zzij;
    /* access modifiers changed from: private */
    public static final zzcm zzjs;
    private int zzie;
    private String zzjm = "";
    private int zzjn;
    private int zzjo;
    private int zzjp;
    private int zzjq;
    private int zzjr;

    private zzcm() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzcm$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzcm, zza> implements zzgn {
        private zza() {
            super(zzcm.zzjs);
        }

        public final zza zzab(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcm) this.zzqq).zzaa(str);
            return this;
        }

        public final zza zzi(int i) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcm) this.zzqq).zzf(i);
            return this;
        }

        public final zza zzj(int i) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcm) this.zzqq).zzg(i);
            return this;
        }

        public final zza zzk(int i) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcm) this.zzqq).zzh(i);
            return this;
        }

        /* synthetic */ zza(zzco zzco) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zzaa(String str) {
        str.getClass();
        this.zzie |= 1;
        this.zzjm = str;
    }

    /* access modifiers changed from: private */
    public final void zzf(int i) {
        this.zzie |= 8;
        this.zzjp = i;
    }

    public final boolean zzds() {
        return (this.zzie & 16) != 0;
    }

    /* access modifiers changed from: private */
    public final void zzg(int i) {
        this.zzie |= 16;
        this.zzjq = i;
    }

    /* access modifiers changed from: private */
    public final void zzh(int i) {
        this.zzie |= 32;
        this.zzjr = i;
    }

    public static zza zzdt() {
        return (zza) zzjs.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzco.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzcm();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzjs, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\b\u0000\u0002\u0004\u0001\u0003\u0004\u0003\u0004\u0004\u0004\u0005\u0004\u0005\u0006\u0004\u0002", new Object[]{"zzie", "zzjm", "zzjn", "zzjp", "zzjq", "zzjr", "zzjo"});
            case 4:
                return zzjs;
            case 5:
                zzgv<zzcm> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzcm.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzjs);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzcm zzdu() {
        return zzjs;
    }

    static {
        zzcm zzcm = new zzcm();
        zzjs = zzcm;
        zzfc.zza(zzcm.class, zzcm);
    }
}
