package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzez;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzez  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public interface zzez<T extends zzez<T>> extends Comparable<T> {
    int getNumber();

    zzgo zza(zzgo zzgo, zzgl zzgl);

    zzgr zza(zzgr zzgr, zzgr zzgr2);

    zzih zzhb();

    zzio zzhc();

    boolean zzhd();

    boolean zzhe();
}
