package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcvv implements zzcub<zzcvs> {
    private ScheduledExecutorService zzffx;
    private zzaoz zzgig;
    private Context zzup;

    public zzcvv(zzaoz zzaoz, ScheduledExecutorService scheduledExecutorService, Context context) {
        this.zzgig = zzaoz;
        this.zzffx = scheduledExecutorService;
        this.zzup = context;
    }

    public final zzdhe<zzcvs> zzanc() {
        return zzdgs.zzb(zzdgs.zza(this.zzgig.zzr(this.zzup), ((Long) zzve.zzoy().zzd(zzzn.zzcmu)).longValue(), TimeUnit.MILLISECONDS, this.zzffx), zzcvu.zzdoq, zzazd.zzdwe);
    }
}
