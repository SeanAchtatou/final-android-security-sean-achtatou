package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzblu implements zzdxg<zzaea> {
    private final zzbls zzffa;

    public zzblu(zzbls zzbls) {
        this.zzffa = zzbls;
    }

    public final /* synthetic */ Object get() {
        return (zzaea) zzdxm.zza(this.zzffa.zzagm(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
