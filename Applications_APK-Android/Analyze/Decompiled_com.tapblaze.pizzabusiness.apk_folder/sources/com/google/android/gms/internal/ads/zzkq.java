package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzkq {
    public int zzavh;
    public final zzks zzaxc;
    public final zzku zzaxd;
    public final zzjo zzaxe;

    public zzkq(zzks zzks, zzku zzku, zzjo zzjo) {
        this.zzaxc = zzks;
        this.zzaxd = zzku;
        this.zzaxe = zzjo;
    }
}
