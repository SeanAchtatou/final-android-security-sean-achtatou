package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdmk extends zzdrt<zzdmk, zza> implements zzdtg {
    private static volatile zzdtn<zzdmk> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmk zzhbr;
    private int zzhaa;
    private zzdqk zzhab = zzdqk.zzhhx;
    private zzdmn zzhbq;

    private zzdmk() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdmk, zza> implements zzdtg {
        private zza() {
            super(zzdmk.zzhbr);
        }

        public final zza zzej(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmk) this.zzhmp).setVersion(0);
            return this;
        }

        public final zza zzb(zzdmn zzdmn) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmk) this.zzhmp).zza(zzdmn);
            return this;
        }

        public final zza zzam(zzdqk zzdqk) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdmk) this.zzhmp).zzs(zzdqk);
            return this;
        }

        /* synthetic */ zza(zzdml zzdml) {
            this();
        }
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public final zzdmn zzaum() {
        zzdmn zzdmn = this.zzhbq;
        return zzdmn == null ? zzdmn.zzaus() : zzdmn;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdmn zzdmn) {
        zzdmn.getClass();
        this.zzhbq = zzdmn;
    }

    public final zzdqk zzass() {
        return this.zzhab;
    }

    /* access modifiers changed from: private */
    public final void zzs(zzdqk zzdqk) {
        zzdqk.getClass();
        this.zzhab = zzdqk;
    }

    public static zzdmk zzal(zzdqk zzdqk) throws zzdse {
        return (zzdmk) zzdrt.zza(zzhbr, zzdqk);
    }

    public static zza zzaun() {
        return (zza) zzhbr.zzazt();
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdml.zzdk[i - 1]) {
            case 1:
                return new zzdmk();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhbr, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\n", new Object[]{"zzhaa", "zzhbq", "zzhab"});
            case 4:
                return zzhbr;
            case 5:
                zzdtn<zzdmk> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmk.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhbr);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzdmk zzdmk = new zzdmk();
        zzhbr = zzdmk;
        zzdrt.zza(zzdmk.class, zzdmk);
    }
}
