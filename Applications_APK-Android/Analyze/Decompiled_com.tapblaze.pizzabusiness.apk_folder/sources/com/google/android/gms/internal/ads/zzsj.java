package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzsj implements Runnable {
    private final zzsg zzbrw;
    private final zzrz zzbrx;
    private final zzry zzbry;
    private final zzazl zzbrz;

    zzsj(zzsg zzsg, zzrz zzrz, zzry zzry, zzazl zzazl) {
        this.zzbrw = zzsg;
        this.zzbrx = zzrz;
        this.zzbry = zzry;
        this.zzbrz = zzazl;
    }

    public final void run() {
        zzsg zzsg = this.zzbrw;
        zzrz zzrz = this.zzbrx;
        zzry zzry = this.zzbry;
        zzazl zzazl = this.zzbrz;
        try {
            zzrx zza = zzrz.zzms().zza(zzry);
            if (!zza.zzmp()) {
                zzazl.setException(new RuntimeException("No entry contents."));
                zzsg.zzbrt.disconnect();
                return;
            }
            zzsl zzsl = new zzsl(zzsg, zza.zzmq(), 1);
            int read = zzsl.read();
            if (read != -1) {
                zzsl.unread(read);
                zzazl.set(zzsl);
                return;
            }
            throw new IOException("Unable to read from cache.");
        } catch (RemoteException | IOException e) {
            zzavs.zzc("Unable to obtain a cache service instance.", e);
            zzazl.setException(e);
            zzsg.zzbrt.disconnect();
        }
    }
}
