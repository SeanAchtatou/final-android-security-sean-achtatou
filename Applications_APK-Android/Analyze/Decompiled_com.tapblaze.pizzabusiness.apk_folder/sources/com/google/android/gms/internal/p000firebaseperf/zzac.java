package com.google.android.gms.internal.p000firebaseperf;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzac  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzac {
    private final ConcurrentHashMap<zzab, List<Throwable>> zzw = new ConcurrentHashMap<>(16, 0.75f, 10);
    private final ReferenceQueue<Throwable> zzx = new ReferenceQueue<>();

    zzac() {
    }

    public final List<Throwable> zza(Throwable th, boolean z) {
        Reference<? extends Throwable> poll = this.zzx.poll();
        while (poll != null) {
            this.zzw.remove(poll);
            poll = this.zzx.poll();
        }
        List<Throwable> list = this.zzw.get(new zzab(th, null));
        if (list != null) {
            return list;
        }
        Vector vector = new Vector(2);
        List<Throwable> putIfAbsent = this.zzw.putIfAbsent(new zzab(th, this.zzx), vector);
        return putIfAbsent == null ? vector : putIfAbsent;
    }
}
