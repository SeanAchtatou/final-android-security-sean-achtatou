package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public abstract class zzder {
    protected zzder() {
    }

    /* access modifiers changed from: protected */
    public abstract Object zzaqw();

    public String toString() {
        return zzaqw().toString();
    }
}
