package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbik implements zzdxg<zzatv> {
    private final zzbhx zzfbh;

    public zzbik(zzbhx zzbhx) {
        this.zzfbh = zzbhx;
    }

    public final /* synthetic */ Object get() {
        return (zzatv) zzdxm.zza(zzq.zzlo(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
