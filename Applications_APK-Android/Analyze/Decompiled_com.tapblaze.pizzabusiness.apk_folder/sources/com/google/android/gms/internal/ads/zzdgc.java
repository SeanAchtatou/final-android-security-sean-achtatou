package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdfs;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.logging.Level;
import java.util.logging.Logger;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
abstract class zzdgc<OutputT> extends zzdfs.zzj<OutputT> {
    private static final Logger zzgvj;
    private static final zzb zzgwi;
    private volatile int remaining;
    /* access modifiers changed from: private */
    public volatile Set<Throwable> seenExceptions = null;

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    static abstract class zzb {
        private zzb() {
        }

        /* access modifiers changed from: package-private */
        public abstract void zza(zzdgc zzdgc, Set<Throwable> set, Set<Throwable> set2);

        /* access modifiers changed from: package-private */
        public abstract int zzc(zzdgc zzdgc);
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    static final class zzc extends zzb {
        private zzc() {
            super();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.ads.zzdgc.zza(com.google.android.gms.internal.ads.zzdgc, java.util.Set):java.util.Set
         arg types: [com.google.android.gms.internal.ads.zzdgc, java.util.Set<java.lang.Throwable>]
         candidates:
          com.google.android.gms.internal.ads.zzdfs.zza(com.google.android.gms.internal.ads.zzdfs, com.google.android.gms.internal.ads.zzdfs$zzd):com.google.android.gms.internal.ads.zzdfs$zzd
          com.google.android.gms.internal.ads.zzdfs.zza(com.google.android.gms.internal.ads.zzdfs, com.google.android.gms.internal.ads.zzdfs$zzk):com.google.android.gms.internal.ads.zzdfs$zzk
          com.google.android.gms.internal.ads.zzdfs.zza(com.google.android.gms.internal.ads.zzdfs, java.lang.Object):java.lang.Object
          com.google.android.gms.internal.ads.zzdfs.zza(java.lang.Runnable, java.util.concurrent.Executor):void
          com.google.android.gms.internal.ads.zzdgc.zza(com.google.android.gms.internal.ads.zzdgc, java.util.Set):java.util.Set */
        /* access modifiers changed from: package-private */
        public final void zza(zzdgc zzdgc, Set<Throwable> set, Set<Throwable> set2) {
            synchronized (zzdgc) {
                if (zzdgc.seenExceptions == null) {
                    Set unused = zzdgc.seenExceptions = (Set) set2;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public final int zzc(zzdgc zzdgc) {
            int zzb;
            synchronized (zzdgc) {
                zzb = zzdgc.zzb(zzdgc);
            }
            return zzb;
        }
    }

    zzdgc(int i) {
        this.remaining = i;
    }

    /* access modifiers changed from: package-private */
    public abstract void zzg(Set<Throwable> set);

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    static final class zza extends zzb {
        private final AtomicReferenceFieldUpdater<zzdgc, Set<Throwable>> zzgwj;
        private final AtomicIntegerFieldUpdater<zzdgc> zzgwk;

        zza(AtomicReferenceFieldUpdater atomicReferenceFieldUpdater, AtomicIntegerFieldUpdater atomicIntegerFieldUpdater) {
            super();
            this.zzgwj = atomicReferenceFieldUpdater;
            this.zzgwk = atomicIntegerFieldUpdater;
        }

        /* access modifiers changed from: package-private */
        public final void zza(zzdgc zzdgc, Set<Throwable> set, Set<Throwable> set2) {
            this.zzgwj.compareAndSet(zzdgc, null, set2);
        }

        /* access modifiers changed from: package-private */
        public final int zzc(zzdgc zzdgc) {
            return this.zzgwk.decrementAndGet(zzdgc);
        }
    }

    /* access modifiers changed from: package-private */
    public final Set<Throwable> zzarp() {
        Set<Throwable> set = this.seenExceptions;
        if (set != null) {
            return set;
        }
        Set newSetFromMap = Collections.newSetFromMap(new ConcurrentHashMap());
        zzg(newSetFromMap);
        zzgwi.zza(this, null, newSetFromMap);
        return this.seenExceptions;
    }

    /* access modifiers changed from: package-private */
    public final int zzarq() {
        return zzgwi.zzc(this);
    }

    /* access modifiers changed from: package-private */
    public final void zzarr() {
        this.seenExceptions = null;
    }

    static /* synthetic */ int zzb(zzdgc zzdgc) {
        int i = zzdgc.remaining - 1;
        zzdgc.remaining = i;
        return i;
    }

    static {
        Throwable th;
        zzb zzb2;
        Class<zzdgc> cls = zzdgc.class;
        zzgvj = Logger.getLogger(cls.getName());
        try {
            zzb2 = new zza(AtomicReferenceFieldUpdater.newUpdater(cls, Set.class, "seenExceptions"), AtomicIntegerFieldUpdater.newUpdater(cls, "remaining"));
            th = null;
        } catch (Throwable th2) {
            zzb2 = new zzc();
            th = th2;
        }
        zzgwi = zzb2;
        if (th != null) {
            zzgvj.logp(Level.SEVERE, "com.google.common.util.concurrent.AggregateFutureState", "<clinit>", "SafeAtomicHelper is broken!", th);
        }
    }
}
