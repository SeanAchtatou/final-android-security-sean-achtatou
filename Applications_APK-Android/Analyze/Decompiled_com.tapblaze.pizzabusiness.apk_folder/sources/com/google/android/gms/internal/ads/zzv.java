package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public enum zzv {
    LOW,
    NORMAL,
    HIGH,
    IMMEDIATE
}
