package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.MuteThisAdReason;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzww implements MuteThisAdReason {
    private final String description;
    private zzwr zzcec;

    public zzww(zzwr zzwr) {
        String str;
        this.zzcec = zzwr;
        try {
            str = zzwr.getDescription();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            str = null;
        }
        this.description = str;
    }

    public final String getDescription() {
        return this.description;
    }

    public final zzwr zzpi() {
        return this.zzcec;
    }

    public final String toString() {
        return this.description;
    }
}
