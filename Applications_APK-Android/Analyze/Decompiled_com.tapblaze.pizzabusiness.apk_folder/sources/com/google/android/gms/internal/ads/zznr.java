package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zznr extends IOException {
    private final int type;
    private final zznq zzbev;

    public zznr(String str, zznq zznq, int i) {
        super(str);
        this.zzbev = zznq;
        this.type = 1;
    }

    public zznr(IOException iOException, zznq zznq, int i) {
        super(iOException);
        this.zzbev = zznq;
        this.type = i;
    }

    public zznr(String str, IOException iOException, zznq zznq, int i) {
        super(str, iOException);
        this.zzbev = zznq;
        this.type = 1;
    }
}
