package com.google.android.gms.internal.p000firebaseperf;

import java.util.NoSuchElementException;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzea  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzea extends zzec {
    private final int limit = this.zzmu.size();
    private int position = 0;
    private final /* synthetic */ zzeb zzmu;

    zzea(zzeb zzeb) {
        this.zzmu = zzeb;
    }

    public final boolean hasNext() {
        return this.position < this.limit;
    }

    public final byte nextByte() {
        int i = this.position;
        if (i < this.limit) {
            this.position = i + 1;
            return this.zzmu.zzr(i);
        }
        throw new NoSuchElementException();
    }
}
