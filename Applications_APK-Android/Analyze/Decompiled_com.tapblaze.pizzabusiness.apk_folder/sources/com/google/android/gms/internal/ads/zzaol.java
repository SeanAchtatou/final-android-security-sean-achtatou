package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzq;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaol extends zzaoo implements zzafn<zzbdi> {
    private float density;
    private int maxHeight = -1;
    private int maxWidth = -1;
    private int rotation;
    private final WindowManager zzbnl;
    private final zzbdi zzcza;
    private final zzyy zzdge;
    private int zzdgf = -1;
    private int zzdgg = -1;
    private int zzdgh = -1;
    private int zzdgi = -1;
    private final Context zzup;
    private DisplayMetrics zzwe;

    public zzaol(zzbdi zzbdi, Context context, zzyy zzyy) {
        super(zzbdi);
        this.zzcza = zzbdi;
        this.zzup = context;
        this.zzdge = zzyy;
        this.zzbnl = (WindowManager) context.getSystemService("window");
    }

    public final void zzj(int i, int i2) {
        int i3 = 0;
        if (this.zzup instanceof Activity) {
            i3 = zzq.zzkq().zzf((Activity) this.zzup)[0];
        }
        if (this.zzcza.zzzy() == null || !this.zzcza.zzzy().zzabt()) {
            int width = this.zzcza.getWidth();
            int height = this.zzcza.getHeight();
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzchq)).booleanValue()) {
                if (width == 0 && this.zzcza.zzzy() != null) {
                    width = this.zzcza.zzzy().widthPixels;
                }
                if (height == 0 && this.zzcza.zzzy() != null) {
                    height = this.zzcza.zzzy().heightPixels;
                }
            }
            this.zzdgh = zzve.zzou().zzb(this.zzup, width);
            this.zzdgi = zzve.zzou().zzb(this.zzup, height);
        }
        zzc(i, i2 - i3, this.zzdgh, this.zzdgi);
        this.zzcza.zzaaa().zzi(i, i2);
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        this.zzwe = new DisplayMetrics();
        Display defaultDisplay = this.zzbnl.getDefaultDisplay();
        defaultDisplay.getMetrics(this.zzwe);
        this.density = this.zzwe.density;
        this.rotation = defaultDisplay.getRotation();
        zzve.zzou();
        DisplayMetrics displayMetrics = this.zzwe;
        this.zzdgf = zzayk.zzb(displayMetrics, displayMetrics.widthPixels);
        zzve.zzou();
        DisplayMetrics displayMetrics2 = this.zzwe;
        this.zzdgg = zzayk.zzb(displayMetrics2, displayMetrics2.heightPixels);
        Activity zzyn = this.zzcza.zzyn();
        if (zzyn == null || zzyn.getWindow() == null) {
            this.maxWidth = this.zzdgf;
            this.maxHeight = this.zzdgg;
        } else {
            zzq.zzkq();
            int[] zzd = zzawb.zzd(zzyn);
            zzve.zzou();
            this.maxWidth = zzayk.zzb(this.zzwe, zzd[0]);
            zzve.zzou();
            this.maxHeight = zzayk.zzb(this.zzwe, zzd[1]);
        }
        if (this.zzcza.zzzy().zzabt()) {
            this.zzdgh = this.zzdgf;
            this.zzdgi = this.zzdgg;
        } else {
            this.zzcza.measure(0, 0);
        }
        zza(this.zzdgf, this.zzdgg, this.maxWidth, this.maxHeight, this.density, this.rotation);
        this.zzcza.zzb("onDeviceFeaturesReceived", new zzaok(new zzaom().zzae(this.zzdge.zzqb()).zzad(this.zzdge.zzqc()).zzaf(this.zzdge.zzqe()).zzag(this.zzdge.zzqd()).zzah(true)).zzth());
        int[] iArr = new int[2];
        this.zzcza.getLocationOnScreen(iArr);
        zzj(zzve.zzou().zzb(this.zzup, iArr[0]), zzve.zzou().zzb(this.zzup, iArr[1]));
        if (zzavs.isLoggable(2)) {
            zzavs.zzey("Dispatching Ready Event.");
        }
        zzdt(this.zzcza.zzyr().zzbma);
    }
}
