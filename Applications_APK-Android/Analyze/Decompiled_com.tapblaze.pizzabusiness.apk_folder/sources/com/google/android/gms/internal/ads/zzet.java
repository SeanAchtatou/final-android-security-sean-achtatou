package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzet extends BroadcastReceiver {
    private final /* synthetic */ zzer zzzj;

    zzet(zzer zzer) {
        this.zzzj = zzer;
    }

    public final void onReceive(Context context, Intent intent) {
        this.zzzj.zzcm();
    }
}
