package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdsr implements zzdtz {
    private static final zzdtb zzhom = new zzdsu();
    private final zzdtb zzhol;

    public zzdsr() {
        this(new zzdst(zzdru.zzbaa(), zzbaz()));
    }

    private zzdsr(zzdtb zzdtb) {
        this.zzhol = (zzdtb) zzdrv.zza(zzdtb, "messageInfoFactory");
    }

    public final <T> zzdua<T> zzg(Class<T> cls) {
        zzduc.zzi(cls);
        zzdtc zzf = this.zzhol.zzf(cls);
        if (zzf.zzbbh()) {
            if (zzdrt.class.isAssignableFrom(cls)) {
                return zzdtk.zza(zzduc.zzbbz(), zzdrj.zzazk(), zzf.zzbbi());
            }
            return zzdtk.zza(zzduc.zzbbx(), zzdrj.zzazl(), zzf.zzbbi());
        } else if (zzdrt.class.isAssignableFrom(cls)) {
            if (zza(zzf)) {
                return zzdti.zza(cls, zzf, zzdto.zzbbk(), zzdso.zzbay(), zzduc.zzbbz(), zzdrj.zzazk(), zzdsz.zzbbe());
            }
            return zzdti.zza(cls, zzf, zzdto.zzbbk(), zzdso.zzbay(), zzduc.zzbbz(), (zzdri<?>) null, zzdsz.zzbbe());
        } else if (zza(zzf)) {
            return zzdti.zza(cls, zzf, zzdto.zzbbj(), zzdso.zzbax(), zzduc.zzbbx(), zzdrj.zzazl(), zzdsz.zzbbd());
        } else {
            return zzdti.zza(cls, zzf, zzdto.zzbbj(), zzdso.zzbax(), zzduc.zzbby(), (zzdri<?>) null, zzdsz.zzbbd());
        }
    }

    private static boolean zza(zzdtc zzdtc) {
        return zzdtc.zzbbg() == zzdrt.zze.zzhna;
    }

    private static zzdtb zzbaz() {
        try {
            return (zzdtb) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return zzhom;
        }
    }
}
