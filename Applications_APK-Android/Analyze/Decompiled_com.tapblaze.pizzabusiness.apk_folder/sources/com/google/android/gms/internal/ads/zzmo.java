package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzmo {
    boolean isReady();

    int zzb(zzgy zzgy, zzis zzis, boolean z);

    void zzeh(long j);

    void zzhk() throws IOException;
}
