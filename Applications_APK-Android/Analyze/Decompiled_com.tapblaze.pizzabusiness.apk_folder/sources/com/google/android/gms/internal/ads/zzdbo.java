package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdbo implements Cloneable {
    public boolean zzgpi;
    public int zzgpj;

    /* access modifiers changed from: private */
    /* renamed from: zzapq */
    public final zzdbo clone() {
        try {
            return (zzdbo) super.clone();
        } catch (CloneNotSupportedException unused) {
            throw new AssertionError();
        }
    }
}
