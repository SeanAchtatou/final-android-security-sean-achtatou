package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzan  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzan extends zzaz<Long> {
    private static zzan zzar;

    private zzan() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.TimeLimitSec";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_rl_time_limit_sec";
    }

    public static synchronized zzan zzap() {
        zzan zzan;
        synchronized (zzan.class) {
            if (zzar == null) {
                zzar = new zzan();
            }
            zzan = zzar;
        }
        return zzan;
    }
}
