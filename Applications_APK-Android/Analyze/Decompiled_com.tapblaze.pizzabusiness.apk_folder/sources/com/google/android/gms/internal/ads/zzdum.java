package com.google.android.gms.internal.ads;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
class zzdum extends AbstractSet<Map.Entry<K, V>> {
    private final /* synthetic */ zzdub zzhqw;

    private zzdum(zzdub zzdub) {
        this.zzhqw = zzdub;
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        return new zzduj(this.zzhqw, null);
    }

    public int size() {
        return this.zzhqw.size();
    }

    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.zzhqw.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.zzhqw.remove(entry.getKey());
        return true;
    }

    public void clear() {
        this.zzhqw.clear();
    }

    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.zzhqw.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    /* synthetic */ zzdum(zzdub zzdub, zzdue zzdue) {
        this(zzdub);
    }
}
