package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzbma {
    private final zzbqp zzffm;

    public zzbma(zzbqp zzbqp) {
        this.zzffm = zzbqp;
    }

    public final zzbqp zzagq() {
        return this.zzffm;
    }
}
