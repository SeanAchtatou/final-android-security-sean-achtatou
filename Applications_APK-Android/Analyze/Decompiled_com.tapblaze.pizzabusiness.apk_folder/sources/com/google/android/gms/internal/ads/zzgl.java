package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzgl extends Exception {
    private final int type;
    private final int zzacl;

    public static zzgl zza(Exception exc, int i) {
        return new zzgl(1, null, exc, i);
    }

    public static zzgl zza(IOException iOException) {
        return new zzgl(0, null, iOException, -1);
    }

    static zzgl zza(RuntimeException runtimeException) {
        return new zzgl(2, null, runtimeException, -1);
    }

    private zzgl(int i, String str, Throwable th, int i2) {
        super(null, th);
        this.type = i;
        this.zzacl = i2;
    }
}
