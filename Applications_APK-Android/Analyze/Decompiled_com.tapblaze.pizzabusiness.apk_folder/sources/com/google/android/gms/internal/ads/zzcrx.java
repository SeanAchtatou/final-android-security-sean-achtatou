package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcrx implements zzcub<zzcru> {
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcrx(Context context, zzdhd zzdhd) {
        this.zzup = context;
        this.zzfov = zzdhd;
    }

    public final zzdhe<zzcru> zzanc() {
        return this.zzfov.zzd(new zzcrw(this));
    }
}
