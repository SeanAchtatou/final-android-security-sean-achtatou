package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbxx implements zzbeu {
    private final Map zzdvw;
    private final zzbxr zzfof;

    zzbxx(zzbxr zzbxr, Map map) {
        this.zzfof = zzbxr;
        this.zzdvw = map;
    }

    public final void zzak(boolean z) {
        this.zzfof.zza(this.zzdvw, z);
    }
}
