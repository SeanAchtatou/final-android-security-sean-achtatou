package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzdik<PrimitiveT, KeyT> {
    private final Class<PrimitiveT> zzgxv;

    public zzdik(Class<PrimitiveT> cls) {
        this.zzgxv = cls;
    }

    public abstract PrimitiveT zzak(KeyT keyt) throws GeneralSecurityException;

    /* access modifiers changed from: package-private */
    public final Class<PrimitiveT> zzarz() {
        return this.zzgxv;
    }
}
