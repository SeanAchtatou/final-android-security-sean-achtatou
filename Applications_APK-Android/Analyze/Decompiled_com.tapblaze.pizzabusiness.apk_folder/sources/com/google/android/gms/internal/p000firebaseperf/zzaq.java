package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzaq  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzaq extends zzaz<Long> {
    private static zzaq zzau;

    private zzaq() {
    }

    /* access modifiers changed from: protected */
    public final String zzaf() {
        return "sessions_max_length_minutes";
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.SessionsMaxDurationMinutes";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_session_max_duration_min";
    }

    public static synchronized zzaq zzas() {
        zzaq zzaq;
        synchronized (zzaq.class) {
            if (zzau == null) {
                zzau = new zzaq();
            }
            zzaq = zzau;
        }
        return zzaq;
    }
}
