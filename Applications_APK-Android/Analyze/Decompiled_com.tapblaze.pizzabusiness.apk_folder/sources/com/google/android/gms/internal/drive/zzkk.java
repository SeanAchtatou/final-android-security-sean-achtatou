package com.google.android.gms.internal.drive;

import com.google.android.gms.internal.drive.zzkk;
import com.google.android.gms.internal.drive.zzkk.zza;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class zzkk<MessageType extends zzkk<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzit<MessageType, BuilderType> {
    private static Map<Object, zzkk<?, ?>> zzrs = new ConcurrentHashMap();
    protected zzmy zzrq = zzmy.zzfa();
    private int zzrr = -1;

    public static class zzb<T extends zzkk<T, ?>> extends zziv<T> {
        private final T zzrt;

        public zzb(T t) {
            this.zzrt = t;
        }
    }

    public static class zzd<ContainingType extends zzlq, Type> extends zzjv<ContainingType, Type> {
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class zze {
        public static final int zzrx = 1;
        public static final int zzry = 2;
        public static final int zzrz = 3;
        public static final int zzsa = 4;
        public static final int zzsb = 5;
        public static final int zzsc = 6;
        public static final int zzsd = 7;
        private static final /* synthetic */ int[] zzse = {zzrx, zzry, zzrz, zzsa, zzsb, zzsc, zzsd};
        public static final int zzsf = 1;
        public static final int zzsg = 2;
        private static final /* synthetic */ int[] zzsh = {zzsf, zzsg};
        public static final int zzsi = 1;
        public static final int zzsj = 2;
        private static final /* synthetic */ int[] zzsk = {zzsi, zzsj};

        public static int[] zzdh() {
            return (int[]) zzse.clone();
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    public static abstract class zzc<MessageType extends zzc<MessageType, BuilderType>, BuilderType> extends zzkk<MessageType, BuilderType> implements zzls {
        protected zzkb<Object> zzrw = zzkb.zzcn();

        /* access modifiers changed from: package-private */
        public final zzkb<Object> zzdg() {
            if (this.zzrw.isImmutable()) {
                this.zzrw = (zzkb) this.zzrw.clone();
            }
            return this.zzrw;
        }
    }

    public String toString() {
        return zzlt.zza(this, super.toString());
    }

    public int hashCode() {
        if (this.zzne != 0) {
            return this.zzne;
        }
        this.zzne = zzmd.zzej().zzq(this).hashCode(this);
        return this.zzne;
    }

    public static abstract class zza<MessageType extends zzkk<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zziu<MessageType, BuilderType> {
        private final MessageType zzrt;
        protected MessageType zzru;
        private boolean zzrv = false;

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected zza(MessageType r3) {
            /*
                r2 = this;
                r2.<init>()
                r2.zzrt = r3
                int r0 = com.google.android.gms.internal.drive.zzkk.zze.zzsa
                r1 = 0
                java.lang.Object r3 = r3.zza(r0, r1, r1)
                com.google.android.gms.internal.drive.zzkk r3 = (com.google.android.gms.internal.drive.zzkk) r3
                r2.zzru = r3
                r3 = 0
                r2.zzrv = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.drive.zzkk.zza.<init>(com.google.android.gms.internal.drive.zzkk):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected final void zzdb() {
            /*
                r3 = this;
                boolean r0 = r3.zzrv
                if (r0 == 0) goto L_0x0019
                MessageType r0 = r3.zzru
                int r1 = com.google.android.gms.internal.drive.zzkk.zze.zzsa
                r2 = 0
                java.lang.Object r0 = r0.zza(r1, r2, r2)
                com.google.android.gms.internal.drive.zzkk r0 = (com.google.android.gms.internal.drive.zzkk) r0
                MessageType r1 = r3.zzru
                zza(r0, r1)
                r3.zzru = r0
                r0 = 0
                r3.zzrv = r0
            L_0x0019:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.drive.zzkk.zza.zzdb():void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.drive.zzkk.zza(com.google.android.gms.internal.drive.zzkk, boolean):boolean
         arg types: [MessageType, int]
         candidates:
          com.google.android.gms.internal.drive.zzkk.zza(java.lang.Class, com.google.android.gms.internal.drive.zzkk):void
          com.google.android.gms.internal.drive.zzkk.zza(com.google.android.gms.internal.drive.zzkk, boolean):boolean */
        public final boolean isInitialized() {
            return zzkk.zza((zzkk) this.zzru, false);
        }

        /* renamed from: zzdc */
        public MessageType zzde() {
            if (this.zzrv) {
                return this.zzru;
            }
            this.zzru.zzbp();
            this.zzrv = true;
            return this.zzru;
        }

        /* renamed from: zzdd */
        public final MessageType zzdf() {
            MessageType messagetype = (zzkk) zzde();
            if (messagetype.isInitialized()) {
                return messagetype;
            }
            throw new zzmw(messagetype);
        }

        public final BuilderType zza(MessageType messagetype) {
            zzdb();
            zza(this.zzru, messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzmd.zzej().zzq(messagetype).zzc(messagetype, messagetype2);
        }

        public final /* synthetic */ zziu zzbn() {
            return (zza) clone();
        }

        public final /* synthetic */ zzlq zzda() {
            return this.zzrt;
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zza zza = (zza) ((zzkk) this.zzrt).zza(zze.zzsb, (Object) null, (Object) null);
            zza.zza((zzkk) zzde());
            return zza;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((zzkk) zza(zze.zzsc, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return zzmd.zzej().zzq(this).equals(this, (zzkk) obj);
    }

    /* access modifiers changed from: protected */
    public final void zzbp() {
        zzmd.zzej().zzq(this).zzd(this);
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzkk<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> BuilderType zzcw() {
        return (zza) zza(zze.zzsb, (Object) null, (Object) null);
    }

    public final boolean isInitialized() {
        return zza(this, Boolean.TRUE.booleanValue());
    }

    /* access modifiers changed from: package-private */
    public final int zzbm() {
        return this.zzrr;
    }

    /* access modifiers changed from: package-private */
    public final void zzo(int i) {
        this.zzrr = i;
    }

    public final void zzb(zzjr zzjr) throws IOException {
        zzmd.zzej().zzf(getClass()).zza(this, zzjt.zza(zzjr));
    }

    public final int zzcx() {
        if (this.zzrr == -1) {
            this.zzrr = zzmd.zzej().zzq(this).zzn(this);
        }
        return this.zzrr;
    }

    static <T extends zzkk<?, ?>> T zzd(Class cls) {
        T t = (zzkk) zzrs.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (zzkk) zzrs.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (t == null) {
            t = (zzkk) ((zzkk) zznd.zzh(cls)).zza(zze.zzsc, (Object) null, (Object) null);
            if (t != null) {
                zzrs.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    protected static <T extends zzkk<?, ?>> void zza(Class cls, zzkk zzkk) {
        zzrs.put(cls, zzkk);
    }

    protected static Object zza(zzlq zzlq, String str, Object[] objArr) {
        return new zzme(zzlq, str, objArr);
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    protected static final <T extends zzkk<T, ?>> boolean zza(zzkk zzkk, boolean z) {
        byte byteValue = ((Byte) zzkk.zza(zze.zzrx, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzp = zzmd.zzej().zzq(zzkk).zzp(zzkk);
        if (z) {
            zzkk.zza(zze.zzry, zzp ? zzkk : null, (Object) null);
        }
        return zzp;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static <T extends com.google.android.gms.internal.drive.zzkk<T, ?>> T zza(T r6, byte[] r7, int r8, int r9, com.google.android.gms.internal.drive.zzjx r10) throws com.google.android.gms.internal.drive.zzkq {
        /*
            int r8 = com.google.android.gms.internal.drive.zzkk.zze.zzsa
            r0 = 0
            java.lang.Object r6 = r6.zza(r8, r0, r0)
            com.google.android.gms.internal.drive.zzkk r6 = (com.google.android.gms.internal.drive.zzkk) r6
            com.google.android.gms.internal.drive.zzmd r8 = com.google.android.gms.internal.drive.zzmd.zzej()     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            com.google.android.gms.internal.drive.zzmf r0 = r8.zzq(r6)     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r3 = 0
            com.google.android.gms.internal.drive.zziz r5 = new com.google.android.gms.internal.drive.zziz     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r5.<init>(r10)     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r1 = r6
            r2 = r7
            r4 = r9
            r0.zza(r1, r2, r3, r4, r5)     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r6.zzbp()     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            int r7 = r6.zzne     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            if (r7 != 0) goto L_0x0025
            return r6
        L_0x0025:
            java.lang.RuntimeException r7 = new java.lang.RuntimeException     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            r7.<init>()     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
            throw r7     // Catch:{ IOException -> 0x0034, IndexOutOfBoundsException -> 0x002b }
        L_0x002b:
            com.google.android.gms.internal.drive.zzkq r7 = com.google.android.gms.internal.drive.zzkq.zzdi()
            com.google.android.gms.internal.drive.zzkq r6 = r7.zzg(r6)
            throw r6
        L_0x0034:
            r7 = move-exception
            java.lang.Throwable r8 = r7.getCause()
            boolean r8 = r8 instanceof com.google.android.gms.internal.drive.zzkq
            if (r8 == 0) goto L_0x0044
            java.lang.Throwable r6 = r7.getCause()
            com.google.android.gms.internal.drive.zzkq r6 = (com.google.android.gms.internal.drive.zzkq) r6
            throw r6
        L_0x0044:
            com.google.android.gms.internal.drive.zzkq r8 = new com.google.android.gms.internal.drive.zzkq
            java.lang.String r7 = r7.getMessage()
            r8.<init>(r7)
            com.google.android.gms.internal.drive.zzkq r6 = r8.zzg(r6)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.drive.zzkk.zza(com.google.android.gms.internal.drive.zzkk, byte[], int, int, com.google.android.gms.internal.drive.zzjx):com.google.android.gms.internal.drive.zzkk");
    }

    protected static <T extends zzkk<T, ?>> T zza(zzkk zzkk, byte[] bArr, zzjx zzjx) throws zzkq {
        T zza2 = zza(zzkk, bArr, 0, bArr.length, zzjx);
        if (zza2 == null || zza2.isInitialized()) {
            return zza2;
        }
        throw new zzkq(new zzmw(zza2).getMessage()).zzg(zza2);
    }

    public final /* synthetic */ zzlr zzcy() {
        zza zza2 = (zza) zza(zze.zzsb, (Object) null, (Object) null);
        zza2.zza(this);
        return zza2;
    }

    public final /* synthetic */ zzlr zzcz() {
        return (zza) zza(zze.zzsb, (Object) null, (Object) null);
    }

    public final /* synthetic */ zzlq zzda() {
        return (zzkk) zza(zze.zzsc, (Object) null, (Object) null);
    }
}
