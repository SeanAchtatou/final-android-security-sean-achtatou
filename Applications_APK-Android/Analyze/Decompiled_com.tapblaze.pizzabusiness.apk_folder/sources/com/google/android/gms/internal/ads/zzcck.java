package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcck implements zzsp {
    private final String zzcyr;
    private final int zzdvv;
    private final zztt zzfrt;
    private final String zzfru;

    zzcck(int i, String str, zztt zztt, String str2) {
        this.zzdvv = i;
        this.zzcyr = str;
        this.zzfrt = zztt;
        this.zzfru = str2;
    }

    public final void zza(zztu zztu) {
        int i = this.zzdvv;
        String str = this.zzcyr;
        zztt zztt = this.zzfrt;
        String str2 = this.zzfru;
        zztu.zzcay.zzbzt = Integer.valueOf(i);
        zztu.zzcav.zzcae = str;
        zztu.zzcav.zzcah = zztt;
        zztu.zzcaq = str2;
    }
}
