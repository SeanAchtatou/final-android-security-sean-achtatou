package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcoq implements zzty {
    private zzvg zzgdd;

    public final synchronized void zzb(zzvg zzvg) {
        this.zzgdd = zzvg;
    }

    public final synchronized void onAdClicked() {
        if (this.zzgdd != null) {
            try {
                this.zzgdd.onAdClicked();
            } catch (RemoteException e) {
                zzavs.zzd("Remote Exception at onAdClicked.", e);
            }
        }
    }
}
