package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzdo  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public enum zzdo implements zzff {
    VISIBILITY_STATE_UNKNOWN(0),
    VISIBLE(1),
    HIDDEN(2),
    PRERENDER(3),
    UNLOADED(4);
    
    private static final zzfi<zzdo> zzja = new zzdq();
    private final int value;

    public final int getNumber() {
        return this.value;
    }

    public static zzfh zzdp() {
        return zzdp.zzjf;
    }

    public final String toString() {
        return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + getNumber() + " name=" + name() + '>';
    }

    private zzdo(int i) {
        this.value = i;
    }
}
