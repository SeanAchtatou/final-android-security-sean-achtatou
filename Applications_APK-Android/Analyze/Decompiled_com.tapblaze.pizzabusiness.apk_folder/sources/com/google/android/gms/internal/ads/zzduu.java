package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzduu extends zzdus<zzdur, zzdur> {
    zzduu() {
    }

    /* access modifiers changed from: package-private */
    public final boolean zza(zzdtu zzdtu) {
        return false;
    }

    private static void zza(Object obj, zzdur zzdur) {
        ((zzdrt) obj).zzhmk = zzdur;
    }

    /* access modifiers changed from: package-private */
    public final void zzan(Object obj) {
        ((zzdrt) obj).zzhmk.zzaxq();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ int zzax(Object obj) {
        return ((zzdur) obj).zzazu();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ int zzbd(Object obj) {
        return ((zzdur) obj).zzbch();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzj(Object obj, Object obj2) {
        zzdur zzdur = (zzdur) obj;
        zzdur zzdur2 = (zzdur) obj2;
        if (zzdur2.equals(zzdur.zzbcf())) {
            return zzdur;
        }
        return zzdur.zza(zzdur, zzdur2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzc(Object obj, zzdvl zzdvl) throws IOException {
        ((zzdur) obj).zza(zzdvl);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(Object obj, zzdvl zzdvl) throws IOException {
        ((zzdur) obj).zzb(zzdvl);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzi(Object obj, Object obj2) {
        zza(obj, (zzdur) obj2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzbc(Object obj) {
        zzdur zzdur = ((zzdrt) obj).zzhmk;
        if (zzdur != zzdur.zzbcf()) {
            return zzdur;
        }
        zzdur zzbcg = zzdur.zzbcg();
        zza(obj, zzbcg);
        return zzbcg;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzbb(Object obj) {
        return ((zzdrt) obj).zzhmk;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzh(Object obj, Object obj2) {
        zza(obj, (zzdur) obj2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzat(Object obj) {
        zzdur zzdur = (zzdur) obj;
        zzdur.zzaxq();
        return zzdur;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzbci() {
        return zzdur.zzbcg();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(Object obj, int i, Object obj2) {
        ((zzdur) obj).zzd((i << 3) | 3, (zzdur) obj2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(Object obj, int i, zzdqk zzdqk) {
        ((zzdur) obj).zzd((i << 3) | 2, zzdqk);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(Object obj, int i, long j) {
        ((zzdur) obj).zzd((i << 3) | 1, Long.valueOf(j));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzc(Object obj, int i, int i2) {
        ((zzdur) obj).zzd((i << 3) | 5, Integer.valueOf(i2));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(Object obj, int i, long j) {
        ((zzdur) obj).zzd(i << 3, Long.valueOf(j));
    }
}
