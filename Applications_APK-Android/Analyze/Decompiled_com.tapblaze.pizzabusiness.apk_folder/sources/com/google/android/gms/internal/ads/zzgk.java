package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzgk {
    long getBufferedPosition();

    long getDuration();

    int getPlaybackState();

    void release();

    void seekTo(long j);

    void stop();

    void zza(zzgn zzgn);

    void zza(zzmb zzmb);

    void zza(zzgp... zzgpArr);

    void zzb(zzgn zzgn);

    void zzb(zzgp... zzgpArr);

    boolean zzea();

    int zzeb();

    long zzec();

    void zzf(boolean z);
}
