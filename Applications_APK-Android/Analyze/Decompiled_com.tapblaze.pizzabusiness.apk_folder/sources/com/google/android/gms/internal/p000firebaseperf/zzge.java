package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzge  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzge implements zzgm {
    private zzgm[] zzsl;

    zzge(zzgm... zzgmArr) {
        this.zzsl = zzgmArr;
    }

    public final boolean zzb(Class<?> cls) {
        for (zzgm zzb : this.zzsl) {
            if (zzb.zzb(cls)) {
                return true;
            }
        }
        return false;
    }

    public final zzgj zzc(Class<?> cls) {
        for (zzgm zzgm : this.zzsl) {
            if (zzgm.zzb(cls)) {
                return zzgm.zzc(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
