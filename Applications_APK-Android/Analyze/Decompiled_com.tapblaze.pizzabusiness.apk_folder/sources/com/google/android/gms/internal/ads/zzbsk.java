package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbsk extends zzbrl<zzbsm> implements zzbsm {
    public zzbsk(Set<zzbsu<zzbsm>> set) {
        super(set);
    }

    public final void zzahx() {
        zza(zzbsj.zzfhp);
    }
}
