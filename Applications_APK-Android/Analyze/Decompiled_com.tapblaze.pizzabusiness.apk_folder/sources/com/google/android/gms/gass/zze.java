package com.google.android.gms.gass;

import android.content.Context;
import android.os.DeadObjectException;
import android.os.HandlerThread;
import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.gass.internal.zzd;
import com.google.android.gms.gass.internal.zzg;
import com.google.android.gms.internal.ads.zzbs;
import com.google.android.gms.internal.ads.zzdrt;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zze implements BaseGmsClient.BaseConnectionCallbacks, BaseGmsClient.BaseOnConnectionFailedListener {
    private final String packageName;
    private final HandlerThread zzdup = new HandlerThread("GassClient");
    private zzd zzgsb;
    private final String zzgsc;
    private final LinkedBlockingQueue<zzbs.zza> zzgsd;

    public zze(Context context, String str, String str2) {
        this.packageName = str;
        this.zzgsc = str2;
        this.zzdup.start();
        this.zzgsb = new zzd(context, this.zzdup.getLooper(), this, this);
        this.zzgsd = new LinkedBlockingQueue<>();
        this.zzgsb.checkAvailabilityAndConnect();
    }

    public final zzbs.zza zzdn(int i) {
        zzbs.zza zza;
        try {
            zza = this.zzgsd.poll(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException unused) {
            zza = null;
        }
        return zza == null ? zzaqi() : zza;
    }

    private final zzg zzaqh() {
        try {
            return this.zzgsb.zzaqp();
        } catch (DeadObjectException | IllegalStateException unused) {
            return null;
        }
    }

    public final void onConnectionSuspended(int i) {
        try {
            this.zzgsd.put(zzaqi());
        } catch (InterruptedException unused) {
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:6|7|11|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0038, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
        zzalu();
        r3.zzdup.quit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r3.zzgsd.put(zzaqi());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002f, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0030, code lost:
        zzalu();
        r3.zzdup.quit();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0025 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onConnected(android.os.Bundle r4) {
        /*
            r3 = this;
            com.google.android.gms.gass.internal.zzg r4 = r3.zzaqh()
            if (r4 == 0) goto L_0x0041
            com.google.android.gms.gass.internal.zzc r0 = new com.google.android.gms.gass.internal.zzc     // Catch:{ all -> 0x0025 }
            java.lang.String r1 = r3.packageName     // Catch:{ all -> 0x0025 }
            java.lang.String r2 = r3.zzgsc     // Catch:{ all -> 0x0025 }
            r0.<init>(r1, r2)     // Catch:{ all -> 0x0025 }
            com.google.android.gms.gass.internal.zze r4 = r4.zza(r0)     // Catch:{ all -> 0x0025 }
            com.google.android.gms.internal.ads.zzbs$zza r4 = r4.zzaqq()     // Catch:{ all -> 0x0025 }
            java.util.concurrent.LinkedBlockingQueue<com.google.android.gms.internal.ads.zzbs$zza> r0 = r3.zzgsd     // Catch:{ all -> 0x0025 }
            r0.put(r4)     // Catch:{ all -> 0x0025 }
            r3.zzalu()
            android.os.HandlerThread r4 = r3.zzdup
            r4.quit()
            return
        L_0x0025:
            java.util.concurrent.LinkedBlockingQueue<com.google.android.gms.internal.ads.zzbs$zza> r4 = r3.zzgsd     // Catch:{ InterruptedException -> 0x0039, all -> 0x002f }
            com.google.android.gms.internal.ads.zzbs$zza r0 = zzaqi()     // Catch:{ InterruptedException -> 0x0039, all -> 0x002f }
            r4.put(r0)     // Catch:{ InterruptedException -> 0x0039, all -> 0x002f }
            goto L_0x0039
        L_0x002f:
            r4 = move-exception
            r3.zzalu()
            android.os.HandlerThread r0 = r3.zzdup
            r0.quit()
            throw r4
        L_0x0039:
            r3.zzalu()
            android.os.HandlerThread r4 = r3.zzdup
            r4.quit()
        L_0x0041:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.gass.zze.onConnected(android.os.Bundle):void");
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        try {
            this.zzgsd.put(zzaqi());
        } catch (InterruptedException unused) {
        }
    }

    private final void zzalu() {
        zzd zzd = this.zzgsb;
        if (zzd == null) {
            return;
        }
        if (zzd.isConnected() || this.zzgsb.isConnecting()) {
            this.zzgsb.disconnect();
        }
    }

    private static zzbs.zza zzaqi() {
        return (zzbs.zza) ((zzdrt) zzbs.zza.zzan().zzau(PlaybackStateCompat.ACTION_PREPARE_FROM_MEDIA_ID).zzbaf());
    }
}
