package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import com.google.android.gms.internal.ads.zzcty;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcrn<S extends zzcty<?>> {
    private final Clock zzbmq;
    public final zzdhe<S> zzgfq;
    private final long zzgfr;

    public zzcrn(zzdhe<S> zzdhe, long j, Clock clock) {
        this.zzgfq = zzdhe;
        this.zzbmq = clock;
        this.zzgfr = clock.elapsedRealtime() + j;
    }

    public final boolean hasExpired() {
        return this.zzgfr < this.zzbmq.elapsedRealtime();
    }
}
