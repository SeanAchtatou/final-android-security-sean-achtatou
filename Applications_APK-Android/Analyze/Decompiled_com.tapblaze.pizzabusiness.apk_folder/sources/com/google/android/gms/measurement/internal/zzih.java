package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
interface zzih {
    void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map);
}
