package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcsm implements Callable {
    private final zzcsn zzgfz;

    zzcsm(zzcsn zzcsn) {
        this.zzgfz = zzcsn;
    }

    public final Object call() {
        return this.zzgfz.zzanj();
    }
}
