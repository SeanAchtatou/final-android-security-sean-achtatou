package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzdwy {
    public abstract void zzhp(String str);

    public static zzdwy zzn(Class cls) {
        if (System.getProperty("java.vm.name").equalsIgnoreCase("Dalvik")) {
            return new zzdwr(cls.getSimpleName());
        }
        return new zzdwt(cls.getSimpleName());
    }
}
