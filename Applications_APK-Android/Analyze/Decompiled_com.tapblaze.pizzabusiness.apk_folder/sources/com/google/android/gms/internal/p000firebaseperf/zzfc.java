package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;
import com.google.android.gms.internal.p000firebaseperf.zzfc.zzb;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfc  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public abstract class zzfc<MessageType extends zzfc<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzdt<MessageType, BuilderType> {
    private static Map<Object, zzfc<?, ?>> zzqn = new ConcurrentHashMap();
    protected zzhw zzql = zzhw.zzje();
    private int zzqm = -1;

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzfc$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static class zza<T extends zzfc<T, ?>> extends zzdx<T> {
        private final T zzqp;

        public zza(T t) {
            this.zzqp = t;
        }
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzfc$zzc */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static final class zzc implements zzez<zzc> {
        public final int getNumber() {
            throw new NoSuchMethodError();
        }

        public final zzih zzhb() {
            throw new NoSuchMethodError();
        }

        public final zzio zzhc() {
            throw new NoSuchMethodError();
        }

        public final boolean zzhd() {
            throw new NoSuchMethodError();
        }

        public final boolean zzhe() {
            throw new NoSuchMethodError();
        }

        public final zzgo zza(zzgo zzgo, zzgl zzgl) {
            throw new NoSuchMethodError();
        }

        public final zzgr zza(zzgr zzgr, zzgr zzgr2) {
            throw new NoSuchMethodError();
        }

        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzfc$zzd */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static abstract class zzd<MessageType extends zzd<MessageType, BuilderType>, BuilderType> extends zzfc<MessageType, BuilderType> implements zzgn {
        protected zzex<zzc> zzqs = zzex.zzgz();
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzfc$zze */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public enum zze {
        GET_MEMOIZED_IS_INITIALIZED,
        SET_MEMOIZED_IS_INITIALIZED,
        BUILD_MESSAGE_INFO,
        NEW_MUTABLE_INSTANCE,
        NEW_BUILDER,
        GET_DEFAULT_INSTANCE,
        GET_PARSER
    }

    /* access modifiers changed from: protected */
    public abstract Object dynamicMethod(zze zze2, Object obj, Object obj2);

    public String toString() {
        return zzgq.zza(this, super.toString());
    }

    public int hashCode() {
        if (this.zzmo != 0) {
            return this.zzmo;
        }
        this.zzmo = zzha.zzio().zzo(this).hashCode(this);
        return this.zzmo;
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzfc$zzb */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static abstract class zzb<MessageType extends zzfc<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzdv<MessageType, BuilderType> {
        private final MessageType zzqp;
        protected MessageType zzqq;
        protected boolean zzqr = false;

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected zzb(MessageType r3) {
            /*
                r2 = this;
                r2.<init>()
                r2.zzqp = r3
                com.google.android.gms.internal.firebase-perf.zzfc$zze r0 = com.google.android.gms.internal.p000firebaseperf.zzfc.zze.NEW_MUTABLE_INSTANCE
                r1 = 0
                java.lang.Object r3 = r3.dynamicMethod(r0, r1, r1)
                com.google.android.gms.internal.firebase-perf.zzfc r3 = (com.google.android.gms.internal.p000firebaseperf.zzfc) r3
                r2.zzqq = r3
                r3 = 0
                r2.zzqr = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzfc.zzb.<init>(com.google.android.gms.internal.firebase-perf.zzfc):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected void zzhl() {
            /*
                r3 = this;
                MessageType r0 = r3.zzqq
                com.google.android.gms.internal.firebase-perf.zzfc$zze r1 = com.google.android.gms.internal.p000firebaseperf.zzfc.zze.NEW_MUTABLE_INSTANCE
                r2 = 0
                java.lang.Object r0 = r0.dynamicMethod(r1, r2, r2)
                com.google.android.gms.internal.firebase-perf.zzfc r0 = (com.google.android.gms.internal.p000firebaseperf.zzfc) r0
                MessageType r1 = r3.zzqq
                zza(r0, r1)
                r3.zzqq = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzfc.zzb.zzhl():void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.firebase-perf.zzfc.zza(com.google.android.gms.internal.firebase-perf.zzfc, boolean):boolean
         arg types: [MessageType, int]
         candidates:
          com.google.android.gms.internal.firebase-perf.zzfc.zza(java.lang.Class, com.google.android.gms.internal.firebase-perf.zzfc):void
          com.google.android.gms.internal.firebase-perf.zzdt.zza(java.lang.Iterable, java.util.List):void
          com.google.android.gms.internal.firebase-perf.zzfc.zza(com.google.android.gms.internal.firebase-perf.zzfc, boolean):boolean */
        public final boolean isInitialized() {
            return zzfc.zza((zzfc) this.zzqq, false);
        }

        /* renamed from: zzhm */
        public MessageType zzho() {
            if (this.zzqr) {
                return this.zzqq;
            }
            MessageType messagetype = this.zzqq;
            zzha.zzio().zzo(messagetype).zzf(messagetype);
            this.zzqr = true;
            return this.zzqq;
        }

        /* renamed from: zzhn */
        public final MessageType zzhp() {
            MessageType messagetype = (zzfc) zzho();
            if (messagetype.isInitialized()) {
                return messagetype;
            }
            throw new zzhu(messagetype);
        }

        public final BuilderType zza(MessageType messagetype) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            zza(this.zzqq, messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzha.zzio().zzo(messagetype).zzd(messagetype, messagetype2);
        }

        public final /* synthetic */ zzdv zzge() {
            return (zzb) clone();
        }

        public final /* synthetic */ zzgl zzhj() {
            return this.zzqp;
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zzb zzb = (zzb) ((zzfc) this.zzqp).dynamicMethod(zze.NEW_BUILDER, null, null);
            zzb.zza((zzfc) zzho());
            return zzb;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return zzha.zzio().zzo(this).equals(this, (zzfc) obj);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzfc<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> BuilderType zzhf() {
        return (zzb) dynamicMethod(zze.NEW_BUILDER, null, null);
    }

    public final boolean isInitialized() {
        return zza(this, Boolean.TRUE.booleanValue());
    }

    /* access modifiers changed from: package-private */
    public final int zzgc() {
        return this.zzqm;
    }

    /* access modifiers changed from: package-private */
    public final void zzp(int i) {
        this.zzqm = i;
    }

    public void writeTo(zzeo zzeo) throws IOException {
        zzha.zzio().zzo(this).zza(this, zzer.zza(zzeo));
    }

    public int getSerializedSize() {
        if (this.zzqm == -1) {
            this.zzqm = zzha.zzio().zzo(this).zzm(this);
        }
        return this.zzqm;
    }

    static <T extends zzfc<?, ?>> T zza(Class<T> cls) {
        T t = (zzfc) zzqn.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (zzfc) zzqn.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (t == null) {
            t = (zzfc) ((zzfc) zzhz.zzg(cls)).dynamicMethod(zze.GET_DEFAULT_INSTANCE, null, null);
            if (t != null) {
                zzqn.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    protected static <T extends zzfc<?, ?>> void zza(Class cls, zzfc zzfc) {
        zzqn.put(cls, zzfc);
    }

    protected static Object zza(zzgl zzgl, String str, Object[] objArr) {
        return new zzhc(zzgl, str, objArr);
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    protected static final <T extends zzfc<T, ?>> boolean zza(zzfc zzfc, boolean z) {
        byte byteValue = ((Byte) zzfc.dynamicMethod(zze.GET_MEMOIZED_IS_INITIALIZED, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzn = zzha.zzio().zzo(zzfc).zzn(zzfc);
        if (z) {
            zzfc.dynamicMethod(zze.SET_MEMOIZED_IS_INITIALIZED, zzn ? zzfc : null, null);
        }
        return zzn;
    }

    protected static zzfk zzhg() {
        return zzfe.zzhq();
    }

    protected static <E> zzfm<E> zzhh() {
        return zzgz.zzin();
    }

    protected static <E> zzfm<E> zza(zzfm<E> zzfm) {
        int size = zzfm.size();
        return zzfm.zzao(size == 0 ? 10 : size << 1);
    }

    public final /* synthetic */ zzgo zzhi() {
        zzb zzb2 = (zzb) dynamicMethod(zze.NEW_BUILDER, null, null);
        zzb2.zza(this);
        return zzb2;
    }

    public final /* synthetic */ zzgl zzhj() {
        return (zzfc) dynamicMethod(zze.GET_DEFAULT_INSTANCE, null, null);
    }
}
