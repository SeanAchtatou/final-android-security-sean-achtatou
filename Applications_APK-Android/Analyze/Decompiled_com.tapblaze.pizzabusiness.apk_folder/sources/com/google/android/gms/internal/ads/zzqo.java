package com.google.android.gms.internal.ads;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzqo {
    private static MessageDigest zzbqe;
    protected Object mLock = new Object();

    /* access modifiers changed from: package-private */
    public abstract byte[] zzbv(String str);

    /* access modifiers changed from: protected */
    public final MessageDigest zzmg() {
        synchronized (this.mLock) {
            if (zzbqe != null) {
                MessageDigest messageDigest = zzbqe;
                return messageDigest;
            }
            for (int i = 0; i < 2; i++) {
                try {
                    zzbqe = MessageDigest.getInstance("MD5");
                } catch (NoSuchAlgorithmException unused) {
                }
            }
            MessageDigest messageDigest2 = zzbqe;
            return messageDigest2;
        }
    }
}
