package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdki implements zzdis<zzdie> {
    zzdki() {
    }

    public final Class<zzdie> zzarz() {
        return zzdie.class;
    }

    public final /* synthetic */ Object zza(zzdiq zzdiq) throws GeneralSecurityException {
        return new zzdkl(zzdiq);
    }
}
