package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfl  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class zzfl extends IOException {
    private zzgl zzrf = null;

    public zzfl(String str) {
        super(str);
    }

    static zzfo zzhr() {
        return new zzfo("Protocol message tag had invalid wire type.");
    }
}
