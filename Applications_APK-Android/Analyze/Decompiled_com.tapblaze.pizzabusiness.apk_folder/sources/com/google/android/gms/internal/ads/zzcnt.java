package com.google.android.gms.internal.ads;

import android.view.View;
import com.google.android.gms.ads.internal.zze;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcnt implements zze {
    private zze zzgcb;

    public final synchronized void zza(zze zze) {
        this.zzgcb = zze;
    }

    public final synchronized void zzg(View view) {
        if (this.zzgcb != null) {
            this.zzgcb.zzg(view);
        }
    }

    public final synchronized void zzjr() {
        if (this.zzgcb != null) {
            this.zzgcb.zzjr();
        }
    }

    public final synchronized void zzjs() {
        if (this.zzgcb != null) {
            this.zzgcb.zzjs();
        }
    }
}
