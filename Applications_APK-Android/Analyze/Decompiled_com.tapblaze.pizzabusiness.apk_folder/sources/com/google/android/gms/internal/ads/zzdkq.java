package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdkq extends zzdii<zzdmv> {
    public zzdkq() {
        super(zzdmv.class, new zzdkt(zzdio.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.HmacKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.SYMMETRIC;
    }

    /* access modifiers changed from: private */
    public static void zza(zzdmz zzdmz) throws GeneralSecurityException {
        if (zzdmz.zzasx() >= 10) {
            int i = zzdkv.zzgzw[zzdmz.zzavf().ordinal()];
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        throw new GeneralSecurityException("unknown hash type");
                    } else if (zzdmz.zzasx() > 64) {
                        throw new GeneralSecurityException("tag size too big");
                    }
                } else if (zzdmz.zzasx() > 32) {
                    throw new GeneralSecurityException("tag size too big");
                }
            } else if (zzdmz.zzasx() > 20) {
                throw new GeneralSecurityException("tag size too big");
            }
        } else {
            throw new GeneralSecurityException("tag size too small");
        }
    }

    public final zzdih<zzdmw, zzdmv> zzasg() {
        return new zzdks(this, zzdmw.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdmv zzdmv = (zzdmv) zzdte;
        zzdpo.zzx(zzdmv.getVersion(), 0);
        if (zzdmv.zzass().size() >= 16) {
            zza(zzdmv.zzauz());
            return;
        }
        throw new GeneralSecurityException("key too short");
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdmv.zzas(zzdqk);
    }
}
