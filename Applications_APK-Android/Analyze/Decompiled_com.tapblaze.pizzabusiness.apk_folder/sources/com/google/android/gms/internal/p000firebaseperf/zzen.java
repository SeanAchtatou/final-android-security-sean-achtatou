package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzen  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public abstract class zzen {
    private int zznc;
    private int zznd;
    private boolean zzne;

    static zzen zza(byte[] bArr, int i, int i2, boolean z) {
        zzep zzep = new zzep(bArr, 0, i2, false);
        try {
            zzep.zzt(i2);
            return zzep;
        } catch (zzfl e) {
            throw new IllegalArgumentException(e);
        }
    }

    public abstract int zzgq();

    public abstract int zzt(int i) throws zzfl;

    private zzen() {
        this.zznc = 100;
        this.zznd = Integer.MAX_VALUE;
        this.zzne = false;
    }
}
