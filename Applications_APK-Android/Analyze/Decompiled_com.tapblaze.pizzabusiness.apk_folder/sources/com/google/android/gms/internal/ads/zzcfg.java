package com.google.android.gms.internal.ads;

import java.util.concurrent.ExecutionException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcfg implements zzdgf {
    static final zzdgf zzbkw = new zzcfg();

    private zzcfg() {
    }

    public final zzdhe zzf(Object obj) {
        return zzdgs.zzk(((ExecutionException) obj).getCause());
    }
}
