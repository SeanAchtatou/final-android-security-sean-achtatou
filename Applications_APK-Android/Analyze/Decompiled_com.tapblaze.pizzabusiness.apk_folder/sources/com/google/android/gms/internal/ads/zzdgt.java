package com.google.android.gms.internal.ads;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzdgt<V> {
    void onSuccess(@NullableDecl V v);

    void zzb(Throwable th);
}
