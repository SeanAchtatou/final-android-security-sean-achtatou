package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.AdapterStatus;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.mediation.rtb.RtbAdapter;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.HashMap;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzxq {
    private static final Object lock = new Object();
    private static zzxq zzcez;
    private zzwk zzcfa;
    private RewardedVideoAd zzcfb;
    private RequestConfiguration zzcfc = new RequestConfiguration.Builder().build();
    private InitializationStatus zzcfd;

    private zzxq() {
    }

    public static zzxq zzpw() {
        zzxq zzxq;
        synchronized (lock) {
            if (zzcez == null) {
                zzcez = new zzxq();
            }
            zzxq = zzcez;
        }
        return zzxq;
    }

    public final void zza(Context context, String str, zzxv zzxv, OnInitializationCompleteListener onInitializationCompleteListener) {
        synchronized (lock) {
            if (this.zzcfa == null) {
                if (context != null) {
                    try {
                        zzaku.zzsj().zzc(context, str);
                        this.zzcfa = (zzwk) new zzux(zzve.zzov(), context).zzd(context, false);
                        if (onInitializationCompleteListener != null) {
                            this.zzcfa.zza(new zzxt(this, onInitializationCompleteListener, null));
                        }
                        this.zzcfa.zza(new zzakz());
                        this.zzcfa.initialize();
                        this.zzcfa.zza(str, ObjectWrapper.wrap(new zzxp(this, context)));
                        if (!(this.zzcfc.getTagForChildDirectedTreatment() == -1 && this.zzcfc.getTagForUnderAgeOfConsent() == -1)) {
                            zza(this.zzcfc);
                        }
                        zzzn.initialize(context);
                        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcom)).booleanValue() && !zzpx()) {
                            zzayu.zzex("Google Mobile Ads SDK initialization functionality unavailable for this session. Ad requests can be made at any time.");
                            this.zzcfd = new zzxr(this);
                            if (onInitializationCompleteListener != null) {
                                zzayk.zzyu.post(new zzxs(this, onInitializationCompleteListener));
                            }
                        }
                    } catch (RemoteException e) {
                        zzayu.zzd("MobileAdsSettingManager initialization failed", e);
                    }
                } else {
                    throw new IllegalArgumentException("Context cannot be null.");
                }
            }
        }
    }

    public final RewardedVideoAd getRewardedVideoAdInstance(Context context) {
        synchronized (lock) {
            if (this.zzcfb != null) {
                RewardedVideoAd rewardedVideoAd = this.zzcfb;
                return rewardedVideoAd;
            }
            this.zzcfb = new zzarw(context, (zzarl) new zzvc(zzve.zzov(), context, new zzakz()).zzd(context, false));
            RewardedVideoAd rewardedVideoAd2 = this.zzcfb;
            return rewardedVideoAd2;
        }
    }

    public final void setAppVolume(float f) {
        boolean z = true;
        Preconditions.checkArgument(0.0f <= f && f <= 1.0f, "The app volume must be a value between 0 and 1 inclusive.");
        if (this.zzcfa == null) {
            z = false;
        }
        Preconditions.checkState(z, "MobileAds.initialize() must be called prior to setting the app volume.");
        try {
            this.zzcfa.setAppVolume(f);
        } catch (RemoteException e) {
            zzayu.zzc("Unable to set app volume.", e);
        }
    }

    public final float zzpe() {
        zzwk zzwk = this.zzcfa;
        if (zzwk == null) {
            return 1.0f;
        }
        try {
            return zzwk.zzpe();
        } catch (RemoteException e) {
            zzayu.zzc("Unable to get app volume.", e);
            return 1.0f;
        }
    }

    public final void setAppMuted(boolean z) {
        Preconditions.checkState(this.zzcfa != null, "MobileAds.initialize() must be called prior to setting app muted state.");
        try {
            this.zzcfa.setAppMuted(z);
        } catch (RemoteException e) {
            zzayu.zzc("Unable to set app mute state.", e);
        }
    }

    public final boolean zzpf() {
        zzwk zzwk = this.zzcfa;
        if (zzwk == null) {
            return false;
        }
        try {
            return zzwk.zzpf();
        } catch (RemoteException e) {
            zzayu.zzc("Unable to get app mute state.", e);
            return false;
        }
    }

    public final void openDebugMenu(Context context, String str) {
        Preconditions.checkState(this.zzcfa != null, "MobileAds.initialize() must be called prior to opening debug menu.");
        try {
            this.zzcfa.zzb(ObjectWrapper.wrap(context), str);
        } catch (RemoteException e) {
            zzayu.zzc("Unable to open debug menu.", e);
        }
    }

    public final String getVersionString() {
        Preconditions.checkState(this.zzcfa != null, "MobileAds.initialize() must be called prior to getting version string.");
        try {
            return this.zzcfa.getVersionString();
        } catch (RemoteException e) {
            zzayu.zzc("Unable to get version string.", e);
            return "";
        }
    }

    public final void registerRtbAdapter(Class<? extends RtbAdapter> cls) {
        try {
            this.zzcfa.zzce(cls.getCanonicalName());
        } catch (RemoteException e) {
            zzayu.zzc("Unable to register RtbAdapter", e);
        }
    }

    public final InitializationStatus getInitializationStatus() {
        Preconditions.checkState(this.zzcfa != null, "MobileAds.initialize() must be called prior to getting initialization status.");
        try {
            if (this.zzcfd != null) {
                return this.zzcfd;
            }
            return zzb(this.zzcfa.zzpg());
        } catch (RemoteException unused) {
            zzayu.zzex("Unable to get Initialization status.");
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static InitializationStatus zzb(List<zzagn> list) {
        HashMap hashMap = new HashMap();
        for (zzagn next : list) {
            hashMap.put(next.zzcyc, new zzagv(next.zzcyd ? AdapterStatus.State.READY : AdapterStatus.State.NOT_READY, next.description, next.zzcye));
        }
        return new zzagy(hashMap);
    }

    public final RequestConfiguration getRequestConfiguration() {
        return this.zzcfc;
    }

    public final void setRequestConfiguration(RequestConfiguration requestConfiguration) {
        Preconditions.checkArgument(requestConfiguration != null, "Null passed to setRequestConfiguration.");
        RequestConfiguration requestConfiguration2 = this.zzcfc;
        this.zzcfc = requestConfiguration;
        if (this.zzcfa != null) {
            if (requestConfiguration2.getTagForChildDirectedTreatment() != requestConfiguration.getTagForChildDirectedTreatment() || requestConfiguration2.getTagForUnderAgeOfConsent() != requestConfiguration.getTagForUnderAgeOfConsent()) {
                zza(requestConfiguration);
            }
        }
    }

    private final void zza(RequestConfiguration requestConfiguration) {
        try {
            this.zzcfa.zza(new zzyq(requestConfiguration));
        } catch (RemoteException e) {
            zzayu.zzc("Unable to set request configuration parcel.", e);
        }
    }

    private final boolean zzpx() throws RemoteException {
        try {
            return this.zzcfa.getVersionString().endsWith(AppEventsConstants.EVENT_PARAM_VALUE_NO);
        } catch (RemoteException unused) {
            zzayu.zzex("Unable to get version string.");
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(OnInitializationCompleteListener onInitializationCompleteListener) {
        onInitializationCompleteListener.onInitializationComplete(this.zzcfd);
    }
}
