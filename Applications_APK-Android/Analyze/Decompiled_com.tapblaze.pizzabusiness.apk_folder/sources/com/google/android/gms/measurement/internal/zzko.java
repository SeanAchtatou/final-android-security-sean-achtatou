package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class zzko implements zzfh {
    private final /* synthetic */ zzkj zza;

    zzko(zzkj zzkj) {
        this.zza = zzkj;
    }

    public final void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.zza.zza(str, i, th, bArr, map);
    }
}
