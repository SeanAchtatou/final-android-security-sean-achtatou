package com.google.android.gms.ads.formats;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.internal.ads.zzabt;
import com.google.android.gms.internal.ads.zzabv;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class MediaView extends FrameLayout {
    private UnifiedNativeAd.MediaContent zzbjp;
    private boolean zzbjq;
    private zzabt zzbjr;
    private ImageView.ScaleType zzbjs;
    private boolean zzbjt;
    private zzabv zzbju;

    public MediaView(Context context) {
        super(context);
    }

    public MediaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MediaView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public MediaView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    public void setMediaContent(UnifiedNativeAd.MediaContent mediaContent) {
        this.zzbjq = true;
        this.zzbjp = mediaContent;
        zzabt zzabt = this.zzbjr;
        if (zzabt != null) {
            zzabt.setMediaContent(mediaContent);
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void zza(zzabt zzabt) {
        this.zzbjr = zzabt;
        if (this.zzbjq) {
            zzabt.setMediaContent(this.zzbjp);
        }
    }

    public void setImageScaleType(ImageView.ScaleType scaleType) {
        this.zzbjt = true;
        this.zzbjs = scaleType;
        zzabv zzabv = this.zzbju;
        if (zzabv != null) {
            zzabv.setImageScaleType(this.zzbjs);
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void zza(zzabv zzabv) {
        this.zzbju = zzabv;
        if (this.zzbjt) {
            zzabv.setImageScaleType(this.zzbjs);
        }
    }
}
