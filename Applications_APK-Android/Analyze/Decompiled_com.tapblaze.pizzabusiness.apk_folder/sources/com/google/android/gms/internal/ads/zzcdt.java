package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcdt {
    /* access modifiers changed from: private */
    public String label;
    /* access modifiers changed from: private */
    public zzdco zzfsr;
    /* access modifiers changed from: private */
    public zzdco zzfss;

    zzcdt(String str, zzdco zzdco, zzdco zzdco2) {
        this.label = str;
        this.zzfsr = zzdco;
        this.zzfss = zzdco2;
    }
}
