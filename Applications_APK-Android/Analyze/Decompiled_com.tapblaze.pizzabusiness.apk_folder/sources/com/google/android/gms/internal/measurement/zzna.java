package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzna implements zznb {
    private static final zzcn<Boolean> zza;
    private static final zzcn<Boolean> zzb;
    private static final zzcn<Boolean> zzc;
    private static final zzcn<Boolean> zzd;

    public final boolean zza() {
        return zza.zzc().booleanValue();
    }

    public final boolean zzb() {
        return zzc.zzc().booleanValue();
    }

    public final boolean zzc() {
        return zzd.zzc().booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, boolean):com.google.android.gms.internal.measurement.zzcn<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, double):com.google.android.gms.internal.measurement.zzcn<java.lang.Double>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, long):com.google.android.gms.internal.measurement.zzcn<java.lang.Long>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.zzcn<java.lang.String>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, boolean):com.google.android.gms.internal.measurement.zzcn<java.lang.Boolean> */
    static {
        zzct zzct = new zzct(zzck.zza("com.google.android.gms.measurement"));
        zza = zzct.zza("measurement.client.sessions.background_sessions_enabled", true);
        zzb = zzct.zza("measurement.client.sessions.immediate_start_enabled_foreground", true);
        zzc = zzct.zza("measurement.client.sessions.remove_expired_session_properties_enabled", true);
        zzd = zzct.zza("measurement.client.sessions.session_id_enabled", true);
    }
}
