package com.google.android.gms.internal.ads;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public abstract class zzdey<K, V> implements Serializable, Map<K, V> {
    private static final Map.Entry<?, ?>[] zzguk = new Map.Entry[0];
    private transient zzdfb<Map.Entry<K, V>> zzgul;
    private transient zzdfb<K> zzgum;
    private transient zzdet<V> zzgun;

    public static <K, V> zzdey<K, V> zza(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        zzdeo.zzb(k, v);
        zzdeo.zzb(k2, v2);
        zzdeo.zzb(k3, v3);
        zzdeo.zzb(k4, v4);
        zzdeo.zzb(k5, v5);
        return zzdfh.zzc(5, new Object[]{k, v, k2, v2, k3, v3, k4, v4, k5, v5});
    }

    public abstract V get(@NullableDecl Object obj);

    /* access modifiers changed from: package-private */
    public abstract zzdfb<Map.Entry<K, V>> zzare();

    /* access modifiers changed from: package-private */
    public abstract zzdfb<K> zzarf();

    /* access modifiers changed from: package-private */
    public abstract zzdet<V> zzarg();

    zzdey() {
    }

    @Deprecated
    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final V remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean containsKey(@NullableDecl Object obj) {
        return get(obj) != null;
    }

    public boolean containsValue(@NullableDecl Object obj) {
        return ((zzdet) values()).contains(obj);
    }

    public final V getOrDefault(@NullableDecl Object obj, @NullableDecl V v) {
        V v2 = get(obj);
        return v2 != null ? v2 : v;
    }

    public boolean equals(@NullableDecl Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    public int hashCode() {
        return zzdfn.zzf((zzdfb) entrySet());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public String toString() {
        int size = size();
        zzdeo.zze(size, "size");
        StringBuilder sb = new StringBuilder((int) Math.min(((long) size) << 3, 1073741824L));
        sb.append('{');
        boolean z = true;
        for (Map.Entry entry : entrySet()) {
            if (!z) {
                sb.append(", ");
            }
            z = false;
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue());
        }
        sb.append('}');
        return sb.toString();
    }

    public /* synthetic */ Set entrySet() {
        zzdfb<Map.Entry<K, V>> zzdfb = this.zzgul;
        if (zzdfb != null) {
            return zzdfb;
        }
        zzdfb<Map.Entry<K, V>> zzare = zzare();
        this.zzgul = zzare;
        return zzare;
    }

    public /* synthetic */ Collection values() {
        zzdet<V> zzdet = this.zzgun;
        if (zzdet != null) {
            return zzdet;
        }
        zzdet<V> zzarg = zzarg();
        this.zzgun = zzarg;
        return zzarg;
    }

    public /* synthetic */ Set keySet() {
        zzdfb<K> zzdfb = this.zzgum;
        if (zzdfb != null) {
            return zzdfb;
        }
        zzdfb<K> zzarf = zzarf();
        this.zzgum = zzarf;
        return zzarf;
    }
}
