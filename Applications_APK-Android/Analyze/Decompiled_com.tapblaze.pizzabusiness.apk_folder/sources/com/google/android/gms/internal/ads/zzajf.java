package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzajf extends zzazo<zzajq> {
    private final Object lock = new Object();
    /* access modifiers changed from: private */
    public final zzajj zzdab;
    private boolean zzdac;

    public zzajf(zzajj zzajj) {
        this.zzdab = zzajj;
    }

    public final void release() {
        synchronized (this.lock) {
            if (!this.zzdac) {
                this.zzdac = true;
                zza(new zzaji(this), new zzazm());
                zza(new zzajh(this), new zzajk(this));
            }
        }
    }
}
