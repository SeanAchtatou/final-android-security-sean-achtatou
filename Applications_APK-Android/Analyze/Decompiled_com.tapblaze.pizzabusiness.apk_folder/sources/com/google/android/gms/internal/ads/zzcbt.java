package com.google.android.gms.internal.ads;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcbt implements View.OnTouchListener {
    private final zzcbp zzfrj;

    zzcbt(zzcbp zzcbp) {
        this.zzfrj = zzcbp;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.zzfrj.zza(view, motionEvent);
    }
}
