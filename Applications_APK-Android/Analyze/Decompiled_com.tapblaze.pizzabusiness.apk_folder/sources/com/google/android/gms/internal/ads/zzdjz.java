package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdjz extends zzdih<zzdoc, zzdnz> {
    private final /* synthetic */ zzdjx zzgzm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdjz(zzdjx zzdjx, Class cls) {
        super(cls);
        this.zzgzm = zzdjx;
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        zzdoc zzdoc = (zzdoc) zzdte;
        return (zzdnz) ((zzdrt) zzdnz.zzawx().zzex(0).zzbc(zzdqk.zzu(zzdpn.zzey(32))).zzbaf());
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdoc.zzbd(zzdqk);
    }

    public final /* bridge */ /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdoc zzdoc = (zzdoc) zzdte;
    }
}
