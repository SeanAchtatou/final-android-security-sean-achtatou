package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.webkit.JsResult;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbdd implements DialogInterface.OnCancelListener {
    private final /* synthetic */ JsResult zzeea;

    zzbdd(JsResult jsResult) {
        this.zzeea = jsResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.zzeea.cancel();
    }
}
