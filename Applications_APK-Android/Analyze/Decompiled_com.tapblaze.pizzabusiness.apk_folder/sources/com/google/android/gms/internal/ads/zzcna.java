package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbpu;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcna<AdT, AdapterT, ListenerT extends zzbpu> implements zzcio<AdT> {
    private final zzcis<AdapterT, ListenerT> zzfaq;
    private final zzdcr zzfgm;
    private final zzcir<AdT, AdapterT, ListenerT> zzgbg;
    private final zzdhd zzgbh;

    public zzcna(zzdcr zzdcr, zzdhd zzdhd, zzcis<AdapterT, ListenerT> zzcis, zzcir<AdT, AdapterT, ListenerT> zzcir) {
        this.zzfgm = zzdcr;
        this.zzgbh = zzdhd;
        this.zzgbg = zzcir;
        this.zzfaq = zzcis;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        return !zzczl.zzglp.isEmpty();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ListenerT
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final com.google.android.gms.internal.ads.zzdhe<AdT> zzb(com.google.android.gms.internal.ads.zzczt r6, com.google.android.gms.internal.ads.zzczl r7) {
        /*
            r5 = this;
            java.util.List<java.lang.String> r0 = r7.zzglp
            java.util.Iterator r0 = r0.iterator()
        L_0x0006:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x001d
            java.lang.Object r1 = r0.next()
            java.lang.String r1 = (java.lang.String) r1
            com.google.android.gms.internal.ads.zzcis<AdapterT, ListenerT> r2 = r5.zzfaq     // Catch:{ zzdab -> 0x001b }
            org.json.JSONObject r3 = r7.zzglr     // Catch:{ zzdab -> 0x001b }
            com.google.android.gms.internal.ads.zzcip r0 = r2.zzd(r1, r3)     // Catch:{ zzdab -> 0x001b }
            goto L_0x001e
        L_0x001b:
            goto L_0x0006
        L_0x001d:
            r0 = 0
        L_0x001e:
            if (r0 != 0) goto L_0x002c
            com.google.android.gms.internal.ads.zzclf r6 = new com.google.android.gms.internal.ads.zzclf
            java.lang.String r7 = "unable to instantiate mediation adapter class"
            r6.<init>(r7)
            com.google.android.gms.internal.ads.zzdhe r6 = com.google.android.gms.internal.ads.zzdgs.zzk(r6)
            return r6
        L_0x002c:
            com.google.android.gms.internal.ads.zzazl r1 = new com.google.android.gms.internal.ads.zzazl
            r1.<init>()
            com.google.android.gms.internal.ads.zzcnb r2 = new com.google.android.gms.internal.ads.zzcnb
            r2.<init>(r5, r1, r0)
            ListenerT r3 = r0.zzfyf
            r3.zza(r2)
            boolean r2 = r7.zzdmf
            if (r2 == 0) goto L_0x0067
            com.google.android.gms.internal.ads.zzczo r2 = r6.zzgmh
            com.google.android.gms.internal.ads.zzczu r2 = r2.zzfgl
            com.google.android.gms.internal.ads.zzug r2 = r2.zzgml
            android.os.Bundle r2 = r2.zzccf
            java.lang.Class<com.google.ads.mediation.admob.AdMobAdapter> r3 = com.google.ads.mediation.admob.AdMobAdapter.class
            java.lang.String r3 = r3.getName()
            android.os.Bundle r3 = r2.getBundle(r3)
            if (r3 != 0) goto L_0x0061
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            java.lang.Class<com.google.ads.mediation.admob.AdMobAdapter> r4 = com.google.ads.mediation.admob.AdMobAdapter.class
            java.lang.String r4 = r4.getName()
            r2.putBundle(r4, r3)
        L_0x0061:
            r2 = 1
            java.lang.String r4 = "render_test_ad_label"
            r3.putBoolean(r4, r2)
        L_0x0067:
            com.google.android.gms.internal.ads.zzdcr r2 = r5.zzfgm
            com.google.android.gms.internal.ads.zzdco r3 = com.google.android.gms.internal.ads.zzdco.ADAPTER_LOAD_AD_SYN
            com.google.android.gms.internal.ads.zzdch r2 = r2.zzu(r3)
            com.google.android.gms.internal.ads.zzcmz r3 = new com.google.android.gms.internal.ads.zzcmz
            r3.<init>(r5, r6, r7, r0)
            com.google.android.gms.internal.ads.zzdhd r4 = r5.zzgbh
            com.google.android.gms.internal.ads.zzdcj r2 = r2.zza(r3, r4)
            com.google.android.gms.internal.ads.zzdco r3 = com.google.android.gms.internal.ads.zzdco.ADAPTER_LOAD_AD_ACK
            com.google.android.gms.internal.ads.zzdcj r2 = r2.zzw(r3)
            com.google.android.gms.internal.ads.zzdcj r1 = r2.zzc(r1)
            com.google.android.gms.internal.ads.zzdco r2 = com.google.android.gms.internal.ads.zzdco.ADAPTER_WRAP_ADAPTER
            com.google.android.gms.internal.ads.zzdcj r1 = r1.zzw(r2)
            com.google.android.gms.internal.ads.zzcnc r2 = new com.google.android.gms.internal.ads.zzcnc
            r2.<init>(r5, r6, r7, r0)
            com.google.android.gms.internal.ads.zzdcj r6 = r1.zzb(r2)
            com.google.android.gms.internal.ads.zzdca r6 = r6.zzaqg()
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcna.zzb(com.google.android.gms.internal.ads.zzczt, com.google.android.gms.internal.ads.zzczl):com.google.android.gms.internal.ads.zzdhe");
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zza(zzczt zzczt, zzczl zzczl, zzcip zzcip, Void voidR) throws Exception {
        return this.zzgbg.zzb(zzczt, zzczl, zzcip);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws Exception {
        this.zzgbg.zza(zzczt, zzczl, zzcip);
    }
}
