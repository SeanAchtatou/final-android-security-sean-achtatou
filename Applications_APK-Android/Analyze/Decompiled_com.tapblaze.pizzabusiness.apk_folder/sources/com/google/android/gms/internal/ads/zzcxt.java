package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzcxt<RequestComponentT, ResponseT> {
    zzdhe<ResponseT> zza(zzcxs zzcxs, zzcxv<RequestComponentT> zzcxv);

    RequestComponentT zzaog();
}
