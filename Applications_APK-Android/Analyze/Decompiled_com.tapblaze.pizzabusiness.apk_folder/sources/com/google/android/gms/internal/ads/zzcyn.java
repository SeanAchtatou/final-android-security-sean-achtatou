package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcyn implements zzcxo {
    static final zzcxo zzgjk = new zzcyn();

    private zzcyn() {
    }

    public final void zzt(Object obj) {
        ((zzasl) obj).onRewardedAdClosed();
    }
}
