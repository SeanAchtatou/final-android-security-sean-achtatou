package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.io.InputStream;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzrx extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzrx> CREATOR = new zzrw();
    private ParcelFileDescriptor zzbri;

    public zzrx() {
        this(null);
    }

    public zzrx(ParcelFileDescriptor parcelFileDescriptor) {
        this.zzbri = parcelFileDescriptor;
    }

    public final synchronized boolean zzmp() {
        return this.zzbri != null;
    }

    public final synchronized InputStream zzmq() {
        if (this.zzbri == null) {
            return null;
        }
        ParcelFileDescriptor.AutoCloseInputStream autoCloseInputStream = new ParcelFileDescriptor.AutoCloseInputStream(this.zzbri);
        this.zzbri = null;
        return autoCloseInputStream;
    }

    private final synchronized ParcelFileDescriptor zzmr() {
        return this.zzbri;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 2, zzmr(), i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
