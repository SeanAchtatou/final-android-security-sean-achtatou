package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzmy extends zzmz {
    private static final int[] zzbds = new int[0];
    private final zznd zzbdt;
    private final AtomicReference<zzmx> zzbdu;

    public zzmy() {
        this(null);
    }

    private static int zze(int i, int i2) {
        if (i == -1) {
            return i2 == -1 ? 0 : -1;
        }
        if (i2 == -1) {
            return 1;
        }
        return i - i2;
    }

    private static boolean zze(int i, boolean z) {
        int i2 = i & 3;
        if (i2 != 3) {
            return z && i2 == 2;
        }
        return true;
    }

    private zzmy(zznd zznd) {
        this.zzbdt = null;
        this.zzbdu = new AtomicReference<>(new zzmx());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzmy.zze(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.ads.zzmy.zze(int, int):int
      com.google.android.gms.internal.ads.zzmy.zze(int, boolean):boolean */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0182, code lost:
        if (r9.zzafa <= r11) goto L_0x0187;
     */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x01d4  */
    /* JADX WARNING: Removed duplicated region for block: B:273:0x01e6 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x019c  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01a8  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01ac  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01ae  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01b1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.ads.zzne[] zza(com.google.android.gms.internal.ads.zzhe[] r37, com.google.android.gms.internal.ads.zzmr[] r38, int[][][] r39) throws com.google.android.gms.internal.ads.zzgl {
        /*
            r36 = this;
            r0 = r37
            int r1 = r0.length
            com.google.android.gms.internal.ads.zzne[] r2 = new com.google.android.gms.internal.ads.zzne[r1]
            r3 = r36
            java.util.concurrent.atomic.AtomicReference<com.google.android.gms.internal.ads.zzmx> r4 = r3.zzbdu
            java.lang.Object r4 = r4.get()
            com.google.android.gms.internal.ads.zzmx r4 = (com.google.android.gms.internal.ads.zzmx) r4
            r6 = 0
            r7 = 0
            r8 = 0
        L_0x0012:
            r9 = 2
            if (r6 >= r1) goto L_0x0263
            r13 = r0[r6]
            int r13 = r13.getTrackType()
            if (r9 != r13) goto L_0x024b
            if (r7 != 0) goto L_0x0232
            r7 = r38[r6]
            r13 = r39[r6]
            int r14 = r4.zzbdm
            int r15 = r4.zzbdn
            int r11 = r4.zzbdo
            int r9 = r4.viewportWidth
            int r5 = r4.viewportHeight
            boolean r10 = r4.zzbdr
            boolean r12 = r4.zzbdp
            boolean r3 = r4.zzbdq
            r21 = r1
            r20 = r4
            r25 = r8
            r0 = 0
            r1 = 0
            r4 = 0
            r22 = 0
            r23 = -1
            r24 = -1
        L_0x0042:
            int r8 = r7.length
            if (r4 >= r8) goto L_0x0210
            com.google.android.gms.internal.ads.zzms r8 = r7.zzav(r4)
            r26 = r7
            java.util.ArrayList r7 = new java.util.ArrayList
            r27 = r2
            int r2 = r8.length
            r7.<init>(r2)
            r28 = r6
            r2 = 0
        L_0x0058:
            int r6 = r8.length
            if (r2 >= r6) goto L_0x0066
            java.lang.Integer r6 = java.lang.Integer.valueOf(r2)
            r7.add(r6)
            int r2 = r2 + 1
            goto L_0x0058
        L_0x0066:
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r9 == r2) goto L_0x0138
            if (r5 != r2) goto L_0x006f
            goto L_0x0138
        L_0x006f:
            r29 = r1
            r6 = 0
        L_0x0072:
            int r1 = r8.length
            if (r6 >= r1) goto L_0x0102
            com.google.android.gms.internal.ads.zzgw r1 = r8.zzaw(r6)
            r30 = r0
            int r0 = r1.width
            if (r0 <= 0) goto L_0x00e8
            int r0 = r1.height
            if (r0 <= 0) goto L_0x00e8
            int r0 = r1.width
            r31 = r12
            int r12 = r1.height
            if (r10 == 0) goto L_0x00a2
            r32 = r10
            if (r0 <= r12) goto L_0x0092
            r10 = 1
            goto L_0x0093
        L_0x0092:
            r10 = 0
        L_0x0093:
            r33 = r5
            if (r9 <= r5) goto L_0x0099
            r5 = 1
            goto L_0x009a
        L_0x0099:
            r5 = 0
        L_0x009a:
            if (r10 == r5) goto L_0x00a6
            r5 = r9
            r34 = r5
            r10 = r33
            goto L_0x00ab
        L_0x00a2:
            r33 = r5
            r32 = r10
        L_0x00a6:
            r10 = r9
            r34 = r10
            r5 = r33
        L_0x00ab:
            int r9 = r0 * r5
            r35 = r11
            int r11 = r12 * r10
            if (r9 < r11) goto L_0x00be
            android.graphics.Point r5 = new android.graphics.Point
            int r0 = com.google.android.gms.internal.ads.zzoq.zzf(r11, r0)
            r5.<init>(r10, r0)
            r0 = r5
            goto L_0x00c7
        L_0x00be:
            android.graphics.Point r0 = new android.graphics.Point
            int r9 = com.google.android.gms.internal.ads.zzoq.zzf(r9, r12)
            r0.<init>(r9, r5)
        L_0x00c7:
            int r5 = r1.width
            int r9 = r1.height
            int r5 = r5 * r9
            int r9 = r1.width
            int r10 = r0.x
            float r10 = (float) r10
            r11 = 1065017672(0x3f7ae148, float:0.98)
            float r10 = r10 * r11
            int r10 = (int) r10
            if (r9 < r10) goto L_0x00f2
            int r1 = r1.height
            int r0 = r0.y
            float r0 = (float) r0
            float r0 = r0 * r11
            int r0 = (int) r0
            if (r1 < r0) goto L_0x00f2
            if (r5 >= r2) goto L_0x00f2
            r2 = r5
            goto L_0x00f2
        L_0x00e8:
            r33 = r5
            r34 = r9
            r32 = r10
            r35 = r11
            r31 = r12
        L_0x00f2:
            int r6 = r6 + 1
            r0 = r30
            r12 = r31
            r10 = r32
            r5 = r33
            r9 = r34
            r11 = r35
            goto L_0x0072
        L_0x0102:
            r30 = r0
            r33 = r5
            r34 = r9
            r32 = r10
            r35 = r11
            r31 = r12
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r2 == r0) goto L_0x0146
            int r0 = r7.size()
            r1 = 1
            int r0 = r0 - r1
        L_0x0119:
            if (r0 < 0) goto L_0x0146
            java.lang.Object r1 = r7.get(r0)
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r1 = r1.intValue()
            com.google.android.gms.internal.ads.zzgw r1 = r8.zzaw(r1)
            int r1 = r1.zzep()
            r5 = -1
            if (r1 == r5) goto L_0x0132
            if (r1 <= r2) goto L_0x0135
        L_0x0132:
            r7.remove(r0)
        L_0x0135:
            int r0 = r0 + -1
            goto L_0x0119
        L_0x0138:
            r30 = r0
            r29 = r1
            r33 = r5
            r34 = r9
            r32 = r10
            r35 = r11
            r31 = r12
        L_0x0146:
            r0 = r13[r4]
            r2 = r22
            r5 = r23
            r6 = r24
            r1 = 0
        L_0x014f:
            int r9 = r8.length
            if (r1 >= r9) goto L_0x01f0
            r9 = r0[r1]
            boolean r9 = zze(r9, r3)
            if (r9 == 0) goto L_0x01e0
            com.google.android.gms.internal.ads.zzgw r9 = r8.zzaw(r1)
            java.lang.Integer r10 = java.lang.Integer.valueOf(r1)
            boolean r10 = r7.contains(r10)
            if (r10 == 0) goto L_0x0189
            int r10 = r9.width
            r11 = -1
            if (r10 == r11) goto L_0x0172
            int r10 = r9.width
            if (r10 > r14) goto L_0x0189
        L_0x0172:
            int r10 = r9.height
            if (r10 == r11) goto L_0x017a
            int r10 = r9.height
            if (r10 > r15) goto L_0x0189
        L_0x017a:
            int r10 = r9.zzafa
            if (r10 == r11) goto L_0x0185
            int r10 = r9.zzafa
            r11 = r35
            if (r10 > r11) goto L_0x018b
            goto L_0x0187
        L_0x0185:
            r11 = r35
        L_0x0187:
            r10 = 1
            goto L_0x018c
        L_0x0189:
            r11 = r35
        L_0x018b:
            r10 = 0
        L_0x018c:
            if (r10 != 0) goto L_0x0196
            if (r31 == 0) goto L_0x0191
            goto L_0x0196
        L_0x0191:
            r23 = r0
            r22 = r3
            goto L_0x01e6
        L_0x0196:
            r22 = r3
            if (r10 == 0) goto L_0x019c
            r12 = 2
            goto L_0x019d
        L_0x019c:
            r12 = 1
        L_0x019d:
            r3 = r0[r1]
            r23 = r0
            r0 = 0
            boolean r3 = zze(r3, r0)
            if (r3 == 0) goto L_0x01aa
            int r12 = r12 + 1000
        L_0x01aa:
            if (r12 <= r2) goto L_0x01ae
            r0 = 1
            goto L_0x01af
        L_0x01ae:
            r0 = 0
        L_0x01af:
            if (r12 != r2) goto L_0x01d2
            int r0 = r9.zzep()
            if (r0 == r5) goto L_0x01c0
            int r0 = r9.zzep()
            int r0 = zze(r0, r5)
            goto L_0x01c6
        L_0x01c0:
            int r0 = r9.zzafa
            int r0 = zze(r0, r6)
        L_0x01c6:
            if (r3 == 0) goto L_0x01cd
            if (r10 == 0) goto L_0x01cd
            if (r0 <= 0) goto L_0x01d1
            goto L_0x01cf
        L_0x01cd:
            if (r0 >= 0) goto L_0x01d1
        L_0x01cf:
            r0 = 1
            goto L_0x01d2
        L_0x01d1:
            r0 = 0
        L_0x01d2:
            if (r0 == 0) goto L_0x01e6
            int r6 = r9.zzafa
            int r5 = r9.zzep()
            r29 = r1
            r30 = r8
            r2 = r12
            goto L_0x01e6
        L_0x01e0:
            r23 = r0
            r22 = r3
            r11 = r35
        L_0x01e6:
            int r1 = r1 + 1
            r35 = r11
            r3 = r22
            r0 = r23
            goto L_0x014f
        L_0x01f0:
            r22 = r3
            r11 = r35
            int r4 = r4 + 1
            r23 = r5
            r24 = r6
            r7 = r26
            r6 = r28
            r1 = r29
            r0 = r30
            r12 = r31
            r10 = r32
            r5 = r33
            r9 = r34
            r22 = r2
            r2 = r27
            goto L_0x0042
        L_0x0210:
            r30 = r0
            r29 = r1
            r27 = r2
            r28 = r6
            if (r30 != 0) goto L_0x021d
            r16 = 0
            goto L_0x0228
        L_0x021d:
            com.google.android.gms.internal.ads.zzna r11 = new com.google.android.gms.internal.ads.zzna
            r1 = r29
            r0 = r30
            r11.<init>(r0, r1)
            r16 = r11
        L_0x0228:
            r27[r28] = r16
            r0 = r27[r28]
            if (r0 == 0) goto L_0x0230
            r7 = 1
            goto L_0x023c
        L_0x0230:
            r7 = 0
            goto L_0x023c
        L_0x0232:
            r21 = r1
            r27 = r2
            r20 = r4
            r28 = r6
            r25 = r8
        L_0x023c:
            r0 = r38[r28]
            int r0 = r0.length
            if (r0 <= 0) goto L_0x0245
            r19 = 1
            goto L_0x0247
        L_0x0245:
            r19 = 0
        L_0x0247:
            r0 = r25 | r19
            r8 = r0
            goto L_0x0255
        L_0x024b:
            r21 = r1
            r27 = r2
            r20 = r4
            r28 = r6
            r25 = r8
        L_0x0255:
            int r6 = r28 + 1
            r3 = r36
            r0 = r37
            r4 = r20
            r1 = r21
            r2 = r27
            goto L_0x0012
        L_0x0263:
            r27 = r2
            r20 = r4
            r25 = r8
            r0 = r1
            r1 = 0
            r2 = 0
            r3 = 0
        L_0x026d:
            if (r1 >= r0) goto L_0x0456
            r4 = r37[r1]
            int r4 = r4.getTrackType()
            r5 = 3
            r6 = 1
            if (r4 == r6) goto L_0x03af
            r6 = 2
            if (r4 == r6) goto L_0x03a7
            if (r4 == r5) goto L_0x02fd
            r4 = r37[r1]
            r4.getTrackType()
            r4 = r38[r1]
            r5 = r39[r1]
            r6 = r20
            boolean r7 = r6.zzbdq
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
        L_0x028f:
            int r12 = r4.length
            if (r8 >= r12) goto L_0x02e7
            com.google.android.gms.internal.ads.zzms r12 = r4.zzav(r8)
            r13 = r5[r8]
            r14 = r11
            r11 = r10
            r10 = r9
            r9 = 0
        L_0x029d:
            int r15 = r12.length
            if (r9 >= r15) goto L_0x02dd
            r15 = r13[r9]
            boolean r15 = zze(r15, r7)
            if (r15 == 0) goto L_0x02d2
            com.google.android.gms.internal.ads.zzgw r15 = r12.zzaw(r9)
            int r15 = r15.zzafu
            r19 = 1
            r15 = r15 & 1
            if (r15 == 0) goto L_0x02b7
            r15 = 1
            goto L_0x02b8
        L_0x02b7:
            r15 = 0
        L_0x02b8:
            r21 = r0
            if (r15 == 0) goto L_0x02be
            r15 = 2
            goto L_0x02bf
        L_0x02be:
            r15 = 1
        L_0x02bf:
            r0 = r13[r9]
            r20 = r4
            r4 = 0
            boolean r0 = zze(r0, r4)
            if (r0 == 0) goto L_0x02cc
            int r15 = r15 + 1000
        L_0x02cc:
            if (r15 <= r14) goto L_0x02d6
            r11 = r9
            r10 = r12
            r14 = r15
            goto L_0x02d6
        L_0x02d2:
            r21 = r0
            r20 = r4
        L_0x02d6:
            int r9 = r9 + 1
            r4 = r20
            r0 = r21
            goto L_0x029d
        L_0x02dd:
            r21 = r0
            r20 = r4
            int r8 = r8 + 1
            r9 = r10
            r10 = r11
            r11 = r14
            goto L_0x028f
        L_0x02e7:
            r21 = r0
            if (r9 != 0) goto L_0x02ed
            r11 = 0
            goto L_0x02f2
        L_0x02ed:
            com.google.android.gms.internal.ads.zzna r11 = new com.google.android.gms.internal.ads.zzna
            r11.<init>(r9, r10)
        L_0x02f2:
            r27[r1] = r11
            r18 = r2
            r2 = 0
            r5 = -1
            r15 = 0
            r17 = 2
            goto L_0x0449
        L_0x02fd:
            r21 = r0
            r6 = r20
            if (r3 != 0) goto L_0x03ab
            r0 = r38[r1]
            r3 = r39[r1]
            boolean r4 = r6.zzbdq
            r7 = 0
            r8 = 0
            r10 = 0
            r11 = 0
        L_0x030d:
            int r12 = r0.length
            if (r7 >= r12) goto L_0x038a
            com.google.android.gms.internal.ads.zzms r12 = r0.zzav(r7)
            r13 = r3[r7]
            r14 = r11
            r11 = r10
            r10 = r8
            r8 = 0
        L_0x031b:
            int r15 = r12.length
            if (r8 >= r15) goto L_0x037f
            r15 = r13[r8]
            boolean r15 = zze(r15, r4)
            if (r15 == 0) goto L_0x0375
            com.google.android.gms.internal.ads.zzgw r15 = r12.zzaw(r8)
            int r5 = r15.zzafu
            r19 = 1
            r5 = r5 & 1
            if (r5 == 0) goto L_0x0335
            r5 = 1
            goto L_0x0336
        L_0x0335:
            r5 = 0
        L_0x0336:
            int r9 = r15.zzafu
            r17 = 2
            r9 = r9 & 2
            r23 = r0
            r0 = 0
            if (r9 == 0) goto L_0x0343
            r9 = 1
            goto L_0x0344
        L_0x0343:
            r9 = 0
        L_0x0344:
            boolean r24 = zza(r15, r0)
            if (r24 == 0) goto L_0x0354
            if (r5 == 0) goto L_0x034e
            r9 = 6
            goto L_0x0364
        L_0x034e:
            if (r9 != 0) goto L_0x0352
            r9 = 5
            goto L_0x0364
        L_0x0352:
            r9 = 4
            goto L_0x0364
        L_0x0354:
            if (r5 == 0) goto L_0x0358
            r9 = 3
            goto L_0x0364
        L_0x0358:
            if (r9 == 0) goto L_0x0379
            r0 = 0
            boolean r5 = zza(r15, r0)
            if (r5 == 0) goto L_0x0363
            r9 = 2
            goto L_0x0364
        L_0x0363:
            r9 = 1
        L_0x0364:
            r0 = r13[r8]
            r5 = 0
            boolean r0 = zze(r0, r5)
            if (r0 == 0) goto L_0x036f
            int r9 = r9 + 1000
        L_0x036f:
            if (r9 <= r14) goto L_0x0379
            r11 = r8
            r14 = r9
            r10 = r12
            goto L_0x0379
        L_0x0375:
            r23 = r0
            r17 = 2
        L_0x0379:
            int r8 = r8 + 1
            r0 = r23
            r5 = 3
            goto L_0x031b
        L_0x037f:
            r23 = r0
            r17 = 2
            int r7 = r7 + 1
            r8 = r10
            r10 = r11
            r11 = r14
            r5 = 3
            goto L_0x030d
        L_0x038a:
            r17 = 2
            if (r8 != 0) goto L_0x0390
            r11 = 0
            goto L_0x0395
        L_0x0390:
            com.google.android.gms.internal.ads.zzna r11 = new com.google.android.gms.internal.ads.zzna
            r11.<init>(r8, r10)
        L_0x0395:
            r27[r1] = r11
            r0 = r27[r1]
            if (r0 == 0) goto L_0x039d
            r0 = 1
            goto L_0x039e
        L_0x039d:
            r0 = 0
        L_0x039e:
            r3 = r0
            r0 = r2
            r2 = 0
            r5 = -1
            r15 = 0
            r19 = 1
            goto L_0x044d
        L_0x03a7:
            r21 = r0
            r6 = r20
        L_0x03ab:
            r17 = 2
            goto L_0x0444
        L_0x03af:
            r21 = r0
            r6 = r20
            r17 = 2
            if (r2 != 0) goto L_0x0444
            r0 = r38[r1]
            r2 = r39[r1]
            boolean r4 = r6.zzbdq
            r5 = 0
            r7 = -1
            r8 = -1
            r9 = 0
        L_0x03c1:
            int r10 = r0.length
            if (r5 >= r10) goto L_0x0428
            com.google.android.gms.internal.ads.zzms r10 = r0.zzav(r5)
            r11 = r2[r5]
            r12 = r9
            r9 = r8
            r8 = r7
            r7 = 0
        L_0x03cf:
            int r13 = r10.length
            if (r7 >= r13) goto L_0x041a
            r13 = r11[r7]
            boolean r13 = zze(r13, r4)
            if (r13 == 0) goto L_0x040f
            com.google.android.gms.internal.ads.zzgw r13 = r10.zzaw(r7)
            r14 = r11[r7]
            int r15 = r13.zzafu
            r19 = 1
            r15 = r15 & 1
            r16 = r2
            r2 = 0
            if (r15 == 0) goto L_0x03ee
            r15 = 1
            goto L_0x03ef
        L_0x03ee:
            r15 = 0
        L_0x03ef:
            boolean r13 = zza(r13, r2)
            if (r13 == 0) goto L_0x03fb
            if (r15 == 0) goto L_0x03f9
            r13 = 4
            goto L_0x0400
        L_0x03f9:
            r13 = 3
            goto L_0x0400
        L_0x03fb:
            if (r15 == 0) goto L_0x03ff
            r13 = 2
            goto L_0x0400
        L_0x03ff:
            r13 = 1
        L_0x0400:
            r15 = 0
            boolean r14 = zze(r14, r15)
            if (r14 == 0) goto L_0x0409
            int r13 = r13 + 1000
        L_0x0409:
            if (r13 <= r12) goto L_0x0415
            r8 = r5
            r9 = r7
            r12 = r13
            goto L_0x0415
        L_0x040f:
            r16 = r2
            r2 = 0
            r15 = 0
            r19 = 1
        L_0x0415:
            int r7 = r7 + 1
            r2 = r16
            goto L_0x03cf
        L_0x041a:
            r16 = r2
            r2 = 0
            r15 = 0
            r19 = 1
            int r5 = r5 + 1
            r7 = r8
            r8 = r9
            r9 = r12
            r2 = r16
            goto L_0x03c1
        L_0x0428:
            r2 = 0
            r5 = -1
            r15 = 0
            r19 = 1
            if (r7 != r5) goto L_0x0431
            r11 = r2
            goto L_0x043a
        L_0x0431:
            com.google.android.gms.internal.ads.zzms r0 = r0.zzav(r7)
            com.google.android.gms.internal.ads.zzna r11 = new com.google.android.gms.internal.ads.zzna
            r11.<init>(r0, r8)
        L_0x043a:
            r27[r1] = r11
            r0 = r27[r1]
            if (r0 == 0) goto L_0x0442
            r0 = 1
            goto L_0x044d
        L_0x0442:
            r0 = 0
            goto L_0x044d
        L_0x0444:
            r18 = r2
            r2 = 0
            r5 = -1
            r15 = 0
        L_0x0449:
            r19 = 1
            r0 = r18
        L_0x044d:
            int r1 = r1 + 1
            r2 = r0
            r20 = r6
            r0 = r21
            goto L_0x026d
        L_0x0456:
            return r27
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzmy.zza(com.google.android.gms.internal.ads.zzhe[], com.google.android.gms.internal.ads.zzmr[], int[][][]):com.google.android.gms.internal.ads.zzne[]");
    }

    private static boolean zza(zzgw zzgw, String str) {
        return str != null && TextUtils.equals(str, zzoq.zzbl(zzgw.zzafv));
    }
}
