package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdad {
    public static void zzc(Throwable th, String str) {
        int zzd = zzcfb.zzd(th);
        StringBuilder sb = new StringBuilder(31);
        sb.append("Ad failed to load : ");
        sb.append(zzd);
        zzavs.zzey(sb.toString());
        zzavs.zza(str, th);
        if (zzcfb.zzd(th) != 3) {
            zzq.zzku().zzb(th, str);
        }
    }

    public static void zze(Context context, boolean z) {
        if (z) {
            zzavs.zzey("This request is sent from a test device.");
            return;
        }
        zzve.zzou();
        String zzbi = zzayk.zzbi(context);
        StringBuilder sb = new StringBuilder(String.valueOf(zzbi).length() + 71);
        sb.append("Use AdRequest.Builder.addTestDevice(\"");
        sb.append(zzbi);
        sb.append("\") to get test ads on this device.");
        zzavs.zzey(sb.toString());
    }
}
