package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzclw<AdT> implements zzdxg<zzclu<AdT>> {
    public static <AdT> zzclu<AdT> zza(zzdcr zzdcr, zzclp zzclp, zzbou zzbou, zzdda zzdda, zzbmi<AdT> zzbmi, Executor executor, ScheduledExecutorService scheduledExecutorService) {
        return new zzclu(zzdcr, zzclp, zzbou, zzdda, zzbmi, executor, scheduledExecutorService);
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
