package com.google.android.gms.internal.ads;

import com.google.ads.AdRequest;
import com.google.ads.mediation.MediationAdRequest;
import java.util.Date;
import java.util.HashSet;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzamr {
    public static int zza(AdRequest.ErrorCode errorCode) {
        int i = zzamu.zzdej[errorCode.ordinal()];
        if (i == 2) {
            return 1;
        }
        if (i != 3) {
            return i != 4 ? 0 : 3;
        }
        return 2;
    }

    public static MediationAdRequest zza(zzug zzug, boolean z) {
        AdRequest.Gender gender;
        HashSet hashSet = zzug.zzcca != null ? new HashSet(zzug.zzcca) : null;
        Date date = new Date(zzug.zzcby);
        int i = zzug.zzcbz;
        if (i == 1) {
            gender = AdRequest.Gender.MALE;
        } else if (i != 2) {
            gender = AdRequest.Gender.UNKNOWN;
        } else {
            gender = AdRequest.Gender.FEMALE;
        }
        return new MediationAdRequest(date, gender, hashSet, z, zzug.zzmi);
    }
}
