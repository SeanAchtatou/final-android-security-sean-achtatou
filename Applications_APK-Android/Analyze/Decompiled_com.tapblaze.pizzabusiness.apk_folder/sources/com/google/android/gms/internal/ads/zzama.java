package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.InitializationCompleteCallback;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzama implements InitializationCompleteCallback {
    private final /* synthetic */ zzagp zzddw;

    zzama(zzaly zzaly, zzagp zzagp) {
        this.zzddw = zzagp;
    }

    public final void onInitializationSucceeded() {
        try {
            this.zzddw.onInitializationSucceeded();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }

    public final void onInitializationFailed(String str) {
        try {
            this.zzddw.onInitializationFailed(str);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
