package com.google.android.gms.internal.p000firebaseperf;

import java.util.NoSuchElementException;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbo  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzbo<T> {
    private static final zzbo<?> zzhy = new zzbo<>();
    private final T value;

    private zzbo() {
        this.value = null;
    }

    public static <T> zzbo<T> zzcy() {
        return zzhy;
    }

    private zzbo(T t) {
        if (t != null) {
            this.value = t;
            return;
        }
        throw new NullPointerException("value for optional is empty.");
    }

    public static <T> zzbo<T> zzb(T t) {
        return new zzbo<>(t);
    }

    public static <T> zzbo<T> zzc(T t) {
        if (t == null) {
            return zzhy;
        }
        return zzb(t);
    }

    public final T get() {
        T t = this.value;
        if (t != null) {
            return t;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean isPresent() {
        return this.value != null;
    }
}
