package com.google.android.gms.measurement.internal;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class zzjx implements Runnable {
    private final zzjy zza;

    zzjx(zzjy zzjy) {
        this.zza = zzjy;
    }

    public final void run() {
        zzjy zzjy = this.zza;
        zzjy.zza.zzq().zza(new zzka(zzjy));
    }
}
