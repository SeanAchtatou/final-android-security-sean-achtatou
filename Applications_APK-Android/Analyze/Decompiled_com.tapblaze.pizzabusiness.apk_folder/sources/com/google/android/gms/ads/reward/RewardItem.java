package com.google.android.gms.ads.reward;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface RewardItem {
    int getAmount();

    String getType();
}
