package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdmj extends zzdrt<zzdmj, zza> implements zzdtg {
    private static volatile zzdtn<zzdmj> zzdz;
    /* access modifiers changed from: private */
    public static final zzdmj zzhbp;
    private zzdmo zzhbm;
    private zzdmf zzhbn;
    private int zzhbo;

    private zzdmj() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdmj, zza> implements zzdtg {
        private zza() {
            super(zzdmj.zzhbp);
        }

        /* synthetic */ zza(zzdmi zzdmi) {
            this();
        }
    }

    public final zzdmo zzauh() {
        zzdmo zzdmo = this.zzhbm;
        return zzdmo == null ? zzdmo.zzaux() : zzdmo;
    }

    public final zzdmf zzaui() {
        zzdmf zzdmf = this.zzhbn;
        return zzdmf == null ? zzdmf.zzaud() : zzdmf;
    }

    public final zzdmd zzauj() {
        zzdmd zzei = zzdmd.zzei(this.zzhbo);
        return zzei == null ? zzdmd.UNRECOGNIZED : zzei;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdmi.zzdk[i - 1]) {
            case 1:
                return new zzdmj();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhbp, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\t\u0002\t\u0003\f", new Object[]{"zzhbm", "zzhbn", "zzhbo"});
            case 4:
                return zzhbp;
            case 5:
                zzdtn<zzdmj> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdmj.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhbp);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdmj zzauk() {
        return zzhbp;
    }

    static {
        zzdmj zzdmj = new zzdmj();
        zzhbp = zzdmj;
        zzdrt.zza(zzdmj.class, zzdmj);
    }
}
