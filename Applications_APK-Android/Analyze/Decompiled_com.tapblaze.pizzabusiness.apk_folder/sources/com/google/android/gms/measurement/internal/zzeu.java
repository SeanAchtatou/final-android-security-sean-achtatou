package com.google.android.gms.measurement.internal;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzeu<V> {
    private static final Object zzf = new Object();
    private final String zza;
    private final zzes<V> zzb;
    private final V zzc;
    private final V zzd;
    private final Object zze;
    private volatile V zzg;
    private volatile V zzh;

    private zzeu(String str, V v, V v2, zzes<V> zzes) {
        this.zze = new Object();
        this.zzg = null;
        this.zzh = null;
        this.zza = str;
        this.zzc = v;
        this.zzd = v2;
        this.zzb = zzes;
    }

    public final String zza() {
        return this.zza;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:29|30|31|(1:33)|34|35|4e) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0025, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r4 = com.google.android.gms.measurement.internal.zzap.zzcz.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r0.zzh = r1;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x004c */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x004f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0068 A[SYNTHETIC, Splitter:B:52:0x0068] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final V zza(V r4) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.zze
            monitor-enter(r0)
            V r1 = r3.zzg     // Catch:{ all -> 0x007a }
            monitor-exit(r0)     // Catch:{ all -> 0x007a }
            if (r4 == 0) goto L_0x0009
            return r4
        L_0x0009:
            com.google.android.gms.measurement.internal.zzw r4 = com.google.android.gms.measurement.internal.zzer.zza
            if (r4 != 0) goto L_0x0010
            V r4 = r3.zzc
            return r4
        L_0x0010:
            com.google.android.gms.measurement.internal.zzw r4 = com.google.android.gms.measurement.internal.zzer.zza
            java.lang.Object r4 = com.google.android.gms.measurement.internal.zzeu.zzf
            monitor-enter(r4)
            boolean r0 = com.google.android.gms.measurement.internal.zzw.zza()     // Catch:{ all -> 0x0077 }
            if (r0 == 0) goto L_0x0026
            V r0 = r3.zzh     // Catch:{ all -> 0x0077 }
            if (r0 != 0) goto L_0x0022
            V r0 = r3.zzc     // Catch:{ all -> 0x0077 }
            goto L_0x0024
        L_0x0022:
            V r0 = r3.zzh     // Catch:{ all -> 0x0077 }
        L_0x0024:
            monitor-exit(r4)     // Catch:{ all -> 0x0077 }
            return r0
        L_0x0026:
            monitor-exit(r4)     // Catch:{ all -> 0x0077 }
            java.util.List r4 = com.google.android.gms.measurement.internal.zzap.zzcz     // Catch:{ SecurityException -> 0x005e }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ SecurityException -> 0x005e }
        L_0x002f:
            boolean r0 = r4.hasNext()     // Catch:{ SecurityException -> 0x005e }
            if (r0 == 0) goto L_0x005f
            java.lang.Object r0 = r4.next()     // Catch:{ SecurityException -> 0x005e }
            com.google.android.gms.measurement.internal.zzeu r0 = (com.google.android.gms.measurement.internal.zzeu) r0     // Catch:{ SecurityException -> 0x005e }
            boolean r1 = com.google.android.gms.measurement.internal.zzw.zza()     // Catch:{ SecurityException -> 0x005e }
            if (r1 != 0) goto L_0x0056
            r1 = 0
            com.google.android.gms.measurement.internal.zzes<V> r2 = r0.zzb     // Catch:{ IllegalStateException -> 0x004c }
            if (r2 == 0) goto L_0x004c
            com.google.android.gms.measurement.internal.zzes<V> r2 = r0.zzb     // Catch:{ IllegalStateException -> 0x004c }
            java.lang.Object r1 = r2.zza()     // Catch:{ IllegalStateException -> 0x004c }
        L_0x004c:
            java.lang.Object r2 = com.google.android.gms.measurement.internal.zzeu.zzf     // Catch:{ SecurityException -> 0x005e }
            monitor-enter(r2)     // Catch:{ SecurityException -> 0x005e }
            r0.zzh = r1     // Catch:{ all -> 0x0053 }
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            goto L_0x002f
        L_0x0053:
            r4 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            throw r4     // Catch:{ SecurityException -> 0x005e }
        L_0x0056:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException     // Catch:{ SecurityException -> 0x005e }
            java.lang.String r0 = "Refreshing flag cache must be done on a worker thread."
            r4.<init>(r0)     // Catch:{ SecurityException -> 0x005e }
            throw r4     // Catch:{ SecurityException -> 0x005e }
        L_0x005e:
        L_0x005f:
            com.google.android.gms.measurement.internal.zzes<V> r4 = r3.zzb
            if (r4 != 0) goto L_0x0068
            com.google.android.gms.measurement.internal.zzw r4 = com.google.android.gms.measurement.internal.zzer.zza
            V r4 = r3.zzc
            return r4
        L_0x0068:
            java.lang.Object r4 = r4.zza()     // Catch:{ SecurityException -> 0x0072, IllegalStateException -> 0x006d }
            return r4
        L_0x006d:
            com.google.android.gms.measurement.internal.zzw r4 = com.google.android.gms.measurement.internal.zzer.zza
            V r4 = r3.zzc
            return r4
        L_0x0072:
            com.google.android.gms.measurement.internal.zzw r4 = com.google.android.gms.measurement.internal.zzer.zza
            V r4 = r3.zzc
            return r4
        L_0x0077:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0077 }
            throw r0
        L_0x007a:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x007a }
            goto L_0x007e
        L_0x007d:
            throw r4
        L_0x007e:
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzeu.zza(java.lang.Object):java.lang.Object");
    }
}
