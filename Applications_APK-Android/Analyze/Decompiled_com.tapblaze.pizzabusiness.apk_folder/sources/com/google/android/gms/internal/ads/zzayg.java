package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.ArrayList;
import java.util.Map;
import java.util.WeakHashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzayg {
    private final BroadcastReceiver zzduz = new zzayj(this);
    private final Map<BroadcastReceiver, IntentFilter> zzdva = new WeakHashMap();
    private boolean zzdvb;
    private boolean zzxx = false;
    private Context zzyv;

    public final synchronized void initialize(Context context) {
        if (!this.zzxx) {
            this.zzyv = context.getApplicationContext();
            if (this.zzyv == null) {
                this.zzyv = context;
            }
            zzzn.initialize(this.zzyv);
            this.zzdvb = ((Boolean) zzve.zzoy().zzd(zzzn.zzcmg)).booleanValue();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            this.zzyv.registerReceiver(this.zzduz, intentFilter);
            this.zzxx = true;
        }
    }

    public final synchronized void zza(Context context, BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        if (this.zzdvb) {
            this.zzdva.put(broadcastReceiver, intentFilter);
        } else {
            context.registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    public final synchronized void zza(Context context, BroadcastReceiver broadcastReceiver) {
        if (this.zzdvb) {
            this.zzdva.remove(broadcastReceiver);
        } else {
            context.unregisterReceiver(broadcastReceiver);
        }
    }

    /* access modifiers changed from: private */
    public final synchronized void zzc(Context context, Intent intent) {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : this.zzdva.entrySet()) {
            if (((IntentFilter) next.getValue()).hasAction(intent.getAction())) {
                arrayList.add((BroadcastReceiver) next.getKey());
            }
        }
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            ((BroadcastReceiver) obj).onReceive(context, intent);
        }
    }
}
