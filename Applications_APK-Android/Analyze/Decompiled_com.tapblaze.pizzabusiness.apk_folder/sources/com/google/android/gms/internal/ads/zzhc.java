package com.google.android.gms.internal.ads;

import com.ironsource.mediationsdk.logger.IronSourceError;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzhc {
    public static final zzhc zzagb = new zzhc(1.0f, 1.0f);
    public final float zzagc;
    public final float zzagd;
    private final int zzage;

    public zzhc(float f, float f2) {
        this.zzagc = f;
        this.zzagd = f2;
        this.zzage = Math.round(f * 1000.0f);
    }

    public final long zzdu(long j) {
        return j * ((long) this.zzage);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            zzhc zzhc = (zzhc) obj;
            return this.zzagc == zzhc.zzagc && this.zzagd == zzhc.zzagd;
        }
    }

    public final int hashCode() {
        return ((Float.floatToRawIntBits(this.zzagc) + IronSourceError.ERROR_NON_EXISTENT_INSTANCE) * 31) + Float.floatToRawIntBits(this.zzagd);
    }
}
