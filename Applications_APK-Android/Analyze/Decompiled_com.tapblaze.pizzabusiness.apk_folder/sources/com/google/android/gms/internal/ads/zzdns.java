package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdns extends zzdrt<zzdns, zza> implements zzdtg {
    private static volatile zzdtn<zzdns> zzdz;
    /* access modifiers changed from: private */
    public static final zzdns zzhef;
    private int zzhaa;
    private zzdnt zzhee;

    private zzdns() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdns, zza> implements zzdtg {
        private zza() {
            super(zzdns.zzhef);
        }

        public final zza zzev(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdns) this.zzhmp).setVersion(0);
            return this;
        }

        public final zza zzb(zzdnt zzdnt) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdns) this.zzhmp).zza(zzdnt);
            return this;
        }

        /* synthetic */ zza(zzdnr zzdnr) {
            this();
        }
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public final zzdnt zzawn() {
        zzdnt zzdnt = this.zzhee;
        return zzdnt == null ? zzdnt.zzaws() : zzdnt;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdnt zzdnt) {
        zzdnt.getClass();
        this.zzhee = zzdnt;
    }

    public static zzdns zzaz(zzdqk zzdqk) throws zzdse {
        return (zzdns) zzdrt.zza(zzhef, zzdqk);
    }

    public static zza zzawo() {
        return (zza) zzhef.zzazt();
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnr.zzdk[i - 1]) {
            case 1:
                return new zzdns();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhef, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u000b\u0002\t", new Object[]{"zzhaa", "zzhee"});
            case 4:
                return zzhef;
            case 5:
                zzdtn<zzdns> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdns.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhef);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzdns zzdns = new zzdns();
        zzhef = zzdns;
        zzdrt.zza(zzdns.class, zzdns);
    }
}
