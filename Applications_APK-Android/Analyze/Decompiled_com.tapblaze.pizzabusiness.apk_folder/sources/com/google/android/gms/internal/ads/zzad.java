package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzad {
    int zza();

    void zza(zzae zzae) throws zzae;

    int zzb();
}
