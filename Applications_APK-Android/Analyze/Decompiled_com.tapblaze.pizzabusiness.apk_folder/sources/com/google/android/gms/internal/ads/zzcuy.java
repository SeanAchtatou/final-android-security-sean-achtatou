package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcuy implements Callable {
    private final zzcuz zzghr;

    zzcuy(zzcuz zzcuz) {
        this.zzghr = zzcuz;
    }

    public final Object call() {
        return this.zzghr.zzans();
    }
}
