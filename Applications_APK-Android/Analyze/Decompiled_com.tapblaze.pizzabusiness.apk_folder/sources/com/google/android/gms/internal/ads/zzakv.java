package com.google.android.gms.internal.ads;

import android.os.IBinder;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzakv implements zzayw {
    static final zzayw zzbtz = new zzakv();

    private zzakv() {
    }

    public final Object apply(Object obj) {
        return zzbfv.zzao((IBinder) obj);
    }
}
