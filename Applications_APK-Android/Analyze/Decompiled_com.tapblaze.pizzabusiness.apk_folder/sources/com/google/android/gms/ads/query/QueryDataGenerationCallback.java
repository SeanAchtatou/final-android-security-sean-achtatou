package com.google.android.gms.ads.query;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface QueryDataGenerationCallback {
    void onFailure(String str);

    void onSuccess(QueryData queryData);
}
