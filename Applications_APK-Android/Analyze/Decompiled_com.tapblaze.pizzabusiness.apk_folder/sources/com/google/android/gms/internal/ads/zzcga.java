package com.google.android.gms.internal.ads;

import java.io.InputStream;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcga implements zzdgf {
    private final zzcfx zzfvi;

    zzcga(zzcfx zzcfx) {
        this.zzfvi = zzcfx;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfvi.zzd((InputStream) obj);
    }
}
