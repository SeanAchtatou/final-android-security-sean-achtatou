package com.google.android.gms.internal.ads;

import android.location.Location;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzuf {
    private Bundle extras = new Bundle();
    private int zzabo = -1;
    private int zzabp = -1;
    private String zzabq = null;
    private boolean zzbkh = false;
    private long zzcby = -1;
    private int zzcbz = -1;
    private List<String> zzcca = new ArrayList();
    private boolean zzccb = false;
    private String zzccc = null;
    private zzys zzccd = null;
    private String zzcce = null;
    private Bundle zzccf = new Bundle();
    private Bundle zzccg = new Bundle();
    private List<String> zzcch = new ArrayList();
    private String zzcci = null;
    private String zzccj = null;
    private boolean zzcck = false;
    private List<String> zzccl = new ArrayList();
    private Location zzmi = null;

    public final zzug zzok() {
        return new zzug(8, this.zzcby, this.extras, this.zzcbz, this.zzcca, false, this.zzabo, false, null, null, null, null, this.zzccf, this.zzccg, this.zzcch, null, null, false, null, this.zzabp, null, this.zzccl);
    }
}
