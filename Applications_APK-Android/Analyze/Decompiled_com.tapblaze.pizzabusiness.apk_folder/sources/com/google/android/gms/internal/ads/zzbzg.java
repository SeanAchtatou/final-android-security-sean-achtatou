package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbzg implements zzded {
    private final String zzcyz;

    zzbzg(String str) {
        this.zzcyz = str;
    }

    public final Object apply(Object obj) {
        return new zzbzf(this.zzcyz, (zzabu) obj);
    }
}
