package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzboz extends zzbrl<zzbpa> implements zzbpa {
    public zzboz(Set<zzbsu<zzbpa>> set) {
        super(set);
    }

    public final void zzco(int i) {
        zza(new zzboy(i));
    }
}
