package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcng implements zzcio<zzbkk> {
    private final zzdcr zzfgm;
    private final zzblg zzfyj;
    private final zzdhd zzgbh;
    private final Context zzgbm;
    private final zzaak zzgbn;

    public zzcng(Context context, zzblg zzblg, zzdcr zzdcr, zzdhd zzdhd, zzaak zzaak) {
        this.zzgbm = context;
        this.zzfyj = zzblg;
        this.zzfgm = zzdcr;
        this.zzgbh = zzdhd;
        this.zzgbn = zzaak;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        return (this.zzgbn == null || zzczl.zzglo == null || zzczl.zzglo.zzdht == null) ? false : true;
    }

    public final zzdhe<zzbkk> zzb(zzczt zzczt, zzczl zzczl) {
        zzbkj zza = this.zzfyj.zza(new zzbmt(zzczt, zzczl, null), new zzcnh(this, new View(this.zzgbm), null, zzcnf.zzgbl, zzczl.zzglq.get(0)));
        return this.zzfgm.zzu(zzdco.CUSTOM_RENDER_SYN).zza(new zzcni(this, new zzaad(zza.zzaej(), zzczl.zzglo.zzdhr, zzczl.zzglo.zzdht)), this.zzgbh).zzw(zzdco.CUSTOM_RENDER_ACK).zzc(zzdgs.zzaj(zza.zzaeh())).zzaqg();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzaad zzaad) throws Exception {
        this.zzgbn.zza(zzaad);
    }
}
