package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcst implements zzcty<Bundle> {
    private final String zzggc;

    /* access modifiers changed from: private */
    public static boolean zzd(Set<String> set) {
        return set.contains(Constants.CONVERT_REWARDED) || set.contains("interstitial") || set.contains(AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_NATIVE) || set.contains("banner");
    }

    public zzcst(String str) {
        this.zzggc = str;
    }

    public final /* synthetic */ void zzr(Object obj) {
        zzdaa.zza((Bundle) obj, "omid_v", this.zzggc);
    }
}
