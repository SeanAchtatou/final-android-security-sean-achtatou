package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzzy {
    public static final zzzy zzcrr = new zzzx();
    public static final zzzy zzcrs = new zzaaa();
    public static final zzzy zzcrt = new zzzz();

    public abstract String zzg(String str, String str2);
}
