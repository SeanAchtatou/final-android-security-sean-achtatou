package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbjp implements Runnable {
    private final JSONObject zzfcs;
    private final zzbjq zzfdg;

    zzbjp(zzbjq zzbjq, JSONObject jSONObject) {
        this.zzfdg = zzbjq;
        this.zzfcs = jSONObject;
    }

    public final void run() {
        this.zzfdg.zzh(this.zzfcs);
    }
}
