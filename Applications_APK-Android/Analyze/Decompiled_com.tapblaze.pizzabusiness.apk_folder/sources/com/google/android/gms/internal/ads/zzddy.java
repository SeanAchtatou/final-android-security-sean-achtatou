package com.google.android.gms.internal.ads;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzddy<T> extends zzdej<T> {
    static final zzddy<Object> zzgtx = new zzddy<>();

    private zzddy() {
    }

    public final boolean equals(@NullableDecl Object obj) {
        return obj == this;
    }

    public final int hashCode() {
        return 2040732332;
    }

    public final String toString() {
        return "Optional.absent()";
    }

    @NullableDecl
    public final T zzaqt() {
        return null;
    }
}
