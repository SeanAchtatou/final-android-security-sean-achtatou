package com.google.android.gms.internal.ads;

import java.security.SecureRandom;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdpm extends ThreadLocal<SecureRandom> {
    zzdpm() {
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object initialValue() {
        return zzdpn.zzaxf();
    }
}
