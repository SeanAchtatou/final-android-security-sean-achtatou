package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

@Deprecated
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdkj {
    @Deprecated
    public static zzdie zza(zzdij zzdij, zzdid<zzdie> zzdid) throws GeneralSecurityException {
        zzdit.zza(new zzdki());
        return (zzdie) zzdit.zza(zzdit.zza(zzdij, (zzdid) null, zzdie.class));
    }
}
