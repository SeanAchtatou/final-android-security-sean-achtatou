package com.google.android.gms.internal.p000firebaseperf;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Unsafe;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhz  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhz {
    private static final Logger logger = Logger.getLogger(zzhz.class.getName());
    private static final Class<?> zzms = zzdz.zzgj();
    private static final boolean zznf = zzjj();
    private static final Unsafe zzsu = zzji();
    private static final boolean zzuw = zzj(Long.TYPE);
    private static final boolean zzux = zzj(Integer.TYPE);
    private static final zzd zzuy;
    private static final boolean zzuz = zzjk();
    private static final long zzva = ((long) zzh(byte[].class));
    private static final long zzvb;
    private static final long zzvc;
    private static final long zzvd;
    private static final long zzve;
    private static final long zzvf;
    private static final long zzvg;
    private static final long zzvh;
    private static final long zzvi;
    private static final long zzvj;
    private static final long zzvk;
    private static final long zzvl = ((long) zzh(Object[].class));
    private static final long zzvm = ((long) zzi(Object[].class));
    private static final long zzvn;
    private static final int zzvo = ((int) (zzva & 7));
    static final boolean zzvp = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);

    private zzhz() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzhz$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static final class zza extends zzd {
        zza(Unsafe unsafe) {
            super(unsafe);
        }

        public final byte zzx(Object obj, long j) {
            if (zzhz.zzvp) {
                return zzhz.zzp(obj, j);
            }
            return zzhz.zzq(obj, j);
        }

        public final void zze(Object obj, long j, byte b) {
            if (zzhz.zzvp) {
                zzhz.zza(obj, j, b);
            } else {
                zzhz.zzb(obj, j, b);
            }
        }

        public final boolean zzl(Object obj, long j) {
            if (zzhz.zzvp) {
                return zzhz.zzr(obj, j);
            }
            return zzhz.zzs(obj, j);
        }

        public final void zza(Object obj, long j, boolean z) {
            if (zzhz.zzvp) {
                zzhz.zzb(obj, j, z);
            } else {
                zzhz.zzc(obj, j, z);
            }
        }

        public final float zzm(Object obj, long j) {
            return Float.intBitsToFloat(zzj(obj, j));
        }

        public final void zza(Object obj, long j, float f) {
            zza(obj, j, Float.floatToIntBits(f));
        }

        public final double zzn(Object obj, long j) {
            return Double.longBitsToDouble(zzk(obj, j));
        }

        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzhz$zzb */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static final class zzb extends zzd {
        zzb(Unsafe unsafe) {
            super(unsafe);
        }

        public final byte zzx(Object obj, long j) {
            return this.zzvs.getByte(obj, j);
        }

        public final void zze(Object obj, long j, byte b) {
            this.zzvs.putByte(obj, j, b);
        }

        public final boolean zzl(Object obj, long j) {
            return this.zzvs.getBoolean(obj, j);
        }

        public final void zza(Object obj, long j, boolean z) {
            this.zzvs.putBoolean(obj, j, z);
        }

        public final float zzm(Object obj, long j) {
            return this.zzvs.getFloat(obj, j);
        }

        public final void zza(Object obj, long j, float f) {
            this.zzvs.putFloat(obj, j, f);
        }

        public final double zzn(Object obj, long j) {
            return this.zzvs.getDouble(obj, j);
        }

        public final void zza(Object obj, long j, double d) {
            this.zzvs.putDouble(obj, j, d);
        }
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzhz$zzc */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static final class zzc extends zzd {
        zzc(Unsafe unsafe) {
            super(unsafe);
        }

        public final byte zzx(Object obj, long j) {
            if (zzhz.zzvp) {
                return zzhz.zzp(obj, j);
            }
            return zzhz.zzq(obj, j);
        }

        public final void zze(Object obj, long j, byte b) {
            if (zzhz.zzvp) {
                zzhz.zza(obj, j, b);
            } else {
                zzhz.zzb(obj, j, b);
            }
        }

        public final boolean zzl(Object obj, long j) {
            if (zzhz.zzvp) {
                return zzhz.zzr(obj, j);
            }
            return zzhz.zzs(obj, j);
        }

        public final void zza(Object obj, long j, boolean z) {
            if (zzhz.zzvp) {
                zzhz.zzb(obj, j, z);
            } else {
                zzhz.zzc(obj, j, z);
            }
        }

        public final float zzm(Object obj, long j) {
            return Float.intBitsToFloat(zzj(obj, j));
        }

        public final void zza(Object obj, long j, float f) {
            zza(obj, j, Float.floatToIntBits(f));
        }

        public final double zzn(Object obj, long j) {
            return Double.longBitsToDouble(zzk(obj, j));
        }

        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }
    }

    static boolean zzjg() {
        return zznf;
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzhz$zzd */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static abstract class zzd {
        Unsafe zzvs;

        zzd(Unsafe unsafe) {
            this.zzvs = unsafe;
        }

        public abstract void zza(Object obj, long j, double d);

        public abstract void zza(Object obj, long j, float f);

        public abstract void zza(Object obj, long j, boolean z);

        public abstract void zze(Object obj, long j, byte b);

        public abstract boolean zzl(Object obj, long j);

        public abstract float zzm(Object obj, long j);

        public abstract double zzn(Object obj, long j);

        public abstract byte zzx(Object obj, long j);

        public final int zzj(Object obj, long j) {
            return this.zzvs.getInt(obj, j);
        }

        public final void zza(Object obj, long j, int i) {
            this.zzvs.putInt(obj, j, i);
        }

        public final long zzk(Object obj, long j) {
            return this.zzvs.getLong(obj, j);
        }

        public final void zza(Object obj, long j, long j2) {
            this.zzvs.putLong(obj, j, j2);
        }
    }

    static boolean zzjh() {
        return zzuz;
    }

    static <T> T zzg(Class<T> cls) {
        try {
            return zzsu.allocateInstance(cls);
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        }
    }

    private static int zzh(Class<?> cls) {
        if (zznf) {
            return zzuy.zzvs.arrayBaseOffset(cls);
        }
        return -1;
    }

    private static int zzi(Class<?> cls) {
        if (zznf) {
            return zzuy.zzvs.arrayIndexScale(cls);
        }
        return -1;
    }

    static int zzj(Object obj, long j) {
        return zzuy.zzj(obj, j);
    }

    static void zza(Object obj, long j, int i) {
        zzuy.zza(obj, j, i);
    }

    static long zzk(Object obj, long j) {
        return zzuy.zzk(obj, j);
    }

    static void zza(Object obj, long j, long j2) {
        zzuy.zza(obj, j, j2);
    }

    static boolean zzl(Object obj, long j) {
        return zzuy.zzl(obj, j);
    }

    static void zza(Object obj, long j, boolean z) {
        zzuy.zza(obj, j, z);
    }

    static float zzm(Object obj, long j) {
        return zzuy.zzm(obj, j);
    }

    static void zza(Object obj, long j, float f) {
        zzuy.zza(obj, j, f);
    }

    static double zzn(Object obj, long j) {
        return zzuy.zzn(obj, j);
    }

    static void zza(Object obj, long j, double d) {
        zzuy.zza(obj, j, d);
    }

    static Object zzo(Object obj, long j) {
        return zzuy.zzvs.getObject(obj, j);
    }

    static void zza(Object obj, long j, Object obj2) {
        zzuy.zzvs.putObject(obj, j, obj2);
    }

    static byte zza(byte[] bArr, long j) {
        return zzuy.zzx(bArr, zzva + j);
    }

    static void zza(byte[] bArr, long j, byte b) {
        zzuy.zze(bArr, zzva + j, b);
    }

    static Unsafe zzji() {
        try {
            return (Unsafe) AccessController.doPrivileged(new zzib());
        } catch (Throwable unused) {
            return null;
        }
    }

    private static boolean zzjj() {
        Unsafe unsafe = zzsu;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("arrayBaseOffset", Class.class);
            cls.getMethod("arrayIndexScale", Class.class);
            cls.getMethod("getInt", Object.class, Long.TYPE);
            cls.getMethod("putInt", Object.class, Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            cls.getMethod("putLong", Object.class, Long.TYPE, Long.TYPE);
            cls.getMethod("getObject", Object.class, Long.TYPE);
            cls.getMethod("putObject", Object.class, Long.TYPE, Object.class);
            if (zzdz.zzgi()) {
                return true;
            }
            cls.getMethod("getByte", Object.class, Long.TYPE);
            cls.getMethod("putByte", Object.class, Long.TYPE, Byte.TYPE);
            cls.getMethod("getBoolean", Object.class, Long.TYPE);
            cls.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE);
            cls.getMethod("getFloat", Object.class, Long.TYPE);
            cls.getMethod("putFloat", Object.class, Long.TYPE, Float.TYPE);
            cls.getMethod("getDouble", Object.class, Long.TYPE);
            cls.getMethod("putDouble", Object.class, Long.TYPE, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    private static boolean zzjk() {
        Unsafe unsafe = zzsu;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            if (zzjl() == null) {
                return false;
            }
            if (zzdz.zzgi()) {
                return true;
            }
            cls.getMethod("getByte", Long.TYPE);
            cls.getMethod("putByte", Long.TYPE, Byte.TYPE);
            cls.getMethod("getInt", Long.TYPE);
            cls.getMethod("putInt", Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Long.TYPE);
            cls.getMethod("putLong", Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Long.TYPE, Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    private static boolean zzj(Class<?> cls) {
        Class<byte[]> cls2 = byte[].class;
        if (!zzdz.zzgi()) {
            return false;
        }
        try {
            Class<?> cls3 = zzms;
            cls3.getMethod("peekLong", cls, Boolean.TYPE);
            cls3.getMethod("pokeLong", cls, Long.TYPE, Boolean.TYPE);
            cls3.getMethod("pokeInt", cls, Integer.TYPE, Boolean.TYPE);
            cls3.getMethod("peekInt", cls, Boolean.TYPE);
            cls3.getMethod("pokeByte", cls, Byte.TYPE);
            cls3.getMethod("peekByte", cls);
            cls3.getMethod("pokeByteArray", cls, cls2, Integer.TYPE, Integer.TYPE);
            cls3.getMethod("peekByteArray", cls, cls2, Integer.TYPE, Integer.TYPE);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    private static Field zzjl() {
        Field zzb2;
        if (zzdz.zzgi() && (zzb2 = zzb(Buffer.class, "effectiveDirectAddress")) != null) {
            return zzb2;
        }
        Field zzb3 = zzb(Buffer.class, "address");
        if (zzb3 == null || zzb3.getType() != Long.TYPE) {
            return null;
        }
        return zzb3;
    }

    private static Field zzb(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static byte zzp(Object obj, long j) {
        return (byte) (zzj(obj, -4 & j) >>> ((int) (((j ^ -1) & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static byte zzq(Object obj, long j) {
        return (byte) (zzj(obj, -4 & j) >>> ((int) ((j & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static void zza(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int i = ((((int) j) ^ -1) & 3) << 3;
        zza(obj, j2, ((255 & b) << i) | (zzj(obj, j2) & ((255 << i) ^ -1)));
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int i = (((int) j) & 3) << 3;
        zza(obj, j2, ((255 & b) << i) | (zzj(obj, j2) & ((255 << i) ^ -1)));
    }

    /* access modifiers changed from: private */
    public static boolean zzr(Object obj, long j) {
        return zzp(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    public static boolean zzs(Object obj, long j) {
        return zzq(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, boolean z) {
        zza(obj, j, z ? (byte) 1 : 0);
    }

    /* access modifiers changed from: private */
    public static void zzc(Object obj, long j, boolean z) {
        zzb(obj, j, z ? (byte) 1 : 0);
    }

    static {
        zzd zzd2;
        Class<double[]> cls = double[].class;
        Class<float[]> cls2 = float[].class;
        Class<long[]> cls3 = long[].class;
        Class<int[]> cls4 = int[].class;
        Class<boolean[]> cls5 = boolean[].class;
        zzd zzd3 = null;
        if (zzsu != null) {
            if (!zzdz.zzgi()) {
                zzd3 = new zzb(zzsu);
            } else if (zzuw) {
                zzd3 = new zzc(zzsu);
            } else if (zzux) {
                zzd3 = new zza(zzsu);
            }
        }
        zzuy = zzd3;
        zzvb = (long) zzh(cls5);
        zzvc = (long) zzi(cls5);
        zzvd = (long) zzh(cls4);
        zzve = (long) zzi(cls4);
        zzvf = (long) zzh(cls3);
        zzvg = (long) zzi(cls3);
        zzvh = (long) zzh(cls2);
        zzvi = (long) zzi(cls2);
        zzvj = (long) zzh(cls);
        zzvk = (long) zzi(cls);
        Field zzjl = zzjl();
        zzvn = (zzjl == null || (zzd2 = zzuy) == null) ? -1 : zzd2.zzvs.objectFieldOffset(zzjl);
    }
}
