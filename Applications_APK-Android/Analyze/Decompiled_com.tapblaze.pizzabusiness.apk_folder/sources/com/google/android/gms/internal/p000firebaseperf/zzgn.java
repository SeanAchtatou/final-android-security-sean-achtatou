package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgn  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public interface zzgn {
    boolean isInitialized();

    zzgl zzhj();
}
