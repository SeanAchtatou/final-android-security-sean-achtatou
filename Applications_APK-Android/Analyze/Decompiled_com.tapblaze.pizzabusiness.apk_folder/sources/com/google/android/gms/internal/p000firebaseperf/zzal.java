package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzal  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzal extends zzaz<Long> {
    private static zzal zzap;

    private zzal() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.NetworkEventCountForeground";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_rl_network_event_count_fg";
    }

    public static synchronized zzal zzan() {
        zzal zzal;
        synchronized (zzal.class) {
            if (zzap == null) {
                zzap = new zzal();
            }
            zzal = zzap;
        }
        return zzal;
    }
}
