package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzc;
import com.google.android.gms.common.util.CollectionUtils;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzafs implements zzafn<zzbdi> {
    private static final Map<String, Integer> zzcxr = CollectionUtils.mapOfKeyValueArrays(new String[]{"resize", "playVideo", "storePicture", "createCalendarEvent", "setOrientationProperties", "closeResizedAd", "unload"}, new Integer[]{1, 2, 3, 4, 5, 6, 7});
    private final zzc zzcxo;
    private final zzaoe zzcxp;
    private final zzaon zzcxq;

    public zzafs(zzc zzc, zzaoe zzaoe, zzaon zzaon) {
        this.zzcxo = zzc;
        this.zzcxp = zzaoe;
        this.zzcxq = zzaon;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzc zzc;
        zzbdi zzbdi = (zzbdi) obj;
        int intValue = zzcxr.get((String) map.get("a")).intValue();
        if (intValue != 5 && intValue != 7 && (zzc = this.zzcxo) != null && !zzc.zzjq()) {
            this.zzcxo.zzbq(null);
        } else if (intValue == 1) {
            this.zzcxp.zzg(map);
        } else if (intValue == 3) {
            new zzaof(zzbdi, map).execute();
        } else if (intValue == 4) {
            new zzanz(zzbdi, map).execute();
        } else if (intValue == 5) {
            new zzaog(zzbdi, map).execute();
        } else if (intValue == 6) {
            this.zzcxp.zzac(true);
        } else if (intValue != 7) {
            zzavs.zzey("Unknown MRAID command called.");
        } else {
            this.zzcxq.zztj();
        }
    }
}
