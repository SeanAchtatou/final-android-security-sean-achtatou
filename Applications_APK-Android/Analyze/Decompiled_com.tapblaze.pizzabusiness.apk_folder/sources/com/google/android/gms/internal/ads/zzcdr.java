package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcdr implements zzdxg<zzcds> {
    private final zzdxp<zzcdv> zzekn;

    private zzcdr(zzdxp<zzcdv> zzdxp) {
        this.zzekn = zzdxp;
    }

    public static zzcdr zzz(zzdxp<zzcdv> zzdxp) {
        return new zzcdr(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcds(this.zzekn.get());
    }
}
