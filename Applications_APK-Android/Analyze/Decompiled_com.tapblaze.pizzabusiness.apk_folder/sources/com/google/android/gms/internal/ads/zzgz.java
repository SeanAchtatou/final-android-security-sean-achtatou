package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzgz implements Parcelable.Creator<zzgw> {
    zzgz() {
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgw[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zzgw(parcel);
    }
}
