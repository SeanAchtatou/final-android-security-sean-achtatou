package com.google.android.gms.games.appcontent;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.games.internal.zzd;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class AppContentAnnotationEntity extends zzc implements zzb {
    public static final Parcelable.Creator<AppContentAnnotationEntity> CREATOR = new zzd();
    private final String description;
    private final String zzca;
    private final String zzft;
    private final Uri zzfw;
    private final String zzfx;
    private final String zzfy;
    private final int zzfz;
    private final int zzga;
    private final Bundle zzgb;

    AppContentAnnotationEntity(String str, Uri uri, String str2, String str3, String str4, String str5, int i, int i2, Bundle bundle) {
        this.description = str;
        this.zzft = str3;
        this.zzfy = str5;
        this.zzfz = i;
        this.zzfw = uri;
        this.zzga = i2;
        this.zzfx = str4;
        this.zzgb = bundle;
        this.zzca = str2;
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getId() {
        return this.zzft;
    }

    public final String zzaj() {
        return this.zzfy;
    }

    public final int zzak() {
        return this.zzfz;
    }

    public final Uri zzal() {
        return this.zzfw;
    }

    public final int zzan() {
        return this.zzga;
    }

    public final String zzao() {
        return this.zzfx;
    }

    public final Bundle zzam() {
        return this.zzgb;
    }

    public final String getTitle() {
        return this.zzca;
    }

    public final int hashCode() {
        return Objects.hashCode(getDescription(), getId(), zzaj(), Integer.valueOf(zzak()), zzal(), Integer.valueOf(zzan()), zzao(), Integer.valueOf(zzd.zza(zzam())), getTitle());
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzb)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        zzb zzb = (zzb) obj;
        if (!Objects.equal(zzb.getDescription(), getDescription()) || !Objects.equal(zzb.getId(), getId()) || !Objects.equal(zzb.zzaj(), zzaj()) || !Objects.equal(Integer.valueOf(zzb.zzak()), Integer.valueOf(zzak())) || !Objects.equal(zzb.zzal(), zzal()) || !Objects.equal(Integer.valueOf(zzb.zzan()), Integer.valueOf(zzan())) || !Objects.equal(zzb.zzao(), zzao()) || !zzd.zza(zzb.zzam(), zzam()) || !Objects.equal(zzb.getTitle(), getTitle())) {
            return false;
        }
        return true;
    }

    public final String toString() {
        return Objects.toStringHelper(this).add("Description", getDescription()).add("Id", getId()).add("ImageDefaultId", zzaj()).add("ImageHeight", Integer.valueOf(zzak())).add("ImageUri", zzal()).add("ImageWidth", Integer.valueOf(zzan())).add("LayoutSlot", zzao()).add("Modifiers", zzam()).add("Title", getTitle()).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.description, false);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzfw, i, false);
        SafeParcelWriter.writeString(parcel, 3, this.zzca, false);
        SafeParcelWriter.writeString(parcel, 5, this.zzft, false);
        SafeParcelWriter.writeString(parcel, 6, this.zzfx, false);
        SafeParcelWriter.writeString(parcel, 7, this.zzfy, false);
        SafeParcelWriter.writeInt(parcel, 8, this.zzfz);
        SafeParcelWriter.writeInt(parcel, 9, this.zzga);
        SafeParcelWriter.writeBundle(parcel, 10, this.zzgb, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
