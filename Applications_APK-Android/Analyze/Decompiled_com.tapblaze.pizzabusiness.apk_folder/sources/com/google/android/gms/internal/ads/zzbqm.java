package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbqm implements zzdxg<zzbqj> {
    private final zzdxp<Set<zzbsu<zzo>>> zzfeo;

    private zzbqm(zzdxp<Set<zzbsu<zzo>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbqm zzn(zzdxp<Set<zzbsu<zzo>>> zzdxp) {
        return new zzbqm(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbqj(this.zzfeo.get());
    }
}
