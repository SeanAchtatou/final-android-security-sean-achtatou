package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzail implements Runnable {
    private final /* synthetic */ String zzczd;
    private final /* synthetic */ zzaih zzcze;

    zzail(zzaih zzaih, String str) {
        this.zzcze = zzaih;
        this.zzczd = str;
    }

    public final void run() {
        this.zzcze.zzcza.loadData(this.zzczd, "text/html", "UTF-8");
    }
}
