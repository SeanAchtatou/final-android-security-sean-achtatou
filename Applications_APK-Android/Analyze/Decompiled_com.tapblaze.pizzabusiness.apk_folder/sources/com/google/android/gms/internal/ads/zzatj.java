package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzatj implements zzdgt<Void> {
    private final /* synthetic */ zzdhe zzdor;

    zzatj(zzatf zzatf, zzdhe zzdhe) {
        this.zzdor = zzdhe;
    }

    public final void zzb(Throwable th) {
        zzatf.zzdod.remove(this.zzdor);
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        Void voidR = (Void) obj;
        zzatf.zzdod.remove(this.zzdor);
    }
}
