package com.google.android.gms.ads.internal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaak;
import com.google.android.gms.internal.ads.zzaar;
import com.google.android.gms.internal.ads.zzaoy;
import com.google.android.gms.internal.ads.zzape;
import com.google.android.gms.internal.ads.zzaro;
import com.google.android.gms.internal.ads.zzavs;
import com.google.android.gms.internal.ads.zzayk;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzazd;
import com.google.android.gms.internal.ads.zzdq;
import com.google.android.gms.internal.ads.zzdt;
import com.google.android.gms.internal.ads.zzrg;
import com.google.android.gms.internal.ads.zzug;
import com.google.android.gms.internal.ads.zzuj;
import com.google.android.gms.internal.ads.zzuo;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzvg;
import com.google.android.gms.internal.ads.zzvh;
import com.google.android.gms.internal.ads.zzvt;
import com.google.android.gms.internal.ads.zzvx;
import com.google.android.gms.internal.ads.zzwc;
import com.google.android.gms.internal.ads.zzwi;
import com.google.android.gms.internal.ads.zzxa;
import com.google.android.gms.internal.ads.zzxb;
import com.google.android.gms.internal.ads.zzxh;
import com.google.android.gms.internal.ads.zzyw;
import java.util.Map;
import java.util.concurrent.Future;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzl extends zzvt {
    /* access modifiers changed from: private */
    public final zzazb zzbll;
    private final zzuj zzblm;
    /* access modifiers changed from: private */
    public final Future<zzdq> zzbln = zzazd.zzdwe.zzd(new zzm(this));
    private final zzo zzblo;
    /* access modifiers changed from: private */
    public WebView zzblp = new WebView(this.zzup);
    /* access modifiers changed from: private */
    public zzvh zzblq;
    /* access modifiers changed from: private */
    public zzdq zzblr;
    private AsyncTask<Void, Void, String> zzbls;
    /* access modifiers changed from: private */
    public final Context zzup;

    public zzl(Context context, zzuj zzuj, String str, zzazb zzazb) {
        this.zzup = context;
        this.zzbll = zzazb;
        this.zzblm = zzuj;
        this.zzblo = new zzo(str);
        zzbm(0);
        this.zzblp.setVerticalScrollBarEnabled(false);
        this.zzblp.getSettings().setJavaScriptEnabled(true);
        this.zzblp.setWebViewClient(new zzk(this));
        this.zzblp.setOnTouchListener(new zzn(this));
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        return null;
    }

    public final zzxb getVideoController() {
        return null;
    }

    public final boolean isLoading() throws RemoteException {
        return false;
    }

    public final boolean isReady() throws RemoteException {
        return false;
    }

    public final void setManualImpressionsEnabled(boolean z) throws RemoteException {
    }

    public final void stopLoading() throws RemoteException {
    }

    public final String zzka() throws RemoteException {
        return null;
    }

    public final zzxa zzkb() {
        return null;
    }

    public final IObjectWrapper zzjx() throws RemoteException {
        Preconditions.checkMainThread("getAdFrame must be called on the main UI thread.");
        return ObjectWrapper.wrap(this.zzblp);
    }

    public final void destroy() throws RemoteException {
        Preconditions.checkMainThread("destroy must be called on the main UI thread.");
        this.zzbls.cancel(true);
        this.zzbln.cancel(true);
        this.zzblp.destroy();
        this.zzblp = null;
    }

    public final boolean zza(zzug zzug) throws RemoteException {
        Preconditions.checkNotNull(this.zzblp, "This Search Ad has already been torn down");
        this.zzblo.zza(zzug, this.zzbll);
        this.zzbls = new zzp(this, null).execute(new Void[0]);
        return true;
    }

    public final void pause() throws RemoteException {
        Preconditions.checkMainThread("pause must be called on the main UI thread.");
    }

    public final void resume() throws RemoteException {
        Preconditions.checkMainThread("resume must be called on the main UI thread.");
    }

    public final void zza(zzvh zzvh) throws RemoteException {
        this.zzblq = zzvh;
    }

    public final void zza(zzwc zzwc) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzvx zzvx) {
        throw new IllegalStateException("Unused method");
    }

    public final Bundle getAdMetadata() {
        throw new IllegalStateException("Unused method");
    }

    public final void showInterstitial() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zzjy() throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final zzuj zzjz() throws RemoteException {
        return this.zzblm;
    }

    public final void zza(zzuj zzuj) throws RemoteException {
        throw new IllegalStateException("AdSize must be set before initialization");
    }

    public final void zza(zzaoy zzaoy) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzape zzape, String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final String getAdUnitId() {
        throw new IllegalStateException("getAdUnitId not implemented");
    }

    public final zzwc zzkc() {
        throw new IllegalStateException("getIAppEventListener not implemented");
    }

    public final zzvh zzkd() {
        throw new IllegalStateException("getIAdListener not implemented");
    }

    public final void zza(zzaak zzaak) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzvg zzvg) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzwi zzwi) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzaro zzaro) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void setUserId(String str) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void zzbr(String str) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzyw zzyw) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzxh zzxh) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzuo zzuo) {
        throw new IllegalStateException("Unused method");
    }

    public final void zza(zzrg zzrg) throws RemoteException {
        throw new IllegalStateException("Unused method");
    }

    public final void setImmersiveMode(boolean z) {
        throw new IllegalStateException("Unused method");
    }

    /* access modifiers changed from: package-private */
    public final int zzbs(String str) {
        String queryParameter = Uri.parse(str).getQueryParameter("height");
        if (TextUtils.isEmpty(queryParameter)) {
            return 0;
        }
        try {
            zzve.zzou();
            return zzayk.zza(this.zzup, Integer.parseInt(queryParameter));
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzbm(int i) {
        if (this.zzblp != null) {
            this.zzblp.setLayoutParams(new ViewGroup.LayoutParams(-1, i));
        }
    }

    /* access modifiers changed from: package-private */
    public final String zzke() {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https://").appendEncodedPath(zzaar.zzcso.get());
        builder.appendQueryParameter("query", this.zzblo.getQuery());
        builder.appendQueryParameter("pubId", this.zzblo.zzkh());
        Map<String, String> zzki = this.zzblo.zzki();
        for (String next : zzki.keySet()) {
            builder.appendQueryParameter(next, zzki.get(next));
        }
        Uri build = builder.build();
        zzdq zzdq = this.zzblr;
        if (zzdq != null) {
            try {
                build = zzdq.zza(build, this.zzup);
            } catch (zzdt e) {
                zzavs.zzd("Unable to process ad data", e);
            }
        }
        String zzkf = zzkf();
        String encodedQuery = build.getEncodedQuery();
        StringBuilder sb = new StringBuilder(String.valueOf(zzkf).length() + 1 + String.valueOf(encodedQuery).length());
        sb.append(zzkf);
        sb.append("#");
        sb.append(encodedQuery);
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final String zzkf() {
        String zzkg = this.zzblo.zzkg();
        if (TextUtils.isEmpty(zzkg)) {
            zzkg = "www.google.com";
        }
        String str = zzaar.zzcso.get();
        StringBuilder sb = new StringBuilder(String.valueOf(zzkg).length() + 8 + String.valueOf(str).length());
        sb.append("https://");
        sb.append(zzkg);
        sb.append(str);
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public final String zzbt(String str) {
        if (this.zzblr == null) {
            return str;
        }
        Uri parse = Uri.parse(str);
        try {
            parse = this.zzblr.zza(parse, this.zzup, null, null);
        } catch (zzdt e) {
            zzavs.zzd("Unable to process ad data", e);
        }
        return parse.toString();
    }

    /* access modifiers changed from: private */
    public final void zzbu(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        this.zzup.startActivity(intent);
    }
}
