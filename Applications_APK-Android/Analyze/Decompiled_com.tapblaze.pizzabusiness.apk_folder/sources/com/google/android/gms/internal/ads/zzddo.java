package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzddn;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzddo implements zzdsa {
    static final zzdsa zzew = new zzddo();

    private zzddo() {
    }

    public final boolean zzf(int i) {
        return zzddn.zza.zzdq(i) != null;
    }
}
