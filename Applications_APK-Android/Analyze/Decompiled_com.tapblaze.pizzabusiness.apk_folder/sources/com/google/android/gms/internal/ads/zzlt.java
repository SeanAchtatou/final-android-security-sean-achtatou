package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzlt implements Runnable {
    private final /* synthetic */ zzlp zzbat;
    private final /* synthetic */ IOException zzbau;

    zzlt(zzlp zzlp, IOException iOException) {
        this.zzbat = zzlp;
        this.zzbau = iOException;
    }

    public final void run() {
        this.zzbat.zzazt.zzb(this.zzbau);
    }
}
