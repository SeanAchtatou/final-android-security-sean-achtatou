package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzvn extends IInterface {
    void zza(PublisherAdViewOptions publisherAdViewOptions) throws RemoteException;

    void zza(zzaby zzaby) throws RemoteException;

    void zza(zzadi zzadi) throws RemoteException;

    void zza(zzadj zzadj) throws RemoteException;

    void zza(zzadu zzadu, zzuj zzuj) throws RemoteException;

    void zza(zzadv zzadv) throws RemoteException;

    void zza(zzagz zzagz) throws RemoteException;

    void zza(zzahh zzahh) throws RemoteException;

    void zza(String str, zzadp zzadp, zzado zzado) throws RemoteException;

    void zzb(zzvh zzvh) throws RemoteException;

    void zzb(zzwi zzwi) throws RemoteException;

    zzvm zzpd() throws RemoteException;
}
