package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdno extends zzdrt<zzdno, zza> implements zzdtg {
    private static volatile zzdtn<zzdno> zzdz;
    /* access modifiers changed from: private */
    public static final zzdno zzheb;
    private int zzhaa;
    private zzdnp zzhea;

    private zzdno() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdno, zza> implements zzdtg {
        private zza() {
            super(zzdno.zzheb);
        }

        public final zza zzeu(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdno) this.zzhmp).setVersion(0);
            return this;
        }

        public final zza zzb(zzdnp zzdnp) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdno) this.zzhmp).zza(zzdnp);
            return this;
        }

        /* synthetic */ zza(zzdnn zzdnn) {
            this();
        }
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public final zzdnp zzawh() {
        zzdnp zzdnp = this.zzhea;
        return zzdnp == null ? zzdnp.zzawl() : zzdnp;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdnp zzdnp) {
        zzdnp.getClass();
        this.zzhea = zzdnp;
    }

    public static zzdno zzax(zzdqk zzdqk) throws zzdse {
        return (zzdno) zzdrt.zza(zzheb, zzdqk);
    }

    public static zza zzawi() {
        return (zza) zzheb.zzazt();
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnn.zzdk[i - 1]) {
            case 1:
                return new zzdno();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzheb, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u000b\u0002\t", new Object[]{"zzhaa", "zzhea"});
            case 4:
                return zzheb;
            case 5:
                zzdtn<zzdno> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdno.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzheb);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzdno zzdno = new zzdno();
        zzheb = zzdno;
        zzdrt.zza(zzdno.class, zzdno);
    }
}
