package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.util.JsonReader;
import com.google.android.gms.ads.internal.zzq;
import java.io.IOException;
import org.json.JSONException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcps {
    public final String zzgeg;
    public String zzgeh;

    public zzcps(JsonReader jsonReader) throws IllegalStateException, IOException, JSONException, NumberFormatException {
        jsonReader.beginObject();
        String str = "";
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            nextName = nextName == null ? "" : nextName;
            char c = 65535;
            if (nextName.hashCode() == -995427962 && nextName.equals("params")) {
                c = 0;
            }
            if (c != 0) {
                jsonReader.skipValue();
            } else {
                str = jsonReader.nextString();
            }
        }
        this.zzgeg = str;
        jsonReader.endObject();
    }

    /* access modifiers changed from: package-private */
    public final zzcps zzn(Bundle bundle) {
        try {
            this.zzgeh = zzq.zzkq().zzd(bundle).toString();
        } catch (JSONException unused) {
            this.zzgeh = "{}";
        }
        return this;
    }
}
