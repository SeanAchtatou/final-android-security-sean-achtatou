package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzatx implements zzaui {
    static final zzaui zzdpr = new zzatx();

    private zzatx() {
    }

    public final Object zzb(zzbfq zzbfq) {
        return zzbfq.getAppIdOrigin();
    }
}
