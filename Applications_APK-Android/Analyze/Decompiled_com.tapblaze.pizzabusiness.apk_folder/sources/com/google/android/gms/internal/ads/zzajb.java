package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzajb implements zzazp<zzaif> {
    private final /* synthetic */ zzais zzczu;
    private final /* synthetic */ zzajj zzczy;

    zzajb(zzais zzais, zzajj zzajj) {
        this.zzczu = zzais;
        this.zzczy = zzajj;
    }

    public final /* synthetic */ void zzh(Object obj) {
        zzaif zzaif = (zzaif) obj;
        synchronized (this.zzczu.lock) {
            int unused = this.zzczu.status = 0;
            if (!(this.zzczu.zzczo == null || this.zzczy == this.zzczu.zzczo)) {
                zzavs.zzed("New JS engine is loaded, marking previous one as destroyable.");
                this.zzczu.zzczo.zzse();
            }
            zzajj unused2 = this.zzczu.zzczo = this.zzczy;
        }
    }
}
