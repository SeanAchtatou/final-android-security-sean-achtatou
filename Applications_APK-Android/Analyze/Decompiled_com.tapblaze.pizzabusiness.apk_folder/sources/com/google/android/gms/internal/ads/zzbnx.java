package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbnx implements zzdxg<zzbsu<zzbqx>> {
    private final zzdxp<zzbnw> zzfdd;
    private final zzbny zzfhc;

    private zzbnx(zzbny zzbny, zzdxp<zzbnw> zzdxp) {
        this.zzfhc = zzbny;
        this.zzfdd = zzdxp;
    }

    public static zzbnx zza(zzbny zzbny, zzdxp<zzbnw> zzdxp) {
        return new zzbnx(zzbny, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
