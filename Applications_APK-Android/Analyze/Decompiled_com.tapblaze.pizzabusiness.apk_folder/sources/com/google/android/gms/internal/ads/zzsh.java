package com.google.android.gms.internal.ads;

import java.io.InputStream;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzsh extends zzazl<InputStream> {
    private final /* synthetic */ zzse zzbrt;

    zzsh(zzse zzse) {
        this.zzbrt = zzse;
    }

    public final boolean cancel(boolean z) {
        this.zzbrt.disconnect();
        return super.cancel(z);
    }
}
