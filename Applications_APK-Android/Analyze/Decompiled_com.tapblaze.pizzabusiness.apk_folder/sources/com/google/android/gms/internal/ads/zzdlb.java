package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdlb extends zzdrt<zzdlb, zza> implements zzdtg {
    private static volatile zzdtn<zzdlb> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlb zzhag;
    private zzdlc zzhac;
    private int zzhaf;

    private zzdlb() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdlb, zza> implements zzdtg {
        private zza() {
            super(zzdlb.zzhag);
        }

        /* synthetic */ zza(zzdla zzdla) {
            this();
        }
    }

    public final int getKeySize() {
        return this.zzhaf;
    }

    public final zzdlc zzast() {
        zzdlc zzdlc = this.zzhac;
        return zzdlc == null ? zzdlc.zzasy() : zzdlc;
    }

    public static zzdlb zzv(zzdqk zzdqk) throws zzdse {
        return (zzdlb) zzdrt.zza(zzhag, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdla.zzdk[i - 1]) {
            case 1:
                return new zzdlb();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhag, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u000b\u0002\t", new Object[]{"zzhaf", "zzhac"});
            case 4:
                return zzhag;
            case 5:
                zzdtn<zzdlb> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlb.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhag);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzdlb zzdlb = new zzdlb();
        zzhag = zzdlb;
        zzdrt.zza(zzdlb.class, zzdlb);
    }
}
