package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcow implements zzcoz<zzbmd> {
    private final /* synthetic */ zzcot zzgdp;

    zzcow(zzcot zzcot) {
        this.zzgdp = zzcot;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcot.zza(com.google.android.gms.internal.ads.zzcot, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzcot, int]
     candidates:
      com.google.android.gms.internal.ads.zzcot.zza(com.google.android.gms.internal.ads.zzcot, com.google.android.gms.internal.ads.zzxa):com.google.android.gms.internal.ads.zzxa
      com.google.android.gms.internal.ads.zzcot.zza(com.google.android.gms.internal.ads.zzug, int):void
      com.google.android.gms.internal.ads.zzcot.zza(com.google.android.gms.internal.ads.zzcot, boolean):boolean */
    public final void zzamx() {
        synchronized (this.zzgdp) {
            boolean unused = this.zzgdp.zzadd = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcot.zza(com.google.android.gms.internal.ads.zzcot, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzcot, int]
     candidates:
      com.google.android.gms.internal.ads.zzcot.zza(com.google.android.gms.internal.ads.zzcot, com.google.android.gms.internal.ads.zzxa):com.google.android.gms.internal.ads.zzxa
      com.google.android.gms.internal.ads.zzcot.zza(com.google.android.gms.internal.ads.zzug, int):void
      com.google.android.gms.internal.ads.zzcot.zza(com.google.android.gms.internal.ads.zzcot, boolean):boolean */
    public final /* synthetic */ void onSuccess(Object obj) {
        zzbmd zzbmd = (zzbmd) obj;
        synchronized (this.zzgdp) {
            boolean unused = this.zzgdp.zzadd = false;
            zzxa unused2 = this.zzgdp.zzgdj = zzbmd.zzags();
            zzbmd.zzagf();
        }
    }
}
