package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzia extends Exception {
    public zzia(Throwable th) {
        super(th);
    }

    public zzia(String str) {
        super(str);
    }
}
