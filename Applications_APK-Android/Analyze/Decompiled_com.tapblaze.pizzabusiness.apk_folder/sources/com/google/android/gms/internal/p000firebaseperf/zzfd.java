package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfd  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzfd implements zzgm {
    private static final zzfd zzqo = new zzfd();

    private zzfd() {
    }

    public static zzfd zzhk() {
        return zzqo;
    }

    public final boolean zzb(Class<?> cls) {
        return zzfc.class.isAssignableFrom(cls);
    }

    public final zzgj zzc(Class<?> cls) {
        if (!zzfc.class.isAssignableFrom(cls)) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (zzgj) zzfc.zza(cls.asSubclass(zzfc.class)).dynamicMethod(zzfc.zze.BUILD_MESSAGE_INFO, null, null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
