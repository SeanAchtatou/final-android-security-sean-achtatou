package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbei {
    Context getContext();

    Activity zzyn();
}
