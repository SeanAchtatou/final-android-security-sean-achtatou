package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcmn implements zzbeu {
    private final zzbdi zzfpv;
    private final zzccd zzfzn;

    zzcmn(zzccd zzccd, zzbdi zzbdi) {
        this.zzfzn = zzccd;
        this.zzfpv = zzbdi;
    }

    public final void zzak(boolean z) {
        zzccd zzccd = this.zzfzn;
        zzbdi zzbdi = this.zzfpv;
        zzccd.zzakx();
        zzbdi.zzzu();
        zzbdi.zzaaa().zzaaz();
    }
}
