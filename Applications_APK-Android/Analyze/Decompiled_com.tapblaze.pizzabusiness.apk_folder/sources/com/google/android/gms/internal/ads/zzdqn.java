package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdqn extends zzdqu {
    private final int zzhia;
    private final int zzhib;

    zzdqn(byte[] bArr, int i, int i2) {
        super(bArr);
        zzi(i, i + i2, bArr.length);
        this.zzhia = i;
        this.zzhib = i2;
    }

    public final byte zzfe(int i) {
        zzz(i, size());
        return this.zzhid[this.zzhia + i];
    }

    /* access modifiers changed from: package-private */
    public final byte zzff(int i) {
        return this.zzhid[this.zzhia + i];
    }

    public final int size() {
        return this.zzhib;
    }

    /* access modifiers changed from: protected */
    public final int zzaxz() {
        return this.zzhia;
    }

    /* access modifiers changed from: protected */
    public final void zzb(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.zzhid, zzaxz() + i, bArr, i2, i3);
    }
}
