package com.google.android.gms.internal.ads;

import java.util.Set;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcem implements zzdxg<Set<zzbsu<zzbpe>>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzceo> zzfsb;
    private final zzcee zzfth;

    private zzcem(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfth = zzcee;
        this.zzfsb = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzcem zzh(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzcem(zzcee, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(zzcee.zzf(this.zzfsb.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
