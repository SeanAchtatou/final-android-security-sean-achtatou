package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzalw extends zzxe {
    private final Object lock = new Object();
    private volatile zzxg zzddj;

    public final void play() throws RemoteException {
        throw new RemoteException();
    }

    public final void pause() throws RemoteException {
        throw new RemoteException();
    }

    public final void stop() throws RemoteException {
        throw new RemoteException();
    }

    public final void mute(boolean z) throws RemoteException {
        throw new RemoteException();
    }

    public final boolean isMuted() throws RemoteException {
        throw new RemoteException();
    }

    public final int getPlaybackState() throws RemoteException {
        throw new RemoteException();
    }

    public final float zzpk() throws RemoteException {
        throw new RemoteException();
    }

    public final float zzpl() throws RemoteException {
        throw new RemoteException();
    }

    public final void zza(zzxg zzxg) throws RemoteException {
        synchronized (this.lock) {
            this.zzddj = zzxg;
        }
    }

    public final zzxg zzpm() throws RemoteException {
        zzxg zzxg;
        synchronized (this.lock) {
            zzxg = this.zzddj;
        }
        return zzxg;
    }

    public final float getAspectRatio() throws RemoteException {
        throw new RemoteException();
    }

    public final boolean isCustomControlsEnabled() throws RemoteException {
        throw new RemoteException();
    }

    public final boolean isClickToExpandEnabled() throws RemoteException {
        throw new RemoteException();
    }
}
