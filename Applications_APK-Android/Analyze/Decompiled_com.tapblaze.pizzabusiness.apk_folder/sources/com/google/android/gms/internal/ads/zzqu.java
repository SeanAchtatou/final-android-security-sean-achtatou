package com.google.android.gms.internal.ads;

import android.app.Activity;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzqu {
    void onActivityPaused(Activity activity);

    void onActivityResumed(Activity activity);

    boolean zza(Activity activity);
}
