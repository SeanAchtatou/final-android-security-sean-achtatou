package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbjz extends zzboe<zzbka> {
    zzbjz zza(zzbkf zzbkf);

    zzbjz zzb(zzbod zzbod);

    zzbjz zzb(zzbrm zzbrm);
}
