package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzdz  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzdz {
    private static final Class<?> zzms = zzai("libcore.io.Memory");
    private static final boolean zzmt = (zzai("org.robolectric.Robolectric") != null);

    static boolean zzgi() {
        return zzms != null && !zzmt;
    }

    static Class<?> zzgj() {
        return zzms;
    }

    private static <T> Class<T> zzai(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable unused) {
            return null;
        }
    }
}
