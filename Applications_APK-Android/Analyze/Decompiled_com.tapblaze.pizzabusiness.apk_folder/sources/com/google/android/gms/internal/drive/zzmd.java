package com.google.android.gms.internal.drive;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class zzmd {
    private static final zzmd zzuw = new zzmd();
    private final zzmg zzux = new zzlf();
    private final ConcurrentMap<Class<?>, zzmf<?>> zzuy = new ConcurrentHashMap();

    public static zzmd zzej() {
        return zzuw;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.drive.zzkm.zza(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class<T>, java.lang.String]
     candidates:
      com.google.android.gms.internal.drive.zzkm.zza(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.drive.zzkm.zza(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.drive.zzkm.zza(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.drive.zzmf<T>, java.lang.String]
     candidates:
      com.google.android.gms.internal.drive.zzkm.zza(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.drive.zzkm.zza(java.lang.Object, java.lang.String):T */
    public final <T> zzmf<T> zzf(Class<T> cls) {
        zzkm.zza((Object) cls, "messageType");
        zzmf<T> zzmf = this.zzuy.get(cls);
        if (zzmf != null) {
            return zzmf;
        }
        zzmf<T> zze = this.zzux.zze(cls);
        zzkm.zza((Object) cls, "messageType");
        zzkm.zza((Object) zze, "schema");
        zzmf<T> putIfAbsent = this.zzuy.putIfAbsent(cls, zze);
        return putIfAbsent != null ? putIfAbsent : zze;
    }

    public final <T> zzmf<T> zzq(T t) {
        return zzf(t.getClass());
    }

    private zzmd() {
    }
}
