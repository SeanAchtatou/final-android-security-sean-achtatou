package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdjk extends zzdik<zzdhx, zzdlo> {
    zzdjk(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        zzdlo zzdlo = (zzdlo) obj;
        return new zzdoe(zzdlo.zzass().toByteArray(), zzdlo.zzatq().zzatn());
    }
}
