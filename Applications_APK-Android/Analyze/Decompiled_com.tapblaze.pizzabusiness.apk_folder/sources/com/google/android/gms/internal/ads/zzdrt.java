package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;
import com.google.android.gms.internal.ads.zzdrt.zzb;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzdrt<MessageType extends zzdrt<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzdqa<MessageType, BuilderType> {
    private static Map<Object, zzdrt<?, ?>> zzhmm = new ConcurrentHashMap();
    protected zzdur zzhmk = zzdur.zzbcf();
    private int zzhml = -1;

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static class zza<T extends zzdrt<T, ?>> extends zzdqb<T> {
        private final T zzhmo;

        public zza(T t) {
            this.zzhmo = t;
        }
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static final class zzc implements zzdro<zzc> {
        public final int zzae() {
            throw new NoSuchMethodError();
        }

        public final zzdvf zzazo() {
            throw new NoSuchMethodError();
        }

        public final zzdvm zzazp() {
            throw new NoSuchMethodError();
        }

        public final boolean zzazq() {
            throw new NoSuchMethodError();
        }

        public final boolean zzazr() {
            throw new NoSuchMethodError();
        }

        public final zzdtd zza(zzdtd zzdtd, zzdte zzdte) {
            throw new NoSuchMethodError();
        }

        public final zzdtj zza(zzdtj zzdtj, zzdtj zzdtj2) {
            throw new NoSuchMethodError();
        }

        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static final class zze {
        public static final int zzhms = 1;
        public static final int zzhmt = 2;
        public static final int zzhmu = 3;
        public static final int zzhmv = 4;
        public static final int zzhmw = 5;
        public static final int zzhmx = 6;
        public static final int zzhmy = 7;
        private static final /* synthetic */ int[] zzhmz = {zzhms, zzhmt, zzhmu, zzhmv, zzhmw, zzhmx, zzhmy};
        public static final int zzhna = 1;
        public static final int zzhnb = 2;
        private static final /* synthetic */ int[] zzhnc = {zzhna, zzhnb};
        public static final int zzhnd = 1;
        public static final int zzhne = 2;
        private static final /* synthetic */ int[] zzhnf = {zzhnd, zzhne};

        public static int[] zzbah() {
            return (int[]) zzhmz.clone();
        }
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static class zzf<ContainingType extends zzdte, Type> extends zzdrh<ContainingType, Type> {
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static abstract class zzd<MessageType extends zzd<MessageType, BuilderType>, BuilderType> extends zzdrt<MessageType, BuilderType> implements zzdtg {
        protected zzdrm<zzc> zzhmr = zzdrm.zzazm();

        /* access modifiers changed from: package-private */
        public final zzdrm<zzc> zzbag() {
            if (this.zzhmr.isImmutable()) {
                this.zzhmr = (zzdrm) this.zzhmr.clone();
            }
            return this.zzhmr;
        }
    }

    public String toString() {
        return zzdtf.zza(this, super.toString());
    }

    public int hashCode() {
        if (this.zzhhk != 0) {
            return this.zzhhk;
        }
        this.zzhhk = zzdtp.zzbbm().zzba(this).hashCode(this);
        return this.zzhhk;
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static abstract class zzb<MessageType extends zzdrt<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> extends zzdpz<MessageType, BuilderType> {
        private final MessageType zzhmo;
        protected MessageType zzhmp;
        protected boolean zzhmq = false;

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected zzb(MessageType r3) {
            /*
                r2 = this;
                r2.<init>()
                r2.zzhmo = r3
                int r0 = com.google.android.gms.internal.ads.zzdrt.zze.zzhmv
                r1 = 0
                java.lang.Object r3 = r3.zza(r0, r1, r1)
                com.google.android.gms.internal.ads.zzdrt r3 = (com.google.android.gms.internal.ads.zzdrt) r3
                r2.zzhmp = r3
                r3 = 0
                r2.zzhmq = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdrt.zzb.<init>(com.google.android.gms.internal.ads.zzdrt):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected void zzbab() {
            /*
                r3 = this;
                MessageType r0 = r3.zzhmp
                int r1 = com.google.android.gms.internal.ads.zzdrt.zze.zzhmv
                r2 = 0
                java.lang.Object r0 = r0.zza(r1, r2, r2)
                com.google.android.gms.internal.ads.zzdrt r0 = (com.google.android.gms.internal.ads.zzdrt) r0
                MessageType r1 = r3.zzhmp
                zza(r0, r1)
                r3.zzhmp = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdrt.zzb.zzbab():void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.ads.zzdrt.zza(com.google.android.gms.internal.ads.zzdrt, boolean):boolean
         arg types: [MessageType, int]
         candidates:
          com.google.android.gms.internal.ads.zzdrt.zza(com.google.android.gms.internal.ads.zzdrt, com.google.android.gms.internal.ads.zzdqk):T
          com.google.android.gms.internal.ads.zzdrt.zza(com.google.android.gms.internal.ads.zzdrt, byte[]):T
          com.google.android.gms.internal.ads.zzdrt.zza(java.lang.Class, com.google.android.gms.internal.ads.zzdrt):void
          com.google.android.gms.internal.ads.zzdqa.zza(java.lang.Iterable, java.util.List):void
          com.google.android.gms.internal.ads.zzdrt.zza(com.google.android.gms.internal.ads.zzdrt, boolean):boolean */
        public final boolean isInitialized() {
            return zzdrt.zza((zzdrt) this.zzhmp, false);
        }

        /* renamed from: zzbac */
        public MessageType zzbae() {
            if (this.zzhmq) {
                return this.zzhmp;
            }
            MessageType messagetype = this.zzhmp;
            zzdtp.zzbbm().zzba(messagetype).zzan(messagetype);
            this.zzhmq = true;
            return this.zzhmp;
        }

        /* renamed from: zzbad */
        public final MessageType zzbaf() {
            MessageType messagetype = (zzdrt) zzbae();
            if (messagetype.isInitialized()) {
                return messagetype;
            }
            throw new zzdup(messagetype);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.ads.zzdrt.zzb.zza(com.google.android.gms.internal.ads.zzdrt, com.google.android.gms.internal.ads.zzdrt):void
         arg types: [MessageType, MessageType]
         candidates:
          com.google.android.gms.internal.ads.zzdrt.zzb.zza(com.google.android.gms.internal.ads.zzdqw, com.google.android.gms.internal.ads.zzdrg):com.google.android.gms.internal.ads.zzdpz
          com.google.android.gms.internal.ads.zzdpz.zza(com.google.android.gms.internal.ads.zzdqw, com.google.android.gms.internal.ads.zzdrg):BuilderType
          com.google.android.gms.internal.ads.zzdrt.zzb.zza(com.google.android.gms.internal.ads.zzdrt, com.google.android.gms.internal.ads.zzdrt):void */
        /* renamed from: zzb */
        public final BuilderType zza(MessageType messagetype) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            zza((zzdrt) this.zzhmp, (zzdrt) messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzdtp.zzbbm().zzba(messagetype).zzf(messagetype, messagetype2);
        }

        private final BuilderType zzb(byte[] bArr, int i, int i2, zzdrg zzdrg) throws zzdse {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            try {
                zzdtp.zzbbm().zzba(this.zzhmp).zza(this.zzhmp, bArr, 0, i2 + 0, new zzdqf(zzdrg));
                return this;
            } catch (zzdse e) {
                throw e;
            } catch (IndexOutOfBoundsException unused) {
                throw zzdse.zzbaj();
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: zzb */
        public final BuilderType zza(zzdqw zzdqw, zzdrg zzdrg) throws IOException {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            try {
                zzdtp.zzbbm().zzba(this.zzhmp).zza(this.zzhmp, zzdqz.zza(zzdqw), zzdrg);
                return this;
            } catch (RuntimeException e) {
                if (e.getCause() instanceof IOException) {
                    throw ((IOException) e.getCause());
                }
                throw e;
            }
        }

        public final /* synthetic */ zzdpz zza(byte[] bArr, int i, int i2, zzdrg zzdrg) throws zzdse {
            return zzb(bArr, 0, i2, zzdrg);
        }

        public final /* synthetic */ zzdpz zzaxj() {
            return (zzb) clone();
        }

        public final /* synthetic */ zzdte zzazz() {
            return this.zzhmo;
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zzb zzb = (zzb) ((zzdrt) this.zzhmo).zza(zze.zzhmw, (Object) null, (Object) null);
            zzb.zza((zzdrt) zzbae());
            return zzb;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((zzdrt) zza(zze.zzhmx, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return zzdtp.zzbbm().zzba(this).equals(this, (zzdrt) obj);
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzdrt<MessageType, BuilderType>, BuilderType extends zzb<MessageType, BuilderType>> BuilderType zzazt() {
        return (zzb) zza(zze.zzhmw, (Object) null, (Object) null);
    }

    public final boolean isInitialized() {
        return zza(this, Boolean.TRUE.booleanValue());
    }

    /* access modifiers changed from: package-private */
    public final int zzaxl() {
        return this.zzhml;
    }

    /* access modifiers changed from: package-private */
    public final void zzfa(int i) {
        this.zzhml = i;
    }

    public final void zzb(zzdrb zzdrb) throws IOException {
        zzdtp.zzbbm().zzba(this).zza(this, zzdrf.zza(zzdrb));
    }

    public final int zzazu() {
        if (this.zzhml == -1) {
            this.zzhml = zzdtp.zzbbm().zzba(this).zzax(this);
        }
        return this.zzhml;
    }

    static <T extends zzdrt<?, ?>> T zzd(Class cls) {
        T t = (zzdrt) zzhmm.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (zzdrt) zzhmm.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (t == null) {
            t = (zzdrt) ((zzdrt) zzduy.zzj(cls)).zza(zze.zzhmx, (Object) null, (Object) null);
            if (t != null) {
                zzhmm.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    protected static <T extends zzdrt<?, ?>> void zza(Class cls, zzdrt zzdrt) {
        zzhmm.put(cls, zzdrt);
    }

    protected static Object zza(zzdte zzdte, String str, Object[] objArr) {
        return new zzdtr(zzdte, str, objArr);
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    protected static final <T extends zzdrt<T, ?>> boolean zza(zzdrt zzdrt, boolean z) {
        byte byteValue = ((Byte) zzdrt.zza(zze.zzhms, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzaz = zzdtp.zzbbm().zzba(zzdrt).zzaz(zzdrt);
        if (z) {
            zzdrt.zza(zze.zzhmt, zzaz ? zzdrt : null, (Object) null);
        }
        return zzaz;
    }

    protected static zzdrz zzazv() {
        return zzdrw.zzbai();
    }

    protected static zzdrz zza(zzdrz zzdrz) {
        int size = zzdrz.size();
        return zzdrz.zzgk(size == 0 ? 10 : size << 1);
    }

    protected static <E> zzdsb<E> zzazw() {
        return zzdts.zzbbp();
    }

    protected static <E> zzdsb<E> zza(zzdsb zzdsb) {
        int size = zzdsb.size();
        return zzdsb.zzfd(size == 0 ? 10 : size << 1);
    }

    private static <T extends zzdrt<T, ?>> T zza(zzdrt zzdrt, zzdqw zzdqw, zzdrg zzdrg) throws zzdse {
        T t = (zzdrt) zzdrt.zza(zze.zzhmv, (Object) null, (Object) null);
        try {
            zzdua zzba = zzdtp.zzbbm().zzba(t);
            zzba.zza(t, zzdqz.zza(zzdqw), zzdrg);
            zzba.zzan(t);
            return t;
        } catch (IOException e) {
            if (e.getCause() instanceof zzdse) {
                throw ((zzdse) e.getCause());
            }
            throw new zzdse(e.getMessage()).zzl(t);
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof zzdse) {
                throw ((zzdse) e2.getCause());
            }
            throw e2;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private static <T extends com.google.android.gms.internal.ads.zzdrt<T, ?>> T zza(T r6, byte[] r7, int r8, int r9, com.google.android.gms.internal.ads.zzdrg r10) throws com.google.android.gms.internal.ads.zzdse {
        /*
            int r8 = com.google.android.gms.internal.ads.zzdrt.zze.zzhmv
            r0 = 0
            java.lang.Object r6 = r6.zza(r8, r0, r0)
            com.google.android.gms.internal.ads.zzdrt r6 = (com.google.android.gms.internal.ads.zzdrt) r6
            com.google.android.gms.internal.ads.zzdtp r8 = com.google.android.gms.internal.ads.zzdtp.zzbbm()     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
            com.google.android.gms.internal.ads.zzdua r8 = r8.zzba(r6)     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
            r3 = 0
            com.google.android.gms.internal.ads.zzdqf r5 = new com.google.android.gms.internal.ads.zzdqf     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
            r5.<init>(r10)     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
            r0 = r8
            r1 = r6
            r2 = r7
            r4 = r9
            r0.zza(r1, r2, r3, r4, r5)     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
            r8.zzan(r6)     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
            int r7 = r6.zzhhk     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
            if (r7 != 0) goto L_0x0026
            return r6
        L_0x0026:
            java.lang.RuntimeException r7 = new java.lang.RuntimeException     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
            r7.<init>()     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
            throw r7     // Catch:{ IOException -> 0x0035, IndexOutOfBoundsException -> 0x002c }
        L_0x002c:
            com.google.android.gms.internal.ads.zzdse r7 = com.google.android.gms.internal.ads.zzdse.zzbaj()
            com.google.android.gms.internal.ads.zzdse r6 = r7.zzl(r6)
            throw r6
        L_0x0035:
            r7 = move-exception
            java.lang.Throwable r8 = r7.getCause()
            boolean r8 = r8 instanceof com.google.android.gms.internal.ads.zzdse
            if (r8 == 0) goto L_0x0045
            java.lang.Throwable r6 = r7.getCause()
            com.google.android.gms.internal.ads.zzdse r6 = (com.google.android.gms.internal.ads.zzdse) r6
            throw r6
        L_0x0045:
            com.google.android.gms.internal.ads.zzdse r8 = new com.google.android.gms.internal.ads.zzdse
            java.lang.String r7 = r7.getMessage()
            r8.<init>(r7)
            com.google.android.gms.internal.ads.zzdse r6 = r8.zzl(r6)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdrt.zza(com.google.android.gms.internal.ads.zzdrt, byte[], int, int, com.google.android.gms.internal.ads.zzdrg):com.google.android.gms.internal.ads.zzdrt");
    }

    private static <T extends zzdrt<T, ?>> T zza(zzdrt zzdrt) throws zzdse {
        if (zzdrt == null || zzdrt.isInitialized()) {
            return zzdrt;
        }
        throw new zzdse(new zzdup(zzdrt).getMessage()).zzl(zzdrt);
    }

    protected static <T extends zzdrt<T, ?>> T zza(zzdrt zzdrt, zzdqk zzdqk) throws zzdse {
        return zza(zza(zzb(zzdrt, zzdqk, zzdrg.zzazh())));
    }

    protected static <T extends zzdrt<T, ?>> T zza(zzdrt zzdrt, zzdqk zzdqk, zzdrg zzdrg) throws zzdse {
        return zza(zzb(zzdrt, zzdqk, zzdrg));
    }

    private static <T extends zzdrt<T, ?>> T zzb(T t, zzdqk zzdqk, zzdrg zzdrg) throws zzdse {
        T zza2;
        try {
            zzdqw zzaxv = zzdqk.zzaxv();
            zza2 = zza(t, zzaxv, zzdrg);
            zzaxv.zzfh(0);
            return zza2;
        } catch (zzdse e) {
            throw e.zzl(zza2);
        } catch (zzdse e2) {
            throw e2;
        }
    }

    protected static <T extends zzdrt<T, ?>> T zza(zzdrt zzdrt, byte[] bArr) throws zzdse {
        return zza(zza(zzdrt, bArr, 0, bArr.length, zzdrg.zzazh()));
    }

    protected static <T extends zzdrt<T, ?>> T zza(zzdrt zzdrt, byte[] bArr, zzdrg zzdrg) throws zzdse {
        return zza(zza(zzdrt, bArr, 0, bArr.length, zzdrg));
    }

    public final /* synthetic */ zzdtd zzazx() {
        zzb zzb2 = (zzb) zza(zze.zzhmw, (Object) null, (Object) null);
        zzb2.zza(this);
        return zzb2;
    }

    public final /* synthetic */ zzdtd zzazy() {
        return (zzb) zza(zze.zzhmw, (Object) null, (Object) null);
    }

    public final /* synthetic */ zzdte zzazz() {
        return (zzdrt) zza(zze.zzhmx, (Object) null, (Object) null);
    }
}
