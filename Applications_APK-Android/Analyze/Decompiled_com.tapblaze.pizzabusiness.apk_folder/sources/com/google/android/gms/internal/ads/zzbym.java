package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbym implements zzdxg<zzbsu<VideoController.VideoLifecycleCallbacks>> {
    private final zzdxp<zzcax> zzfdd;
    private final zzdxp<Executor> zzfei;
    private final zzbyg zzfon;

    public zzbym(zzbyg zzbyg, zzdxp<zzcax> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfon = zzbyg;
        this.zzfdd = zzdxp;
        this.zzfei = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), this.zzfei.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
