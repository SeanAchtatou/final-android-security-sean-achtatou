package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzht  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
abstract class zzht<T, B> {
    zzht() {
    }

    /* access modifiers changed from: package-private */
    public abstract void zza(T t, zzin zzin) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zzc(T t, zzin zzin) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void zzf(Object obj);

    /* access modifiers changed from: package-private */
    public abstract void zzf(Object obj, T t);

    /* access modifiers changed from: package-private */
    public abstract T zzg(T t, T t2);

    /* access modifiers changed from: package-private */
    public abstract int zzm(T t);

    /* access modifiers changed from: package-private */
    public abstract T zzp(Object obj);

    /* access modifiers changed from: package-private */
    public abstract int zzq(T t);
}
