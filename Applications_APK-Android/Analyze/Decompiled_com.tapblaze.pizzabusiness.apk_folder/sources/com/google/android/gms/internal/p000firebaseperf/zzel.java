package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;
import java.nio.charset.Charset;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzel  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
class zzel extends zzei {
    protected final byte[] zznb;

    zzel(byte[] bArr) {
        if (bArr != null) {
            this.zznb = bArr;
            return;
        }
        throw new NullPointerException();
    }

    /* access modifiers changed from: protected */
    public int zzgn() {
        return 0;
    }

    public byte zzq(int i) {
        return this.zznb[i];
    }

    /* access modifiers changed from: package-private */
    public byte zzr(int i) {
        return this.zznb[i];
    }

    public int size() {
        return this.zznb.length;
    }

    public final zzeb zzd(int i, int i2) {
        int zzc = zzc(0, i2, size());
        if (zzc == 0) {
            return zzeb.zzmv;
        }
        return new zzee(this.zznb, zzgn(), zzc);
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdy zzdy) throws IOException {
        zzdy.zza(this.zznb, zzgn(), size());
    }

    /* access modifiers changed from: protected */
    public final String zza(Charset charset) {
        return new String(this.zznb, zzgn(), size(), charset);
    }

    public final boolean zzgl() {
        int zzgn = zzgn();
        return zzic.zzc(this.zznb, zzgn, size() + zzgn);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzeb) || size() != ((zzeb) obj).size()) {
            return false;
        }
        if (size() == 0) {
            return true;
        }
        if (!(obj instanceof zzel)) {
            return obj.equals(this);
        }
        zzel zzel = (zzel) obj;
        int zzgm = zzgm();
        int zzgm2 = zzel.zzgm();
        if (zzgm == 0 || zzgm2 == 0 || zzgm == zzgm2) {
            return zza(zzel, 0, size());
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean zza(zzeb zzeb, int i, int i2) {
        if (i2 > zzeb.size()) {
            int size = size();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(i2);
            sb.append(size);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 > zzeb.size()) {
            int size2 = zzeb.size();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: 0, ");
            sb2.append(i2);
            sb2.append(", ");
            sb2.append(size2);
            throw new IllegalArgumentException(sb2.toString());
        } else if (!(zzeb instanceof zzel)) {
            return zzeb.zzd(0, i2).equals(zzd(0, i2));
        } else {
            zzel zzel = (zzel) zzeb;
            byte[] bArr = this.zznb;
            byte[] bArr2 = zzel.zznb;
            int zzgn = zzgn() + i2;
            int zzgn2 = zzgn();
            int zzgn3 = zzel.zzgn();
            while (zzgn2 < zzgn) {
                if (bArr[zzgn2] != bArr2[zzgn3]) {
                    return false;
                }
                zzgn2++;
                zzgn3++;
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public final int zzb(int i, int i2, int i3) {
        return zzfg.zza(i, this.zznb, zzgn(), i3);
    }
}
