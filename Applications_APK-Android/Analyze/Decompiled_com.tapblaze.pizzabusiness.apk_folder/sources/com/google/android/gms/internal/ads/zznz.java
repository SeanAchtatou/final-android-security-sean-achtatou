package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zznz<S> {
    void zza(Object obj, zznq zznq);

    void zzc(S s, int i);

    void zze(S s);
}
