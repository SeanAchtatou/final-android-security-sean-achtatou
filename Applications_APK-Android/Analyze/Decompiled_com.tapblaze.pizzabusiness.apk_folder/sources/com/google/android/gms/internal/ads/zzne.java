package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzne {
    int length();

    zzgw zzaw(int i);

    int zzax(int i);

    zzms zzid();
}
