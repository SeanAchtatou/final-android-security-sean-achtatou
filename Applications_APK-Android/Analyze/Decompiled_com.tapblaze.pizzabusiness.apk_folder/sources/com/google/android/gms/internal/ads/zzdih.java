package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdte;
import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzdih<KeyFormatProtoT extends zzdte, KeyT> {
    private final Class<KeyFormatProtoT> zzgxv;

    public zzdih(Class<KeyFormatProtoT> cls) {
        this.zzgxv = cls;
    }

    public abstract void zzc(KeyFormatProtoT keyformatprotot) throws GeneralSecurityException;

    public abstract KeyT zzd(KeyFormatProtoT keyformatprotot) throws GeneralSecurityException;

    public abstract KeyFormatProtoT zzq(zzdqk zzdqk) throws zzdse;

    public final Class<KeyFormatProtoT> zzasb() {
        return this.zzgxv;
    }
}
