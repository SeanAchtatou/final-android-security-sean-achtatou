package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzakh {
    private zzais zzdbc;
    private zzdhe<zzajq> zzdbd;

    zzakh(zzais zzais) {
        this.zzdbc = zzais;
    }

    private final void zzsi() {
        if (this.zzdbd == null) {
            zzazl zzazl = new zzazl();
            this.zzdbd = zzazl;
            this.zzdbc.zzb((zzdq) null).zza(new zzakk(zzazl), new zzakj(zzazl));
        }
    }

    public final <I, O> zzako<I, O> zzb(String str, zzajv<I> zzajv, zzajw<O> zzajw) {
        zzsi();
        return new zzako<>(this.zzdbd, str, zzajv, zzajw);
    }

    public final void zzc(String str, zzafn<? super zzajq> zzafn) {
        zzsi();
        this.zzdbd = zzdgs.zzb(this.zzdbd, new zzakm(str, zzafn), zzazd.zzdwj);
    }

    public final void zzd(String str, zzafn<? super zzajq> zzafn) {
        this.zzdbd = zzdgs.zzb(this.zzdbd, new zzakl(str, zzafn), zzazd.zzdwj);
    }
}
