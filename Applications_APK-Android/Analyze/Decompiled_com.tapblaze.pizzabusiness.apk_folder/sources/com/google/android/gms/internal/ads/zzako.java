package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzako<I, O> implements zzdgf<I, O> {
    /* access modifiers changed from: private */
    public final zzajw<O> zzdaw;
    private final zzajv<I> zzdax;
    private final String zzday;
    private final zzdhe<zzajq> zzdbd;

    zzako(zzdhe<zzajq> zzdhe, String str, zzajv<I> zzajv, zzajw<O> zzajw) {
        this.zzdbd = zzdhe;
        this.zzday = str;
        this.zzdax = zzajv;
        this.zzdaw = zzajw;
    }

    public final zzdhe<O> zzf(I i) throws Exception {
        return zzdgs.zzb(this.zzdbd, new zzakn(this, i), zzazd.zzdwj);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(Object obj, zzajq zzajq) throws Exception {
        zzazl zzazl = new zzazl();
        zzq.zzkq();
        String zzwk = zzawb.zzwk();
        zzafa.zzcxi.zza(zzwk, new zzakq(this, zzazl));
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", zzwk);
        jSONObject.put("args", this.zzdax.zzj(obj));
        zzajq.zza(this.zzday, jSONObject);
        return zzazl;
    }
}
