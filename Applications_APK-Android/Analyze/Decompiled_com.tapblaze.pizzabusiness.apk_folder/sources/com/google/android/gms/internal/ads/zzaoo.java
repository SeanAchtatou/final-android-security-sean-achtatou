package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzaoo {
    private final zzbdi zzcza;
    private final String zzdgj;

    public zzaoo(zzbdi zzbdi) {
        this(zzbdi, "");
    }

    public zzaoo(zzbdi zzbdi, String str) {
        this.zzcza = zzbdi;
        this.zzdgj = str;
    }

    public final void zzds(String str) {
        try {
            JSONObject put = new JSONObject().put("message", str).put("action", this.zzdgj);
            if (this.zzcza != null) {
                this.zzcza.zzb("onError", put);
            }
        } catch (JSONException e) {
            zzavs.zzc("Error occurred while dispatching error event.", e);
        }
    }

    public final void zzdt(String str) {
        try {
            this.zzcza.zzb("onReadyEventReceived", new JSONObject().put("js", str));
        } catch (JSONException e) {
            zzavs.zzc("Error occurred while dispatching ready Event.", e);
        }
    }

    public final void zzb(int i, int i2, int i3, int i4) {
        try {
            this.zzcza.zzb("onSizeChanged", new JSONObject().put("x", i).put("y", i2).put("width", i3).put("height", i4));
        } catch (JSONException e) {
            zzavs.zzc("Error occurred while dispatching size change.", e);
        }
    }

    public final void zzc(int i, int i2, int i3, int i4) {
        try {
            this.zzcza.zzb("onDefaultPositionReceived", new JSONObject().put("x", i).put("y", i2).put("width", i3).put("height", i4));
        } catch (JSONException e) {
            zzavs.zzc("Error occurred while dispatching default position.", e);
        }
    }

    public final void zzdu(String str) {
        try {
            this.zzcza.zzb("onStateChanged", new JSONObject().put("state", str));
        } catch (JSONException e) {
            zzavs.zzc("Error occurred while dispatching state change.", e);
        }
    }

    public final void zza(int i, int i2, int i3, int i4, float f, int i5) {
        try {
            this.zzcza.zzb("onScreenInfoChanged", new JSONObject().put("width", i).put("height", i2).put("maxSizeWidth", i3).put("maxSizeHeight", i4).put("density", (double) f).put("rotation", i5));
        } catch (JSONException e) {
            zzavs.zzc("Error occurred while obtaining screen information.", e);
        }
    }
}
