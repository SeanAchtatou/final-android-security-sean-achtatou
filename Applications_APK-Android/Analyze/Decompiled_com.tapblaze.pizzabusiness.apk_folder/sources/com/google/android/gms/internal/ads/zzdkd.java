package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdkd extends zzdii<zzdmn> {
    public zzdkd() {
        super(zzdmn.class, new zzdkg(zzdie.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPublicKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.ASYMMETRIC_PUBLIC;
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdmn zzdmn = (zzdmn) zzdte;
        zzdpo.zzx(zzdmn.getVersion(), 0);
        zzdkk.zza(zzdmn.zzauf());
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdmn.zzap(zzdqk);
    }
}
