package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcac implements zzdxg<zzbzz> {
    private final zzdxp<String> zzfhl;
    private final zzdxp<zzbws> zzfkx;
    private final zzdxp<zzbwk> zzfqc;

    private zzcac(zzdxp<String> zzdxp, zzdxp<zzbwk> zzdxp2, zzdxp<zzbws> zzdxp3) {
        this.zzfhl = zzdxp;
        this.zzfqc = zzdxp2;
        this.zzfkx = zzdxp3;
    }

    public static zzcac zzi(zzdxp<String> zzdxp, zzdxp<zzbwk> zzdxp2, zzdxp<zzbws> zzdxp3) {
        return new zzcac(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzbzz(this.zzfhl.get(), this.zzfqc.get(), this.zzfkx.get());
    }
}
