package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdjv extends zzdik<zzdhx, zzdns> {
    zzdjv(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        zzdns zzdns = (zzdns) obj;
        String zzawq = zzdns.zzawn().zzawq();
        return new zzdjt(zzdns.zzawn().zzawr(), zzdil.zzgu(zzawq).zzgw(zzawq));
    }
}
