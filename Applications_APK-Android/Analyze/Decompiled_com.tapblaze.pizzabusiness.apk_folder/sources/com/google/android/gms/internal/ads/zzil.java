package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzil implements zzic {
    private final /* synthetic */ zzij zzakx;

    private zzil(zzij zzij) {
        this.zzakx = zzij;
    }

    public final void zzr(int i) {
        this.zzakx.zzakq.zzs(i);
        zzij.zzr(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzij.zza(com.google.android.gms.internal.ads.zzij, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzij, int]
     candidates:
      com.google.android.gms.internal.ads.zzij.zza(com.google.android.gms.internal.ads.zzky, com.google.android.gms.internal.ads.zzgw):int
      com.google.android.gms.internal.ads.zzij.zza(int, java.lang.Object):void
      com.google.android.gms.internal.ads.zzij.zza(long, boolean):void
      com.google.android.gms.internal.ads.zzkw.zza(com.google.android.gms.internal.ads.zzky, com.google.android.gms.internal.ads.zzgw):int
      com.google.android.gms.internal.ads.zzkw.zza(long, boolean):void
      com.google.android.gms.internal.ads.zzgj.zza(int, java.lang.Object):void
      com.google.android.gms.internal.ads.zzgj.zza(long, boolean):void
      com.google.android.gms.internal.ads.zzgj.zza(com.google.android.gms.internal.ads.zzgw[], long):void
      com.google.android.gms.internal.ads.zzgm.zza(int, java.lang.Object):void
      com.google.android.gms.internal.ads.zzij.zza(com.google.android.gms.internal.ads.zzij, boolean):boolean */
    public final void zzed() {
        zzij.zzft();
        boolean unused = this.zzakx.zzakw = true;
    }

    public final void zzc(int i, long j, long j2) {
        this.zzakx.zzakq.zzb(i, j, j2);
        zzij.zza(i, j, j2);
    }
}
