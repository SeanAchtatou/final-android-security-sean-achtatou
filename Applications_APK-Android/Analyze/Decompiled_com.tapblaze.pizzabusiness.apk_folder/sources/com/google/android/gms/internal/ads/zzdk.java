package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdk implements Runnable {
    private final /* synthetic */ Context zzwf;

    zzdk(zzdi zzdi, Context context) {
        this.zzwf = context;
    }

    public final void run() {
        zzdi.zzvc.zzb(this.zzwf);
    }
}
