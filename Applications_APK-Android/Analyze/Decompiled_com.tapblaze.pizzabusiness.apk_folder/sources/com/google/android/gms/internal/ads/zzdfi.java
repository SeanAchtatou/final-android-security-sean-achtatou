package com.google.android.gms.internal.ads;

import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzdfi<K> extends zzdfb<K> {
    private final transient zzdeu<K> zzguj;
    private final transient zzdey<K, ?> zzguv;

    zzdfi(zzdey<K, ?> zzdey, zzdeu<K> zzdeu) {
        this.zzguv = zzdey;
        this.zzguj = zzdeu;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzarc() {
        return true;
    }

    public final zzdfp<K> zzaqx() {
        return (zzdfp) zzarb().iterator();
    }

    /* access modifiers changed from: package-private */
    public final int zza(Object[] objArr, int i) {
        return zzarb().zza(objArr, i);
    }

    public final zzdeu<K> zzarb() {
        return this.zzguj;
    }

    public final boolean contains(@NullableDecl Object obj) {
        return this.zzguv.get(obj) != null;
    }

    public final int size() {
        return this.zzguv.size();
    }

    public final /* synthetic */ Iterator iterator() {
        return iterator();
    }
}
