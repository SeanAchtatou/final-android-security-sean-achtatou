package com.google.android.gms.internal.p000firebaseperf;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhf  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhf extends zzhg<FieldDescriptorType, Object> {
    zzhf(int i) {
        super(i, null);
    }

    public final void zzgg() {
        if (!isImmutable()) {
            for (int i = 0; i < zziw(); i++) {
                Map.Entry zzau = zzau(i);
                if (((zzez) zzau.getKey()).zzhd()) {
                    zzau.setValue(Collections.unmodifiableList((List) zzau.getValue()));
                }
            }
            for (Map.Entry entry : zzix()) {
                if (((zzez) entry.getKey()).zzhd()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.zzgg();
    }
}
