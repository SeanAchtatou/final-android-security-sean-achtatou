package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgg  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzgg<K, V> {
    public final zzih zzsn;
    private final K zzso;
    public final zzih zzsp;
    private final V zzsq;

    public zzgg(zzih zzih, K k, zzih zzih2, V v) {
        this.zzsn = zzih;
        this.zzso = k;
        this.zzsp = zzih2;
        this.zzsq = v;
    }
}
