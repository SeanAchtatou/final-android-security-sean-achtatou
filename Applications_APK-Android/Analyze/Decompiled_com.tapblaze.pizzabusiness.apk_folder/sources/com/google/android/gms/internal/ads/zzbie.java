package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbie implements zzdxg<zzza> {
    private static final zzbie zzfbc = new zzbie();

    public static zzza zzafd() {
        return (zzza) zzdxm.zza(new zzza(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzafd();
    }
}
