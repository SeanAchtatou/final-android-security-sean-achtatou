package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzul extends zzwb {
    private final AppEventListener zzbkj;

    public zzul(AppEventListener appEventListener) {
        this.zzbkj = appEventListener;
    }

    public final void onAppEvent(String str, String str2) {
        this.zzbkj.onAppEvent(str, str2);
    }

    public final AppEventListener getAppEventListener() {
        return this.zzbkj;
    }
}
