package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgl  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public interface zzgl extends zzgn {
    int getSerializedSize();

    void writeTo(zzeo zzeo) throws IOException;

    zzeb zzgb();

    zzgo zzhi();
}
