package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdvx {

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt<zza, zzb> implements zzdtg {
        private static volatile zzdtn<zza> zzdz;
        /* access modifiers changed from: private */
        public static final zza zzhum;
        private int zzdl;
        private int zzhuf;
        private C0017zza zzhug;
        private zzdqk zzhuh = zzdqk.zzhhx;
        private zzdqk zzhui = zzdqk.zzhhx;
        private boolean zzhuj;
        private boolean zzhuk;
        private byte zzhul = 2;

        /* renamed from: com.google.android.gms.internal.ads.zzdvx$zza$zza  reason: collision with other inner class name */
        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class C0017zza extends zzdrt<C0017zza, C0018zza> implements zzdtg {
            private static volatile zzdtn<C0017zza> zzdz;
            /* access modifiers changed from: private */
            public static final C0017zza zzhur;
            private int zzdl;
            private String zzhun = "";
            private String zzhuo = "";
            private String zzhup = "";
            private int zzhuq;

            private C0017zza() {
            }

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zza$zza$zza  reason: collision with other inner class name */
            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class C0018zza extends zzdrt.zzb<C0017zza, C0018zza> implements zzdtg {
                private C0018zza() {
                    super(C0017zza.zzhur);
                }

                /* synthetic */ C0018zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new C0017zza();
                    case 2:
                        return new C0018zza(null);
                    case 3:
                        return zza(zzhur, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001\u0003\b\u0002\u0004\u0004\u0003", new Object[]{"zzdl", "zzhun", "zzhuo", "zzhup", "zzhuq"});
                    case 4:
                        return zzhur;
                    case 5:
                        zzdtn<C0017zza> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (C0017zza.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhur);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                C0017zza zza = new C0017zza();
                zzhur = zza;
                zzdrt.zza(C0017zza.class, zza);
            }
        }

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public enum zzc implements zzdry {
            SAFE(0),
            DANGEROUS(1),
            UNKNOWN(2),
            POTENTIALLY_UNWANTED(3),
            DANGEROUS_HOST(4);
            
            private static final zzdrx<zzc> zzen = new zzdwb();
            private final int value;

            public final int zzae() {
                return this.value;
            }

            public static zzc zzhd(int i) {
                if (i == 0) {
                    return SAFE;
                }
                if (i == 1) {
                    return DANGEROUS;
                }
                if (i == 2) {
                    return UNKNOWN;
                }
                if (i == 3) {
                    return POTENTIALLY_UNWANTED;
                }
                if (i != 4) {
                    return null;
                }
                return DANGEROUS_HOST;
            }

            public static zzdsa zzaf() {
                return zzdwa.zzew;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            private zzc(int i) {
                this.value = i;
            }
        }

        private zza() {
        }

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class zzb extends zzdrt.zzb<zza, zzb> implements zzdtg {
            private zzb() {
                super(zza.zzhum);
            }

            /* synthetic */ zzb(zzdvz zzdvz) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            int i2 = 0;
            switch (zzdvz.zzdk[i - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new zzb(null);
                case 3:
                    return zza(zzhum, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0001\u0001Ԍ\u0000\u0002\t\u0001\u0003\n\u0002\u0004\n\u0003\u0005\u0007\u0004\u0006\u0007\u0005", new Object[]{"zzdl", "zzhuf", zzc.zzaf(), "zzhug", "zzhuh", "zzhui", "zzhuj", "zzhuk"});
                case 4:
                    return zzhum;
                case 5:
                    zzdtn<zza> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zza.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzhum);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return Byte.valueOf(this.zzhul);
                case 7:
                    if (obj != null) {
                        i2 = 1;
                    }
                    this.zzhul = (byte) i2;
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zza zza = new zza();
            zzhum = zza;
            zzdrt.zza(zza.class, zza);
        }
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zzb extends zzdrt<zzb, zza> implements zzdtg {
        private static volatile zzdtn<zzb> zzdz;
        /* access modifiers changed from: private */
        public static final zzb zzhvn;
        private int zzbut;
        private int zzdl;
        private zzdqk zzhuh = zzdqk.zzhhx;
        private byte zzhul = 2;
        private String zzhuo = "";
        private int zzhuy;
        private String zzhuz = "";
        private String zzhva = "";
        private C0019zzb zzhvb;
        private zzdsb<zzh> zzhvc = zzazw();
        private String zzhvd = "";
        private zzf zzhve;
        private boolean zzhvf;
        private zzdsb<String> zzhvg = zzdrt.zzazw();
        private String zzhvh = "";
        private boolean zzhvi;
        private boolean zzhvj;
        private zzi zzhvk;
        private zzdsb<String> zzhvl = zzdrt.zzazw();
        private zzdsb<String> zzhvm = zzdrt.zzazw();

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class zzd extends zzdrt<zzd, zza> implements zzdtg {
            private static volatile zzdtn<zzd> zzdz;
            /* access modifiers changed from: private */
            public static final zzd zzhvx;
            private int zzdl;
            private byte zzhul = 2;
            private C0020zzb zzhvs;
            private zzdsb<zzc> zzhvt = zzazw();
            private zzdqk zzhvu = zzdqk.zzhhx;
            private zzdqk zzhvv = zzdqk.zzhhx;
            private int zzhvw;

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzd$zzb  reason: collision with other inner class name */
            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class C0020zzb extends zzdrt<C0020zzb, zza> implements zzdtg {
                private static volatile zzdtn<C0020zzb> zzdz;
                /* access modifiers changed from: private */
                public static final C0020zzb zzhwb;
                private int zzdl;
                private zzdqk zzhvy = zzdqk.zzhhx;
                private zzdqk zzhvz = zzdqk.zzhhx;
                private zzdqk zzhwa = zzdqk.zzhhx;

                private C0020zzb() {
                }

                /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzd$zzb$zza */
                /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
                public static final class zza extends zzdrt.zzb<C0020zzb, zza> implements zzdtg {
                    private zza() {
                        super(C0020zzb.zzhwb);
                    }

                    /* synthetic */ zza(zzdvz zzdvz) {
                        this();
                    }
                }

                /* access modifiers changed from: protected */
                public final Object zza(int i, Object obj, Object obj2) {
                    switch (zzdvz.zzdk[i - 1]) {
                        case 1:
                            return new C0020zzb();
                        case 2:
                            return new zza(null);
                        case 3:
                            return zza(zzhwb, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\n\u0000\u0002\n\u0001\u0003\n\u0002", new Object[]{"zzdl", "zzhvy", "zzhvz", "zzhwa"});
                        case 4:
                            return zzhwb;
                        case 5:
                            zzdtn<C0020zzb> zzdtn = zzdz;
                            if (zzdtn == null) {
                                synchronized (C0020zzb.class) {
                                    zzdtn = zzdz;
                                    if (zzdtn == null) {
                                        zzdtn = new zzdrt.zza<>(zzhwb);
                                        zzdz = zzdtn;
                                    }
                                }
                            }
                            return zzdtn;
                        case 6:
                            return (byte) 1;
                        case 7:
                            return null;
                        default:
                            throw new UnsupportedOperationException();
                    }
                }

                static {
                    C0020zzb zzb = new C0020zzb();
                    zzhwb = zzb;
                    zzdrt.zza(C0020zzb.class, zzb);
                }
            }

            private zzd() {
            }

            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class zza extends zzdrt.zzb<zzd, zza> implements zzdtg {
                private zza() {
                    super(zzd.zzhvx);
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                int i2 = 0;
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzd();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzhvx, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0001\u0001\t\u0000\u0002Л\u0003\n\u0001\u0004\n\u0002\u0005\u0004\u0003", new Object[]{"zzdl", "zzhvs", "zzhvt", zzc.class, "zzhvu", "zzhvv", "zzhvw"});
                    case 4:
                        return zzhvx;
                    case 5:
                        zzdtn<zzd> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzd.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhvx);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return Byte.valueOf(this.zzhul);
                    case 7:
                        if (obj != null) {
                            i2 = 1;
                        }
                        this.zzhul = (byte) i2;
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzd zzd = new zzd();
                zzhvx = zzd;
                zzdrt.zza(zzd.class, zzd);
            }
        }

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class zze extends zzdrt<zze, zza> implements zzdtg {
            private static volatile zzdtn<zze> zzdz;
            /* access modifiers changed from: private */
            public static final zze zzhwe;
            private int zzdl;
            private byte zzhul = 2;
            private zzdsb<zzc> zzhvt = zzazw();
            private zzdqk zzhvu = zzdqk.zzhhx;
            private zzdqk zzhvv = zzdqk.zzhhx;
            private int zzhvw;
            private C0021zzb zzhwc;
            private zzdqk zzhwd = zzdqk.zzhhx;

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zze$zzb  reason: collision with other inner class name */
            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class C0021zzb extends zzdrt<C0021zzb, zza> implements zzdtg {
                private static volatile zzdtn<C0021zzb> zzdz;
                /* access modifiers changed from: private */
                public static final C0021zzb zzhwh;
                private int zzdl;
                private zzdqk zzhwa = zzdqk.zzhhx;
                private int zzhwf;
                private zzdqk zzhwg = zzdqk.zzhhx;

                private C0021zzb() {
                }

                /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zze$zzb$zza */
                /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
                public static final class zza extends zzdrt.zzb<C0021zzb, zza> implements zzdtg {
                    private zza() {
                        super(C0021zzb.zzhwh);
                    }

                    /* synthetic */ zza(zzdvz zzdvz) {
                        this();
                    }
                }

                /* access modifiers changed from: protected */
                public final Object zza(int i, Object obj, Object obj2) {
                    switch (zzdvz.zzdk[i - 1]) {
                        case 1:
                            return new C0021zzb();
                        case 2:
                            return new zza(null);
                        case 3:
                            return zza(zzhwh, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0004\u0000\u0002\n\u0001\u0003\n\u0002", new Object[]{"zzdl", "zzhwf", "zzhwg", "zzhwa"});
                        case 4:
                            return zzhwh;
                        case 5:
                            zzdtn<C0021zzb> zzdtn = zzdz;
                            if (zzdtn == null) {
                                synchronized (C0021zzb.class) {
                                    zzdtn = zzdz;
                                    if (zzdtn == null) {
                                        zzdtn = new zzdrt.zza<>(zzhwh);
                                        zzdz = zzdtn;
                                    }
                                }
                            }
                            return zzdtn;
                        case 6:
                            return (byte) 1;
                        case 7:
                            return null;
                        default:
                            throw new UnsupportedOperationException();
                    }
                }

                static {
                    C0021zzb zzb = new C0021zzb();
                    zzhwh = zzb;
                    zzdrt.zza(C0021zzb.class, zzb);
                }
            }

            private zze() {
            }

            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class zza extends zzdrt.zzb<zze, zza> implements zzdtg {
                private zza() {
                    super(zze.zzhwe);
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                int i2 = 0;
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zze();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzhwe, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0001\u0001\u0001\t\u0000\u0002Л\u0003\n\u0001\u0004\n\u0002\u0005\u0004\u0003\u0006\n\u0004", new Object[]{"zzdl", "zzhwc", "zzhvt", zzc.class, "zzhvu", "zzhvv", "zzhvw", "zzhwd"});
                    case 4:
                        return zzhwe;
                    case 5:
                        zzdtn<zze> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zze.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhwe);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return Byte.valueOf(this.zzhul);
                    case 7:
                        if (obj != null) {
                            i2 = 1;
                        }
                        this.zzhul = (byte) i2;
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zze zze = new zze();
                zzhwe = zze;
                zzdrt.zza(zze.class, zze);
            }
        }

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class zzf extends zzdrt<zzf, zza> implements zzdtg {
            private static volatile zzdtn<zzf> zzdz;
            /* access modifiers changed from: private */
            public static final zzf zzhwk;
            private int zzbut;
            private int zzdl;
            private String zzhwi = "";
            private zzdqk zzhwj = zzdqk.zzhhx;

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzf$zzb  reason: collision with other inner class name */
            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public enum C0022zzb implements zzdry {
                TYPE_UNKNOWN(0),
                TYPE_CREATIVE(1);
                
                private static final zzdrx<C0022zzb> zzen = new zzdwc();
                private final int value;

                public final int zzae() {
                    return this.value;
                }

                public static C0022zzb zzhe(int i) {
                    if (i == 0) {
                        return TYPE_UNKNOWN;
                    }
                    if (i != 1) {
                        return null;
                    }
                    return TYPE_CREATIVE;
                }

                public static zzdsa zzaf() {
                    return zzdwd.zzew;
                }

                public final String toString() {
                    return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
                }

                private C0022zzb(int i) {
                    this.value = i;
                }
            }

            private zzf() {
            }

            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class zza extends zzdrt.zzb<zzf, zza> implements zzdtg {
                private zza() {
                    super(zzf.zzhwk);
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzf();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzhwk, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\f\u0000\u0002\b\u0001\u0003\n\u0002", new Object[]{"zzdl", "zzbut", C0022zzb.zzaf(), "zzhwi", "zzhwj"});
                    case 4:
                        return zzhwk;
                    case 5:
                        zzdtn<zzf> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzf.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhwk);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzf zzf = new zzf();
                zzhwk = zzf;
                zzdrt.zza(zzf.class, zzf);
            }
        }

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public enum zzg implements zzdry {
            UNKNOWN(0),
            URL_PHISHING(1),
            URL_MALWARE(2),
            URL_UNWANTED(3),
            CLIENT_SIDE_PHISHING_URL(4),
            CLIENT_SIDE_MALWARE_URL(5),
            DANGEROUS_DOWNLOAD_RECOVERY(6),
            DANGEROUS_DOWNLOAD_WARNING(7),
            OCTAGON_AD(8),
            OCTAGON_AD_SB_MATCH(9);
            
            private static final zzdrx<zzg> zzen = new zzdwf();
            private final int value;

            public final int zzae() {
                return this.value;
            }

            public static zzg zzhf(int i) {
                switch (i) {
                    case 0:
                        return UNKNOWN;
                    case 1:
                        return URL_PHISHING;
                    case 2:
                        return URL_MALWARE;
                    case 3:
                        return URL_UNWANTED;
                    case 4:
                        return CLIENT_SIDE_PHISHING_URL;
                    case 5:
                        return CLIENT_SIDE_MALWARE_URL;
                    case 6:
                        return DANGEROUS_DOWNLOAD_RECOVERY;
                    case 7:
                        return DANGEROUS_DOWNLOAD_WARNING;
                    case 8:
                        return OCTAGON_AD;
                    case 9:
                        return OCTAGON_AD_SB_MATCH;
                    default:
                        return null;
                }
            }

            public static zzdsa zzaf() {
                return zzdwe.zzew;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
            }

            private zzg(int i) {
                this.value = i;
            }
        }

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class zzh extends zzdrt<zzh, C0023zzb> implements zzdtg {
            private static volatile zzdtn<zzh> zzdz;
            /* access modifiers changed from: private */
            public static final zzh zzhxm;
            private int zzdl;
            private byte zzhul = 2;
            private String zzhuo = "";
            private int zzhxe;
            private zzd zzhxf;
            private zze zzhxg;
            private int zzhxh;
            private zzdrz zzhxi = zzazv();
            private String zzhxj = "";
            private int zzhxk;
            private zzdsb<String> zzhxl = zzdrt.zzazw();

            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public enum zza implements zzdry {
                AD_RESOURCE_UNKNOWN(0),
                AD_RESOURCE_CREATIVE(1),
                AD_RESOURCE_POST_CLICK(2),
                AD_RESOURCE_AUTO_CLICK_DESTINATION(3);
                
                private static final zzdrx<zza> zzen = new zzdwh();
                private final int value;

                public final int zzae() {
                    return this.value;
                }

                public static zza zzhg(int i) {
                    if (i == 0) {
                        return AD_RESOURCE_UNKNOWN;
                    }
                    if (i == 1) {
                        return AD_RESOURCE_CREATIVE;
                    }
                    if (i == 2) {
                        return AD_RESOURCE_POST_CLICK;
                    }
                    if (i != 3) {
                        return null;
                    }
                    return AD_RESOURCE_AUTO_CLICK_DESTINATION;
                }

                public static zzdsa zzaf() {
                    return zzdwg.zzew;
                }

                public final String toString() {
                    return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
                }

                private zza(int i) {
                    this.value = i;
                }
            }

            private zzh() {
            }

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzh$zzb  reason: collision with other inner class name */
            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class C0023zzb extends zzdrt.zzb<zzh, C0023zzb> implements zzdtg {
                private C0023zzb() {
                    super(zzh.zzhxm);
                }

                /* synthetic */ C0023zzb(zzdvz zzdvz) {
                    this();
                }
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                int i2 = 0;
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzh();
                    case 2:
                        return new C0023zzb(null);
                    case 3:
                        return zza(zzhxm, "\u0001\t\u0000\u0001\u0001\t\t\u0000\u0002\u0003\u0001Ԅ\u0000\u0002\b\u0001\u0003Љ\u0002\u0004Љ\u0003\u0005\u0004\u0004\u0006\u0016\u0007\b\u0005\b\f\u0006\t\u001a", new Object[]{"zzdl", "zzhxe", "zzhuo", "zzhxf", "zzhxg", "zzhxh", "zzhxi", "zzhxj", "zzhxk", zza.zzaf(), "zzhxl"});
                    case 4:
                        return zzhxm;
                    case 5:
                        zzdtn<zzh> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzh.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhxm);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return Byte.valueOf(this.zzhul);
                    case 7:
                        if (obj != null) {
                            i2 = 1;
                        }
                        this.zzhul = (byte) i2;
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzh zzh = new zzh();
                zzhxm = zzh;
                zzdrt.zza(zzh.class, zzh);
            }
        }

        private zzb() {
        }

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class zza extends zzdrt.zzb<zzb, zza> implements zzdtg {
            private zza() {
                super(zzb.zzhvn);
            }

            /* synthetic */ zza(zzdvz zzdvz) {
                this();
            }
        }

        /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzb  reason: collision with other inner class name */
        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class C0019zzb extends zzdrt<C0019zzb, zza> implements zzdtg {
            private static volatile zzdtn<C0019zzb> zzdz;
            /* access modifiers changed from: private */
            public static final C0019zzb zzhvp;
            private int zzdl;
            private String zzhvo = "";

            private C0019zzb() {
            }

            /* renamed from: com.google.android.gms.internal.ads.zzdvx$zzb$zzb$zza */
            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class zza extends zzdrt.zzb<C0019zzb, zza> implements zzdtg {
                private zza() {
                    super(C0019zzb.zzhvp);
                }

                public final zza zzhl(String str) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((C0019zzb) this.zzhmp).zzhm(str);
                    return this;
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            public final void zzhm(String str) {
                str.getClass();
                this.zzdl |= 1;
                this.zzhvo = str;
            }

            public static zza zzbcw() {
                return (zza) zzhvp.zzazt();
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new C0019zzb();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzhvp, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001\b\u0000", new Object[]{"zzdl", "zzhvo"});
                    case 4:
                        return zzhvp;
                    case 5:
                        zzdtn<C0019zzb> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (C0019zzb.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhvp);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                C0019zzb zzb = new C0019zzb();
                zzhvp = zzb;
                zzdrt.zza(C0019zzb.class, zzb);
            }
        }

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class zzc extends zzdrt<zzc, zza> implements zzdtg {
            private static volatile zzdtn<zzc> zzdz;
            /* access modifiers changed from: private */
            public static final zzc zzhvr;
            private int zzdl;
            private zzdqk zzhct = zzdqk.zzhhx;
            private byte zzhul = 2;
            private zzdqk zzhvq = zzdqk.zzhhx;

            private zzc() {
            }

            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class zza extends zzdrt.zzb<zzc, zza> implements zzdtg {
                private zza() {
                    super(zzc.zzhvr);
                }

                public final zza zzbk(zzdqk zzdqk) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzc) this.zzhmp).zzbm(zzdqk);
                    return this;
                }

                public final zza zzbl(zzdqk zzdqk) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzc) this.zzhmp).zzav(zzdqk);
                    return this;
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            public final void zzbm(zzdqk zzdqk) {
                zzdqk.getClass();
                this.zzdl |= 1;
                this.zzhvq = zzdqk;
            }

            /* access modifiers changed from: private */
            public final void zzav(zzdqk zzdqk) {
                zzdqk.getClass();
                this.zzdl |= 2;
                this.zzhct = zzdqk;
            }

            public static zza zzbcy() {
                return (zza) zzhvr.zzazt();
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                int i2 = 0;
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzc();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzhvr, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0001\u0001Ԋ\u0000\u0002\n\u0001", new Object[]{"zzdl", "zzhvq", "zzhct"});
                    case 4:
                        return zzhvr;
                    case 5:
                        zzdtn<zzc> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzc.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhvr);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return Byte.valueOf(this.zzhul);
                    case 7:
                        if (obj != null) {
                            i2 = 1;
                        }
                        this.zzhul = (byte) i2;
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzc zzc = new zzc();
                zzhvr = zzc;
                zzdrt.zza(zzc.class, zzc);
            }
        }

        /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
        public static final class zzi extends zzdrt<zzi, zza> implements zzdtg {
            private static volatile zzdtn<zzi> zzdz;
            /* access modifiers changed from: private */
            public static final zzi zzhxq;
            private int zzdl;
            private String zzhxn = "";
            private long zzhxo;
            private boolean zzhxp;

            private zzi() {
            }

            /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
            public static final class zza extends zzdrt.zzb<zzi, zza> implements zzdtg {
                private zza() {
                    super(zzi.zzhxq);
                }

                public final zza zzho(String str) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzi) this.zzhmp).zzhn(str);
                    return this;
                }

                public final zza zzfu(long j) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzi) this.zzhmp).zzft(j);
                    return this;
                }

                public final zza zzbs(boolean z) {
                    if (this.zzhmq) {
                        zzbab();
                        this.zzhmq = false;
                    }
                    ((zzi) this.zzhmp).zzbr(z);
                    return this;
                }

                /* synthetic */ zza(zzdvz zzdvz) {
                    this();
                }
            }

            /* access modifiers changed from: private */
            public final void zzhn(String str) {
                str.getClass();
                this.zzdl |= 1;
                this.zzhxn = str;
            }

            /* access modifiers changed from: private */
            public final void zzft(long j) {
                this.zzdl |= 2;
                this.zzhxo = j;
            }

            /* access modifiers changed from: private */
            public final void zzbr(boolean z) {
                this.zzdl |= 4;
                this.zzhxp = z;
            }

            public static zza zzbdg() {
                return (zza) zzhxq.zzazt();
            }

            /* access modifiers changed from: protected */
            public final Object zza(int i, Object obj, Object obj2) {
                switch (zzdvz.zzdk[i - 1]) {
                    case 1:
                        return new zzi();
                    case 2:
                        return new zza(null);
                    case 3:
                        return zza(zzhxq, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\b\u0000\u0002\u0002\u0001\u0003\u0007\u0002", new Object[]{"zzdl", "zzhxn", "zzhxo", "zzhxp"});
                    case 4:
                        return zzhxq;
                    case 5:
                        zzdtn<zzi> zzdtn = zzdz;
                        if (zzdtn == null) {
                            synchronized (zzi.class) {
                                zzdtn = zzdz;
                                if (zzdtn == null) {
                                    zzdtn = new zzdrt.zza<>(zzhxq);
                                    zzdz = zzdtn;
                                }
                            }
                        }
                        return zzdtn;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                zzi zzi = new zzi();
                zzhxq = zzi;
                zzdrt.zza(zzi.class, zzi);
            }
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i, Object obj, Object obj2) {
            int i2 = 0;
            switch (zzdvz.zzdk[i - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza(null);
                case 3:
                    return zza(zzhvn, "\u0001\u0012\u0000\u0001\u0001\u0015\u0012\u0000\u0004\u0001\u0001\b\u0002\u0002\b\u0003\u0003\b\u0004\u0004Л\u0005\u0007\b\u0006\u001a\u0007\b\t\b\u0007\n\t\u0007\u000b\n\f\u0000\u000b\f\u0001\f\t\u0005\r\b\u0006\u000e\t\u0007\u000f\n\f\u0011\t\r\u0014\u001a\u0015\u001a", new Object[]{"zzdl", "zzhuo", "zzhuz", "zzhva", "zzhvc", zzh.class, "zzhvf", "zzhvg", "zzhvh", "zzhvi", "zzhvj", "zzbut", zzg.zzaf(), "zzhuy", zza.zzc.zzaf(), "zzhvb", "zzhvd", "zzhve", "zzhuh", "zzhvk", "zzhvl", "zzhvm"});
                case 4:
                    return zzhvn;
                case 5:
                    zzdtn<zzb> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzb.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzhvn);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return Byte.valueOf(this.zzhul);
                case 7:
                    if (obj != null) {
                        i2 = 1;
                    }
                    this.zzhul = (byte) i2;
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zzb zzb = new zzb();
            zzhvn = zzb;
            zzdrt.zza(zzb.class, zzb);
        }
    }
}
