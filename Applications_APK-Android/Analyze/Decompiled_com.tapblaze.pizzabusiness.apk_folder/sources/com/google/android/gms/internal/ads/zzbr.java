package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzbr implements zzdsa {
    static final zzdsa zzew = new zzbr();

    private zzbr() {
    }

    public final boolean zzf(int i) {
        return zzbq.zze(i) != null;
    }
}
