package com.google.android.gms.ads.internal.overlay;

import android.graphics.Bitmap;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.ads.zzavo;
import com.google.android.gms.internal.ads.zzawb;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzl extends zzavo {
    final /* synthetic */ zzc zzdho;

    private zzl(zzc zzc) {
        this.zzdho = zzc;
    }

    public final void zztu() {
        Bitmap zza = zzq.zzlj().zza(Integer.valueOf(this.zzdho.zzdgn.zzdhx.zzblc));
        if (zza != null) {
            zzawb.zzdsr.post(new zzk(this, zzq.zzks().zza(this.zzdho.zzzk, zza, this.zzdho.zzdgn.zzdhx.zzbla, this.zzdho.zzdgn.zzdhx.zzblb)));
        }
    }
}
