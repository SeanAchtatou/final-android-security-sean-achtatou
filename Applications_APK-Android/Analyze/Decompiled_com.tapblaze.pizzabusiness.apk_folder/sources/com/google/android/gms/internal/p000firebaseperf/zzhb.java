package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhb  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
interface zzhb<T> {
    boolean equals(T t, T t2);

    int hashCode(T t);

    void zza(T t, zzin zzin) throws IOException;

    void zzd(T t, T t2);

    void zzf(T t);

    int zzm(T t);

    boolean zzn(T t);
}
