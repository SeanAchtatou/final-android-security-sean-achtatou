package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbiv implements zzdxg<zzbiw> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdda> zzepi;
    private final zzdxp<zzczt> zzfbo;
    private final zzdxp<zzczl> zzfbp;
    private final zzdxp<View> zzfbq;
    private final zzdxp<zzdq> zzfbr;

    private zzbiv(zzdxp<Context> zzdxp, zzdxp<zzczt> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzdda> zzdxp4, zzdxp<View> zzdxp5, zzdxp<zzdq> zzdxp6) {
        this.zzejv = zzdxp;
        this.zzfbo = zzdxp2;
        this.zzfbp = zzdxp3;
        this.zzepi = zzdxp4;
        this.zzfbq = zzdxp5;
        this.zzfbr = zzdxp6;
    }

    public static zzbiv zza(zzdxp<Context> zzdxp, zzdxp<zzczt> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzdda> zzdxp4, zzdxp<View> zzdxp5, zzdxp<zzdq> zzdxp6) {
        return new zzbiv(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6);
    }

    public final /* synthetic */ Object get() {
        return new zzbiw(this.zzejv.get(), this.zzfbo.get(), this.zzfbp.get(), this.zzepi.get(), this.zzfbq.get(), this.zzfbr.get());
    }
}
