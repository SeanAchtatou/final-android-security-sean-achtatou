package com.google.android.gms.internal.p000firebaseperf;

import android.os.Build;
import android.os.Process;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbb  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzbb {
    private static final long zzbg = TimeUnit.SECONDS.toMicros(1);
    private static zzbb zzbh = null;
    private ScheduledFuture zzbi = null;
    private final ScheduledExecutorService zzbj;
    private long zzbk;
    private final long zzbl;
    private final String zzbm;
    public final ConcurrentLinkedQueue<zzch> zzbn;

    private zzbb() {
        long j = -1;
        this.zzbk = -1;
        this.zzbn = new ConcurrentLinkedQueue<>();
        this.zzbj = Executors.newSingleThreadScheduledExecutor();
        String num = Integer.toString(Process.myPid());
        StringBuilder sb = new StringBuilder(String.valueOf(num).length() + 11);
        sb.append("/proc/");
        sb.append(num);
        sb.append("/stat");
        this.zzbm = sb.toString();
        this.zzbl = Build.VERSION.SDK_INT >= 21 ? Os.sysconf(OsConstants._SC_CLK_TCK) : j;
    }

    public static boolean zzi(long j) {
        return j <= 0;
    }

    public static zzbb zzbc() {
        if (zzbh == null) {
            zzbh = new zzbb();
        }
        return zzbh;
    }

    public final void zza(long j, zzbt zzbt) {
        long j2 = this.zzbl;
        if (j2 != -1 && j2 != 0 && !zzi(j)) {
            if (this.zzbi == null) {
                zzb(j, zzbt);
            } else if (this.zzbk != j) {
                zzbd();
                zzb(j, zzbt);
            }
        }
    }

    public final void zzbd() {
        ScheduledFuture scheduledFuture = this.zzbi;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
            this.zzbi = null;
            this.zzbk = -1;
        }
    }

    public final void zza(zzbt zzbt) {
        zzb(zzbt);
    }

    private final synchronized void zzb(long j, zzbt zzbt) {
        this.zzbk = j;
        try {
            this.zzbi = this.zzbj.scheduleAtFixedRate(new zzba(this, zzbt), 0, j, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("FirebasePerformance", valueOf.length() != 0 ? "Unable to start collecting Cpu Metrics: ".concat(valueOf) : new String("Unable to start collecting Cpu Metrics: "));
        }
    }

    private final synchronized void zzb(zzbt zzbt) {
        try {
            this.zzbj.schedule(new zzbd(this, zzbt), 0, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("FirebasePerformance", valueOf.length() != 0 ? "Unable to collect Cpu Metric: ".concat(valueOf) : new String("Unable to collect Cpu Metric: "));
        }
    }

    private final zzch zzc(zzbt zzbt) {
        BufferedReader bufferedReader;
        if (zzbt == null) {
            return null;
        }
        try {
            bufferedReader = new BufferedReader(new FileReader(this.zzbm));
            long zzdb = zzbt.zzdb();
            String[] split = bufferedReader.readLine().split(" ");
            zzch zzch = (zzch) ((zzfc) zzch.zzdq().zzy(zzdb).zzaa(zzh(Long.parseLong(split[14]) + Long.parseLong(split[16]))).zzz(zzh(Long.parseLong(split[13]) + Long.parseLong(split[15]))).zzhp());
            bufferedReader.close();
            return zzch;
        } catch (IOException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("FirebasePerformance", valueOf.length() != 0 ? "Unable to read 'proc/[pid]/stat' file: ".concat(valueOf) : new String("Unable to read 'proc/[pid]/stat' file: "));
            return null;
        } catch (ArrayIndexOutOfBoundsException | NullPointerException | NumberFormatException e2) {
            String valueOf2 = String.valueOf(e2.getMessage());
            Log.w("FirebasePerformance", valueOf2.length() != 0 ? "Unexpected '/proc/[pid]/stat' file format encountered: ".concat(valueOf2) : new String("Unexpected '/proc/[pid]/stat' file format encountered: "));
            return null;
        } catch (Throwable th) {
            zzaa.zza(th, th);
        }
        throw th;
    }

    private final long zzh(long j) {
        double d = (double) j;
        double d2 = (double) this.zzbl;
        Double.isNaN(d);
        Double.isNaN(d2);
        double d3 = d / d2;
        double d4 = (double) zzbg;
        Double.isNaN(d4);
        return Math.round(d3 * d4);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(zzbt zzbt) {
        zzch zzc = zzc(zzbt);
        if (zzc != null) {
            this.zzbn.add(zzc);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zze(zzbt zzbt) {
        zzch zzc = zzc(zzbt);
        if (zzc != null) {
            this.zzbn.add(zzc);
        }
    }
}
