package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbky implements zzdxg<Set<zzbsu<zzbqb>>> {
    private final zzdxp<zzbly> zzfdq;
    private final zzbkn zzfen;

    public zzbky(zzbkn zzbkn, zzdxp<zzbly> zzdxp) {
        this.zzfen = zzbkn;
        this.zzfdq = zzdxp;
    }

    public static Set<zzbsu<zzbqb>> zza(zzbkn zzbkn, zzbly zzbly) {
        return (Set) zzdxm.zza(Collections.singleton(new zzbsu(zzbly, zzazd.zzdwj)), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfen, this.zzfdq.get());
    }
}
