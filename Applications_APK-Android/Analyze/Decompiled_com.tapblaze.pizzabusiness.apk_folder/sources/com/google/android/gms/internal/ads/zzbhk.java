package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbhk implements zzczd {
    private final /* synthetic */ zzbgr zzerr;
    private zzdxp<Context> zzewd;
    private zzdxp<String> zzewe;
    private zzdxp<zzcxt<zzcbi, zzcbb>> zzezh;
    private zzdxp<zzcxz> zzezi;
    private zzdxp<zzczs> zzezj;
    private zzdxp<zzcyt> zzezk;
    private zzdxp<zzczf> zzezl;
    private zzdxp<zzcyz> zzezm;

    private zzbhk(zzbgr zzbgr, Context context, String str) {
        this.zzerr = zzbgr;
        this.zzewd = zzdxf.zzbe(context);
        this.zzezh = new zzcxx(this.zzewd, this.zzerr.zzelm, this.zzerr.zzeln);
        this.zzezi = zzdxd.zzan(new zzcyr(this.zzerr.zzelm));
        this.zzezj = zzdxd.zzan(zzczv.zzaop());
        this.zzezk = zzdxd.zzan(new zzcyw(this.zzewd, this.zzerr.zzejz, this.zzerr.zzejt, this.zzezh, this.zzezi, zzczz.zzaot(), this.zzezj));
        this.zzezl = zzdxd.zzan(new zzczg(this.zzezk, this.zzezi, this.zzezj));
        this.zzewe = zzdxf.zzbf(str);
        this.zzezm = zzdxd.zzan(new zzcza(this.zzewe, this.zzezk, this.zzezi, this.zzezj));
    }

    public final zzczf zzaep() {
        return this.zzezl.get();
    }

    public final zzcyz zzaeq() {
        return this.zzezm.get();
    }
}
