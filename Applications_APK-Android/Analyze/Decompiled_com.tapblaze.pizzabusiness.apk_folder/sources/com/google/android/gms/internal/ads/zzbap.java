package com.google.android.gms.internal.ads;

import com.ironsource.sdk.ISNAdView.ISNAdViewConstants;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbap implements Runnable {
    private final /* synthetic */ zzbai zzdyo;
    private final /* synthetic */ boolean zzdyr;

    zzbap(zzbai zzbai, boolean z) {
        this.zzdyo = zzbai;
        this.zzdyr = z;
    }

    public final void run() {
        this.zzdyo.zzd("windowVisibilityChanged", ISNAdViewConstants.IS_VISIBLE_KEY, String.valueOf(this.zzdyr));
    }
}
