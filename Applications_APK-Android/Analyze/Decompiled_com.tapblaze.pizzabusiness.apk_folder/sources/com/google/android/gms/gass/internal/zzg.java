package com.google.android.gms.gass.internal;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public interface zzg extends IInterface {
    zze zza(zzc zzc) throws RemoteException;

    zzo zza(zzm zzm) throws RemoteException;

    void zza(zzb zzb) throws RemoteException;
}
