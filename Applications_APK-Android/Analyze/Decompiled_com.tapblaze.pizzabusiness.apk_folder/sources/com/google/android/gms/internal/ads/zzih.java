package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzih implements zzhm {
    private int zzafo = -1;
    private ByteBuffer zzajh = zzaha;
    private int zzakk = -1;
    private int[] zzakl;
    private boolean zzakm;
    private int[] zzakn;
    private ByteBuffer zzako = zzaha;
    private boolean zzakp;

    public final int zzfa() {
        return 2;
    }

    public final void zzb(int[] iArr) {
        this.zzakl = iArr;
    }

    public final boolean zzb(int i, int i2, int i3) throws zzhp {
        boolean z = !Arrays.equals(this.zzakl, this.zzakn);
        this.zzakn = this.zzakl;
        if (this.zzakn == null) {
            this.zzakm = false;
            return z;
        } else if (i3 != 2) {
            throw new zzhp(i, i2, i3);
        } else if (!z && this.zzakk == i && this.zzafo == i2) {
            return false;
        } else {
            this.zzakk = i;
            this.zzafo = i2;
            this.zzakm = i2 != this.zzakn.length;
            int i4 = 0;
            while (true) {
                int[] iArr = this.zzakn;
                if (i4 >= iArr.length) {
                    return true;
                }
                int i5 = iArr[i4];
                if (i5 < i2) {
                    this.zzakm = (i5 != i4) | this.zzakm;
                    i4++;
                } else {
                    throw new zzhp(i, i2, i3);
                }
            }
        }
    }

    public final boolean isActive() {
        return this.zzakm;
    }

    public final int zzez() {
        int[] iArr = this.zzakn;
        return iArr == null ? this.zzafo : iArr.length;
    }

    public final void zzi(ByteBuffer byteBuffer) {
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        int length = (((limit - position) / (this.zzafo * 2)) * this.zzakn.length) << 1;
        if (this.zzako.capacity() < length) {
            this.zzako = ByteBuffer.allocateDirect(length).order(ByteOrder.nativeOrder());
        } else {
            this.zzako.clear();
        }
        while (position < limit) {
            for (int i : this.zzakn) {
                this.zzako.putShort(byteBuffer.getShort((i * 2) + position));
            }
            position += this.zzafo << 1;
        }
        byteBuffer.position(limit);
        this.zzako.flip();
        this.zzajh = this.zzako;
    }

    public final void zzfb() {
        this.zzakp = true;
    }

    public final ByteBuffer zzfc() {
        ByteBuffer byteBuffer = this.zzajh;
        this.zzajh = zzaha;
        return byteBuffer;
    }

    public final boolean zzeu() {
        return this.zzakp && this.zzajh == zzaha;
    }

    public final void flush() {
        this.zzajh = zzaha;
        this.zzakp = false;
    }

    public final void reset() {
        flush();
        this.zzako = zzaha;
        this.zzafo = -1;
        this.zzakk = -1;
        this.zzakn = null;
        this.zzakm = false;
    }
}
