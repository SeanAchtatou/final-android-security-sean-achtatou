package com.google.android.gms.internal.ads;

import android.os.IBinder;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzapi implements zzayw {
    static final zzayw zzbtz = new zzapi();

    private zzapi() {
    }

    public final Object apply(Object obj) {
        return zzaus.zzan((IBinder) obj);
    }
}
