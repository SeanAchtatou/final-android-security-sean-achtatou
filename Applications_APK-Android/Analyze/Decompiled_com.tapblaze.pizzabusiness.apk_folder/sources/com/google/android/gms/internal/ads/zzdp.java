package com.google.android.gms.internal.ads;

import android.view.MotionEvent;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdp implements Runnable {
    private final /* synthetic */ MotionEvent zzwm;

    zzdp(zzdi zzdi, MotionEvent motionEvent) {
        this.zzwm = motionEvent;
    }

    public final void run() {
        zzdi.zzvc.zza(this.zzwm);
    }
}
