package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.instream.InstreamAd;
import com.google.android.gms.common.internal.Preconditions;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzahq {
    private final zzvn zzabd;
    private final Context zzup;

    public zzahq(Context context, String str) {
        this((Context) Preconditions.checkNotNull(context, "context cannot be null"), zzve.zzov().zzb(context, str, new zzakz()));
    }

    private zzahq(Context context, zzvn zzvn) {
        this.zzup = context;
        this.zzabd = zzvn;
    }

    public final zzahq zza(InstreamAd.InstreamAdLoadCallback instreamAdLoadCallback) {
        try {
            this.zzabd.zza(new zzaho(instreamAdLoadCallback));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        return this;
    }

    public final zzahq zza(zzahl zzahl) {
        try {
            this.zzabd.zza(new zzagz(zzahl));
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
        return this;
    }

    public final zzahn zzry() {
        try {
            return new zzahn(this.zzup, this.zzabd.zzpd());
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            return null;
        }
    }
}
