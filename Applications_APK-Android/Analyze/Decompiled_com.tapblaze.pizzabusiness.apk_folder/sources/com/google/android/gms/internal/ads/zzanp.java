package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationInterstitialAd;
import com.google.android.gms.ads.mediation.MediationInterstitialAdCallback;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzanp implements MediationAdLoadCallback<MediationInterstitialAd, MediationInterstitialAdCallback> {
    private final /* synthetic */ zzamx zzden;
    private final /* synthetic */ zzali zzdeo;
    private final /* synthetic */ zzann zzdep;

    zzanp(zzann zzann, zzamx zzamx, zzali zzali) {
        this.zzdep = zzann;
        this.zzden = zzamx;
        this.zzdeo = zzali;
    }

    /* access modifiers changed from: private */
    /* renamed from: zza */
    public final MediationInterstitialAdCallback onSuccess(MediationInterstitialAd mediationInterstitialAd) {
        if (mediationInterstitialAd == null) {
            zzayu.zzez("Adapter incorrectly returned a null ad. The onFailure() callback should be called if an adapter fails to load an ad.");
            try {
                this.zzden.zzdl("Adapter returned null.");
                return null;
            } catch (RemoteException e) {
                zzayu.zzc("", e);
                return null;
            }
        } else {
            try {
                MediationInterstitialAd unused = this.zzdep.zzdel = mediationInterstitialAd;
                this.zzden.zztb();
            } catch (RemoteException e2) {
                zzayu.zzc("", e2);
            }
            return new zzant(this.zzdeo);
        }
    }

    public final void onFailure(String str) {
        try {
            this.zzden.zzdl(str);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
