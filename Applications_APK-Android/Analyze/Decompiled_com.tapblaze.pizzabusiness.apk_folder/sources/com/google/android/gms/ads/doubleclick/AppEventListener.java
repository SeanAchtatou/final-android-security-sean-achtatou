package com.google.android.gms.ads.doubleclick;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface AppEventListener {
    void onAppEvent(String str, String str2);
}
