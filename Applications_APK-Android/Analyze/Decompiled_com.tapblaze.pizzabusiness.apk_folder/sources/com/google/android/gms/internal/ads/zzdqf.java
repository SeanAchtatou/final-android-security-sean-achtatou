package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdqf {
    public int zzhhq;
    public long zzhhr;
    public Object zzhhs;
    public final zzdrg zzhht;

    zzdqf() {
        this.zzhht = zzdrg.zzazh();
    }

    zzdqf(zzdrg zzdrg) {
        if (zzdrg != null) {
            this.zzhht = zzdrg;
            return;
        }
        throw new NullPointerException();
    }
}
