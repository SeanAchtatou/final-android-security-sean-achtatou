package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

@Deprecated
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzdia<P> {
    zzdis<P> zzary() throws GeneralSecurityException;

    zzdid<P> zzb(String str, String str2, int i) throws GeneralSecurityException;
}
