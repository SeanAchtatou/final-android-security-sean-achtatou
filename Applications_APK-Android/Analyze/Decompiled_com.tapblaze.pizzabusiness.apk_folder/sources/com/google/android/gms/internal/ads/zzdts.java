package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdts<E> extends zzdqe<E> implements RandomAccess {
    private static final zzdts<Object> zzhpt;
    private int size;
    private E[] zzguu;

    public static <E> zzdts<E> zzbbp() {
        return zzhpt;
    }

    zzdts() {
        this(new Object[10], 0);
    }

    private zzdts(E[] eArr, int i) {
        this.zzguu = eArr;
        this.size = i;
    }

    public final boolean add(E e) {
        zzaxr();
        int i = this.size;
        E[] eArr = this.zzguu;
        if (i == eArr.length) {
            this.zzguu = Arrays.copyOf(eArr, ((i * 3) / 2) + 1);
        }
        E[] eArr2 = this.zzguu;
        int i2 = this.size;
        this.size = i2 + 1;
        eArr2[i2] = e;
        this.modCount++;
        return true;
    }

    public final void add(int i, E e) {
        int i2;
        zzaxr();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
        E[] eArr = this.zzguu;
        if (i2 < eArr.length) {
            System.arraycopy(eArr, i, eArr, i + 1, i2 - i);
        } else {
            E[] eArr2 = new Object[(((i2 * 3) / 2) + 1)];
            System.arraycopy(eArr, 0, eArr2, 0, i);
            System.arraycopy(this.zzguu, i, eArr2, i + 1, this.size - i);
            this.zzguu = eArr2;
        }
        this.zzguu[i] = e;
        this.size++;
        this.modCount++;
    }

    public final E get(int i) {
        zzfb(i);
        return this.zzguu[i];
    }

    public final E remove(int i) {
        zzaxr();
        zzfb(i);
        E[] eArr = this.zzguu;
        E e = eArr[i];
        int i2 = this.size;
        if (i < i2 - 1) {
            System.arraycopy(eArr, i + 1, eArr, i, (i2 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return e;
    }

    public final E set(int i, E e) {
        zzaxr();
        zzfb(i);
        E[] eArr = this.zzguu;
        E e2 = eArr[i];
        eArr[i] = e;
        this.modCount++;
        return e2;
    }

    public final int size() {
        return this.size;
    }

    private final void zzfb(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzfc(i));
        }
    }

    private final String zzfc(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final /* synthetic */ zzdsb zzfd(int i) {
        if (i >= this.size) {
            return new zzdts(Arrays.copyOf(this.zzguu, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    static {
        zzdts<Object> zzdts = new zzdts<>(new Object[0], 0);
        zzhpt = zzdts;
        zzdts.zzaxq();
    }
}
