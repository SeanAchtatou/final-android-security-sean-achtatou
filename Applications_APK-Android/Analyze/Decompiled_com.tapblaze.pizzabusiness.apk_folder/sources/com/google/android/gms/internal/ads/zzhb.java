package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzhb extends IllegalStateException {
    private final zzhg zzade;
    private final int zzaev;
    private final long zzaga;

    public zzhb(zzhg zzhg, int i, long j) {
        this.zzade = zzhg;
        this.zzaev = i;
        this.zzaga = j;
    }
}
