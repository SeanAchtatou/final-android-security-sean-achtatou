package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdpb;
import com.google.android.gms.security.ProviderInstaller;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;
import java.security.Signature;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdoy<T_WRAPPER extends zzdpb<T_ENGINE>, T_ENGINE> {
    private static final Logger logger = Logger.getLogger(zzdoy.class.getName());
    private static final List<Provider> zzhgf;
    public static final zzdoy<zzdpa, Cipher> zzhgg = new zzdoy<>(new zzdpa());
    public static final zzdoy<zzdpe, Mac> zzhgh = new zzdoy<>(new zzdpe());
    private static final zzdoy<zzdpg, Signature> zzhgi = new zzdoy<>(new zzdpg());
    private static final zzdoy<zzdph, MessageDigest> zzhgj = new zzdoy<>(new zzdph());
    public static final zzdoy<zzdpd, KeyAgreement> zzhgk = new zzdoy<>(new zzdpd());
    public static final zzdoy<zzdpf, KeyPairGenerator> zzhgl = new zzdoy<>(new zzdpf());
    public static final zzdoy<zzdpc, KeyFactory> zzhgm = new zzdoy<>(new zzdpc());
    private T_WRAPPER zzhgn;
    private List<Provider> zzhgo = zzhgf;
    private boolean zzhgp = true;

    private zzdoy(T_WRAPPER t_wrapper) {
        this.zzhgn = t_wrapper;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T_WRAPPER
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final T_ENGINE zzhd(java.lang.String r6) throws java.security.GeneralSecurityException {
        /*
            r5 = this;
            java.util.List<java.security.Provider> r0 = r5.zzhgo
            java.util.Iterator r0 = r0.iterator()
            r1 = 0
            r2 = r1
        L_0x0008:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x0020
            java.lang.Object r3 = r0.next()
            java.security.Provider r3 = (java.security.Provider) r3
            T_WRAPPER r4 = r5.zzhgn     // Catch:{ Exception -> 0x001b }
            java.lang.Object r6 = r4.zza(r6, r3)     // Catch:{ Exception -> 0x001b }
            return r6
        L_0x001b:
            r3 = move-exception
            if (r2 != 0) goto L_0x0008
            r2 = r3
            goto L_0x0008
        L_0x0020:
            boolean r0 = r5.zzhgp
            if (r0 == 0) goto L_0x002b
            T_WRAPPER r0 = r5.zzhgn
            java.lang.Object r6 = r0.zza(r6, r1)
            return r6
        L_0x002b:
            java.security.GeneralSecurityException r6 = new java.security.GeneralSecurityException
            java.lang.String r0 = "No good Provider found."
            r6.<init>(r0, r2)
            goto L_0x0034
        L_0x0033:
            throw r6
        L_0x0034:
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdoy.zzhd(java.lang.String):java.lang.Object");
    }

    static {
        if (zzdpp.zzaxh()) {
            String[] strArr = {ProviderInstaller.PROVIDER_NAME, "AndroidOpenSSL"};
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 2; i++) {
                String str = strArr[i];
                Provider provider = Security.getProvider(str);
                if (provider != null) {
                    arrayList.add(provider);
                } else {
                    logger.logp(Level.INFO, "com.google.crypto.tink.subtle.EngineFactory", "toProviderList", String.format("Provider %s not available", str));
                }
            }
            zzhgf = arrayList;
        } else {
            zzhgf = new ArrayList();
        }
    }
}
