package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzctf implements zzcub<zzcty<Bundle>> {
    private final String zzfhg;
    private final Context zzup;

    zzctf(Context context, String str) {
        this.zzup = context;
        this.zzfhg = str;
    }

    public final zzdhe<zzcty<Bundle>> zzanc() {
        zzcte zzcte;
        if (this.zzfhg == null) {
            zzcte = null;
        } else {
            zzcte = new zzcte(this);
        }
        return zzdgs.zzaj(zzcte);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzo(Bundle bundle) {
        bundle.putString("rewarded_sku_package", this.zzup.getPackageName());
    }
}
