package com.google.android.gms.internal.ads;

import java.util.concurrent.ThreadFactory;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzop implements ThreadFactory {
    private final /* synthetic */ String zzbgu;

    zzop(String str) {
        this.zzbgu = str;
    }

    public final Thread newThread(Runnable runnable) {
        return new Thread(runnable, this.zzbgu);
    }
}
