package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdao {
    public final zzavp zzdpz;
    public final zzavu zzdrk;
    public final Context zzyv;

    private zzdao(Context context, zzavu zzavu, zzavp zzavp) {
        this.zzyv = context;
        this.zzdrk = zzavu;
        this.zzdpz = zzavp;
    }
}
