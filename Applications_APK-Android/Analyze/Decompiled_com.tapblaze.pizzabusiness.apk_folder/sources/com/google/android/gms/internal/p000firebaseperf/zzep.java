package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzep  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzep extends zzen {
    private final byte[] buffer;
    private final boolean immutable;
    private int limit;
    private int pos;
    private int zznh;
    private int zzni;
    private int zznj;

    private zzep(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.zznj = Integer.MAX_VALUE;
        this.buffer = bArr;
        this.limit = i2 + i;
        this.pos = i;
        this.zzni = this.pos;
        this.immutable = z;
    }

    public final int zzt(int i) throws zzfl {
        if (i >= 0) {
            int zzgq = i + zzgq();
            int i2 = this.zznj;
            if (zzgq <= i2) {
                this.zznj = zzgq;
                this.limit += this.zznh;
                int i3 = this.limit;
                int i4 = i3 - this.zzni;
                int i5 = this.zznj;
                if (i4 > i5) {
                    this.zznh = i4 - i5;
                    this.limit = i3 - this.zznh;
                } else {
                    this.zznh = 0;
                }
                return i2;
            }
            throw new zzfl("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
        }
        throw new zzfl("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    public final int zzgq() {
        return this.pos - this.zzni;
    }
}
