package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgw  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzgw {
    private static final zzgu zztm = zzim();
    private static final zzgu zztn = new zzgt();

    static zzgu zzik() {
        return zztm;
    }

    static zzgu zzil() {
        return zztn;
    }

    private static zzgu zzim() {
        try {
            return (zzgu) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
