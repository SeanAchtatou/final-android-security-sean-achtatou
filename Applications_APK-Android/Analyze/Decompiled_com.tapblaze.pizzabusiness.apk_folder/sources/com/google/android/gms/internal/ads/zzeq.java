package com.google.android.gms.internal.ads;

import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzeq extends zzcj<Integer, Long> {
    public Long zzyj;
    public Long zzyk;
    public Long zzyl;
    public Long zzym;
    public Long zzyn;
    public Long zzyo;
    public Long zzyp;
    public Long zzyq;
    public Long zzyr;
    public Long zzys;
    public Long zzyt;

    public zzeq() {
    }

    public zzeq(String str) {
        zzap(str);
    }

    /* access modifiers changed from: protected */
    public final void zzap(String str) {
        HashMap zzaq = zzaq(str);
        if (zzaq != null) {
            this.zzyj = (Long) zzaq.get(0);
            this.zzyk = (Long) zzaq.get(1);
            this.zzyl = (Long) zzaq.get(2);
            this.zzym = (Long) zzaq.get(3);
            this.zzyn = (Long) zzaq.get(4);
            this.zzyo = (Long) zzaq.get(5);
            this.zzyp = (Long) zzaq.get(6);
            this.zzyq = (Long) zzaq.get(7);
            this.zzyr = (Long) zzaq.get(8);
            this.zzys = (Long) zzaq.get(9);
            this.zzyt = (Long) zzaq.get(10);
        }
    }

    /* access modifiers changed from: protected */
    public final HashMap<Integer, Long> zzbk() {
        HashMap<Integer, Long> hashMap = new HashMap<>();
        hashMap.put(0, this.zzyj);
        hashMap.put(1, this.zzyk);
        hashMap.put(2, this.zzyl);
        hashMap.put(3, this.zzym);
        hashMap.put(4, this.zzyn);
        hashMap.put(5, this.zzyo);
        hashMap.put(6, this.zzyp);
        hashMap.put(7, this.zzyq);
        hashMap.put(8, this.zzyr);
        hashMap.put(9, this.zzys);
        hashMap.put(10, this.zzyt);
        return hashMap;
    }
}
