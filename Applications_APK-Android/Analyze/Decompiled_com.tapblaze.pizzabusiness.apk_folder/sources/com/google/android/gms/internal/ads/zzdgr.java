package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzdgr<V> extends zzdgo<V> implements zzdhe<V> {
    protected zzdgr() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzarv */
    public abstract zzdhe<? extends V> zzaru();

    public void addListener(Runnable runnable, Executor executor) {
        zzaru().addListener(runnable, executor);
    }
}
