package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzasj extends zzgb implements zzasg {
    public zzasj() {
        super("com.google.android.gms.ads.internal.rewarded.client.IRewardedAd");
    }

    public static zzasg zzam(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.rewarded.client.IRewardedAd");
        if (queryLocalInterface instanceof zzasg) {
            return (zzasg) queryLocalInterface;
        }
        return new zzasi(iBinder);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r5v1 */
    /* JADX WARN: Type inference failed for: r5v2, types: [com.google.android.gms.internal.ads.zzaso] */
    /* JADX WARN: Type inference failed for: r5v7, types: [com.google.android.gms.internal.ads.zzasl] */
    /* JADX WARN: Type inference failed for: r5v12, types: [com.google.android.gms.internal.ads.zzast] */
    /* JADX WARN: Type inference failed for: r5v17 */
    /* JADX WARN: Type inference failed for: r5v18 */
    /* JADX WARN: Type inference failed for: r5v19 */
    /* JADX WARN: Type inference failed for: r5v20 */
    /* JADX WARN: Type inference failed for: r5v21 */
    /* JADX WARN: Type inference failed for: r5v22 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(int r2, android.os.Parcel r3, android.os.Parcel r4, int r5) throws android.os.RemoteException {
        /*
            r1 = this;
            r5 = 0
            switch(r2) {
                case 1: goto L_0x00c6;
                case 2: goto L_0x00a5;
                case 3: goto L_0x009a;
                case 4: goto L_0x008f;
                case 5: goto L_0x0080;
                case 6: goto L_0x005e;
                case 7: goto L_0x004e;
                case 8: goto L_0x003e;
                case 9: goto L_0x0032;
                case 10: goto L_0x001e;
                case 11: goto L_0x0012;
                case 12: goto L_0x0006;
                default: goto L_0x0004;
            }
        L_0x0004:
            r2 = 0
            return r2
        L_0x0006:
            com.google.android.gms.internal.ads.zzxa r2 = r1.zzkb()
            r4.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza(r4, r2)
            goto L_0x00ed
        L_0x0012:
            com.google.android.gms.internal.ads.zzasf r2 = r1.zzpz()
            r4.writeNoException()
            com.google.android.gms.internal.ads.zzge.zza(r4, r2)
            goto L_0x00ed
        L_0x001e:
            android.os.IBinder r2 = r3.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.Stub.asInterface(r2)
            boolean r3 = com.google.android.gms.internal.ads.zzge.zza(r3)
            r1.zza(r2, r3)
            r4.writeNoException()
            goto L_0x00ed
        L_0x0032:
            android.os.Bundle r2 = r1.getAdMetadata()
            r4.writeNoException()
            com.google.android.gms.internal.ads.zzge.zzb(r4, r2)
            goto L_0x00ed
        L_0x003e:
            android.os.IBinder r2 = r3.readStrongBinder()
            com.google.android.gms.internal.ads.zzwv r2 = com.google.android.gms.internal.ads.zzwy.zzh(r2)
            r1.zza(r2)
            r4.writeNoException()
            goto L_0x00ed
        L_0x004e:
            android.os.Parcelable$Creator<com.google.android.gms.internal.ads.zzatb> r2 = com.google.android.gms.internal.ads.zzatb.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.ads.zzge.zza(r3, r2)
            com.google.android.gms.internal.ads.zzatb r2 = (com.google.android.gms.internal.ads.zzatb) r2
            r1.zza(r2)
            r4.writeNoException()
            goto L_0x00ed
        L_0x005e:
            android.os.IBinder r2 = r3.readStrongBinder()
            if (r2 != 0) goto L_0x0065
            goto L_0x0078
        L_0x0065:
            java.lang.String r3 = "com.google.android.gms.ads.internal.rewarded.client.IRewardedAdSkuListener"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)
            boolean r5 = r3 instanceof com.google.android.gms.internal.ads.zzast
            if (r5 == 0) goto L_0x0073
            r5 = r3
            com.google.android.gms.internal.ads.zzast r5 = (com.google.android.gms.internal.ads.zzast) r5
            goto L_0x0078
        L_0x0073:
            com.google.android.gms.internal.ads.zzass r5 = new com.google.android.gms.internal.ads.zzass
            r5.<init>(r2)
        L_0x0078:
            r1.zza(r5)
            r4.writeNoException()
            goto L_0x00ed
        L_0x0080:
            android.os.IBinder r2 = r3.readStrongBinder()
            com.google.android.gms.dynamic.IObjectWrapper r2 = com.google.android.gms.dynamic.IObjectWrapper.Stub.asInterface(r2)
            r1.zzh(r2)
            r4.writeNoException()
            goto L_0x00ed
        L_0x008f:
            java.lang.String r2 = r1.getMediationAdapterClassName()
            r4.writeNoException()
            r4.writeString(r2)
            goto L_0x00ed
        L_0x009a:
            boolean r2 = r1.isLoaded()
            r4.writeNoException()
            com.google.android.gms.internal.ads.zzge.writeBoolean(r4, r2)
            goto L_0x00ed
        L_0x00a5:
            android.os.IBinder r2 = r3.readStrongBinder()
            if (r2 != 0) goto L_0x00ac
            goto L_0x00bf
        L_0x00ac:
            java.lang.String r3 = "com.google.android.gms.ads.internal.rewarded.client.IRewardedAdCallback"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)
            boolean r5 = r3 instanceof com.google.android.gms.internal.ads.zzasl
            if (r5 == 0) goto L_0x00ba
            r5 = r3
            com.google.android.gms.internal.ads.zzasl r5 = (com.google.android.gms.internal.ads.zzasl) r5
            goto L_0x00bf
        L_0x00ba:
            com.google.android.gms.internal.ads.zzasn r5 = new com.google.android.gms.internal.ads.zzasn
            r5.<init>(r2)
        L_0x00bf:
            r1.zza(r5)
            r4.writeNoException()
            goto L_0x00ed
        L_0x00c6:
            android.os.Parcelable$Creator<com.google.android.gms.internal.ads.zzug> r2 = com.google.android.gms.internal.ads.zzug.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.internal.ads.zzge.zza(r3, r2)
            com.google.android.gms.internal.ads.zzug r2 = (com.google.android.gms.internal.ads.zzug) r2
            android.os.IBinder r3 = r3.readStrongBinder()
            if (r3 != 0) goto L_0x00d5
            goto L_0x00e7
        L_0x00d5:
            java.lang.String r5 = "com.google.android.gms.ads.internal.rewarded.client.IRewardedAdLoadCallback"
            android.os.IInterface r5 = r3.queryLocalInterface(r5)
            boolean r0 = r5 instanceof com.google.android.gms.internal.ads.zzaso
            if (r0 == 0) goto L_0x00e2
            com.google.android.gms.internal.ads.zzaso r5 = (com.google.android.gms.internal.ads.zzaso) r5
            goto L_0x00e7
        L_0x00e2:
            com.google.android.gms.internal.ads.zzasq r5 = new com.google.android.gms.internal.ads.zzasq
            r5.<init>(r3)
        L_0x00e7:
            r1.zza(r2, r5)
            r4.writeNoException()
        L_0x00ed:
            r2 = 1
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzasj.zza(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
