package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbca {
    void zza(String str, Exception exc);

    void zzb(boolean z, long j);

    void zzda(int i);

    void zzn(int i, int i2);
}
