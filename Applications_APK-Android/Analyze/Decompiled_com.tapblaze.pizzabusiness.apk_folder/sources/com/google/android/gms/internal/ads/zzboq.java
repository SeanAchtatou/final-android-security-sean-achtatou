package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzboq extends zzbrl<zzty> implements zzty {
    public zzboq(Set<zzbsu<zzty>> set) {
        super(set);
    }

    public final void onAdClicked() {
        zza(zzbot.zzfhp);
    }
}
