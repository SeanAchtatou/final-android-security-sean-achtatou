package com.google.android.gms.internal.ads;

import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzmr {
    public static final zzmr zzbdd = new zzmr(new zzms[0]);
    public final int length;
    private int zzafx;
    private final zzms[] zzbde;

    public zzmr(zzms... zzmsArr) {
        this.zzbde = zzmsArr;
        this.length = zzmsArr.length;
    }

    public final zzms zzav(int i) {
        return this.zzbde[i];
    }

    public final int zza(zzms zzms) {
        for (int i = 0; i < this.length; i++) {
            if (this.zzbde[i] == zzms) {
                return i;
            }
        }
        return -1;
    }

    public final int hashCode() {
        if (this.zzafx == 0) {
            this.zzafx = Arrays.hashCode(this.zzbde);
        }
        return this.zzafx;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            zzmr zzmr = (zzmr) obj;
            return this.length == zzmr.length && Arrays.equals(this.zzbde, zzmr.zzbde);
        }
    }
}
