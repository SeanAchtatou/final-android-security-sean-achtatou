package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzjs {
    /* access modifiers changed from: private */
    public final int zzanj;
    /* access modifiers changed from: private */
    public final long zzanl;

    private zzjs(int i, long j) {
        this.zzanj = i;
        this.zzanl = j;
    }
}
