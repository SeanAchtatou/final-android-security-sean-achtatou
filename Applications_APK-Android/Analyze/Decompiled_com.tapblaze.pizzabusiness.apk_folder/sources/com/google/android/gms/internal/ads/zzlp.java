package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.util.SparseArray;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzlp implements zzjf, zzlz, zzml, zznv<zzlw> {
    private final Uri uri;
    private final zzddu zzact;
    private boolean zzadv;
    /* access modifiers changed from: private */
    public final zzddu zzaee;
    /* access modifiers changed from: private */
    public boolean zzaek;
    private long zzagj;
    private final zznl zzamu;
    private final int zzazs;
    /* access modifiers changed from: private */
    public final zzma zzazt;
    private final zzme zzazu;
    private final zznj zzazv;
    /* access modifiers changed from: private */
    public final String zzazw;
    /* access modifiers changed from: private */
    public final long zzazx;
    private final zznw zzazy = new zznw("Loader:ExtractorMediaPeriod");
    private final zzlv zzazz;
    private final zzoe zzbaa;
    private final Runnable zzbab;
    /* access modifiers changed from: private */
    public final Runnable zzbac;
    /* access modifiers changed from: private */
    public final SparseArray<zzmj> zzbad;
    /* access modifiers changed from: private */
    public zzmc zzbae;
    private zzjm zzbaf;
    private boolean zzbag;
    private boolean zzbah;
    private boolean zzbai;
    private int zzbaj;
    private zzmr zzbak;
    private boolean[] zzbal;
    private boolean[] zzbam;
    private boolean zzban;
    private long zzbao;
    private long zzbap;
    private int zzbaq;
    private boolean zzbar;
    private long zzce;

    public zzlp(Uri uri2, zznl zznl, zzjd[] zzjdArr, int i, zzddu zzddu, zzma zzma, zzme zzme, zznj zznj, String str, int i2) {
        this.uri = uri2;
        this.zzamu = zznl;
        this.zzazs = i;
        this.zzact = zzddu;
        this.zzazt = zzma;
        this.zzazu = zzme;
        this.zzazv = zznj;
        this.zzazw = str;
        this.zzazx = (long) i2;
        this.zzazz = new zzlv(zzjdArr, this);
        this.zzbaa = new zzoe();
        this.zzbab = new zzls(this);
        this.zzbac = new zzlr(this);
        this.zzaee = new zzddu();
        this.zzbap = -9223372036854775807L;
        this.zzbad = new SparseArray<>();
        this.zzce = -1;
    }

    public final void zzee(long j) {
    }

    public final void release() {
        this.zzazy.zza(new zzlu(this, this.zzazz));
        this.zzaee.removeCallbacksAndMessages(null);
        this.zzaek = true;
    }

    public final void zza(zzmc zzmc, long j) {
        this.zzbae = zzmc;
        this.zzbaa.open();
        startLoading();
    }

    public final void zzhf() throws IOException {
        this.zzazy.zzbc(Integer.MIN_VALUE);
    }

    public final zzmr zzhg() {
        return this.zzbak;
    }

    public final long zza(zzne[] zzneArr, boolean[] zArr, zzmo[] zzmoArr, boolean[] zArr2, long j) {
        zzoc.checkState(this.zzadv);
        for (int i = 0; i < zzneArr.length; i++) {
            if (zzmoArr[i] != null && (zzneArr[i] == null || !zArr[i])) {
                int zza = ((zzly) zzmoArr[i]).track;
                zzoc.checkState(this.zzbal[zza]);
                this.zzbaj--;
                this.zzbal[zza] = false;
                this.zzbad.valueAt(zza).disable();
                zzmoArr[i] = null;
            }
        }
        boolean z = false;
        for (int i2 = 0; i2 < zzneArr.length; i2++) {
            if (zzmoArr[i2] == null && zzneArr[i2] != null) {
                zzne zzne = zzneArr[i2];
                zzoc.checkState(zzne.length() == 1);
                zzoc.checkState(zzne.zzax(0) == 0);
                int zza2 = this.zzbak.zza(zzne.zzid());
                zzoc.checkState(!this.zzbal[zza2]);
                this.zzbaj++;
                this.zzbal[zza2] = true;
                zzmoArr[i2] = new zzly(this, zza2);
                zArr2[i2] = true;
                z = true;
            }
        }
        if (!this.zzbah) {
            int size = this.zzbad.size();
            for (int i3 = 0; i3 < size; i3++) {
                if (!this.zzbal[i3]) {
                    this.zzbad.valueAt(i3).disable();
                }
            }
        }
        if (this.zzbaj == 0) {
            this.zzbai = false;
            if (this.zzazy.isLoading()) {
                this.zzazy.zzil();
            }
        } else if (!this.zzbah ? j != 0 : z) {
            j = zzeg(j);
            for (int i4 = 0; i4 < zzmoArr.length; i4++) {
                if (zzmoArr[i4] != null) {
                    zArr2[i4] = true;
                }
            }
        }
        this.zzbah = true;
        return j;
    }

    public final boolean zzef(long j) {
        if (this.zzbar) {
            return false;
        }
        if (this.zzadv && this.zzbaj == 0) {
            return false;
        }
        boolean open = this.zzbaa.open();
        if (this.zzazy.isLoading()) {
            return open;
        }
        startLoading();
        return true;
    }

    public final long zzhh() {
        if (this.zzbaj == 0) {
            return Long.MIN_VALUE;
        }
        return zzhj();
    }

    public final long zzhi() {
        if (!this.zzbai) {
            return -9223372036854775807L;
        }
        this.zzbai = false;
        return this.zzbao;
    }

    public final long zzhj() {
        long j;
        if (this.zzbar) {
            return Long.MIN_VALUE;
        }
        if (zzho()) {
            return this.zzbap;
        }
        if (this.zzban) {
            j = Long.MAX_VALUE;
            int size = this.zzbad.size();
            for (int i = 0; i < size; i++) {
                if (this.zzbam[i]) {
                    j = Math.min(j, this.zzbad.valueAt(i).zzhn());
                }
            }
        } else {
            j = zzhn();
        }
        return j == Long.MIN_VALUE ? this.zzbao : j;
    }

    public final long zzeg(long j) {
        if (!this.zzbaf.zzgh()) {
            j = 0;
        }
        this.zzbao = j;
        int size = this.zzbad.size();
        boolean z = !zzho();
        int i = 0;
        while (z && i < size) {
            if (this.zzbal[i]) {
                z = this.zzbad.valueAt(i).zze(j, false);
            }
            i++;
        }
        if (!z) {
            this.zzbap = j;
            this.zzbar = false;
            if (this.zzazy.isLoading()) {
                this.zzazy.zzil();
            } else {
                for (int i2 = 0; i2 < size; i2++) {
                    this.zzbad.valueAt(i2).zzk(this.zzbal[i2]);
                }
            }
        }
        this.zzbai = false;
        return j;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzat(int i) {
        if (!this.zzbar) {
            return !zzho() && this.zzbad.valueAt(i).zzhw();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void zzhk() throws IOException {
        this.zzazy.zzbc(Integer.MIN_VALUE);
    }

    /* access modifiers changed from: package-private */
    public final int zza(int i, zzgy zzgy, zzis zzis, boolean z) {
        if (this.zzbai || zzho()) {
            return -3;
        }
        return this.zzbad.valueAt(i).zza(zzgy, zzis, z, this.zzbar, this.zzbao);
    }

    /* access modifiers changed from: package-private */
    public final void zzd(int i, long j) {
        zzmj valueAt = this.zzbad.valueAt(i);
        if (!this.zzbar || j <= valueAt.zzhn()) {
            valueAt.zze(j, true);
        } else {
            valueAt.zzhz();
        }
    }

    public final zzjo zzc(int i, int i2) {
        zzmj zzmj = this.zzbad.get(i);
        if (zzmj != null) {
            return zzmj;
        }
        zzmj zzmj2 = new zzmj(this.zzazv);
        zzmj2.zza(this);
        this.zzbad.put(i, zzmj2);
        return zzmj2;
    }

    public final void zzgj() {
        this.zzbag = true;
        this.zzaee.post(this.zzbab);
    }

    public final void zza(zzjm zzjm) {
        this.zzbaf = zzjm;
        this.zzaee.post(this.zzbab);
    }

    public final void zzf(zzgw zzgw) {
        this.zzaee.post(this.zzbab);
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [int, boolean], vars: [r4v0 ?, r4v3 ?, r4v5 ?]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:102)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:78)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:69)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:51)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:32)
        */
    public final void zzhl() {
        /*
            r8 = this;
            boolean r0 = r8.zzaek
            if (r0 != 0) goto L_0x009d
            boolean r0 = r8.zzadv
            if (r0 != 0) goto L_0x009d
            com.google.android.gms.internal.ads.zzjm r0 = r8.zzbaf
            if (r0 == 0) goto L_0x009d
            boolean r0 = r8.zzbag
            if (r0 != 0) goto L_0x0012
            goto L_0x009d
        L_0x0012:
            android.util.SparseArray<com.google.android.gms.internal.ads.zzmj> r0 = r8.zzbad
            int r0 = r0.size()
            r1 = 0
            r2 = 0
        L_0x001a:
            if (r2 >= r0) goto L_0x002e
            android.util.SparseArray<com.google.android.gms.internal.ads.zzmj> r3 = r8.zzbad
            java.lang.Object r3 = r3.valueAt(r2)
            com.google.android.gms.internal.ads.zzmj r3 = (com.google.android.gms.internal.ads.zzmj) r3
            com.google.android.gms.internal.ads.zzgw r3 = r3.zzhx()
            if (r3 != 0) goto L_0x002b
            return
        L_0x002b:
            int r2 = r2 + 1
            goto L_0x001a
        L_0x002e:
            com.google.android.gms.internal.ads.zzoe r2 = r8.zzbaa
            r2.zzim()
            com.google.android.gms.internal.ads.zzms[] r2 = new com.google.android.gms.internal.ads.zzms[r0]
            boolean[] r3 = new boolean[r0]
            r8.zzbam = r3
            boolean[] r3 = new boolean[r0]
            r8.zzbal = r3
            com.google.android.gms.internal.ads.zzjm r3 = r8.zzbaf
            long r3 = r3.getDurationUs()
            r8.zzagj = r3
            r3 = 0
        L_0x0046:
            r4 = 1
            if (r3 >= r0) goto L_0x007c
            android.util.SparseArray<com.google.android.gms.internal.ads.zzmj> r5 = r8.zzbad
            java.lang.Object r5 = r5.valueAt(r3)
            com.google.android.gms.internal.ads.zzmj r5 = (com.google.android.gms.internal.ads.zzmj) r5
            com.google.android.gms.internal.ads.zzgw r5 = r5.zzhx()
            com.google.android.gms.internal.ads.zzms r6 = new com.google.android.gms.internal.ads.zzms
            com.google.android.gms.internal.ads.zzgw[] r7 = new com.google.android.gms.internal.ads.zzgw[r4]
            r7[r1] = r5
            r6.<init>(r7)
            r2[r3] = r6
            java.lang.String r5 = r5.zzafe
            boolean r6 = com.google.android.gms.internal.ads.zzof.zzbi(r5)
            if (r6 != 0) goto L_0x0070
            boolean r5 = com.google.android.gms.internal.ads.zzof.zzbh(r5)
            if (r5 == 0) goto L_0x006f
            goto L_0x0070
        L_0x006f:
            r4 = 0
        L_0x0070:
            boolean[] r5 = r8.zzbam
            r5[r3] = r4
            boolean r5 = r8.zzban
            r4 = r4 | r5
            r8.zzban = r4
            int r3 = r3 + 1
            goto L_0x0046
        L_0x007c:
            com.google.android.gms.internal.ads.zzmr r0 = new com.google.android.gms.internal.ads.zzmr
            r0.<init>(r2)
            r8.zzbak = r0
            r8.zzadv = r4
            com.google.android.gms.internal.ads.zzme r0 = r8.zzazu
            com.google.android.gms.internal.ads.zzmp r1 = new com.google.android.gms.internal.ads.zzmp
            long r2 = r8.zzagj
            com.google.android.gms.internal.ads.zzjm r4 = r8.zzbaf
            boolean r4 = r4.zzgh()
            r1.<init>(r2, r4)
            r2 = 0
            r0.zzb(r1, r2)
            com.google.android.gms.internal.ads.zzmc r0 = r8.zzbae
            r0.zza(r8)
        L_0x009d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzlp.zzhl():void");
    }

    private final void zza(zzlw zzlw) {
        if (this.zzce == -1) {
            this.zzce = zzlw.zzce;
        }
    }

    private final void startLoading() {
        zzjm zzjm;
        zzlw zzlw = new zzlw(this, this.uri, this.zzamu, this.zzazz, this.zzbaa);
        if (this.zzadv) {
            zzoc.checkState(zzho());
            long j = this.zzagj;
            if (j == -9223372036854775807L || this.zzbap < j) {
                zzlw.zze(this.zzbaf.zzdz(this.zzbap), this.zzbap);
                this.zzbap = -9223372036854775807L;
            } else {
                this.zzbar = true;
                this.zzbap = -9223372036854775807L;
                return;
            }
        }
        this.zzbaq = zzhm();
        int i = this.zzazs;
        if (i == -1) {
            i = (this.zzadv && this.zzce == -1 && ((zzjm = this.zzbaf) == null || zzjm.getDurationUs() == -9223372036854775807L)) ? 6 : 3;
        }
        this.zzazy.zza(zzlw, this, i);
    }

    private final int zzhm() {
        int size = this.zzbad.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            i += this.zzbad.valueAt(i2).zzhv();
        }
        return i;
    }

    private final long zzhn() {
        int size = this.zzbad.size();
        long j = Long.MIN_VALUE;
        for (int i = 0; i < size; i++) {
            j = Math.max(j, this.zzbad.valueAt(i).zzhn());
        }
        return j;
    }

    private final boolean zzho() {
        return this.zzbap != -9223372036854775807L;
    }

    public final /* synthetic */ int zza(zznx zznx, long j, long j2, IOException iOException) {
        zzjm zzjm;
        zzlw zzlw = (zzlw) zznx;
        zza(zzlw);
        zzddu zzddu = this.zzact;
        if (!(zzddu == null || this.zzazt == null)) {
            zzddu.post(new zzlt(this, iOException));
        }
        if (iOException instanceof zzmu) {
            return 3;
        }
        boolean z = zzhm() > this.zzbaq;
        if (this.zzce == -1 && ((zzjm = this.zzbaf) == null || zzjm.getDurationUs() == -9223372036854775807L)) {
            this.zzbao = 0;
            this.zzbai = this.zzadv;
            int size = this.zzbad.size();
            for (int i = 0; i < size; i++) {
                this.zzbad.valueAt(i).zzk(!this.zzadv || this.zzbal[i]);
            }
            zzlw.zze(0, 0);
        }
        this.zzbaq = zzhm();
        return z ? 1 : 0;
    }

    public final /* synthetic */ void zza(zznx zznx, long j, long j2, boolean z) {
        zza((zzlw) zznx);
        if (!z && this.zzbaj > 0) {
            int size = this.zzbad.size();
            for (int i = 0; i < size; i++) {
                this.zzbad.valueAt(i).zzk(this.zzbal[i]);
            }
            this.zzbae.zza((zzmn) this);
        }
    }

    public final /* synthetic */ void zza(zznx zznx, long j, long j2) {
        zza((zzlw) zznx);
        this.zzbar = true;
        if (this.zzagj == -9223372036854775807L) {
            long zzhn = zzhn();
            this.zzagj = zzhn == Long.MIN_VALUE ? 0 : zzhn + 10000;
            this.zzazu.zzb(new zzmp(this.zzagj, this.zzbaf.zzgh()), null);
        }
        this.zzbae.zza((zzmn) this);
    }
}
