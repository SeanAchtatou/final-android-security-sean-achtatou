package com.google.android.gms.internal.ads;

import android.os.Bundle;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcql implements zzcty {
    private final ArrayList zzgex;

    zzcql(ArrayList arrayList) {
        this.zzgex = arrayList;
    }

    public final void zzr(Object obj) {
        ((Bundle) obj).putStringArrayList("ad_types", this.zzgex);
    }
}
