package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.common.util.Clock;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzddc implements zzdxg<zzdda> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzayy> zzekm;
    private final zzdxp<zzczs> zzezj;
    private final zzdxp<zzdq> zzfbr;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<Clock> zzfcz;
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<String> zzgfg;
    private final zzdxp<String> zzgfh;
    private final zzdxp<zzclp> zzgro;

    private zzddc(zzdxp<Executor> zzdxp, zzdxp<zzayy> zzdxp2, zzdxp<zzclp> zzdxp3, zzdxp<zzazb> zzdxp4, zzdxp<String> zzdxp5, zzdxp<String> zzdxp6, zzdxp<Context> zzdxp7, zzdxp<zzczs> zzdxp8, zzdxp<Clock> zzdxp9, zzdxp<zzdq> zzdxp10) {
        this.zzfcv = zzdxp;
        this.zzekm = zzdxp2;
        this.zzgro = zzdxp3;
        this.zzfdb = zzdxp4;
        this.zzgfg = zzdxp5;
        this.zzgfh = zzdxp6;
        this.zzejv = zzdxp7;
        this.zzezj = zzdxp8;
        this.zzfcz = zzdxp9;
        this.zzfbr = zzdxp10;
    }

    public static zzddc zzb(zzdxp<Executor> zzdxp, zzdxp<zzayy> zzdxp2, zzdxp<zzclp> zzdxp3, zzdxp<zzazb> zzdxp4, zzdxp<String> zzdxp5, zzdxp<String> zzdxp6, zzdxp<Context> zzdxp7, zzdxp<zzczs> zzdxp8, zzdxp<Clock> zzdxp9, zzdxp<zzdq> zzdxp10) {
        return new zzddc(zzdxp, zzdxp2, zzdxp3, zzdxp4, zzdxp5, zzdxp6, zzdxp7, zzdxp8, zzdxp9, zzdxp10);
    }

    public final /* synthetic */ Object get() {
        return new zzdda(this.zzfcv.get(), this.zzekm.get(), this.zzgro.get(), this.zzfdb.get(), this.zzgfg.get(), this.zzgfh.get(), this.zzejv.get(), this.zzezj.get(), this.zzfcz.get(), this.zzfbr.get());
    }
}
