package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzjv implements zzju {
    private final /* synthetic */ zzjt zzapn;

    private zzjv(zzjt zzjt) {
        this.zzapn = zzjt;
    }

    public final int zzah(int i) {
        return zzjt.zzah(i);
    }

    public final boolean zzai(int i) {
        return zzjt.zzai(i);
    }

    public final void zzd(int i, long j, long j2) throws zzhd {
        this.zzapn.zzd(i, j, j2);
    }

    public final void zzaj(int i) throws zzhd {
        this.zzapn.zzaj(i);
    }

    public final void zzc(int i, long j) throws zzhd {
        this.zzapn.zzc(i, j);
    }

    public final void zza(int i, double d) throws zzhd {
        this.zzapn.zza(i, d);
    }

    public final void zza(int i, String str) throws zzhd {
        this.zzapn.zza(i, str);
    }

    public final void zza(int i, int i2, zzjg zzjg) throws IOException, InterruptedException {
        this.zzapn.zza(i, i2, zzjg);
    }

    /* synthetic */ zzjv(zzjt zzjt, zzjw zzjw) {
        this(zzjt);
    }
}
