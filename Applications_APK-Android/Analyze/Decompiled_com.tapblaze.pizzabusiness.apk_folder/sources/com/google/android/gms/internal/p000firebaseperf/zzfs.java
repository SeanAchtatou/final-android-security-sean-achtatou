package com.google.android.gms.internal.p000firebaseperf;

import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfs  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzfs<K> implements Map.Entry<K, Object> {
    private Map.Entry<K, zzfq> zzrv;

    private zzfs(Map.Entry<K, zzfq> entry) {
        this.zzrv = entry;
    }

    public final K getKey() {
        return this.zzrv.getKey();
    }

    public final Object getValue() {
        if (this.zzrv.getValue() == null) {
            return null;
        }
        return zzfq.zzht();
    }

    public final zzfq zzhu() {
        return this.zzrv.getValue();
    }

    public final Object setValue(Object obj) {
        if (obj instanceof zzgl) {
            return this.zzrv.getValue().zzh((zzgl) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
