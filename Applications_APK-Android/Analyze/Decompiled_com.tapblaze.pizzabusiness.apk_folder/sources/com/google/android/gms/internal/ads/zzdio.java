package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzdio {
    byte[] zzl(byte[] bArr) throws GeneralSecurityException;
}
