package com.google.android.gms.ads.internal;

import android.os.AsyncTask;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzdq;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzp extends AsyncTask<Void, Void, String> {
    private final /* synthetic */ zzl zzblk;

    private zzp(zzl zzl) {
        this.zzblk = zzl;
    }

    /* access modifiers changed from: private */
    /* renamed from: zza */
    public final String doInBackground(Void... voidArr) {
        try {
            zzdq unused = this.zzblk.zzblr = (zzdq) this.zzblk.zzbln.get(1000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            zzayu.zzd("", e);
        }
        return this.zzblk.zzke();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        if (this.zzblk.zzblp != null && str != null) {
            this.zzblk.zzblp.loadUrl(str);
        }
    }

    /* synthetic */ zzp(zzl zzl, zzk zzk) {
        this(zzl);
    }
}
