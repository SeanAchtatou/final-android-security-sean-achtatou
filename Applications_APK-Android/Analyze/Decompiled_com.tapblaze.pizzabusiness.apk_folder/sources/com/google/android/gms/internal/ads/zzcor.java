package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzbod;
import com.google.android.gms.internal.ads.zzbrm;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcor extends zzvt {
    private final Executor zzfci;
    private final zzbfx zzfzz;
    private final Context zzgcr;
    private final zzczw zzgcs = new zzczw();
    private final zzcop zzgcw = new zzcop();
    private final zzcos zzgcy = new zzcos();
    private zzaak zzgda;
    /* access modifiers changed from: private */
    public zzdhe<zzbtu> zzgdb;
    private final zzcxz zzgde = new zzcxz(new zzdax());
    /* access modifiers changed from: private */
    public zzbtu zzgdf;
    private boolean zzgdg = false;

    public zzcor(zzbfx zzbfx, Context context, zzuj zzuj, String str) {
        this.zzfzz = zzbfx;
        this.zzgcs.zzd(zzuj).zzgk(str);
        this.zzfci = zzbfx.zzaca();
        this.zzgcr = context;
    }

    public final zzxb getVideoController() {
        return null;
    }

    public final void setUserId(String str) {
    }

    public final void stopLoading() {
    }

    public final void zza(zzaoy zzaoy) {
    }

    public final void zza(zzape zzape, String str) {
    }

    public final void zza(zzrg zzrg) {
    }

    public final void zza(zzuj zzuj) {
    }

    public final void zza(zzuo zzuo) {
    }

    public final void zza(zzvg zzvg) {
    }

    public final void zza(zzxh zzxh) {
    }

    public final void zzbr(String str) {
    }

    public final IObjectWrapper zzjx() {
        return null;
    }

    public final void zzjy() {
    }

    public final zzuj zzjz() {
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcxz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcxz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcxz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcop, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcop, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcop, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcop, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    public final synchronized boolean zza(zzug zzug) {
        Preconditions.checkMainThread("loadAd must be called on the main UI thread.");
        if (this.zzgdb == null) {
            if (!zzamp()) {
                zzdad.zze(this.zzgcr, zzug.zzccb);
                this.zzgdf = null;
                zzczu zzaos = this.zzgcs.zzg(zzug).zzaos();
                zzbrm.zza zza = new zzbrm.zza();
                if (this.zzgde != null) {
                    zza.zza((zzbov) this.zzgde, this.zzfzz.zzaca()).zza((zzbqb) this.zzgde, this.zzfzz.zzaca()).zza((zzbow) this.zzgde, this.zzfzz.zzaca());
                }
                zzbup zzaek = this.zzfzz.zzack().zzd(new zzbod.zza().zzbz(this.zzgcr).zza(zzaos).zzahh()).zzd(zza.zza((zzbov) this.zzgcw, this.zzfzz.zzaca()).zza((zzbqb) this.zzgcw, this.zzfzz.zzaca()).zza((zzbow) this.zzgcw, this.zzfzz.zzaca()).zza((zzty) this.zzgcw, this.zzfzz.zzaca()).zza(this.zzgcy, this.zzfzz.zzaca()).zzahw()).zzb(new zzcns(this.zzgda)).zzaek();
                this.zzgdb = zzaek.zzadc().zzaha();
                zzdgs.zza(this.zzgdb, new zzcou(this, zzaek), this.zzfci);
                return true;
            }
        }
        return false;
    }

    public final synchronized void destroy() {
        Preconditions.checkMainThread("destroy must be called on the main UI thread.");
        if (this.zzgdf != null) {
            this.zzgdf.zzagr().zzbx(null);
        }
    }

    public final synchronized void pause() {
        Preconditions.checkMainThread("pause must be called on the main UI thread.");
        if (this.zzgdf != null) {
            this.zzgdf.zzagr().zzbv(null);
        }
    }

    public final synchronized void resume() {
        Preconditions.checkMainThread("resume must be called on the main UI thread.");
        if (this.zzgdf != null) {
            this.zzgdf.zzagr().zzbw(null);
        }
    }

    public final void zza(zzvh zzvh) {
        Preconditions.checkMainThread("setAdListener must be called on the main UI thread.");
        this.zzgcw.zzc(zzvh);
    }

    public final void zza(zzwc zzwc) {
        Preconditions.checkMainThread("setAppEventListener must be called on the main UI thread.");
        this.zzgcy.zzb(zzwc);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void showInterstitial() {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.String r0 = "showInterstitial must be called on the main UI thread."
            com.google.android.gms.common.internal.Preconditions.checkMainThread(r0)     // Catch:{ all -> 0x001d }
            com.google.android.gms.internal.ads.zzbtu r0 = r2.zzgdf     // Catch:{ all -> 0x001d }
            if (r0 != 0) goto L_0x000c
            monitor-exit(r2)
            return
        L_0x000c:
            com.google.android.gms.internal.ads.zzbtu r0 = r2.zzgdf     // Catch:{ all -> 0x001d }
            boolean r0 = r0.zzaid()     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001b
            com.google.android.gms.internal.ads.zzbtu r0 = r2.zzgdf     // Catch:{ all -> 0x001d }
            boolean r1 = r2.zzgdg     // Catch:{ all -> 0x001d }
            r0.zzbg(r1)     // Catch:{ all -> 0x001d }
        L_0x001b:
            monitor-exit(r2)
            return
        L_0x001d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcor.showInterstitial():void");
    }

    public final synchronized String getMediationAdapterClassName() {
        if (this.zzgdf == null || this.zzgdf.zzags() == null) {
            return null;
        }
        return this.zzgdf.zzags().getMediationAdapterClassName();
    }

    public final synchronized String zzka() {
        if (this.zzgdf == null || this.zzgdf.zzags() == null) {
            return null;
        }
        return this.zzgdf.zzags().getMediationAdapterClassName();
    }

    public final synchronized zzxa zzkb() {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcrf)).booleanValue()) {
            return null;
        }
        if (this.zzgdf == null) {
            return null;
        }
        return this.zzgdf.zzags();
    }

    public final synchronized void zza(zzwi zzwi) {
        Preconditions.checkMainThread("setCorrelationIdProvider must be called on the main UI thread");
        this.zzgcs.zzc(zzwi);
    }

    public final synchronized void setManualImpressionsEnabled(boolean z) {
        Preconditions.checkMainThread("setManualImpressionsEnabled must be called from the main thread.");
        this.zzgcs.zzbm(z);
    }

    public final synchronized boolean isLoading() {
        return this.zzgdb != null && !this.zzgdb.isDone();
    }

    public final synchronized boolean isReady() {
        Preconditions.checkMainThread("isLoaded must be called on the main UI thread.");
        return zzamp();
    }

    public final synchronized String getAdUnitId() {
        return this.zzgcs.zzaor();
    }

    public final zzwc zzkc() {
        return this.zzgcy.zzamq();
    }

    public final zzvh zzkd() {
        return this.zzgcw.zzamo();
    }

    public final synchronized void setImmersiveMode(boolean z) {
        Preconditions.checkMainThread("setImmersiveMode must be called on the main UI thread.");
        this.zzgdg = z;
    }

    public final synchronized void zza(zzyw zzyw) {
        this.zzgcs.zzc(zzyw);
    }

    public final synchronized void zza(zzaak zzaak) {
        Preconditions.checkMainThread("setOnCustomRenderedAdLoadedListener must be called on the main UI thread.");
        this.zzgda = zzaak;
    }

    private final synchronized boolean zzamp() {
        return this.zzgdf != null && !this.zzgdf.isClosed();
    }

    public final void zza(zzvx zzvx) {
        Preconditions.checkMainThread("setAdMetadataListener must be called on the main UI thread.");
    }

    public final Bundle getAdMetadata() {
        Preconditions.checkMainThread("getAdMetadata must be called on the main UI thread.");
        return new Bundle();
    }

    public final void zza(zzaro zzaro) {
        this.zzgde.zzb(zzaro);
    }
}
