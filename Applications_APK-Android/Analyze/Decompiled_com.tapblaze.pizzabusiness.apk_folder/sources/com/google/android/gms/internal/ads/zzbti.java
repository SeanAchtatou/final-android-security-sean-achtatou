package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbti implements zzdxg<zzbte> {
    private final zzdxp<Set<zzbsu<zzafx>>> zzfeo;

    public zzbti(zzdxp<Set<zzbsu<zzafx>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzbte(this.zzfeo.get());
    }
}
