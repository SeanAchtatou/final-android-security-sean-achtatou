package com.google.android.gms.gass;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.internal.ads.zzddh;
import com.google.android.gms.internal.ads.zzddn;
import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zzf {
    private final Looper zzgse;
    private final Context zzup;

    public zzf(Context context, Looper looper) {
        this.zzup = context;
        this.zzgse = looper;
    }

    public final void zzgq(String str) {
        new zzh(this.zzup, this.zzgse, (zzddn) ((zzdrt) zzddn.zzaqn().zzgt(this.zzup.getPackageName()).zzb(zzddn.zza.BLOCKED_IMPRESSION).zza(zzddh.zzaqk().zzgs(str).zzb(zzddh.zza.BLOCKED_REASON_BACKGROUND)).zzbaf())).zzaqm();
    }
}
