package com.google.android.gms.internal.ads;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzdut extends AbstractList<String> implements zzdsl, RandomAccess {
    /* access modifiers changed from: private */
    public final zzdsl zzhrf;

    public zzdut(zzdsl zzdsl) {
        this.zzhrf = zzdsl;
    }

    public final zzdsl zzbaw() {
        return this;
    }

    public final Object zzgm(int i) {
        return this.zzhrf.zzgm(i);
    }

    public final int size() {
        return this.zzhrf.size();
    }

    public final void zzbg(zzdqk zzdqk) {
        throw new UnsupportedOperationException();
    }

    public final ListIterator<String> listIterator(int i) {
        return new zzduw(this, i);
    }

    public final Iterator<String> iterator() {
        return new zzduv(this);
    }

    public final List<?> zzbav() {
        return this.zzhrf.zzbav();
    }

    public final /* synthetic */ Object get(int i) {
        return (String) this.zzhrf.get(i);
    }
}
