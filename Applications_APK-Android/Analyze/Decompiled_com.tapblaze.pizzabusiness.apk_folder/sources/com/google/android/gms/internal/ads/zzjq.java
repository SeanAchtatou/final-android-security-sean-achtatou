package com.google.android.gms.internal.ads;

import com.flurry.android.Constants;
import java.io.IOException;
import java.util.Stack;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzjq implements zzjr {
    private final byte[] zzane = new byte[8];
    private final Stack<zzjs> zzanf = new Stack<>();
    private final zzka zzang = new zzka();
    private zzju zzanh;
    private int zzani;
    private int zzanj;
    private long zzank;

    zzjq() {
    }

    public final void zza(zzju zzju) {
        this.zzanh = zzju;
    }

    public final void reset() {
        this.zzani = 0;
        this.zzanf.clear();
        this.zzang.reset();
    }

    public final boolean zzb(zzjg zzjg) throws IOException, InterruptedException {
        String str;
        double d;
        int zzan;
        int zza;
        zzoc.checkState(this.zzanh != null);
        while (true) {
            if (this.zzanf.isEmpty() || zzjg.getPosition() < this.zzanf.peek().zzanl) {
                if (this.zzani == 0) {
                    long zza2 = this.zzang.zza(zzjg, true, false, 4);
                    if (zza2 == -2) {
                        zzjg.zzgi();
                        while (true) {
                            zzjg.zza(this.zzane, 0, 4);
                            zzan = zzka.zzan(this.zzane[0]);
                            if (zzan != -1 && zzan <= 4) {
                                zza = (int) zzka.zza(this.zzane, zzan, false);
                                if (this.zzanh.zzai(zza)) {
                                    break;
                                }
                            }
                            zzjg.zzac(1);
                        }
                        zzjg.zzac(zzan);
                        zza2 = (long) zza;
                    }
                    if (zza2 == -1) {
                        return false;
                    }
                    this.zzanj = (int) zza2;
                    this.zzani = 1;
                }
                if (this.zzani == 1) {
                    this.zzank = this.zzang.zza(zzjg, false, true, 8);
                    this.zzani = 2;
                }
                int zzah = this.zzanh.zzah(this.zzanj);
                if (zzah == 0) {
                    zzjg.zzac((int) this.zzank);
                    this.zzani = 0;
                } else if (zzah == 1) {
                    long position = zzjg.getPosition();
                    this.zzanf.add(new zzjs(this.zzanj, this.zzank + position));
                    this.zzanh.zzd(this.zzanj, position, this.zzank);
                    this.zzani = 0;
                    return true;
                } else if (zzah == 2) {
                    long j = this.zzank;
                    if (j <= 8) {
                        this.zzanh.zzc(this.zzanj, zza(zzjg, (int) j));
                        this.zzani = 0;
                        return true;
                    }
                    StringBuilder sb = new StringBuilder(42);
                    sb.append("Invalid integer size: ");
                    sb.append(j);
                    throw new zzhd(sb.toString());
                } else if (zzah == 3) {
                    long j2 = this.zzank;
                    if (j2 <= 2147483647L) {
                        zzju zzju = this.zzanh;
                        int i = this.zzanj;
                        int i2 = (int) j2;
                        if (i2 == 0) {
                            str = "";
                        } else {
                            byte[] bArr = new byte[i2];
                            zzjg.readFully(bArr, 0, i2);
                            str = new String(bArr);
                        }
                        zzju.zza(i, str);
                        this.zzani = 0;
                        return true;
                    }
                    StringBuilder sb2 = new StringBuilder(41);
                    sb2.append("String element size: ");
                    sb2.append(j2);
                    throw new zzhd(sb2.toString());
                } else if (zzah == 4) {
                    this.zzanh.zza(this.zzanj, (int) this.zzank, zzjg);
                    this.zzani = 0;
                    return true;
                } else if (zzah == 5) {
                    long j3 = this.zzank;
                    if (j3 == 4 || j3 == 8) {
                        zzju zzju2 = this.zzanh;
                        int i3 = this.zzanj;
                        int i4 = (int) this.zzank;
                        long zza3 = zza(zzjg, i4);
                        if (i4 == 4) {
                            d = (double) Float.intBitsToFloat((int) zza3);
                        } else {
                            d = Double.longBitsToDouble(zza3);
                        }
                        zzju2.zza(i3, d);
                        this.zzani = 0;
                        return true;
                    }
                    StringBuilder sb3 = new StringBuilder(40);
                    sb3.append("Invalid float size: ");
                    sb3.append(j3);
                    throw new zzhd(sb3.toString());
                } else {
                    StringBuilder sb4 = new StringBuilder(32);
                    sb4.append("Invalid element type ");
                    sb4.append(zzah);
                    throw new zzhd(sb4.toString());
                }
            } else {
                this.zzanh.zzaj(this.zzanf.pop().zzanj);
                return true;
            }
        }
    }

    private final long zza(zzjg zzjg, int i) throws IOException, InterruptedException {
        zzjg.readFully(this.zzane, 0, i);
        long j = 0;
        for (int i2 = 0; i2 < i; i2++) {
            j = (j << 8) | ((long) (this.zzane[i2] & Constants.UNKNOWN));
        }
        return j;
    }
}
