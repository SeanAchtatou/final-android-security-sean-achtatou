package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzag  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzag extends zzaz<Boolean> {
    private static zzag zzaj;

    private zzag() {
    }

    /* access modifiers changed from: protected */
    public final String zzaf() {
        return "firebase_performance_collection_enabled";
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "isEnabled";
    }

    protected static synchronized zzag zzae() {
        zzag zzag;
        synchronized (zzag.class) {
            if (zzaj == null) {
                zzaj = new zzag();
            }
            zzag = zzaj;
        }
        return zzag;
    }
}
