package com.google.android.gms.ads;

import android.content.Context;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.mediation.rtb.RtbAdapter;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.internal.ads.zzxq;
import com.google.android.gms.internal.ads.zzxv;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class MobileAds {
    private MobileAds() {
    }

    public static void initialize(Context context, String str) {
        initialize(context, str, null);
    }

    @Deprecated
    public static void initialize(Context context, String str, Settings settings) {
        zzxv zzxv;
        zzxq zzpw = zzxq.zzpw();
        if (settings == null) {
            zzxv = null;
        } else {
            zzxv = settings.zzdk();
        }
        zzpw.zza(context, str, zzxv, null);
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static final class Settings {
        private final zzxv zzabn = new zzxv();

        @Deprecated
        public final String getTrackingId() {
            return null;
        }

        @Deprecated
        public final boolean isGoogleAnalyticsEnabled() {
            return false;
        }

        @Deprecated
        public final Settings setGoogleAnalyticsEnabled(boolean z) {
            return this;
        }

        @Deprecated
        public final Settings setTrackingId(String str) {
            return this;
        }

        /* access modifiers changed from: package-private */
        public final zzxv zzdk() {
            return this.zzabn;
        }
    }

    public static void initialize(Context context) {
        initialize(context, null, null);
    }

    public static void initialize(Context context, OnInitializationCompleteListener onInitializationCompleteListener) {
        zzxq.zzpw().zza(context, null, null, onInitializationCompleteListener);
    }

    public static RewardedVideoAd getRewardedVideoAdInstance(Context context) {
        return zzxq.zzpw().getRewardedVideoAdInstance(context);
    }

    public static void setAppVolume(float f) {
        zzxq.zzpw().setAppVolume(f);
    }

    public static void setAppMuted(boolean z) {
        zzxq.zzpw().setAppMuted(z);
    }

    public static void openDebugMenu(Context context, String str) {
        zzxq.zzpw().openDebugMenu(context, str);
    }

    public static String getVersionString() {
        return zzxq.zzpw().getVersionString();
    }

    public static void registerRtbAdapter(Class<? extends RtbAdapter> cls) {
        zzxq.zzpw().registerRtbAdapter(cls);
    }

    public static InitializationStatus getInitializationStatus() {
        return zzxq.zzpw().getInitializationStatus();
    }

    public static RequestConfiguration getRequestConfiguration() {
        return zzxq.zzpw().getRequestConfiguration();
    }

    public static void setRequestConfiguration(RequestConfiguration requestConfiguration) {
        zzxq.zzpw().setRequestConfiguration(requestConfiguration);
    }
}
