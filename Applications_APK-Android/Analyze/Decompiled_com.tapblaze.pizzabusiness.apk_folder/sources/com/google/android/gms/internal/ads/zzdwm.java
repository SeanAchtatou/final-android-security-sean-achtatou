package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdwm extends zzdvq<zzdwm> {
    private static volatile zzdwm[] zzhyx;
    public String url = null;
    public Integer zzhyy = null;
    public zzdwk zzhyz = null;
    private zzdvx.zzb.zze zzhza = null;
    private Integer zzhzb = null;
    private int[] zzhzc = zzdvy.zzhow;
    private String zzhzd = null;
    public zzdvx.zzb.zzh.zza zzhze = null;
    public String[] zzhzf = zzdvy.zzhuc;

    public static zzdwm[] zzbdk() {
        if (zzhyx == null) {
            synchronized (zzdvu.zzhtt) {
                if (zzhyx == null) {
                    zzhyx = new zzdwm[0];
                }
            }
        }
        return zzhyx;
    }

    public zzdwm() {
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    public final void zza(zzdvo zzdvo) throws IOException {
        zzdvo.zzab(1, this.zzhyy.intValue());
        String str = this.url;
        if (str != null) {
            zzdvo.zzf(2, str);
        }
        zzdwk zzdwk = this.zzhyz;
        if (zzdwk != null) {
            zzdvo.zza(3, zzdwk);
        }
        int[] iArr = this.zzhzc;
        int i = 0;
        if (iArr != null && iArr.length > 0) {
            int i2 = 0;
            while (true) {
                int[] iArr2 = this.zzhzc;
                if (i2 >= iArr2.length) {
                    break;
                }
                zzdvo.zzab(6, iArr2[i2]);
                i2++;
            }
        }
        zzdvx.zzb.zzh.zza zza = this.zzhze;
        if (!(zza == null || zza == null)) {
            zzdvo.zzab(8, zza.zzae());
        }
        String[] strArr = this.zzhzf;
        if (strArr != null && strArr.length > 0) {
            while (true) {
                String[] strArr2 = this.zzhzf;
                if (i >= strArr2.length) {
                    break;
                }
                String str2 = strArr2[i];
                if (str2 != null) {
                    zzdvo.zzf(9, str2);
                }
                i++;
            }
        }
        super.zza(zzdvo);
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        int[] iArr;
        int zzoi = super.zzoi() + zzdvo.zzaf(1, this.zzhyy.intValue());
        String str = this.url;
        if (str != null) {
            zzoi += zzdvo.zzg(2, str);
        }
        zzdwk zzdwk = this.zzhyz;
        if (zzdwk != null) {
            zzoi += zzdvo.zzb(3, zzdwk);
        }
        int[] iArr2 = this.zzhzc;
        int i = 0;
        if (iArr2 != null && iArr2.length > 0) {
            int i2 = 0;
            int i3 = 0;
            while (true) {
                iArr = this.zzhzc;
                if (i2 >= iArr.length) {
                    break;
                }
                i3 += zzdvo.zzga(iArr[i2]);
                i2++;
            }
            zzoi = zzoi + i3 + (iArr.length * 1);
        }
        zzdvx.zzb.zzh.zza zza = this.zzhze;
        if (!(zza == null || zza == null)) {
            zzoi += zzdvo.zzaf(8, zza.zzae());
        }
        String[] strArr = this.zzhzf;
        if (strArr == null || strArr.length <= 0) {
            return zzoi;
        }
        int i4 = 0;
        int i5 = 0;
        while (true) {
            String[] strArr2 = this.zzhzf;
            if (i >= strArr2.length) {
                return zzoi + i4 + (i5 * 1);
            }
            String str2 = strArr2[i];
            if (str2 != null) {
                i5++;
                i4 += zzdvo.zzhh(str2);
            }
            i++;
        }
    }
}
