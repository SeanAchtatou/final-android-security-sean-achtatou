package com.google.android.gms.internal.p000firebaseperf;

import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhh  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhh extends zzhn {
    private final /* synthetic */ zzhg zzui;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private zzhh(zzhg zzhg) {
        super(zzhg, null);
        this.zzui = zzhg;
    }

    public final Iterator<Map.Entry<K, V>> iterator() {
        return new zzhi(this.zzui, null);
    }

    /* synthetic */ zzhh(zzhg zzhg, zzhf zzhf) {
        this(zzhg);
    }
}
