package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzde  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzde extends zzfc<zzde, zza> implements zzgn {
    private static volatile zzgv<zzde> zzij;
    private static final zzfj<Integer, zzdi> zzln = new zzdd();
    /* access modifiers changed from: private */
    public static final zzde zzlo;
    private int zzie;
    private String zzjt = "";
    private zzfk zzlm = zzhg();

    private zzde() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzde$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzde, zza> implements zzgn {
        private zza() {
            super(zzde.zzlo);
        }

        public final zza zzag(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzde) this.zzqq).zzac(str);
            return this;
        }

        public final zza zzb(zzdi zzdi) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzde) this.zzqq).zza(zzdi);
            return this;
        }

        /* synthetic */ zza(zzdd zzdd) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zzac(String str) {
        str.getClass();
        this.zzie |= 1;
        this.zzjt = str;
    }

    public final int zzfn() {
        return this.zzlm.size();
    }

    public final zzdi zzn(int i) {
        return zzln.convert(Integer.valueOf(this.zzlm.getInt(0)));
    }

    /* access modifiers changed from: private */
    public final void zza(zzdi zzdi) {
        zzdi.getClass();
        if (!this.zzlm.zzgf()) {
            zzfk zzfk = this.zzlm;
            int size = zzfk.size();
            this.zzlm = zzfk.zzak(size == 0 ? 10 : size << 1);
        }
        this.zzlm.zzal(zzdi.getNumber());
    }

    public static zza zzfo() {
        return (zza) zzlo.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzdf.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzde();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzlo, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\b\u0000\u0002\u001e", new Object[]{"zzie", "zzjt", "zzlm", zzdi.zzdp()});
            case 4:
                return zzlo;
            case 5:
                zzgv<zzde> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzde.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzlo);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzde zzde = new zzde();
        zzlo = zzde;
        zzfc.zza(zzde.class, zzde);
    }
}
