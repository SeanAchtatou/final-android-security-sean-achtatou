package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcui implements zzcty<Bundle> {
    private final String zzdmn;
    private final int zzdmo;
    private final int zzdmp;
    private final int zzdmq;
    private final boolean zzdmr;
    private final int zzdms;

    public zzcui(String str, int i, int i2, int i3, boolean z, int i4) {
        this.zzdmn = str;
        this.zzdmo = i;
        this.zzdmp = i2;
        this.zzdmq = i3;
        this.zzdmr = z;
        this.zzdms = i4;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        String str = this.zzdmn;
        boolean z = true;
        zzdaa.zza(bundle, "carrier", str, !TextUtils.isEmpty(str));
        Integer valueOf = Integer.valueOf(this.zzdmo);
        if (this.zzdmo == -2) {
            z = false;
        }
        zzdaa.zza(bundle, "cnt", valueOf, z);
        bundle.putInt("gnt", this.zzdmp);
        bundle.putInt("pt", this.zzdmq);
        Bundle zza = zzdaa.zza(bundle, Constants.ParametersKeys.ORIENTATION_DEVICE);
        bundle.putBundle(Constants.ParametersKeys.ORIENTATION_DEVICE, zza);
        Bundle zza2 = zzdaa.zza(zza, "network");
        zza.putBundle("network", zza2);
        zza2.putInt("active_network_state", this.zzdms);
        zza2.putBoolean("active_network_metered", this.zzdmr);
    }
}
