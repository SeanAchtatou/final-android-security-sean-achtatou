package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzaw  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzaw extends zzaz<Float> {
    private static zzaw zzba;

    private zzaw() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.TraceSamplingRate";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_vc_trace_sampling_rate";
    }

    protected static synchronized zzaw zzay() {
        zzaw zzaw;
        synchronized (zzaw.class) {
            if (zzba == null) {
                zzba = new zzaw();
            }
            zzaw = zzba;
        }
        return zzaw;
    }
}
