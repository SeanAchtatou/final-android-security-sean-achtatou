package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbps implements zzbrn {
    static final zzbrn zzfhp = new zzbps();

    private zzbps() {
    }

    public final void zzp(Object obj) {
        ((zzbov) obj).onRewardedVideoCompleted();
    }
}
