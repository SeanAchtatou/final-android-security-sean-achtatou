package com.google.android.gms.internal.p000firebaseperf;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzha  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzha {
    private static final zzha zztv = new zzha();
    private final zzhe zztw = new zzgc();
    private final ConcurrentMap<Class<?>, zzhb<?>> zztx = new ConcurrentHashMap();

    public static zzha zzio() {
        return zztv;
    }

    public final <T> zzhb<T> zze(Class<T> cls) {
        zzfg.checkNotNull(cls, "messageType");
        zzhb<T> zzhb = this.zztx.get(cls);
        if (zzhb != null) {
            return zzhb;
        }
        zzhb<T> zzd = this.zztw.zzd(cls);
        zzfg.checkNotNull(cls, "messageType");
        zzfg.checkNotNull(zzd, "schema");
        zzhb<T> putIfAbsent = this.zztx.putIfAbsent(cls, zzd);
        return putIfAbsent != null ? putIfAbsent : zzd;
    }

    public final <T> zzhb<T> zzo(T t) {
        return zze(t.getClass());
    }

    private zzha() {
    }
}
