package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzciq {
    private zzbpd zzfjm;
    private zzczl zzfjn;

    public zzciq(zzczl zzczl) {
        this.zzfjn = zzczl;
    }

    public final void zzamd() {
        if (this.zzfjm != null && this.zzfjn.zzglz == 2) {
            this.zzfjm.onAdImpression();
        }
    }

    public final void zza(zzbpd zzbpd) {
        this.zzfjm = zzbpd;
    }
}
