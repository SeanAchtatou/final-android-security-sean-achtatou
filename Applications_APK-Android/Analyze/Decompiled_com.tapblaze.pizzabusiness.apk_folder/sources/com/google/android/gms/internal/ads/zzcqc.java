package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcqc implements zzcty {
    private final String zzcyz;

    zzcqc(String str) {
        this.zzcyz = str;
    }

    public final void zzr(Object obj) {
        ((Bundle) obj).putString("ms", this.zzcyz);
    }
}
