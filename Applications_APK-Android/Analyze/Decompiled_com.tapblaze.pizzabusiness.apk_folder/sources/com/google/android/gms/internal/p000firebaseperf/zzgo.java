package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgo  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public interface zzgo extends zzgn, Cloneable {
    zzgo zza(zzgl zzgl);

    zzgl zzho();

    zzgl zzhp();
}
