package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.internal.ads.zzsy;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcig implements zzdgt<Bundle> {
    private final /* synthetic */ boolean zzfxw;
    final /* synthetic */ zzcid zzfxx;

    zzcig(zzcid zzcid, boolean z) {
        this.zzfxx = zzcid;
        this.zzfxw = z;
    }

    public final void zzb(Throwable th) {
        zzavs.zzex("Failed to get signals bundle");
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        Bundle bundle = (Bundle) obj;
        ArrayList zza = zzcid.zzl(bundle);
        zzsy.zzj.zzc zzb = zzcid.zzk(bundle);
        this.zzfxx.zzfxo.zza(new zzcif(this, this.zzfxw, zza, this.zzfxx.zzj(bundle), zzb));
    }
}
