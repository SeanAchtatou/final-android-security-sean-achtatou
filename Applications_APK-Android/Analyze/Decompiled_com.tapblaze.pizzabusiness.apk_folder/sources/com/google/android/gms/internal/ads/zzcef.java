package com.google.android.gms.internal.ads;

import java.util.Set;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcef implements zzdxg<Set<zzbsu<zzbph>>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzceo> zzfsb;
    private final zzcee zzfth;

    private zzcef(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfth = zzcee;
        this.zzfsb = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzcef zzb(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzcef(zzcee, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(zzcee.zza(this.zzfsb.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
