package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcdq implements zzdxg<zzbsu<zzdcx>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzcdu> zzfdd;

    private zzcdq(zzdxp<zzcdu> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzcdq zzz(zzdxp<zzcdu> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzcdq(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
