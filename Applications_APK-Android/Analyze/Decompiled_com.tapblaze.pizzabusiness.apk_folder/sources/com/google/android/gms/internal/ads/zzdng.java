package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdng extends zzdrt<zzdng, zza> implements zzdtg {
    private static volatile zzdtn<zzdng> zzdz;
    /* access modifiers changed from: private */
    public static final zzdng zzhdk;
    private String zzhcs = "";
    private zzdqk zzhct = zzdqk.zzhhx;
    private int zzhdj;

    private zzdng() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdng, zza> implements zzdtg {
        private zza() {
            super(zzdng.zzhdk);
        }

        /* synthetic */ zza(zzdnf zzdnf) {
            this();
        }
    }

    public final String zzavi() {
        return this.zzhcs;
    }

    public final zzdqk zzavj() {
        return this.zzhct;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnf.zzdk[i - 1]) {
            case 1:
                return new zzdng();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhdk, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001Ȉ\u0002\n\u0003\f", new Object[]{"zzhcs", "zzhct", "zzhdj"});
            case 4:
                return zzhdk;
            case 5:
                zzdtn<zzdng> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdng.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhdk);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdng zzavo() {
        return zzhdk;
    }

    static {
        zzdng zzdng = new zzdng();
        zzhdk = zzdng;
        zzdrt.zza(zzdng.class, zzdng);
    }
}
