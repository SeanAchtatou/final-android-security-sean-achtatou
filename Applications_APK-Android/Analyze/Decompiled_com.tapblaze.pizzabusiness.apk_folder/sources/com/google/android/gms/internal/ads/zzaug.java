package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaug implements zzaui {
    static final zzaui zzdpr = new zzaug();

    private zzaug() {
    }

    public final Object zzb(zzbfq zzbfq) {
        return Long.valueOf(zzbfq.generateEventId());
    }
}
