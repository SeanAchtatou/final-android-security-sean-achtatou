package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;
import java.util.List;

@Deprecated
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdny extends zzdrt<zzdny, zza> implements zzdtg {
    private static volatile zzdtn<zzdny> zzdz;
    /* access modifiers changed from: private */
    public static final zzdny zzhes;
    private String zzheq = "";
    private zzdsb<zzdnh> zzher = zzazw();

    private zzdny() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdny, zza> implements zzdtg {
        private zza() {
            super(zzdny.zzhes);
        }

        /* synthetic */ zza(zzdnx zzdnx) {
            this();
        }
    }

    public final List<zzdnh> zzawu() {
        return this.zzher;
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdnx.zzdk[i - 1]) {
            case 1:
                return new zzdny();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhes, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001Ȉ\u0002\u001b", new Object[]{"zzheq", "zzher", zzdnh.class});
            case 4:
                return zzhes;
            case 5:
                zzdtn<zzdny> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdny.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhes);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdny zzawv() {
        return zzhes;
    }

    static {
        zzdny zzdny = new zzdny();
        zzhes = zzdny;
        zzdrt.zza(zzdny.class, zzdny);
    }
}
