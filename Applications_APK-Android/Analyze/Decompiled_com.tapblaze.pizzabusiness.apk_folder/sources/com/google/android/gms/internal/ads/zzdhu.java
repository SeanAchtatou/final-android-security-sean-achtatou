package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdhu extends zzdha<V> {
    private final Callable<V> zzgwq;
    private final /* synthetic */ zzdhs zzgxo;

    zzdhu(zzdhs zzdhs, Callable<V> callable) {
        this.zzgxo = zzdhs;
        this.zzgwq = (Callable) zzdei.checkNotNull(callable);
    }

    /* access modifiers changed from: package-private */
    public final boolean isDone() {
        return this.zzgxo.isDone();
    }

    /* access modifiers changed from: package-private */
    public final V zzars() throws Exception {
        return this.zzgwq.call();
    }

    /* access modifiers changed from: package-private */
    public final void zzb(V v, Throwable th) {
        if (th == null) {
            this.zzgxo.set(v);
        } else {
            this.zzgxo.setException(th);
        }
    }

    /* access modifiers changed from: package-private */
    public final String zzart() {
        return this.zzgwq.toString();
    }
}
