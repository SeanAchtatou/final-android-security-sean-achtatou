package com.google.android.gms.drive.query.internal;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.zzb;
import java.util.List;

public interface zzj<F> {
    <T> F zza(zzb zzb, Object obj);

    <T> F zza(zzx zzx, MetadataField<T> metadataField, T t);

    F zza(zzx zzx, List list);

    F zza(Object obj);

    F zzbj();

    F zzbk();

    <T> F zzc(MetadataField<T> metadataField, T t);

    F zze(MetadataField<?> metadataField);

    F zzi(String str);
}
