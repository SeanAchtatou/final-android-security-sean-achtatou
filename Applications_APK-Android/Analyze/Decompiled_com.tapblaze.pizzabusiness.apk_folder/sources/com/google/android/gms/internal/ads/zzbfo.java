package com.google.android.gms.internal.ads;

import android.webkit.WebView;
import com.google.android.gms.common.util.PlatformVersion;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbfo {
    private static Boolean zzeid;

    private zzbfo() {
    }

    private static boolean zzb(WebView webView) {
        boolean booleanValue;
        synchronized (zzbfo.class) {
            if (zzeid == null) {
                try {
                    webView.evaluateJavascript("(function(){})()", null);
                    zzeid = true;
                } catch (IllegalStateException unused) {
                    zzeid = false;
                }
            }
            booleanValue = zzeid.booleanValue();
        }
        return booleanValue;
    }

    static void zza(WebView webView, String str) {
        if (!PlatformVersion.isAtLeastKitKat() || !zzb(webView)) {
            String valueOf = String.valueOf(str);
            webView.loadUrl(valueOf.length() != 0 ? "javascript:".concat(valueOf) : new String("javascript:"));
            return;
        }
        webView.evaluateJavascript(str, null);
    }
}
