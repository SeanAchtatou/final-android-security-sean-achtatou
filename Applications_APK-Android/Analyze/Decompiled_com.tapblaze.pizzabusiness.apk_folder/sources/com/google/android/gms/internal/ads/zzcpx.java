package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcpx implements Callable {
    private final zzcpu zzgen;

    zzcpx(zzcpu zzcpu) {
        this.zzgen = zzcpu;
    }

    public final Object call() {
        return this.zzgen.zzand();
    }
}
