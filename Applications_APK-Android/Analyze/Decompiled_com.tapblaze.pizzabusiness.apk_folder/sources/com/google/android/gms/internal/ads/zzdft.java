package com.google.android.gms.internal.ads;

import java.lang.Throwable;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdft<V, X extends Throwable> extends zzdfr<V, X, zzded<? super X, ? extends V>, V> {
    zzdft(zzdhe<? extends V> zzdhe, Class<X> cls, zzded<? super X, ? extends V> zzded) {
        super(zzdhe, cls, zzded);
    }

    /* access modifiers changed from: package-private */
    public final void setResult(@NullableDecl V v) {
        set(v);
    }

    /* access modifiers changed from: package-private */
    @NullableDecl
    public final /* synthetic */ Object zza(Object obj, Throwable th) throws Exception {
        return ((zzded) obj).apply(th);
    }
}
