package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbxp implements zzafn {
    private final zzbxq zzfnv;
    private final zzaeb zzfnw;

    zzbxp(zzbxq zzbxq, zzaeb zzaeb) {
        this.zzfnv = zzbxq;
        this.zzfnw = zzaeb;
    }

    public final void zza(Object obj, Map map) {
        zzbxq zzbxq = this.zzfnv;
        zzaeb zzaeb = this.zzfnw;
        try {
            zzbxq.zzfob = Long.valueOf(Long.parseLong((String) map.get("timestamp")));
        } catch (NumberFormatException unused) {
            zzavs.zzex("Failed to call parse unconfirmedClickTimestamp.");
        }
        zzbxq.zzfoa = (String) map.get("id");
        String str = (String) map.get("asset_id");
        if (zzaeb == null) {
            zzavs.zzea("Received unconfirmed click but UnconfirmedClickListener is null.");
            return;
        }
        try {
            zzaeb.onUnconfirmedClickReceived(str);
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
