package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.collection.ArrayMap;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.internal.measurement.zzbq;
import com.google.android.gms.internal.measurement.zzbs;
import com.google.android.gms.internal.measurement.zzfe;
import com.google.android.gms.internal.measurement.zzkp;
import com.google.android.gms.internal.measurement.zzkq;
import com.google.android.gms.internal.measurement.zzkw;
import com.google.android.gms.internal.measurement.zzln;
import com.google.android.gms.internal.measurement.zzv;
import com.ironsource.eventsmodule.DataBaseEventsStorage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public class zzkj implements zzhc {
    private static volatile zzkj zza;
    private zzfz zzb;
    private zzff zzc;
    private zzac zzd;
    private zzfm zze;
    private zzkf zzf;
    private zzn zzg;
    private final zzkr zzh;
    private zzim zzi;
    private final zzgf zzj;
    private boolean zzk;
    private boolean zzl;
    private boolean zzm;
    private long zzn;
    private List<Runnable> zzo;
    private int zzp;
    private int zzq;
    private boolean zzr;
    private boolean zzs;
    private boolean zzt;
    private FileLock zzu;
    private FileChannel zzv;
    private List<Long> zzw;
    private List<Long> zzx;
    private long zzy;

    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    class zza implements zzae {
        zzbs.zzg zza;
        List<Long> zzb;
        List<zzbs.zzc> zzc;
        private long zzd;

        private zza() {
        }

        public final void zza(zzbs.zzg zzg) {
            Preconditions.checkNotNull(zzg);
            this.zza = zzg;
        }

        public final boolean zza(long j, zzbs.zzc zzc2) {
            Preconditions.checkNotNull(zzc2);
            if (this.zzc == null) {
                this.zzc = new ArrayList();
            }
            if (this.zzb == null) {
                this.zzb = new ArrayList();
            }
            if (this.zzc.size() > 0 && zza(this.zzc.get(0)) != zza(zzc2)) {
                return false;
            }
            long zzbm = this.zzd + ((long) zzc2.zzbm());
            if (zzbm >= ((long) Math.max(0, zzap.zzh.zza(null).intValue()))) {
                return false;
            }
            this.zzd = zzbm;
            this.zzc.add(zzc2);
            this.zzb.add(Long.valueOf(j));
            if (this.zzc.size() >= Math.max(1, zzap.zzi.zza(null).intValue())) {
                return false;
            }
            return true;
        }

        private static long zza(zzbs.zzc zzc2) {
            return ((zzc2.zze() / 1000) / 60) / 60;
        }

        /* synthetic */ zza(zzkj zzkj, zzkm zzkm) {
            this();
        }
    }

    public static zzkj zza(Context context) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(context.getApplicationContext());
        if (zza == null) {
            synchronized (zzkj.class) {
                if (zza == null) {
                    zza = new zzkj(new zzkp(context));
                }
            }
        }
        return zza;
    }

    private zzkj(zzkp zzkp) {
        this(zzkp, null);
    }

    private zzkj(zzkp zzkp, zzgf zzgf) {
        this.zzk = false;
        Preconditions.checkNotNull(zzkp);
        this.zzj = zzgf.zza(zzkp.zza, (zzv) null);
        this.zzy = -1;
        zzkr zzkr = new zzkr(this);
        zzkr.zzal();
        this.zzh = zzkr;
        zzff zzff = new zzff(this);
        zzff.zzal();
        this.zzc = zzff;
        zzfz zzfz = new zzfz(this);
        zzfz.zzal();
        this.zzb = zzfz;
        this.zzj.zzq().zza(new zzkm(this, zzkp));
    }

    /* access modifiers changed from: private */
    public final void zza(zzkp zzkp) {
        this.zzj.zzq().zzd();
        zzac zzac = new zzac(this);
        zzac.zzal();
        this.zzd = zzac;
        this.zzj.zzb().zza(this.zzb);
        zzn zzn2 = new zzn(this);
        zzn2.zzal();
        this.zzg = zzn2;
        zzim zzim = new zzim(this);
        zzim.zzal();
        this.zzi = zzim;
        zzkf zzkf = new zzkf(this);
        zzkf.zzal();
        this.zzf = zzkf;
        this.zze = new zzfm(this);
        if (this.zzp != this.zzq) {
            this.zzj.zzr().zzf().zza("Not all upload components initialized", Integer.valueOf(this.zzp), Integer.valueOf(this.zzq));
        }
        this.zzk = true;
    }

    /* access modifiers changed from: protected */
    public final void zza() {
        this.zzj.zzq().zzd();
        zze().zzv();
        if (this.zzj.zzc().zzc.zza() == 0) {
            this.zzj.zzc().zzc.zza(this.zzj.zzm().currentTimeMillis());
        }
        zzz();
    }

    public final zzw zzu() {
        return this.zzj.zzu();
    }

    public final zzx zzb() {
        return this.zzj.zzb();
    }

    public final zzfb zzr() {
        return this.zzj.zzr();
    }

    public final zzgc zzq() {
        return this.zzj.zzq();
    }

    public final zzfz zzc() {
        zzb(this.zzb);
        return this.zzb;
    }

    public final zzff zzd() {
        zzb(this.zzc);
        return this.zzc;
    }

    public final zzac zze() {
        zzb(this.zzd);
        return this.zzd;
    }

    private final zzfm zzt() {
        zzfm zzfm = this.zze;
        if (zzfm != null) {
            return zzfm;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    private final zzkf zzv() {
        zzb(this.zzf);
        return this.zzf;
    }

    public final zzn zzf() {
        zzb(this.zzg);
        return this.zzg;
    }

    public final zzim zzg() {
        zzb(this.zzi);
        return this.zzi;
    }

    public final zzkr zzh() {
        zzb(this.zzh);
        return this.zzh;
    }

    public final zzez zzi() {
        return this.zzj.zzj();
    }

    public final Context zzn() {
        return this.zzj.zzn();
    }

    public final Clock zzm() {
        return this.zzj.zzm();
    }

    public final zzkv zzj() {
        return this.zzj.zzi();
    }

    private final void zzw() {
        this.zzj.zzq().zzd();
    }

    /* access modifiers changed from: package-private */
    public final void zzk() {
        if (!this.zzk) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    private static void zzb(zzkk zzkk) {
        if (zzkk == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!zzkk.zzaj()) {
            String valueOf = String.valueOf(zzkk.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    private final long zzx() {
        long currentTimeMillis = this.zzj.zzm().currentTimeMillis();
        zzfo zzc2 = this.zzj.zzc();
        zzc2.zzaa();
        zzc2.zzd();
        long zza2 = zzc2.zzg.zza();
        if (zza2 == 0) {
            zza2 = 1 + ((long) zzc2.zzp().zzh().nextInt(86400000));
            zzc2.zzg.zza(zza2);
        }
        return ((((currentTimeMillis + zza2) / 1000) / 60) / 60) / 24;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void */
    /* access modifiers changed from: package-private */
    public final void zza(zzan zzan, String str) {
        zzan zzan2 = zzan;
        zzg zzb2 = zze().zzb(str);
        if (zzb2 == null || TextUtils.isEmpty(zzb2.zzl())) {
            this.zzj.zzr().zzw().zza("No app data available; dropping event", str);
            return;
        }
        Boolean zzb3 = zzb(zzb2);
        if (zzb3 == null) {
            if (!"_ui".equals(zzan2.zza)) {
                this.zzj.zzr().zzi().zza("Could not find package. appId", zzfb.zza(str));
            }
        } else if (!zzb3.booleanValue()) {
            this.zzj.zzr().zzf().zza("App version does not match; dropping event. appId", zzfb.zza(str));
            return;
        }
        zzm zzm2 = r2;
        zzm zzm3 = new zzm(str, zzb2.zze(), zzb2.zzl(), zzb2.zzm(), zzb2.zzn(), zzb2.zzo(), zzb2.zzp(), (String) null, zzb2.zzr(), false, zzb2.zzi(), zzb2.zzae(), 0L, 0, zzb2.zzaf(), zzb2.zzag(), false, zzb2.zzf(), zzb2.zzah(), zzb2.zzq(), zzb2.zzai(), (!zzln.zzb() || !this.zzj.zzb().zze(zzb2.zzc(), zzap.zzcf)) ? null : zzb2.zzg());
        zza(zzan2, zzm2);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x013b A[Catch:{ all -> 0x0392 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x021a A[Catch:{ all -> 0x0392 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(com.google.android.gms.measurement.internal.zzan r20, com.google.android.gms.measurement.internal.zzm r21) {
        /*
            r19 = this;
            r1 = r19
            r0 = r20
            r2 = r21
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r21)
            java.lang.String r3 = r2.zza
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r3)
            r19.zzw()
            r19.zzk()
            java.lang.String r3 = r2.zza
            long r11 = r0.zzd
            com.google.android.gms.measurement.internal.zzkr r4 = r19.zzh()
            boolean r4 = r4.zza(r0, r2)
            if (r4 != 0) goto L_0x0023
            return
        L_0x0023:
            boolean r4 = r2.zzh
            if (r4 != 0) goto L_0x002b
            r1.zzc(r2)
            return
        L_0x002b:
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj
            com.google.android.gms.measurement.internal.zzx r4 = r4.zzb()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.zzap.zzbj
            boolean r4 = r4.zze(r3, r5)
            if (r4 == 0) goto L_0x007f
            java.util.List<java.lang.String> r4 = r2.zzu
            if (r4 == 0) goto L_0x007f
            java.util.List<java.lang.String> r4 = r2.zzu
            java.lang.String r5 = r0.zza
            boolean r4 = r4.contains(r5)
            if (r4 == 0) goto L_0x006b
            com.google.android.gms.measurement.internal.zzam r4 = r0.zzb
            android.os.Bundle r4 = r4.zzb()
            r5 = 1
            java.lang.String r7 = "ga_safelisted"
            r4.putLong(r7, r5)
            com.google.android.gms.measurement.internal.zzan r5 = new com.google.android.gms.measurement.internal.zzan
            java.lang.String r14 = r0.zza
            com.google.android.gms.measurement.internal.zzam r15 = new com.google.android.gms.measurement.internal.zzam
            r15.<init>(r4)
            java.lang.String r4 = r0.zzc
            long r6 = r0.zzd
            r13 = r5
            r16 = r4
            r17 = r6
            r13.<init>(r14, r15, r16, r17)
            r0 = r5
            goto L_0x007f
        L_0x006b:
            com.google.android.gms.measurement.internal.zzgf r2 = r1.zzj
            com.google.android.gms.measurement.internal.zzfb r2 = r2.zzr()
            com.google.android.gms.measurement.internal.zzfd r2 = r2.zzw()
            java.lang.String r4 = r0.zza
            java.lang.String r0 = r0.zzc
            java.lang.String r5 = "Dropping non-safelisted event. appId, event name, origin"
            r2.zza(r5, r3, r4, r0)
            return
        L_0x007f:
            com.google.android.gms.measurement.internal.zzac r4 = r19.zze()
            r4.zzf()
            com.google.android.gms.measurement.internal.zzac r4 = r19.zze()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r3)     // Catch:{ all -> 0x0392 }
            r4.zzd()     // Catch:{ all -> 0x0392 }
            r4.zzak()     // Catch:{ all -> 0x0392 }
            r5 = 2
            r6 = 0
            r13 = 0
            r14 = 1
            int r8 = (r11 > r6 ? 1 : (r11 == r6 ? 0 : -1))
            if (r8 >= 0) goto L_0x00b6
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzi()     // Catch:{ all -> 0x0392 }
            java.lang.String r8 = "Invalid time querying timed out conditional properties"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzfb.zza(r3)     // Catch:{ all -> 0x0392 }
            java.lang.Long r10 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x0392 }
            r4.zza(r8, r9, r10)     // Catch:{ all -> 0x0392 }
            java.util.List r4 = java.util.Collections.emptyList()     // Catch:{ all -> 0x0392 }
            goto L_0x00c6
        L_0x00b6:
            java.lang.String r8 = "active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout"
            java.lang.String[] r9 = new java.lang.String[r5]     // Catch:{ all -> 0x0392 }
            r9[r13] = r3     // Catch:{ all -> 0x0392 }
            java.lang.String r10 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x0392 }
            r9[r14] = r10     // Catch:{ all -> 0x0392 }
            java.util.List r4 = r4.zza(r8, r9)     // Catch:{ all -> 0x0392 }
        L_0x00c6:
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x0392 }
        L_0x00ca:
            boolean r8 = r4.hasNext()     // Catch:{ all -> 0x0392 }
            if (r8 == 0) goto L_0x0154
            java.lang.Object r8 = r4.next()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzv r8 = (com.google.android.gms.measurement.internal.zzv) r8     // Catch:{ all -> 0x0392 }
            if (r8 == 0) goto L_0x00ca
            boolean r9 = com.google.android.gms.internal.measurement.zzkw.zzb()     // Catch:{ all -> 0x0392 }
            java.lang.String r10 = "User property timed out"
            if (r9 == 0) goto L_0x0114
            com.google.android.gms.measurement.internal.zzgf r9 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzx r9 = r9.zzb()     // Catch:{ all -> 0x0392 }
            java.lang.String r15 = r2.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r14 = com.google.android.gms.measurement.internal.zzap.zzcx     // Catch:{ all -> 0x0392 }
            boolean r9 = r9.zze(r15, r14)     // Catch:{ all -> 0x0392 }
            if (r9 == 0) goto L_0x0114
            com.google.android.gms.measurement.internal.zzgf r9 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfb r9 = r9.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r9 = r9.zzx()     // Catch:{ all -> 0x0392 }
            java.lang.String r14 = r8.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzgf r15 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzez r15 = r15.zzj()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r13 = r8.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r13 = r13.zza     // Catch:{ all -> 0x0392 }
            java.lang.String r13 = r15.zzc(r13)     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r15 = r8.zzc     // Catch:{ all -> 0x0392 }
            java.lang.Object r15 = r15.zza()     // Catch:{ all -> 0x0392 }
            r9.zza(r10, r14, r13, r15)     // Catch:{ all -> 0x0392 }
            goto L_0x0137
        L_0x0114:
            com.google.android.gms.measurement.internal.zzgf r9 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfb r9 = r9.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r9 = r9.zzw()     // Catch:{ all -> 0x0392 }
            java.lang.String r13 = r8.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzgf r14 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzez r14 = r14.zzj()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r15 = r8.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r15 = r15.zza     // Catch:{ all -> 0x0392 }
            java.lang.String r14 = r14.zzc(r15)     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r15 = r8.zzc     // Catch:{ all -> 0x0392 }
            java.lang.Object r15 = r15.zza()     // Catch:{ all -> 0x0392 }
            r9.zza(r10, r13, r14, r15)     // Catch:{ all -> 0x0392 }
        L_0x0137:
            com.google.android.gms.measurement.internal.zzan r9 = r8.zzg     // Catch:{ all -> 0x0392 }
            if (r9 == 0) goto L_0x0145
            com.google.android.gms.measurement.internal.zzan r9 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzan r10 = r8.zzg     // Catch:{ all -> 0x0392 }
            r9.<init>(r10, r11)     // Catch:{ all -> 0x0392 }
            r1.zzb(r9, r2)     // Catch:{ all -> 0x0392 }
        L_0x0145:
            com.google.android.gms.measurement.internal.zzac r9 = r19.zze()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r8 = r8.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r8 = r8.zza     // Catch:{ all -> 0x0392 }
            r9.zze(r3, r8)     // Catch:{ all -> 0x0392 }
            r13 = 0
            r14 = 1
            goto L_0x00ca
        L_0x0154:
            com.google.android.gms.measurement.internal.zzac r4 = r19.zze()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r3)     // Catch:{ all -> 0x0392 }
            r4.zzd()     // Catch:{ all -> 0x0392 }
            r4.zzak()     // Catch:{ all -> 0x0392 }
            int r8 = (r11 > r6 ? 1 : (r11 == r6 ? 0 : -1))
            if (r8 >= 0) goto L_0x017f
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzi()     // Catch:{ all -> 0x0392 }
            java.lang.String r8 = "Invalid time querying expired conditional properties"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzfb.zza(r3)     // Catch:{ all -> 0x0392 }
            java.lang.Long r10 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x0392 }
            r4.zza(r8, r9, r10)     // Catch:{ all -> 0x0392 }
            java.util.List r4 = java.util.Collections.emptyList()     // Catch:{ all -> 0x0392 }
            goto L_0x0191
        L_0x017f:
            java.lang.String r8 = "active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live"
            java.lang.String[] r9 = new java.lang.String[r5]     // Catch:{ all -> 0x0392 }
            r10 = 0
            r9[r10] = r3     // Catch:{ all -> 0x0392 }
            java.lang.String r10 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x0392 }
            r13 = 1
            r9[r13] = r10     // Catch:{ all -> 0x0392 }
            java.util.List r4 = r4.zza(r8, r9)     // Catch:{ all -> 0x0392 }
        L_0x0191:
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ all -> 0x0392 }
            int r9 = r4.size()     // Catch:{ all -> 0x0392 }
            r8.<init>(r9)     // Catch:{ all -> 0x0392 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x0392 }
        L_0x019e:
            boolean r9 = r4.hasNext()     // Catch:{ all -> 0x0392 }
            if (r9 == 0) goto L_0x022d
            java.lang.Object r9 = r4.next()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzv r9 = (com.google.android.gms.measurement.internal.zzv) r9     // Catch:{ all -> 0x0392 }
            if (r9 == 0) goto L_0x019e
            boolean r10 = com.google.android.gms.internal.measurement.zzkw.zzb()     // Catch:{ all -> 0x0392 }
            java.lang.String r13 = "User property expired"
            if (r10 == 0) goto L_0x01e8
            com.google.android.gms.measurement.internal.zzgf r10 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzx r10 = r10.zzb()     // Catch:{ all -> 0x0392 }
            java.lang.String r14 = r2.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r15 = com.google.android.gms.measurement.internal.zzap.zzcx     // Catch:{ all -> 0x0392 }
            boolean r10 = r10.zze(r14, r15)     // Catch:{ all -> 0x0392 }
            if (r10 == 0) goto L_0x01e8
            com.google.android.gms.measurement.internal.zzgf r10 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfb r10 = r10.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r10 = r10.zzx()     // Catch:{ all -> 0x0392 }
            java.lang.String r14 = r9.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzgf r15 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzez r15 = r15.zzj()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r5 = r9.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r5 = r5.zza     // Catch:{ all -> 0x0392 }
            java.lang.String r5 = r15.zzc(r5)     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r15 = r9.zzc     // Catch:{ all -> 0x0392 }
            java.lang.Object r15 = r15.zza()     // Catch:{ all -> 0x0392 }
            r10.zza(r13, r14, r5, r15)     // Catch:{ all -> 0x0392 }
            goto L_0x020b
        L_0x01e8:
            com.google.android.gms.measurement.internal.zzgf r5 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfb r5 = r5.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r5 = r5.zzw()     // Catch:{ all -> 0x0392 }
            java.lang.String r10 = r9.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzgf r14 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzez r14 = r14.zzj()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r15 = r9.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r15 = r15.zza     // Catch:{ all -> 0x0392 }
            java.lang.String r14 = r14.zzc(r15)     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r15 = r9.zzc     // Catch:{ all -> 0x0392 }
            java.lang.Object r15 = r15.zza()     // Catch:{ all -> 0x0392 }
            r5.zza(r13, r10, r14, r15)     // Catch:{ all -> 0x0392 }
        L_0x020b:
            com.google.android.gms.measurement.internal.zzac r5 = r19.zze()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r10 = r9.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r10 = r10.zza     // Catch:{ all -> 0x0392 }
            r5.zzb(r3, r10)     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzan r5 = r9.zzk     // Catch:{ all -> 0x0392 }
            if (r5 == 0) goto L_0x021f
            com.google.android.gms.measurement.internal.zzan r5 = r9.zzk     // Catch:{ all -> 0x0392 }
            r8.add(r5)     // Catch:{ all -> 0x0392 }
        L_0x021f:
            com.google.android.gms.measurement.internal.zzac r5 = r19.zze()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzkq r9 = r9.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r9 = r9.zza     // Catch:{ all -> 0x0392 }
            r5.zze(r3, r9)     // Catch:{ all -> 0x0392 }
            r5 = 2
            goto L_0x019e
        L_0x022d:
            java.util.ArrayList r8 = (java.util.ArrayList) r8     // Catch:{ all -> 0x0392 }
            int r4 = r8.size()     // Catch:{ all -> 0x0392 }
            r5 = 0
        L_0x0234:
            if (r5 >= r4) goto L_0x0247
            java.lang.Object r9 = r8.get(r5)     // Catch:{ all -> 0x0392 }
            int r5 = r5 + 1
            com.google.android.gms.measurement.internal.zzan r9 = (com.google.android.gms.measurement.internal.zzan) r9     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzan r10 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x0392 }
            r10.<init>(r9, r11)     // Catch:{ all -> 0x0392 }
            r1.zzb(r10, r2)     // Catch:{ all -> 0x0392 }
            goto L_0x0234
        L_0x0247:
            com.google.android.gms.measurement.internal.zzac r4 = r19.zze()     // Catch:{ all -> 0x0392 }
            java.lang.String r5 = r0.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r3)     // Catch:{ all -> 0x0392 }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r5)     // Catch:{ all -> 0x0392 }
            r4.zzd()     // Catch:{ all -> 0x0392 }
            r4.zzak()     // Catch:{ all -> 0x0392 }
            int r8 = (r11 > r6 ? 1 : (r11 == r6 ? 0 : -1))
            if (r8 >= 0) goto L_0x0280
            com.google.android.gms.measurement.internal.zzfb r6 = r4.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r6 = r6.zzi()     // Catch:{ all -> 0x0392 }
            java.lang.String r7 = "Invalid time querying triggered conditional properties"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.zzfb.zza(r3)     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzez r4 = r4.zzo()     // Catch:{ all -> 0x0392 }
            java.lang.String r4 = r4.zza(r5)     // Catch:{ all -> 0x0392 }
            java.lang.Long r5 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x0392 }
            r6.zza(r7, r3, r4, r5)     // Catch:{ all -> 0x0392 }
            java.util.List r3 = java.util.Collections.emptyList()     // Catch:{ all -> 0x0392 }
            r13 = 0
            goto L_0x0296
        L_0x0280:
            java.lang.String r6 = "active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout"
            r7 = 3
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ all -> 0x0392 }
            r13 = 0
            r7[r13] = r3     // Catch:{ all -> 0x0392 }
            r3 = 1
            r7[r3] = r5     // Catch:{ all -> 0x0392 }
            java.lang.String r3 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x0392 }
            r5 = 2
            r7[r5] = r3     // Catch:{ all -> 0x0392 }
            java.util.List r3 = r4.zza(r6, r7)     // Catch:{ all -> 0x0392 }
        L_0x0296:
            java.util.ArrayList r14 = new java.util.ArrayList     // Catch:{ all -> 0x0392 }
            int r4 = r3.size()     // Catch:{ all -> 0x0392 }
            r14.<init>(r4)     // Catch:{ all -> 0x0392 }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x0392 }
        L_0x02a3:
            boolean r4 = r3.hasNext()     // Catch:{ all -> 0x0392 }
            if (r4 == 0) goto L_0x0366
            java.lang.Object r4 = r3.next()     // Catch:{ all -> 0x0392 }
            r15 = r4
            com.google.android.gms.measurement.internal.zzv r15 = (com.google.android.gms.measurement.internal.zzv) r15     // Catch:{ all -> 0x0392 }
            if (r15 == 0) goto L_0x02a3
            com.google.android.gms.measurement.internal.zzkq r4 = r15.zzc     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzks r10 = new com.google.android.gms.measurement.internal.zzks     // Catch:{ all -> 0x0392 }
            java.lang.String r5 = r15.zza     // Catch:{ all -> 0x0392 }
            java.lang.String r6 = r15.zzb     // Catch:{ all -> 0x0392 }
            java.lang.String r7 = r4.zza     // Catch:{ all -> 0x0392 }
            java.lang.Object r16 = r4.zza()     // Catch:{ all -> 0x0392 }
            r4 = r10
            r8 = r11
            r13 = r10
            r10 = r16
            r4.<init>(r5, r6, r7, r8, r10)     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzac r4 = r19.zze()     // Catch:{ all -> 0x0392 }
            boolean r4 = r4.zza(r13)     // Catch:{ all -> 0x0392 }
            if (r4 == 0) goto L_0x0326
            boolean r4 = com.google.android.gms.internal.measurement.zzkw.zzb()     // Catch:{ all -> 0x0392 }
            java.lang.String r5 = "User property triggered"
            if (r4 == 0) goto L_0x0308
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzx r4 = r4.zzb()     // Catch:{ all -> 0x0392 }
            java.lang.String r6 = r2.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.zzap.zzcx     // Catch:{ all -> 0x0392 }
            boolean r4 = r4.zze(r6, r7)     // Catch:{ all -> 0x0392 }
            if (r4 == 0) goto L_0x0308
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzx()     // Catch:{ all -> 0x0392 }
            java.lang.String r6 = r15.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzez r7 = r7.zzj()     // Catch:{ all -> 0x0392 }
            java.lang.String r8 = r13.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r7 = r7.zzc(r8)     // Catch:{ all -> 0x0392 }
            java.lang.Object r8 = r13.zze     // Catch:{ all -> 0x0392 }
            r4.zza(r5, r6, r7, r8)     // Catch:{ all -> 0x0392 }
            goto L_0x0349
        L_0x0308:
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzw()     // Catch:{ all -> 0x0392 }
            java.lang.String r6 = r15.zza     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzez r7 = r7.zzj()     // Catch:{ all -> 0x0392 }
            java.lang.String r8 = r13.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r7 = r7.zzc(r8)     // Catch:{ all -> 0x0392 }
            java.lang.Object r8 = r13.zze     // Catch:{ all -> 0x0392 }
            r4.zza(r5, r6, r7, r8)     // Catch:{ all -> 0x0392 }
            goto L_0x0349
        L_0x0326:
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzf()     // Catch:{ all -> 0x0392 }
            java.lang.String r5 = "Too many active user properties, ignoring"
            java.lang.String r6 = r15.zza     // Catch:{ all -> 0x0392 }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.zzfb.zza(r6)     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzez r7 = r7.zzj()     // Catch:{ all -> 0x0392 }
            java.lang.String r8 = r13.zzc     // Catch:{ all -> 0x0392 }
            java.lang.String r7 = r7.zzc(r8)     // Catch:{ all -> 0x0392 }
            java.lang.Object r8 = r13.zze     // Catch:{ all -> 0x0392 }
            r4.zza(r5, r6, r7, r8)     // Catch:{ all -> 0x0392 }
        L_0x0349:
            com.google.android.gms.measurement.internal.zzan r4 = r15.zzi     // Catch:{ all -> 0x0392 }
            if (r4 == 0) goto L_0x0352
            com.google.android.gms.measurement.internal.zzan r4 = r15.zzi     // Catch:{ all -> 0x0392 }
            r14.add(r4)     // Catch:{ all -> 0x0392 }
        L_0x0352:
            com.google.android.gms.measurement.internal.zzkq r4 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x0392 }
            r4.<init>(r13)     // Catch:{ all -> 0x0392 }
            r15.zzc = r4     // Catch:{ all -> 0x0392 }
            r4 = 1
            r15.zze = r4     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzac r5 = r19.zze()     // Catch:{ all -> 0x0392 }
            r5.zza(r15)     // Catch:{ all -> 0x0392 }
            r13 = 0
            goto L_0x02a3
        L_0x0366:
            r1.zzb(r0, r2)     // Catch:{ all -> 0x0392 }
            java.util.ArrayList r14 = (java.util.ArrayList) r14     // Catch:{ all -> 0x0392 }
            int r0 = r14.size()     // Catch:{ all -> 0x0392 }
            r3 = 0
        L_0x0370:
            if (r3 >= r0) goto L_0x0383
            java.lang.Object r4 = r14.get(r3)     // Catch:{ all -> 0x0392 }
            int r3 = r3 + 1
            com.google.android.gms.measurement.internal.zzan r4 = (com.google.android.gms.measurement.internal.zzan) r4     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzan r5 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x0392 }
            r5.<init>(r4, r11)     // Catch:{ all -> 0x0392 }
            r1.zzb(r5, r2)     // Catch:{ all -> 0x0392 }
            goto L_0x0370
        L_0x0383:
            com.google.android.gms.measurement.internal.zzac r0 = r19.zze()     // Catch:{ all -> 0x0392 }
            r0.b_()     // Catch:{ all -> 0x0392 }
            com.google.android.gms.measurement.internal.zzac r0 = r19.zze()
            r0.zzh()
            return
        L_0x0392:
            r0 = move-exception
            com.google.android.gms.measurement.internal.zzac r2 = r19.zze()
            r2.zzh()
            goto L_0x039c
        L_0x039b:
            throw r0
        L_0x039c:
            goto L_0x039b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzkj.zza(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzkv.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
     arg types: [android.os.Bundle, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.zzkv.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(android.os.Bundle, java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x08a1 A[Catch:{ SQLiteException -> 0x023a, all -> 0x092c }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0272 A[Catch:{ SQLiteException -> 0x023a, all -> 0x092c }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x02a9 A[Catch:{ SQLiteException -> 0x023a, all -> 0x092c }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x02f7 A[Catch:{ SQLiteException -> 0x023a, all -> 0x092c }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0324  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzb(com.google.android.gms.measurement.internal.zzan r28, com.google.android.gms.measurement.internal.zzm r29) {
        /*
            r27 = this;
            r1 = r27
            r2 = r28
            r3 = r29
            java.lang.String r4 = "_s"
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r29)
            java.lang.String r5 = r3.zza
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r5)
            long r5 = java.lang.System.nanoTime()
            r27.zzw()
            r27.zzk()
            java.lang.String r15 = r3.zza
            com.google.android.gms.measurement.internal.zzkr r7 = r27.zzh()
            boolean r7 = r7.zza(r2, r3)
            if (r7 != 0) goto L_0x0027
            return
        L_0x0027:
            boolean r7 = r3.zzh
            if (r7 != 0) goto L_0x002f
            r1.zzc(r3)
            return
        L_0x002f:
            com.google.android.gms.measurement.internal.zzfz r7 = r27.zzc()
            java.lang.String r8 = r2.zza
            boolean r7 = r7.zzb(r15, r8)
            java.lang.String r14 = "_err"
            r13 = 0
            r12 = 1
            if (r7 == 0) goto L_0x00db
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzi()
            java.lang.Object r4 = com.google.android.gms.measurement.internal.zzfb.zza(r15)
            com.google.android.gms.measurement.internal.zzgf r5 = r1.zzj
            com.google.android.gms.measurement.internal.zzez r5 = r5.zzj()
            java.lang.String r6 = r2.zza
            java.lang.String r5 = r5.zza(r6)
            java.lang.String r6 = "Dropping blacklisted event. appId"
            r3.zza(r6, r4, r5)
            com.google.android.gms.measurement.internal.zzfz r3 = r27.zzc()
            boolean r3 = r3.zzg(r15)
            if (r3 != 0) goto L_0x0075
            com.google.android.gms.measurement.internal.zzfz r3 = r27.zzc()
            boolean r3 = r3.zzh(r15)
            if (r3 == 0) goto L_0x0073
            goto L_0x0075
        L_0x0073:
            r3 = 0
            goto L_0x0076
        L_0x0075:
            r3 = 1
        L_0x0076:
            if (r3 != 0) goto L_0x0091
            java.lang.String r4 = r2.zza
            boolean r4 = r14.equals(r4)
            if (r4 != 0) goto L_0x0091
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj
            com.google.android.gms.measurement.internal.zzkv r7 = r4.zzi()
            r9 = 11
            java.lang.String r11 = r2.zza
            r12 = 0
            java.lang.String r10 = "_ev"
            r8 = r15
            r7.zza(r8, r9, r10, r11, r12)
        L_0x0091:
            if (r3 == 0) goto L_0x00da
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()
            com.google.android.gms.measurement.internal.zzg r2 = r2.zzb(r15)
            if (r2 == 0) goto L_0x00da
            long r3 = r2.zzu()
            long r5 = r2.zzt()
            long r3 = java.lang.Math.max(r3, r5)
            com.google.android.gms.measurement.internal.zzgf r5 = r1.zzj
            com.google.android.gms.common.util.Clock r5 = r5.zzm()
            long r5 = r5.currentTimeMillis()
            long r5 = r5 - r3
            long r3 = java.lang.Math.abs(r5)
            com.google.android.gms.measurement.internal.zzeu<java.lang.Long> r5 = com.google.android.gms.measurement.internal.zzap.zzy
            java.lang.Object r5 = r5.zza(r13)
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x00da
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzw()
            java.lang.String r4 = "Fetching config for blacklisted app"
            r3.zza(r4)
            r1.zza(r2)
        L_0x00da:
            return
        L_0x00db:
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj
            com.google.android.gms.measurement.internal.zzfb r7 = r7.zzr()
            r10 = 2
            boolean r7 = r7.zza(r10)
            if (r7 == 0) goto L_0x0101
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj
            com.google.android.gms.measurement.internal.zzfb r7 = r7.zzr()
            com.google.android.gms.measurement.internal.zzfd r7 = r7.zzx()
            com.google.android.gms.measurement.internal.zzgf r8 = r1.zzj
            com.google.android.gms.measurement.internal.zzez r8 = r8.zzj()
            java.lang.String r8 = r8.zza(r2)
            java.lang.String r9 = "Logging event"
            r7.zza(r9, r8)
        L_0x0101:
            com.google.android.gms.measurement.internal.zzac r7 = r27.zze()
            r7.zzf()
            r1.zzc(r3)     // Catch:{ all -> 0x092c }
            java.lang.String r7 = "_iap"
            java.lang.String r8 = r2.zza     // Catch:{ all -> 0x092c }
            boolean r7 = r7.equals(r8)     // Catch:{ all -> 0x092c }
            java.lang.String r8 = "ecommerce_purchase"
            if (r7 != 0) goto L_0x0125
            java.lang.String r7 = r2.zza     // Catch:{ all -> 0x092c }
            boolean r7 = r8.equals(r7)     // Catch:{ all -> 0x092c }
            if (r7 == 0) goto L_0x0120
            goto L_0x0125
        L_0x0120:
            r23 = r5
            r6 = 0
            goto L_0x02b8
        L_0x0125:
            com.google.android.gms.measurement.internal.zzam r7 = r2.zzb     // Catch:{ all -> 0x092c }
            java.lang.String r9 = "currency"
            java.lang.String r7 = r7.zzd(r9)     // Catch:{ all -> 0x092c }
            java.lang.String r9 = r2.zza     // Catch:{ all -> 0x092c }
            boolean r8 = r8.equals(r9)     // Catch:{ all -> 0x092c }
            java.lang.String r9 = "value"
            if (r8 == 0) goto L_0x018c
            com.google.android.gms.measurement.internal.zzam r8 = r2.zzb     // Catch:{ all -> 0x092c }
            java.lang.Double r8 = r8.zzc(r9)     // Catch:{ all -> 0x092c }
            double r16 = r8.doubleValue()     // Catch:{ all -> 0x092c }
            r18 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r16 = r16 * r18
            r20 = 0
            int r8 = (r16 > r20 ? 1 : (r16 == r20 ? 0 : -1))
            if (r8 != 0) goto L_0x015e
            com.google.android.gms.measurement.internal.zzam r8 = r2.zzb     // Catch:{ all -> 0x092c }
            java.lang.Long r8 = r8.zzb(r9)     // Catch:{ all -> 0x092c }
            long r8 = r8.longValue()     // Catch:{ all -> 0x092c }
            double r8 = (double) r8
            java.lang.Double.isNaN(r8)
            double r16 = r8 * r18
        L_0x015e:
            r8 = 4890909195324358656(0x43e0000000000000, double:9.223372036854776E18)
            int r18 = (r16 > r8 ? 1 : (r16 == r8 ? 0 : -1))
            if (r18 > 0) goto L_0x016f
            r8 = -4332462841530417152(0xc3e0000000000000, double:-9.223372036854776E18)
            int r18 = (r16 > r8 ? 1 : (r16 == r8 ? 0 : -1))
            if (r18 < 0) goto L_0x016f
            long r8 = java.lang.Math.round(r16)     // Catch:{ all -> 0x092c }
            goto L_0x0196
        L_0x016f:
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r7 = r7.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r7 = r7.zzi()     // Catch:{ all -> 0x092c }
            java.lang.String r8 = "Data lost. Currency value is too big. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzfb.zza(r15)     // Catch:{ all -> 0x092c }
            java.lang.Double r10 = java.lang.Double.valueOf(r16)     // Catch:{ all -> 0x092c }
            r7.zza(r8, r9, r10)     // Catch:{ all -> 0x092c }
            r23 = r5
            r6 = 0
            r11 = 0
            goto L_0x02a7
        L_0x018c:
            com.google.android.gms.measurement.internal.zzam r8 = r2.zzb     // Catch:{ all -> 0x092c }
            java.lang.Long r8 = r8.zzb(r9)     // Catch:{ all -> 0x092c }
            long r8 = r8.longValue()     // Catch:{ all -> 0x092c }
        L_0x0196:
            boolean r10 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x092c }
            if (r10 != 0) goto L_0x02a3
            java.util.Locale r10 = java.util.Locale.US     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r7.toUpperCase(r10)     // Catch:{ all -> 0x092c }
            java.lang.String r10 = "[A-Z]{3}"
            boolean r10 = r7.matches(r10)     // Catch:{ all -> 0x092c }
            if (r10 == 0) goto L_0x02a3
            java.lang.String r10 = "_ltv_"
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ all -> 0x092c }
            int r16 = r7.length()     // Catch:{ all -> 0x092c }
            if (r16 == 0) goto L_0x01bb
            java.lang.String r7 = r10.concat(r7)     // Catch:{ all -> 0x092c }
            goto L_0x01c0
        L_0x01bb:
            java.lang.String r7 = new java.lang.String     // Catch:{ all -> 0x092c }
            r7.<init>(r10)     // Catch:{ all -> 0x092c }
        L_0x01c0:
            r10 = r7
            com.google.android.gms.measurement.internal.zzac r7 = r27.zze()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzks r7 = r7.zzc(r15, r10)     // Catch:{ all -> 0x092c }
            if (r7 == 0) goto L_0x0201
            java.lang.Object r11 = r7.zze     // Catch:{ all -> 0x092c }
            boolean r11 = r11 instanceof java.lang.Long     // Catch:{ all -> 0x092c }
            if (r11 != 0) goto L_0x01d2
            goto L_0x0201
        L_0x01d2:
            java.lang.Object r7 = r7.zze     // Catch:{ all -> 0x092c }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x092c }
            long r19 = r7.longValue()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzks r17 = new com.google.android.gms.measurement.internal.zzks     // Catch:{ all -> 0x092c }
            java.lang.String r11 = r2.zzc     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.common.util.Clock r7 = r7.zzm()     // Catch:{ all -> 0x092c }
            long r21 = r7.currentTimeMillis()     // Catch:{ all -> 0x092c }
            long r19 = r19 + r8
            java.lang.Long r19 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x092c }
            r7 = r17
            r8 = r15
            r9 = r11
            r11 = 2
            r23 = r5
            r5 = 1
            r6 = 0
            r11 = r21
            r13 = r19
            r7.<init>(r8, r9, r10, r11, r13)     // Catch:{ all -> 0x092c }
            r5 = r17
            goto L_0x0268
        L_0x0201:
            r23 = r5
            r5 = 1
            r6 = 0
            com.google.android.gms.measurement.internal.zzac r7 = r27.zze()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r11 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r11 = r11.zzb()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Integer> r12 = com.google.android.gms.measurement.internal.zzap.zzad     // Catch:{ all -> 0x092c }
            int r11 = r11.zzb(r15, r12)     // Catch:{ all -> 0x092c }
            int r11 = r11 - r5
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r15)     // Catch:{ all -> 0x092c }
            r7.zzd()     // Catch:{ all -> 0x092c }
            r7.zzak()     // Catch:{ all -> 0x092c }
            android.database.sqlite.SQLiteDatabase r12 = r7.c_()     // Catch:{ SQLiteException -> 0x023a }
            java.lang.String r13 = "delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);"
            r5 = 3
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x023a }
            r5[r6] = r15     // Catch:{ SQLiteException -> 0x023a }
            r16 = 1
            r5[r16] = r15     // Catch:{ SQLiteException -> 0x023a }
            java.lang.String r11 = java.lang.String.valueOf(r11)     // Catch:{ SQLiteException -> 0x023a }
            r16 = 2
            r5[r16] = r11     // Catch:{ SQLiteException -> 0x023a }
            r12.execSQL(r13, r5)     // Catch:{ SQLiteException -> 0x023a }
            goto L_0x024d
        L_0x023a:
            r0 = move-exception
            r5 = r0
            com.google.android.gms.measurement.internal.zzfb r7 = r7.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r7 = r7.zzf()     // Catch:{ all -> 0x092c }
            java.lang.String r11 = "Error pruning currencies. appId"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.zzfb.zza(r15)     // Catch:{ all -> 0x092c }
            r7.zza(r11, r12, r5)     // Catch:{ all -> 0x092c }
        L_0x024d:
            com.google.android.gms.measurement.internal.zzks r5 = new com.google.android.gms.measurement.internal.zzks     // Catch:{ all -> 0x092c }
            java.lang.String r11 = r2.zzc     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.common.util.Clock r7 = r7.zzm()     // Catch:{ all -> 0x092c }
            long r12 = r7.currentTimeMillis()     // Catch:{ all -> 0x092c }
            java.lang.Long r16 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x092c }
            r7 = r5
            r8 = r15
            r9 = r11
            r11 = r12
            r13 = r16
            r7.<init>(r8, r9, r10, r11, r13)     // Catch:{ all -> 0x092c }
        L_0x0268:
            com.google.android.gms.measurement.internal.zzac r7 = r27.zze()     // Catch:{ all -> 0x092c }
            boolean r7 = r7.zza(r5)     // Catch:{ all -> 0x092c }
            if (r7 != 0) goto L_0x02a6
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r7 = r7.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r7 = r7.zzf()     // Catch:{ all -> 0x092c }
            java.lang.String r8 = "Too many unique user properties are set. Ignoring user property. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzfb.zza(r15)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r10 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzez r10 = r10.zzj()     // Catch:{ all -> 0x092c }
            java.lang.String r11 = r5.zzc     // Catch:{ all -> 0x092c }
            java.lang.String r10 = r10.zzc(r11)     // Catch:{ all -> 0x092c }
            java.lang.Object r5 = r5.zze     // Catch:{ all -> 0x092c }
            r7.zza(r8, r9, r10, r5)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r5 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkv r7 = r5.zzi()     // Catch:{ all -> 0x092c }
            r9 = 9
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r15
            r7.zza(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x092c }
            goto L_0x02a6
        L_0x02a3:
            r23 = r5
            r6 = 0
        L_0x02a6:
            r11 = 1
        L_0x02a7:
            if (r11 != 0) goto L_0x02b8
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()     // Catch:{ all -> 0x092c }
            r2.b_()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()
            r2.zzh()
            return
        L_0x02b8:
            java.lang.String r5 = r2.zza     // Catch:{ all -> 0x092c }
            boolean r5 = com.google.android.gms.measurement.internal.zzkv.zza(r5)     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r2.zza     // Catch:{ all -> 0x092c }
            boolean r16 = r14.equals(r7)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r7 = r27.zze()     // Catch:{ all -> 0x092c }
            long r8 = r27.zzx()     // Catch:{ all -> 0x092c }
            r11 = 1
            r13 = 0
            r17 = 0
            r10 = r15
            r12 = r5
            r14 = r16
            r18 = r15
            r15 = r17
            com.google.android.gms.measurement.internal.zzab r7 = r7.zza(r8, r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x092c }
            long r8 = r7.zzb     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Integer> r10 = com.google.android.gms.measurement.internal.zzap.zzj     // Catch:{ all -> 0x092c }
            r14 = 0
            java.lang.Object r10 = r10.zza(r14)     // Catch:{ all -> 0x092c }
            java.lang.Integer r10 = (java.lang.Integer) r10     // Catch:{ all -> 0x092c }
            int r10 = r10.intValue()     // Catch:{ all -> 0x092c }
            long r10 = (long) r10     // Catch:{ all -> 0x092c }
            long r8 = r8 - r10
            r10 = 1000(0x3e8, double:4.94E-321)
            r12 = 1
            r14 = 0
            int r17 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r17 <= 0) goto L_0x0324
            long r8 = r8 % r10
            int r2 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r2 != 0) goto L_0x0315
            com.google.android.gms.measurement.internal.zzgf r2 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r2 = r2.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r2 = r2.zzf()     // Catch:{ all -> 0x092c }
            java.lang.String r3 = "Data loss. Too many events logged. appId, count"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.zzfb.zza(r18)     // Catch:{ all -> 0x092c }
            long r5 = r7.zzb     // Catch:{ all -> 0x092c }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x092c }
            r2.zza(r3, r4, r5)     // Catch:{ all -> 0x092c }
        L_0x0315:
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()     // Catch:{ all -> 0x092c }
            r2.b_()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()
            r2.zzh()
            return
        L_0x0324:
            if (r5 == 0) goto L_0x037c
            long r8 = r7.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Integer> r6 = com.google.android.gms.measurement.internal.zzap.zzl     // Catch:{ all -> 0x092c }
            r12 = 0
            java.lang.Object r6 = r6.zza(r12)     // Catch:{ all -> 0x092c }
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch:{ all -> 0x092c }
            int r6 = r6.intValue()     // Catch:{ all -> 0x092c }
            long r12 = (long) r6     // Catch:{ all -> 0x092c }
            long r8 = r8 - r12
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 <= 0) goto L_0x037c
            long r8 = r8 % r10
            r3 = 1
            int r5 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x035b
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzf()     // Catch:{ all -> 0x092c }
            java.lang.String r4 = "Data loss. Too many public events logged. appId, count"
            java.lang.Object r5 = com.google.android.gms.measurement.internal.zzfb.zza(r18)     // Catch:{ all -> 0x092c }
            long r6 = r7.zza     // Catch:{ all -> 0x092c }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x092c }
            r3.zza(r4, r5, r6)     // Catch:{ all -> 0x092c }
        L_0x035b:
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkv r7 = r3.zzi()     // Catch:{ all -> 0x092c }
            r9 = 16
            java.lang.String r10 = "_ev"
            java.lang.String r11 = r2.zza     // Catch:{ all -> 0x092c }
            r12 = 0
            r8 = r18
            r7.zza(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()     // Catch:{ all -> 0x092c }
            r2.b_()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()
            r2.zzh()
            return
        L_0x037c:
            if (r16 == 0) goto L_0x03ce
            long r8 = r7.zzd     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r6 = r6.zzb()     // Catch:{ all -> 0x092c }
            java.lang.String r10 = r3.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Integer> r11 = com.google.android.gms.measurement.internal.zzap.zzk     // Catch:{ all -> 0x092c }
            int r6 = r6.zzb(r10, r11)     // Catch:{ all -> 0x092c }
            r10 = 1000000(0xf4240, float:1.401298E-39)
            int r6 = java.lang.Math.min(r10, r6)     // Catch:{ all -> 0x092c }
            r12 = 0
            int r6 = java.lang.Math.max(r12, r6)     // Catch:{ all -> 0x092c }
            long r10 = (long) r6     // Catch:{ all -> 0x092c }
            long r8 = r8 - r10
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 <= 0) goto L_0x03cf
            r10 = 1
            int r2 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r2 != 0) goto L_0x03bf
            com.google.android.gms.measurement.internal.zzgf r2 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r2 = r2.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r2 = r2.zzf()     // Catch:{ all -> 0x092c }
            java.lang.String r3 = "Too many error events logged. appId, count"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.zzfb.zza(r18)     // Catch:{ all -> 0x092c }
            long r5 = r7.zzd     // Catch:{ all -> 0x092c }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x092c }
            r2.zza(r3, r4, r5)     // Catch:{ all -> 0x092c }
        L_0x03bf:
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()     // Catch:{ all -> 0x092c }
            r2.b_()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()
            r2.zzh()
            return
        L_0x03ce:
            r12 = 0
        L_0x03cf:
            com.google.android.gms.measurement.internal.zzam r6 = r2.zzb     // Catch:{ all -> 0x092c }
            android.os.Bundle r6 = r6.zzb()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkv r7 = r7.zzi()     // Catch:{ all -> 0x092c }
            java.lang.String r8 = "_o"
            java.lang.String r9 = r2.zzc     // Catch:{ all -> 0x092c }
            r7.zza(r6, r8, r9)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkv r7 = r7.zzi()     // Catch:{ all -> 0x092c }
            r13 = r18
            boolean r7 = r7.zzf(r13)     // Catch:{ all -> 0x092c }
            java.lang.String r11 = "_r"
            if (r7 == 0) goto L_0x0410
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkv r7 = r7.zzi()     // Catch:{ all -> 0x092c }
            java.lang.String r8 = "_dbg"
            r9 = 1
            java.lang.Long r12 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x092c }
            r7.zza(r6, r8, r12)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkv r7 = r7.zzi()     // Catch:{ all -> 0x092c }
            java.lang.Long r8 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x092c }
            r7.zza(r6, r11, r8)     // Catch:{ all -> 0x092c }
        L_0x0410:
            java.lang.String r7 = r2.zza     // Catch:{ all -> 0x092c }
            boolean r7 = r4.equals(r7)     // Catch:{ all -> 0x092c }
            java.lang.String r8 = "_sno"
            if (r7 == 0) goto L_0x0447
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r7 = r7.zzb()     // Catch:{ all -> 0x092c }
            java.lang.String r9 = r3.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r10 = com.google.android.gms.measurement.internal.zzap.zzap     // Catch:{ all -> 0x092c }
            boolean r7 = r7.zze(r9, r10)     // Catch:{ all -> 0x092c }
            if (r7 == 0) goto L_0x0447
            com.google.android.gms.measurement.internal.zzac r7 = r27.zze()     // Catch:{ all -> 0x092c }
            java.lang.String r9 = r3.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzks r7 = r7.zzc(r9, r8)     // Catch:{ all -> 0x092c }
            if (r7 == 0) goto L_0x0447
            java.lang.Object r9 = r7.zze     // Catch:{ all -> 0x092c }
            boolean r9 = r9 instanceof java.lang.Long     // Catch:{ all -> 0x092c }
            if (r9 == 0) goto L_0x0447
            com.google.android.gms.measurement.internal.zzgf r9 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkv r9 = r9.zzi()     // Catch:{ all -> 0x092c }
            java.lang.Object r7 = r7.zze     // Catch:{ all -> 0x092c }
            r9.zza(r6, r8, r7)     // Catch:{ all -> 0x092c }
        L_0x0447:
            java.lang.String r7 = r2.zza     // Catch:{ all -> 0x092c }
            boolean r4 = r4.equals(r7)     // Catch:{ all -> 0x092c }
            if (r4 == 0) goto L_0x0479
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r4 = r4.zzb()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.zzap.zzat     // Catch:{ all -> 0x092c }
            boolean r4 = r4.zze(r7, r9)     // Catch:{ all -> 0x092c }
            if (r4 == 0) goto L_0x0479
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r4 = r4.zzb()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.zzap.zzap     // Catch:{ all -> 0x092c }
            boolean r4 = r4.zze(r7, r9)     // Catch:{ all -> 0x092c }
            if (r4 != 0) goto L_0x0479
            com.google.android.gms.measurement.internal.zzkq r4 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x092c }
            r12 = 0
            r4.<init>(r8, r14, r12)     // Catch:{ all -> 0x092c }
            r1.zzb(r4, r3)     // Catch:{ all -> 0x092c }
            goto L_0x047a
        L_0x0479:
            r12 = 0
        L_0x047a:
            com.google.android.gms.measurement.internal.zzac r4 = r27.zze()     // Catch:{ all -> 0x092c }
            long r7 = r4.zzc(r13)     // Catch:{ all -> 0x092c }
            int r4 = (r7 > r14 ? 1 : (r7 == r14 ? 0 : -1))
            if (r4 <= 0) goto L_0x049d
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzi()     // Catch:{ all -> 0x092c }
            java.lang.String r9 = "Data lost. Too many events stored on disk, deleted. appId"
            java.lang.Object r10 = com.google.android.gms.measurement.internal.zzfb.zza(r13)     // Catch:{ all -> 0x092c }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x092c }
            r4.zza(r9, r10, r7)     // Catch:{ all -> 0x092c }
        L_0x049d:
            com.google.android.gms.measurement.internal.zzak r4 = new com.google.android.gms.measurement.internal.zzak     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r8 = r1.zzj     // Catch:{ all -> 0x092c }
            java.lang.String r9 = r2.zzc     // Catch:{ all -> 0x092c }
            java.lang.String r10 = r2.zza     // Catch:{ all -> 0x092c }
            long r14 = r2.zzd     // Catch:{ all -> 0x092c }
            r19 = 0
            r7 = r4
            r2 = r10
            r10 = r13
            r26 = r11
            r11 = r2
            r16 = r12
            r2 = r13
            r25 = 0
            r12 = r14
            r28 = r16
            r14 = r19
            r16 = r6
            r7.<init>(r8, r9, r10, r11, r12, r14, r16)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r6 = r27.zze()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r4.zzb     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzaj r6 = r6.zza(r2, r7)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x0530
            com.google.android.gms.measurement.internal.zzac r6 = r27.zze()     // Catch:{ all -> 0x092c }
            long r6 = r6.zzh(r2)     // Catch:{ all -> 0x092c }
            r8 = 500(0x1f4, double:2.47E-321)
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 < 0) goto L_0x0516
            if (r5 == 0) goto L_0x0516
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzf()     // Catch:{ all -> 0x092c }
            java.lang.String r5 = "Too many event names used, ignoring event. appId, name, supported count"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.zzfb.zza(r2)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzez r7 = r7.zzj()     // Catch:{ all -> 0x092c }
            java.lang.String r4 = r4.zzb     // Catch:{ all -> 0x092c }
            java.lang.String r4 = r7.zza(r4)     // Catch:{ all -> 0x092c }
            r7 = 500(0x1f4, float:7.0E-43)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x092c }
            r3.zza(r5, r6, r4, r7)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkv r7 = r3.zzi()     // Catch:{ all -> 0x092c }
            r9 = 8
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r2
            r7.zza(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()
            r2.zzh()
            return
        L_0x0516:
            com.google.android.gms.measurement.internal.zzaj r5 = new com.google.android.gms.measurement.internal.zzaj     // Catch:{ all -> 0x092c }
            java.lang.String r9 = r4.zzb     // Catch:{ all -> 0x092c }
            r10 = 0
            r12 = 0
            long r14 = r4.zzc     // Catch:{ all -> 0x092c }
            r16 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r7 = r5
            r8 = r2
            r7.<init>(r8, r9, r10, r12, r14, r16, r18, r19, r20, r21)     // Catch:{ all -> 0x092c }
            goto L_0x053e
        L_0x0530:
            com.google.android.gms.measurement.internal.zzgf r2 = r1.zzj     // Catch:{ all -> 0x092c }
            long r7 = r6.zzf     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzak r4 = r4.zza(r2, r7)     // Catch:{ all -> 0x092c }
            long r7 = r4.zzc     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzaj r5 = r6.zza(r7)     // Catch:{ all -> 0x092c }
        L_0x053e:
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()     // Catch:{ all -> 0x092c }
            r2.zza(r5)     // Catch:{ all -> 0x092c }
            r27.zzw()     // Catch:{ all -> 0x092c }
            r27.zzk()     // Catch:{ all -> 0x092c }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r4)     // Catch:{ all -> 0x092c }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r29)     // Catch:{ all -> 0x092c }
            java.lang.String r2 = r4.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r2)     // Catch:{ all -> 0x092c }
            java.lang.String r2 = r4.zza     // Catch:{ all -> 0x092c }
            java.lang.String r5 = r3.zza     // Catch:{ all -> 0x092c }
            boolean r2 = r2.equals(r5)     // Catch:{ all -> 0x092c }
            com.google.android.gms.common.internal.Preconditions.checkArgument(r2)     // Catch:{ all -> 0x092c }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r2 = com.google.android.gms.internal.measurement.zzbs.zzg.zzbf()     // Catch:{ all -> 0x092c }
            r5 = 1
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r2 = r2.zza(r5)     // Catch:{ all -> 0x092c }
            java.lang.String r6 = "android"
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r2 = r2.zza(r6)     // Catch:{ all -> 0x092c }
            java.lang.String r6 = r3.zza     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x057d
            java.lang.String r6 = r3.zza     // Catch:{ all -> 0x092c }
            r2.zzf(r6)     // Catch:{ all -> 0x092c }
        L_0x057d:
            java.lang.String r6 = r3.zzd     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x058a
            java.lang.String r6 = r3.zzd     // Catch:{ all -> 0x092c }
            r2.zze(r6)     // Catch:{ all -> 0x092c }
        L_0x058a:
            java.lang.String r6 = r3.zzc     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x0597
            java.lang.String r6 = r3.zzc     // Catch:{ all -> 0x092c }
            r2.zzg(r6)     // Catch:{ all -> 0x092c }
        L_0x0597:
            long r6 = r3.zzj     // Catch:{ all -> 0x092c }
            r8 = -2147483648(0xffffffff80000000, double:NaN)
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x05a6
            long r6 = r3.zzj     // Catch:{ all -> 0x092c }
            int r7 = (int) r6     // Catch:{ all -> 0x092c }
            r2.zzh(r7)     // Catch:{ all -> 0x092c }
        L_0x05a6:
            long r6 = r3.zze     // Catch:{ all -> 0x092c }
            r2.zzf(r6)     // Catch:{ all -> 0x092c }
            java.lang.String r6 = r3.zzb     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x05b8
            java.lang.String r6 = r3.zzb     // Catch:{ all -> 0x092c }
            r2.zzk(r6)     // Catch:{ all -> 0x092c }
        L_0x05b8:
            boolean r6 = com.google.android.gms.internal.measurement.zzln.zzb()     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x0607
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r6 = r6.zzb()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r8 = com.google.android.gms.measurement.internal.zzap.zzcf     // Catch:{ all -> 0x092c }
            boolean r6 = r6.zze(r7, r8)     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x0607
            java.lang.String r6 = r2.zzl()     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x05e5
            java.lang.String r6 = r3.zzv     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x05e5
            java.lang.String r6 = r3.zzv     // Catch:{ all -> 0x092c }
            r2.zzp(r6)     // Catch:{ all -> 0x092c }
        L_0x05e5:
            java.lang.String r6 = r2.zzl()     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x063a
            java.lang.String r6 = r2.zzo()     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x063a
            java.lang.String r6 = r3.zzr     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x063a
            java.lang.String r6 = r3.zzr     // Catch:{ all -> 0x092c }
            r2.zzo(r6)     // Catch:{ all -> 0x092c }
            goto L_0x063a
        L_0x0607:
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r6 = r6.zzb()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.zzap.zzbf     // Catch:{ all -> 0x092c }
            boolean r6 = r6.zza(r7)     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x062d
            java.lang.String r6 = r2.zzl()     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x063a
            java.lang.String r6 = r3.zzr     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x063a
            java.lang.String r6 = r3.zzr     // Catch:{ all -> 0x092c }
            r2.zzo(r6)     // Catch:{ all -> 0x092c }
            goto L_0x063a
        L_0x062d:
            java.lang.String r6 = r3.zzr     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x063a
            java.lang.String r6 = r3.zzr     // Catch:{ all -> 0x092c }
            r2.zzo(r6)     // Catch:{ all -> 0x092c }
        L_0x063a:
            long r6 = r3.zzf     // Catch:{ all -> 0x092c }
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x0647
            long r6 = r3.zzf     // Catch:{ all -> 0x092c }
            r2.zzh(r6)     // Catch:{ all -> 0x092c }
        L_0x0647:
            long r6 = r3.zzt     // Catch:{ all -> 0x092c }
            r2.zzk(r6)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r6 = r6.zzb()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r10 = com.google.android.gms.measurement.internal.zzap.zzbc     // Catch:{ all -> 0x092c }
            boolean r6 = r6.zze(r7, r10)     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x0669
            com.google.android.gms.measurement.internal.zzkr r6 = r27.zzh()     // Catch:{ all -> 0x092c }
            java.util.List r6 = r6.zzf()     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x0669
            r2.zzd(r6)     // Catch:{ all -> 0x092c }
        L_0x0669:
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfo r6 = r6.zzc()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zza     // Catch:{ all -> 0x092c }
            android.util.Pair r6 = r6.zza(r7)     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x069c
            java.lang.Object r7 = r6.first     // Catch:{ all -> 0x092c }
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7     // Catch:{ all -> 0x092c }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x092c }
            if (r7 != 0) goto L_0x069c
            boolean r7 = r3.zzo     // Catch:{ all -> 0x092c }
            if (r7 == 0) goto L_0x06fe
            java.lang.Object r7 = r6.first     // Catch:{ all -> 0x092c }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x092c }
            r2.zzh(r7)     // Catch:{ all -> 0x092c }
            java.lang.Object r7 = r6.second     // Catch:{ all -> 0x092c }
            if (r7 == 0) goto L_0x06fe
            java.lang.Object r6 = r6.second     // Catch:{ all -> 0x092c }
            java.lang.Boolean r6 = (java.lang.Boolean) r6     // Catch:{ all -> 0x092c }
            boolean r6 = r6.booleanValue()     // Catch:{ all -> 0x092c }
            r2.zza(r6)     // Catch:{ all -> 0x092c }
            goto L_0x06fe
        L_0x069c:
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzah r6 = r6.zzx()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            android.content.Context r7 = r7.zzn()     // Catch:{ all -> 0x092c }
            boolean r6 = r6.zza(r7)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x06fe
            boolean r6 = r3.zzp     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x06fe
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            android.content.Context r6 = r6.zzn()     // Catch:{ all -> 0x092c }
            android.content.ContentResolver r6 = r6.getContentResolver()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = "android_id"
            java.lang.String r6 = android.provider.Settings.Secure.getString(r6, r7)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x06de
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r6 = r6.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r6 = r6.zzi()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = "null secure ID. appId"
            java.lang.String r10 = r2.zzj()     // Catch:{ all -> 0x092c }
            java.lang.Object r10 = com.google.android.gms.measurement.internal.zzfb.zza(r10)     // Catch:{ all -> 0x092c }
            r6.zza(r7, r10)     // Catch:{ all -> 0x092c }
            java.lang.String r6 = "null"
            goto L_0x06fb
        L_0x06de:
            boolean r7 = r6.isEmpty()     // Catch:{ all -> 0x092c }
            if (r7 == 0) goto L_0x06fb
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r7 = r7.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r7 = r7.zzi()     // Catch:{ all -> 0x092c }
            java.lang.String r10 = "empty secure ID. appId"
            java.lang.String r11 = r2.zzj()     // Catch:{ all -> 0x092c }
            java.lang.Object r11 = com.google.android.gms.measurement.internal.zzfb.zza(r11)     // Catch:{ all -> 0x092c }
            r7.zza(r10, r11)     // Catch:{ all -> 0x092c }
        L_0x06fb:
            r2.zzm(r6)     // Catch:{ all -> 0x092c }
        L_0x06fe:
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzah r6 = r6.zzx()     // Catch:{ all -> 0x092c }
            r6.zzaa()     // Catch:{ all -> 0x092c }
            java.lang.String r6 = android.os.Build.MODEL     // Catch:{ all -> 0x092c }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r6 = r2.zzc(r6)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzah r7 = r7.zzx()     // Catch:{ all -> 0x092c }
            r7.zzaa()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x092c }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r6 = r6.zzb(r7)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzah r7 = r7.zzx()     // Catch:{ all -> 0x092c }
            long r10 = r7.zzf()     // Catch:{ all -> 0x092c }
            int r7 = (int) r10     // Catch:{ all -> 0x092c }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r6 = r6.zzf(r7)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzah r7 = r7.zzx()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r7.zzg()     // Catch:{ all -> 0x092c }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r6 = r6.zzd(r7)     // Catch:{ all -> 0x092c }
            long r10 = r3.zzl     // Catch:{ all -> 0x092c }
            r6.zzj(r10)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            boolean r6 = r6.zzab()     // Catch:{ all -> 0x092c }
            if (r6 == 0) goto L_0x0754
            r2.zzj()     // Catch:{ all -> 0x092c }
            boolean r6 = android.text.TextUtils.isEmpty(r28)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x0754
            r6 = r28
            r2.zzn(r6)     // Catch:{ all -> 0x092c }
        L_0x0754:
            com.google.android.gms.measurement.internal.zzac r6 = r27.zze()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzg r6 = r6.zzb(r7)     // Catch:{ all -> 0x092c }
            if (r6 != 0) goto L_0x07c7
            com.google.android.gms.measurement.internal.zzg r6 = new com.google.android.gms.measurement.internal.zzg     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            java.lang.String r10 = r3.zza     // Catch:{ all -> 0x092c }
            r6.<init>(r7, r10)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkv r7 = r7.zzi()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r7.zzk()     // Catch:{ all -> 0x092c }
            r6.zza(r7)     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zzk     // Catch:{ all -> 0x092c }
            r6.zzf(r7)     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zzb     // Catch:{ all -> 0x092c }
            r6.zzb(r7)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfo r7 = r7.zzc()     // Catch:{ all -> 0x092c }
            java.lang.String r10 = r3.zza     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r7.zzb(r10)     // Catch:{ all -> 0x092c }
            r6.zze(r7)     // Catch:{ all -> 0x092c }
            r6.zzg(r8)     // Catch:{ all -> 0x092c }
            r6.zza(r8)     // Catch:{ all -> 0x092c }
            r6.zzb(r8)     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zzc     // Catch:{ all -> 0x092c }
            r6.zzg(r7)     // Catch:{ all -> 0x092c }
            long r10 = r3.zzj     // Catch:{ all -> 0x092c }
            r6.zzc(r10)     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zzd     // Catch:{ all -> 0x092c }
            r6.zzh(r7)     // Catch:{ all -> 0x092c }
            long r10 = r3.zze     // Catch:{ all -> 0x092c }
            r6.zzd(r10)     // Catch:{ all -> 0x092c }
            long r10 = r3.zzf     // Catch:{ all -> 0x092c }
            r6.zze(r10)     // Catch:{ all -> 0x092c }
            boolean r7 = r3.zzh     // Catch:{ all -> 0x092c }
            r6.zza(r7)     // Catch:{ all -> 0x092c }
            long r10 = r3.zzl     // Catch:{ all -> 0x092c }
            r6.zzp(r10)     // Catch:{ all -> 0x092c }
            long r10 = r3.zzt     // Catch:{ all -> 0x092c }
            r6.zzf(r10)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r7 = r27.zze()     // Catch:{ all -> 0x092c }
            r7.zza(r6)     // Catch:{ all -> 0x092c }
        L_0x07c7:
            java.lang.String r7 = r6.zzd()     // Catch:{ all -> 0x092c }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x092c }
            if (r7 != 0) goto L_0x07d8
            java.lang.String r7 = r6.zzd()     // Catch:{ all -> 0x092c }
            r2.zzi(r7)     // Catch:{ all -> 0x092c }
        L_0x07d8:
            java.lang.String r7 = r6.zzi()     // Catch:{ all -> 0x092c }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x092c }
            if (r7 != 0) goto L_0x07e9
            java.lang.String r6 = r6.zzi()     // Catch:{ all -> 0x092c }
            r2.zzl(r6)     // Catch:{ all -> 0x092c }
        L_0x07e9:
            com.google.android.gms.measurement.internal.zzac r6 = r27.zze()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = r3.zza     // Catch:{ all -> 0x092c }
            java.util.List r6 = r6.zza(r7)     // Catch:{ all -> 0x092c }
            r7 = 0
        L_0x07f4:
            int r10 = r6.size()     // Catch:{ all -> 0x092c }
            if (r7 >= r10) goto L_0x082b
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r10 = com.google.android.gms.internal.measurement.zzbs.zzk.zzj()     // Catch:{ all -> 0x092c }
            java.lang.Object r11 = r6.get(r7)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzks r11 = (com.google.android.gms.measurement.internal.zzks) r11     // Catch:{ all -> 0x092c }
            java.lang.String r11 = r11.zzc     // Catch:{ all -> 0x092c }
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r10 = r10.zza(r11)     // Catch:{ all -> 0x092c }
            java.lang.Object r11 = r6.get(r7)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzks r11 = (com.google.android.gms.measurement.internal.zzks) r11     // Catch:{ all -> 0x092c }
            long r11 = r11.zzd     // Catch:{ all -> 0x092c }
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r10 = r10.zza(r11)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzkr r11 = r27.zzh()     // Catch:{ all -> 0x092c }
            java.lang.Object r12 = r6.get(r7)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzks r12 = (com.google.android.gms.measurement.internal.zzks) r12     // Catch:{ all -> 0x092c }
            java.lang.Object r12 = r12.zze     // Catch:{ all -> 0x092c }
            r11.zza(r10, r12)     // Catch:{ all -> 0x092c }
            r2.zza(r10)     // Catch:{ all -> 0x092c }
            int r7 = r7 + 1
            goto L_0x07f4
        L_0x082b:
            com.google.android.gms.measurement.internal.zzac r6 = r27.zze()     // Catch:{ IOException -> 0x08a4 }
            com.google.android.gms.internal.measurement.zzgm r7 = r2.zzv()     // Catch:{ IOException -> 0x08a4 }
            com.google.android.gms.internal.measurement.zzfe r7 = (com.google.android.gms.internal.measurement.zzfe) r7     // Catch:{ IOException -> 0x08a4 }
            com.google.android.gms.internal.measurement.zzbs$zzg r7 = (com.google.android.gms.internal.measurement.zzbs.zzg) r7     // Catch:{ IOException -> 0x08a4 }
            long r6 = r6.zza(r7)     // Catch:{ IOException -> 0x08a4 }
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzam r10 = r4.zze     // Catch:{ all -> 0x092c }
            if (r10 == 0) goto L_0x089a
            com.google.android.gms.measurement.internal.zzam r10 = r4.zze     // Catch:{ all -> 0x092c }
            java.util.Iterator r10 = r10.iterator()     // Catch:{ all -> 0x092c }
        L_0x0849:
            boolean r11 = r10.hasNext()     // Catch:{ all -> 0x092c }
            if (r11 == 0) goto L_0x0861
            java.lang.Object r11 = r10.next()     // Catch:{ all -> 0x092c }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x092c }
            r12 = r26
            boolean r11 = r12.equals(r11)     // Catch:{ all -> 0x092c }
            if (r11 == 0) goto L_0x085e
            goto L_0x089b
        L_0x085e:
            r26 = r12
            goto L_0x0849
        L_0x0861:
            com.google.android.gms.measurement.internal.zzfz r10 = r27.zzc()     // Catch:{ all -> 0x092c }
            java.lang.String r11 = r4.zza     // Catch:{ all -> 0x092c }
            java.lang.String r12 = r4.zzb     // Catch:{ all -> 0x092c }
            boolean r10 = r10.zzc(r11, r12)     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzac r11 = r27.zze()     // Catch:{ all -> 0x092c }
            long r12 = r27.zzx()     // Catch:{ all -> 0x092c }
            java.lang.String r14 = r4.zza     // Catch:{ all -> 0x092c }
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            com.google.android.gms.measurement.internal.zzab r11 = r11.zza(r12, r14, r15, r16, r17, r18, r19)     // Catch:{ all -> 0x092c }
            if (r10 == 0) goto L_0x089a
            long r10 = r11.zze     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzgf r12 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r12 = r12.zzb()     // Catch:{ all -> 0x092c }
            java.lang.String r13 = r4.zza     // Catch:{ all -> 0x092c }
            int r12 = r12.zza(r13)     // Catch:{ all -> 0x092c }
            long r12 = (long) r12     // Catch:{ all -> 0x092c }
            int r14 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r14 >= 0) goto L_0x089a
            goto L_0x089b
        L_0x089a:
            r5 = 0
        L_0x089b:
            boolean r2 = r2.zza(r4, r6, r5)     // Catch:{ all -> 0x092c }
            if (r2 == 0) goto L_0x08bd
            r1.zzn = r8     // Catch:{ all -> 0x092c }
            goto L_0x08bd
        L_0x08a4:
            r0 = move-exception
            r5 = r0
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r6 = r6.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r6 = r6.zzf()     // Catch:{ all -> 0x092c }
            java.lang.String r7 = "Data loss. Failed to insert raw event metadata. appId"
            java.lang.String r2 = r2.zzj()     // Catch:{ all -> 0x092c }
            java.lang.Object r2 = com.google.android.gms.measurement.internal.zzfb.zza(r2)     // Catch:{ all -> 0x092c }
            r6.zza(r7, r2, r5)     // Catch:{ all -> 0x092c }
        L_0x08bd:
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()     // Catch:{ all -> 0x092c }
            r2.b_()     // Catch:{ all -> 0x092c }
            boolean r2 = com.google.android.gms.internal.measurement.zzkw.zzb()     // Catch:{ all -> 0x092c }
            if (r2 == 0) goto L_0x08da
            com.google.android.gms.measurement.internal.zzgf r2 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzx r2 = r2.zzb()     // Catch:{ all -> 0x092c }
            java.lang.String r3 = r3.zza     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.zzap.zzcx     // Catch:{ all -> 0x092c }
            boolean r2 = r2.zze(r3, r5)     // Catch:{ all -> 0x092c }
            if (r2 != 0) goto L_0x0900
        L_0x08da:
            com.google.android.gms.measurement.internal.zzgf r2 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r2 = r2.zzr()     // Catch:{ all -> 0x092c }
            r3 = 2
            boolean r2 = r2.zza(r3)     // Catch:{ all -> 0x092c }
            if (r2 == 0) goto L_0x0900
            com.google.android.gms.measurement.internal.zzgf r2 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfb r2 = r2.zzr()     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzfd r2 = r2.zzx()     // Catch:{ all -> 0x092c }
            java.lang.String r3 = "Event recorded"
            com.google.android.gms.measurement.internal.zzgf r5 = r1.zzj     // Catch:{ all -> 0x092c }
            com.google.android.gms.measurement.internal.zzez r5 = r5.zzj()     // Catch:{ all -> 0x092c }
            java.lang.String r4 = r5.zza(r4)     // Catch:{ all -> 0x092c }
            r2.zza(r3, r4)     // Catch:{ all -> 0x092c }
        L_0x0900:
            com.google.android.gms.measurement.internal.zzac r2 = r27.zze()
            r2.zzh()
            r27.zzz()
            com.google.android.gms.measurement.internal.zzgf r2 = r1.zzj
            com.google.android.gms.measurement.internal.zzfb r2 = r2.zzr()
            com.google.android.gms.measurement.internal.zzfd r2 = r2.zzx()
            long r3 = java.lang.System.nanoTime()
            long r3 = r3 - r23
            r5 = 500000(0x7a120, double:2.47033E-318)
            long r3 = r3 + r5
            r5 = 1000000(0xf4240, double:4.940656E-318)
            long r3 = r3 / r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            java.lang.String r4 = "Background event processing time, ms"
            r2.zza(r4, r3)
            return
        L_0x092c:
            r0 = move-exception
            r2 = r0
            com.google.android.gms.measurement.internal.zzac r3 = r27.zze()
            r3.zzh()
            goto L_0x0937
        L_0x0936:
            throw r2
        L_0x0937:
            goto L_0x0936
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzkj.zzb(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:89|90) */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        r1.zzj.zzr().zzf().zza("Failed to parse upload URL. Not uploading. appId", com.google.android.gms.measurement.internal.zzfb.zza(r5), r9);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:89:0x02bb */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzl() {
        /*
            r17 = this;
            r1 = r17
            r17.zzw()
            r17.zzk()
            r0 = 1
            r1.zzt = r0
            r2 = 0
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x02f7 }
            r3.zzu()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzis r3 = r3.zzw()     // Catch:{ all -> 0x02f7 }
            java.lang.Boolean r3 = r3.zzag()     // Catch:{ all -> 0x02f7 }
            if (r3 != 0) goto L_0x0032
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfb r0 = r0.zzr()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzi()     // Catch:{ all -> 0x02f7 }
            java.lang.String r3 = "Upload data called on the client side before use of service was decided"
            r0.zza(r3)     // Catch:{ all -> 0x02f7 }
            r1.zzt = r2
            r17.zzaa()
            return
        L_0x0032:
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x02f7 }
            if (r3 == 0) goto L_0x004d
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfb r0 = r0.zzr()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzf()     // Catch:{ all -> 0x02f7 }
            java.lang.String r3 = "Upload called in the client side when service should be used"
            r0.zza(r3)     // Catch:{ all -> 0x02f7 }
            r1.zzt = r2
            r17.zzaa()
            return
        L_0x004d:
            long r3 = r1.zzn     // Catch:{ all -> 0x02f7 }
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x005e
            r17.zzz()     // Catch:{ all -> 0x02f7 }
            r1.zzt = r2
            r17.zzaa()
            return
        L_0x005e:
            r17.zzw()     // Catch:{ all -> 0x02f7 }
            java.util.List<java.lang.Long> r3 = r1.zzw     // Catch:{ all -> 0x02f7 }
            if (r3 == 0) goto L_0x0067
            r3 = 1
            goto L_0x0068
        L_0x0067:
            r3 = 0
        L_0x0068:
            if (r3 == 0) goto L_0x007f
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfb r0 = r0.zzr()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzx()     // Catch:{ all -> 0x02f7 }
            java.lang.String r3 = "Uploading requested multiple times"
            r0.zza(r3)     // Catch:{ all -> 0x02f7 }
            r1.zzt = r2
            r17.zzaa()
            return
        L_0x007f:
            com.google.android.gms.measurement.internal.zzff r3 = r17.zzd()     // Catch:{ all -> 0x02f7 }
            boolean r3 = r3.zzf()     // Catch:{ all -> 0x02f7 }
            if (r3 != 0) goto L_0x00a1
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfb r0 = r0.zzr()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzx()     // Catch:{ all -> 0x02f7 }
            java.lang.String r3 = "Network not connected, ignoring upload request"
            r0.zza(r3)     // Catch:{ all -> 0x02f7 }
            r17.zzz()     // Catch:{ all -> 0x02f7 }
            r1.zzt = r2
            r17.zzaa()
            return
        L_0x00a1:
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.common.util.Clock r3 = r3.zzm()     // Catch:{ all -> 0x02f7 }
            long r3 = r3.currentTimeMillis()     // Catch:{ all -> 0x02f7 }
            long r7 = com.google.android.gms.measurement.internal.zzx.zzk()     // Catch:{ all -> 0x02f7 }
            long r7 = r3 - r7
            r9 = 0
            r1.zza(r9, r7)     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfo r7 = r7.zzc()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfp r7 = r7.zzc     // Catch:{ all -> 0x02f7 }
            long r7 = r7.zza()     // Catch:{ all -> 0x02f7 }
            int r10 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r10 == 0) goto L_0x00de
            com.google.android.gms.measurement.internal.zzgf r5 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfb r5 = r5.zzr()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfd r5 = r5.zzw()     // Catch:{ all -> 0x02f7 }
            java.lang.String r6 = "Uploading events. Elapsed time since last upload attempt (ms)"
            long r7 = r3 - r7
            long r7 = java.lang.Math.abs(r7)     // Catch:{ all -> 0x02f7 }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x02f7 }
            r5.zza(r6, r7)     // Catch:{ all -> 0x02f7 }
        L_0x00de:
            com.google.android.gms.measurement.internal.zzac r5 = r17.zze()     // Catch:{ all -> 0x02f7 }
            java.lang.String r5 = r5.d_()     // Catch:{ all -> 0x02f7 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x02f7 }
            r7 = -1
            if (r6 != 0) goto L_0x02cf
            long r10 = r1.zzy     // Catch:{ all -> 0x02f7 }
            int r6 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r6 != 0) goto L_0x00fe
            com.google.android.gms.measurement.internal.zzac r6 = r17.zze()     // Catch:{ all -> 0x02f7 }
            long r6 = r6.zzaa()     // Catch:{ all -> 0x02f7 }
            r1.zzy = r6     // Catch:{ all -> 0x02f7 }
        L_0x00fe:
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzx r6 = r6.zzb()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Integer> r7 = com.google.android.gms.measurement.internal.zzap.zzf     // Catch:{ all -> 0x02f7 }
            int r6 = r6.zzb(r5, r7)     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzx r7 = r7.zzb()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Integer> r8 = com.google.android.gms.measurement.internal.zzap.zzg     // Catch:{ all -> 0x02f7 }
            int r7 = r7.zzb(r5, r8)     // Catch:{ all -> 0x02f7 }
            int r7 = java.lang.Math.max(r2, r7)     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzac r8 = r17.zze()     // Catch:{ all -> 0x02f7 }
            java.util.List r6 = r8.zza(r5, r6, r7)     // Catch:{ all -> 0x02f7 }
            boolean r7 = r6.isEmpty()     // Catch:{ all -> 0x02f7 }
            if (r7 != 0) goto L_0x02f1
            java.util.Iterator r7 = r6.iterator()     // Catch:{ all -> 0x02f7 }
        L_0x012c:
            boolean r8 = r7.hasNext()     // Catch:{ all -> 0x02f7 }
            if (r8 == 0) goto L_0x014b
            java.lang.Object r8 = r7.next()     // Catch:{ all -> 0x02f7 }
            android.util.Pair r8 = (android.util.Pair) r8     // Catch:{ all -> 0x02f7 }
            java.lang.Object r8 = r8.first     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzbs$zzg r8 = (com.google.android.gms.internal.measurement.zzbs.zzg) r8     // Catch:{ all -> 0x02f7 }
            java.lang.String r10 = r8.zzad()     // Catch:{ all -> 0x02f7 }
            boolean r10 = android.text.TextUtils.isEmpty(r10)     // Catch:{ all -> 0x02f7 }
            if (r10 != 0) goto L_0x012c
            java.lang.String r7 = r8.zzad()     // Catch:{ all -> 0x02f7 }
            goto L_0x014c
        L_0x014b:
            r7 = r9
        L_0x014c:
            if (r7 == 0) goto L_0x017b
            r8 = 0
        L_0x014f:
            int r10 = r6.size()     // Catch:{ all -> 0x02f7 }
            if (r8 >= r10) goto L_0x017b
            java.lang.Object r10 = r6.get(r8)     // Catch:{ all -> 0x02f7 }
            android.util.Pair r10 = (android.util.Pair) r10     // Catch:{ all -> 0x02f7 }
            java.lang.Object r10 = r10.first     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = (com.google.android.gms.internal.measurement.zzbs.zzg) r10     // Catch:{ all -> 0x02f7 }
            java.lang.String r11 = r10.zzad()     // Catch:{ all -> 0x02f7 }
            boolean r11 = android.text.TextUtils.isEmpty(r11)     // Catch:{ all -> 0x02f7 }
            if (r11 != 0) goto L_0x0178
            java.lang.String r10 = r10.zzad()     // Catch:{ all -> 0x02f7 }
            boolean r10 = r10.equals(r7)     // Catch:{ all -> 0x02f7 }
            if (r10 != 0) goto L_0x0178
            java.util.List r6 = r6.subList(r2, r8)     // Catch:{ all -> 0x02f7 }
            goto L_0x017b
        L_0x0178:
            int r8 = r8 + 1
            goto L_0x014f
        L_0x017b:
            com.google.android.gms.internal.measurement.zzbs$zzf$zza r7 = com.google.android.gms.internal.measurement.zzbs.zzf.zzb()     // Catch:{ all -> 0x02f7 }
            int r8 = r6.size()     // Catch:{ all -> 0x02f7 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ all -> 0x02f7 }
            int r11 = r6.size()     // Catch:{ all -> 0x02f7 }
            r10.<init>(r11)     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzgf r11 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzx r11 = r11.zzb()     // Catch:{ all -> 0x02f7 }
            boolean r11 = r11.zzd(r5)     // Catch:{ all -> 0x02f7 }
            r12 = 0
        L_0x0197:
            if (r12 >= r8) goto L_0x0202
            java.lang.Object r13 = r6.get(r12)     // Catch:{ all -> 0x02f7 }
            android.util.Pair r13 = (android.util.Pair) r13     // Catch:{ all -> 0x02f7 }
            java.lang.Object r13 = r13.first     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzbs$zzg r13 = (com.google.android.gms.internal.measurement.zzbs.zzg) r13     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzfe$zza r13 = r13.zzbl()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzfe$zza r13 = (com.google.android.gms.internal.measurement.zzfe.zza) r13     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r13 = (com.google.android.gms.internal.measurement.zzbs.zzg.zza) r13     // Catch:{ all -> 0x02f7 }
            java.lang.Object r14 = r6.get(r12)     // Catch:{ all -> 0x02f7 }
            android.util.Pair r14 = (android.util.Pair) r14     // Catch:{ all -> 0x02f7 }
            java.lang.Object r14 = r14.second     // Catch:{ all -> 0x02f7 }
            java.lang.Long r14 = (java.lang.Long) r14     // Catch:{ all -> 0x02f7 }
            r10.add(r14)     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzgf r14 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzx r14 = r14.zzb()     // Catch:{ all -> 0x02f7 }
            long r14 = r14.zze()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r14 = r13.zzg(r14)     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r14 = r14.zza(r3)     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzgf r15 = r1.zzj     // Catch:{ all -> 0x02f7 }
            r15.zzu()     // Catch:{ all -> 0x02f7 }
            r14.zzb(r2)     // Catch:{ all -> 0x02f7 }
            if (r11 != 0) goto L_0x01d7
            r13.zzn()     // Catch:{ all -> 0x02f7 }
        L_0x01d7:
            com.google.android.gms.measurement.internal.zzgf r14 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzx r14 = r14.zzb()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r15 = com.google.android.gms.measurement.internal.zzap.zzbe     // Catch:{ all -> 0x02f7 }
            boolean r14 = r14.zze(r5, r15)     // Catch:{ all -> 0x02f7 }
            if (r14 == 0) goto L_0x01fc
            com.google.android.gms.internal.measurement.zzgm r14 = r13.zzv()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzfe r14 = (com.google.android.gms.internal.measurement.zzfe) r14     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzbs$zzg r14 = (com.google.android.gms.internal.measurement.zzbs.zzg) r14     // Catch:{ all -> 0x02f7 }
            byte[] r14 = r14.zzbi()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzkr r15 = r17.zzh()     // Catch:{ all -> 0x02f7 }
            long r14 = r15.zza(r14)     // Catch:{ all -> 0x02f7 }
            r13.zzl(r14)     // Catch:{ all -> 0x02f7 }
        L_0x01fc:
            r7.zza(r13)     // Catch:{ all -> 0x02f7 }
            int r12 = r12 + 1
            goto L_0x0197
        L_0x0202:
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfb r6 = r6.zzr()     // Catch:{ all -> 0x02f7 }
            r11 = 2
            boolean r6 = r6.zza(r11)     // Catch:{ all -> 0x02f7 }
            if (r6 == 0) goto L_0x0220
            com.google.android.gms.measurement.internal.zzkr r6 = r17.zzh()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzgm r11 = r7.zzv()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzfe r11 = (com.google.android.gms.internal.measurement.zzfe) r11     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzbs$zzf r11 = (com.google.android.gms.internal.measurement.zzbs.zzf) r11     // Catch:{ all -> 0x02f7 }
            java.lang.String r6 = r6.zza(r11)     // Catch:{ all -> 0x02f7 }
            goto L_0x0221
        L_0x0220:
            r6 = r9
        L_0x0221:
            r17.zzh()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzgm r11 = r7.zzv()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzfe r11 = (com.google.android.gms.internal.measurement.zzfe) r11     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.internal.measurement.zzbs$zzf r11 = (com.google.android.gms.internal.measurement.zzbs.zzf) r11     // Catch:{ all -> 0x02f7 }
            byte[] r14 = r11.zzbi()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzeu<java.lang.String> r11 = com.google.android.gms.measurement.internal.zzap.zzp     // Catch:{ all -> 0x02f7 }
            java.lang.Object r9 = r11.zza(r9)     // Catch:{ all -> 0x02f7 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x02f7 }
            java.net.URL r13 = new java.net.URL     // Catch:{ MalformedURLException -> 0x02bb }
            r13.<init>(r9)     // Catch:{ MalformedURLException -> 0x02bb }
            boolean r11 = r10.isEmpty()     // Catch:{ MalformedURLException -> 0x02bb }
            if (r11 != 0) goto L_0x0245
            r11 = 1
            goto L_0x0246
        L_0x0245:
            r11 = 0
        L_0x0246:
            com.google.android.gms.common.internal.Preconditions.checkArgument(r11)     // Catch:{ MalformedURLException -> 0x02bb }
            java.util.List<java.lang.Long> r11 = r1.zzw     // Catch:{ MalformedURLException -> 0x02bb }
            if (r11 == 0) goto L_0x025d
            com.google.android.gms.measurement.internal.zzgf r10 = r1.zzj     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzfb r10 = r10.zzr()     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzfd r10 = r10.zzf()     // Catch:{ MalformedURLException -> 0x02bb }
            java.lang.String r11 = "Set uploading progress before finishing the previous upload"
            r10.zza(r11)     // Catch:{ MalformedURLException -> 0x02bb }
            goto L_0x0264
        L_0x025d:
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ MalformedURLException -> 0x02bb }
            r11.<init>(r10)     // Catch:{ MalformedURLException -> 0x02bb }
            r1.zzw = r11     // Catch:{ MalformedURLException -> 0x02bb }
        L_0x0264:
            com.google.android.gms.measurement.internal.zzgf r10 = r1.zzj     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzfo r10 = r10.zzc()     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzfp r10 = r10.zzd     // Catch:{ MalformedURLException -> 0x02bb }
            r10.zza(r3)     // Catch:{ MalformedURLException -> 0x02bb }
            java.lang.String r3 = "?"
            if (r8 <= 0) goto L_0x027b
            com.google.android.gms.internal.measurement.zzbs$zzg r3 = r7.zza(r2)     // Catch:{ MalformedURLException -> 0x02bb }
            java.lang.String r3 = r3.zzx()     // Catch:{ MalformedURLException -> 0x02bb }
        L_0x027b:
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzx()     // Catch:{ MalformedURLException -> 0x02bb }
            java.lang.String r7 = "Uploading data. app, uncompressed size, data"
            int r8 = r14.length     // Catch:{ MalformedURLException -> 0x02bb }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ MalformedURLException -> 0x02bb }
            r4.zza(r7, r3, r8, r6)     // Catch:{ MalformedURLException -> 0x02bb }
            r1.zzs = r0     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzff r11 = r17.zzd()     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzkl r0 = new com.google.android.gms.measurement.internal.zzkl     // Catch:{ MalformedURLException -> 0x02bb }
            r0.<init>(r1, r5)     // Catch:{ MalformedURLException -> 0x02bb }
            r11.zzd()     // Catch:{ MalformedURLException -> 0x02bb }
            r11.zzak()     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r13)     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r14)     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r0)     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzgc r3 = r11.zzq()     // Catch:{ MalformedURLException -> 0x02bb }
            com.google.android.gms.measurement.internal.zzfj r4 = new com.google.android.gms.measurement.internal.zzfj     // Catch:{ MalformedURLException -> 0x02bb }
            r15 = 0
            r10 = r4
            r12 = r5
            r16 = r0
            r10.<init>(r11, r12, r13, r14, r15, r16)     // Catch:{ MalformedURLException -> 0x02bb }
            r3.zzb(r4)     // Catch:{ MalformedURLException -> 0x02bb }
            goto L_0x02f1
        L_0x02bb:
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfb r0 = r0.zzr()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzf()     // Catch:{ all -> 0x02f7 }
            java.lang.String r3 = "Failed to parse upload URL. Not uploading. appId"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.zzfb.zza(r5)     // Catch:{ all -> 0x02f7 }
            r0.zza(r3, r4, r9)     // Catch:{ all -> 0x02f7 }
            goto L_0x02f1
        L_0x02cf:
            r1.zzy = r7     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzac r0 = r17.zze()     // Catch:{ all -> 0x02f7 }
            long r5 = com.google.android.gms.measurement.internal.zzx.zzk()     // Catch:{ all -> 0x02f7 }
            long r3 = r3 - r5
            java.lang.String r0 = r0.zza(r3)     // Catch:{ all -> 0x02f7 }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x02f7 }
            if (r3 != 0) goto L_0x02f1
            com.google.android.gms.measurement.internal.zzac r3 = r17.zze()     // Catch:{ all -> 0x02f7 }
            com.google.android.gms.measurement.internal.zzg r0 = r3.zzb(r0)     // Catch:{ all -> 0x02f7 }
            if (r0 == 0) goto L_0x02f1
            r1.zza(r0)     // Catch:{ all -> 0x02f7 }
        L_0x02f1:
            r1.zzt = r2
            r17.zzaa()
            return
        L_0x02f7:
            r0 = move-exception
            r1.zzt = r2
            r17.zzaa()
            goto L_0x02ff
        L_0x02fe:
            throw r0
        L_0x02ff:
            goto L_0x02fe
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzkj.zzl():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzkj.zza(com.google.android.gms.internal.measurement.zzbs$zzg$zza, long, boolean):void
     arg types: [com.google.android.gms.internal.measurement.zzbs$zzg$zza, long, int]
     candidates:
      com.google.android.gms.measurement.internal.zzkj.zza(com.google.android.gms.measurement.internal.zzm, com.google.android.gms.measurement.internal.zzg, java.lang.String):com.google.android.gms.measurement.internal.zzg
      com.google.android.gms.measurement.internal.zzkj.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, int, java.lang.String):void
      com.google.android.gms.measurement.internal.zzkj.zza(com.google.android.gms.internal.measurement.zzbs$zzg$zza, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzkr.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, java.lang.Object):void
     arg types: [com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.zzkr.zza(boolean, boolean, boolean):java.lang.String
      com.google.android.gms.measurement.internal.zzkr.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.measurement.zzbk$zzc):void
      com.google.android.gms.measurement.internal.zzkr.zza(java.lang.StringBuilder, int, java.util.List<com.google.android.gms.internal.measurement.zzbs$zze>):void
      com.google.android.gms.measurement.internal.zzkr.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0040, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        r2 = r0;
        r8 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0046, code lost:
        r7 = null;
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0099, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x009a, code lost:
        r8 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040 A[ExcHandler: all (r0v16 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r3 
      PHI: (r3v182 android.database.Cursor) = (r3v177 android.database.Cursor), (r3v177 android.database.Cursor), (r3v177 android.database.Cursor), (r3v185 android.database.Cursor), (r3v185 android.database.Cursor), (r3v185 android.database.Cursor), (r3v185 android.database.Cursor), (r3v1 android.database.Cursor), (r3v1 android.database.Cursor) binds: [B:47:0x00da, B:53:0x00e7, B:54:?, B:24:0x007c, B:30:0x0089, B:32:0x008d, B:33:?, B:9:0x0031, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0278 A[SYNTHETIC, Splitter:B:131:0x0278] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x027f A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x028d A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x03c9 A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x03d4 A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x03d5 A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x05c0 A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:275:0x069b A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x07b2 A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:318:0x07cc A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:454:0x0b9b A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:455:0x0bae A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:457:0x0bb1 A[Catch:{ IOException -> 0x022e, all -> 0x0f8c }] */
    /* JADX WARNING: Removed duplicated region for block: B:458:0x0bd8 A[SYNTHETIC, Splitter:B:458:0x0bd8] */
    /* JADX WARNING: Removed duplicated region for block: B:588:0x0f74 A[SYNTHETIC, Splitter:B:588:0x0f74] */
    /* JADX WARNING: Removed duplicated region for block: B:595:0x0f88 A[SYNTHETIC, Splitter:B:595:0x0f88] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zza(java.lang.String r60, long r61) {
        /*
            r59 = this;
            r1 = r59
            com.google.android.gms.measurement.internal.zzac r2 = r59.zze()
            r2.zzf()
            com.google.android.gms.measurement.internal.zzkj$zza r2 = new com.google.android.gms.measurement.internal.zzkj$zza     // Catch:{ all -> 0x0f8c }
            r3 = 0
            r2.<init>(r1, r3)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzac r4 = r59.zze()     // Catch:{ all -> 0x0f8c }
            long r5 = r1.zzy     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r2)     // Catch:{ all -> 0x0f8c }
            r4.zzd()     // Catch:{ all -> 0x0f8c }
            r4.zzak()     // Catch:{ all -> 0x0f8c }
            r8 = -1
            r10 = 2
            r11 = 0
            r12 = 1
            android.database.sqlite.SQLiteDatabase r15 = r4.c_()     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            boolean r13 = android.text.TextUtils.isEmpty(r3)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            if (r13 == 0) goto L_0x009c
            int r13 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r13 == 0) goto L_0x004b
            java.lang.String[] r13 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            java.lang.String r14 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            r13[r11] = r14     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            java.lang.String r14 = java.lang.String.valueOf(r61)     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            r13[r12] = r14     // Catch:{ SQLiteException -> 0x0045, all -> 0x0040 }
            goto L_0x0053
        L_0x0040:
            r0 = move-exception
            r2 = r0
            r8 = r3
            goto L_0x0f86
        L_0x0045:
            r0 = move-exception
            r7 = r3
            r8 = r7
        L_0x0048:
            r3 = r0
            goto L_0x0265
        L_0x004b:
            java.lang.String[] r13 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            java.lang.String r14 = java.lang.String.valueOf(r61)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            r13[r11] = r14     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
        L_0x0053:
            int r14 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r14 == 0) goto L_0x005a
            java.lang.String r14 = "rowid <= ? and "
            goto L_0x005c
        L_0x005a:
            java.lang.String r14 = ""
        L_0x005c:
            int r7 = r14.length()     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            int r7 = r7 + 148
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            r3.<init>(r7)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            java.lang.String r7 = "select app_id, metadata_fingerprint from raw_events where "
            r3.append(r7)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            r3.append(r14)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            java.lang.String r7 = "app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;"
            r3.append(r7)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            android.database.Cursor r3 = r15.rawQuery(r3, r13)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            boolean r7 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            if (r7 != 0) goto L_0x0089
            if (r3 == 0) goto L_0x027b
            r3.close()     // Catch:{ all -> 0x0f8c }
            goto L_0x027b
        L_0x0089:
            java.lang.String r7 = r3.getString(r11)     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            java.lang.String r13 = r3.getString(r12)     // Catch:{ SQLiteException -> 0x0099, all -> 0x0040 }
            r3.close()     // Catch:{ SQLiteException -> 0x0099, all -> 0x0040 }
            r22 = r3
            r3 = r7
            r7 = r13
            goto L_0x00f2
        L_0x0099:
            r0 = move-exception
            r8 = r3
            goto L_0x0048
        L_0x009c:
            int r3 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r3 == 0) goto L_0x00ac
            java.lang.String[] r3 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            r7 = 0
            r3[r11] = r7     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            java.lang.String r7 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            r3[r12] = r7     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            goto L_0x00b1
        L_0x00ac:
            java.lang.String[] r3 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            r7 = 0
            r3[r11] = r7     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
        L_0x00b1:
            int r7 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r7 == 0) goto L_0x00b8
            java.lang.String r7 = " and rowid <= ?"
            goto L_0x00ba
        L_0x00b8:
            java.lang.String r7 = ""
        L_0x00ba:
            int r13 = r7.length()     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            int r13 = r13 + 84
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            r14.<init>(r13)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            java.lang.String r13 = "select metadata_fingerprint from raw_events where app_id = ?"
            r14.append(r13)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            r14.append(r7)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            java.lang.String r7 = " order by rowid limit 1;"
            r14.append(r7)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            java.lang.String r7 = r14.toString()     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            android.database.Cursor r3 = r15.rawQuery(r7, r3)     // Catch:{ SQLiteException -> 0x0261, all -> 0x025c }
            boolean r7 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            if (r7 != 0) goto L_0x00e7
            if (r3 == 0) goto L_0x027b
            r3.close()     // Catch:{ all -> 0x0f8c }
            goto L_0x027b
        L_0x00e7:
            java.lang.String r13 = r3.getString(r11)     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            r3.close()     // Catch:{ SQLiteException -> 0x0257, all -> 0x0040 }
            r22 = r3
            r7 = r13
            r3 = 0
        L_0x00f2:
            java.lang.String r14 = "raw_events_metadata"
            java.lang.String[] r13 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x0251, all -> 0x024b }
            java.lang.String r16 = "metadata"
            r13[r11] = r16     // Catch:{ SQLiteException -> 0x0251, all -> 0x024b }
            java.lang.String r16 = "app_id = ? and metadata_fingerprint = ?"
            java.lang.String[] r8 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0251, all -> 0x024b }
            r8[r11] = r3     // Catch:{ SQLiteException -> 0x0251, all -> 0x024b }
            r8[r12] = r7     // Catch:{ SQLiteException -> 0x0251, all -> 0x024b }
            r18 = 0
            r19 = 0
            java.lang.String r20 = "rowid"
            java.lang.String r21 = "2"
            r9 = r13
            r13 = r15
            r23 = r15
            r15 = r9
            r17 = r8
            android.database.Cursor r8 = r13.query(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ SQLiteException -> 0x0251, all -> 0x024b }
            boolean r9 = r8.moveToFirst()     // Catch:{ SQLiteException -> 0x0247 }
            if (r9 != 0) goto L_0x0133
            com.google.android.gms.measurement.internal.zzfb r5 = r4.zzr()     // Catch:{ SQLiteException -> 0x0247 }
            com.google.android.gms.measurement.internal.zzfd r5 = r5.zzf()     // Catch:{ SQLiteException -> 0x0247 }
            java.lang.String r6 = "Raw event metadata record is missing. appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.zzfb.zza(r3)     // Catch:{ SQLiteException -> 0x0247 }
            r5.zza(r6, r7)     // Catch:{ SQLiteException -> 0x0247 }
            if (r8 == 0) goto L_0x027b
            r8.close()     // Catch:{ all -> 0x0f8c }
            goto L_0x027b
        L_0x0133:
            byte[] r9 = r8.getBlob(r11)     // Catch:{ SQLiteException -> 0x0247 }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r13 = com.google.android.gms.internal.measurement.zzbs.zzg.zzbf()     // Catch:{ IOException -> 0x022e }
            com.google.android.gms.internal.measurement.zzgp r9 = com.google.android.gms.measurement.internal.zzkr.zza(r13, r9)     // Catch:{ IOException -> 0x022e }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r9 = (com.google.android.gms.internal.measurement.zzbs.zzg.zza) r9     // Catch:{ IOException -> 0x022e }
            com.google.android.gms.internal.measurement.zzgm r9 = r9.zzv()     // Catch:{ IOException -> 0x022e }
            com.google.android.gms.internal.measurement.zzfe r9 = (com.google.android.gms.internal.measurement.zzfe) r9     // Catch:{ IOException -> 0x022e }
            com.google.android.gms.internal.measurement.zzbs$zzg r9 = (com.google.android.gms.internal.measurement.zzbs.zzg) r9     // Catch:{ IOException -> 0x022e }
            boolean r13 = r8.moveToNext()     // Catch:{ SQLiteException -> 0x0247 }
            if (r13 == 0) goto L_0x0160
            com.google.android.gms.measurement.internal.zzfb r13 = r4.zzr()     // Catch:{ SQLiteException -> 0x0247 }
            com.google.android.gms.measurement.internal.zzfd r13 = r13.zzi()     // Catch:{ SQLiteException -> 0x0247 }
            java.lang.String r14 = "Get multiple raw event metadata records, expected one. appId"
            java.lang.Object r15 = com.google.android.gms.measurement.internal.zzfb.zza(r3)     // Catch:{ SQLiteException -> 0x0247 }
            r13.zza(r14, r15)     // Catch:{ SQLiteException -> 0x0247 }
        L_0x0160:
            r8.close()     // Catch:{ SQLiteException -> 0x0247 }
            r2.zza(r9)     // Catch:{ SQLiteException -> 0x0247 }
            r13 = -1
            int r9 = (r5 > r13 ? 1 : (r5 == r13 ? 0 : -1))
            if (r9 == 0) goto L_0x0180
            java.lang.String r9 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?"
            r13 = 3
            java.lang.String[] r14 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x0247 }
            r14[r11] = r3     // Catch:{ SQLiteException -> 0x0247 }
            r14[r12] = r7     // Catch:{ SQLiteException -> 0x0247 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0247 }
            r14[r10] = r5     // Catch:{ SQLiteException -> 0x0247 }
            r16 = r9
            r17 = r14
            goto L_0x018c
        L_0x0180:
            java.lang.String r5 = "app_id = ? and metadata_fingerprint = ?"
            java.lang.String[] r6 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0247 }
            r6[r11] = r3     // Catch:{ SQLiteException -> 0x0247 }
            r6[r12] = r7     // Catch:{ SQLiteException -> 0x0247 }
            r16 = r5
            r17 = r6
        L_0x018c:
            java.lang.String r14 = "raw_events"
            r5 = 4
            java.lang.String[] r15 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0247 }
            java.lang.String r5 = "rowid"
            r15[r11] = r5     // Catch:{ SQLiteException -> 0x0247 }
            java.lang.String r5 = "name"
            r15[r12] = r5     // Catch:{ SQLiteException -> 0x0247 }
            java.lang.String r5 = "timestamp"
            r15[r10] = r5     // Catch:{ SQLiteException -> 0x0247 }
            java.lang.String r5 = "data"
            r6 = 3
            r15[r6] = r5     // Catch:{ SQLiteException -> 0x0247 }
            r18 = 0
            r19 = 0
            java.lang.String r20 = "rowid"
            r21 = 0
            r13 = r23
            android.database.Cursor r5 = r13.query(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ SQLiteException -> 0x0247 }
            boolean r6 = r5.moveToFirst()     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            if (r6 != 0) goto L_0x01ce
            com.google.android.gms.measurement.internal.zzfb r6 = r4.zzr()     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            com.google.android.gms.measurement.internal.zzfd r6 = r6.zzi()     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            java.lang.String r7 = "Raw event data disappeared while in transaction. appId"
            java.lang.Object r8 = com.google.android.gms.measurement.internal.zzfb.zza(r3)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            r6.zza(r7, r8)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            if (r5 == 0) goto L_0x027b
            r5.close()     // Catch:{ all -> 0x0f8c }
            goto L_0x027b
        L_0x01ce:
            long r6 = r5.getLong(r11)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            r8 = 3
            byte[] r9 = r5.getBlob(r8)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r8 = com.google.android.gms.internal.measurement.zzbs.zzc.zzj()     // Catch:{ IOException -> 0x0205 }
            com.google.android.gms.internal.measurement.zzgp r8 = com.google.android.gms.measurement.internal.zzkr.zza(r8, r9)     // Catch:{ IOException -> 0x0205 }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r8 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r8     // Catch:{ IOException -> 0x0205 }
            java.lang.String r9 = r5.getString(r12)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r9 = r8.zza(r9)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            long r13 = r5.getLong(r10)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            r9.zza(r13)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            com.google.android.gms.internal.measurement.zzgm r8 = r8.zzv()     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            com.google.android.gms.internal.measurement.zzfe r8 = (com.google.android.gms.internal.measurement.zzfe) r8     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            com.google.android.gms.internal.measurement.zzbs$zzc r8 = (com.google.android.gms.internal.measurement.zzbs.zzc) r8     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            boolean r6 = r2.zza(r6, r8)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            if (r6 != 0) goto L_0x0218
            if (r5 == 0) goto L_0x027b
            r5.close()     // Catch:{ all -> 0x0f8c }
            goto L_0x027b
        L_0x0205:
            r0 = move-exception
            r6 = r0
            com.google.android.gms.measurement.internal.zzfb r7 = r4.zzr()     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            com.google.android.gms.measurement.internal.zzfd r7 = r7.zzf()     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            java.lang.String r8 = "Data loss. Failed to merge raw event. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzfb.zza(r3)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            r7.zza(r8, r9, r6)     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
        L_0x0218:
            boolean r6 = r5.moveToNext()     // Catch:{ SQLiteException -> 0x0229, all -> 0x0224 }
            if (r6 != 0) goto L_0x01ce
            if (r5 == 0) goto L_0x027b
            r5.close()     // Catch:{ all -> 0x0f8c }
            goto L_0x027b
        L_0x0224:
            r0 = move-exception
            r2 = r0
            r8 = r5
            goto L_0x0f86
        L_0x0229:
            r0 = move-exception
            r7 = r3
            r8 = r5
            goto L_0x0048
        L_0x022e:
            r0 = move-exception
            r5 = r0
            com.google.android.gms.measurement.internal.zzfb r6 = r4.zzr()     // Catch:{ SQLiteException -> 0x0247 }
            com.google.android.gms.measurement.internal.zzfd r6 = r6.zzf()     // Catch:{ SQLiteException -> 0x0247 }
            java.lang.String r7 = "Data loss. Failed to merge raw event metadata. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzfb.zza(r3)     // Catch:{ SQLiteException -> 0x0247 }
            r6.zza(r7, r9, r5)     // Catch:{ SQLiteException -> 0x0247 }
            if (r8 == 0) goto L_0x027b
            r8.close()     // Catch:{ all -> 0x0f8c }
            goto L_0x027b
        L_0x0247:
            r0 = move-exception
            r7 = r3
            goto L_0x0048
        L_0x024b:
            r0 = move-exception
            r2 = r0
            r8 = r22
            goto L_0x0f86
        L_0x0251:
            r0 = move-exception
            r7 = r3
            r8 = r22
            goto L_0x0048
        L_0x0257:
            r0 = move-exception
            r8 = r3
            r7 = 0
            goto L_0x0048
        L_0x025c:
            r0 = move-exception
            r2 = r0
            r8 = 0
            goto L_0x0f86
        L_0x0261:
            r0 = move-exception
            r3 = r0
            r7 = 0
            r8 = 0
        L_0x0265:
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0f84 }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzf()     // Catch:{ all -> 0x0f84 }
            java.lang.String r5 = "Data loss. Error selecting raw event. appId"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.zzfb.zza(r7)     // Catch:{ all -> 0x0f84 }
            r4.zza(r5, r6, r3)     // Catch:{ all -> 0x0f84 }
            if (r8 == 0) goto L_0x027b
            r8.close()     // Catch:{ all -> 0x0f8c }
        L_0x027b:
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r3 = r2.zzc     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x028a
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r3 = r2.zzc     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x0288
            goto L_0x028a
        L_0x0288:
            r3 = 0
            goto L_0x028b
        L_0x028a:
            r3 = 1
        L_0x028b:
            if (r3 != 0) goto L_0x0f74
            com.google.android.gms.internal.measurement.zzbs$zzg r3 = r2.zza     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r3 = r3.zzbl()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r3 = (com.google.android.gms.internal.measurement.zzfe.zza) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r3 = (com.google.android.gms.internal.measurement.zzbs.zzg.zza) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r3 = r3.zzc()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r4 = r4.zzb()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r5 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r5.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.zzap.zzay     // Catch:{ all -> 0x0f8c }
            boolean r4 = r4.zze(r5, r6)     // Catch:{ all -> 0x0f8c }
            r5 = -1
            r6 = 0
            r8 = 0
            r9 = 0
            r11 = -1
            r13 = 0
            r15 = 0
            r17 = 0
        L_0x02b7:
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r7 = r2.zzc     // Catch:{ all -> 0x0f8c }
            int r7 = r7.size()     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = "_e"
            java.lang.String r12 = "_et"
            r23 = r13
            if (r8 >= r7) goto L_0x0827
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r7 = r2.zzc     // Catch:{ all -> 0x0f8c }
            java.lang.Object r7 = r7.get(r8)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r7 = (com.google.android.gms.internal.measurement.zzbs.zzc) r7     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r7 = r7.zzbl()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r7 = (com.google.android.gms.internal.measurement.zzfe.zza) r7     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r7 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r7     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfz r13 = r59.zzc()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r14 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r14 = r14.zzx()     // Catch:{ all -> 0x0f8c }
            r21 = r9
            java.lang.String r9 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            boolean r9 = r13.zzb(r14, r9)     // Catch:{ all -> 0x0f8c }
            java.lang.String r13 = "_err"
            if (r9 == 0) goto L_0x036e
            com.google.android.gms.measurement.internal.zzgf r9 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r9 = r9.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r9 = r9.zzi()     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = "Dropping blacklisted raw event. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r12 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r12 = r12.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.Object r12 = com.google.android.gms.measurement.internal.zzfb.zza(r12)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzgf r14 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzez r14 = r14.zzj()     // Catch:{ all -> 0x0f8c }
            r22 = r15
            java.lang.String r15 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            java.lang.String r14 = r14.zza(r15)     // Catch:{ all -> 0x0f8c }
            r9.zza(r10, r12, r14)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfz r9 = r59.zzc()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = r10.zzx()     // Catch:{ all -> 0x0f8c }
            boolean r9 = r9.zzg(r10)     // Catch:{ all -> 0x0f8c }
            if (r9 != 0) goto L_0x0339
            com.google.android.gms.measurement.internal.zzfz r9 = r59.zzc()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = r10.zzx()     // Catch:{ all -> 0x0f8c }
            boolean r9 = r9.zzh(r10)     // Catch:{ all -> 0x0f8c }
            if (r9 == 0) goto L_0x0337
            goto L_0x0339
        L_0x0337:
            r9 = 0
            goto L_0x033a
        L_0x0339:
            r9 = 1
        L_0x033a:
            if (r9 != 0) goto L_0x035f
            java.lang.String r9 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            boolean r9 = r13.equals(r9)     // Catch:{ all -> 0x0f8c }
            if (r9 != 0) goto L_0x035f
            com.google.android.gms.measurement.internal.zzgf r9 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzkv r25 = r9.zzi()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r9 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r26 = r9.zzx()     // Catch:{ all -> 0x0f8c }
            r27 = 11
            java.lang.String r28 = "_ev"
            java.lang.String r29 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            r30 = 0
            r25.zza(r26, r27, r28, r29, r30)     // Catch:{ all -> 0x0f8c }
        L_0x035f:
            r28 = r4
            r10 = r8
            r9 = r21
            r15 = r22
            r13 = r23
            r4 = -1
            r8 = r3
            r3 = r11
            r11 = 3
            goto L_0x081c
        L_0x036e:
            r22 = r15
            com.google.android.gms.measurement.internal.zzfz r9 = r59.zzc()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r14 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r14 = r14.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.String r15 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            boolean r9 = r9.zzc(r14, r15)     // Catch:{ all -> 0x0f8c }
            java.lang.String r14 = "_c"
            if (r9 != 0) goto L_0x03de
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            java.lang.String r15 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r15)     // Catch:{ all -> 0x0f8c }
            r27 = r8
            int r8 = r15.hashCode()     // Catch:{ all -> 0x0f8c }
            r28 = r4
            r4 = 94660(0x171c4, float:1.32647E-40)
            if (r8 == r4) goto L_0x03bc
            r4 = 95025(0x17331, float:1.33158E-40)
            if (r8 == r4) goto L_0x03b2
            r4 = 95027(0x17333, float:1.33161E-40)
            if (r8 == r4) goto L_0x03a8
            goto L_0x03c6
        L_0x03a8:
            java.lang.String r4 = "_ui"
            boolean r4 = r15.equals(r4)     // Catch:{ all -> 0x0f8c }
            if (r4 == 0) goto L_0x03c6
            r4 = 1
            goto L_0x03c7
        L_0x03b2:
            java.lang.String r4 = "_ug"
            boolean r4 = r15.equals(r4)     // Catch:{ all -> 0x0f8c }
            if (r4 == 0) goto L_0x03c6
            r4 = 2
            goto L_0x03c7
        L_0x03bc:
            java.lang.String r4 = "_in"
            boolean r4 = r15.equals(r4)     // Catch:{ all -> 0x0f8c }
            if (r4 == 0) goto L_0x03c6
            r4 = 0
            goto L_0x03c7
        L_0x03c6:
            r4 = -1
        L_0x03c7:
            if (r4 == 0) goto L_0x03d1
            r8 = 1
            if (r4 == r8) goto L_0x03d1
            r8 = 2
            if (r4 == r8) goto L_0x03d1
            r4 = 0
            goto L_0x03d2
        L_0x03d1:
            r4 = 1
        L_0x03d2:
            if (r4 == 0) goto L_0x03d5
            goto L_0x03e2
        L_0x03d5:
            r8 = r3
            r30 = r6
            r29 = r11
            r31 = r12
            goto L_0x05be
        L_0x03de:
            r28 = r4
            r27 = r8
        L_0x03e2:
            r29 = r11
            r4 = 0
            r8 = 0
            r15 = 0
        L_0x03e7:
            int r11 = r7.zzb()     // Catch:{ all -> 0x0f8c }
            r30 = r6
            java.lang.String r6 = "_r"
            if (r4 >= r11) goto L_0x0455
            com.google.android.gms.internal.measurement.zzbs$zze r11 = r7.zza(r4)     // Catch:{ all -> 0x0f8c }
            java.lang.String r11 = r11.zzb()     // Catch:{ all -> 0x0f8c }
            boolean r11 = r14.equals(r11)     // Catch:{ all -> 0x0f8c }
            if (r11 == 0) goto L_0x0420
            com.google.android.gms.internal.measurement.zzbs$zze r6 = r7.zza(r4)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r6 = r6.zzbl()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r6 = (com.google.android.gms.internal.measurement.zzfe.zza) r6     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = (com.google.android.gms.internal.measurement.zzbs.zze.zza) r6     // Catch:{ all -> 0x0f8c }
            r31 = r12
            r11 = 1
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = r6.zza(r11)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r6 = r6.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r6 = (com.google.android.gms.internal.measurement.zzfe) r6     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r6 = (com.google.android.gms.internal.measurement.zzbs.zze) r6     // Catch:{ all -> 0x0f8c }
            r7.zza(r4, r6)     // Catch:{ all -> 0x0f8c }
            r8 = 1
            goto L_0x044e
        L_0x0420:
            r31 = r12
            com.google.android.gms.internal.measurement.zzbs$zze r11 = r7.zza(r4)     // Catch:{ all -> 0x0f8c }
            java.lang.String r11 = r11.zzb()     // Catch:{ all -> 0x0f8c }
            boolean r6 = r6.equals(r11)     // Catch:{ all -> 0x0f8c }
            if (r6 == 0) goto L_0x044e
            com.google.android.gms.internal.measurement.zzbs$zze r6 = r7.zza(r4)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r6 = r6.zzbl()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r6 = (com.google.android.gms.internal.measurement.zzfe.zza) r6     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = (com.google.android.gms.internal.measurement.zzbs.zze.zza) r6     // Catch:{ all -> 0x0f8c }
            r11 = 1
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = r6.zza(r11)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r6 = r6.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r6 = (com.google.android.gms.internal.measurement.zzfe) r6     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r6 = (com.google.android.gms.internal.measurement.zzbs.zze) r6     // Catch:{ all -> 0x0f8c }
            r7.zza(r4, r6)     // Catch:{ all -> 0x0f8c }
            r15 = 1
        L_0x044e:
            int r4 = r4 + 1
            r6 = r30
            r12 = r31
            goto L_0x03e7
        L_0x0455:
            r31 = r12
            if (r8 != 0) goto L_0x0489
            if (r9 == 0) goto L_0x0489
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.String r8 = "Marking event as conversion"
            com.google.android.gms.measurement.internal.zzgf r11 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzez r11 = r11.zzj()     // Catch:{ all -> 0x0f8c }
            java.lang.String r12 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            java.lang.String r11 = r11.zza(r12)     // Catch:{ all -> 0x0f8c }
            r4.zza(r8, r11)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r4 = com.google.android.gms.internal.measurement.zzbs.zze.zzk()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r4 = r4.zza(r14)     // Catch:{ all -> 0x0f8c }
            r11 = 1
            com.google.android.gms.internal.measurement.zzbs$zze$zza r4 = r4.zza(r11)     // Catch:{ all -> 0x0f8c }
            r7.zza(r4)     // Catch:{ all -> 0x0f8c }
        L_0x0489:
            if (r15 != 0) goto L_0x04b9
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.String r8 = "Marking event as real-time"
            com.google.android.gms.measurement.internal.zzgf r11 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzez r11 = r11.zzj()     // Catch:{ all -> 0x0f8c }
            java.lang.String r12 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            java.lang.String r11 = r11.zza(r12)     // Catch:{ all -> 0x0f8c }
            r4.zza(r8, r11)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r4 = com.google.android.gms.internal.measurement.zzbs.zze.zzk()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r4 = r4.zza(r6)     // Catch:{ all -> 0x0f8c }
            r11 = 1
            com.google.android.gms.internal.measurement.zzbs$zze$zza r4 = r4.zza(r11)     // Catch:{ all -> 0x0f8c }
            r7.zza(r4)     // Catch:{ all -> 0x0f8c }
        L_0x04b9:
            com.google.android.gms.measurement.internal.zzac r32 = r59.zze()     // Catch:{ all -> 0x0f8c }
            long r33 = r59.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r4 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r35 = r4.zzx()     // Catch:{ all -> 0x0f8c }
            r36 = 0
            r37 = 0
            r38 = 0
            r39 = 0
            r40 = 1
            com.google.android.gms.measurement.internal.zzab r4 = r32.zza(r33, r35, r36, r37, r38, r39, r40)     // Catch:{ all -> 0x0f8c }
            long r11 = r4.zze     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r4 = r4.zzb()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r8 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r8 = r8.zzx()     // Catch:{ all -> 0x0f8c }
            int r4 = r4.zza(r8)     // Catch:{ all -> 0x0f8c }
            r8 = r3
            long r3 = (long) r4     // Catch:{ all -> 0x0f8c }
            int r15 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r15 <= 0) goto L_0x04f1
            zza(r7, r6)     // Catch:{ all -> 0x0f8c }
            goto L_0x04f3
        L_0x04f1:
            r21 = 1
        L_0x04f3:
            java.lang.String r3 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            boolean r3 = com.google.android.gms.measurement.internal.zzkv.zza(r3)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x05be
            if (r9 == 0) goto L_0x05be
            com.google.android.gms.measurement.internal.zzac r32 = r59.zze()     // Catch:{ all -> 0x0f8c }
            long r33 = r59.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r3 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r35 = r3.zzx()     // Catch:{ all -> 0x0f8c }
            r36 = 0
            r37 = 0
            r38 = 1
            r39 = 0
            r40 = 0
            com.google.android.gms.measurement.internal.zzab r3 = r32.zza(r33, r35, r36, r37, r38, r39, r40)     // Catch:{ all -> 0x0f8c }
            long r3 = r3.zzc     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r6 = r6.zzb()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r11 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r11 = r11.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Integer> r12 = com.google.android.gms.measurement.internal.zzap.zzm     // Catch:{ all -> 0x0f8c }
            int r6 = r6.zzb(r11, r12)     // Catch:{ all -> 0x0f8c }
            long r11 = (long) r6     // Catch:{ all -> 0x0f8c }
            int r6 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r6 <= 0) goto L_0x05be
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzi()     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = "Too many conversions. Not logging as conversion. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = r6.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.zzfb.zza(r6)     // Catch:{ all -> 0x0f8c }
            r3.zza(r4, r6)     // Catch:{ all -> 0x0f8c }
            r3 = 0
            r4 = 0
            r6 = 0
            r11 = -1
        L_0x0551:
            int r12 = r7.zzb()     // Catch:{ all -> 0x0f8c }
            if (r3 >= r12) goto L_0x057d
            com.google.android.gms.internal.measurement.zzbs$zze r12 = r7.zza(r3)     // Catch:{ all -> 0x0f8c }
            java.lang.String r15 = r12.zzb()     // Catch:{ all -> 0x0f8c }
            boolean r15 = r14.equals(r15)     // Catch:{ all -> 0x0f8c }
            if (r15 == 0) goto L_0x056f
            com.google.android.gms.internal.measurement.zzfe$zza r6 = r12.zzbl()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r6 = (com.google.android.gms.internal.measurement.zzfe.zza) r6     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r6 = (com.google.android.gms.internal.measurement.zzbs.zze.zza) r6     // Catch:{ all -> 0x0f8c }
            r11 = r3
            goto L_0x057a
        L_0x056f:
            java.lang.String r12 = r12.zzb()     // Catch:{ all -> 0x0f8c }
            boolean r12 = r13.equals(r12)     // Catch:{ all -> 0x0f8c }
            if (r12 == 0) goto L_0x057a
            r4 = 1
        L_0x057a:
            int r3 = r3 + 1
            goto L_0x0551
        L_0x057d:
            if (r4 == 0) goto L_0x0585
            if (r6 == 0) goto L_0x0585
            r7.zzb(r11)     // Catch:{ all -> 0x0f8c }
            goto L_0x05be
        L_0x0585:
            if (r6 == 0) goto L_0x05a5
            java.lang.Object r3 = r6.clone()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r3 = (com.google.android.gms.internal.measurement.zzfe.zza) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r3 = (com.google.android.gms.internal.measurement.zzbs.zze.zza) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze$zza r3 = r3.zza(r13)     // Catch:{ all -> 0x0f8c }
            r12 = 10
            com.google.android.gms.internal.measurement.zzbs$zze$zza r3 = r3.zza(r12)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r3 = r3.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r3 = (com.google.android.gms.internal.measurement.zzfe) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r3 = (com.google.android.gms.internal.measurement.zzbs.zze) r3     // Catch:{ all -> 0x0f8c }
            r7.zza(r11, r3)     // Catch:{ all -> 0x0f8c }
            goto L_0x05be
        L_0x05a5:
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzf()     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = "Did not find conversion parameter. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = r6.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.zzfb.zza(r6)     // Catch:{ all -> 0x0f8c }
            r3.zza(r4, r6)     // Catch:{ all -> 0x0f8c }
        L_0x05be:
            if (r9 == 0) goto L_0x0685
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x0f8c }
            java.util.List r4 = r7.zza()     // Catch:{ all -> 0x0f8c }
            r3.<init>(r4)     // Catch:{ all -> 0x0f8c }
            r4 = 0
            r6 = -1
            r9 = -1
        L_0x05cc:
            int r11 = r3.size()     // Catch:{ all -> 0x0f8c }
            if (r4 >= r11) goto L_0x05fc
            java.lang.String r11 = "value"
            java.lang.Object r12 = r3.get(r4)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r12 = (com.google.android.gms.internal.measurement.zzbs.zze) r12     // Catch:{ all -> 0x0f8c }
            java.lang.String r12 = r12.zzb()     // Catch:{ all -> 0x0f8c }
            boolean r11 = r11.equals(r12)     // Catch:{ all -> 0x0f8c }
            if (r11 == 0) goto L_0x05e6
            r6 = r4
            goto L_0x05f9
        L_0x05e6:
            java.lang.String r11 = "currency"
            java.lang.Object r12 = r3.get(r4)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r12 = (com.google.android.gms.internal.measurement.zzbs.zze) r12     // Catch:{ all -> 0x0f8c }
            java.lang.String r12 = r12.zzb()     // Catch:{ all -> 0x0f8c }
            boolean r11 = r11.equals(r12)     // Catch:{ all -> 0x0f8c }
            if (r11 == 0) goto L_0x05f9
            r9 = r4
        L_0x05f9:
            int r4 = r4 + 1
            goto L_0x05cc
        L_0x05fc:
            r4 = -1
            if (r6 == r4) goto L_0x0686
            java.lang.Object r4 = r3.get(r6)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r4 = (com.google.android.gms.internal.measurement.zzbs.zze) r4     // Catch:{ all -> 0x0f8c }
            boolean r4 = r4.zze()     // Catch:{ all -> 0x0f8c }
            if (r4 != 0) goto L_0x0634
            java.lang.Object r4 = r3.get(r6)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r4 = (com.google.android.gms.internal.measurement.zzbs.zze) r4     // Catch:{ all -> 0x0f8c }
            boolean r4 = r4.zzg()     // Catch:{ all -> 0x0f8c }
            if (r4 != 0) goto L_0x0634
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzk()     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = "Value must be specified with a numeric type."
            r3.zza(r4)     // Catch:{ all -> 0x0f8c }
            r7.zzb(r6)     // Catch:{ all -> 0x0f8c }
            zza(r7, r14)     // Catch:{ all -> 0x0f8c }
            r3 = 18
            java.lang.String r4 = "value"
            zza(r7, r3, r4)     // Catch:{ all -> 0x0f8c }
            goto L_0x0685
        L_0x0634:
            r4 = -1
            if (r9 != r4) goto L_0x063a
            r3 = 1
            r11 = 3
            goto L_0x0666
        L_0x063a:
            java.lang.Object r3 = r3.get(r9)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r3 = (com.google.android.gms.internal.measurement.zzbs.zze) r3     // Catch:{ all -> 0x0f8c }
            java.lang.String r3 = r3.zzd()     // Catch:{ all -> 0x0f8c }
            int r9 = r3.length()     // Catch:{ all -> 0x0f8c }
            r11 = 3
            if (r9 == r11) goto L_0x064d
        L_0x064b:
            r3 = 1
            goto L_0x0666
        L_0x064d:
            r9 = 0
        L_0x064e:
            int r12 = r3.length()     // Catch:{ all -> 0x0f8c }
            if (r9 >= r12) goto L_0x0665
            int r12 = r3.codePointAt(r9)     // Catch:{ all -> 0x0f8c }
            boolean r13 = java.lang.Character.isLetter(r12)     // Catch:{ all -> 0x0f8c }
            if (r13 != 0) goto L_0x065f
            goto L_0x064b
        L_0x065f:
            int r12 = java.lang.Character.charCount(r12)     // Catch:{ all -> 0x0f8c }
            int r9 = r9 + r12
            goto L_0x064e
        L_0x0665:
            r3 = 0
        L_0x0666:
            if (r3 == 0) goto L_0x0687
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzk()     // Catch:{ all -> 0x0f8c }
            java.lang.String r9 = "Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter."
            r3.zza(r9)     // Catch:{ all -> 0x0f8c }
            r7.zzb(r6)     // Catch:{ all -> 0x0f8c }
            zza(r7, r14)     // Catch:{ all -> 0x0f8c }
            r3 = 19
            java.lang.String r6 = "currency"
            zza(r7, r3, r6)     // Catch:{ all -> 0x0f8c }
            goto L_0x0687
        L_0x0685:
            r4 = -1
        L_0x0686:
            r11 = 3
        L_0x0687:
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = r6.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.zzap.zzax     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.zze(r6, r9)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x079c
            java.lang.String r3 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            boolean r3 = r10.equals(r3)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x06f0
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r3 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r3 = (com.google.android.gms.internal.measurement.zzfe) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r3 = (com.google.android.gms.internal.measurement.zzbs.zzc) r3     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = "_fr"
            com.google.android.gms.internal.measurement.zzbs$zze r3 = com.google.android.gms.measurement.internal.zzkr.zza(r3, r6)     // Catch:{ all -> 0x0f8c }
            if (r3 != 0) goto L_0x079c
            if (r17 == 0) goto L_0x06e8
            long r12 = r17.zzf()     // Catch:{ all -> 0x0f8c }
            long r14 = r7.zzf()     // Catch:{ all -> 0x0f8c }
            long r12 = r12 - r14
            long r12 = java.lang.Math.abs(r12)     // Catch:{ all -> 0x0f8c }
            r14 = 1000(0x3e8, double:4.94E-321)
            int r3 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r3 > 0) goto L_0x06e8
            java.lang.Object r3 = r17.clone()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r3 = (com.google.android.gms.internal.measurement.zzfe.zza) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r3 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r3     // Catch:{ all -> 0x0f8c }
            boolean r6 = r1.zza(r7, r3)     // Catch:{ all -> 0x0f8c }
            if (r6 == 0) goto L_0x06e8
            r8.zza(r5, r3)     // Catch:{ all -> 0x0f8c }
            r9 = r29
            r6 = r31
        L_0x06e2:
            r17 = 0
        L_0x06e4:
            r30 = 0
            goto L_0x07a0
        L_0x06e8:
            r30 = r7
            r9 = r22
        L_0x06ec:
            r6 = r31
            goto L_0x07a0
        L_0x06f0:
            java.lang.String r3 = "_vs"
            java.lang.String r6 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.equals(r6)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x0742
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r3 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r3 = (com.google.android.gms.internal.measurement.zzfe) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r3 = (com.google.android.gms.internal.measurement.zzbs.zzc) r3     // Catch:{ all -> 0x0f8c }
            r6 = r31
            com.google.android.gms.internal.measurement.zzbs$zze r3 = com.google.android.gms.measurement.internal.zzkr.zza(r3, r6)     // Catch:{ all -> 0x0f8c }
            if (r3 != 0) goto L_0x073f
            if (r30 == 0) goto L_0x0738
            long r12 = r30.zzf()     // Catch:{ all -> 0x0f8c }
            long r14 = r7.zzf()     // Catch:{ all -> 0x0f8c }
            long r12 = r12 - r14
            long r12 = java.lang.Math.abs(r12)     // Catch:{ all -> 0x0f8c }
            r14 = 1000(0x3e8, double:4.94E-321)
            int r3 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r3 > 0) goto L_0x0738
            java.lang.Object r3 = r30.clone()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r3 = (com.google.android.gms.internal.measurement.zzfe.zza) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r3 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r3     // Catch:{ all -> 0x0f8c }
            boolean r9 = r1.zza(r3, r7)     // Catch:{ all -> 0x0f8c }
            if (r9 == 0) goto L_0x0738
            r9 = r29
            r8.zza(r9, r3)     // Catch:{ all -> 0x0f8c }
            goto L_0x06e2
        L_0x0738:
            r9 = r29
            r17 = r7
            r5 = r22
            goto L_0x07a0
        L_0x073f:
            r9 = r29
            goto L_0x07a0
        L_0x0742:
            r9 = r29
            r6 = r31
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r12 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r12 = r12.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r13 = com.google.android.gms.measurement.internal.zzap.zzcj     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.zze(r12, r13)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x07a0
            java.lang.String r3 = "_ab"
            java.lang.String r12 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.equals(r12)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x07a0
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r3 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r3 = (com.google.android.gms.internal.measurement.zzfe) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r3 = (com.google.android.gms.internal.measurement.zzbs.zzc) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r3 = com.google.android.gms.measurement.internal.zzkr.zza(r3, r6)     // Catch:{ all -> 0x0f8c }
            if (r3 != 0) goto L_0x07a0
            if (r30 == 0) goto L_0x07a0
            long r12 = r30.zzf()     // Catch:{ all -> 0x0f8c }
            long r14 = r7.zzf()     // Catch:{ all -> 0x0f8c }
            long r12 = r12 - r14
            long r12 = java.lang.Math.abs(r12)     // Catch:{ all -> 0x0f8c }
            r14 = 4000(0xfa0, double:1.9763E-320)
            int r3 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r3 > 0) goto L_0x07a0
            java.lang.Object r3 = r30.clone()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe$zza r3 = (com.google.android.gms.internal.measurement.zzfe.zza) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r3 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r3     // Catch:{ all -> 0x0f8c }
            r1.zzb(r3, r7)     // Catch:{ all -> 0x0f8c }
            r8.zza(r9, r3)     // Catch:{ all -> 0x0f8c }
            goto L_0x06e4
        L_0x079c:
            r9 = r29
            goto L_0x06ec
        L_0x07a0:
            if (r28 != 0) goto L_0x0800
            java.lang.String r3 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            boolean r3 = r10.equals(r3)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x0800
            int r3 = r7.zzb()     // Catch:{ all -> 0x0f8c }
            if (r3 != 0) goto L_0x07cc
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzi()     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = "Engagement event does not contain any parameters. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = r10.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.Object r10 = com.google.android.gms.measurement.internal.zzfb.zza(r10)     // Catch:{ all -> 0x0f8c }
            r3.zza(r6, r10)     // Catch:{ all -> 0x0f8c }
            goto L_0x0800
        L_0x07cc:
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r3 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r3 = (com.google.android.gms.internal.measurement.zzfe) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r3 = (com.google.android.gms.internal.measurement.zzbs.zzc) r3     // Catch:{ all -> 0x0f8c }
            java.lang.Object r3 = com.google.android.gms.measurement.internal.zzkr.zzb(r3, r6)     // Catch:{ all -> 0x0f8c }
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ all -> 0x0f8c }
            if (r3 != 0) goto L_0x07f9
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzi()     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = "Engagement event does not include duration. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = r10.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.Object r10 = com.google.android.gms.measurement.internal.zzfb.zza(r10)     // Catch:{ all -> 0x0f8c }
            r3.zza(r6, r10)     // Catch:{ all -> 0x0f8c }
            goto L_0x0800
        L_0x07f9:
            long r12 = r3.longValue()     // Catch:{ all -> 0x0f8c }
            long r12 = r23 + r12
            goto L_0x0802
        L_0x0800:
            r12 = r23
        L_0x0802:
            java.util.List<com.google.android.gms.internal.measurement.zzbs$zzc> r3 = r2.zzc     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r6 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r6 = (com.google.android.gms.internal.measurement.zzfe) r6     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r6 = (com.google.android.gms.internal.measurement.zzbs.zzc) r6     // Catch:{ all -> 0x0f8c }
            r10 = r27
            r3.set(r10, r6)     // Catch:{ all -> 0x0f8c }
            int r15 = r22 + 1
            r8.zza(r7)     // Catch:{ all -> 0x0f8c }
            r3 = r9
            r13 = r12
            r9 = r21
            r6 = r30
        L_0x081c:
            int r7 = r10 + 1
            r11 = r3
            r3 = r8
            r4 = r28
            r10 = 2
            r12 = 1
            r8 = r7
            goto L_0x02b7
        L_0x0827:
            r8 = r3
            r28 = r4
            r21 = r9
            r6 = r12
            r22 = r15
            if (r28 == 0) goto L_0x0886
            r4 = r22
            r13 = r23
            r3 = 0
        L_0x0836:
            if (r3 >= r4) goto L_0x0888
            com.google.android.gms.internal.measurement.zzbs$zzc r5 = r8.zzb(r3)     // Catch:{ all -> 0x0f8c }
            java.lang.String r7 = r5.zzc()     // Catch:{ all -> 0x0f8c }
            boolean r7 = r10.equals(r7)     // Catch:{ all -> 0x0f8c }
            if (r7 == 0) goto L_0x0859
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            java.lang.String r7 = "_fr"
            com.google.android.gms.internal.measurement.zzbs$zze r7 = com.google.android.gms.measurement.internal.zzkr.zza(r5, r7)     // Catch:{ all -> 0x0f8c }
            if (r7 == 0) goto L_0x0859
            r8.zzc(r3)     // Catch:{ all -> 0x0f8c }
            int r4 = r4 + -1
            int r3 = r3 + -1
            goto L_0x0883
        L_0x0859:
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r5 = com.google.android.gms.measurement.internal.zzkr.zza(r5, r6)     // Catch:{ all -> 0x0f8c }
            if (r5 == 0) goto L_0x0883
            boolean r7 = r5.zze()     // Catch:{ all -> 0x0f8c }
            if (r7 == 0) goto L_0x0871
            long r11 = r5.zzf()     // Catch:{ all -> 0x0f8c }
            java.lang.Long r5 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x0f8c }
            goto L_0x0872
        L_0x0871:
            r5 = 0
        L_0x0872:
            if (r5 == 0) goto L_0x0883
            long r11 = r5.longValue()     // Catch:{ all -> 0x0f8c }
            r17 = 0
            int r7 = (r11 > r17 ? 1 : (r11 == r17 ? 0 : -1))
            if (r7 <= 0) goto L_0x0883
            long r11 = r5.longValue()     // Catch:{ all -> 0x0f8c }
            long r13 = r13 + r11
        L_0x0883:
            r5 = 1
            int r3 = r3 + r5
            goto L_0x0836
        L_0x0886:
            r13 = r23
        L_0x0888:
            r3 = 0
            r1.zza(r8, r13, r3)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.zzap.zzbm     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.zze(r4, r5)     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = "_se"
            if (r3 == 0) goto L_0x0951
            java.util.List r3 = r8.zza()     // Catch:{ all -> 0x0f8c }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x0f8c }
        L_0x08a8:
            boolean r5 = r3.hasNext()     // Catch:{ all -> 0x0f8c }
            if (r5 == 0) goto L_0x08c2
            java.lang.Object r5 = r3.next()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r5 = (com.google.android.gms.internal.measurement.zzbs.zzc) r5     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = "_s"
            java.lang.String r5 = r5.zzc()     // Catch:{ all -> 0x0f8c }
            boolean r5 = r6.equals(r5)     // Catch:{ all -> 0x0f8c }
            if (r5 == 0) goto L_0x08a8
            r3 = 1
            goto L_0x08c3
        L_0x08c2:
            r3 = 0
        L_0x08c3:
            if (r3 == 0) goto L_0x08d0
            com.google.android.gms.measurement.internal.zzac r3 = r59.zze()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            r3.zzb(r5, r4)     // Catch:{ all -> 0x0f8c }
        L_0x08d0:
            boolean r3 = com.google.android.gms.internal.measurement.zzms.zzb()     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x094c
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.zzap.zzbn     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.zze(r5, r6)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x094c
            java.lang.String r3 = "_sid"
            int r3 = com.google.android.gms.measurement.internal.zzkr.zza(r8, r3)     // Catch:{ all -> 0x0f8c }
            if (r3 < 0) goto L_0x08f2
            r3 = 1
            goto L_0x08f3
        L_0x08f2:
            r3 = 0
        L_0x08f3:
            if (r3 != 0) goto L_0x094c
            int r3 = com.google.android.gms.measurement.internal.zzkr.zza(r8, r4)     // Catch:{ all -> 0x0f8c }
            if (r3 < 0) goto L_0x096e
            r8.zze(r3)     // Catch:{ all -> 0x0f8c }
            boolean r3 = com.google.android.gms.internal.measurement.zzkw.zzb()     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x0932
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r4 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = r4.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.zzap.zzcx     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.zze(r4, r5)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x0932
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzf()     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = "Session engagement user property is in the bundle without session ID. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r5 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r5.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.Object r5 = com.google.android.gms.measurement.internal.zzfb.zza(r5)     // Catch:{ all -> 0x0f8c }
            r3.zza(r4, r5)     // Catch:{ all -> 0x0f8c }
            goto L_0x096e
        L_0x0932:
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r3 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r3 = r3.zzi()     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = "Session engagement user property is in the bundle without session ID. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r5 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r5.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.Object r5 = com.google.android.gms.measurement.internal.zzfb.zza(r5)     // Catch:{ all -> 0x0f8c }
            r3.zza(r4, r5)     // Catch:{ all -> 0x0f8c }
            goto L_0x096e
        L_0x094c:
            r3 = 1
            r1.zza(r8, r13, r3)     // Catch:{ all -> 0x0f8c }
            goto L_0x096e
        L_0x0951:
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.zzap.zzbp     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.zze(r5, r6)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x096e
            com.google.android.gms.measurement.internal.zzac r3 = r59.zze()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            r3.zzb(r5, r4)     // Catch:{ all -> 0x0f8c }
        L_0x096e:
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.zzap.zzaz     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.zze(r4, r5)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x0a13
            com.google.android.gms.measurement.internal.zzkr r3 = r59.zzh()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r4 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = "Checking account type status for ad personalization signals"
            r4.zza(r5)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfz r4 = r3.zzj()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            boolean r4 = r4.zze(r5)     // Catch:{ all -> 0x0f8c }
            if (r4 == 0) goto L_0x0a13
            com.google.android.gms.measurement.internal.zzac r4 = r3.zzi()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzg r4 = r4.zzb(r5)     // Catch:{ all -> 0x0f8c }
            if (r4 == 0) goto L_0x0a13
            boolean r4 = r4.zzaf()     // Catch:{ all -> 0x0f8c }
            if (r4 == 0) goto L_0x0a13
            com.google.android.gms.measurement.internal.zzah r4 = r3.zzl()     // Catch:{ all -> 0x0f8c }
            boolean r4 = r4.zzj()     // Catch:{ all -> 0x0f8c }
            if (r4 == 0) goto L_0x0a13
            com.google.android.gms.measurement.internal.zzfb r4 = r3.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzw()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = "Turning off ad personalization due to account type"
            r4.zza(r5)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r4 = com.google.android.gms.internal.measurement.zzbs.zzk.zzj()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = "_npa"
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r4 = r4.zza(r5)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzah r3 = r3.zzl()     // Catch:{ all -> 0x0f8c }
            long r5 = r3.zzh()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r3 = r4.zza(r5)     // Catch:{ all -> 0x0f8c }
            r4 = 1
            com.google.android.gms.internal.measurement.zzbs$zzk$zza r3 = r3.zzb(r4)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r3 = r3.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r3 = (com.google.android.gms.internal.measurement.zzfe) r3     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzk r3 = (com.google.android.gms.internal.measurement.zzbs.zzk) r3     // Catch:{ all -> 0x0f8c }
            r4 = 0
        L_0x09ef:
            int r5 = r8.zze()     // Catch:{ all -> 0x0f8c }
            if (r4 >= r5) goto L_0x0a0d
            java.lang.String r5 = "_npa"
            com.google.android.gms.internal.measurement.zzbs$zzk r6 = r8.zzd(r4)     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = r6.zzc()     // Catch:{ all -> 0x0f8c }
            boolean r5 = r5.equals(r6)     // Catch:{ all -> 0x0f8c }
            if (r5 == 0) goto L_0x0a0a
            r8.zza(r4, r3)     // Catch:{ all -> 0x0f8c }
            r4 = 1
            goto L_0x0a0e
        L_0x0a0a:
            int r4 = r4 + 1
            goto L_0x09ef
        L_0x0a0d:
            r4 = 0
        L_0x0a0e:
            if (r4 != 0) goto L_0x0a13
            r8.zza(r3)     // Catch:{ all -> 0x0f8c }
        L_0x0a13:
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.zzap.zzce     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.zze(r4, r5)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x0a28
            zza(r8)     // Catch:{ all -> 0x0f8c }
        L_0x0a28:
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r3 = r8.zzm()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzn r9 = r59.zzf()     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            java.util.List r11 = r8.zza()     // Catch:{ all -> 0x0f8c }
            java.util.List r12 = r8.zzd()     // Catch:{ all -> 0x0f8c }
            long r4 = r8.zzf()     // Catch:{ all -> 0x0f8c }
            java.lang.Long r13 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x0f8c }
            long r4 = r8.zzg()     // Catch:{ all -> 0x0f8c }
            java.lang.Long r14 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x0f8c }
            java.util.List r4 = r9.zza(r10, r11, r12, r13, r14)     // Catch:{ all -> 0x0f8c }
            r3.zzc(r4)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r4 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = r4.zzx()     // Catch:{ all -> 0x0f8c }
            boolean r3 = r3.zze(r4)     // Catch:{ all -> 0x0f8c }
            if (r3 == 0) goto L_0x0dcd
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ all -> 0x0dc8 }
            r3.<init>()     // Catch:{ all -> 0x0dc8 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x0dc8 }
            r4.<init>()     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.measurement.internal.zzgf r5 = r1.zzj     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.measurement.internal.zzkv r5 = r5.zzi()     // Catch:{ all -> 0x0dc8 }
            java.security.SecureRandom r5 = r5.zzh()     // Catch:{ all -> 0x0dc8 }
            r6 = 0
        L_0x0a7a:
            int r7 = r8.zzb()     // Catch:{ all -> 0x0dc8 }
            if (r6 >= r7) goto L_0x0d93
            com.google.android.gms.internal.measurement.zzbs$zzc r7 = r8.zzb(r6)     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzfe$zza r7 = r7.zzbl()     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzfe$zza r7 = (com.google.android.gms.internal.measurement.zzfe.zza) r7     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzbs$zzc$zza r7 = (com.google.android.gms.internal.measurement.zzbs.zzc.zza) r7     // Catch:{ all -> 0x0dc8 }
            java.lang.String r9 = r7.zzd()     // Catch:{ all -> 0x0dc8 }
            java.lang.String r10 = "_ep"
            boolean r9 = r9.equals(r10)     // Catch:{ all -> 0x0dc8 }
            if (r9 == 0) goto L_0x0b0d
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r9 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r9 = (com.google.android.gms.internal.measurement.zzfe) r9     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = "_en"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzkr.zzb(r9, r10)     // Catch:{ all -> 0x0f8c }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x0f8c }
            java.lang.Object r10 = r3.get(r9)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzaj r10 = (com.google.android.gms.measurement.internal.zzaj) r10     // Catch:{ all -> 0x0f8c }
            if (r10 != 0) goto L_0x0ac4
            com.google.android.gms.measurement.internal.zzac r10 = r59.zze()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r11 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r11 = r11.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzaj r10 = r10.zza(r11, r9)     // Catch:{ all -> 0x0f8c }
            r3.put(r9, r10)     // Catch:{ all -> 0x0f8c }
        L_0x0ac4:
            java.lang.Long r9 = r10.zzi     // Catch:{ all -> 0x0f8c }
            if (r9 != 0) goto L_0x0b03
            java.lang.Long r9 = r10.zzj     // Catch:{ all -> 0x0f8c }
            long r11 = r9.longValue()     // Catch:{ all -> 0x0f8c }
            r13 = 1
            int r9 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r9 <= 0) goto L_0x0ade
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            java.lang.String r9 = "_sr"
            java.lang.Long r11 = r10.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzkr.zza(r7, r9, r11)     // Catch:{ all -> 0x0f8c }
        L_0x0ade:
            java.lang.Boolean r9 = r10.zzk     // Catch:{ all -> 0x0f8c }
            if (r9 == 0) goto L_0x0af8
            java.lang.Boolean r9 = r10.zzk     // Catch:{ all -> 0x0f8c }
            boolean r9 = r9.booleanValue()     // Catch:{ all -> 0x0f8c }
            if (r9 == 0) goto L_0x0af8
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            java.lang.String r9 = "_efs"
            r10 = 1
            java.lang.Long r12 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzkr.zza(r7, r9, r12)     // Catch:{ all -> 0x0f8c }
        L_0x0af8:
            com.google.android.gms.internal.measurement.zzgm r9 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r9 = (com.google.android.gms.internal.measurement.zzfe) r9     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0f8c }
            r4.add(r9)     // Catch:{ all -> 0x0f8c }
        L_0x0b03:
            r8.zza(r6, r7)     // Catch:{ all -> 0x0f8c }
        L_0x0b06:
            r22 = r2
            r60 = r5
            r1 = r6
            goto L_0x0d89
        L_0x0b0d:
            com.google.android.gms.measurement.internal.zzfz r9 = r59.zzc()     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zza     // Catch:{ all -> 0x0dc8 }
            java.lang.String r10 = r10.zzx()     // Catch:{ all -> 0x0dc8 }
            long r9 = r9.zzf(r10)     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.measurement.internal.zzgf r11 = r1.zzj     // Catch:{ all -> 0x0dc8 }
            r11.zzi()     // Catch:{ all -> 0x0dc8 }
            long r11 = r7.zzf()     // Catch:{ all -> 0x0dc8 }
            long r11 = com.google.android.gms.measurement.internal.zzkv.zza(r11, r9)     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzgm r13 = r7.zzv()     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzfe r13 = (com.google.android.gms.internal.measurement.zzfe) r13     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzbs$zzc r13 = (com.google.android.gms.internal.measurement.zzbs.zzc) r13     // Catch:{ all -> 0x0dc8 }
            java.lang.String r14 = "_dbg"
            r17 = 1
            java.lang.Long r15 = java.lang.Long.valueOf(r17)     // Catch:{ all -> 0x0dc8 }
            boolean r17 = android.text.TextUtils.isEmpty(r14)     // Catch:{ all -> 0x0dc8 }
            if (r17 != 0) goto L_0x0b98
            if (r15 != 0) goto L_0x0b41
            goto L_0x0b98
        L_0x0b41:
            java.util.List r13 = r13.zza()     // Catch:{ all -> 0x0f8c }
            java.util.Iterator r13 = r13.iterator()     // Catch:{ all -> 0x0f8c }
        L_0x0b49:
            boolean r17 = r13.hasNext()     // Catch:{ all -> 0x0f8c }
            if (r17 == 0) goto L_0x0b98
            java.lang.Object r17 = r13.next()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zze r17 = (com.google.android.gms.internal.measurement.zzbs.zze) r17     // Catch:{ all -> 0x0f8c }
            r60 = r13
            java.lang.String r13 = r17.zzb()     // Catch:{ all -> 0x0f8c }
            boolean r13 = r14.equals(r13)     // Catch:{ all -> 0x0f8c }
            if (r13 == 0) goto L_0x0b95
            boolean r13 = r15 instanceof java.lang.Long     // Catch:{ all -> 0x0f8c }
            if (r13 == 0) goto L_0x0b73
            long r13 = r17.zzf()     // Catch:{ all -> 0x0f8c }
            java.lang.Long r13 = java.lang.Long.valueOf(r13)     // Catch:{ all -> 0x0f8c }
            boolean r13 = r15.equals(r13)     // Catch:{ all -> 0x0f8c }
            if (r13 != 0) goto L_0x0b93
        L_0x0b73:
            boolean r13 = r15 instanceof java.lang.String     // Catch:{ all -> 0x0f8c }
            if (r13 == 0) goto L_0x0b81
            java.lang.String r13 = r17.zzd()     // Catch:{ all -> 0x0f8c }
            boolean r13 = r15.equals(r13)     // Catch:{ all -> 0x0f8c }
            if (r13 != 0) goto L_0x0b93
        L_0x0b81:
            boolean r13 = r15 instanceof java.lang.Double     // Catch:{ all -> 0x0f8c }
            if (r13 == 0) goto L_0x0b98
            double r13 = r17.zzh()     // Catch:{ all -> 0x0f8c }
            java.lang.Double r13 = java.lang.Double.valueOf(r13)     // Catch:{ all -> 0x0f8c }
            boolean r13 = r15.equals(r13)     // Catch:{ all -> 0x0f8c }
            if (r13 == 0) goto L_0x0b98
        L_0x0b93:
            r13 = 1
            goto L_0x0b99
        L_0x0b95:
            r13 = r60
            goto L_0x0b49
        L_0x0b98:
            r13 = 0
        L_0x0b99:
            if (r13 != 0) goto L_0x0bae
            com.google.android.gms.measurement.internal.zzfz r13 = r59.zzc()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r14 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r14 = r14.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.String r15 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            int r13 = r13.zzd(r14, r15)     // Catch:{ all -> 0x0f8c }
            goto L_0x0baf
        L_0x0bae:
            r13 = 1
        L_0x0baf:
            if (r13 > 0) goto L_0x0bd8
            com.google.android.gms.measurement.internal.zzgf r9 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r9 = r9.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r9 = r9.zzi()     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = "Sample rate must be positive. event, rate"
            java.lang.String r11 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x0f8c }
            r9.zza(r10, r11, r12)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r9 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r9 = (com.google.android.gms.internal.measurement.zzfe) r9     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0f8c }
            r4.add(r9)     // Catch:{ all -> 0x0f8c }
            r8.zza(r6, r7)     // Catch:{ all -> 0x0f8c }
            goto L_0x0b06
        L_0x0bd8:
            java.lang.String r14 = r7.zzd()     // Catch:{ all -> 0x0dc8 }
            java.lang.Object r14 = r3.get(r14)     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.measurement.internal.zzaj r14 = (com.google.android.gms.measurement.internal.zzaj) r14     // Catch:{ all -> 0x0dc8 }
            if (r14 != 0) goto L_0x0c72
            com.google.android.gms.measurement.internal.zzac r14 = r59.zze()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r15 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r15 = r15.zzx()     // Catch:{ all -> 0x0f8c }
            r17 = r9
            java.lang.String r9 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzaj r14 = r14.zza(r15, r9)     // Catch:{ all -> 0x0f8c }
            if (r14 != 0) goto L_0x0c74
            com.google.android.gms.measurement.internal.zzgf r9 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r9 = r9.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r9 = r9.zzi()     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = "Event being bundled has no eventAggregate. appId, eventName"
            com.google.android.gms.internal.measurement.zzbs$zzg r14 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r14 = r14.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.String r15 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            r9.zza(r10, r14, r15)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzgf r9 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r9 = r9.zzb()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r10 = r10.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r14 = com.google.android.gms.measurement.internal.zzap.zzbl     // Catch:{ all -> 0x0f8c }
            boolean r9 = r9.zze(r10, r14)     // Catch:{ all -> 0x0f8c }
            if (r9 == 0) goto L_0x0c4d
            com.google.android.gms.measurement.internal.zzaj r9 = new com.google.android.gms.measurement.internal.zzaj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r28 = r10.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.String r29 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            r30 = 1
            r32 = 1
            r34 = 1
            long r36 = r7.zzf()     // Catch:{ all -> 0x0f8c }
            r38 = 0
            r40 = 0
            r41 = 0
            r42 = 0
            r43 = 0
            r27 = r9
            r27.<init>(r28, r29, r30, r32, r34, r36, r38, r40, r41, r42, r43)     // Catch:{ all -> 0x0f8c }
            goto L_0x0c70
        L_0x0c4d:
            com.google.android.gms.measurement.internal.zzaj r9 = new com.google.android.gms.measurement.internal.zzaj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r10 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r45 = r10.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.String r46 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            r47 = 1
            r49 = 1
            long r51 = r7.zzf()     // Catch:{ all -> 0x0f8c }
            r53 = 0
            r55 = 0
            r56 = 0
            r57 = 0
            r58 = 0
            r44 = r9
            r44.<init>(r45, r46, r47, r49, r51, r53, r55, r56, r57, r58)     // Catch:{ all -> 0x0f8c }
        L_0x0c70:
            r14 = r9
            goto L_0x0c74
        L_0x0c72:
            r17 = r9
        L_0x0c74:
            r59.zzh()     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzgm r9 = r7.zzv()     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzfe r9 = (com.google.android.gms.internal.measurement.zzfe) r9     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0dc8 }
            java.lang.String r10 = "_eid"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.zzkr.zzb(r9, r10)     // Catch:{ all -> 0x0dc8 }
            java.lang.Long r9 = (java.lang.Long) r9     // Catch:{ all -> 0x0dc8 }
            if (r9 == 0) goto L_0x0c8b
            r10 = 1
            goto L_0x0c8c
        L_0x0c8b:
            r10 = 0
        L_0x0c8c:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ all -> 0x0dc8 }
            r15 = 1
            if (r13 != r15) goto L_0x0cc1
            com.google.android.gms.internal.measurement.zzgm r9 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r9 = (com.google.android.gms.internal.measurement.zzfe) r9     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0f8c }
            r4.add(r9)     // Catch:{ all -> 0x0f8c }
            boolean r9 = r10.booleanValue()     // Catch:{ all -> 0x0f8c }
            if (r9 == 0) goto L_0x0cbc
            java.lang.Long r9 = r14.zzi     // Catch:{ all -> 0x0f8c }
            if (r9 != 0) goto L_0x0cb0
            java.lang.Long r9 = r14.zzj     // Catch:{ all -> 0x0f8c }
            if (r9 != 0) goto L_0x0cb0
            java.lang.Boolean r9 = r14.zzk     // Catch:{ all -> 0x0f8c }
            if (r9 == 0) goto L_0x0cbc
        L_0x0cb0:
            r9 = 0
            com.google.android.gms.measurement.internal.zzaj r10 = r14.zza(r9, r9, r9)     // Catch:{ all -> 0x0f8c }
            java.lang.String r9 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            r3.put(r9, r10)     // Catch:{ all -> 0x0f8c }
        L_0x0cbc:
            r8.zza(r6, r7)     // Catch:{ all -> 0x0f8c }
            goto L_0x0b06
        L_0x0cc1:
            int r15 = r5.nextInt(r13)     // Catch:{ all -> 0x0dc8 }
            if (r15 != 0) goto L_0x0d04
            r59.zzh()     // Catch:{ all -> 0x0f8c }
            java.lang.String r9 = "_sr"
            r60 = r5
            r15 = r6
            long r5 = (long) r13     // Catch:{ all -> 0x0f8c }
            java.lang.Long r13 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzkr.zza(r7, r9, r13)     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r9 = r7.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r9 = (com.google.android.gms.internal.measurement.zzfe) r9     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzc r9 = (com.google.android.gms.internal.measurement.zzbs.zzc) r9     // Catch:{ all -> 0x0f8c }
            r4.add(r9)     // Catch:{ all -> 0x0f8c }
            boolean r9 = r10.booleanValue()     // Catch:{ all -> 0x0f8c }
            if (r9 == 0) goto L_0x0cf1
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0f8c }
            r6 = 0
            com.google.android.gms.measurement.internal.zzaj r14 = r14.zza(r6, r5, r6)     // Catch:{ all -> 0x0f8c }
        L_0x0cf1:
            java.lang.String r5 = r7.zzd()     // Catch:{ all -> 0x0f8c }
            long r9 = r7.zzf()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzaj r6 = r14.zza(r9, r11)     // Catch:{ all -> 0x0f8c }
            r3.put(r5, r6)     // Catch:{ all -> 0x0f8c }
            r22 = r2
            goto L_0x0d85
        L_0x0d04:
            r60 = r5
            r15 = r6
            java.lang.Long r5 = r14.zzh     // Catch:{ all -> 0x0dc8 }
            if (r5 == 0) goto L_0x0d14
            java.lang.Long r5 = r14.zzh     // Catch:{ all -> 0x0f8c }
            long r5 = r5.longValue()     // Catch:{ all -> 0x0f8c }
            r22 = r2
            goto L_0x0d25
        L_0x0d14:
            com.google.android.gms.measurement.internal.zzgf r5 = r1.zzj     // Catch:{ all -> 0x0dc8 }
            r5.zzi()     // Catch:{ all -> 0x0dc8 }
            long r5 = r7.zzg()     // Catch:{ all -> 0x0dc8 }
            r22 = r2
            r1 = r17
            long r5 = com.google.android.gms.measurement.internal.zzkv.zza(r5, r1)     // Catch:{ all -> 0x0dc8 }
        L_0x0d25:
            int r1 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r1 == 0) goto L_0x0d73
            r59.zzh()     // Catch:{ all -> 0x0dc8 }
            java.lang.String r1 = "_efs"
            r5 = 1
            java.lang.Long r2 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.measurement.internal.zzkr.zza(r7, r1, r2)     // Catch:{ all -> 0x0dc8 }
            r59.zzh()     // Catch:{ all -> 0x0dc8 }
            java.lang.String r1 = "_sr"
            long r5 = (long) r13     // Catch:{ all -> 0x0dc8 }
            java.lang.Long r2 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.measurement.internal.zzkr.zza(r7, r1, r2)     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzgm r1 = r7.zzv()     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzfe r1 = (com.google.android.gms.internal.measurement.zzfe) r1     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.internal.measurement.zzbs$zzc r1 = (com.google.android.gms.internal.measurement.zzbs.zzc) r1     // Catch:{ all -> 0x0dc8 }
            r4.add(r1)     // Catch:{ all -> 0x0dc8 }
            boolean r1 = r10.booleanValue()     // Catch:{ all -> 0x0dc8 }
            if (r1 == 0) goto L_0x0d63
            java.lang.Long r1 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0dc8 }
            r2 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x0dc8 }
            r2 = 0
            com.google.android.gms.measurement.internal.zzaj r14 = r14.zza(r2, r1, r5)     // Catch:{ all -> 0x0dc8 }
        L_0x0d63:
            java.lang.String r1 = r7.zzd()     // Catch:{ all -> 0x0dc8 }
            long r5 = r7.zzf()     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.measurement.internal.zzaj r2 = r14.zza(r5, r11)     // Catch:{ all -> 0x0dc8 }
            r3.put(r1, r2)     // Catch:{ all -> 0x0dc8 }
            goto L_0x0d85
        L_0x0d73:
            boolean r1 = r10.booleanValue()     // Catch:{ all -> 0x0dc8 }
            if (r1 == 0) goto L_0x0d85
            java.lang.String r1 = r7.zzd()     // Catch:{ all -> 0x0dc8 }
            r2 = 0
            com.google.android.gms.measurement.internal.zzaj r5 = r14.zza(r9, r2, r2)     // Catch:{ all -> 0x0dc8 }
            r3.put(r1, r5)     // Catch:{ all -> 0x0dc8 }
        L_0x0d85:
            r1 = r15
            r8.zza(r1, r7)     // Catch:{ all -> 0x0dc8 }
        L_0x0d89:
            int r6 = r1 + 1
            r1 = r59
            r5 = r60
            r2 = r22
            goto L_0x0a7a
        L_0x0d93:
            r22 = r2
            int r1 = r4.size()     // Catch:{ all -> 0x0dc8 }
            int r2 = r8.zzb()     // Catch:{ all -> 0x0dc8 }
            if (r1 >= r2) goto L_0x0da6
            com.google.android.gms.internal.measurement.zzbs$zzg$zza r1 = r8.zzc()     // Catch:{ all -> 0x0dc8 }
            r1.zza(r4)     // Catch:{ all -> 0x0dc8 }
        L_0x0da6:
            java.util.Set r1 = r3.entrySet()     // Catch:{ all -> 0x0dc8 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x0dc8 }
        L_0x0dae:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x0dc8 }
            if (r2 == 0) goto L_0x0dcf
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x0dc8 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.measurement.internal.zzac r3 = r59.zze()     // Catch:{ all -> 0x0dc8 }
            java.lang.Object r2 = r2.getValue()     // Catch:{ all -> 0x0dc8 }
            com.google.android.gms.measurement.internal.zzaj r2 = (com.google.android.gms.measurement.internal.zzaj) r2     // Catch:{ all -> 0x0dc8 }
            r3.zza(r2)     // Catch:{ all -> 0x0dc8 }
            goto L_0x0dae
        L_0x0dc8:
            r0 = move-exception
            r1 = r59
            goto L_0x0f8d
        L_0x0dcd:
            r22 = r2
        L_0x0dcf:
            r1 = r59
            com.google.android.gms.measurement.internal.zzgf r2 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzx r2 = r2.zzb()     // Catch:{ all -> 0x0f8c }
            java.lang.String r3 = r8.zzj()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r4 = com.google.android.gms.measurement.internal.zzap.zzce     // Catch:{ all -> 0x0f8c }
            boolean r2 = r2.zze(r3, r4)     // Catch:{ all -> 0x0f8c }
            if (r2 != 0) goto L_0x0de6
            zza(r8)     // Catch:{ all -> 0x0f8c }
        L_0x0de6:
            r2 = r22
            com.google.android.gms.internal.measurement.zzbs$zzg r3 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r3 = r3.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzac r4 = r59.zze()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzg r4 = r4.zzb(r3)     // Catch:{ all -> 0x0f8c }
            if (r4 != 0) goto L_0x0e12
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzf()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = "Bundling raw events w/o app info. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = r6.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.zzfb.zza(r6)     // Catch:{ all -> 0x0f8c }
            r4.zza(r5, r6)     // Catch:{ all -> 0x0f8c }
            goto L_0x0e6d
        L_0x0e12:
            int r5 = r8.zzb()     // Catch:{ all -> 0x0f8c }
            if (r5 <= 0) goto L_0x0e6d
            long r5 = r4.zzk()     // Catch:{ all -> 0x0f8c }
            r9 = 0
            int r7 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r7 == 0) goto L_0x0e26
            r8.zze(r5)     // Catch:{ all -> 0x0f8c }
            goto L_0x0e29
        L_0x0e26:
            r8.zzi()     // Catch:{ all -> 0x0f8c }
        L_0x0e29:
            long r9 = r4.zzj()     // Catch:{ all -> 0x0f8c }
            r11 = 0
            int r7 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r7 != 0) goto L_0x0e34
            goto L_0x0e35
        L_0x0e34:
            r5 = r9
        L_0x0e35:
            int r7 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r7 == 0) goto L_0x0e3d
            r8.zzd(r5)     // Catch:{ all -> 0x0f8c }
            goto L_0x0e40
        L_0x0e3d:
            r8.zzh()     // Catch:{ all -> 0x0f8c }
        L_0x0e40:
            r4.zzv()     // Catch:{ all -> 0x0f8c }
            long r5 = r4.zzs()     // Catch:{ all -> 0x0f8c }
            int r6 = (int) r5     // Catch:{ all -> 0x0f8c }
            r8.zzg(r6)     // Catch:{ all -> 0x0f8c }
            long r5 = r8.zzf()     // Catch:{ all -> 0x0f8c }
            r4.zza(r5)     // Catch:{ all -> 0x0f8c }
            long r5 = r8.zzg()     // Catch:{ all -> 0x0f8c }
            r4.zzb(r5)     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r4.zzad()     // Catch:{ all -> 0x0f8c }
            if (r5 == 0) goto L_0x0e63
            r8.zzj(r5)     // Catch:{ all -> 0x0f8c }
            goto L_0x0e66
        L_0x0e63:
            r8.zzk()     // Catch:{ all -> 0x0f8c }
        L_0x0e66:
            com.google.android.gms.measurement.internal.zzac r5 = r59.zze()     // Catch:{ all -> 0x0f8c }
            r5.zza(r4)     // Catch:{ all -> 0x0f8c }
        L_0x0e6d:
            int r4 = r8.zzb()     // Catch:{ all -> 0x0f8c }
            if (r4 <= 0) goto L_0x0ed3
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0f8c }
            r4.zzu()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfz r4 = r59.zzc()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r5 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = r5.zzx()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbq$zzb r4 = r4.zza(r5)     // Catch:{ all -> 0x0f8c }
            if (r4 == 0) goto L_0x0e97
            boolean r5 = r4.zza()     // Catch:{ all -> 0x0f8c }
            if (r5 != 0) goto L_0x0e8f
            goto L_0x0e97
        L_0x0e8f:
            long r4 = r4.zzb()     // Catch:{ all -> 0x0f8c }
            r8.zzi(r4)     // Catch:{ all -> 0x0f8c }
            goto L_0x0ec2
        L_0x0e97:
            com.google.android.gms.internal.measurement.zzbs$zzg r4 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r4 = r4.zzam()     // Catch:{ all -> 0x0f8c }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0f8c }
            if (r4 == 0) goto L_0x0ea9
            r4 = -1
            r8.zzi(r4)     // Catch:{ all -> 0x0f8c }
            goto L_0x0ec2
        L_0x0ea9:
            com.google.android.gms.measurement.internal.zzgf r4 = r1.zzj     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzi()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = "Did not find measurement config or missing version info. appId"
            com.google.android.gms.internal.measurement.zzbs$zzg r6 = r2.zza     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = r6.zzx()     // Catch:{ all -> 0x0f8c }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.zzfb.zza(r6)     // Catch:{ all -> 0x0f8c }
            r4.zza(r5, r6)     // Catch:{ all -> 0x0f8c }
        L_0x0ec2:
            com.google.android.gms.measurement.internal.zzac r4 = r59.zze()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzgm r5 = r8.zzv()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzfe r5 = (com.google.android.gms.internal.measurement.zzfe) r5     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.internal.measurement.zzbs$zzg r5 = (com.google.android.gms.internal.measurement.zzbs.zzg) r5     // Catch:{ all -> 0x0f8c }
            r9 = r21
            r4.zza(r5, r9)     // Catch:{ all -> 0x0f8c }
        L_0x0ed3:
            com.google.android.gms.measurement.internal.zzac r4 = r59.zze()     // Catch:{ all -> 0x0f8c }
            java.util.List<java.lang.Long> r2 = r2.zzb     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r2)     // Catch:{ all -> 0x0f8c }
            r4.zzd()     // Catch:{ all -> 0x0f8c }
            r4.zzak()     // Catch:{ all -> 0x0f8c }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = "rowid in ("
            r5.<init>(r6)     // Catch:{ all -> 0x0f8c }
            r6 = 0
        L_0x0eea:
            int r7 = r2.size()     // Catch:{ all -> 0x0f8c }
            if (r6 >= r7) goto L_0x0f07
            if (r6 == 0) goto L_0x0ef7
            java.lang.String r7 = ","
            r5.append(r7)     // Catch:{ all -> 0x0f8c }
        L_0x0ef7:
            java.lang.Object r7 = r2.get(r6)     // Catch:{ all -> 0x0f8c }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x0f8c }
            long r7 = r7.longValue()     // Catch:{ all -> 0x0f8c }
            r5.append(r7)     // Catch:{ all -> 0x0f8c }
            int r6 = r6 + 1
            goto L_0x0eea
        L_0x0f07:
            java.lang.String r6 = ")"
            r5.append(r6)     // Catch:{ all -> 0x0f8c }
            android.database.sqlite.SQLiteDatabase r6 = r4.c_()     // Catch:{ all -> 0x0f8c }
            java.lang.String r7 = "raw_events"
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0f8c }
            r8 = 0
            int r5 = r6.delete(r7, r5, r8)     // Catch:{ all -> 0x0f8c }
            int r6 = r2.size()     // Catch:{ all -> 0x0f8c }
            if (r5 == r6) goto L_0x0f3a
            com.google.android.gms.measurement.internal.zzfb r4 = r4.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzf()     // Catch:{ all -> 0x0f8c }
            java.lang.String r6 = "Deleted fewer rows from raw events table than expected"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0f8c }
            int r2 = r2.size()     // Catch:{ all -> 0x0f8c }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0f8c }
            r4.zza(r6, r5, r2)     // Catch:{ all -> 0x0f8c }
        L_0x0f3a:
            com.google.android.gms.measurement.internal.zzac r2 = r59.zze()     // Catch:{ all -> 0x0f8c }
            android.database.sqlite.SQLiteDatabase r4 = r2.c_()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = "delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)"
            r6 = 2
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0f51 }
            r7 = 0
            r6[r7] = r3     // Catch:{ SQLiteException -> 0x0f51 }
            r7 = 1
            r6[r7] = r3     // Catch:{ SQLiteException -> 0x0f51 }
            r4.execSQL(r5, r6)     // Catch:{ SQLiteException -> 0x0f51 }
            goto L_0x0f64
        L_0x0f51:
            r0 = move-exception
            r4 = r0
            com.google.android.gms.measurement.internal.zzfb r2 = r2.zzr()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzfd r2 = r2.zzf()     // Catch:{ all -> 0x0f8c }
            java.lang.String r5 = "Failed to remove unused event metadata. appId"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.zzfb.zza(r3)     // Catch:{ all -> 0x0f8c }
            r2.zza(r5, r3, r4)     // Catch:{ all -> 0x0f8c }
        L_0x0f64:
            com.google.android.gms.measurement.internal.zzac r2 = r59.zze()     // Catch:{ all -> 0x0f8c }
            r2.b_()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzac r2 = r59.zze()
            r2.zzh()
            r2 = 1
            return r2
        L_0x0f74:
            com.google.android.gms.measurement.internal.zzac r2 = r59.zze()     // Catch:{ all -> 0x0f8c }
            r2.b_()     // Catch:{ all -> 0x0f8c }
            com.google.android.gms.measurement.internal.zzac r2 = r59.zze()
            r2.zzh()
            r2 = 0
            return r2
        L_0x0f84:
            r0 = move-exception
            r2 = r0
        L_0x0f86:
            if (r8 == 0) goto L_0x0f8b
            r8.close()     // Catch:{ all -> 0x0f8c }
        L_0x0f8b:
            throw r2     // Catch:{ all -> 0x0f8c }
        L_0x0f8c:
            r0 = move-exception
        L_0x0f8d:
            r2 = r0
            com.google.android.gms.measurement.internal.zzac r3 = r59.zze()
            r3.zzh()
            goto L_0x0f97
        L_0x0f96:
            throw r2
        L_0x0f97:
            goto L_0x0f96
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzkj.zza(java.lang.String, long):boolean");
    }

    private static void zza(zzbs.zzg.zza zza2) {
        zza2.zzb(Long.MAX_VALUE).zzc(Long.MIN_VALUE);
        for (int i = 0; i < zza2.zzb(); i++) {
            zzbs.zzc zzb2 = zza2.zzb(i);
            if (zzb2.zze() < zza2.zzf()) {
                zza2.zzb(zzb2.zze());
            }
            if (zzb2.zze() > zza2.zzg()) {
                zza2.zzc(zzb2.zze());
            }
        }
    }

    private final void zza(zzbs.zzg.zza zza2, long j, boolean z) {
        zzks zzks;
        String str = z ? "_se" : "_lte";
        zzks zzc2 = zze().zzc(zza2.zzj(), str);
        if (zzc2 == null || zzc2.zze == null) {
            zzks = new zzks(zza2.zzj(), "auto", str, this.zzj.zzm().currentTimeMillis(), Long.valueOf(j));
        } else {
            zzks = new zzks(zza2.zzj(), "auto", str, this.zzj.zzm().currentTimeMillis(), Long.valueOf(((Long) zzc2.zze).longValue() + j));
        }
        zzbs.zzk zzk2 = (zzbs.zzk) ((zzfe) zzbs.zzk.zzj().zza(str).zza(this.zzj.zzm().currentTimeMillis()).zzb(((Long) zzks.zze).longValue()).zzv());
        boolean z2 = false;
        int zza3 = zzkr.zza(zza2, str);
        if (zza3 >= 0) {
            zza2.zza(zza3, zzk2);
            z2 = true;
        }
        if (!z2) {
            zza2.zza(zzk2);
        }
        if (j > 0) {
            zze().zza(zzks);
            String str2 = z ? "session-scoped" : "lifetime";
            if (!zzkw.zzb() || !this.zzj.zzb().zze(zza2.zzj(), zzap.zzcx)) {
                this.zzj.zzr().zzw().zza("Updated engagement user property. scope, value", str2, zzks.zze);
            } else {
                this.zzj.zzr().zzx().zza("Updated engagement user property. scope, value", str2, zzks.zze);
            }
        }
    }

    private final boolean zza(zzbs.zzc.zza zza2, zzbs.zzc.zza zza3) {
        String str;
        Preconditions.checkArgument("_e".equals(zza2.zzd()));
        zzh();
        zzbs.zze zza4 = zzkr.zza((zzbs.zzc) ((zzfe) zza2.zzv()), "_sc");
        String str2 = null;
        if (zza4 == null) {
            str = null;
        } else {
            str = zza4.zzd();
        }
        zzh();
        zzbs.zze zza5 = zzkr.zza((zzbs.zzc) ((zzfe) zza3.zzv()), "_pc");
        if (zza5 != null) {
            str2 = zza5.zzd();
        }
        if (str2 == null || !str2.equals(str)) {
            return false;
        }
        zzb(zza2, zza3);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzkr.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, java.lang.Object):void
     arg types: [com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.zzkr.zza(boolean, boolean, boolean):java.lang.String
      com.google.android.gms.measurement.internal.zzkr.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.measurement.zzbk$zzc):void
      com.google.android.gms.measurement.internal.zzkr.zza(java.lang.StringBuilder, int, java.util.List<com.google.android.gms.internal.measurement.zzbs$zze>):void
      com.google.android.gms.measurement.internal.zzkr.zza(com.google.android.gms.internal.measurement.zzbs$zzc$zza, java.lang.String, java.lang.Object):void */
    private final void zzb(zzbs.zzc.zza zza2, zzbs.zzc.zza zza3) {
        Preconditions.checkArgument("_e".equals(zza2.zzd()));
        zzh();
        zzbs.zze zza4 = zzkr.zza((zzbs.zzc) ((zzfe) zza2.zzv()), "_et");
        if (zza4.zze() && zza4.zzf() > 0) {
            long zzf2 = zza4.zzf();
            zzh();
            zzbs.zze zza5 = zzkr.zza((zzbs.zzc) ((zzfe) zza3.zzv()), "_et");
            if (zza5 != null && zza5.zzf() > 0) {
                zzf2 += zza5.zzf();
            }
            zzh();
            zzkr.zza(zza3, "_et", Long.valueOf(zzf2));
            zzh();
            zzkr.zza(zza2, "_fr", (Object) 1L);
        }
    }

    private static void zza(zzbs.zzc.zza zza2, String str) {
        List<zzbs.zze> zza3 = zza2.zza();
        for (int i = 0; i < zza3.size(); i++) {
            if (str.equals(zza3.get(i).zzb())) {
                zza2.zzb(i);
                return;
            }
        }
    }

    private static void zza(zzbs.zzc.zza zza2, int i, String str) {
        List<zzbs.zze> zza3 = zza2.zza();
        int i2 = 0;
        while (i2 < zza3.size()) {
            if (!"_err".equals(zza3.get(i2).zzb())) {
                i2++;
            } else {
                return;
            }
        }
        zza2.zza((zzbs.zze) ((zzfe) zzbs.zze.zzk().zza("_err").zza(Long.valueOf((long) i).longValue()).zzv())).zza((zzbs.zze) ((zzfe) zzbs.zze.zzk().zza("_ev").zzb(str).zzv()));
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public final void zza(int i, Throwable th, byte[] bArr, String str) {
        zzac zze2;
        zzw();
        zzk();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.zzs = false;
                zzaa();
                throw th2;
            }
        }
        List<Long> list = this.zzw;
        this.zzw = null;
        boolean z = true;
        if ((i == 200 || i == 204) && th == null) {
            try {
                this.zzj.zzc().zzc.zza(this.zzj.zzm().currentTimeMillis());
                this.zzj.zzc().zzd.zza(0);
                zzz();
                this.zzj.zzr().zzx().zza("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                zze().zzf();
                try {
                    for (Long next : list) {
                        try {
                            zze2 = zze();
                            long longValue = next.longValue();
                            zze2.zzd();
                            zze2.zzak();
                            if (zze2.c_().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e) {
                            zze2.zzr().zzf().zza("Failed to delete a bundle in a queue table", e);
                            throw e;
                        } catch (SQLiteException e2) {
                            if (this.zzx == null || !this.zzx.contains(next)) {
                                throw e2;
                            }
                        }
                    }
                    zze().b_();
                    zze().zzh();
                    this.zzx = null;
                    if (!zzd().zzf() || !zzy()) {
                        this.zzy = -1;
                        zzz();
                    } else {
                        zzl();
                    }
                    this.zzn = 0;
                } catch (Throwable th3) {
                    zze().zzh();
                    throw th3;
                }
            } catch (SQLiteException e3) {
                this.zzj.zzr().zzf().zza("Database error while trying to delete uploaded bundles", e3);
                this.zzn = this.zzj.zzm().elapsedRealtime();
                this.zzj.zzr().zzx().zza("Disable upload, time", Long.valueOf(this.zzn));
            }
        } else {
            this.zzj.zzr().zzx().zza("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            this.zzj.zzc().zzd.zza(this.zzj.zzm().currentTimeMillis());
            if (i != 503) {
                if (i != 429) {
                    z = false;
                }
            }
            if (z) {
                this.zzj.zzc().zze.zza(this.zzj.zzm().currentTimeMillis());
            }
            zze().zza(list);
            zzz();
        }
        this.zzs = false;
        zzaa();
    }

    private final boolean zzy() {
        zzw();
        zzk();
        return zze().zzy() || !TextUtils.isEmpty(zze().d_());
    }

    private final void zza(zzg zzg2) {
        ArrayMap arrayMap;
        zzw();
        if (!zzln.zzb() || !this.zzj.zzb().zze(zzg2.zzc(), zzap.zzcf)) {
            if (TextUtils.isEmpty(zzg2.zze()) && TextUtils.isEmpty(zzg2.zzf())) {
                zza(zzg2.zzc(), 204, null, null, null);
                return;
            }
        } else if (TextUtils.isEmpty(zzg2.zze()) && TextUtils.isEmpty(zzg2.zzg()) && TextUtils.isEmpty(zzg2.zzf())) {
            zza(zzg2.zzc(), 204, null, null, null);
            return;
        }
        String zza2 = this.zzj.zzb().zza(zzg2);
        try {
            URL url = new URL(zza2);
            this.zzj.zzr().zzx().zza("Fetching remote configuration", zzg2.zzc());
            zzbq.zzb zza3 = zzc().zza(zzg2.zzc());
            String zzb2 = zzc().zzb(zzg2.zzc());
            if (zza3 == null || TextUtils.isEmpty(zzb2)) {
                arrayMap = null;
            } else {
                ArrayMap arrayMap2 = new ArrayMap();
                arrayMap2.put("If-Modified-Since", zzb2);
                arrayMap = arrayMap2;
            }
            this.zzr = true;
            zzff zzd2 = zzd();
            String zzc2 = zzg2.zzc();
            zzko zzko = new zzko(this);
            zzd2.zzd();
            zzd2.zzak();
            Preconditions.checkNotNull(url);
            Preconditions.checkNotNull(zzko);
            zzd2.zzq().zzb(new zzfj(zzd2, zzc2, url, null, arrayMap, zzko));
        } catch (MalformedURLException unused) {
            this.zzj.zzr().zzf().zza("Failed to parse config URL. Not fetching. appId", zzfb.zza(zzg2.zzc()), zza2);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x013a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x014a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(java.lang.String r7, int r8, java.lang.Throwable r9, byte[] r10, java.util.Map<java.lang.String, java.util.List<java.lang.String>> r11) {
        /*
            r6 = this;
            r6.zzw()
            r6.zzk()
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r7)
            r0 = 0
            if (r10 != 0) goto L_0x000e
            byte[] r10 = new byte[r0]     // Catch:{ all -> 0x0196 }
        L_0x000e:
            com.google.android.gms.measurement.internal.zzgf r1 = r6.zzj     // Catch:{ all -> 0x0196 }
            com.google.android.gms.measurement.internal.zzfb r1 = r1.zzr()     // Catch:{ all -> 0x0196 }
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzx()     // Catch:{ all -> 0x0196 }
            java.lang.String r2 = "onConfigFetched. Response size"
            int r3 = r10.length     // Catch:{ all -> 0x0196 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0196 }
            r1.zza(r2, r3)     // Catch:{ all -> 0x0196 }
            com.google.android.gms.measurement.internal.zzac r1 = r6.zze()     // Catch:{ all -> 0x0196 }
            r1.zzf()     // Catch:{ all -> 0x0196 }
            com.google.android.gms.measurement.internal.zzac r1 = r6.zze()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzg r1 = r1.zzb(r7)     // Catch:{ all -> 0x018d }
            r2 = 200(0xc8, float:2.8E-43)
            r3 = 304(0x130, float:4.26E-43)
            r4 = 1
            if (r8 == r2) goto L_0x003e
            r2 = 204(0xcc, float:2.86E-43)
            if (r8 == r2) goto L_0x003e
            if (r8 != r3) goto L_0x0042
        L_0x003e:
            if (r9 != 0) goto L_0x0042
            r2 = 1
            goto L_0x0043
        L_0x0042:
            r2 = 0
        L_0x0043:
            if (r1 != 0) goto L_0x005a
            com.google.android.gms.measurement.internal.zzgf r8 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfb r8 = r8.zzr()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfd r8 = r8.zzi()     // Catch:{ all -> 0x018d }
            java.lang.String r9 = "App does not exist in onConfigFetched. appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.zzfb.zza(r7)     // Catch:{ all -> 0x018d }
            r8.zza(r9, r7)     // Catch:{ all -> 0x018d }
            goto L_0x0179
        L_0x005a:
            r5 = 404(0x194, float:5.66E-43)
            if (r2 != 0) goto L_0x00ca
            if (r8 != r5) goto L_0x0061
            goto L_0x00ca
        L_0x0061:
            com.google.android.gms.measurement.internal.zzgf r10 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.common.util.Clock r10 = r10.zzm()     // Catch:{ all -> 0x018d }
            long r10 = r10.currentTimeMillis()     // Catch:{ all -> 0x018d }
            r1.zzi(r10)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzac r10 = r6.zze()     // Catch:{ all -> 0x018d }
            r10.zza(r1)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzgf r10 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfb r10 = r10.zzr()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfd r10 = r10.zzx()     // Catch:{ all -> 0x018d }
            java.lang.String r11 = "Fetching config failed. code, error"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x018d }
            r10.zza(r11, r1, r9)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfz r9 = r6.zzc()     // Catch:{ all -> 0x018d }
            r9.zzc(r7)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzgf r7 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfo r7 = r7.zzc()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfp r7 = r7.zzd     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzgf r9 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.common.util.Clock r9 = r9.zzm()     // Catch:{ all -> 0x018d }
            long r9 = r9.currentTimeMillis()     // Catch:{ all -> 0x018d }
            r7.zza(r9)     // Catch:{ all -> 0x018d }
            r7 = 503(0x1f7, float:7.05E-43)
            if (r8 == r7) goto L_0x00ae
            r7 = 429(0x1ad, float:6.01E-43)
            if (r8 != r7) goto L_0x00ad
            goto L_0x00ae
        L_0x00ad:
            r4 = 0
        L_0x00ae:
            if (r4 == 0) goto L_0x00c5
            com.google.android.gms.measurement.internal.zzgf r7 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfo r7 = r7.zzc()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfp r7 = r7.zze     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzgf r8 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.common.util.Clock r8 = r8.zzm()     // Catch:{ all -> 0x018d }
            long r8 = r8.currentTimeMillis()     // Catch:{ all -> 0x018d }
            r7.zza(r8)     // Catch:{ all -> 0x018d }
        L_0x00c5:
            r6.zzz()     // Catch:{ all -> 0x018d }
            goto L_0x0179
        L_0x00ca:
            r9 = 0
            if (r11 == 0) goto L_0x00d6
            java.lang.String r2 = "Last-Modified"
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x018d }
            java.util.List r11 = (java.util.List) r11     // Catch:{ all -> 0x018d }
            goto L_0x00d7
        L_0x00d6:
            r11 = r9
        L_0x00d7:
            if (r11 == 0) goto L_0x00e6
            int r2 = r11.size()     // Catch:{ all -> 0x018d }
            if (r2 <= 0) goto L_0x00e6
            java.lang.Object r11 = r11.get(r0)     // Catch:{ all -> 0x018d }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x018d }
            goto L_0x00e7
        L_0x00e6:
            r11 = r9
        L_0x00e7:
            if (r8 == r5) goto L_0x0103
            if (r8 != r3) goto L_0x00ec
            goto L_0x0103
        L_0x00ec:
            com.google.android.gms.measurement.internal.zzfz r9 = r6.zzc()     // Catch:{ all -> 0x018d }
            boolean r9 = r9.zza(r7, r10, r11)     // Catch:{ all -> 0x018d }
            if (r9 != 0) goto L_0x0124
            com.google.android.gms.measurement.internal.zzac r7 = r6.zze()     // Catch:{ all -> 0x0196 }
            r7.zzh()     // Catch:{ all -> 0x0196 }
            r6.zzr = r0
            r6.zzaa()
            return
        L_0x0103:
            com.google.android.gms.measurement.internal.zzfz r11 = r6.zzc()     // Catch:{ all -> 0x018d }
            com.google.android.gms.internal.measurement.zzbq$zzb r11 = r11.zza(r7)     // Catch:{ all -> 0x018d }
            if (r11 != 0) goto L_0x0124
            com.google.android.gms.measurement.internal.zzfz r11 = r6.zzc()     // Catch:{ all -> 0x018d }
            boolean r9 = r11.zza(r7, r9, r9)     // Catch:{ all -> 0x018d }
            if (r9 != 0) goto L_0x0124
            com.google.android.gms.measurement.internal.zzac r7 = r6.zze()     // Catch:{ all -> 0x0196 }
            r7.zzh()     // Catch:{ all -> 0x0196 }
            r6.zzr = r0
            r6.zzaa()
            return
        L_0x0124:
            com.google.android.gms.measurement.internal.zzgf r9 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.common.util.Clock r9 = r9.zzm()     // Catch:{ all -> 0x018d }
            long r2 = r9.currentTimeMillis()     // Catch:{ all -> 0x018d }
            r1.zzh(r2)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzac r9 = r6.zze()     // Catch:{ all -> 0x018d }
            r9.zza(r1)     // Catch:{ all -> 0x018d }
            if (r8 != r5) goto L_0x014a
            com.google.android.gms.measurement.internal.zzgf r8 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfb r8 = r8.zzr()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfd r8 = r8.zzk()     // Catch:{ all -> 0x018d }
            java.lang.String r9 = "Config not found. Using empty config. appId"
            r8.zza(r9, r7)     // Catch:{ all -> 0x018d }
            goto L_0x0162
        L_0x014a:
            com.google.android.gms.measurement.internal.zzgf r7 = r6.zzj     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfb r7 = r7.zzr()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzfd r7 = r7.zzx()     // Catch:{ all -> 0x018d }
            java.lang.String r9 = "Successfully fetched config. Got network response. code, size"
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x018d }
            int r10 = r10.length     // Catch:{ all -> 0x018d }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x018d }
            r7.zza(r9, r8, r10)     // Catch:{ all -> 0x018d }
        L_0x0162:
            com.google.android.gms.measurement.internal.zzff r7 = r6.zzd()     // Catch:{ all -> 0x018d }
            boolean r7 = r7.zzf()     // Catch:{ all -> 0x018d }
            if (r7 == 0) goto L_0x0176
            boolean r7 = r6.zzy()     // Catch:{ all -> 0x018d }
            if (r7 == 0) goto L_0x0176
            r6.zzl()     // Catch:{ all -> 0x018d }
            goto L_0x0179
        L_0x0176:
            r6.zzz()     // Catch:{ all -> 0x018d }
        L_0x0179:
            com.google.android.gms.measurement.internal.zzac r7 = r6.zze()     // Catch:{ all -> 0x018d }
            r7.b_()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.zzac r7 = r6.zze()     // Catch:{ all -> 0x0196 }
            r7.zzh()     // Catch:{ all -> 0x0196 }
            r6.zzr = r0
            r6.zzaa()
            return
        L_0x018d:
            r7 = move-exception
            com.google.android.gms.measurement.internal.zzac r8 = r6.zze()     // Catch:{ all -> 0x0196 }
            r8.zzh()     // Catch:{ all -> 0x0196 }
            throw r7     // Catch:{ all -> 0x0196 }
        L_0x0196:
            r7 = move-exception
            r6.zzr = r0
            r6.zzaa()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzkj.zza(java.lang.String, int, java.lang.Throwable, byte[], java.util.Map):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01c0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzz() {
        /*
            r21 = this;
            r0 = r21
            r21.zzw()
            r21.zzk()
            boolean r1 = r21.zzac()
            if (r1 != 0) goto L_0x001d
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzx r1 = r1.zzb()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.zzap.zzbb
            boolean r1 = r1.zza(r2)
            if (r1 != 0) goto L_0x001d
            return
        L_0x001d:
            long r1 = r0.zzn
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x0062
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.common.util.Clock r1 = r1.zzm()
            long r1 = r1.elapsedRealtime()
            r5 = 3600000(0x36ee80, double:1.7786363E-317)
            long r7 = r0.zzn
            long r1 = r1 - r7
            long r1 = java.lang.Math.abs(r1)
            long r5 = r5 - r1
            int r1 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0060
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzfb r1 = r1.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzx()
            java.lang.Long r2 = java.lang.Long.valueOf(r5)
            java.lang.String r3 = "Upload has been suspended. Will update scheduling later in approximately ms"
            r1.zza(r3, r2)
            com.google.android.gms.measurement.internal.zzfm r1 = r21.zzt()
            r1.zzb()
            com.google.android.gms.measurement.internal.zzkf r1 = r21.zzv()
            r1.zzf()
            return
        L_0x0060:
            r0.zzn = r3
        L_0x0062:
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            boolean r1 = r1.zzah()
            if (r1 == 0) goto L_0x026b
            boolean r1 = r21.zzy()
            if (r1 != 0) goto L_0x0072
            goto L_0x026b
        L_0x0072:
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.common.util.Clock r1 = r1.zzm()
            long r1 = r1.currentTimeMillis()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Long> r5 = com.google.android.gms.measurement.internal.zzap.zzz
            r6 = 0
            java.lang.Object r5 = r5.zza(r6)
            java.lang.Long r5 = (java.lang.Long) r5
            long r7 = r5.longValue()
            long r7 = java.lang.Math.max(r3, r7)
            com.google.android.gms.measurement.internal.zzac r5 = r21.zze()
            boolean r5 = r5.zzz()
            if (r5 != 0) goto L_0x00a4
            com.google.android.gms.measurement.internal.zzac r5 = r21.zze()
            boolean r5 = r5.zzk()
            if (r5 == 0) goto L_0x00a2
            goto L_0x00a4
        L_0x00a2:
            r5 = 0
            goto L_0x00a5
        L_0x00a4:
            r5 = 1
        L_0x00a5:
            if (r5 == 0) goto L_0x00e1
            com.google.android.gms.measurement.internal.zzgf r10 = r0.zzj
            com.google.android.gms.measurement.internal.zzx r10 = r10.zzb()
            java.lang.String r10 = r10.zzv()
            boolean r11 = android.text.TextUtils.isEmpty(r10)
            if (r11 != 0) goto L_0x00d0
            java.lang.String r11 = ".none."
            boolean r10 = r11.equals(r10)
            if (r10 != 0) goto L_0x00d0
            com.google.android.gms.measurement.internal.zzeu<java.lang.Long> r10 = com.google.android.gms.measurement.internal.zzap.zzu
            java.lang.Object r10 = r10.zza(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
            goto L_0x00f1
        L_0x00d0:
            com.google.android.gms.measurement.internal.zzeu<java.lang.Long> r10 = com.google.android.gms.measurement.internal.zzap.zzt
            java.lang.Object r10 = r10.zza(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
            goto L_0x00f1
        L_0x00e1:
            com.google.android.gms.measurement.internal.zzeu<java.lang.Long> r10 = com.google.android.gms.measurement.internal.zzap.zzs
            java.lang.Object r10 = r10.zza(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
        L_0x00f1:
            com.google.android.gms.measurement.internal.zzgf r12 = r0.zzj
            com.google.android.gms.measurement.internal.zzfo r12 = r12.zzc()
            com.google.android.gms.measurement.internal.zzfp r12 = r12.zzc
            long r12 = r12.zza()
            com.google.android.gms.measurement.internal.zzgf r14 = r0.zzj
            com.google.android.gms.measurement.internal.zzfo r14 = r14.zzc()
            com.google.android.gms.measurement.internal.zzfp r14 = r14.zzd
            long r14 = r14.zza()
            com.google.android.gms.measurement.internal.zzac r16 = r21.zze()
            r17 = r10
            long r9 = r16.zzw()
            com.google.android.gms.measurement.internal.zzac r11 = r21.zze()
            r19 = r7
            long r6 = r11.zzx()
            long r6 = java.lang.Math.max(r9, r6)
            int r8 = (r6 > r3 ? 1 : (r6 == r3 ? 0 : -1))
            if (r8 != 0) goto L_0x0128
        L_0x0125:
            r8 = r3
            goto L_0x019e
        L_0x0128:
            long r6 = r6 - r1
            long r6 = java.lang.Math.abs(r6)
            long r6 = r1 - r6
            long r12 = r12 - r1
            long r8 = java.lang.Math.abs(r12)
            long r8 = r1 - r8
            long r14 = r14 - r1
            long r10 = java.lang.Math.abs(r14)
            long r1 = r1 - r10
            long r8 = java.lang.Math.max(r8, r1)
            long r10 = r6 + r19
            if (r5 == 0) goto L_0x014e
            int r5 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x014e
            long r10 = java.lang.Math.min(r6, r8)
            long r10 = r10 + r17
        L_0x014e:
            com.google.android.gms.measurement.internal.zzkr r5 = r21.zzh()
            r12 = r17
            boolean r5 = r5.zza(r8, r12)
            if (r5 != 0) goto L_0x015c
            long r8 = r8 + r12
            goto L_0x015d
        L_0x015c:
            r8 = r10
        L_0x015d:
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x019e
            int r5 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x019e
            r5 = 0
        L_0x0166:
            r6 = 20
            com.google.android.gms.measurement.internal.zzeu<java.lang.Integer> r7 = com.google.android.gms.measurement.internal.zzap.zzab
            r10 = 0
            java.lang.Object r7 = r7.zza(r10)
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            r11 = 0
            int r7 = java.lang.Math.max(r11, r7)
            int r6 = java.lang.Math.min(r6, r7)
            if (r5 >= r6) goto L_0x0125
            r6 = 1
            long r6 = r6 << r5
            com.google.android.gms.measurement.internal.zzeu<java.lang.Long> r12 = com.google.android.gms.measurement.internal.zzap.zzaa
            java.lang.Object r12 = r12.zza(r10)
            java.lang.Long r12 = (java.lang.Long) r12
            long r12 = r12.longValue()
            long r12 = java.lang.Math.max(r3, r12)
            long r12 = r12 * r6
            long r8 = r8 + r12
            int r6 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r6 <= 0) goto L_0x019b
            goto L_0x019e
        L_0x019b:
            int r5 = r5 + 1
            goto L_0x0166
        L_0x019e:
            int r1 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x01c0
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzfb r1 = r1.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzx()
            java.lang.String r2 = "Next upload time is 0"
            r1.zza(r2)
            com.google.android.gms.measurement.internal.zzfm r1 = r21.zzt()
            r1.zzb()
            com.google.android.gms.measurement.internal.zzkf r1 = r21.zzv()
            r1.zzf()
            return
        L_0x01c0:
            com.google.android.gms.measurement.internal.zzff r1 = r21.zzd()
            boolean r1 = r1.zzf()
            if (r1 != 0) goto L_0x01e8
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzfb r1 = r1.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzx()
            java.lang.String r2 = "No network"
            r1.zza(r2)
            com.google.android.gms.measurement.internal.zzfm r1 = r21.zzt()
            r1.zza()
            com.google.android.gms.measurement.internal.zzkf r1 = r21.zzv()
            r1.zzf()
            return
        L_0x01e8:
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzfo r1 = r1.zzc()
            com.google.android.gms.measurement.internal.zzfp r1 = r1.zze
            long r1 = r1.zza()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Long> r5 = com.google.android.gms.measurement.internal.zzap.zzq
            r6 = 0
            java.lang.Object r5 = r5.zza(r6)
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            long r5 = java.lang.Math.max(r3, r5)
            com.google.android.gms.measurement.internal.zzkr r7 = r21.zzh()
            boolean r7 = r7.zza(r1, r5)
            if (r7 != 0) goto L_0x0214
            long r1 = r1 + r5
            long r8 = java.lang.Math.max(r8, r1)
        L_0x0214:
            com.google.android.gms.measurement.internal.zzfm r1 = r21.zzt()
            r1.zzb()
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.common.util.Clock r1 = r1.zzm()
            long r1 = r1.currentTimeMillis()
            long r8 = r8 - r1
            int r1 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r1 > 0) goto L_0x0250
            com.google.android.gms.measurement.internal.zzeu<java.lang.Long> r1 = com.google.android.gms.measurement.internal.zzap.zzv
            r2 = 0
            java.lang.Object r1 = r1.zza(r2)
            java.lang.Long r1 = (java.lang.Long) r1
            long r1 = r1.longValue()
            long r8 = java.lang.Math.max(r3, r1)
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzfo r1 = r1.zzc()
            com.google.android.gms.measurement.internal.zzfp r1 = r1.zzc
            com.google.android.gms.measurement.internal.zzgf r2 = r0.zzj
            com.google.android.gms.common.util.Clock r2 = r2.zzm()
            long r2 = r2.currentTimeMillis()
            r1.zza(r2)
        L_0x0250:
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzfb r1 = r1.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzx()
            java.lang.Long r2 = java.lang.Long.valueOf(r8)
            java.lang.String r3 = "Upload scheduled in approximately ms"
            r1.zza(r3, r2)
            com.google.android.gms.measurement.internal.zzkf r1 = r21.zzv()
            r1.zza(r8)
            return
        L_0x026b:
            com.google.android.gms.measurement.internal.zzgf r1 = r0.zzj
            com.google.android.gms.measurement.internal.zzfb r1 = r1.zzr()
            com.google.android.gms.measurement.internal.zzfd r1 = r1.zzx()
            java.lang.String r2 = "Nothing to upload or uploading impossible"
            r1.zza(r2)
            com.google.android.gms.measurement.internal.zzfm r1 = r21.zzt()
            r1.zzb()
            com.google.android.gms.measurement.internal.zzkf r1 = r21.zzv()
            r1.zzf()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzkj.zzz():void");
    }

    /* access modifiers changed from: package-private */
    public final void zza(Runnable runnable) {
        zzw();
        if (this.zzo == null) {
            this.zzo = new ArrayList();
        }
        this.zzo.add(runnable);
    }

    private final void zzaa() {
        zzw();
        if (this.zzr || this.zzs || this.zzt) {
            this.zzj.zzr().zzx().zza("Not stopping services. fetch, network, upload", Boolean.valueOf(this.zzr), Boolean.valueOf(this.zzs), Boolean.valueOf(this.zzt));
            return;
        }
        this.zzj.zzr().zzx().zza("Stopping uploading service(s)");
        List<Runnable> list = this.zzo;
        if (list != null) {
            for (Runnable run : list) {
                run.run();
            }
            this.zzo.clear();
        }
    }

    private final Boolean zzb(zzg zzg2) {
        try {
            if (zzg2.zzm() != -2147483648L) {
                if (zzg2.zzm() == ((long) Wrappers.packageManager(this.zzj.zzn()).getPackageInfo(zzg2.zzc(), 0).versionCode)) {
                    return true;
                }
            } else {
                String str = Wrappers.packageManager(this.zzj.zzn()).getPackageInfo(zzg2.zzc(), 0).versionName;
                if (zzg2.zzl() != null && zzg2.zzl().equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    private final boolean zzab() {
        FileLock fileLock;
        zzw();
        if (!this.zzj.zzb().zza(zzap.zzcd) || (fileLock = this.zzu) == null || !fileLock.isValid()) {
            try {
                this.zzv = new RandomAccessFile(new File(this.zzj.zzn().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
                this.zzu = this.zzv.tryLock();
                if (this.zzu != null) {
                    this.zzj.zzr().zzx().zza("Storage concurrent access okay");
                    return true;
                }
                this.zzj.zzr().zzf().zza("Storage concurrent data access panic");
                return false;
            } catch (FileNotFoundException e) {
                this.zzj.zzr().zzf().zza("Failed to acquire storage lock", e);
                return false;
            } catch (IOException e2) {
                this.zzj.zzr().zzf().zza("Failed to access storage lock file", e2);
                return false;
            } catch (OverlappingFileLockException e3) {
                this.zzj.zzr().zzi().zza("Storage lock already acquired", e3);
                return false;
            }
        } else {
            this.zzj.zzr().zzx().zza("Storage concurrent access okay");
            return true;
        }
    }

    private final int zza(FileChannel fileChannel) {
        zzw();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.zzj.zzr().zzf().zza("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0L);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    this.zzj.zzr().zzi().zza("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e) {
            this.zzj.zzr().zzf().zza("Failed to read from channel", e);
            return 0;
        }
    }

    private final boolean zza(int i, FileChannel fileChannel) {
        zzw();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.zzj.zzr().zzf().zza("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i);
        allocate.flip();
        try {
            fileChannel.truncate(0L);
            if (this.zzj.zzb().zza(zzap.zzcs) && Build.VERSION.SDK_INT <= 19) {
                fileChannel.position(0L);
            }
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                this.zzj.zzr().zzf().zza("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e) {
            this.zzj.zzr().zzf().zza("Failed to write to channel", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzo() {
        zzw();
        zzk();
        if (!this.zzm) {
            this.zzm = true;
            zzw();
            zzk();
            if ((this.zzj.zzb().zza(zzap.zzbb) || zzac()) && zzab()) {
                int zza2 = zza(this.zzv);
                int zzaf = this.zzj.zzy().zzaf();
                zzw();
                if (zza2 > zzaf) {
                    this.zzj.zzr().zzf().zza("Panic: can't downgrade version. Previous, current version", Integer.valueOf(zza2), Integer.valueOf(zzaf));
                } else if (zza2 < zzaf) {
                    if (zza(zzaf, this.zzv)) {
                        this.zzj.zzr().zzx().zza("Storage version upgraded. Previous, current version", Integer.valueOf(zza2), Integer.valueOf(zzaf));
                    } else {
                        this.zzj.zzr().zzf().zza("Storage version upgrade failed. Previous, current version", Integer.valueOf(zza2), Integer.valueOf(zzaf));
                    }
                }
            }
        }
        if (!this.zzl && !this.zzj.zzb().zza(zzap.zzbb)) {
            this.zzj.zzr().zzv().zza("This instance being marked as an uploader");
            this.zzl = true;
            zzz();
        }
    }

    private final boolean zzac() {
        zzw();
        zzk();
        return this.zzl;
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzm zzm2) {
        if (this.zzw != null) {
            this.zzx = new ArrayList();
            this.zzx.addAll(this.zzw);
        }
        zzac zze2 = zze();
        String str = zzm2.zza;
        Preconditions.checkNotEmpty(str);
        zze2.zzd();
        zze2.zzak();
        try {
            SQLiteDatabase c_ = zze2.c_();
            String[] strArr = {str};
            int delete = c_.delete("apps", "app_id=?", strArr) + 0 + c_.delete(DataBaseEventsStorage.EventEntry.TABLE_NAME, "app_id=?", strArr) + c_.delete("user_attributes", "app_id=?", strArr) + c_.delete("conditional_properties", "app_id=?", strArr) + c_.delete("raw_events", "app_id=?", strArr) + c_.delete("raw_events_metadata", "app_id=?", strArr) + c_.delete("queue", "app_id=?", strArr) + c_.delete("audience_filter_values", "app_id=?", strArr) + c_.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                zze2.zzr().zzx().zza("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zze2.zzr().zzf().zza("Error resetting analytics data. appId, error", zzfb.zza(str), e);
        }
        if (!zzkp.zzb() || !this.zzj.zzb().zza(zzap.zzck)) {
            zzm zza2 = zza(this.zzj.zzn(), zzm2.zza, zzm2.zzb, zzm2.zzh, zzm2.zzo, zzm2.zzp, zzm2.zzm, zzm2.zzr, zzm2.zzv);
            if (zzm2.zzh) {
                zzb(zza2);
            }
        } else if (zzm2.zzh) {
            zzb(zzm2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, int, long, int, boolean, boolean, int, java.lang.String, ?[OBJECT, ARRAY], int, ?[OBJECT, ARRAY], java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void */
    private final zzm zza(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j, String str3, String str4) {
        String str5;
        String str6;
        int i;
        String str7 = str;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            this.zzj.zzr().zzf().zza("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str5 = packageManager.getInstallerPackageName(str7);
        } catch (IllegalArgumentException unused) {
            this.zzj.zzr().zzf().zza("Error retrieving installer package name. appId", zzfb.zza(str));
            str5 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        if (str5 == null) {
            str5 = "manual_install";
        } else if ("com.android.vending".equals(str5)) {
            str5 = "";
        }
        String str8 = str5;
        try {
            PackageInfo packageInfo = Wrappers.packageManager(context).getPackageInfo(str7, 0);
            if (packageInfo != null) {
                CharSequence applicationLabel = Wrappers.packageManager(context).getApplicationLabel(str7);
                if (!TextUtils.isEmpty(applicationLabel)) {
                    String charSequence = applicationLabel.toString();
                }
                str6 = packageInfo.versionName;
                i = packageInfo.versionCode;
            } else {
                str6 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                i = Integer.MIN_VALUE;
            }
            return new zzm(str, str2, str6, (long) i, str8, this.zzj.zzb().zze(), this.zzj.zzi().zza(context, str7), (String) null, z, false, "", 0L, j, 0, z2, z3, false, str3, (Boolean) null, 0L, (List<String>) null, (!zzln.zzb() || !this.zzj.zzb().zze(str7, zzap.zzcf)) ? null : str4);
        } catch (PackageManager.NameNotFoundException unused2) {
            this.zzj.zzr().zzf().zza("Error retrieving newly installed package info. appId, appName", zzfb.zza(str), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.zzkv.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzkv.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.zzkv.zza(java.lang.String, int, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public final void zza(zzkq zzkq, zzm zzm2) {
        zzaj zza2;
        zzw();
        zzk();
        if (zze(zzm2)) {
            if (!zzm2.zzh) {
                zzc(zzm2);
                return;
            }
            int zzc2 = this.zzj.zzi().zzc(zzkq.zza);
            if (zzc2 != 0) {
                this.zzj.zzi();
                this.zzj.zzi().zza(zzm2.zza, zzc2, "_ev", zzkv.zza(zzkq.zza, 24, true), zzkq.zza != null ? zzkq.zza.length() : 0);
                return;
            }
            int zzb2 = this.zzj.zzi().zzb(zzkq.zza, zzkq.zza());
            if (zzb2 != 0) {
                this.zzj.zzi();
                String zza3 = zzkv.zza(zzkq.zza, 24, true);
                Object zza4 = zzkq.zza();
                this.zzj.zzi().zza(zzm2.zza, zzb2, "_ev", zza3, (zza4 == null || (!(zza4 instanceof String) && !(zza4 instanceof CharSequence))) ? 0 : String.valueOf(zza4).length());
                return;
            }
            Object zzc3 = this.zzj.zzi().zzc(zzkq.zza, zzkq.zza());
            if (zzc3 != null) {
                if ("_sid".equals(zzkq.zza) && this.zzj.zzb().zze(zzm2.zza, zzap.zzap)) {
                    long j = zzkq.zzb;
                    String str = zzkq.zze;
                    long j2 = 0;
                    zzks zzc4 = zze().zzc(zzm2.zza, "_sno");
                    if (zzc4 == null || !(zzc4.zze instanceof Long)) {
                        if (zzc4 != null) {
                            this.zzj.zzr().zzi().zza("Retrieved last session number from database does not contain a valid (long) value", zzc4.zze);
                        }
                        if (this.zzj.zzb().zze(zzm2.zza, zzap.zzas) && (zza2 = zze().zza(zzm2.zza, "_s")) != null) {
                            j2 = zza2.zzc;
                            this.zzj.zzr().zzx().zza("Backfill the session number. Last used session number", Long.valueOf(j2));
                        }
                    } else {
                        j2 = ((Long) zzc4.zze).longValue();
                    }
                    zza(new zzkq("_sno", j, Long.valueOf(j2 + 1), str), zzm2);
                }
                zzks zzks = new zzks(zzm2.zza, zzkq.zze, zzkq.zza, zzkq.zzb, zzc3);
                if (!zzkw.zzb() || !this.zzj.zzb().zze(zzm2.zza, zzap.zzcx)) {
                    this.zzj.zzr().zzw().zza("Setting user property", this.zzj.zzj().zzc(zzks.zzc), zzc3);
                } else {
                    this.zzj.zzr().zzx().zza("Setting user property", this.zzj.zzj().zzc(zzks.zzc), zzc3);
                }
                zze().zzf();
                try {
                    zzc(zzm2);
                    boolean zza5 = zze().zza(zzks);
                    zze().b_();
                    if (!zza5) {
                        this.zzj.zzr().zzf().zza("Too many unique user properties are set. Ignoring user property", this.zzj.zzj().zzc(zzks.zzc), zzks.zze);
                        this.zzj.zzi().zza(zzm2.zza, 9, (String) null, (String) null, 0);
                    } else if (!zzkw.zzb() || !this.zzj.zzb().zze(zzm2.zza, zzap.zzcx)) {
                        this.zzj.zzr().zzw().zza("User property set", this.zzj.zzj().zzc(zzks.zzc), zzks.zze);
                    }
                } finally {
                    zze().zzh();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(zzkq zzkq, zzm zzm2) {
        zzw();
        zzk();
        if (zze(zzm2)) {
            if (!zzm2.zzh) {
                zzc(zzm2);
            } else if (!this.zzj.zzb().zze(zzm2.zza, zzap.zzaz)) {
                this.zzj.zzr().zzw().zza("Removing user property", this.zzj.zzj().zzc(zzkq.zza));
                zze().zzf();
                try {
                    zzc(zzm2);
                    zze().zzb(zzm2.zza, zzkq.zza);
                    zze().b_();
                    this.zzj.zzr().zzw().zza("User property removed", this.zzj.zzj().zzc(zzkq.zza));
                } finally {
                    zze().zzh();
                }
            } else if (!"_npa".equals(zzkq.zza) || zzm2.zzs == null) {
                this.zzj.zzr().zzw().zza("Removing user property", this.zzj.zzj().zzc(zzkq.zza));
                zze().zzf();
                try {
                    zzc(zzm2);
                    zze().zzb(zzm2.zza, zzkq.zza);
                    zze().b_();
                    this.zzj.zzr().zzw().zza("User property removed", this.zzj.zzj().zzc(zzkq.zza));
                } finally {
                    zze().zzh();
                }
            } else {
                this.zzj.zzr().zzw().zza("Falling back to manifest metadata value for ad personalization");
                zza(new zzkq("_npa", this.zzj.zzm().currentTimeMillis(), Long.valueOf(zzm2.zzs.booleanValue() ? 1 : 0), "auto"), zzm2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzkk zzkk) {
        this.zzp++;
    }

    /* access modifiers changed from: package-private */
    public final void zzp() {
        this.zzq++;
    }

    /* access modifiers changed from: package-private */
    public final zzgf zzs() {
        return this.zzj;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x04b2 A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01eb A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0225 A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0248 A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x024f A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x025c A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x026f A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzb(com.google.android.gms.measurement.internal.zzm r22) {
        /*
            r21 = this;
            r1 = r21
            r2 = r22
            java.lang.String r3 = "_sys"
            java.lang.String r4 = "_pfo"
            java.lang.String r5 = "_uwa"
            java.lang.String r0 = "app_id=?"
            r21.zzw()
            r21.zzk()
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r22)
            java.lang.String r6 = r2.zza
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r6)
            boolean r6 = r21.zze(r22)
            if (r6 != 0) goto L_0x0021
            return
        L_0x0021:
            com.google.android.gms.measurement.internal.zzac r6 = r21.zze()
            java.lang.String r7 = r2.zza
            com.google.android.gms.measurement.internal.zzg r6 = r6.zzb(r7)
            r7 = 0
            if (r6 == 0) goto L_0x0054
            java.lang.String r9 = r6.zze()
            boolean r9 = android.text.TextUtils.isEmpty(r9)
            if (r9 == 0) goto L_0x0054
            java.lang.String r9 = r2.zzb
            boolean r9 = android.text.TextUtils.isEmpty(r9)
            if (r9 != 0) goto L_0x0054
            r6.zzh(r7)
            com.google.android.gms.measurement.internal.zzac r9 = r21.zze()
            r9.zza(r6)
            com.google.android.gms.measurement.internal.zzfz r6 = r21.zzc()
            java.lang.String r9 = r2.zza
            r6.zzd(r9)
        L_0x0054:
            boolean r6 = r2.zzh
            if (r6 != 0) goto L_0x005c
            r21.zzc(r22)
            return
        L_0x005c:
            long r9 = r2.zzm
            int r6 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r6 != 0) goto L_0x006c
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj
            com.google.android.gms.common.util.Clock r6 = r6.zzm()
            long r9 = r6.currentTimeMillis()
        L_0x006c:
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj
            com.google.android.gms.measurement.internal.zzx r6 = r6.zzb()
            java.lang.String r11 = r2.zza
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r12 = com.google.android.gms.measurement.internal.zzap.zzaz
            boolean r6 = r6.zze(r11, r12)
            if (r6 == 0) goto L_0x0085
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj
            com.google.android.gms.measurement.internal.zzah r6 = r6.zzx()
            r6.zzi()
        L_0x0085:
            int r6 = r2.zzn
            r15 = 0
            r13 = 1
            if (r6 == 0) goto L_0x00a7
            if (r6 == r13) goto L_0x00a7
            com.google.android.gms.measurement.internal.zzgf r11 = r1.zzj
            com.google.android.gms.measurement.internal.zzfb r11 = r11.zzr()
            com.google.android.gms.measurement.internal.zzfd r11 = r11.zzi()
            java.lang.String r12 = r2.zza
            java.lang.Object r12 = com.google.android.gms.measurement.internal.zzfb.zza(r12)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            java.lang.String r14 = "Incorrect app type, assuming installed app. appId, appType"
            r11.zza(r14, r12, r6)
            r6 = 0
        L_0x00a7:
            com.google.android.gms.measurement.internal.zzac r11 = r21.zze()
            r11.zzf()
            com.google.android.gms.measurement.internal.zzgf r11 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzx r11 = r11.zzb()     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = r2.zza     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r14 = com.google.android.gms.measurement.internal.zzap.zzaz     // Catch:{ all -> 0x04dd }
            boolean r11 = r11.zze(r12, r14)     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x0127
            com.google.android.gms.measurement.internal.zzac r11 = r21.zze()     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = r2.zza     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "_npa"
            com.google.android.gms.measurement.internal.zzks r14 = r11.zzc(r12, r14)     // Catch:{ all -> 0x04dd }
            if (r14 == 0) goto L_0x00d6
            java.lang.String r11 = "auto"
            java.lang.String r12 = r14.zzb     // Catch:{ all -> 0x04dd }
            boolean r11 = r11.equals(r12)     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x0127
        L_0x00d6:
            java.lang.Boolean r11 = r2.zzs     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x0111
            com.google.android.gms.measurement.internal.zzkq r12 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r16 = "_npa"
            java.lang.Boolean r11 = r2.zzs     // Catch:{ all -> 0x04dd }
            boolean r11 = r11.booleanValue()     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x00e9
            r17 = 1
            goto L_0x00eb
        L_0x00e9:
            r17 = 0
        L_0x00eb:
            java.lang.Long r17 = java.lang.Long.valueOf(r17)     // Catch:{ all -> 0x04dd }
            java.lang.String r18 = "auto"
            r11 = r12
            r7 = r12
            r12 = r16
            r19 = r3
            r8 = r14
            r3 = 1
            r13 = r9
            r15 = r17
            r16 = r18
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            if (r8 == 0) goto L_0x010d
            java.lang.Object r8 = r8.zze     // Catch:{ all -> 0x04dd }
            java.lang.Long r11 = r7.zzc     // Catch:{ all -> 0x04dd }
            boolean r8 = r8.equals(r11)     // Catch:{ all -> 0x04dd }
            if (r8 != 0) goto L_0x012a
        L_0x010d:
            r1.zza(r7, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x012a
        L_0x0111:
            r19 = r3
            r8 = r14
            r3 = 1
            if (r8 == 0) goto L_0x012a
            com.google.android.gms.measurement.internal.zzkq r7 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_npa"
            r15 = 0
            java.lang.String r16 = "auto"
            r11 = r7
            r13 = r9
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            r1.zzb(r7, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x012a
        L_0x0127:
            r19 = r3
            r3 = 1
        L_0x012a:
            com.google.android.gms.measurement.internal.zzac r7 = r21.zze()     // Catch:{ all -> 0x04dd }
            java.lang.String r8 = r2.zza     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzg r7 = r7.zzb(r8)     // Catch:{ all -> 0x04dd }
            if (r7 == 0) goto L_0x01e8
            com.google.android.gms.measurement.internal.zzgf r11 = r1.zzj     // Catch:{ all -> 0x04dd }
            r11.zzi()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = r2.zzb     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = r7.zze()     // Catch:{ all -> 0x04dd }
            java.lang.String r13 = r2.zzr     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = r7.zzf()     // Catch:{ all -> 0x04dd }
            boolean r11 = com.google.android.gms.measurement.internal.zzkv.zza(r11, r12, r13, r14)     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x01e8
            com.google.android.gms.measurement.internal.zzgf r11 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfb r11 = r11.zzr()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfd r11 = r11.zzi()     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "New GMP App Id passed in. Removing cached database data. appId"
            java.lang.String r13 = r7.zzc()     // Catch:{ all -> 0x04dd }
            java.lang.Object r13 = com.google.android.gms.measurement.internal.zzfb.zza(r13)     // Catch:{ all -> 0x04dd }
            r11.zza(r12, r13)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzac r11 = r21.zze()     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = r7.zzc()     // Catch:{ all -> 0x04dd }
            r11.zzak()     // Catch:{ all -> 0x04dd }
            r11.zzd()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r7)     // Catch:{ all -> 0x04dd }
            android.database.sqlite.SQLiteDatabase r12 = r11.c_()     // Catch:{ SQLiteException -> 0x01d3 }
            java.lang.String[] r13 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x01d3 }
            r15 = 0
            r13[r15] = r7     // Catch:{ SQLiteException -> 0x01d1 }
            java.lang.String r14 = "events"
            int r14 = r12.delete(r14, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r15
            java.lang.String r8 = "user_attributes"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "conditional_properties"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "apps"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "raw_events"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "raw_events_metadata"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "event_filters"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "property_filters"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "audience_filter_values"
            int r0 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r0
            if (r14 <= 0) goto L_0x01e6
            com.google.android.gms.measurement.internal.zzfb r0 = r11.zzr()     // Catch:{ SQLiteException -> 0x01d1 }
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzx()     // Catch:{ SQLiteException -> 0x01d1 }
            java.lang.String r8 = "Deleted application data. app, records"
            java.lang.Integer r12 = java.lang.Integer.valueOf(r14)     // Catch:{ SQLiteException -> 0x01d1 }
            r0.zza(r8, r7, r12)     // Catch:{ SQLiteException -> 0x01d1 }
            goto L_0x01e6
        L_0x01d1:
            r0 = move-exception
            goto L_0x01d5
        L_0x01d3:
            r0 = move-exception
            r15 = 0
        L_0x01d5:
            com.google.android.gms.measurement.internal.zzfb r8 = r11.zzr()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfd r8 = r8.zzf()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = "Error deleting application data. appId, error"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.zzfb.zza(r7)     // Catch:{ all -> 0x04dd }
            r8.zza(r11, r7, r0)     // Catch:{ all -> 0x04dd }
        L_0x01e6:
            r7 = 0
            goto L_0x01e9
        L_0x01e8:
            r15 = 0
        L_0x01e9:
            if (r7 == 0) goto L_0x0248
            long r11 = r7.zzm()     // Catch:{ all -> 0x04dd }
            r13 = -2147483648(0xffffffff80000000, double:NaN)
            int r0 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r0 == 0) goto L_0x0203
            long r11 = r7.zzm()     // Catch:{ all -> 0x04dd }
            r8 = r4
            long r3 = r2.zzj     // Catch:{ all -> 0x04dd }
            int r0 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x0204
            r0 = 1
            goto L_0x0205
        L_0x0203:
            r8 = r4
        L_0x0204:
            r0 = 0
        L_0x0205:
            long r3 = r7.zzm()     // Catch:{ all -> 0x04dd }
            int r11 = (r3 > r13 ? 1 : (r3 == r13 ? 0 : -1))
            if (r11 != 0) goto L_0x0221
            java.lang.String r3 = r7.zzl()     // Catch:{ all -> 0x04dd }
            if (r3 == 0) goto L_0x0221
            java.lang.String r3 = r7.zzl()     // Catch:{ all -> 0x04dd }
            java.lang.String r4 = r2.zzc     // Catch:{ all -> 0x04dd }
            boolean r3 = r3.equals(r4)     // Catch:{ all -> 0x04dd }
            if (r3 != 0) goto L_0x0221
            r3 = 1
            goto L_0x0222
        L_0x0221:
            r3 = 0
        L_0x0222:
            r0 = r0 | r3
            if (r0 == 0) goto L_0x0249
            android.os.Bundle r0 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r0.<init>()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = "_pv"
            java.lang.String r4 = r7.zzl()     // Catch:{ all -> 0x04dd }
            r0.putString(r3, r4)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzan r3 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_au"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r0)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r3
            r4 = 0
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.zza(r3, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x024a
        L_0x0248:
            r8 = r4
        L_0x0249:
            r4 = 0
        L_0x024a:
            r21.zzc(r22)     // Catch:{ all -> 0x04dd }
            if (r6 != 0) goto L_0x025c
            com.google.android.gms.measurement.internal.zzac r0 = r21.zze()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = r2.zza     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = "_f"
            com.google.android.gms.measurement.internal.zzaj r0 = r0.zza(r3, r7)     // Catch:{ all -> 0x04dd }
            goto L_0x026d
        L_0x025c:
            r3 = 1
            if (r6 != r3) goto L_0x026c
            com.google.android.gms.measurement.internal.zzac r0 = r21.zze()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = r2.zza     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = "_v"
            com.google.android.gms.measurement.internal.zzaj r0 = r0.zza(r3, r7)     // Catch:{ all -> 0x04dd }
            goto L_0x026d
        L_0x026c:
            r0 = 0
        L_0x026d:
            if (r0 != 0) goto L_0x04b2
            r11 = 3600000(0x36ee80, double:1.7786363E-317)
            long r13 = r9 / r11
            r15 = 1
            long r13 = r13 + r15
            long r13 = r13 * r11
            java.lang.String r0 = "_dac"
            java.lang.String r3 = "_r"
            java.lang.String r7 = "_c"
            java.lang.String r15 = "_et"
            if (r6 != 0) goto L_0x0415
            com.google.android.gms.measurement.internal.zzkq r6 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_fot"
            java.lang.Long r16 = java.lang.Long.valueOf(r13)     // Catch:{ all -> 0x04dd }
            java.lang.String r20 = "auto"
            r11 = r6
            r13 = r9
            r4 = r15
            r15 = r16
            r16 = r20
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            r1.zza(r6, r2)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzx r6 = r6.zzb()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = r2.zzb     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r12 = com.google.android.gms.measurement.internal.zzap.zzan     // Catch:{ all -> 0x04dd }
            boolean r6 = r6.zze(r11, r12)     // Catch:{ all -> 0x04dd }
            if (r6 == 0) goto L_0x02b8
            r21.zzw()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzgf r6 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfu r6 = r6.zzf()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = r2.zza     // Catch:{ all -> 0x04dd }
            r6.zza(r11)     // Catch:{ all -> 0x04dd }
        L_0x02b8:
            r21.zzw()     // Catch:{ all -> 0x04dd }
            r21.zzk()     // Catch:{ all -> 0x04dd }
            android.os.Bundle r6 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r6.<init>()     // Catch:{ all -> 0x04dd }
            r11 = 1
            r6.putLong(r7, r11)     // Catch:{ all -> 0x04dd }
            r6.putLong(r3, r11)     // Catch:{ all -> 0x04dd }
            r11 = 0
            r6.putLong(r5, r11)     // Catch:{ all -> 0x04dd }
            r6.putLong(r8, r11)     // Catch:{ all -> 0x04dd }
            r3 = r19
            r6.putLong(r3, r11)     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = "_sysu"
            r6.putLong(r7, r11)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzx r7 = r7.zzb()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = r2.zza     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r12 = com.google.android.gms.measurement.internal.zzap.zzax     // Catch:{ all -> 0x04dd }
            boolean r7 = r7.zze(r11, r12)     // Catch:{ all -> 0x04dd }
            r11 = 1
            if (r7 == 0) goto L_0x02f2
            r6.putLong(r4, r11)     // Catch:{ all -> 0x04dd }
        L_0x02f2:
            boolean r7 = r2.zzq     // Catch:{ all -> 0x04dd }
            if (r7 == 0) goto L_0x02f9
            r6.putLong(r0, r11)     // Catch:{ all -> 0x04dd }
        L_0x02f9:
            com.google.android.gms.measurement.internal.zzac r0 = r21.zze()     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = r2.zza     // Catch:{ all -> 0x04dd }
            com.google.android.gms.common.internal.Preconditions.checkNotEmpty(r7)     // Catch:{ all -> 0x04dd }
            r0.zzd()     // Catch:{ all -> 0x04dd }
            r0.zzak()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = "first_open_count"
            long r13 = r0.zzh(r7, r11)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ all -> 0x04dd }
            android.content.Context r0 = r0.zzn()     // Catch:{ all -> 0x04dd }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ all -> 0x04dd }
            if (r0 != 0) goto L_0x0336
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfb r0 = r0.zzr()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzf()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = "PackageManager is null, first open report might be inaccurate. appId"
            java.lang.String r5 = r2.zza     // Catch:{ all -> 0x04dd }
            java.lang.Object r5 = com.google.android.gms.measurement.internal.zzfb.zza(r5)     // Catch:{ all -> 0x04dd }
            r0.zza(r3, r5)     // Catch:{ all -> 0x04dd }
            r19 = r8
            r7 = r13
        L_0x0332:
            r11 = 0
            goto L_0x03f8
        L_0x0336:
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ NameNotFoundException -> 0x0348 }
            android.content.Context r0 = r0.zzn()     // Catch:{ NameNotFoundException -> 0x0348 }
            com.google.android.gms.common.wrappers.PackageManagerWrapper r0 = com.google.android.gms.common.wrappers.Wrappers.packageManager(r0)     // Catch:{ NameNotFoundException -> 0x0348 }
            java.lang.String r7 = r2.zza     // Catch:{ NameNotFoundException -> 0x0348 }
            r11 = 0
            android.content.pm.PackageInfo r0 = r0.getPackageInfo(r7, r11)     // Catch:{ NameNotFoundException -> 0x0348 }
            goto L_0x035f
        L_0x0348:
            r0 = move-exception
            com.google.android.gms.measurement.internal.zzgf r7 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfb r7 = r7.zzr()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfd r7 = r7.zzf()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = "Package info is null, first open report might be inaccurate. appId"
            java.lang.String r12 = r2.zza     // Catch:{ all -> 0x04dd }
            java.lang.Object r12 = com.google.android.gms.measurement.internal.zzfb.zza(r12)     // Catch:{ all -> 0x04dd }
            r7.zza(r11, r12, r0)     // Catch:{ all -> 0x04dd }
            r0 = 0
        L_0x035f:
            if (r0 == 0) goto L_0x03b0
            long r11 = r0.firstInstallTime     // Catch:{ all -> 0x04dd }
            r15 = 0
            int r7 = (r11 > r15 ? 1 : (r11 == r15 ? 0 : -1))
            if (r7 == 0) goto L_0x03b0
            long r11 = r0.firstInstallTime     // Catch:{ all -> 0x04dd }
            r19 = r8
            long r7 = r0.lastUpdateTime     // Catch:{ all -> 0x04dd }
            int r0 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x0394
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzx r0 = r0.zzb()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.zzap.zzcl     // Catch:{ all -> 0x04dd }
            boolean r0 = r0.zza(r7)     // Catch:{ all -> 0x04dd }
            if (r0 == 0) goto L_0x038d
            r7 = 0
            int r0 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x0392
            r7 = 1
            r6.putLong(r5, r7)     // Catch:{ all -> 0x04dd }
            goto L_0x0392
        L_0x038d:
            r7 = 1
            r6.putLong(r5, r7)     // Catch:{ all -> 0x04dd }
        L_0x0392:
            r0 = 0
            goto L_0x0395
        L_0x0394:
            r0 = 1
        L_0x0395:
            com.google.android.gms.measurement.internal.zzkq r5 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_fi"
            if (r0 == 0) goto L_0x039e
            r7 = 1
            goto L_0x03a0
        L_0x039e:
            r7 = 0
        L_0x03a0:
            java.lang.Long r15 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x04dd }
            java.lang.String r16 = "auto"
            r11 = r5
            r7 = r13
            r13 = r9
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            r1.zza(r5, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x03b3
        L_0x03b0:
            r19 = r8
            r7 = r13
        L_0x03b3:
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ NameNotFoundException -> 0x03c5 }
            android.content.Context r0 = r0.zzn()     // Catch:{ NameNotFoundException -> 0x03c5 }
            com.google.android.gms.common.wrappers.PackageManagerWrapper r0 = com.google.android.gms.common.wrappers.Wrappers.packageManager(r0)     // Catch:{ NameNotFoundException -> 0x03c5 }
            java.lang.String r5 = r2.zza     // Catch:{ NameNotFoundException -> 0x03c5 }
            r11 = 0
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo(r5, r11)     // Catch:{ NameNotFoundException -> 0x03c5 }
            goto L_0x03dc
        L_0x03c5:
            r0 = move-exception
            com.google.android.gms.measurement.internal.zzgf r5 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfb r5 = r5.zzr()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzfd r5 = r5.zzf()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = "Application info is null, first open report might be inaccurate. appId"
            java.lang.String r12 = r2.zza     // Catch:{ all -> 0x04dd }
            java.lang.Object r12 = com.google.android.gms.measurement.internal.zzfb.zza(r12)     // Catch:{ all -> 0x04dd }
            r5.zza(r11, r12, r0)     // Catch:{ all -> 0x04dd }
            r0 = 0
        L_0x03dc:
            if (r0 == 0) goto L_0x0332
            int r5 = r0.flags     // Catch:{ all -> 0x04dd }
            r11 = 1
            r5 = r5 & r11
            if (r5 == 0) goto L_0x03e9
            r11 = 1
            r6.putLong(r3, r11)     // Catch:{ all -> 0x04dd }
        L_0x03e9:
            int r0 = r0.flags     // Catch:{ all -> 0x04dd }
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0332
            java.lang.String r0 = "_sysu"
            r11 = 1
            r6.putLong(r0, r11)     // Catch:{ all -> 0x04dd }
            goto L_0x0332
        L_0x03f8:
            int r0 = (r7 > r11 ? 1 : (r7 == r11 ? 0 : -1))
            if (r0 < 0) goto L_0x0401
            r3 = r19
            r6.putLong(r3, r7)     // Catch:{ all -> 0x04dd }
        L_0x0401:
            com.google.android.gms.measurement.internal.zzan r0 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_f"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r6)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r0
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.zza(r0, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x046d
        L_0x0415:
            r4 = r15
            r5 = 1
            if (r6 != r5) goto L_0x046d
            com.google.android.gms.measurement.internal.zzkq r5 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_fvt"
            java.lang.Long r15 = java.lang.Long.valueOf(r13)     // Catch:{ all -> 0x04dd }
            java.lang.String r16 = "auto"
            r11 = r5
            r13 = r9
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            r1.zza(r5, r2)     // Catch:{ all -> 0x04dd }
            r21.zzw()     // Catch:{ all -> 0x04dd }
            r21.zzk()     // Catch:{ all -> 0x04dd }
            android.os.Bundle r5 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r5.<init>()     // Catch:{ all -> 0x04dd }
            r11 = 1
            r5.putLong(r7, r11)     // Catch:{ all -> 0x04dd }
            r5.putLong(r3, r11)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x04dd }
            java.lang.String r6 = r2.zza     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.zzap.zzax     // Catch:{ all -> 0x04dd }
            boolean r3 = r3.zze(r6, r7)     // Catch:{ all -> 0x04dd }
            r6 = 1
            if (r3 == 0) goto L_0x0453
            r5.putLong(r4, r6)     // Catch:{ all -> 0x04dd }
        L_0x0453:
            boolean r3 = r2.zzq     // Catch:{ all -> 0x04dd }
            if (r3 == 0) goto L_0x045a
            r5.putLong(r0, r6)     // Catch:{ all -> 0x04dd }
        L_0x045a:
            com.google.android.gms.measurement.internal.zzan r0 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_v"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r5)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r0
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.zza(r0, r2)     // Catch:{ all -> 0x04dd }
        L_0x046d:
            com.google.android.gms.measurement.internal.zzgf r0 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzx r0 = r0.zzb()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = r2.zza     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.zzap.zzay     // Catch:{ all -> 0x04dd }
            boolean r0 = r0.zze(r3, r5)     // Catch:{ all -> 0x04dd }
            if (r0 != 0) goto L_0x04ce
            android.os.Bundle r0 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r0.<init>()     // Catch:{ all -> 0x04dd }
            r5 = 1
            r0.putLong(r4, r5)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzgf r3 = r1.zzj     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzx r3 = r3.zzb()     // Catch:{ all -> 0x04dd }
            java.lang.String r4 = r2.zza     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.zzap.zzax     // Catch:{ all -> 0x04dd }
            boolean r3 = r3.zze(r4, r5)     // Catch:{ all -> 0x04dd }
            if (r3 == 0) goto L_0x049e
            java.lang.String r3 = "_fr"
            r4 = 1
            r0.putLong(r3, r4)     // Catch:{ all -> 0x04dd }
        L_0x049e:
            com.google.android.gms.measurement.internal.zzan r3 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_e"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r0)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r3
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.zza(r3, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x04ce
        L_0x04b2:
            boolean r0 = r2.zzi     // Catch:{ all -> 0x04dd }
            if (r0 == 0) goto L_0x04ce
            android.os.Bundle r0 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r0.<init>()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzan r3 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_cd"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r0)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r3
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.zza(r3, r2)     // Catch:{ all -> 0x04dd }
        L_0x04ce:
            com.google.android.gms.measurement.internal.zzac r0 = r21.zze()     // Catch:{ all -> 0x04dd }
            r0.b_()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzac r0 = r21.zze()
            r0.zzh()
            return
        L_0x04dd:
            r0 = move-exception
            com.google.android.gms.measurement.internal.zzac r2 = r21.zze()
            r2.zzh()
            goto L_0x04e7
        L_0x04e6:
            throw r0
        L_0x04e7:
            goto L_0x04e6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzkj.zzb(com.google.android.gms.measurement.internal.zzm):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void */
    private final zzm zza(String str) {
        String str2 = str;
        zzg zzb2 = zze().zzb(str2);
        if (zzb2 == null || TextUtils.isEmpty(zzb2.zzl())) {
            this.zzj.zzr().zzw().zza("No app data available; dropping", str2);
            return null;
        }
        Boolean zzb3 = zzb(zzb2);
        if (zzb3 == null || zzb3.booleanValue()) {
            return new zzm(str, zzb2.zze(), zzb2.zzl(), zzb2.zzm(), zzb2.zzn(), zzb2.zzo(), zzb2.zzp(), (String) null, zzb2.zzr(), false, zzb2.zzi(), zzb2.zzae(), 0L, 0, zzb2.zzaf(), zzb2.zzag(), false, zzb2.zzf(), zzb2.zzah(), zzb2.zzq(), zzb2.zzai(), (!zzln.zzb() || !this.zzj.zzb().zze(str2, zzap.zzcf)) ? null : zzb2.zzg());
        }
        this.zzj.zzr().zzf().zza("App version does not match; dropping. appId", zzfb.zza(str));
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzv zzv2) {
        zzm zza2 = zza(zzv2.zza);
        if (zza2 != null) {
            zza(zzv2, zza2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzv zzv2, zzm zzm2) {
        Preconditions.checkNotNull(zzv2);
        Preconditions.checkNotEmpty(zzv2.zza);
        Preconditions.checkNotNull(zzv2.zzb);
        Preconditions.checkNotNull(zzv2.zzc);
        Preconditions.checkNotEmpty(zzv2.zzc.zza);
        zzw();
        zzk();
        if (zze(zzm2)) {
            if (!zzm2.zzh) {
                zzc(zzm2);
                return;
            }
            zzv zzv3 = new zzv(zzv2);
            boolean z = false;
            zzv3.zze = false;
            zze().zzf();
            try {
                zzv zzd2 = zze().zzd(zzv3.zza, zzv3.zzc.zza);
                if (zzd2 != null && !zzd2.zzb.equals(zzv3.zzb)) {
                    this.zzj.zzr().zzi().zza("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.zzj.zzj().zzc(zzv3.zzc.zza), zzv3.zzb, zzd2.zzb);
                }
                if (zzd2 != null && zzd2.zze) {
                    zzv3.zzb = zzd2.zzb;
                    zzv3.zzd = zzd2.zzd;
                    zzv3.zzh = zzd2.zzh;
                    zzv3.zzf = zzd2.zzf;
                    zzv3.zzi = zzd2.zzi;
                    zzv3.zze = zzd2.zze;
                    zzv3.zzc = new zzkq(zzv3.zzc.zza, zzd2.zzc.zzb, zzv3.zzc.zza(), zzd2.zzc.zze);
                } else if (TextUtils.isEmpty(zzv3.zzf)) {
                    zzv3.zzc = new zzkq(zzv3.zzc.zza, zzv3.zzd, zzv3.zzc.zza(), zzv3.zzc.zze);
                    zzv3.zze = true;
                    z = true;
                }
                if (zzv3.zze) {
                    zzkq zzkq = zzv3.zzc;
                    zzks zzks = new zzks(zzv3.zza, zzv3.zzb, zzkq.zza, zzkq.zzb, zzkq.zza());
                    if (zze().zza(zzks)) {
                        this.zzj.zzr().zzw().zza("User property updated immediately", zzv3.zza, this.zzj.zzj().zzc(zzks.zzc), zzks.zze);
                    } else {
                        this.zzj.zzr().zzf().zza("(2)Too many active user properties, ignoring", zzfb.zza(zzv3.zza), this.zzj.zzj().zzc(zzks.zzc), zzks.zze);
                    }
                    if (z && zzv3.zzi != null) {
                        zzb(new zzan(zzv3.zzi, zzv3.zzd), zzm2);
                    }
                }
                if (zze().zza(zzv3)) {
                    this.zzj.zzr().zzw().zza("Conditional property added", zzv3.zza, this.zzj.zzj().zzc(zzv3.zzc.zza), zzv3.zzc.zza());
                } else {
                    this.zzj.zzr().zzf().zza("Too many conditional properties, ignoring", zzfb.zza(zzv3.zza), this.zzj.zzj().zzc(zzv3.zzc.zza), zzv3.zzc.zza());
                }
                zze().b_();
            } finally {
                zze().zzh();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(zzv zzv2) {
        zzm zza2 = zza(zzv2.zza);
        if (zza2 != null) {
            zzb(zzv2, zza2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzb(zzv zzv2, zzm zzm2) {
        Preconditions.checkNotNull(zzv2);
        Preconditions.checkNotEmpty(zzv2.zza);
        Preconditions.checkNotNull(zzv2.zzc);
        Preconditions.checkNotEmpty(zzv2.zzc.zza);
        zzw();
        zzk();
        if (zze(zzm2)) {
            if (!zzm2.zzh) {
                zzc(zzm2);
                return;
            }
            zze().zzf();
            try {
                zzc(zzm2);
                zzv zzd2 = zze().zzd(zzv2.zza, zzv2.zzc.zza);
                if (zzd2 != null) {
                    this.zzj.zzr().zzw().zza("Removing conditional user property", zzv2.zza, this.zzj.zzj().zzc(zzv2.zzc.zza));
                    zze().zze(zzv2.zza, zzv2.zzc.zza);
                    if (zzd2.zze) {
                        zze().zzb(zzv2.zza, zzv2.zzc.zza);
                    }
                    if (zzv2.zzk != null) {
                        Bundle bundle = null;
                        if (zzv2.zzk.zzb != null) {
                            bundle = zzv2.zzk.zzb.zzb();
                        }
                        Bundle bundle2 = bundle;
                        zzb(this.zzj.zzi().zza(zzv2.zza, zzv2.zzk.zza, bundle2, zzd2.zzb, zzv2.zzk.zzd, true, false), zzm2);
                    }
                } else {
                    this.zzj.zzr().zzi().zza("Conditional user property doesn't exist", zzfb.zza(zzv2.zza), this.zzj.zzj().zzc(zzv2.zzc.zza));
                }
                zze().b_();
            } finally {
                zze().zzh();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0194  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.measurement.internal.zzg zza(com.google.android.gms.measurement.internal.zzm r9, com.google.android.gms.measurement.internal.zzg r10, java.lang.String r11) {
        /*
            r8 = this;
            r0 = 1
            if (r10 != 0) goto L_0x001e
            com.google.android.gms.measurement.internal.zzg r10 = new com.google.android.gms.measurement.internal.zzg
            com.google.android.gms.measurement.internal.zzgf r1 = r8.zzj
            java.lang.String r2 = r9.zza
            r10.<init>(r1, r2)
            com.google.android.gms.measurement.internal.zzgf r1 = r8.zzj
            com.google.android.gms.measurement.internal.zzkv r1 = r1.zzi()
            java.lang.String r1 = r1.zzk()
            r10.zza(r1)
            r10.zze(r11)
        L_0x001c:
            r11 = 1
            goto L_0x003a
        L_0x001e:
            java.lang.String r1 = r10.zzh()
            boolean r1 = r11.equals(r1)
            if (r1 != 0) goto L_0x0039
            r10.zze(r11)
            com.google.android.gms.measurement.internal.zzgf r11 = r8.zzj
            com.google.android.gms.measurement.internal.zzkv r11 = r11.zzi()
            java.lang.String r11 = r11.zzk()
            r10.zza(r11)
            goto L_0x001c
        L_0x0039:
            r11 = 0
        L_0x003a:
            java.lang.String r1 = r9.zzb
            java.lang.String r2 = r10.zze()
            boolean r1 = android.text.TextUtils.equals(r1, r2)
            if (r1 != 0) goto L_0x004c
            java.lang.String r11 = r9.zzb
            r10.zzb(r11)
            r11 = 1
        L_0x004c:
            java.lang.String r1 = r9.zzr
            java.lang.String r2 = r10.zzf()
            boolean r1 = android.text.TextUtils.equals(r1, r2)
            if (r1 != 0) goto L_0x005e
            java.lang.String r11 = r9.zzr
            r10.zzc(r11)
            r11 = 1
        L_0x005e:
            boolean r1 = com.google.android.gms.internal.measurement.zzln.zzb()
            if (r1 == 0) goto L_0x0088
            com.google.android.gms.measurement.internal.zzgf r1 = r8.zzj
            com.google.android.gms.measurement.internal.zzx r1 = r1.zzb()
            java.lang.String r2 = r10.zzc()
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.zzap.zzcf
            boolean r1 = r1.zze(r2, r3)
            if (r1 == 0) goto L_0x0088
            java.lang.String r1 = r9.zzv
            java.lang.String r2 = r10.zzg()
            boolean r1 = android.text.TextUtils.equals(r1, r2)
            if (r1 != 0) goto L_0x0088
            java.lang.String r11 = r9.zzv
            r10.zzd(r11)
            r11 = 1
        L_0x0088:
            java.lang.String r1 = r9.zzk
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x00a2
            java.lang.String r1 = r9.zzk
            java.lang.String r2 = r10.zzi()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00a2
            java.lang.String r11 = r9.zzk
            r10.zzf(r11)
            r11 = 1
        L_0x00a2:
            long r1 = r9.zze
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x00ba
            long r1 = r9.zze
            long r5 = r10.zzo()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00ba
            long r1 = r9.zze
            r10.zzd(r1)
            r11 = 1
        L_0x00ba:
            java.lang.String r1 = r9.zzc
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x00d4
            java.lang.String r1 = r9.zzc
            java.lang.String r2 = r10.zzl()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00d4
            java.lang.String r11 = r9.zzc
            r10.zzg(r11)
            r11 = 1
        L_0x00d4:
            long r1 = r9.zzj
            long r5 = r10.zzm()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00e4
            long r1 = r9.zzj
            r10.zzc(r1)
            r11 = 1
        L_0x00e4:
            java.lang.String r1 = r9.zzd
            if (r1 == 0) goto L_0x00fa
            java.lang.String r1 = r9.zzd
            java.lang.String r2 = r10.zzn()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00fa
            java.lang.String r11 = r9.zzd
            r10.zzh(r11)
            r11 = 1
        L_0x00fa:
            long r1 = r9.zzf
            long r5 = r10.zzp()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x010a
            long r1 = r9.zzf
            r10.zze(r1)
            r11 = 1
        L_0x010a:
            boolean r1 = r9.zzh
            boolean r2 = r10.zzr()
            if (r1 == r2) goto L_0x0118
            boolean r11 = r9.zzh
            r10.zza(r11)
            r11 = 1
        L_0x0118:
            java.lang.String r1 = r9.zzg
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x0132
            java.lang.String r1 = r9.zzg
            java.lang.String r2 = r10.zzac()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x0132
            java.lang.String r11 = r9.zzg
            r10.zzi(r11)
            r11 = 1
        L_0x0132:
            long r1 = r9.zzl
            long r5 = r10.zzae()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x0142
            long r1 = r9.zzl
            r10.zzp(r1)
            r11 = 1
        L_0x0142:
            boolean r1 = r9.zzo
            boolean r2 = r10.zzaf()
            if (r1 == r2) goto L_0x0150
            boolean r11 = r9.zzo
            r10.zzb(r11)
            r11 = 1
        L_0x0150:
            boolean r1 = r9.zzp
            boolean r2 = r10.zzag()
            if (r1 == r2) goto L_0x015e
            boolean r11 = r9.zzp
            r10.zzc(r11)
            r11 = 1
        L_0x015e:
            com.google.android.gms.measurement.internal.zzgf r1 = r8.zzj
            com.google.android.gms.measurement.internal.zzx r1 = r1.zzb()
            java.lang.String r2 = r9.zza
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.zzap.zzaz
            boolean r1 = r1.zze(r2, r5)
            if (r1 == 0) goto L_0x017c
            java.lang.Boolean r1 = r9.zzs
            java.lang.Boolean r2 = r10.zzah()
            if (r1 == r2) goto L_0x017c
            java.lang.Boolean r11 = r9.zzs
            r10.zza(r11)
            r11 = 1
        L_0x017c:
            long r1 = r9.zzt
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x0192
            long r1 = r9.zzt
            long r3 = r10.zzq()
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x0192
            long r1 = r9.zzt
            r10.zzf(r1)
            r11 = 1
        L_0x0192:
            if (r11 == 0) goto L_0x019b
            com.google.android.gms.measurement.internal.zzac r9 = r8.zze()
            r9.zza(r10)
        L_0x019b:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzkj.zza(com.google.android.gms.measurement.internal.zzm, com.google.android.gms.measurement.internal.zzg, java.lang.String):com.google.android.gms.measurement.internal.zzg");
    }

    /* access modifiers changed from: package-private */
    public final zzg zzc(zzm zzm2) {
        zzw();
        zzk();
        Preconditions.checkNotNull(zzm2);
        Preconditions.checkNotEmpty(zzm2.zza);
        zzg zzb2 = zze().zzb(zzm2.zza);
        String zzb3 = this.zzj.zzc().zzb(zzm2.zza);
        if (!zzkq.zzb() || !this.zzj.zzb().zza(zzap.zzcn)) {
            return zza(zzm2, zzb2, zzb3);
        }
        if (zzb2 == null) {
            zzb2 = new zzg(this.zzj, zzm2.zza);
            zzb2.zza(this.zzj.zzi().zzk());
            zzb2.zze(zzb3);
        } else if (!zzb3.equals(zzb2.zzh())) {
            zzb2.zze(zzb3);
            zzb2.zza(this.zzj.zzi().zzk());
        }
        zzb2.zzb(zzm2.zzb);
        zzb2.zzc(zzm2.zzr);
        if (zzln.zzb() && this.zzj.zzb().zze(zzb2.zzc(), zzap.zzcf)) {
            zzb2.zzd(zzm2.zzv);
        }
        if (!TextUtils.isEmpty(zzm2.zzk)) {
            zzb2.zzf(zzm2.zzk);
        }
        if (zzm2.zze != 0) {
            zzb2.zzd(zzm2.zze);
        }
        if (!TextUtils.isEmpty(zzm2.zzc)) {
            zzb2.zzg(zzm2.zzc);
        }
        zzb2.zzc(zzm2.zzj);
        if (zzm2.zzd != null) {
            zzb2.zzh(zzm2.zzd);
        }
        zzb2.zze(zzm2.zzf);
        zzb2.zza(zzm2.zzh);
        if (!TextUtils.isEmpty(zzm2.zzg)) {
            zzb2.zzi(zzm2.zzg);
        }
        zzb2.zzp(zzm2.zzl);
        zzb2.zzb(zzm2.zzo);
        zzb2.zzc(zzm2.zzp);
        if (this.zzj.zzb().zze(zzm2.zza, zzap.zzaz)) {
            zzb2.zza(zzm2.zzs);
        }
        zzb2.zzf(zzm2.zzt);
        if (zzb2.zza()) {
            zze().zza(zzb2);
        }
        return zzb2;
    }

    /* access modifiers changed from: package-private */
    public final String zzd(zzm zzm2) {
        try {
            return (String) this.zzj.zzq().zza(new zzkn(this, zzm2)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            this.zzj.zzr().zzf().zza("Failed to get app instance id. appId", zzfb.zza(zzm2.zza), e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(boolean z) {
        zzz();
    }

    private final boolean zze(zzm zzm2) {
        return (!zzln.zzb() || !this.zzj.zzb().zze(zzm2.zza, zzap.zzcf)) ? !TextUtils.isEmpty(zzm2.zzb) || !TextUtils.isEmpty(zzm2.zzr) : !TextUtils.isEmpty(zzm2.zzb) || !TextUtils.isEmpty(zzm2.zzv) || !TextUtils.isEmpty(zzm2.zzr);
    }
}
