package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcwf implements zzdxg<zzcub<? extends zzcty<JSONObject>>> {
    private static final zzcwf zzgij = new zzcwf();

    public static zzcub<? extends zzcty<JSONObject>> zzaoa() {
        return (zzcub) zzdxm.zza(zzcwd.zzgii, "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzaoa();
    }
}
