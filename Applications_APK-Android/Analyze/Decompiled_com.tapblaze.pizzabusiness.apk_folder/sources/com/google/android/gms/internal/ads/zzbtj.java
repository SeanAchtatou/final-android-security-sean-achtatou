package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.VideoController;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbtj extends zzbrl<VideoController.VideoLifecycleCallbacks> {
    private boolean zzehh;

    protected zzbtj(Set<zzbsu<VideoController.VideoLifecycleCallbacks>> set) {
        super(set);
    }

    public final void onVideoPause() {
        zza(zzbtm.zzfhp);
    }

    public final void onVideoEnd() {
        zza(zzbtl.zzfhp);
    }

    public final synchronized void onVideoStart() {
        zza(zzbto.zzfhp);
        this.zzehh = true;
    }

    public final synchronized void onVideoPlay() {
        if (!this.zzehh) {
            zza(zzbtn.zzfhp);
            this.zzehh = true;
        }
        zza(zzbtq.zzfhp);
    }
}
