package com.google.android.gms.internal.p000firebaseperf;

import android.support.v4.media.session.PlaybackStateCompat;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbn  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public enum zzbn {
    TERABYTES(1099511627776L),
    GIGABYTES(1073741824),
    MEGABYTES(PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED),
    KILOBYTES(PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID),
    BYTES(1);
    
    private long zzhw;

    private zzbn(long j) {
        this.zzhw = j;
    }

    public final long zzt(long j) {
        return (j * this.zzhw) / KILOBYTES.zzhw;
    }
}
