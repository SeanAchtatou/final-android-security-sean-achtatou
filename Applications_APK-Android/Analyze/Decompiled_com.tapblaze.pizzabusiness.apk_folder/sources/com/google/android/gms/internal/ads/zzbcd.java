package com.google.android.gms.internal.ads;

import android.net.Uri;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbcd implements zznl {
    private Uri uri;
    private final zznl zzecs;
    private final long zzect;
    private final zznl zzecu;
    private long zzecv;

    zzbcd(zznl zznl, int i, zznl zznl2) {
        this.zzecs = zznl;
        this.zzect = (long) i;
        this.zzecu = zznl2;
    }

    public final long zza(zznq zznq) throws IOException {
        zznq zznq2;
        zznq zznq3;
        long j;
        zznq zznq4 = zznq;
        this.uri = zznq4.uri;
        if (zznq4.zzamw >= this.zzect) {
            zznq2 = null;
        } else {
            long j2 = zznq4.zzamw;
            if (zznq4.zzce != -1) {
                j = Math.min(zznq4.zzce, this.zzect - j2);
            } else {
                j = this.zzect - j2;
            }
            zznq2 = new zznq(zznq4.uri, j2, j, null);
        }
        if (zznq4.zzce == -1 || zznq4.zzamw + zznq4.zzce > this.zzect) {
            zznq3 = new zznq(zznq4.uri, Math.max(this.zzect, zznq4.zzamw), zznq4.zzce != -1 ? Math.min(zznq4.zzce, (zznq4.zzamw + zznq4.zzce) - this.zzect) : -1, null);
        } else {
            zznq3 = null;
        }
        long j3 = 0;
        long zza = zznq2 != null ? this.zzecs.zza(zznq2) : 0;
        if (zznq3 != null) {
            j3 = this.zzecu.zza(zznq3);
        }
        this.zzecv = zznq4.zzamw;
        if (zza == -1 || j3 == -1) {
            return -1;
        }
        return zza + j3;
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        long j = this.zzecv;
        long j2 = this.zzect;
        if (j < j2) {
            i3 = this.zzecs.read(bArr, i, (int) Math.min((long) i2, j2 - j));
            this.zzecv += (long) i3;
        } else {
            i3 = 0;
        }
        if (this.zzecv < this.zzect) {
            return i3;
        }
        int read = this.zzecu.read(bArr, i + i3, i2 - i3);
        int i4 = i3 + read;
        this.zzecv += (long) read;
        return i4;
    }

    public final Uri getUri() {
        return this.uri;
    }

    public final void close() throws IOException {
        this.zzecs.close();
        this.zzecu.close();
    }
}
