package com.google.android.gms.internal.ads;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public abstract class zzdfp<E> implements Iterator<E> {
    protected zzdfp() {
    }

    @Deprecated
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
