package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzkc extends zzjz {
    public final long zzaum;
    public final List<zzkb> zzaun = new ArrayList();
    public final List<zzkc> zzauo = new ArrayList();

    public zzkc(int i, long j) {
        super(i);
        this.zzaum = j;
    }

    public final zzkb zzao(int i) {
        int size = this.zzaun.size();
        for (int i2 = 0; i2 < size; i2++) {
            zzkb zzkb = this.zzaun.get(i2);
            if (zzkb.type == i) {
                return zzkb;
            }
        }
        return null;
    }

    public final zzkc zzap(int i) {
        int size = this.zzauo.size();
        for (int i2 = 0; i2 < size; i2++) {
            zzkc zzkc = this.zzauo.get(i2);
            if (zzkc.type == i) {
                return zzkc;
            }
        }
        return null;
    }

    public final String toString() {
        String zzam = zzam(this.type);
        String arrays = Arrays.toString(this.zzaun.toArray());
        String arrays2 = Arrays.toString(this.zzauo.toArray());
        StringBuilder sb = new StringBuilder(String.valueOf(zzam).length() + 22 + String.valueOf(arrays).length() + String.valueOf(arrays2).length());
        sb.append(zzam);
        sb.append(" leaves: ");
        sb.append(arrays);
        sb.append(" containers: ");
        sb.append(arrays2);
        return sb.toString();
    }
}
