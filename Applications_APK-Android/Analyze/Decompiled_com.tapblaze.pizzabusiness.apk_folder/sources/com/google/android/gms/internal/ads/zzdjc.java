package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdjc extends zzdii<zzdlf> {
    zzdjc() {
        super(zzdlf.class, new zzdje(zzdhx.class));
    }

    public final String getKeyType() {
        return "type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey";
    }

    public final zzdna.zzb zzasd() {
        return zzdna.zzb.SYMMETRIC;
    }

    public final zzdih<zzdlg, zzdlf> zzasg() {
        return new zzdjd(this, zzdlg.class);
    }

    public final /* synthetic */ void zze(zzdte zzdte) throws GeneralSecurityException {
        zzdpo.zzx(((zzdlf) zzdte).getVersion(), 0);
    }

    public final /* synthetic */ zzdte zzr(zzdqk zzdqk) throws zzdse {
        return zzdlf.zzw(zzdqk);
    }
}
