package com.google.android.gms.ads.internal;

import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.ads.zzaar;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzug;
import com.ironsource.sdk.constants.Constants;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzo {
    private final String zzblt;
    private final Map<String, String> zzblu = new TreeMap();
    private String zzblv;
    private String zzblw;

    public zzo(String str) {
        this.zzblt = str;
    }

    public final String zzkg() {
        return this.zzblw;
    }

    public final String getQuery() {
        return this.zzblv;
    }

    public final String zzkh() {
        return this.zzblt;
    }

    public final Map<String, String> zzki() {
        return this.zzblu;
    }

    public final void zza(zzug zzug, zzazb zzazb) {
        this.zzblv = zzug.zzccd.zzblv;
        Bundle bundle = zzug.zzccf != null ? zzug.zzccf.getBundle(AdMobAdapter.class.getName()) : null;
        if (bundle != null) {
            String str = zzaar.zzcsn.get();
            for (String next : bundle.keySet()) {
                if (str.equals(next)) {
                    this.zzblw = bundle.getString(next);
                } else if (next.startsWith("csa_")) {
                    this.zzblu.put(next.substring(4), bundle.getString(next));
                }
            }
            this.zzblu.put(Constants.RequestParameters.SDK_VERSION, zzazb.zzbma);
        }
    }
}
