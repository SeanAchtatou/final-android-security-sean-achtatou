package com.google.android.gms.measurement.internal;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class zzip implements Runnable {
    private final /* synthetic */ zzio zza;
    private final /* synthetic */ long zzb;
    private final /* synthetic */ zzin zzc;

    zzip(zzin zzin, zzio zzio, long j) {
        this.zzc = zzin;
        this.zza = zzio;
        this.zzb = j;
    }

    public final void run() {
        this.zzc.zza(this.zza, false, this.zzb);
        zzin zzin = this.zzc;
        zzin.zza = null;
        zzin.zzh().zza((zzio) null);
    }
}
