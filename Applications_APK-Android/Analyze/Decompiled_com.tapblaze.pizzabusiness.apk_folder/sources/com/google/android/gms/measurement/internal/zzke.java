package com.google.android.gms.measurement.internal;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class zzke extends zzaf {
    private final /* synthetic */ zzkb zza;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzke(zzkb zzkb, zzhc zzhc) {
        super(zzhc);
        this.zza = zzkb;
    }

    public final void zza() {
        this.zza.zzc();
    }
}
