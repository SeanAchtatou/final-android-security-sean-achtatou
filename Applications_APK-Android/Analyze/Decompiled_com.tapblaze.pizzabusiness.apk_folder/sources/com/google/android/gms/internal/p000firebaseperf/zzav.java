package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzav  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzav extends zzaz<Float> {
    private static zzav zzaz;

    private zzav() {
    }

    /* access modifiers changed from: protected */
    public final String zzaf() {
        return "sessions_sampling_percentage";
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.SessionSamplingRate";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_vc_session_sampling_rate";
    }

    public static synchronized zzav zzax() {
        zzav zzav;
        synchronized (zzav.class) {
            if (zzaz == null) {
                zzaz = new zzav();
            }
            zzav = zzaz;
        }
        return zzav;
    }
}
