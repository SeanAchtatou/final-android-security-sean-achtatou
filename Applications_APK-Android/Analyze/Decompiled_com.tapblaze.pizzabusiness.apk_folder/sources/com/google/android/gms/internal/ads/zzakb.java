package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzakb implements zzaxh<zzaif> {
    zzakb() {
    }

    public final /* synthetic */ void zzh(Object obj) {
        zzaif zzaif = (zzaif) obj;
        zzaif.zza("/log", zzafa.zzcxa);
        zzaif.zza("/result", zzafa.zzcxi);
    }
}
