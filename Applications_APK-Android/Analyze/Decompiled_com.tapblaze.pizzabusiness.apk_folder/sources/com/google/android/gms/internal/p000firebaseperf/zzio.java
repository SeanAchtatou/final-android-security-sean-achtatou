package com.google.android.gms.internal.p000firebaseperf;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzio  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public enum zzio {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf((double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(zzeb.zzmv),
    ENUM(null),
    MESSAGE(null);
    
    private final Object zzrs;

    private zzio(Object obj) {
        this.zzrs = obj;
    }
}
