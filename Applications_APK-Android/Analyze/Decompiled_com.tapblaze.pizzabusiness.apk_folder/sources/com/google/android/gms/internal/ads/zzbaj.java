package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzbaj {
    public abstract zzbag zza(Context context, zzbaz zzbaz, int i, boolean z, zzaae zzaae, zzbaw zzbaw);

    protected static boolean zzb(zzbaz zzbaz) {
        return zzbaz.zzzy().zzabt();
    }
}
