package com.google.android.gms.internal.ads;

import android.webkit.ValueCallback;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzqn implements ValueCallback<String> {
    private final /* synthetic */ zzqk zzbqd;

    zzqn(zzqk zzqk) {
        this.zzbqd = zzqk;
    }

    public final /* synthetic */ void onReceiveValue(Object obj) {
        this.zzbqd.zzbpz.zza(this.zzbqd.zzbpw, this.zzbqd.zzbpx, (String) obj, this.zzbqd.zzbpy);
    }
}
