package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzei  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
abstract class zzei extends zzeb {
    zzei() {
    }

    /* access modifiers changed from: package-private */
    public abstract boolean zza(zzeb zzeb, int i, int i2);
}
