package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzat  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzat extends zzaz<Long> {
    private static zzat zzax;

    private zzat() {
    }

    /* access modifiers changed from: protected */
    public final String zzaf() {
        return "sessions_memory_capture_frequency_bg_ms";
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.SessionsMemoryCaptureFrequencyBackgroundMs";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_session_gauge_memory_capture_frequency_bg_ms";
    }

    public static synchronized zzat zzav() {
        zzat zzat;
        synchronized (zzat.class) {
            if (zzax == null) {
                zzax = new zzat();
            }
            zzat = zzax;
        }
        return zzat;
    }
}
