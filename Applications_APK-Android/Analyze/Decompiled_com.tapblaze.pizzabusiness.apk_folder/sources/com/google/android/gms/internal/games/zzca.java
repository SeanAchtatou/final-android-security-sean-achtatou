package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.games.Players;
import com.google.android.gms.games.internal.zzg;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
final class zzca extends zzcc {
    private final /* synthetic */ int zzkn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzca(zzbu zzbu, GoogleApiClient googleApiClient, int i) {
        super(googleApiClient);
        this.zzkn = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.games.internal.zzg.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.Players$LoadPlayersResult>, java.lang.String, int, boolean, boolean):void
     arg types: [com.google.android.gms.internal.games.zzca, java.lang.String, int, int, int]
     candidates:
      com.google.android.gms.games.internal.zzg.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.leaderboard.Leaderboards$LoadPlayerScoreResult>, java.lang.String, java.lang.String, int, int):void
      com.google.android.gms.games.internal.zzg.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.snapshot.Snapshots$OpenSnapshotResult>, java.lang.String, java.lang.String, com.google.android.gms.games.snapshot.SnapshotMetadataChange, com.google.android.gms.games.snapshot.SnapshotContents):void
      com.google.android.gms.games.internal.zzg.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer$UpdateMatchResult>, java.lang.String, byte[], java.lang.String, com.google.android.gms.games.multiplayer.ParticipantResult[]):void
      com.google.android.gms.games.internal.zzg.zza(com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder<com.google.android.gms.games.Players$LoadPlayersResult>, java.lang.String, int, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzg) anyClient).zza((BaseImplementation.ResultHolder<Players.LoadPlayersResult>) this, "played_with", this.zzkn, true, false);
    }
}
