package com.google.android.gms.games.appcontent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.games.internal.zzc;
import com.google.android.gms.games.internal.zzd;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class AppContentCardEntity extends zzc implements zze {
    public static final Parcelable.Creator<AppContentCardEntity> CREATOR = new zzg();
    private final String description;
    private final Bundle extras;
    private final String type;
    private final String zzca;
    private final ArrayList<AppContentConditionEntity> zzfr;
    private final String zzfs;
    private final String zzft;
    private final ArrayList<AppContentActionEntity> zzgc;
    private final ArrayList<AppContentAnnotationEntity> zzgd;
    private final int zzge;
    private final String zzgf;
    private final int zzgg;

    AppContentCardEntity(ArrayList<AppContentActionEntity> arrayList, ArrayList<AppContentAnnotationEntity> arrayList2, ArrayList<AppContentConditionEntity> arrayList3, String str, int i, String str2, Bundle bundle, String str3, String str4, int i2, String str5, String str6) {
        this.zzgc = arrayList;
        this.zzgd = arrayList2;
        this.zzfr = arrayList3;
        this.zzfs = str;
        this.zzge = i;
        this.description = str2;
        this.extras = bundle;
        this.zzft = str6;
        this.zzgf = str3;
        this.zzca = str4;
        this.zzgg = i2;
        this.type = str5;
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    public final List<zza> getActions() {
        return new ArrayList(this.zzgc);
    }

    public final List<zzb> zzap() {
        return new ArrayList(this.zzgd);
    }

    public final List<zzf> zzag() {
        return new ArrayList(this.zzfr);
    }

    public final String zzah() {
        return this.zzfs;
    }

    public final int zzaq() {
        return this.zzge;
    }

    public final String getDescription() {
        return this.description;
    }

    public final Bundle getExtras() {
        return this.extras;
    }

    public final String getId() {
        return this.zzft;
    }

    public final String zzar() {
        return this.zzgf;
    }

    public final String getTitle() {
        return this.zzca;
    }

    public final int zzas() {
        return this.zzgg;
    }

    public final String getType() {
        return this.type;
    }

    public final int hashCode() {
        return Objects.hashCode(getActions(), zzap(), zzag(), zzah(), Integer.valueOf(zzaq()), getDescription(), Integer.valueOf(zzd.zza(getExtras())), getId(), zzar(), getTitle(), Integer.valueOf(zzas()), getType());
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zze)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        zze zze = (zze) obj;
        if (!Objects.equal(zze.getActions(), getActions()) || !Objects.equal(zze.zzap(), zzap()) || !Objects.equal(zze.zzag(), zzag()) || !Objects.equal(zze.zzah(), zzah()) || !Objects.equal(Integer.valueOf(zze.zzaq()), Integer.valueOf(zzaq())) || !Objects.equal(zze.getDescription(), getDescription()) || !zzd.zza(zze.getExtras(), getExtras()) || !Objects.equal(zze.getId(), getId()) || !Objects.equal(zze.zzar(), zzar()) || !Objects.equal(zze.getTitle(), getTitle()) || !Objects.equal(Integer.valueOf(zze.zzas()), Integer.valueOf(zzas())) || !Objects.equal(zze.getType(), getType())) {
            return false;
        }
        return true;
    }

    public final String toString() {
        return Objects.toStringHelper(this).add("Actions", getActions()).add("Annotations", zzap()).add("Conditions", zzag()).add("ContentDescription", zzah()).add("CurrentSteps", Integer.valueOf(zzaq())).add("Description", getDescription()).add("Extras", getExtras()).add("Id", getId()).add("Subtitle", zzar()).add("Title", getTitle()).add("TotalSteps", Integer.valueOf(zzas())).add("Type", getType()).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, getActions(), false);
        SafeParcelWriter.writeTypedList(parcel, 2, zzap(), false);
        SafeParcelWriter.writeTypedList(parcel, 3, zzag(), false);
        SafeParcelWriter.writeString(parcel, 4, this.zzfs, false);
        SafeParcelWriter.writeInt(parcel, 5, this.zzge);
        SafeParcelWriter.writeString(parcel, 6, this.description, false);
        SafeParcelWriter.writeBundle(parcel, 7, this.extras, false);
        SafeParcelWriter.writeString(parcel, 10, this.zzgf, false);
        SafeParcelWriter.writeString(parcel, 11, this.zzca, false);
        SafeParcelWriter.writeInt(parcel, 12, this.zzgg);
        SafeParcelWriter.writeString(parcel, 13, this.type, false);
        SafeParcelWriter.writeString(parcel, 14, this.zzft, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
