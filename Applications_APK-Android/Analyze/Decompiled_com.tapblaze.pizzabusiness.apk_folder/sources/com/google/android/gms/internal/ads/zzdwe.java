package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdwe implements zzdsa {
    static final zzdsa zzew = new zzdwe();

    private zzdwe() {
    }

    public final boolean zzf(int i) {
        return zzdvx.zzb.zzg.zzhf(i) != null;
    }
}
