package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzdcp<E> {
    void zza(zzdca<E, ?> zzdca);

    void zza(zzdca<E, ?> zzdca, Throwable th);

    void zzb(zzdca<E, ?> zzdca);

    void zzc(zzdca<E, ?> zzdca);
}
