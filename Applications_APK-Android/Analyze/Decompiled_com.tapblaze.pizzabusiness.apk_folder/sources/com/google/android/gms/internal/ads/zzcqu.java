package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcqu implements zzcty {
    private final Bundle zzdpq;
    private final zzcqv zzgfa;

    zzcqu(zzcqv zzcqv, Bundle bundle) {
        this.zzgfa = zzcqv;
        this.zzdpq = bundle;
    }

    public final void zzr(Object obj) {
        this.zzgfa.zzb(this.zzdpq, (Bundle) obj);
    }
}
