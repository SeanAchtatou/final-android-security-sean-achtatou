package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdtr implements zzdtc {
    private final int flags;
    private final String info;
    private final Object[] zzhoy;
    private final zzdte zzhpb;

    zzdtr(zzdte zzdte, String str, Object[] objArr) {
        this.zzhpb = zzdte;
        this.info = str;
        this.zzhoy = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.flags = charAt;
            return;
        }
        char c = charAt & 8191;
        int i = 13;
        int i2 = 1;
        while (true) {
            int i3 = i2 + 1;
            char charAt2 = str.charAt(i2);
            if (charAt2 >= 55296) {
                c |= (charAt2 & 8191) << i;
                i += 13;
                i2 = i3;
            } else {
                this.flags = c | (charAt2 << i);
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final String zzbbn() {
        return this.info;
    }

    /* access modifiers changed from: package-private */
    public final Object[] zzbbo() {
        return this.zzhoy;
    }

    public final zzdte zzbbi() {
        return this.zzhpb;
    }

    public final int zzbbg() {
        return (this.flags & 1) == 1 ? zzdrt.zze.zzhna : zzdrt.zze.zzhnb;
    }

    public final boolean zzbbh() {
        return (this.flags & 2) == 2;
    }
}
