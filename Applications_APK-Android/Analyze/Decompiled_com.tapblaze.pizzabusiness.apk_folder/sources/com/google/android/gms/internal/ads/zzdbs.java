package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdbs implements zzdxg<zzdhd> {
    private static final zzdbs zzgpq = new zzdbs();

    public static zzdbs zzapw() {
        return zzgpq;
    }

    public final /* synthetic */ Object get() {
        zzdhd zzdhd;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpc)).booleanValue()) {
            zzdhd = zzazd.zzdwg;
        } else {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpb)).booleanValue()) {
                zzdhd = zzazd.zzdwe;
            } else {
                zzdhd = zzazd.zzdwi;
            }
        }
        return (zzdhd) zzdxm.zza(zzdhd, "Cannot return null from a non-@Nullable @Provides method");
    }
}
