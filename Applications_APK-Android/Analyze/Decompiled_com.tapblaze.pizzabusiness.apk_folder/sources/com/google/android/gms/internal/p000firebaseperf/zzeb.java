package com.google.android.gms.internal.p000firebaseperf;

import com.flurry.android.Constants;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzeb  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public abstract class zzeb implements Serializable, Iterable<Byte> {
    public static final zzeb zzmv = new zzel(zzfg.EMPTY_BYTE_ARRAY);
    private static final zzeh zzmw = (zzdz.zzgi() ? new zzek(null) : new zzef(null));
    private static final Comparator<zzeb> zzmx = new zzed();
    private int zzv = 0;

    zzeb() {
    }

    /* access modifiers changed from: private */
    public static int zza(byte b) {
        return b & Constants.UNKNOWN;
    }

    public abstract boolean equals(Object obj);

    public abstract int size();

    /* access modifiers changed from: protected */
    public abstract String zza(Charset charset);

    /* access modifiers changed from: package-private */
    public abstract void zza(zzdy zzdy) throws IOException;

    /* access modifiers changed from: protected */
    public abstract int zzb(int i, int i2, int i3);

    public abstract zzeb zzd(int i, int i2);

    public abstract boolean zzgl();

    public abstract byte zzq(int i);

    /* access modifiers changed from: package-private */
    public abstract byte zzr(int i);

    public static zzeb zzaj(String str) {
        return new zzel(str.getBytes(zzfg.UTF_8));
    }

    public final String zzgk() {
        return size() == 0 ? "" : zza(zzfg.UTF_8);
    }

    public final int hashCode() {
        int i = this.zzv;
        if (i == 0) {
            int size = size();
            i = zzb(size, 0, size);
            if (i == 0) {
                i = 1;
            }
            this.zzv = i;
        }
        return i;
    }

    static zzej zzs(int i) {
        return new zzej(i, null);
    }

    /* access modifiers changed from: protected */
    public final int zzgm() {
        return this.zzv;
    }

    static int zzc(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Beginning index: ");
            sb.append(i);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (i2 < i) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            throw new IndexOutOfBoundsException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder(37);
            sb3.append("End index: ");
            sb3.append(i2);
            sb3.append(" >= ");
            sb3.append(i3);
            throw new IndexOutOfBoundsException(sb3.toString());
        }
    }

    public final String toString() {
        Locale locale = Locale.ROOT;
        Object[] objArr = new Object[3];
        objArr[0] = Integer.toHexString(System.identityHashCode(this));
        objArr[1] = Integer.valueOf(size());
        objArr[2] = size() <= 50 ? zzhp.zzd(this) : String.valueOf(zzhp.zzd(zzd(0, 47))).concat("...");
        return String.format(locale, "<ByteString@%s size=%d contents=\"%s\">", objArr);
    }

    public /* synthetic */ Iterator iterator() {
        return new zzea(this);
    }
}
