package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzis extends zzip {
    public final zzio zzamc = new zzio();
    public long zzamd;
    private final int zzame = 0;
    public ByteBuffer zzcs;

    public zzis(int i) {
    }

    public final void zzy(int i) throws IllegalStateException {
        ByteBuffer byteBuffer = this.zzcs;
        if (byteBuffer == null) {
            this.zzcs = zzz(i);
            return;
        }
        int capacity = byteBuffer.capacity();
        int position = this.zzcs.position();
        int i2 = i + position;
        if (capacity < i2) {
            ByteBuffer zzz = zzz(i2);
            if (position > 0) {
                this.zzcs.position(0);
                this.zzcs.limit(position);
                zzz.put(this.zzcs);
            }
            this.zzcs = zzz;
        }
    }

    public final boolean zzgd() {
        return zzx(1073741824);
    }

    public final void clear() {
        super.clear();
        ByteBuffer byteBuffer = this.zzcs;
        if (byteBuffer != null) {
            byteBuffer.clear();
        }
    }

    private final ByteBuffer zzz(int i) {
        ByteBuffer byteBuffer = this.zzcs;
        int capacity = byteBuffer == null ? 0 : byteBuffer.capacity();
        StringBuilder sb = new StringBuilder(44);
        sb.append("Buffer too small (");
        sb.append(capacity);
        sb.append(" < ");
        sb.append(i);
        sb.append(")");
        throw new IllegalStateException(sb.toString());
    }
}
