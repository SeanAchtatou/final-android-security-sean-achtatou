package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public enum zzdvf {
    DOUBLE(zzdvm.DOUBLE, 1),
    FLOAT(zzdvm.FLOAT, 5),
    INT64(zzdvm.LONG, 0),
    UINT64(zzdvm.LONG, 0),
    INT32(zzdvm.INT, 0),
    FIXED64(zzdvm.LONG, 1),
    FIXED32(zzdvm.INT, 5),
    BOOL(zzdvm.BOOLEAN, 0),
    STRING(zzdvm.STRING, 2),
    GROUP(zzdvm.MESSAGE, 3),
    MESSAGE(zzdvm.MESSAGE, 2),
    BYTES(zzdvm.BYTE_STRING, 2),
    UINT32(zzdvm.INT, 0),
    ENUM(zzdvm.ENUM, 0),
    SFIXED32(zzdvm.INT, 5),
    SFIXED64(zzdvm.LONG, 1),
    SINT32(zzdvm.INT, 0),
    SINT64(zzdvm.LONG, 0);
    
    private final zzdvm zzhsx;
    private final int zzhsy;

    private zzdvf(zzdvm zzdvm, int i) {
        this.zzhsx = zzdvm;
        this.zzhsy = i;
    }

    public final zzdvm zzbcp() {
        return this.zzhsx;
    }

    public final int zzbcq() {
        return this.zzhsy;
    }
}
