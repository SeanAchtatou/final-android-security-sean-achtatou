package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbd  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final /* synthetic */ class zzbd implements Runnable {
    private final zzbb zzbe;
    private final zzbt zzbf;

    zzbd(zzbb zzbb, zzbt zzbt) {
        this.zzbe = zzbb;
        this.zzbf = zzbt;
    }

    public final void run() {
        this.zzbe.zzd(this.zzbf);
    }
}
