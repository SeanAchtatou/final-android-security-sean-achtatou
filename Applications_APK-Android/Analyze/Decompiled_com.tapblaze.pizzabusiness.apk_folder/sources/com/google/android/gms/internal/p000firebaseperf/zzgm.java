package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgm  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
interface zzgm {
    boolean zzb(Class<?> cls);

    zzgj zzc(Class<?> cls);
}
