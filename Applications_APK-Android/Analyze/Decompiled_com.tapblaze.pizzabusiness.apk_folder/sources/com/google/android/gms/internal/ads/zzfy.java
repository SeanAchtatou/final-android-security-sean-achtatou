package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zzfy extends zzdrt<zzfy, zza> implements zzdtg {
    /* access modifiers changed from: private */
    public static final zzfy zzaan;
    private static volatile zzdtn<zzfy> zzdz;
    private zzfz zzaak;
    private zzdqk zzaal = zzdqk.zzhhx;
    private zzdqk zzaam = zzdqk.zzhhx;
    private int zzdl;

    private zzfy() {
    }

    /* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzfy, zza> implements zzdtg {
        private zza() {
            super(zzfy.zzaan);
        }

        /* synthetic */ zza(zzfx zzfx) {
            this();
        }
    }

    public final zzfz zzct() {
        zzfz zzfz = this.zzaak;
        return zzfz == null ? zzfz.zzdd() : zzfz;
    }

    public final zzdqk zzcu() {
        return this.zzaal;
    }

    public final zzdqk zzcv() {
        return this.zzaam;
    }

    public static zzfy zza(zzdqk zzdqk, zzdrg zzdrg) throws zzdse {
        return (zzfy) zzdrt.zza(zzaan, zzdqk, zzdrg);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzfx.zzdk[i - 1]) {
            case 1:
                return new zzfy();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzaan, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\t\u0000\u0002\n\u0001\u0003\n\u0002", new Object[]{"zzdl", "zzaak", "zzaal", "zzaam"});
            case 4:
                return zzaan;
            case 5:
                zzdtn<zzfy> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzfy.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzaan);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzfy zzfy = new zzfy();
        zzaan = zzfy;
        zzdrt.zza(zzfy.class, zzfy);
    }
}
