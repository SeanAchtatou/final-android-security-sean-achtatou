package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfb  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final /* synthetic */ class zzfb {
    static final /* synthetic */ int[] zzqj = new int[zzfa.values().length];
    static final /* synthetic */ int[] zzqk = new int[zzfn.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(14:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x003d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
    static {
        /*
            com.google.android.gms.internal.firebase-perf.zzfn[] r0 = com.google.android.gms.internal.p000firebaseperf.zzfn.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.android.gms.internal.p000firebaseperf.zzfb.zzqk = r0
            r0 = 1
            int[] r1 = com.google.android.gms.internal.p000firebaseperf.zzfb.zzqk     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.google.android.gms.internal.firebase-perf.zzfn r2 = com.google.android.gms.internal.p000firebaseperf.zzfn.BYTE_STRING     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.google.android.gms.internal.p000firebaseperf.zzfb.zzqk     // Catch:{ NoSuchFieldError -> 0x001f }
            com.google.android.gms.internal.firebase-perf.zzfn r3 = com.google.android.gms.internal.p000firebaseperf.zzfn.MESSAGE     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            r2 = 3
            int[] r3 = com.google.android.gms.internal.p000firebaseperf.zzfb.zzqk     // Catch:{ NoSuchFieldError -> 0x002a }
            com.google.android.gms.internal.firebase-perf.zzfn r4 = com.google.android.gms.internal.p000firebaseperf.zzfn.STRING     // Catch:{ NoSuchFieldError -> 0x002a }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            com.google.android.gms.internal.firebase-perf.zzfa[] r3 = com.google.android.gms.internal.p000firebaseperf.zzfa.values()
            int r3 = r3.length
            int[] r3 = new int[r3]
            com.google.android.gms.internal.p000firebaseperf.zzfb.zzqj = r3
            int[] r3 = com.google.android.gms.internal.p000firebaseperf.zzfb.zzqj     // Catch:{ NoSuchFieldError -> 0x003d }
            com.google.android.gms.internal.firebase-perf.zzfa r4 = com.google.android.gms.internal.p000firebaseperf.zzfa.MAP     // Catch:{ NoSuchFieldError -> 0x003d }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x003d }
            r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x003d }
        L_0x003d:
            int[] r0 = com.google.android.gms.internal.p000firebaseperf.zzfb.zzqj     // Catch:{ NoSuchFieldError -> 0x0047 }
            com.google.android.gms.internal.firebase-perf.zzfa r3 = com.google.android.gms.internal.p000firebaseperf.zzfa.VECTOR     // Catch:{ NoSuchFieldError -> 0x0047 }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
            r0[r3] = r1     // Catch:{ NoSuchFieldError -> 0x0047 }
        L_0x0047:
            int[] r0 = com.google.android.gms.internal.p000firebaseperf.zzfb.zzqj     // Catch:{ NoSuchFieldError -> 0x0051 }
            com.google.android.gms.internal.firebase-perf.zzfa r1 = com.google.android.gms.internal.p000firebaseperf.zzfa.SCALAR     // Catch:{ NoSuchFieldError -> 0x0051 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0051 }
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0051 }
        L_0x0051:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzfb.<clinit>():void");
    }
}
