package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzax  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzax extends zzaz<Long> {
    private static zzax zzbb;

    private zzax() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.TraceEventCountForeground";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_rl_trace_event_count_fg";
    }

    public static synchronized zzax zzaz() {
        zzax zzax;
        synchronized (zzax.class) {
            if (zzbb == null) {
                zzbb = new zzax();
            }
            zzax = zzbb;
        }
        return zzax;
    }
}
