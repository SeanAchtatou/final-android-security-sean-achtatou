package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzgp {
    public final zzgm zzacm;
    public final int zzacn;
    public final Object zzaco;

    public zzgp(zzgm zzgm, int i, Object obj) {
        this.zzacm = zzgm;
        this.zzacn = i;
        this.zzaco = obj;
    }
}
