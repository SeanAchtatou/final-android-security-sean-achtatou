package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzvc extends zzvb<zzarl> {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ zzalc zzcdh;
    private final /* synthetic */ zzup zzcdi;

    zzvc(zzup zzup, Context context, zzalc zzalc) {
        this.zzcdi = zzup;
        this.val$context = context;
        this.zzcdh = zzalc;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzop() {
        zzup.zza(this.val$context, "rewarded_video");
        return new zzyl();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzcdb.zza(this.val$context, this.zzcdh);
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zza(ObjectWrapper.wrap(this.val$context), this.zzcdh, 19649000);
    }
}
