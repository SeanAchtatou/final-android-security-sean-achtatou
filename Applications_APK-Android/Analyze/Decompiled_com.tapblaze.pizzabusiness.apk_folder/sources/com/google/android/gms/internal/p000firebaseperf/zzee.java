package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzee  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzee extends zzel {
    private final int zzmy;
    private final int zzmz;

    zzee(byte[] bArr, int i, int i2) {
        super(bArr);
        zzc(i, i + i2, bArr.length);
        this.zzmy = i;
        this.zzmz = i2;
    }

    public final byte zzq(int i) {
        int size = size();
        if (((size - (i + 1)) | i) >= 0) {
            return this.zznb[this.zzmy + i];
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(22);
            sb.append("Index < 0: ");
            sb.append(i);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder(40);
        sb2.append("Index > length: ");
        sb2.append(i);
        sb2.append(", ");
        sb2.append(size);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }

    /* access modifiers changed from: package-private */
    public final byte zzr(int i) {
        return this.zznb[this.zzmy + i];
    }

    public final int size() {
        return this.zzmz;
    }

    /* access modifiers changed from: protected */
    public final int zzgn() {
        return this.zzmy;
    }
}
