package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.List;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzabs extends RelativeLayout {
    private static final float[] zzcvk = {5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f};
    private AnimationDrawable zzcvl;

    public zzabs(Context context, zzabp zzabp, RelativeLayout.LayoutParams layoutParams) {
        super(context);
        Preconditions.checkNotNull(zzabp);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(zzcvk, null, null));
        shapeDrawable.getPaint().setColor(zzabp.getBackgroundColor());
        setLayoutParams(layoutParams);
        zzq.zzks();
        setBackground(shapeDrawable);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        if (!TextUtils.isEmpty(zzabp.getText())) {
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            TextView textView = new TextView(context);
            textView.setLayoutParams(layoutParams3);
            textView.setId(1195835393);
            textView.setTypeface(Typeface.DEFAULT);
            textView.setText(zzabp.getText());
            textView.setTextColor(zzabp.getTextColor());
            textView.setTextSize((float) zzabp.getTextSize());
            zzve.zzou();
            int zza = zzayk.zza(context, 4);
            zzve.zzou();
            textView.setPadding(zza, 0, zzayk.zza(context, 4), 0);
            addView(textView);
            layoutParams2.addRule(1, textView.getId());
        }
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(layoutParams2);
        imageView.setId(1195835394);
        List<zzabu> zzqy = zzabp.zzqy();
        if (zzqy != null && zzqy.size() > 1) {
            this.zzcvl = new AnimationDrawable();
            for (zzabu zzrc : zzqy) {
                try {
                    this.zzcvl.addFrame((Drawable) ObjectWrapper.unwrap(zzrc.zzrc()), zzabp.zzqz());
                } catch (Exception e) {
                    zzavs.zzc("Error while getting drawable.", e);
                }
            }
            zzq.zzks();
            imageView.setBackground(this.zzcvl);
        } else if (zzqy.size() == 1) {
            try {
                imageView.setImageDrawable((Drawable) ObjectWrapper.unwrap(zzqy.get(0).zzrc()));
            } catch (Exception e2) {
                zzavs.zzc("Error while getting drawable.", e2);
            }
        }
        addView(imageView);
    }

    public final void onAttachedToWindow() {
        AnimationDrawable animationDrawable = this.zzcvl;
        if (animationDrawable != null) {
            animationDrawable.start();
        }
        super.onAttachedToWindow();
    }
}
