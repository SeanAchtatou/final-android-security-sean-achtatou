package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzczg implements zzdxg<zzczf> {
    private final zzdxp<zzczs> zzezj;
    private final zzdxp<zzcyt> zzgjb;
    private final zzdxp<zzcxz> zzgjc;

    public zzczg(zzdxp<zzcyt> zzdxp, zzdxp<zzcxz> zzdxp2, zzdxp<zzczs> zzdxp3) {
        this.zzgjb = zzdxp;
        this.zzgjc = zzdxp2;
        this.zzezj = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzczf(this.zzgjb.get(), this.zzgjc.get(), this.zzezj.get());
    }
}
