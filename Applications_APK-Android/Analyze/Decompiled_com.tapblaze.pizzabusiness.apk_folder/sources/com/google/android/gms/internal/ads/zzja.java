package com.google.android.gms.internal.ads;

import android.os.Looper;
import com.google.android.gms.internal.ads.zziz;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzja<T extends zziz> {
    zziy<T> zza(Looper looper, zziv zziv);

    void zza(zziy<T> zziy);
}
