package com.google.android.gms.internal.ads;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdfw<I, O> extends zzdfu<I, O, zzded<? super I, ? extends O>, O> {
    zzdfw(zzdhe<? extends I> zzdhe, zzded<? super I, ? extends O> zzded) {
        super(zzdhe, zzded);
    }

    /* access modifiers changed from: package-private */
    public final void setResult(@NullableDecl O o) {
        set(o);
    }

    /* access modifiers changed from: package-private */
    @NullableDecl
    public final /* synthetic */ Object zzc(Object obj, @NullableDecl Object obj2) throws Exception {
        return ((zzded) obj).apply(obj2);
    }
}
