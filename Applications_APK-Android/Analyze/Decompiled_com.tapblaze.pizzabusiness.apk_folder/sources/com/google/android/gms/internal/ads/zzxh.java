package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzxh extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzxh> CREATOR = new zzxk();
    private final int zzced;

    public zzxh(int i) {
        this.zzced = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 2, this.zzced);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
