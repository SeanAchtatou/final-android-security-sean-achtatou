package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.util.logging.Logger;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzdkh implements zzdis<zzdib> {
    private static final Logger logger = Logger.getLogger(zzdkh.class.getName());

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    static class zza implements zzdib {
        private final zzdiq<zzdib> zzgzq;

        public zza(zzdiq<zzdib> zzdiq) {
            this.zzgzq = zzdiq;
        }
    }

    zzdkh() {
    }

    public final Class<zzdib> zzarz() {
        return zzdib.class;
    }

    public final /* synthetic */ Object zza(zzdiq zzdiq) throws GeneralSecurityException {
        return new zza(zzdiq);
    }
}
