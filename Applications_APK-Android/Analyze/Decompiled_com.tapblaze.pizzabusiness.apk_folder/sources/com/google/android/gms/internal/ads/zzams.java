package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzams implements Runnable {
    private final /* synthetic */ zzamf zzdef;

    zzams(zzamf zzamf) {
        this.zzdef = zzamf;
    }

    public final void run() {
        try {
            this.zzdef.zzdds.onAdLeftApplication();
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }
}
