package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzboh implements zzdxg<String> {
    private final zzbod zzfhi;

    private zzboh(zzbod zzbod) {
        this.zzfhi = zzbod;
    }

    public static zzboh zzg(zzbod zzbod) {
        return new zzboh(zzbod);
    }

    public final /* synthetic */ Object get() {
        return this.zzfhi.zzahg();
    }
}
