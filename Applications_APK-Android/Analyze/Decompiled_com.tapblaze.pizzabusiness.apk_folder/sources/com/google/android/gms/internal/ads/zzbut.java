package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbut implements zzdxg<String> {
    private static final zzbut zzfjh = new zzbut();

    public static zzbut zzaih() {
        return zzfjh;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza("interstitial", "Cannot return null from a non-@Nullable @Provides method");
    }
}
