package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdwv implements Iterator<E> {
    private int pos = 0;
    private final /* synthetic */ zzdww zzhzm;

    zzdwv(zzdww zzdww) {
        this.zzhzm = zzdww;
    }

    public final boolean hasNext() {
        return this.pos < this.zzhzm.zzhzn.size() || this.zzhzm.zzhzo.hasNext();
    }

    public final E next() {
        while (this.pos >= this.zzhzm.zzhzn.size()) {
            this.zzhzm.zzhzn.add(this.zzhzm.zzhzo.next());
        }
        List<E> list = this.zzhzm.zzhzn;
        int i = this.pos;
        this.pos = i + 1;
        return list.get(i);
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
