package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbmj {
    public final List<? extends zzdhe<? extends zzbmd>> zzffu;

    public zzbmj(List<? extends zzdhe<? extends zzbmd>> list) {
        this.zzffu = list;
    }

    public zzbmj(zzbmd zzbmd) {
        this.zzffu = Collections.singletonList(zzdgs.zzaj(zzbmd));
    }

    public static zzcio<zzbmj> zza(zzckr<? extends zzbmd> zzckr) {
        return new zzcin(zzckr, zzbmm.zzdoq);
    }

    public static zzcio<zzbmj> zza(zzcio<? extends zzbmd> zzcio) {
        return new zzcin(zzcio, zzbml.zzdoq);
    }
}
