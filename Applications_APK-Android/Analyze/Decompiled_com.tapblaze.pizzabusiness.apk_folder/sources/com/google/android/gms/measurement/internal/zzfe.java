package com.google.android.gms.measurement.internal;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class zzfe implements Runnable {
    private final /* synthetic */ int zza;
    private final /* synthetic */ String zzb;
    private final /* synthetic */ Object zzc;
    private final /* synthetic */ Object zzd;
    private final /* synthetic */ Object zze;
    private final /* synthetic */ zzfb zzf;

    zzfe(zzfb zzfb, int i, String str, Object obj, Object obj2, Object obj3) {
        this.zzf = zzfb;
        this.zza = i;
        this.zzb = str;
        this.zzc = obj;
        this.zzd = obj2;
        this.zze = obj3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzfb.zza(com.google.android.gms.measurement.internal.zzfb, char):char
     arg types: [com.google.android.gms.measurement.internal.zzfb, int]
     candidates:
      com.google.android.gms.measurement.internal.zzfb.zza(com.google.android.gms.measurement.internal.zzfb, long):long
      com.google.android.gms.measurement.internal.zzfb.zza(boolean, java.lang.Object):java.lang.String
      com.google.android.gms.measurement.internal.zzfb.zza(int, java.lang.String):void
      com.google.android.gms.measurement.internal.zzfb.zza(com.google.android.gms.measurement.internal.zzfb, char):char */
    public final void run() {
        zzfo zzc2 = this.zzf.zzx.zzc();
        if (zzc2.zzz()) {
            if (this.zzf.zza == 0) {
                if (this.zzf.zzt().zzf()) {
                    zzfb zzfb = this.zzf;
                    zzfb.zzu();
                    char unused = zzfb.zza = 'C';
                } else {
                    zzfb zzfb2 = this.zzf;
                    zzfb2.zzu();
                    char unused2 = zzfb2.zza = 'c';
                }
            }
            if (this.zzf.zzb < 0) {
                zzfb zzfb3 = this.zzf;
                long unused3 = zzfb3.zzb = zzfb3.zzt().zze();
            }
            char charAt = "01VDIWEA?".charAt(this.zza);
            char zza2 = this.zzf.zza;
            long zzb2 = this.zzf.zzb;
            String zza3 = zzfb.zza(true, this.zzb, this.zzc, this.zzd, this.zze);
            StringBuilder sb = new StringBuilder(String.valueOf(zza3).length() + 24);
            sb.append("2");
            sb.append(charAt);
            sb.append(zza2);
            sb.append(zzb2);
            sb.append(":");
            sb.append(zza3);
            String sb2 = sb.toString();
            if (sb2.length() > 1024) {
                sb2 = this.zzb.substring(0, 1024);
            }
            zzc2.zzb.zza(sb2, 1);
            return;
        }
        this.zzf.zza(6, "Persisted config not initialized. Not logging error/warn");
    }
}
