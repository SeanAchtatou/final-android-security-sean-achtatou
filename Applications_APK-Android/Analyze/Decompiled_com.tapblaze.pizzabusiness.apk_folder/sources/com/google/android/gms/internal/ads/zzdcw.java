package com.google.android.gms.internal.ads;

import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdcw implements zzdxg<zzdcr> {
    private final zzdxp<zzdcq> zzenf;
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<ScheduledExecutorService> zzfdw;

    private zzdcw(zzdxp<zzdhd> zzdxp, zzdxp<ScheduledExecutorService> zzdxp2, zzdxp<zzdcq> zzdxp3) {
        this.zzfcv = zzdxp;
        this.zzfdw = zzdxp2;
        this.zzenf = zzdxp3;
    }

    public static zzdcw zzr(zzdxp<zzdhd> zzdxp, zzdxp<ScheduledExecutorService> zzdxp2, zzdxp<zzdcq> zzdxp3) {
        return new zzdcw(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzdcr(this.zzfcv.get(), this.zzfdw.get(), this.zzenf.get());
    }
}
