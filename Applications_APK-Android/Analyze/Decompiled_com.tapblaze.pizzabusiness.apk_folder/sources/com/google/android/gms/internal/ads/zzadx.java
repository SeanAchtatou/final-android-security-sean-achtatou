package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzadx extends zzgc implements zzadv {
    zzadx(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.formats.client.IOnUnifiedNativeAdLoadedListener");
    }

    public final void zza(zzaeg zzaeg) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, zzaeg);
        zza(1, obtainAndWriteInterfaceToken);
    }
}
