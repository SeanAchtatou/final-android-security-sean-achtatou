package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbkt implements zzdxg<zzbme> {
    private final zzbkn zzfen;

    public zzbkt(zzbkn zzbkn) {
        this.zzfen = zzbkn;
    }

    public static zzbme zzb(zzbkn zzbkn) {
        return (zzbme) zzdxm.zza(zzbkn.zzagh(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzb(this.zzfen);
    }
}
