package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcjw implements zzdgt<zzbkk> {
    private final /* synthetic */ zzcjr zzfzd;

    zzcjw(zzcjr zzcjr) {
        this.zzfzd = zzcjr;
    }

    public final void zzb(Throwable th) {
        this.zzfzd.zzfik.onAdFailedToLoad(zzcfb.zzd(th));
        zzdad.zzc(th, "DelayedBannerAd.onFailure");
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        ((zzbkk) obj).zzagf();
    }
}
