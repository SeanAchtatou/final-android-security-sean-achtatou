package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzko implements zzjd, zzjm {
    private static final zzji zzanm = new zzkn();
    private static final int zzawr = zzoq.zzbn("qt  ");
    private long zzagj;
    private final zzoj zzant = new zzoj(zzoi.zzbga);
    private final zzoj zzanu = new zzoj(4);
    private int zzapi;
    private int zzapj;
    private zzjf zzapm;
    private final zzoj zzaws = new zzoj(16);
    private final Stack<zzkc> zzawt = new Stack<>();
    private int zzawu;
    private int zzawv;
    private long zzaww;
    private int zzawx;
    private zzoj zzawy;
    private zzkq[] zzawz;
    private boolean zzaxa;

    public final void release() {
    }

    public final boolean zzgh() {
        return true;
    }

    public final boolean zza(zzjg zzjg) throws IOException, InterruptedException {
        return zzkp.zzd(zzjg);
    }

    public final void zza(zzjf zzjf) {
        this.zzapm = zzjf;
    }

    public final void zzc(long j, long j2) {
        this.zzawt.clear();
        this.zzawx = 0;
        this.zzapj = 0;
        this.zzapi = 0;
        if (j == 0) {
            zzgt();
            return;
        }
        zzkq[] zzkqArr = this.zzawz;
        if (zzkqArr != null) {
            for (zzkq zzkq : zzkqArr) {
                zzku zzku = zzkq.zzaxd;
                int zzec = zzku.zzec(j2);
                if (zzec == -1) {
                    zzec = zzku.zzed(j2);
                }
                zzkq.zzavh = zzec;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:150:0x019a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x02af A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0006 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0006 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zza(com.google.android.gms.internal.ads.zzjg r25, com.google.android.gms.internal.ads.zzjj r26) throws java.io.IOException, java.lang.InterruptedException {
        /*
            r24 = this;
            r0 = r24
            r1 = r25
            r2 = r26
        L_0x0006:
            int r3 = r0.zzawu
            r4 = -1
            r5 = 8
            r6 = 1
            if (r3 == 0) goto L_0x019c
            r8 = 262144(0x40000, double:1.295163E-318)
            r10 = 2
            if (r3 == r6) goto L_0x0115
            if (r3 != r10) goto L_0x010f
            r12 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r3 = 0
            r5 = -1
        L_0x001d:
            com.google.android.gms.internal.ads.zzkq[] r14 = r0.zzawz
            int r15 = r14.length
            if (r3 >= r15) goto L_0x003b
            r14 = r14[r3]
            int r15 = r14.zzavh
            com.google.android.gms.internal.ads.zzku r11 = r14.zzaxd
            int r11 = r11.zzavf
            if (r15 == r11) goto L_0x0038
            com.google.android.gms.internal.ads.zzku r11 = r14.zzaxd
            long[] r11 = r11.zzamq
            r14 = r11[r15]
            int r11 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r11 >= 0) goto L_0x0038
            r5 = r3
            r12 = r14
        L_0x0038:
            int r3 = r3 + 1
            goto L_0x001d
        L_0x003b:
            if (r5 != r4) goto L_0x003e
            return r4
        L_0x003e:
            r3 = r14[r5]
            com.google.android.gms.internal.ads.zzjo r4 = r3.zzaxe
            int r5 = r3.zzavh
            com.google.android.gms.internal.ads.zzku r11 = r3.zzaxd
            long[] r11 = r11.zzamq
            r12 = r11[r5]
            com.google.android.gms.internal.ads.zzku r11 = r3.zzaxd
            int[] r11 = r11.zzamp
            r11 = r11[r5]
            com.google.android.gms.internal.ads.zzks r14 = r3.zzaxc
            int r14 = r14.zzaxj
            if (r14 != r6) goto L_0x005b
            r14 = 8
            long r12 = r12 + r14
            int r11 = r11 + -8
        L_0x005b:
            long r14 = r25.getPosition()
            long r14 = r12 - r14
            int r10 = r0.zzapj
            long r6 = (long) r10
            long r14 = r14 + r6
            r6 = 0
            int r10 = (r14 > r6 ? 1 : (r14 == r6 ? 0 : -1))
            if (r10 < 0) goto L_0x010b
            int r6 = (r14 > r8 ? 1 : (r14 == r8 ? 0 : -1))
            if (r6 < 0) goto L_0x0071
            goto L_0x010b
        L_0x0071:
            int r2 = (int) r14
            r1.zzac(r2)
            com.google.android.gms.internal.ads.zzks r2 = r3.zzaxc
            int r2 = r2.zzaqu
            if (r2 == 0) goto L_0x00d2
            com.google.android.gms.internal.ads.zzoj r2 = r0.zzanu
            byte[] r2 = r2.data
            r6 = 0
            r2[r6] = r6
            r7 = 1
            r2[r7] = r6
            r7 = 2
            r2[r7] = r6
            com.google.android.gms.internal.ads.zzks r2 = r3.zzaxc
            int r2 = r2.zzaqu
            com.google.android.gms.internal.ads.zzks r6 = r3.zzaxc
            int r6 = r6.zzaqu
            r7 = 4
            int r6 = 4 - r6
        L_0x0093:
            int r7 = r0.zzapj
            if (r7 >= r11) goto L_0x00e8
            int r7 = r0.zzapi
            if (r7 != 0) goto L_0x00c2
            com.google.android.gms.internal.ads.zzoj r7 = r0.zzanu
            byte[] r7 = r7.data
            r1.readFully(r7, r6, r2)
            com.google.android.gms.internal.ads.zzoj r7 = r0.zzanu
            r8 = 0
            r7.zzbe(r8)
            com.google.android.gms.internal.ads.zzoj r7 = r0.zzanu
            int r7 = r7.zzis()
            r0.zzapi = r7
            com.google.android.gms.internal.ads.zzoj r7 = r0.zzant
            r7.zzbe(r8)
            com.google.android.gms.internal.ads.zzoj r7 = r0.zzant
            r9 = 4
            r4.zza(r7, r9)
            int r7 = r0.zzapj
            int r7 = r7 + r9
            r0.zzapj = r7
            int r11 = r11 + r6
            goto L_0x0093
        L_0x00c2:
            r8 = 0
            int r7 = r4.zza(r1, r7, r8)
            int r8 = r0.zzapj
            int r8 = r8 + r7
            r0.zzapj = r8
            int r8 = r0.zzapi
            int r8 = r8 - r7
            r0.zzapi = r8
            goto L_0x0093
        L_0x00d2:
            int r2 = r0.zzapj
            if (r2 >= r11) goto L_0x00e8
            int r2 = r11 - r2
            r6 = 0
            int r2 = r4.zza(r1, r2, r6)
            int r6 = r0.zzapj
            int r6 = r6 + r2
            r0.zzapj = r6
            int r6 = r0.zzapi
            int r6 = r6 - r2
            r0.zzapi = r6
            goto L_0x00d2
        L_0x00e8:
            r20 = r11
            com.google.android.gms.internal.ads.zzku r1 = r3.zzaxd
            long[] r1 = r1.zzaxq
            r17 = r1[r5]
            com.google.android.gms.internal.ads.zzku r1 = r3.zzaxd
            int[] r1 = r1.zzawq
            r19 = r1[r5]
            r21 = 0
            r22 = 0
            r16 = r4
            r16.zza(r17, r19, r20, r21, r22)
            int r1 = r3.zzavh
            r4 = 1
            int r1 = r1 + r4
            r3.zzavh = r1
            r1 = 0
            r0.zzapj = r1
            r0.zzapi = r1
            return r1
        L_0x010b:
            r4 = 1
            r2.zzamw = r12
            return r4
        L_0x010f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            r1.<init>()
            throw r1
        L_0x0115:
            long r3 = r0.zzaww
            int r6 = r0.zzawx
            long r6 = (long) r6
            long r3 = r3 - r6
            long r6 = r25.getPosition()
            long r6 = r6 + r3
            com.google.android.gms.internal.ads.zzoj r10 = r0.zzawy
            if (r10 == 0) goto L_0x0177
            byte[] r8 = r10.data
            int r9 = r0.zzawx
            int r4 = (int) r3
            r1.readFully(r8, r9, r4)
            int r3 = r0.zzawv
            int r4 = com.google.android.gms.internal.ads.zzjz.zzaqv
            if (r3 != r4) goto L_0x0158
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzawy
            r3.zzbe(r5)
            int r4 = r3.readInt()
            int r5 = com.google.android.gms.internal.ads.zzko.zzawr
            if (r4 != r5) goto L_0x0141
        L_0x013f:
            r3 = 1
            goto L_0x0155
        L_0x0141:
            r4 = 4
            r3.zzbf(r4)
        L_0x0145:
            int r4 = r3.zzin()
            if (r4 <= 0) goto L_0x0154
            int r4 = r3.readInt()
            int r5 = com.google.android.gms.internal.ads.zzko.zzawr
            if (r4 != r5) goto L_0x0145
            goto L_0x013f
        L_0x0154:
            r3 = 0
        L_0x0155:
            r0.zzaxa = r3
            goto L_0x017f
        L_0x0158:
            java.util.Stack<com.google.android.gms.internal.ads.zzkc> r3 = r0.zzawt
            boolean r3 = r3.isEmpty()
            if (r3 != 0) goto L_0x017f
            java.util.Stack<com.google.android.gms.internal.ads.zzkc> r3 = r0.zzawt
            java.lang.Object r3 = r3.peek()
            com.google.android.gms.internal.ads.zzkc r3 = (com.google.android.gms.internal.ads.zzkc) r3
            com.google.android.gms.internal.ads.zzkb r4 = new com.google.android.gms.internal.ads.zzkb
            int r5 = r0.zzawv
            com.google.android.gms.internal.ads.zzoj r8 = r0.zzawy
            r4.<init>(r5, r8)
            java.util.List<com.google.android.gms.internal.ads.zzkb> r3 = r3.zzaun
            r3.add(r4)
            goto L_0x017f
        L_0x0177:
            int r5 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r5 >= 0) goto L_0x0181
            int r4 = (int) r3
            r1.zzac(r4)
        L_0x017f:
            r3 = 0
            goto L_0x0189
        L_0x0181:
            long r8 = r25.getPosition()
            long r8 = r8 + r3
            r2.zzamw = r8
            r3 = 1
        L_0x0189:
            r0.zzeb(r6)
            if (r3 == 0) goto L_0x0196
            int r3 = r0.zzawu
            r4 = 2
            if (r3 == r4) goto L_0x0196
            r23 = 1
            goto L_0x0198
        L_0x0196:
            r23 = 0
        L_0x0198:
            if (r23 == 0) goto L_0x0006
            r3 = 1
            return r3
        L_0x019c:
            r3 = 1
            int r6 = r0.zzawx
            if (r6 != 0) goto L_0x01c6
            com.google.android.gms.internal.ads.zzoj r6 = r0.zzaws
            byte[] r6 = r6.data
            r7 = 0
            boolean r6 = r1.zza(r6, r7, r5, r3)
            if (r6 != 0) goto L_0x01af
            r3 = 0
            goto L_0x02ad
        L_0x01af:
            r0.zzawx = r5
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            r3.zzbe(r7)
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            long r6 = r3.zzip()
            r0.zzaww = r6
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            int r3 = r3.readInt()
            r0.zzawv = r3
        L_0x01c6:
            long r6 = r0.zzaww
            r8 = 1
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 != 0) goto L_0x01e2
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            byte[] r3 = r3.data
            r1.readFully(r3, r5, r5)
            int r3 = r0.zzawx
            int r3 = r3 + r5
            r0.zzawx = r3
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            long r6 = r3.zzit()
            r0.zzaww = r6
        L_0x01e2:
            int r3 = r0.zzawv
            int r6 = com.google.android.gms.internal.ads.zzjz.zzarw
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzjz.zzary
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzjz.zzarz
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzjz.zzasa
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzjz.zzasb
            if (r3 == r6) goto L_0x01ff
            int r6 = com.google.android.gms.internal.ads.zzjz.zzask
            if (r3 != r6) goto L_0x01fd
            goto L_0x01ff
        L_0x01fd:
            r3 = 0
            goto L_0x0200
        L_0x01ff:
            r3 = 1
        L_0x0200:
            if (r3 == 0) goto L_0x022c
            long r5 = r25.getPosition()
            long r7 = r0.zzaww
            long r5 = r5 + r7
            int r3 = r0.zzawx
            long r7 = (long) r3
            long r5 = r5 - r7
            java.util.Stack<com.google.android.gms.internal.ads.zzkc> r3 = r0.zzawt
            com.google.android.gms.internal.ads.zzkc r7 = new com.google.android.gms.internal.ads.zzkc
            int r8 = r0.zzawv
            r7.<init>(r8, r5)
            r3.add(r7)
            long r7 = r0.zzaww
            int r3 = r0.zzawx
            long r9 = (long) r3
            int r3 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r3 != 0) goto L_0x0226
            r0.zzeb(r5)
            goto L_0x0229
        L_0x0226:
            r24.zzgt()
        L_0x0229:
            r3 = 1
            goto L_0x02ad
        L_0x022c:
            int r3 = r0.zzawv
            int r6 = com.google.android.gms.internal.ads.zzjz.zzasm
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzarx
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzasn
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzaso
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzath
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzati
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzatj
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzasl
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzatk
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzatl
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzatm
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzatn
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzato
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzasj
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzaqv
            if (r3 == r6) goto L_0x0271
            int r6 = com.google.android.gms.internal.ads.zzjz.zzatv
            if (r3 != r6) goto L_0x026f
            goto L_0x0271
        L_0x026f:
            r3 = 0
            goto L_0x0272
        L_0x0271:
            r3 = 1
        L_0x0272:
            if (r3 == 0) goto L_0x02a7
            int r3 = r0.zzawx
            if (r3 != r5) goto L_0x027a
            r3 = 1
            goto L_0x027b
        L_0x027a:
            r3 = 0
        L_0x027b:
            com.google.android.gms.internal.ads.zzoc.checkState(r3)
            long r6 = r0.zzaww
            r8 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 > 0) goto L_0x0289
            r3 = 1
            goto L_0x028a
        L_0x0289:
            r3 = 0
        L_0x028a:
            com.google.android.gms.internal.ads.zzoc.checkState(r3)
            com.google.android.gms.internal.ads.zzoj r3 = new com.google.android.gms.internal.ads.zzoj
            long r6 = r0.zzaww
            int r7 = (int) r6
            r3.<init>(r7)
            r0.zzawy = r3
            com.google.android.gms.internal.ads.zzoj r3 = r0.zzaws
            byte[] r3 = r3.data
            com.google.android.gms.internal.ads.zzoj r6 = r0.zzawy
            byte[] r6 = r6.data
            r7 = 0
            java.lang.System.arraycopy(r3, r7, r6, r7, r5)
            r3 = 1
            r0.zzawu = r3
            goto L_0x02ad
        L_0x02a7:
            r3 = 1
            r5 = 0
            r0.zzawy = r5
            r0.zzawu = r3
        L_0x02ad:
            if (r3 != 0) goto L_0x0006
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzko.zza(com.google.android.gms.internal.ads.zzjg, com.google.android.gms.internal.ads.zzjj):int");
    }

    public final long getDurationUs() {
        return this.zzagj;
    }

    public final long zzdz(long j) {
        long j2 = Long.MAX_VALUE;
        for (zzkq zzkq : this.zzawz) {
            zzku zzku = zzkq.zzaxd;
            int zzec = zzku.zzec(j);
            if (zzec == -1) {
                zzec = zzku.zzed(j);
            }
            long j3 = zzku.zzamq[zzec];
            if (j3 < j2) {
                j2 = j3;
            }
        }
        return j2;
    }

    private final void zzgt() {
        this.zzawu = 0;
        this.zzawx = 0;
    }

    private final void zzeb(long j) throws zzhd {
        zzle zzle;
        zzjh zzjh;
        zzks zza;
        while (!this.zzawt.isEmpty() && this.zzawt.peek().zzaum == j) {
            zzkc pop = this.zzawt.pop();
            if (pop.type == zzjz.zzarw) {
                ArrayList arrayList = new ArrayList();
                zzle zzle2 = null;
                zzjh zzjh2 = new zzjh();
                zzkb zzao = pop.zzao(zzjz.zzatv);
                if (!(zzao == null || (zzle2 = zzke.zza(zzao, this.zzaxa)) == null)) {
                    zzjh2.zzb(zzle2);
                }
                long j2 = Long.MAX_VALUE;
                long j3 = -9223372036854775807L;
                int i = 0;
                while (i < pop.zzauo.size()) {
                    zzkc zzkc = pop.zzauo.get(i);
                    if (zzkc.type == zzjz.zzary && (zza = zzke.zza(zzkc, pop.zzao(zzjz.zzarx), -9223372036854775807L, (zziv) null, this.zzaxa)) != null) {
                        zzku zza2 = zzke.zza(zza, zzkc.zzap(zzjz.zzarz).zzap(zzjz.zzasa).zzap(zzjz.zzasb), zzjh2);
                        if (zza2.zzavf != 0) {
                            zzkq zzkq = new zzkq(zza, zza2, this.zzapm.zzc(i, zza.type));
                            zzgw zzp = zza.zzafz.zzp(zza2.zzawo + 30);
                            if (zza.type == 1) {
                                if (zzjh2.zzgk()) {
                                    zzp = zzp.zzb(zzjh2.zzafr, zzjh2.zzafs);
                                }
                                if (zzle2 != null) {
                                    zzp = zzp.zza(zzle2);
                                }
                            }
                            zzkq.zzaxe.zze(zzp);
                            zzle = zzle2;
                            zzjh = zzjh2;
                            j3 = Math.max(j3, zza.zzagj);
                            arrayList.add(zzkq);
                            long j4 = zza2.zzamq[0];
                            if (j4 < j2) {
                                j2 = j4;
                            }
                            i++;
                            zzjh2 = zzjh;
                            zzle2 = zzle;
                        }
                    }
                    zzle = zzle2;
                    zzjh = zzjh2;
                    i++;
                    zzjh2 = zzjh;
                    zzle2 = zzle;
                }
                this.zzagj = j3;
                this.zzawz = (zzkq[]) arrayList.toArray(new zzkq[arrayList.size()]);
                this.zzapm.zzgj();
                this.zzapm.zza(this);
                this.zzawt.clear();
                this.zzawu = 2;
            } else if (!this.zzawt.isEmpty()) {
                this.zzawt.peek().zzauo.add(pop);
            }
        }
        if (this.zzawu != 2) {
            zzgt();
        }
    }
}
