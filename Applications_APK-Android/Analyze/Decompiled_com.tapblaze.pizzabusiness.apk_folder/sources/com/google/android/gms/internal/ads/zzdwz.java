package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
abstract class zzdwz<K, V, V2> implements zzdxg<Map<K, V2>> {
    private final Map<K, zzdxp<V>> zzhzx;

    zzdwz(Map<K, zzdxp<V>> map) {
        this.zzhzx = Collections.unmodifiableMap(map);
    }

    /* access modifiers changed from: package-private */
    public final Map<K, zzdxp<V>> zzbdn() {
        return this.zzhzx;
    }
}
