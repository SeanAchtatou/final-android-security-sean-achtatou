package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zznx;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zznv<T extends zznx> {
    int zza(zznx zznx, long j, long j2, IOException iOException);

    void zza(T t, long j, long j2);

    void zza(zznx zznx, long j, long j2, boolean z);
}
