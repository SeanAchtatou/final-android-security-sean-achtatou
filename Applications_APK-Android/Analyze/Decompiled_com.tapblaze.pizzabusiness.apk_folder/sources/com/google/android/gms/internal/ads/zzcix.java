package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcix implements zzcio<zzbkk> {
    private final Executor zzfci;
    private final zzczu zzfgl;
    private final zzcbn zzfod;
    private final zzblg zzfyj;
    private final zzded<zzczl, zzawt> zzfyk;
    private final Context zzup;

    public zzcix(zzblg zzblg, Context context, Executor executor, zzcbn zzcbn, zzczu zzczu, zzded<zzczl, zzawt> zzded) {
        this.zzup = context;
        this.zzfyj = zzblg;
        this.zzfci = executor;
        this.zzfod = zzcbn;
        this.zzfgl = zzczu;
        this.zzfyk = zzded;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        return (zzczl.zzglo == null || zzczl.zzglo.zzdht == null) ? false : true;
    }

    public final zzdhe<zzbkk> zzb(zzczt zzczt, zzczl zzczl) {
        return zzdgs.zzb(zzdgs.zzaj(null), new zzcja(this, zzczt, zzczl), this.zzfci);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzo(zzbdi zzbdi) {
        zzbdi.zzzu();
        zzbed zzyl = zzbdi.zzyl();
        if (this.zzfgl.zzgmk != null && zzyl != null) {
            zzyl.zzb(this.zzfgl.zzgmk);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zzb(zzczt zzczt, zzczl zzczl, Object obj) throws Exception {
        zzuj zza = zzczy.zza(this.zzup, zzczl.zzglq);
        zzbdi zzc = this.zzfod.zzc(zza);
        zzc.zzba(zzczl.zzdll);
        zzblg zzblg = this.zzfyj;
        zzbmt zzbmt = new zzbmt(zzczt, zzczl, null);
        zzcce zzcce = new zzcce(this.zzup, zzc.getView(), this.zzfyk.apply(zzczl));
        zzc.getClass();
        zzbkj zza2 = zzblg.zza(zzbmt, new zzbkn(zzcce, zzc, zzciz.zzp(zzc), zzczy.zze(zza)));
        zza2.zzadx().zzb(zzc, false);
        zza2.zzadk().zza(new zzcjc(zzc), zzazd.zzdwj);
        zza2.zzadx();
        zzdhe<?> zza3 = zzcbp.zza(zzc, zzczl.zzglo.zzdhr, zzczl.zzglo.zzdht);
        if (zzczl.zzdmf) {
            zzc.getClass();
            zza3.addListener(zzcjb.zzh(zzc), this.zzfci);
        }
        zza3.addListener(new zzcje(this, zzc), this.zzfci);
        return zzdgs.zzb(zza3, new zzcjd(zza2), zzazd.zzdwj);
    }
}
