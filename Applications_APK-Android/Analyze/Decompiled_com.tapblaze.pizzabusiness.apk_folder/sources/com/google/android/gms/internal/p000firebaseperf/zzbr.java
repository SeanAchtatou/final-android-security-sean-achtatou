package com.google.android.gms.internal.p000firebaseperf;

import android.support.v4.media.session.PlaybackStateCompat;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: com.google.android.gms.internal.firebase-perf.zzbr  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzbr extends zzbn {
    zzbr(String str, int i, long j) {
        super(str, 3, PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID, null);
    }
}
