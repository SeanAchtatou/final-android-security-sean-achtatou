package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdax {
    private zzdaw zzgnh = null;
    private zzdaw zzgni = null;
    private zzdaw zzgnj = null;
    private zzdaw zzgnk = null;
    private zzdaw zzgnl = null;
    private zzdaw zzgnm = null;
    private zzdaw zzgnn = null;
    private zzdaw zzgno = null;

    public final void zza(zzdaw zzdaw) {
        this.zzgnk = zzdaw;
    }

    public final void onAdClosed() {
        zzdaw zzdaw = this.zzgnk;
        if (zzdaw != null) {
            zzdaw.execute();
        }
    }
}
