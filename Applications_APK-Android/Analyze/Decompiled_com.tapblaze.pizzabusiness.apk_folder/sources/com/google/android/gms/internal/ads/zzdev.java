package com.google.android.gms.internal.ads;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public abstract class zzdev<E> {
    zzdev() {
    }

    public abstract zzdev<E> zzae(E e);

    public zzdev<E> zze(Iterable<? extends E> iterable) {
        for (Object zzae : iterable) {
            zzae(zzae);
        }
        return this;
    }

    public zzdev<E> zza(Iterator<? extends E> it) {
        while (it.hasNext()) {
            zzae(it.next());
        }
        return this;
    }
}
