package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdlf extends zzdrt<zzdlf, zza> implements zzdtg {
    private static volatile zzdtn<zzdlf> zzdz;
    /* access modifiers changed from: private */
    public static final zzdlf zzhal;
    private int zzhaa;
    private zzdlj zzhaj;
    private zzdmv zzhak;

    private zzdlf() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdlf, zza> implements zzdtg {
        private zza() {
            super(zzdlf.zzhal);
        }

        public final zza zzed(int i) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlf) this.zzhmp).setVersion(i);
            return this;
        }

        public final zza zzb(zzdlj zzdlj) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlf) this.zzhmp).zza(zzdlj);
            return this;
        }

        public final zza zzb(zzdmv zzdmv) {
            if (this.zzhmq) {
                zzbab();
                this.zzhmq = false;
            }
            ((zzdlf) this.zzhmp).zza(zzdmv);
            return this;
        }

        /* synthetic */ zza(zzdle zzdle) {
            this();
        }
    }

    public final int getVersion() {
        return this.zzhaa;
    }

    /* access modifiers changed from: private */
    public final void setVersion(int i) {
        this.zzhaa = i;
    }

    public final zzdlj zzata() {
        zzdlj zzdlj = this.zzhaj;
        return zzdlj == null ? zzdlj.zzatj() : zzdlj;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdlj zzdlj) {
        zzdlj.getClass();
        this.zzhaj = zzdlj;
    }

    public final zzdmv zzatb() {
        zzdmv zzdmv = this.zzhak;
        return zzdmv == null ? zzdmv.zzavb() : zzdmv;
    }

    /* access modifiers changed from: private */
    public final void zza(zzdmv zzdmv) {
        zzdmv.getClass();
        this.zzhak = zzdmv;
    }

    public static zzdlf zzw(zzdqk zzdqk) throws zzdse {
        return (zzdlf) zzdrt.zza(zzhal, zzdqk);
    }

    public static zza zzatc() {
        return (zza) zzhal.zzazt();
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdle.zzdk[i - 1]) {
            case 1:
                return new zzdlf();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzhal, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u000b\u0002\t\u0003\t", new Object[]{"zzhaa", "zzhaj", "zzhak"});
            case 4:
                return zzhal;
            case 5:
                zzdtn<zzdlf> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdlf.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzhal);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzdlf zzdlf = new zzdlf();
        zzhal = zzdlf;
        zzdrt.zza(zzdlf.class, zzdlf);
    }
}
