package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zztp extends zzdvq<zztp> {
    public String zzbzo = null;
    private zzsy.zzb[] zzbzp = new zzsy.zzb[0];
    private zzte zzbzq = null;
    private zzte zzbzr = null;
    private zzte zzbzs = null;

    public zztp() {
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    public final void zza(zzdvo zzdvo) throws IOException {
        String str = this.zzbzo;
        if (str != null) {
            zzdvo.zzf(1, str);
        }
        zzsy.zzb[] zzbArr = this.zzbzp;
        if (zzbArr != null && zzbArr.length > 0) {
            int i = 0;
            while (true) {
                zzsy.zzb[] zzbArr2 = this.zzbzp;
                if (i >= zzbArr2.length) {
                    break;
                }
                zzsy.zzb zzb = zzbArr2[i];
                if (zzb != null) {
                    zzdvo.zze(2, zzb);
                }
                i++;
            }
        }
        super.zza(zzdvo);
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        int zzoi = super.zzoi();
        String str = this.zzbzo;
        if (str != null) {
            zzoi += zzdvo.zzg(1, str);
        }
        zzsy.zzb[] zzbArr = this.zzbzp;
        if (zzbArr != null && zzbArr.length > 0) {
            int i = 0;
            while (true) {
                zzsy.zzb[] zzbArr2 = this.zzbzp;
                if (i >= zzbArr2.length) {
                    break;
                }
                zzsy.zzb zzb = zzbArr2[i];
                if (zzb != null) {
                    zzoi += zzdrb.zzc(2, zzb);
                }
                i++;
            }
        }
        return zzoi;
    }
}
