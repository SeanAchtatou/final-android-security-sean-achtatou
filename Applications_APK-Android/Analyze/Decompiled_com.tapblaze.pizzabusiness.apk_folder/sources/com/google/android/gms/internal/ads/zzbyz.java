package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbyz implements zzdgf {
    private final zzdhe zzfpn;

    zzbyz(zzdhe zzdhe) {
        this.zzfpn = zzdhe;
    }

    public final zzdhe zzf(Object obj) {
        zzdhe zzdhe = this.zzfpn;
        if (obj != null) {
            return zzdhe;
        }
        return zzdgs.zzk(new zzclr("Retrieve required value in native ad response failed.", 0));
    }
}
