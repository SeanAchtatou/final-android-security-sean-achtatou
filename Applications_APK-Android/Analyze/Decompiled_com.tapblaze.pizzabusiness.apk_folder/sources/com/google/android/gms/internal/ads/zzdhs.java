package com.google.android.gms.internal.ads;

import com.ironsource.sdk.constants.Constants;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.RunnableFuture;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdhs<V> extends zzdgm<V> implements RunnableFuture<V> {
    private volatile zzdha<?> zzgxp;

    static <V> zzdhs<V> zze(Callable callable) {
        return new zzdhs<>(callable);
    }

    static <V> zzdhs<V> zza(Runnable runnable, @NullableDecl Object obj) {
        return new zzdhs<>(Executors.callable(runnable, obj));
    }

    private zzdhs(Callable callable) {
        this.zzgxp = new zzdhu(this, callable);
    }

    zzdhs(zzdgd zzdgd) {
        this.zzgxp = new zzdhr(this, zzdgd);
    }

    public final void run() {
        zzdha<?> zzdha = this.zzgxp;
        if (zzdha != null) {
            zzdha.run();
        }
        this.zzgxp = null;
    }

    /* access modifiers changed from: protected */
    public final void afterDone() {
        zzdha<?> zzdha;
        super.afterDone();
        if (wasInterrupted() && (zzdha = this.zzgxp) != null) {
            zzdha.interruptTask();
        }
        this.zzgxp = null;
    }

    /* access modifiers changed from: protected */
    public final String pendingToString() {
        zzdha<?> zzdha = this.zzgxp;
        if (zzdha == null) {
            return super.pendingToString();
        }
        String valueOf = String.valueOf(zzdha);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 7);
        sb.append("task=[");
        sb.append(valueOf);
        sb.append(Constants.RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
