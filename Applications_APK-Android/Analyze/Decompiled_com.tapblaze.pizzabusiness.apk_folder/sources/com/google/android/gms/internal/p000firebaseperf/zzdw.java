package com.google.android.gms.internal.p000firebaseperf;

import java.util.AbstractList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzdw  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
abstract class zzdw<E> extends AbstractList<E> implements zzfm<E> {
    private boolean zzmp = true;

    zzdw() {
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        if (!(obj instanceof RandomAccess)) {
            return super.equals(obj);
        }
        List list = (List) obj;
        int size = size();
        if (size != list.size()) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!get(i).equals(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (i * 31) + get(i2).hashCode();
        }
        return i;
    }

    public boolean add(E e) {
        zzgh();
        return super.add(e);
    }

    public void add(int i, E e) {
        zzgh();
        super.add(i, e);
    }

    public boolean addAll(Collection<? extends E> collection) {
        zzgh();
        return super.addAll(collection);
    }

    public boolean addAll(int i, Collection<? extends E> collection) {
        zzgh();
        return super.addAll(i, collection);
    }

    public void clear() {
        zzgh();
        super.clear();
    }

    public boolean zzgf() {
        return this.zzmp;
    }

    public final void zzgg() {
        this.zzmp = false;
    }

    public E remove(int i) {
        zzgh();
        return super.remove(i);
    }

    public boolean remove(Object obj) {
        zzgh();
        return super.remove(obj);
    }

    public boolean removeAll(Collection<?> collection) {
        zzgh();
        return super.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        zzgh();
        return super.retainAll(collection);
    }

    public E set(int i, E e) {
        zzgh();
        return super.set(i, e);
    }

    /* access modifiers changed from: protected */
    public final void zzgh() {
        if (!this.zzmp) {
            throw new UnsupportedOperationException();
        }
    }
}
