package com.google.android.gms.internal.ads;

import android.os.Trace;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzon {
    public static void beginSection(String str) {
        if (zzoq.SDK_INT >= 18) {
            Trace.beginSection(str);
        }
    }

    public static void endSection() {
        if (zzoq.SDK_INT >= 18) {
            Trace.endSection();
        }
    }
}
