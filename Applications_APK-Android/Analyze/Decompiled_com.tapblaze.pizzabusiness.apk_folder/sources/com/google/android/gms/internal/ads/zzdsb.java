package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzdsb<E> extends List<E>, RandomAccess {
    boolean zzaxp();

    void zzaxq();

    zzdsb<E> zzfd(int i);
}
