package com.google.android.gms.ads.query;

import android.content.Context;
import com.google.android.gms.ads.AdSize;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class BannerQueryDataConfiguration extends QueryDataConfiguration {
    private final AdSize zzdi;

    public BannerQueryDataConfiguration(Context context, String str, AdSize adSize) {
        super(context, str);
        this.zzdi = adSize;
    }

    public AdSize getAdSize() {
        return this.zzdi;
    }
}
