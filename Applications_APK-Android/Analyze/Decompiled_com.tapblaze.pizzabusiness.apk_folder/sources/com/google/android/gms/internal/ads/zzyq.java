package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzyq extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzyq> CREATOR = new zzyp();
    private final int zzabo;
    private final int zzabp;

    public zzyq(int i, int i2) {
        this.zzabo = i;
        this.zzabp = i2;
    }

    public zzyq(RequestConfiguration requestConfiguration) {
        this.zzabo = requestConfiguration.getTagForChildDirectedTreatment();
        this.zzabp = requestConfiguration.getTagForUnderAgeOfConsent();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zzabo);
        SafeParcelWriter.writeInt(parcel, 2, this.zzabp);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
