package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zztg implements zzdsa {
    static final zzdsa zzew = new zztg();

    private zztg() {
    }

    public final boolean zzf(int i) {
        return zzsy.zzh.zza.zzby(i) != null;
    }
}
