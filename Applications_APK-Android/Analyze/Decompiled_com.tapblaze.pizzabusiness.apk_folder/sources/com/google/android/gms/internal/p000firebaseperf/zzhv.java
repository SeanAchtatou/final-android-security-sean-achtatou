package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhv  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhv extends zzht<zzhw, zzhw> {
    zzhv() {
    }

    /* access modifiers changed from: package-private */
    public final void zzf(Object obj) {
        ((zzfc) obj).zzql.zzgg();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ int zzm(Object obj) {
        return ((zzhw) obj).getSerializedSize();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ int zzq(Object obj) {
        return ((zzhw) obj).zzjf();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzg(Object obj, Object obj2) {
        zzhw zzhw = (zzhw) obj;
        zzhw zzhw2 = (zzhw) obj2;
        if (zzhw2.equals(zzhw.zzje())) {
            return zzhw;
        }
        return zzhw.zza(zzhw, zzhw2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzc(Object obj, zzin zzin) throws IOException {
        ((zzhw) obj).zza(zzin);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(Object obj, zzin zzin) throws IOException {
        ((zzhw) obj).zzb(zzin);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzp(Object obj) {
        return ((zzfc) obj).zzql;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzf(Object obj, Object obj2) {
        ((zzfc) obj).zzql = (zzhw) obj2;
    }
}
