package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
abstract class zzdx implements zzeb {
    zzdx() {
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public /* synthetic */ Object next() {
        return Byte.valueOf(zza());
    }
}
