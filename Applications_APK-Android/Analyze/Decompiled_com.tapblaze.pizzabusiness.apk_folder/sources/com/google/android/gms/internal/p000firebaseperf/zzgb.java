package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgb  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzgb implements zzgm {
    zzgb() {
    }

    public final boolean zzb(Class<?> cls) {
        return false;
    }

    public final zzgj zzc(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
