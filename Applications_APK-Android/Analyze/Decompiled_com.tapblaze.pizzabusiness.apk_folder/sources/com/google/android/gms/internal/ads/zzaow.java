package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaow implements zzaoz {
    public final zzdhe<Bundle> zzr(Context context) {
        return zzdgs.zzaj(new Bundle());
    }
}
