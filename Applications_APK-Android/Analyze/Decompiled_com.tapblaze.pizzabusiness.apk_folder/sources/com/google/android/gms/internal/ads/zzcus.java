package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcus implements zzcty<JSONObject> {
    private final String zzghn;

    public zzcus(String str) {
        this.zzghn = str;
    }

    public final /* synthetic */ void zzr(Object obj) {
        try {
            ((JSONObject) obj).put("ms", this.zzghn);
        } catch (JSONException e) {
            zzavs.zza("Failed putting Ad ID.", e);
        }
    }
}
