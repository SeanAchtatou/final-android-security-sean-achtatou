package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zztc implements zzdsa {
    static final zzdsa zzew = new zztc();

    private zztc() {
    }

    public final boolean zzf(int i) {
        return zzsy.zzb.zza.zzbw(i) != null;
    }
}
