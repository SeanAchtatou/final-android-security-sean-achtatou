package com.google.android.gms.internal.drive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class zzlc extends zzla {
    private static final Class<?> zzto = Collections.unmodifiableList(Collections.emptyList()).getClass();

    private zzlc() {
        super();
    }

    /* access modifiers changed from: package-private */
    public final void zza(Object obj, long j) {
        Object obj2;
        List list = (List) zznd.zzo(obj, j);
        if (list instanceof zzkz) {
            obj2 = ((zzkz) list).zzds();
        } else if (!zzto.isAssignableFrom(list.getClass())) {
            if (!(list instanceof zzmc) || !(list instanceof zzkp)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                zzkp zzkp = (zzkp) list;
                if (zzkp.zzbo()) {
                    zzkp.zzbp();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        zznd.zza(obj, j, obj2);
    }

    /* access modifiers changed from: package-private */
    public final <E> void zza(Object obj, Object obj2, long j) {
        List zzky;
        List zzb = zzb(obj2, j);
        int size = zzb.size();
        List zzb2 = zzb(obj, j);
        if (zzb2.isEmpty()) {
            if (zzb2 instanceof zzkz) {
                zzb2 = new zzky(size);
            } else if (!(zzb2 instanceof zzmc) || !(zzb2 instanceof zzkp)) {
                zzb2 = new ArrayList(size);
            } else {
                zzb2 = ((zzkp) zzb2).zzr(size);
            }
            zznd.zza(obj, j, zzb2);
        } else {
            if (zzto.isAssignableFrom(zzb2.getClass())) {
                zzky = new ArrayList(zzb2.size() + size);
                zzky.addAll(zzb2);
                zznd.zza(obj, j, zzky);
            } else if (zzb2 instanceof zzna) {
                zzky = new zzky(zzb2.size() + size);
                zzky.addAll((zzna) zzb2);
                zznd.zza(obj, j, zzky);
            } else if ((zzb2 instanceof zzmc) && (zzb2 instanceof zzkp)) {
                zzkp zzkp = (zzkp) zzb2;
                if (!zzkp.zzbo()) {
                    zzkp zzr = zzkp.zzr(zzb2.size() + size);
                    zznd.zza(obj, j, zzr);
                    zzb2 = zzr;
                }
            }
            zzb2 = zzky;
        }
        int size2 = zzb2.size();
        int size3 = zzb.size();
        if (size2 > 0 && size3 > 0) {
            zzb2.addAll(zzb);
        }
        if (size2 > 0) {
            zzb = zzb2;
        }
        zznd.zza(obj, j, zzb);
    }

    private static <E> List<E> zzb(Object obj, long j) {
        return (List) zznd.zzo(obj, j);
    }
}
