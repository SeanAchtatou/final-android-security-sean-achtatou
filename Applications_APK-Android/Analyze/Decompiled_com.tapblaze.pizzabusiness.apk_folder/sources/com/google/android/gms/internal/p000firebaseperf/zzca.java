package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzca  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzca extends zzfc<zzca, zza> implements zzgn {
    /* access modifiers changed from: private */
    public static final zzca zzii;
    private static volatile zzgv<zzca> zzij;
    private int zzie;
    private String zzif = "";
    private String zzig = "";
    private String zzih = "";

    private zzca() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzca$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzca, zza> implements zzgn {
        private zza() {
            super(zzca.zzii);
        }

        public final zza zzt(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzca) this.zzqq).zzq(str);
            return this;
        }

        public final zza zzu(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzca) this.zzqq).zzr(str);
            return this;
        }

        public final zza zzv(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzca) this.zzqq).zzs(str);
            return this;
        }

        /* synthetic */ zza(zzbz zzbz) {
            this();
        }
    }

    public final boolean hasPackageName() {
        return (this.zzie & 1) != 0;
    }

    /* access modifiers changed from: private */
    public final void zzq(String str) {
        str.getClass();
        this.zzie |= 1;
        this.zzif = str;
    }

    public final boolean hasSdkVersion() {
        return (this.zzie & 2) != 0;
    }

    /* access modifiers changed from: private */
    public final void zzr(String str) {
        str.getClass();
        this.zzie |= 2;
        this.zzig = str;
    }

    /* access modifiers changed from: private */
    public final void zzs(String str) {
        str.getClass();
        this.zzie |= 4;
        this.zzih = str;
    }

    public static zza zzdc() {
        return (zza) zzii.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzbz.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzca();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzii, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001\u0003\b\u0002", new Object[]{"zzie", "zzif", "zzig", "zzih"});
            case 4:
                return zzii;
            case 5:
                zzgv<zzca> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzca.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzii);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzca zzdd() {
        return zzii;
    }

    static {
        zzca zzca = new zzca();
        zzii = zzca;
        zzfc.zza(zzca.class, zzca);
    }
}
