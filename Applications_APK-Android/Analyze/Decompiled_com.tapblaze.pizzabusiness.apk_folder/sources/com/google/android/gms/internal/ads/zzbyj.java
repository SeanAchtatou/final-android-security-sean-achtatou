package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbyj implements zzdxg<zzalr> {
    private final zzbyg zzfon;

    public zzbyj(zzbyg zzbyg) {
        this.zzfon = zzbyg;
    }

    public final /* synthetic */ Object get() {
        return this.zzfon.zzakm();
    }
}
