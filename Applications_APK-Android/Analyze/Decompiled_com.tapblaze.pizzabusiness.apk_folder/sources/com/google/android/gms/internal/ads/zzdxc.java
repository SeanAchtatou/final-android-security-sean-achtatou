package com.google.android.gms.internal.ads;

import com.ironsource.sdk.constants.Constants;
import java.util.LinkedHashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzdxc<K, V, V2> {
    final LinkedHashMap<K, zzdxp<V>> zzhzy;

    zzdxc(int i) {
        this.zzhzy = zzdxb.zzhj(i);
    }

    /* access modifiers changed from: package-private */
    public zzdxc<K, V, V2> zza(K k, zzdxp<V> zzdxp) {
        this.zzhzy.put(zzdxm.zza(k, Constants.ParametersKeys.KEY), zzdxm.zza(zzdxp, "provider"));
        return this;
    }
}
