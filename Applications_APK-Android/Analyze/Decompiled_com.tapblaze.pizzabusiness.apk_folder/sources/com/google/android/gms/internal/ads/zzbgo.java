package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbhq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbgo extends zzbhq.zza {
    public final zzbcv zzacw() {
        return new zzbce();
    }

    public final zzbaj zzacx() {
        return new zzbar();
    }

    public final zzsr zzacy() {
        return new zzsr();
    }

    public final zzatq zzacz() {
        return new zzatk();
    }
}
