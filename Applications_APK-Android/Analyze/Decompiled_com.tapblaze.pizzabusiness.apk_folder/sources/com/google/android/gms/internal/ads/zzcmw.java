package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcmw extends zzary {
    private final /* synthetic */ zzbqj zzgaz;
    private final /* synthetic */ zzboq zzgba;
    private final /* synthetic */ zzbpm zzgbb;
    private final /* synthetic */ zzbte zzgbc;

    zzcmw(zzcms zzcms, zzbqj zzbqj, zzboq zzboq, zzbpm zzbpm, zzbte zzbte) {
        this.zzgaz = zzbqj;
        this.zzgba = zzboq;
        this.zzgbb = zzbpm;
        this.zzgbc = zzbte;
    }

    public final void zzaf(IObjectWrapper iObjectWrapper) {
    }

    public final void zzag(IObjectWrapper iObjectWrapper) {
    }

    public final void zzb(Bundle bundle) {
    }

    public final void zzd(IObjectWrapper iObjectWrapper, int i) {
    }

    public final void zze(IObjectWrapper iObjectWrapper, int i) {
    }

    public final void zzah(IObjectWrapper iObjectWrapper) {
        this.zzgaz.zztf();
    }

    public final void zzaj(IObjectWrapper iObjectWrapper) {
        this.zzgaz.zzte();
    }

    public final void zzak(IObjectWrapper iObjectWrapper) {
        this.zzgba.onAdClicked();
    }

    public final void zzal(IObjectWrapper iObjectWrapper) {
        this.zzgbb.onAdLeftApplication();
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzasd zzasd) {
        this.zzgbc.zza(zzasd);
    }

    public final void zzai(IObjectWrapper iObjectWrapper) {
        this.zzgbc.zzrs();
    }

    public final void zzam(IObjectWrapper iObjectWrapper) throws RemoteException {
        this.zzgbb.onRewardedVideoCompleted();
    }
}
