package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcey implements Runnable {
    private final zzceq zzftj;
    private final zzazl zzftt;

    zzcey(zzceq zzceq, zzazl zzazl) {
        this.zzftj = zzceq;
        this.zzftt = zzazl;
    }

    public final void run() {
        zzazl zzazl = this.zzftt;
        String zzvl = zzq.zzku().zzvf().zzwa().zzvl();
        if (!TextUtils.isEmpty(zzvl)) {
            zzazl.set(zzvl);
        } else {
            zzazl.setException(new Exception());
        }
    }
}
