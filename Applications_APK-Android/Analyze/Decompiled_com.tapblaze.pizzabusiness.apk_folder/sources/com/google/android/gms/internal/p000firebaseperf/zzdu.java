package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzdu  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public abstract class zzdu implements zzgr {
    private boolean zzmp = true;
    private int zzmq = -1;

    /* renamed from: zzgd */
    public final zzgr clone() {
        throw new UnsupportedOperationException("clone() should be implemented by subclasses.");
    }
}
