package com.google.android.gms.internal.ads;

import android.net.Uri;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcjq implements zzdgf {
    private final zzcjn zzfyu;
    private final Uri zzfyv;
    private final zzczt zzfyw;
    private final zzczl zzfyx;

    zzcjq(zzcjn zzcjn, Uri uri, zzczt zzczt, zzczl zzczl) {
        this.zzfyu = zzcjn;
        this.zzfyv = uri;
        this.zzfyw = zzczt;
        this.zzfyx = zzczl;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfyu.zza(this.zzfyv, this.zzfyw, this.zzfyx, obj);
    }
}
