package com.google.android.gms.ads.query;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class RewardedQueryDataConfiguration extends QueryDataConfiguration {
    public RewardedQueryDataConfiguration(Context context, String str) {
        super(context, str);
    }
}
