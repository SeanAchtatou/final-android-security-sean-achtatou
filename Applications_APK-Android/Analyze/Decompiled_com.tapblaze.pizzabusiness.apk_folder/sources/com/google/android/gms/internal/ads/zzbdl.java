package com.google.android.gms.internal.ads;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.core.view.ViewCompat;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzo;
import com.google.android.gms.ads.internal.overlay.zzt;
import com.google.android.gms.ads.internal.zzc;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.Predicate;
import com.google.android.gms.games.Notifications;
import com.google.android.gms.internal.ads.zzso;
import cz.msebera.android.httpclient.HttpHost;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzbdl extends WebViewClient implements zzbev {
    private final Object lock;
    private boolean zzbmb;
    private zzty zzcbt;
    private zzaew zzcwq;
    private zzaey zzcws;
    private zzc zzcxo;
    private zzaoe zzcxp;
    private zzo zzdhq;
    private zzt zzdhu;
    private boolean zzdll;
    protected zzbdi zzeef;
    private final zzsm zzeeg;
    private final HashMap<String, List<zzafn<? super zzbdi>>> zzeeh;
    private zzbeu zzeei;
    private zzbex zzeej;
    private zzbew zzeek;
    private boolean zzeel;
    private boolean zzeem;
    private boolean zzeen;
    private boolean zzeeo;
    private final zzaol zzeep;
    protected zzato zzeeq;
    private boolean zzeer;
    private boolean zzees;
    private int zzeet;
    private View.OnAttachStateChangeListener zzeeu;

    public zzbdl(zzbdi zzbdi, zzsm zzsm, boolean z) {
        this(zzbdi, zzsm, z, new zzaol(zzbdi, zzbdi.zzzv(), new zzyy(zzbdi.getContext())), null);
    }

    private zzbdl(zzbdi zzbdi, zzsm zzsm, boolean z, zzaol zzaol, zzaoe zzaoe) {
        this.zzeeh = new HashMap<>();
        this.lock = new Object();
        this.zzeel = false;
        this.zzeeg = zzsm;
        this.zzeef = zzbdi;
        this.zzbmb = z;
        this.zzeep = zzaol;
        this.zzcxp = null;
    }

    public final void zza(int i, int i2, boolean z) {
        this.zzeep.zzj(i, i2);
        zzaoe zzaoe = this.zzcxp;
        if (zzaoe != null) {
            zzaoe.zza(i, i2, false);
        }
    }

    public final void zza(zzty zzty, zzaew zzaew, zzo zzo, zzaey zzaey, zzt zzt, boolean z, zzafq zzafq, zzc zzc, zzaon zzaon, zzato zzato) {
        if (zzc == null) {
            zzc = new zzc(this.zzeef.getContext(), zzato, null);
        }
        this.zzcxp = new zzaoe(this.zzeef, zzaon);
        this.zzeeq = zzato;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzciz)).booleanValue()) {
            zza("/adMetadata", new zzaet(zzaew));
        }
        zza("/appEvent", new zzaev(zzaey));
        zza("/backButton", zzafa.zzcxd);
        zza("/refresh", zzafa.zzcxe);
        zza("/canOpenURLs", zzafa.zzcwu);
        zza("/canOpenIntents", zzafa.zzcwv);
        zza("/click", zzafa.zzcww);
        zza("/close", zzafa.zzcwx);
        zza("/customClose", zzafa.zzcwy);
        zza("/instrument", zzafa.zzcxh);
        zza("/delayPageLoaded", zzafa.zzcxj);
        zza("/delayPageClosed", zzafa.zzcxk);
        zza("/getLocationInfo", zzafa.zzcxl);
        zza("/httpTrack", zzafa.zzcwz);
        zza("/log", zzafa.zzcxa);
        zza("/mraid", new zzafs(zzc, this.zzcxp, zzaon));
        zza("/mraidLoaded", this.zzeep);
        zza("/open", new zzafr(zzc, this.zzcxp));
        zza("/precache", new zzbcs());
        zza("/touch", zzafa.zzcxc);
        zza("/video", zzafa.zzcxf);
        zza("/videoMeta", zzafa.zzcxg);
        if (zzq.zzlo().zzab(this.zzeef.getContext())) {
            zza("/logScionEvent", new zzafp(this.zzeef.getContext()));
        }
        this.zzcbt = zzty;
        this.zzdhq = zzo;
        this.zzcwq = zzaew;
        this.zzcws = zzaey;
        this.zzdhu = zzt;
        this.zzcxo = zzc;
        this.zzeel = z;
    }

    public final zzc zzaas() {
        return this.zzcxo;
    }

    public final boolean zzaat() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzbmb;
        }
        return z;
    }

    public final boolean zzaau() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzeem;
        }
        return z;
    }

    public final boolean zzaav() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzeen;
        }
        return z;
    }

    public final ViewTreeObserver.OnGlobalLayoutListener zzaaw() {
        synchronized (this.lock) {
        }
        return null;
    }

    public final ViewTreeObserver.OnScrollChangedListener zzaax() {
        synchronized (this.lock) {
        }
        return null;
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        zzro zzaaq = this.zzeef.zzaaq();
        if (zzaaq != null && webView == zzaaq.getWebView()) {
            zzaaq.onPageStarted(webView, str, bitmap);
        }
        super.onPageStarted(webView, str, bitmap);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
        if (r1 == null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        r1.zzsb();
        r0.zzeej = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        zzabd();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0018, code lost:
        r0.zzeer = true;
        r1 = r0.zzeej;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onPageFinished(android.webkit.WebView r1, java.lang.String r2) {
        /*
            r0 = this;
            java.lang.Object r1 = r0.lock
            monitor-enter(r1)
            com.google.android.gms.internal.ads.zzbdi r2 = r0.zzeef     // Catch:{ all -> 0x0029 }
            boolean r2 = r2.isDestroyed()     // Catch:{ all -> 0x0029 }
            if (r2 == 0) goto L_0x0017
            java.lang.String r2 = "Blank page loaded, 1..."
            com.google.android.gms.internal.ads.zzavs.zzed(r2)     // Catch:{ all -> 0x0029 }
            com.google.android.gms.internal.ads.zzbdi r2 = r0.zzeef     // Catch:{ all -> 0x0029 }
            r2.zzaag()     // Catch:{ all -> 0x0029 }
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
            return
        L_0x0017:
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
            r1 = 1
            r0.zzeer = r1
            com.google.android.gms.internal.ads.zzbex r1 = r0.zzeej
            if (r1 == 0) goto L_0x0025
            r1.zzsb()
            r1 = 0
            r0.zzeej = r1
        L_0x0025:
            r0.zzabd()
            return
        L_0x0029:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0029 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbdl.onPageFinished(android.webkit.WebView, java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public final void zza(View view, zzato zzato, int i) {
        if (zzato.zzul() && i > 0) {
            zzato.zzj(view);
            if (zzato.zzul()) {
                zzawb.zzdsr.postDelayed(new zzbdm(this, view, zzato, i), 100);
            }
        }
    }

    private final void zzaay() {
        if (this.zzeeu != null) {
            this.zzeef.getView().removeOnAttachStateChangeListener(this.zzeeu);
        }
    }

    public final void zzaaz() {
        zzato zzato = this.zzeeq;
        if (zzato != null) {
            WebView webView = this.zzeef.getWebView();
            if (ViewCompat.isAttachedToWindow(webView)) {
                zza(webView, zzato, 10);
                return;
            }
            zzaay();
            this.zzeeu = new zzbdp(this, zzato);
            this.zzeef.getView().addOnAttachStateChangeListener(this.zzeeu);
        }
    }

    public final void zzaba() {
        synchronized (this.lock) {
            this.zzeeo = true;
        }
        this.zzeet++;
        zzabd();
    }

    public final void zzabb() {
        this.zzeet--;
        zzabd();
    }

    public final void zzabc() {
        zzsm zzsm = this.zzeeg;
        if (zzsm != null) {
            zzsm.zza(zzso.zza.C0024zza.DELAY_PAGE_LOAD_CANCELLED_AD);
        }
        this.zzees = true;
        zzabd();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcpe)).booleanValue()) {
            this.zzeef.destroy();
        }
    }

    private final void zzabd() {
        if (this.zzeei != null && ((this.zzeer && this.zzeet <= 0) || this.zzees)) {
            this.zzeei.zzak(!this.zzees);
            this.zzeei = null;
        }
        this.zzeef.zzaak();
    }

    public final void zza(zzd zzd) {
        zzo zzo;
        boolean zzaaf = this.zzeef.zzaaf();
        zzty zzty = (!zzaaf || this.zzeef.zzzy().zzabt()) ? this.zzcbt : null;
        if (zzaaf) {
            zzo = null;
        } else {
            zzo = this.zzdhq;
        }
        zza(new AdOverlayInfoParcel(zzd, zzty, zzo, this.zzdhu, this.zzeef.zzyr()));
    }

    public final void zzc(boolean z, int i) {
        zzty zzty = (!this.zzeef.zzaaf() || this.zzeef.zzzy().zzabt()) ? this.zzcbt : null;
        zzo zzo = this.zzdhq;
        zzt zzt = this.zzdhu;
        zzbdi zzbdi = this.zzeef;
        zza(new AdOverlayInfoParcel(zzty, zzo, zzt, zzbdi, z, i, zzbdi.zzyr()));
    }

    public final void zza(boolean z, int i, String str) {
        zzbdo zzbdo;
        boolean zzaaf = this.zzeef.zzaaf();
        zzty zzty = (!zzaaf || this.zzeef.zzzy().zzabt()) ? this.zzcbt : null;
        if (zzaaf) {
            zzbdo = null;
        } else {
            zzbdo = new zzbdo(this.zzeef, this.zzdhq);
        }
        zzaew zzaew = this.zzcwq;
        zzaey zzaey = this.zzcws;
        zzt zzt = this.zzdhu;
        zzbdi zzbdi = this.zzeef;
        zza(new AdOverlayInfoParcel(zzty, zzbdo, zzaew, zzaey, zzt, zzbdi, z, i, str, zzbdi.zzyr()));
    }

    public final void zza(boolean z, int i, String str, String str2) {
        zzbdo zzbdo;
        boolean zzaaf = this.zzeef.zzaaf();
        zzty zzty = (!zzaaf || this.zzeef.zzzy().zzabt()) ? this.zzcbt : null;
        if (zzaaf) {
            zzbdo = null;
        } else {
            zzbdo = new zzbdo(this.zzeef, this.zzdhq);
        }
        zzaew zzaew = this.zzcwq;
        zzaey zzaey = this.zzcws;
        zzt zzt = this.zzdhu;
        zzbdi zzbdi = this.zzeef;
        zza(new AdOverlayInfoParcel(zzty, zzbdo, zzaew, zzaey, zzt, zzbdi, z, i, str, str2, zzbdi.zzyr()));
    }

    private final void zza(AdOverlayInfoParcel adOverlayInfoParcel) {
        zzaoe zzaoe = this.zzcxp;
        boolean zztg = zzaoe != null ? zzaoe.zztg() : false;
        zzq.zzkp();
        zzn.zza(this.zzeef.getContext(), adOverlayInfoParcel, !zztg);
        if (this.zzeeq != null) {
            String str = adOverlayInfoParcel.url;
            if (str == null && adOverlayInfoParcel.zzdhp != null) {
                str = adOverlayInfoParcel.zzdhp.url;
            }
            this.zzeeq.zzdv(str);
        }
    }

    public final void zza(String str, zzafn<? super zzbdi> zzafn) {
        synchronized (this.lock) {
            List list = this.zzeeh.get(str);
            if (list == null) {
                list = new CopyOnWriteArrayList();
                this.zzeeh.put(str, list);
            }
            list.add(zzafn);
        }
    }

    public final void zzb(String str, zzafn<? super zzbdi> zzafn) {
        synchronized (this.lock) {
            List list = this.zzeeh.get(str);
            if (list != null) {
                list.remove(zzafn);
            }
        }
    }

    public final void zza(String str, Predicate<zzafn<? super zzbdi>> predicate) {
        synchronized (this.lock) {
            List<zzafn> list = this.zzeeh.get(str);
            if (list != null) {
                ArrayList arrayList = new ArrayList();
                for (zzafn zzafn : list) {
                    if (predicate.apply(zzafn)) {
                        arrayList.add(zzafn);
                    }
                }
                list.removeAll(arrayList);
            }
        }
    }

    public final void reset() {
        zzato zzato = this.zzeeq;
        if (zzato != null) {
            zzato.zzun();
            this.zzeeq = null;
        }
        zzaay();
        synchronized (this.lock) {
            this.zzeeh.clear();
            this.zzcbt = null;
            this.zzdhq = null;
            this.zzeei = null;
            this.zzeej = null;
            this.zzcwq = null;
            this.zzcws = null;
            this.zzeel = false;
            this.zzbmb = false;
            this.zzeem = false;
            this.zzeeo = false;
            this.zzdhu = null;
            this.zzeek = null;
            if (this.zzcxp != null) {
                this.zzcxp.zzac(true);
                this.zzcxp = null;
            }
        }
    }

    public final void zza(zzbeu zzbeu) {
        this.zzeei = zzbeu;
    }

    public final void zza(zzbex zzbex) {
        this.zzeej = zzbex;
    }

    public final void onLoadResource(WebView webView, String str) {
        String valueOf = String.valueOf(str);
        zzavs.zzed(valueOf.length() != 0 ? "Loading resource: ".concat(valueOf) : new String("Loading resource: "));
        Uri parse = Uri.parse(str);
        if ("gmsg".equalsIgnoreCase(parse.getScheme()) && "mobileads.google.com".equalsIgnoreCase(parse.getHost())) {
            zzh(parse);
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String valueOf = String.valueOf(str);
        zzavs.zzed(valueOf.length() != 0 ? "AdWebView shouldOverrideUrlLoading: ".concat(valueOf) : new String("AdWebView shouldOverrideUrlLoading: "));
        Uri parse = Uri.parse(str);
        if (!"gmsg".equalsIgnoreCase(parse.getScheme()) || !"mobileads.google.com".equalsIgnoreCase(parse.getHost())) {
            if (this.zzeel && webView == this.zzeef.getWebView()) {
                String scheme = parse.getScheme();
                if (HttpHost.DEFAULT_SCHEME_NAME.equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)) {
                    zzty zzty = this.zzcbt;
                    if (zzty != null) {
                        zzty.onAdClicked();
                        zzato zzato = this.zzeeq;
                        if (zzato != null) {
                            zzato.zzdv(str);
                        }
                        this.zzcbt = null;
                    }
                    return super.shouldOverrideUrlLoading(webView, str);
                }
            }
            if (!this.zzeef.getWebView().willNotDraw()) {
                try {
                    zzdq zzaad = this.zzeef.zzaad();
                    if (zzaad != null && zzaad.zzb(parse)) {
                        parse = zzaad.zza(parse, this.zzeef.getContext(), this.zzeef.getView(), this.zzeef.zzyn());
                    }
                } catch (zzdt unused) {
                    String valueOf2 = String.valueOf(str);
                    zzavs.zzez(valueOf2.length() != 0 ? "Unable to append parameter to URL: ".concat(valueOf2) : new String("Unable to append parameter to URL: "));
                }
                zzc zzc = this.zzcxo;
                if (zzc == null || zzc.zzjq()) {
                    zza(new zzd("android.intent.action.VIEW", parse.toString(), null, null, null, null, null));
                } else {
                    this.zzcxo.zzbq(str);
                }
            } else {
                String valueOf3 = String.valueOf(str);
                zzavs.zzez(valueOf3.length() != 0 ? "AdWebView unable to handle URL: ".concat(valueOf3) : new String("AdWebView unable to handle URL: "));
            }
        } else {
            zzh(parse);
        }
        return true;
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        return zzd(str, Collections.emptyMap());
    }

    /* access modifiers changed from: protected */
    public final WebResourceResponse zzd(String str, Map<String, String> map) {
        zzrx zza;
        try {
            String zzb = zzauk.zzb(str, this.zzeef.getContext(), this.zzdll);
            if (!zzb.equals(str)) {
                return zze(zzb, map);
            }
            zzry zzby = zzry.zzby(str);
            if (zzby != null && (zza = zzq.zzkw().zza(zzby)) != null && zza.zzmp()) {
                return new WebResourceResponse("", "", zza.zzmq());
            }
            if (!zzayo.isEnabled() || !zzaax.zzcte.get().booleanValue()) {
                return null;
            }
            return zze(str, map);
        } catch (Exception | NoClassDefFoundError e) {
            zzq.zzku().zza(e, "AdWebViewClient.interceptRequest");
            return zzabe();
        }
    }

    private static WebResourceResponse zzabe() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcip)).booleanValue()) {
            return new WebResourceResponse("", "", new ByteArrayInputStream(new byte[0]));
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzawb.zza(android.content.Context, java.lang.String, boolean, java.net.HttpURLConnection):void
     arg types: [android.content.Context, java.lang.String, int, java.net.HttpURLConnection]
     candidates:
      com.google.android.gms.internal.ads.zzawb.zza(android.view.View, int, int, boolean):android.widget.PopupWindow
      com.google.android.gms.internal.ads.zzawb.zza(android.content.Context, java.lang.String, boolean, java.net.HttpURLConnection):void */
    private final WebResourceResponse zze(String str, Map<String, String> map) throws IOException {
        HttpURLConnection httpURLConnection;
        URL url = new URL(str);
        int i = 0;
        while (true) {
            i++;
            if (i <= 20) {
                URLConnection openConnection = url.openConnection();
                openConnection.setConnectTimeout(10000);
                openConnection.setReadTimeout(10000);
                for (Map.Entry next : map.entrySet()) {
                    openConnection.addRequestProperty((String) next.getKey(), (String) next.getValue());
                }
                if (openConnection instanceof HttpURLConnection) {
                    httpURLConnection = (HttpURLConnection) openConnection;
                    zzq.zzkq().zza(this.zzeef.getContext(), this.zzeef.zzyr().zzbma, false, httpURLConnection);
                    zzayo zzayo = new zzayo();
                    zzayo.zza(httpURLConnection, (byte[]) null);
                    int responseCode = httpURLConnection.getResponseCode();
                    zzayo.zza(httpURLConnection, responseCode);
                    if (responseCode < 300 || responseCode >= 400) {
                        zzq.zzkq();
                    } else {
                        String headerField = httpURLConnection.getHeaderField("Location");
                        if (headerField == null) {
                            throw new IOException("Missing Location header in redirect");
                        } else if (headerField.startsWith("tel:")) {
                            return null;
                        } else {
                            URL url2 = new URL(url, headerField);
                            String protocol = url2.getProtocol();
                            if (protocol == null) {
                                zzavs.zzez("Protocol is null");
                                return zzabe();
                            } else if (protocol.equals(HttpHost.DEFAULT_SCHEME_NAME) || protocol.equals("https")) {
                                String valueOf = String.valueOf(headerField);
                                zzavs.zzea(valueOf.length() != 0 ? "Redirecting to ".concat(valueOf) : new String("Redirecting to "));
                                httpURLConnection.disconnect();
                                url = url2;
                            } else {
                                String valueOf2 = String.valueOf(protocol);
                                zzavs.zzez(valueOf2.length() != 0 ? "Unsupported scheme: ".concat(valueOf2) : new String("Unsupported scheme: "));
                                return zzabe();
                            }
                        }
                    }
                } else {
                    throw new IOException("Invalid protocol.");
                }
            } else {
                StringBuilder sb = new StringBuilder(32);
                sb.append("Too many redirects (20)");
                throw new IOException(sb.toString());
            }
        }
        zzq.zzkq();
        return zzawb.zzd(httpURLConnection);
    }

    public final void zzav(boolean z) {
        this.zzeel = z;
    }

    public final zzato zzabf() {
        return this.zzeeq;
    }

    public final void zztn() {
        synchronized (this.lock) {
            this.zzeel = false;
            this.zzbmb = true;
            zzazd.zzdwi.execute(new zzbdk(this));
        }
    }

    public final void zzba(boolean z) {
        this.zzdll = z;
    }

    public final void zzi(int i, int i2) {
        zzaoe zzaoe = this.zzcxp;
        if (zzaoe != null) {
            zzaoe.zzi(i, i2);
        }
    }

    public boolean shouldOverrideKeyEvent(WebView webView, KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyCode == 79 || keyCode == 222) {
            return true;
        }
        switch (keyCode) {
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
                return true;
            default:
                switch (keyCode) {
                    case 126:
                    case Notifications.NOTIFICATION_TYPES_ALL:
                    case 128:
                    case 129:
                    case 130:
                        return true;
                    default:
                        return false;
                }
        }
    }

    public final void zzh(Uri uri) {
        String path = uri.getPath();
        List<zzafn> list = this.zzeeh.get(path);
        if (list != null) {
            zzq.zzkq();
            Map<String, String> zzi = zzawb.zzi(uri);
            if (zzavs.isLoggable(2)) {
                String valueOf = String.valueOf(path);
                zzavs.zzed(valueOf.length() != 0 ? "Received GMSG: ".concat(valueOf) : new String("Received GMSG: "));
                for (String next : zzi.keySet()) {
                    String str = zzi.get(next);
                    StringBuilder sb = new StringBuilder(String.valueOf(next).length() + 4 + String.valueOf(str).length());
                    sb.append("  ");
                    sb.append(next);
                    sb.append(": ");
                    sb.append(str);
                    zzavs.zzed(sb.toString());
                }
            }
            for (zzafn zza : list) {
                zza.zza(this.zzeef, zzi);
            }
            return;
        }
        String valueOf2 = String.valueOf(uri);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 32);
        sb2.append("No GMSG handler found for GMSG: ");
        sb2.append(valueOf2);
        zzavs.zzed(sb2.toString());
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcre)).booleanValue() && zzq.zzku().zzuz() != null) {
            zzazd.zzdwe.execute(new zzbdn(path));
        }
    }

    public final void zzbb(boolean z) {
        synchronized (this.lock) {
            this.zzeem = true;
        }
    }

    public final void zzbc(boolean z) {
        synchronized (this.lock) {
            this.zzeen = z;
        }
    }

    public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
        return this.zzeef.zzb(renderProcessGoneDetail.didCrash(), renderProcessGoneDetail.rendererPriorityAtExit());
    }
}
