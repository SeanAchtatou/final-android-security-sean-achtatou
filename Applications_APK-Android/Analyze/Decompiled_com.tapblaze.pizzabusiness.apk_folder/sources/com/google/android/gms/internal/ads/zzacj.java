package com.google.android.gms.internal.ads;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzacj extends NativeAd.Image {
    private final int height;
    private final Uri uri;
    private final int width;
    private final double zzcvn;
    private final zzaci zzcvs;
    private final Drawable zzcvt;

    public zzacj(zzaci zzaci) {
        Drawable drawable;
        int i;
        this.zzcvs = zzaci;
        Uri uri2 = null;
        try {
            IObjectWrapper zzrc = this.zzcvs.zzrc();
            if (zzrc != null) {
                drawable = (Drawable) ObjectWrapper.unwrap(zzrc);
                this.zzcvt = drawable;
                uri2 = this.zzcvs.getUri();
                this.uri = uri2;
                double d = 1.0d;
                d = this.zzcvs.getScale();
                this.zzcvn = d;
                int i2 = -1;
                i = this.zzcvs.getWidth();
                this.width = i;
                i2 = this.zzcvs.getHeight();
                this.height = i2;
            }
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
        drawable = null;
        this.zzcvt = drawable;
        try {
            uri2 = this.zzcvs.getUri();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
        this.uri = uri2;
        double d2 = 1.0d;
        try {
            d2 = this.zzcvs.getScale();
        } catch (RemoteException e3) {
            zzayu.zzc("", e3);
        }
        this.zzcvn = d2;
        int i22 = -1;
        try {
            i = this.zzcvs.getWidth();
        } catch (RemoteException e4) {
            zzayu.zzc("", e4);
            i = -1;
        }
        this.width = i;
        try {
            i22 = this.zzcvs.getHeight();
        } catch (RemoteException e5) {
            zzayu.zzc("", e5);
        }
        this.height = i22;
    }

    public final Drawable getDrawable() {
        return this.zzcvt;
    }

    public final Uri getUri() {
        return this.uri;
    }

    public final double getScale() {
        return this.zzcvn;
    }

    public final int getWidth() {
        return this.width;
    }

    public final int getHeight() {
        return this.height;
    }
}
