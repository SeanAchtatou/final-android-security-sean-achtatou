package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Predicate;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaij implements Predicate {
    private final zzafn zzczb;

    zzaij(zzafn zzafn) {
        this.zzczb = zzafn;
    }

    public final boolean apply(Object obj) {
        zzafn zzafn = (zzafn) obj;
        return (zzafn instanceof zzaiq) && ((zzaiq) zzafn).zzczh.equals(this.zzczb);
    }
}
