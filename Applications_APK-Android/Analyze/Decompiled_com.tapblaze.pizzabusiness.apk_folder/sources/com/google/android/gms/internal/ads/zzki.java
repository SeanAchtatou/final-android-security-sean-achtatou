package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzki implements zzkg {
    private final zzoj zzaul;
    private final int zzavf = this.zzaul.zzis();
    private final int zzavj = this.zzaul.zzis();

    public zzki(zzkb zzkb) {
        this.zzaul = zzkb.zzaul;
        this.zzaul.zzbe(12);
    }

    public final int zzgq() {
        return this.zzavf;
    }

    public final int zzgr() {
        int i = this.zzavj;
        return i == 0 ? this.zzaul.zzis() : i;
    }

    public final boolean zzgs() {
        return this.zzavj != 0;
    }
}
