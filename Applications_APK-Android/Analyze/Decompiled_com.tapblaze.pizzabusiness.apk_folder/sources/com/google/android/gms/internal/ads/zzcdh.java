package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzso;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcdh {
    public final zzso.zza.C0024zza zzfsj;
    public final zzso.zza.C0024zza zzfsk;
    public final zzso.zza.C0024zza zzfsl;

    public zzcdh(zzso.zza.C0024zza zza, zzso.zza.C0024zza zza2, zzso.zza.C0024zza zza3) {
        this.zzfsj = zza;
        this.zzfsl = zza3;
        this.zzfsk = zza2;
    }
}
