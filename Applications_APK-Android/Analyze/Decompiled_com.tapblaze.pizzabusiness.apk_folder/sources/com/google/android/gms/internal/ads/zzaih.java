package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzq;
import java.util.Map;
import javax.annotation.ParametersAreNonnullByDefault;
import org.json.JSONObject;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaih implements zzaia, zzaif {
    /* access modifiers changed from: private */
    public final zzbdi zzcza;
    private final Context zzup;

    public zzaih(Context context, zzazb zzazb, zzdq zzdq, zza zza) throws zzbdv {
        Context context2 = context;
        this.zzup = context2;
        zzq.zzkr();
        this.zzcza = zzbdr.zza(context2, zzbey.zzabq(), "", false, false, zzdq, zzazb, null, null, null, zzsm.zzmt(), null, false);
        this.zzcza.getView().setWillNotDraw(true);
    }

    public final void zza(String str, Map map) {
        zzahz.zza(this, str, map);
    }

    public final void zza(String str, JSONObject jSONObject) {
        zzahz.zza(this, str, jSONObject);
    }

    public final void zzb(String str, JSONObject jSONObject) {
        zzahz.zzb(this, str, jSONObject);
    }

    public final void zzj(String str, String str2) {
        zzahz.zza(this, str, str2);
    }

    private static void runOnUiThread(Runnable runnable) {
        zzve.zzou();
        if (zzayk.zzxe()) {
            runnable.run();
        } else {
            zzawb.zzdsr.post(runnable);
        }
    }

    public final void zzcy(String str) {
        runOnUiThread(new zzaik(this, str));
    }

    public final void zzcv(String str) {
        runOnUiThread(new zzail(this, String.format("<!DOCTYPE html><html><head><script src=\"%s\"></script></head><body></body></html>", str)));
    }

    public final void zzcw(String str) {
        runOnUiThread(new zzaio(this, str));
    }

    public final void zzcx(String str) {
        runOnUiThread(new zzain(this, str));
    }

    public final void zza(String str, zzafn<? super zzajq> zzafn) {
        this.zzcza.zza(str, new zzaiq(this, zzafn));
    }

    public final void zzb(String str, zzafn<? super zzajq> zzafn) {
        this.zzcza.zza(str, new zzaij(zzafn));
    }

    public final void zza(zzaii zzaii) {
        zzbev zzaaa = this.zzcza.zzaaa();
        zzaii.getClass();
        zzaaa.zza(zzaim.zzb(zzaii));
    }

    public final zzajp zzrz() {
        return new zzajs(this);
    }

    public final void destroy() {
        this.zzcza.destroy();
    }

    public final boolean isDestroyed() {
        return this.zzcza.isDestroyed();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzdd(String str) {
        this.zzcza.zzcy(str);
    }
}
