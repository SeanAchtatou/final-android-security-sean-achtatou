package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import java.util.logging.Logger;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzdjb implements zzdis<zzdhx> {
    private static final Logger logger = Logger.getLogger(zzdjb.class.getName());

    zzdjb() {
    }

    public final Class<zzdhx> zzarz() {
        return zzdhx.class;
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    static class zza implements zzdhx {
        private final zzdiq<zzdhx> zzgzb;

        private zza(zzdiq<zzdhx> zzdiq) {
            this.zzgzb = zzdiq;
        }

        public final byte[] zzc(byte[] bArr, byte[] bArr2) throws GeneralSecurityException {
            return zzdoi.zza(this.zzgzb.zzasm().zzasl(), this.zzgzb.zzasm().zzasi().zzc(bArr, bArr2));
        }
    }

    public final /* synthetic */ Object zza(zzdiq zzdiq) throws GeneralSecurityException {
        return new zza(zzdiq);
    }
}
