package com.google.android.gms.internal.ads;

import com.ironsource.sdk.constants.Constants;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzdwq implements zzbi, Closeable, Iterator<zzbf> {
    private static zzdwy zzcr = zzdwy.zzn(zzdwq.class);
    private static final zzbf zzhzh = new zzdwp("eof ");
    long zzaum = 0;
    long zzbcu = 0;
    protected zzdws zzhyv;
    protected zzbe zzhzi;
    private zzbf zzhzj = null;
    long zzhzk = 0;
    private List<zzbf> zzhzl = new ArrayList();

    public final List<zzbf> zzbdl() {
        if (this.zzhyv == null || this.zzhzj == zzhzh) {
            return this.zzhzl;
        }
        return new zzdww(this.zzhzl, this);
    }

    public void zza(zzdws zzdws, long j, zzbe zzbe) throws IOException {
        this.zzhyv = zzdws;
        long position = zzdws.position();
        this.zzbcu = position;
        this.zzhzk = position;
        zzdws.zzfc(zzdws.position() + j);
        this.zzaum = zzdws.position();
        this.zzhzi = zzbe;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public boolean hasNext() {
        zzbf zzbf = this.zzhzj;
        if (zzbf == zzhzh) {
            return false;
        }
        if (zzbf != null) {
            return true;
        }
        try {
            this.zzhzj = (zzbf) next();
            return true;
        } catch (NoSuchElementException unused) {
            this.zzhzj = zzhzh;
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: zzbdm */
    public final zzbf next() {
        zzbf zza;
        zzbf zzbf = this.zzhzj;
        if (zzbf == null || zzbf == zzhzh) {
            zzdws zzdws = this.zzhyv;
            if (zzdws == null || this.zzhzk >= this.zzaum) {
                this.zzhzj = zzhzh;
                throw new NoSuchElementException();
            }
            try {
                synchronized (zzdws) {
                    this.zzhyv.zzfc(this.zzhzk);
                    zza = this.zzhzi.zza(this.zzhyv, this);
                    this.zzhzk = this.zzhyv.position();
                }
                return zza;
            } catch (EOFException unused) {
                throw new NoSuchElementException();
            } catch (IOException unused2) {
                throw new NoSuchElementException();
            }
        } else {
            this.zzhzj = null;
            return zzbf;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(Constants.RequestParameters.LEFT_BRACKETS);
        for (int i = 0; i < this.zzhzl.size(); i++) {
            if (i > 0) {
                sb.append(";");
            }
            sb.append(this.zzhzl.get(i).toString());
        }
        sb.append(Constants.RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public void close() throws IOException {
        this.zzhyv.close();
    }
}
