package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;
import java.io.IOException;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdrk extends zzdri<zzdrt.zzc> {
    zzdrk() {
    }

    /* access modifiers changed from: package-private */
    public final boolean zzj(zzdte zzdte) {
        return zzdte instanceof zzdrt.zzd;
    }

    /* access modifiers changed from: package-private */
    public final zzdrm<zzdrt.zzc> zzal(Object obj) {
        return ((zzdrt.zzd) obj).zzhmr;
    }

    /* access modifiers changed from: package-private */
    public final zzdrm<zzdrt.zzc> zzam(Object obj) {
        return ((zzdrt.zzd) obj).zzbag();
    }

    /* access modifiers changed from: package-private */
    public final void zzan(Object obj) {
        zzal(obj).zzaxq();
    }

    /* access modifiers changed from: package-private */
    public final <UT, UB> UB zza(zzdtu zzdtu, Object obj, zzdrg zzdrg, zzdrm<zzdrt.zzc> zzdrm, UB ub, zzdus<UT, UB> zzdus) throws IOException {
        zzdrt.zzf zzf = (zzdrt.zzf) obj;
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final int zza(Map.Entry<?, ?> entry) {
        zzdrt.zzc zzc = (zzdrt.zzc) entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdvl zzdvl, Map.Entry<?, ?> entry) throws IOException {
        zzdrt.zzc zzc = (zzdrt.zzc) entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final Object zza(zzdrg zzdrg, zzdte zzdte, int i) {
        return zzdrg.zza(zzdte, i);
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdtu zzdtu, Object obj, zzdrg zzdrg, zzdrm<zzdrt.zzc> zzdrm) throws IOException {
        zzdrt.zzf zzf = (zzdrt.zzf) obj;
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzdqk zzdqk, Object obj, zzdrg zzdrg, zzdrm<zzdrt.zzc> zzdrm) throws IOException {
        zzdrt.zzf zzf = (zzdrt.zzf) obj;
        throw new NoSuchMethodError();
    }
}
