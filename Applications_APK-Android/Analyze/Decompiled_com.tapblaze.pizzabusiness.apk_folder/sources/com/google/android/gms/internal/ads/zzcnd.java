package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzcnd extends zzalh {
    private final zzbpw zzffq;
    private final zzbpm zzfgg;
    private final zzbqj zzfjf;
    private final zzbpd zzfkd;
    private final zzboq zzfke;
    private final zzbra zzfqq;
    private final zzbtj zzgbj;

    public zzcnd(zzboq zzboq, zzbpd zzbpd, zzbpm zzbpm, zzbpw zzbpw, zzbra zzbra, zzbqj zzbqj, zzbtj zzbtj) {
        this.zzfke = zzboq;
        this.zzfkd = zzbpd;
        this.zzfgg = zzbpm;
        this.zzffq = zzbpw;
        this.zzfqq = zzbra;
        this.zzfjf = zzbqj;
        this.zzgbj = zzbtj;
    }

    public final void onAdFailedToLoad(int i) {
    }

    public final void zza(zzade zzade, String str) {
    }

    public final void zza(zzalj zzalj) {
    }

    public void zza(zzasf zzasf) throws RemoteException {
    }

    public void zzb(Bundle bundle) throws RemoteException {
    }

    public void zzb(zzasd zzasd) {
    }

    public void zzco(int i) throws RemoteException {
    }

    public final void zzdj(String str) {
    }

    public void zzst() throws RemoteException {
    }

    public final void onAdClicked() {
        this.zzfke.onAdClicked();
    }

    public final void onAdClosed() {
        this.zzfjf.zzte();
    }

    public final void onAdLeftApplication() {
        this.zzfgg.onAdLeftApplication();
    }

    public final void onAdOpened() {
        this.zzfjf.zztf();
    }

    public final void onAppEvent(String str, String str2) {
        this.zzfqq.onAppEvent(str, str2);
    }

    public final void onAdLoaded() {
        this.zzffq.onAdLoaded();
    }

    public final void onAdImpression() {
        this.zzfkd.onAdImpression();
    }

    public final void onVideoPause() {
        this.zzgbj.onVideoPause();
    }

    public void zzss() {
        this.zzgbj.onVideoStart();
    }

    public void onVideoEnd() {
        this.zzgbj.onVideoEnd();
    }

    public final void onVideoPlay() throws RemoteException {
        this.zzgbj.onVideoPlay();
    }
}
