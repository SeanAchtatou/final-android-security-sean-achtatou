package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdro;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzdro<T extends zzdro<T>> extends Comparable<T> {
    zzdtd zza(zzdtd zzdtd, zzdte zzdte);

    zzdtj zza(zzdtj zzdtj, zzdtj zzdtj2);

    int zzae();

    zzdvf zzazo();

    zzdvm zzazp();

    boolean zzazq();

    boolean zzazr();
}
