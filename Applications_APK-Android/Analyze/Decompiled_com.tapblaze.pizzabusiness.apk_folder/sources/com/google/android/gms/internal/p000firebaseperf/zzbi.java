package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbi  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzbi {
    private static zzbi zzfk;
    private boolean zzdk;
    private zzbh zzfl;

    public static synchronized zzbi zzco() {
        zzbi zzbi;
        synchronized (zzbi.class) {
            if (zzfk == null) {
                zzfk = new zzbi();
            }
            zzbi = zzfk;
        }
        return zzbi;
    }

    private zzbi(zzbh zzbh) {
        this.zzdk = false;
        this.zzfl = zzbh.zzcn();
    }

    private zzbi() {
        this(null);
    }

    public final void zze(boolean z) {
        this.zzdk = z;
    }

    public final void zzm(String str) {
        if (this.zzdk) {
            zzbh.zzm(str);
        }
    }
}
