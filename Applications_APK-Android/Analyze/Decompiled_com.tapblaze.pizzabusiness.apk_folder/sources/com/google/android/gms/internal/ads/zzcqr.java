package com.google.android.gms.internal.ads;

import android.os.Bundle;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcqr implements zzcub<zzcty<Bundle>> {
    private final Executor executor;
    private final zzave zzbmm;

    zzcqr(Executor executor2, zzave zzave) {
        this.executor = executor2;
        this.zzbmm = zzave;
    }

    public final zzdhe<zzcty<Bundle>> zzanc() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzclf)).booleanValue()) {
            return zzdgs.zzaj(null);
        }
        return zzdgs.zzb(this.zzbmm.zzvg(), zzcqq.zzdoq, this.executor);
    }
}
