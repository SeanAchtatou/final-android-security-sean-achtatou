package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzke;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class zzdh implements zzes {
    static final zzes zza = new zzdh();

    private zzdh() {
    }

    public final Object zza() {
        return Boolean.valueOf(zzke.zzb());
    }
}
