package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;
import com.google.android.gms.internal.p000firebaseperf.zziq;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzcr  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzcr extends zzfc<zzcr, zza> implements zzgn {
    private static volatile zzgv<zzcr> zzij;
    /* access modifiers changed from: private */
    public static final zzcr zzkc;
    private int zzie;
    private String zzig = "";
    private String zzjz = "";
    private String zzka = "";
    private zziq.zza zzkb;

    private zzcr() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzcr$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzcr, zza> implements zzgn {
        private zza() {
            super(zzcr.zzkc);
        }

        /* synthetic */ zza(zzcs zzcs) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzcs.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzcr();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzkc, "\u0001\u0004\u0000\u0001\u0002\u0005\u0004\u0000\u0000\u0000\u0002\b\u0000\u0003\b\u0001\u0004\b\u0002\u0005\t\u0003", new Object[]{"zzie", "zzig", "zzjz", "zzka", "zzkb"});
            case 4:
                return zzkc;
            case 5:
                zzgv<zzcr> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzcr.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzkc);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzcr zzcr = new zzcr();
        zzkc = zzcr;
        zzfc.zza(zzcr.class, zzcr);
    }
}
