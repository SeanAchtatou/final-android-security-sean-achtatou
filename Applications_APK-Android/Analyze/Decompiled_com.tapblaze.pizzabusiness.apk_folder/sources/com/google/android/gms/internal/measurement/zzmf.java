package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzmf implements zzdb<zzme> {
    private static zzmf zza = new zzmf();
    private final zzdb<zzme> zzb;

    public static boolean zzb() {
        return ((zzme) zza.zza()).zza();
    }

    private zzmf(zzdb<zzme> zzdb) {
        this.zzb = zzda.zza((zzdb) zzdb);
    }

    public zzmf() {
        this(zzda.zza(new zzmh()));
    }

    public final /* synthetic */ Object zza() {
        return this.zzb.zza();
    }
}
