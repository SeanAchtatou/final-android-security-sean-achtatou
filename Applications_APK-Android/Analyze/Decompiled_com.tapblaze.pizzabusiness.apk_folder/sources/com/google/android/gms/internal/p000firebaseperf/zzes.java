package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzez;
import java.io.IOException;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzes  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
abstract class zzes<T extends zzez<T>> {
    zzes() {
    }

    /* access modifiers changed from: package-private */
    public abstract void zza(zzin zzin, Map.Entry<?, ?> entry) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract int zzb(Map.Entry<?, ?> entry);

    /* access modifiers changed from: package-private */
    public abstract zzex<T> zzd(Object obj);

    /* access modifiers changed from: package-private */
    public abstract zzex<T> zze(Object obj);

    /* access modifiers changed from: package-private */
    public abstract boolean zze(zzgl zzgl);

    /* access modifiers changed from: package-private */
    public abstract void zzf(Object obj);
}
