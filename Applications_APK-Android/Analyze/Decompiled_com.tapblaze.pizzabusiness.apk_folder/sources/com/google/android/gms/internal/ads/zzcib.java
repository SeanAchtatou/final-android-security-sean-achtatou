package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcib implements zzdcx {
    private final zzchz zzfxk;

    zzcib(zzchz zzchz) {
        this.zzfxk = zzchz;
    }

    public final void zza(zzdco zzdco, String str) {
    }

    public final void zzb(zzdco zzdco, String str) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue() && zzdco.RENDERER == zzdco) {
            this.zzfxk.zzfe(zzq.zzkx().elapsedRealtime());
        }
    }

    public final void zza(zzdco zzdco, String str, Throwable th) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue() && zzdco.RENDERER == zzdco && this.zzfxk.zzalz() != 0) {
            this.zzfxk.zzer(zzq.zzkx().elapsedRealtime() - this.zzfxk.zzalz());
        }
    }

    public final void zzc(zzdco zzdco, String str) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcqf)).booleanValue() && zzdco.RENDERER == zzdco && this.zzfxk.zzalz() != 0) {
            this.zzfxk.zzer(zzq.zzkx().elapsedRealtime() - this.zzfxk.zzalz());
        }
    }
}
