package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzdt;
import com.google.android.gms.internal.p000firebaseperf.zzdv;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzdv  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public abstract class zzdv<MessageType extends zzdt<MessageType, BuilderType>, BuilderType extends zzdv<MessageType, BuilderType>> implements zzgo {
    /* access modifiers changed from: protected */
    public abstract BuilderType zza(zzdt zzdt);

    /* renamed from: zzge */
    public abstract BuilderType clone();

    public final /* synthetic */ zzgo zza(zzgl zzgl) {
        if (zzhj().getClass().isInstance(zzgl)) {
            return zza((zzdt) zzgl);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
