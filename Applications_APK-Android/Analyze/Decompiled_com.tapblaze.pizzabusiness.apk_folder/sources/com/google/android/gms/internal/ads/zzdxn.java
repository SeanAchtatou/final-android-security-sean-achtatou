package com.google.android.gms.internal.ads;

import java.util.Collection;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdxn<T> {
    private final List<zzdxp<T>> zziaf;
    private final List<zzdxp<Collection<T>>> zziag;

    private zzdxn(int i, int i2) {
        this.zziaf = zzdxb.zzhh(i);
        this.zziag = zzdxb.zzhh(i2);
    }

    public final zzdxn<T> zzap(zzdxp<? extends T> zzdxp) {
        this.zziaf.add(zzdxp);
        return this;
    }

    public final zzdxn<T> zzaq(zzdxp<? extends Collection<? extends T>> zzdxp) {
        this.zziag.add(zzdxp);
        return this;
    }

    public final zzdxl<T> zzbdp() {
        return new zzdxl<>(this.zziaf, this.zziag);
    }
}
