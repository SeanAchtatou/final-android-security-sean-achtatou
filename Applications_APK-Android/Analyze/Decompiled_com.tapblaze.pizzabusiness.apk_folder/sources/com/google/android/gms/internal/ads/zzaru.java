package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaru extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaru> CREATOR = new zzarx();
    public final String zzbqz;
    public final zzug zzdio;

    public zzaru(zzug zzug, String str) {
        this.zzdio = zzug;
        this.zzbqz = str;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzdio, i, false);
        SafeParcelWriter.writeString(parcel, 3, this.zzbqz, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
