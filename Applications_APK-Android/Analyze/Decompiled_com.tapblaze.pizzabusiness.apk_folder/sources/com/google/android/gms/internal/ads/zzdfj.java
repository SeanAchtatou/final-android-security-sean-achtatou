package com.google.android.gms.internal.ads;

import java.util.AbstractMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzdfj extends zzdeu<Map.Entry<K, V>> {
    private final /* synthetic */ zzdfg zzgva;

    zzdfj(zzdfg zzdfg) {
        this.zzgva = zzdfg;
    }

    public final boolean zzarc() {
        return true;
    }

    public final int size() {
        return this.zzgva.size;
    }

    public final /* synthetic */ Object get(int i) {
        zzdei.zzs(i, this.zzgva.size);
        Object[] zzb = this.zzgva.zzguw;
        int i2 = i * 2;
        zzdfg zzdfg = this.zzgva;
        Object obj = zzb[i2];
        Object[] zzb2 = zzdfg.zzguw;
        zzdfg zzdfg2 = this.zzgva;
        return new AbstractMap.SimpleImmutableEntry(obj, zzb2[i2 + 1]);
    }
}
