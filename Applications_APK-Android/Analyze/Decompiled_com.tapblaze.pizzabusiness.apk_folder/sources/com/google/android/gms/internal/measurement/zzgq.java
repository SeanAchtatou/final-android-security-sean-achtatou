package com.google.android.gms.internal.measurement;

import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.internal.measurement.zzfe;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import cz.msebera.android.httpclient.message.TokenParser;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class zzgq<T> implements zzhf<T> {
    private static final int[] zza = new int[0];
    private static final Unsafe zzb = zzid.zzc();
    private final int[] zzc;
    private final Object[] zzd;
    private final int zze;
    private final int zzf;
    private final zzgm zzg;
    private final boolean zzh;
    private final boolean zzi;
    private final boolean zzj;
    private final boolean zzk;
    private final int[] zzl;
    private final int zzm;
    private final int zzn;
    private final zzgu zzo;
    private final zzfw zzp;
    private final zzhx<?, ?> zzq;
    private final zzet<?> zzr;
    private final zzgj zzs;

    private zzgq(int[] iArr, Object[] objArr, int i, int i2, zzgm zzgm, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzgu zzgu, zzfw zzfw, zzhx<?, ?> zzhx, zzet<?> zzet, zzgj zzgj) {
        this.zzc = iArr;
        this.zzd = objArr;
        this.zze = i;
        this.zzf = i2;
        this.zzi = zzgm instanceof zzfe;
        this.zzj = z;
        this.zzh = zzet != null && zzet.zza(zzgm);
        this.zzk = false;
        this.zzl = iArr2;
        this.zzm = i3;
        this.zzn = i4;
        this.zzo = zzgu;
        this.zzp = zzfw;
        this.zzq = zzhx;
        this.zzr = zzet;
        this.zzg = zzgm;
        this.zzs = zzgj;
    }

    private static boolean zzf(int i) {
        return (i & DriveFile.MODE_WRITE_ONLY) != 0;
    }

    static <T> zzgq<T> zza(Class<T> cls, zzgk zzgk, zzgu zzgu, zzfw zzfw, zzhx<?, ?> zzhx, zzet<?> zzet, zzgj zzgj) {
        int i;
        int i2;
        char c;
        int[] iArr;
        char c2;
        char c3;
        int i3;
        char c4;
        char c5;
        int i4;
        int i5;
        String str;
        char c6;
        int i6;
        char c7;
        int i7;
        int i8;
        int i9;
        int i10;
        Class<?> cls2;
        int i11;
        int i12;
        Field field;
        int i13;
        char charAt;
        int i14;
        char c8;
        Field field2;
        Field field3;
        int i15;
        char charAt2;
        int i16;
        char charAt3;
        int i17;
        char charAt4;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        char charAt5;
        int i24;
        char charAt6;
        int i25;
        char charAt7;
        int i26;
        char charAt8;
        char charAt9;
        char charAt10;
        char charAt11;
        char charAt12;
        char charAt13;
        char charAt14;
        zzgk zzgk2 = zzgk;
        if (zzgk2 instanceof zzhd) {
            zzhd zzhd = (zzhd) zzgk2;
            char c9 = 0;
            boolean z = zzhd.zza() == zzfe.zzf.zzi;
            String zzd2 = zzhd.zzd();
            int length = zzd2.length();
            char charAt15 = zzd2.charAt(0);
            if (charAt15 >= 55296) {
                char c10 = charAt15 & 8191;
                int i27 = 1;
                int i28 = 13;
                while (true) {
                    i = i27 + 1;
                    charAt14 = zzd2.charAt(i27);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c10 |= (charAt14 & 8191) << i28;
                    i28 += 13;
                    i27 = i;
                }
                charAt15 = (charAt14 << i28) | c10;
            } else {
                i = 1;
            }
            int i29 = i + 1;
            char charAt16 = zzd2.charAt(i);
            if (charAt16 >= 55296) {
                char c11 = charAt16 & 8191;
                int i30 = 13;
                while (true) {
                    i2 = i29 + 1;
                    charAt13 = zzd2.charAt(i29);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c11 |= (charAt13 & 8191) << i30;
                    i30 += 13;
                    i29 = i2;
                }
                charAt16 = c11 | (charAt13 << i30);
            } else {
                i2 = i29;
            }
            if (charAt16 == 0) {
                iArr = zza;
                c5 = 0;
                c4 = 0;
                i3 = 0;
                c3 = 0;
                c2 = 0;
                c = 0;
            } else {
                int i31 = i2 + 1;
                char charAt17 = zzd2.charAt(i2);
                if (charAt17 >= 55296) {
                    char c12 = charAt17 & 8191;
                    int i32 = 13;
                    while (true) {
                        i18 = i31 + 1;
                        charAt12 = zzd2.charAt(i31);
                        if (charAt12 < 55296) {
                            break;
                        }
                        c12 |= (charAt12 & 8191) << i32;
                        i32 += 13;
                        i31 = i18;
                    }
                    charAt17 = (charAt12 << i32) | c12;
                } else {
                    i18 = i31;
                }
                int i33 = i18 + 1;
                char charAt18 = zzd2.charAt(i18);
                if (charAt18 >= 55296) {
                    char c13 = charAt18 & 8191;
                    int i34 = 13;
                    while (true) {
                        i19 = i33 + 1;
                        charAt11 = zzd2.charAt(i33);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c13 |= (charAt11 & 8191) << i34;
                        i34 += 13;
                        i33 = i19;
                    }
                    charAt18 = c13 | (charAt11 << i34);
                } else {
                    i19 = i33;
                }
                int i35 = i19 + 1;
                char charAt19 = zzd2.charAt(i19);
                if (charAt19 >= 55296) {
                    char c14 = charAt19 & 8191;
                    int i36 = 13;
                    while (true) {
                        i20 = i35 + 1;
                        charAt10 = zzd2.charAt(i35);
                        if (charAt10 < 55296) {
                            break;
                        }
                        c14 |= (charAt10 & 8191) << i36;
                        i36 += 13;
                        i35 = i20;
                    }
                    charAt19 = (charAt10 << i36) | c14;
                } else {
                    i20 = i35;
                }
                int i37 = i20 + 1;
                c3 = zzd2.charAt(i20);
                if (c3 >= 55296) {
                    char c15 = c3 & 8191;
                    int i38 = 13;
                    while (true) {
                        i21 = i37 + 1;
                        charAt9 = zzd2.charAt(i37);
                        if (charAt9 < 55296) {
                            break;
                        }
                        c15 |= (charAt9 & 8191) << i38;
                        i38 += 13;
                        i37 = i21;
                    }
                    c3 = (charAt9 << i38) | c15;
                } else {
                    i21 = i37;
                }
                int i39 = i21 + 1;
                c2 = zzd2.charAt(i21);
                if (c2 >= 55296) {
                    char c16 = c2 & 8191;
                    int i40 = 13;
                    while (true) {
                        i26 = i39 + 1;
                        charAt8 = zzd2.charAt(i39);
                        if (charAt8 < 55296) {
                            break;
                        }
                        c16 |= (charAt8 & 8191) << i40;
                        i40 += 13;
                        i39 = i26;
                    }
                    c2 = (charAt8 << i40) | c16;
                    i39 = i26;
                }
                int i41 = i39 + 1;
                c5 = zzd2.charAt(i39);
                if (c5 >= 55296) {
                    char c17 = c5 & 8191;
                    int i42 = 13;
                    while (true) {
                        i25 = i41 + 1;
                        charAt7 = zzd2.charAt(i41);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c17 |= (charAt7 & 8191) << i42;
                        i42 += 13;
                        i41 = i25;
                    }
                    c5 = c17 | (charAt7 << i42);
                    i41 = i25;
                }
                int i43 = i41 + 1;
                char charAt20 = zzd2.charAt(i41);
                if (charAt20 >= 55296) {
                    int i44 = 13;
                    int i45 = i43;
                    char c18 = charAt20 & 8191;
                    int i46 = i45;
                    while (true) {
                        i24 = i46 + 1;
                        charAt6 = zzd2.charAt(i46);
                        if (charAt6 < 55296) {
                            break;
                        }
                        c18 |= (charAt6 & 8191) << i44;
                        i44 += 13;
                        i46 = i24;
                    }
                    charAt20 = c18 | (charAt6 << i44);
                    i22 = i24;
                } else {
                    i22 = i43;
                }
                int i47 = i22 + 1;
                c9 = zzd2.charAt(i22);
                if (c9 >= 55296) {
                    int i48 = 13;
                    int i49 = i47;
                    char c19 = c9 & 8191;
                    int i50 = i49;
                    while (true) {
                        i23 = i50 + 1;
                        charAt5 = zzd2.charAt(i50);
                        if (charAt5 < 55296) {
                            break;
                        }
                        c19 |= (charAt5 & 8191) << i48;
                        i48 += 13;
                        i50 = i23;
                    }
                    c9 = c19 | (charAt5 << i48);
                    i47 = i23;
                }
                iArr = new int[(c9 + c5 + charAt20)];
                i3 = (charAt17 << 1) + charAt18;
                int i51 = i47;
                c = charAt17;
                c4 = charAt19;
                i2 = i51;
            }
            Unsafe unsafe = zzb;
            Object[] zze2 = zzhd.zze();
            Class<?> cls3 = zzhd.zzc().getClass();
            int i52 = i3;
            int[] iArr2 = new int[(c2 * 3)];
            Object[] objArr = new Object[(c2 << 1)];
            int i53 = c9 + c5;
            char c20 = c9;
            int i54 = i53;
            int i55 = 0;
            int i56 = 0;
            while (i2 < length) {
                int i57 = i2 + 1;
                char charAt21 = zzd2.charAt(i2);
                char c21 = 55296;
                if (charAt21 >= 55296) {
                    int i58 = 13;
                    int i59 = i57;
                    char c22 = charAt21 & 8191;
                    int i60 = i59;
                    while (true) {
                        i17 = i60 + 1;
                        charAt4 = zzd2.charAt(i60);
                        if (charAt4 < c21) {
                            break;
                        }
                        c22 |= (charAt4 & 8191) << i58;
                        i58 += 13;
                        i60 = i17;
                        c21 = 55296;
                    }
                    charAt21 = c22 | (charAt4 << i58);
                    i4 = i17;
                } else {
                    i4 = i57;
                }
                int i61 = i4 + 1;
                char charAt22 = zzd2.charAt(i4);
                int i62 = length;
                char c23 = 55296;
                if (charAt22 >= 55296) {
                    int i63 = 13;
                    int i64 = i61;
                    char c24 = charAt22 & 8191;
                    int i65 = i64;
                    while (true) {
                        i16 = i65 + 1;
                        charAt3 = zzd2.charAt(i65);
                        if (charAt3 < c23) {
                            break;
                        }
                        c24 |= (charAt3 & 8191) << i63;
                        i63 += 13;
                        i65 = i16;
                        c23 = 55296;
                    }
                    charAt22 = c24 | (charAt3 << i63);
                    i5 = i16;
                } else {
                    i5 = i61;
                }
                char c25 = c9;
                char c26 = charAt22 & 255;
                boolean z2 = z;
                if ((charAt22 & 1024) != 0) {
                    iArr[i55] = i56;
                    i55++;
                }
                int i66 = i55;
                if (c26 >= '3') {
                    int i67 = i5 + 1;
                    char charAt23 = zzd2.charAt(i5);
                    char c27 = 55296;
                    if (charAt23 >= 55296) {
                        char c28 = charAt23 & 8191;
                        int i68 = 13;
                        while (true) {
                            i15 = i67 + 1;
                            charAt2 = zzd2.charAt(i67);
                            if (charAt2 < c27) {
                                break;
                            }
                            c28 |= (charAt2 & 8191) << i68;
                            i68 += 13;
                            i67 = i15;
                            c27 = 55296;
                        }
                        charAt23 = c28 | (charAt2 << i68);
                        i67 = i15;
                    }
                    int i69 = c26 - '3';
                    int i70 = i67;
                    if (i69 == 9 || i69 == 17) {
                        c8 = 1;
                        objArr[((i56 / 3) << 1) + 1] = zze2[i52];
                        i52++;
                    } else {
                        if (i69 == 12 && (charAt15 & 1) == 1) {
                            objArr[((i56 / 3) << 1) + 1] = zze2[i52];
                            i52++;
                        }
                        c8 = 1;
                    }
                    int i71 = charAt23 << c8;
                    Object obj = zze2[i71];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = zza(cls3, (String) obj);
                        zze2[i71] = field2;
                    }
                    char c29 = c4;
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i72 = i71 + 1;
                    Object obj2 = zze2[i72];
                    int i73 = objectFieldOffset;
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = zza(cls3, (String) obj2);
                        zze2[i72] = field3;
                    }
                    str = zzd2;
                    i9 = (int) unsafe.objectFieldOffset(field3);
                    cls2 = cls3;
                    i6 = i52;
                    i8 = i73;
                    i10 = 0;
                    c6 = c29;
                    c7 = c3;
                    i7 = charAt21;
                    i12 = i70;
                } else {
                    char c30 = c4;
                    int i74 = i52 + 1;
                    Field zza2 = zza(cls3, (String) zze2[i52]);
                    c7 = c3;
                    if (c26 == 9 || c26 == 17) {
                        c6 = c30;
                        objArr[((i56 / 3) << 1) + 1] = zza2.getType();
                    } else {
                        if (c26 == 27 || c26 == '1') {
                            c6 = c30;
                            i14 = i74 + 1;
                            objArr[((i56 / 3) << 1) + 1] = zze2[i74];
                        } else if (c26 == 12 || c26 == 30 || c26 == ',') {
                            c6 = c30;
                            if ((charAt15 & 1) == 1) {
                                i14 = i74 + 1;
                                objArr[((i56 / 3) << 1) + 1] = zze2[i74];
                            }
                        } else if (c26 == '2') {
                            int i75 = c20 + 1;
                            iArr[c20] = i56;
                            int i76 = (i56 / 3) << 1;
                            int i77 = i74 + 1;
                            objArr[i76] = zze2[i74];
                            if ((charAt22 & 2048) != 0) {
                                i74 = i77 + 1;
                                objArr[i76 + 1] = zze2[i77];
                                c6 = c30;
                                c20 = i75;
                            } else {
                                c20 = i75;
                                i74 = i77;
                                c6 = c30;
                            }
                        } else {
                            c6 = c30;
                        }
                        i7 = charAt21;
                        i74 = i14;
                        i8 = (int) unsafe.objectFieldOffset(zza2);
                        if ((charAt15 & 1) == 1 || c26 > 17) {
                            str = zzd2;
                            cls2 = cls3;
                            i6 = i74;
                            i11 = i5;
                            i10 = 0;
                            i9 = 0;
                        } else {
                            i11 = i5 + 1;
                            char charAt24 = zzd2.charAt(i5);
                            if (charAt24 >= 55296) {
                                char c31 = charAt24 & 8191;
                                int i78 = 13;
                                while (true) {
                                    i13 = i11 + 1;
                                    charAt = zzd2.charAt(i11);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    c31 |= (charAt & 8191) << i78;
                                    i78 += 13;
                                    i11 = i13;
                                }
                                charAt24 = c31 | (charAt << i78);
                                i11 = i13;
                            }
                            int i79 = (c << 1) + (charAt24 / TokenParser.SP);
                            Object obj3 = zze2[i79];
                            str = zzd2;
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = zza(cls3, (String) obj3);
                                zze2[i79] = field;
                            }
                            cls2 = cls3;
                            i6 = i74;
                            i9 = (int) unsafe.objectFieldOffset(field);
                            i10 = charAt24 % TokenParser.SP;
                        }
                        if (c26 >= 18 && c26 <= '1') {
                            iArr[i54] = i8;
                            i54++;
                        }
                        i12 = i11;
                    }
                    i7 = charAt21;
                    i8 = (int) unsafe.objectFieldOffset(zza2);
                    if ((charAt15 & 1) == 1) {
                    }
                    str = zzd2;
                    cls2 = cls3;
                    i6 = i74;
                    i11 = i5;
                    i10 = 0;
                    i9 = 0;
                    iArr[i54] = i8;
                    i54++;
                    i12 = i11;
                }
                int i80 = i56 + 1;
                iArr2[i56] = i7;
                int i81 = i80 + 1;
                iArr2[i80] = (c26 << 20) | ((charAt22 & 256) != 0 ? DriveFile.MODE_READ_ONLY : 0) | ((charAt22 & 512) != 0 ? DriveFile.MODE_WRITE_ONLY : 0) | i8;
                i56 = i81 + 1;
                iArr2[i81] = (i10 << 20) | i9;
                cls3 = cls2;
                c3 = c7;
                c9 = c25;
                i52 = i6;
                length = i62;
                z = z2;
                c4 = c6;
                i55 = i66;
                zzd2 = str;
            }
            return new zzgq(iArr2, objArr, c4, c3, zzhd.zzc(), z, false, iArr, c9, i53, zzgu, zzfw, zzhx, zzet, zzgj);
        }
        int zza3 = ((zzhq) zzgk2).zza();
        int i82 = zzfe.zzf.zzi;
        throw new NoSuchMethodError();
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    public final T zza() {
        return this.zzo.zza(this.zzg);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzid.zzf(r10, r6), com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zzb(r10, r6) == com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zza(r10, r6) == com.google.android.gms.internal.measurement.zzid.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zzb(r10, r6) == com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zza(r10, r6) == com.google.android.gms.internal.measurement.zzid.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zza(r10, r6) == com.google.android.gms.internal.measurement.zzid.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zza(r10, r6) == com.google.android.gms.internal.measurement.zzid.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzid.zzf(r10, r6), com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzid.zzf(r10, r6), com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzid.zzf(r10, r6), com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zzc(r10, r6) == com.google.android.gms.internal.measurement.zzid.zzc(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zza(r10, r6) == com.google.android.gms.internal.measurement.zzid.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zzb(r10, r6) == com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zza(r10, r6) == com.google.android.gms.internal.measurement.zzid.zza(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zzb(r10, r6) == com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.measurement.zzid.zzb(r10, r6) == com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.zzid.zzd(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.zzid.zzd(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.zzid.zze(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.zzid.zze(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzid.zzf(r10, r6), com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)) != false) goto L_0x01c2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(T r10, T r11) {
        /*
            r9 = this;
            int[] r0 = r9.zzc
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            r3 = 1
            if (r2 >= r0) goto L_0x01c9
            int r4 = r9.zzd(r2)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r4 & r5
            long r6 = (long) r6
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r8
            int r4 = r4 >>> 20
            switch(r4) {
                case 0: goto L_0x01a7;
                case 1: goto L_0x018e;
                case 2: goto L_0x017b;
                case 3: goto L_0x0168;
                case 4: goto L_0x0157;
                case 5: goto L_0x0144;
                case 6: goto L_0x0132;
                case 7: goto L_0x0120;
                case 8: goto L_0x010a;
                case 9: goto L_0x00f4;
                case 10: goto L_0x00de;
                case 11: goto L_0x00cc;
                case 12: goto L_0x00ba;
                case 13: goto L_0x00a8;
                case 14: goto L_0x0094;
                case 15: goto L_0x0082;
                case 16: goto L_0x006e;
                case 17: goto L_0x0058;
                case 18: goto L_0x004a;
                case 19: goto L_0x004a;
                case 20: goto L_0x004a;
                case 21: goto L_0x004a;
                case 22: goto L_0x004a;
                case 23: goto L_0x004a;
                case 24: goto L_0x004a;
                case 25: goto L_0x004a;
                case 26: goto L_0x004a;
                case 27: goto L_0x004a;
                case 28: goto L_0x004a;
                case 29: goto L_0x004a;
                case 30: goto L_0x004a;
                case 31: goto L_0x004a;
                case 32: goto L_0x004a;
                case 33: goto L_0x004a;
                case 34: goto L_0x004a;
                case 35: goto L_0x004a;
                case 36: goto L_0x004a;
                case 37: goto L_0x004a;
                case 38: goto L_0x004a;
                case 39: goto L_0x004a;
                case 40: goto L_0x004a;
                case 41: goto L_0x004a;
                case 42: goto L_0x004a;
                case 43: goto L_0x004a;
                case 44: goto L_0x004a;
                case 45: goto L_0x004a;
                case 46: goto L_0x004a;
                case 47: goto L_0x004a;
                case 48: goto L_0x004a;
                case 49: goto L_0x004a;
                case 50: goto L_0x003c;
                case 51: goto L_0x001c;
                case 52: goto L_0x001c;
                case 53: goto L_0x001c;
                case 54: goto L_0x001c;
                case 55: goto L_0x001c;
                case 56: goto L_0x001c;
                case 57: goto L_0x001c;
                case 58: goto L_0x001c;
                case 59: goto L_0x001c;
                case 60: goto L_0x001c;
                case 61: goto L_0x001c;
                case 62: goto L_0x001c;
                case 63: goto L_0x001c;
                case 64: goto L_0x001c;
                case 65: goto L_0x001c;
                case 66: goto L_0x001c;
                case 67: goto L_0x001c;
                case 68: goto L_0x001c;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x01c2
        L_0x001c:
            int r4 = r9.zze(r2)
            r4 = r4 & r5
            long r4 = (long) r4
            int r8 = com.google.android.gms.internal.measurement.zzid.zza(r10, r4)
            int r4 = com.google.android.gms.internal.measurement.zzid.zza(r11, r4)
            if (r8 != r4) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzid.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzhh.zza(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x003c:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.zzid.zzf(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)
            boolean r3 = com.google.android.gms.internal.measurement.zzhh.zza(r3, r4)
            goto L_0x01c2
        L_0x004a:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.zzid.zzf(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)
            boolean r3 = com.google.android.gms.internal.measurement.zzhh.zza(r3, r4)
            goto L_0x01c2
        L_0x0058:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzid.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzhh.zza(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x006e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzid.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0082:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzid.zza(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzid.zza(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0094:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzid.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00a8:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzid.zza(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzid.zza(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00ba:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzid.zza(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzid.zza(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00cc:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzid.zza(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzid.zza(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00de:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzid.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzhh.zza(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00f4:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzid.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzhh.zza(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x010a:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzid.zzf(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzid.zzf(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.zzhh.zza(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0120:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            boolean r4 = com.google.android.gms.internal.measurement.zzid.zzc(r10, r6)
            boolean r5 = com.google.android.gms.internal.measurement.zzid.zzc(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0132:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzid.zza(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzid.zza(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0144:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzid.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0157:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.zzid.zza(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.zzid.zza(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0168:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzid.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x017b:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.zzid.zzb(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.zzid.zzb(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x018e:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            float r4 = com.google.android.gms.internal.measurement.zzid.zzd(r10, r6)
            int r4 = java.lang.Float.floatToIntBits(r4)
            float r5 = com.google.android.gms.internal.measurement.zzid.zzd(r11, r6)
            int r5 = java.lang.Float.floatToIntBits(r5)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x01a7:
            boolean r4 = r9.zzc(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            double r4 = com.google.android.gms.internal.measurement.zzid.zze(r10, r6)
            long r4 = java.lang.Double.doubleToLongBits(r4)
            double r6 = com.google.android.gms.internal.measurement.zzid.zze(r11, r6)
            long r6 = java.lang.Double.doubleToLongBits(r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
        L_0x01c1:
            r3 = 0
        L_0x01c2:
            if (r3 != 0) goto L_0x01c5
            return r1
        L_0x01c5:
            int r2 = r2 + 3
            goto L_0x0005
        L_0x01c9:
            com.google.android.gms.internal.measurement.zzhx<?, ?> r0 = r9.zzq
            java.lang.Object r0 = r0.zzb(r10)
            com.google.android.gms.internal.measurement.zzhx<?, ?> r2 = r9.zzq
            java.lang.Object r2 = r2.zzb(r11)
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x01dc
            return r1
        L_0x01dc:
            boolean r0 = r9.zzh
            if (r0 == 0) goto L_0x01f1
            com.google.android.gms.internal.measurement.zzet<?> r0 = r9.zzr
            com.google.android.gms.internal.measurement.zzeu r10 = r0.zza(r10)
            com.google.android.gms.internal.measurement.zzet<?> r0 = r9.zzr
            com.google.android.gms.internal.measurement.zzeu r11 = r0.zza(r11)
            boolean r10 = r10.equals(r11)
            return r10
        L_0x01f1:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, java.lang.Object):boolean");
    }

    public final int zza(T t) {
        int i;
        int i2;
        int length = this.zzc.length;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4 += 3) {
            int zzd2 = zzd(i4);
            int i5 = this.zzc[i4];
            long j = (long) (1048575 & zzd2);
            int i6 = 37;
            switch ((zzd2 & 267386880) >>> 20) {
                case 0:
                    i2 = i3 * 53;
                    i = zzfh.zza(Double.doubleToLongBits(zzid.zze(t, j)));
                    i3 = i2 + i;
                    break;
                case 1:
                    i2 = i3 * 53;
                    i = Float.floatToIntBits(zzid.zzd(t, j));
                    i3 = i2 + i;
                    break;
                case 2:
                    i2 = i3 * 53;
                    i = zzfh.zza(zzid.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 3:
                    i2 = i3 * 53;
                    i = zzfh.zza(zzid.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 4:
                    i2 = i3 * 53;
                    i = zzid.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 5:
                    i2 = i3 * 53;
                    i = zzfh.zza(zzid.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 6:
                    i2 = i3 * 53;
                    i = zzid.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 7:
                    i2 = i3 * 53;
                    i = zzfh.zza(zzid.zzc(t, j));
                    i3 = i2 + i;
                    break;
                case 8:
                    i2 = i3 * 53;
                    i = ((String) zzid.zzf(t, j)).hashCode();
                    i3 = i2 + i;
                    break;
                case 9:
                    Object zzf2 = zzid.zzf(t, j);
                    if (zzf2 != null) {
                        i6 = zzf2.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 10:
                    i2 = i3 * 53;
                    i = zzid.zzf(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 11:
                    i2 = i3 * 53;
                    i = zzid.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 12:
                    i2 = i3 * 53;
                    i = zzid.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 13:
                    i2 = i3 * 53;
                    i = zzid.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 14:
                    i2 = i3 * 53;
                    i = zzfh.zza(zzid.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 15:
                    i2 = i3 * 53;
                    i = zzid.zza(t, j);
                    i3 = i2 + i;
                    break;
                case 16:
                    i2 = i3 * 53;
                    i = zzfh.zza(zzid.zzb(t, j));
                    i3 = i2 + i;
                    break;
                case 17:
                    Object zzf3 = zzid.zzf(t, j);
                    if (zzf3 != null) {
                        i6 = zzf3.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i2 = i3 * 53;
                    i = zzid.zzf(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 50:
                    i2 = i3 * 53;
                    i = zzid.zzf(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 51:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfh.zza(Double.doubleToLongBits(zzb(t, j)));
                        i3 = i2 + i;
                        break;
                    }
                case 52:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = Float.floatToIntBits(zzc(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 53:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfh.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 54:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfh.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 55:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 56:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfh.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 57:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 58:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfh.zza(zzf(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 59:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = ((String) zzid.zzf(t, j)).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 60:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzid.zzf(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 61:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzid.zzf(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 62:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 63:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 64:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 65:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfh.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 66:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzd(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 67:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzfh.zza(zze(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 68:
                    if (!zza(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = zzid.zzf(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
            }
        }
        int hashCode = (i3 * 53) + this.zzq.zzb(t).hashCode();
        return this.zzh ? (hashCode * 53) + this.zzr.zza((Object) t).hashCode() : hashCode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zza(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object):int
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
     arg types: [T, long, long]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.zzgq.zzb(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhf.zzb(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void */
    public final void zzb(T t, T t2) {
        if (t2 != null) {
            for (int i = 0; i < this.zzc.length; i += 3) {
                int zzd2 = zzd(i);
                long j = (long) (1048575 & zzd2);
                int i2 = this.zzc[i];
                switch ((zzd2 & 267386880) >>> 20) {
                    case 0:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza(t, j, zzid.zze(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 1:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zzd(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 2:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zzb(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 3:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zzb(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 4:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zza(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 5:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zzb(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 6:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zza(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 7:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zzc(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 8:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza(t, j, zzid.zzf(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 9:
                        zza(t, t2, i);
                        break;
                    case 10:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza(t, j, zzid.zzf(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 11:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zza(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 12:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zza(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 13:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zza(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 14:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zzb(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 15:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zza(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 16:
                        if (!zza((Object) t2, i)) {
                            break;
                        } else {
                            zzid.zza((Object) t, j, zzid.zzb(t2, j));
                            zzb((Object) t, i);
                            break;
                        }
                    case 17:
                        zza(t, t2, i);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.zzp.zza(t, t2, j);
                        break;
                    case 50:
                        zzhh.zza(this.zzs, t, t2, j);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzid.zza(t, j, zzid.zzf(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 60:
                        zzb(t, t2, i);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!zza(t2, i2, i)) {
                            break;
                        } else {
                            zzid.zza(t, j, zzid.zzf(t2, j));
                            zzb(t, i2, i);
                            break;
                        }
                    case 68:
                        zzb(t, t2, i);
                        break;
                }
            }
            zzhh.zza(this.zzq, t, t2);
            if (this.zzh) {
                zzhh.zza(this.zzr, t, t2);
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zza(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object):int
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.zzgq.zzb(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhf.zzb(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, int):void */
    private final void zza(T t, T t2, int i) {
        long zzd2 = (long) (zzd(i) & 1048575);
        if (zza((Object) t2, i)) {
            Object zzf2 = zzid.zzf(t, zzd2);
            Object zzf3 = zzid.zzf(t2, zzd2);
            if (zzf2 != null && zzf3 != null) {
                zzid.zza(t, zzd2, zzfh.zza(zzf2, zzf3));
                zzb((Object) t, i);
            } else if (zzf3 != null) {
                zzid.zza(t, zzd2, zzf3);
                zzb((Object) t, i);
            }
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzd2 = zzd(i);
        int i2 = this.zzc[i];
        long j = (long) (zzd2 & 1048575);
        if (zza(t2, i2, i)) {
            Object zzf2 = zzid.zzf(t, j);
            Object zzf3 = zzid.zzf(t2, j);
            if (zzf2 != null && zzf3 != null) {
                zzid.zza(t, j, zzfh.zza(zzf2, zzf3));
                zzb(t, i2, i);
            } else if (zzf3 != null) {
                zzid.zza(t, j, zzf3);
                zzb(t, i2, i);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzel.zzh(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.zzel.zzh(int, int):int
      com.google.android.gms.internal.measurement.zzel.zzh(int, long):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzel.zzb(int, boolean):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.zzel.zzb(int, double):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, float):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, com.google.android.gms.internal.measurement.zzfv):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, com.google.android.gms.internal.measurement.zzgm):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, java.lang.String):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, int):void
      com.google.android.gms.internal.measurement.zzel.zzb(int, long):void
      com.google.android.gms.internal.measurement.zzel.zzb(int, com.google.android.gms.internal.measurement.zzdw):void
      com.google.android.gms.internal.measurement.zzel.zzb(int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzel.zzg(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.zzel.zzg(int, int):int
      com.google.android.gms.internal.measurement.zzel.zzg(int, long):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzel.zzb(int, float):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.zzel.zzb(int, double):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, com.google.android.gms.internal.measurement.zzfv):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, com.google.android.gms.internal.measurement.zzgm):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, java.lang.String):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, boolean):int
      com.google.android.gms.internal.measurement.zzel.zzb(int, int):void
      com.google.android.gms.internal.measurement.zzel.zzb(int, long):void
      com.google.android.gms.internal.measurement.zzel.zzb(int, com.google.android.gms.internal.measurement.zzdw):void
      com.google.android.gms.internal.measurement.zzel.zzb(int, float):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<com.google.android.gms.internal.measurement.zzgm>, com.google.android.gms.internal.measurement.zzhf):int
     arg types: [int, java.util.List<?>, com.google.android.gms.internal.measurement.zzhf]
     candidates:
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<java.lang.Long>, boolean):int
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<com.google.android.gms.internal.measurement.zzdw>, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<com.google.android.gms.internal.measurement.zzgm>, com.google.android.gms.internal.measurement.zzhf):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<java.lang.Long>, boolean):int
     arg types: [int, java.util.List<?>, int]
     candidates:
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<com.google.android.gms.internal.measurement.zzgm>, com.google.android.gms.internal.measurement.zzhf):int
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<com.google.android.gms.internal.measurement.zzdw>, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<java.lang.Long>, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.Long>, boolean):int
     arg types: [int, java.util.List<?>, int]
     candidates:
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.lang.Object, com.google.android.gms.internal.measurement.zzhf):int
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<?>, com.google.android.gms.internal.measurement.zzhf):int
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.String>, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzet, java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.Long>, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zza(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object):int
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object):int
     arg types: [com.google.android.gms.internal.measurement.zzhx<?, ?>, T]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zza(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzgq.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<java.lang.Long>, boolean):int
     arg types: [int, java.util.List, int]
     candidates:
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<com.google.android.gms.internal.measurement.zzgm>, com.google.android.gms.internal.measurement.zzhf):int
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<com.google.android.gms.internal.measurement.zzdw>, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<java.lang.Long>, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.Long>, boolean):int
     arg types: [int, java.util.List, int]
     candidates:
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.lang.Object, com.google.android.gms.internal.measurement.zzhf):int
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<?>, com.google.android.gms.internal.measurement.zzhf):int
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.String>, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzet, java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.Long>, boolean):int */
    public final int zzb(T t) {
        int i;
        int i2;
        long j;
        int i3;
        int zzb2;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int zzb3;
        int i9;
        int i10;
        int i11;
        T t2 = t;
        int i12 = 267386880;
        int i13 = 1048575;
        int i14 = 1;
        if (this.zzj) {
            Unsafe unsafe = zzb;
            int i15 = 0;
            int i16 = 0;
            while (i15 < this.zzc.length) {
                int zzd2 = zzd(i15);
                int i17 = (zzd2 & i12) >>> 20;
                int i18 = this.zzc[i15];
                long j2 = (long) (zzd2 & 1048575);
                int i19 = (i17 < zzez.DOUBLE_LIST_PACKED.zza() || i17 > zzez.SINT64_LIST_PACKED.zza()) ? 0 : this.zzc[i15 + 2] & 1048575;
                switch (i17) {
                    case 0:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzb(i18, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 1:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzb(i18, 0.0f);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 2:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzd(i18, zzid.zzb(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 3:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zze(i18, zzid.zzb(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 4:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzf(i18, zzid.zza(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 5:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzg(i18, 0L);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 6:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzi(i18, 0);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 7:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzb(i18, true);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 8:
                        if (zza((Object) t2, i15)) {
                            Object zzf2 = zzid.zzf(t2, j2);
                            if (!(zzf2 instanceof zzdw)) {
                                zzb3 = zzel.zzb(i18, (String) zzf2);
                                break;
                            } else {
                                zzb3 = zzel.zzc(i18, (zzdw) zzf2);
                                break;
                            }
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 9:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzhh.zza(i18, zzid.zzf(t2, j2), zza(i15));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 10:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzc(i18, (zzdw) zzid.zzf(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 11:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzg(i18, zzid.zza(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 12:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzk(i18, zzid.zza(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 13:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzj(i18, 0);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 14:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzh(i18, 0L);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 15:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzh(i18, zzid.zza(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 16:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzf(i18, zzid.zzb(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 17:
                        if (zza((Object) t2, i15)) {
                            zzb3 = zzel.zzc(i18, (zzgm) zzid.zzf(t2, j2), zza(i15));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 18:
                        zzb3 = zzhh.zzi(i18, zza(t2, j2), false);
                        break;
                    case 19:
                        zzb3 = zzhh.zzh(i18, zza(t2, j2), false);
                        break;
                    case 20:
                        zzb3 = zzhh.zza(i18, (List<Long>) zza(t2, j2), false);
                        break;
                    case 21:
                        zzb3 = zzhh.zzb(i18, (List<Long>) zza(t2, j2), false);
                        break;
                    case 22:
                        zzb3 = zzhh.zze(i18, zza(t2, j2), false);
                        break;
                    case 23:
                        zzb3 = zzhh.zzi(i18, zza(t2, j2), false);
                        break;
                    case 24:
                        zzb3 = zzhh.zzh(i18, zza(t2, j2), false);
                        break;
                    case 25:
                        zzb3 = zzhh.zzj(i18, zza(t2, j2), false);
                        break;
                    case 26:
                        zzb3 = zzhh.zza(i18, zza(t2, j2));
                        break;
                    case 27:
                        zzb3 = zzhh.zza(i18, zza(t2, j2), zza(i15));
                        break;
                    case 28:
                        zzb3 = zzhh.zzb(i18, zza(t2, j2));
                        break;
                    case 29:
                        zzb3 = zzhh.zzf(i18, zza(t2, j2), false);
                        break;
                    case 30:
                        zzb3 = zzhh.zzd(i18, zza(t2, j2), false);
                        break;
                    case 31:
                        zzb3 = zzhh.zzh(i18, zza(t2, j2), false);
                        break;
                    case 32:
                        zzb3 = zzhh.zzi(i18, zza(t2, j2), false);
                        break;
                    case 33:
                        zzb3 = zzhh.zzg(i18, zza(t2, j2), false);
                        break;
                    case 34:
                        zzb3 = zzhh.zzc(i18, zza(t2, j2), false);
                        break;
                    case 35:
                        i10 = zzhh.zzi((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 36:
                        i10 = zzhh.zzh((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 37:
                        i10 = zzhh.zza((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 38:
                        i10 = zzhh.zzb((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 39:
                        i10 = zzhh.zze((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 40:
                        i10 = zzhh.zzi((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 41:
                        i10 = zzhh.zzh((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 42:
                        i10 = zzhh.zzj((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 43:
                        i10 = zzhh.zzf((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 44:
                        i10 = zzhh.zzd((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 45:
                        i10 = zzhh.zzh((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 46:
                        i10 = zzhh.zzi((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 47:
                        i10 = zzhh.zzg((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 48:
                        i10 = zzhh.zzc((List) unsafe.getObject(t2, j2));
                        if (i10 > 0) {
                            if (this.zzk) {
                                unsafe.putInt(t2, (long) i19, i10);
                            }
                            i11 = zzel.zze(i18);
                            i9 = zzel.zzg(i10);
                            zzb3 = i11 + i9 + i10;
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 49:
                        zzb3 = zzhh.zzb(i18, (List<zzgm>) zza(t2, j2), zza(i15));
                        break;
                    case 50:
                        zzb3 = this.zzs.zza(i18, zzid.zzf(t2, j2), zzb(i15));
                        break;
                    case 51:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzb(i18, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 52:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzb(i18, 0.0f);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 53:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzd(i18, zze(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 54:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zze(i18, zze(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 55:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzf(i18, zzd(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 56:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzg(i18, 0L);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 57:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzi(i18, 0);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 58:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzb(i18, true);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 59:
                        if (zza(t2, i18, i15)) {
                            Object zzf3 = zzid.zzf(t2, j2);
                            if (!(zzf3 instanceof zzdw)) {
                                zzb3 = zzel.zzb(i18, (String) zzf3);
                                break;
                            } else {
                                zzb3 = zzel.zzc(i18, (zzdw) zzf3);
                                break;
                            }
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 60:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzhh.zza(i18, zzid.zzf(t2, j2), zza(i15));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 61:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzc(i18, (zzdw) zzid.zzf(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 62:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzg(i18, zzd(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 63:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzk(i18, zzd(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 64:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzj(i18, 0);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 65:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzh(i18, 0L);
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 66:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzh(i18, zzd(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 67:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzf(i18, zze(t2, j2));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    case 68:
                        if (zza(t2, i18, i15)) {
                            zzb3 = zzel.zzc(i18, (zzgm) zzid.zzf(t2, j2), zza(i15));
                            break;
                        } else {
                            continue;
                            i15 += 3;
                            i12 = 267386880;
                        }
                    default:
                        i15 += 3;
                        i12 = 267386880;
                }
                i16 += zzb3;
                i15 += 3;
                i12 = 267386880;
            }
            return i16 + zza((zzhx) this.zzq, (Object) t2);
        }
        Unsafe unsafe2 = zzb;
        int i20 = 0;
        int i21 = 0;
        int i22 = -1;
        int i23 = 0;
        while (i20 < this.zzc.length) {
            int zzd3 = zzd(i20);
            int[] iArr = this.zzc;
            int i24 = iArr[i20];
            int i25 = (zzd3 & 267386880) >>> 20;
            if (i25 <= 17) {
                i2 = iArr[i20 + 2];
                int i26 = i2 & i13;
                i = i14 << (i2 >>> 20);
                if (i26 != i22) {
                    i23 = unsafe2.getInt(t2, (long) i26);
                } else {
                    i26 = i22;
                }
                i22 = i26;
            } else {
                i2 = (!this.zzk || i25 < zzez.DOUBLE_LIST_PACKED.zza() || i25 > zzez.SINT64_LIST_PACKED.zza()) ? 0 : this.zzc[i20 + 2] & i13;
                i = 0;
            }
            long j3 = (long) (zzd3 & i13);
            switch (i25) {
                case 0:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i21 += zzel.zzb(i24, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
                        continue;
                        i20 += 3;
                        i13 = 1048575;
                        i14 = 1;
                    }
                    break;
                case 1:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i21 += zzel.zzb(i24, 0.0f);
                        break;
                    }
                    break;
                case 2:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i3 = zzel.zzd(i24, unsafe2.getLong(t2, j3));
                        i21 += i3;
                        break;
                    }
                    break;
                case 3:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i3 = zzel.zze(i24, unsafe2.getLong(t2, j3));
                        i21 += i3;
                        break;
                    }
                    break;
                case 4:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i3 = zzel.zzf(i24, unsafe2.getInt(t2, j3));
                        i21 += i3;
                        break;
                    }
                    break;
                case 5:
                    j = 0;
                    if ((i23 & i) != 0) {
                        i3 = zzel.zzg(i24, 0L);
                        i21 += i3;
                        break;
                    }
                    break;
                case 6:
                    if ((i23 & i) != 0) {
                        i21 += zzel.zzi(i24, 0);
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 7:
                    if ((i23 & i) != 0) {
                        i21 += zzel.zzb(i24, true);
                        j = 0;
                        i20 += 3;
                        i13 = 1048575;
                        i14 = 1;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 8:
                    if ((i23 & i) != 0) {
                        Object object = unsafe2.getObject(t2, j3);
                        if (object instanceof zzdw) {
                            zzb2 = zzel.zzc(i24, (zzdw) object);
                        } else {
                            zzb2 = zzel.zzb(i24, (String) object);
                        }
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 9:
                    if ((i23 & i) != 0) {
                        zzb2 = zzhh.zza(i24, unsafe2.getObject(t2, j3), zza(i20));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 10:
                    if ((i23 & i) != 0) {
                        zzb2 = zzel.zzc(i24, (zzdw) unsafe2.getObject(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 11:
                    if ((i23 & i) != 0) {
                        zzb2 = zzel.zzg(i24, unsafe2.getInt(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 12:
                    if ((i23 & i) != 0) {
                        zzb2 = zzel.zzk(i24, unsafe2.getInt(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 13:
                    if ((i23 & i) != 0) {
                        i4 = zzel.zzj(i24, 0);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 14:
                    if ((i23 & i) != 0) {
                        zzb2 = zzel.zzh(i24, 0L);
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 15:
                    if ((i23 & i) != 0) {
                        zzb2 = zzel.zzh(i24, unsafe2.getInt(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 16:
                    if ((i23 & i) != 0) {
                        zzb2 = zzel.zzf(i24, unsafe2.getLong(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 17:
                    if ((i23 & i) != 0) {
                        zzb2 = zzel.zzc(i24, (zzgm) unsafe2.getObject(t2, j3), zza(i20));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 18:
                    zzb2 = zzhh.zzi(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += zzb2;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 19:
                    i5 = zzhh.zzh(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 20:
                    i5 = zzhh.zza(i24, (List<Long>) ((List) unsafe2.getObject(t2, j3)), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 21:
                    i5 = zzhh.zzb(i24, (List<Long>) ((List) unsafe2.getObject(t2, j3)), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 22:
                    i5 = zzhh.zze(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 23:
                    i5 = zzhh.zzi(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 24:
                    i5 = zzhh.zzh(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 25:
                    i5 = zzhh.zzj(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 26:
                    zzb2 = zzhh.zza(i24, (List) unsafe2.getObject(t2, j3));
                    i21 += zzb2;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 27:
                    zzb2 = zzhh.zza(i24, (List<?>) ((List) unsafe2.getObject(t2, j3)), zza(i20));
                    i21 += zzb2;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 28:
                    zzb2 = zzhh.zzb(i24, (List) unsafe2.getObject(t2, j3));
                    i21 += zzb2;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 29:
                    zzb2 = zzhh.zzf(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += zzb2;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 30:
                    i5 = zzhh.zzd(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 31:
                    i5 = zzhh.zzh(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 32:
                    i5 = zzhh.zzi(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 33:
                    i5 = zzhh.zzg(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 34:
                    i5 = zzhh.zzc(i24, (List) unsafe2.getObject(t2, j3), false);
                    i21 += i5;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 35:
                    i8 = zzhh.zzi((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 36:
                    i8 = zzhh.zzh((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 37:
                    i8 = zzhh.zza((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 38:
                    i8 = zzhh.zzb((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 39:
                    i8 = zzhh.zze((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 40:
                    i8 = zzhh.zzi((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 41:
                    i8 = zzhh.zzh((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 42:
                    i8 = zzhh.zzj((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 43:
                    i8 = zzhh.zzf((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 44:
                    i8 = zzhh.zzd((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 45:
                    i8 = zzhh.zzh((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 46:
                    i8 = zzhh.zzi((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 47:
                    i8 = zzhh.zzg((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 48:
                    i8 = zzhh.zzc((List) unsafe2.getObject(t2, j3));
                    if (i8 > 0) {
                        if (this.zzk) {
                            unsafe2.putInt(t2, (long) i2, i8);
                        }
                        i7 = zzel.zze(i24);
                        i6 = zzel.zzg(i8);
                        i4 = i7 + i6 + i8;
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 49:
                    zzb2 = zzhh.zzb(i24, (List) unsafe2.getObject(t2, j3), zza(i20));
                    i21 += zzb2;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 50:
                    zzb2 = this.zzs.zza(i24, unsafe2.getObject(t2, j3), zzb(i20));
                    i21 += zzb2;
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 51:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzb(i24, (double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 52:
                    if (zza(t2, i24, i20)) {
                        i4 = zzel.zzb(i24, 0.0f);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 53:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzd(i24, zze(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 54:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zze(i24, zze(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 55:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzf(i24, zzd(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 56:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzg(i24, 0L);
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 57:
                    if (zza(t2, i24, i20)) {
                        i4 = zzel.zzi(i24, 0);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 58:
                    if (zza(t2, i24, i20)) {
                        i4 = zzel.zzb(i24, true);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 59:
                    if (zza(t2, i24, i20)) {
                        Object object2 = unsafe2.getObject(t2, j3);
                        if (object2 instanceof zzdw) {
                            zzb2 = zzel.zzc(i24, (zzdw) object2);
                        } else {
                            zzb2 = zzel.zzb(i24, (String) object2);
                        }
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 60:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzhh.zza(i24, unsafe2.getObject(t2, j3), zza(i20));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 61:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzc(i24, (zzdw) unsafe2.getObject(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 62:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzg(i24, zzd(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 63:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzk(i24, zzd(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 64:
                    if (zza(t2, i24, i20)) {
                        i4 = zzel.zzj(i24, 0);
                        i21 += i4;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 65:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzh(i24, 0L);
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 66:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzh(i24, zzd(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 67:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzf(i24, zze(t2, j3));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                case 68:
                    if (zza(t2, i24, i20)) {
                        zzb2 = zzel.zzc(i24, (zzgm) unsafe2.getObject(t2, j3), zza(i20));
                        i21 += zzb2;
                    }
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
                default:
                    j = 0;
                    i20 += 3;
                    i13 = 1048575;
                    i14 = 1;
            }
            i20 += 3;
            i13 = 1048575;
            i14 = 1;
        }
        int zza2 = i21 + zza((zzhx) this.zzq, (Object) t2);
        if (!this.zzh) {
            return zza2;
        }
        zzeu<?> zza3 = this.zzr.zza((Object) t2);
        int i27 = 0;
        for (int i28 = 0; i28 < zza3.zza.zzc(); i28++) {
            Map.Entry<T, Object> zzb4 = zza3.zza.zzb(i28);
            i27 += zzeu.zza((zzew) zzb4.getKey(), zzb4.getValue());
        }
        for (Map.Entry next : zza3.zza.zzd()) {
            i27 += zzeu.zza((zzew) next.getKey(), next.getValue());
        }
        return zza2 + i27;
    }

    private static <UT, UB> int zza(zzhx<UT, UB> zzhx, T t) {
        return zzhx.zzf(zzhx.zzb(t));
    }

    private static List<?> zza(Object obj, long j) {
        return (List) zzid.zzf(obj, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.zziq, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.zziq, int]
     candidates:
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<?>, com.google.android.gms.internal.measurement.zziq, com.google.android.gms.internal.measurement.zzhf):void
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.zziq, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.zziq, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.zziq, int]
     candidates:
      com.google.android.gms.internal.measurement.zzhh.zza(int, int, java.lang.Object, com.google.android.gms.internal.measurement.zzhx):UB
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<?>, com.google.android.gms.internal.measurement.zziq, com.google.android.gms.internal.measurement.zzhf):void
      com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzgj, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.zziq, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zza(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object):int
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
     arg types: [T, com.google.android.gms.internal.measurement.zziq]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.zzgq.zzb(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, int):void
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhf.zzb(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0513  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0553  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a2b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r14, com.google.android.gms.internal.measurement.zziq r15) throws java.io.IOException {
        /*
            r13 = this;
            int r0 = r15.zza()
            int r1 = com.google.android.gms.internal.measurement.zzfe.zzf.zzk
            r2 = 267386880(0xff00000, float:2.3665827E-29)
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
            if (r0 != r1) goto L_0x0529
            com.google.android.gms.internal.measurement.zzhx<?, ?> r0 = r13.zzq
            zza(r0, r14, r15)
            boolean r0 = r13.zzh
            if (r0 == 0) goto L_0x0032
            com.google.android.gms.internal.measurement.zzet<?> r0 = r13.zzr
            com.google.android.gms.internal.measurement.zzeu r0 = r0.zza(r14)
            com.google.android.gms.internal.measurement.zzhg<T, java.lang.Object> r1 = r0.zza
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0032
            java.util.Iterator r0 = r0.zze()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0034
        L_0x0032:
            r0 = r3
            r1 = r0
        L_0x0034:
            int[] r7 = r13.zzc
            int r7 = r7.length
            int r7 = r7 + -3
        L_0x0039:
            if (r7 < 0) goto L_0x0511
            int r8 = r13.zzd(r7)
            int[] r9 = r13.zzc
            r9 = r9[r7]
        L_0x0043:
            if (r1 == 0) goto L_0x0061
            com.google.android.gms.internal.measurement.zzet<?> r10 = r13.zzr
            int r10 = r10.zza(r1)
            if (r10 <= r9) goto L_0x0061
            com.google.android.gms.internal.measurement.zzet<?> r10 = r13.zzr
            r10.zza(r15, r1)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x005f
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0043
        L_0x005f:
            r1 = r3
            goto L_0x0043
        L_0x0061:
            r10 = r8 & r2
            int r10 = r10 >>> 20
            switch(r10) {
                case 0: goto L_0x04fe;
                case 1: goto L_0x04ee;
                case 2: goto L_0x04de;
                case 3: goto L_0x04ce;
                case 4: goto L_0x04be;
                case 5: goto L_0x04ae;
                case 6: goto L_0x049e;
                case 7: goto L_0x048d;
                case 8: goto L_0x047c;
                case 9: goto L_0x0467;
                case 10: goto L_0x0454;
                case 11: goto L_0x0443;
                case 12: goto L_0x0432;
                case 13: goto L_0x0421;
                case 14: goto L_0x0410;
                case 15: goto L_0x03ff;
                case 16: goto L_0x03ee;
                case 17: goto L_0x03d9;
                case 18: goto L_0x03c8;
                case 19: goto L_0x03b7;
                case 20: goto L_0x03a6;
                case 21: goto L_0x0395;
                case 22: goto L_0x0384;
                case 23: goto L_0x0373;
                case 24: goto L_0x0362;
                case 25: goto L_0x0351;
                case 26: goto L_0x0340;
                case 27: goto L_0x032b;
                case 28: goto L_0x031a;
                case 29: goto L_0x0309;
                case 30: goto L_0x02f8;
                case 31: goto L_0x02e7;
                case 32: goto L_0x02d6;
                case 33: goto L_0x02c5;
                case 34: goto L_0x02b4;
                case 35: goto L_0x02a3;
                case 36: goto L_0x0292;
                case 37: goto L_0x0281;
                case 38: goto L_0x0270;
                case 39: goto L_0x025f;
                case 40: goto L_0x024e;
                case 41: goto L_0x023d;
                case 42: goto L_0x022c;
                case 43: goto L_0x021b;
                case 44: goto L_0x020a;
                case 45: goto L_0x01f9;
                case 46: goto L_0x01e8;
                case 47: goto L_0x01d7;
                case 48: goto L_0x01c6;
                case 49: goto L_0x01b1;
                case 50: goto L_0x01a6;
                case 51: goto L_0x0195;
                case 52: goto L_0x0184;
                case 53: goto L_0x0173;
                case 54: goto L_0x0162;
                case 55: goto L_0x0151;
                case 56: goto L_0x0140;
                case 57: goto L_0x012f;
                case 58: goto L_0x011e;
                case 59: goto L_0x010d;
                case 60: goto L_0x00f8;
                case 61: goto L_0x00e5;
                case 62: goto L_0x00d4;
                case 63: goto L_0x00c3;
                case 64: goto L_0x00b2;
                case 65: goto L_0x00a1;
                case 66: goto L_0x0090;
                case 67: goto L_0x007f;
                case 68: goto L_0x006a;
                default: goto L_0x0068;
            }
        L_0x0068:
            goto L_0x050d
        L_0x006a:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            com.google.android.gms.internal.measurement.zzhf r10 = r13.zza(r7)
            r15.zzb(r9, r8, r10)
            goto L_0x050d
        L_0x007f:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zze(r9, r10)
            goto L_0x050d
        L_0x0090:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zzf(r9, r8)
            goto L_0x050d
        L_0x00a1:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zzb(r9, r10)
            goto L_0x050d
        L_0x00b2:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x00c3:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zzb(r9, r8)
            goto L_0x050d
        L_0x00d4:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zze(r9, r8)
            goto L_0x050d
        L_0x00e5:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            com.google.android.gms.internal.measurement.zzdw r8 = (com.google.android.gms.internal.measurement.zzdw) r8
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x00f8:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            com.google.android.gms.internal.measurement.zzhf r10 = r13.zza(r7)
            r15.zza(r9, r8, r10)
            goto L_0x050d
        L_0x010d:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            zza(r9, r8, r15)
            goto L_0x050d
        L_0x011e:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = zzf(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x012f:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zzd(r9, r8)
            goto L_0x050d
        L_0x0140:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zzd(r9, r10)
            goto L_0x050d
        L_0x0151:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = zzd(r14, r10)
            r15.zzc(r9, r8)
            goto L_0x050d
        L_0x0162:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zzc(r9, r10)
            goto L_0x050d
        L_0x0173:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = zze(r14, r10)
            r15.zza(r9, r10)
            goto L_0x050d
        L_0x0184:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = zzc(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x0195:
            boolean r10 = r13.zza(r14, r9, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = zzb(r14, r10)
            r15.zza(r9, r10)
            goto L_0x050d
        L_0x01a6:
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            r13.zza(r15, r9, r8, r7)
            goto L_0x050d
        L_0x01b1:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhf r10 = r13.zza(r7)
            com.google.android.gms.internal.measurement.zzhh.zzb(r9, r8, r15, r10)
            goto L_0x050d
        L_0x01c6:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zze(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01d7:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzj(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01e8:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzg(r9, r8, r15, r4)
            goto L_0x050d
        L_0x01f9:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzl(r9, r8, r15, r4)
            goto L_0x050d
        L_0x020a:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzm(r9, r8, r15, r4)
            goto L_0x050d
        L_0x021b:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzi(r9, r8, r15, r4)
            goto L_0x050d
        L_0x022c:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzn(r9, r8, r15, r4)
            goto L_0x050d
        L_0x023d:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzk(r9, r8, r15, r4)
            goto L_0x050d
        L_0x024e:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzf(r9, r8, r15, r4)
            goto L_0x050d
        L_0x025f:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzh(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0270:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzd(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0281:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzc(r9, r8, r15, r4)
            goto L_0x050d
        L_0x0292:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzb(r9, r8, r15, r4)
            goto L_0x050d
        L_0x02a3:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zza(r9, r8, r15, r4)
            goto L_0x050d
        L_0x02b4:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zze(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02c5:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzj(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02d6:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzg(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02e7:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzl(r9, r8, r15, r5)
            goto L_0x050d
        L_0x02f8:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzm(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0309:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzi(r9, r8, r15, r5)
            goto L_0x050d
        L_0x031a:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzb(r9, r8, r15)
            goto L_0x050d
        L_0x032b:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhf r10 = r13.zza(r7)
            com.google.android.gms.internal.measurement.zzhh.zza(r9, r8, r15, r10)
            goto L_0x050d
        L_0x0340:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zza(r9, r8, r15)
            goto L_0x050d
        L_0x0351:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzn(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0362:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzk(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0373:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzf(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0384:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzh(r9, r8, r15, r5)
            goto L_0x050d
        L_0x0395:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzd(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03a6:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzc(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03b7:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zzb(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03c8:
            int[] r9 = r13.zzc
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.zzhh.zza(r9, r8, r15, r5)
            goto L_0x050d
        L_0x03d9:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            com.google.android.gms.internal.measurement.zzhf r10 = r13.zza(r7)
            r15.zzb(r9, r8, r10)
            goto L_0x050d
        L_0x03ee:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r10)
            r15.zze(r9, r10)
            goto L_0x050d
        L_0x03ff:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzid.zza(r14, r10)
            r15.zzf(r9, r8)
            goto L_0x050d
        L_0x0410:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r10)
            r15.zzb(r9, r10)
            goto L_0x050d
        L_0x0421:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzid.zza(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x0432:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzid.zza(r14, r10)
            r15.zzb(r9, r8)
            goto L_0x050d
        L_0x0443:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzid.zza(r14, r10)
            r15.zze(r9, r8)
            goto L_0x050d
        L_0x0454:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            com.google.android.gms.internal.measurement.zzdw r8 = (com.google.android.gms.internal.measurement.zzdw) r8
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x0467:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            com.google.android.gms.internal.measurement.zzhf r10 = r13.zza(r7)
            r15.zza(r9, r8, r10)
            goto L_0x050d
        L_0x047c:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r10)
            zza(r9, r8, r15)
            goto L_0x050d
        L_0x048d:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = com.google.android.gms.internal.measurement.zzid.zzc(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x049e:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzid.zza(r14, r10)
            r15.zzd(r9, r8)
            goto L_0x050d
        L_0x04ae:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r10)
            r15.zzd(r9, r10)
            goto L_0x050d
        L_0x04be:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.zzid.zza(r14, r10)
            r15.zzc(r9, r8)
            goto L_0x050d
        L_0x04ce:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r10)
            r15.zzc(r9, r10)
            goto L_0x050d
        L_0x04de:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r10)
            r15.zza(r9, r10)
            goto L_0x050d
        L_0x04ee:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = com.google.android.gms.internal.measurement.zzid.zzd(r14, r10)
            r15.zza(r9, r8)
            goto L_0x050d
        L_0x04fe:
            boolean r10 = r13.zza(r14, r7)
            if (r10 == 0) goto L_0x050d
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = com.google.android.gms.internal.measurement.zzid.zze(r14, r10)
            r15.zza(r9, r10)
        L_0x050d:
            int r7 = r7 + -3
            goto L_0x0039
        L_0x0511:
            if (r1 == 0) goto L_0x0528
            com.google.android.gms.internal.measurement.zzet<?> r14 = r13.zzr
            r14.zza(r15, r1)
            boolean r14 = r0.hasNext()
            if (r14 == 0) goto L_0x0526
            java.lang.Object r14 = r0.next()
            java.util.Map$Entry r14 = (java.util.Map.Entry) r14
            r1 = r14
            goto L_0x0511
        L_0x0526:
            r1 = r3
            goto L_0x0511
        L_0x0528:
            return
        L_0x0529:
            boolean r0 = r13.zzj
            if (r0 == 0) goto L_0x0a46
            boolean r0 = r13.zzh
            if (r0 == 0) goto L_0x054a
            com.google.android.gms.internal.measurement.zzet<?> r0 = r13.zzr
            com.google.android.gms.internal.measurement.zzeu r0 = r0.zza(r14)
            com.google.android.gms.internal.measurement.zzhg<T, java.lang.Object> r1 = r0.zza
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x054a
            java.util.Iterator r0 = r0.zzd()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x054c
        L_0x054a:
            r0 = r3
            r1 = r0
        L_0x054c:
            int[] r7 = r13.zzc
            int r7 = r7.length
            r8 = r1
            r1 = 0
        L_0x0551:
            if (r1 >= r7) goto L_0x0a29
            int r9 = r13.zzd(r1)
            int[] r10 = r13.zzc
            r10 = r10[r1]
        L_0x055b:
            if (r8 == 0) goto L_0x0579
            com.google.android.gms.internal.measurement.zzet<?> r11 = r13.zzr
            int r11 = r11.zza(r8)
            if (r11 > r10) goto L_0x0579
            com.google.android.gms.internal.measurement.zzet<?> r11 = r13.zzr
            r11.zza(r15, r8)
            boolean r8 = r0.hasNext()
            if (r8 == 0) goto L_0x0577
            java.lang.Object r8 = r0.next()
            java.util.Map$Entry r8 = (java.util.Map.Entry) r8
            goto L_0x055b
        L_0x0577:
            r8 = r3
            goto L_0x055b
        L_0x0579:
            r11 = r9 & r2
            int r11 = r11 >>> 20
            switch(r11) {
                case 0: goto L_0x0a16;
                case 1: goto L_0x0a06;
                case 2: goto L_0x09f6;
                case 3: goto L_0x09e6;
                case 4: goto L_0x09d6;
                case 5: goto L_0x09c6;
                case 6: goto L_0x09b6;
                case 7: goto L_0x09a5;
                case 8: goto L_0x0994;
                case 9: goto L_0x097f;
                case 10: goto L_0x096c;
                case 11: goto L_0x095b;
                case 12: goto L_0x094a;
                case 13: goto L_0x0939;
                case 14: goto L_0x0928;
                case 15: goto L_0x0917;
                case 16: goto L_0x0906;
                case 17: goto L_0x08f1;
                case 18: goto L_0x08e0;
                case 19: goto L_0x08cf;
                case 20: goto L_0x08be;
                case 21: goto L_0x08ad;
                case 22: goto L_0x089c;
                case 23: goto L_0x088b;
                case 24: goto L_0x087a;
                case 25: goto L_0x0869;
                case 26: goto L_0x0858;
                case 27: goto L_0x0843;
                case 28: goto L_0x0832;
                case 29: goto L_0x0821;
                case 30: goto L_0x0810;
                case 31: goto L_0x07ff;
                case 32: goto L_0x07ee;
                case 33: goto L_0x07dd;
                case 34: goto L_0x07cc;
                case 35: goto L_0x07bb;
                case 36: goto L_0x07aa;
                case 37: goto L_0x0799;
                case 38: goto L_0x0788;
                case 39: goto L_0x0777;
                case 40: goto L_0x0766;
                case 41: goto L_0x0755;
                case 42: goto L_0x0744;
                case 43: goto L_0x0733;
                case 44: goto L_0x0722;
                case 45: goto L_0x0711;
                case 46: goto L_0x0700;
                case 47: goto L_0x06ef;
                case 48: goto L_0x06de;
                case 49: goto L_0x06c9;
                case 50: goto L_0x06be;
                case 51: goto L_0x06ad;
                case 52: goto L_0x069c;
                case 53: goto L_0x068b;
                case 54: goto L_0x067a;
                case 55: goto L_0x0669;
                case 56: goto L_0x0658;
                case 57: goto L_0x0647;
                case 58: goto L_0x0636;
                case 59: goto L_0x0625;
                case 60: goto L_0x0610;
                case 61: goto L_0x05fd;
                case 62: goto L_0x05ec;
                case 63: goto L_0x05db;
                case 64: goto L_0x05ca;
                case 65: goto L_0x05b9;
                case 66: goto L_0x05a8;
                case 67: goto L_0x0597;
                case 68: goto L_0x0582;
                default: goto L_0x0580;
            }
        L_0x0580:
            goto L_0x0a25
        L_0x0582:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            com.google.android.gms.internal.measurement.zzhf r11 = r13.zza(r1)
            r15.zzb(r10, r9, r11)
            goto L_0x0a25
        L_0x0597:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zze(r10, r11)
            goto L_0x0a25
        L_0x05a8:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zzf(r10, r9)
            goto L_0x0a25
        L_0x05b9:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zzb(r10, r11)
            goto L_0x0a25
        L_0x05ca:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x05db:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zzb(r10, r9)
            goto L_0x0a25
        L_0x05ec:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zze(r10, r9)
            goto L_0x0a25
        L_0x05fd:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            com.google.android.gms.internal.measurement.zzdw r9 = (com.google.android.gms.internal.measurement.zzdw) r9
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x0610:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            com.google.android.gms.internal.measurement.zzhf r11 = r13.zza(r1)
            r15.zza(r10, r9, r11)
            goto L_0x0a25
        L_0x0625:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            zza(r10, r9, r15)
            goto L_0x0a25
        L_0x0636:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = zzf(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x0647:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zzd(r10, r9)
            goto L_0x0a25
        L_0x0658:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zzd(r10, r11)
            goto L_0x0a25
        L_0x0669:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = zzd(r14, r11)
            r15.zzc(r10, r9)
            goto L_0x0a25
        L_0x067a:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zzc(r10, r11)
            goto L_0x0a25
        L_0x068b:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = zze(r14, r11)
            r15.zza(r10, r11)
            goto L_0x0a25
        L_0x069c:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = zzc(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x06ad:
            boolean r11 = r13.zza(r14, r10, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = zzb(r14, r11)
            r15.zza(r10, r11)
            goto L_0x0a25
        L_0x06be:
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            r13.zza(r15, r10, r9, r1)
            goto L_0x0a25
        L_0x06c9:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhf r11 = r13.zza(r1)
            com.google.android.gms.internal.measurement.zzhh.zzb(r10, r9, r15, r11)
            goto L_0x0a25
        L_0x06de:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zze(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x06ef:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzj(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0700:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzg(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0711:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzl(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0722:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzm(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0733:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzi(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0744:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzn(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0755:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzk(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0766:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzf(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0777:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzh(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0788:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzd(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x0799:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzc(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x07aa:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzb(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x07bb:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zza(r10, r9, r15, r4)
            goto L_0x0a25
        L_0x07cc:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zze(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x07dd:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzj(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x07ee:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzg(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x07ff:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzl(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x0810:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzm(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x0821:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzi(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x0832:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzb(r10, r9, r15)
            goto L_0x0a25
        L_0x0843:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhf r11 = r13.zza(r1)
            com.google.android.gms.internal.measurement.zzhh.zza(r10, r9, r15, r11)
            goto L_0x0a25
        L_0x0858:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zza(r10, r9, r15)
            goto L_0x0a25
        L_0x0869:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzn(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x087a:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzk(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x088b:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzf(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x089c:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzh(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08ad:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzd(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08be:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzc(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08cf:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzb(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08e0:
            int[] r10 = r13.zzc
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zza(r10, r9, r15, r5)
            goto L_0x0a25
        L_0x08f1:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            com.google.android.gms.internal.measurement.zzhf r11 = r13.zza(r1)
            r15.zzb(r10, r9, r11)
            goto L_0x0a25
        L_0x0906:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r11)
            r15.zze(r10, r11)
            goto L_0x0a25
        L_0x0917:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzid.zza(r14, r11)
            r15.zzf(r10, r9)
            goto L_0x0a25
        L_0x0928:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r11)
            r15.zzb(r10, r11)
            goto L_0x0a25
        L_0x0939:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzid.zza(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x094a:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzid.zza(r14, r11)
            r15.zzb(r10, r9)
            goto L_0x0a25
        L_0x095b:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzid.zza(r14, r11)
            r15.zze(r10, r9)
            goto L_0x0a25
        L_0x096c:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            com.google.android.gms.internal.measurement.zzdw r9 = (com.google.android.gms.internal.measurement.zzdw) r9
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x097f:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            com.google.android.gms.internal.measurement.zzhf r11 = r13.zza(r1)
            r15.zza(r10, r9, r11)
            goto L_0x0a25
        L_0x0994:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.zzid.zzf(r14, r11)
            zza(r10, r9, r15)
            goto L_0x0a25
        L_0x09a5:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = com.google.android.gms.internal.measurement.zzid.zzc(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x09b6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzid.zza(r14, r11)
            r15.zzd(r10, r9)
            goto L_0x0a25
        L_0x09c6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r11)
            r15.zzd(r10, r11)
            goto L_0x0a25
        L_0x09d6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.zzid.zza(r14, r11)
            r15.zzc(r10, r9)
            goto L_0x0a25
        L_0x09e6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r11)
            r15.zzc(r10, r11)
            goto L_0x0a25
        L_0x09f6:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.zzid.zzb(r14, r11)
            r15.zza(r10, r11)
            goto L_0x0a25
        L_0x0a06:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = com.google.android.gms.internal.measurement.zzid.zzd(r14, r11)
            r15.zza(r10, r9)
            goto L_0x0a25
        L_0x0a16:
            boolean r11 = r13.zza(r14, r1)
            if (r11 == 0) goto L_0x0a25
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = com.google.android.gms.internal.measurement.zzid.zze(r14, r11)
            r15.zza(r10, r11)
        L_0x0a25:
            int r1 = r1 + 3
            goto L_0x0551
        L_0x0a29:
            if (r8 == 0) goto L_0x0a40
            com.google.android.gms.internal.measurement.zzet<?> r1 = r13.zzr
            r1.zza(r15, r8)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0a3e
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            r8 = r1
            goto L_0x0a29
        L_0x0a3e:
            r8 = r3
            goto L_0x0a29
        L_0x0a40:
            com.google.android.gms.internal.measurement.zzhx<?, ?> r0 = r13.zzq
            zza(r0, r14, r15)
            return
        L_0x0a46:
            r13.zzb(r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.zziq, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.zziq, int]
     candidates:
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<?>, com.google.android.gms.internal.measurement.zziq, com.google.android.gms.internal.measurement.zzhf):void
      com.google.android.gms.internal.measurement.zzhh.zzb(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.zziq, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.zziq, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.zziq, int]
     candidates:
      com.google.android.gms.internal.measurement.zzhh.zza(int, int, java.lang.Object, com.google.android.gms.internal.measurement.zzhx):UB
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<?>, com.google.android.gms.internal.measurement.zziq, com.google.android.gms.internal.measurement.zzhf):void
      com.google.android.gms.internal.measurement.zzhh.zza(com.google.android.gms.internal.measurement.zzgj, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.measurement.zzhh.zza(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.zziq, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04b3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzb(T r19, com.google.android.gms.internal.measurement.zziq r20) throws java.io.IOException {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            boolean r3 = r0.zzh
            if (r3 == 0) goto L_0x0023
            com.google.android.gms.internal.measurement.zzet<?> r3 = r0.zzr
            com.google.android.gms.internal.measurement.zzeu r3 = r3.zza(r1)
            com.google.android.gms.internal.measurement.zzhg<T, java.lang.Object> r5 = r3.zza
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x0023
            java.util.Iterator r3 = r3.zzd()
            java.lang.Object r5 = r3.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            goto L_0x0025
        L_0x0023:
            r3 = 0
            r5 = 0
        L_0x0025:
            r6 = -1
            int[] r7 = r0.zzc
            int r7 = r7.length
            sun.misc.Unsafe r8 = com.google.android.gms.internal.measurement.zzgq.zzb
            r10 = r5
            r5 = 0
            r11 = 0
        L_0x002e:
            if (r5 >= r7) goto L_0x04ad
            int r12 = r0.zzd(r5)
            int[] r13 = r0.zzc
            r14 = r13[r5]
            r15 = 267386880(0xff00000, float:2.3665827E-29)
            r15 = r15 & r12
            int r15 = r15 >>> 20
            boolean r4 = r0.zzj
            r16 = 1048575(0xfffff, float:1.469367E-39)
            if (r4 != 0) goto L_0x0062
            r4 = 17
            if (r15 > r4) goto L_0x0062
            int r4 = r5 + 2
            r4 = r13[r4]
            r13 = r4 & r16
            r17 = r10
            if (r13 == r6) goto L_0x0058
            long r9 = (long) r13
            int r11 = r8.getInt(r1, r9)
            goto L_0x0059
        L_0x0058:
            r13 = r6
        L_0x0059:
            int r4 = r4 >>> 20
            r6 = 1
            int r9 = r6 << r4
            r6 = r13
            r10 = r17
            goto L_0x0067
        L_0x0062:
            r17 = r10
            r10 = r17
            r9 = 0
        L_0x0067:
            if (r10 == 0) goto L_0x0086
            com.google.android.gms.internal.measurement.zzet<?> r4 = r0.zzr
            int r4 = r4.zza(r10)
            if (r4 > r14) goto L_0x0086
            com.google.android.gms.internal.measurement.zzet<?> r4 = r0.zzr
            r4.zza(r2, r10)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0084
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            r10 = r4
            goto L_0x0067
        L_0x0084:
            r10 = 0
            goto L_0x0067
        L_0x0086:
            r4 = r12 & r16
            long r12 = (long) r4
            switch(r15) {
                case 0: goto L_0x049d;
                case 1: goto L_0x0490;
                case 2: goto L_0x0483;
                case 3: goto L_0x0476;
                case 4: goto L_0x0469;
                case 5: goto L_0x045c;
                case 6: goto L_0x044f;
                case 7: goto L_0x0442;
                case 8: goto L_0x0434;
                case 9: goto L_0x0422;
                case 10: goto L_0x0412;
                case 11: goto L_0x0404;
                case 12: goto L_0x03f6;
                case 13: goto L_0x03e8;
                case 14: goto L_0x03da;
                case 15: goto L_0x03cc;
                case 16: goto L_0x03be;
                case 17: goto L_0x03ac;
                case 18: goto L_0x039c;
                case 19: goto L_0x038c;
                case 20: goto L_0x037c;
                case 21: goto L_0x036c;
                case 22: goto L_0x035c;
                case 23: goto L_0x034c;
                case 24: goto L_0x033c;
                case 25: goto L_0x032c;
                case 26: goto L_0x031d;
                case 27: goto L_0x030a;
                case 28: goto L_0x02fb;
                case 29: goto L_0x02eb;
                case 30: goto L_0x02db;
                case 31: goto L_0x02cb;
                case 32: goto L_0x02bb;
                case 33: goto L_0x02ab;
                case 34: goto L_0x029b;
                case 35: goto L_0x028b;
                case 36: goto L_0x027b;
                case 37: goto L_0x026b;
                case 38: goto L_0x025b;
                case 39: goto L_0x024b;
                case 40: goto L_0x023b;
                case 41: goto L_0x022b;
                case 42: goto L_0x021b;
                case 43: goto L_0x020b;
                case 44: goto L_0x01fb;
                case 45: goto L_0x01eb;
                case 46: goto L_0x01db;
                case 47: goto L_0x01cb;
                case 48: goto L_0x01bb;
                case 49: goto L_0x01a8;
                case 50: goto L_0x019f;
                case 51: goto L_0x0190;
                case 52: goto L_0x0181;
                case 53: goto L_0x0172;
                case 54: goto L_0x0163;
                case 55: goto L_0x0154;
                case 56: goto L_0x0145;
                case 57: goto L_0x0136;
                case 58: goto L_0x0127;
                case 59: goto L_0x0118;
                case 60: goto L_0x0105;
                case 61: goto L_0x00f5;
                case 62: goto L_0x00e7;
                case 63: goto L_0x00d9;
                case 64: goto L_0x00cb;
                case 65: goto L_0x00bd;
                case 66: goto L_0x00af;
                case 67: goto L_0x00a1;
                case 68: goto L_0x008f;
                default: goto L_0x008c;
            }
        L_0x008c:
            r15 = 0
            goto L_0x04a9
        L_0x008f:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.zzhf r9 = r0.zza(r5)
            r2.zzb(r14, r4, r9)
            goto L_0x008c
        L_0x00a1:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zze(r1, r12)
            r2.zze(r14, r12)
            goto L_0x008c
        L_0x00af:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzd(r1, r12)
            r2.zzf(r14, r4)
            goto L_0x008c
        L_0x00bd:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zze(r1, r12)
            r2.zzb(r14, r12)
            goto L_0x008c
        L_0x00cb:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzd(r1, r12)
            r2.zza(r14, r4)
            goto L_0x008c
        L_0x00d9:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzd(r1, r12)
            r2.zzb(r14, r4)
            goto L_0x008c
        L_0x00e7:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzd(r1, r12)
            r2.zze(r14, r4)
            goto L_0x008c
        L_0x00f5:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.zzdw r4 = (com.google.android.gms.internal.measurement.zzdw) r4
            r2.zza(r14, r4)
            goto L_0x008c
        L_0x0105:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.zzhf r9 = r0.zza(r5)
            r2.zza(r14, r4, r9)
            goto L_0x008c
        L_0x0118:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            zza(r14, r4, r2)
            goto L_0x008c
        L_0x0127:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            boolean r4 = zzf(r1, r12)
            r2.zza(r14, r4)
            goto L_0x008c
        L_0x0136:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzd(r1, r12)
            r2.zzd(r14, r4)
            goto L_0x008c
        L_0x0145:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zze(r1, r12)
            r2.zzd(r14, r12)
            goto L_0x008c
        L_0x0154:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = zzd(r1, r12)
            r2.zzc(r14, r4)
            goto L_0x008c
        L_0x0163:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zze(r1, r12)
            r2.zzc(r14, r12)
            goto L_0x008c
        L_0x0172:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = zze(r1, r12)
            r2.zza(r14, r12)
            goto L_0x008c
        L_0x0181:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            float r4 = zzc(r1, r12)
            r2.zza(r14, r4)
            goto L_0x008c
        L_0x0190:
            boolean r4 = r0.zza(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            double r12 = zzb(r1, r12)
            r2.zza(r14, r12)
            goto L_0x008c
        L_0x019f:
            java.lang.Object r4 = r8.getObject(r1, r12)
            r0.zza(r2, r14, r4, r5)
            goto L_0x008c
        L_0x01a8:
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhf r12 = r0.zza(r5)
            com.google.android.gms.internal.measurement.zzhh.zzb(r4, r9, r2, r12)
            goto L_0x008c
        L_0x01bb:
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r14 = 1
            com.google.android.gms.internal.measurement.zzhh.zze(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01cb:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzj(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01db:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzg(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01eb:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzl(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01fb:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzm(r4, r9, r2, r14)
            goto L_0x008c
        L_0x020b:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzi(r4, r9, r2, r14)
            goto L_0x008c
        L_0x021b:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzn(r4, r9, r2, r14)
            goto L_0x008c
        L_0x022b:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzk(r4, r9, r2, r14)
            goto L_0x008c
        L_0x023b:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzf(r4, r9, r2, r14)
            goto L_0x008c
        L_0x024b:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzh(r4, r9, r2, r14)
            goto L_0x008c
        L_0x025b:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzd(r4, r9, r2, r14)
            goto L_0x008c
        L_0x026b:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzc(r4, r9, r2, r14)
            goto L_0x008c
        L_0x027b:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzb(r4, r9, r2, r14)
            goto L_0x008c
        L_0x028b:
            r14 = 1
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zza(r4, r9, r2, r14)
            goto L_0x008c
        L_0x029b:
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r14 = 0
            com.google.android.gms.internal.measurement.zzhh.zze(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02ab:
            r14 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzj(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02bb:
            r14 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzg(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02cb:
            r14 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzl(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02db:
            r14 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzm(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02eb:
            r14 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzi(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02fb:
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzb(r4, r9, r2)
            goto L_0x008c
        L_0x030a:
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhf r12 = r0.zza(r5)
            com.google.android.gms.internal.measurement.zzhh.zza(r4, r9, r2, r12)
            goto L_0x008c
        L_0x031d:
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zza(r4, r9, r2)
            goto L_0x008c
        L_0x032c:
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r15 = 0
            com.google.android.gms.internal.measurement.zzhh.zzn(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x033c:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzk(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x034c:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzf(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x035c:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzh(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x036c:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzd(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x037c:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzc(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x038c:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zzb(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x039c:
            r15 = 0
            int[] r4 = r0.zzc
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.zzhh.zza(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x03ac:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.zzhf r9 = r0.zza(r5)
            r2.zzb(r14, r4, r9)
            goto L_0x04a9
        L_0x03be:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zze(r14, r12)
            goto L_0x04a9
        L_0x03cc:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzf(r14, r4)
            goto L_0x04a9
        L_0x03da:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzb(r14, r12)
            goto L_0x04a9
        L_0x03e8:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zza(r14, r4)
            goto L_0x04a9
        L_0x03f6:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzb(r14, r4)
            goto L_0x04a9
        L_0x0404:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zze(r14, r4)
            goto L_0x04a9
        L_0x0412:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.zzdw r4 = (com.google.android.gms.internal.measurement.zzdw) r4
            r2.zza(r14, r4)
            goto L_0x04a9
        L_0x0422:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.zzhf r9 = r0.zza(r5)
            r2.zza(r14, r4, r9)
            goto L_0x04a9
        L_0x0434:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            zza(r14, r4, r2)
            goto L_0x04a9
        L_0x0442:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            boolean r4 = com.google.android.gms.internal.measurement.zzid.zzc(r1, r12)
            r2.zza(r14, r4)
            goto L_0x04a9
        L_0x044f:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzd(r14, r4)
            goto L_0x04a9
        L_0x045c:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzd(r14, r12)
            goto L_0x04a9
        L_0x0469:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.zzc(r14, r4)
            goto L_0x04a9
        L_0x0476:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zzc(r14, r12)
            goto L_0x04a9
        L_0x0483:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.zza(r14, r12)
            goto L_0x04a9
        L_0x0490:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            float r4 = com.google.android.gms.internal.measurement.zzid.zzd(r1, r12)
            r2.zza(r14, r4)
            goto L_0x04a9
        L_0x049d:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            double r12 = com.google.android.gms.internal.measurement.zzid.zze(r1, r12)
            r2.zza(r14, r12)
        L_0x04a9:
            int r5 = r5 + 3
            goto L_0x002e
        L_0x04ad:
            r17 = r10
            r4 = r17
        L_0x04b1:
            if (r4 == 0) goto L_0x04c7
            com.google.android.gms.internal.measurement.zzet<?> r5 = r0.zzr
            r5.zza(r2, r4)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x04c5
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            goto L_0x04b1
        L_0x04c5:
            r4 = 0
            goto L_0x04b1
        L_0x04c7:
            com.google.android.gms.internal.measurement.zzhx<?, ?> r3 = r0.zzq
            zza(r3, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void");
    }

    private final <K, V> void zza(zziq zziq, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zziq.zza(i, this.zzs.zzb(zzb(i2)), this.zzs.zzc(obj));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
     arg types: [UT, com.google.android.gms.internal.measurement.zziq]
     candidates:
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzhc):boolean
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void */
    private static <UT, UB> void zza(zzhx<UT, UB> zzhx, T t, zziq zziq) throws IOException {
        zzhx.zza((Object) zzhx.zzb(t), zziq);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzhc):boolean
     arg types: [?, com.google.android.gms.internal.measurement.zzhc]
     candidates:
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzhc):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.zzgq.zzb(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhf.zzb(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzgq.zzb(java.lang.Object, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zza(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object):int
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
     arg types: [T, long, long]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:148|149|(1:151)|152|(5:174|154|(2:157|155)|256|(2:159|264)(1:265))(1:252)) */
    /* JADX WARNING: Code restructure failed: missing block: B:149:?, code lost:
        r7.zza(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x05a0, code lost:
        if (r10 == null) goto L_0x05a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x05a2, code lost:
        r10 = r7.zzc(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x05ab, code lost:
        if (r7.zza((java.lang.Object) r10, r14) == false) goto L_0x05ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x05ad, code lost:
        r14 = r12.zzm;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x05b1, code lost:
        if (r14 < r12.zzn) goto L_0x05b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x05b3, code lost:
        r10 = zza(r13, r12.zzl[r14], r10, r7);
        r14 = r14 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x05be, code lost:
        if (r10 != null) goto L_0x05c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x05c0, code lost:
        r7.zzb(r13, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:?, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:148:0x059d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r13, com.google.android.gms.internal.measurement.zzhc r14, com.google.android.gms.internal.measurement.zzer r15) throws java.io.IOException {
        /*
            r12 = this;
            if (r15 == 0) goto L_0x05dc
            com.google.android.gms.internal.measurement.zzhx<?, ?> r7 = r12.zzq
            com.google.android.gms.internal.measurement.zzet<?> r8 = r12.zzr
            r9 = 0
            r0 = r9
            r10 = r0
        L_0x0009:
            int r1 = r14.zza()     // Catch:{ all -> 0x05c4 }
            int r2 = r12.zzg(r1)     // Catch:{ all -> 0x05c4 }
            if (r2 >= 0) goto L_0x0077
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r1 != r2) goto L_0x002f
            int r14 = r12.zzm
        L_0x001a:
            int r15 = r12.zzn
            if (r14 >= r15) goto L_0x0029
            int[] r15 = r12.zzl
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x001a
        L_0x0029:
            if (r10 == 0) goto L_0x002e
            r7.zzb(r13, r10)
        L_0x002e:
            return
        L_0x002f:
            boolean r2 = r12.zzh     // Catch:{ all -> 0x05c4 }
            if (r2 != 0) goto L_0x0035
            r2 = r9
            goto L_0x003c
        L_0x0035:
            com.google.android.gms.internal.measurement.zzgm r2 = r12.zzg     // Catch:{ all -> 0x05c4 }
            java.lang.Object r1 = r8.zza(r15, r2, r1)     // Catch:{ all -> 0x05c4 }
            r2 = r1
        L_0x003c:
            if (r2 == 0) goto L_0x0051
            if (r0 != 0) goto L_0x0044
            com.google.android.gms.internal.measurement.zzeu r0 = r8.zzb(r13)     // Catch:{ all -> 0x05c4 }
        L_0x0044:
            r11 = r0
            r0 = r8
            r1 = r14
            r3 = r15
            r4 = r11
            r5 = r10
            r6 = r7
            java.lang.Object r10 = r0.zza(r1, r2, r3, r4, r5, r6)     // Catch:{ all -> 0x05c4 }
            r0 = r11
            goto L_0x0009
        L_0x0051:
            r7.zza(r14)     // Catch:{ all -> 0x05c4 }
            if (r10 != 0) goto L_0x005a
            java.lang.Object r10 = r7.zzc(r13)     // Catch:{ all -> 0x05c4 }
        L_0x005a:
            boolean r1 = r7.zza(r10, r14)     // Catch:{ all -> 0x05c4 }
            if (r1 != 0) goto L_0x0009
            int r14 = r12.zzm
        L_0x0062:
            int r15 = r12.zzn
            if (r14 >= r15) goto L_0x0071
            int[] r15 = r12.zzl
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x0062
        L_0x0071:
            if (r10 == 0) goto L_0x0076
            r7.zzb(r13, r10)
        L_0x0076:
            return
        L_0x0077:
            int r3 = r12.zzd(r2)     // Catch:{ all -> 0x05c4 }
            r4 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r3
            int r4 = r4 >>> 20
            r5 = 1048575(0xfffff, float:1.469367E-39)
            switch(r4) {
                case 0: goto L_0x0571;
                case 1: goto L_0x0562;
                case 2: goto L_0x0553;
                case 3: goto L_0x0544;
                case 4: goto L_0x0535;
                case 5: goto L_0x0526;
                case 6: goto L_0x0517;
                case 7: goto L_0x0508;
                case 8: goto L_0x0500;
                case 9: goto L_0x04cf;
                case 10: goto L_0x04c0;
                case 11: goto L_0x04b1;
                case 12: goto L_0x048f;
                case 13: goto L_0x0480;
                case 14: goto L_0x0471;
                case 15: goto L_0x0462;
                case 16: goto L_0x0453;
                case 17: goto L_0x0422;
                case 18: goto L_0x0414;
                case 19: goto L_0x0406;
                case 20: goto L_0x03f8;
                case 21: goto L_0x03ea;
                case 22: goto L_0x03dc;
                case 23: goto L_0x03ce;
                case 24: goto L_0x03c0;
                case 25: goto L_0x03b2;
                case 26: goto L_0x0390;
                case 27: goto L_0x037e;
                case 28: goto L_0x0370;
                case 29: goto L_0x0362;
                case 30: goto L_0x034d;
                case 31: goto L_0x033f;
                case 32: goto L_0x0331;
                case 33: goto L_0x0323;
                case 34: goto L_0x0315;
                case 35: goto L_0x0307;
                case 36: goto L_0x02f9;
                case 37: goto L_0x02eb;
                case 38: goto L_0x02dd;
                case 39: goto L_0x02cf;
                case 40: goto L_0x02c1;
                case 41: goto L_0x02b3;
                case 42: goto L_0x02a5;
                case 43: goto L_0x0297;
                case 44: goto L_0x0282;
                case 45: goto L_0x0274;
                case 46: goto L_0x0266;
                case 47: goto L_0x0258;
                case 48: goto L_0x024a;
                case 49: goto L_0x0238;
                case 50: goto L_0x01f6;
                case 51: goto L_0x01e4;
                case 52: goto L_0x01d2;
                case 53: goto L_0x01c0;
                case 54: goto L_0x01ae;
                case 55: goto L_0x019c;
                case 56: goto L_0x018a;
                case 57: goto L_0x0178;
                case 58: goto L_0x0166;
                case 59: goto L_0x015e;
                case 60: goto L_0x012d;
                case 61: goto L_0x011f;
                case 62: goto L_0x010d;
                case 63: goto L_0x00e8;
                case 64: goto L_0x00d6;
                case 65: goto L_0x00c4;
                case 66: goto L_0x00b2;
                case 67: goto L_0x00a0;
                case 68: goto L_0x008e;
                default: goto L_0x0086;
            }
        L_0x0086:
            if (r10 != 0) goto L_0x0580
            java.lang.Object r10 = r7.zza()     // Catch:{ zzfp -> 0x059d }
            goto L_0x0580
        L_0x008e:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzhf r5 = r12.zza(r2)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r5 = r14.zzb(r5, r15)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x00a0:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzt()     // Catch:{ zzfp -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x00b2:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            int r5 = r14.zzs()     // Catch:{ zzfp -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x00c4:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzr()     // Catch:{ zzfp -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x00d6:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            int r5 = r14.zzq()     // Catch:{ zzfp -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x00e8:
            int r4 = r14.zzp()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzfi r6 = r12.zzc(r2)     // Catch:{ zzfp -> 0x059d }
            if (r6 == 0) goto L_0x00ff
            boolean r6 = r6.zza(r4)     // Catch:{ zzfp -> 0x059d }
            if (r6 == 0) goto L_0x00f9
            goto L_0x00ff
        L_0x00f9:
            java.lang.Object r10 = com.google.android.gms.internal.measurement.zzhh.zza(r1, r4, r10, r7)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x00ff:
            r3 = r3 & r5
            long r5 = (long) r3     // Catch:{ zzfp -> 0x059d }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r5, r3)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x010d:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            int r5 = r14.zzo()     // Catch:{ zzfp -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x011f:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzdw r5 = r14.zzn()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x012d:
            boolean r4 = r12.zza(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            if (r4 == 0) goto L_0x0149
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzid.zzf(r13, r3)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzhf r6 = r12.zza(r2)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r6 = r14.zza(r6, r15)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r5 = com.google.android.gms.internal.measurement.zzfh.zza(r5, r6)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0159
        L_0x0149:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzhf r5 = r12.zza(r2)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r5 = r14.zza(r5, r15)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
        L_0x0159:
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x015e:
            r12.zza(r13, r3, r14)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0166:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            boolean r5 = r14.zzk()     // Catch:{ zzfp -> 0x059d }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0178:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            int r5 = r14.zzj()     // Catch:{ zzfp -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x018a:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzi()     // Catch:{ zzfp -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x019c:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            int r5 = r14.zzh()     // Catch:{ zzfp -> 0x059d }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x01ae:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzf()     // Catch:{ zzfp -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x01c0:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzg()     // Catch:{ zzfp -> 0x059d }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x01d2:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            float r5 = r14.zze()     // Catch:{ zzfp -> 0x059d }
            java.lang.Float r5 = java.lang.Float.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x01e4:
            r3 = r3 & r5
            long r3 = (long) r3     // Catch:{ zzfp -> 0x059d }
            double r5 = r14.zzd()     // Catch:{ zzfp -> 0x059d }
            java.lang.Double r5 = java.lang.Double.valueOf(r5)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r1, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x01f6:
            java.lang.Object r1 = r12.zzb(r2)     // Catch:{ zzfp -> 0x059d }
            int r2 = r12.zzd(r2)     // Catch:{ zzfp -> 0x059d }
            r2 = r2 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r4 = com.google.android.gms.internal.measurement.zzid.zzf(r13, r2)     // Catch:{ zzfp -> 0x059d }
            if (r4 != 0) goto L_0x0210
            com.google.android.gms.internal.measurement.zzgj r4 = r12.zzs     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r4 = r4.zzf(r1)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r2, r4)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0227
        L_0x0210:
            com.google.android.gms.internal.measurement.zzgj r5 = r12.zzs     // Catch:{ zzfp -> 0x059d }
            boolean r5 = r5.zzd(r4)     // Catch:{ zzfp -> 0x059d }
            if (r5 == 0) goto L_0x0227
            com.google.android.gms.internal.measurement.zzgj r5 = r12.zzs     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r5 = r5.zzf(r1)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzgj r6 = r12.zzs     // Catch:{ zzfp -> 0x059d }
            r6.zza(r5, r4)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r2, r5)     // Catch:{ zzfp -> 0x059d }
            r4 = r5
        L_0x0227:
            com.google.android.gms.internal.measurement.zzgj r2 = r12.zzs     // Catch:{ zzfp -> 0x059d }
            java.util.Map r2 = r2.zza(r4)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzgj r3 = r12.zzs     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzgh r1 = r3.zzb(r1)     // Catch:{ zzfp -> 0x059d }
            r14.zza(r2, r1, r15)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0238:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzhf r1 = r12.zza(r2)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzfw r2 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            java.util.List r2 = r2.zza(r13, r3)     // Catch:{ zzfp -> 0x059d }
            r14.zzb(r2, r1, r15)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x024a:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzq(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0258:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzp(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0266:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzo(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0274:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzn(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0282:
            com.google.android.gms.internal.measurement.zzfw r4 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r3 = r3 & r5
            long r5 = (long) r3     // Catch:{ zzfp -> 0x059d }
            java.util.List r3 = r4.zza(r13, r5)     // Catch:{ zzfp -> 0x059d }
            r14.zzm(r3)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzfi r2 = r12.zzc(r2)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r10 = com.google.android.gms.internal.measurement.zzhh.zza(r1, r3, r2, r10, r7)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0297:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzl(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x02a5:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzh(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x02b3:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzg(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x02c1:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzf(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x02cf:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zze(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x02dd:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzc(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x02eb:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzd(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x02f9:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzb(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0307:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zza(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0315:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzq(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0323:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzp(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0331:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzo(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x033f:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzn(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x034d:
            com.google.android.gms.internal.measurement.zzfw r4 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r3 = r3 & r5
            long r5 = (long) r3     // Catch:{ zzfp -> 0x059d }
            java.util.List r3 = r4.zza(r13, r5)     // Catch:{ zzfp -> 0x059d }
            r14.zzm(r3)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzfi r2 = r12.zzc(r2)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r10 = com.google.android.gms.internal.measurement.zzhh.zza(r1, r3, r2, r10, r7)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0362:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzl(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0370:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzk(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x037e:
            com.google.android.gms.internal.measurement.zzhf r1 = r12.zza(r2)     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzfw r4 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            java.util.List r2 = r4.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zza(r2, r1, r15)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0390:
            boolean r1 = zzf(r3)     // Catch:{ zzfp -> 0x059d }
            if (r1 == 0) goto L_0x03a4
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzj(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x03a4:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzi(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x03b2:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzh(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x03c0:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzg(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x03ce:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzf(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x03dc:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zze(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x03ea:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzc(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x03f8:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzd(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0406:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zzb(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0414:
            com.google.android.gms.internal.measurement.zzfw r1 = r12.zzp     // Catch:{ zzfp -> 0x059d }
            r2 = r3 & r5
            long r2 = (long) r2     // Catch:{ zzfp -> 0x059d }
            java.util.List r1 = r1.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            r14.zza(r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0422:
            boolean r1 = r12.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            if (r1 == 0) goto L_0x0440
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzid.zzf(r13, r3)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzhf r2 = r12.zza(r2)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r2 = r14.zzb(r2, r15)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzfh.zza(r1, r2)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0440:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzhf r1 = r12.zza(r2)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r1 = r14.zzb(r1, r15)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0453:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzt()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0462:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            int r1 = r14.zzs()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0471:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzr()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0480:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            int r1 = r14.zzq()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x048f:
            int r4 = r14.zzp()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzfi r6 = r12.zzc(r2)     // Catch:{ zzfp -> 0x059d }
            if (r6 == 0) goto L_0x04a6
            boolean r6 = r6.zza(r4)     // Catch:{ zzfp -> 0x059d }
            if (r6 == 0) goto L_0x04a0
            goto L_0x04a6
        L_0x04a0:
            java.lang.Object r10 = com.google.android.gms.internal.measurement.zzhh.zza(r1, r4, r10, r7)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x04a6:
            r1 = r3 & r5
            long r5 = (long) r1     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r5, r4)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x04b1:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            int r1 = r14.zzo()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x04c0:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzdw r1 = r14.zzn()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x04cf:
            boolean r1 = r12.zza(r13, r2)     // Catch:{ zzfp -> 0x059d }
            if (r1 == 0) goto L_0x04ed
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzid.zzf(r13, r3)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzhf r2 = r12.zza(r2)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r2 = r14.zza(r2, r15)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzfh.zza(r1, r2)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x04ed:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzhf r1 = r12.zza(r2)     // Catch:{ zzfp -> 0x059d }
            java.lang.Object r1 = r14.zza(r1, r15)     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0500:
            r12.zza(r13, r3, r14)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0508:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            boolean r1 = r14.zzk()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0517:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            int r1 = r14.zzj()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0526:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzi()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0535:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            int r1 = r14.zzh()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0544:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzf()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0553:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            long r5 = r14.zzg()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0562:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            float r1 = r14.zze()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r1)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0571:
            r1 = r3 & r5
            long r3 = (long) r1     // Catch:{ zzfp -> 0x059d }
            double r5 = r14.zzd()     // Catch:{ zzfp -> 0x059d }
            com.google.android.gms.internal.measurement.zzid.zza(r13, r3, r5)     // Catch:{ zzfp -> 0x059d }
            r12.zzb(r13, r2)     // Catch:{ zzfp -> 0x059d }
            goto L_0x0009
        L_0x0580:
            boolean r1 = r7.zza(r10, r14)     // Catch:{ zzfp -> 0x059d }
            if (r1 != 0) goto L_0x0009
            int r14 = r12.zzm
        L_0x0588:
            int r15 = r12.zzn
            if (r14 >= r15) goto L_0x0597
            int[] r15 = r12.zzl
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x0588
        L_0x0597:
            if (r10 == 0) goto L_0x059c
            r7.zzb(r13, r10)
        L_0x059c:
            return
        L_0x059d:
            r7.zza(r14)     // Catch:{ all -> 0x05c4 }
            if (r10 != 0) goto L_0x05a7
            java.lang.Object r1 = r7.zzc(r13)     // Catch:{ all -> 0x05c4 }
            r10 = r1
        L_0x05a7:
            boolean r1 = r7.zza(r10, r14)     // Catch:{ all -> 0x05c4 }
            if (r1 != 0) goto L_0x0009
            int r14 = r12.zzm
        L_0x05af:
            int r15 = r12.zzn
            if (r14 >= r15) goto L_0x05be
            int[] r15 = r12.zzl
            r15 = r15[r14]
            java.lang.Object r10 = r12.zza(r13, r15, r10, r7)
            int r14 = r14 + 1
            goto L_0x05af
        L_0x05be:
            if (r10 == 0) goto L_0x05c3
            r7.zzb(r13, r10)
        L_0x05c3:
            return
        L_0x05c4:
            r14 = move-exception
            int r15 = r12.zzm
        L_0x05c7:
            int r0 = r12.zzn
            if (r15 >= r0) goto L_0x05d6
            int[] r0 = r12.zzl
            r0 = r0[r15]
            java.lang.Object r10 = r12.zza(r13, r0, r10, r7)
            int r15 = r15 + 1
            goto L_0x05c7
        L_0x05d6:
            if (r10 == 0) goto L_0x05db
            r7.zzb(r13, r10)
        L_0x05db:
            throw r14
        L_0x05dc:
            java.lang.NullPointerException r13 = new java.lang.NullPointerException
            r13.<init>()
            goto L_0x05e3
        L_0x05e2:
            throw r13
        L_0x05e3:
            goto L_0x05e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzhc, com.google.android.gms.internal.measurement.zzer):void");
    }

    private static zzhw zze(Object obj) {
        zzfe zzfe = (zzfe) obj;
        zzhw zzhw = zzfe.zzb;
        if (zzhw != zzhw.zza()) {
            return zzhw;
        }
        zzhw zzb2 = zzhw.zzb();
        zzfe.zzb = zzb2;
        return zzb2;
    }

    private static int zza(byte[] bArr, int i, int i2, zzik zzik, Class<?> cls, zzdr zzdr) throws IOException {
        switch (zzgt.zza[zzik.ordinal()]) {
            case 1:
                int zzb2 = zzds.zzb(bArr, i, zzdr);
                zzdr.zzc = Boolean.valueOf(zzdr.zzb != 0);
                return zzb2;
            case 2:
                return zzds.zze(bArr, i, zzdr);
            case 3:
                zzdr.zzc = Double.valueOf(zzds.zzc(bArr, i));
                return i + 8;
            case 4:
            case 5:
                zzdr.zzc = Integer.valueOf(zzds.zza(bArr, i));
                return i + 4;
            case 6:
            case 7:
                zzdr.zzc = Long.valueOf(zzds.zzb(bArr, i));
                return i + 8;
            case 8:
                zzdr.zzc = Float.valueOf(zzds.zzd(bArr, i));
                return i + 4;
            case 9:
            case 10:
            case 11:
                int zza2 = zzds.zza(bArr, i, zzdr);
                zzdr.zzc = Integer.valueOf(zzdr.zza);
                return zza2;
            case 12:
            case 13:
                int zzb3 = zzds.zzb(bArr, i, zzdr);
                zzdr.zzc = Long.valueOf(zzdr.zzb);
                return zzb3;
            case 14:
                return zzds.zza(zzhb.zza().zza((Class) cls), bArr, i, i2, zzdr);
            case 15:
                int zza3 = zzds.zza(bArr, i, zzdr);
                zzdr.zzc = Integer.valueOf(zzei.zze(zzdr.zza));
                return zza3;
            case 16:
                int zzb4 = zzds.zzb(bArr, i, zzdr);
                zzdr.zzc = Long.valueOf(zzei.zza(zzdr.zzb));
                return zzb4;
            case 17:
                return zzds.zzd(bArr, i, zzdr);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0422 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01eb  */
    private final int zza(T r17, byte[] r18, int r19, int r20, int r21, int r22, int r23, int r24, long r25, int r27, long r28, com.google.android.gms.internal.measurement.zzdr r30) throws java.io.IOException {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r3 = r18
            r4 = r19
            r5 = r20
            r2 = r21
            r6 = r23
            r8 = r24
            r9 = r28
            r7 = r30
            sun.misc.Unsafe r11 = com.google.android.gms.internal.measurement.zzgq.zzb
            java.lang.Object r11 = r11.getObject(r1, r9)
            com.google.android.gms.internal.measurement.zzfn r11 = (com.google.android.gms.internal.measurement.zzfn) r11
            boolean r12 = r11.zza()
            r13 = 1
            if (r12 != 0) goto L_0x0036
            int r12 = r11.size()
            if (r12 != 0) goto L_0x002c
            r12 = 10
            goto L_0x002d
        L_0x002c:
            int r12 = r12 << r13
        L_0x002d:
            com.google.android.gms.internal.measurement.zzfn r11 = r11.zza(r12)
            sun.misc.Unsafe r12 = com.google.android.gms.internal.measurement.zzgq.zzb
            r12.putObject(r1, r9, r11)
        L_0x0036:
            r9 = 5
            r14 = 0
            r10 = 2
            switch(r27) {
                case 18: goto L_0x03e4;
                case 19: goto L_0x03a6;
                case 20: goto L_0x0365;
                case 21: goto L_0x0365;
                case 22: goto L_0x034b;
                case 23: goto L_0x030c;
                case 24: goto L_0x02cd;
                case 25: goto L_0x0276;
                case 26: goto L_0x01c3;
                case 27: goto L_0x01a9;
                case 28: goto L_0x0151;
                case 29: goto L_0x034b;
                case 30: goto L_0x0119;
                case 31: goto L_0x02cd;
                case 32: goto L_0x030c;
                case 33: goto L_0x00cc;
                case 34: goto L_0x007f;
                case 35: goto L_0x03e4;
                case 36: goto L_0x03a6;
                case 37: goto L_0x0365;
                case 38: goto L_0x0365;
                case 39: goto L_0x034b;
                case 40: goto L_0x030c;
                case 41: goto L_0x02cd;
                case 42: goto L_0x0276;
                case 43: goto L_0x034b;
                case 44: goto L_0x0119;
                case 45: goto L_0x02cd;
                case 46: goto L_0x030c;
                case 47: goto L_0x00cc;
                case 48: goto L_0x007f;
                case 49: goto L_0x003f;
                default: goto L_0x003d;
            }
        L_0x003d:
            goto L_0x0422
        L_0x003f:
            r1 = 3
            if (r6 != r1) goto L_0x0422
            com.google.android.gms.internal.measurement.zzhf r1 = r0.zza(r8)
            r6 = r2 & -8
            r6 = r6 | 4
            r22 = r1
            r23 = r18
            r24 = r19
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.zzc
            r11.add(r8)
        L_0x005f:
            if (r4 >= r5) goto L_0x0422
            int r8 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r9 = r7.zza
            if (r2 != r9) goto L_0x0422
            r22 = r1
            r23 = r18
            r24 = r8
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.zzc
            r11.add(r8)
            goto L_0x005f
        L_0x007f:
            if (r6 != r10) goto L_0x00a3
            com.google.android.gms.internal.measurement.zzga r11 = (com.google.android.gms.internal.measurement.zzga) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r2 = r7.zza
            int r2 = r2 + r1
        L_0x008a:
            if (r1 >= r2) goto L_0x009a
            int r1 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r1, r7)
            long r4 = r7.zzb
            long r4 = com.google.android.gms.internal.measurement.zzei.zza(r4)
            r11.zza(r4)
            goto L_0x008a
        L_0x009a:
            if (r1 != r2) goto L_0x009e
            goto L_0x0423
        L_0x009e:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x00a3:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.measurement.zzga r11 = (com.google.android.gms.internal.measurement.zzga) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r4, r7)
            long r8 = r7.zzb
            long r8 = com.google.android.gms.internal.measurement.zzei.zza(r8)
            r11.zza(r8)
        L_0x00b4:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1, r7)
            int r6 = r7.zza
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r4, r7)
            long r8 = r7.zzb
            long r8 = com.google.android.gms.internal.measurement.zzei.zza(r8)
            r11.zza(r8)
            goto L_0x00b4
        L_0x00cc:
            if (r6 != r10) goto L_0x00f0
            com.google.android.gms.internal.measurement.zzff r11 = (com.google.android.gms.internal.measurement.zzff) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r2 = r7.zza
            int r2 = r2 + r1
        L_0x00d7:
            if (r1 >= r2) goto L_0x00e7
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1, r7)
            int r4 = r7.zza
            int r4 = com.google.android.gms.internal.measurement.zzei.zze(r4)
            r11.zzd(r4)
            goto L_0x00d7
        L_0x00e7:
            if (r1 != r2) goto L_0x00eb
            goto L_0x0423
        L_0x00eb:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x00f0:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.measurement.zzff r11 = (com.google.android.gms.internal.measurement.zzff) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r4 = r7.zza
            int r4 = com.google.android.gms.internal.measurement.zzei.zze(r4)
            r11.zzd(r4)
        L_0x0101:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1, r7)
            int r6 = r7.zza
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r4 = r7.zza
            int r4 = com.google.android.gms.internal.measurement.zzei.zze(r4)
            r11.zzd(r4)
            goto L_0x0101
        L_0x0119:
            if (r6 != r10) goto L_0x0120
            int r2 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r11, r7)
            goto L_0x0131
        L_0x0120:
            if (r6 != 0) goto L_0x0422
            r2 = r21
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r11
            r7 = r30
            int r2 = com.google.android.gms.internal.measurement.zzds.zza(r2, r3, r4, r5, r6, r7)
        L_0x0131:
            com.google.android.gms.internal.measurement.zzfe r1 = (com.google.android.gms.internal.measurement.zzfe) r1
            com.google.android.gms.internal.measurement.zzhw r3 = r1.zzb
            com.google.android.gms.internal.measurement.zzhw r4 = com.google.android.gms.internal.measurement.zzhw.zza()
            if (r3 != r4) goto L_0x013c
            r3 = 0
        L_0x013c:
            com.google.android.gms.internal.measurement.zzfi r4 = r0.zzc(r8)
            com.google.android.gms.internal.measurement.zzhx<?, ?> r5 = r0.zzq
            r6 = r22
            java.lang.Object r3 = com.google.android.gms.internal.measurement.zzhh.zza(r6, r11, r4, r3, r5)
            com.google.android.gms.internal.measurement.zzhw r3 = (com.google.android.gms.internal.measurement.zzhw) r3
            if (r3 == 0) goto L_0x014e
            r1.zzb = r3
        L_0x014e:
            r1 = r2
            goto L_0x0423
        L_0x0151:
            if (r6 != r10) goto L_0x0422
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r4 = r7.zza
            if (r4 < 0) goto L_0x01a4
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x019f
            if (r4 != 0) goto L_0x0167
            com.google.android.gms.internal.measurement.zzdw r4 = com.google.android.gms.internal.measurement.zzdw.zza
            r11.add(r4)
            goto L_0x016f
        L_0x0167:
            com.google.android.gms.internal.measurement.zzdw r6 = com.google.android.gms.internal.measurement.zzdw.zza(r3, r1, r4)
            r11.add(r6)
        L_0x016e:
            int r1 = r1 + r4
        L_0x016f:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1, r7)
            int r6 = r7.zza
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r4 = r7.zza
            if (r4 < 0) goto L_0x019a
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x0195
            if (r4 != 0) goto L_0x018d
            com.google.android.gms.internal.measurement.zzdw r4 = com.google.android.gms.internal.measurement.zzdw.zza
            r11.add(r4)
            goto L_0x016f
        L_0x018d:
            com.google.android.gms.internal.measurement.zzdw r6 = com.google.android.gms.internal.measurement.zzdw.zza(r3, r1, r4)
            r11.add(r6)
            goto L_0x016e
        L_0x0195:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x019a:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zzb()
            throw r1
        L_0x019f:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x01a4:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zzb()
            throw r1
        L_0x01a9:
            if (r6 != r10) goto L_0x0422
            com.google.android.gms.internal.measurement.zzhf r1 = r0.zza(r8)
            r22 = r1
            r23 = r21
            r24 = r18
            r25 = r19
            r26 = r20
            r27 = r11
            r28 = r30
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r22, r23, r24, r25, r26, r27, r28)
            goto L_0x0423
        L_0x01c3:
            if (r6 != r10) goto L_0x0422
            r8 = 536870912(0x20000000, double:2.652494739E-315)
            long r8 = r25 & r8
            java.lang.String r1 = ""
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 != 0) goto L_0x0216
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r6 = r7.zza
            if (r6 < 0) goto L_0x0211
            if (r6 != 0) goto L_0x01de
            r11.add(r1)
            goto L_0x01e9
        L_0x01de:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.google.android.gms.internal.measurement.zzfh.zza
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
        L_0x01e8:
            int r4 = r4 + r6
        L_0x01e9:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r8 = r7.zza
            if (r2 != r8) goto L_0x0422
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r6, r7)
            int r6 = r7.zza
            if (r6 < 0) goto L_0x020c
            if (r6 != 0) goto L_0x0201
            r11.add(r1)
            goto L_0x01e9
        L_0x0201:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.google.android.gms.internal.measurement.zzfh.zza
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
            goto L_0x01e8
        L_0x020c:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zzb()
            throw r1
        L_0x0211:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zzb()
            throw r1
        L_0x0216:
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r6 = r7.zza
            if (r6 < 0) goto L_0x0271
            if (r6 != 0) goto L_0x0224
            r11.add(r1)
            goto L_0x0237
        L_0x0224:
            int r8 = r4 + r6
            boolean r9 = com.google.android.gms.internal.measurement.zzif.zza(r3, r4, r8)
            if (r9 == 0) goto L_0x026c
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.google.android.gms.internal.measurement.zzfh.zza
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
        L_0x0236:
            r4 = r8
        L_0x0237:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r8 = r7.zza
            if (r2 != r8) goto L_0x0422
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r6, r7)
            int r6 = r7.zza
            if (r6 < 0) goto L_0x0267
            if (r6 != 0) goto L_0x024f
            r11.add(r1)
            goto L_0x0237
        L_0x024f:
            int r8 = r4 + r6
            boolean r9 = com.google.android.gms.internal.measurement.zzif.zza(r3, r4, r8)
            if (r9 == 0) goto L_0x0262
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.google.android.gms.internal.measurement.zzfh.zza
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
            goto L_0x0236
        L_0x0262:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zzh()
            throw r1
        L_0x0267:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zzb()
            throw r1
        L_0x026c:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zzh()
            throw r1
        L_0x0271:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zzb()
            throw r1
        L_0x0276:
            r1 = 0
            if (r6 != r10) goto L_0x029e
            com.google.android.gms.internal.measurement.zzdu r11 = (com.google.android.gms.internal.measurement.zzdu) r11
            int r2 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r4 = r7.zza
            int r4 = r4 + r2
        L_0x0282:
            if (r2 >= r4) goto L_0x0295
            int r2 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r2, r7)
            long r5 = r7.zzb
            int r8 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r8 == 0) goto L_0x0290
            r5 = 1
            goto L_0x0291
        L_0x0290:
            r5 = 0
        L_0x0291:
            r11.zza(r5)
            goto L_0x0282
        L_0x0295:
            if (r2 != r4) goto L_0x0299
            goto L_0x014e
        L_0x0299:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x029e:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.measurement.zzdu r11 = (com.google.android.gms.internal.measurement.zzdu) r11
            int r4 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r4, r7)
            long r8 = r7.zzb
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02ae
            r6 = 1
            goto L_0x02af
        L_0x02ae:
            r6 = 0
        L_0x02af:
            r11.zza(r6)
        L_0x02b2:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r8 = r7.zza
            if (r2 != r8) goto L_0x0422
            int r4 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r6, r7)
            long r8 = r7.zzb
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02c8
            r6 = 1
            goto L_0x02c9
        L_0x02c8:
            r6 = 0
        L_0x02c9:
            r11.zza(r6)
            goto L_0x02b2
        L_0x02cd:
            if (r6 != r10) goto L_0x02ed
            com.google.android.gms.internal.measurement.zzff r11 = (com.google.android.gms.internal.measurement.zzff) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r2 = r7.zza
            int r2 = r2 + r1
        L_0x02d8:
            if (r1 >= r2) goto L_0x02e4
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1)
            r11.zzd(r4)
            int r1 = r1 + 4
            goto L_0x02d8
        L_0x02e4:
            if (r1 != r2) goto L_0x02e8
            goto L_0x0423
        L_0x02e8:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x02ed:
            if (r6 != r9) goto L_0x0422
            com.google.android.gms.internal.measurement.zzff r11 = (com.google.android.gms.internal.measurement.zzff) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r18, r19)
            r11.zzd(r1)
        L_0x02f8:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1, r7)
            int r6 = r7.zza
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4)
            r11.zzd(r1)
            goto L_0x02f8
        L_0x030c:
            if (r6 != r10) goto L_0x032c
            com.google.android.gms.internal.measurement.zzga r11 = (com.google.android.gms.internal.measurement.zzga) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r2 = r7.zza
            int r2 = r2 + r1
        L_0x0317:
            if (r1 >= r2) goto L_0x0323
            long r4 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r1)
            r11.zza(r4)
            int r1 = r1 + 8
            goto L_0x0317
        L_0x0323:
            if (r1 != r2) goto L_0x0327
            goto L_0x0423
        L_0x0327:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x032c:
            if (r6 != r13) goto L_0x0422
            com.google.android.gms.internal.measurement.zzga r11 = (com.google.android.gms.internal.measurement.zzga) r11
            long r8 = com.google.android.gms.internal.measurement.zzds.zzb(r18, r19)
            r11.zza(r8)
        L_0x0337:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1, r7)
            int r6 = r7.zza
            if (r2 != r6) goto L_0x0423
            long r8 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r4)
            r11.zza(r8)
            goto L_0x0337
        L_0x034b:
            if (r6 != r10) goto L_0x0353
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r11, r7)
            goto L_0x0423
        L_0x0353:
            if (r6 != 0) goto L_0x0422
            r22 = r18
            r23 = r19
            r24 = r20
            r25 = r11
            r26 = r30
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r21, r22, r23, r24, r25, r26)
            goto L_0x0423
        L_0x0365:
            if (r6 != r10) goto L_0x0385
            com.google.android.gms.internal.measurement.zzga r11 = (com.google.android.gms.internal.measurement.zzga) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r2 = r7.zza
            int r2 = r2 + r1
        L_0x0370:
            if (r1 >= r2) goto L_0x037c
            int r1 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r1, r7)
            long r4 = r7.zzb
            r11.zza(r4)
            goto L_0x0370
        L_0x037c:
            if (r1 != r2) goto L_0x0380
            goto L_0x0423
        L_0x0380:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x0385:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.measurement.zzga r11 = (com.google.android.gms.internal.measurement.zzga) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r4, r7)
            long r8 = r7.zzb
            r11.zza(r8)
        L_0x0392:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1, r7)
            int r6 = r7.zza
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.zzds.zzb(r3, r4, r7)
            long r8 = r7.zzb
            r11.zza(r8)
            goto L_0x0392
        L_0x03a6:
            if (r6 != r10) goto L_0x03c5
            com.google.android.gms.internal.measurement.zzfa r11 = (com.google.android.gms.internal.measurement.zzfa) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r2 = r7.zza
            int r2 = r2 + r1
        L_0x03b1:
            if (r1 >= r2) goto L_0x03bd
            float r4 = com.google.android.gms.internal.measurement.zzds.zzd(r3, r1)
            r11.zza(r4)
            int r1 = r1 + 4
            goto L_0x03b1
        L_0x03bd:
            if (r1 != r2) goto L_0x03c0
            goto L_0x0423
        L_0x03c0:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x03c5:
            if (r6 != r9) goto L_0x0422
            com.google.android.gms.internal.measurement.zzfa r11 = (com.google.android.gms.internal.measurement.zzfa) r11
            float r1 = com.google.android.gms.internal.measurement.zzds.zzd(r18, r19)
            r11.zza(r1)
        L_0x03d0:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1, r7)
            int r6 = r7.zza
            if (r2 != r6) goto L_0x0423
            float r1 = com.google.android.gms.internal.measurement.zzds.zzd(r3, r4)
            r11.zza(r1)
            goto L_0x03d0
        L_0x03e4:
            if (r6 != r10) goto L_0x0403
            com.google.android.gms.internal.measurement.zzeq r11 = (com.google.android.gms.internal.measurement.zzeq) r11
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r3, r4, r7)
            int r2 = r7.zza
            int r2 = r2 + r1
        L_0x03ef:
            if (r1 >= r2) goto L_0x03fb
            double r4 = com.google.android.gms.internal.measurement.zzds.zzc(r3, r1)
            r11.zza(r4)
            int r1 = r1 + 8
            goto L_0x03ef
        L_0x03fb:
            if (r1 != r2) goto L_0x03fe
            goto L_0x0423
        L_0x03fe:
            com.google.android.gms.internal.measurement.zzfm r1 = com.google.android.gms.internal.measurement.zzfm.zza()
            throw r1
        L_0x0403:
            if (r6 != r13) goto L_0x0422
            com.google.android.gms.internal.measurement.zzeq r11 = (com.google.android.gms.internal.measurement.zzeq) r11
            double r8 = com.google.android.gms.internal.measurement.zzds.zzc(r18, r19)
            r11.zza(r8)
        L_0x040e:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r3, r1, r7)
            int r6 = r7.zza
            if (r2 != r6) goto L_0x0423
            double r8 = com.google.android.gms.internal.measurement.zzds.zzc(r3, r4)
            r11.zza(r8)
            goto L_0x040e
        L_0x0422:
            r1 = r4
        L_0x0423:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.google.android.gms.internal.measurement.zzdr):int");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v12, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final <K, V> int zza(T r8, byte[] r9, int r10, int r11, int r12, long r13, com.google.android.gms.internal.measurement.zzdr r15) throws java.io.IOException {
        /*
            r7 = this;
            sun.misc.Unsafe r0 = com.google.android.gms.internal.measurement.zzgq.zzb
            java.lang.Object r12 = r7.zzb(r12)
            java.lang.Object r1 = r0.getObject(r8, r13)
            com.google.android.gms.internal.measurement.zzgj r2 = r7.zzs
            boolean r2 = r2.zzd(r1)
            if (r2 == 0) goto L_0x0021
            com.google.android.gms.internal.measurement.zzgj r2 = r7.zzs
            java.lang.Object r2 = r2.zzf(r12)
            com.google.android.gms.internal.measurement.zzgj r3 = r7.zzs
            r3.zza(r2, r1)
            r0.putObject(r8, r13, r2)
            r1 = r2
        L_0x0021:
            com.google.android.gms.internal.measurement.zzgj r8 = r7.zzs
            com.google.android.gms.internal.measurement.zzgh r8 = r8.zzb(r12)
            com.google.android.gms.internal.measurement.zzgj r12 = r7.zzs
            java.util.Map r12 = r12.zza(r1)
            int r10 = com.google.android.gms.internal.measurement.zzds.zza(r9, r10, r15)
            int r13 = r15.zza
            if (r13 < 0) goto L_0x0097
            int r14 = r11 - r10
            if (r13 > r14) goto L_0x0097
            int r13 = r13 + r10
            K r14 = r8.zzb
            V r0 = r8.zzd
        L_0x003e:
            if (r10 >= r13) goto L_0x008c
            int r1 = r10 + 1
            byte r10 = r9[r10]
            if (r10 >= 0) goto L_0x004c
            int r1 = com.google.android.gms.internal.measurement.zzds.zza(r10, r9, r1, r15)
            int r10 = r15.zza
        L_0x004c:
            r2 = r1
            int r1 = r10 >>> 3
            r3 = r10 & 7
            r4 = 1
            if (r1 == r4) goto L_0x0072
            r4 = 2
            if (r1 == r4) goto L_0x0058
            goto L_0x0087
        L_0x0058:
            com.google.android.gms.internal.measurement.zzik r1 = r8.zzc
            int r1 = r1.zzb()
            if (r3 != r1) goto L_0x0087
            com.google.android.gms.internal.measurement.zzik r4 = r8.zzc
            V r10 = r8.zzd
            java.lang.Class r5 = r10.getClass()
            r1 = r9
            r3 = r11
            r6 = r15
            int r10 = zza(r1, r2, r3, r4, r5, r6)
            java.lang.Object r0 = r15.zzc
            goto L_0x003e
        L_0x0072:
            com.google.android.gms.internal.measurement.zzik r1 = r8.zza
            int r1 = r1.zzb()
            if (r3 != r1) goto L_0x0087
            com.google.android.gms.internal.measurement.zzik r4 = r8.zza
            r5 = 0
            r1 = r9
            r3 = r11
            r6 = r15
            int r10 = zza(r1, r2, r3, r4, r5, r6)
            java.lang.Object r14 = r15.zzc
            goto L_0x003e
        L_0x0087:
            int r10 = com.google.android.gms.internal.measurement.zzds.zza(r10, r9, r2, r11, r15)
            goto L_0x003e
        L_0x008c:
            if (r10 != r13) goto L_0x0092
            r12.put(r14, r0)
            return r13
        L_0x0092:
            com.google.android.gms.internal.measurement.zzfm r8 = com.google.android.gms.internal.measurement.zzfm.zzg()
            throw r8
        L_0x0097:
            com.google.android.gms.internal.measurement.zzfm r8 = com.google.android.gms.internal.measurement.zzfm.zza()
            goto L_0x009d
        L_0x009c:
            throw r8
        L_0x009d:
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, byte[], int, int, int, long, com.google.android.gms.internal.measurement.zzdr):int");
    }

    private final int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, zzdr zzdr) throws IOException {
        int i9;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i10 = i;
        int i11 = i3;
        int i12 = i4;
        int i13 = i5;
        long j2 = j;
        int i14 = i8;
        zzdr zzdr2 = zzdr;
        Unsafe unsafe = zzb;
        long j3 = (long) (this.zzc[i14 + 2] & 1048575);
        switch (i7) {
            case 51:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Double.valueOf(zzds.zzc(bArr, i)));
                    i9 = i10 + 8;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 52:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Float.valueOf(zzds.zzd(bArr, i)));
                    i9 = i10 + 4;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 53:
            case 54:
                if (i13 == 0) {
                    i9 = zzds.zzb(bArr2, i10, zzdr2);
                    unsafe.putObject(t2, j2, Long.valueOf(zzdr2.zzb));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 55:
            case 62:
                if (i13 == 0) {
                    i9 = zzds.zza(bArr2, i10, zzdr2);
                    unsafe.putObject(t2, j2, Integer.valueOf(zzdr2.zza));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 56:
            case 65:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Long.valueOf(zzds.zzb(bArr, i)));
                    i9 = i10 + 8;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 57:
            case 64:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Integer.valueOf(zzds.zza(bArr, i)));
                    i9 = i10 + 4;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 58:
                if (i13 == 0) {
                    i9 = zzds.zzb(bArr2, i10, zzdr2);
                    unsafe.putObject(t2, j2, Boolean.valueOf(zzdr2.zzb != 0));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 59:
                if (i13 == 2) {
                    int zza2 = zzds.zza(bArr2, i10, zzdr2);
                    int i15 = zzdr2.zza;
                    if (i15 == 0) {
                        unsafe.putObject(t2, j2, "");
                    } else if ((i6 & DriveFile.MODE_WRITE_ONLY) == 0 || zzif.zza(bArr2, zza2, zza2 + i15)) {
                        unsafe.putObject(t2, j2, new String(bArr2, zza2, i15, zzfh.zza));
                        zza2 += i15;
                    } else {
                        throw zzfm.zzh();
                    }
                    unsafe.putInt(t2, j3, i12);
                    return zza2;
                }
                return i10;
            case 60:
                if (i13 == 2) {
                    int zza3 = zzds.zza(zza(i14), bArr2, i10, i2, zzdr2);
                    Object object = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object == null) {
                        unsafe.putObject(t2, j2, zzdr2.zzc);
                    } else {
                        unsafe.putObject(t2, j2, zzfh.zza(object, zzdr2.zzc));
                    }
                    unsafe.putInt(t2, j3, i12);
                    return zza3;
                }
                return i10;
            case 61:
                if (i13 == 2) {
                    i9 = zzds.zze(bArr2, i10, zzdr2);
                    unsafe.putObject(t2, j2, zzdr2.zzc);
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 63:
                if (i13 == 0) {
                    int zza4 = zzds.zza(bArr2, i10, zzdr2);
                    int i16 = zzdr2.zza;
                    zzfi zzc2 = zzc(i14);
                    if (zzc2 == null || zzc2.zza(i16)) {
                        unsafe.putObject(t2, j2, Integer.valueOf(i16));
                        i9 = zza4;
                        unsafe.putInt(t2, j3, i12);
                        return i9;
                    }
                    zze(t).zza(i11, Long.valueOf((long) i16));
                    return zza4;
                }
                return i10;
            case 66:
                if (i13 == 0) {
                    i9 = zzds.zza(bArr2, i10, zzdr2);
                    unsafe.putObject(t2, j2, Integer.valueOf(zzei.zze(zzdr2.zza)));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 67:
                if (i13 == 0) {
                    i9 = zzds.zzb(bArr2, i10, zzdr2);
                    unsafe.putObject(t2, j2, Long.valueOf(zzei.zza(zzdr2.zzb)));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 68:
                if (i13 == 3) {
                    i9 = zzds.zza(zza(i14), bArr, i, i2, (i11 & -8) | 4, zzdr);
                    Object object2 = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object2 == null) {
                        unsafe.putObject(t2, j2, zzdr2.zzc);
                    } else {
                        unsafe.putObject(t2, j2, zzfh.zza(object2, zzdr2.zzc));
                    }
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            default:
                return i10;
        }
    }

    private final zzhf zza(int i) {
        int i2 = (i / 3) << 1;
        zzhf zzhf = (zzhf) this.zzd[i2];
        if (zzhf != null) {
            return zzhf;
        }
        zzhf zza2 = zzhb.zza().zza((Class) this.zzd[i2 + 1]);
        this.zzd[i2] = zza2;
        return zza2;
    }

    private final Object zzb(int i) {
        return this.zzd[(i / 3) << 1];
    }

    private final zzfi zzc(int i) {
        return (zzfi) this.zzd[((i / 3) << 1) + 1];
    }

    /* JADX WARN: Type inference failed for: r34v0, types: [int] */
    /* JADX WARN: Type inference failed for: r3v27, types: [int] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0370, code lost:
        if (r0 == r15) goto L_0x03df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x03b9, code lost:
        if (r0 == r15) goto L_0x03df;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x03e5 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x03fa  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x043c  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int zza(java.lang.Object r30, byte[] r31, int r32, int r33, int r34, com.google.android.gms.internal.measurement.zzdr r35) throws java.io.IOException {
        /*
            r29 = this;
            r15 = r29
            r14 = r30
            r12 = r31
            r13 = r33
            r11 = r34
            r9 = r35
            sun.misc.Unsafe r10 = com.google.android.gms.internal.measurement.zzgq.zzb
            r16 = 0
            r0 = r32
            r1 = -1
            r2 = 0
            r3 = 0
            r6 = 0
            r7 = -1
        L_0x0017:
            if (r0 >= r13) goto L_0x047e
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x0028
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r12, r3, r9)
            int r3 = r9.zza
            r4 = r0
            r5 = r3
            goto L_0x002a
        L_0x0028:
            r5 = r0
            r4 = r3
        L_0x002a:
            int r3 = r5 >>> 3
            r0 = r5 & 7
            r8 = 3
            if (r3 <= r1) goto L_0x0037
            int r2 = r2 / r8
            int r1 = r15.zza(r3, r2)
            goto L_0x003b
        L_0x0037:
            int r1 = r15.zzg(r3)
        L_0x003b:
            r2 = r1
            r1 = -1
            if (r2 != r1) goto L_0x004e
            r24 = r3
            r2 = r4
            r19 = r6
            r17 = r7
            r26 = r10
            r6 = r11
            r18 = 0
            r7 = r5
            goto L_0x03e3
        L_0x004e:
            int[] r1 = r15.zzc
            int r18 = r2 + 1
            r8 = r1[r18]
            r18 = 267386880(0xff00000, float:2.3665827E-29)
            r18 = r8 & r18
            int r11 = r18 >>> 20
            r18 = 1048575(0xfffff, float:1.469367E-39)
            r19 = r5
            r5 = r8 & r18
            long r12 = (long) r5
            r5 = 17
            r20 = r8
            if (r11 > r5) goto L_0x02da
            int r5 = r2 + 2
            r1 = r1[r5]
            int r5 = r1 >>> 20
            r8 = 1
            int r22 = r8 << r5
            r1 = r1 & r18
            r5 = -1
            if (r1 == r7) goto L_0x0082
            if (r7 == r5) goto L_0x007c
            long r8 = (long) r7
            r10.putInt(r14, r8, r6)
        L_0x007c:
            long r6 = (long) r1
            int r6 = r10.getInt(r14, r6)
            r7 = r1
        L_0x0082:
            r1 = 5
            switch(r11) {
                case 0: goto L_0x02a2;
                case 1: goto L_0x0288;
                case 2: goto L_0x0262;
                case 3: goto L_0x0262;
                case 4: goto L_0x0247;
                case 5: goto L_0x0222;
                case 6: goto L_0x01ff;
                case 7: goto L_0x01d7;
                case 8: goto L_0x01b2;
                case 9: goto L_0x017c;
                case 10: goto L_0x0161;
                case 11: goto L_0x0247;
                case 12: goto L_0x012f;
                case 13: goto L_0x01ff;
                case 14: goto L_0x0222;
                case 15: goto L_0x0114;
                case 16: goto L_0x00e7;
                case 17: goto L_0x0095;
                default: goto L_0x0086;
            }
        L_0x0086:
            r12 = r31
            r13 = r35
            r9 = r2
            r11 = r3
            r32 = r7
            r8 = r19
            r18 = -1
        L_0x0092:
            r7 = r4
            goto L_0x02ca
        L_0x0095:
            r8 = 3
            if (r0 != r8) goto L_0x00db
            int r0 = r3 << 3
            r8 = r0 | 4
            com.google.android.gms.internal.measurement.zzhf r0 = r15.zza(r2)
            r1 = r31
            r9 = r2
            r2 = r4
            r11 = r3
            r3 = r33
            r4 = r8
            r8 = r19
            r18 = -1
            r5 = r35
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r1, r2, r3, r4, r5)
            r1 = r6 & r22
            if (r1 != 0) goto L_0x00be
            r5 = r35
            java.lang.Object r1 = r5.zzc
            r10.putObject(r14, r12, r1)
            goto L_0x00cd
        L_0x00be:
            r5 = r35
            java.lang.Object r1 = r10.getObject(r14, r12)
            java.lang.Object r2 = r5.zzc
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzfh.zza(r1, r2)
            r10.putObject(r14, r12, r1)
        L_0x00cd:
            r6 = r6 | r22
            r12 = r31
            r13 = r33
            r3 = r8
            r2 = r9
            r1 = r11
            r11 = r34
            r9 = r5
            goto L_0x0017
        L_0x00db:
            r9 = r2
            r11 = r3
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            goto L_0x0243
        L_0x00e7:
            r5 = r35
            r9 = r2
            r11 = r3
            r8 = r19
            r18 = -1
            if (r0 != 0) goto L_0x010f
            r2 = r12
            r12 = r31
            int r13 = com.google.android.gms.internal.measurement.zzds.zzb(r12, r4, r5)
            long r0 = r5.zzb
            long r19 = com.google.android.gms.internal.measurement.zzei.zza(r0)
            r0 = r10
            r1 = r30
            r32 = r13
            r13 = r5
            r4 = r19
            r0.putLong(r1, r2, r4)
            r6 = r6 | r22
            r0 = r32
            goto L_0x02c0
        L_0x010f:
            r12 = r31
            r13 = r5
            goto L_0x0243
        L_0x0114:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != 0) goto L_0x0243
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r12, r4, r13)
            int r1 = r13.zza
            int r1 = com.google.android.gms.internal.measurement.zzei.zze(r1)
            r10.putInt(r14, r2, r1)
            goto L_0x0178
        L_0x012f:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != 0) goto L_0x0243
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r12, r4, r13)
            int r1 = r13.zza
            com.google.android.gms.internal.measurement.zzfi r4 = r15.zzc(r9)
            if (r4 == 0) goto L_0x015d
            boolean r4 = r4.zza(r1)
            if (r4 == 0) goto L_0x014f
            goto L_0x015d
        L_0x014f:
            com.google.android.gms.internal.measurement.zzhw r2 = zze(r30)
            long r3 = (long) r1
            java.lang.Long r1 = java.lang.Long.valueOf(r3)
            r2.zza(r8, r1)
            goto L_0x02c0
        L_0x015d:
            r10.putInt(r14, r2, r1)
            goto L_0x0178
        L_0x0161:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r1 = 2
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x0243
            int r0 = com.google.android.gms.internal.measurement.zzds.zze(r12, r4, r13)
            java.lang.Object r1 = r13.zzc
            r10.putObject(r14, r2, r1)
        L_0x0178:
            r6 = r6 | r22
            goto L_0x02c0
        L_0x017c:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r1 = 2
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x01ae
            com.google.android.gms.internal.measurement.zzhf r0 = r15.zza(r9)
            r5 = r33
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r12, r4, r5, r13)
            r1 = r6 & r22
            if (r1 != 0) goto L_0x019f
            java.lang.Object r1 = r13.zzc
            r10.putObject(r14, r2, r1)
            goto L_0x0217
        L_0x019f:
            java.lang.Object r1 = r10.getObject(r14, r2)
            java.lang.Object r4 = r13.zzc
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzfh.zza(r1, r4)
            r10.putObject(r14, r2, r1)
            goto L_0x0217
        L_0x01ae:
            r5 = r33
            goto L_0x0243
        L_0x01b2:
            r5 = r33
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r1 = 2
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x0243
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r20 & r0
            if (r0 != 0) goto L_0x01cd
            int r0 = com.google.android.gms.internal.measurement.zzds.zzc(r12, r4, r13)
            goto L_0x01d1
        L_0x01cd:
            int r0 = com.google.android.gms.internal.measurement.zzds.zzd(r12, r4, r13)
        L_0x01d1:
            java.lang.Object r1 = r13.zzc
            r10.putObject(r14, r2, r1)
            goto L_0x0217
        L_0x01d7:
            r5 = r33
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != 0) goto L_0x0243
            int r0 = com.google.android.gms.internal.measurement.zzds.zzb(r12, r4, r13)
            r32 = r0
            long r0 = r13.zzb
            r19 = 0
            int r4 = (r0 > r19 ? 1 : (r0 == r19 ? 0 : -1))
            if (r4 == 0) goto L_0x01f6
            r0 = 1
            goto L_0x01f7
        L_0x01f6:
            r0 = 0
        L_0x01f7:
            com.google.android.gms.internal.measurement.zzid.zza(r14, r2, r0)
            r6 = r6 | r22
            r0 = r32
            goto L_0x0219
        L_0x01ff:
            r5 = r33
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x0243
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r12, r4)
            r10.putInt(r14, r2, r0)
            int r0 = r4 + 4
        L_0x0217:
            r6 = r6 | r22
        L_0x0219:
            r3 = r8
            r2 = r9
            r1 = r11
            r9 = r13
            r11 = r34
            r13 = r5
            goto L_0x0017
        L_0x0222:
            r5 = r33
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r19
            r1 = 1
            r18 = -1
            r12 = r31
            r13 = r35
            if (r0 != r1) goto L_0x0243
            long r19 = com.google.android.gms.internal.measurement.zzds.zzb(r12, r4)
            r0 = r10
            r1 = r30
            r32 = r7
            r7 = r4
            r4 = r19
            r0.putLong(r1, r2, r4)
            goto L_0x02ba
        L_0x0243:
            r32 = r7
            goto L_0x0092
        L_0x0247:
            r9 = r2
            r11 = r3
            r32 = r7
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            r7 = r4
            if (r0 != 0) goto L_0x02ca
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r12, r7, r13)
            int r1 = r13.zza
            r10.putInt(r14, r2, r1)
            goto L_0x02bc
        L_0x0262:
            r9 = r2
            r11 = r3
            r32 = r7
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            r7 = r4
            if (r0 != 0) goto L_0x02ca
            int r7 = com.google.android.gms.internal.measurement.zzds.zzb(r12, r7, r13)
            long r4 = r13.zzb
            r0 = r10
            r1 = r30
            r0.putLong(r1, r2, r4)
            r6 = r6 | r22
            r0 = r7
            r3 = r8
            r2 = r9
            r1 = r11
            r9 = r13
            r7 = r32
            goto L_0x02c4
        L_0x0288:
            r9 = r2
            r11 = r3
            r32 = r7
            r2 = r12
            r8 = r19
            r18 = -1
            r12 = r31
            r13 = r35
            r7 = r4
            if (r0 != r1) goto L_0x02ca
            float r0 = com.google.android.gms.internal.measurement.zzds.zzd(r12, r7)
            com.google.android.gms.internal.measurement.zzid.zza(r14, r2, r0)
            int r0 = r7 + 4
            goto L_0x02bc
        L_0x02a2:
            r9 = r2
            r11 = r3
            r32 = r7
            r2 = r12
            r8 = r19
            r1 = 1
            r18 = -1
            r12 = r31
            r13 = r35
            r7 = r4
            if (r0 != r1) goto L_0x02ca
            double r0 = com.google.android.gms.internal.measurement.zzds.zzc(r12, r7)
            com.google.android.gms.internal.measurement.zzid.zza(r14, r2, r0)
        L_0x02ba:
            int r0 = r7 + 8
        L_0x02bc:
            r6 = r6 | r22
            r7 = r32
        L_0x02c0:
            r3 = r8
            r2 = r9
            r1 = r11
            r9 = r13
        L_0x02c4:
            r13 = r33
            r11 = r34
            goto L_0x0017
        L_0x02ca:
            r17 = r32
            r19 = r6
            r2 = r7
            r7 = r8
            r18 = r9
            r26 = r10
            r24 = r11
            r6 = r34
            goto L_0x03e3
        L_0x02da:
            r5 = r3
            r17 = r7
            r8 = r19
            r18 = -1
            r7 = r4
            r27 = r12
            r12 = r31
            r13 = r9
            r9 = r2
            r2 = r27
            r1 = 27
            if (r11 != r1) goto L_0x033f
            r1 = 2
            if (r0 != r1) goto L_0x0332
            java.lang.Object r0 = r10.getObject(r14, r2)
            com.google.android.gms.internal.measurement.zzfn r0 = (com.google.android.gms.internal.measurement.zzfn) r0
            boolean r1 = r0.zza()
            if (r1 != 0) goto L_0x030f
            int r1 = r0.size()
            if (r1 != 0) goto L_0x0306
            r1 = 10
            goto L_0x0308
        L_0x0306:
            int r1 = r1 << 1
        L_0x0308:
            com.google.android.gms.internal.measurement.zzfn r0 = r0.zza(r1)
            r10.putObject(r14, r2, r0)
        L_0x030f:
            r11 = r0
            com.google.android.gms.internal.measurement.zzhf r0 = r15.zza(r9)
            r1 = r8
            r2 = r31
            r3 = r7
            r4 = r33
            r7 = r5
            r5 = r11
            r19 = r6
            r6 = r35
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r1, r2, r3, r4, r5, r6)
            r11 = r34
            r1 = r7
            r3 = r8
            r2 = r9
            r9 = r13
            r7 = r17
            r6 = r19
            r13 = r33
            goto L_0x0017
        L_0x0332:
            r19 = r6
            r24 = r5
            r15 = r7
            r25 = r8
            r18 = r9
            r26 = r10
            goto L_0x03bc
        L_0x033f:
            r19 = r6
            r6 = r5
            r1 = 49
            if (r11 > r1) goto L_0x038e
            r5 = r20
            long r4 = (long) r5
            r1 = r0
            r0 = r29
            r32 = r1
            r1 = r30
            r22 = r2
            r2 = r31
            r3 = r7
            r20 = r4
            r4 = r33
            r5 = r8
            r24 = r6
            r15 = r7
            r7 = r32
            r25 = r8
            r8 = r9
            r18 = r9
            r26 = r10
            r9 = r20
            r12 = r22
            r14 = r35
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x0374
            goto L_0x03df
        L_0x0374:
            r15 = r29
            r14 = r30
            r12 = r31
            r13 = r33
            r11 = r34
            r9 = r35
            r7 = r17
            r2 = r18
            r6 = r19
            r1 = r24
            r3 = r25
        L_0x038a:
            r10 = r26
            goto L_0x0017
        L_0x038e:
            r32 = r0
            r22 = r2
            r24 = r6
            r15 = r7
            r25 = r8
            r18 = r9
            r26 = r10
            r5 = r20
            r0 = 50
            if (r11 != r0) goto L_0x03c2
            r7 = r32
            r0 = 2
            if (r7 != r0) goto L_0x03bc
            r0 = r29
            r1 = r30
            r2 = r31
            r3 = r15
            r4 = r33
            r5 = r18
            r6 = r22
            r8 = r35
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r8)
            if (r0 != r15) goto L_0x0374
            goto L_0x03df
        L_0x03bc:
            r6 = r34
            r2 = r15
        L_0x03bf:
            r7 = r25
            goto L_0x03e3
        L_0x03c2:
            r7 = r32
            r0 = r29
            r1 = r30
            r2 = r31
            r3 = r15
            r4 = r33
            r8 = r5
            r5 = r25
            r6 = r24
            r9 = r11
            r10 = r22
            r12 = r18
            r13 = r35
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x0464
        L_0x03df:
            r6 = r34
            r2 = r0
            goto L_0x03bf
        L_0x03e3:
            if (r7 != r6) goto L_0x03f4
            if (r6 != 0) goto L_0x03e8
            goto L_0x03f4
        L_0x03e8:
            r4 = -1
            r8 = r29
            r11 = r30
            r3 = r7
            r0 = r17
            r1 = r19
            goto L_0x048d
        L_0x03f4:
            r8 = r29
            boolean r0 = r8.zzh
            if (r0 == 0) goto L_0x043c
            r9 = r35
            com.google.android.gms.internal.measurement.zzer r0 = r9.zzd
            com.google.android.gms.internal.measurement.zzer r1 = com.google.android.gms.internal.measurement.zzer.zza()
            if (r0 == r1) goto L_0x0439
            com.google.android.gms.internal.measurement.zzgm r0 = r8.zzg
            com.google.android.gms.internal.measurement.zzer r1 = r9.zzd
            r10 = r24
            com.google.android.gms.internal.measurement.zzfe$zzd r0 = r1.zza(r0, r10)
            if (r0 != 0) goto L_0x0429
            com.google.android.gms.internal.measurement.zzhw r4 = zze(r30)
            r0 = r7
            r1 = r31
            r3 = r33
            r5 = r35
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r1, r2, r3, r4, r5)
            r14 = r30
            r12 = r31
            r13 = r33
            r11 = r6
            r3 = r7
            r15 = r8
            goto L_0x0475
        L_0x0429:
            r11 = r30
            r0 = r11
            com.google.android.gms.internal.measurement.zzfe$zzb r0 = (com.google.android.gms.internal.measurement.zzfe.zzb) r0
            r0.zza()
            com.google.android.gms.internal.measurement.zzeu<com.google.android.gms.internal.measurement.zzfe$zze> r0 = r0.zzc
            java.lang.NoSuchMethodError r0 = new java.lang.NoSuchMethodError
            r0.<init>()
            throw r0
        L_0x0439:
            r11 = r30
            goto L_0x0440
        L_0x043c:
            r11 = r30
            r9 = r35
        L_0x0440:
            r10 = r24
            com.google.android.gms.internal.measurement.zzhw r4 = zze(r30)
            r0 = r7
            r1 = r31
            r3 = r33
            r5 = r35
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r1, r2, r3, r4, r5)
            r12 = r31
            r13 = r33
            r3 = r7
            r15 = r8
            r1 = r10
            r14 = r11
            r7 = r17
            r2 = r18
            r10 = r26
            r11 = r6
            r6 = r19
            goto L_0x0017
        L_0x0464:
            r10 = r24
            r7 = r25
            r15 = r29
            r14 = r30
            r12 = r31
            r13 = r33
            r11 = r34
            r9 = r35
            r3 = r7
        L_0x0475:
            r1 = r10
            r7 = r17
            r2 = r18
            r6 = r19
            goto L_0x038a
        L_0x047e:
            r19 = r6
            r17 = r7
            r26 = r10
            r6 = r11
            r11 = r14
            r8 = r15
            r2 = r0
            r0 = r17
            r1 = r19
            r4 = -1
        L_0x048d:
            if (r0 == r4) goto L_0x0495
            long r4 = (long) r0
            r0 = r26
            r0.putInt(r11, r4, r1)
        L_0x0495:
            r0 = 0
            int r1 = r8.zzm
        L_0x0498:
            int r4 = r8.zzn
            if (r1 >= r4) goto L_0x04ab
            int[] r4 = r8.zzl
            r4 = r4[r1]
            com.google.android.gms.internal.measurement.zzhx<?, ?> r5 = r8.zzq
            java.lang.Object r0 = r8.zza(r11, r4, r0, r5)
            com.google.android.gms.internal.measurement.zzhw r0 = (com.google.android.gms.internal.measurement.zzhw) r0
            int r1 = r1 + 1
            goto L_0x0498
        L_0x04ab:
            if (r0 == 0) goto L_0x04b2
            com.google.android.gms.internal.measurement.zzhx<?, ?> r1 = r8.zzq
            r1.zzb(r11, r0)
        L_0x04b2:
            if (r6 != 0) goto L_0x04be
            r0 = r33
            if (r2 != r0) goto L_0x04b9
            goto L_0x04c4
        L_0x04b9:
            com.google.android.gms.internal.measurement.zzfm r0 = com.google.android.gms.internal.measurement.zzfm.zzg()
            throw r0
        L_0x04be:
            r0 = r33
            if (r2 > r0) goto L_0x04c5
            if (r3 != r6) goto L_0x04c5
        L_0x04c4:
            return r2
        L_0x04c5:
            com.google.android.gms.internal.measurement.zzfm r0 = com.google.android.gms.internal.measurement.zzfm.zzg()
            goto L_0x04cb
        L_0x04ca:
            throw r0
        L_0x04cb:
            goto L_0x04ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, byte[], int, int, int, com.google.android.gms.internal.measurement.zzdr):int");
    }

    /* JADX WARN: Type inference failed for: r3v13, types: [int] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01e2, code lost:
        if (r0 == r15) goto L_0x0230;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x020f, code lost:
        if (r0 == r15) goto L_0x0230;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x022e, code lost:
        if (r0 == r15) goto L_0x0230;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r28, byte[] r29, int r30, int r31, com.google.android.gms.internal.measurement.zzdr r32) throws java.io.IOException {
        /*
            r27 = this;
            r15 = r27
            r14 = r28
            r12 = r29
            r13 = r31
            r11 = r32
            boolean r0 = r15.zzj
            if (r0 == 0) goto L_0x025d
            sun.misc.Unsafe r9 = com.google.android.gms.internal.measurement.zzgq.zzb
            r10 = -1
            r16 = 0
            r0 = r30
            r1 = -1
            r2 = 0
        L_0x0017:
            if (r0 >= r13) goto L_0x0254
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x0029
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r12, r3, r11)
            int r3 = r11.zza
            r8 = r0
            r17 = r3
            goto L_0x002c
        L_0x0029:
            r17 = r0
            r8 = r3
        L_0x002c:
            int r7 = r17 >>> 3
            r6 = r17 & 7
            if (r7 <= r1) goto L_0x0039
            int r2 = r2 / 3
            int r0 = r15.zza(r7, r2)
            goto L_0x003d
        L_0x0039:
            int r0 = r15.zzg(r7)
        L_0x003d:
            r4 = r0
            if (r4 != r10) goto L_0x004b
            r24 = r7
            r2 = r8
            r18 = r9
            r19 = 0
            r26 = -1
            goto L_0x0231
        L_0x004b:
            int[] r0 = r15.zzc
            int r1 = r4 + 1
            r5 = r0[r1]
            r0 = 267386880(0xff00000, float:2.3665827E-29)
            r0 = r0 & r5
            int r3 = r0 >>> 20
            r0 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r0 & r5
            long r1 = (long) r0
            r0 = 17
            r10 = 2
            if (r3 > r0) goto L_0x0167
            r0 = 1
            switch(r3) {
                case 0: goto L_0x014e;
                case 1: goto L_0x013f;
                case 2: goto L_0x012d;
                case 3: goto L_0x012d;
                case 4: goto L_0x011f;
                case 5: goto L_0x010f;
                case 6: goto L_0x00fe;
                case 7: goto L_0x00e8;
                case 8: goto L_0x00d1;
                case 9: goto L_0x00b0;
                case 10: goto L_0x00a3;
                case 11: goto L_0x011f;
                case 12: goto L_0x0094;
                case 13: goto L_0x00fe;
                case 14: goto L_0x010f;
                case 15: goto L_0x0081;
                case 16: goto L_0x0066;
                default: goto L_0x0064;
            }
        L_0x0064:
            goto L_0x01a4
        L_0x0066:
            if (r6 != 0) goto L_0x01a4
            int r6 = com.google.android.gms.internal.measurement.zzds.zzb(r12, r8, r11)
            r19 = r1
            long r0 = r11.zzb
            long r21 = com.google.android.gms.internal.measurement.zzei.zza(r0)
            r0 = r9
            r2 = r19
            r1 = r28
            r10 = r4
            r4 = r21
            r0.putLong(r1, r2, r4)
            goto L_0x013d
        L_0x0081:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r12, r8, r11)
            int r1 = r11.zza
            int r1 = com.google.android.gms.internal.measurement.zzei.zze(r1)
            r9.putInt(r14, r2, r1)
            goto L_0x015b
        L_0x0094:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r12, r8, r11)
            int r1 = r11.zza
            r9.putInt(r14, r2, r1)
            goto L_0x015b
        L_0x00a3:
            r2 = r1
            if (r6 != r10) goto L_0x01a4
            int r0 = com.google.android.gms.internal.measurement.zzds.zze(r12, r8, r11)
            java.lang.Object r1 = r11.zzc
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00b0:
            r2 = r1
            if (r6 != r10) goto L_0x01a4
            com.google.android.gms.internal.measurement.zzhf r0 = r15.zza(r4)
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r12, r8, r13, r11)
            java.lang.Object r1 = r9.getObject(r14, r2)
            if (r1 != 0) goto L_0x00c7
            java.lang.Object r1 = r11.zzc
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00c7:
            java.lang.Object r5 = r11.zzc
            java.lang.Object r1 = com.google.android.gms.internal.measurement.zzfh.zza(r1, r5)
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00d1:
            r2 = r1
            if (r6 != r10) goto L_0x01a4
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r0 & r5
            if (r0 != 0) goto L_0x00de
            int r0 = com.google.android.gms.internal.measurement.zzds.zzc(r12, r8, r11)
            goto L_0x00e2
        L_0x00de:
            int r0 = com.google.android.gms.internal.measurement.zzds.zzd(r12, r8, r11)
        L_0x00e2:
            java.lang.Object r1 = r11.zzc
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00e8:
            r2 = r1
            if (r6 != 0) goto L_0x01a4
            int r1 = com.google.android.gms.internal.measurement.zzds.zzb(r12, r8, r11)
            long r5 = r11.zzb
            r19 = 0
            int r8 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r8 == 0) goto L_0x00f8
            goto L_0x00f9
        L_0x00f8:
            r0 = 0
        L_0x00f9:
            com.google.android.gms.internal.measurement.zzid.zza(r14, r2, r0)
            r0 = r1
            goto L_0x010b
        L_0x00fe:
            r2 = r1
            r0 = 5
            if (r6 != r0) goto L_0x01a4
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r12, r8)
            r9.putInt(r14, r2, r0)
            int r0 = r8 + 4
        L_0x010b:
            r2 = r4
            r1 = r7
            goto L_0x0251
        L_0x010f:
            r2 = r1
            if (r6 != r0) goto L_0x01a4
            long r5 = com.google.android.gms.internal.measurement.zzds.zzb(r12, r8)
            r0 = r9
            r1 = r28
            r10 = r4
            r4 = r5
            r0.putLong(r1, r2, r4)
            goto L_0x0159
        L_0x011f:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r12, r8, r11)
            int r1 = r11.zza
            r9.putInt(r14, r2, r1)
            goto L_0x015b
        L_0x012d:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r6 = com.google.android.gms.internal.measurement.zzds.zzb(r12, r8, r11)
            long r4 = r11.zzb
            r0 = r9
            r1 = r28
            r0.putLong(r1, r2, r4)
        L_0x013d:
            r0 = r6
            goto L_0x015b
        L_0x013f:
            r2 = r1
            r10 = r4
            r0 = 5
            if (r6 != r0) goto L_0x015f
            float r0 = com.google.android.gms.internal.measurement.zzds.zzd(r12, r8)
            com.google.android.gms.internal.measurement.zzid.zza(r14, r2, r0)
            int r0 = r8 + 4
            goto L_0x015b
        L_0x014e:
            r2 = r1
            r10 = r4
            if (r6 != r0) goto L_0x015f
            double r0 = com.google.android.gms.internal.measurement.zzds.zzc(r12, r8)
            com.google.android.gms.internal.measurement.zzid.zza(r14, r2, r0)
        L_0x0159:
            int r0 = r8 + 8
        L_0x015b:
            r1 = r7
            r2 = r10
            goto L_0x0251
        L_0x015f:
            r24 = r7
            r15 = r8
            r18 = r9
            r19 = r10
            goto L_0x01ab
        L_0x0167:
            r0 = 27
            if (r3 != r0) goto L_0x01af
            if (r6 != r10) goto L_0x01a4
            java.lang.Object r0 = r9.getObject(r14, r1)
            com.google.android.gms.internal.measurement.zzfn r0 = (com.google.android.gms.internal.measurement.zzfn) r0
            boolean r3 = r0.zza()
            if (r3 != 0) goto L_0x018b
            int r3 = r0.size()
            if (r3 != 0) goto L_0x0182
            r3 = 10
            goto L_0x0184
        L_0x0182:
            int r3 = r3 << 1
        L_0x0184:
            com.google.android.gms.internal.measurement.zzfn r0 = r0.zza(r3)
            r9.putObject(r14, r1, r0)
        L_0x018b:
            r5 = r0
            com.google.android.gms.internal.measurement.zzhf r0 = r15.zza(r4)
            r1 = r17
            r2 = r29
            r3 = r8
            r19 = r4
            r4 = r31
            r6 = r32
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r1, r2, r3, r4, r5, r6)
            r1 = r7
            r2 = r19
            goto L_0x0251
        L_0x01a4:
            r19 = r4
            r24 = r7
            r15 = r8
            r18 = r9
        L_0x01ab:
            r26 = -1
            goto L_0x0212
        L_0x01af:
            r19 = r4
            r0 = 49
            if (r3 > r0) goto L_0x01e5
            long r4 = (long) r5
            r0 = r27
            r20 = r1
            r1 = r28
            r2 = r29
            r10 = r3
            r3 = r8
            r22 = r4
            r4 = r31
            r5 = r17
            r30 = r6
            r6 = r7
            r24 = r7
            r7 = r30
            r15 = r8
            r8 = r19
            r18 = r9
            r25 = r10
            r26 = -1
            r9 = r22
            r11 = r25
            r12 = r20
            r14 = r32
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x0241
            goto L_0x0230
        L_0x01e5:
            r20 = r1
            r25 = r3
            r30 = r6
            r24 = r7
            r15 = r8
            r18 = r9
            r26 = -1
            r0 = 50
            r9 = r25
            if (r9 != r0) goto L_0x0214
            r7 = r30
            if (r7 != r10) goto L_0x0212
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r15
            r4 = r31
            r5 = r19
            r6 = r20
            r8 = r32
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r8)
            if (r0 != r15) goto L_0x0241
            goto L_0x0230
        L_0x0212:
            r2 = r15
            goto L_0x0231
        L_0x0214:
            r7 = r30
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r15
            r4 = r31
            r8 = r5
            r5 = r17
            r6 = r24
            r10 = r20
            r12 = r19
            r13 = r32
            int r0 = r0.zza(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x0241
        L_0x0230:
            r2 = r0
        L_0x0231:
            com.google.android.gms.internal.measurement.zzhw r4 = zze(r28)
            r0 = r17
            r1 = r29
            r3 = r31
            r5 = r32
            int r0 = com.google.android.gms.internal.measurement.zzds.zza(r0, r1, r2, r3, r4, r5)
        L_0x0241:
            r15 = r27
            r14 = r28
            r12 = r29
            r13 = r31
            r11 = r32
            r9 = r18
            r2 = r19
            r1 = r24
        L_0x0251:
            r10 = -1
            goto L_0x0017
        L_0x0254:
            r4 = r13
            if (r0 != r4) goto L_0x0258
            return
        L_0x0258:
            com.google.android.gms.internal.measurement.zzfm r0 = com.google.android.gms.internal.measurement.zzfm.zzg()
            throw r0
        L_0x025d:
            r4 = r13
            r5 = 0
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r30
            r4 = r31
            r6 = r32
            r0.zza(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, byte[], int, int, com.google.android.gms.internal.measurement.zzdr):void");
    }

    public final void zzc(Object obj) {
        int i;
        int i2 = this.zzm;
        while (true) {
            i = this.zzn;
            if (i2 >= i) {
                break;
            }
            long zzd2 = (long) (zzd(this.zzl[i2]) & 1048575);
            Object zzf2 = zzid.zzf(obj, zzd2);
            if (zzf2 != null) {
                zzid.zza(obj, zzd2, this.zzs.zze(zzf2));
            }
            i2++;
        }
        int length = this.zzl.length;
        while (i < length) {
            this.zzp.zzb(obj, (long) this.zzl[i]);
            i++;
        }
        this.zzq.zzd(obj);
        if (this.zzh) {
            this.zzr.zzc(obj);
        }
    }

    private final <UT, UB> UB zza(Object obj, int i, UB ub, zzhx<UT, UB> zzhx) {
        zzfi zzc2;
        int i2 = this.zzc[i];
        Object zzf2 = zzid.zzf(obj, (long) (zzd(i) & 1048575));
        if (zzf2 == null || (zzc2 = zzc(i)) == null) {
            return ub;
        }
        return zza(i, i2, this.zzs.zza(zzf2), zzc2, ub, zzhx);
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map map, zzfi zzfi, Object obj, zzhx zzhx) {
        zzgh<?, ?> zzb2 = this.zzs.zzb(zzb(i));
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (!zzfi.zza(((Integer) entry.getValue()).intValue())) {
                if (obj == null) {
                    obj = zzhx.zza();
                }
                zzee zzc2 = zzdw.zzc(zzge.zza(zzb2, entry.getKey(), entry.getValue()));
                try {
                    zzge.zza(zzc2.zzb(), zzb2, entry.getKey(), entry.getValue());
                    zzhx.zza(obj, i2, zzc2.zza());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return obj;
    }

    public final boolean zzd(T t) {
        int i;
        int i2 = 0;
        int i3 = -1;
        int i4 = 0;
        while (true) {
            boolean z = true;
            if (i2 >= this.zzm) {
                return !this.zzh || this.zzr.zza(t).zzf();
            }
            int i5 = this.zzl[i2];
            int i6 = this.zzc[i5];
            int zzd2 = zzd(i5);
            if (!this.zzj) {
                int i7 = this.zzc[i5 + 2];
                int i8 = i7 & 1048575;
                i = 1 << (i7 >>> 20);
                if (i8 != i3) {
                    i4 = zzb.getInt(t, (long) i8);
                    i3 = i8;
                }
            } else {
                i = 0;
            }
            if (((268435456 & zzd2) != 0) && !zza(t, i5, i4, i)) {
                return false;
            }
            int i9 = (267386880 & zzd2) >>> 20;
            if (i9 != 9 && i9 != 17) {
                if (i9 != 27) {
                    if (i9 == 60 || i9 == 68) {
                        if (zza(t, i6, i5) && !zza(t, zzd2, zza(i5))) {
                            return false;
                        }
                    } else if (i9 != 49) {
                        if (i9 != 50) {
                            continue;
                        } else {
                            Map<?, ?> zzc2 = this.zzs.zzc(zzid.zzf(t, (long) (zzd2 & 1048575)));
                            if (!zzc2.isEmpty()) {
                                if (this.zzs.zzb(zzb(i5)).zzc.zza() == zzir.MESSAGE) {
                                    zzhf zzhf = null;
                                    Iterator<?> it = zzc2.values().iterator();
                                    while (true) {
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        Object next = it.next();
                                        if (zzhf == null) {
                                            zzhf = zzhb.zza().zza((Class) next.getClass());
                                        }
                                        if (!zzhf.zzd(next)) {
                                            z = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!z) {
                                return false;
                            }
                        }
                    }
                }
                List list = (List) zzid.zzf(t, (long) (zzd2 & 1048575));
                if (!list.isEmpty()) {
                    zzhf zza2 = zza(i5);
                    int i10 = 0;
                    while (true) {
                        if (i10 >= list.size()) {
                            break;
                        } else if (!zza2.zzd(list.get(i10))) {
                            z = false;
                            break;
                        } else {
                            i10++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (zza(t, i5, i4, i) && !zza(t, zzd2, zza(i5))) {
                return false;
            }
            i2++;
        }
    }

    private static boolean zza(Object obj, int i, zzhf zzhf) {
        return zzhf.zzd(zzid.zzf(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zziq zziq) throws IOException {
        if (obj instanceof String) {
            zziq.zza(i, (String) obj);
        } else {
            zziq.zza(i, (zzdw) obj);
        }
    }

    private final void zza(Object obj, int i, zzhc zzhc) throws IOException {
        if (zzf(i)) {
            zzid.zza(obj, (long) (i & 1048575), zzhc.zzm());
        } else if (this.zzi) {
            zzid.zza(obj, (long) (i & 1048575), zzhc.zzl());
        } else {
            zzid.zza(obj, (long) (i & 1048575), zzhc.zzn());
        }
    }

    private final int zzd(int i) {
        return this.zzc[i + 1];
    }

    private final int zze(int i) {
        return this.zzc[i + 2];
    }

    private static <T> double zzb(T t, long j) {
        return ((Double) zzid.zzf(t, j)).doubleValue();
    }

    private static <T> float zzc(T t, long j) {
        return ((Float) zzid.zzf(t, j)).floatValue();
    }

    private static <T> int zzd(T t, long j) {
        return ((Integer) zzid.zzf(t, j)).intValue();
    }

    private static <T> long zze(T t, long j) {
        return ((Long) zzid.zzf(t, j)).longValue();
    }

    private static <T> boolean zzf(T t, long j) {
        return ((Boolean) zzid.zzf(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zza(t, i) == zza(t2, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.zzgq.zza(int, int):int
      com.google.android.gms.internal.measurement.zzgq.zza(com.google.android.gms.internal.measurement.zzhx, java.lang.Object):int
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzgq.zza(java.lang.Object, int):boolean */
    private final boolean zza(T t, int i, int i2, int i3) {
        if (this.zzj) {
            return zza((Object) t, i);
        }
        return (i2 & i3) != 0;
    }

    private final boolean zza(T t, int i) {
        if (this.zzj) {
            int zzd2 = zzd(i);
            long j = (long) (zzd2 & 1048575);
            switch ((zzd2 & 267386880) >>> 20) {
                case 0:
                    return zzid.zze(t, j) != FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
                case 1:
                    return zzid.zzd(t, j) != 0.0f;
                case 2:
                    return zzid.zzb(t, j) != 0;
                case 3:
                    return zzid.zzb(t, j) != 0;
                case 4:
                    return zzid.zza(t, j) != 0;
                case 5:
                    return zzid.zzb(t, j) != 0;
                case 6:
                    return zzid.zza(t, j) != 0;
                case 7:
                    return zzid.zzc(t, j);
                case 8:
                    Object zzf2 = zzid.zzf(t, j);
                    if (zzf2 instanceof String) {
                        return !((String) zzf2).isEmpty();
                    }
                    if (zzf2 instanceof zzdw) {
                        return !zzdw.zza.equals(zzf2);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzid.zzf(t, j) != null;
                case 10:
                    return !zzdw.zza.equals(zzid.zzf(t, j));
                case 11:
                    return zzid.zza(t, j) != 0;
                case 12:
                    return zzid.zza(t, j) != 0;
                case 13:
                    return zzid.zza(t, j) != 0;
                case 14:
                    return zzid.zzb(t, j) != 0;
                case 15:
                    return zzid.zza(t, j) != 0;
                case 16:
                    return zzid.zzb(t, j) != 0;
                case 17:
                    return zzid.zzf(t, j) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int zze2 = zze(i);
            return (zzid.zza(t, (long) (zze2 & 1048575)) & (1 << (zze2 >>> 20))) != 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void */
    private final void zzb(T t, int i) {
        if (!this.zzj) {
            int zze2 = zze(i);
            long j = (long) (zze2 & 1048575);
            zzid.zza((Object) t, j, zzid.zza(t, j) | (1 << (zze2 >>> 20)));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzid.zza(t, (long) (zze(i2) & 1048575)) == i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.zzid.zza(byte[], long, byte):void
      com.google.android.gms.internal.measurement.zzid.zza(java.lang.Object, long, int):void */
    private final void zzb(T t, int i, int i2) {
        zzid.zza((Object) t, (long) (zze(i2) & 1048575), i);
    }

    private final int zzg(int i) {
        if (i < this.zze || i > this.zzf) {
            return -1;
        }
        return zzb(i, 0);
    }

    private final int zza(int i, int i2) {
        if (i < this.zze || i > this.zzf) {
            return -1;
        }
        return zzb(i, i2);
    }

    private final int zzb(int i, int i2) {
        int length = (this.zzc.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.zzc[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }
}
