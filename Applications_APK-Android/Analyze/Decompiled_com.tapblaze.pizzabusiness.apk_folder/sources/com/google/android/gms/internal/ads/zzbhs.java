package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zza;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbhs implements zzdxg<zza> {
    private final zzbhq zzfao;

    public zzbhs(zzbhq zzbhq) {
        this.zzfao = zzbhq;
    }

    public final /* synthetic */ Object get() {
        return (zza) zzdxm.zza(this.zzfao.zzaex(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
