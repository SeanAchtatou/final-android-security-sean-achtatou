package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zziv;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zziw implements Parcelable.Creator<zziv.zza> {
    zziw() {
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zziv.zza[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zziv.zza(parcel);
    }
}
