package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzdn  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzdn extends zzfc<zzdn, zzb> implements zzgn {
    private static volatile zzgv<zzdn> zzij;
    /* access modifiers changed from: private */
    public static final zzdn zzmc;
    private int zzie;
    private zzgf<String, String> zzit = zzgf.zzib();
    private long zzkn;
    private zzfm<zzde> zzkr = zzhh();
    private String zzlx = "";
    private boolean zzly;
    private long zzlz;
    private zzgf<String, Long> zzma = zzgf.zzib();
    private zzfm<zzdn> zzmb = zzhh();

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzdn$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static final class zza {
        static final zzgd<String, Long> zziv = zzgd.zza(zzih.STRING, "", zzih.INT64, 0L);
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzdn$zzc */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static final class zzc {
        static final zzgd<String, String> zziv = zzgd.zza(zzih.STRING, "", zzih.STRING, "");
    }

    private zzdn() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzdn$zzb */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zzb extends zzfc.zzb<zzdn, zzb> implements zzgn {
        private zzb() {
            super(zzdn.zzmc);
        }

        public final zzb zzah(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).setName(str);
            return this;
        }

        public final zzb zzao(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).zzad(j);
            return this;
        }

        public final zzb zzap(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).zzan(j);
            return this;
        }

        public final zzb zzc(String str, long j) {
            str.getClass();
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).zzfs().put(str, Long.valueOf(j));
            return this;
        }

        public final zzb zzd(Map<String, Long> map) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).zzfs().putAll(map);
            return this;
        }

        public final zzb zzf(zzdn zzdn) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).zzc(zzdn);
            return this;
        }

        public final zzb zzd(Iterable<? extends zzdn> iterable) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).zzc(iterable);
            return this;
        }

        public final zzb zze(Map<String, String> map) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).zzdl().putAll(map);
            return this;
        }

        public final zzb zzb(zzde zzde) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).zza(zzde);
            return this;
        }

        public final zzb zze(Iterable<? extends zzde> iterable) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzdn) this.zzqq).zza(iterable);
            return this;
        }

        /* synthetic */ zzb(zzdm zzdm) {
            this();
        }
    }

    public final String getName() {
        return this.zzlx;
    }

    /* access modifiers changed from: private */
    public final void setName(String str) {
        str.getClass();
        this.zzie |= 1;
        this.zzlx = str;
    }

    public final boolean zzep() {
        return (this.zzie & 4) != 0;
    }

    /* access modifiers changed from: private */
    public final void zzad(long j) {
        this.zzie |= 4;
        this.zzkn = j;
    }

    public final long getDurationUs() {
        return this.zzlz;
    }

    /* access modifiers changed from: private */
    public final void zzan(long j) {
        this.zzie |= 8;
        this.zzlz = j;
    }

    public final int zzfq() {
        return this.zzma.size();
    }

    public final Map<String, Long> zzfr() {
        return Collections.unmodifiableMap(this.zzma);
    }

    /* access modifiers changed from: private */
    public final Map<String, Long> zzfs() {
        if (!this.zzma.isMutable()) {
            this.zzma = this.zzma.zzic();
        }
        return this.zzma;
    }

    public final List<zzdn> zzft() {
        return this.zzmb;
    }

    private final void zzfu() {
        if (!this.zzmb.zzgf()) {
            this.zzmb = zzfc.zza(this.zzmb);
        }
    }

    /* access modifiers changed from: private */
    public final void zzc(zzdn zzdn) {
        zzdn.getClass();
        zzfu();
        this.zzmb.add(zzdn);
    }

    /* access modifiers changed from: private */
    public final void zzc(Iterable<? extends zzdn> iterable) {
        zzfu();
        zzdt.zza(iterable, this.zzmb);
    }

    public final Map<String, String> zzfv() {
        return Collections.unmodifiableMap(this.zzit);
    }

    /* access modifiers changed from: private */
    public final Map<String, String> zzdl() {
        if (!this.zzit.isMutable()) {
            this.zzit = this.zzit.zzic();
        }
        return this.zzit;
    }

    public final List<zzde> zzex() {
        return this.zzkr;
    }

    private final void zzfw() {
        if (!this.zzkr.zzgf()) {
            this.zzkr = zzfc.zza(this.zzkr);
        }
    }

    /* access modifiers changed from: private */
    public final void zza(zzde zzde) {
        zzde.getClass();
        zzfw();
        this.zzkr.add(zzde);
    }

    /* access modifiers changed from: private */
    public final void zza(Iterable<? extends zzde> iterable) {
        zzfw();
        zzdt.zza(iterable, this.zzkr);
    }

    public static zzb zzfx() {
        return (zzb) zzmc.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        Class<zzdn> cls = zzdn.class;
        switch (zzdm.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzdn();
            case 2:
                return new zzb(null);
            case 3:
                return zza(zzmc, "\u0001\b\u0000\u0001\u0001\t\b\u0002\u0002\u0000\u0001\b\u0000\u0002\u0007\u0001\u0004\u0002\u0002\u0005\u0002\u0003\u00062\u0007\u001b\b2\t\u001b", new Object[]{"zzie", "zzlx", "zzly", "zzkn", "zzlz", "zzma", zza.zziv, "zzmb", cls, "zzit", zzc.zziv, "zzkr", zzde.class});
            case 4:
                return zzmc;
            case 5:
                zzgv<zzdn> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (cls) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzmc);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzdn zzfy() {
        return zzmc;
    }

    static {
        zzdn zzdn = new zzdn();
        zzmc = zzdn;
        zzfc.zza(zzdn.class, zzdn);
    }
}
