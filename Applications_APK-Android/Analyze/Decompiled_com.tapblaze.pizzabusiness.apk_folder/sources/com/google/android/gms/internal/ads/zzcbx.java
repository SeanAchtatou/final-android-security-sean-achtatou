package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcbx implements zzbtk {
    private final zzbdi zzehp;

    private zzcbx(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    static zzbtk zzn(zzbdi zzbdi) {
        return new zzcbx(zzbdi);
    }

    public final void zzaib() {
        this.zzehp.destroy();
    }
}
