package com.google.android.gms.internal.ads;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzduv implements Iterator<String> {
    private Iterator<String> zzhrg = this.zzhrh.zzhrf.iterator();
    private final /* synthetic */ zzdut zzhrh;

    zzduv(zzdut zzdut) {
        this.zzhrh = zzdut;
    }

    public final boolean hasNext() {
        return this.zzhrg.hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ Object next() {
        return this.zzhrg.next();
    }
}
