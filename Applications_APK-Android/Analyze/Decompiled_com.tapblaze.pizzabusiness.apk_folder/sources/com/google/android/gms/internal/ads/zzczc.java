package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzczc {
    zzczd zzaer();

    zzczc zzbu(Context context);

    zzczc zzfr(String str);
}
