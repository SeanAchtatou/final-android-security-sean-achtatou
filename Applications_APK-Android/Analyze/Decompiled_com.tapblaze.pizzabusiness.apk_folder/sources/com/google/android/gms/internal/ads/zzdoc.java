package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdoc extends zzdrt<zzdoc, zza> implements zzdtg {
    private static volatile zzdtn<zzdoc> zzdz;
    /* access modifiers changed from: private */
    public static final zzdoc zzheu;

    private zzdoc() {
    }

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static final class zza extends zzdrt.zzb<zzdoc, zza> implements zzdtg {
        private zza() {
            super(zzdoc.zzheu);
        }

        /* synthetic */ zza(zzdob zzdob) {
            this();
        }
    }

    public static zzdoc zzbd(zzdqk zzdqk) throws zzdse {
        return (zzdoc) zzdrt.zza(zzheu, zzdqk);
    }

    /* access modifiers changed from: protected */
    public final Object zza(int i, Object obj, Object obj2) {
        switch (zzdob.zzdk[i - 1]) {
            case 1:
                return new zzdoc();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzheu, "\u0000\u0000", (Object[]) null);
            case 4:
                return zzheu;
            case 5:
                zzdtn<zzdoc> zzdtn = zzdz;
                if (zzdtn == null) {
                    synchronized (zzdoc.class) {
                        zzdtn = zzdz;
                        if (zzdtn == null) {
                            zzdtn = new zzdrt.zza<>(zzheu);
                            zzdz = zzdtn;
                        }
                    }
                }
                return zzdtn;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzdoc zzdoc = new zzdoc();
        zzheu = zzdoc;
        zzdrt.zza(zzdoc.class, zzdoc);
    }
}
