package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.internal.ads.zzavs;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzk extends WebViewClient {
    private final /* synthetic */ zzl zzblk;

    zzk(zzl zzl) {
        this.zzblk = zzl;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith(this.zzblk.zzkf())) {
            return false;
        }
        if (str.startsWith("gmsg://noAdLoaded")) {
            if (this.zzblk.zzblq != null) {
                try {
                    this.zzblk.zzblq.onAdFailedToLoad(3);
                } catch (RemoteException e) {
                    zzavs.zze("#007 Could not call remote method.", e);
                }
            }
            this.zzblk.zzbm(0);
            return true;
        } else if (str.startsWith("gmsg://scriptLoadFailed")) {
            if (this.zzblk.zzblq != null) {
                try {
                    this.zzblk.zzblq.onAdFailedToLoad(0);
                } catch (RemoteException e2) {
                    zzavs.zze("#007 Could not call remote method.", e2);
                }
            }
            this.zzblk.zzbm(0);
            return true;
        } else if (str.startsWith("gmsg://adResized")) {
            if (this.zzblk.zzblq != null) {
                try {
                    this.zzblk.zzblq.onAdLoaded();
                } catch (RemoteException e3) {
                    zzavs.zze("#007 Could not call remote method.", e3);
                }
            }
            this.zzblk.zzbm(this.zzblk.zzbs(str));
            return true;
        } else if (str.startsWith("gmsg://")) {
            return true;
        } else {
            if (this.zzblk.zzblq != null) {
                try {
                    this.zzblk.zzblq.onAdLeftApplication();
                } catch (RemoteException e4) {
                    zzavs.zze("#007 Could not call remote method.", e4);
                }
            }
            this.zzblk.zzbu(this.zzblk.zzbt(str));
            return true;
        }
    }

    public final void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        if (this.zzblk.zzblq != null) {
            try {
                this.zzblk.zzblq.onAdFailedToLoad(0);
            } catch (RemoteException e) {
                zzavs.zze("#007 Could not call remote method.", e);
            }
        }
    }
}
