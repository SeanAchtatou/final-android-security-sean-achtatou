package com.google.android.gms.internal.ads;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdht extends RuntimeException {
    protected zzdht() {
    }

    public zzdht(@NullableDecl Throwable th) {
        super(th);
    }
}
