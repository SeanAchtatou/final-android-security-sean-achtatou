package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbpu;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcip<AdapterT, ListenerT extends zzbpu> {
    public final AdapterT zzddn;
    public final String zzfge;
    public final ListenerT zzfyf;

    public zzcip(AdapterT adaptert, ListenerT listenert, String str) {
        this.zzddn = adaptert;
        this.zzfyf = listenert;
        this.zzfge = str;
    }
}
