package com.google.android.gms.internal.ads;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzdsl extends List {
    List<?> zzbav();

    zzdsl zzbaw();

    void zzbg(zzdqk zzdqk);

    Object zzgm(int i);
}
