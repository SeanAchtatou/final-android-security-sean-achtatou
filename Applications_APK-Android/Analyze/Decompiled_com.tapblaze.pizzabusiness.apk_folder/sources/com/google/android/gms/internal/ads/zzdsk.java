package com.google.android.gms.internal.ads;

import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdsk<K> implements Iterator<Map.Entry<K, Object>> {
    private Iterator<Map.Entry<K, Object>> zzhoe;

    public zzdsk(Iterator<Map.Entry<K, Object>> it) {
        this.zzhoe = it;
    }

    public final boolean hasNext() {
        return this.zzhoe.hasNext();
    }

    public final void remove() {
        this.zzhoe.remove();
    }

    public final /* synthetic */ Object next() {
        Map.Entry next = this.zzhoe.next();
        return next.getValue() instanceof zzdsf ? new zzdsh(next) : next;
    }
}
