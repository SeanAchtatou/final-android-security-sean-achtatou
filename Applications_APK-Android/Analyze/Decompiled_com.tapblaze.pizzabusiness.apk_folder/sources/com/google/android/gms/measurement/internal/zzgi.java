package com.google.android.gms.measurement.internal;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class zzgi implements zzih {
    private final zzgf zza;

    zzgi(zzgf zzgf) {
        this.zza = zzgf;
    }

    public final void zza(String str, int i, Throwable th, byte[] bArr, Map map) {
        this.zza.zza(str, i, th, bArr, map);
    }
}
