package com.google.android.gms.gass;

import com.google.android.gms.internal.ads.zzbm;
import com.google.android.gms.internal.ads.zzdrt;
import com.google.android.gms.internal.ads.zzsr;
import com.google.android.gms.internal.ads.zzsv;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final /* synthetic */ class zza implements Continuation {
    private final int zzdtf;
    private final zzbm.zza.C0010zza zzgry;

    zza(zzbm.zza.C0010zza zza, int i) {
        this.zzgry = zza;
        this.zzdtf = i;
    }

    public final Object then(Task task) {
        zzbm.zza.C0010zza zza = this.zzgry;
        int i = this.zzdtf;
        if (!task.isSuccessful()) {
            return false;
        }
        zzsv zzf = ((zzsr) task.getResult()).zzf(((zzbm.zza) ((zzdrt) zza.zzbaf())).toByteArray());
        zzf.zzbr(i);
        zzf.zzdn();
        return true;
    }
}
