package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhc  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhc implements zzgj {
    private final int flags;
    private final String info;
    private final Object[] zzsw;
    private final zzgl zzsz;

    zzhc(zzgl zzgl, String str, Object[] objArr) {
        this.zzsz = zzgl;
        this.info = str;
        this.zzsw = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.flags = charAt;
            return;
        }
        char c = charAt & 8191;
        int i = 13;
        int i2 = 1;
        while (true) {
            int i3 = i2 + 1;
            char charAt2 = str.charAt(i2);
            if (charAt2 >= 55296) {
                c |= (charAt2 & 8191) << i;
                i += 13;
                i2 = i3;
            } else {
                this.flags = c | (charAt2 << i);
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final String zzip() {
        return this.info;
    }

    /* access modifiers changed from: package-private */
    public final Object[] zziq() {
        return this.zzsw;
    }

    public final zzgl zzig() {
        return this.zzsz;
    }

    public final int zzie() {
        return (this.flags & 1) == 1 ? zzgx.zzto : zzgx.zztp;
    }

    public final boolean zzif() {
        return (this.flags & 2) == 2;
    }
}
