package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzsr {
    zzgf zzbtx;
    boolean zzbty;

    public final zzsv zzf(byte[] bArr) {
        return new zzsv(this, bArr);
    }

    public zzsr(Context context, String str, String str2) {
        zzzn.initialize(context);
        try {
            this.zzbtx = (zzgf) zzayx.zza(context, "com.google.android.gms.ads.clearcut.DynamiteClearcutLogger", zzsu.zzbtz);
            ObjectWrapper.wrap(context);
            this.zzbtx.zza(ObjectWrapper.wrap(context), str, null);
            this.zzbty = true;
        } catch (RemoteException | zzayz | NullPointerException unused) {
            zzayu.zzea("Cannot dynamite load clearcut");
        }
    }

    public zzsr(Context context) {
        zzzn.initialize(context);
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnw)).booleanValue()) {
            try {
                this.zzbtx = (zzgf) zzayx.zza(context, "com.google.android.gms.ads.clearcut.DynamiteClearcutLogger", zzst.zzbtz);
                ObjectWrapper.wrap(context);
                this.zzbtx.zza(ObjectWrapper.wrap(context), "GMA_SDK");
                this.zzbty = true;
            } catch (RemoteException | zzayz | NullPointerException unused) {
                zzayu.zzea("Cannot dynamite load clearcut");
            }
        }
    }

    public zzsr() {
    }
}
