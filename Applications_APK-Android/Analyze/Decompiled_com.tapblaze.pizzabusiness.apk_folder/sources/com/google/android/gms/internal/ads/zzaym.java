package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzaym extends Thread {
    private final /* synthetic */ String zzdug;

    zzaym(zzayn zzayn, String str) {
        this.zzdug = str;
    }

    public final void run() {
        new zzayy().zzen(this.zzdug);
    }
}
