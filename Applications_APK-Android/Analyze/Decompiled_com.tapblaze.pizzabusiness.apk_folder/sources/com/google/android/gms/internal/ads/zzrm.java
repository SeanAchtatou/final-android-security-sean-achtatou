package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.appopen.AppOpenAd;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzrm extends AppOpenAd {
    private final zzrf zzbqx;

    public zzrm(zzrf zzrf) {
        this.zzbqx = zzrf;
    }

    /* access modifiers changed from: protected */
    public final zzvu zzdm() {
        try {
            return this.zzbqx.zzdm();
        } catch (RemoteException e) {
            zzayu.zzc("", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void zza(zzrl zzrl) {
        try {
            this.zzbqx.zza(zzrl);
        } catch (RemoteException e) {
            zzayu.zzc("", e);
        }
    }
}
