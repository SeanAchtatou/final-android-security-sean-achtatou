package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbbb implements Runnable {
    private boolean zzbpe = false;
    private zzbai zzdyq;

    zzbbb(zzbai zzbai) {
        this.zzdyq = zzbai;
    }

    public final void run() {
        if (!this.zzbpe) {
            this.zzdyq.zzyb();
            zzyw();
        }
    }

    public final void pause() {
        this.zzbpe = true;
        this.zzdyq.zzyb();
    }

    public final void resume() {
        this.zzbpe = false;
        zzyw();
    }

    private final void zzyw() {
        zzawb.zzdsr.removeCallbacks(this);
        zzawb.zzdsr.postDelayed(this, 250);
    }
}
