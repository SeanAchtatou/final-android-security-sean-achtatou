package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdwd implements zzdsa {
    static final zzdsa zzew = new zzdwd();

    private zzdwd() {
    }

    public final boolean zzf(int i) {
        return zzdvx.zzb.zzf.C0022zzb.zzhe(i) != null;
    }
}
