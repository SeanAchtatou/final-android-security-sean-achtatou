package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzca;
import com.google.android.gms.internal.p000firebaseperf.zzfc;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzce  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzce extends zzfc<zzce, zzb> implements zzgn {
    private static volatile zzgv<zzce> zzij;
    /* access modifiers changed from: private */
    public static final zzce zziu;
    private int zzie;
    private String zzin = "";
    private String zzio = "";
    private zzca zzip;
    private zzcr zziq;
    private zzds zzir;
    private int zzis;
    private zzgf<String, String> zzit = zzgf.zzib();

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzce$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    static final class zza {
        static final zzgd<String, String> zziv = zzgd.zza(zzih.STRING, "", zzih.STRING, "");
    }

    private zzce() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzce$zzb */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zzb extends zzfc.zzb<zzce, zzb> implements zzgn {
        private zzb() {
            super(zzce.zziu);
        }

        public final zzb zzy(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzce) this.zzqq).zzw(str);
            return this;
        }

        public final boolean hasAppInstanceId() {
            return ((zzce) this.zzqq).hasAppInstanceId();
        }

        public final zzb zzz(String str) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzce) this.zzqq).zzx(str);
            return this;
        }

        public final zzb zza(zzca.zza zza) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzce) this.zzqq).zza((zzca) ((zzfc) zza.zzhp()));
            return this;
        }

        public final zzb zzf(zzcg zzcg) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzce) this.zzqq).zze(zzcg);
            return this;
        }

        public final zzb zzb(Map<String, String> map) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzce) this.zzqq).zzdl().putAll(map);
            return this;
        }

        /* synthetic */ zzb(zzcd zzcd) {
            this();
        }
    }

    public final boolean zzdh() {
        return (this.zzie & 1) != 0;
    }

    /* access modifiers changed from: private */
    public final void zzw(String str) {
        str.getClass();
        this.zzie |= 1;
        this.zzin = str;
    }

    public final boolean hasAppInstanceId() {
        return (this.zzie & 2) != 0;
    }

    /* access modifiers changed from: private */
    public final void zzx(String str) {
        str.getClass();
        this.zzie |= 2;
        this.zzio = str;
    }

    public final boolean zzdi() {
        return (this.zzie & 4) != 0;
    }

    public final zzca zzdj() {
        zzca zzca = this.zzip;
        return zzca == null ? zzca.zzdd() : zzca;
    }

    /* access modifiers changed from: private */
    public final void zza(zzca zzca) {
        zzca.getClass();
        this.zzip = zzca;
        this.zzie |= 4;
    }

    public final boolean zzdk() {
        return (this.zzie & 32) != 0;
    }

    /* access modifiers changed from: private */
    public final void zze(zzcg zzcg) {
        this.zzis = zzcg.getNumber();
        this.zzie |= 32;
    }

    /* access modifiers changed from: private */
    public final Map<String, String> zzdl() {
        if (!this.zzit.isMutable()) {
            this.zzit = this.zzit.zzic();
        }
        return this.zzit;
    }

    public static zzb zzdm() {
        return (zzb) zziu.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzcd.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzce();
            case 2:
                return new zzb(null);
            case 3:
                return zza(zziu, "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0001\u0000\u0000\u0001\b\u0000\u0002\b\u0001\u0003\t\u0002\u0004\t\u0003\u0005\f\u0005\u00062\u0007\t\u0004", new Object[]{"zzie", "zzin", "zzio", "zzip", "zziq", "zzis", zzcg.zzdp(), "zzit", zza.zziv, "zzir"});
            case 4:
                return zziu;
            case 5:
                zzgv<zzce> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzce.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zziu);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static zzce zzdn() {
        return zziu;
    }

    static {
        zzce zzce = new zzce();
        zziu = zzce;
        zzfc.zza(zzce.class, zzce);
    }
}
