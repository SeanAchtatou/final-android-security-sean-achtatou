package com.google.android.gms.ads.appopen;

import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzrc;
import com.google.android.gms.internal.ads.zzuj;
import com.google.android.gms.internal.ads.zzvu;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class AppOpenAdView extends ViewGroup {
    private AppOpenAd zzaby;
    private AppOpenAdPresentationCallback zzabz;

    public AppOpenAdView(Context context) {
        super(context);
        Preconditions.checkNotNull(context, "Context cannot be null");
    }

    public AppOpenAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AppOpenAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void setAppOpenAd(AppOpenAd appOpenAd) {
        IObjectWrapper zzjx;
        try {
            zzvu zzdm = appOpenAd.zzdm();
            if (zzdm != null && (zzjx = zzdm.zzjx()) != null) {
                View view = (View) ObjectWrapper.unwrap(zzjx);
                if (view.getParent() == null) {
                    removeAllViews();
                    addView(view);
                    this.zzaby = appOpenAd;
                    zzdo();
                    return;
                }
                zzayu.zzex("Trying to set AppOpenAd which is already in use.");
            }
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
        }
    }

    public final void setAppOpenAdPresentationCallback(AppOpenAdPresentationCallback appOpenAdPresentationCallback) {
        this.zzabz = appOpenAdPresentationCallback;
        zzdo();
    }

    private final void zzdo() {
        AppOpenAdPresentationCallback appOpenAdPresentationCallback;
        AppOpenAd appOpenAd = this.zzaby;
        if (appOpenAd != null && (appOpenAdPresentationCallback = this.zzabz) != null) {
            appOpenAd.zza(new zzrc(appOpenAdPresentationCallback));
        }
    }

    private final AdSize getAdSize() {
        zzvu zzdm = this.zzaby.zzdm();
        if (zzdm == null) {
            return null;
        }
        try {
            zzuj zzjz = zzdm.zzjz();
            if (zzjz != null) {
                return zzjz.zzoo();
            }
            return null;
        } catch (RemoteException e) {
            zzayu.zze("#007 Could not call remote method.", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i5 = ((i3 - i) - measuredWidth) / 2;
            int i6 = ((i4 - i2) - measuredHeight) / 2;
            childAt.layout(i5, i6, measuredWidth + i5, measuredHeight + i6);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int i3;
        int i4 = 0;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e) {
                zzayu.zzc("Unable to retrieve ad size.", e);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i3 = adSize.getHeightInPixels(context);
                i4 = widthInPixels;
            } else {
                i3 = 0;
            }
        } else {
            measureChild(childAt, i, i2);
            i4 = childAt.getMeasuredWidth();
            i3 = childAt.getMeasuredHeight();
        }
        setMeasuredDimension(View.resolveSize(Math.max(i4, getSuggestedMinimumWidth()), i), View.resolveSize(Math.max(i3, getSuggestedMinimumHeight()), i2));
    }
}
