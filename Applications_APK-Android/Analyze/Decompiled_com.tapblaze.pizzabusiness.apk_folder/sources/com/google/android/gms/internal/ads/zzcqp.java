package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcqp implements Callable {
    private final zzcqm zzgez;

    zzcqp(zzcqm zzcqm) {
        this.zzgez = zzcqm;
    }

    public final Object call() {
        return this.zzgez.zzane();
    }
}
