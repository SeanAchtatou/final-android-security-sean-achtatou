package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzxz extends zzvq {
    /* access modifiers changed from: private */
    public zzvh zzblq;

    public final void zza(PublisherAdViewOptions publisherAdViewOptions) throws RemoteException {
    }

    public final void zza(zzaby zzaby) throws RemoteException {
    }

    public final void zza(zzadi zzadi) throws RemoteException {
    }

    public final void zza(zzadj zzadj) throws RemoteException {
    }

    public final void zza(zzadu zzadu, zzuj zzuj) throws RemoteException {
    }

    public final void zza(zzadv zzadv) throws RemoteException {
    }

    public final void zza(zzagz zzagz) throws RemoteException {
    }

    public final void zza(zzahh zzahh) throws RemoteException {
    }

    public final void zza(String str, zzadp zzadp, zzado zzado) throws RemoteException {
    }

    public final void zzb(zzwi zzwi) throws RemoteException {
    }

    public final void zzb(zzvh zzvh) throws RemoteException {
        this.zzblq = zzvh;
    }

    public final zzvm zzpd() throws RemoteException {
        return new zzyb(this);
    }
}
