package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdij {
    private zzdnk zzgxy;

    private zzdij(zzdnk zzdnk) {
        this.zzgxy = zzdnk;
    }

    static final zzdij zza(zzdnk zzdnk) throws GeneralSecurityException {
        if (zzdnk != null && zzdnk.zzavx() > 0) {
            return new zzdij(zzdnk);
        }
        throw new GeneralSecurityException("empty keyset");
    }

    /* access modifiers changed from: package-private */
    public final zzdnk zzash() {
        return this.zzgxy;
    }

    public final String toString() {
        return zzdiz.zzb(this.zzgxy).toString();
    }
}
