package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzaif extends zzajq {
    void destroy();

    boolean isDestroyed();

    void zza(zzaii zzaii);

    void zzcv(String str);

    void zzcw(String str);

    void zzcx(String str);

    zzajp zzrz();
}
