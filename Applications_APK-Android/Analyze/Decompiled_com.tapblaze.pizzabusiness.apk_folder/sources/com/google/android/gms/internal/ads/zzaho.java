package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.instream.InstreamAd;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaho extends zzahk {
    private final InstreamAd.InstreamAdLoadCallback zzcyl;

    public zzaho(InstreamAd.InstreamAdLoadCallback instreamAdLoadCallback) {
        this.zzcyl = instreamAdLoadCallback;
    }

    public final void zza(zzahb zzahb) {
        this.zzcyl.onInstreamAdLoaded(new zzahm(zzahb));
    }

    public final void onInstreamAdFailedToLoad(int i) {
        this.zzcyl.onInstreamAdFailedToLoad(i);
    }
}
