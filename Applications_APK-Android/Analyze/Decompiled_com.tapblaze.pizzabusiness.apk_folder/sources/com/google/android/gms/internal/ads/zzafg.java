package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzc;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzafg implements zzafn<zzbdi> {
    zzafg() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        if (zzbdi.zzaao() != null) {
            zzbdi.zzaao().zzmm();
        }
        zzc zzzw = zzbdi.zzzw();
        if (zzzw != null) {
            zzzw.close();
            return;
        }
        zzc zzzx = zzbdi.zzzx();
        if (zzzx != null) {
            zzzx.close();
        } else {
            zzavs.zzez("A GMSG tried to close something that wasn't an overlay.");
        }
    }
}
