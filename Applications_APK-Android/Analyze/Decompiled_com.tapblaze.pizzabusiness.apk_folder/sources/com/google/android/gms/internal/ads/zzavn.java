package com.google.android.gms.internal.ads;

import com.facebook.appevents.AppEventsConstants;
import java.math.BigInteger;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzavn {
    private String zzdir = AppEventsConstants.EVENT_PARAM_VALUE_NO;
    private BigInteger zzdrn = BigInteger.ONE;

    public final synchronized String zzvp() {
        String bigInteger;
        bigInteger = this.zzdrn.toString();
        this.zzdrn = this.zzdrn.add(BigInteger.ONE);
        this.zzdir = bigInteger;
        return bigInteger;
    }

    public final synchronized String zzvq() {
        return this.zzdir;
    }
}
