package com.google.android.gms.ads.mediation;

import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface MediationBannerAd {
    View getView();
}
