package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzate implements zzdgf {
    private final zzatf zzdoc;

    zzate(zzatf zzatf) {
        this.zzdoc = zzatf;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzdoc.zzh((Map) obj);
    }
}
