package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaoa implements Runnable {
    private final /* synthetic */ zzany zzdew;
    private final /* synthetic */ AdOverlayInfoParcel zzdfe;

    zzaoa(zzany zzany, AdOverlayInfoParcel adOverlayInfoParcel) {
        this.zzdew = zzany;
        this.zzdfe = adOverlayInfoParcel;
    }

    public final void run() {
        zzq.zzkp();
        zzn.zza(this.zzdew.zzdex, this.zzdfe, true);
    }
}
