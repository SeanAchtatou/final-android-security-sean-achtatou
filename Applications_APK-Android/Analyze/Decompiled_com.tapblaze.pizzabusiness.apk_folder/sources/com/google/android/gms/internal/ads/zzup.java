package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class zzup {
    /* access modifiers changed from: private */
    public final zzue zzccx;
    /* access modifiers changed from: private */
    public final zzub zzccy;
    /* access modifiers changed from: private */
    public final zzya zzccz;
    /* access modifiers changed from: private */
    public final zzaej zzcda;
    /* access modifiers changed from: private */
    public final zzars zzcdb;
    private final zzasw zzcdc;
    /* access modifiers changed from: private */
    public final zzaor zzcdd;
    /* access modifiers changed from: private */
    public final zzaem zzcde;

    public zzup(zzue zzue, zzub zzub, zzya zzya, zzaej zzaej, zzars zzars, zzasw zzasw, zzaor zzaor, zzaem zzaem) {
        this.zzccx = zzue;
        this.zzccy = zzub;
        this.zzccz = zzya;
        this.zzcda = zzaej;
        this.zzcdb = zzars;
        this.zzcdc = zzasw;
        this.zzcdd = zzaor;
        this.zzcde = zzaem;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzayk.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, android.os.Bundle, int]
     candidates:
      com.google.android.gms.internal.ads.zzayk.zza(android.view.ViewGroup, com.google.android.gms.internal.ads.zzuj, java.lang.String, int, int):void
      com.google.android.gms.internal.ads.zzayk.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle, boolean):void */
    /* access modifiers changed from: private */
    public static void zza(Context context, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("action", "no_ads_fallback");
        bundle.putString("flow", str);
        zzve.zzou().zza(context, zzve.zzpa().zzbma, "gmob-apps", bundle, true);
    }

    public final zzvu zza(Context context, zzuj zzuj, String str, zzalc zzalc) {
        return (zzvu) new zzut(this, context, zzuj, str, zzalc).zzd(context, false);
    }

    public final zzvn zzb(Context context, String str, zzalc zzalc) {
        return (zzvn) new zzuy(this, context, str, zzalc).zzd(context, false);
    }

    public final zzacm zza(Context context, FrameLayout frameLayout, FrameLayout frameLayout2) {
        return (zzacm) new zzva(this, frameLayout, frameLayout2, context).zzd(context, false);
    }

    public final zzacp zza(View view, HashMap<String, View> hashMap, HashMap<String, View> hashMap2) {
        return (zzacp) new zzuz(this, view, hashMap, hashMap2).zzd(view.getContext(), false);
    }

    public final zzasg zzc(Context context, String str, zzalc zzalc) {
        return (zzasg) new zzur(this, context, str, zzalc).zzd(context, false);
    }

    public final zzaot zzb(Activity activity) {
        zzuu zzuu = new zzuu(this, activity);
        Intent intent = activity.getIntent();
        boolean z = false;
        if (!intent.hasExtra("com.google.android.gms.ads.internal.overlay.useClientJar")) {
            zzayu.zzex("useClientJar flag not found in activity intent extras.");
        } else {
            z = intent.getBooleanExtra("com.google.android.gms.ads.internal.overlay.useClientJar", false);
        }
        return (zzaot) zzuu.zzd(activity, z);
    }
}
