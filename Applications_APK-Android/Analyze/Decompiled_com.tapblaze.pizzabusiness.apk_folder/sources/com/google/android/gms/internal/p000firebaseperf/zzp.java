package com.google.android.gms.internal.p000firebaseperf;

import com.ironsource.sdk.constants.Constants;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzp  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzp<K, V> extends zzo<K, V> {
    private static final zzo<Object, Object> zzk = new zzp(null, new Object[0], 0);
    private final transient int size;
    private final transient Object zzl;
    private final transient Object[] zzm;

    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [short[], byte[]], vars: [r0v4 ?, r0v2 ?, r0v5 ?, r0v6 ?]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:102)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:78)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:69)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:51)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:32)
        */
    static <K, V> com.google.android.gms.internal.p000firebaseperf.zzp<K, V> zza(int r10, java.lang.Object[] r11) {
        /*
            int r10 = r11.length
            r0 = 1
            int r10 = r10 >> r0
            r1 = 4
            com.google.android.gms.internal.p000firebaseperf.zzd.zzb(r1, r10)
            r10 = 2
            int r10 = java.lang.Math.max(r1, r10)
            r2 = 1073741824(0x40000000, float:2.0)
            r3 = 0
            r4 = 751619276(0x2ccccccc, float:5.8207657E-12)
            if (r10 >= r4) goto L_0x0030
            int r2 = r10 + -1
            int r2 = java.lang.Integer.highestOneBit(r2)
            int r0 = r2 << 1
            r2 = r0
        L_0x001d:
            double r4 = (double) r2
            r6 = 4604480259023595110(0x3fe6666666666666, double:0.7)
            java.lang.Double.isNaN(r4)
            double r4 = r4 * r6
            double r6 = (double) r10
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 >= 0) goto L_0x0039
            int r2 = r2 << 1
            goto L_0x001d
        L_0x0030:
            if (r10 >= r2) goto L_0x0033
            goto L_0x0034
        L_0x0033:
            r0 = 0
        L_0x0034:
            java.lang.String r10 = "collection too large"
            com.google.android.gms.internal.p000firebaseperf.zzd.checkArgument(r0, r10)
        L_0x0039:
            int r10 = r2 + -1
            r0 = 128(0x80, float:1.794E-43)
            r4 = -1
            if (r2 > r0) goto L_0x0078
            byte[] r0 = new byte[r2]
            java.util.Arrays.fill(r0, r4)
        L_0x0045:
            if (r3 >= r1) goto L_0x00ea
            int r2 = r3 * 2
            r4 = r11[r2]
            r5 = r2 ^ 1
            r5 = r11[r5]
            com.google.android.gms.internal.p000firebaseperf.zzi.zza(r4, r5)
            int r6 = r4.hashCode()
            int r6 = com.google.android.gms.internal.p000firebaseperf.zzh.zza(r6)
        L_0x005a:
            r6 = r6 & r10
            byte r7 = r0[r6]
            r8 = 255(0xff, float:3.57E-43)
            r7 = r7 & r8
            if (r7 != r8) goto L_0x0068
            byte r2 = (byte) r2
            r0[r6] = r2
            int r3 = r3 + 1
            goto L_0x0045
        L_0x0068:
            r8 = r11[r7]
            boolean r8 = r8.equals(r4)
            if (r8 != 0) goto L_0x0073
            int r6 = r6 + 1
            goto L_0x005a
        L_0x0073:
            java.lang.IllegalArgumentException r10 = zza(r4, r5, r11, r7)
            throw r10
        L_0x0078:
            r0 = 32768(0x8000, float:4.5918E-41)
            if (r2 > r0) goto L_0x00b6
            short[] r0 = new short[r2]
            java.util.Arrays.fill(r0, r4)
        L_0x0082:
            if (r3 >= r1) goto L_0x00ea
            int r2 = r3 * 2
            r4 = r11[r2]
            r5 = r2 ^ 1
            r5 = r11[r5]
            com.google.android.gms.internal.p000firebaseperf.zzi.zza(r4, r5)
            int r6 = r4.hashCode()
            int r6 = com.google.android.gms.internal.p000firebaseperf.zzh.zza(r6)
        L_0x0097:
            r6 = r6 & r10
            short r7 = r0[r6]
            r8 = 65535(0xffff, float:9.1834E-41)
            r7 = r7 & r8
            if (r7 != r8) goto L_0x00a6
            short r2 = (short) r2
            r0[r6] = r2
            int r3 = r3 + 1
            goto L_0x0082
        L_0x00a6:
            r8 = r11[r7]
            boolean r8 = r8.equals(r4)
            if (r8 != 0) goto L_0x00b1
            int r6 = r6 + 1
            goto L_0x0097
        L_0x00b1:
            java.lang.IllegalArgumentException r10 = zza(r4, r5, r11, r7)
            throw r10
        L_0x00b6:
            int[] r0 = new int[r2]
            java.util.Arrays.fill(r0, r4)
        L_0x00bb:
            if (r3 >= r1) goto L_0x00ea
            int r2 = r3 * 2
            r5 = r11[r2]
            r6 = r2 ^ 1
            r6 = r11[r6]
            com.google.android.gms.internal.p000firebaseperf.zzi.zza(r5, r6)
            int r7 = r5.hashCode()
            int r7 = com.google.android.gms.internal.p000firebaseperf.zzh.zza(r7)
        L_0x00d0:
            r7 = r7 & r10
            r8 = r0[r7]
            if (r8 != r4) goto L_0x00da
            r0[r7] = r2
            int r3 = r3 + 1
            goto L_0x00bb
        L_0x00da:
            r9 = r11[r8]
            boolean r9 = r9.equals(r5)
            if (r9 != 0) goto L_0x00e5
            int r7 = r7 + 1
            goto L_0x00d0
        L_0x00e5:
            java.lang.IllegalArgumentException r10 = zza(r5, r6, r11, r8)
            throw r10
        L_0x00ea:
            com.google.android.gms.internal.firebase-perf.zzp r10 = new com.google.android.gms.internal.firebase-perf.zzp
            r10.<init>(r0, r11, r1)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzp.zza(int, java.lang.Object[]):com.google.android.gms.internal.firebase-perf.zzp");
    }

    private static IllegalArgumentException zza(Object obj, Object obj2, Object[] objArr, int i) {
        String valueOf = String.valueOf(obj);
        String valueOf2 = String.valueOf(obj2);
        String valueOf3 = String.valueOf(objArr[i]);
        String valueOf4 = String.valueOf(objArr[i ^ 1]);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length());
        sb.append("Multiple entries with same key: ");
        sb.append(valueOf);
        sb.append(Constants.RequestParameters.EQUAL);
        sb.append(valueOf2);
        sb.append(" and ");
        sb.append(valueOf3);
        sb.append(Constants.RequestParameters.EQUAL);
        sb.append(valueOf4);
        return new IllegalArgumentException(sb.toString());
    }

    private zzp(Object obj, Object[] objArr, int i) {
        this.zzl = obj;
        this.zzm = objArr;
        this.size = i;
    }

    public final int size() {
        return this.size;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @org.checkerframework.checker.nullness.compatqual.NullableDecl
    public final V get(@org.checkerframework.checker.nullness.compatqual.NullableDecl java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r0 = r8.zzl
            java.lang.Object[] r1 = r8.zzm
            int r2 = r8.size
            r3 = 0
            if (r9 != 0) goto L_0x000a
            return r3
        L_0x000a:
            r4 = 1
            if (r2 != r4) goto L_0x001a
            r0 = 0
            r0 = r1[r0]
            boolean r9 = r0.equals(r9)
            if (r9 == 0) goto L_0x0019
            r9 = r1[r4]
            return r9
        L_0x0019:
            return r3
        L_0x001a:
            if (r0 != 0) goto L_0x001d
            return r3
        L_0x001d:
            boolean r2 = r0 instanceof byte[]
            if (r2 == 0) goto L_0x0048
            r2 = r0
            byte[] r2 = (byte[]) r2
            int r0 = r2.length
            int r5 = r0 + -1
            int r0 = r9.hashCode()
            int r0 = com.google.android.gms.internal.p000firebaseperf.zzh.zza(r0)
        L_0x002f:
            r0 = r0 & r5
            byte r6 = r2[r0]
            r7 = 255(0xff, float:3.57E-43)
            r6 = r6 & r7
            if (r6 != r7) goto L_0x0038
            return r3
        L_0x0038:
            r7 = r1[r6]
            boolean r7 = r7.equals(r9)
            if (r7 == 0) goto L_0x0045
            r9 = r6 ^ 1
            r9 = r1[r9]
            return r9
        L_0x0045:
            int r0 = r0 + 1
            goto L_0x002f
        L_0x0048:
            boolean r2 = r0 instanceof short[]
            if (r2 == 0) goto L_0x0074
            r2 = r0
            short[] r2 = (short[]) r2
            int r0 = r2.length
            int r5 = r0 + -1
            int r0 = r9.hashCode()
            int r0 = com.google.android.gms.internal.p000firebaseperf.zzh.zza(r0)
        L_0x005a:
            r0 = r0 & r5
            short r6 = r2[r0]
            r7 = 65535(0xffff, float:9.1834E-41)
            r6 = r6 & r7
            if (r6 != r7) goto L_0x0064
            return r3
        L_0x0064:
            r7 = r1[r6]
            boolean r7 = r7.equals(r9)
            if (r7 == 0) goto L_0x0071
            r9 = r6 ^ 1
            r9 = r1[r9]
            return r9
        L_0x0071:
            int r0 = r0 + 1
            goto L_0x005a
        L_0x0074:
            int[] r0 = (int[]) r0
            int r2 = r0.length
            int r2 = r2 - r4
            int r5 = r9.hashCode()
            int r5 = com.google.android.gms.internal.p000firebaseperf.zzh.zza(r5)
        L_0x0080:
            r5 = r5 & r2
            r6 = r0[r5]
            r7 = -1
            if (r6 != r7) goto L_0x0087
            return r3
        L_0x0087:
            r7 = r1[r6]
            boolean r7 = r7.equals(r9)
            if (r7 == 0) goto L_0x0094
            r9 = r6 ^ 1
            r9 = r1[r9]
            return r9
        L_0x0094:
            int r5 = r5 + 1
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzp.get(java.lang.Object):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public final zzn<Map.Entry<K, V>> zzh() {
        return new zzs(this, this.zzm, 0, this.size);
    }

    /* access modifiers changed from: package-private */
    public final zzn<K> zzi() {
        return new zzu(this, new zzt(this.zzm, 0, this.size));
    }

    /* access modifiers changed from: package-private */
    public final zzk<V> zzj() {
        return new zzt(this.zzm, 1, this.size);
    }
}
