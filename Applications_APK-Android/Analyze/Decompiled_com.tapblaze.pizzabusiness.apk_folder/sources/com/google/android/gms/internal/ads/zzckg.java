package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzckg implements zzded {
    private final zzczl zzfel;
    private final zzbdi zzfpv;
    private final zzckc zzfzj;
    private final zzbtw zzfzo;

    zzckg(zzckc zzckc, zzbdi zzbdi, zzczl zzczl, zzbtw zzbtw) {
        this.zzfzj = zzckc;
        this.zzfpv = zzbdi;
        this.zzfel = zzczl;
        this.zzfzo = zzbtw;
    }

    public final Object apply(Object obj) {
        zzbdi zzbdi = this.zzfpv;
        zzczl zzczl = this.zzfel;
        zzbtw zzbtw = this.zzfzo;
        if (zzczl.zzdmf) {
            zzbdi.zzaan();
        }
        zzbdi.zzzu();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzciq)).booleanValue()) {
            zzq.zzks();
            zzawh.zza(zzbdi);
        }
        return zzbtw.zzaem();
    }
}
