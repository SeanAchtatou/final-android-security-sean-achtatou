package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdbp {
    private int zzgpj = 0;
    private final long zzgpk = zzq.zzkx().currentTimeMillis();
    private final zzdbo zzgpl = new zzdbo();
    private long zzgpm = this.zzgpk;
    private int zzgpn = 0;
    private int zzgpo = 0;

    public final void zzapr() {
        this.zzgpm = zzq.zzkx().currentTimeMillis();
        this.zzgpn++;
    }

    public final void zzaps() {
        this.zzgpo++;
        this.zzgpl.zzgpi = true;
    }

    public final void zzapt() {
        this.zzgpj++;
        this.zzgpl.zzgpj++;
    }

    public final long getCreationTimeMillis() {
        return this.zzgpk;
    }

    public final long zzaoy() {
        return this.zzgpm;
    }

    public final int zzaoz() {
        return this.zzgpn;
    }

    public final zzdbo zzapu() {
        zzdbo zzdbo = (zzdbo) this.zzgpl.clone();
        zzdbo zzdbo2 = this.zzgpl;
        zzdbo2.zzgpi = false;
        zzdbo2.zzgpj = 0;
        return zzdbo;
    }

    public final String zzapk() {
        return "Created: " + this.zzgpk + " Last accessed: " + this.zzgpm + " Accesses: " + this.zzgpn + "\nEntries retrieved: Valid: " + this.zzgpo + " Stale: " + this.zzgpj;
    }
}
