package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzcb  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzcb extends zzfc<zzcb, zza> implements zzgn {
    private static volatile zzgv<zzcb> zzij;
    /* access modifiers changed from: private */
    public static final zzcb zzim;
    private int zzie;
    private long zzik;
    private int zzil;

    private zzcb() {
    }

    /* renamed from: com.google.android.gms.internal.firebase-perf.zzcb$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc.zzb<zzcb, zza> implements zzgn {
        private zza() {
            super(zzcb.zzim);
        }

        public final zza zzv(long j) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcb) this.zzqq).zzu(j);
            return this;
        }

        public final zza zze(int i) {
            if (this.zzqr) {
                zzhl();
                this.zzqr = false;
            }
            ((zzcb) this.zzqq).zzd(i);
            return this;
        }

        /* synthetic */ zza(zzcc zzcc) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public final void zzu(long j) {
        this.zzie |= 1;
        this.zzik = j;
    }

    /* access modifiers changed from: private */
    public final void zzd(int i) {
        this.zzie |= 2;
        this.zzil = i;
    }

    public static zza zzdf() {
        return (zza) zzim.zzhf();
    }

    /* access modifiers changed from: protected */
    public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
        switch (zzcc.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
            case 1:
                return new zzcb();
            case 2:
                return new zza(null);
            case 3:
                return zza(zzim, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0002\u0000\u0002\u0004\u0001", new Object[]{"zzie", "zzik", "zzil"});
            case 4:
                return zzim;
            case 5:
                zzgv<zzcb> zzgv = zzij;
                if (zzgv == null) {
                    synchronized (zzcb.class) {
                        zzgv = zzij;
                        if (zzgv == null) {
                            zzgv = new zzfc.zza<>(zzim);
                            zzij = zzgv;
                        }
                    }
                }
                return zzgv;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        zzcb zzcb = new zzcb();
        zzim = zzcb;
        zzfc.zza(zzcb.class, zzcb);
    }
}
