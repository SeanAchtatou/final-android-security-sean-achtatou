package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzae  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzae extends zzz {
    zzae() {
    }

    public final void zza(Throwable th, Throwable th2) {
        th.addSuppressed(th2);
    }
}
