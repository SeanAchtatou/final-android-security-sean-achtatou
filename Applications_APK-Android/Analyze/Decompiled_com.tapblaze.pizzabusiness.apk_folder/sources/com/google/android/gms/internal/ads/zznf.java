package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zznf {
    private zzni zzbed;

    public abstract zznh zza(zzhe[] zzheArr, zzmr zzmr) throws zzgl;

    public abstract void zzd(Object obj);

    public final void zza(zzni zzni) {
        this.zzbed = zzni;
    }

    /* access modifiers changed from: protected */
    public final void invalidate() {
        zzni zzni = this.zzbed;
        if (zzni != null) {
            zzni.zzei();
        }
    }
}
