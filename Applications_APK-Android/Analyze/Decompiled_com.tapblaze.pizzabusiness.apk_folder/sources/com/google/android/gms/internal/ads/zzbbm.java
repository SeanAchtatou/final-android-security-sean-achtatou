package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbbm implements Runnable {
    private final boolean zzdym;
    private final zzbbc zzebm;
    private final long zzebn;

    zzbbm(zzbbc zzbbc, boolean z, long j) {
        this.zzebm = zzbbc;
        this.zzdym = z;
        this.zzebn = j;
    }

    public final void run() {
        this.zzebm.zzc(this.zzdym, this.zzebn);
    }
}
