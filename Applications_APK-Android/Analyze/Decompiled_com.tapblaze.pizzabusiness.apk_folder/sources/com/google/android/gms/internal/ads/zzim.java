package com.google.android.gms.internal.ads;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzim implements zzhm {
    private int zzafo = -1;
    private float zzagc = 1.0f;
    private float zzagd = 1.0f;
    private ByteBuffer zzajh = zzaha;
    private int zzakk = -1;
    private ByteBuffer zzako = zzaha;
    private boolean zzakp;
    private zzin zzaky;
    private ShortBuffer zzakz = this.zzako.asShortBuffer();
    private long zzala;
    private long zzalb;

    public final int zzfa() {
        return 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzoq.zza(float, float, float):float
     arg types: [float, int, int]
     candidates:
      com.google.android.gms.internal.ads.zzoq.zza(long, long, long):long
      com.google.android.gms.internal.ads.zzoq.zza(long[], long, long):void
      com.google.android.gms.internal.ads.zzoq.zza(float, float, float):float */
    public final float zza(float f) {
        this.zzagc = zzoq.zza(f, 0.1f, 8.0f);
        return this.zzagc;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzoq.zza(float, float, float):float
     arg types: [float, int, int]
     candidates:
      com.google.android.gms.internal.ads.zzoq.zza(long, long, long):long
      com.google.android.gms.internal.ads.zzoq.zza(long[], long, long):void
      com.google.android.gms.internal.ads.zzoq.zza(float, float, float):float */
    public final float zzb(float f) {
        this.zzagd = zzoq.zza(f, 0.1f, 8.0f);
        return f;
    }

    public final long zzfv() {
        return this.zzala;
    }

    public final long zzfw() {
        return this.zzalb;
    }

    public final boolean zzb(int i, int i2, int i3) throws zzhp {
        if (i3 != 2) {
            throw new zzhp(i, i2, i3);
        } else if (this.zzakk == i && this.zzafo == i2) {
            return false;
        } else {
            this.zzakk = i;
            this.zzafo = i2;
            return true;
        }
    }

    public final boolean isActive() {
        return Math.abs(this.zzagc - 1.0f) >= 0.01f || Math.abs(this.zzagd - 1.0f) >= 0.01f;
    }

    public final int zzez() {
        return this.zzafo;
    }

    public final void zzi(ByteBuffer byteBuffer) {
        if (byteBuffer.hasRemaining()) {
            ShortBuffer asShortBuffer = byteBuffer.asShortBuffer();
            int remaining = byteBuffer.remaining();
            this.zzala += (long) remaining;
            this.zzaky.zza(asShortBuffer);
            byteBuffer.position(byteBuffer.position() + remaining);
        }
        int zzfx = (this.zzaky.zzfx() * this.zzafo) << 1;
        if (zzfx > 0) {
            if (this.zzako.capacity() < zzfx) {
                this.zzako = ByteBuffer.allocateDirect(zzfx).order(ByteOrder.nativeOrder());
                this.zzakz = this.zzako.asShortBuffer();
            } else {
                this.zzako.clear();
                this.zzakz.clear();
            }
            this.zzaky.zzb(this.zzakz);
            this.zzalb += (long) zzfx;
            this.zzako.limit(zzfx);
            this.zzajh = this.zzako;
        }
    }

    public final void zzfb() {
        this.zzaky.zzfb();
        this.zzakp = true;
    }

    public final ByteBuffer zzfc() {
        ByteBuffer byteBuffer = this.zzajh;
        this.zzajh = zzaha;
        return byteBuffer;
    }

    public final boolean zzeu() {
        if (!this.zzakp) {
            return false;
        }
        zzin zzin = this.zzaky;
        return zzin == null || zzin.zzfx() == 0;
    }

    public final void flush() {
        this.zzaky = new zzin(this.zzakk, this.zzafo);
        this.zzaky.setSpeed(this.zzagc);
        this.zzaky.zzc(this.zzagd);
        this.zzajh = zzaha;
        this.zzala = 0;
        this.zzalb = 0;
        this.zzakp = false;
    }

    public final void reset() {
        this.zzaky = null;
        this.zzako = zzaha;
        this.zzakz = this.zzako.asShortBuffer();
        this.zzajh = zzaha;
        this.zzafo = -1;
        this.zzakk = -1;
        this.zzala = 0;
        this.zzalb = 0;
        this.zzakp = false;
    }
}
