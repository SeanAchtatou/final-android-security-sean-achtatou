package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class zzgs<T> implements zzhf<T> {
    private final zzgm zza;
    private final zzhx<?, ?> zzb;
    private final boolean zzc;
    private final zzet<?> zzd;

    private zzgs(zzhx<?, ?> zzhx, zzet<?> zzet, zzgm zzgm) {
        this.zzb = zzhx;
        this.zzc = zzet.zza(zzgm);
        this.zzd = zzet;
        this.zza = zzgm;
    }

    static <T> zzgs<T> zza(zzhx<?, ?> zzhx, zzet<?> zzet, zzgm zzgm) {
        return new zzgs<>(zzhx, zzet, zzgm);
    }

    public final T zza() {
        return this.zza.zzbr().zzu();
    }

    public final boolean zza(T t, T t2) {
        if (!this.zzb.zzb(t).equals(this.zzb.zzb(t2))) {
            return false;
        }
        if (this.zzc) {
            return this.zzd.zza((Object) t).equals(this.zzd.zza((Object) t2));
        }
        return true;
    }

    public final int zza(T t) {
        int hashCode = this.zzb.zzb(t).hashCode();
        return this.zzc ? (hashCode * 53) + this.zzd.zza((Object) t).hashCode() : hashCode;
    }

    public final void zzb(T t, T t2) {
        zzhh.zza(this.zzb, t, t2);
        if (this.zzc) {
            zzhh.zza(this.zzd, t, t2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zziq.zza(int, java.lang.Object):void
     arg types: [int, com.google.android.gms.internal.measurement.zzdw]
     candidates:
      com.google.android.gms.internal.measurement.zziq.zza(int, double):void
      com.google.android.gms.internal.measurement.zziq.zza(int, float):void
      com.google.android.gms.internal.measurement.zziq.zza(int, int):void
      com.google.android.gms.internal.measurement.zziq.zza(int, long):void
      com.google.android.gms.internal.measurement.zziq.zza(int, com.google.android.gms.internal.measurement.zzdw):void
      com.google.android.gms.internal.measurement.zziq.zza(int, java.lang.String):void
      com.google.android.gms.internal.measurement.zziq.zza(int, java.util.List<java.lang.String>):void
      com.google.android.gms.internal.measurement.zziq.zza(int, boolean):void
      com.google.android.gms.internal.measurement.zziq.zza(int, java.lang.Object):void */
    public final void zza(T t, zziq zziq) throws IOException {
        Iterator<Map.Entry<?, Object>> zzd2 = this.zzd.zza((Object) t).zzd();
        while (zzd2.hasNext()) {
            Map.Entry next = zzd2.next();
            zzew zzew = (zzew) next.getKey();
            if (zzew.zzc() != zzir.MESSAGE || zzew.zzd() || zzew.zze()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof zzft) {
                zziq.zza(zzew.zza(), (Object) ((zzft) next).zza().zzc());
            } else {
                zziq.zza(zzew.zza(), next.getValue());
            }
        }
        zzhx<?, ?> zzhx = this.zzb;
        zzhx.zzb(zzhx.zzb(t), zziq);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: com.google.android.gms.internal.measurement.zzfe$zzd} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zza(T r10, byte[] r11, int r12, int r13, com.google.android.gms.internal.measurement.zzdr r14) throws java.io.IOException {
        /*
            r9 = this;
            r0 = r10
            com.google.android.gms.internal.measurement.zzfe r0 = (com.google.android.gms.internal.measurement.zzfe) r0
            com.google.android.gms.internal.measurement.zzhw r1 = r0.zzb
            com.google.android.gms.internal.measurement.zzhw r2 = com.google.android.gms.internal.measurement.zzhw.zza()
            if (r1 != r2) goto L_0x0011
            com.google.android.gms.internal.measurement.zzhw r1 = com.google.android.gms.internal.measurement.zzhw.zzb()
            r0.zzb = r1
        L_0x0011:
            com.google.android.gms.internal.measurement.zzfe$zzb r10 = (com.google.android.gms.internal.measurement.zzfe.zzb) r10
            r10.zza()
            r10 = 0
            r0 = r10
        L_0x0018:
            if (r12 >= r13) goto L_0x00a4
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r11, r12, r14)
            int r2 = r14.zza
            r12 = 11
            r3 = 2
            if (r2 == r12) goto L_0x0051
            r12 = r2 & 7
            if (r12 != r3) goto L_0x004c
            com.google.android.gms.internal.measurement.zzet<?> r12 = r9.zzd
            com.google.android.gms.internal.measurement.zzer r0 = r14.zzd
            com.google.android.gms.internal.measurement.zzgm r3 = r9.zza
            int r5 = r2 >>> 3
            java.lang.Object r12 = r12.zza(r0, r3, r5)
            r0 = r12
            com.google.android.gms.internal.measurement.zzfe$zzd r0 = (com.google.android.gms.internal.measurement.zzfe.zzd) r0
            if (r0 != 0) goto L_0x0043
            r3 = r11
            r5 = r13
            r6 = r1
            r7 = r14
            int r12 = com.google.android.gms.internal.measurement.zzds.zza(r2, r3, r4, r5, r6, r7)
            goto L_0x0018
        L_0x0043:
            com.google.android.gms.internal.measurement.zzhb.zza()
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L_0x004c:
            int r12 = com.google.android.gms.internal.measurement.zzds.zza(r2, r11, r4, r13, r14)
            goto L_0x0018
        L_0x0051:
            r12 = 0
            r2 = r10
        L_0x0053:
            if (r4 >= r13) goto L_0x0099
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r11, r4, r14)
            int r5 = r14.zza
            int r6 = r5 >>> 3
            r7 = r5 & 7
            if (r6 == r3) goto L_0x007b
            r8 = 3
            if (r6 == r8) goto L_0x0065
            goto L_0x0090
        L_0x0065:
            if (r0 != 0) goto L_0x0072
            if (r7 != r3) goto L_0x0090
            int r4 = com.google.android.gms.internal.measurement.zzds.zze(r11, r4, r14)
            java.lang.Object r2 = r14.zzc
            com.google.android.gms.internal.measurement.zzdw r2 = (com.google.android.gms.internal.measurement.zzdw) r2
            goto L_0x0053
        L_0x0072:
            com.google.android.gms.internal.measurement.zzhb.zza()
            java.lang.NoSuchMethodError r10 = new java.lang.NoSuchMethodError
            r10.<init>()
            throw r10
        L_0x007b:
            if (r7 != 0) goto L_0x0090
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r11, r4, r14)
            int r12 = r14.zza
            com.google.android.gms.internal.measurement.zzet<?> r0 = r9.zzd
            com.google.android.gms.internal.measurement.zzer r5 = r14.zzd
            com.google.android.gms.internal.measurement.zzgm r6 = r9.zza
            java.lang.Object r0 = r0.zza(r5, r6, r12)
            com.google.android.gms.internal.measurement.zzfe$zzd r0 = (com.google.android.gms.internal.measurement.zzfe.zzd) r0
            goto L_0x0053
        L_0x0090:
            r6 = 12
            if (r5 == r6) goto L_0x0099
            int r4 = com.google.android.gms.internal.measurement.zzds.zza(r5, r11, r4, r13, r14)
            goto L_0x0053
        L_0x0099:
            if (r2 == 0) goto L_0x00a1
            int r12 = r12 << 3
            r12 = r12 | r3
            r1.zza(r12, r2)
        L_0x00a1:
            r12 = r4
            goto L_0x0018
        L_0x00a4:
            if (r12 != r13) goto L_0x00a7
            return
        L_0x00a7:
            com.google.android.gms.internal.measurement.zzfm r10 = com.google.android.gms.internal.measurement.zzfm.zzg()
            goto L_0x00ad
        L_0x00ac:
            throw r10
        L_0x00ad:
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzgs.zza(java.lang.Object, byte[], int, int, com.google.android.gms.internal.measurement.zzdr):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzhc):boolean
     arg types: [?, com.google.android.gms.internal.measurement.zzhc]
     candidates:
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, com.google.android.gms.internal.measurement.zzhc):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, int, com.google.android.gms.internal.measurement.zzdw):void
     arg types: [?, int, com.google.android.gms.internal.measurement.zzdw]
     candidates:
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, int, int):void
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, int, long):void
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, int, java.lang.Object):void
      com.google.android.gms.internal.measurement.zzhx.zza(java.lang.Object, int, com.google.android.gms.internal.measurement.zzdw):void */
    public final void zza(Object obj, zzhc zzhc, zzer zzer) throws IOException {
        boolean z;
        zzhx<?, ?> zzhx = this.zzb;
        zzet<?> zzet = this.zzd;
        Object zzc2 = zzhx.zzc(obj);
        zzeu<?> zzb2 = zzet.zzb(obj);
        do {
            try {
                if (zzhc.zza() == Integer.MAX_VALUE) {
                    zzhx.zzb(obj, zzc2);
                    return;
                }
                int zzb3 = zzhc.zzb();
                if (zzb3 == 11) {
                    int i = 0;
                    Object obj2 = null;
                    zzdw zzdw = null;
                    while (zzhc.zza() != Integer.MAX_VALUE) {
                        int zzb4 = zzhc.zzb();
                        if (zzb4 == 16) {
                            i = zzhc.zzo();
                            obj2 = zzet.zza(zzer, this.zza, i);
                        } else if (zzb4 == 26) {
                            if (obj2 != null) {
                                zzet.zza(zzhc, obj2, zzer, zzb2);
                            } else {
                                zzdw = zzhc.zzn();
                            }
                        } else if (!zzhc.zzc()) {
                            break;
                        }
                    }
                    if (zzhc.zzb() != 12) {
                        throw zzfm.zze();
                    } else if (zzdw != null) {
                        if (obj2 != null) {
                            zzet.zza(zzdw, obj2, zzer, zzb2);
                        } else {
                            zzhx.zza((Object) zzc2, i, zzdw);
                        }
                    }
                } else if ((zzb3 & 7) == 2) {
                    Object zza2 = zzet.zza(zzer, this.zza, zzb3 >>> 3);
                    if (zza2 != null) {
                        zzet.zza(zzhc, zza2, zzer, zzb2);
                    } else {
                        z = zzhx.zza((Object) zzc2, zzhc);
                        continue;
                    }
                } else {
                    z = zzhc.zzc();
                    continue;
                }
                z = true;
                continue;
            } finally {
                zzhx.zzb(obj, zzc2);
            }
        } while (z);
    }

    public final void zzc(T t) {
        this.zzb.zzd(t);
        this.zzd.zzc(t);
    }

    public final boolean zzd(T t) {
        return this.zzd.zza((Object) t).zzf();
    }

    public final int zzb(T t) {
        zzhx<?, ?> zzhx = this.zzb;
        int zze = zzhx.zze(zzhx.zzb(t)) + 0;
        return this.zzc ? zze + this.zzd.zza((Object) t).zzg() : zze;
    }
}
