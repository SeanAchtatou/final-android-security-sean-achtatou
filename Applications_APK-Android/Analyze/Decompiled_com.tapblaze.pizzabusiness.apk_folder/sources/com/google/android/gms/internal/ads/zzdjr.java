package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdjr extends zzdik<zzdhx, zzdno> {
    zzdjr(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        String zzawk = ((zzdno) obj).zzawh().zzawk();
        return zzdil.zzgu(zzawk).zzgw(zzawk);
    }
}
