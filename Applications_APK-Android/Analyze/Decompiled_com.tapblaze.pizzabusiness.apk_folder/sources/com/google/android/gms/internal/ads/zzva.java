package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.widget.FrameLayout;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzva extends zzvb<zzacm> {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ zzup zzcdi;
    private final /* synthetic */ FrameLayout zzcdn;
    private final /* synthetic */ FrameLayout zzcdo;

    zzva(zzup zzup, FrameLayout frameLayout, FrameLayout frameLayout2, Context context) {
        this.zzcdi = zzup;
        this.zzcdn = frameLayout;
        this.zzcdo = frameLayout2;
        this.val$context = context;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object zzop() {
        zzup.zza(this.val$context, "native_ad_view_delegate");
        return new zzyh();
    }

    public final /* synthetic */ Object zzoq() throws RemoteException {
        return this.zzcdi.zzcda.zzb(this.val$context, this.zzcdn, this.zzcdo);
    }

    public final /* synthetic */ Object zza(zzwd zzwd) throws RemoteException {
        return zzwd.zza(ObjectWrapper.wrap(this.zzcdn), ObjectWrapper.wrap(this.zzcdo));
    }
}
