package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdjl extends zzdik<zzdhx, zzdlv> {
    zzdjl(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        return new zzdoh(((zzdlv) obj).zzass().toByteArray());
    }
}
