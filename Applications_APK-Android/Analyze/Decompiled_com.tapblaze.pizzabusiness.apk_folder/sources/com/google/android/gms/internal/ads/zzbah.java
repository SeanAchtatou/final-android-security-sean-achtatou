package com.google.android.gms.internal.ads;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbah {
    void onPaused();

    void onWindowVisibilityChanged(int i);

    void zzer();

    void zzk(int i, int i2);

    void zzm(String str, String str2);

    void zzxt();

    void zzxu();

    void zzxv();

    void zzxw();

    void zzxx();
}
