package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdgh<V> extends zzdge<V, List<V>> {
    zzdgh(zzdet<? extends zzdhe<? extends V>> zzdet, boolean z) {
        super(zzdet, true);
        zzarn();
    }

    public final /* synthetic */ Object zzh(List list) {
        ArrayList zzdz = zzdfc.zzdz(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            zzdej zzdej = (zzdej) it.next();
            zzdz.add(zzdej != null ? zzdej.zzaqt() : null);
        }
        return Collections.unmodifiableList(zzdz);
    }
}
