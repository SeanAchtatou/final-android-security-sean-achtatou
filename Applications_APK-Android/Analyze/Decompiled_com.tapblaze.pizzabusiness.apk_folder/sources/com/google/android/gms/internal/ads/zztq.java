package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zztq extends zzdvq<zztq> {
    public Integer zzbzt = null;
    private zzte zzbzu = null;
    private zzsy.zzc zzbzv = null;
    public zztp zzbzw = null;
    private zzsy.zzb[] zzbzx = new zzsy.zzb[0];
    private zzsy.zzd zzbzy = null;
    private zzsy.zzk zzbzz = null;
    private zzsy.zzi zzcaa = null;
    private zzsy.zzf zzcab = null;
    private zzsy.zzg zzcac = null;
    private zztw[] zzcad = zztw.zzoj();

    public zztq() {
        this.zzhtm = null;
        this.zzhhn = -1;
    }

    public final void zza(zzdvo zzdvo) throws IOException {
        Integer num = this.zzbzt;
        if (num != null) {
            zzdvo.zzab(7, num.intValue());
        }
        zztp zztp = this.zzbzw;
        if (zztp != null) {
            zzdvo.zza(10, zztp);
        }
        zzsy.zzb[] zzbArr = this.zzbzx;
        int i = 0;
        if (zzbArr != null && zzbArr.length > 0) {
            int i2 = 0;
            while (true) {
                zzsy.zzb[] zzbArr2 = this.zzbzx;
                if (i2 >= zzbArr2.length) {
                    break;
                }
                zzsy.zzb zzb = zzbArr2[i2];
                if (zzb != null) {
                    zzdvo.zze(11, zzb);
                }
                i2++;
            }
        }
        zztw[] zztwArr = this.zzcad;
        if (zztwArr != null && zztwArr.length > 0) {
            while (true) {
                zztw[] zztwArr2 = this.zzcad;
                if (i >= zztwArr2.length) {
                    break;
                }
                zztw zztw = zztwArr2[i];
                if (zztw != null) {
                    zzdvo.zza(17, zztw);
                }
                i++;
            }
        }
        super.zza(zzdvo);
    }

    /* access modifiers changed from: protected */
    public final int zzoi() {
        int zzoi = super.zzoi();
        Integer num = this.zzbzt;
        if (num != null) {
            zzoi += zzdvo.zzaf(7, num.intValue());
        }
        zztp zztp = this.zzbzw;
        if (zztp != null) {
            zzoi += zzdvo.zzb(10, zztp);
        }
        zzsy.zzb[] zzbArr = this.zzbzx;
        int i = 0;
        if (zzbArr != null && zzbArr.length > 0) {
            int i2 = zzoi;
            int i3 = 0;
            while (true) {
                zzsy.zzb[] zzbArr2 = this.zzbzx;
                if (i3 >= zzbArr2.length) {
                    break;
                }
                zzsy.zzb zzb = zzbArr2[i3];
                if (zzb != null) {
                    i2 += zzdrb.zzc(11, zzb);
                }
                i3++;
            }
            zzoi = i2;
        }
        zztw[] zztwArr = this.zzcad;
        if (zztwArr != null && zztwArr.length > 0) {
            while (true) {
                zztw[] zztwArr2 = this.zzcad;
                if (i >= zztwArr2.length) {
                    break;
                }
                zztw zztw = zztwArr2[i];
                if (zztw != null) {
                    zzoi += zzdvo.zzb(17, zztw);
                }
                i++;
            }
        }
        return zzoi;
    }
}
