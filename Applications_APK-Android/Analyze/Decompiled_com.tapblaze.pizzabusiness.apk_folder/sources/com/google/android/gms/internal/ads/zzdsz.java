package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdsz {
    private static final zzdsx zzhou = zzbbf();
    private static final zzdsx zzhov = new zzdta();

    static zzdsx zzbbd() {
        return zzhou;
    }

    static zzdsx zzbbe() {
        return zzhov;
    }

    private static zzdsx zzbbf() {
        try {
            return (zzdsx) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
