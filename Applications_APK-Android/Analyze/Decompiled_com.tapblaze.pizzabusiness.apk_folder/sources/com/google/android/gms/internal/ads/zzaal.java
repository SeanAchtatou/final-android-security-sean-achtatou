package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.OnCustomRenderedAdLoadedListener;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaal extends zzaaj {
    private final OnCustomRenderedAdLoadedListener zzcen;

    public zzaal(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.zzcen = onCustomRenderedAdLoadedListener;
    }

    public final void zza(zzaaf zzaaf) {
        this.zzcen.onCustomRenderedAdLoaded(new zzaag(zzaaf));
    }
}
