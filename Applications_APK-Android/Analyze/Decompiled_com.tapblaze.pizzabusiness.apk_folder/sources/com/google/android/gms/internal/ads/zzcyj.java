package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcyj implements zzcxo {
    private final int zzdvv;

    zzcyj(int i) {
        this.zzdvv = i;
    }

    public final void zzt(Object obj) {
        ((zzaso) obj).onRewardedAdFailedToLoad(this.zzdvv);
    }
}
