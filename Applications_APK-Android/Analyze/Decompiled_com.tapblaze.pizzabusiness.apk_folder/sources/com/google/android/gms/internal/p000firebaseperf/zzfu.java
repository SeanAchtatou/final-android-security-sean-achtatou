package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfu  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class zzfu {
    private static final zzet zzmr = zzet.zzgv();
    private zzeb zzrz;
    private volatile zzgl zzsa;
    private volatile zzeb zzsb;

    public int hashCode() {
        return 1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzfu)) {
            return false;
        }
        zzfu zzfu = (zzfu) obj;
        zzgl zzgl = this.zzsa;
        zzgl zzgl2 = zzfu.zzsa;
        if (zzgl == null && zzgl2 == null) {
            return zzgb().equals(zzfu.zzgb());
        }
        if (zzgl != null && zzgl2 != null) {
            return zzgl.equals(zzgl2);
        }
        if (zzgl != null) {
            return zzgl.equals(zzfu.zzg(zzgl.zzhj()));
        }
        return zzg(zzgl2.zzhj()).equals(zzgl2);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.p000firebaseperf.zzgl zzg(com.google.android.gms.internal.p000firebaseperf.zzgl r2) {
        /*
            r1 = this;
            com.google.android.gms.internal.firebase-perf.zzgl r0 = r1.zzsa
            if (r0 != 0) goto L_0x001d
            monitor-enter(r1)
            com.google.android.gms.internal.firebase-perf.zzgl r0 = r1.zzsa     // Catch:{ all -> 0x001a }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x000b:
            r1.zzsa = r2     // Catch:{ zzfl -> 0x0012 }
            com.google.android.gms.internal.firebase-perf.zzeb r0 = com.google.android.gms.internal.p000firebaseperf.zzeb.zzmv     // Catch:{ zzfl -> 0x0012 }
            r1.zzsb = r0     // Catch:{ zzfl -> 0x0012 }
            goto L_0x0018
        L_0x0012:
            r1.zzsa = r2     // Catch:{ all -> 0x001a }
            com.google.android.gms.internal.firebase-perf.zzeb r2 = com.google.android.gms.internal.p000firebaseperf.zzeb.zzmv     // Catch:{ all -> 0x001a }
            r1.zzsb = r2     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x001a:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            throw r2
        L_0x001d:
            com.google.android.gms.internal.firebase-perf.zzgl r2 = r1.zzsa
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzfu.zzg(com.google.android.gms.internal.firebase-perf.zzgl):com.google.android.gms.internal.firebase-perf.zzgl");
    }

    public final zzgl zzh(zzgl zzgl) {
        zzgl zzgl2 = this.zzsa;
        this.zzrz = null;
        this.zzsb = null;
        this.zzsa = zzgl;
        return zzgl2;
    }

    public final int getSerializedSize() {
        if (this.zzsb != null) {
            return this.zzsb.size();
        }
        if (this.zzsa != null) {
            return this.zzsa.getSerializedSize();
        }
        return 0;
    }

    public final zzeb zzgb() {
        if (this.zzsb != null) {
            return this.zzsb;
        }
        synchronized (this) {
            if (this.zzsb != null) {
                zzeb zzeb = this.zzsb;
                return zzeb;
            }
            if (this.zzsa == null) {
                this.zzsb = zzeb.zzmv;
            } else {
                this.zzsb = this.zzsa.zzgb();
            }
            zzeb zzeb2 = this.zzsb;
            return zzeb2;
        }
    }
}
