package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzfc;

/* renamed from: com.google.android.gms.internal.firebase-perf.zziq  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zziq {

    /* renamed from: com.google.android.gms.internal.firebase-perf.zziq$zza */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static final class zza extends zzfc<zza, C0037zza> implements zzgn {
        private static volatile zzgv<zza> zzij;
        /* access modifiers changed from: private */
        public static final zza zzxa;
        private int zzie;
        private int zzwy = -1;
        private int zzwz;

        /* renamed from: com.google.android.gms.internal.firebase-perf.zziq$zza$zzb */
        /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
        public enum zzb implements zzff {
            UNKNOWN_MOBILE_SUBTYPE(0),
            GPRS(1),
            EDGE(2),
            UMTS(3),
            CDMA(4),
            EVDO_0(5),
            EVDO_A(6),
            RTT(7),
            HSDPA(8),
            HSUPA(9),
            HSPA(10),
            IDEN(11),
            EVDO_B(12),
            LTE(13),
            EHRPD(14),
            HSPAP(15),
            GSM(16),
            TD_SCDMA(17),
            IWLAN(18),
            LTE_CA(19),
            COMBINED(100);
            
            private static final zzfi<zzb> zzja = new zzir();
            private final int value;

            public final int getNumber() {
                return this.value;
            }

            public static zzfh zzdp() {
                return zzis.zzjf;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + getNumber() + " name=" + name() + '>';
            }

            private zzb(int i) {
                this.value = i;
            }
        }

        /* renamed from: com.google.android.gms.internal.firebase-perf.zziq$zza$zzc */
        /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
        public enum zzc implements zzff {
            NONE(-1),
            MOBILE(0),
            WIFI(1),
            MOBILE_MMS(2),
            MOBILE_SUPL(3),
            MOBILE_DUN(4),
            MOBILE_HIPRI(5),
            WIMAX(6),
            BLUETOOTH(7),
            DUMMY(8),
            ETHERNET(9),
            MOBILE_FOTA(10),
            MOBILE_IMS(11),
            MOBILE_CBS(12),
            WIFI_P2P(13),
            MOBILE_IA(14),
            MOBILE_EMERGENCY(15),
            PROXY(16),
            VPN(17);
            
            private static final zzfi<zzc> zzja = new zziu();
            private final int value;

            public final int getNumber() {
                return this.value;
            }

            public static zzfh zzdp() {
                return zzit.zzjf;
            }

            public final String toString() {
                return "<" + getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + getNumber() + " name=" + name() + '>';
            }

            private zzc(int i) {
                this.value = i;
            }
        }

        private zza() {
        }

        /* renamed from: com.google.android.gms.internal.firebase-perf.zziq$zza$zza  reason: collision with other inner class name */
        /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
        public static final class C0037zza extends zzfc.zzb<zza, C0037zza> implements zzgn {
            private C0037zza() {
                super(zza.zzxa);
            }

            /* synthetic */ C0037zza(zzip zzip) {
                this();
            }
        }

        /* access modifiers changed from: protected */
        public final Object dynamicMethod(zzfc.zze zze, Object obj, Object obj2) {
            switch (zzip.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[zze.ordinal()]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0037zza(null);
                case 3:
                    return zza(zzxa, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\f\u0000\u0002\f\u0001", new Object[]{"zzie", "zzwy", zzc.zzdp(), "zzwz", zzb.zzdp()});
                case 4:
                    return zzxa;
                case 5:
                    zzgv<zza> zzgv = zzij;
                    if (zzgv == null) {
                        synchronized (zza.class) {
                            zzgv = zzij;
                            if (zzgv == null) {
                                zzgv = new zzfc.zza<>(zzxa);
                                zzij = zzgv;
                            }
                        }
                    }
                    return zzgv;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            zza zza = new zza();
            zzxa = zza;
            zzfc.zza(zza.class, zza);
        }
    }
}
