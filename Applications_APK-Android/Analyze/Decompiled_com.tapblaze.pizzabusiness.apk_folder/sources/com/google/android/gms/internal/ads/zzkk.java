package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzkk {
    /* access modifiers changed from: private */
    public final int id;
    /* access modifiers changed from: private */
    public final int zzafj;
    /* access modifiers changed from: private */
    public final long zzcw;

    public zzkk(int i, long j, int i2) {
        this.id = i;
        this.zzcw = j;
        this.zzafj = i2;
    }
}
