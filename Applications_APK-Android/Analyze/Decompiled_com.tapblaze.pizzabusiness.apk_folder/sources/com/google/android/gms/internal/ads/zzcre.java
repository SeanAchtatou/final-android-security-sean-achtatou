package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcre implements Callable {
    private final zzcrf zzgfk;

    zzcre(zzcrf zzcrf) {
        this.zzgfk = zzcrf;
    }

    public final Object call() {
        return this.zzgfk.zzanf();
    }
}
