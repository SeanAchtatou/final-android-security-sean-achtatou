package com.google.android.gms.internal.ads;

import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzmb {
    zzlz zza(int i, zznj zznj);

    void zza(zzgk zzgk, boolean z, zzme zzme);

    void zzb(zzlz zzlz);

    void zzhr() throws IOException;

    void zzhs();
}
