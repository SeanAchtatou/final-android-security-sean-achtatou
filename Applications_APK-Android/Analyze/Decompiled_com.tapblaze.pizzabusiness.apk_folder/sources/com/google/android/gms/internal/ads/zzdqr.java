package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
abstract class zzdqr extends zzdqk {
    zzdqr() {
    }

    /* access modifiers changed from: package-private */
    public abstract boolean zza(zzdqk zzdqk, int i, int i2);

    /* access modifiers changed from: protected */
    public final int zzaxw() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public final boolean zzaxx() {
        return true;
    }
}
