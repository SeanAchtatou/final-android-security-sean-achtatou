package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdxf<T> implements zzdxa<T>, zzdxg<T> {
    private static final zzdxf<Object> zziac = new zzdxf<>(null);
    private final T zzduw;

    public static <T> zzdxg<T> zzbe(T t) {
        return new zzdxf(zzdxm.zza(t, "instance cannot be null"));
    }

    public static <T> zzdxg<T> zzbf(T t) {
        if (t == null) {
            return zziac;
        }
        return new zzdxf(t);
    }

    private zzdxf(T t) {
        this.zzduw = t;
    }

    public final T get() {
        return this.zzduw;
    }
}
