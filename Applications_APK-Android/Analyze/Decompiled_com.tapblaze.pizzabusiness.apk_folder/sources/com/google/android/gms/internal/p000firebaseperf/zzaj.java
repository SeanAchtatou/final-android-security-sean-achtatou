package com.google.android.gms.internal.p000firebaseperf;

import com.google.firebase.perf.internal.zzd;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzaj  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzaj extends zzaz<String> {
    private static zzaj zzam;
    private static final zzo<Long, String> zzan = zzo.zza(461L, "FIREPERF_AUTOPUSH", 462L, zzd.zzcw, 675L, "FIREPERF_INTERNAL_LOW", 676L, "FIREPERF_INTERNAL_HIGH");

    private zzaj() {
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.LogSourceName";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_log_source";
    }

    public static synchronized zzaj zzak() {
        zzaj zzaj;
        synchronized (zzaj.class) {
            if (zzam == null) {
                zzam = new zzaj();
            }
            zzaj = zzam;
        }
        return zzaj;
    }

    protected static String zzf(long j) {
        return zzan.get(Long.valueOf(j));
    }

    protected static boolean zzg(long j) {
        return zzan.containsKey(Long.valueOf(j));
    }

    protected static String zzal() {
        return zzd.zzcw;
    }
}
