package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzih  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public enum zzih {
    DOUBLE(zzio.DOUBLE, 1),
    FLOAT(zzio.FLOAT, 5),
    INT64(zzio.LONG, 0),
    UINT64(zzio.LONG, 0),
    INT32(zzio.INT, 0),
    FIXED64(zzio.LONG, 1),
    FIXED32(zzio.INT, 5),
    BOOL(zzio.BOOLEAN, 0),
    STRING(zzio.STRING, 2),
    GROUP(zzio.MESSAGE, 3),
    MESSAGE(zzio.MESSAGE, 2),
    BYTES(zzio.BYTE_STRING, 2),
    UINT32(zzio.INT, 0),
    ENUM(zzio.ENUM, 0),
    SFIXED32(zzio.INT, 5),
    SFIXED64(zzio.LONG, 1),
    SINT32(zzio.INT, 0),
    SINT64(zzio.LONG, 0);
    
    private final zzio zzwl;
    private final int zzwm;

    private zzih(zzio zzio, int i) {
        this.zzwl = zzio;
        this.zzwm = i;
    }

    public final zzio zzjm() {
        return this.zzwl;
    }

    public final int zzjn() {
        return this.zzwm;
    }
}
