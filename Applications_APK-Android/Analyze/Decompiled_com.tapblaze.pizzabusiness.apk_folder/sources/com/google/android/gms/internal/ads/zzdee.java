package com.google.android.gms.internal.ads;

import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzdee {
    private final String className;
    private final zzdeh zzgty;
    private zzdeh zzgtz;
    private boolean zzgua;

    private zzdee(String str) {
        this.zzgty = new zzdeh();
        this.zzgtz = this.zzgty;
        this.zzgua = false;
        this.className = (String) zzdei.checkNotNull(str);
    }

    public final zzdee zzaa(@NullableDecl Object obj) {
        zzdeh zzdeh = new zzdeh();
        this.zzgtz.zzgub = zzdeh;
        this.zzgtz = zzdeh;
        zzdeh.value = obj;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public final String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.className);
        sb.append('{');
        zzdeh zzdeh = this.zzgty.zzgub;
        String str = "";
        while (zzdeh != null) {
            Object obj = zzdeh.value;
            sb.append(str);
            if (obj == null || !obj.getClass().isArray()) {
                sb.append(obj);
            } else {
                String deepToString = Arrays.deepToString(new Object[]{obj});
                sb.append((CharSequence) deepToString, 1, deepToString.length() - 1);
            }
            zzdeh = zzdeh.zzgub;
            str = ", ";
        }
        sb.append('}');
        return sb.toString();
    }
}
