package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdan implements zzdxg<zzdak> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzavp> zzelx;

    private zzdan(zzdxp<Context> zzdxp, zzdxp<zzavp> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzelx = zzdxp2;
    }

    public static zzdan zzaw(zzdxp<Context> zzdxp, zzdxp<zzavp> zzdxp2) {
        return new zzdan(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzdak(this.zzejv.get(), this.zzelx.get());
    }
}
