package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbse implements zzdxg<zzbrm> {
    private final zzbrm zzfim;

    private zzbse(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbse zzv(zzbrm zzbrm) {
        return new zzbse(zzbrm);
    }

    public final /* synthetic */ Object get() {
        zzbrm zzbrm = this.zzfim;
        if (zzbrm != null) {
            return (zzbrm) zzdxm.zza(zzbrm, "Cannot return null from a non-@Nullable @Provides method");
        }
        throw null;
    }
}
