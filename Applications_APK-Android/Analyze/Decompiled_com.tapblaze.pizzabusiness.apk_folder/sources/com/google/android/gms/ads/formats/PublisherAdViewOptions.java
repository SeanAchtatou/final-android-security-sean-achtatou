package com.google.android.gms.ads.formats;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.ads.zzadz;
import com.google.android.gms.internal.ads.zzaea;
import com.google.android.gms.internal.ads.zzul;
import com.google.android.gms.internal.ads.zzwb;
import com.google.android.gms.internal.ads.zzwc;
import com.google.android.gms.internal.ads.zzyu;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class PublisherAdViewOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<PublisherAdViewOptions> CREATOR = new zzc();
    private final boolean zzbkh;
    private final zzwc zzbki;
    private AppEventListener zzbkj;
    private final IBinder zzbkk;

    private PublisherAdViewOptions(Builder builder) {
        this.zzbkh = builder.zzbkh;
        this.zzbkj = builder.zzbkj;
        AppEventListener appEventListener = this.zzbkj;
        zzyu zzyu = null;
        this.zzbki = appEventListener != null ? new zzul(appEventListener) : null;
        this.zzbkk = builder.zzbkl != null ? new zzyu(builder.zzbkl) : zzyu;
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static final class Builder {
        /* access modifiers changed from: private */
        public boolean zzbkh = false;
        /* access modifiers changed from: private */
        public AppEventListener zzbkj;
        /* access modifiers changed from: private */
        public ShouldDelayBannerRenderingListener zzbkl;

        public final Builder setManualImpressionsEnabled(boolean z) {
            this.zzbkh = z;
            return this;
        }

        public final Builder setAppEventListener(AppEventListener appEventListener) {
            this.zzbkj = appEventListener;
            return this;
        }

        public final Builder setShouldDelayBannerRenderingListener(ShouldDelayBannerRenderingListener shouldDelayBannerRenderingListener) {
            this.zzbkl = shouldDelayBannerRenderingListener;
            return this;
        }

        public final PublisherAdViewOptions build() {
            return new PublisherAdViewOptions(this);
        }
    }

    PublisherAdViewOptions(boolean z, IBinder iBinder, IBinder iBinder2) {
        this.zzbkh = z;
        this.zzbki = iBinder != null ? zzwb.zze(iBinder) : null;
        this.zzbkk = iBinder2;
    }

    public final AppEventListener getAppEventListener() {
        return this.zzbkj;
    }

    public final boolean getManualImpressionsEnabled() {
        return this.zzbkh;
    }

    public final zzwc zzjm() {
        return this.zzbki;
    }

    public final zzaea zzjn() {
        return zzadz.zzw(this.zzbkk);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, getManualImpressionsEnabled());
        zzwc zzwc = this.zzbki;
        SafeParcelWriter.writeIBinder(parcel, 2, zzwc == null ? null : zzwc.asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 3, this.zzbkk, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
