package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzxx {
    private String zzcfh;

    public zzxx(String str) {
        this.zzcfh = str;
    }

    public final String getQuery() {
        return this.zzcfh;
    }
}
