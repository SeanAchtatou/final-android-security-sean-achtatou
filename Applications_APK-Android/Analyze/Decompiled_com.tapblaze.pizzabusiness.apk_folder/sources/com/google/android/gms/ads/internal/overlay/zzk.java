package com.google.android.gms.ads.internal.overlay;

import android.graphics.drawable.Drawable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzk implements Runnable {
    private final zzl zzdhm;
    private final Drawable zzdhn;

    zzk(zzl zzl, Drawable drawable) {
        this.zzdhm = zzl;
        this.zzdhn = drawable;
    }

    public final void run() {
        zzl zzl = this.zzdhm;
        zzl.zzdho.zzzk.getWindow().setBackgroundDrawable(this.zzdhn);
    }
}
