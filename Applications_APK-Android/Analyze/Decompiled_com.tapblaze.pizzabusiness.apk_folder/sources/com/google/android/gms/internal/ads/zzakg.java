package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzakg implements zzazp<zzajq> {
    private final /* synthetic */ zzazl zzcxt;
    private final /* synthetic */ zzajf zzdaz;
    private final /* synthetic */ Object zzdba;
    private final /* synthetic */ zzakd zzdbb;

    zzakg(zzakd zzakd, zzajf zzajf, Object obj, zzazl zzazl) {
        this.zzdbb = zzakd;
        this.zzdaz = zzajf;
        this.zzdba = obj;
        this.zzcxt = zzazl;
    }

    public final /* synthetic */ void zzh(Object obj) {
        this.zzdbb.zza(this.zzdaz, (zzajq) obj, this.zzdba, this.zzcxt);
    }
}
