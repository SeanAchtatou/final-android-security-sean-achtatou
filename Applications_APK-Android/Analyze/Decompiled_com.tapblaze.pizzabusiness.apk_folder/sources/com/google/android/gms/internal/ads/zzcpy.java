package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcpy implements zzcub<zzcpz> {
    private final zzczu zzfgl;

    public zzcpy(zzczu zzczu) {
        this.zzfgl = zzczu;
    }

    public final zzdhe<zzcpz> zzanc() {
        return zzdgs.zzaj(new zzcpz(this.zzfgl));
    }
}
