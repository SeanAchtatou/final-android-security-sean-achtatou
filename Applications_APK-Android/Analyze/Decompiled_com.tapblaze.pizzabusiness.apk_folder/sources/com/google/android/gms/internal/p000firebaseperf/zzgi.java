package com.google.android.gms.internal.p000firebaseperf;

import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgi  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
interface zzgi {
    int zzb(int i, Object obj, Object obj2);

    Object zzc(Object obj, Object obj2);

    zzgg<?, ?> zzj(Object obj);

    Map<?, ?> zzk(Object obj);

    Object zzl(Object obj);
}
