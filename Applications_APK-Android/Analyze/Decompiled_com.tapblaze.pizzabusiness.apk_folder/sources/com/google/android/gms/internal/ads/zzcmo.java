package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzt;
import com.google.android.gms.ads.internal.zzg;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcmo implements zzbuv {
    private final zzazb zzbli;
    private final zzczl zzffc;
    private final zzczu zzfgl;
    private final zzcbn zzfod;
    private final zzdhe<zzcbd> zzfzp;
    private final zzbdi zzgas;
    private final Context zzup;

    private zzcmo(Context context, zzcbn zzcbn, zzczu zzczu, zzazb zzazb, zzczl zzczl, zzdhe<zzcbd> zzdhe, zzbdi zzbdi) {
        this.zzup = context;
        this.zzfod = zzcbn;
        this.zzfgl = zzczu;
        this.zzbli = zzazb;
        this.zzffc = zzczl;
        this.zzfzp = zzdhe;
        this.zzgas = zzbdi;
    }

    public final void zza(boolean z, Context context) {
        zzbdi zzbdi;
        zzbdi zzbdi2;
        zzcbd zzcbd = (zzcbd) zzdgs.zzc(this.zzfzp);
        try {
            zzczl zzczl = this.zzffc;
            if (!this.zzgas.zzaap()) {
                zzbdi2 = this.zzgas;
            } else {
                if (!((Boolean) zzve.zzoy().zzd(zzzn.zzciu)).booleanValue()) {
                    zzbdi2 = this.zzgas;
                } else {
                    zzbdi zzc = this.zzfod.zzc(this.zzfgl.zzblm);
                    zzafy.zza(zzc, zzcbd.zzaev());
                    zzccd zzccd = new zzccd();
                    zzccd.zza(this.zzup, zzc.getView());
                    zzcbd.zzadx().zzb(zzc, true);
                    zzc.zzaaa().zza(new zzcmn(zzccd, zzc));
                    zzbev zzaaa = zzc.zzaaa();
                    zzc.getClass();
                    zzaaa.zza(zzcmq.zzq(zzc));
                    zzc.zzb(zzczl.zzglo.zzdhr, zzczl.zzglo.zzdht, null);
                    zzbdi = zzc;
                    zzbdi.zzax(true);
                    zzq.zzkq();
                    zzg zzg = new zzg(false, zzawb.zzbb(this.zzup), false, 0.0f, -1, z, this.zzffc.zzglv, this.zzffc.zzblf);
                    zzq.zzkp();
                    Context context2 = context;
                    zzn.zza(context2, new AdOverlayInfoParcel((zzty) null, zzcbd.zzaeo(), (zzt) null, zzbdi, this.zzffc.zzglw, this.zzbli, this.zzffc.zzdkp, zzg, this.zzffc.zzglo.zzdhr, this.zzffc.zzglo.zzdht), true);
                }
            }
            zzbdi = zzbdi2;
            zzbdi.zzax(true);
            zzq.zzkq();
            zzg zzg2 = new zzg(false, zzawb.zzbb(this.zzup), false, 0.0f, -1, z, this.zzffc.zzglv, this.zzffc.zzblf);
            zzq.zzkp();
            Context context22 = context;
            zzn.zza(context22, new AdOverlayInfoParcel((zzty) null, zzcbd.zzaeo(), (zzt) null, zzbdi, this.zzffc.zzglw, this.zzbli, this.zzffc.zzdkp, zzg2, this.zzffc.zzglo.zzdhr, this.zzffc.zzglo.zzdht), true);
        } catch (zzbdv e) {
            zzayu.zzc("", e);
        }
    }
}
