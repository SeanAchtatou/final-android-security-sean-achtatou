package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzlr implements Runnable {
    private final /* synthetic */ zzlp zzbat;

    zzlr(zzlp zzlp) {
        this.zzbat = zzlp;
    }

    public final void run() {
        if (!this.zzbat.zzaek) {
            this.zzbat.zzbae.zza((zzmn) this.zzbat);
        }
    }
}
