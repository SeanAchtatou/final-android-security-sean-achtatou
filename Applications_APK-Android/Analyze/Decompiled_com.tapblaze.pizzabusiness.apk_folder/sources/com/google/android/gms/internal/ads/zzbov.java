package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbov {
    void onAdClosed();

    void onAdLeftApplication();

    void onAdOpened();

    void onRewardedVideoCompleted();

    void onRewardedVideoStarted();

    void zzb(zzare zzare, String str, String str2);
}
