package com.google.android.gms.internal.ads;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzsk implements BaseGmsClient.BaseOnConnectionFailedListener {
    private final /* synthetic */ zzazl zzbrs;
    private final /* synthetic */ zzse zzbrt;

    zzsk(zzse zzse, zzazl zzazl) {
        this.zzbrt = zzse;
        this.zzbrs = zzazl;
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        synchronized (this.zzbrt.lock) {
            this.zzbrs.setException(new RuntimeException("Connection failed."));
        }
    }
}
