package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzctu implements zzdxg<zzcts> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<zzdhd> zzfcv;

    private zzctu(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzazb> zzdxp3) {
        this.zzfcv = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfav = zzdxp3;
    }

    public static zzctu zzq(zzdxp<zzdhd> zzdxp, zzdxp<Context> zzdxp2, zzdxp<zzazb> zzdxp3) {
        return new zzctu(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzcts(this.zzfcv.get(), this.zzejv.get(), this.zzfav.get());
    }
}
