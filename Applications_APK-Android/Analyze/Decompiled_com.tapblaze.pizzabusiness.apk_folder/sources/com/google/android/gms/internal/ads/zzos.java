package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzos {
    private final int height;
    private final int width;
    public final List<byte[]> zzafg;
    public final int zzaqu;
    public final float zzbgf;

    public static zzos zzf(zzoj zzoj) throws zzhd {
        float f;
        int i;
        int i2;
        try {
            zzoj.zzbf(4);
            int readUnsignedByte = (zzoj.readUnsignedByte() & 3) + 1;
            if (readUnsignedByte != 3) {
                ArrayList arrayList = new ArrayList();
                int readUnsignedByte2 = zzoj.readUnsignedByte() & 31;
                for (int i3 = 0; i3 < readUnsignedByte2; i3++) {
                    arrayList.add(zzg(zzoj));
                }
                int readUnsignedByte3 = zzoj.readUnsignedByte();
                for (int i4 = 0; i4 < readUnsignedByte3; i4++) {
                    arrayList.add(zzg(zzoj));
                }
                if (readUnsignedByte2 > 0) {
                    zzoh zzd = zzoi.zzd((byte[]) arrayList.get(0), readUnsignedByte, ((byte[]) arrayList.get(0)).length);
                    int i5 = zzd.width;
                    int i6 = zzd.height;
                    f = zzd.zzbgf;
                    i2 = i5;
                    i = i6;
                } else {
                    i2 = -1;
                    i = -1;
                    f = 1.0f;
                }
                return new zzos(arrayList, readUnsignedByte, i2, i, f);
            }
            throw new IllegalStateException();
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new zzhd("Error parsing AVC config", e);
        }
    }

    private zzos(List<byte[]> list, int i, int i2, int i3, float f) {
        this.zzafg = list;
        this.zzaqu = i;
        this.width = i2;
        this.height = i3;
        this.zzbgf = f;
    }

    private static byte[] zzg(zzoj zzoj) {
        int readUnsignedShort = zzoj.readUnsignedShort();
        int position = zzoj.getPosition();
        zzoj.zzbf(readUnsignedShort);
        return zzob.zzc(zzoj.data, position, readUnsignedShort);
    }
}
