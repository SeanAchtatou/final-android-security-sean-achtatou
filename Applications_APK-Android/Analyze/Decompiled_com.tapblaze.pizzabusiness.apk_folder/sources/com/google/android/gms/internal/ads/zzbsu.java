package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbsu<T> {
    public Executor executor;
    public T zzfir;

    public static <T> zzbsu<T> zzb(T t, Executor executor2) {
        return new zzbsu<>(t, executor2);
    }

    public zzbsu(T t, Executor executor2) {
        this.zzfir = t;
        this.executor = executor2;
    }
}
