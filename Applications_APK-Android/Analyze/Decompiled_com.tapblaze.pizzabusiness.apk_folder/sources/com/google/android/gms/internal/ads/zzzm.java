package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzzm implements zzden {
    private final zzzj zzcgf;
    private final zzzc zzcgg;

    zzzm(zzzj zzzj, zzzc zzzc) {
        this.zzcgf = zzzj;
        this.zzcgg = zzzc;
    }

    public final Object get() {
        return this.zzcgf.zze(this.zzcgg);
    }
}
