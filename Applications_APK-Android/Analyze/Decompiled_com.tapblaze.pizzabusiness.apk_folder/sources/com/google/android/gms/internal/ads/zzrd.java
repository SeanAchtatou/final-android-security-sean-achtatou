package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.appopen.AppOpenAd;
import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzrd extends zzrj {
    private final WeakReference<AppOpenAd.AppOpenAdLoadCallback> zzbqw;

    public zzrd(AppOpenAd.AppOpenAdLoadCallback appOpenAdLoadCallback) {
        this.zzbqw = new WeakReference<>(appOpenAdLoadCallback);
    }

    public final void zza(zzrf zzrf) {
        AppOpenAd.AppOpenAdLoadCallback appOpenAdLoadCallback = this.zzbqw.get();
        if (appOpenAdLoadCallback != null) {
            appOpenAdLoadCallback.onAppOpenAdLoaded(new zzrm(zzrf));
        }
    }

    public final void onAppOpenAdFailedToLoad(int i) {
        AppOpenAd.AppOpenAdLoadCallback appOpenAdLoadCallback = this.zzbqw.get();
        if (appOpenAdLoadCallback != null) {
            appOpenAdLoadCallback.onAppOpenAdFailedToLoad(i);
        }
    }
}
