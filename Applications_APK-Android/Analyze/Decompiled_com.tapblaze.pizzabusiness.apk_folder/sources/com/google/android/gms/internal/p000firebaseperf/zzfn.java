package com.google.android.gms.internal.p000firebaseperf;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfn  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public enum zzfn {
    VOID(Void.class, Void.class, null),
    INT(Integer.TYPE, Integer.class, 0),
    LONG(Long.TYPE, Long.class, 0L),
    FLOAT(Float.TYPE, Float.class, Float.valueOf(0.0f)),
    DOUBLE(Double.TYPE, Double.class, Double.valueOf((double) FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE)),
    BOOLEAN(Boolean.TYPE, Boolean.class, false),
    STRING(String.class, String.class, ""),
    BYTE_STRING(zzeb.class, zzeb.class, zzeb.zzmv),
    ENUM(Integer.TYPE, Integer.class, null),
    MESSAGE(Object.class, Object.class, null);
    
    private final Class<?> zzrq;
    private final Class<?> zzrr;
    private final Object zzrs;

    private zzfn(Class<?> cls, Class<?> cls2, Object obj) {
        this.zzrq = cls;
        this.zzrr = cls2;
        this.zzrs = obj;
    }

    public final Class<?> zzhs() {
        return this.zzrr;
    }
}
