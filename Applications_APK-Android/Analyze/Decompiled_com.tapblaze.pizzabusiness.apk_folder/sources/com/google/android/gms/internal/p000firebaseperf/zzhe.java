package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhe  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
interface zzhe {
    <T> zzhb<T> zzd(Class<T> cls);
}
