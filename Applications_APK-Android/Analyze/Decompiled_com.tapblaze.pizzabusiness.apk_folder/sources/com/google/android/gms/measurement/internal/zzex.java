package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.util.Clock;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzex extends zze {
    private final zzfa zza = new zzfa(this, zzn(), "google_app_measurement_local.db");
    private boolean zzb;

    zzex(zzgf zzgf) {
        super(zzgf);
    }

    /* access modifiers changed from: protected */
    public final boolean zzz() {
        return false;
    }

    public final void zzab() {
        zzb();
        zzd();
        try {
            int delete = zzae().delete("messages", null, null) + 0;
            if (delete > 0) {
                zzr().zzx().zza("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zzr().zzf().zza("Error resetting local analytics data. error", e);
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:71:0x0100 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:62:0x00ee */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r7v0 */
    /* JADX WARN: Type inference failed for: r7v1 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r7v2, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r7v3, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r7v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r7v5 */
    /* JADX WARN: Type inference failed for: r7v6 */
    /* JADX WARN: Type inference failed for: r7v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00c5 A[SYNTHETIC, Splitter:B:47:0x00c5] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x011a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x011a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x011a A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zza(int r17, byte[] r18) {
        /*
            r16 = this;
            r1 = r16
            r16.zzb()
            r16.zzd()
            boolean r0 = r1.zzb
            r2 = 0
            if (r0 == 0) goto L_0x000e
            return r2
        L_0x000e:
            android.content.ContentValues r3 = new android.content.ContentValues
            r3.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r17)
            java.lang.String r4 = "type"
            r3.put(r4, r0)
            java.lang.String r0 = "entry"
            r4 = r18
            r3.put(r0, r4)
            r4 = 5
            r5 = 0
            r6 = 5
        L_0x0026:
            if (r5 >= r4) goto L_0x012d
            r7 = 0
            r8 = 1
            android.database.sqlite.SQLiteDatabase r9 = r16.zzae()     // Catch:{ SQLiteFullException -> 0x00fe, SQLiteDatabaseLockedException -> 0x00ec, SQLiteException -> 0x00c1, all -> 0x00bd }
            if (r9 != 0) goto L_0x0038
            r1.zzb = r8     // Catch:{ SQLiteFullException -> 0x00bb, SQLiteDatabaseLockedException -> 0x00ed, SQLiteException -> 0x00b7 }
            if (r9 == 0) goto L_0x0037
            r9.close()
        L_0x0037:
            return r2
        L_0x0038:
            r9.beginTransaction()     // Catch:{ SQLiteFullException -> 0x00bb, SQLiteDatabaseLockedException -> 0x00ed, SQLiteException -> 0x00b7 }
            r10 = 0
            java.lang.String r0 = "select count(1) from messages"
            android.database.Cursor r12 = r9.rawQuery(r0, r7)     // Catch:{ SQLiteFullException -> 0x00bb, SQLiteDatabaseLockedException -> 0x00ed, SQLiteException -> 0x00b7 }
            if (r12 == 0) goto L_0x0059
            boolean r0 = r12.moveToFirst()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            if (r0 == 0) goto L_0x0059
            long r10 = r12.getLong(r2)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            goto L_0x0059
        L_0x0050:
            r0 = move-exception
            goto L_0x0122
        L_0x0053:
            r0 = move-exception
            goto L_0x00b9
        L_0x0055:
            r0 = move-exception
            r7 = r12
            goto L_0x0100
        L_0x0059:
            java.lang.String r0 = "messages"
            r13 = 100000(0x186a0, double:4.94066E-319)
            int r15 = (r10 > r13 ? 1 : (r10 == r13 ? 0 : -1))
            if (r15 < 0) goto L_0x00a0
            com.google.android.gms.measurement.internal.zzfb r15 = r16.zzr()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            com.google.android.gms.measurement.internal.zzfd r15 = r15.zzf()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            java.lang.String r4 = "Data loss, local db full"
            r15.zza(r4)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            long r13 = r13 - r10
            r10 = 1
            long r13 = r13 + r10
            java.lang.String r4 = "rowid in (select rowid from messages order by rowid asc limit ?)"
            java.lang.String[] r10 = new java.lang.String[r8]     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            java.lang.String r11 = java.lang.Long.toString(r13)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            r10[r2] = r11     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            int r4 = r9.delete(r0, r4, r10)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            long r10 = (long) r4     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            int r4 = (r10 > r13 ? 1 : (r10 == r13 ? 0 : -1))
            if (r4 == 0) goto L_0x00a0
            com.google.android.gms.measurement.internal.zzfb r4 = r16.zzr()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            com.google.android.gms.measurement.internal.zzfd r4 = r4.zzf()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            java.lang.String r15 = "Different delete count than expected in local db. expected, received, difference"
            java.lang.Long r2 = java.lang.Long.valueOf(r13)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            java.lang.Long r8 = java.lang.Long.valueOf(r10)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            long r13 = r13 - r10
            java.lang.Long r10 = java.lang.Long.valueOf(r13)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            r4.zza(r15, r2, r8, r10)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
        L_0x00a0:
            r9.insertOrThrow(r0, r7, r3)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            r9.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            r9.endTransaction()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            if (r12 == 0) goto L_0x00ae
            r12.close()
        L_0x00ae:
            if (r9 == 0) goto L_0x00b3
            r9.close()
        L_0x00b3:
            r2 = 1
            return r2
        L_0x00b5:
            r7 = r12
            goto L_0x00ed
        L_0x00b7:
            r0 = move-exception
            r12 = r7
        L_0x00b9:
            r7 = r9
            goto L_0x00c3
        L_0x00bb:
            r0 = move-exception
            goto L_0x0100
        L_0x00bd:
            r0 = move-exception
            r9 = r7
            r12 = r9
            goto L_0x0122
        L_0x00c1:
            r0 = move-exception
            r12 = r7
        L_0x00c3:
            if (r7 == 0) goto L_0x00ce
            boolean r2 = r7.inTransaction()     // Catch:{ all -> 0x00e9 }
            if (r2 == 0) goto L_0x00ce
            r7.endTransaction()     // Catch:{ all -> 0x00e9 }
        L_0x00ce:
            com.google.android.gms.measurement.internal.zzfb r2 = r16.zzr()     // Catch:{ all -> 0x00e9 }
            com.google.android.gms.measurement.internal.zzfd r2 = r2.zzf()     // Catch:{ all -> 0x00e9 }
            java.lang.String r4 = "Error writing entry to local database"
            r2.zza(r4, r0)     // Catch:{ all -> 0x00e9 }
            r2 = 1
            r1.zzb = r2     // Catch:{ all -> 0x00e9 }
            if (r12 == 0) goto L_0x00e3
            r12.close()
        L_0x00e3:
            if (r7 == 0) goto L_0x011a
            r7.close()
            goto L_0x011a
        L_0x00e9:
            r0 = move-exception
            r9 = r7
            goto L_0x0122
        L_0x00ec:
            r9 = r7
        L_0x00ed:
            long r10 = (long) r6
            android.os.SystemClock.sleep(r10)     // Catch:{ all -> 0x0120 }
            int r6 = r6 + 20
            if (r7 == 0) goto L_0x00f8
            r7.close()
        L_0x00f8:
            if (r9 == 0) goto L_0x011a
            r9.close()
            goto L_0x011a
        L_0x00fe:
            r0 = move-exception
            r9 = r7
        L_0x0100:
            com.google.android.gms.measurement.internal.zzfb r2 = r16.zzr()     // Catch:{ all -> 0x0120 }
            com.google.android.gms.measurement.internal.zzfd r2 = r2.zzf()     // Catch:{ all -> 0x0120 }
            java.lang.String r4 = "Error writing entry; local database full"
            r2.zza(r4, r0)     // Catch:{ all -> 0x0120 }
            r2 = 1
            r1.zzb = r2     // Catch:{ all -> 0x0120 }
            if (r7 == 0) goto L_0x0115
            r7.close()
        L_0x0115:
            if (r9 == 0) goto L_0x011a
            r9.close()
        L_0x011a:
            int r5 = r5 + 1
            r2 = 0
            r4 = 5
            goto L_0x0026
        L_0x0120:
            r0 = move-exception
            r12 = r7
        L_0x0122:
            if (r12 == 0) goto L_0x0127
            r12.close()
        L_0x0127:
            if (r9 == 0) goto L_0x012c
            r9.close()
        L_0x012c:
            throw r0
        L_0x012d:
            com.google.android.gms.measurement.internal.zzfb r0 = r16.zzr()
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzx()
            java.lang.String r2 = "Failed to write entry to local database"
            r0.zza(r2)
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzex.zza(int, byte[]):boolean");
    }

    public final boolean zza(zzan zzan) {
        Parcel obtain = Parcel.obtain();
        zzan.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(0, marshall);
        }
        zzr().zzg().zza("Event is too long for local database. Sending event directly to service");
        return false;
    }

    public final boolean zza(zzkq zzkq) {
        Parcel obtain = Parcel.obtain();
        zzkq.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return zza(1, marshall);
        }
        zzr().zzg().zza("User property too long for local database. Sending directly to service");
        return false;
    }

    public final boolean zza(zzv zzv) {
        zzp();
        byte[] zza2 = zzkv.zza((Parcelable) zzv);
        if (zza2.length <= 131072) {
            return zza(2, zza2);
        }
        zzr().zzg().zza("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r10v14 */
    /* JADX WARN: Type inference failed for: r3v31 */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:66|67|68|69) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:81|82|83|84) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:53|54|55|56|180) */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01da, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x01db, code lost:
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01dd, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01de, code lost:
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x01e0, code lost:
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x01e4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x01f5, code lost:
        if (r3.inTransaction() != false) goto L_0x01f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x01f7, code lost:
        r3.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0209, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x020e, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x0212, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x021e, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0223, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0250, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0255, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0065, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        zzr().zzf().zza("Failed to load event from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r11.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        zzr().zzf().zza("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        r11.recycle();
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        zzr().zzf().zza("Failed to load conditional user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        r11.recycle();
        r12 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x00f3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:66:0x0123 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:81:0x0159 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01da A[ExcHandler: all (th java.lang.Throwable), Splitter:B:20:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x01dd A[ExcHandler: SQLiteException (e android.database.sqlite.SQLiteException), Splitter:B:20:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:115:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:12:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x01f1 A[SYNTHETIC, Splitter:B:128:0x01f1] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0209  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x020e  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x021e  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0223  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x023d  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x0242  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x0250  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x0255  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x0245 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0245 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0245 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable> zza(int r25) {
        /*
            r24 = this;
            r1 = r24
            java.lang.String r2 = "Error reading entries from local database"
            r24.zzd()
            r24.zzb()
            boolean r0 = r1.zzb
            r3 = 0
            if (r0 == 0) goto L_0x0010
            return r3
        L_0x0010:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            boolean r0 = r24.zzaf()
            if (r0 != 0) goto L_0x001c
            return r4
        L_0x001c:
            r5 = 5
            r6 = 0
            r7 = 0
            r8 = 5
        L_0x0020:
            if (r7 >= r5) goto L_0x0259
            r9 = 1
            android.database.sqlite.SQLiteDatabase r15 = r24.zzae()     // Catch:{ SQLiteFullException -> 0x022b, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x01ec, all -> 0x01e8 }
            if (r15 != 0) goto L_0x0039
            r1.zzb = r9     // Catch:{ SQLiteFullException -> 0x0036, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x0031 }
            if (r15 == 0) goto L_0x0030
            r15.close()
        L_0x0030:
            return r3
        L_0x0031:
            r0 = move-exception
            r10 = r3
            r3 = r15
            goto L_0x01ef
        L_0x0036:
            r0 = move-exception
            goto L_0x022e
        L_0x0039:
            r15.beginTransaction()     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            com.google.android.gms.measurement.internal.zzx r0 = r24.zzt()     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            com.google.android.gms.measurement.internal.zzeu<java.lang.Boolean> r10 = com.google.android.gms.measurement.internal.zzap.zzbx     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            boolean r0 = r0.zza(r10)     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            java.lang.String r11 = "entry"
            java.lang.String r12 = "type"
            java.lang.String r13 = "rowid"
            r19 = -1
            r14 = 3
            r5 = 2
            if (r0 == 0) goto L_0x0095
            long r16 = zza(r15)     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            int r0 = (r16 > r19 ? 1 : (r16 == r19 ? 0 : -1))
            if (r0 == 0) goto L_0x0068
            java.lang.String r0 = "rowid<?"
            java.lang.String[] r3 = new java.lang.String[r9]     // Catch:{ SQLiteFullException -> 0x0065, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            java.lang.String r16 = java.lang.String.valueOf(r16)     // Catch:{ SQLiteFullException -> 0x0065, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            r3[r6] = r16     // Catch:{ SQLiteFullException -> 0x0065, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            goto L_0x006a
        L_0x0065:
            r0 = move-exception
            goto L_0x01e6
        L_0x0068:
            r0 = 0
            r3 = 0
        L_0x006a:
            java.lang.String r16 = "messages"
            java.lang.String[] r10 = new java.lang.String[r14]     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            r10[r6] = r13     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            r10[r9] = r12     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            r10[r5] = r11     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            r18 = 0
            r21 = 0
            java.lang.String r22 = "rowid asc"
            r11 = 100
            java.lang.String r23 = java.lang.Integer.toString(r11)     // Catch:{ SQLiteFullException -> 0x01e4, SQLiteDatabaseLockedException -> 0x01e0, SQLiteException -> 0x01dd, all -> 0x01da }
            r12 = r10
            r10 = r15
            r11 = r16
            r13 = r0
            r0 = 3
            r14 = r3
            r3 = r15
            r15 = r18
            r16 = r21
            r17 = r22
            r18 = r23
            android.database.Cursor r10 = r10.query(r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ SQLiteFullException -> 0x01d7, SQLiteDatabaseLockedException -> 0x01e1, SQLiteException -> 0x01d5, all -> 0x01d3 }
            goto L_0x00c1
        L_0x0095:
            r3 = r15
            r0 = 3
            java.lang.String r14 = "messages"
            java.lang.String[] r15 = new java.lang.String[r0]     // Catch:{ SQLiteFullException -> 0x01d7, SQLiteDatabaseLockedException -> 0x01e1, SQLiteException -> 0x01d5, all -> 0x01d3 }
            r15[r6] = r13     // Catch:{ SQLiteFullException -> 0x01d7, SQLiteDatabaseLockedException -> 0x01e1, SQLiteException -> 0x01d5, all -> 0x01d3 }
            r15[r9] = r12     // Catch:{ SQLiteFullException -> 0x01d7, SQLiteDatabaseLockedException -> 0x01e1, SQLiteException -> 0x01d5, all -> 0x01d3 }
            r15[r5] = r11     // Catch:{ SQLiteFullException -> 0x01d7, SQLiteDatabaseLockedException -> 0x01e1, SQLiteException -> 0x01d5, all -> 0x01d3 }
            r13 = 0
            r16 = 0
            r18 = 0
            r21 = 0
            java.lang.String r22 = "rowid asc"
            r10 = 100
            java.lang.String r23 = java.lang.Integer.toString(r10)     // Catch:{ SQLiteFullException -> 0x01d7, SQLiteDatabaseLockedException -> 0x01e1, SQLiteException -> 0x01d5, all -> 0x01d3 }
            r10 = r3
            r11 = r14
            r12 = r15
            r14 = r16
            r15 = r18
            r16 = r21
            r17 = r22
            r18 = r23
            android.database.Cursor r10 = r10.query(r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ SQLiteFullException -> 0x01d7, SQLiteDatabaseLockedException -> 0x01e1, SQLiteException -> 0x01d5, all -> 0x01d3 }
        L_0x00c1:
            boolean r11 = r10.moveToNext()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            if (r11 == 0) goto L_0x0195
            long r19 = r10.getLong(r6)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            int r11 = r10.getInt(r9)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            byte[] r12 = r10.getBlob(r5)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            if (r11 != 0) goto L_0x0108
            android.os.Parcel r11 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            int r13 = r12.length     // Catch:{ ParseException -> 0x00f3 }
            r11.unmarshall(r12, r6, r13)     // Catch:{ ParseException -> 0x00f3 }
            r11.setDataPosition(r6)     // Catch:{ ParseException -> 0x00f3 }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzan> r12 = com.google.android.gms.measurement.internal.zzan.CREATOR     // Catch:{ ParseException -> 0x00f3 }
            java.lang.Object r12 = r12.createFromParcel(r11)     // Catch:{ ParseException -> 0x00f3 }
            com.google.android.gms.measurement.internal.zzan r12 = (com.google.android.gms.measurement.internal.zzan) r12     // Catch:{ ParseException -> 0x00f3 }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            if (r12 == 0) goto L_0x00c1
            r4.add(r12)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            goto L_0x00c1
        L_0x00f1:
            r0 = move-exception
            goto L_0x0104
        L_0x00f3:
            com.google.android.gms.measurement.internal.zzfb r12 = r24.zzr()     // Catch:{ all -> 0x00f1 }
            com.google.android.gms.measurement.internal.zzfd r12 = r12.zzf()     // Catch:{ all -> 0x00f1 }
            java.lang.String r13 = "Failed to load event from local database"
            r12.zza(r13)     // Catch:{ all -> 0x00f1 }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            goto L_0x00c1
        L_0x0104:
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            throw r0     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
        L_0x0108:
            if (r11 != r9) goto L_0x013e
            android.os.Parcel r11 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            int r13 = r12.length     // Catch:{ ParseException -> 0x0123 }
            r11.unmarshall(r12, r6, r13)     // Catch:{ ParseException -> 0x0123 }
            r11.setDataPosition(r6)     // Catch:{ ParseException -> 0x0123 }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzkq> r12 = com.google.android.gms.measurement.internal.zzkq.CREATOR     // Catch:{ ParseException -> 0x0123 }
            java.lang.Object r12 = r12.createFromParcel(r11)     // Catch:{ ParseException -> 0x0123 }
            com.google.android.gms.measurement.internal.zzkq r12 = (com.google.android.gms.measurement.internal.zzkq) r12     // Catch:{ ParseException -> 0x0123 }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            goto L_0x0134
        L_0x0121:
            r0 = move-exception
            goto L_0x013a
        L_0x0123:
            com.google.android.gms.measurement.internal.zzfb r12 = r24.zzr()     // Catch:{ all -> 0x0121 }
            com.google.android.gms.measurement.internal.zzfd r12 = r12.zzf()     // Catch:{ all -> 0x0121 }
            java.lang.String r13 = "Failed to load user property from local database"
            r12.zza(r13)     // Catch:{ all -> 0x0121 }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            r12 = 0
        L_0x0134:
            if (r12 == 0) goto L_0x00c1
            r4.add(r12)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            goto L_0x00c1
        L_0x013a:
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            throw r0     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
        L_0x013e:
            if (r11 != r5) goto L_0x0175
            android.os.Parcel r11 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            int r13 = r12.length     // Catch:{ ParseException -> 0x0159 }
            r11.unmarshall(r12, r6, r13)     // Catch:{ ParseException -> 0x0159 }
            r11.setDataPosition(r6)     // Catch:{ ParseException -> 0x0159 }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzv> r12 = com.google.android.gms.measurement.internal.zzv.CREATOR     // Catch:{ ParseException -> 0x0159 }
            java.lang.Object r12 = r12.createFromParcel(r11)     // Catch:{ ParseException -> 0x0159 }
            com.google.android.gms.measurement.internal.zzv r12 = (com.google.android.gms.measurement.internal.zzv) r12     // Catch:{ ParseException -> 0x0159 }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            goto L_0x016a
        L_0x0157:
            r0 = move-exception
            goto L_0x0171
        L_0x0159:
            com.google.android.gms.measurement.internal.zzfb r12 = r24.zzr()     // Catch:{ all -> 0x0157 }
            com.google.android.gms.measurement.internal.zzfd r12 = r12.zzf()     // Catch:{ all -> 0x0157 }
            java.lang.String r13 = "Failed to load conditional user property from local database"
            r12.zza(r13)     // Catch:{ all -> 0x0157 }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            r12 = 0
        L_0x016a:
            if (r12 == 0) goto L_0x00c1
            r4.add(r12)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            goto L_0x00c1
        L_0x0171:
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            throw r0     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
        L_0x0175:
            if (r11 != r0) goto L_0x0186
            com.google.android.gms.measurement.internal.zzfb r11 = r24.zzr()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            com.google.android.gms.measurement.internal.zzfd r11 = r11.zzi()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            java.lang.String r12 = "Skipping app launch break"
            r11.zza(r12)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            goto L_0x00c1
        L_0x0186:
            com.google.android.gms.measurement.internal.zzfb r11 = r24.zzr()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            com.google.android.gms.measurement.internal.zzfd r11 = r11.zzf()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            java.lang.String r12 = "Unknown record type in local database"
            r11.zza(r12)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            goto L_0x00c1
        L_0x0195:
            java.lang.String r0 = "messages"
            java.lang.String r5 = "rowid <= ?"
            java.lang.String[] r11 = new java.lang.String[r9]     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            java.lang.String r12 = java.lang.Long.toString(r19)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            r11[r6] = r12     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            int r0 = r3.delete(r0, r5, r11)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            int r5 = r4.size()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            if (r0 >= r5) goto L_0x01b8
            com.google.android.gms.measurement.internal.zzfb r0 = r24.zzr()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzf()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            java.lang.String r5 = "Fewer entries removed from local database than expected"
            r0.zza(r5)     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
        L_0x01b8:
            r3.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            r3.endTransaction()     // Catch:{ SQLiteFullException -> 0x01ce, SQLiteDatabaseLockedException -> 0x01cb, SQLiteException -> 0x01c9 }
            if (r10 == 0) goto L_0x01c3
            r10.close()
        L_0x01c3:
            if (r3 == 0) goto L_0x01c8
            r3.close()
        L_0x01c8:
            return r4
        L_0x01c9:
            r0 = move-exception
            goto L_0x01ef
        L_0x01cb:
            r5 = r3
            r3 = r10
            goto L_0x0216
        L_0x01ce:
            r0 = move-exception
            r15 = r3
            r3 = r10
            goto L_0x022e
        L_0x01d3:
            r0 = move-exception
            goto L_0x01ea
        L_0x01d5:
            r0 = move-exception
            goto L_0x01ee
        L_0x01d7:
            r0 = move-exception
            r15 = r3
            goto L_0x01e6
        L_0x01da:
            r0 = move-exception
            r3 = r15
            goto L_0x01ea
        L_0x01dd:
            r0 = move-exception
            r3 = r15
            goto L_0x01ee
        L_0x01e0:
            r3 = r15
        L_0x01e1:
            r5 = r3
            r3 = 0
            goto L_0x0216
        L_0x01e4:
            r0 = move-exception
            r3 = r15
        L_0x01e6:
            r3 = 0
            goto L_0x022e
        L_0x01e8:
            r0 = move-exception
            r3 = 0
        L_0x01ea:
            r10 = 0
            goto L_0x024e
        L_0x01ec:
            r0 = move-exception
            r3 = 0
        L_0x01ee:
            r10 = 0
        L_0x01ef:
            if (r3 == 0) goto L_0x01fa
            boolean r5 = r3.inTransaction()     // Catch:{ all -> 0x0212 }
            if (r5 == 0) goto L_0x01fa
            r3.endTransaction()     // Catch:{ all -> 0x0212 }
        L_0x01fa:
            com.google.android.gms.measurement.internal.zzfb r5 = r24.zzr()     // Catch:{ all -> 0x0212 }
            com.google.android.gms.measurement.internal.zzfd r5 = r5.zzf()     // Catch:{ all -> 0x0212 }
            r5.zza(r2, r0)     // Catch:{ all -> 0x0212 }
            r1.zzb = r9     // Catch:{ all -> 0x0212 }
            if (r10 == 0) goto L_0x020c
            r10.close()
        L_0x020c:
            if (r3 == 0) goto L_0x0245
            r3.close()
            goto L_0x0245
        L_0x0212:
            r0 = move-exception
            goto L_0x024e
        L_0x0214:
            r3 = 0
            r5 = 0
        L_0x0216:
            long r9 = (long) r8
            android.os.SystemClock.sleep(r9)     // Catch:{ all -> 0x0227 }
            int r8 = r8 + 20
            if (r3 == 0) goto L_0x0221
            r3.close()
        L_0x0221:
            if (r5 == 0) goto L_0x0245
            r5.close()
            goto L_0x0245
        L_0x0227:
            r0 = move-exception
            r10 = r3
            r3 = r5
            goto L_0x024e
        L_0x022b:
            r0 = move-exception
            r3 = 0
            r15 = 0
        L_0x022e:
            com.google.android.gms.measurement.internal.zzfb r5 = r24.zzr()     // Catch:{ all -> 0x024b }
            com.google.android.gms.measurement.internal.zzfd r5 = r5.zzf()     // Catch:{ all -> 0x024b }
            r5.zza(r2, r0)     // Catch:{ all -> 0x024b }
            r1.zzb = r9     // Catch:{ all -> 0x024b }
            if (r3 == 0) goto L_0x0240
            r3.close()
        L_0x0240:
            if (r15 == 0) goto L_0x0245
            r15.close()
        L_0x0245:
            int r7 = r7 + 1
            r3 = 0
            r5 = 5
            goto L_0x0020
        L_0x024b:
            r0 = move-exception
            r10 = r3
            r3 = r15
        L_0x024e:
            if (r10 == 0) goto L_0x0253
            r10.close()
        L_0x0253:
            if (r3 == 0) goto L_0x0258
            r3.close()
        L_0x0258:
            throw r0
        L_0x0259:
            com.google.android.gms.measurement.internal.zzfb r0 = r24.zzr()
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzi()
            java.lang.String r2 = "Failed to read events from database in reasonable time"
            r0.zza(r2)
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzex.zza(int):java.util.List");
    }

    public final boolean zzac() {
        return zza(3, new byte[0]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0089, code lost:
        r3 = r3 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzad() {
        /*
            r11 = this;
            java.lang.String r0 = "Error deleting app launch break from local database"
            r11.zzd()
            r11.zzb()
            boolean r1 = r11.zzb
            r2 = 0
            if (r1 == 0) goto L_0x000e
            return r2
        L_0x000e:
            boolean r1 = r11.zzaf()
            if (r1 != 0) goto L_0x0015
            return r2
        L_0x0015:
            r1 = 5
            r3 = 0
            r4 = 5
        L_0x0018:
            if (r3 >= r1) goto L_0x0092
            r5 = 0
            r6 = 1
            android.database.sqlite.SQLiteDatabase r5 = r11.zzae()     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            if (r5 != 0) goto L_0x002a
            r11.zzb = r6     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            if (r5 == 0) goto L_0x0029
            r5.close()
        L_0x0029:
            return r2
        L_0x002a:
            r5.beginTransaction()     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            java.lang.String r7 = "messages"
            java.lang.String r8 = "type == ?"
            java.lang.String[] r9 = new java.lang.String[r6]     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r10 = 3
            java.lang.String r10 = java.lang.Integer.toString(r10)     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r9[r2] = r10     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r5.delete(r7, r8, r9)     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r5.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r5.endTransaction()     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            if (r5 == 0) goto L_0x0048
            r5.close()
        L_0x0048:
            return r6
        L_0x0049:
            r0 = move-exception
            goto L_0x008c
        L_0x004b:
            r7 = move-exception
            if (r5 == 0) goto L_0x0057
            boolean r8 = r5.inTransaction()     // Catch:{ all -> 0x0049 }
            if (r8 == 0) goto L_0x0057
            r5.endTransaction()     // Catch:{ all -> 0x0049 }
        L_0x0057:
            com.google.android.gms.measurement.internal.zzfb r8 = r11.zzr()     // Catch:{ all -> 0x0049 }
            com.google.android.gms.measurement.internal.zzfd r8 = r8.zzf()     // Catch:{ all -> 0x0049 }
            r8.zza(r0, r7)     // Catch:{ all -> 0x0049 }
            r11.zzb = r6     // Catch:{ all -> 0x0049 }
            if (r5 == 0) goto L_0x0089
            r5.close()
            goto L_0x0089
        L_0x006a:
            long r6 = (long) r4
            android.os.SystemClock.sleep(r6)     // Catch:{ all -> 0x0049 }
            int r4 = r4 + 20
            if (r5 == 0) goto L_0x0089
            r5.close()
            goto L_0x0089
        L_0x0076:
            r7 = move-exception
            com.google.android.gms.measurement.internal.zzfb r8 = r11.zzr()     // Catch:{ all -> 0x0049 }
            com.google.android.gms.measurement.internal.zzfd r8 = r8.zzf()     // Catch:{ all -> 0x0049 }
            r8.zza(r0, r7)     // Catch:{ all -> 0x0049 }
            r11.zzb = r6     // Catch:{ all -> 0x0049 }
            if (r5 == 0) goto L_0x0089
            r5.close()
        L_0x0089:
            int r3 = r3 + 1
            goto L_0x0018
        L_0x008c:
            if (r5 == 0) goto L_0x0091
            r5.close()
        L_0x0091:
            throw r0
        L_0x0092:
            com.google.android.gms.measurement.internal.zzfb r0 = r11.zzr()
            com.google.android.gms.measurement.internal.zzfd r0 = r0.zzi()
            java.lang.String r1 = "Error deleting app launch break from local database in reasonable time"
            r0.zza(r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzex.zzad():boolean");
    }

    private static long zza(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor = null;
        try {
            cursor = sQLiteDatabase.query("messages", new String[]{"rowid"}, "type=?", new String[]{"3"}, null, null, "rowid desc", "1");
            if (cursor.moveToFirst()) {
                return cursor.getLong(0);
            }
            if (cursor == null) {
                return -1;
            }
            cursor.close();
            return -1;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private final SQLiteDatabase zzae() throws SQLiteException {
        if (this.zzb) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.zza.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.zzb = true;
        return null;
    }

    private final boolean zzaf() {
        return zzn().getDatabasePath("google_app_measurement_local.db").exists();
    }

    public final /* bridge */ /* synthetic */ void zza() {
        super.zza();
    }

    public final /* bridge */ /* synthetic */ void zzb() {
        super.zzb();
    }

    public final /* bridge */ /* synthetic */ void zzc() {
        super.zzc();
    }

    public final /* bridge */ /* synthetic */ void zzd() {
        super.zzd();
    }

    public final /* bridge */ /* synthetic */ zzb zze() {
        return super.zze();
    }

    public final /* bridge */ /* synthetic */ zzhk zzf() {
        return super.zzf();
    }

    public final /* bridge */ /* synthetic */ zzey zzg() {
        return super.zzg();
    }

    public final /* bridge */ /* synthetic */ zzis zzh() {
        return super.zzh();
    }

    public final /* bridge */ /* synthetic */ zzin zzi() {
        return super.zzi();
    }

    public final /* bridge */ /* synthetic */ zzex zzj() {
        return super.zzj();
    }

    public final /* bridge */ /* synthetic */ zzjt zzk() {
        return super.zzk();
    }

    public final /* bridge */ /* synthetic */ zzah zzl() {
        return super.zzl();
    }

    public final /* bridge */ /* synthetic */ Clock zzm() {
        return super.zzm();
    }

    public final /* bridge */ /* synthetic */ Context zzn() {
        return super.zzn();
    }

    public final /* bridge */ /* synthetic */ zzez zzo() {
        return super.zzo();
    }

    public final /* bridge */ /* synthetic */ zzkv zzp() {
        return super.zzp();
    }

    public final /* bridge */ /* synthetic */ zzgc zzq() {
        return super.zzq();
    }

    public final /* bridge */ /* synthetic */ zzfb zzr() {
        return super.zzr();
    }

    public final /* bridge */ /* synthetic */ zzfo zzs() {
        return super.zzs();
    }

    public final /* bridge */ /* synthetic */ zzx zzt() {
        return super.zzt();
    }

    public final /* bridge */ /* synthetic */ zzw zzu() {
        return super.zzu();
    }
}
