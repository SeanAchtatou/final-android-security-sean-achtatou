package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvx;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdwa implements zzdsa {
    static final zzdsa zzew = new zzdwa();

    private zzdwa() {
    }

    public final boolean zzf(int i) {
        return zzdvx.zza.zzc.zzhd(i) != null;
    }
}
