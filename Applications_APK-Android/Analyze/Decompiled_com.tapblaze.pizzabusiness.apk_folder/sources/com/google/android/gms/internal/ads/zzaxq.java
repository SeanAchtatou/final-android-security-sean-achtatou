package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaxq extends zzq<zzo> {
    private final Map<String, String> zzab;
    private final zzazl<zzo> zzdul;
    private final zzayo zzdum;

    public zzaxq(String str, zzazl<zzo> zzazl) {
        this(str, null, zzazl);
    }

    private zzaxq(String str, Map<String, String> map, zzazl<zzo> zzazl) {
        super(0, str, new zzaxt(zzazl));
        this.zzab = null;
        this.zzdul = zzazl;
        this.zzdum = new zzayo();
        this.zzdum.zza(str, "GET", null, null);
    }

    /* access modifiers changed from: protected */
    public final zzz<zzo> zza(zzo zzo) {
        return zzz.zza(zzo, zzas.zzb(zzo));
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Object obj) {
        zzo zzo = (zzo) obj;
        this.zzdum.zza(zzo.zzab, zzo.statusCode);
        zzayo zzayo = this.zzdum;
        byte[] bArr = zzo.data;
        if (zzayo.isEnabled() && bArr != null) {
            zzayo.zzi(bArr);
        }
        this.zzdul.set(zzo);
    }
}
