package com.google.android.gms.games.achievement;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Asserts;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.DataUtils;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzc;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class AchievementEntity extends zzc implements Achievement {
    public static final Parcelable.Creator<AchievementEntity> CREATOR = new zza();
    private final String description;
    private final String name;
    private final int state;
    private final int type;
    private final String zzfe;
    private final Uri zzff;
    private final String zzfg;
    private final Uri zzfh;
    private final String zzfi;
    private final int zzfj;
    private final String zzfk;
    private final PlayerEntity zzfl;
    private final int zzfm;
    private final String zzfn;
    private final long zzfo;
    private final long zzfp;
    private final float zzfq;
    private final String zzi;

    public AchievementEntity(Achievement achievement) {
        this.zzfe = achievement.getAchievementId();
        this.type = achievement.getType();
        this.name = achievement.getName();
        this.description = achievement.getDescription();
        this.zzff = achievement.getUnlockedImageUri();
        this.zzfg = achievement.getUnlockedImageUrl();
        this.zzfh = achievement.getRevealedImageUri();
        this.zzfi = achievement.getRevealedImageUrl();
        if (achievement.zzad() != null) {
            this.zzfl = (PlayerEntity) achievement.zzad().freeze();
        } else {
            this.zzfl = null;
        }
        this.state = achievement.getState();
        this.zzfo = achievement.getLastUpdatedTimestamp();
        this.zzfp = achievement.getXpValue();
        this.zzfq = achievement.zzae();
        this.zzi = achievement.getApplicationId();
        if (achievement.getType() == 1) {
            this.zzfj = achievement.getTotalSteps();
            this.zzfk = achievement.getFormattedTotalSteps();
            this.zzfm = achievement.getCurrentSteps();
            this.zzfn = achievement.getFormattedCurrentSteps();
        } else {
            this.zzfj = 0;
            this.zzfk = null;
            this.zzfm = 0;
            this.zzfn = null;
        }
        Asserts.checkNotNull(this.zzfe);
        Asserts.checkNotNull(this.description);
    }

    public final Achievement freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    AchievementEntity(String str, int i, String str2, String str3, Uri uri, String str4, Uri uri2, String str5, int i2, String str6, PlayerEntity playerEntity, int i3, int i4, String str7, long j, long j2, float f, String str8) {
        this.zzfe = str;
        this.type = i;
        this.name = str2;
        this.description = str3;
        this.zzff = uri;
        this.zzfg = str4;
        this.zzfh = uri2;
        this.zzfi = str5;
        this.zzfj = i2;
        this.zzfk = str6;
        this.zzfl = playerEntity;
        this.state = i3;
        this.zzfm = i4;
        this.zzfn = str7;
        this.zzfo = j;
        this.zzfp = j2;
        this.zzfq = f;
        this.zzi = str8;
    }

    public final String getAchievementId() {
        return this.zzfe;
    }

    public final String getApplicationId() {
        return this.zzi;
    }

    public final int getType() {
        return this.type;
    }

    public final String getName() {
        return this.name;
    }

    public final void getName(CharArrayBuffer charArrayBuffer) {
        DataUtils.copyStringToBuffer(this.name, charArrayBuffer);
    }

    public final String getDescription() {
        return this.description;
    }

    public final void getDescription(CharArrayBuffer charArrayBuffer) {
        DataUtils.copyStringToBuffer(this.description, charArrayBuffer);
    }

    public final Uri getUnlockedImageUri() {
        return this.zzff;
    }

    public final String getUnlockedImageUrl() {
        return this.zzfg;
    }

    public final Uri getRevealedImageUri() {
        return this.zzfh;
    }

    public final String getRevealedImageUrl() {
        return this.zzfi;
    }

    public final int getTotalSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        Asserts.checkState(z);
        return this.zzfj;
    }

    public final String getFormattedTotalSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        Asserts.checkState(z);
        return this.zzfk;
    }

    public final void getFormattedTotalSteps(CharArrayBuffer charArrayBuffer) {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        Asserts.checkState(z);
        DataUtils.copyStringToBuffer(this.zzfk, charArrayBuffer);
    }

    public final Player getPlayer() {
        return (Player) Preconditions.checkNotNull(this.zzfl);
    }

    public final Player zzad() {
        return this.zzfl;
    }

    public final int getState() {
        return this.state;
    }

    public final int getCurrentSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        Asserts.checkState(z);
        return this.zzfm;
    }

    public final String getFormattedCurrentSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        Asserts.checkState(z);
        return this.zzfn;
    }

    public final void getFormattedCurrentSteps(CharArrayBuffer charArrayBuffer) {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        Asserts.checkState(z);
        DataUtils.copyStringToBuffer(this.zzfn, charArrayBuffer);
    }

    public final long getLastUpdatedTimestamp() {
        return this.zzfo;
    }

    public final long getXpValue() {
        return this.zzfp;
    }

    public final float zzae() {
        return this.zzfq;
    }

    public final int hashCode() {
        return zza(this);
    }

    static int zza(Achievement achievement) {
        int i;
        int i2;
        if (achievement.getType() == 1) {
            i2 = achievement.getCurrentSteps();
            i = achievement.getTotalSteps();
        } else {
            i2 = 0;
            i = 0;
        }
        return Objects.hashCode(achievement.getAchievementId(), achievement.getApplicationId(), achievement.getName(), Integer.valueOf(achievement.getType()), achievement.getDescription(), Long.valueOf(achievement.getXpValue()), Integer.valueOf(achievement.getState()), Long.valueOf(achievement.getLastUpdatedTimestamp()), achievement.zzad(), Integer.valueOf(i2), Integer.valueOf(i));
    }

    public final boolean equals(Object obj) {
        return zza(this, obj);
    }

    static boolean zza(Achievement achievement, Object obj) {
        if (!(obj instanceof Achievement)) {
            return false;
        }
        if (achievement == obj) {
            return true;
        }
        Achievement achievement2 = (Achievement) obj;
        if (achievement2.getType() != achievement.getType()) {
            return false;
        }
        return (achievement.getType() != 1 || (achievement2.getCurrentSteps() == achievement.getCurrentSteps() && achievement2.getTotalSteps() == achievement.getTotalSteps())) && achievement2.getXpValue() == achievement.getXpValue() && achievement2.getState() == achievement.getState() && achievement2.getLastUpdatedTimestamp() == achievement.getLastUpdatedTimestamp() && Objects.equal(achievement2.getAchievementId(), achievement.getAchievementId()) && Objects.equal(achievement2.getApplicationId(), achievement.getApplicationId()) && Objects.equal(achievement2.getName(), achievement.getName()) && Objects.equal(achievement2.getDescription(), achievement.getDescription()) && Objects.equal(achievement2.zzad(), achievement.zzad()) && achievement2.zzae() == achievement.zzae();
    }

    public final String toString() {
        return zzb(this);
    }

    static String zzb(Achievement achievement) {
        Objects.ToStringHelper add = Objects.toStringHelper(achievement).add("Id", achievement.getAchievementId()).add("Game Id", achievement.getApplicationId()).add("Type", Integer.valueOf(achievement.getType())).add("Name", achievement.getName()).add("Description", achievement.getDescription()).add("Player", achievement.zzad()).add("State", Integer.valueOf(achievement.getState())).add("Rarity Percent", Float.valueOf(achievement.zzae()));
        if (achievement.getType() == 1) {
            add.add("CurrentSteps", Integer.valueOf(achievement.getCurrentSteps()));
            add.add("TotalSteps", Integer.valueOf(achievement.getTotalSteps()));
        }
        return add.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, getAchievementId(), false);
        SafeParcelWriter.writeInt(parcel, 2, getType());
        SafeParcelWriter.writeString(parcel, 3, getName(), false);
        SafeParcelWriter.writeString(parcel, 4, getDescription(), false);
        SafeParcelWriter.writeParcelable(parcel, 5, getUnlockedImageUri(), i, false);
        SafeParcelWriter.writeString(parcel, 6, getUnlockedImageUrl(), false);
        SafeParcelWriter.writeParcelable(parcel, 7, getRevealedImageUri(), i, false);
        SafeParcelWriter.writeString(parcel, 8, getRevealedImageUrl(), false);
        SafeParcelWriter.writeInt(parcel, 9, this.zzfj);
        SafeParcelWriter.writeString(parcel, 10, this.zzfk, false);
        SafeParcelWriter.writeParcelable(parcel, 11, this.zzfl, i, false);
        SafeParcelWriter.writeInt(parcel, 12, getState());
        SafeParcelWriter.writeInt(parcel, 13, this.zzfm);
        SafeParcelWriter.writeString(parcel, 14, this.zzfn, false);
        SafeParcelWriter.writeLong(parcel, 15, getLastUpdatedTimestamp());
        SafeParcelWriter.writeLong(parcel, 16, getXpValue());
        SafeParcelWriter.writeFloat(parcel, 17, this.zzfq);
        SafeParcelWriter.writeString(parcel, 18, this.zzi, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
