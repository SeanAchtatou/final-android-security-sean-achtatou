package com.google.android.gms.internal.ads;

import android.util.Log;
import cz.msebera.android.httpclient.HttpStatus;
import cz.msebera.android.httpclient.message.TokenParser;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzjy {
    public int height;
    public int number;
    public int type;
    public int width;
    public zziv zzafh;
    public int zzafl;
    public byte[] zzafm;
    public int zzafo;
    public int zzafp;
    /* access modifiers changed from: private */
    public String zzafv;
    public String zzapp;
    public int zzapq;
    public boolean zzapr;
    public byte[] zzaps;
    public zzjn zzapt;
    public byte[] zzapu;
    public int zzapv;
    public int zzapw;
    public int zzapx;
    public boolean zzapy;
    public int zzapz;
    public int zzaqa;
    public int zzaqb;
    public int zzaqc;
    public int zzaqd;
    public float zzaqe;
    public float zzaqf;
    public float zzaqg;
    public float zzaqh;
    public float zzaqi;
    public float zzaqj;
    public float zzaqk;
    public float zzaql;
    public float zzaqm;
    public float zzaqn;
    public int zzaqo;
    public long zzaqp;
    public long zzaqq;
    public boolean zzaqr;
    public boolean zzaqs;
    public zzjo zzaqt;
    public int zzaqu;

    private zzjy() {
        this.width = -1;
        this.height = -1;
        this.zzapv = -1;
        this.zzapw = -1;
        this.zzapx = 0;
        this.zzafm = null;
        this.zzafl = -1;
        this.zzapy = false;
        this.zzapz = -1;
        this.zzaqa = -1;
        this.zzaqb = -1;
        this.zzaqc = 1000;
        this.zzaqd = HttpStatus.SC_OK;
        this.zzaqe = -1.0f;
        this.zzaqf = -1.0f;
        this.zzaqg = -1.0f;
        this.zzaqh = -1.0f;
        this.zzaqi = -1.0f;
        this.zzaqj = -1.0f;
        this.zzaqk = -1.0f;
        this.zzaql = -1.0f;
        this.zzaqm = -1.0f;
        this.zzaqn = -1.0f;
        this.zzafo = 1;
        this.zzaqo = -1;
        this.zzafp = 8000;
        this.zzaqp = 0;
        this.zzaqq = 0;
        this.zzaqs = true;
        this.zzafv = "eng";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void zza(zzjf zzjf, int i) throws zzhd {
        char c;
        int i2;
        int i3;
        String str;
        ArrayList arrayList;
        zzgw zzgw;
        zzor zzor;
        int i4;
        String str2;
        String str3;
        List<byte[]> list;
        String str4;
        String str5;
        int zzbk;
        String str6 = this.zzapp;
        int i5 = 3;
        switch (str6.hashCode()) {
            case -2095576542:
                if (str6.equals("V_MPEG4/ISO/AP")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            case -2095575984:
                if (str6.equals("V_MPEG4/ISO/SP")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case -1985379776:
                if (str6.equals("A_MS/ACM")) {
                    c = 22;
                    break;
                }
                c = 65535;
                break;
            case -1784763192:
                if (str6.equals("A_TRUEHD")) {
                    c = 17;
                    break;
                }
                c = 65535;
                break;
            case -1730367663:
                if (str6.equals("A_VORBIS")) {
                    c = 10;
                    break;
                }
                c = 65535;
                break;
            case -1482641358:
                if (str6.equals("A_MPEG/L2")) {
                    c = TokenParser.CR;
                    break;
                }
                c = 65535;
                break;
            case -1482641357:
                if (str6.equals("A_MPEG/L3")) {
                    c = 14;
                    break;
                }
                c = 65535;
                break;
            case -1373388978:
                if (str6.equals("V_MS/VFW/FOURCC")) {
                    c = 8;
                    break;
                }
                c = 65535;
                break;
            case -933872740:
                if (str6.equals("S_DVBSUB")) {
                    c = 27;
                    break;
                }
                c = 65535;
                break;
            case -538363189:
                if (str6.equals("V_MPEG4/ISO/ASP")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case -538363109:
                if (str6.equals("V_MPEG4/ISO/AVC")) {
                    c = 6;
                    break;
                }
                c = 65535;
                break;
            case -425012669:
                if (str6.equals("S_VOBSUB")) {
                    c = 25;
                    break;
                }
                c = 65535;
                break;
            case -356037306:
                if (str6.equals("A_DTS/LOSSLESS")) {
                    c = 20;
                    break;
                }
                c = 65535;
                break;
            case 62923557:
                if (str6.equals("A_AAC")) {
                    c = 12;
                    break;
                }
                c = 65535;
                break;
            case 62923603:
                if (str6.equals("A_AC3")) {
                    c = 15;
                    break;
                }
                c = 65535;
                break;
            case 62927045:
                if (str6.equals("A_DTS")) {
                    c = 18;
                    break;
                }
                c = 65535;
                break;
            case 82338133:
                if (str6.equals("V_VP8")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 82338134:
                if (str6.equals("V_VP9")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            case 99146302:
                if (str6.equals("S_HDMV/PGS")) {
                    c = 26;
                    break;
                }
                c = 65535;
                break;
            case 444813526:
                if (str6.equals("V_THEORA")) {
                    c = 9;
                    break;
                }
                c = 65535;
                break;
            case 542569478:
                if (str6.equals("A_DTS/EXPRESS")) {
                    c = 19;
                    break;
                }
                c = 65535;
                break;
            case 725957860:
                if (str6.equals("A_PCM/INT/LIT")) {
                    c = 23;
                    break;
                }
                c = 65535;
                break;
            case 855502857:
                if (str6.equals("V_MPEGH/ISO/HEVC")) {
                    c = 7;
                    break;
                }
                c = 65535;
                break;
            case 1422270023:
                if (str6.equals("S_TEXT/UTF8")) {
                    c = 24;
                    break;
                }
                c = 65535;
                break;
            case 1809237540:
                if (str6.equals("V_MPEG2")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 1950749482:
                if (str6.equals("A_EAC3")) {
                    c = 16;
                    break;
                }
                c = 65535;
                break;
            case 1950789798:
                if (str6.equals("A_FLAC")) {
                    c = 21;
                    break;
                }
                c = 65535;
                break;
            case 1951062397:
                if (str6.equals("A_OPUS")) {
                    c = 11;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        byte[] bArr = null;
        switch (c) {
            case 0:
                str2 = "video/x-vnd.on2.vp8";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 1:
                str2 = "video/x-vnd.on2.vp9";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 2:
                str2 = "video/mpeg2";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 3:
            case 4:
            case 5:
                byte[] bArr2 = this.zzapu;
                arrayList = bArr2 == null ? null : Collections.singletonList(bArr2);
                str3 = "video/mp4v-es";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            case 6:
                zzos zzf = zzos.zzf(new zzoj(this.zzapu));
                list = zzf.zzafg;
                this.zzaqu = zzf.zzaqu;
                str4 = "video/avc";
                str = str4;
                arrayList = list;
                i3 = -1;
                i2 = -1;
                break;
            case 7:
                zzoy zzh = zzoy.zzh(new zzoj(this.zzapu));
                list = zzh.zzafg;
                this.zzaqu = zzh.zzaqu;
                str4 = "video/hevc";
                str = str4;
                arrayList = list;
                i3 = -1;
                i2 = -1;
                break;
            case 8:
                arrayList = zza(new zzoj(this.zzapu));
                if (arrayList != null) {
                    str3 = "video/wvc1";
                    str = str3;
                    i3 = -1;
                    i2 = -1;
                    break;
                } else {
                    Log.w("MatroskaExtractor", "Unsupported FourCC. Setting mimeType to video/x-unknown");
                    str = "video/x-unknown";
                    i3 = -1;
                    i2 = -1;
                }
            case 9:
                str = "video/x-unknown";
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 10:
                arrayList = zzd(this.zzapu);
                str = "audio/vorbis";
                i3 = 8192;
                i2 = -1;
                break;
            case 11:
                ArrayList arrayList2 = new ArrayList(3);
                arrayList2.add(this.zzapu);
                arrayList2.add(ByteBuffer.allocate(8).order(ByteOrder.nativeOrder()).putLong(this.zzaqp).array());
                arrayList2.add(ByteBuffer.allocate(8).order(ByteOrder.nativeOrder()).putLong(this.zzaqq).array());
                arrayList = arrayList2;
                str = "audio/opus";
                i3 = 5760;
                i2 = -1;
                break;
            case 12:
                arrayList = Collections.singletonList(this.zzapu);
                str3 = "audio/mp4a-latm";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            case 13:
                str5 = "audio/mpeg-L2";
                str = str5;
                arrayList = null;
                i3 = 4096;
                i2 = -1;
                break;
            case 14:
                str5 = "audio/mpeg";
                str = str5;
                arrayList = null;
                i3 = 4096;
                i2 = -1;
                break;
            case 15:
                str2 = "audio/ac3";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 16:
                str2 = "audio/eac3";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 17:
                str2 = "audio/true-hd";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 18:
            case 19:
                str2 = "audio/vnd.dts";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 20:
                str2 = "audio/vnd.dts.hd";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 21:
                arrayList = Collections.singletonList(this.zzapu);
                str3 = "audio/x-flac";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            case 22:
                if (zzb(new zzoj(this.zzapu))) {
                    zzbk = zzoq.zzbk(this.zzaqo);
                    if (zzbk == 0) {
                        int i6 = this.zzaqo;
                        StringBuilder sb = new StringBuilder("audio/x-unknown".length() + 60);
                        sb.append("Unsupported PCM bit depth: ");
                        sb.append(i6);
                        sb.append(". Setting mimeType to ");
                        sb.append("audio/x-unknown");
                        Log.w("MatroskaExtractor", sb.toString());
                    }
                    i2 = zzbk;
                    str = "audio/raw";
                    arrayList = null;
                    i3 = -1;
                    break;
                } else {
                    Log.w("MatroskaExtractor", "audio/x-unknown".length() != 0 ? "Non-PCM MS/ACM is unsupported. Setting mimeType to ".concat("audio/x-unknown") : new String("Non-PCM MS/ACM is unsupported. Setting mimeType to "));
                }
                str = "audio/x-unknown";
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 23:
                zzbk = zzoq.zzbk(this.zzaqo);
                if (zzbk == 0) {
                    int i7 = this.zzaqo;
                    StringBuilder sb2 = new StringBuilder("audio/x-unknown".length() + 60);
                    sb2.append("Unsupported PCM bit depth: ");
                    sb2.append(i7);
                    sb2.append(". Setting mimeType to ");
                    sb2.append("audio/x-unknown");
                    Log.w("MatroskaExtractor", sb2.toString());
                    str = "audio/x-unknown";
                    arrayList = null;
                    i3 = -1;
                    i2 = -1;
                    break;
                }
                i2 = zzbk;
                str = "audio/raw";
                arrayList = null;
                i3 = -1;
                break;
            case 24:
                str2 = "application/x-subrip";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 25:
                arrayList = Collections.singletonList(this.zzapu);
                str3 = "application/vobsub";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            case 26:
                str2 = "application/pgs";
                str = str2;
                arrayList = null;
                i3 = -1;
                i2 = -1;
                break;
            case 27:
                byte[] bArr3 = this.zzapu;
                arrayList = Collections.singletonList(new byte[]{bArr3[0], bArr3[1], bArr3[2], bArr3[3]});
                str3 = "application/dvbsubs";
                str = str3;
                i3 = -1;
                i2 = -1;
                break;
            default:
                throw new zzhd("Unrecognized codec identifier.");
        }
        boolean z = this.zzaqs | false | (this.zzaqr ? (char) 2 : 0);
        if (zzof.zzbh(str)) {
            zzgw = zzgw.zza(Integer.toString(i), str, null, -1, i3, this.zzafo, this.zzafp, i2, arrayList, this.zzafh, z ? 1 : 0, this.zzafv);
            i5 = 1;
        } else if (zzof.zzbi(str)) {
            if (this.zzapx == 0) {
                int i8 = this.zzapv;
                if (i8 == -1) {
                    i8 = this.width;
                }
                this.zzapv = i8;
                int i9 = this.zzapw;
                if (i9 == -1) {
                    i9 = this.height;
                }
                this.zzapw = i9;
            }
            int i10 = this.zzapv;
            float f = (i10 == -1 || (i4 = this.zzapw) == -1) ? -1.0f : ((float) (this.height * i10)) / ((float) (this.width * i4));
            if (this.zzapy) {
                if (!(this.zzaqe == -1.0f || this.zzaqf == -1.0f || this.zzaqg == -1.0f || this.zzaqh == -1.0f || this.zzaqi == -1.0f || this.zzaqj == -1.0f || this.zzaqk == -1.0f || this.zzaql == -1.0f || this.zzaqm == -1.0f || this.zzaqn == -1.0f)) {
                    bArr = new byte[25];
                    ByteBuffer wrap = ByteBuffer.wrap(bArr);
                    wrap.put((byte) 0);
                    wrap.putShort((short) ((int) ((this.zzaqe * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzaqf * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzaqg * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzaqh * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzaqi * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzaqj * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzaqk * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) ((this.zzaql * 50000.0f) + 0.5f)));
                    wrap.putShort((short) ((int) (this.zzaqm + 0.5f)));
                    wrap.putShort((short) ((int) (this.zzaqn + 0.5f)));
                    wrap.putShort((short) this.zzaqc);
                    wrap.putShort((short) this.zzaqd);
                }
                zzor = new zzor(this.zzapz, this.zzaqb, this.zzaqa, bArr);
            } else {
                zzor = null;
            }
            zzgw = zzgw.zza(Integer.toString(i), str, null, -1, i3, this.width, this.height, -1.0f, arrayList, -1, f, this.zzafm, this.zzafl, zzor, this.zzafh);
            i5 = 2;
        } else if ("application/x-subrip".equals(str)) {
            zzgw = zzgw.zza(Integer.toString(i), str, (String) null, -1, z ? 1 : 0, this.zzafv, this.zzafh);
        } else if ("application/vobsub".equals(str) || "application/pgs".equals(str) || "application/dvbsubs".equals(str)) {
            zzgw = zzgw.zza(Integer.toString(i), str, (String) null, -1, arrayList, this.zzafv, this.zzafh);
        } else {
            throw new zzhd("Unexpected MIME type.");
        }
        this.zzaqt = zzjf.zzc(this.number, i5);
        this.zzaqt.zze(zzgw);
    }

    private static List<byte[]> zza(zzoj zzoj) throws zzhd {
        try {
            zzoj.zzbf(16);
            if (zzoj.zziq() != 826496599) {
                return null;
            }
            byte[] bArr = zzoj.data;
            for (int position = zzoj.getPosition() + 20; position < bArr.length - 4; position++) {
                if (bArr[position] == 0 && bArr[position + 1] == 0 && bArr[position + 2] == 1 && bArr[position + 3] == 15) {
                    return Collections.singletonList(Arrays.copyOfRange(bArr, position, bArr.length));
                }
            }
            throw new zzhd("Failed to find FourCC VC1 initialization data");
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new zzhd("Error parsing FourCC VC1 codec private");
        }
    }

    private static List<byte[]> zzd(byte[] bArr) throws zzhd {
        try {
            if (bArr[0] == 2) {
                int i = 1;
                int i2 = 0;
                while (bArr[i] == -1) {
                    i2 += 255;
                    i++;
                }
                int i3 = i + 1;
                int i4 = i2 + bArr[i];
                int i5 = 0;
                while (bArr[i3] == -1) {
                    i5 += 255;
                    i3++;
                }
                int i6 = i3 + 1;
                int i7 = i5 + bArr[i3];
                if (bArr[i6] == 1) {
                    byte[] bArr2 = new byte[i4];
                    System.arraycopy(bArr, i6, bArr2, 0, i4);
                    int i8 = i6 + i4;
                    if (bArr[i8] == 3) {
                        int i9 = i8 + i7;
                        if (bArr[i9] == 5) {
                            byte[] bArr3 = new byte[(bArr.length - i9)];
                            System.arraycopy(bArr, i9, bArr3, 0, bArr.length - i9);
                            ArrayList arrayList = new ArrayList(2);
                            arrayList.add(bArr2);
                            arrayList.add(bArr3);
                            return arrayList;
                        }
                        throw new zzhd("Error parsing vorbis codec private");
                    }
                    throw new zzhd("Error parsing vorbis codec private");
                }
                throw new zzhd("Error parsing vorbis codec private");
            }
            throw new zzhd("Error parsing vorbis codec private");
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new zzhd("Error parsing vorbis codec private");
        }
    }

    private static boolean zzb(zzoj zzoj) throws zzhd {
        try {
            int zzio = zzoj.zzio();
            if (zzio == 1) {
                return true;
            }
            if (zzio == 65534) {
                zzoj.zzbe(24);
                return zzoj.readLong() == zzjt.zzanp.getMostSignificantBits() && zzoj.readLong() == zzjt.zzanp.getLeastSignificantBits();
            }
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new zzhd("Error parsing MS/ACM codec private");
        }
    }

    /* synthetic */ zzjy(zzjw zzjw) {
        this();
    }
}
