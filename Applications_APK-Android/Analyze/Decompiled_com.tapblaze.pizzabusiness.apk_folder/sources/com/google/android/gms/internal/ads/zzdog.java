package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdog extends ThreadLocal<Cipher> {
    zzdog() {
    }

    private static Cipher zzaxb() {
        try {
            return zzdoy.zzhgg.zzhd("AES/GCM/NoPadding");
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object initialValue() {
        return zzaxb();
    }
}
