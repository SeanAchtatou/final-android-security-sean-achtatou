package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzas  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzas extends zzaz<Long> {
    private static zzas zzaw;

    private zzas() {
    }

    /* access modifiers changed from: protected */
    public final String zzaf() {
        return "sessions_memory_capture_frequency_fg_ms";
    }

    /* access modifiers changed from: protected */
    public final String zzag() {
        return "com.google.firebase.perf.SessionsMemoryCaptureFrequencyForegroundMs";
    }

    /* access modifiers changed from: protected */
    public final String zzaj() {
        return "fpr_session_gauge_memory_capture_frequency_fg_ms";
    }

    public static synchronized zzas zzau() {
        zzas zzas;
        synchronized (zzas.class) {
            if (zzaw == null) {
                zzaw = new zzas();
            }
            zzas = zzaw;
        }
        return zzas;
    }
}
