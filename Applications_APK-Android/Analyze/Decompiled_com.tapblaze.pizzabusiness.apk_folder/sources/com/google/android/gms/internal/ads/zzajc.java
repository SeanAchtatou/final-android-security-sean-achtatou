package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzajc implements Runnable {
    private final zzaif zzczv;

    private zzajc(zzaif zzaif) {
        this.zzczv = zzaif;
    }

    static Runnable zzb(zzaif zzaif) {
        return new zzajc(zzaif);
    }

    public final void run() {
        this.zzczv.destroy();
    }
}
