package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzajv<T> {
    JSONObject zzj(T t) throws JSONException;
}
