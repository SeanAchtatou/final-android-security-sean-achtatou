package com.google.android.gms.internal.ads;

import java.io.PrintWriter;

/* compiled from: com.google.android.gms:play-services-ads-base@@18.3.0 */
abstract class zzdps {
    private static final Throwable[] zzhha = new Throwable[0];

    zzdps() {
    }

    public abstract void zza(Throwable th, PrintWriter printWriter);

    public abstract void zza(Throwable th, Throwable th2);

    public abstract void zzl(Throwable th);
}
