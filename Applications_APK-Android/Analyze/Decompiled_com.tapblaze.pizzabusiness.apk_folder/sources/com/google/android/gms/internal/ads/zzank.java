package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzank extends zzgc implements zzani {
    zzank(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.mediation.client.rtb.IRtbAdapter");
    }

    public final void zza(IObjectWrapper iObjectWrapper, String str, Bundle bundle, Bundle bundle2, zzuj zzuj, zzanj zzanj) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        obtainAndWriteInterfaceToken.writeString(str);
        zzge.zza(obtainAndWriteInterfaceToken, bundle);
        zzge.zza(obtainAndWriteInterfaceToken, bundle2);
        zzge.zza(obtainAndWriteInterfaceToken, zzuj);
        zzge.zza(obtainAndWriteInterfaceToken, zzanj);
        zza(1, obtainAndWriteInterfaceToken);
    }

    public final zzanw zztc() throws RemoteException {
        Parcel transactAndReadException = transactAndReadException(2, obtainAndWriteInterfaceToken());
        zzanw zzanw = (zzanw) zzge.zza(transactAndReadException, zzanw.CREATOR);
        transactAndReadException.recycle();
        return zzanw;
    }

    public final zzanw zztd() throws RemoteException {
        Parcel transactAndReadException = transactAndReadException(3, obtainAndWriteInterfaceToken());
        zzanw zzanw = (zzanw) zzge.zza(transactAndReadException, zzanw.CREATOR);
        transactAndReadException.recycle();
        return zzanw;
    }

    public final zzxb getVideoController() throws RemoteException {
        Parcel transactAndReadException = transactAndReadException(5, obtainAndWriteInterfaceToken());
        zzxb zzj = zzxe.zzj(transactAndReadException.readStrongBinder());
        transactAndReadException.recycle();
        return zzj;
    }

    public final void zzy(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zza(10, obtainAndWriteInterfaceToken);
    }

    public final void zza(String[] strArr, Bundle[] bundleArr) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        obtainAndWriteInterfaceToken.writeStringArray(strArr);
        obtainAndWriteInterfaceToken.writeTypedArray(bundleArr, 0);
        zza(11, obtainAndWriteInterfaceToken);
    }

    public final void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzamw zzamw, zzali zzali, zzuj zzuj) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        obtainAndWriteInterfaceToken.writeString(str);
        obtainAndWriteInterfaceToken.writeString(str2);
        zzge.zza(obtainAndWriteInterfaceToken, zzug);
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, zzamw);
        zzge.zza(obtainAndWriteInterfaceToken, zzali);
        zzge.zza(obtainAndWriteInterfaceToken, zzuj);
        zza(13, obtainAndWriteInterfaceToken);
    }

    public final void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzamx zzamx, zzali zzali) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        obtainAndWriteInterfaceToken.writeString(str);
        obtainAndWriteInterfaceToken.writeString(str2);
        zzge.zza(obtainAndWriteInterfaceToken, zzug);
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, zzamx);
        zzge.zza(obtainAndWriteInterfaceToken, zzali);
        zza(14, obtainAndWriteInterfaceToken);
    }

    public final boolean zzz(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        Parcel transactAndReadException = transactAndReadException(15, obtainAndWriteInterfaceToken);
        boolean zza = zzge.zza(transactAndReadException);
        transactAndReadException.recycle();
        return zza;
    }

    public final void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzand zzand, zzali zzali) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        obtainAndWriteInterfaceToken.writeString(str);
        obtainAndWriteInterfaceToken.writeString(str2);
        zzge.zza(obtainAndWriteInterfaceToken, zzug);
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, zzand);
        zzge.zza(obtainAndWriteInterfaceToken, zzali);
        zza(16, obtainAndWriteInterfaceToken);
    }

    public final boolean zzaa(IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        Parcel transactAndReadException = transactAndReadException(17, obtainAndWriteInterfaceToken);
        boolean zza = zzge.zza(transactAndReadException);
        transactAndReadException.recycle();
        return zza;
    }

    public final void zza(String str, String str2, zzug zzug, IObjectWrapper iObjectWrapper, zzanc zzanc, zzali zzali) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        obtainAndWriteInterfaceToken.writeString(str);
        obtainAndWriteInterfaceToken.writeString(str2);
        zzge.zza(obtainAndWriteInterfaceToken, zzug);
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, zzanc);
        zzge.zza(obtainAndWriteInterfaceToken, zzali);
        zza(18, obtainAndWriteInterfaceToken);
    }

    public final void zzdm(String str) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        obtainAndWriteInterfaceToken.writeString(str);
        zza(19, obtainAndWriteInterfaceToken);
    }
}
