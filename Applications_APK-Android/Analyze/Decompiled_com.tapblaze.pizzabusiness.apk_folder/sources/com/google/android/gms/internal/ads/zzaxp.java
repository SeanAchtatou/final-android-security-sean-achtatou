package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaxp extends zzax {
    private final /* synthetic */ byte[] zzdui;
    private final /* synthetic */ Map zzduj;
    private final /* synthetic */ zzayo zzduk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzaxp(zzaxk zzaxk, int i, String str, zzab zzab, zzy zzy, byte[] bArr, Map map, zzayo zzayo) {
        super(i, str, zzab, zzy);
        this.zzdui = bArr;
        this.zzduj = map;
        this.zzduk = zzayo;
    }

    public final byte[] zzf() throws zzb {
        byte[] bArr = this.zzdui;
        return bArr == null ? super.zzf() : bArr;
    }

    public final Map<String, String> getHeaders() throws zzb {
        Map<String, String> map = this.zzduj;
        return map == null ? super.getHeaders() : map;
    }

    /* access modifiers changed from: protected */
    public final void zzh(String str) {
        this.zzduk.zzeu(str);
        super.zza(str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void zza(Object obj) {
        zza((String) obj);
    }
}
