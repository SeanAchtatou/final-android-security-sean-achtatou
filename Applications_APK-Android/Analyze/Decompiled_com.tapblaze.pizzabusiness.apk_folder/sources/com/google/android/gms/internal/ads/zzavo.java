package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzavo {
    /* access modifiers changed from: private */
    public volatile Thread thread;
    private final Runnable zzdro = new zzavr(this);
    private boolean zzdrp = false;

    public abstract void zztu();

    public final zzdhe<?> zzvr() {
        return zzazd.zzdwf.zzf(this.zzdro);
    }
}
