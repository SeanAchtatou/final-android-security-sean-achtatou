package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzard implements zzaqo {
    private zzaju<JSONObject, JSONObject> zzdnk;
    private zzaju<JSONObject, JSONObject> zzdnp;

    public zzard(Context context) {
        this.zzdnp = zzq.zzld().zza(context, zzazb.zzxm()).zza("google.afma.request.getAdDictionary", zzajx.zzdaq, zzajx.zzdaq);
        this.zzdnk = zzq.zzld().zza(context, zzazb.zzxm()).zza("google.afma.sdkConstants.getSdkConstants", zzajx.zzdaq, zzajx.zzdaq);
    }

    public final zzaju<JSONObject, JSONObject> zztz() {
        return this.zzdnk;
    }
}
