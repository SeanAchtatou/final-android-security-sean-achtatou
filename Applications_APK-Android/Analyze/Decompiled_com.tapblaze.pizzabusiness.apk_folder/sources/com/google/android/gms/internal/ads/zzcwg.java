package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcwg implements zzcub<zzcwh> {
    private final zzdhd zzfov;

    public zzcwg(zzdhd zzdhd) {
        this.zzfov = zzdhd;
    }

    public final zzdhe<zzcwh> zzanc() {
        return this.zzfov.zzd(zzcwj.zzgfx);
    }
}
