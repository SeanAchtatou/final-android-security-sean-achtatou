package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.ironsource.sdk.constants.Constants;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcsc implements zzcty<Bundle> {
    private final Bundle zzdjn;

    public zzcsc(Bundle bundle) {
        this.zzdjn = bundle;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        Bundle zza = zzdaa.zza(bundle, Constants.ParametersKeys.ORIENTATION_DEVICE);
        zza.putBundle("android_mem_info", this.zzdjn);
        bundle.putBundle(Constants.ParametersKeys.ORIENTATION_DEVICE, zza);
    }
}
