package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbpo implements zzbrn {
    static final zzbrn zzfhp = new zzbpo();

    private zzbpo() {
    }

    public final void zzp(Object obj) {
        ((zzbov) obj).onAdLeftApplication();
    }
}
