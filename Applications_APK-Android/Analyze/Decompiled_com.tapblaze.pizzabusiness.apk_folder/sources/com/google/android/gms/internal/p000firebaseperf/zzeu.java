package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzeu  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzeu {
    private static final zzes<?> zznp = new zzev();
    private static final zzes<?> zznq = zzgw();

    private static zzes<?> zzgw() {
        try {
            return (zzes) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    static zzes<?> zzgx() {
        return zznp;
    }

    static zzes<?> zzgy() {
        zzes<?> zzes = zznq;
        if (zzes != null) {
            return zzes;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
