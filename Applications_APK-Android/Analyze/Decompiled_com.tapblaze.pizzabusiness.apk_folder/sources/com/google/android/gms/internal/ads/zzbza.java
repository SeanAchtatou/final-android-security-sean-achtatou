package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbza implements zzdgf {
    private final Object zzfpo;

    zzbza(Object obj) {
        this.zzfpo = obj;
    }

    public final zzdhe zzf(Object obj) {
        Object obj2 = this.zzfpo;
        zzavs.zza("Error during loading assets.", (Exception) obj);
        return zzdgs.zzaj(obj2);
    }
}
