package com.google.android.gms.internal.ads;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import libcore.io.Memory;
import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzduy {
    private static final Logger logger = Logger.getLogger(zzduy.class.getName());
    private static final Unsafe zzgvq = zzbcl();
    private static final Class<?> zzhho = zzdqd.zzaxo();
    private static final boolean zzhiu = zzbcm();
    private static final boolean zzhrj = zzm(Long.TYPE);
    private static final boolean zzhrk = zzm(Integer.TYPE);
    private static final zzc zzhrl;
    private static final boolean zzhrm = zzbcn();
    static final long zzhrn = ((long) zzk(byte[].class));
    private static final long zzhro;
    private static final long zzhrp;
    private static final long zzhrq;
    private static final long zzhrr;
    private static final long zzhrs;
    private static final long zzhrt;
    private static final long zzhru;
    private static final long zzhrv;
    private static final long zzhrw;
    private static final long zzhrx;
    private static final long zzhry = ((long) zzk(Object[].class));
    private static final long zzhrz = ((long) zzl(Object[].class));
    private static final long zzhsa;
    private static final int zzhsb = ((int) (zzhrn & 7));
    static final boolean zzhsc = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);

    private zzduy() {
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static final class zza extends zzc {
        zza(Unsafe unsafe) {
            super(unsafe);
        }

        public final void zza(long j, byte b) {
            Memory.pokeByte(j, b);
        }

        public final byte zzy(Object obj, long j) {
            if (zzduy.zzhsc) {
                return zzduy.zzq(obj, j);
            }
            return zzduy.zzr(obj, j);
        }

        public final void zze(Object obj, long j, byte b) {
            if (zzduy.zzhsc) {
                zzduy.zza(obj, j, b);
            } else {
                zzduy.zzb(obj, j, b);
            }
        }

        public final boolean zzm(Object obj, long j) {
            if (zzduy.zzhsc) {
                return zzduy.zzs(obj, j);
            }
            return zzduy.zzt(obj, j);
        }

        public final void zza(Object obj, long j, boolean z) {
            if (zzduy.zzhsc) {
                zzduy.zzb(obj, j, z);
            } else {
                zzduy.zzc(obj, j, z);
            }
        }

        public final float zzn(Object obj, long j) {
            return Float.intBitsToFloat(zzk(obj, j));
        }

        public final void zza(Object obj, long j, float f) {
            zzb(obj, j, Float.floatToIntBits(f));
        }

        public final double zzo(Object obj, long j) {
            return Double.longBitsToDouble(zzl(obj, j));
        }

        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }

        public final void zza(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray(j2, bArr, (int) j, (int) j3);
        }
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static final class zzd extends zzc {
        zzd(Unsafe unsafe) {
            super(unsafe);
        }

        public final void zza(long j, byte b) {
            this.zzhsd.putByte(j, b);
        }

        public final byte zzy(Object obj, long j) {
            return this.zzhsd.getByte(obj, j);
        }

        public final void zze(Object obj, long j, byte b) {
            this.zzhsd.putByte(obj, j, b);
        }

        public final boolean zzm(Object obj, long j) {
            return this.zzhsd.getBoolean(obj, j);
        }

        public final void zza(Object obj, long j, boolean z) {
            this.zzhsd.putBoolean(obj, j, z);
        }

        public final float zzn(Object obj, long j) {
            return this.zzhsd.getFloat(obj, j);
        }

        public final void zza(Object obj, long j, float f) {
            this.zzhsd.putFloat(obj, j, f);
        }

        public final double zzo(Object obj, long j) {
            return this.zzhsd.getDouble(obj, j);
        }

        public final void zza(Object obj, long j, double d) {
            this.zzhsd.putDouble(obj, j, d);
        }

        public final void zza(byte[] bArr, long j, long j2, long j3) {
            this.zzhsd.copyMemory(bArr, zzduy.zzhrn + j, (Object) null, j2, j3);
        }
    }

    static boolean zzbcj() {
        return zzhiu;
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static abstract class zzc {
        Unsafe zzhsd;

        zzc(Unsafe unsafe) {
            this.zzhsd = unsafe;
        }

        public abstract void zza(long j, byte b);

        public abstract void zza(Object obj, long j, double d);

        public abstract void zza(Object obj, long j, float f);

        public abstract void zza(Object obj, long j, boolean z);

        public abstract void zza(byte[] bArr, long j, long j2, long j3);

        public abstract void zze(Object obj, long j, byte b);

        public abstract boolean zzm(Object obj, long j);

        public abstract float zzn(Object obj, long j);

        public abstract double zzo(Object obj, long j);

        public abstract byte zzy(Object obj, long j);

        public final int zzk(Object obj, long j) {
            return this.zzhsd.getInt(obj, j);
        }

        public final void zzb(Object obj, long j, int i) {
            this.zzhsd.putInt(obj, j, i);
        }

        public final long zzl(Object obj, long j) {
            return this.zzhsd.getLong(obj, j);
        }

        public final void zza(Object obj, long j, long j2) {
            this.zzhsd.putLong(obj, j, j2);
        }
    }

    static boolean zzbck() {
        return zzhrm;
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static final class zzb extends zzc {
        zzb(Unsafe unsafe) {
            super(unsafe);
        }

        public final void zza(long j, byte b) {
            Memory.pokeByte((int) (j & -1), b);
        }

        public final byte zzy(Object obj, long j) {
            if (zzduy.zzhsc) {
                return zzduy.zzq(obj, j);
            }
            return zzduy.zzr(obj, j);
        }

        public final void zze(Object obj, long j, byte b) {
            if (zzduy.zzhsc) {
                zzduy.zza(obj, j, b);
            } else {
                zzduy.zzb(obj, j, b);
            }
        }

        public final boolean zzm(Object obj, long j) {
            if (zzduy.zzhsc) {
                return zzduy.zzs(obj, j);
            }
            return zzduy.zzt(obj, j);
        }

        public final void zza(Object obj, long j, boolean z) {
            if (zzduy.zzhsc) {
                zzduy.zzb(obj, j, z);
            } else {
                zzduy.zzc(obj, j, z);
            }
        }

        public final float zzn(Object obj, long j) {
            return Float.intBitsToFloat(zzk(obj, j));
        }

        public final void zza(Object obj, long j, float f) {
            zzb(obj, j, Float.floatToIntBits(f));
        }

        public final double zzo(Object obj, long j) {
            return Double.longBitsToDouble(zzl(obj, j));
        }

        public final void zza(Object obj, long j, double d) {
            zza(obj, j, Double.doubleToLongBits(d));
        }

        public final void zza(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray((int) (j2 & -1), bArr, (int) j, (int) j3);
        }
    }

    static <T> T zzj(Class<T> cls) {
        try {
            return zzgvq.allocateInstance(cls);
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        }
    }

    private static int zzk(Class<?> cls) {
        if (zzhiu) {
            return zzhrl.zzhsd.arrayBaseOffset(cls);
        }
        return -1;
    }

    private static int zzl(Class<?> cls) {
        if (zzhiu) {
            return zzhrl.zzhsd.arrayIndexScale(cls);
        }
        return -1;
    }

    static int zzk(Object obj, long j) {
        return zzhrl.zzk(obj, j);
    }

    static void zzb(Object obj, long j, int i) {
        zzhrl.zzb(obj, j, i);
    }

    static long zzl(Object obj, long j) {
        return zzhrl.zzl(obj, j);
    }

    static void zza(Object obj, long j, long j2) {
        zzhrl.zza(obj, j, j2);
    }

    static boolean zzm(Object obj, long j) {
        return zzhrl.zzm(obj, j);
    }

    static void zza(Object obj, long j, boolean z) {
        zzhrl.zza(obj, j, z);
    }

    static float zzn(Object obj, long j) {
        return zzhrl.zzn(obj, j);
    }

    static void zza(Object obj, long j, float f) {
        zzhrl.zza(obj, j, f);
    }

    static double zzo(Object obj, long j) {
        return zzhrl.zzo(obj, j);
    }

    static void zza(Object obj, long j, double d) {
        zzhrl.zza(obj, j, d);
    }

    static Object zzp(Object obj, long j) {
        return zzhrl.zzhsd.getObject(obj, j);
    }

    static void zza(Object obj, long j, Object obj2) {
        zzhrl.zzhsd.putObject(obj, j, obj2);
    }

    static byte zza(byte[] bArr, long j) {
        return zzhrl.zzy(bArr, zzhrn + j);
    }

    static void zza(byte[] bArr, long j, byte b) {
        zzhrl.zze(bArr, zzhrn + j, b);
    }

    static void zza(byte[] bArr, long j, long j2, long j3) {
        zzhrl.zza(bArr, j, j2, j3);
    }

    static void zza(long j, byte b) {
        zzhrl.zza(j, b);
    }

    static long zzn(ByteBuffer byteBuffer) {
        return zzhrl.zzl(byteBuffer, zzhsa);
    }

    static Unsafe zzbcl() {
        try {
            return (Unsafe) AccessController.doPrivileged(new zzdux());
        } catch (Throwable unused) {
            return null;
        }
    }

    private static boolean zzbcm() {
        Unsafe unsafe = zzgvq;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("arrayBaseOffset", Class.class);
            cls.getMethod("arrayIndexScale", Class.class);
            cls.getMethod("getInt", Object.class, Long.TYPE);
            cls.getMethod("putInt", Object.class, Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            cls.getMethod("putLong", Object.class, Long.TYPE, Long.TYPE);
            cls.getMethod("getObject", Object.class, Long.TYPE);
            cls.getMethod("putObject", Object.class, Long.TYPE, Object.class);
            if (zzdqd.zzaxn()) {
                return true;
            }
            cls.getMethod("getByte", Object.class, Long.TYPE);
            cls.getMethod("putByte", Object.class, Long.TYPE, Byte.TYPE);
            cls.getMethod("getBoolean", Object.class, Long.TYPE);
            cls.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE);
            cls.getMethod("getFloat", Object.class, Long.TYPE);
            cls.getMethod("putFloat", Object.class, Long.TYPE, Float.TYPE);
            cls.getMethod("getDouble", Object.class, Long.TYPE);
            cls.getMethod("putDouble", Object.class, Long.TYPE, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    private static boolean zzbcn() {
        Unsafe unsafe = zzgvq;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            if (zzbco() == null) {
                return false;
            }
            if (zzdqd.zzaxn()) {
                return true;
            }
            cls.getMethod("getByte", Long.TYPE);
            cls.getMethod("putByte", Long.TYPE, Byte.TYPE);
            cls.getMethod("getInt", Long.TYPE);
            cls.getMethod("putInt", Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Long.TYPE);
            cls.getMethod("putLong", Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Long.TYPE, Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger2.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    private static boolean zzm(Class<?> cls) {
        Class<byte[]> cls2 = byte[].class;
        if (!zzdqd.zzaxn()) {
            return false;
        }
        try {
            Class<?> cls3 = zzhho;
            cls3.getMethod("peekLong", cls, Boolean.TYPE);
            cls3.getMethod("pokeLong", cls, Long.TYPE, Boolean.TYPE);
            cls3.getMethod("pokeInt", cls, Integer.TYPE, Boolean.TYPE);
            cls3.getMethod("peekInt", cls, Boolean.TYPE);
            cls3.getMethod("pokeByte", cls, Byte.TYPE);
            cls3.getMethod("peekByte", cls);
            cls3.getMethod("pokeByteArray", cls, cls2, Integer.TYPE, Integer.TYPE);
            cls3.getMethod("peekByteArray", cls, cls2, Integer.TYPE, Integer.TYPE);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    private static Field zzbco() {
        Field zzb2;
        if (zzdqd.zzaxn() && (zzb2 = zzb(Buffer.class, "effectiveDirectAddress")) != null) {
            return zzb2;
        }
        Field zzb3 = zzb(Buffer.class, "address");
        if (zzb3 == null || zzb3.getType() != Long.TYPE) {
            return null;
        }
        return zzb3;
    }

    private static Field zzb(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static byte zzq(Object obj, long j) {
        return (byte) (zzk(obj, -4 & j) >>> ((int) (((j ^ -1) & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static byte zzr(Object obj, long j) {
        return (byte) (zzk(obj, -4 & j) >>> ((int) ((j & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static void zza(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int i = ((((int) j) ^ -1) & 3) << 3;
        zzb(obj, j2, ((255 & b) << i) | (zzk(obj, j2) & ((255 << i) ^ -1)));
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int i = (((int) j) & 3) << 3;
        zzb(obj, j2, ((255 & b) << i) | (zzk(obj, j2) & ((255 << i) ^ -1)));
    }

    /* access modifiers changed from: private */
    public static boolean zzs(Object obj, long j) {
        return zzq(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    public static boolean zzt(Object obj, long j) {
        return zzr(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    public static void zzb(Object obj, long j, boolean z) {
        zza(obj, j, z ? (byte) 1 : 0);
    }

    /* access modifiers changed from: private */
    public static void zzc(Object obj, long j, boolean z) {
        zzb(obj, j, z ? (byte) 1 : 0);
    }

    static {
        zzc zzc2;
        Class<double[]> cls = double[].class;
        Class<float[]> cls2 = float[].class;
        Class<long[]> cls3 = long[].class;
        Class<int[]> cls4 = int[].class;
        Class<boolean[]> cls5 = boolean[].class;
        zzc zzc3 = null;
        if (zzgvq != null) {
            if (!zzdqd.zzaxn()) {
                zzc3 = new zzd(zzgvq);
            } else if (zzhrj) {
                zzc3 = new zza(zzgvq);
            } else if (zzhrk) {
                zzc3 = new zzb(zzgvq);
            }
        }
        zzhrl = zzc3;
        zzhro = (long) zzk(cls5);
        zzhrp = (long) zzl(cls5);
        zzhrq = (long) zzk(cls4);
        zzhrr = (long) zzl(cls4);
        zzhrs = (long) zzk(cls3);
        zzhrt = (long) zzl(cls3);
        zzhru = (long) zzk(cls2);
        zzhrv = (long) zzl(cls2);
        zzhrw = (long) zzk(cls);
        zzhrx = (long) zzl(cls);
        Field zzbco = zzbco();
        zzhsa = (zzbco == null || (zzc2 = zzhrl) == null) ? -1 : zzc2.zzhsd.objectFieldOffset(zzbco);
    }
}
