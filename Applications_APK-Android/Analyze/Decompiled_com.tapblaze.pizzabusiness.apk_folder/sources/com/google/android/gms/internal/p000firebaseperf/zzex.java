package com.google.android.gms.internal.p000firebaseperf;

import com.google.android.gms.internal.p000firebaseperf.zzez;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzex  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzex<T extends zzez<T>> {
    private static final zzex zznw = new zzex(true);
    final zzhg<T, Object> zznt;
    private boolean zznu;
    private boolean zznv;

    private zzex() {
        this.zznt = zzhg.zzat(16);
    }

    private zzex(boolean z) {
        this(zzhg.zzat(0));
        zzgg();
    }

    private zzex(zzhg<T, Object> zzhg) {
        this.zznt = zzhg;
        zzgg();
    }

    public static <T extends zzez<T>> zzex<T> zzgz() {
        return zznw;
    }

    public final void zzgg() {
        if (!this.zznu) {
            this.zznt.zzgg();
            this.zznu = true;
        }
    }

    public final boolean isImmutable() {
        return this.zznu;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzex)) {
            return false;
        }
        return this.zznt.equals(((zzex) obj).zznt);
    }

    public final int hashCode() {
        return this.zznt.hashCode();
    }

    public final Iterator<Map.Entry<T, Object>> iterator() {
        if (this.zznv) {
            return new zzfr(this.zznt.entrySet().iterator());
        }
        return this.zznt.entrySet().iterator();
    }

    /* access modifiers changed from: package-private */
    public final Iterator<Map.Entry<T, Object>> descendingIterator() {
        if (this.zznv) {
            return new zzfr(this.zznt.zziy().iterator());
        }
        return this.zznt.zziy().iterator();
    }

    private final Object zza(T t) {
        Object obj = this.zznt.get(t);
        if (!(obj instanceof zzfq)) {
            return obj;
        }
        zzfq zzfq = (zzfq) obj;
        return zzfq.zzht();
    }

    private final void zza(T t, Object obj) {
        if (!t.zzhd()) {
            zza(t.zzhb(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList2.get(i);
                i++;
                zza(t.zzhb(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof zzfq) {
            this.zznv = true;
        }
        this.zznt.put(t, obj);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        if ((r3 instanceof com.google.android.gms.internal.p000firebaseperf.zzff) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001c, code lost:
        if ((r3 instanceof com.google.android.gms.internal.p000firebaseperf.zzfq) == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void zza(com.google.android.gms.internal.p000firebaseperf.zzih r2, java.lang.Object r3) {
        /*
            com.google.android.gms.internal.p000firebaseperf.zzfg.checkNotNull(r3)
            int[] r0 = com.google.android.gms.internal.p000firebaseperf.zzew.zznr
            com.google.android.gms.internal.firebase-perf.zzio r2 = r2.zzjm()
            int r2 = r2.ordinal()
            r2 = r0[r2]
            r0 = 1
            r1 = 0
            switch(r2) {
                case 1: goto L_0x0040;
                case 2: goto L_0x003d;
                case 3: goto L_0x003a;
                case 4: goto L_0x0037;
                case 5: goto L_0x0034;
                case 6: goto L_0x0031;
                case 7: goto L_0x0028;
                case 8: goto L_0x001f;
                case 9: goto L_0x0016;
                default: goto L_0x0014;
            }
        L_0x0014:
            r0 = 0
            goto L_0x0042
        L_0x0016:
            boolean r2 = r3 instanceof com.google.android.gms.internal.p000firebaseperf.zzgl
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.google.android.gms.internal.p000firebaseperf.zzfq
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x001f:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.google.android.gms.internal.p000firebaseperf.zzff
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0028:
            boolean r2 = r3 instanceof com.google.android.gms.internal.p000firebaseperf.zzeb
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0031:
            boolean r0 = r3 instanceof java.lang.String
            goto L_0x0042
        L_0x0034:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L_0x0042
        L_0x0037:
            boolean r0 = r3 instanceof java.lang.Double
            goto L_0x0042
        L_0x003a:
            boolean r0 = r3 instanceof java.lang.Float
            goto L_0x0042
        L_0x003d:
            boolean r0 = r3 instanceof java.lang.Long
            goto L_0x0042
        L_0x0040:
            boolean r0 = r3 instanceof java.lang.Integer
        L_0x0042:
            if (r0 == 0) goto L_0x0045
            return
        L_0x0045:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            goto L_0x004e
        L_0x004d:
            throw r2
        L_0x004e:
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.p000firebaseperf.zzex.zza(com.google.android.gms.internal.firebase-perf.zzih, java.lang.Object):void");
    }

    public final boolean isInitialized() {
        for (int i = 0; i < this.zznt.zziw(); i++) {
            if (!zzc(this.zznt.zzau(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> zzc : this.zznt.zzix()) {
            if (!zzc(zzc)) {
                return false;
            }
        }
        return true;
    }

    private static <T extends zzez<T>> boolean zzc(Map.Entry<T, Object> entry) {
        zzez zzez = (zzez) entry.getKey();
        if (zzez.zzhc() == zzio.MESSAGE) {
            if (zzez.zzhd()) {
                for (zzgl isInitialized : (List) entry.getValue()) {
                    if (!isInitialized.isInitialized()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof zzgl) {
                    if (!((zzgl) value).isInitialized()) {
                        return false;
                    }
                } else if (value instanceof zzfq) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    public final void zza(zzex<T> zzex) {
        for (int i = 0; i < zzex.zznt.zziw(); i++) {
            zzd(zzex.zznt.zzau(i));
        }
        for (Map.Entry<T, Object> zzd : zzex.zznt.zzix()) {
            zzd(zzd);
        }
    }

    private static Object zzg(Object obj) {
        if (obj instanceof zzgr) {
            return ((zzgr) obj).zzgd();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    private final void zzd(Map.Entry<T, Object> entry) {
        Object obj;
        zzez zzez = (zzez) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof zzfq) {
            zzfq zzfq = (zzfq) value;
            value = zzfq.zzht();
        }
        if (zzez.zzhd()) {
            Object zza = zza(zzez);
            if (zza == null) {
                zza = new ArrayList();
            }
            for (Object zzg : (List) value) {
                ((List) zza).add(zzg(zzg));
            }
            this.zznt.put(zzez, zza);
        } else if (zzez.zzhc() == zzio.MESSAGE) {
            Object zza2 = zza(zzez);
            if (zza2 == null) {
                this.zznt.put(zzez, zzg(value));
                return;
            }
            if (zza2 instanceof zzgr) {
                obj = zzez.zza((zzgr) zza2, (zzgr) value);
            } else {
                obj = zzez.zza(((zzgl) zza2).zzhi(), (zzgl) value).zzhp();
            }
            this.zznt.put(zzez, obj);
        } else {
            this.zznt.put(zzez, zzg(value));
        }
    }

    static void zza(zzeo zzeo, zzih zzih, int i, Object obj) throws IOException {
        if (zzih == zzih.GROUP) {
            zzgl zzgl = (zzgl) obj;
            zzfg.zzf(zzgl);
            zzeo.zze(i, 3);
            zzgl.writeTo(zzeo);
            zzeo.zze(i, 4);
            return;
        }
        zzeo.zze(i, zzih.zzjn());
        switch (zzew.zzns[zzih.ordinal()]) {
            case 1:
                zzeo.zza(((Double) obj).doubleValue());
                return;
            case 2:
                zzeo.zzb(((Float) obj).floatValue());
                return;
            case 3:
                zzeo.zzaq(((Long) obj).longValue());
                return;
            case 4:
                zzeo.zzaq(((Long) obj).longValue());
                return;
            case 5:
                zzeo.zzu(((Integer) obj).intValue());
                return;
            case 6:
                zzeo.zzas(((Long) obj).longValue());
                return;
            case 7:
                zzeo.zzx(((Integer) obj).intValue());
                return;
            case 8:
                zzeo.zzf(((Boolean) obj).booleanValue());
                return;
            case 9:
                ((zzgl) obj).writeTo(zzeo);
                return;
            case 10:
                zzeo.zzb((zzgl) obj);
                return;
            case 11:
                if (obj instanceof zzeb) {
                    zzeo.zza((zzeb) obj);
                    return;
                } else {
                    zzeo.zzak((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof zzeb) {
                    zzeo.zza((zzeb) obj);
                    return;
                }
                byte[] bArr = (byte[]) obj;
                zzeo.zzb(bArr, 0, bArr.length);
                return;
            case 13:
                zzeo.zzv(((Integer) obj).intValue());
                return;
            case 14:
                zzeo.zzx(((Integer) obj).intValue());
                return;
            case 15:
                zzeo.zzas(((Long) obj).longValue());
                return;
            case 16:
                zzeo.zzw(((Integer) obj).intValue());
                return;
            case 17:
                zzeo.zzar(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof zzff) {
                    zzeo.zzu(((zzff) obj).getNumber());
                    return;
                } else {
                    zzeo.zzu(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    public final int zzha() {
        int i = 0;
        for (int i2 = 0; i2 < this.zznt.zziw(); i2++) {
            i += zze(this.zznt.zzau(i2));
        }
        for (Map.Entry<T, Object> zze : this.zznt.zzix()) {
            i += zze(zze);
        }
        return i;
    }

    private static int zze(Map.Entry<T, Object> entry) {
        zzez zzez = (zzez) entry.getKey();
        Object value = entry.getValue();
        if (zzez.zzhc() != zzio.MESSAGE || zzez.zzhd() || zzez.zzhe()) {
            return zzb(zzez, value);
        }
        if (value instanceof zzfq) {
            return zzeo.zzb(((zzez) entry.getKey()).getNumber(), (zzfq) value);
        }
        return zzeo.zzb(((zzez) entry.getKey()).getNumber(), (zzgl) value);
    }

    static int zza(zzih zzih, int i, Object obj) {
        int zzy = zzeo.zzy(i);
        if (zzih == zzih.GROUP) {
            zzfg.zzf((zzgl) obj);
            zzy <<= 1;
        }
        return zzy + zzb(zzih, obj);
    }

    private static int zzb(zzih zzih, Object obj) {
        switch (zzew.zzns[zzih.ordinal()]) {
            case 1:
                return zzeo.zzb(((Double) obj).doubleValue());
            case 2:
                return zzeo.zzc(((Float) obj).floatValue());
            case 3:
                return zzeo.zzat(((Long) obj).longValue());
            case 4:
                return zzeo.zzau(((Long) obj).longValue());
            case 5:
                return zzeo.zzz(((Integer) obj).intValue());
            case 6:
                return zzeo.zzaw(((Long) obj).longValue());
            case 7:
                return zzeo.zzac(((Integer) obj).intValue());
            case 8:
                return zzeo.zzg(((Boolean) obj).booleanValue());
            case 9:
                return zzeo.zzd((zzgl) obj);
            case 10:
                if (obj instanceof zzfq) {
                    return zzeo.zza((zzfq) obj);
                }
                return zzeo.zzc((zzgl) obj);
            case 11:
                if (obj instanceof zzeb) {
                    return zzeo.zzb((zzeb) obj);
                }
                return zzeo.zzal((String) obj);
            case 12:
                if (obj instanceof zzeb) {
                    return zzeo.zzb((zzeb) obj);
                }
                return zzeo.zzb((byte[]) obj);
            case 13:
                return zzeo.zzaa(((Integer) obj).intValue());
            case 14:
                return zzeo.zzad(((Integer) obj).intValue());
            case 15:
                return zzeo.zzax(((Long) obj).longValue());
            case 16:
                return zzeo.zzab(((Integer) obj).intValue());
            case 17:
                return zzeo.zzav(((Long) obj).longValue());
            case 18:
                if (obj instanceof zzff) {
                    return zzeo.zzae(((zzff) obj).getNumber());
                }
                return zzeo.zzae(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int zzb(zzez<?> zzez, Object obj) {
        zzih zzhb = zzez.zzhb();
        int number = zzez.getNumber();
        if (!zzez.zzhd()) {
            return zza(zzhb, number, obj);
        }
        int i = 0;
        if (zzez.zzhe()) {
            for (Object zzb : (List) obj) {
                i += zzb(zzhb, zzb);
            }
            return zzeo.zzy(number) + i + zzeo.zzah(i);
        }
        for (Object zza : (List) obj) {
            i += zza(zzhb, number, zza);
        }
        return i;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzex zzex = new zzex();
        for (int i = 0; i < this.zznt.zziw(); i++) {
            Map.Entry<T, Object> zzau = this.zznt.zzau(i);
            zzex.zza((zzez) zzau.getKey(), zzau.getValue());
        }
        for (Map.Entry next : this.zznt.zzix()) {
            zzex.zza((zzez) next.getKey(), next.getValue());
        }
        zzex.zznv = this.zznv;
        return zzex;
    }
}
