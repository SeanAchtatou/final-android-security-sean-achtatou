package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbdq implements zzdgf {
    private final Context zzcri;
    private final zzdq zzczj;
    private final zzazb zzeez;
    private final zza zzefa;
    private final String zzefb;

    zzbdq(Context context, zzdq zzdq, zzazb zzazb, zza zza, String str) {
        this.zzcri = context;
        this.zzczj = zzdq;
        this.zzeez = zzazb;
        this.zzefa = zza;
        this.zzefb = str;
    }

    public final zzdhe zzf(Object obj) {
        Context context = this.zzcri;
        zzdq zzdq = this.zzczj;
        zzazb zzazb = this.zzeez;
        zza zza = this.zzefa;
        String str = this.zzefb;
        zzq.zzkr();
        zzbdi zza2 = zzbdr.zza(context, zzbey.zzabq(), "", false, false, zzdq, zzazb, null, null, zza, zzsm.zzmt(), null, false);
        zzazi zzl = zzazi.zzl(zza2);
        zza2.zzaaa().zza(new zzbds(zzl));
        zza2.loadUrl(str);
        return zzl;
    }
}
