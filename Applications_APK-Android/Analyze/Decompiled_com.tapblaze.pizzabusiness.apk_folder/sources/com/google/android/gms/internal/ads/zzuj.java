package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.zzb;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzuj extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzuj> CREATOR = new zzum();
    public final int height;
    public final int heightPixels;
    public final int width;
    public final int widthPixels;
    public final String zzabg;
    public final boolean zzbmc;
    public final boolean zzcco;
    public final zzuj[] zzccp;
    public final boolean zzccq;
    public boolean zzccr;
    public boolean zzccs;
    private boolean zzcct;
    public boolean zzccu;
    public boolean zzccv;

    public static int zzb(DisplayMetrics displayMetrics) {
        return displayMetrics.widthPixels;
    }

    public static int zzc(DisplayMetrics displayMetrics) {
        return (int) (((float) zzd(displayMetrics)) * displayMetrics.density);
    }

    private static int zzd(DisplayMetrics displayMetrics) {
        int i = (int) (((float) displayMetrics.heightPixels) / displayMetrics.density);
        if (i <= 400) {
            return 32;
        }
        return i <= 720 ? 50 : 90;
    }

    public static zzuj zzg(Context context) {
        return new zzuj("320x50_mb", 0, 0, false, 0, 0, null, true, false, false, false, false, false, false);
    }

    public static zzuj zzol() {
        return new zzuj("reward_mb", 0, 0, true, 0, 0, null, false, false, false, false, false, false, false);
    }

    public static zzuj zzom() {
        return new zzuj("interstitial_mb", 0, 0, false, 0, 0, null, false, false, false, false, true, false, false);
    }

    public zzuj() {
        this("interstitial_mb", 0, 0, true, 0, 0, null, false, false, false, false, false, false, false);
    }

    public static zzuj zzon() {
        return new zzuj("invalid", 0, 0, false, 0, 0, null, false, false, false, true, false, false, false);
    }

    public zzuj(Context context, AdSize adSize) {
        this(context, new AdSize[]{adSize});
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzuj(android.content.Context r13, com.google.android.gms.ads.AdSize[] r14) {
        /*
            r12 = this;
            r12.<init>()
            r0 = 0
            r1 = r14[r0]
            r12.zzcco = r0
            boolean r2 = r1.isFluid()
            r12.zzccq = r2
            boolean r2 = com.google.android.gms.ads.zzb.zza(r1)
            r12.zzccu = r2
            boolean r2 = com.google.android.gms.ads.zzb.zzb(r1)
            r12.zzccv = r2
            boolean r2 = r12.zzccq
            if (r2 == 0) goto L_0x002f
            com.google.android.gms.ads.AdSize r2 = com.google.android.gms.ads.AdSize.BANNER
            int r2 = r2.getWidth()
            r12.width = r2
            com.google.android.gms.ads.AdSize r2 = com.google.android.gms.ads.AdSize.BANNER
            int r2 = r2.getHeight()
            r12.height = r2
            goto L_0x004c
        L_0x002f:
            boolean r2 = r12.zzccv
            if (r2 == 0) goto L_0x0040
            int r2 = r1.getWidth()
            r12.width = r2
            int r2 = com.google.android.gms.ads.zzb.zzc(r1)
            r12.height = r2
            goto L_0x004c
        L_0x0040:
            int r2 = r1.getWidth()
            r12.width = r2
            int r2 = r1.getHeight()
            r12.height = r2
        L_0x004c:
            int r2 = r12.width
            r3 = -1
            r4 = 1
            if (r2 != r3) goto L_0x0054
            r2 = 1
            goto L_0x0055
        L_0x0054:
            r2 = 0
        L_0x0055:
            int r3 = r12.height
            r5 = -2
            if (r3 != r5) goto L_0x005c
            r3 = 1
            goto L_0x005d
        L_0x005c:
            r3 = 0
        L_0x005d:
            android.content.res.Resources r5 = r13.getResources()
            android.util.DisplayMetrics r5 = r5.getDisplayMetrics()
            if (r2 == 0) goto L_0x00a6
            com.google.android.gms.internal.ads.zzve.zzou()
            boolean r6 = com.google.android.gms.internal.ads.zzayk.zzbl(r13)
            if (r6 == 0) goto L_0x0086
            com.google.android.gms.internal.ads.zzve.zzou()
            boolean r6 = com.google.android.gms.internal.ads.zzayk.zzbm(r13)
            if (r6 == 0) goto L_0x0086
            int r6 = r5.widthPixels
            com.google.android.gms.internal.ads.zzve.zzou()
            int r7 = com.google.android.gms.internal.ads.zzayk.zzbn(r13)
            int r6 = r6 - r7
            r12.widthPixels = r6
            goto L_0x008a
        L_0x0086:
            int r6 = r5.widthPixels
            r12.widthPixels = r6
        L_0x008a:
            int r6 = r12.widthPixels
            float r6 = (float) r6
            float r7 = r5.density
            float r6 = r6 / r7
            double r6 = (double) r6
            int r8 = (int) r6
            double r9 = (double) r8
            java.lang.Double.isNaN(r6)
            java.lang.Double.isNaN(r9)
            double r6 = r6 - r9
            r9 = 4576918229304087675(0x3f847ae147ae147b, double:0.01)
            int r11 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r11 < 0) goto L_0x00b3
            int r8 = r8 + 1
            goto L_0x00b3
        L_0x00a6:
            int r8 = r12.width
            com.google.android.gms.internal.ads.zzve.zzou()
            int r6 = r12.width
            int r6 = com.google.android.gms.internal.ads.zzayk.zza(r5, r6)
            r12.widthPixels = r6
        L_0x00b3:
            if (r3 == 0) goto L_0x00ba
            int r6 = zzd(r5)
            goto L_0x00bc
        L_0x00ba:
            int r6 = r12.height
        L_0x00bc:
            com.google.android.gms.internal.ads.zzve.zzou()
            int r5 = com.google.android.gms.internal.ads.zzayk.zza(r5, r6)
            r12.heightPixels = r5
            java.lang.String r5 = "_as"
            java.lang.String r7 = "x"
            r9 = 26
            if (r2 != 0) goto L_0x0100
            if (r3 == 0) goto L_0x00d0
            goto L_0x0100
        L_0x00d0:
            boolean r2 = r12.zzccv
            if (r2 == 0) goto L_0x00f0
            int r1 = r12.width
            int r2 = r12.height
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r9)
            r3.append(r1)
            r3.append(r7)
            r3.append(r2)
            r3.append(r5)
            java.lang.String r1 = r3.toString()
            r12.zzabg = r1
            goto L_0x0117
        L_0x00f0:
            boolean r2 = r12.zzccq
            if (r2 == 0) goto L_0x00f9
            java.lang.String r1 = "320x50_mb"
            r12.zzabg = r1
            goto L_0x0117
        L_0x00f9:
            java.lang.String r1 = r1.toString()
            r12.zzabg = r1
            goto L_0x0117
        L_0x0100:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r9)
            r1.append(r8)
            r1.append(r7)
            r1.append(r6)
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            r12.zzabg = r1
        L_0x0117:
            int r1 = r14.length
            if (r1 <= r4) goto L_0x0131
            int r1 = r14.length
            com.google.android.gms.internal.ads.zzuj[] r1 = new com.google.android.gms.internal.ads.zzuj[r1]
            r12.zzccp = r1
            r1 = 0
        L_0x0120:
            int r2 = r14.length
            if (r1 >= r2) goto L_0x0134
            com.google.android.gms.internal.ads.zzuj[] r2 = r12.zzccp
            com.google.android.gms.internal.ads.zzuj r3 = new com.google.android.gms.internal.ads.zzuj
            r4 = r14[r1]
            r3.<init>(r13, r4)
            r2[r1] = r3
            int r1 = r1 + 1
            goto L_0x0120
        L_0x0131:
            r13 = 0
            r12.zzccp = r13
        L_0x0134:
            r12.zzbmc = r0
            r12.zzccr = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzuj.<init>(android.content.Context, com.google.android.gms.ads.AdSize[]):void");
    }

    zzuj(String str, int i, int i2, boolean z, int i3, int i4, zzuj[] zzujArr, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8) {
        this.zzabg = str;
        this.height = i;
        this.heightPixels = i2;
        this.zzcco = z;
        this.width = i3;
        this.widthPixels = i4;
        this.zzccp = zzujArr;
        this.zzbmc = z2;
        this.zzccq = z3;
        this.zzccr = z4;
        this.zzccs = z5;
        this.zzcct = z6;
        this.zzccu = z7;
        this.zzccv = z8;
    }

    public final AdSize zzoo() {
        return zzb.zza(this.width, this.height, this.zzabg);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.zzabg, false);
        SafeParcelWriter.writeInt(parcel, 3, this.height);
        SafeParcelWriter.writeInt(parcel, 4, this.heightPixels);
        SafeParcelWriter.writeBoolean(parcel, 5, this.zzcco);
        SafeParcelWriter.writeInt(parcel, 6, this.width);
        SafeParcelWriter.writeInt(parcel, 7, this.widthPixels);
        SafeParcelWriter.writeTypedArray(parcel, 8, this.zzccp, i, false);
        SafeParcelWriter.writeBoolean(parcel, 9, this.zzbmc);
        SafeParcelWriter.writeBoolean(parcel, 10, this.zzccq);
        SafeParcelWriter.writeBoolean(parcel, 11, this.zzccr);
        SafeParcelWriter.writeBoolean(parcel, 12, this.zzccs);
        SafeParcelWriter.writeBoolean(parcel, 13, this.zzcct);
        SafeParcelWriter.writeBoolean(parcel, 14, this.zzccu);
        SafeParcelWriter.writeBoolean(parcel, 15, this.zzccv);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
