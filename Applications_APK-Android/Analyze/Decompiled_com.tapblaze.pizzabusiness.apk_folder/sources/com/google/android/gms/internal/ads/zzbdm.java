package com.google.android.gms.internal.ads;

import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbdm implements Runnable {
    private final /* synthetic */ View val$view;
    private final /* synthetic */ zzato zzeev;
    private final /* synthetic */ int zzeew;
    private final /* synthetic */ zzbdl zzeex;

    zzbdm(zzbdl zzbdl, View view, zzato zzato, int i) {
        this.zzeex = zzbdl;
        this.val$view = view;
        this.zzeev = zzato;
        this.zzeew = i;
    }

    public final void run() {
        this.zzeex.zza(this.val$view, this.zzeev, this.zzeew - 1);
    }
}
