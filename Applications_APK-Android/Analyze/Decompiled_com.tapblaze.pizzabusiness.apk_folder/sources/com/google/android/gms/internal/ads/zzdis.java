package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzdis<P> {
    P zza(zzdiq<P> zzdiq) throws GeneralSecurityException;

    Class<P> zzarz();
}
