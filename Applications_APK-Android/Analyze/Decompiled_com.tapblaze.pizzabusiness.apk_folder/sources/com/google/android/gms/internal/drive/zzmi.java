package com.google.android.gms.internal.drive;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class zzmi<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    private boolean zzot;
    private final int zzvd;
    /* access modifiers changed from: private */
    public List<zzmp> zzve;
    /* access modifiers changed from: private */
    public Map<K, V> zzvf;
    private volatile zzmr zzvg;
    /* access modifiers changed from: private */
    public Map<K, V> zzvh;
    private volatile zzml zzvi;

    static <FieldDescriptorType extends zzkd<FieldDescriptorType>> zzmi<FieldDescriptorType, Object> zzav(int i) {
        return new zzmj(i);
    }

    private zzmi(int i) {
        this.zzvd = i;
        this.zzve = Collections.emptyList();
        this.zzvf = Collections.emptyMap();
        this.zzvh = Collections.emptyMap();
    }

    public void zzbp() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.zzot) {
            if (this.zzvf.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.zzvf);
            }
            this.zzvf = map;
            if (this.zzvh.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.zzvh);
            }
            this.zzvh = map2;
            this.zzot = true;
        }
    }

    public final boolean isImmutable() {
        return this.zzot;
    }

    public final int zzer() {
        return this.zzve.size();
    }

    public final Map.Entry<K, V> zzaw(int i) {
        return this.zzve.get(i);
    }

    public final Iterable<Map.Entry<K, V>> zzes() {
        if (this.zzvf.isEmpty()) {
            return zzmm.zzex();
        }
        return this.zzvf.entrySet();
    }

    public int size() {
        return this.zzve.size() + this.zzvf.size();
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return zza(comparable) >= 0 || this.zzvf.containsKey(comparable);
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzve.get(zza).getValue();
        }
        return this.zzvf.get(comparable);
    }

    /* renamed from: zza */
    public final V put(Comparable comparable, Object obj) {
        zzeu();
        int zza = zza(comparable);
        if (zza >= 0) {
            return this.zzve.get(zza).setValue(obj);
        }
        zzeu();
        if (this.zzve.isEmpty() && !(this.zzve instanceof ArrayList)) {
            this.zzve = new ArrayList(this.zzvd);
        }
        int i = -(zza + 1);
        if (i >= this.zzvd) {
            return zzev().put(comparable, obj);
        }
        int size = this.zzve.size();
        int i2 = this.zzvd;
        if (size == i2) {
            zzmp remove = this.zzve.remove(i2 - 1);
            zzev().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.zzve.add(i, new zzmp(this, comparable, obj));
        return null;
    }

    public void clear() {
        zzeu();
        if (!this.zzve.isEmpty()) {
            this.zzve.clear();
        }
        if (!this.zzvf.isEmpty()) {
            this.zzvf.clear();
        }
    }

    public V remove(Object obj) {
        zzeu();
        Comparable comparable = (Comparable) obj;
        int zza = zza(comparable);
        if (zza >= 0) {
            return zzax(zza);
        }
        if (this.zzvf.isEmpty()) {
            return null;
        }
        return this.zzvf.remove(comparable);
    }

    /* access modifiers changed from: private */
    public final V zzax(int i) {
        zzeu();
        V value = this.zzve.remove(i).getValue();
        if (!this.zzvf.isEmpty()) {
            Iterator it = zzev().entrySet().iterator();
            this.zzve.add(new zzmp(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final int zza(K r5) {
        /*
            r4 = this;
            java.util.List<com.google.android.gms.internal.drive.zzmp> r0 = r4.zzve
            int r0 = r0.size()
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x0025
            java.util.List<com.google.android.gms.internal.drive.zzmp> r1 = r4.zzve
            java.lang.Object r1 = r1.get(r0)
            com.google.android.gms.internal.drive.zzmp r1 = (com.google.android.gms.internal.drive.zzmp) r1
            java.lang.Object r1 = r1.getKey()
            java.lang.Comparable r1 = (java.lang.Comparable) r1
            int r1 = r5.compareTo(r1)
            if (r1 <= 0) goto L_0x0022
            int r0 = r0 + 2
            int r5 = -r0
            return r5
        L_0x0022:
            if (r1 != 0) goto L_0x0025
            return r0
        L_0x0025:
            r1 = 0
        L_0x0026:
            if (r1 > r0) goto L_0x0049
            int r2 = r1 + r0
            int r2 = r2 / 2
            java.util.List<com.google.android.gms.internal.drive.zzmp> r3 = r4.zzve
            java.lang.Object r3 = r3.get(r2)
            com.google.android.gms.internal.drive.zzmp r3 = (com.google.android.gms.internal.drive.zzmp) r3
            java.lang.Object r3 = r3.getKey()
            java.lang.Comparable r3 = (java.lang.Comparable) r3
            int r3 = r5.compareTo(r3)
            if (r3 >= 0) goto L_0x0043
            int r0 = r2 + -1
            goto L_0x0026
        L_0x0043:
            if (r3 <= 0) goto L_0x0048
            int r1 = r2 + 1
            goto L_0x0026
        L_0x0048:
            return r2
        L_0x0049:
            int r1 = r1 + 1
            int r5 = -r1
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.drive.zzmi.zza(java.lang.Comparable):int");
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.zzvg == null) {
            this.zzvg = new zzmr(this, null);
        }
        return this.zzvg;
    }

    /* access modifiers changed from: package-private */
    public final Set<Map.Entry<K, V>> zzet() {
        if (this.zzvi == null) {
            this.zzvi = new zzml(this, null);
        }
        return this.zzvi;
    }

    /* access modifiers changed from: private */
    public final void zzeu() {
        if (this.zzot) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> zzev() {
        zzeu();
        if (this.zzvf.isEmpty() && !(this.zzvf instanceof TreeMap)) {
            this.zzvf = new TreeMap();
            this.zzvh = ((TreeMap) this.zzvf).descendingMap();
        }
        return (SortedMap) this.zzvf;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzmi)) {
            return super.equals(obj);
        }
        zzmi zzmi = (zzmi) obj;
        int size = size();
        if (size != zzmi.size()) {
            return false;
        }
        int zzer = zzer();
        if (zzer != zzmi.zzer()) {
            return entrySet().equals(zzmi.entrySet());
        }
        for (int i = 0; i < zzer; i++) {
            if (!zzaw(i).equals(zzmi.zzaw(i))) {
                return false;
            }
        }
        if (zzer != size) {
            return this.zzvf.equals(zzmi.zzvf);
        }
        return true;
    }

    public int hashCode() {
        int zzer = zzer();
        int i = 0;
        for (int i2 = 0; i2 < zzer; i2++) {
            i += this.zzve.get(i2).hashCode();
        }
        return this.zzvf.size() > 0 ? i + this.zzvf.hashCode() : i;
    }

    /* synthetic */ zzmi(int i, zzmj zzmj) {
        this(i);
    }
}
