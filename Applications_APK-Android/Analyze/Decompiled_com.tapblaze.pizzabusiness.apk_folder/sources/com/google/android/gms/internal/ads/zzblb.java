package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzblb implements zzdxg<zzbsu<zzps>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzbmc> zzfdq;
    private final zzbkn zzfen;

    public zzblb(zzbkn zzbkn, zzdxp<zzbmc> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfen = zzbkn;
        this.zzfdq = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdq.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
