package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzhq implements Runnable {
    private final /* synthetic */ zzit zzahe;
    private final /* synthetic */ zzhr zzahf;

    zzhq(zzhr zzhr, zzit zzit) {
        this.zzahf = zzhr;
        this.zzahe = zzit;
    }

    public final void run() {
        this.zzahf.zzahg.zza(this.zzahe);
    }
}
