package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzkm implements zzkn {
    private static final zzcn<Boolean> zza;
    private static final zzcn<Long> zzb;

    public final boolean zza() {
        return true;
    }

    public final boolean zzb() {
        return zza.zzc().booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, boolean):com.google.android.gms.internal.measurement.zzcn<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, double):com.google.android.gms.internal.measurement.zzcn<java.lang.Double>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, long):com.google.android.gms.internal.measurement.zzcn<java.lang.Long>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.zzcn<java.lang.String>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, boolean):com.google.android.gms.internal.measurement.zzcn<java.lang.Boolean> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, long):com.google.android.gms.internal.measurement.zzcn<java.lang.Long>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, double):com.google.android.gms.internal.measurement.zzcn<java.lang.Double>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.zzcn<java.lang.String>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, boolean):com.google.android.gms.internal.measurement.zzcn<java.lang.Boolean>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, long):com.google.android.gms.internal.measurement.zzcn<java.lang.Long> */
    static {
        zzct zzct = new zzct(zzck.zza("com.google.android.gms.measurement"));
        zza = zzct.zza("measurement.engagement_time_main_thread", false);
        zzb = zzct.zza("measurement.id.engagement_time_main_thread", 0L);
    }
}
