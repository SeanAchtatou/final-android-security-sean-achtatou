package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaen extends zzadn {
    private final NativeCustomTemplateAd.OnCustomClickListener zzcwi;

    public zzaen(NativeCustomTemplateAd.OnCustomClickListener onCustomClickListener) {
        this.zzcwi = onCustomClickListener;
    }

    public final void zza(zzade zzade, String str) {
        this.zzcwi.onCustomClick(zzadf.zza(zzade), str);
    }
}
