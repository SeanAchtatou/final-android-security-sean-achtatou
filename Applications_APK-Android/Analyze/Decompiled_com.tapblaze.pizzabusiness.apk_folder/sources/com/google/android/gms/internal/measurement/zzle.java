package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzle implements zzlf {
    private static final zzcn<Boolean> zza;
    private static final zzcn<Boolean> zzb;
    private static final zzcn<Boolean> zzc;

    public final boolean zza() {
        return true;
    }

    public final boolean zzb() {
        return zza.zzc().booleanValue();
    }

    public final boolean zzc() {
        return zzb.zzc().booleanValue();
    }

    public final boolean zzd() {
        return zzc.zzc().booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, boolean):com.google.android.gms.internal.measurement.zzcn<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, double):com.google.android.gms.internal.measurement.zzcn<java.lang.Double>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, long):com.google.android.gms.internal.measurement.zzcn<java.lang.Long>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.zzcn<java.lang.String>
      com.google.android.gms.internal.measurement.zzct.zza(java.lang.String, boolean):com.google.android.gms.internal.measurement.zzcn<java.lang.Boolean> */
    static {
        zzct zzct = new zzct(zzck.zza("com.google.android.gms.measurement"));
        zza = zzct.zza("measurement.client.sessions.check_on_reset_and_enable", false);
        zzb = zzct.zza("measurement.client.sessions.check_on_startup", true);
        zzc = zzct.zza("measurement.client.sessions.start_session_before_view_screen", true);
    }
}
