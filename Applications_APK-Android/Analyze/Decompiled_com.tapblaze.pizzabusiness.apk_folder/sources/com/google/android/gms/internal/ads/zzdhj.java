package com.google.android.gms.internal.ads;

import java.util.concurrent.Delayed;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdhj<V> extends zzdgq<V> implements zzdhe<V>, ScheduledFuture<V> {
    private final ScheduledFuture<?> zzgxh;

    public zzdhj(zzdhe<V> zzdhe, ScheduledFuture<?> scheduledFuture) {
        super(zzdhe);
        this.zzgxh = scheduledFuture;
    }

    public final boolean cancel(boolean z) {
        boolean cancel = super.cancel(z);
        if (cancel) {
            this.zzgxh.cancel(z);
        }
        return cancel;
    }

    public final long getDelay(TimeUnit timeUnit) {
        return this.zzgxh.getDelay(timeUnit);
    }

    public final /* synthetic */ int compareTo(Object obj) {
        return this.zzgxh.compareTo((Delayed) obj);
    }
}
