package com.google.android.gms.internal.p000firebaseperf;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfv  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
abstract class zzfv {
    private static final zzfv zzsc = new zzfx();
    private static final zzfv zzsd = new zzga();

    private zzfv() {
    }

    /* access modifiers changed from: package-private */
    public abstract void zza(Object obj, long j);

    /* access modifiers changed from: package-private */
    public abstract <L> void zza(Object obj, Object obj2, long j);

    static zzfv zzhx() {
        return zzsc;
    }

    static zzfv zzhy() {
        return zzsd;
    }
}
