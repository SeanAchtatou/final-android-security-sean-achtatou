package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcsv implements Callable {
    private final zzcss zzggd;

    zzcsv(zzcss zzcss) {
        this.zzggd = zzcss;
    }

    public final Object call() {
        return this.zzggd.zzank();
    }
}
