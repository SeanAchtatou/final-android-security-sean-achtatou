package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzho {
    void zza(int i, long j, long j2);

    void zza(zzit zzit);

    void zza(String str, long j, long j2);

    void zzb(zzgw zzgw);

    void zzb(zzit zzit);

    void zzr(int i);
}
