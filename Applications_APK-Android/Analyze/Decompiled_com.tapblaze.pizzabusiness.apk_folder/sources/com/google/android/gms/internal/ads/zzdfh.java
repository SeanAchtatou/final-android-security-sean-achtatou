package com.google.android.gms.internal.ads;

import com.ironsource.sdk.constants.Constants;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzdfh<K, V> extends zzdey<K, V> {
    private static final zzdey<Object, Object> zzguy = new zzdfh(null, new Object[0], 0);
    private final transient int size;
    private final transient Object[] zzguw;
    private final transient Object zzguz;

    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [short[], byte[]], vars: [r10v5 ?, r10v4 ?, r10v7 ?, r10v9 ?]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:102)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:78)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:69)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:51)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:32)
        */
    static <K, V> com.google.android.gms.internal.ads.zzdfh<K, V> zzc(int r10, java.lang.Object[] r11) {
        /*
            int r10 = r11.length
            int r10 = r10 >> 1
            r0 = 5
            com.google.android.gms.internal.ads.zzdei.zzt(r0, r10)
            int r10 = com.google.android.gms.internal.ads.zzdfb.zzdx(r0)
            int r1 = r10 + -1
            r2 = 0
            r3 = -1
            r4 = 128(0x80, float:1.794E-43)
            if (r10 > r4) goto L_0x004b
            byte[] r10 = new byte[r10]
            java.util.Arrays.fill(r10, r3)
        L_0x0018:
            if (r2 >= r0) goto L_0x00bd
            int r3 = r2 * 2
            r4 = r11[r3]
            r5 = r3 ^ 1
            r5 = r11[r5]
            com.google.android.gms.internal.ads.zzdeo.zzb(r4, r5)
            int r6 = r4.hashCode()
            int r6 = com.google.android.gms.internal.ads.zzdeq.zzdv(r6)
        L_0x002d:
            r6 = r6 & r1
            byte r7 = r10[r6]
            r8 = 255(0xff, float:3.57E-43)
            r7 = r7 & r8
            if (r7 != r8) goto L_0x003b
            byte r3 = (byte) r3
            r10[r6] = r3
            int r2 = r2 + 1
            goto L_0x0018
        L_0x003b:
            r8 = r11[r7]
            boolean r8 = r8.equals(r4)
            if (r8 != 0) goto L_0x0046
            int r6 = r6 + 1
            goto L_0x002d
        L_0x0046:
            java.lang.IllegalArgumentException r10 = zza(r4, r5, r11, r7)
            throw r10
        L_0x004b:
            r4 = 32768(0x8000, float:4.5918E-41)
            if (r10 > r4) goto L_0x0089
            short[] r10 = new short[r10]
            java.util.Arrays.fill(r10, r3)
        L_0x0055:
            if (r2 >= r0) goto L_0x00bd
            int r3 = r2 * 2
            r4 = r11[r3]
            r5 = r3 ^ 1
            r5 = r11[r5]
            com.google.android.gms.internal.ads.zzdeo.zzb(r4, r5)
            int r6 = r4.hashCode()
            int r6 = com.google.android.gms.internal.ads.zzdeq.zzdv(r6)
        L_0x006a:
            r6 = r6 & r1
            short r7 = r10[r6]
            r8 = 65535(0xffff, float:9.1834E-41)
            r7 = r7 & r8
            if (r7 != r8) goto L_0x0079
            short r3 = (short) r3
            r10[r6] = r3
            int r2 = r2 + 1
            goto L_0x0055
        L_0x0079:
            r8 = r11[r7]
            boolean r8 = r8.equals(r4)
            if (r8 != 0) goto L_0x0084
            int r6 = r6 + 1
            goto L_0x006a
        L_0x0084:
            java.lang.IllegalArgumentException r10 = zza(r4, r5, r11, r7)
            throw r10
        L_0x0089:
            int[] r10 = new int[r10]
            java.util.Arrays.fill(r10, r3)
        L_0x008e:
            if (r2 >= r0) goto L_0x00bd
            int r4 = r2 * 2
            r5 = r11[r4]
            r6 = r4 ^ 1
            r6 = r11[r6]
            com.google.android.gms.internal.ads.zzdeo.zzb(r5, r6)
            int r7 = r5.hashCode()
            int r7 = com.google.android.gms.internal.ads.zzdeq.zzdv(r7)
        L_0x00a3:
            r7 = r7 & r1
            r8 = r10[r7]
            if (r8 != r3) goto L_0x00ad
            r10[r7] = r4
            int r2 = r2 + 1
            goto L_0x008e
        L_0x00ad:
            r9 = r11[r8]
            boolean r9 = r9.equals(r5)
            if (r9 != 0) goto L_0x00b8
            int r7 = r7 + 1
            goto L_0x00a3
        L_0x00b8:
            java.lang.IllegalArgumentException r10 = zza(r5, r6, r11, r8)
            throw r10
        L_0x00bd:
            com.google.android.gms.internal.ads.zzdfh r1 = new com.google.android.gms.internal.ads.zzdfh
            r1.<init>(r10, r11, r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdfh.zzc(int, java.lang.Object[]):com.google.android.gms.internal.ads.zzdfh");
    }

    private static IllegalArgumentException zza(Object obj, Object obj2, Object[] objArr, int i) {
        String valueOf = String.valueOf(obj);
        String valueOf2 = String.valueOf(obj2);
        String valueOf3 = String.valueOf(objArr[i]);
        String valueOf4 = String.valueOf(objArr[i ^ 1]);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length());
        sb.append("Multiple entries with same key: ");
        sb.append(valueOf);
        sb.append(Constants.RequestParameters.EQUAL);
        sb.append(valueOf2);
        sb.append(" and ");
        sb.append(valueOf3);
        sb.append(Constants.RequestParameters.EQUAL);
        sb.append(valueOf4);
        return new IllegalArgumentException(sb.toString());
    }

    private zzdfh(Object obj, Object[] objArr, int i) {
        this.zzguz = obj;
        this.zzguw = objArr;
        this.size = i;
    }

    public final int size() {
        return this.size;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @org.checkerframework.checker.nullness.compatqual.NullableDecl
    public final V get(@org.checkerframework.checker.nullness.compatqual.NullableDecl java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r0 = r8.zzguz
            java.lang.Object[] r1 = r8.zzguw
            int r2 = r8.size
            r3 = 0
            if (r9 != 0) goto L_0x000a
            return r3
        L_0x000a:
            r4 = 1
            if (r2 != r4) goto L_0x001a
            r0 = 0
            r0 = r1[r0]
            boolean r9 = r0.equals(r9)
            if (r9 == 0) goto L_0x0019
            r9 = r1[r4]
            return r9
        L_0x0019:
            return r3
        L_0x001a:
            if (r0 != 0) goto L_0x001d
            return r3
        L_0x001d:
            boolean r2 = r0 instanceof byte[]
            if (r2 == 0) goto L_0x0048
            r2 = r0
            byte[] r2 = (byte[]) r2
            int r0 = r2.length
            int r5 = r0 + -1
            int r0 = r9.hashCode()
            int r0 = com.google.android.gms.internal.ads.zzdeq.zzdv(r0)
        L_0x002f:
            r0 = r0 & r5
            byte r6 = r2[r0]
            r7 = 255(0xff, float:3.57E-43)
            r6 = r6 & r7
            if (r6 != r7) goto L_0x0038
            return r3
        L_0x0038:
            r7 = r1[r6]
            boolean r7 = r7.equals(r9)
            if (r7 == 0) goto L_0x0045
            r9 = r6 ^ 1
            r9 = r1[r9]
            return r9
        L_0x0045:
            int r0 = r0 + 1
            goto L_0x002f
        L_0x0048:
            boolean r2 = r0 instanceof short[]
            if (r2 == 0) goto L_0x0074
            r2 = r0
            short[] r2 = (short[]) r2
            int r0 = r2.length
            int r5 = r0 + -1
            int r0 = r9.hashCode()
            int r0 = com.google.android.gms.internal.ads.zzdeq.zzdv(r0)
        L_0x005a:
            r0 = r0 & r5
            short r6 = r2[r0]
            r7 = 65535(0xffff, float:9.1834E-41)
            r6 = r6 & r7
            if (r6 != r7) goto L_0x0064
            return r3
        L_0x0064:
            r7 = r1[r6]
            boolean r7 = r7.equals(r9)
            if (r7 == 0) goto L_0x0071
            r9 = r6 ^ 1
            r9 = r1[r9]
            return r9
        L_0x0071:
            int r0 = r0 + 1
            goto L_0x005a
        L_0x0074:
            int[] r0 = (int[]) r0
            int r2 = r0.length
            int r2 = r2 - r4
            int r5 = r9.hashCode()
            int r5 = com.google.android.gms.internal.ads.zzdeq.zzdv(r5)
        L_0x0080:
            r5 = r5 & r2
            r6 = r0[r5]
            r7 = -1
            if (r6 != r7) goto L_0x0087
            return r3
        L_0x0087:
            r7 = r1[r6]
            boolean r7 = r7.equals(r9)
            if (r7 == 0) goto L_0x0094
            r9 = r6 ^ 1
            r9 = r1[r9]
            return r9
        L_0x0094:
            int r5 = r5 + 1
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzdfh.get(java.lang.Object):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public final zzdfb<Map.Entry<K, V>> zzare() {
        return new zzdfg(this, this.zzguw, 0, this.size);
    }

    /* access modifiers changed from: package-private */
    public final zzdfb<K> zzarf() {
        return new zzdfi(this, new zzdfl(this.zzguw, 0, this.size));
    }

    /* access modifiers changed from: package-private */
    public final zzdet<V> zzarg() {
        return new zzdfl(this.zzguw, 1, this.size);
    }
}
