package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzajh implements zzazp<zzajq> {
    private final /* synthetic */ zzajf zzdad;

    zzajh(zzajf zzajf) {
        this.zzdad = zzajf;
    }

    public final /* synthetic */ void zzh(Object obj) {
        zzajq zzajq = (zzajq) obj;
        zzavs.zzed("Releasing engine reference.");
        this.zzdad.zzdab.zzsd();
    }
}
