package com.google.android.gms.internal.ads;

import android.content.Context;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzaqr {
    void zza(Context context, String str, JSONObject jSONObject);
}
