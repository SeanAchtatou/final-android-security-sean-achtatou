package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdvq;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzdvq<M extends zzdvq<M>> extends zzdvt {
    protected zzdvs zzhtm;

    /* access modifiers changed from: protected */
    public int zzoi() {
        if (this.zzhtm != null) {
            for (int i = 0; i < this.zzhtm.size(); i++) {
                this.zzhtm.zzhc(i).zzoi();
            }
        }
        return 0;
    }

    public void zza(zzdvo zzdvo) throws IOException {
        if (this.zzhtm != null) {
            for (int i = 0; i < this.zzhtm.size(); i++) {
                this.zzhtm.zzhc(i).zza(zzdvo);
            }
        }
    }

    public final /* synthetic */ zzdvt zzbcr() throws CloneNotSupportedException {
        return (zzdvq) clone();
    }

    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        zzdvq zzdvq = (zzdvq) super.clone();
        zzdvu.zza(this, zzdvq);
        return zzdvq;
    }
}
