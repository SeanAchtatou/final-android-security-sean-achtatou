package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzms;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class zzdf implements zzes {
    static final zzes zza = new zzdf();

    private zzdf() {
    }

    public final Object zza() {
        return Boolean.valueOf(zzms.zzh());
    }
}
