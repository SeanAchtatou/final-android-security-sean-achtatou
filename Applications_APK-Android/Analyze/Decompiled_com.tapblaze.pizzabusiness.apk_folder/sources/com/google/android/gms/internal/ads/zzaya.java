package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaya {
    private final Object lock = new Object();
    private long zzdut;
    private long zzduu = Long.MIN_VALUE;

    public zzaya(long j) {
        this.zzdut = j;
    }

    public final boolean tryAcquire() {
        synchronized (this.lock) {
            long elapsedRealtime = zzq.zzkx().elapsedRealtime();
            if (this.zzduu + this.zzdut > elapsedRealtime) {
                return false;
            }
            this.zzduu = elapsedRealtime;
            return true;
        }
    }

    public final void zzfb(long j) {
        synchronized (this.lock) {
            this.zzdut = j;
        }
    }
}
