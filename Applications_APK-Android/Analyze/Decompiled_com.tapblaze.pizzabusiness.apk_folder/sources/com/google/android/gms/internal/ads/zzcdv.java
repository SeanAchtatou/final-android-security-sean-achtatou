package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.ads.internal.zzq;
import com.ironsource.sdk.constants.Constants;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcdv {
    private final Executor executor;
    private final String packageName;
    private final String zzcrj = zzaav.zzcsv.get();
    private final zzayy zzduv;
    private final Map<String, String> zzfsw = new HashMap();
    private final Context zzup;

    public zzcdv(Executor executor2, zzayy zzayy, Context context) {
        this.executor = executor2;
        this.zzduv = zzayy;
        this.zzup = context;
        this.packageName = context.getPackageName();
        this.zzfsw.put("s", "gmob_sdk");
        this.zzfsw.put("v", "3");
        this.zzfsw.put("os", Build.VERSION.RELEASE);
        this.zzfsw.put("sdk", Build.VERSION.SDK);
        Map<String, String> map = this.zzfsw;
        zzq.zzkq();
        map.put(Constants.ParametersKeys.ORIENTATION_DEVICE, zzawb.zzwl());
        this.zzfsw.put(SettingsJsonConstants.APP_KEY, this.packageName);
        Map<String, String> map2 = this.zzfsw;
        zzq.zzkq();
        map2.put("is_lite_sdk", zzawb.zzay(this.zzup) ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        this.zzfsw.put("e", TextUtils.join(",", zzzn.zzqh()));
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void zzm(java.util.Map<java.lang.String, java.lang.String> r4) {
        /*
            r3 = this;
            java.lang.String r0 = r3.zzcrj
            android.net.Uri r0 = android.net.Uri.parse(r0)
            android.net.Uri$Builder r0 = r0.buildUpon()
            java.util.Set r4 = r4.entrySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x0012:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x002e
            java.lang.Object r1 = r4.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r2 = r1.getKey()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r1 = r1.getValue()
            java.lang.String r1 = (java.lang.String) r1
            r0.appendQueryParameter(r2, r1)
            goto L_0x0012
        L_0x002e:
            android.net.Uri r4 = r0.build()
            java.lang.String r4 = r4.toString()
            com.google.android.gms.internal.ads.zzaan<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzaav.zzcsw
            java.lang.Object r0 = r0.get()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0059
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzzn.zzcku
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()
            java.lang.Object r0 = r1.zzd(r0)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0057
            goto L_0x0059
        L_0x0057:
            r0 = 0
            goto L_0x005a
        L_0x0059:
            r0 = 1
        L_0x005a:
            if (r0 == 0) goto L_0x0066
            java.util.concurrent.Executor r0 = r3.executor
            com.google.android.gms.internal.ads.zzcdy r1 = new com.google.android.gms.internal.ads.zzcdy
            r1.<init>(r3, r4)
            r0.execute(r1)
        L_0x0066:
            com.google.android.gms.internal.ads.zzavs.zzed(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcdv.zzm(java.util.Map):void");
    }

    public final Map<String, String> zzalg() {
        return new HashMap(this.zzfsw);
    }

    public final ConcurrentHashMap<String, String> zzalh() {
        return new ConcurrentHashMap<>(this.zzfsw);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzgc(String str) {
        this.zzduv.zzen(str);
    }
}
