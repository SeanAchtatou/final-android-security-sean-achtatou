package com.google.android.gms.internal.p000firebaseperf;

import android.util.Log;
import com.google.android.gms.internal.p000firebaseperf.zzcv;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.internal.GaugeManager;
import com.google.firebase.perf.internal.SessionManager;
import com.google.firebase.perf.internal.zza;
import com.google.firebase.perf.internal.zzb;
import com.google.firebase.perf.internal.zzf;
import com.google.firebase.perf.internal.zzt;
import com.google.firebase.perf.internal.zzx;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import okhttp3.HttpUrl;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbg  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzbg extends zzb implements zzx {
    private final List<zzt> zzck;
    private final GaugeManager zzcl;
    private zzf zzcm;
    private final zzcv.zza zzcn;
    private boolean zzco;
    private boolean zzcp;
    private final WeakReference<zzx> zzcq;

    public final void zza(zzt zzt) {
        if (this.zzcn.zzep() && !this.zzcn.zzev()) {
            this.zzck.add(zzt);
        }
    }

    public static zzbg zzb(zzf zzf) {
        return new zzbg(zzf);
    }

    private zzbg(zzf zzf) {
        this(zzf, zza.zzbf(), GaugeManager.zzbx());
    }

    private zzbg(zzf zzf, zza zza, GaugeManager gaugeManager) {
        super(zza);
        this.zzcn = zzcv.zzey();
        this.zzcq = new WeakReference<>(this);
        this.zzcm = zzf;
        this.zzcl = gaugeManager;
        this.zzck = new ArrayList();
        zzbp();
    }

    public final void zzbk() {
        this.zzcp = true;
    }

    public final zzbg zzf(String str) {
        HttpUrl parse;
        int lastIndexOf;
        if (str != null) {
            HttpUrl parse2 = HttpUrl.parse(str);
            if (parse2 != null) {
                str = parse2.newBuilder().username("").password("").query(null).fragment(null).toString();
            }
            zzcv.zza zza = this.zzcn;
            if (str.length() > 2000) {
                if (str.charAt(2000) == '/' || (parse = HttpUrl.parse(str)) == null || parse.encodedPath().lastIndexOf(47) < 0 || (lastIndexOf = str.lastIndexOf(47, 1999)) < 0) {
                    str = str.substring(0, 2000);
                } else {
                    str = str.substring(0, lastIndexOf);
                }
            }
            zza.zzae(str);
        }
        return this;
    }

    public final zzbg zzg(String str) {
        zzcv.zzb zzb;
        if (str != null) {
            String upperCase = str.toUpperCase();
            char c = 65535;
            switch (upperCase.hashCode()) {
                case -531492226:
                    if (upperCase.equals("OPTIONS")) {
                        c = 6;
                        break;
                    }
                    break;
                case 70454:
                    if (upperCase.equals("GET")) {
                        c = 0;
                        break;
                    }
                    break;
                case 79599:
                    if (upperCase.equals("PUT")) {
                        c = 1;
                        break;
                    }
                    break;
                case 2213344:
                    if (upperCase.equals("HEAD")) {
                        c = 4;
                        break;
                    }
                    break;
                case 2461856:
                    if (upperCase.equals("POST")) {
                        c = 2;
                        break;
                    }
                    break;
                case 75900968:
                    if (upperCase.equals("PATCH")) {
                        c = 5;
                        break;
                    }
                    break;
                case 80083237:
                    if (upperCase.equals("TRACE")) {
                        c = 7;
                        break;
                    }
                    break;
                case 1669334218:
                    if (upperCase.equals(FirebasePerformance.HttpMethod.CONNECT)) {
                        c = 8;
                        break;
                    }
                    break;
                case 2012838315:
                    if (upperCase.equals("DELETE")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    zzb = zzcv.zzb.GET;
                    break;
                case 1:
                    zzb = zzcv.zzb.PUT;
                    break;
                case 2:
                    zzb = zzcv.zzb.POST;
                    break;
                case 3:
                    zzb = zzcv.zzb.DELETE;
                    break;
                case 4:
                    zzb = zzcv.zzb.HEAD;
                    break;
                case 5:
                    zzb = zzcv.zzb.PATCH;
                    break;
                case 6:
                    zzb = zzcv.zzb.OPTIONS;
                    break;
                case 7:
                    zzb = zzcv.zzb.TRACE;
                    break;
                case 8:
                    zzb = zzcv.zzb.CONNECT;
                    break;
                default:
                    zzb = zzcv.zzb.HTTP_METHOD_UNKNOWN;
                    break;
            }
            this.zzcn.zzb(zzb);
        }
        return this;
    }

    public final zzbg zzc(int i) {
        this.zzcn.zzl(i);
        return this;
    }

    public final boolean zzbl() {
        return this.zzcn.zzbl();
    }

    public final zzbg zzj(long j) {
        this.zzcn.zzah(j);
        return this;
    }

    public final zzbg zza(Map<String, String> map) {
        this.zzcn.zzfc().zzc(map);
        return this;
    }

    public final zzbg zzk(long j) {
        zzt zzcl2 = SessionManager.zzck().zzcl();
        SessionManager.zzck().zzc(this.zzcq);
        this.zzcn.zzaj(j);
        this.zzck.add(zzcl2);
        if (zzcl2.zzcf()) {
            this.zzcl.zzj(zzcl2.zzce());
        }
        return this;
    }

    public final zzbg zzl(long j) {
        this.zzcn.zzak(j);
        return this;
    }

    public final zzbg zzm(long j) {
        this.zzcn.zzal(j);
        return this;
    }

    public final long zzbm() {
        return this.zzcn.zzeu();
    }

    public final zzbg zzn(long j) {
        this.zzcn.zzam(j);
        if (SessionManager.zzck().zzcl().zzcf()) {
            this.zzcl.zzj(SessionManager.zzck().zzcl().zzce());
        }
        return this;
    }

    public final zzbg zzo(long j) {
        this.zzcn.zzai(j);
        return this;
    }

    public final zzbg zzh(String str) {
        if (str == null) {
            this.zzcn.zzfb();
            return this;
        }
        boolean z = false;
        if (str.length() <= 128) {
            int i = 0;
            while (true) {
                if (i >= str.length()) {
                    z = true;
                    break;
                }
                char charAt = str.charAt(i);
                if (charAt <= 31 || charAt > 127) {
                    break;
                }
                i++;
            }
        }
        if (z) {
            this.zzcn.zzaf(str);
        } else {
            String valueOf = String.valueOf(str);
            Log.i("FirebasePerformance", valueOf.length() != 0 ? "The content type of the response is not a valid content-type:".concat(valueOf) : new String("The content type of the response is not a valid content-type:"));
        }
        return this;
    }

    public final zzbg zzbn() {
        this.zzcn.zzb(zzcv.zzd.GENERIC_CLIENT_ERROR);
        return this;
    }

    public final zzcv zzbo() {
        SessionManager.zzck().zzd(this.zzcq);
        zzbq();
        zzde[] zza = zzt.zza(this.zzck);
        if (zza != null) {
            this.zzcn.zzb(Arrays.asList(zza));
        }
        zzcv zzcv = (zzcv) ((zzfc) this.zzcn.zzhp());
        if (!this.zzco) {
            zzf zzf = this.zzcm;
            if (zzf != null) {
                zzf.zza(zzcv, zzbh());
            }
            this.zzco = true;
        } else if (this.zzcp) {
            Log.i("FirebasePerformance", "This metric has already been queued for transmission.  Please create a new HttpMetric for each request/response");
        }
        return zzcv;
    }
}
