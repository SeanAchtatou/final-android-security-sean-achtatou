package com.google.android.gms.ads;

import android.os.RemoteException;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzxb;
import com.google.android.gms.internal.ads.zzyt;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class VideoController {
    public static final int PLAYBACK_STATE_ENDED = 3;
    public static final int PLAYBACK_STATE_PAUSED = 2;
    public static final int PLAYBACK_STATE_PLAYING = 1;
    public static final int PLAYBACK_STATE_READY = 5;
    public static final int PLAYBACK_STATE_UNKNOWN = 0;
    private final Object lock = new Object();
    private zzxb zzabt;
    private VideoLifecycleCallbacks zzabu;

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static abstract class VideoLifecycleCallbacks {
        public void onVideoEnd() {
        }

        public void onVideoMute(boolean z) {
        }

        public void onVideoPause() {
        }

        public void onVideoPlay() {
        }

        public void onVideoStart() {
        }
    }

    public final void zza(zzxb zzxb) {
        synchronized (this.lock) {
            this.zzabt = zzxb;
            if (this.zzabu != null) {
                setVideoLifecycleCallbacks(this.zzabu);
            }
        }
    }

    public final zzxb zzdl() {
        zzxb zzxb;
        synchronized (this.lock) {
            zzxb = this.zzabt;
        }
        return zzxb;
    }

    public final void play() {
        synchronized (this.lock) {
            if (this.zzabt != null) {
                try {
                    this.zzabt.play();
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call play on video controller.", e);
                }
            }
        }
    }

    public final void pause() {
        synchronized (this.lock) {
            if (this.zzabt != null) {
                try {
                    this.zzabt.pause();
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call pause on video controller.", e);
                }
            }
        }
    }

    public final void stop() {
        synchronized (this.lock) {
            if (this.zzabt != null) {
                try {
                    this.zzabt.stop();
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call stop on video controller.", e);
                }
            }
        }
    }

    public final void mute(boolean z) {
        synchronized (this.lock) {
            if (this.zzabt != null) {
                try {
                    this.zzabt.mute(z);
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call mute on video controller.", e);
                }
            }
        }
    }

    public final boolean isMuted() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return true;
            }
            try {
                boolean isMuted = this.zzabt.isMuted();
                return isMuted;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call isMuted on video controller.", e);
                return true;
            }
        }
    }

    public final int getPlaybackState() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return 0;
            }
            try {
                int playbackState = this.zzabt.getPlaybackState();
                return playbackState;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call getPlaybackState on video controller.", e);
                return 0;
            }
        }
    }

    public final boolean isCustomControlsEnabled() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return false;
            }
            try {
                boolean isCustomControlsEnabled = this.zzabt.isCustomControlsEnabled();
                return isCustomControlsEnabled;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call isUsingCustomPlayerControls.", e);
                return false;
            }
        }
    }

    public final boolean isClickToExpandEnabled() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return false;
            }
            try {
                boolean isClickToExpandEnabled = this.zzabt.isClickToExpandEnabled();
                return isClickToExpandEnabled;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call isClickToExpandEnabled.", e);
                return false;
            }
        }
    }

    public final float getVideoDuration() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return 0.0f;
            }
            try {
                float zzpk = this.zzabt.zzpk();
                return zzpk;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call getDuration on video controller.", e);
                return 0.0f;
            }
        }
    }

    public final float getVideoCurrentTime() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return 0.0f;
            }
            try {
                float zzpl = this.zzabt.zzpl();
                return zzpl;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call getCurrentTime on video controller.", e);
                return 0.0f;
            }
        }
    }

    public final void setVideoLifecycleCallbacks(VideoLifecycleCallbacks videoLifecycleCallbacks) {
        Preconditions.checkNotNull(videoLifecycleCallbacks, "VideoLifecycleCallbacks may not be null.");
        synchronized (this.lock) {
            this.zzabu = videoLifecycleCallbacks;
            if (this.zzabt != null) {
                try {
                    this.zzabt.zza(new zzyt(videoLifecycleCallbacks));
                } catch (RemoteException e) {
                    zzayu.zzc("Unable to call setVideoLifecycleCallbacks on video controller.", e);
                }
            }
        }
    }

    public final VideoLifecycleCallbacks getVideoLifecycleCallbacks() {
        VideoLifecycleCallbacks videoLifecycleCallbacks;
        synchronized (this.lock) {
            videoLifecycleCallbacks = this.zzabu;
        }
        return videoLifecycleCallbacks;
    }

    public final boolean hasVideoContent() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzabt != null;
        }
        return z;
    }

    public final float getAspectRatio() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return 0.0f;
            }
            try {
                float aspectRatio = this.zzabt.getAspectRatio();
                return aspectRatio;
            } catch (RemoteException e) {
                zzayu.zzc("Unable to call getAspectRatio on video controller.", e);
                return 0.0f;
            }
        }
    }
}
