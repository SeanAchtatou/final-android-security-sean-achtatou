package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcgz implements zzdby {
    static final zzdby zzfun = new zzcgz();

    private zzcgz() {
    }

    public final Object apply(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        zzavs.zzed("Ad request signals:");
        zzavs.zzed(jSONObject.toString(2));
        return jSONObject;
    }
}
