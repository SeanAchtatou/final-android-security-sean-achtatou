package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzfe;
import com.google.android.gms.internal.measurement.zzfe.zza;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public abstract class zzfe<MessageType extends zzfe<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzdm<MessageType, BuilderType> {
    private static Map<Object, zzfe<?, ?>> zzd = new ConcurrentHashMap();
    protected zzhw zzb = zzhw.zza();
    private int zzc = -1;

    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static class zzc<T extends zzfe<T, ?>> extends zzdn<T> {
        private final T zza;

        public zzc(T t) {
            this.zza = t;
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static class zzd<ContainingType extends zzgm, Type> extends zzep<ContainingType, Type> {
    }

    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    static final class zze implements zzew<zze> {
        public final int zza() {
            throw new NoSuchMethodError();
        }

        public final zzik zzb() {
            throw new NoSuchMethodError();
        }

        public final zzir zzc() {
            throw new NoSuchMethodError();
        }

        public final boolean zzd() {
            throw new NoSuchMethodError();
        }

        public final boolean zze() {
            throw new NoSuchMethodError();
        }

        public final zzgp zza(zzgp zzgp, zzgm zzgm) {
            throw new NoSuchMethodError();
        }

        public final zzgv zza(zzgv zzgv, zzgv zzgv2) {
            throw new NoSuchMethodError();
        }

        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static final class zzf {
        public static final int zza = 1;
        public static final int zzb = 2;
        public static final int zzc = 3;
        public static final int zzd = 4;
        public static final int zze = 5;
        public static final int zzf = 6;
        public static final int zzg = 7;
        public static final int zzh = 1;
        public static final int zzi = 2;
        public static final int zzj = 1;
        public static final int zzk = 2;
        private static final /* synthetic */ int[] zzl = {zza, zzb, zzc, zzd, zze, zzf, zzg};
        private static final /* synthetic */ int[] zzm = {zzh, zzi};
        private static final /* synthetic */ int[] zzn = {zzj, zzk};

        public static int[] zza() {
            return (int[]) zzl.clone();
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object zza(int i, Object obj, Object obj2);

    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static abstract class zzb<MessageType extends zzb<MessageType, BuilderType>, BuilderType> extends zzfe<MessageType, BuilderType> implements zzgo {
        protected zzeu<zze> zzc = zzeu.zza();

        /* access modifiers changed from: package-private */
        public final zzeu<zze> zza() {
            if (this.zzc.zzc()) {
                this.zzc = (zzeu) this.zzc.clone();
            }
            return this.zzc;
        }
    }

    public String toString() {
        return zzgr.zza(this, super.toString());
    }

    public int hashCode() {
        if (this.zza != 0) {
            return this.zza;
        }
        this.zza = zzhb.zza().zza(this).zza(this);
        return this.zza;
    }

    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static abstract class zza<MessageType extends zzfe<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> extends zzdl<MessageType, BuilderType> {
        protected MessageType zza;
        protected boolean zzb = false;
        private final MessageType zzc;

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected zza(MessageType r3) {
            /*
                r2 = this;
                r2.<init>()
                r2.zzc = r3
                int r0 = com.google.android.gms.internal.measurement.zzfe.zzf.zzd
                r1 = 0
                java.lang.Object r3 = r3.zza(r0, r1, r1)
                com.google.android.gms.internal.measurement.zzfe r3 = (com.google.android.gms.internal.measurement.zzfe) r3
                r2.zza = r3
                r3 = 0
                r2.zzb = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzfe.zza.<init>(com.google.android.gms.internal.measurement.zzfe):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected void zzq() {
            /*
                r3 = this;
                MessageType r0 = r3.zza
                int r1 = com.google.android.gms.internal.measurement.zzfe.zzf.zzd
                r2 = 0
                java.lang.Object r0 = r0.zza(r1, r2, r2)
                com.google.android.gms.internal.measurement.zzfe r0 = (com.google.android.gms.internal.measurement.zzfe) r0
                MessageType r1 = r3.zza
                zza(r0, r1)
                r3.zza = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzfe.zza.zzq():void");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.zzfe.zza(com.google.android.gms.internal.measurement.zzfe, boolean):boolean
         arg types: [MessageType, int]
         candidates:
          com.google.android.gms.internal.measurement.zzfe.zza(java.lang.Class, com.google.android.gms.internal.measurement.zzfe):void
          com.google.android.gms.internal.measurement.zzdm.zza(java.lang.Iterable, java.util.List):void
          com.google.android.gms.internal.measurement.zzfe.zza(com.google.android.gms.internal.measurement.zzfe, boolean):boolean */
        public final boolean g_() {
            return zzfe.zza((zzfe) this.zza, false);
        }

        /* renamed from: zzs */
        public MessageType zzu() {
            if (this.zzb) {
                return this.zza;
            }
            MessageType messagetype = this.zza;
            zzhb.zza().zza((Object) messagetype).zzc(messagetype);
            this.zzb = true;
            return this.zza;
        }

        /* renamed from: zzt */
        public final MessageType zzv() {
            MessageType messagetype = (zzfe) zzu();
            if (messagetype.g_()) {
                return messagetype;
            }
            throw new zzhu(messagetype);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.zzfe.zza.zza(com.google.android.gms.internal.measurement.zzfe, com.google.android.gms.internal.measurement.zzfe):void
         arg types: [MessageType, MessageType]
         candidates:
          com.google.android.gms.internal.measurement.zzfe.zza.zza(com.google.android.gms.internal.measurement.zzei, com.google.android.gms.internal.measurement.zzer):com.google.android.gms.internal.measurement.zzdl
          com.google.android.gms.internal.measurement.zzdl.zza(com.google.android.gms.internal.measurement.zzei, com.google.android.gms.internal.measurement.zzer):BuilderType
          com.google.android.gms.internal.measurement.zzdl.zza(byte[], com.google.android.gms.internal.measurement.zzer):com.google.android.gms.internal.measurement.zzgp
          com.google.android.gms.internal.measurement.zzgp.zza(byte[], com.google.android.gms.internal.measurement.zzer):com.google.android.gms.internal.measurement.zzgp
          com.google.android.gms.internal.measurement.zzfe.zza.zza(com.google.android.gms.internal.measurement.zzfe, com.google.android.gms.internal.measurement.zzfe):void */
        public final BuilderType zza(MessageType messagetype) {
            if (this.zzb) {
                zzq();
                this.zzb = false;
            }
            zza((zzfe) this.zza, (zzfe) messagetype);
            return this;
        }

        private static void zza(MessageType messagetype, MessageType messagetype2) {
            zzhb.zza().zza((Object) messagetype).zzb(messagetype, messagetype2);
        }

        private final BuilderType zzb(byte[] bArr, int i, int i2, zzer zzer) throws zzfm {
            if (this.zzb) {
                zzq();
                this.zzb = false;
            }
            try {
                zzhb.zza().zza((Object) this.zza).zza(this.zza, bArr, 0, i2 + 0, new zzdr(zzer));
                return this;
            } catch (zzfm e) {
                throw e;
            } catch (IndexOutOfBoundsException unused) {
                throw zzfm.zza();
            } catch (IOException e2) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e2);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: zzb */
        public final BuilderType zza(zzei zzei, zzer zzer) throws IOException {
            if (this.zzb) {
                zzq();
                this.zzb = false;
            }
            try {
                zzhb.zza().zza((Object) this.zza).zza(this.zza, zzej.zza(zzei), zzer);
                return this;
            } catch (RuntimeException e) {
                if (e.getCause() instanceof IOException) {
                    throw ((IOException) e.getCause());
                }
                throw e;
            }
        }

        public final /* synthetic */ zzdl zza(byte[] bArr, int i, int i2, zzer zzer) throws zzfm {
            return zzb(bArr, 0, i2, zzer);
        }

        public final /* synthetic */ zzdl zza(byte[] bArr, int i, int i2) throws zzfm {
            return zzb(bArr, 0, i2, zzer.zza());
        }

        public final /* synthetic */ zzdl zzp() {
            return (zza) clone();
        }

        public final /* synthetic */ zzgm h_() {
            return this.zzc;
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            zza zza2 = (zza) ((zzfe) this.zzc).zza(zzf.zze, (Object) null, (Object) null);
            zza2.zza((zzfe) zzu());
            return zza2;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return zzhb.zza().zza(this).zza(this, (zzfe) obj);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final <MessageType extends zzfe<MessageType, BuilderType>, BuilderType extends zza<MessageType, BuilderType>> BuilderType zzbk() {
        return (zza) zza(zzf.zze, (Object) null, (Object) null);
    }

    public final boolean g_() {
        return zza(this, Boolean.TRUE.booleanValue());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: BuilderType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final BuilderType zzbl() {
        /*
            r2 = this;
            int r0 = com.google.android.gms.internal.measurement.zzfe.zzf.zze
            r1 = 0
            java.lang.Object r0 = r2.zza(r0, r1, r1)
            com.google.android.gms.internal.measurement.zzfe$zza r0 = (com.google.android.gms.internal.measurement.zzfe.zza) r0
            r0.zza(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzfe.zzbl():com.google.android.gms.internal.measurement.zzfe$zza");
    }

    /* access modifiers changed from: package-private */
    public final int zzbj() {
        return this.zzc;
    }

    /* access modifiers changed from: package-private */
    public final void zzc(int i) {
        this.zzc = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void
     arg types: [com.google.android.gms.internal.measurement.zzfe, com.google.android.gms.internal.measurement.zzeo]
     candidates:
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.zzhf.zza(java.lang.Object, com.google.android.gms.internal.measurement.zziq):void */
    public final void zza(zzel zzel) throws IOException {
        zzhb.zza().zza(this).zza((Object) this, (zziq) zzeo.zza(zzel));
    }

    public final int zzbm() {
        if (this.zzc == -1) {
            this.zzc = zzhb.zza().zza(this).zzb(this);
        }
        return this.zzc;
    }

    static <T extends zzfe<?, ?>> T zza(Class<T> cls) {
        T t = (zzfe) zzd.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (zzfe) zzd.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (t == null) {
            t = (zzfe) ((zzfe) zzid.zza(cls)).zza(zzf.zzf, (Object) null, (Object) null);
            if (t != null) {
                zzd.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    protected static <T extends zzfe<?, ?>> void zza(Class<T> cls, T t) {
        zzd.put(cls, t);
    }

    protected static Object zza(zzgm zzgm, String str, Object[] objArr) {
        return new zzhd(zzgm, str, objArr);
    }

    static Object zza(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected static final <T extends com.google.android.gms.internal.measurement.zzfe<T, ?>> boolean zza(T r3, boolean r4) {
        /*
            int r0 = com.google.android.gms.internal.measurement.zzfe.zzf.zza
            r1 = 0
            java.lang.Object r0 = r3.zza(r0, r1, r1)
            java.lang.Byte r0 = (java.lang.Byte) r0
            byte r0 = r0.byteValue()
            r2 = 1
            if (r0 != r2) goto L_0x0011
            return r2
        L_0x0011:
            if (r0 != 0) goto L_0x0015
            r3 = 0
            return r3
        L_0x0015:
            com.google.android.gms.internal.measurement.zzhb r0 = com.google.android.gms.internal.measurement.zzhb.zza()
            com.google.android.gms.internal.measurement.zzhf r0 = r0.zza(r3)
            boolean r0 = r0.zzd(r3)
            if (r4 == 0) goto L_0x002d
            int r4 = com.google.android.gms.internal.measurement.zzfe.zzf.zzb
            if (r0 == 0) goto L_0x0029
            r2 = r3
            goto L_0x002a
        L_0x0029:
            r2 = r1
        L_0x002a:
            r3.zza(r4, r2, r1)
        L_0x002d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zzfe.zza(com.google.android.gms.internal.measurement.zzfe, boolean):boolean");
    }

    protected static zzfl zzbn() {
        return zzff.zzd();
    }

    protected static zzfk zzbo() {
        return zzga.zzd();
    }

    protected static zzfk zza(zzfk zzfk) {
        int size = zzfk.size();
        return zzfk.zzc(size == 0 ? 10 : size << 1);
    }

    protected static <E> zzfn<E> zzbp() {
        return zzha.zzd();
    }

    protected static <E> zzfn<E> zza(zzfn<E> zzfn) {
        int size = zzfn.size();
        return zzfn.zza(size == 0 ? 10 : size << 1);
    }

    public final /* synthetic */ zzgp zzbq() {
        zza zza2 = (zza) zza(zzf.zze, (Object) null, (Object) null);
        zza2.zza(this);
        return zza2;
    }

    public final /* synthetic */ zzgp zzbr() {
        return (zza) zza(zzf.zze, (Object) null, (Object) null);
    }

    public final /* synthetic */ zzgm h_() {
        return (zzfe) zza(zzf.zzf, (Object) null, (Object) null);
    }
}
