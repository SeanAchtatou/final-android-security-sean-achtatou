package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbbw implements zzno {
    private final zzbbs zzeco;
    private final zzno zzecp;

    zzbbw(zzbbs zzbbs, zzno zzno) {
        this.zzeco = zzbbs;
        this.zzecp = zzno;
    }

    public final zznl zzih() {
        return this.zzeco.zza(this.zzecp);
    }
}
