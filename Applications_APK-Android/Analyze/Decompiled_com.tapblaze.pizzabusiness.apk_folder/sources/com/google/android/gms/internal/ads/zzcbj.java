package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcbj implements zzdxg<zzbsu<zzbov>> {
    private final zzdxp<zzbsl> zzfdd;

    public zzcbj(zzdxp<zzbsl> zzdxp) {
        this.zzfdd = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }
}
