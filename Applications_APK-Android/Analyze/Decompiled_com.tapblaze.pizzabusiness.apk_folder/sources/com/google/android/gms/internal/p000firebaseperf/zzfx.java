package com.google.android.gms.internal.p000firebaseperf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfx  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzfx extends zzfv {
    private static final Class<?> zzse = Collections.unmodifiableList(Collections.emptyList()).getClass();

    private zzfx() {
        super();
    }

    /* access modifiers changed from: package-private */
    public final void zza(Object obj, long j) {
        Object obj2;
        List list = (List) zzhz.zzo(obj, j);
        if (list instanceof zzfw) {
            obj2 = ((zzfw) list).zzhw();
        } else if (!zzse.isAssignableFrom(list.getClass())) {
            if (!(list instanceof zzgy) || !(list instanceof zzfm)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                zzfm zzfm = (zzfm) list;
                if (zzfm.zzgf()) {
                    zzfm.zzgg();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        zzhz.zza(obj, j, obj2);
    }

    /* access modifiers changed from: package-private */
    public final <E> void zza(Object obj, Object obj2, long j) {
        List zzft;
        List zzb = zzb(obj2, j);
        int size = zzb.size();
        List zzb2 = zzb(obj, j);
        if (zzb2.isEmpty()) {
            if (zzb2 instanceof zzfw) {
                zzb2 = new zzft(size);
            } else if (!(zzb2 instanceof zzgy) || !(zzb2 instanceof zzfm)) {
                zzb2 = new ArrayList(size);
            } else {
                zzb2 = ((zzfm) zzb2).zzao(size);
            }
            zzhz.zza(obj, j, zzb2);
        } else {
            if (zzse.isAssignableFrom(zzb2.getClass())) {
                zzft = new ArrayList(zzb2.size() + size);
                zzft.addAll(zzb2);
                zzhz.zza(obj, j, zzft);
            } else if (zzb2 instanceof zzhy) {
                zzft = new zzft(zzb2.size() + size);
                zzft.addAll((zzhy) zzb2);
                zzhz.zza(obj, j, zzft);
            } else if ((zzb2 instanceof zzgy) && (zzb2 instanceof zzfm)) {
                zzfm zzfm = (zzfm) zzb2;
                if (!zzfm.zzgf()) {
                    zzfm zzao = zzfm.zzao(zzb2.size() + size);
                    zzhz.zza(obj, j, zzao);
                    zzb2 = zzao;
                }
            }
            zzb2 = zzft;
        }
        int size2 = zzb2.size();
        int size3 = zzb.size();
        if (size2 > 0 && size3 > 0) {
            zzb2.addAll(zzb);
        }
        if (size2 > 0) {
            zzb = zzb2;
        }
        zzhz.zza(obj, j, zzb);
    }

    private static <E> List<E> zzb(Object obj, long j) {
        return (List) zzhz.zzo(obj, j);
    }
}
