package com.google.android.gms.ads.impl;

public final class R {
    private R() {
    }

    public static final class string {
        public static final int s1 = 2131427452;
        public static final int s2 = 2131427453;
        public static final int s3 = 2131427454;
        public static final int s4 = 2131427455;
        public static final int s5 = 2131427456;
        public static final int s6 = 2131427457;
        public static final int s7 = 2131427458;

        private string() {
        }
    }
}
