package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzxy extends zzwu {
    private final String description;
    private final String zzcfi;

    public zzxy(String str, String str2) {
        this.description = str;
        this.zzcfi = str2;
    }

    public final String getDescription() throws RemoteException {
        return this.description;
    }

    public final String zzph() throws RemoteException {
        return this.zzcfi;
    }
}
