package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzauq extends zzgc implements zzauo {
    zzauq(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.signals.ISignalGenerator");
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzauu zzauu, zzaun zzaun) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, iObjectWrapper);
        zzge.zza(obtainAndWriteInterfaceToken, zzauu);
        zzge.zza(obtainAndWriteInterfaceToken, zzaun);
        zza(1, obtainAndWriteInterfaceToken);
    }
}
