package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzxo extends zzvd {
    private final /* synthetic */ zzxl zzcew;

    zzxo(zzxl zzxl) {
        this.zzcew = zzxl;
    }

    public final void onAdLoaded() {
        this.zzcew.zzcel.zza(this.zzcew.zzdl());
        super.onAdLoaded();
    }

    public final void onAdFailedToLoad(int i) {
        this.zzcew.zzcel.zza(this.zzcew.zzdl());
        super.onAdFailedToLoad(i);
    }
}
