package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzafj implements zzafn<zzbdi> {
    zzafj() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        String str = (String) map.get("action");
        if ("pause".equals(str)) {
            zzbdi.zzjv();
        } else if ("resume".equals(str)) {
            zzbdi.zzjw();
        }
    }
}
