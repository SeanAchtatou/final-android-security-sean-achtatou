package com.google.android.gms.gass;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.gass.internal.zzb;
import com.google.android.gms.gass.internal.zzd;
import com.google.android.gms.internal.ads.zzddn;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzh implements BaseGmsClient.BaseConnectionCallbacks, BaseGmsClient.BaseOnConnectionFailedListener {
    private final Object lock = new Object();
    private boolean zzfvq = false;
    private boolean zzfvr = false;
    private final zzd zzgso;
    private final zzddn zzgsp;

    zzh(Context context, Looper looper, zzddn zzddn) {
        this.zzgsp = zzddn;
        this.zzgso = new zzd(context, looper, this, this);
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
    }

    public final void onConnectionSuspended(int i) {
    }

    /* access modifiers changed from: package-private */
    public final void zzaqm() {
        synchronized (this.lock) {
            if (!this.zzfvq) {
                this.zzfvq = true;
                this.zzgso.checkAvailabilityAndConnect();
            }
        }
    }

    private final void zzalu() {
        synchronized (this.lock) {
            if (this.zzgso.isConnected() || this.zzgso.isConnecting()) {
                this.zzgso.disconnect();
            }
            Binder.flushPendingCommands();
        }
    }

    public final void onConnected(Bundle bundle) {
        synchronized (this.lock) {
            if (!this.zzfvr) {
                this.zzfvr = true;
                try {
                    this.zzgso.zzaqp().zza(new zzb(this.zzgsp.toByteArray()));
                    zzalu();
                } catch (Exception unused) {
                    zzalu();
                } catch (Throwable th) {
                    zzalu();
                    throw th;
                }
            }
        }
    }
}
