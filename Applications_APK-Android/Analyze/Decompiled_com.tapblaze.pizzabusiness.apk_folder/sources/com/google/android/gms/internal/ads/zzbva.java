package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbva implements zzbov {
    private final zzbpd zzfjm;
    private final zzczl zzfjn;

    public zzbva(zzbpd zzbpd, zzczl zzczl) {
        this.zzfjm = zzbpd;
        this.zzfjn = zzczl;
    }

    public final void onAdClosed() {
    }

    public final void onAdLeftApplication() {
    }

    public final void onRewardedVideoCompleted() {
    }

    public final void onRewardedVideoStarted() {
    }

    public final void zzb(zzare zzare, String str, String str2) {
    }

    public final void onAdOpened() {
        if (this.zzfjn.zzglz == 0 || this.zzfjn.zzglz == 1) {
            this.zzfjm.onAdImpression();
        }
    }
}
