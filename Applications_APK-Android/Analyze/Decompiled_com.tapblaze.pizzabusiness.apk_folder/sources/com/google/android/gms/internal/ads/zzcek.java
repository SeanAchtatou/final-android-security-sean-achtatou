package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import java.util.Set;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcek implements zzdxg<Set<zzbsu<AppEventListener>>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzceo> zzfsb;
    private final zzcee zzfth;

    private zzcek(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfth = zzcee;
        this.zzfsb = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzcek zzf(zzcee zzcee, zzdxp<zzceo> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzcek(zzcee, zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(zzcee.zzb(this.zzfsb.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
