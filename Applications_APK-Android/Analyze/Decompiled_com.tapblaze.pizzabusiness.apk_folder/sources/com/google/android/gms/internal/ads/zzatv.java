package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import java.lang.reflect.Method;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzatv {
    private final AtomicReference<ThreadPoolExecutor> zzdpe = new AtomicReference<>(null);
    private final Object zzdpf = new Object();
    private String zzdpg = null;
    private String zzdph = null;
    private final AtomicBoolean zzdpi = new AtomicBoolean(false);
    private final AtomicInteger zzdpj = new AtomicInteger(-1);
    private final AtomicReference<Object> zzdpk = new AtomicReference<>(null);
    private final AtomicReference<Object> zzdpl = new AtomicReference<>(null);
    private final ConcurrentMap<String, Method> zzdpm = new ConcurrentHashMap(9);
    private final AtomicReference<zzbfq> zzdpn = new AtomicReference<>(null);
    private final BlockingQueue<FutureTask<?>> zzdpo = new ArrayBlockingQueue(20);
    private final Object zzdpp = new Object();

    public final boolean zzab(Context context) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzchz)).booleanValue() && !this.zzdpi.get()) {
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcij)).booleanValue()) {
                return true;
            }
            if (this.zzdpj.get() == -1) {
                zzve.zzou();
                if (!zzayk.zzc(context, GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE)) {
                    zzve.zzou();
                    if (zzayk.zzbk(context)) {
                        zzavs.zzez("Google Play Service is out of date, the Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires updated Google Play Service.");
                        this.zzdpj.set(0);
                    }
                }
                this.zzdpj.set(1);
            }
            if (this.zzdpj.get() == 1) {
                return true;
            }
        }
        return false;
    }

    private static boolean zzac(Context context) {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcih)).booleanValue()) {
            if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcig)).booleanValue()) {
                return false;
            }
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcii)).booleanValue()) {
            try {
                context.getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
                return false;
            } catch (ClassNotFoundException unused) {
            }
        }
        return true;
    }

    public final void zza(Context context, zzyq zzyq) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcim)).booleanValue() && zzab(context) && zzac(context)) {
            synchronized (this.zzdpp) {
            }
        }
    }

    public final void zza(Context context, zzug zzug) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcim)).booleanValue() && zzab(context) && zzac(context)) {
            synchronized (this.zzdpp) {
            }
        }
    }

    public final void zze(Context context, String str) {
        if (zzab(context)) {
            if (zzac(context)) {
                zza("beginAdUnitExposure", new zzatu(str));
            } else {
                zza(context, str, "beginAdUnitExposure");
            }
        }
    }

    public final void zzf(Context context, String str) {
        if (zzab(context)) {
            if (zzac(context)) {
                zza("endAdUnitExposure", new zzaub(str));
            } else {
                zza(context, str, "endAdUnitExposure");
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String zzad(android.content.Context r7) {
        /*
            r6 = this;
            java.lang.String r0 = "getCurrentScreenName"
            boolean r1 = r6.zzab(r7)
            java.lang.String r2 = ""
            if (r1 != 0) goto L_0x000b
            return r2
        L_0x000b:
            boolean r1 = zzac(r7)
            if (r1 == 0) goto L_0x001c
            com.google.android.gms.internal.ads.zzaui r7 = com.google.android.gms.internal.ads.zzaua.zzdpr
            java.lang.String r0 = "getCurrentScreenNameOrScreenClass"
            java.lang.Object r7 = r6.zza(r0, r2, r7)
            java.lang.String r7 = (java.lang.String) r7
            return r7
        L_0x001c:
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r1 = r6.zzdpk
            r3 = 1
            java.lang.String r4 = "com.google.android.gms.measurement.AppMeasurement"
            boolean r1 = r6.zza(r7, r4, r1, r3)
            if (r1 != 0) goto L_0x0028
            return r2
        L_0x0028:
            r1 = 0
            java.lang.reflect.Method r3 = r6.zzm(r7, r0)     // Catch:{ Exception -> 0x0056 }
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r4 = r6.zzdpk     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r4 = r4.get()     // Catch:{ Exception -> 0x0056 }
            java.lang.Object[] r5 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r3 = r3.invoke(r4, r5)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0056 }
            if (r3 != 0) goto L_0x0052
            java.lang.String r3 = "getCurrentScreenClass"
            java.lang.reflect.Method r7 = r6.zzm(r7, r3)     // Catch:{ Exception -> 0x0056 }
            java.util.concurrent.atomic.AtomicReference<java.lang.Object> r3 = r6.zzdpk     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r3 = r3.get()     // Catch:{ Exception -> 0x0056 }
            java.lang.Object[] r4 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0056 }
            java.lang.Object r7 = r7.invoke(r3, r4)     // Catch:{ Exception -> 0x0056 }
            r3 = r7
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0056 }
        L_0x0052:
            if (r3 == 0) goto L_0x0055
            return r3
        L_0x0055:
            return r2
        L_0x0056:
            r7 = move-exception
            r6.zza(r7, r0, r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzatv.zzad(android.content.Context):java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    public final void zzg(Context context, String str) {
        if (!zzab(context) || !(context instanceof Activity)) {
            return;
        }
        if (zzac(context)) {
            zza("setScreenName", new zzaud(context, str));
        } else if (zza(context, "com.google.firebase.analytics.FirebaseAnalytics", this.zzdpl, false)) {
            Method zzn = zzn(context, "setCurrentScreen");
            try {
                zzn.invoke(this.zzdpl.get(), (Activity) context, str, context.getPackageName());
            } catch (Exception e) {
                zza(e, "setCurrentScreen", false);
            }
        }
    }

    public final String zzae(Context context) {
        if (!zzab(context)) {
            return null;
        }
        synchronized (this.zzdpf) {
            if (this.zzdpg != null) {
                String str = this.zzdpg;
                return str;
            }
            if (zzac(context)) {
                this.zzdpg = (String) zza("getGmpAppId", this.zzdpg, zzauc.zzdpr);
            } else {
                this.zzdpg = (String) zza("getGmpAppId", context);
            }
            String str2 = this.zzdpg;
            return str2;
        }
    }

    public final String zzaf(Context context) {
        if (!zzab(context)) {
            return null;
        }
        long longValue = ((Long) zzve.zzoy().zzd(zzzn.zzcie)).longValue();
        if (zzac(context)) {
            if (longValue >= 0) {
                return (String) zzuq().submit(new zzaue(this)).get(longValue, TimeUnit.MILLISECONDS);
            }
            try {
                return (String) zza("getAppInstanceId", (Object) null, zzauf.zzdpr);
            } catch (TimeoutException unused) {
                return "TIME_OUT";
            } catch (Exception unused2) {
                return null;
            }
        } else if (longValue < 0) {
            return (String) zza("getAppInstanceId", context);
        } else {
            try {
                return (String) zzuq().submit(new zzauh(this, context)).get(longValue, TimeUnit.MILLISECONDS);
            } catch (TimeoutException unused3) {
                return "TIME_OUT";
            } catch (Exception unused4) {
                return null;
            }
        }
    }

    public final String zzag(Context context) {
        if (!zzab(context)) {
            return null;
        }
        if (zzac(context)) {
            Long l = (Long) zza("getAdEventId", (Object) null, zzaug.zzdpr);
            if (l != null) {
                return Long.toString(l.longValue());
            }
            return null;
        }
        Object zza = zza("generateEventId", context);
        if (zza != null) {
            return zza.toString();
        }
        return null;
    }

    public final String zzah(Context context) {
        if (!zzab(context)) {
            return null;
        }
        synchronized (this.zzdpf) {
            if (this.zzdph != null) {
                String str = this.zzdph;
                return str;
            }
            if (zzac(context)) {
                this.zzdph = (String) zza("getAppIdOrigin", this.zzdph, zzatx.zzdpr);
            } else {
                this.zzdph = "fa";
            }
            String str2 = this.zzdph;
            return str2;
        }
    }

    public final void zzh(Context context, String str) {
        zza(context, "_ac", str, (Bundle) null);
    }

    public final void zzi(Context context, String str) {
        zza(context, "_ai", str, (Bundle) null);
    }

    public final void zzj(Context context, String str) {
        zza(context, "_aq", str, (Bundle) null);
    }

    public final void zzk(Context context, String str) {
        zza(context, "_aa", str, (Bundle) null);
    }

    public final void zza(Context context, String str, String str2, String str3, int i) {
        if (zzab(context)) {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str2);
            bundle.putString("reward_type", str3);
            bundle.putInt("reward_value", i);
            zza(context, "_ar", str, bundle);
            StringBuilder sb = new StringBuilder(String.valueOf(str3).length() + 75);
            sb.append("Log a Firebase reward video event, reward type: ");
            sb.append(str3);
            sb.append(", reward value: ");
            sb.append(i);
            zzavs.zzed(sb.toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final void zza(Context context, String str, String str2, Bundle bundle) {
        if (zzab(context)) {
            Bundle zzl = zzl(str2, str);
            if (bundle != null) {
                zzl.putAll(bundle);
            }
            if (zzac(context)) {
                zza("logEventInternal", new zzatw(str, zzl));
            } else if (zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzdpk, true)) {
                Method zzai = zzai(context);
                try {
                    zzai.invoke(this.zzdpk.get(), "am", str, zzl);
                } catch (Exception e) {
                    zza(e, "logEventInternal", true);
                }
            }
        }
    }

    private static Bundle zzl(String str, String str2) {
        Bundle bundle = new Bundle();
        try {
            bundle.putLong("_aeid", Long.parseLong(str));
        } catch (NullPointerException | NumberFormatException e) {
            String valueOf = String.valueOf(str);
            zzavs.zzc(valueOf.length() != 0 ? "Invalid event ID: ".concat(valueOf) : new String("Invalid event ID: "), e);
        }
        if ("_ac".equals(str2)) {
            bundle.putInt("_r", 1);
        }
        return bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Method zzai(Context context) {
        Method method = this.zzdpm.get("logEventInternal");
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod("logEventInternal", String.class, String.class, Bundle.class);
            this.zzdpm.put("logEventInternal", declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, "logEventInternal", true);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Method zzl(Context context, String str) {
        Method method = this.zzdpm.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, String.class);
            this.zzdpm.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Method zzm(Context context, String str) {
        Method method = this.zzdpm.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement").getDeclaredMethod(str, new Class[0]);
            this.zzdpm.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Method zzn(Context context, String str) {
        Method method = this.zzdpm.get(str);
        if (method != null) {
            return method;
        }
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics").getDeclaredMethod(str, Activity.class, String.class, String.class);
            this.zzdpm.put(str, declaredMethod);
            return declaredMethod;
        } catch (Exception e) {
            zza(e, str, false);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final void zza(Context context, String str, String str2) {
        if (zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzdpk, true)) {
            Method zzl = zzl(context, str2);
            try {
                zzl.invoke(this.zzdpk.get(), str);
                StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 37 + String.valueOf(str).length());
                sb.append("Invoke Firebase method ");
                sb.append(str2);
                sb.append(", Ad Unit Id: ");
                sb.append(str);
                zzavs.zzed(sb.toString());
            } catch (Exception e) {
                zza(e, str2, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.util.concurrent.atomic.AtomicReference<java.lang.Object>, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final Object zza(String str, Context context) {
        if (!zza(context, "com.google.android.gms.measurement.AppMeasurement", this.zzdpk, true)) {
            return null;
        }
        try {
            return zzm(context, str).invoke(this.zzdpk.get(), new Object[0]);
        } catch (Exception e) {
            zza(e, str, true);
            return null;
        }
    }

    private final void zza(Exception exc, String str, boolean z) {
        if (!this.zzdpi.get()) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 30);
            sb.append("Invoke Firebase method ");
            sb.append(str);
            sb.append(" error.");
            zzavs.zzez(sb.toString());
            if (z) {
                zzavs.zzez("The Google Mobile Ads SDK will not integrate with Firebase. Admob/Firebase integration requires the latest Firebase SDK jar, but Firebase SDK is either missing or out of date");
                this.zzdpi.set(true);
            }
        }
    }

    private final ThreadPoolExecutor zzuq() {
        if (this.zzdpe.get() == null) {
            this.zzdpe.compareAndSet(null, new ThreadPoolExecutor(((Integer) zzve.zzoy().zzd(zzzn.zzcif)).intValue(), ((Integer) zzve.zzoy().zzd(zzzn.zzcif)).intValue(), 1, TimeUnit.MINUTES, new LinkedBlockingQueue(), new zzauj(this)));
        }
        return this.zzdpe.get();
    }

    private final boolean zza(Context context, String str, AtomicReference<Object> atomicReference, boolean z) {
        if (atomicReference.get() == null) {
            try {
                atomicReference.compareAndSet(null, context.getClassLoader().loadClass(str).getDeclaredMethod("getInstance", Context.class).invoke(null, context));
            } catch (Exception e) {
                zza(e, "getInstance", z);
                return false;
            }
        }
        return true;
    }

    private final void zza(String str, zzaul zzaul) {
        synchronized (this.zzdpn) {
            FutureTask futureTask = new FutureTask(new zzatz(this, zzaul, str), null);
            if (this.zzdpn.get() != null) {
                futureTask.run();
            } else {
                this.zzdpo.offer(futureTask);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    private final <T> T zza(String str, T t, zzaui<T> zzaui) {
        synchronized (this.zzdpn) {
            if (this.zzdpn.get() != null) {
                try {
                    T zzb = zzaui.zzb(this.zzdpn.get());
                    return zzb;
                } catch (Exception e) {
                    zza(e, str, false);
                    return t;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void
     arg types: [java.lang.Exception, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.String, java.lang.Object, com.google.android.gms.internal.ads.zzaui):T
      com.google.android.gms.internal.ads.zzatv.zza(android.content.Context, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.ads.zzatv.zza(java.lang.Exception, java.lang.String, boolean):void */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzaul zzaul, String str) {
        if (this.zzdpn.get() != null) {
            try {
                zzaul.zza(this.zzdpn.get());
            } catch (Exception e) {
                zza(e, str, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ String zzaj(Context context) throws Exception {
        return (String) zza("getAppInstanceId", context);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ String zzur() throws Exception {
        return (String) zza("getAppInstanceId", (Object) null, zzaty.zzdpr);
    }
}
