package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdsu implements zzdtb {
    zzdsu() {
    }

    public final boolean zze(Class<?> cls) {
        return false;
    }

    public final zzdtc zzf(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
