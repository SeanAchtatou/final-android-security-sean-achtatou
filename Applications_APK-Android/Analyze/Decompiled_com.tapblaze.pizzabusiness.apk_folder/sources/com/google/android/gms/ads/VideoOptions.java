package com.google.android.gms.ads;

import com.google.android.gms.internal.ads.zzyw;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class VideoOptions {
    private final boolean zzabv;
    private final boolean zzabw;
    private final boolean zzabx;

    public VideoOptions(zzyw zzyw) {
        this.zzabv = zzyw.zzabv;
        this.zzabw = zzyw.zzabw;
        this.zzabx = zzyw.zzabx;
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static final class Builder {
        /* access modifiers changed from: private */
        public boolean zzabv = true;
        /* access modifiers changed from: private */
        public boolean zzabw = false;
        /* access modifiers changed from: private */
        public boolean zzabx = false;

        public final Builder setStartMuted(boolean z) {
            this.zzabv = z;
            return this;
        }

        public final Builder setCustomControlsRequested(boolean z) {
            this.zzabw = z;
            return this;
        }

        public final Builder setClickToExpandRequested(boolean z) {
            this.zzabx = z;
            return this;
        }

        public final VideoOptions build() {
            return new VideoOptions(this);
        }
    }

    private VideoOptions(Builder builder) {
        this.zzabv = builder.zzabv;
        this.zzabw = builder.zzabw;
        this.zzabx = builder.zzabx;
    }

    public final boolean getStartMuted() {
        return this.zzabv;
    }

    public final boolean getCustomControlsRequested() {
        return this.zzabw;
    }

    public final boolean getClickToExpandRequested() {
        return this.zzabx;
    }
}
