package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaid implements Runnable {
    private final zzaie zzcyu;
    private final zzafn zzcyv;
    private final Map zzcyw;

    zzaid(zzaie zzaie, zzafn zzafn, Map map) {
        this.zzcyu = zzaie;
        this.zzcyv = zzafn;
        this.zzcyw = map;
    }

    public final void run() {
        this.zzcyu.zza(this.zzcyv, this.zzcyw);
    }
}
