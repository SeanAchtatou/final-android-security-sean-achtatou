package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbpq implements zzbrn {
    static final zzbrn zzfhp = new zzbpq();

    private zzbpq() {
    }

    public final void zzp(Object obj) {
        ((zzbov) obj).onRewardedVideoStarted();
    }
}
