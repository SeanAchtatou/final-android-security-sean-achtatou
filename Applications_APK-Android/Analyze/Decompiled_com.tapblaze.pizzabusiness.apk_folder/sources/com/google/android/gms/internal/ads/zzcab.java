package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcab extends zzacz {
    private final String zzfge;
    private final zzbws zzfkc;
    private final zzbwk zzfnf;

    public zzcab(String str, zzbwk zzbwk, zzbws zzbws) {
        this.zzfge = str;
        this.zzfnf = zzbwk;
        this.zzfkc = zzbws;
    }

    public final IObjectWrapper zzrf() throws RemoteException {
        return ObjectWrapper.wrap(this.zzfnf);
    }

    public final String getHeadline() throws RemoteException {
        return this.zzfkc.getHeadline();
    }

    public final List<?> getImages() throws RemoteException {
        return this.zzfkc.getImages();
    }

    public final String getBody() throws RemoteException {
        return this.zzfkc.getBody();
    }

    public final zzaci zzrj() throws RemoteException {
        return this.zzfkc.zzrj();
    }

    public final String getCallToAction() throws RemoteException {
        return this.zzfkc.getCallToAction();
    }

    public final String getAdvertiser() throws RemoteException {
        return this.zzfkc.getAdvertiser();
    }

    public final Bundle getExtras() throws RemoteException {
        return this.zzfkc.getExtras();
    }

    public final void destroy() throws RemoteException {
        this.zzfnf.destroy();
    }

    public final zzxb getVideoController() throws RemoteException {
        return this.zzfkc.getVideoController();
    }

    public final void performClick(Bundle bundle) throws RemoteException {
        this.zzfnf.zzf(bundle);
    }

    public final boolean recordImpression(Bundle bundle) throws RemoteException {
        return this.zzfnf.zzh(bundle);
    }

    public final void reportTouchEvent(Bundle bundle) throws RemoteException {
        this.zzfnf.zzg(bundle);
    }

    public final zzaca zzrh() throws RemoteException {
        return this.zzfkc.zzrh();
    }

    public final IObjectWrapper zzri() throws RemoteException {
        return this.zzfkc.zzri();
    }

    public final String getMediationAdapterClassName() throws RemoteException {
        return this.zzfge;
    }
}
