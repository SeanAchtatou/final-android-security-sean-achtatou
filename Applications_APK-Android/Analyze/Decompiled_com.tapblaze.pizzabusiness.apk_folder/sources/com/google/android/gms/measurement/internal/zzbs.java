package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzjf;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class zzbs implements zzes {
    static final zzes zza = new zzbs();

    private zzbs() {
    }

    public final Object zza() {
        return zzjf.zze();
    }
}
