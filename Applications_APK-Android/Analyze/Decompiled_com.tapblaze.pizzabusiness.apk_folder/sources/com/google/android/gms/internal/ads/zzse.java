package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Binder;
import com.google.android.gms.ads.internal.zzq;
import java.io.InputStream;
import java.util.concurrent.Future;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzse {
    /* access modifiers changed from: private */
    public final Object lock = new Object();
    /* access modifiers changed from: private */
    public zzrz zzbrd;
    /* access modifiers changed from: private */
    public boolean zzbrq;
    private final Context zzup;

    zzse(Context context) {
        this.zzup = context;
    }

    /* access modifiers changed from: package-private */
    public final Future<InputStream> zzb(zzry zzry) {
        zzsh zzsh = new zzsh(this);
        zzsg zzsg = new zzsg(this, zzry, zzsh);
        zzsk zzsk = new zzsk(this, zzsh);
        synchronized (this.lock) {
            this.zzbrd = new zzrz(this.zzup, zzq.zzle().zzxb(), zzsg, zzsk);
            this.zzbrd.checkAvailabilityAndConnect();
        }
        return zzsh;
    }

    /* access modifiers changed from: private */
    public final void disconnect() {
        synchronized (this.lock) {
            if (this.zzbrd != null) {
                this.zzbrd.disconnect();
                this.zzbrd = null;
                Binder.flushPendingCommands();
            }
        }
    }
}
