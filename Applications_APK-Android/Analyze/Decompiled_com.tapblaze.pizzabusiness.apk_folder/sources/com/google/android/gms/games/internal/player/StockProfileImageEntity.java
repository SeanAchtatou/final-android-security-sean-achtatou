package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.games.internal.zzc;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class StockProfileImageEntity extends zzc implements StockProfileImage {
    public static final Parcelable.Creator<StockProfileImageEntity> CREATOR = new zzf();
    private final Uri zzfw;
    private final String zzns;

    public StockProfileImageEntity(String str, Uri uri) {
        this.zzns = str;
        this.zzfw = uri;
    }

    public final /* bridge */ /* synthetic */ Object freeze() {
        return this;
    }

    public final boolean isDataValid() {
        return true;
    }

    public final String getImageUrl() {
        return this.zzns;
    }

    public final Uri zzal() {
        return this.zzfw;
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzns, this.zzfw);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof StockProfileImage)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        StockProfileImage stockProfileImage = (StockProfileImage) obj;
        return Objects.equal(this.zzns, stockProfileImage.getImageUrl()) && Objects.equal(this.zzfw, stockProfileImage.zzal());
    }

    public final String toString() {
        return Objects.toStringHelper(this).add("ImageId", this.zzns).add("ImageUri", this.zzfw).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, getImageUrl(), false);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzfw, i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
