package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.MuteThisAdListener;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzws extends zzwq {
    private final MuteThisAdListener zzceb;

    public zzws(MuteThisAdListener muteThisAdListener) {
        this.zzceb = muteThisAdListener;
    }

    public final void onAdMuted() {
        this.zzceb.onAdMuted();
    }
}
