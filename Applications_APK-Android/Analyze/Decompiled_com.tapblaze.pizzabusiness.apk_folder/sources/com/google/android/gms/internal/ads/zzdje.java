package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdje extends zzdik<zzdhx, zzdlf> {
    zzdje(Class cls) {
        super(cls);
    }

    public final /* synthetic */ Object zzak(Object obj) throws GeneralSecurityException {
        zzdlf zzdlf = (zzdlf) obj;
        return new zzdoz((zzdpi) new zzdjg().zza(zzdlf.zzata(), zzdpi.class), (zzdio) new zzdkq().zza(zzdlf.zzatb(), zzdio.class), zzdlf.zzatb().zzauz().zzasx());
    }
}
