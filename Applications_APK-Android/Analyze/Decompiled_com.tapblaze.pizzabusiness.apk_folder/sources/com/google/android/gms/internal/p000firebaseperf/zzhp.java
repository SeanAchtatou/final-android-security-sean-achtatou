package com.google.android.gms.internal.p000firebaseperf;

import cz.msebera.android.httpclient.message.TokenParser;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhp  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhp {
    static String zzd(zzeb zzeb) {
        zzhs zzhs = new zzhs(zzeb);
        StringBuilder sb = new StringBuilder(zzhs.size());
        for (int i = 0; i < zzhs.size(); i++) {
            byte zzq = zzhs.zzq(i);
            if (zzq == 34) {
                sb.append("\\\"");
            } else if (zzq == 39) {
                sb.append("\\'");
            } else if (zzq != 92) {
                switch (zzq) {
                    case 7:
                        sb.append("\\a");
                        continue;
                    case 8:
                        sb.append("\\b");
                        continue;
                    case 9:
                        sb.append("\\t");
                        continue;
                    case 10:
                        sb.append("\\n");
                        continue;
                    case 11:
                        sb.append("\\v");
                        continue;
                    case 12:
                        sb.append("\\f");
                        continue;
                    case 13:
                        sb.append("\\r");
                        continue;
                    default:
                        if (zzq < 32 || zzq > 126) {
                            sb.append((char) TokenParser.ESCAPE);
                            sb.append((char) (((zzq >>> 6) & 3) + 48));
                            sb.append((char) (((zzq >>> 3) & 7) + 48));
                            sb.append((char) ((zzq & 7) + 48));
                            break;
                        } else {
                            sb.append((char) zzq);
                            continue;
                        }
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }
}
