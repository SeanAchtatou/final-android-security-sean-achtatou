package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzceb implements zzdxg<zzcec> {
    private final zzdxp<Clock> zzfcz;

    public zzceb(zzdxp<Clock> zzdxp) {
        this.zzfcz = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcec(this.zzfcz.get());
    }
}
