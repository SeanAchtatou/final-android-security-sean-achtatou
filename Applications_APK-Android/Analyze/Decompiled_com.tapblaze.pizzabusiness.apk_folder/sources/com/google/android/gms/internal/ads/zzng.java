package com.google.android.gms.internal.ads;

import com.ironsource.mediationsdk.logger.IronSourceError;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzng {
    public final int length;
    private int zzafx;
    private final zzne[] zzbee;

    public zzng(zzne... zzneArr) {
        this.zzbee = zzneArr;
        this.length = zzneArr.length;
    }

    public final zzne zzay(int i) {
        return this.zzbee[i];
    }

    public final zzne[] zzie() {
        return (zzne[]) this.zzbee.clone();
    }

    public final int hashCode() {
        if (this.zzafx == 0) {
            this.zzafx = Arrays.hashCode(this.zzbee) + IronSourceError.ERROR_NON_EXISTENT_INSTANCE;
        }
        return this.zzafx;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.zzbee, ((zzng) obj).zzbee);
    }
}
