package com.google.android.gms.internal.ads;

import android.view.View;
import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaad extends zzaai {
    private final zze zzcry;
    private final String zzcrz;
    private final String zzcsa;

    public zzaad(zze zze, String str, String str2) {
        this.zzcry = zze;
        this.zzcrz = str;
        this.zzcsa = str2;
    }

    public final String zzqs() {
        return this.zzcrz;
    }

    public final String getContent() {
        return this.zzcsa;
    }

    public final void zzn(IObjectWrapper iObjectWrapper) {
        if (iObjectWrapper != null) {
            this.zzcry.zzg((View) ObjectWrapper.unwrap(iObjectWrapper));
        }
    }

    public final void recordClick() {
        this.zzcry.zzjr();
    }

    public final void recordImpression() {
        this.zzcry.zzjs();
    }
}
