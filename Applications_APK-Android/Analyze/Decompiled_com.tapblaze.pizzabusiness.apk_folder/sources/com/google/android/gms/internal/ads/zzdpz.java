package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdpz;
import com.google.android.gms.internal.ads.zzdqa;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzdpz<MessageType extends zzdqa<MessageType, BuilderType>, BuilderType extends zzdpz<MessageType, BuilderType>> implements zzdtd {
    /* access modifiers changed from: protected */
    public abstract BuilderType zza(zzdqa zzdqa);

    public abstract BuilderType zza(zzdqw zzdqw, zzdrg zzdrg) throws IOException;

    /* renamed from: zzaxj */
    public abstract BuilderType clone();

    public BuilderType zza(byte[] bArr, int i, int i2, zzdrg zzdrg) throws zzdse {
        try {
            zzdqw zzb = zzdqw.zzb(bArr, 0, i2, false);
            zza(zzb, zzdrg);
            zzb.zzfh(0);
            return this;
        } catch (zzdse e) {
            throw e;
        } catch (IOException e2) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 60 + "byte array".length());
            sb.append("Reading ");
            sb.append(name);
            sb.append(" from a ");
            sb.append("byte array");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e2);
        }
    }

    public final /* synthetic */ zzdtd zzf(zzdte zzdte) {
        if (zzazz().getClass().isInstance(zzdte)) {
            return zza((zzdqa) zzdte);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
