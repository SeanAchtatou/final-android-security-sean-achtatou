package com.google.android.gms.ads.internal;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzn implements View.OnTouchListener {
    private final /* synthetic */ zzl zzblk;

    zzn(zzl zzl) {
        this.zzblk = zzl;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.zzblk.zzblr == null) {
            return false;
        }
        this.zzblk.zzblr.zza(motionEvent);
        return false;
    }
}
