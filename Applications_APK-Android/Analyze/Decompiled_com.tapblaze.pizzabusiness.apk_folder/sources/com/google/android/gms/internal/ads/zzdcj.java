package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdcj<O> {
    private final E zzgpx;
    private final String zzgpy;
    private final List<zzdhe<?>> zzgqc;
    final /* synthetic */ zzdcd zzgqd;
    private final zzdhe<?> zzgqg;
    private final zzdhe<O> zzgqh;

    private zzdcj(zzdcd zzdcd, E e, String str, zzdhe<?> zzdhe, List<zzdhe<?>> list, zzdhe<O> zzdhe2) {
        this.zzgqd = zzdcd;
        this.zzgpx = e;
        this.zzgpy = str;
        this.zzgqg = zzdhe;
        this.zzgqc = list;
        this.zzgqh = zzdhe2;
    }

    public final zzdcj<O> zzgn(String str) {
        return new zzdcj(this.zzgqd, this.zzgpx, str, this.zzgqg, this.zzgqc, this.zzgqh);
    }

    public final <O2> zzdcj<O2> zzb(zzdby<O, O2> zzdby) {
        return zza(new zzdci(zzdby));
    }

    public final <O2> zzdcj<O2> zza(zzdgf<O, O2> zzdgf) {
        return zza(zzdgf, this.zzgqd.zzfov);
    }

    private final <O2> zzdcj<O2> zza(zzdgf zzdgf, Executor executor) {
        return new zzdcj(this.zzgqd, this.zzgpx, this.zzgpy, this.zzgqg, this.zzgqc, zzdgs.zzb(this.zzgqh, zzdgf, executor));
    }

    public final <O2> zzdcj<O2> zzc(zzdhe<O2> zzdhe) {
        return zza(new zzdcl(zzdhe), zzazd.zzdwj);
    }

    public final <T extends Throwable> zzdcj<O> zza(Class cls, zzdby zzdby) {
        return zza(cls, new zzdck(zzdby));
    }

    public final <T extends Throwable> zzdcj<O> zza(Class cls, zzdgf zzdgf) {
        zzdcd zzdcd = this.zzgqd;
        return new zzdcj(zzdcd, this.zzgpx, this.zzgpy, this.zzgqg, this.zzgqc, zzdgs.zzb(this.zzgqh, cls, zzdgf, zzdcd.zzfov));
    }

    public final zzdcj<O> zza(long j, TimeUnit timeUnit) {
        zzdcd zzdcd = this.zzgqd;
        return new zzdcj(zzdcd, this.zzgpx, this.zzgpy, this.zzgqg, this.zzgqc, zzdgs.zza(this.zzgqh, j, timeUnit, zzdcd.zzfdi));
    }

    public final zzdca<E, O> zzaqg() {
        E e = this.zzgpx;
        String str = this.zzgpy;
        if (str == null) {
            str = this.zzgqd.zzv(e);
        }
        zzdca<E, O> zzdca = new zzdca<>(e, str, this.zzgqh);
        this.zzgqd.zzgqb.zza(zzdca);
        this.zzgqg.addListener(new zzdcn(this, zzdca), zzazd.zzdwj);
        zzdgs.zza(zzdca, new zzdcm(this, zzdca), zzazd.zzdwj);
        return zzdca;
    }

    public final zzdcj<O> zzw(E e) {
        return this.zzgqd.zza(e, zzaqg());
    }
}
