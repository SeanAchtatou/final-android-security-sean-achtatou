package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbrn<ListenerT> {
    void zzp(ListenerT listenert) throws Exception;
}
