package com.google.android.gms.internal.p000firebaseperf;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzfe  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzfe extends zzdw<Integer> implements zzfk, zzgy, RandomAccess {
    private static final zzfe zzqt;
    private int size;
    private int[] zzqu;

    public static zzfe zzhq() {
        return zzqt;
    }

    zzfe() {
        this(new int[10], 0);
    }

    private zzfe(int[] iArr, int i) {
        this.zzqu = iArr;
        this.size = i;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        zzgh();
        if (i2 >= i) {
            int[] iArr = this.zzqu;
            System.arraycopy(iArr, i2, iArr, i, this.size - i2);
            this.size -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzfe)) {
            return super.equals(obj);
        }
        zzfe zzfe = (zzfe) obj;
        if (this.size != zzfe.size) {
            return false;
        }
        int[] iArr = zzfe.zzqu;
        for (int i = 0; i < this.size; i++) {
            if (this.zzqu[i] != iArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.size; i2++) {
            i = (i * 31) + this.zzqu[i2];
        }
        return i;
    }

    /* renamed from: zzak */
    public final zzfk zzao(int i) {
        if (i >= this.size) {
            return new zzfe(Arrays.copyOf(this.zzqu, i), this.size);
        }
        throw new IllegalArgumentException();
    }

    public final int getInt(int i) {
        zzam(i);
        return this.zzqu[i];
    }

    public final int size() {
        return this.size;
    }

    public final void zzal(int i) {
        zzgh();
        int i2 = this.size;
        int[] iArr = this.zzqu;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[(((i2 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.zzqu = iArr2;
        }
        int[] iArr3 = this.zzqu;
        int i3 = this.size;
        this.size = i3 + 1;
        iArr3[i3] = i;
    }

    public final boolean addAll(Collection<? extends Integer> collection) {
        zzgh();
        zzfg.checkNotNull(collection);
        if (!(collection instanceof zzfe)) {
            return super.addAll(collection);
        }
        zzfe zzfe = (zzfe) collection;
        int i = zzfe.size;
        if (i == 0) {
            return false;
        }
        int i2 = this.size;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.zzqu;
            if (i3 > iArr.length) {
                this.zzqu = Arrays.copyOf(iArr, i3);
            }
            System.arraycopy(zzfe.zzqu, 0, this.zzqu, this.size, zzfe.size);
            this.size = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean remove(Object obj) {
        zzgh();
        for (int i = 0; i < this.size; i++) {
            if (obj.equals(Integer.valueOf(this.zzqu[i]))) {
                int[] iArr = this.zzqu;
                System.arraycopy(iArr, i + 1, iArr, i, (this.size - i) - 1);
                this.size--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    private final void zzam(int i) {
        if (i < 0 || i >= this.size) {
            throw new IndexOutOfBoundsException(zzan(i));
        }
    }

    private final String zzan(int i) {
        int i2 = this.size;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        int intValue = ((Integer) obj).intValue();
        zzgh();
        zzam(i);
        int[] iArr = this.zzqu;
        int i2 = iArr[i];
        iArr[i] = intValue;
        return Integer.valueOf(i2);
    }

    public final /* synthetic */ Object remove(int i) {
        zzgh();
        zzam(i);
        int[] iArr = this.zzqu;
        int i2 = iArr[i];
        int i3 = this.size;
        if (i < i3 - 1) {
            System.arraycopy(iArr, i + 1, iArr, i, (i3 - i) - 1);
        }
        this.size--;
        this.modCount++;
        return Integer.valueOf(i2);
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        int intValue = ((Integer) obj).intValue();
        zzgh();
        if (i < 0 || i > (i2 = this.size)) {
            throw new IndexOutOfBoundsException(zzan(i));
        }
        int[] iArr = this.zzqu;
        if (i2 < iArr.length) {
            System.arraycopy(iArr, i, iArr, i + 1, i2 - i);
        } else {
            int[] iArr2 = new int[(((i2 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i);
            System.arraycopy(this.zzqu, i, iArr2, i + 1, this.size - i);
            this.zzqu = iArr2;
        }
        this.zzqu[i] = intValue;
        this.size++;
        this.modCount++;
    }

    public final /* synthetic */ boolean add(Object obj) {
        zzal(((Integer) obj).intValue());
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        return Integer.valueOf(getInt(i));
    }

    static {
        zzfe zzfe = new zzfe(new int[0], 0);
        zzqt = zzfe;
        zzfe.zzgg();
    }
}
