package com.google.android.gms.internal.ads;

import android.view.MotionEvent;
import android.view.ViewGroup;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbxo implements zzabw {
    private final /* synthetic */ zzbxz zzfns;
    private final /* synthetic */ ViewGroup zzfnt;
    private final /* synthetic */ zzbxj zzfnu;

    zzbxo(zzbxj zzbxj, zzbxz zzbxz, ViewGroup viewGroup) {
        this.zzfnu = zzbxj;
        this.zzfns = zzbxz;
        this.zzfnt = viewGroup;
    }

    public final void zzrd() {
        if (zzbxj.zza(this.zzfns, zzbxh.zzfna)) {
            this.zzfns.onClick(this.zzfnt);
        }
    }

    public final void zzc(MotionEvent motionEvent) {
        this.zzfns.onTouch(null, motionEvent);
    }
}
