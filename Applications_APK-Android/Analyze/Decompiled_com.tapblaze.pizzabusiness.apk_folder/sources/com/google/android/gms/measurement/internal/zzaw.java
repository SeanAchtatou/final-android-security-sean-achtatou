package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.zzkw;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class zzaw implements zzes {
    static final zzes zza = new zzaw();

    private zzaw() {
    }

    public final Object zza() {
        return Boolean.valueOf(zzkw.zzc());
    }
}
