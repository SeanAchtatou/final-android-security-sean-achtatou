package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdfs;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdhl<V> extends zzdfs.zzj<V> {
    public static <V> zzdhl<V> zzarx() {
        return new zzdhl<>();
    }

    public final boolean set(@NullableDecl V v) {
        return super.set(v);
    }

    public final boolean setException(Throwable th) {
        return super.setException(th);
    }

    private zzdhl() {
    }
}
