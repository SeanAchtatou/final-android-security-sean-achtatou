package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.concurrent.Callable;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbyp implements Callable {
    private final zzczl zzfel;
    private final zzbyq zzfos;
    private final zzczt zzfot;
    private final JSONObject zzfou;

    zzbyp(zzbyq zzbyq, zzczt zzczt, zzczl zzczl, JSONObject jSONObject) {
        this.zzfos = zzbyq;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
        this.zzfou = jSONObject;
    }

    public final Object call() {
        zzbyq zzbyq = this.zzfos;
        zzczt zzczt = this.zzfot;
        zzczl zzczl = this.zzfel;
        JSONObject jSONObject = this.zzfou;
        zzbws zzbws = new zzbws();
        zzbws.zzdj(jSONObject.optInt(ReportDBAdapter.ReportColumns.COLUMN_TEMPATE_ID, -1));
        zzbws.zzfw(jSONObject.optString("custom_template_id"));
        JSONObject optJSONObject = jSONObject.optJSONObject("omid_settings");
        zzbws.zzfx(optJSONObject != null ? optJSONObject.optString("omid_partner_name") : null);
        zzczu zzczu = zzczt.zzgmh.zzfgl;
        if (zzczu.zzgmn.contains(Integer.toString(zzbws.zzaja()))) {
            if (zzbws.zzaja() == 3) {
                if (zzbws.getCustomTemplateId() == null) {
                    throw new zzclr("No custom template id for custom template ad response.", 0);
                } else if (!zzczu.zzgmo.contains(zzbws.getCustomTemplateId())) {
                    throw new zzclr("Unexpected custom template id in the response.", 0);
                }
            }
            zzbws.setStarRating(jSONObject.optDouble("rating", -1.0d));
            String optString = jSONObject.optString("headline", null);
            if (zzczl.zzdmf) {
                zzq.zzkq();
                String zzwn = zzawb.zzwn();
                StringBuilder sb = new StringBuilder(String.valueOf(zzwn).length() + 3 + String.valueOf(optString).length());
                sb.append(zzwn);
                sb.append(" : ");
                sb.append(optString);
                optString = sb.toString();
            }
            zzbws.zzn("headline", optString);
            zzbws.zzn("body", jSONObject.optString("body", null));
            zzbws.zzn("call_to_action", jSONObject.optString("call_to_action", null));
            zzbws.zzn("store", jSONObject.optString("store", null));
            zzbws.zzn("price", jSONObject.optString("price", null));
            zzbws.zzn("advertiser", jSONObject.optString("advertiser", null));
            return zzbws;
        }
        int zzaja = zzbws.zzaja();
        StringBuilder sb2 = new StringBuilder(32);
        sb2.append("Invalid template ID: ");
        sb2.append(zzaja);
        throw new zzclr(sb2.toString(), 0);
    }
}
