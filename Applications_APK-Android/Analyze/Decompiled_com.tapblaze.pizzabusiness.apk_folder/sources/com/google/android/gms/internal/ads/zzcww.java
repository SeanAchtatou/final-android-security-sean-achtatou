package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzcww {
    zzcwx zzaeb();

    zzcww zzbt(Context context);

    zzcww zzfq(String str);
}
