package com.google.android.gms.internal.ads;

import android.media.AudioTrack;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzhz extends Thread {
    private final /* synthetic */ AudioTrack zzajz;
    private final /* synthetic */ zzhw zzaka;

    zzhz(zzhw zzhw, AudioTrack audioTrack) {
        this.zzaka = zzhw;
        this.zzajz = audioTrack;
    }

    public final void run() {
        try {
            this.zzajz.flush();
            this.zzajz.release();
        } finally {
            this.zzaka.zzahw.open();
        }
    }
}
