package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.ironsource.mediationsdk.logger.IronSourceError;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzli extends zzll {
    public static final Parcelable.Creator<zzli> CREATOR = new zzlh();
    private final String description;
    private final String mimeType;
    private final int zzazq;
    private final byte[] zzazr;

    public zzli(String str, String str2, int i, byte[] bArr) {
        super("APIC");
        this.mimeType = str;
        this.description = null;
        this.zzazq = 3;
        this.zzazr = bArr;
    }

    zzli(Parcel parcel) {
        super("APIC");
        this.mimeType = parcel.readString();
        this.description = parcel.readString();
        this.zzazq = parcel.readInt();
        this.zzazr = parcel.createByteArray();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            zzli zzli = (zzli) obj;
            return this.zzazq == zzli.zzazq && zzoq.zza(this.mimeType, zzli.mimeType) && zzoq.zza(this.description, zzli.description) && Arrays.equals(this.zzazr, zzli.zzazr);
        }
    }

    public final int hashCode() {
        int i = (this.zzazq + IronSourceError.ERROR_NON_EXISTENT_INSTANCE) * 31;
        String str = this.mimeType;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.description;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return ((hashCode + i2) * 31) + Arrays.hashCode(this.zzazr);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mimeType);
        parcel.writeString(this.description);
        parcel.writeInt(this.zzazq);
        parcel.writeByteArray(this.zzazr);
    }
}
