package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdna;
import com.google.android.gms.internal.ads.zzdte;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzdii<KeyProtoT extends zzdte> {
    private final Class<KeyProtoT> zzgxv;
    private final Map<Class<?>, zzdik<?, KeyProtoT>> zzgxw;
    private final Class<?> zzgxx;

    @SafeVarargs
    protected zzdii(Class<KeyProtoT> cls, zzdik<?, KeyProtoT>... zzdikArr) {
        this.zzgxv = cls;
        HashMap hashMap = new HashMap();
        int length = zzdikArr.length;
        int i = 0;
        while (i < length) {
            zzdik<?, KeyProtoT> zzdik = zzdikArr[i];
            if (hashMap.containsKey(zzdik.zzarz())) {
                String valueOf = String.valueOf(zzdik.zzarz().getCanonicalName());
                throw new IllegalArgumentException(valueOf.length() != 0 ? "KeyTypeManager constructed with duplicate factories for primitive ".concat(valueOf) : new String("KeyTypeManager constructed with duplicate factories for primitive "));
            } else {
                hashMap.put(zzdik.zzarz(), zzdik);
                i++;
            }
        }
        if (zzdikArr.length > 0) {
            this.zzgxx = zzdikArr[0].zzarz();
        } else {
            this.zzgxx = Void.class;
        }
        this.zzgxw = Collections.unmodifiableMap(hashMap);
    }

    public abstract String getKeyType();

    public abstract zzdna.zzb zzasd();

    public abstract void zze(KeyProtoT keyprotot) throws GeneralSecurityException;

    public abstract KeyProtoT zzr(zzdqk zzdqk) throws zzdse;

    public final Class<KeyProtoT> zzasc() {
        return this.zzgxv;
    }

    public final <P> P zza(KeyProtoT keyprotot, Class<P> cls) throws GeneralSecurityException {
        zzdik zzdik = this.zzgxw.get(cls);
        if (zzdik != null) {
            return zzdik.zzak(keyprotot);
        }
        String canonicalName = cls.getCanonicalName();
        StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 41);
        sb.append("Requested primitive class ");
        sb.append(canonicalName);
        sb.append(" not supported.");
        throw new IllegalArgumentException(sb.toString());
    }

    public final Set<Class<?>> zzase() {
        return this.zzgxw.keySet();
    }

    /* access modifiers changed from: package-private */
    public final Class<?> zzasf() {
        return this.zzgxx;
    }

    public zzdih<?, KeyProtoT> zzasg() {
        throw new UnsupportedOperationException("Creating keys is not supported.");
    }
}
