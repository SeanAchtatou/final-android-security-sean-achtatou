package com.google.android.gms.internal.ads;

import cz.msebera.android.httpclient.HttpStatus;
import java.nio.ShortBuffer;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzin {
    private final int zzafp;
    private float zzagc;
    private float zzagd;
    private final int zzalc;
    private final int zzald;
    private final int zzale;
    private final int zzalf = (this.zzale * 2);
    private final short[] zzalg;
    private int zzalh;
    private short[] zzali;
    private int zzalj;
    private short[] zzalk;
    private int zzall;
    private short[] zzalm;
    private int zzaln;
    private int zzalo;
    private int zzalp;
    private int zzalq;
    private int zzalr;
    private int zzals;
    private int zzalt;
    private int zzalu;
    private int zzalv;
    private int zzalw;

    public zzin(int i, int i2) {
        this.zzafp = i;
        this.zzalc = i2;
        this.zzald = i / HttpStatus.SC_BAD_REQUEST;
        this.zzale = i / 65;
        int i3 = this.zzalf;
        this.zzalg = new short[i3];
        this.zzalh = i3;
        this.zzali = new short[(i3 * i2)];
        this.zzalj = i3;
        this.zzalk = new short[(i3 * i2)];
        this.zzall = i3;
        this.zzalm = new short[(i3 * i2)];
        this.zzaln = 0;
        this.zzalo = 0;
        this.zzalt = 0;
        this.zzagc = 1.0f;
        this.zzagd = 1.0f;
    }

    public final void setSpeed(float f) {
        this.zzagc = f;
    }

    public final void zzc(float f) {
        this.zzagd = f;
    }

    public final void zza(ShortBuffer shortBuffer) {
        int remaining = shortBuffer.remaining();
        int i = this.zzalc;
        int i2 = remaining / i;
        zzv(i2);
        shortBuffer.get(this.zzali, this.zzalp * this.zzalc, ((i * i2) << 1) / 2);
        this.zzalp += i2;
        zzfy();
    }

    public final void zzb(ShortBuffer shortBuffer) {
        int min = Math.min(shortBuffer.remaining() / this.zzalc, this.zzalq);
        shortBuffer.put(this.zzalk, 0, this.zzalc * min);
        this.zzalq -= min;
        short[] sArr = this.zzalk;
        int i = this.zzalc;
        System.arraycopy(sArr, min * i, sArr, 0, this.zzalq * i);
    }

    public final void zzfb() {
        int i;
        int i2 = this.zzalp;
        float f = this.zzagc;
        float f2 = this.zzagd;
        int i3 = this.zzalq + ((int) ((((((float) i2) / (f / f2)) + ((float) this.zzalr)) / f2) + 0.5f));
        zzv((this.zzalf * 2) + i2);
        int i4 = 0;
        while (true) {
            i = this.zzalf;
            int i5 = this.zzalc;
            if (i4 >= i * 2 * i5) {
                break;
            }
            this.zzali[(i5 * i2) + i4] = 0;
            i4++;
        }
        this.zzalp += i * 2;
        zzfy();
        if (this.zzalq > i3) {
            this.zzalq = i3;
        }
        this.zzalp = 0;
        this.zzals = 0;
        this.zzalr = 0;
    }

    public final int zzfx() {
        return this.zzalq;
    }

    private final void zzu(int i) {
        int i2 = this.zzalq + i;
        int i3 = this.zzalj;
        if (i2 > i3) {
            this.zzalj = i3 + (i3 / 2) + i;
            this.zzalk = Arrays.copyOf(this.zzalk, this.zzalj * this.zzalc);
        }
    }

    private final void zzv(int i) {
        int i2 = this.zzalp + i;
        int i3 = this.zzalh;
        if (i2 > i3) {
            this.zzalh = i3 + (i3 / 2) + i;
            this.zzali = Arrays.copyOf(this.zzali, this.zzalh * this.zzalc);
        }
    }

    private final void zza(short[] sArr, int i, int i2) {
        zzu(i2);
        int i3 = this.zzalc;
        System.arraycopy(sArr, i * i3, this.zzalk, this.zzalq * i3, i3 * i2);
        this.zzalq += i2;
    }

    private final void zzb(short[] sArr, int i, int i2) {
        int i3 = this.zzalf / i2;
        int i4 = this.zzalc;
        int i5 = i2 * i4;
        int i6 = i * i4;
        for (int i7 = 0; i7 < i3; i7++) {
            int i8 = 0;
            for (int i9 = 0; i9 < i5; i9++) {
                i8 += sArr[(i7 * i5) + i6 + i9];
            }
            this.zzalg[i7] = (short) (i8 / i5);
        }
    }

    private final int zza(short[] sArr, int i, int i2, int i3) {
        int i4 = i * this.zzalc;
        int i5 = 1;
        int i6 = 0;
        int i7 = 0;
        int i8 = 255;
        while (i2 <= i3) {
            int i9 = 0;
            for (int i10 = 0; i10 < i2; i10++) {
                short s = sArr[i4 + i10];
                short s2 = sArr[i4 + i2 + i10];
                i9 += s >= s2 ? s - s2 : s2 - s;
            }
            if (i9 * i6 < i5 * i2) {
                i6 = i2;
                i5 = i9;
            }
            if (i9 * i8 > i7 * i2) {
                i8 = i2;
                i7 = i9;
            }
            i2++;
        }
        this.zzalv = i5 / i6;
        this.zzalw = i7 / i8;
        return i6;
    }

    private final void zzfy() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = this.zzalq;
        float f = this.zzagc / this.zzagd;
        double d = (double) f;
        int i8 = 1;
        if (d > 1.00001d || d < 0.99999d) {
            int i9 = this.zzalp;
            if (i9 >= this.zzalf) {
                int i10 = 0;
                while (true) {
                    int i11 = this.zzals;
                    if (i11 > 0) {
                        int min = Math.min(this.zzalf, i11);
                        zza(this.zzali, i10, min);
                        this.zzals -= min;
                        i10 += min;
                    } else {
                        short[] sArr = this.zzali;
                        int i12 = this.zzafp;
                        int i13 = i12 > 4000 ? i12 / 4000 : 1;
                        if (this.zzalc == i8 && i13 == i8) {
                            i4 = zza(sArr, i10, this.zzald, this.zzale);
                        } else {
                            zzb(sArr, i10, i13);
                            int zza = zza(this.zzalg, 0, this.zzald / i13, this.zzale / i13);
                            if (i13 != i8) {
                                int i14 = zza * i13;
                                int i15 = i13 << 2;
                                int i16 = i14 - i15;
                                int i17 = i14 + i15;
                                int i18 = this.zzald;
                                if (i16 >= i18) {
                                    i18 = i16;
                                }
                                int i19 = this.zzale;
                                if (i17 > i19) {
                                    i17 = i19;
                                }
                                if (this.zzalc == i8) {
                                    i4 = zza(sArr, i10, i18, i17);
                                } else {
                                    zzb(sArr, i10, i8);
                                    i4 = zza(this.zzalg, 0, i18, i17);
                                }
                            } else {
                                i4 = zza;
                            }
                        }
                        int i20 = this.zzalv;
                        int i21 = i20 != 0 && this.zzalt != 0 && this.zzalw <= i20 * 3 && (i20 << 1) > this.zzalu * 3 ? this.zzalt : i4;
                        this.zzalu = this.zzalv;
                        this.zzalt = i4;
                        if (d > 1.0d) {
                            short[] sArr2 = this.zzali;
                            if (f >= 2.0f) {
                                i6 = (int) (((float) i21) / (f - 1.0f));
                            } else {
                                this.zzals = (int) ((((float) i21) * (2.0f - f)) / (f - 1.0f));
                                i6 = i21;
                            }
                            zzu(i6);
                            int i22 = i6;
                            zza(i6, this.zzalc, this.zzalk, this.zzalq, sArr2, i10, sArr2, i10 + i21);
                            this.zzalq += i22;
                            i10 += i21 + i22;
                        } else {
                            int i23 = i21;
                            short[] sArr3 = this.zzali;
                            if (f < 0.5f) {
                                i5 = (int) ((((float) i23) * f) / (1.0f - f));
                            } else {
                                this.zzals = (int) ((((float) i23) * ((2.0f * f) - 1.0f)) / (1.0f - f));
                                i5 = i23;
                            }
                            int i24 = i23 + i5;
                            zzu(i24);
                            int i25 = this.zzalc;
                            System.arraycopy(sArr3, i10 * i25, this.zzalk, this.zzalq * i25, i25 * i23);
                            zza(i5, this.zzalc, this.zzalk, this.zzalq + i23, sArr3, i23 + i10, sArr3, i10);
                            this.zzalq += i24;
                            i10 += i5;
                        }
                    }
                    if (this.zzalf + i10 > i9) {
                        break;
                    }
                    i8 = 1;
                }
                int i26 = this.zzalp - i10;
                short[] sArr4 = this.zzali;
                int i27 = this.zzalc;
                System.arraycopy(sArr4, i10 * i27, sArr4, 0, i27 * i26);
                this.zzalp = i26;
            }
        } else {
            zza(this.zzali, 0, this.zzalp);
            this.zzalp = 0;
        }
        float f2 = this.zzagd;
        if (f2 != 1.0f && this.zzalq != i7) {
            int i28 = this.zzafp;
            int i29 = (int) (((float) i28) / f2);
            while (true) {
                if (i29 <= 16384 && i28 <= 16384) {
                    break;
                }
                i29 /= 2;
                i28 /= 2;
            }
            int i30 = this.zzalq - i7;
            int i31 = this.zzalr + i30;
            int i32 = this.zzall;
            if (i31 > i32) {
                this.zzall = i32 + (i32 / 2) + i30;
                this.zzalm = Arrays.copyOf(this.zzalm, this.zzall * this.zzalc);
            }
            short[] sArr5 = this.zzalk;
            int i33 = this.zzalc;
            System.arraycopy(sArr5, i7 * i33, this.zzalm, this.zzalr * i33, i33 * i30);
            this.zzalq = i7;
            this.zzalr += i30;
            int i34 = 0;
            while (true) {
                i = this.zzalr;
                if (i34 >= i - 1) {
                    break;
                }
                while (true) {
                    i2 = this.zzaln;
                    int i35 = (i2 + 1) * i29;
                    i3 = this.zzalo;
                    if (i35 <= i3 * i28) {
                        break;
                    }
                    zzu(1);
                    int i36 = 0;
                    while (true) {
                        int i37 = this.zzalc;
                        if (i36 >= i37) {
                            break;
                        }
                        short[] sArr6 = this.zzalm;
                        int i38 = (i34 * i37) + i36;
                        short s = sArr6[i38];
                        short s2 = sArr6[i38 + i37];
                        int i39 = this.zzaln;
                        int i40 = i39 * i29;
                        int i41 = (i39 + 1) * i29;
                        int i42 = i41 - (this.zzalo * i28);
                        int i43 = i41 - i40;
                        this.zzalk[(this.zzalq * i37) + i36] = (short) (((s * i42) + ((i43 - i42) * s2)) / i43);
                        i36++;
                    }
                    this.zzalo++;
                    this.zzalq++;
                }
                this.zzaln = i2 + 1;
                if (this.zzaln == i28) {
                    this.zzaln = 0;
                    zzoc.checkState(i3 == i29);
                    this.zzalo = 0;
                }
                i34++;
            }
            int i44 = i - 1;
            if (i44 != 0) {
                short[] sArr7 = this.zzalm;
                int i45 = this.zzalc;
                System.arraycopy(sArr7, i44 * i45, sArr7, 0, (i - i44) * i45);
                this.zzalr -= i44;
            }
        }
    }

    private static void zza(int i, int i2, short[] sArr, int i3, short[] sArr2, int i4, short[] sArr3, int i5) {
        for (int i6 = 0; i6 < i2; i6++) {
            int i7 = (i4 * i2) + i6;
            int i8 = (i5 * i2) + i6;
            int i9 = (i3 * i2) + i6;
            for (int i10 = 0; i10 < i; i10++) {
                sArr[i9] = (short) (((sArr2[i7] * (i - i10)) + (sArr3[i8] * i10)) / i);
                i9 += i2;
                i7 += i2;
                i8 += i2;
            }
        }
    }
}
