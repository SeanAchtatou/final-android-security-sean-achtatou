package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.internal.measurement.zzj;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzjt extends zze {
    protected zzkd zza = new zzkd(this);
    protected zzkb zzb = new zzkb(this);
    /* access modifiers changed from: private */
    public Handler zzc;
    private zzjy zzd = new zzjy(this);

    zzjt(zzgf zzgf) {
        super(zzgf);
    }

    /* access modifiers changed from: protected */
    public final boolean zzz() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void zzab() {
        zzq().zza(new zzjw(this, zzm().elapsedRealtime()));
    }

    /* access modifiers changed from: private */
    public final void zzac() {
        zzd();
        if (this.zzc == null) {
            this.zzc = new zzj(Looper.getMainLooper());
        }
    }

    /* access modifiers changed from: private */
    public final void zza(long j) {
        zzd();
        zzac();
        zzr().zzx().zza("Activity resumed, time", Long.valueOf(j));
        this.zzd.zza();
        this.zzb.zza(j);
        zzkd zzkd = this.zza;
        zzkd.zza.zzd();
        if (zzkd.zza.zzx.zzab()) {
            if (zzkd.zza.zzt().zza(zzap.zzar)) {
                zzkd.zza.zzs().zzt.zza(false);
            }
            zzkd.zza(zzkd.zza.zzm().currentTimeMillis(), false);
        }
    }

    /* access modifiers changed from: private */
    public final void zzb(long j) {
        zzd();
        zzac();
        zzr().zzx().zza("Activity paused, time", Long.valueOf(j));
        this.zzd.zzb();
        this.zzb.zzb(j);
        zzkd zzkd = this.zza;
        if (zzkd.zza.zzt().zza(zzap.zzar)) {
            zzkd.zza.zzs().zzt.zza(true);
        }
    }

    public final boolean zza(boolean z, boolean z2, long j) {
        return this.zzb.zza(z, z2, j);
    }

    public final /* bridge */ /* synthetic */ void zza() {
        super.zza();
    }

    public final /* bridge */ /* synthetic */ void zzb() {
        super.zzb();
    }

    public final /* bridge */ /* synthetic */ void zzc() {
        super.zzc();
    }

    public final /* bridge */ /* synthetic */ void zzd() {
        super.zzd();
    }

    public final /* bridge */ /* synthetic */ zzb zze() {
        return super.zze();
    }

    public final /* bridge */ /* synthetic */ zzhk zzf() {
        return super.zzf();
    }

    public final /* bridge */ /* synthetic */ zzey zzg() {
        return super.zzg();
    }

    public final /* bridge */ /* synthetic */ zzis zzh() {
        return super.zzh();
    }

    public final /* bridge */ /* synthetic */ zzin zzi() {
        return super.zzi();
    }

    public final /* bridge */ /* synthetic */ zzex zzj() {
        return super.zzj();
    }

    public final /* bridge */ /* synthetic */ zzjt zzk() {
        return super.zzk();
    }

    public final /* bridge */ /* synthetic */ zzah zzl() {
        return super.zzl();
    }

    public final /* bridge */ /* synthetic */ Clock zzm() {
        return super.zzm();
    }

    public final /* bridge */ /* synthetic */ Context zzn() {
        return super.zzn();
    }

    public final /* bridge */ /* synthetic */ zzez zzo() {
        return super.zzo();
    }

    public final /* bridge */ /* synthetic */ zzkv zzp() {
        return super.zzp();
    }

    public final /* bridge */ /* synthetic */ zzgc zzq() {
        return super.zzq();
    }

    public final /* bridge */ /* synthetic */ zzfb zzr() {
        return super.zzr();
    }

    public final /* bridge */ /* synthetic */ zzfo zzs() {
        return super.zzs();
    }

    public final /* bridge */ /* synthetic */ zzx zzt() {
        return super.zzt();
    }

    public final /* bridge */ /* synthetic */ zzw zzu() {
        return super.zzu();
    }
}
