package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcoy implements zzbow {
    private final zzcop zzgdq;
    private final zzahh zzgdr;

    zzcoy(zzcop zzcop, zzahh zzahh) {
        this.zzgdq = zzcop;
        this.zzgdr = zzahh;
    }

    public final void onAdFailedToLoad(int i) {
        zzcop zzcop = this.zzgdq;
        zzahh zzahh = this.zzgdr;
        zzcop.onAdFailedToLoad(i);
        if (zzahh != null) {
            try {
                zzahh.onInstreamAdFailedToLoad(i);
            } catch (RemoteException e) {
                zzayu.zze("#007 Could not call remote method.", e);
            }
        }
    }
}
