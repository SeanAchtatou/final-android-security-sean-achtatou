package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zziz;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zziy<T extends zziz> {
    int getState();

    T zzgf();

    zzix zzgg();
}
