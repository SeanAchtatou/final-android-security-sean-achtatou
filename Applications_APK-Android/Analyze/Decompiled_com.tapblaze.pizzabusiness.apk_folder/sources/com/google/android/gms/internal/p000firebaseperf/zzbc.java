package com.google.android.gms.internal.p000firebaseperf;

import android.util.Log;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbc  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzbc {
    private static final zzbc zzbo = new zzbc();
    private final ScheduledExecutorService zzbp;
    public final ConcurrentLinkedQueue<zzcb> zzbq;
    private final Runtime zzbr;
    private ScheduledFuture zzbs;
    private long zzbt;

    private zzbc() {
        this(Executors.newSingleThreadScheduledExecutor(), Runtime.getRuntime());
    }

    public static boolean zzi(long j) {
        return j <= 0;
    }

    private zzbc(ScheduledExecutorService scheduledExecutorService, Runtime runtime) {
        this.zzbs = null;
        this.zzbt = -1;
        this.zzbp = scheduledExecutorService;
        this.zzbq = new ConcurrentLinkedQueue<>();
        this.zzbr = runtime;
    }

    public static zzbc zzbe() {
        return zzbo;
    }

    public final void zza(long j, zzbt zzbt2) {
        if (!zzi(j)) {
            if (this.zzbs == null) {
                zzc(j, zzbt2);
            } else if (this.zzbt != j) {
                zzbd();
                zzc(j, zzbt2);
            }
        }
    }

    public final void zzbd() {
        ScheduledFuture scheduledFuture = this.zzbs;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
            this.zzbs = null;
            this.zzbt = -1;
        }
    }

    public final void zza(zzbt zzbt2) {
        zzf(zzbt2);
    }

    private final synchronized void zzc(long j, zzbt zzbt2) {
        this.zzbt = j;
        try {
            this.zzbs = this.zzbp.scheduleAtFixedRate(new zzbf(this, zzbt2), 0, j, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("FirebasePerformance", valueOf.length() != 0 ? "Unable to start collecting Memory Metrics: ".concat(valueOf) : new String("Unable to start collecting Memory Metrics: "));
        }
    }

    private final synchronized void zzf(zzbt zzbt2) {
        try {
            this.zzbp.schedule(new zzbe(this, zzbt2), 0, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("FirebasePerformance", valueOf.length() != 0 ? "Unable to collect Memory Metric: ".concat(valueOf) : new String("Unable to collect Memory Metric: "));
        }
    }

    private final zzcb zzg(zzbt zzbt2) {
        if (zzbt2 == null) {
            return null;
        }
        return (zzcb) ((zzfc) zzcb.zzdf().zzv(zzbt2.zzdb()).zze(zzx.zza(zzbn.BYTES.zzt(this.zzbr.totalMemory() - this.zzbr.freeMemory()))).zzhp());
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzh(zzbt zzbt2) {
        zzcb zzg = zzg(zzbt2);
        if (zzg != null) {
            this.zzbq.add(zzg);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzi(zzbt zzbt2) {
        zzcb zzg = zzg(zzbt2);
        if (zzg != null) {
            this.zzbq.add(zzg);
        }
    }
}
