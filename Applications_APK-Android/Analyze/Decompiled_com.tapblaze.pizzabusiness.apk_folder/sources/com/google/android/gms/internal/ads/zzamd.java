package com.google.android.gms.internal.ads;

import android.location.Location;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzamd implements NativeMediationAdRequest {
    private final String zzabq;
    private final int zzcbz;
    private final boolean zzcck;
    private final int zzddh;
    private final int zzddi;
    private final zzaby zzddz;
    private final List<String> zzdea = new ArrayList();
    private final Map<String, Boolean> zzdeb = new HashMap();
    private final Date zzme;
    private final Set<String> zzmg;
    private final boolean zzmh;
    private final Location zzmi;

    public zzamd(Date date, int i, Set<String> set, Location location, boolean z, int i2, zzaby zzaby, List<String> list, boolean z2, int i3, String str) {
        this.zzme = date;
        this.zzcbz = i;
        this.zzmg = set;
        this.zzmi = location;
        this.zzmh = z;
        this.zzddh = i2;
        this.zzddz = zzaby;
        this.zzcck = z2;
        this.zzddi = i3;
        this.zzabq = str;
        if (list != null) {
            for (String next : list) {
                if (next.startsWith("custom:")) {
                    String[] split = next.split(":", 3);
                    if (split.length == 3) {
                        if (ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(split[2])) {
                            this.zzdeb.put(split[1], true);
                        } else if ("false".equals(split[2])) {
                            this.zzdeb.put(split[1], false);
                        }
                    }
                } else {
                    this.zzdea.add(next);
                }
            }
        }
    }

    @Deprecated
    public final Date getBirthday() {
        return this.zzme;
    }

    @Deprecated
    public final int getGender() {
        return this.zzcbz;
    }

    public final Set<String> getKeywords() {
        return this.zzmg;
    }

    public final Location getLocation() {
        return this.zzmi;
    }

    public final boolean isTesting() {
        return this.zzmh;
    }

    public final int taggedForChildDirectedTreatment() {
        return this.zzddh;
    }

    public final NativeAdOptions getNativeAdOptions() {
        if (this.zzddz == null) {
            return null;
        }
        NativeAdOptions.Builder requestMultipleImages = new NativeAdOptions.Builder().setReturnUrlsForImageAssets(this.zzddz.zzcvo).setImageOrientation(this.zzddz.zzbjw).setRequestMultipleImages(this.zzddz.zzbjy);
        if (this.zzddz.versionCode >= 2) {
            requestMultipleImages.setAdChoicesPlacement(this.zzddz.zzbjz);
        }
        if (this.zzddz.versionCode >= 3 && this.zzddz.zzcvp != null) {
            requestMultipleImages.setVideoOptions(new VideoOptions(this.zzddz.zzcvp));
        }
        return requestMultipleImages.build();
    }

    public final boolean isAdMuted() {
        return zzxq.zzpw().zzpf();
    }

    public final float getAdVolume() {
        return zzxq.zzpw().zzpe();
    }

    public final boolean isAppInstallAdRequested() {
        List<String> list = this.zzdea;
        if (list != null) {
            return list.contains("2") || this.zzdea.contains("6");
        }
        return false;
    }

    public final boolean isUnifiedNativeAdRequested() {
        List<String> list = this.zzdea;
        return list != null && list.contains("6");
    }

    public final boolean isContentAdRequested() {
        List<String> list = this.zzdea;
        if (list != null) {
            return list.contains("1") || this.zzdea.contains("6");
        }
        return false;
    }

    public final boolean zzsz() {
        List<String> list = this.zzdea;
        return list != null && list.contains("3");
    }

    public final Map<String, Boolean> zzta() {
        return this.zzdeb;
    }

    @Deprecated
    public final boolean isDesignedForFamilies() {
        return this.zzcck;
    }
}
