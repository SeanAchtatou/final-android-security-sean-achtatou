package com.google.android.gms.internal.ads;

import java.security.GeneralSecurityException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdjy extends zzdih<zzdnt, zzdns> {
    private final /* synthetic */ zzdjw zzgzl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdjy(zzdjw zzdjw, Class cls) {
        super(cls);
        this.zzgzl = zzdjw;
    }

    public final /* synthetic */ Object zzd(zzdte zzdte) throws GeneralSecurityException {
        return (zzdns) ((zzdrt) zzdns.zzawo().zzb((zzdnt) zzdte).zzev(0).zzbaf());
    }

    public final /* synthetic */ zzdte zzq(zzdqk zzdqk) throws zzdse {
        return zzdnt.zzba(zzdqk);
    }

    public final /* bridge */ /* synthetic */ void zzc(zzdte zzdte) throws GeneralSecurityException {
        zzdnt zzdnt = (zzdnt) zzdte;
    }
}
