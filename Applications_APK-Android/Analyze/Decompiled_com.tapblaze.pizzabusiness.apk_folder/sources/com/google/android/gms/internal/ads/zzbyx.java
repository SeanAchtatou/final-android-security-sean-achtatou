package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbyx implements zzdgf {
    private final zzdhe zzfpn;

    zzbyx(zzdhe zzdhe) {
        this.zzfpn = zzdhe;
    }

    public final zzdhe zzf(Object obj) {
        zzdhe zzdhe = this.zzfpn;
        zzbdi zzbdi = (zzbdi) obj;
        if (zzbdi != null && zzbdi.zzyl() != null) {
            return zzdhe;
        }
        throw new zzclr("Retrieve video view in instream ad response failed.", 0);
    }
}
