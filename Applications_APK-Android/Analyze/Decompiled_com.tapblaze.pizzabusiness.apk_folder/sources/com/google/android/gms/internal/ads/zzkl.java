package com.google.android.gms.internal.ads;

import android.util.Log;
import com.google.android.gms.internal.ads.zzle;
import com.ironsource.mediationsdk.IronSourceSegment;
import cz.msebera.android.httpclient.HttpHeaders;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzkl {
    private static final int zzavk = zzoq.zzbn("nam");
    private static final int zzavl = zzoq.zzbn("trk");
    private static final int zzavm = zzoq.zzbn("cmt");
    private static final int zzavn = zzoq.zzbn("day");
    private static final int zzavo = zzoq.zzbn("ART");
    private static final int zzavp = zzoq.zzbn("too");
    private static final int zzavq = zzoq.zzbn("alb");
    private static final int zzavr = zzoq.zzbn("com");
    private static final int zzavs = zzoq.zzbn("wrt");
    private static final int zzavt = zzoq.zzbn("lyr");
    private static final int zzavu = zzoq.zzbn(IronSourceSegment.GENDER);
    private static final int zzavv = zzoq.zzbn("covr");
    private static final int zzavw = zzoq.zzbn("gnre");
    private static final int zzavx = zzoq.zzbn("grp");
    private static final int zzavy = zzoq.zzbn("disk");
    private static final int zzavz = zzoq.zzbn("trkn");
    private static final int zzawa = zzoq.zzbn("tmpo");
    private static final int zzawb = zzoq.zzbn("cpil");
    private static final int zzawc = zzoq.zzbn("aART");
    private static final int zzawd = zzoq.zzbn("sonm");
    private static final int zzawe = zzoq.zzbn("soal");
    private static final int zzawf = zzoq.zzbn("soar");
    private static final int zzawg = zzoq.zzbn("soaa");
    private static final int zzawh = zzoq.zzbn("soco");
    private static final int zzawi = zzoq.zzbn("rtng");
    private static final int zzawj = zzoq.zzbn("pgap");
    private static final int zzawk = zzoq.zzbn("sosn");
    private static final int zzawl = zzoq.zzbn("tvsh");
    private static final int zzawm = zzoq.zzbn("----");
    private static final String[] zzawn = {"Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", "New Age", "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial", "Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", "Euro-Techno", "Ambient", "Trip-Hop", "Vocal", "Jazz+Funk", "Fusion", "Trance", "Classical", "Instrumental", "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise", "AlternRock", "Bass", "Soul", "Punk", "Space", "Meditative", "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic", "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta", "Top 40", "Christian Rap", "Pop/Funk", "Jungle", "Native American", "Cabaret", "New Wave", "Psychadelic", "Rave", "Showtunes", HttpHeaders.TRAILER, "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk", "Folk-Rock", "National Folk", "Swing", "Fast Fusion", "Bebob", "Latin", "Revival", "Celtic", "Bluegrass", "Avantgarde", "Gothic Rock", "Progressive Rock", "Psychedelic Rock", "Symphonic Rock", "Slow Rock", "Big Band", "Chorus", "Easy Listening", "Acoustic", "Humour", "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass", "Primus", "Porn Groove", "Satire", "Slow Jam", "Club", "Tango", "Samba", "Folklore", "Ballad", "Power Ballad", "Rhythmic Soul", "Freestyle", "Duet", "Punk Rock", "Drum Solo", "A capella", "Euro-House", "Dance Hall", "Goa", "Drum & Bass", "Club-House", "Hardcore", "Terror", "Indie", "BritPop", "Negerpunk", "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal", "Black Metal", "Crossover", "Contemporary Christian", "Christian Rock", "Merengue", "Salsa", "Thrash Metal", "Anime", "Jpop", "Synthpop"};

    public static zzle.zza zzd(zzoj zzoj) {
        zzlo zzlo;
        int position = zzoj.getPosition() + zzoj.readInt();
        int readInt = zzoj.readInt();
        int i = readInt >>> 24;
        zzll zzll = null;
        if (i == 169 || i == 65533) {
            int i2 = 16777215 & readInt;
            if (i2 == zzavm) {
                int readInt2 = zzoj.readInt();
                if (zzoj.readInt() == zzjz.zzaua) {
                    zzoj.zzbf(8);
                    String zzbg = zzoj.zzbg(readInt2 - 16);
                    zzll = new zzlk("und", zzbg, zzbg);
                } else {
                    String valueOf = String.valueOf(zzjz.zzam(readInt));
                    Log.w("MetadataUtil", valueOf.length() != 0 ? "Failed to parse comment attribute: ".concat(valueOf) : new String("Failed to parse comment attribute: "));
                }
                zzoj.zzbe(position);
                return zzll;
            }
            if (i2 != zzavk) {
                if (i2 != zzavl) {
                    if (i2 != zzavr) {
                        if (i2 != zzavs) {
                            if (i2 == zzavn) {
                                zzlo zza = zza(readInt, "TDRC", zzoj);
                                zzoj.zzbe(position);
                                return zza;
                            } else if (i2 == zzavo) {
                                zzlo zza2 = zza(readInt, "TPE1", zzoj);
                                zzoj.zzbe(position);
                                return zza2;
                            } else if (i2 == zzavp) {
                                zzlo zza3 = zza(readInt, "TSSE", zzoj);
                                zzoj.zzbe(position);
                                return zza3;
                            } else if (i2 == zzavq) {
                                zzlo zza4 = zza(readInt, "TALB", zzoj);
                                zzoj.zzbe(position);
                                return zza4;
                            } else if (i2 == zzavt) {
                                zzlo zza5 = zza(readInt, "USLT", zzoj);
                                zzoj.zzbe(position);
                                return zza5;
                            } else if (i2 == zzavu) {
                                zzlo zza6 = zza(readInt, "TCON", zzoj);
                                zzoj.zzbe(position);
                                return zza6;
                            } else if (i2 == zzavx) {
                                zzlo zza7 = zza(readInt, "TIT1", zzoj);
                                zzoj.zzbe(position);
                                return zza7;
                            }
                        }
                    }
                    zzlo zza8 = zza(readInt, "TCOM", zzoj);
                    zzoj.zzbe(position);
                    return zza8;
                }
            }
            zzlo zza9 = zza(readInt, "TIT2", zzoj);
            zzoj.zzbe(position);
            return zza9;
        }
        try {
            if (readInt == zzavw) {
                int zze = zze(zzoj);
                String str = (zze <= 0 || zze > zzawn.length) ? null : zzawn[zze - 1];
                if (str != null) {
                    zzlo = new zzlo("TCON", null, str);
                } else {
                    Log.w("MetadataUtil", "Failed to parse standard genre code");
                    zzlo = null;
                }
                return zzlo;
            } else if (readInt == zzavy) {
                zzlo zzb = zzb(readInt, "TPOS", zzoj);
                zzoj.zzbe(position);
                return zzb;
            } else if (readInt == zzavz) {
                zzlo zzb2 = zzb(readInt, "TRCK", zzoj);
                zzoj.zzbe(position);
                return zzb2;
            } else if (readInt == zzawa) {
                zzll zza10 = zza(readInt, "TBPM", zzoj, true, false);
                zzoj.zzbe(position);
                return zza10;
            } else if (readInt == zzawb) {
                zzll zza11 = zza(readInt, "TCMP", zzoj, true, true);
                zzoj.zzbe(position);
                return zza11;
            } else if (readInt == zzavv) {
                int readInt3 = zzoj.readInt();
                if (zzoj.readInt() == zzjz.zzaua) {
                    int zzal = zzjz.zzal(zzoj.readInt());
                    String str2 = zzal == 13 ? "image/jpeg" : zzal == 14 ? "image/png" : null;
                    if (str2 == null) {
                        StringBuilder sb = new StringBuilder(41);
                        sb.append("Unrecognized cover art flags: ");
                        sb.append(zzal);
                        Log.w("MetadataUtil", sb.toString());
                    } else {
                        zzoj.zzbf(4);
                        byte[] bArr = new byte[(readInt3 - 16)];
                        zzoj.zze(bArr, 0, bArr.length);
                        zzll = new zzli(str2, null, 3, bArr);
                    }
                } else {
                    Log.w("MetadataUtil", "Failed to parse cover art attribute");
                }
                zzoj.zzbe(position);
                return zzll;
            } else if (readInt == zzawc) {
                zzlo zza12 = zza(readInt, "TPE2", zzoj);
                zzoj.zzbe(position);
                return zza12;
            } else if (readInt == zzawd) {
                zzlo zza13 = zza(readInt, "TSOT", zzoj);
                zzoj.zzbe(position);
                return zza13;
            } else if (readInt == zzawe) {
                zzlo zza14 = zza(readInt, "TSO2", zzoj);
                zzoj.zzbe(position);
                return zza14;
            } else if (readInt == zzawf) {
                zzlo zza15 = zza(readInt, "TSOA", zzoj);
                zzoj.zzbe(position);
                return zza15;
            } else if (readInt == zzawg) {
                zzlo zza16 = zza(readInt, "TSOP", zzoj);
                zzoj.zzbe(position);
                return zza16;
            } else if (readInt == zzawh) {
                zzlo zza17 = zza(readInt, "TSOC", zzoj);
                zzoj.zzbe(position);
                return zza17;
            } else if (readInt == zzawi) {
                zzll zza18 = zza(readInt, "ITUNESADVISORY", zzoj, false, false);
                zzoj.zzbe(position);
                return zza18;
            } else if (readInt == zzawj) {
                zzll zza19 = zza(readInt, "ITUNESGAPLESS", zzoj, false, true);
                zzoj.zzbe(position);
                return zza19;
            } else if (readInt == zzawk) {
                zzlo zza20 = zza(readInt, "TVSHOWSORT", zzoj);
                zzoj.zzbe(position);
                return zza20;
            } else if (readInt == zzawl) {
                zzlo zza21 = zza(readInt, "TVSHOW", zzoj);
                zzoj.zzbe(position);
                return zza21;
            } else if (readInt == zzawm) {
                String str3 = null;
                String str4 = null;
                int i3 = -1;
                int i4 = -1;
                while (zzoj.getPosition() < position) {
                    int position2 = zzoj.getPosition();
                    int readInt4 = zzoj.readInt();
                    int readInt5 = zzoj.readInt();
                    zzoj.zzbf(4);
                    if (readInt5 == zzjz.zzaty) {
                        str3 = zzoj.zzbg(readInt4 - 12);
                    } else if (readInt5 == zzjz.zzatz) {
                        str4 = zzoj.zzbg(readInt4 - 12);
                    } else {
                        if (readInt5 == zzjz.zzaua) {
                            i3 = position2;
                            i4 = readInt4;
                        }
                        zzoj.zzbf(readInt4 - 12);
                    }
                }
                if ("com.apple.iTunes".equals(str3) && "iTunSMPB".equals(str4)) {
                    if (i3 != -1) {
                        zzoj.zzbe(i3);
                        zzoj.zzbf(16);
                        zzll = new zzlk("und", str4, zzoj.zzbg(i4 - 16));
                    }
                }
                zzoj.zzbe(position);
                return zzll;
            }
        } finally {
            zzoj.zzbe(position);
        }
        String valueOf2 = String.valueOf(zzjz.zzam(readInt));
        Log.d("MetadataUtil", valueOf2.length() != 0 ? "Skipped unknown metadata entry: ".concat(valueOf2) : new String("Skipped unknown metadata entry: "));
        zzoj.zzbe(position);
        return null;
    }

    private static zzlo zza(int i, String str, zzoj zzoj) {
        int readInt = zzoj.readInt();
        if (zzoj.readInt() == zzjz.zzaua) {
            zzoj.zzbf(8);
            return new zzlo(str, null, zzoj.zzbg(readInt - 16));
        }
        String valueOf = String.valueOf(zzjz.zzam(i));
        Log.w("MetadataUtil", valueOf.length() != 0 ? "Failed to parse text attribute: ".concat(valueOf) : new String("Failed to parse text attribute: "));
        return null;
    }

    private static zzll zza(int i, String str, zzoj zzoj, boolean z, boolean z2) {
        int zze = zze(zzoj);
        if (z2) {
            zze = Math.min(1, zze);
        }
        if (zze < 0) {
            String valueOf = String.valueOf(zzjz.zzam(i));
            Log.w("MetadataUtil", valueOf.length() != 0 ? "Failed to parse uint8 attribute: ".concat(valueOf) : new String("Failed to parse uint8 attribute: "));
            return null;
        } else if (z) {
            return new zzlo(str, null, Integer.toString(zze));
        } else {
            return new zzlk("und", str, Integer.toString(zze));
        }
    }

    private static zzlo zzb(int i, String str, zzoj zzoj) {
        int readInt = zzoj.readInt();
        if (zzoj.readInt() == zzjz.zzaua && readInt >= 22) {
            zzoj.zzbf(10);
            int readUnsignedShort = zzoj.readUnsignedShort();
            if (readUnsignedShort > 0) {
                StringBuilder sb = new StringBuilder(11);
                sb.append(readUnsignedShort);
                String sb2 = sb.toString();
                int readUnsignedShort2 = zzoj.readUnsignedShort();
                if (readUnsignedShort2 > 0) {
                    String valueOf = String.valueOf(sb2);
                    StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf).length() + 12);
                    sb3.append(valueOf);
                    sb3.append("/");
                    sb3.append(readUnsignedShort2);
                    sb2 = sb3.toString();
                }
                return new zzlo(str, null, sb2);
            }
        }
        String valueOf2 = String.valueOf(zzjz.zzam(i));
        Log.w("MetadataUtil", valueOf2.length() != 0 ? "Failed to parse index/count attribute: ".concat(valueOf2) : new String("Failed to parse index/count attribute: "));
        return null;
    }

    private static int zze(zzoj zzoj) {
        zzoj.zzbf(4);
        if (zzoj.readInt() == zzjz.zzaua) {
            zzoj.zzbf(8);
            return zzoj.readUnsignedByte();
        }
        Log.w("MetadataUtil", "Failed to parse uint8 attribute value");
        return -1;
    }
}
