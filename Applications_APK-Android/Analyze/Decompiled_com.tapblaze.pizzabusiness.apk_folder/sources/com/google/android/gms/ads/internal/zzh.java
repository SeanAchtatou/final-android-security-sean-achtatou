package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.internal.ads.zzavs;
import com.google.android.gms.internal.ads.zzayk;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzazd;
import com.google.android.gms.internal.ads.zzdg;
import com.google.android.gms.internal.ads.zzdr;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzh implements zzdg, Runnable {
    private final List<Object[]> zzblg = new Vector();
    private final AtomicReference<zzdg> zzblh = new AtomicReference<>();
    private zzazb zzbli;
    private CountDownLatch zzblj = new CountDownLatch(1);
    private Context zzup;

    public zzh(Context context, zzazb zzazb) {
        this.zzup = context;
        this.zzbli = zzazb;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzclm)).booleanValue()) {
            zzazd.zzdwe.execute(this);
            return;
        }
        zzve.zzou();
        if (zzayk.zzxe()) {
            zzazd.zzdwe.execute(this);
        } else {
            run();
        }
    }

    private final boolean zzjt() {
        try {
            this.zzblj.await();
            return true;
        } catch (InterruptedException e) {
            zzavs.zzd("Interrupted during GADSignals creation.", e);
            return false;
        }
    }

    private final void zzju() {
        if (!this.zzblg.isEmpty()) {
            for (Object[] next : this.zzblg) {
                if (next.length == 1) {
                    this.zzblh.get().zza((MotionEvent) next[0]);
                } else if (next.length == 3) {
                    this.zzblh.get().zza(((Integer) next[0]).intValue(), ((Integer) next[1]).intValue(), ((Integer) next[2]).intValue());
                }
            }
            this.zzblg.clear();
        }
    }

    private static Context zze(Context context) {
        Context applicationContext = context.getApplicationContext();
        return applicationContext == null ? context : applicationContext;
    }

    public final String zzb(Context context) {
        zzdg zzdg;
        if (!zzjt() || (zzdg = this.zzblh.get()) == null) {
            return "";
        }
        zzju();
        return zzdg.zzb(zze(context));
    }

    public final String zza(Context context, View view, Activity activity) {
        zzdg zzdg = this.zzblh.get();
        return zzdg != null ? zzdg.zza(context, view, activity) : "";
    }

    public final String zza(Context context, String str, View view) {
        return zza(context, str, view, null);
    }

    public final String zza(Context context, String str, View view, Activity activity) {
        zzdg zzdg;
        if (!zzjt() || (zzdg = this.zzblh.get()) == null) {
            return "";
        }
        zzju();
        return zzdg.zza(zze(context), str, view, activity);
    }

    public final void zzb(View view) {
        zzdg zzdg = this.zzblh.get();
        if (zzdg != null) {
            zzdg.zzb(view);
        }
    }

    public final void zza(MotionEvent motionEvent) {
        zzdg zzdg = this.zzblh.get();
        if (zzdg != null) {
            zzju();
            zzdg.zza(motionEvent);
            return;
        }
        this.zzblg.add(new Object[]{motionEvent});
    }

    public final void zza(int i, int i2, int i3) {
        zzdg zzdg = this.zzblh.get();
        if (zzdg != null) {
            zzju();
            zzdg.zza(i, i2, i3);
            return;
        }
        this.zzblg.add(new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)});
    }

    public final void run() {
        boolean z = false;
        try {
            boolean z2 = this.zzbli.zzdwb;
            if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcjf)).booleanValue() && z2) {
                z = true;
            }
            this.zzblh.set(zzdr.zza(this.zzbli.zzbma, zze(this.zzup), z));
        } finally {
            this.zzblj.countDown();
            this.zzup = null;
            this.zzbli = null;
        }
    }
}
