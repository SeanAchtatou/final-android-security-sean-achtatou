package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzkh implements zzkg {
    private final zzoj zzaul;
    private final int zzavf = this.zzaul.zzis();
    private final int zzavg = (this.zzaul.zzis() & 255);
    private int zzavh;
    private int zzavi;

    public zzkh(zzkb zzkb) {
        this.zzaul = zzkb.zzaul;
        this.zzaul.zzbe(12);
    }

    public final boolean zzgs() {
        return false;
    }

    public final int zzgq() {
        return this.zzavf;
    }

    public final int zzgr() {
        int i = this.zzavg;
        if (i == 8) {
            return this.zzaul.readUnsignedByte();
        }
        if (i == 16) {
            return this.zzaul.readUnsignedShort();
        }
        int i2 = this.zzavh;
        this.zzavh = i2 + 1;
        if (i2 % 2 != 0) {
            return this.zzavi & 15;
        }
        this.zzavi = this.zzaul.readUnsignedByte();
        return (this.zzavi & 240) >> 4;
    }
}
