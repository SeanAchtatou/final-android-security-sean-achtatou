package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcqw implements zzcty<Bundle> {
    private final boolean zzgfd = false;
    private final boolean zzgfe = false;
    private final boolean zzgff;

    public zzcqw(boolean z, boolean z2, boolean z3) {
        this.zzgff = z3;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        bundle.putBoolean("c_pcbg", this.zzgfd);
        bundle.putBoolean("c_phbg", this.zzgfe);
        bundle.putBoolean("ar_lr", this.zzgff);
    }
}
