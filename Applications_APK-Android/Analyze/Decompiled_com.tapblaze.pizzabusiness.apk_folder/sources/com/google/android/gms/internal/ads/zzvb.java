package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
abstract class zzvb<T> {
    private static final zzwd zzcdp = zzor();

    zzvb() {
    }

    /* access modifiers changed from: protected */
    public abstract T zza(zzwd zzwd) throws RemoteException;

    /* access modifiers changed from: protected */
    public abstract T zzop();

    /* access modifiers changed from: protected */
    public abstract T zzoq() throws RemoteException;

    private static zzwd zzor() {
        try {
            Object newInstance = zzup.class.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            if (!(newInstance instanceof IBinder)) {
                zzayu.zzez("ClientApi class is not an instance of IBinder.");
                return null;
            }
            IBinder iBinder = (IBinder) newInstance;
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IClientApi");
            if (queryLocalInterface instanceof zzwd) {
                return (zzwd) queryLocalInterface;
            }
            return new zzwf(iBinder);
        } catch (Exception unused) {
            zzayu.zzez("Failed to instantiate ClientApi class.");
            return null;
        }
    }

    private final T zzos() {
        zzwd zzwd = zzcdp;
        if (zzwd == null) {
            zzayu.zzez("ClientApi class cannot be loaded.");
            return null;
        }
        try {
            return zza(zzwd);
        } catch (RemoteException e) {
            zzayu.zzd("Cannot invoke local loader using ClientApi class.", e);
            return null;
        }
    }

    private final T zzot() {
        try {
            return zzoq();
        } catch (RemoteException e) {
            zzayu.zzd("Cannot invoke remote loader.", e);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzayk.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, android.os.Bundle, int]
     candidates:
      com.google.android.gms.internal.ads.zzayk.zza(android.view.ViewGroup, com.google.android.gms.internal.ads.zzuj, java.lang.String, int, int):void
      com.google.android.gms.internal.ads.zzayk.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle, boolean):void */
    public final T zzd(Context context, boolean z) {
        T t;
        boolean z2 = false;
        boolean z3 = z;
        if (!z3) {
            zzve.zzou();
            if (!zzayk.zzc(context, GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE)) {
                zzayu.zzea("Google Play Services is not available.");
                z3 = true;
            }
        }
        if (DynamiteModule.getLocalVersion(context, ModuleDescriptor.MODULE_ID) > DynamiteModule.getRemoteVersion(context, ModuleDescriptor.MODULE_ID)) {
            z3 = true;
        }
        zzzn.initialize(context);
        if (zzaay.zzctf.get().booleanValue()) {
            z3 = false;
        }
        if (z3) {
            t = zzos();
            if (t == null) {
                t = zzot();
            }
        } else {
            T zzot = zzot();
            int i = zzot == null ? 1 : 0;
            if (i != 0) {
                if (zzve.zzpb().nextInt(zzabi.zzcuj.get().intValue()) == 0) {
                    z2 = true;
                }
                if (z2) {
                    Bundle bundle = new Bundle();
                    bundle.putString("action", "dynamite_load");
                    bundle.putInt("is_missing", i);
                    zzve.zzou().zza(context, zzve.zzpa().zzbma, "gmob-apps", bundle, true);
                }
            }
            t = zzot == null ? zzos() : zzot;
        }
        return t == null ? zzop() : t;
    }
}
