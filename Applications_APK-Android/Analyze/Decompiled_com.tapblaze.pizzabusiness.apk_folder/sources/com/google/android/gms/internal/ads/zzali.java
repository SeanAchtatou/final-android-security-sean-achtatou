package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzali extends IInterface {
    void onAdClicked() throws RemoteException;

    void onAdClosed() throws RemoteException;

    void onAdFailedToLoad(int i) throws RemoteException;

    void onAdImpression() throws RemoteException;

    void onAdLeftApplication() throws RemoteException;

    void onAdLoaded() throws RemoteException;

    void onAdOpened() throws RemoteException;

    void onAppEvent(String str, String str2) throws RemoteException;

    void onVideoEnd() throws RemoteException;

    void onVideoPause() throws RemoteException;

    void onVideoPlay() throws RemoteException;

    void zza(zzade zzade, String str) throws RemoteException;

    void zza(zzalj zzalj) throws RemoteException;

    void zza(zzasf zzasf) throws RemoteException;

    void zzb(Bundle bundle) throws RemoteException;

    void zzb(zzasd zzasd) throws RemoteException;

    void zzco(int i) throws RemoteException;

    void zzdj(String str) throws RemoteException;

    void zzss() throws RemoteException;

    void zzst() throws RemoteException;
}
