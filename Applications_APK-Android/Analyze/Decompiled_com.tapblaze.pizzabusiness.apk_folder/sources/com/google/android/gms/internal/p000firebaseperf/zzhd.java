package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;
import java.util.List;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzhd  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzhd {
    private static final Class<?> zzty = zziu();
    private static final zzht<?, ?> zztz = zzi(false);
    private static final zzht<?, ?> zzua = zzi(true);
    private static final zzht<?, ?> zzub = new zzhv();

    public static void zzf(Class<?> cls) {
        Class<?> cls2;
        if (!zzfc.class.isAssignableFrom(cls) && (cls2 = zzty) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static void zza(int i, List<Double> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzg(i, list, z);
        }
    }

    public static void zzb(int i, List<Float> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzf(i, list, z);
        }
    }

    public static void zzc(int i, List<Long> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzc(i, list, z);
        }
    }

    public static void zzd(int i, List<Long> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzd(i, list, z);
        }
    }

    public static void zze(int i, List<Long> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzn(i, list, z);
        }
    }

    public static void zzf(int i, List<Long> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zze(i, list, z);
        }
    }

    public static void zzg(int i, List<Long> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzl(i, list, z);
        }
    }

    public static void zzh(int i, List<Integer> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zza(i, list, z);
        }
    }

    public static void zzi(int i, List<Integer> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzj(i, list, z);
        }
    }

    public static void zzj(int i, List<Integer> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzm(i, list, z);
        }
    }

    public static void zzk(int i, List<Integer> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzb(i, list, z);
        }
    }

    public static void zzl(int i, List<Integer> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzk(i, list, z);
        }
    }

    public static void zzm(int i, List<Integer> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzh(i, list, z);
        }
    }

    public static void zzn(int i, List<Boolean> list, zzin zzin, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzi(i, list, z);
        }
    }

    public static void zza(int i, List<String> list, zzin zzin) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zza(i, list);
        }
    }

    public static void zzb(int i, List<zzeb> list, zzin zzin) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzb(i, list);
        }
    }

    public static void zza(int i, List<?> list, zzin zzin, zzhb zzhb) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zza(i, list, zzhb);
        }
    }

    public static void zzb(int i, List<?> list, zzin zzin, zzhb zzhb) throws IOException {
        if (list != null && !list.isEmpty()) {
            zzin.zzb(i, list, zzhb);
        }
    }

    static int zzc(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfz) {
            zzfz zzfz = (zzfz) list;
            i = 0;
            while (i2 < size) {
                i += zzeo.zzat(zzfz.getLong(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzeo.zzat(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int zzo(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return zzc(list) + (list.size() * zzeo.zzy(i));
    }

    static int zzd(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfz) {
            zzfz zzfz = (zzfz) list;
            i = 0;
            while (i2 < size) {
                i += zzeo.zzau(zzfz.getLong(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzeo.zzau(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int zzp(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzd(list) + (size * zzeo.zzy(i));
    }

    static int zze(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfz) {
            zzfz zzfz = (zzfz) list;
            i = 0;
            while (i2 < size) {
                i += zzeo.zzav(zzfz.getLong(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzeo.zzav(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int zzq(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zze(list) + (size * zzeo.zzy(i));
    }

    static int zzf(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfe) {
            zzfe zzfe = (zzfe) list;
            i = 0;
            while (i2 < size) {
                i += zzeo.zzae(zzfe.getInt(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzeo.zzae(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzr(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzf(list) + (size * zzeo.zzy(i));
    }

    static int zzg(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfe) {
            zzfe zzfe = (zzfe) list;
            i = 0;
            while (i2 < size) {
                i += zzeo.zzz(zzfe.getInt(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzeo.zzz(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzs(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzg(list) + (size * zzeo.zzy(i));
    }

    static int zzh(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfe) {
            zzfe zzfe = (zzfe) list;
            i = 0;
            while (i2 < size) {
                i += zzeo.zzaa(zzfe.getInt(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzeo.zzaa(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzt(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzh(list) + (size * zzeo.zzy(i));
    }

    static int zzi(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof zzfe) {
            zzfe zzfe = (zzfe) list;
            i = 0;
            while (i2 < size) {
                i += zzeo.zzab(zzfe.getInt(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zzeo.zzab(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int zzu(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return zzi(list) + (size * zzeo.zzy(i));
    }

    static int zzj(List<?> list) {
        return list.size() << 2;
    }

    static int zzv(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzeo.zzm(i, 0);
    }

    static int zzk(List<?> list) {
        return list.size() << 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzeo.zzg(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzeo.zzg(int, int):void
      com.google.android.gms.internal.firebase-perf.zzeo.zzg(int, long):int */
    static int zzw(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzeo.zzg(i, 0L);
    }

    static int zzl(List<?> list) {
        return list.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, boolean):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, double):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, float):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, com.google.android.gms.internal.firebase-perf.zzfu):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, com.google.android.gms.internal.firebase-perf.zzgl):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, java.lang.String):int
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, long):void
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, com.google.android.gms.internal.firebase-perf.zzeb):void
      com.google.android.gms.internal.firebase-perf.zzeo.zzb(int, boolean):int */
    static int zzx(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zzeo.zzb(i, true);
    }

    static int zzc(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int zzy = zzeo.zzy(i) * size;
        if (list instanceof zzfw) {
            zzfw zzfw = (zzfw) list;
            while (i4 < size) {
                Object raw = zzfw.getRaw(i4);
                if (raw instanceof zzeb) {
                    i3 = zzeo.zzb((zzeb) raw);
                } else {
                    i3 = zzeo.zzal((String) raw);
                }
                zzy += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof zzeb) {
                    i2 = zzeo.zzb((zzeb) obj);
                } else {
                    i2 = zzeo.zzal((String) obj);
                }
                zzy += i2;
                i4++;
            }
        }
        return zzy;
    }

    static int zzc(int i, Object obj, zzhb zzhb) {
        if (obj instanceof zzfu) {
            return zzeo.zza(i, (zzfu) obj);
        }
        return zzeo.zzb(i, (zzgl) obj, zzhb);
    }

    static int zzc(int i, List<?> list, zzhb zzhb) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int zzy = zzeo.zzy(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof zzfu) {
                i2 = zzeo.zza((zzfu) obj);
            } else {
                i2 = zzeo.zza((zzgl) obj, zzhb);
            }
            zzy += i2;
        }
        return zzy;
    }

    static int zzd(int i, List<zzeb> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int zzy = size * zzeo.zzy(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            zzy += zzeo.zzb(list.get(i2));
        }
        return zzy;
    }

    static int zzd(int i, List<zzgl> list, zzhb zzhb) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zzeo.zzc(i, list.get(i3), zzhb);
        }
        return i2;
    }

    public static zzht<?, ?> zzir() {
        return zztz;
    }

    public static zzht<?, ?> zzis() {
        return zzua;
    }

    public static zzht<?, ?> zzit() {
        return zzub;
    }

    private static zzht<?, ?> zzi(boolean z) {
        try {
            Class<?> zziv = zziv();
            if (zziv == null) {
                return null;
            }
            return (zzht) zziv.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> zziu() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> zziv() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    static boolean zze(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    static <T> void zza(zzgi zzgi, Object obj, Object obj2, long j) {
        zzhz.zza(obj, j, zzgi.zzc(zzhz.zzo(obj, j), zzhz.zzo(obj2, j)));
    }

    static <T, FT extends zzez<FT>> void zza(zzes zzes, Object obj, Object obj2) {
        zzex zzd = zzes.zzd(obj2);
        if (!zzd.zznt.isEmpty()) {
            zzes.zze(obj).zza(zzd);
        }
    }

    static <T, UT, UB> void zza(zzht zzht, Object obj, Object obj2) {
        zzht.zzf(obj, zzht.zzg(zzht.zzp(obj), zzht.zzp(obj2)));
    }
}
