package com.google.android.gms.internal.p000firebaseperf;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import java.net.URI;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzbv  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzbv {
    private static String[] zzib;

    public static boolean zza(URI uri, Context context) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("firebase_performance_whitelisted_domains", "array", context.getPackageName());
        if (identifier <= 0) {
            return true;
        }
        Log.i("FirebasePerformance", "Detected domain whitelist, only whitelisted domains will be measured.");
        if (zzib == null) {
            zzib = resources.getStringArray(identifier);
        }
        for (String str : zzib) {
            String host = uri.getHost();
            if (host == null || host.contains(str)) {
                return true;
            }
        }
        return false;
    }
}
