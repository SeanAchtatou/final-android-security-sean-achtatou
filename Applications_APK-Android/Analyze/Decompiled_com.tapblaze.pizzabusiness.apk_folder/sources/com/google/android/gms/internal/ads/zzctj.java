package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzctj implements zzcub<zzctg> {
    private final ScheduledExecutorService zzffx;
    private final zzczu zzfgl;
    private final zzdhd zzfov;
    private final zzcnz zzgbe;
    private String zzgeb;
    private final zzcob zzggk;
    private final Context zzup;

    public zzctj(zzdhd zzdhd, ScheduledExecutorService scheduledExecutorService, String str, zzcob zzcob, Context context, zzczu zzczu, zzcnz zzcnz) {
        this.zzfov = zzdhd;
        this.zzffx = scheduledExecutorService;
        this.zzgeb = str;
        this.zzggk = zzcob;
        this.zzup = context;
        this.zzfgl = zzczu;
        this.zzgbe = zzcnz;
    }

    public final zzdhe<zzctg> zzanc() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzckn)).booleanValue()) {
            return zzdgs.zza(new zzcti(this), this.zzfov);
        }
        return zzdgs.zzaj(null);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(String str, List list, Bundle bundle) throws Exception {
        zzazl zzazl = new zzazl();
        this.zzgbe.zzgg(str);
        zzani zzgh = this.zzgbe.zzgh(str);
        if (zzgh != null) {
            Bundle bundle2 = bundle;
            zzgh.zza(ObjectWrapper.wrap(this.zzup), this.zzgeb, bundle2, (Bundle) list.get(0), this.zzfgl.zzblm, new zzcoh(str, zzgh, zzazl));
            return zzazl;
        }
        throw new NullPointerException();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zzann() {
        Map<String, List<Bundle>> zzr = this.zzggk.zzr(this.zzgeb, this.zzfgl.zzgmm);
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : zzr.entrySet()) {
            String str = (String) next.getKey();
            arrayList.add(zzdgn.zze(zzdgs.zza(new zzctl(this, str, (List) next.getValue(), this.zzfgl.zzgml.zzccf != null ? this.zzfgl.zzgml.zzccf.getBundle(str) : null), this.zzfov)).zza(((Long) zzve.zzoy().zzd(zzzn.zzckm)).longValue(), TimeUnit.MILLISECONDS, this.zzffx).zza(Throwable.class, new zzctk(str), this.zzfov));
        }
        return zzdgs.zzi(arrayList).zza(new zzctn(arrayList), this.zzfov);
    }
}
