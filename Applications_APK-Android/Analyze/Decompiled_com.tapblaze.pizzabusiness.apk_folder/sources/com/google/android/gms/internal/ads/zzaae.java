package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaae {
    private final Object lock = new Object();
    boolean zzcsb = true;
    private final List<zzaac> zzcsc = new LinkedList();
    private final Map<String, String> zzcsd = new LinkedHashMap();
    private zzaae zzcse;

    public zzaae(boolean z, String str, String str2) {
        this.zzcsd.put("action", str);
        this.zzcsd.put("ad_format", str2);
    }

    public final void zzc(zzaae zzaae) {
        synchronized (this.lock) {
            this.zzcse = zzaae;
        }
    }

    public final zzaac zzex(long j) {
        if (!this.zzcsb) {
            return null;
        }
        return new zzaac(j, null, null);
    }

    public final boolean zza(zzaac zzaac, long j, String... strArr) {
        synchronized (this.lock) {
            for (String zzaac2 : strArr) {
                this.zzcsc.add(new zzaac(j, zzaac2, zzaac));
            }
        }
        return true;
    }

    public final String zzqt() {
        String sb;
        StringBuilder sb2 = new StringBuilder();
        synchronized (this.lock) {
            for (zzaac next : this.zzcsc) {
                long time = next.getTime();
                String zzqq = next.zzqq();
                zzaac zzqr = next.zzqr();
                if (zzqr != null && time > 0) {
                    sb2.append(zzqq);
                    sb2.append('.');
                    sb2.append(time - zzqr.getTime());
                    sb2.append(',');
                }
            }
            this.zzcsc.clear();
            if (!TextUtils.isEmpty(null)) {
                sb2.append((String) null);
            } else if (sb2.length() > 0) {
                sb2.setLength(sb2.length() - 1);
            }
            sb = sb2.toString();
        }
        return sb;
    }

    public final void zzh(String str, String str2) {
        zzzu zzuz;
        if (this.zzcsb && !TextUtils.isEmpty(str2) && (zzuz = zzq.zzku().zzuz()) != null) {
            synchronized (this.lock) {
                zzzy zzcq = zzuz.zzcq(str);
                Map<String, String> map = this.zzcsd;
                map.put(str, zzcq.zzg(map.get(str), str2));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final Map<String, String> zzqu() {
        synchronized (this.lock) {
            zzzu zzuz = zzq.zzku().zzuz();
            if (zzuz != null) {
                if (this.zzcse != null) {
                    Map<String, String> zza = zzuz.zza(this.zzcsd, this.zzcse.zzqu());
                    return zza;
                }
            }
            Map<String, String> map = this.zzcsd;
            return map;
        }
    }
}
