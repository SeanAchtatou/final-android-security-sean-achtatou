package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzdsv<K, V> {
    public final V zzcfu;
    public final zzdvf zzhoq;
    public final K zzhor;
    public final zzdvf zzhos;
}
