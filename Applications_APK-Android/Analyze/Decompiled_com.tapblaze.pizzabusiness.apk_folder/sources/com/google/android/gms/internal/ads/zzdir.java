package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdte;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdir<PrimitiveT, KeyProtoT extends zzdte, PublicKeyProtoT extends zzdte> extends zzdig<PrimitiveT, KeyProtoT> implements zzdid<PrimitiveT> {
    private final zzdiu<KeyProtoT, PublicKeyProtoT> zzgyg;
    private final zzdii<PublicKeyProtoT> zzgyh;

    public zzdir(zzdiu<KeyProtoT, PublicKeyProtoT> zzdiu, zzdii<PublicKeyProtoT> zzdii, Class<PrimitiveT> cls) {
        super(zzdiu, cls);
        this.zzgyg = zzdiu;
        this.zzgyh = zzdii;
    }
}
