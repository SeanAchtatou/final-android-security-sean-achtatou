package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbcx extends zzbcn {
    public zzbcx(zzbaz zzbaz) {
        super(zzbaz);
    }

    public final void abort() {
    }

    public final boolean zzfi(String str) {
        zzbaz zzbaz = (zzbaz) this.zzedg.get();
        if (zzbaz != null) {
            zzbaz.zza(zzfj(str), this);
        }
        zzavs.zzez("VideoStreamNoopCache is doing nothing.");
        zza(str, zzfj(str), "noop", "Noop cache is a noop.");
        return false;
    }
}
