package com.google.android.gms.ads.mediation.rtb;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationConfiguration;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class RtbSignalData {
    private final Bundle zzccf;
    private final AdSize zzdi;
    private final List<MediationConfiguration> zzejo;
    private final Context zzup;

    public RtbSignalData(Context context, List<MediationConfiguration> list, Bundle bundle, AdSize adSize) {
        this.zzup = context;
        this.zzejo = list;
        this.zzccf = bundle;
        this.zzdi = adSize;
    }

    public Context getContext() {
        return this.zzup;
    }

    @Deprecated
    public MediationConfiguration getConfiguration() {
        List<MediationConfiguration> list = this.zzejo;
        if (list == null || list.size() <= 0) {
            return null;
        }
        return this.zzejo.get(0);
    }

    public List<MediationConfiguration> getConfigurations() {
        return this.zzejo;
    }

    public Bundle getNetworkExtras() {
        return this.zzccf;
    }

    public AdSize getAdSize() {
        return this.zzdi;
    }
}
