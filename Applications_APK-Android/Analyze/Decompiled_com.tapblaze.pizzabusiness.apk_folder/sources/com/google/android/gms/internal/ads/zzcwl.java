package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzbod;
import com.google.android.gms.internal.ads.zzbrm;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcwl implements zzcox<zzbke> {
    private final Executor zzfci;
    private final ViewGroup zzfdu;
    private final zzczw zzgcs;
    /* access modifiers changed from: private */
    @Nullable
    public zzdhe<zzbke> zzgdb;
    private final zzbfx zzgea;
    private final Context zzgim;
    private final zzcwz zzgin;
    /* access modifiers changed from: private */
    public final zzcxt<zzbka, zzbke> zzgio;

    public zzcwl(Context context, Executor executor, zzbfx zzbfx, zzcxt<zzbka, zzbke> zzcxt, zzcwz zzcwz, zzczw zzczw) {
        this.zzgim = context;
        this.zzfci = executor;
        this.zzgea = zzbfx;
        this.zzgio = zzcxt;
        this.zzgin = zzcwz;
        this.zzgcs = zzczw;
        this.zzfdu = new FrameLayout(context);
    }

    public final boolean isLoading() {
        zzdhe<zzbke> zzdhe = this.zzgdb;
        return zzdhe != null && !zzdhe.isDone();
    }

    public final synchronized boolean zza(zzug zzug, String str, zzcpa zzcpa, zzcoz<? super zzbke> zzcoz) throws RemoteException {
        Preconditions.checkMainThread("loadAd must be called on the main UI thread.");
        if (str == null) {
            zzavs.zzex("Ad unit ID should not be null for app open ad.");
            this.zzfci.execute(new zzcwk(this));
            return false;
        } else if (this.zzgdb != null) {
            return false;
        } else {
            zzdad.zze(this.zzgim, zzug.zzccb);
            zzczu zzaos = this.zzgcs.zzgk(str).zzd(zzuj.zzom()).zzg(zzug).zzaos();
            zzcwp zzcwp = new zzcwp(null);
            zzcwp.zzfgl = zzaos;
            this.zzgdb = this.zzgio.zza(zzcwp, new zzcwn(this));
            zzdgs.zza(this.zzgdb, new zzcwm(this, zzcoz), this.zzgea.zzaca());
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcwz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcwz, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* access modifiers changed from: private */
    /* renamed from: zza */
    public final synchronized zzbjz zzb(zzcxs zzcxs) {
        zzbrm.zza zza;
        zzcwz zza2 = zzcwz.zza(this.zzgin);
        zza = new zzbrm.zza();
        zza.zza((zzbow) zza2, this.zzfci);
        zza.zza((zzbqg) zza2, this.zzfci);
        zza.zza(zza2);
        return this.zzgea.zzaci().zza(new zzbkf(this.zzfdu)).zzb(new zzbod.zza().zzbz(this.zzgim).zza(((zzcwp) zzcxs).zzfgl).zzahh()).zzb(zza.zzahw());
    }

    public final void zza(zzuo zzuo) {
        this.zzgcs.zzb(zzuo);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzaob() {
        this.zzgin.onAdFailedToLoad(1);
    }
}
