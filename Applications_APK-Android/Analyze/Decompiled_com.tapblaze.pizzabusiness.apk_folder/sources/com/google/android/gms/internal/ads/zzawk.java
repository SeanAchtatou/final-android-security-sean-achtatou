package com.google.android.gms.internal.ads;

import android.view.View;
import android.view.ViewGroup;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzawk extends zzawl {
    public final boolean isAttachedToWindow(View view) {
        return view.isAttachedToWindow();
    }

    public final ViewGroup.LayoutParams zzwp() {
        return new ViewGroup.LayoutParams(-1, -1);
    }
}
