package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzgv {
    public final int zzads;
    public final long zzadt;
    public volatile long zzaex;
    public volatile long zzaey;

    public zzgv(int i, long j) {
        this.zzads = i;
        this.zzadt = j;
        this.zzaex = j;
        this.zzaey = j;
    }
}
