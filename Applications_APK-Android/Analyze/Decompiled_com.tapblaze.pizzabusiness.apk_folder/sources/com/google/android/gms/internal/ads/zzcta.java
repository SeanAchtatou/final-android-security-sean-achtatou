package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcta implements zzcub<zzctb> {
    private final Bundle zzfhf;
    private final zzdhd zzfov;

    public zzcta(zzdhd zzdhd, Bundle bundle) {
        this.zzfov = zzdhd;
        this.zzfhf = bundle;
    }

    public final zzdhe<zzctb> zzanc() {
        return this.zzfov.zzd(new zzctd(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzctb zzanl() throws Exception {
        return new zzctb(this.zzfhf);
    }
}
