package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.google.android.gms.ads.internal.zzq;
import com.ironsource.sdk.ISNAdView.ISNAdViewConstants;
import com.ironsource.sdk.constants.Constants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbjb implements zzajv<zzbjf> {
    private final zzpn zzfcc;
    private final Context zzup;
    private final PowerManager zzyw;

    public zzbjb(Context context, zzpn zzpn) {
        this.zzup = context;
        this.zzfcc = zzpn;
        this.zzyw = (PowerManager) context.getSystemService("power");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    /* renamed from: zza */
    public final JSONObject zzj(zzbjf zzbjf) throws JSONException {
        JSONObject jSONObject;
        boolean z;
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject2 = new JSONObject();
        if (zzbjf.zzfcr == null) {
            jSONObject = new JSONObject();
        } else {
            zzpt zzpt = zzbjf.zzfcr;
            if (this.zzfcc.zzkl() != null) {
                boolean z2 = zzpt.zzbnz;
                JSONObject jSONObject3 = new JSONObject();
                JSONObject put = jSONObject3.put("afmaVersion", this.zzfcc.zzkk()).put("activeViewJSON", this.zzfcc.zzkl()).put("timestamp", zzbjf.timestamp).put("adFormat", this.zzfcc.zzkj()).put("hashCode", this.zzfcc.zzkm());
                zzpn zzpn = this.zzfcc;
                JSONObject put2 = put.put("isMraid", false).put("isStopped", false).put("isPaused", zzbjf.zzfco).put("isNative", this.zzfcc.zzkn());
                if (Build.VERSION.SDK_INT >= 20) {
                    z = this.zzyw.isInteractive();
                } else {
                    z = this.zzyw.isScreenOn();
                }
                put2.put("isScreenOn", z).put("appMuted", zzq.zzkv().zzpf()).put("appVolume", (double) zzq.zzkv().zzpe()).put(Constants.RequestParameters.DEVICE_VOLUME, (double) zzawq.zzbe(this.zzup.getApplicationContext()));
                Rect rect = new Rect();
                Display defaultDisplay = ((WindowManager) this.zzup.getSystemService("window")).getDefaultDisplay();
                rect.right = defaultDisplay.getWidth();
                rect.bottom = defaultDisplay.getHeight();
                jSONObject3.put("windowVisibility", zzpt.zzzd).put("isAttachedToWindow", z2).put("viewBox", new JSONObject().put(ViewHierarchyConstants.DIMENSION_TOP_KEY, zzpt.zzboa.top).put("bottom", zzpt.zzboa.bottom).put(ViewHierarchyConstants.DIMENSION_LEFT_KEY, zzpt.zzboa.left).put("right", zzpt.zzboa.right)).put("adBox", new JSONObject().put(ViewHierarchyConstants.DIMENSION_TOP_KEY, zzpt.zzbob.top).put("bottom", zzpt.zzbob.bottom).put(ViewHierarchyConstants.DIMENSION_LEFT_KEY, zzpt.zzbob.left).put("right", zzpt.zzbob.right)).put("globalVisibleBox", new JSONObject().put(ViewHierarchyConstants.DIMENSION_TOP_KEY, zzpt.zzboc.top).put("bottom", zzpt.zzboc.bottom).put(ViewHierarchyConstants.DIMENSION_LEFT_KEY, zzpt.zzboc.left).put("right", zzpt.zzboc.right)).put("globalVisibleBoxVisible", zzpt.zzbod).put("localVisibleBox", new JSONObject().put(ViewHierarchyConstants.DIMENSION_TOP_KEY, zzpt.zzboe.top).put("bottom", zzpt.zzboe.bottom).put(ViewHierarchyConstants.DIMENSION_LEFT_KEY, zzpt.zzboe.left).put("right", zzpt.zzboe.right)).put("localVisibleBoxVisible", zzpt.zzbof).put("hitBox", new JSONObject().put(ViewHierarchyConstants.DIMENSION_TOP_KEY, zzpt.zzbog.top).put("bottom", zzpt.zzbog.bottom).put(ViewHierarchyConstants.DIMENSION_LEFT_KEY, zzpt.zzbog.left).put("right", zzpt.zzbog.right)).put("screenDensity", (double) this.zzup.getResources().getDisplayMetrics().density);
                jSONObject3.put(ISNAdViewConstants.IS_VISIBLE_KEY, zzbjf.zzbnq);
                if (((Boolean) zzve.zzoy().zzd(zzzn.zzckk)).booleanValue()) {
                    JSONArray jSONArray2 = new JSONArray();
                    if (zzpt.zzboi != null) {
                        for (Rect next : zzpt.zzboi) {
                            jSONArray2.put(new JSONObject().put(ViewHierarchyConstants.DIMENSION_TOP_KEY, next.top).put("bottom", next.bottom).put(ViewHierarchyConstants.DIMENSION_LEFT_KEY, next.left).put("right", next.right));
                        }
                    }
                    jSONObject3.put("scrollableContainerBoxes", jSONArray2);
                }
                if (!TextUtils.isEmpty(zzbjf.zzfcq)) {
                    jSONObject3.put("doneReasonCode", "u");
                }
                jSONObject = jSONObject3;
            } else {
                throw new JSONException("Active view Info cannot be null.");
            }
        }
        jSONArray.put(jSONObject);
        jSONObject2.put("units", jSONArray);
        return jSONObject2;
    }
}
