package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzgs  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzgs<T> implements zzhb<T> {
    private final zzgl zzsz;
    private final boolean zzta;
    private final zzht<?, ?> zztj;
    private final zzes<?> zztk;

    private zzgs(zzht<?, ?> zzht, zzes<?> zzes, zzgl zzgl) {
        this.zztj = zzht;
        this.zzta = zzes.zze(zzgl);
        this.zztk = zzes;
        this.zzsz = zzgl;
    }

    static <T> zzgs<T> zza(zzht<?, ?> zzht, zzes<?> zzes, zzgl zzgl) {
        return new zzgs<>(zzht, zzes, zzgl);
    }

    public final boolean equals(T t, T t2) {
        if (!this.zztj.zzp(t).equals(this.zztj.zzp(t2))) {
            return false;
        }
        if (this.zzta) {
            return this.zztk.zzd(t).equals(this.zztk.zzd(t2));
        }
        return true;
    }

    public final int hashCode(T t) {
        int hashCode = this.zztj.zzp(t).hashCode();
        return this.zzta ? (hashCode * 53) + this.zztk.zzd(t).hashCode() : hashCode;
    }

    public final void zzd(T t, T t2) {
        zzhd.zza(this.zztj, t, t2);
        if (this.zzta) {
            zzhd.zza(this.zztk, t, t2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.firebase-perf.zzin.zza(int, java.lang.Object):void
     arg types: [int, com.google.android.gms.internal.firebase-perf.zzeb]
     candidates:
      com.google.android.gms.internal.firebase-perf.zzin.zza(int, double):void
      com.google.android.gms.internal.firebase-perf.zzin.zza(int, float):void
      com.google.android.gms.internal.firebase-perf.zzin.zza(int, long):void
      com.google.android.gms.internal.firebase-perf.zzin.zza(int, com.google.android.gms.internal.firebase-perf.zzeb):void
      com.google.android.gms.internal.firebase-perf.zzin.zza(int, java.lang.String):void
      com.google.android.gms.internal.firebase-perf.zzin.zza(int, java.util.List<java.lang.String>):void
      com.google.android.gms.internal.firebase-perf.zzin.zza(int, boolean):void
      com.google.android.gms.internal.firebase-perf.zzin.zza(int, java.lang.Object):void */
    public final void zza(T t, zzin zzin) throws IOException {
        Iterator<Map.Entry<?, Object>> it = this.zztk.zzd(t).iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            zzez zzez = (zzez) next.getKey();
            if (zzez.zzhc() != zzio.MESSAGE || zzez.zzhd() || zzez.zzhe()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof zzfs) {
                zzin.zza(zzez.getNumber(), (Object) ((zzfs) next).zzhu().zzgb());
            } else {
                zzin.zza(zzez.getNumber(), next.getValue());
            }
        }
        zzht<?, ?> zzht = this.zztj;
        zzht.zzc(zzht.zzp(t), zzin);
    }

    public final void zzf(T t) {
        this.zztj.zzf(t);
        this.zztk.zzf(t);
    }

    public final boolean zzn(T t) {
        return this.zztk.zzd(t).isInitialized();
    }

    public final int zzm(T t) {
        zzht<?, ?> zzht = this.zztj;
        int zzq = zzht.zzq(zzht.zzp(t)) + 0;
        return this.zzta ? zzq + this.zztk.zzd(t).zzha() : zzq;
    }
}
