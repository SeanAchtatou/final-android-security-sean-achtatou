package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbmd;
import com.google.android.gms.internal.ads.zzbob;
import com.google.android.gms.internal.ads.zzsy;
import java.util.LinkedList;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcxg<R extends zzbob<AdT>, AdT extends zzbmd> implements zzcxt<R, AdT> {
    private final Executor executor;
    private final zzcxt<R, AdT> zzgio;
    /* access modifiers changed from: private */
    public final zzdbb zzgjq;
    private final zzdax zzgjr;
    /* access modifiers changed from: private */
    public final zzcxt<R, zzdbi<AdT>> zzgjs;
    private final LinkedList<zzcxn<R>> zzgjt;
    /* access modifiers changed from: private */
    public zzcxn<R> zzgju;
    private zzdhe<zzdbi<AdT>> zzgjv;
    /* access modifiers changed from: private */
    public int zzgjw = zzcxm.zzgkd;
    private R zzgjx;
    private final zzdgt<zzdbi<AdT>> zzgjy = new zzcxk(this);

    public zzcxg(zzdbb zzdbb, zzdax zzdax, zzcxt<R, AdT> zzcxt, zzcxt<R, zzdbi<AdT>> zzcxt2, Executor executor2) {
        this.zzgjq = zzdbb;
        this.zzgjr = zzdax;
        this.zzgio = zzcxt;
        this.zzgjs = zzcxt2;
        this.zzgjt = new LinkedList<>();
        this.executor = executor2;
        this.zzgjr.zza(new zzcxl(this));
    }

    /* access modifiers changed from: private */
    /* renamed from: zzaoh */
    public final synchronized R zzaog() {
        return this.zzgjx;
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [com.google.android.gms.internal.ads.zzcxv, com.google.android.gms.internal.ads.zzcxv<R>] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final com.google.android.gms.internal.ads.zzdhe<AdT> zza(com.google.android.gms.internal.ads.zzcxs r9, com.google.android.gms.internal.ads.zzcxv<R> r10) {
        /*
            r8 = this;
            com.google.android.gms.internal.ads.zzboe r0 = r10.zzc(r9)
            java.lang.Object r0 = r0.zzadg()
            com.google.android.gms.internal.ads.zzbob r0 = (com.google.android.gms.internal.ads.zzbob) r0
            com.google.android.gms.internal.ads.zzczu r0 = r0.zzady()
            com.google.android.gms.internal.ads.zzug r4 = r0.zzgml
            java.lang.String r5 = r0.zzgmm
            com.google.android.gms.internal.ads.zzuo r7 = r0.zzgmp
            com.google.android.gms.internal.ads.zzcxn r0 = new com.google.android.gms.internal.ads.zzcxn
            java.util.concurrent.Executor r6 = r8.executor
            r1 = r0
            r2 = r10
            r3 = r9
            r1.<init>(r2, r3, r4, r5, r6, r7)
            com.google.android.gms.internal.ads.zzdbb r1 = r8.zzgjq
            com.google.android.gms.internal.ads.zzug r2 = r0.zzdio
            java.lang.String r3 = r0.zzbqz
            com.google.android.gms.internal.ads.zzuo r4 = r0.zzgey
            com.google.android.gms.internal.ads.zzdbl r2 = r1.zza(r2, r3, r4)
            com.google.android.gms.internal.ads.zzdbi r1 = r1.zza(r2)
            if (r1 == 0) goto L_0x003d
            r2 = 0
            r8.zzgjx = r2
            java.util.LinkedList<com.google.android.gms.internal.ads.zzcxn<R>> r2 = r8.zzgjt
            r2.add(r0)
            com.google.android.gms.internal.ads.zzdhe r9 = r8.zza(r1, r9, r10)
            return r9
        L_0x003d:
            com.google.android.gms.internal.ads.zzdhe<com.google.android.gms.internal.ads.zzdbi<AdT>> r1 = r8.zzgjv
            if (r1 == 0) goto L_0x0049
            boolean r1 = r1.isDone()
            if (r1 != 0) goto L_0x0049
            r1 = 1
            goto L_0x004a
        L_0x0049:
            r1 = 0
        L_0x004a:
            if (r1 == 0) goto L_0x008e
            int r1 = com.google.android.gms.internal.ads.zzcxm.zzgkf
            r8.zzgjw = r1
            com.google.android.gms.internal.ads.zzdbb r1 = r8.zzgjq
            com.google.android.gms.internal.ads.zzug r2 = r0.zzdio
            java.lang.String r3 = r0.zzbqz
            com.google.android.gms.internal.ads.zzuo r4 = r0.zzgey
            com.google.android.gms.internal.ads.zzdbl r1 = r1.zza(r2, r3, r4)
            com.google.android.gms.internal.ads.zzdbb r2 = r8.zzgjq
            com.google.android.gms.internal.ads.zzcxn<R> r3 = r8.zzgju
            com.google.android.gms.internal.ads.zzug r3 = r3.zzdio
            com.google.android.gms.internal.ads.zzcxn<R> r4 = r8.zzgju
            java.lang.String r4 = r4.zzbqz
            com.google.android.gms.internal.ads.zzuo r5 = r0.zzgey
            com.google.android.gms.internal.ads.zzdbl r2 = r2.zza(r3, r4, r5)
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x008e
            int r9 = com.google.android.gms.internal.ads.zzcxm.zzgke
            r8.zzgjw = r9
            com.google.android.gms.internal.ads.zzcxt<R, com.google.android.gms.internal.ads.zzdbi<AdT>> r9 = r8.zzgjs
            java.lang.Object r9 = r9.zzaog()
            com.google.android.gms.internal.ads.zzbob r9 = (com.google.android.gms.internal.ads.zzbob) r9
            r8.zzgjx = r9
            com.google.android.gms.internal.ads.zzdhe<com.google.android.gms.internal.ads.zzdbi<AdT>> r9 = r8.zzgjv
            com.google.android.gms.internal.ads.zzcxi r10 = new com.google.android.gms.internal.ads.zzcxi
            r10.<init>(r8)
            java.util.concurrent.Executor r0 = r8.executor
            com.google.android.gms.internal.ads.zzdhe r9 = com.google.android.gms.internal.ads.zzdgs.zzb(r9, r10, r0)
            return r9
        L_0x008e:
            java.util.LinkedList<com.google.android.gms.internal.ads.zzcxn<R>> r1 = r8.zzgjt
            r1.add(r0)
            com.google.android.gms.internal.ads.zzcxt<R, AdT> r0 = r8.zzgio
            com.google.android.gms.internal.ads.zzdhe r9 = r0.zza(r9, r10)
            com.google.android.gms.internal.ads.zzcxt<R, AdT> r10 = r8.zzgio
            java.lang.Object r10 = r10.zzaog()
            com.google.android.gms.internal.ads.zzbob r10 = (com.google.android.gms.internal.ads.zzbob) r10
            r8.zzgjx = r10
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcxg.zza(com.google.android.gms.internal.ads.zzcxs, com.google.android.gms.internal.ads.zzcxv):com.google.android.gms.internal.ads.zzdhe");
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [com.google.android.gms.internal.ads.zzcxv, com.google.android.gms.internal.ads.zzcxv<R>] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private final com.google.android.gms.internal.ads.zzdhe<AdT> zza(com.google.android.gms.internal.ads.zzdbi<AdT> r3, com.google.android.gms.internal.ads.zzcxs r4, com.google.android.gms.internal.ads.zzcxv<R> r5) {
        /*
            r2 = this;
            com.google.android.gms.internal.ads.zzboe r0 = r5.zzc(r4)
            AdT r1 = r3.zzgpd
            if (r1 == 0) goto L_0x0022
            java.lang.Object r4 = r0.zzadg()
            com.google.android.gms.internal.ads.zzbob r4 = (com.google.android.gms.internal.ads.zzbob) r4
            AdT r5 = r3.zzgpd
            com.google.android.gms.internal.ads.zzcxq r5 = r5.zzagt()
            com.google.android.gms.internal.ads.zzcxq r4 = r4.zzadz()
            r5.zzb(r4)
            AdT r3 = r3.zzgpd
            com.google.android.gms.internal.ads.zzdhe r3 = com.google.android.gms.internal.ads.zzdgs.zzaj(r3)
            return r3
        L_0x0022:
            com.google.android.gms.internal.ads.zzczt r3 = r3.zzelt
            r0.zza(r3)
            com.google.android.gms.internal.ads.zzcxt<R, AdT> r3 = r2.zzgio
            com.google.android.gms.internal.ads.zzdhe r3 = r3.zza(r4, r5)
            com.google.android.gms.internal.ads.zzcxj r4 = new com.google.android.gms.internal.ads.zzcxj
            r4.<init>(r0)
            java.util.concurrent.Executor r5 = r2.executor
            com.google.android.gms.internal.ads.zzdhe r3 = com.google.android.gms.internal.ads.zzdgs.zzb(r3, r4, r5)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcxg.zza(com.google.android.gms.internal.ads.zzdbi, com.google.android.gms.internal.ads.zzcxs, com.google.android.gms.internal.ads.zzcxv):com.google.android.gms.internal.ads.zzdhe");
    }

    /* access modifiers changed from: private */
    public final void zza(zzcxn<R> zzcxn) {
        while (true) {
            zzdhe<zzdbi<AdT>> zzdhe = this.zzgjv;
            if (zzdhe == null || zzdhe.isDone()) {
                if (zzcxn != null || !this.zzgjt.isEmpty()) {
                    if (zzcxn == null) {
                        zzcxn = this.zzgjt.remove();
                    }
                    zzdbb zzdbb = this.zzgjq;
                    if (zzdbb.zzb(zzdbb.zza(zzcxn.zzdio, zzcxn.zzbqz, zzcxn.zzgey))) {
                        this.zzgju = zzcxn;
                        this.zzgjv = this.zzgjs.zza(zzcxn.zzgki, zzcxn.zzgkh);
                        zzdgs.zza(this.zzgjv, this.zzgjy, zzcxn.executor);
                        return;
                    }
                    zzcxn = null;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzaoi() {
        synchronized (this) {
            zza(this.zzgju);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzdbi zzdbi) throws Exception {
        zzdbi.zzgpc.zzahb().zzc((zzsy.zza) ((zzdrt) zzsy.zza.zzmz().zza(zzsy.zza.C0025zza.zzmx().zzb(zzsy.zza.zzc.IN_MEMORY).zza(zzsy.zza.zzd.zznb())).zzbaf()));
        return zza(zzdbi, this.zzgju.zzgki, this.zzgju.zzgkh);
    }
}
