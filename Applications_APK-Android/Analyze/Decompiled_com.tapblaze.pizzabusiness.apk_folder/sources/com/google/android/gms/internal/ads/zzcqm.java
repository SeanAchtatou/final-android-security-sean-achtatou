package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcqm implements zzcub<zzcqn> {
    private final zzczu zzfgl;
    private final zzdhd zzfov;

    public zzcqm(zzdhd zzdhd, zzczu zzczu) {
        this.zzfov = zzdhd;
        this.zzfgl = zzczu;
    }

    public final zzdhe<zzcqn> zzanc() {
        return this.zzfov.zzd(new zzcqp(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcqn zzane() throws Exception {
        return new zzcqn(this.zzfgl.zzgmp);
    }
}
