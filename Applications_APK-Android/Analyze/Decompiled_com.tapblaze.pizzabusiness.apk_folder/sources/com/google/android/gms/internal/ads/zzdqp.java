package com.google.android.gms.internal.ads;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzdqp extends Iterator<Byte> {
    byte nextByte();
}
