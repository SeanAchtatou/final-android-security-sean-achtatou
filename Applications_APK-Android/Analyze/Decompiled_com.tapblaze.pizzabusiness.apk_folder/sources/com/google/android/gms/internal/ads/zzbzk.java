package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbzk implements zzdgf {
    private final JSONObject zzfcs;
    private final zzbzh zzfpu;

    zzbzk(zzbzh zzbzh, JSONObject jSONObject) {
        this.zzfpu = zzbzh;
        this.zzfcs = jSONObject;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfpu.zza(this.zzfcs, (zzbdi) obj);
    }
}
