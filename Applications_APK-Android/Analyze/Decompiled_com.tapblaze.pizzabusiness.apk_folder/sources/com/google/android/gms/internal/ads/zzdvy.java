package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdvy {
    public static final int[] zzhow = new int[0];
    private static final int zzhtu = 11;
    private static final int zzhtv = 12;
    private static final int zzhtw = 16;
    private static final int zzhtx = 26;
    public static final long[] zzhty = new long[0];
    private static final float[] zzhtz = new float[0];
    private static final double[] zzhua = new double[0];
    private static final boolean[] zzhub = new boolean[0];
    public static final String[] zzhuc = new String[0];
    private static final byte[][] zzhud = new byte[0][];
    private static final byte[] zzhue = new byte[0];
}
