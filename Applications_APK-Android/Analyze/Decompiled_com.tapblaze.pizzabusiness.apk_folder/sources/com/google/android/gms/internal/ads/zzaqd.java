package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaqd extends zzgc implements zzaqb {
    zzaqd(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.internal.request.IAdRequestService");
    }

    public final zzapx zza(zzapv zzapv) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, zzapv);
        Parcel transactAndReadException = transactAndReadException(1, obtainAndWriteInterfaceToken);
        zzapx zzapx = (zzapx) zzge.zza(transactAndReadException, zzapx.CREATOR);
        transactAndReadException.recycle();
        return zzapx;
    }

    public final void zza(zzapv zzapv, zzaqc zzaqc) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, zzapv);
        zzge.zza(obtainAndWriteInterfaceToken, zzaqc);
        zza(2, obtainAndWriteInterfaceToken);
    }

    public final void zza(zzaqk zzaqk, zzaqe zzaqe) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, zzaqk);
        zzge.zza(obtainAndWriteInterfaceToken, zzaqe);
        zza(4, obtainAndWriteInterfaceToken);
    }

    public final void zzb(zzaqk zzaqk, zzaqe zzaqe) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, zzaqk);
        zzge.zza(obtainAndWriteInterfaceToken, zzaqe);
        zza(5, obtainAndWriteInterfaceToken);
    }

    public final void zzc(zzaqk zzaqk, zzaqe zzaqe) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        zzge.zza(obtainAndWriteInterfaceToken, zzaqk);
        zzge.zza(obtainAndWriteInterfaceToken, zzaqe);
        zza(6, obtainAndWriteInterfaceToken);
    }

    public final void zza(String str, zzaqe zzaqe) throws RemoteException {
        Parcel obtainAndWriteInterfaceToken = obtainAndWriteInterfaceToken();
        obtainAndWriteInterfaceToken.writeString(str);
        zzge.zza(obtainAndWriteInterfaceToken, zzaqe);
        zza(7, obtainAndWriteInterfaceToken);
    }
}
