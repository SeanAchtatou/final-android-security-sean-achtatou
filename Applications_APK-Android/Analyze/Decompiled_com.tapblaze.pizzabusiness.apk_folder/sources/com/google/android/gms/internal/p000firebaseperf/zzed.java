package com.google.android.gms.internal.p000firebaseperf;

import java.util.Comparator;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzed  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzed implements Comparator<zzeb> {
    zzed() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        zzeb zzeb = (zzeb) obj;
        zzeb zzeb2 = (zzeb) obj2;
        zzeg zzeg = (zzeg) zzeb.iterator();
        zzeg zzeg2 = (zzeg) zzeb2.iterator();
        while (zzeg.hasNext() && zzeg2.hasNext()) {
            int compare = Integer.compare(zzeb.zza(zzeg.nextByte()), zzeb.zza(zzeg2.nextByte()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(zzeb.size(), zzeb2.size());
    }
}
