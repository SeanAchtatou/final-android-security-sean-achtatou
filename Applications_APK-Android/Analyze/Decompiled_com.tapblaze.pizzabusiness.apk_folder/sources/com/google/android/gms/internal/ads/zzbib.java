package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbib implements zzdxg<zzbdr> {
    private static final zzbib zzfaz = new zzbib();

    public static zzbib zzafa() {
        return zzfaz;
    }

    public final /* synthetic */ Object get() {
        return (zzbdr) zzdxm.zza(new zzbdr(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
