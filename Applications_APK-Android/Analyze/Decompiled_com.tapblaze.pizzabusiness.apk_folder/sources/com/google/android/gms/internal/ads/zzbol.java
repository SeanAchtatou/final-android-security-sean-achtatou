package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbol implements zzdxg<zzczs> {
    private final zzbod zzfhi;

    private zzbol(zzbod zzbod) {
        this.zzfhi = zzbod;
    }

    public static zzbol zzk(zzbod zzbod) {
        return new zzbol(zzbod);
    }

    public final /* synthetic */ Object get() {
        return this.zzfhi.zzahe();
    }
}
