package com.google.android.gms.internal.p000firebaseperf;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzby  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzby {
    private final URL zzid;

    public zzby(URL url) {
        this.zzid = url;
    }

    public final URLConnection openConnection() throws IOException {
        return this.zzid.openConnection();
    }

    public final String toString() {
        return this.zzid.toString();
    }
}
