package com.google.android.gms.internal.p000firebaseperf;

import android.content.Context;
import com.google.firebase.perf.internal.RemoteConfigManager;
import com.google.firebase.perf.internal.zzd;

/* renamed from: com.google.android.gms.internal.firebase-perf.zzaf  reason: invalid package */
/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzaf {
    private static volatile zzaf zzab;
    private zzbl zzaf = new zzbl();
    private RemoteConfigManager zzag = RemoteConfigManager.zzch();
    private zzay zzah = zzay.zzba();
    private zzbi zzai = zzbi.zzco();

    private zzaf(RemoteConfigManager remoteConfigManager, zzbl zzbl, zzay zzay) {
    }

    private static boolean zza(float f) {
        return 0.0f <= f && f <= 1.0f;
    }

    private static boolean zzb(long j) {
        return j >= 0;
    }

    private static boolean zzc(long j) {
        return j > 0;
    }

    private static boolean zzd(long j) {
        return j >= 0;
    }

    private static boolean zze(long j) {
        return j > 0;
    }

    public static synchronized zzaf zzl() {
        zzaf zzaf2;
        synchronized (zzaf.class) {
            if (zzab == null) {
                zzab = new zzaf(null, null, null);
            }
            zzaf2 = zzab;
        }
        return zzaf2;
    }

    public final void zzb(Context context) {
        zzc(context.getApplicationContext());
    }

    public final void zzc(Context context) {
        zzbi.zzco().zze(zzbx.zzg(context));
        this.zzah.zzd(context);
    }

    public final void zza(zzbl zzbl) {
        this.zzaf = zzbl;
    }

    public final boolean zzm() {
        Boolean zzn = zzn();
        if ((zzn == null || zzn.booleanValue()) && zzp()) {
            return true;
        }
        return false;
    }

    public final Boolean zzn() {
        if (zzo().booleanValue()) {
            return false;
        }
        zzag zzae = zzag.zzae();
        zzbo<Boolean> zzg = zzg(zzae);
        if (zzg.isPresent()) {
            return (Boolean) zza(zzae, zzg.get());
        }
        zzbo<Boolean> zza = zza(zzae);
        if (zza.isPresent()) {
            return (Boolean) zza(zzae, zza.get());
        }
        this.zzai.zzm("CollectionEnabled metadata key unknown or value not found in manifest.");
        return (Boolean) zza(zzae, null);
    }

    public final Boolean zzo() {
        zzah zzah2 = zzah.zzah();
        zzbo<Boolean> zza = zza(zzah2);
        if (zza.isPresent()) {
            return (Boolean) zza(zzah2, zza.get());
        }
        return (Boolean) zza(zzah2, false);
    }

    public final void zza(boolean z) {
        String zzag2;
        if (!zzo().booleanValue() && (zzag2 = zzag.zzae().zzag()) != null) {
            this.zzah.zza(zzag2, z);
        }
    }

    public final boolean zzp() {
        boolean z;
        boolean z2;
        this.zzai.zzm("Retrieving master flag for Firebase Performance SDK enabled configuration value.");
        zzap zzar = zzap.zzar();
        zzbo<Boolean> zzb = this.zzag.zzb(zzar.zzaj());
        if (!zzb.isPresent()) {
            zzbo<Boolean> zzg = zzg(zzar);
            if (zzg.isPresent()) {
                z = ((Boolean) zza(zzar, zzg.get())).booleanValue();
            } else {
                z = ((Boolean) zza(zzar, true)).booleanValue();
            }
        } else if (this.zzag.zzci()) {
            z = ((Boolean) zza(zzar, false)).booleanValue();
        } else {
            this.zzah.zza(zzar.zzag(), zzb.get().booleanValue());
            z = ((Boolean) zza(zzar, zzb.get())).booleanValue();
        }
        if (z) {
            this.zzai.zzm("Retrieving Firebase Performance SDK disabled versions configuration value.");
            zzam zzao = zzam.zzao();
            zzbo<String> zzc = this.zzag.zzc(zzao.zzaj());
            if (zzc.isPresent()) {
                this.zzah.zza(zzao.zzag(), zzc.get());
                z2 = zza(zzao, zzc.get(), zza(zzc.get()));
            } else {
                zzbo<String> zzh = zzh(zzao);
                if (zzh.isPresent()) {
                    z2 = zza(zzao, zzh.get(), zza(zzh.get()));
                } else {
                    z2 = zza(zzao, "", zza(""));
                }
            }
            if (!z2) {
                return true;
            }
        }
        return false;
    }

    private static boolean zza(String str) {
        if (str.trim().isEmpty()) {
            return false;
        }
        for (String trim : str.split(";")) {
            if (trim.trim().equals(zzd.VERSION_NAME)) {
                return true;
            }
        }
        return false;
    }

    public final float zzq() {
        this.zzai.zzm("Retrieving trace sampling rate configuration value.");
        zzaw zzay = zzaw.zzay();
        zzbo<Float> zzc = zzc(zzay);
        if (!zzc.isPresent() || !zza(zzc.get().floatValue())) {
            zzbo<Float> zze = zze(zzay);
            if (!zze.isPresent() || !zza(zze.get().floatValue())) {
                return ((Float) zza(zzay, Float.valueOf(1.0f))).floatValue();
            }
            return ((Float) zza(zzay, zze.get())).floatValue();
        }
        this.zzah.zza(zzay.zzag(), zzc.get().floatValue());
        return ((Float) zza(zzay, zzc.get())).floatValue();
    }

    public final float zzr() {
        this.zzai.zzm("Retrieving network request sampling rate configuration value.");
        zzak zzam = zzak.zzam();
        zzbo<Float> zzc = zzc(zzam);
        if (!zzc.isPresent() || !zza(zzc.get().floatValue())) {
            zzbo<Float> zze = zze(zzam);
            if (!zze.isPresent() || !zza(zze.get().floatValue())) {
                return ((Float) zza(zzam, Float.valueOf(1.0f))).floatValue();
            }
            return ((Float) zza(zzam, zze.get())).floatValue();
        }
        this.zzah.zza(zzam.zzag(), zzc.get().floatValue());
        return ((Float) zza(zzam, zzc.get())).floatValue();
    }

    public final float zzs() {
        this.zzai.zzm("Retrieving session sampling rate configuration value.");
        zzav zzax = zzav.zzax();
        zzbo<Float> zzd = this.zzaf.zzd(zzax.zzaf());
        if (zzd.isPresent()) {
            float floatValue = zzd.get().floatValue() / 100.0f;
            if (zza(floatValue)) {
                return ((Float) zza(zzax, Float.valueOf(floatValue))).floatValue();
            }
        }
        zzbo<Float> zzc = zzc(zzax);
        if (!zzc.isPresent() || !zza(zzc.get().floatValue())) {
            zzbo<Float> zze = zze(zzax);
            if (!zze.isPresent() || !zza(zze.get().floatValue())) {
                return ((Float) zza(zzax, Float.valueOf(0.01f))).floatValue();
            }
            return ((Float) zza(zzax, zze.get())).floatValue();
        }
        this.zzah.zza(zzax.zzag(), zzc.get().floatValue());
        return ((Float) zza(zzax, zzc.get())).floatValue();
    }

    public final long zzt() {
        this.zzai.zzm("Retrieving Session CPU Capture Frequency on foreground (milliseonds) configuration value.");
        zzar zzat = zzar.zzat();
        zzbo<Long> zzb = zzb(zzat);
        if (zzb.isPresent() && zzd(zzb.get().longValue())) {
            return ((Long) zza(zzat, zzb.get())).longValue();
        }
        zzbo<Long> zzd = zzd(zzat);
        if (!zzd.isPresent() || !zzd(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzat);
            if (!zzf.isPresent() || !zzd(zzf.get().longValue())) {
                return ((Long) zza(zzat, 100L)).longValue();
            }
            return ((Long) zza(zzat, zzf.get())).longValue();
        }
        this.zzah.zza(zzat.zzag(), zzd.get().longValue());
        return ((Long) zza(zzat, zzd.get())).longValue();
    }

    public final long zzu() {
        this.zzai.zzm("Retrieving Session CPU Capture Frequency on background (milliseonds) configuration value.");
        zzao zzaq = zzao.zzaq();
        zzbo<Long> zzb = zzb(zzaq);
        if (zzb.isPresent() && zzd(zzb.get().longValue())) {
            return ((Long) zza(zzaq, zzb.get())).longValue();
        }
        zzbo<Long> zzd = zzd(zzaq);
        if (!zzd.isPresent() || !zzd(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzaq);
            if (!zzf.isPresent() || !zzd(zzf.get().longValue())) {
                return ((Long) zza(zzaq, 0L)).longValue();
            }
            return ((Long) zza(zzaq, zzf.get())).longValue();
        }
        this.zzah.zza(zzaq.zzag(), zzd.get().longValue());
        return ((Long) zza(zzaq, zzd.get())).longValue();
    }

    public final long zzv() {
        this.zzai.zzm("Retrieving Session Memory Capture Frequency on foreground (milliseonds) configuration value.");
        zzas zzau = zzas.zzau();
        zzbo<Long> zzb = zzb(zzau);
        if (zzb.isPresent() && zzd(zzb.get().longValue())) {
            return ((Long) zza(zzau, zzb.get())).longValue();
        }
        zzbo<Long> zzd = zzd(zzau);
        if (!zzd.isPresent() || !zzd(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzau);
            if (!zzf.isPresent() || !zzd(zzf.get().longValue())) {
                return ((Long) zza(zzau, 100L)).longValue();
            }
            return ((Long) zza(zzau, zzf.get())).longValue();
        }
        this.zzah.zza(zzau.zzag(), zzd.get().longValue());
        return ((Long) zza(zzau, zzd.get())).longValue();
    }

    public final long zzw() {
        this.zzai.zzm("Retrieving Session Memory Capture Frequency on background (milliseonds) configuration value.");
        zzat zzav = zzat.zzav();
        zzbo<Long> zzb = zzb(zzav);
        if (zzb.isPresent() && zzd(zzb.get().longValue())) {
            return ((Long) zza(zzav, zzb.get())).longValue();
        }
        zzbo<Long> zzd = zzd(zzav);
        if (!zzd.isPresent() || !zzd(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzav);
            if (!zzf.isPresent() || !zzd(zzf.get().longValue())) {
                return ((Long) zza(zzav, 0L)).longValue();
            }
            return ((Long) zza(zzav, zzf.get())).longValue();
        }
        this.zzah.zza(zzav.zzag(), zzd.get().longValue());
        return ((Long) zza(zzav, zzd.get())).longValue();
    }

    public final long zzx() {
        this.zzai.zzm("Retrieving Max Duration (in minutes) of single Session configuration value.");
        zzaq zzas = zzaq.zzas();
        zzbo<Long> zzb = zzb(zzas);
        if (zzb.isPresent() && zze(zzb.get().longValue())) {
            return ((Long) zza(zzas, zzb.get())).longValue();
        }
        zzbo<Long> zzd = zzd(zzas);
        if (!zzd.isPresent() || !zze(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzas);
            if (!zzf.isPresent() || !zze(zzf.get().longValue())) {
                return ((Long) zza(zzas, 240L)).longValue();
            }
            return ((Long) zza(zzas, zzf.get())).longValue();
        }
        this.zzah.zza(zzas.zzag(), zzd.get().longValue());
        return ((Long) zza(zzas, zzd.get())).longValue();
    }

    public final long zzy() {
        this.zzai.zzm("Retrieving trace event count foreground configuration value.");
        zzax zzaz = zzax.zzaz();
        zzbo<Long> zzd = zzd(zzaz);
        if (!zzd.isPresent() || !zzb(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzaz);
            if (!zzf.isPresent() || !zzb(zzf.get().longValue())) {
                return ((Long) zza(zzaz, 300L)).longValue();
            }
            return ((Long) zza(zzaz, zzf.get())).longValue();
        }
        this.zzah.zza(zzaz.zzag(), zzd.get().longValue());
        return ((Long) zza(zzaz, zzd.get())).longValue();
    }

    public final long zzz() {
        this.zzai.zzm("Retrieving trace event count background configuration value.");
        zzau zzaw = zzau.zzaw();
        zzbo<Long> zzd = zzd(zzaw);
        if (!zzd.isPresent() || !zzb(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzaw);
            if (!zzf.isPresent() || !zzb(zzf.get().longValue())) {
                return ((Long) zza(zzaw, 30L)).longValue();
            }
            return ((Long) zza(zzaw, zzf.get())).longValue();
        }
        this.zzah.zza(zzaw.zzag(), zzd.get().longValue());
        return ((Long) zza(zzaw, zzd.get())).longValue();
    }

    public final long zzaa() {
        this.zzai.zzm("Retrieving network event count foreground configuration value.");
        zzal zzan = zzal.zzan();
        zzbo<Long> zzd = zzd(zzan);
        if (!zzd.isPresent() || !zzb(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzan);
            if (!zzf.isPresent() || !zzb(zzf.get().longValue())) {
                return ((Long) zza(zzan, 700L)).longValue();
            }
            return ((Long) zza(zzan, zzf.get())).longValue();
        }
        this.zzah.zza(zzan.zzag(), zzd.get().longValue());
        return ((Long) zza(zzan, zzd.get())).longValue();
    }

    public final long zzab() {
        this.zzai.zzm("Retrieving network event count background configuration value.");
        zzai zzai2 = zzai.zzai();
        zzbo<Long> zzd = zzd(zzai2);
        if (!zzd.isPresent() || !zzb(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzai2);
            if (!zzf.isPresent() || !zzb(zzf.get().longValue())) {
                return ((Long) zza(zzai2, 70L)).longValue();
            }
            return ((Long) zza(zzai2, zzf.get())).longValue();
        }
        this.zzah.zza(zzai2.zzag(), zzd.get().longValue());
        return ((Long) zza(zzai2, zzd.get())).longValue();
    }

    public final long zzac() {
        this.zzai.zzm("Retrieving rate limiting time range (in seconds) configuration value.");
        zzan zzap = zzan.zzap();
        zzbo<Long> zzd = zzd(zzap);
        if (!zzd.isPresent() || !zzc(zzd.get().longValue())) {
            zzbo<Long> zzf = zzf(zzap);
            if (!zzf.isPresent() || !zzc(zzf.get().longValue())) {
                return ((Long) zza(zzap, 600L)).longValue();
            }
            return ((Long) zza(zzap, zzf.get())).longValue();
        }
        this.zzah.zza(zzap.zzag(), zzd.get().longValue());
        return ((Long) zza(zzap, zzd.get())).longValue();
    }

    public final String zzad() {
        String zzf;
        zzaj zzak = zzaj.zzak();
        if (zzd.zzcx) {
            return (String) zza(zzak, zzaj.zzal());
        }
        String zzaj = zzak.zzaj();
        long j = -1;
        if (zzaj != null) {
            j = ((Long) this.zzag.zza(zzaj, -1L)).longValue();
        }
        String zzag2 = zzak.zzag();
        if (!zzaj.zzg(j) || (zzf = zzaj.zzf(j)) == null) {
            zzbo<String> zzh = zzh(zzak);
            if (zzh.isPresent()) {
                return (String) zza(zzak, zzh.get());
            }
            return (String) zza(zzak, zzaj.zzal());
        }
        this.zzah.zza(zzag2, zzf);
        return (String) zza(zzak, zzf);
    }

    private final zzbo<Boolean> zza(zzaz<Boolean> zzaz) {
        return this.zzaf.zzb(zzaz.zzaf());
    }

    private final zzbo<Long> zzb(zzaz<Long> zzaz) {
        return this.zzaf.zze(zzaz.zzaf());
    }

    private final zzbo<Float> zzc(zzaz<Float> zzaz) {
        return this.zzag.zzd(zzaz.zzaj());
    }

    private final zzbo<Long> zzd(zzaz<Long> zzaz) {
        return this.zzag.zze(zzaz.zzaj());
    }

    private final zzbo<Float> zze(zzaz<Float> zzaz) {
        return this.zzah.zzd(zzaz.zzag());
    }

    private final zzbo<Long> zzf(zzaz<Long> zzaz) {
        return this.zzah.zze(zzaz.zzag());
    }

    private final zzbo<Boolean> zzg(zzaz<Boolean> zzaz) {
        return this.zzah.zzb(zzaz.zzag());
    }

    private final zzbo<String> zzh(zzaz<String> zzaz) {
        return this.zzah.zzc(zzaz.zzag());
    }

    private final <T> T zza(zzaz<T> zzaz, T t) {
        this.zzai.zzm(String.format("Config resolver result for flag: '%s' is: '%s'.", zzaz.getClass().getName(), String.valueOf(t)));
        return t;
    }

    private final <T> boolean zza(zzaz<T> zzaz, T t, boolean z) {
        this.zzai.zzm(String.format("Config resolver result for flag: '%s' is: '%s'. Resolving value as '%s'", zzaz.getClass().getName(), String.valueOf(t), String.valueOf(z)));
        return z;
    }
}
