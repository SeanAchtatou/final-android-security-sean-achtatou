package com.google.android.gms.internal.ads;

import android.net.Uri;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzlx implements zzmb, zzme {
    private final Uri uri;
    private final zzddu zzact;
    private final zzhi zzacx;
    private zzhg zzade;
    private final int zzazs;
    private final zzma zzazt;
    private zzme zzazu;
    private final String zzazw = null;
    private final zzno zzbbc;
    private final zzji zzbbd;
    private final int zzbbe;
    private boolean zzbbf;

    public zzlx(Uri uri2, zzno zzno, zzji zzji, int i, zzddu zzddu, zzma zzma, String str, int i2) {
        this.uri = uri2;
        this.zzbbc = zzno;
        this.zzbbd = zzji;
        this.zzazs = i;
        this.zzact = zzddu;
        this.zzazt = zzma;
        this.zzbbe = i2;
        this.zzacx = new zzhi();
    }

    public final void zzhr() throws IOException {
    }

    public final void zza(zzgk zzgk, boolean z, zzme zzme) {
        this.zzazu = zzme;
        this.zzade = new zzmp(-9223372036854775807L, false);
        zzme.zzb(this.zzade, null);
    }

    public final zzlz zza(int i, zznj zznj) {
        zzoc.checkArgument(i == 0);
        return new zzlp(this.uri, this.zzbbc.zzih(), this.zzbbd.zzgl(), this.zzazs, this.zzact, this.zzazt, this, zznj, null, this.zzbbe);
    }

    public final void zzb(zzlz zzlz) {
        ((zzlp) zzlz).release();
    }

    public final void zzhs() {
        this.zzazu = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzhg.zza(int, com.google.android.gms.internal.ads.zzhi, boolean):com.google.android.gms.internal.ads.zzhi
     arg types: [int, com.google.android.gms.internal.ads.zzhi, int]
     candidates:
      com.google.android.gms.internal.ads.zzhg.zza(int, com.google.android.gms.internal.ads.zzhl, boolean):com.google.android.gms.internal.ads.zzhl
      com.google.android.gms.internal.ads.zzhg.zza(int, com.google.android.gms.internal.ads.zzhi, boolean):com.google.android.gms.internal.ads.zzhi */
    public final void zzb(zzhg zzhg, Object obj) {
        boolean z = false;
        if (zzhg.zza(0, this.zzacx, false).zzagj != -9223372036854775807L) {
            z = true;
        }
        if (!this.zzbbf || z) {
            this.zzade = zzhg;
            this.zzbbf = z;
            this.zzazu.zzb(this.zzade, null);
        }
    }
}
