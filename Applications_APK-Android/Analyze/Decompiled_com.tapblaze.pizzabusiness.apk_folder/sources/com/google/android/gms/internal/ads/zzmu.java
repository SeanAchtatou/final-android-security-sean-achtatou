package com.google.android.gms.internal.ads;

import android.net.Uri;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzmu extends zzhd {
    private final Uri uri;

    public zzmu(String str, Uri uri2) {
        super(str);
        this.uri = uri2;
    }
}
