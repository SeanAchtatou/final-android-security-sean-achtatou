package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzzw {
    public static void zza(zzzu zzzu, zzzr zzzr) {
        if (zzzr.getContext() == null) {
            throw new IllegalArgumentException("Context can't be null. Please set up context in CsiConfiguration.");
        } else if (!TextUtils.isEmpty(zzzr.zzkk())) {
            zzzu.zza(zzzr.getContext(), zzzr.zzkk(), zzzr.zzqk(), zzzr.zzql());
        } else {
            throw new IllegalArgumentException("AfmaVersion can't be null or empty. Please set up afmaVersion in CsiConfiguration.");
        }
    }
}
