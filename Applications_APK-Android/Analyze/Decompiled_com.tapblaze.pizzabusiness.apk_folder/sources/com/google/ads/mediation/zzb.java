package com.google.ads.mediation;

import com.google.android.gms.ads.reward.AdMetadataListener;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzb extends AdMetadataListener {
    private final /* synthetic */ AbstractAdViewAdapter zzlp;

    zzb(AbstractAdViewAdapter abstractAdViewAdapter) {
        this.zzlp = abstractAdViewAdapter;
    }

    public final void onAdMetadataChanged() {
        if (this.zzlp.zzlu != null && this.zzlp.zzlv != null) {
            this.zzlp.zzlv.zzb(this.zzlp.zzlu.getAdMetadata());
        }
    }
}
