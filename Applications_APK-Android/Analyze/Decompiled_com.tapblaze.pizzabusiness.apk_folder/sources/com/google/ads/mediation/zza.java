package com.google.ads.mediation;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zza implements RewardedVideoAdListener {
    private final /* synthetic */ AbstractAdViewAdapter zzlp;

    zza(AbstractAdViewAdapter abstractAdViewAdapter) {
        this.zzlp = abstractAdViewAdapter;
    }

    public final void onRewardedVideoAdLoaded() {
        this.zzlp.zzlv.onAdLoaded(this.zzlp);
    }

    public final void onRewardedVideoAdOpened() {
        this.zzlp.zzlv.onAdOpened(this.zzlp);
    }

    public final void onRewardedVideoStarted() {
        this.zzlp.zzlv.onVideoStarted(this.zzlp);
    }

    public final void onRewardedVideoAdClosed() {
        this.zzlp.zzlv.onAdClosed(this.zzlp);
        InterstitialAd unused = this.zzlp.zzlu = null;
    }

    public final void onRewarded(RewardItem rewardItem) {
        this.zzlp.zzlv.onRewarded(this.zzlp, rewardItem);
    }

    public final void onRewardedVideoAdLeftApplication() {
        this.zzlp.zzlv.onAdLeftApplication(this.zzlp);
    }

    public final void onRewardedVideoAdFailedToLoad(int i) {
        this.zzlp.zzlv.onAdFailedToLoad(this.zzlp, i);
    }

    public final void onRewardedVideoCompleted() {
        this.zzlp.zzlv.onVideoCompleted(this.zzlp);
    }
}
