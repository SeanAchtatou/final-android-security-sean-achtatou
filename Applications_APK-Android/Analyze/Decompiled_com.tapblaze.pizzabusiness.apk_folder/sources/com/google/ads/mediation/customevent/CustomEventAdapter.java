package com.google.ads.mediation.customevent;

import android.app.Activity;
import android.view.View;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.customevent.CustomEventExtras;
import com.google.android.gms.internal.ads.zzayu;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class CustomEventAdapter implements MediationBannerAdapter<CustomEventExtras, CustomEventServerParameters>, MediationInterstitialAdapter<CustomEventExtras, CustomEventServerParameters> {
    private View zzmj;
    private CustomEventBanner zzmk;
    private CustomEventInterstitial zzml;

    private static <T> T zzao(String str) {
        try {
            return Class.forName(str).newInstance();
        } catch (Throwable th) {
            String message = th.getMessage();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 46 + String.valueOf(message).length());
            sb.append("Could not instantiate custom event adapter: ");
            sb.append(str);
            sb.append(". ");
            sb.append(message);
            zzayu.zzez(sb.toString());
            return null;
        }
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    class zza implements CustomEventInterstitialListener {
        private final CustomEventAdapter zzmm;
        private final MediationInterstitialListener zzmn;

        public zza(CustomEventAdapter customEventAdapter, MediationInterstitialListener mediationInterstitialListener) {
            this.zzmm = customEventAdapter;
            this.zzmn = mediationInterstitialListener;
        }

        public final void onReceivedAd() {
            zzayu.zzea("Custom event adapter called onReceivedAd.");
            this.zzmn.onReceivedAd(CustomEventAdapter.this);
        }

        public final void onFailedToReceiveAd() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.zzmn.onFailedToReceiveAd(this.zzmm, AdRequest.ErrorCode.NO_FILL);
        }

        public final void onPresentScreen() {
            zzayu.zzea("Custom event adapter called onPresentScreen.");
            this.zzmn.onPresentScreen(this.zzmm);
        }

        public final void onDismissScreen() {
            zzayu.zzea("Custom event adapter called onDismissScreen.");
            this.zzmn.onDismissScreen(this.zzmm);
        }

        public final void onLeaveApplication() {
            zzayu.zzea("Custom event adapter called onLeaveApplication.");
            this.zzmn.onLeaveApplication(this.zzmm);
        }
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    static final class zzb implements CustomEventBannerListener {
        private final CustomEventAdapter zzmm;
        private final MediationBannerListener zzmp;

        public zzb(CustomEventAdapter customEventAdapter, MediationBannerListener mediationBannerListener) {
            this.zzmm = customEventAdapter;
            this.zzmp = mediationBannerListener;
        }

        public final void onReceivedAd(View view) {
            zzayu.zzea("Custom event adapter called onReceivedAd.");
            this.zzmm.zza(view);
            this.zzmp.onReceivedAd(this.zzmm);
        }

        public final void onFailedToReceiveAd() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.zzmp.onFailedToReceiveAd(this.zzmm, AdRequest.ErrorCode.NO_FILL);
        }

        public final void onClick() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.zzmp.onClick(this.zzmm);
        }

        public final void onPresentScreen() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.zzmp.onPresentScreen(this.zzmm);
        }

        public final void onDismissScreen() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.zzmp.onDismissScreen(this.zzmm);
        }

        public final void onLeaveApplication() {
            zzayu.zzea("Custom event adapter called onFailedToReceiveAd.");
            this.zzmp.onLeaveApplication(this.zzmm);
        }
    }

    public final void destroy() {
        CustomEventBanner customEventBanner = this.zzmk;
        if (customEventBanner != null) {
            customEventBanner.destroy();
        }
        CustomEventInterstitial customEventInterstitial = this.zzml;
        if (customEventInterstitial != null) {
            customEventInterstitial.destroy();
        }
    }

    public final Class<CustomEventExtras> getAdditionalParametersType() {
        return CustomEventExtras.class;
    }

    public final View getBannerView() {
        return this.zzmj;
    }

    public final Class<CustomEventServerParameters> getServerParametersType() {
        return CustomEventServerParameters.class;
    }

    public final void requestBannerAd(MediationBannerListener mediationBannerListener, Activity activity, CustomEventServerParameters customEventServerParameters, AdSize adSize, MediationAdRequest mediationAdRequest, CustomEventExtras customEventExtras) {
        Object obj;
        this.zzmk = (CustomEventBanner) zzao(customEventServerParameters.className);
        if (this.zzmk == null) {
            mediationBannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        if (customEventExtras == null) {
            obj = null;
        } else {
            obj = customEventExtras.getExtra(customEventServerParameters.label);
        }
        Activity activity2 = activity;
        this.zzmk.requestBannerAd(new zzb(this, mediationBannerListener), activity2, customEventServerParameters.label, customEventServerParameters.parameter, adSize, mediationAdRequest, obj);
    }

    public final void requestInterstitialAd(MediationInterstitialListener mediationInterstitialListener, Activity activity, CustomEventServerParameters customEventServerParameters, MediationAdRequest mediationAdRequest, CustomEventExtras customEventExtras) {
        Object obj;
        this.zzml = (CustomEventInterstitial) zzao(customEventServerParameters.className);
        if (this.zzml == null) {
            mediationInterstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        if (customEventExtras == null) {
            obj = null;
        } else {
            obj = customEventExtras.getExtra(customEventServerParameters.label);
        }
        Activity activity2 = activity;
        this.zzml.requestInterstitialAd(new zza(this, mediationInterstitialListener), activity2, customEventServerParameters.label, customEventServerParameters.parameter, mediationAdRequest, obj);
    }

    public final void showInterstitial() {
        this.zzml.showInterstitial();
    }

    /* access modifiers changed from: private */
    public final void zza(View view) {
        this.zzmj = view;
    }
}
