package com.google.zxing.pdf417.decoder;

import com.google.zxing.FormatException;
import com.google.zxing.pdf417.PDF417ResultMetadata;
import java.math.BigInteger;
import java.util.Arrays;

final class DecodedBitStreamParser {
    private static final int AL = 28;
    private static final int AS = 27;
    private static final int BEGIN_MACRO_PDF417_CONTROL_BLOCK = 928;
    private static final int BEGIN_MACRO_PDF417_OPTIONAL_FIELD = 923;
    private static final int BYTE_COMPACTION_MODE_LATCH = 901;
    private static final int BYTE_COMPACTION_MODE_LATCH_6 = 924;
    private static final int ECI_CHARSET = 927;
    private static final int ECI_GENERAL_PURPOSE = 926;
    private static final int ECI_USER_DEFINED = 925;
    private static final BigInteger[] EXP900;
    private static final int LL = 27;
    private static final int MACRO_PDF417_OPTIONAL_FIELD_ADDRESSEE = 4;
    private static final int MACRO_PDF417_OPTIONAL_FIELD_CHECKSUM = 6;
    private static final int MACRO_PDF417_OPTIONAL_FIELD_FILE_NAME = 0;
    private static final int MACRO_PDF417_OPTIONAL_FIELD_FILE_SIZE = 5;
    private static final int MACRO_PDF417_OPTIONAL_FIELD_SEGMENT_COUNT = 1;
    private static final int MACRO_PDF417_OPTIONAL_FIELD_SENDER = 3;
    private static final int MACRO_PDF417_OPTIONAL_FIELD_TIME_STAMP = 2;
    private static final int MACRO_PDF417_TERMINATOR = 922;
    private static final int MAX_NUMERIC_CODEWORDS = 15;
    private static final char[] MIXED_CHARS = "0123456789&\r\t,:#-.$/+%*=^".toCharArray();
    private static final int ML = 28;
    private static final int MODE_SHIFT_TO_BYTE_COMPACTION_MODE = 913;
    private static final int NUMBER_OF_SEQUENCE_CODEWORDS = 2;
    private static final int NUMERIC_COMPACTION_MODE_LATCH = 902;
    private static final int PAL = 29;
    private static final int PL = 25;
    private static final int PS = 29;
    private static final char[] PUNCT_CHARS = ";<>@[\\]_`~!\r\t,:\n-.$/\"|*()?{}'".toCharArray();
    private static final int TEXT_COMPACTION_MODE_LATCH = 900;

    private enum Mode {
        ALPHA,
        LOWER,
        MIXED,
        PUNCT,
        ALPHA_SHIFT,
        PUNCT_SHIFT
    }

    static {
        BigInteger[] bigIntegerArr = new BigInteger[16];
        EXP900 = bigIntegerArr;
        bigIntegerArr[0] = BigInteger.ONE;
        BigInteger valueOf = BigInteger.valueOf(900);
        EXP900[1] = valueOf;
        int i = 2;
        while (true) {
            BigInteger[] bigIntegerArr2 = EXP900;
            if (i < bigIntegerArr2.length) {
                bigIntegerArr2[i] = bigIntegerArr2[i - 1].multiply(valueOf);
                i++;
            } else {
                return;
            }
        }
    }

    private DecodedBitStreamParser() {
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.google.zxing.common.DecoderResult decode(int[] r6, java.lang.String r7) throws com.google.zxing.FormatException {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            int r1 = r6.length
            r2 = 1
            int r1 = r1 << r2
            r0.<init>(r1)
            java.nio.charset.Charset r1 = java.nio.charset.StandardCharsets.ISO_8859_1
            r2 = r6[r2]
            com.google.zxing.pdf417.PDF417ResultMetadata r3 = new com.google.zxing.pdf417.PDF417ResultMetadata
            r3.<init>()
            r4 = 2
        L_0x0012:
            r5 = 0
            r5 = r6[r5]
            if (r4 >= r5) goto L_0x006d
            r5 = 913(0x391, float:1.28E-42)
            if (r2 == r5) goto L_0x0058
            switch(r2) {
                case 900: goto L_0x0053;
                case 901: goto L_0x004e;
                case 902: goto L_0x0049;
                default: goto L_0x001e;
            }
        L_0x001e:
            switch(r2) {
                case 922: goto L_0x0044;
                case 923: goto L_0x0044;
                case 924: goto L_0x004e;
                case 925: goto L_0x0041;
                case 926: goto L_0x003e;
                case 927: goto L_0x002d;
                case 928: goto L_0x0028;
                default: goto L_0x0021;
            }
        L_0x0021:
            int r4 = r4 + -1
            int r2 = textCompaction(r6, r4, r0)
            goto L_0x0060
        L_0x0028:
            int r2 = decodeMacroBlock(r6, r4, r3)
            goto L_0x0060
        L_0x002d:
            int r2 = r4 + 1
            r1 = r6[r4]
            com.google.zxing.common.CharacterSetECI r1 = com.google.zxing.common.CharacterSetECI.getCharacterSetECIByValue(r1)
            java.lang.String r1 = r1.name()
            java.nio.charset.Charset r1 = java.nio.charset.Charset.forName(r1)
            goto L_0x0060
        L_0x003e:
            int r2 = r4 + 2
            goto L_0x0060
        L_0x0041:
            int r2 = r4 + 1
            goto L_0x0060
        L_0x0044:
            com.google.zxing.FormatException r6 = com.google.zxing.FormatException.getFormatInstance()
            throw r6
        L_0x0049:
            int r2 = numericCompaction(r6, r4, r0)
            goto L_0x0060
        L_0x004e:
            int r2 = byteCompaction(r2, r6, r1, r4, r0)
            goto L_0x0060
        L_0x0053:
            int r2 = textCompaction(r6, r4, r0)
            goto L_0x0060
        L_0x0058:
            int r2 = r4 + 1
            r4 = r6[r4]
            char r4 = (char) r4
            r0.append(r4)
        L_0x0060:
            int r4 = r6.length
            if (r2 >= r4) goto L_0x0068
            int r4 = r2 + 1
            r2 = r6[r2]
            goto L_0x0012
        L_0x0068:
            com.google.zxing.FormatException r6 = com.google.zxing.FormatException.getFormatInstance()
            throw r6
        L_0x006d:
            int r6 = r0.length()
            if (r6 == 0) goto L_0x0081
            com.google.zxing.common.DecoderResult r6 = new com.google.zxing.common.DecoderResult
            java.lang.String r0 = r0.toString()
            r1 = 0
            r6.<init>(r1, r0, r1, r7)
            r6.setOther(r3)
            return r6
        L_0x0081:
            com.google.zxing.FormatException r6 = com.google.zxing.FormatException.getFormatInstance()
            goto L_0x0087
        L_0x0086:
            throw r6
        L_0x0087:
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.pdf417.decoder.DecodedBitStreamParser.decode(int[], java.lang.String):com.google.zxing.common.DecoderResult");
    }

    static int decodeMacroBlock(int[] iArr, int i, PDF417ResultMetadata pDF417ResultMetadata) throws FormatException {
        if (i + 2 <= iArr[0]) {
            int[] iArr2 = new int[2];
            int i2 = i;
            int i3 = 0;
            while (i3 < 2) {
                iArr2[i3] = iArr[i2];
                i3++;
                i2++;
            }
            pDF417ResultMetadata.setSegmentIndex(Integer.parseInt(decodeBase900toBase10(iArr2, 2)));
            StringBuilder sb = new StringBuilder();
            int textCompaction = textCompaction(iArr, i2, sb);
            pDF417ResultMetadata.setFileId(sb.toString());
            int i4 = iArr[textCompaction] == BEGIN_MACRO_PDF417_OPTIONAL_FIELD ? textCompaction + 1 : -1;
            while (textCompaction < iArr[0]) {
                int i5 = iArr[textCompaction];
                if (i5 == MACRO_PDF417_TERMINATOR) {
                    textCompaction++;
                    pDF417ResultMetadata.setLastSegment(true);
                } else if (i5 == BEGIN_MACRO_PDF417_OPTIONAL_FIELD) {
                    int i6 = textCompaction + 1;
                    switch (iArr[i6]) {
                        case 0:
                            StringBuilder sb2 = new StringBuilder();
                            textCompaction = textCompaction(iArr, i6 + 1, sb2);
                            pDF417ResultMetadata.setFileName(sb2.toString());
                            continue;
                        case 1:
                            StringBuilder sb3 = new StringBuilder();
                            textCompaction = numericCompaction(iArr, i6 + 1, sb3);
                            pDF417ResultMetadata.setSegmentCount(Integer.parseInt(sb3.toString()));
                            continue;
                        case 2:
                            StringBuilder sb4 = new StringBuilder();
                            textCompaction = numericCompaction(iArr, i6 + 1, sb4);
                            pDF417ResultMetadata.setTimestamp(Long.parseLong(sb4.toString()));
                            continue;
                        case 3:
                            StringBuilder sb5 = new StringBuilder();
                            textCompaction = textCompaction(iArr, i6 + 1, sb5);
                            pDF417ResultMetadata.setSender(sb5.toString());
                            continue;
                        case 4:
                            StringBuilder sb6 = new StringBuilder();
                            textCompaction = textCompaction(iArr, i6 + 1, sb6);
                            pDF417ResultMetadata.setAddressee(sb6.toString());
                            continue;
                        case 5:
                            StringBuilder sb7 = new StringBuilder();
                            textCompaction = numericCompaction(iArr, i6 + 1, sb7);
                            pDF417ResultMetadata.setFileSize(Long.parseLong(sb7.toString()));
                            continue;
                        case 6:
                            StringBuilder sb8 = new StringBuilder();
                            textCompaction = numericCompaction(iArr, i6 + 1, sb8);
                            pDF417ResultMetadata.setChecksum(Integer.parseInt(sb8.toString()));
                            continue;
                        default:
                            throw FormatException.getFormatInstance();
                    }
                } else {
                    throw FormatException.getFormatInstance();
                }
            }
            if (i4 != -1) {
                int i7 = textCompaction - i4;
                if (pDF417ResultMetadata.isLastSegment()) {
                    i7--;
                }
                pDF417ResultMetadata.setOptionalData(Arrays.copyOfRange(iArr, i4, i7 + i4));
            }
            return textCompaction;
        }
        throw FormatException.getFormatInstance();
    }

    private static int textCompaction(int[] iArr, int i, StringBuilder sb) {
        int[] iArr2 = new int[((iArr[0] - i) << 1)];
        int[] iArr3 = new int[((iArr[0] - i) << 1)];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i4 < TEXT_COMPACTION_MODE_LATCH) {
                iArr2[i2] = i4 / 30;
                iArr2[i2 + 1] = i4 % 30;
                i2 += 2;
            } else if (i4 != MODE_SHIFT_TO_BYTE_COMPACTION_MODE) {
                if (i4 != 928) {
                    switch (i4) {
                        case TEXT_COMPACTION_MODE_LATCH /*900*/:
                            iArr2[i2] = TEXT_COMPACTION_MODE_LATCH;
                            i2++;
                            break;
                        default:
                            switch (i4) {
                            }
                        case BYTE_COMPACTION_MODE_LATCH /*901*/:
                        case NUMERIC_COMPACTION_MODE_LATCH /*902*/:
                            i = i3 - 1;
                            z = true;
                            break;
                    }
                }
                i = i3 - 1;
                z = true;
            } else {
                iArr2[i2] = MODE_SHIFT_TO_BYTE_COMPACTION_MODE;
                i = i3 + 1;
                iArr3[i2] = iArr[i3];
                i2++;
            }
            i = i3;
        }
        decodeTextCompaction(iArr2, iArr3, i2, sb);
        return i;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00de A[PHI: r1 
      PHI: (r1v5 com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode) = (r1v2 com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode), (r1v2 com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode), (r1v2 com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode), (r1v20 com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode) binds: [B:55:0x00cd, B:44:0x00ac, B:33:0x0085, B:19:0x0054] A[DONT_GENERATE, DONT_INLINE]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void decodeTextCompaction(int[] r12, int[] r13, int r14, java.lang.StringBuilder r15) {
        /*
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r0 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            r2 = 0
            r3 = r1
            r1 = r0
            r0 = 0
        L_0x0008:
            if (r0 >= r14) goto L_0x00f4
            r4 = r12[r0]
            int[] r5 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.AnonymousClass1.$SwitchMap$com$google$zxing$pdf417$decoder$DecodedBitStreamParser$Mode
            int r6 = r1.ordinal()
            r5 = r5[r6]
            r6 = 32
            r7 = 29
            r8 = 26
            r9 = 913(0x391, float:1.28E-42)
            r10 = 900(0x384, float:1.261E-42)
            switch(r5) {
                case 1: goto L_0x00c3;
                case 2: goto L_0x00a3;
                case 3: goto L_0x0077;
                case 4: goto L_0x0057;
                case 5: goto L_0x0042;
                case 6: goto L_0x0023;
                default: goto L_0x0021;
            }
        L_0x0021:
            goto L_0x00ea
        L_0x0023:
            if (r4 >= r7) goto L_0x002a
            char[] r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.PUNCT_CHARS
            char r1 = r1[r4]
            goto L_0x0047
        L_0x002a:
            if (r4 == r7) goto L_0x003e
            if (r4 == r10) goto L_0x003a
            if (r4 == r9) goto L_0x0031
            goto L_0x0037
        L_0x0031:
            r1 = r13[r0]
            char r1 = (char) r1
            r15.append(r1)
        L_0x0037:
            r1 = r3
            goto L_0x00ea
        L_0x003a:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            goto L_0x00ea
        L_0x003e:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            goto L_0x00ea
        L_0x0042:
            if (r4 >= r8) goto L_0x004b
            int r4 = r4 + 65
            char r1 = (char) r4
        L_0x0047:
            r4 = r1
            r1 = r3
            goto L_0x00eb
        L_0x004b:
            if (r4 == r8) goto L_0x0054
            if (r4 == r10) goto L_0x0050
            goto L_0x0037
        L_0x0050:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            goto L_0x00ea
        L_0x0054:
            r1 = r3
            goto L_0x00de
        L_0x0057:
            if (r4 >= r7) goto L_0x005f
            char[] r5 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.PUNCT_CHARS
            char r4 = r5[r4]
            goto L_0x00eb
        L_0x005f:
            if (r4 == r7) goto L_0x0073
            if (r4 == r10) goto L_0x006f
            if (r4 == r9) goto L_0x0067
            goto L_0x00ea
        L_0x0067:
            r4 = r13[r0]
            char r4 = (char) r4
            r15.append(r4)
            goto L_0x00ea
        L_0x006f:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            goto L_0x00ea
        L_0x0073:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            goto L_0x00ea
        L_0x0077:
            r5 = 25
            if (r4 >= r5) goto L_0x0081
            char[] r5 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.MIXED_CHARS
            char r4 = r5[r4]
            goto L_0x00eb
        L_0x0081:
            if (r4 == r10) goto L_0x00a0
            if (r4 == r9) goto L_0x0099
            switch(r4) {
                case 25: goto L_0x0095;
                case 26: goto L_0x00de;
                case 27: goto L_0x0091;
                case 28: goto L_0x008d;
                case 29: goto L_0x008a;
                default: goto L_0x0088;
            }
        L_0x0088:
            goto L_0x00ea
        L_0x008a:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r3 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.PUNCT_SHIFT
            goto L_0x00d3
        L_0x008d:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            goto L_0x00ea
        L_0x0091:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.LOWER
            goto L_0x00ea
        L_0x0095:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.PUNCT
            goto L_0x00ea
        L_0x0099:
            r4 = r13[r0]
            char r4 = (char) r4
            r15.append(r4)
            goto L_0x00ea
        L_0x00a0:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            goto L_0x00ea
        L_0x00a3:
            if (r4 >= r8) goto L_0x00a8
            int r4 = r4 + 97
            goto L_0x00c7
        L_0x00a8:
            if (r4 == r10) goto L_0x00c0
            if (r4 == r9) goto L_0x00b9
            switch(r4) {
                case 26: goto L_0x00de;
                case 27: goto L_0x00b6;
                case 28: goto L_0x00b3;
                case 29: goto L_0x00b0;
                default: goto L_0x00af;
            }
        L_0x00af:
            goto L_0x00ea
        L_0x00b0:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r3 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.PUNCT_SHIFT
            goto L_0x00d3
        L_0x00b3:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.MIXED
            goto L_0x00ea
        L_0x00b6:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r3 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA_SHIFT
            goto L_0x00d3
        L_0x00b9:
            r4 = r13[r0]
            char r4 = (char) r4
            r15.append(r4)
            goto L_0x00ea
        L_0x00c0:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
            goto L_0x00ea
        L_0x00c3:
            if (r4 >= r8) goto L_0x00c9
            int r4 = r4 + 65
        L_0x00c7:
            char r4 = (char) r4
            goto L_0x00eb
        L_0x00c9:
            if (r4 == r10) goto L_0x00e8
            if (r4 == r9) goto L_0x00e1
            switch(r4) {
                case 26: goto L_0x00de;
                case 27: goto L_0x00db;
                case 28: goto L_0x00d8;
                case 29: goto L_0x00d1;
                default: goto L_0x00d0;
            }
        L_0x00d0:
            goto L_0x00ea
        L_0x00d1:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r3 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.PUNCT_SHIFT
        L_0x00d3:
            r4 = 0
            r11 = r3
            r3 = r1
            r1 = r11
            goto L_0x00eb
        L_0x00d8:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.MIXED
            goto L_0x00ea
        L_0x00db:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.LOWER
            goto L_0x00ea
        L_0x00de:
            r4 = 32
            goto L_0x00eb
        L_0x00e1:
            r4 = r13[r0]
            char r4 = (char) r4
            r15.append(r4)
            goto L_0x00ea
        L_0x00e8:
            com.google.zxing.pdf417.decoder.DecodedBitStreamParser$Mode r1 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.Mode.ALPHA
        L_0x00ea:
            r4 = 0
        L_0x00eb:
            if (r4 == 0) goto L_0x00f0
            r15.append(r4)
        L_0x00f0:
            int r0 = r0 + 1
            goto L_0x0008
        L_0x00f4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.pdf417.decoder.DecodedBitStreamParser.decodeTextCompaction(int[], int[], int, java.lang.StringBuilder):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0021 A[ADDED_TO_REGION, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int byteCompaction(int r16, int[] r17, java.nio.charset.Charset r18, int r19, java.lang.StringBuilder r20) {
        /*
            r0 = r16
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            r2 = 901(0x385, float:1.263E-42)
            r3 = 900(0x384, double:4.447E-321)
            r5 = 928(0x3a0, float:1.3E-42)
            r6 = 900(0x384, float:1.261E-42)
            r7 = 6
            r10 = 0
            if (r0 == r2) goto L_0x0059
            r2 = 924(0x39c, float:1.295E-42)
            if (r0 == r2) goto L_0x001b
            r0 = r19
            goto L_0x00c9
        L_0x001b:
            r0 = r19
            r2 = 0
        L_0x001e:
            r12 = 0
            r13 = 0
        L_0x0021:
            r15 = r17[r10]
            if (r0 >= r15) goto L_0x00c9
            if (r2 != 0) goto L_0x00c9
            int r15 = r0 + 1
            r0 = r17[r0]
            if (r0 >= r6) goto L_0x0034
            int r12 = r12 + 1
            long r13 = r13 * r3
            long r8 = (long) r0
            long r13 = r13 + r8
            goto L_0x003c
        L_0x0034:
            if (r0 == r5) goto L_0x003e
            switch(r0) {
                case 900: goto L_0x003e;
                case 901: goto L_0x003e;
                case 902: goto L_0x003e;
                default: goto L_0x0039;
            }
        L_0x0039:
            switch(r0) {
                case 922: goto L_0x003e;
                case 923: goto L_0x003e;
                case 924: goto L_0x003e;
                default: goto L_0x003c;
            }
        L_0x003c:
            r0 = r15
            goto L_0x0042
        L_0x003e:
            int r15 = r15 + -1
            r0 = r15
            r2 = 1
        L_0x0042:
            int r8 = r12 % 5
            if (r8 != 0) goto L_0x0021
            if (r12 <= 0) goto L_0x0021
            r8 = 0
        L_0x0049:
            if (r8 >= r7) goto L_0x001e
            int r9 = 5 - r8
            int r9 = r9 * 8
            long r11 = r13 >> r9
            int r9 = (int) r11
            byte r9 = (byte) r9
            r1.write(r9)
            int r8 = r8 + 1
            goto L_0x0049
        L_0x0059:
            int[] r0 = new int[r7]
            int r2 = r19 + 1
            r8 = r17[r19]
            r9 = r8
            r8 = 0
        L_0x0061:
            r11 = 0
            r12 = 0
        L_0x0064:
            r14 = r17[r10]
            if (r2 >= r14) goto L_0x00b2
            if (r8 != 0) goto L_0x00b2
            int r14 = r11 + 1
            r0[r11] = r9
            long r12 = r12 * r3
            long r3 = (long) r9
            long r12 = r12 + r3
            int r3 = r2 + 1
            r9 = r17[r2]
            if (r9 == r5) goto L_0x00aa
            switch(r9) {
                case 900: goto L_0x00aa;
                case 901: goto L_0x00aa;
                case 902: goto L_0x00aa;
                default: goto L_0x007b;
            }
        L_0x007b:
            switch(r9) {
                case 922: goto L_0x00aa;
                case 923: goto L_0x00aa;
                case 924: goto L_0x00aa;
                default: goto L_0x007e;
            }
        L_0x007e:
            int r2 = r14 % 5
            if (r2 != 0) goto L_0x00a0
            if (r14 <= 0) goto L_0x00a0
            r2 = 0
        L_0x0085:
            if (r2 >= r7) goto L_0x009a
            int r4 = 5 - r2
            int r4 = r4 * 8
            r16 = r8
            long r7 = r12 >> r4
            int r4 = (int) r7
            byte r4 = (byte) r4
            r1.write(r4)
            int r2 = r2 + 1
            r7 = 6
            r8 = r16
            goto L_0x0085
        L_0x009a:
            r16 = r8
            r2 = r3
            r3 = 900(0x384, double:4.447E-321)
            goto L_0x0061
        L_0x00a0:
            r16 = r8
            r8 = r16
            r2 = r3
            r11 = r14
            r3 = 900(0x384, double:4.447E-321)
            r7 = 6
            goto L_0x0064
        L_0x00aa:
            int r2 = r3 + -1
            r11 = r14
            r3 = 900(0x384, double:4.447E-321)
            r7 = 6
            r8 = 1
            goto L_0x0064
        L_0x00b2:
            r3 = r17[r10]
            if (r2 != r3) goto L_0x00bd
            if (r9 >= r6) goto L_0x00bd
            int r3 = r11 + 1
            r0[r11] = r9
            r11 = r3
        L_0x00bd:
            if (r10 >= r11) goto L_0x00c8
            r3 = r0[r10]
            byte r3 = (byte) r3
            r1.write(r3)
            int r10 = r10 + 1
            goto L_0x00bd
        L_0x00c8:
            r0 = r2
        L_0x00c9:
            java.lang.String r2 = new java.lang.String
            byte[] r1 = r1.toByteArray()
            r3 = r18
            r2.<init>(r1, r3)
            r1 = r20
            r1.append(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.pdf417.decoder.DecodedBitStreamParser.byteCompaction(int, int[], java.nio.charset.Charset, int, java.lang.StringBuilder):int");
    }

    private static int numericCompaction(int[] iArr, int i, StringBuilder sb) throws FormatException {
        int[] iArr2 = new int[15];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i3 == iArr[0]) {
                z = true;
            }
            if (i4 < TEXT_COMPACTION_MODE_LATCH) {
                iArr2[i2] = i4;
                i2++;
            } else {
                if (!(i4 == TEXT_COMPACTION_MODE_LATCH || i4 == BYTE_COMPACTION_MODE_LATCH || i4 == 928)) {
                    switch (i4) {
                    }
                }
                i3--;
                z = true;
            }
            if ((i2 % 15 == 0 || i4 == NUMERIC_COMPACTION_MODE_LATCH || z) && i2 > 0) {
                sb.append(decodeBase900toBase10(iArr2, i2));
                i2 = 0;
            }
            i = i3;
        }
        return i;
    }

    private static String decodeBase900toBase10(int[] iArr, int i) throws FormatException {
        BigInteger bigInteger = BigInteger.ZERO;
        for (int i2 = 0; i2 < i; i2++) {
            bigInteger = bigInteger.add(EXP900[(i - i2) - 1].multiply(BigInteger.valueOf((long) iArr[i2])));
        }
        String bigInteger2 = bigInteger.toString();
        if (bigInteger2.charAt(0) == '1') {
            return bigInteger2.substring(1);
        }
        throw FormatException.getFormatInstance();
    }
}
