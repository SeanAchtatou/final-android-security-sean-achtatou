package com.google.firebase.remoteconfig;

@Deprecated
/* compiled from: com.google.firebase:firebase-config@@19.0.4 */
public class FirebaseRemoteConfigFetchException extends FirebaseRemoteConfigException {
    public FirebaseRemoteConfigFetchException(String str) {
        super(str);
    }

    public FirebaseRemoteConfigFetchException(String str, Throwable th) {
        super(str, th);
    }
}
