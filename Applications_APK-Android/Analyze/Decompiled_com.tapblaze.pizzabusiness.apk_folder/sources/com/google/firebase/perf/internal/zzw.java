package com.google.firebase.perf.internal;

import com.google.android.gms.tasks.OnFailureListener;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final /* synthetic */ class zzw implements OnFailureListener {
    private final RemoteConfigManager zzfa;

    zzw(RemoteConfigManager remoteConfigManager) {
        this.zzfa = remoteConfigManager;
    }

    public final void onFailure(Exception exc) {
        this.zzfa.zza(exc);
    }
}
