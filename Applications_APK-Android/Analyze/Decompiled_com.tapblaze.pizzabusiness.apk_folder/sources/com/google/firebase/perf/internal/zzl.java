package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzcq;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzl extends zzq {
    private final zzcq zzdr;

    zzl(zzcq zzcq) {
        this.zzdr = zzcq;
    }

    public final boolean zzbr() {
        if (!this.zzdr.zzdw()) {
            return false;
        }
        if (this.zzdr.zzea() > 0 || this.zzdr.zzeb() > 0) {
            return true;
        }
        return this.zzdr.zzdy() && this.zzdr.zzdz().zzds();
    }
}
