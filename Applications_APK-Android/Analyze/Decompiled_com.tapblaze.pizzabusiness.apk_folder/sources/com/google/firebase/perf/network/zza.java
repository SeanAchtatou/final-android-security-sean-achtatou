package com.google.firebase.perf.network;

import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zza extends InputStream {
    private final zzbt zzfz;
    private final InputStream zzgn;
    private final zzbg zzgo;
    private long zzgp = -1;
    private long zzgq;
    private long zzgr = -1;

    public zza(InputStream inputStream, zzbg zzbg, zzbt zzbt) {
        this.zzfz = zzbt;
        this.zzgn = inputStream;
        this.zzgo = zzbg;
        this.zzgq = this.zzgo.zzbm();
    }

    public final int available() throws IOException {
        try {
            return this.zzgn.available();
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final void close() throws IOException {
        long zzda = this.zzfz.zzda();
        if (this.zzgr == -1) {
            this.zzgr = zzda;
        }
        try {
            this.zzgn.close();
            if (this.zzgp != -1) {
                this.zzgo.zzo(this.zzgp);
            }
            if (this.zzgq != -1) {
                this.zzgo.zzm(this.zzgq);
            }
            this.zzgo.zzn(this.zzgr);
            this.zzgo.zzbo();
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final void mark(int i) {
        this.zzgn.mark(i);
    }

    public final boolean markSupported() {
        return this.zzgn.markSupported();
    }

    public final int read() throws IOException {
        try {
            int read = this.zzgn.read();
            long zzda = this.zzfz.zzda();
            if (this.zzgq == -1) {
                this.zzgq = zzda;
            }
            if (read == -1 && this.zzgr == -1) {
                this.zzgr = zzda;
                this.zzgo.zzn(this.zzgr);
                this.zzgo.zzbo();
            } else {
                this.zzgp++;
                this.zzgo.zzo(this.zzgp);
            }
            return read;
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        try {
            int read = this.zzgn.read(bArr, i, i2);
            long zzda = this.zzfz.zzda();
            if (this.zzgq == -1) {
                this.zzgq = zzda;
            }
            if (read == -1 && this.zzgr == -1) {
                this.zzgr = zzda;
                this.zzgo.zzn(this.zzgr);
                this.zzgo.zzbo();
            } else {
                this.zzgp += (long) read;
                this.zzgo.zzo(this.zzgp);
            }
            return read;
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final int read(byte[] bArr) throws IOException {
        try {
            int read = this.zzgn.read(bArr);
            long zzda = this.zzfz.zzda();
            if (this.zzgq == -1) {
                this.zzgq = zzda;
            }
            if (read == -1 && this.zzgr == -1) {
                this.zzgr = zzda;
                this.zzgo.zzn(this.zzgr);
                this.zzgo.zzbo();
            } else {
                this.zzgp += (long) read;
                this.zzgo.zzo(this.zzgp);
            }
            return read;
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final void reset() throws IOException {
        try {
            this.zzgn.reset();
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final long skip(long j) throws IOException {
        try {
            long skip = this.zzgn.skip(j);
            long zzda = this.zzfz.zzda();
            if (this.zzgq == -1) {
                this.zzgq = zzda;
            }
            if (skip == -1 && this.zzgr == -1) {
                this.zzgr = zzda;
                this.zzgo.zzn(this.zzgr);
            } else {
                this.zzgp += skip;
                this.zzgo.zzo(this.zzgp);
            }
            return skip;
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }
}
