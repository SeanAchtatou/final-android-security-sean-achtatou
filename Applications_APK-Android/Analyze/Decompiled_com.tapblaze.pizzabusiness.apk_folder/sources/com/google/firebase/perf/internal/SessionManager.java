package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzcg;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class SessionManager extends zzb {
    private static final SessionManager zzff = new SessionManager();
    private final GaugeManager zzcl;
    private final zza zzdj;
    private final Set<WeakReference<zzx>> zzfg;
    private zzt zzfh;

    public static SessionManager zzck() {
        return zzff;
    }

    public final zzt zzcl() {
        return this.zzfh;
    }

    private SessionManager() {
        this(GaugeManager.zzbx(), zzt.zzcc(), zza.zzbf());
    }

    private SessionManager(GaugeManager gaugeManager, zzt zzt, zza zza) {
        this.zzfg = new HashSet();
        this.zzcl = gaugeManager;
        this.zzfh = zzt;
        this.zzdj = zza;
        zzbp();
    }

    public final void zzb(zzcg zzcg) {
        super.zzb(zzcg);
        if (!this.zzdj.zzbg()) {
            if (zzcg == zzcg.FOREGROUND) {
                zzc(zzcg);
            } else if (!zzcm()) {
                zzd(zzcg);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean zzcm() {
        if (!this.zzfh.isExpired()) {
            return false;
        }
        zzc(this.zzdj.zzbh());
        return true;
    }

    public final void zzc(zzcg zzcg) {
        this.zzfh = zzt.zzcc();
        synchronized (this.zzfg) {
            Iterator<WeakReference<zzx>> it = this.zzfg.iterator();
            while (it.hasNext()) {
                zzx zzx = (zzx) it.next().get();
                if (zzx != null) {
                    zzx.zza(this.zzfh);
                } else {
                    it.remove();
                }
            }
        }
        if (this.zzfh.zzcf()) {
            this.zzcl.zzb(this.zzfh.zzcd(), zzcg);
        }
        zzd(zzcg);
    }

    public final void zzc(WeakReference<zzx> weakReference) {
        synchronized (this.zzfg) {
            this.zzfg.add(weakReference);
        }
    }

    public final void zzd(WeakReference<zzx> weakReference) {
        synchronized (this.zzfg) {
            this.zzfg.remove(weakReference);
        }
    }

    private final void zzd(zzcg zzcg) {
        if (this.zzfh.zzcf()) {
            this.zzcl.zza(this.zzfh, zzcg);
        } else {
            this.zzcl.zzby();
        }
    }
}
