package com.google.firebase.perf.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbi;
import com.google.android.gms.internal.p000firebaseperf.zzbk;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.android.gms.internal.p000firebaseperf.zzde;
import com.google.android.gms.internal.p000firebaseperf.zzdi;
import com.google.android.gms.internal.p000firebaseperf.zzfc;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class zzt implements Parcelable {
    public static final Parcelable.Creator<zzt> CREATOR = new zzs();
    private String zzel;
    private boolean zzem;
    private zzbt zzen;

    public static zzt zzcc() {
        String replaceAll = UUID.randomUUID().toString().replaceAll("\\-", "");
        zzt zzt = new zzt(replaceAll, new zzbk());
        zzaf zzl = zzaf.zzl();
        zzt.zzem = zzl.zzm() && Math.random() < ((double) zzl.zzs());
        zzbi zzco = zzbi.zzco();
        Object[] objArr = new Object[2];
        objArr[0] = zzt.zzem ? "Verbose" : "Non Verbose";
        objArr[1] = replaceAll;
        zzco.zzm(String.format("Creating a new %s Session: %s", objArr));
        return zzt;
    }

    public int describeContents() {
        return 0;
    }

    private zzt(String str, zzbk zzbk) {
        this.zzem = false;
        this.zzel = str;
        this.zzen = new zzbt();
    }

    private zzt(Parcel parcel) {
        boolean z = false;
        this.zzem = false;
        this.zzel = parcel.readString();
        this.zzem = parcel.readByte() != 0 ? true : z;
        this.zzen = (zzbt) parcel.readParcelable(zzbt.class.getClassLoader());
    }

    public final String zzcd() {
        return this.zzel;
    }

    public final zzbt zzce() {
        return this.zzen;
    }

    public final boolean zzcf() {
        return this.zzem;
    }

    public final boolean isExpired() {
        return TimeUnit.MICROSECONDS.toMinutes(this.zzen.zzda()) > zzaf.zzl().zzx();
    }

    public final zzde zzcg() {
        zzde.zza zzag = zzde.zzfo().zzag(this.zzel);
        if (this.zzem) {
            zzag.zzb(zzdi.GAUGES_AND_SYSTEM_EVENTS);
        }
        return (zzde) ((zzfc) zzag.zzhp());
    }

    public static zzde[] zza(List<zzt> list) {
        if (list.isEmpty()) {
            return null;
        }
        zzde[] zzdeArr = new zzde[list.size()];
        zzde zzcg = list.get(0).zzcg();
        boolean z = false;
        for (int i = 1; i < list.size(); i++) {
            zzde zzcg2 = list.get(i).zzcg();
            if (z || !list.get(i).zzem) {
                zzdeArr[i] = zzcg2;
            } else {
                zzdeArr[0] = zzcg2;
                zzdeArr[i] = zzcg;
                z = true;
            }
        }
        if (!z) {
            zzdeArr[0] = zzcg;
        }
        return zzdeArr;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.zzel);
        parcel.writeByte(this.zzem ? (byte) 1 : 0);
        parcel.writeParcelable(this.zzen, 0);
    }

    /* synthetic */ zzt(Parcel parcel, zzs zzs) {
        this(parcel);
    }
}
