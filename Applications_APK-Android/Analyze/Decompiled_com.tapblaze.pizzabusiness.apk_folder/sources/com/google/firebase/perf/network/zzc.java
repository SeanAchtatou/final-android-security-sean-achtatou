package com.google.firebase.perf.network;

import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import java.io.IOException;
import java.io.OutputStream;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzc extends OutputStream {
    private final zzbt zzfz;
    private zzbg zzgo;
    private OutputStream zzgt;
    private long zzgu = -1;

    public zzc(OutputStream outputStream, zzbg zzbg, zzbt zzbt) {
        this.zzgt = outputStream;
        this.zzgo = zzbg;
        this.zzfz = zzbt;
    }

    public final void close() throws IOException {
        long j = this.zzgu;
        if (j != -1) {
            this.zzgo.zzj(j);
        }
        this.zzgo.zzl(this.zzfz.zzda());
        try {
            this.zzgt.close();
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final void flush() throws IOException {
        try {
            this.zzgt.flush();
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final void write(int i) throws IOException {
        try {
            this.zzgt.write(i);
            this.zzgu++;
            this.zzgo.zzj(this.zzgu);
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final void write(byte[] bArr) throws IOException {
        try {
            this.zzgt.write(bArr);
            this.zzgu += (long) bArr.length;
            this.zzgo.zzj(this.zzgu);
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final void write(byte[] bArr, int i, int i2) throws IOException {
        try {
            this.zzgt.write(bArr, i, i2);
            this.zzgu += (long) i2;
            this.zzgo.zzj(this.zzgu);
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }
}
