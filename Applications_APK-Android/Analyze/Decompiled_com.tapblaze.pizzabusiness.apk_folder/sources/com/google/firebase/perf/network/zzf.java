package com.google.firebase.perf.network;

import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzf implements Callback {
    private final zzbt zzfz;
    private final zzbg zzgo;
    private final Callback zzgy;
    private final long zzgz;

    public zzf(Callback callback, com.google.firebase.perf.internal.zzf zzf, zzbt zzbt, long j) {
        this.zzgy = callback;
        this.zzgo = zzbg.zzb(zzf);
        this.zzgz = j;
        this.zzfz = zzbt;
    }

    public final void onFailure(Call call, IOException iOException) {
        Request request = call.request();
        if (request != null) {
            HttpUrl url = request.url();
            if (url != null) {
                this.zzgo.zzf(url.url().toString());
            }
            if (request.method() != null) {
                this.zzgo.zzg(request.method());
            }
        }
        this.zzgo.zzk(this.zzgz);
        this.zzgo.zzn(this.zzfz.zzda());
        zzh.zza(this.zzgo);
        this.zzgy.onFailure(call, iOException);
    }

    public final void onResponse(Call call, Response response) throws IOException {
        Response response2 = response;
        FirebasePerfOkHttpClient.zza(response2, this.zzgo, this.zzgz, this.zzfz.zzda());
        this.zzgy.onResponse(call, response);
    }
}
