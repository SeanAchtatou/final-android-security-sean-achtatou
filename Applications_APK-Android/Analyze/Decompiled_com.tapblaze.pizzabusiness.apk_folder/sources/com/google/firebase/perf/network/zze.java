package com.google.firebase.perf.network;

import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ProtocolException;
import java.net.URL;
import java.security.Permission;
import java.security.Principal;
import java.security.cert.Certificate;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zze extends HttpsURLConnection {
    private final zzd zzgs;
    private final HttpsURLConnection zzgx;

    zze(HttpsURLConnection httpsURLConnection, zzbt zzbt, zzbg zzbg) {
        super(httpsURLConnection.getURL());
        this.zzgx = httpsURLConnection;
        this.zzgs = new zzd(httpsURLConnection, zzbt, zzbg);
    }

    public final void connect() throws IOException {
        this.zzgs.connect();
    }

    public final void disconnect() {
        this.zzgs.disconnect();
    }

    public final Object getContent() throws IOException {
        return this.zzgs.getContent();
    }

    public final Object getContent(Class[] clsArr) throws IOException {
        return this.zzgs.getContent(clsArr);
    }

    public final InputStream getInputStream() throws IOException {
        return this.zzgs.getInputStream();
    }

    public final long getLastModified() {
        return this.zzgs.getLastModified();
    }

    public final OutputStream getOutputStream() throws IOException {
        return this.zzgs.getOutputStream();
    }

    public final Permission getPermission() throws IOException {
        return this.zzgs.getPermission();
    }

    public final int getResponseCode() throws IOException {
        return this.zzgs.getResponseCode();
    }

    public final String getResponseMessage() throws IOException {
        return this.zzgs.getResponseMessage();
    }

    public final long getExpiration() {
        return this.zzgs.getExpiration();
    }

    public final String getHeaderField(int i) {
        return this.zzgs.getHeaderField(i);
    }

    public final String getHeaderField(String str) {
        return this.zzgs.getHeaderField(str);
    }

    public final long getHeaderFieldDate(String str, long j) {
        return this.zzgs.getHeaderFieldDate(str, j);
    }

    public final int getHeaderFieldInt(String str, int i) {
        return this.zzgs.getHeaderFieldInt(str, i);
    }

    public final long getHeaderFieldLong(String str, long j) {
        return this.zzgs.getHeaderFieldLong(str, j);
    }

    public final String getHeaderFieldKey(int i) {
        return this.zzgs.getHeaderFieldKey(i);
    }

    public final Map<String, List<String>> getHeaderFields() {
        return this.zzgs.getHeaderFields();
    }

    public final String getContentEncoding() {
        return this.zzgs.getContentEncoding();
    }

    public final int getContentLength() {
        return this.zzgs.getContentLength();
    }

    public final long getContentLengthLong() {
        return this.zzgs.getContentLengthLong();
    }

    public final String getContentType() {
        return this.zzgs.getContentType();
    }

    public final long getDate() {
        return this.zzgs.getDate();
    }

    public final void addRequestProperty(String str, String str2) {
        this.zzgs.addRequestProperty(str, str2);
    }

    public final boolean equals(Object obj) {
        return this.zzgs.equals(obj);
    }

    public final boolean getAllowUserInteraction() {
        return this.zzgs.getAllowUserInteraction();
    }

    public final int getConnectTimeout() {
        return this.zzgs.getConnectTimeout();
    }

    public final boolean getDefaultUseCaches() {
        return this.zzgs.getDefaultUseCaches();
    }

    public final boolean getDoInput() {
        return this.zzgs.getDoInput();
    }

    public final boolean getDoOutput() {
        return this.zzgs.getDoOutput();
    }

    public final InputStream getErrorStream() {
        return this.zzgs.getErrorStream();
    }

    public final long getIfModifiedSince() {
        return this.zzgs.getIfModifiedSince();
    }

    public final boolean getInstanceFollowRedirects() {
        return this.zzgs.getInstanceFollowRedirects();
    }

    public final int getReadTimeout() {
        return this.zzgs.getReadTimeout();
    }

    public final String getRequestMethod() {
        return this.zzgs.getRequestMethod();
    }

    public final Map<String, List<String>> getRequestProperties() {
        return this.zzgs.getRequestProperties();
    }

    public final String getRequestProperty(String str) {
        return this.zzgs.getRequestProperty(str);
    }

    public final URL getURL() {
        return this.zzgs.getURL();
    }

    public final boolean getUseCaches() {
        return this.zzgs.getUseCaches();
    }

    public final int hashCode() {
        return this.zzgs.hashCode();
    }

    public final void setAllowUserInteraction(boolean z) {
        this.zzgs.setAllowUserInteraction(z);
    }

    public final void setChunkedStreamingMode(int i) {
        this.zzgs.setChunkedStreamingMode(i);
    }

    public final void setConnectTimeout(int i) {
        this.zzgs.setConnectTimeout(i);
    }

    public final void setDefaultUseCaches(boolean z) {
        this.zzgs.setDefaultUseCaches(z);
    }

    public final void setDoInput(boolean z) {
        this.zzgs.setDoInput(z);
    }

    public final void setDoOutput(boolean z) {
        this.zzgs.setDoOutput(z);
    }

    public final void setFixedLengthStreamingMode(int i) {
        this.zzgs.setFixedLengthStreamingMode(i);
    }

    public final void setFixedLengthStreamingMode(long j) {
        this.zzgs.setFixedLengthStreamingMode(j);
    }

    public final void setIfModifiedSince(long j) {
        this.zzgs.setIfModifiedSince(j);
    }

    public final void setInstanceFollowRedirects(boolean z) {
        this.zzgs.setInstanceFollowRedirects(z);
    }

    public final void setReadTimeout(int i) {
        this.zzgs.setReadTimeout(i);
    }

    public final void setRequestMethod(String str) throws ProtocolException {
        this.zzgs.setRequestMethod(str);
    }

    public final void setRequestProperty(String str, String str2) {
        this.zzgs.setRequestProperty(str, str2);
    }

    public final void setUseCaches(boolean z) {
        this.zzgs.setUseCaches(z);
    }

    public final String toString() {
        return this.zzgs.toString();
    }

    public final boolean usingProxy() {
        return this.zzgs.usingProxy();
    }

    public final String getCipherSuite() {
        return this.zzgx.getCipherSuite();
    }

    public final HostnameVerifier getHostnameVerifier() {
        return this.zzgx.getHostnameVerifier();
    }

    public final Certificate[] getLocalCertificates() {
        return this.zzgx.getLocalCertificates();
    }

    public final Principal getLocalPrincipal() {
        return this.zzgx.getLocalPrincipal();
    }

    public final Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
        return this.zzgx.getPeerPrincipal();
    }

    public final Certificate[] getServerCertificates() throws SSLPeerUnverifiedException {
        return this.zzgx.getServerCertificates();
    }

    public final SSLSocketFactory getSSLSocketFactory() {
        return this.zzgx.getSSLSocketFactory();
    }

    public final void setHostnameVerifier(HostnameVerifier hostnameVerifier) {
        this.zzgx.setHostnameVerifier(hostnameVerifier);
    }

    public final void setSSLSocketFactory(SSLSocketFactory sSLSocketFactory) {
        this.zzgx.setSSLSocketFactory(sSLSocketFactory);
    }
}
