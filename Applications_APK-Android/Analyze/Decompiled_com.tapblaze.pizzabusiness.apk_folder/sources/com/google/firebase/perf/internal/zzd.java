package com.google.firebase.perf.internal;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public interface zzd {
    public static final String VERSION_NAME = "19.0.5";
    public static final String zzcw = "FIREPERF";
    public static final boolean zzcx;

    static {
        Boolean bool = false;
        zzcx = bool.booleanValue();
    }
}
