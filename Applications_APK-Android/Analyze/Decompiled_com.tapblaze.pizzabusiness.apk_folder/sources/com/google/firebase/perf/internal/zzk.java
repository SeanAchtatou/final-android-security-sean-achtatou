package com.google.firebase.perf.internal;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.internal.p000firebaseperf.zzbv;
import com.google.android.gms.internal.p000firebaseperf.zzcv;
import cz.msebera.android.httpclient.HttpHost;
import java.net.URI;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzk extends zzq {
    private final Context zzde;
    private final zzcv zzdq;

    zzk(zzcv zzcv, Context context) {
        this.zzde = context;
        this.zzdq = zzcv;
    }

    private static boolean zzp(long j) {
        return j >= 0;
    }

    private static boolean zzq(long j) {
        return j >= 0;
    }

    public final boolean zzbr() {
        boolean z;
        if (zzj(this.zzdq.getUrl())) {
            String valueOf = String.valueOf(this.zzdq.getUrl());
            Log.i("FirebasePerformance", valueOf.length() != 0 ? "URL is missing:".concat(valueOf) : new String("URL is missing:"));
            return false;
        }
        URI zzi = zzi(this.zzdq.getUrl());
        if (zzi == null) {
            Log.i("FirebasePerformance", "URL cannot be parsed");
            return false;
        }
        Context context = this.zzde;
        if (zzi == null) {
            z = false;
        } else {
            z = zzbv.zza(zzi, context);
        }
        if (!z) {
            String valueOf2 = String.valueOf(zzi);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf2).length() + 26);
            sb.append("URL fails whitelist rule: ");
            sb.append(valueOf2);
            Log.i("FirebasePerformance", sb.toString());
            return false;
        }
        String host = zzi.getHost();
        if (!(host != null && !zzj(host) && host.length() <= 255)) {
            Log.i("FirebasePerformance", "URL host is null or invalid");
            return false;
        }
        String scheme = zzi.getScheme();
        if (!(scheme != null && (HttpHost.DEFAULT_SCHEME_NAME.equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)))) {
            Log.i("FirebasePerformance", "URL scheme is null or invalid");
            return false;
        }
        if (!(zzi.getUserInfo() == null)) {
            Log.i("FirebasePerformance", "URL user info is null");
            return false;
        }
        int port = zzi.getPort();
        if (!(port == -1 || port > 0)) {
            Log.i("FirebasePerformance", "URL port is less than or equal to 0");
            return false;
        }
        zzcv.zzb zzei = this.zzdq.zzeh() ? this.zzdq.zzei() : null;
        if (!((zzei == null || zzei == zzcv.zzb.HTTP_METHOD_UNKNOWN) ? false : true)) {
            String valueOf3 = String.valueOf(this.zzdq.zzei());
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf3).length() + 32);
            sb2.append("HTTP Method is null or invalid: ");
            sb2.append(valueOf3);
            Log.i("FirebasePerformance", sb2.toString());
            return false;
        }
        if (this.zzdq.zzbl()) {
            if (!(this.zzdq.zzen() > 0)) {
                int zzen = this.zzdq.zzen();
                StringBuilder sb3 = new StringBuilder(49);
                sb3.append("HTTP ResponseCode is a negative value:");
                sb3.append(zzen);
                Log.i("FirebasePerformance", sb3.toString());
                return false;
            }
        }
        if (this.zzdq.zzej() && !zzq(this.zzdq.zzek())) {
            long zzek = this.zzdq.zzek();
            StringBuilder sb4 = new StringBuilder(56);
            sb4.append("Request Payload is a negative value:");
            sb4.append(zzek);
            Log.i("FirebasePerformance", sb4.toString());
            return false;
        } else if (this.zzdq.zzel() && !zzq(this.zzdq.zzem())) {
            long zzem = this.zzdq.zzem();
            StringBuilder sb5 = new StringBuilder(57);
            sb5.append("Response Payload is a negative value:");
            sb5.append(zzem);
            Log.i("FirebasePerformance", sb5.toString());
            return false;
        } else if (!this.zzdq.zzep() || this.zzdq.zzeq() <= 0) {
            long zzeq = this.zzdq.zzeq();
            StringBuilder sb6 = new StringBuilder(84);
            sb6.append("Start time of the request is null, or zero, or a negative value:");
            sb6.append(zzeq);
            Log.i("FirebasePerformance", sb6.toString());
            return false;
        } else if (this.zzdq.zzer() && !zzp(this.zzdq.zzes())) {
            long zzes = this.zzdq.zzes();
            StringBuilder sb7 = new StringBuilder(69);
            sb7.append("Time to complete the request is a negative value:");
            sb7.append(zzes);
            Log.i("FirebasePerformance", sb7.toString());
            return false;
        } else if (this.zzdq.zzet() && !zzp(this.zzdq.zzeu())) {
            long zzeu = this.zzdq.zzeu();
            StringBuilder sb8 = new StringBuilder(112);
            sb8.append("Time from the start of the request to the start of the response is null or a negative value:");
            sb8.append(zzeu);
            Log.i("FirebasePerformance", sb8.toString());
            return false;
        } else if (!this.zzdq.zzev() || this.zzdq.zzew() <= 0) {
            long zzew = this.zzdq.zzew();
            StringBuilder sb9 = new StringBuilder(108);
            sb9.append("Time from the start of the request to the end of the response is null, negative or zero:");
            sb9.append(zzew);
            Log.i("FirebasePerformance", sb9.toString());
            return false;
        } else if (this.zzdq.zzbl()) {
            return true;
        } else {
            Log.i("FirebasePerformance", "Did not receive a HTTP Response Code");
            return false;
        }
    }

    private static URI zzi(String str) {
        if (str == null) {
            return null;
        }
        try {
            return URI.create(str);
        } catch (IllegalArgumentException | IllegalStateException e) {
            Log.w("FirebasePerformance", "getResultUrl throws exception", e);
            return null;
        }
    }

    private static boolean zzj(String str) {
        if (str == null) {
            return true;
        }
        return str.trim().isEmpty();
    }
}
