package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzcg;
import com.google.android.gms.internal.p000firebaseperf.zzcq;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzj implements Runnable {
    private final /* synthetic */ zzf zzcy;
    private final /* synthetic */ zzcg zzdm;
    private final /* synthetic */ zzcq zzdp;

    zzj(zzf zzf, zzcq zzcq, zzcg zzcg) {
        this.zzcy = zzf;
        this.zzdp = zzcq;
        this.zzdm = zzcg;
    }

    public final void run() {
        this.zzcy.zzb(this.zzdp, this.zzdm);
    }
}
