package com.google.firebase.perf.internal;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzs implements Parcelable.Creator<zzt> {
    zzs() {
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzt[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new zzt(parcel, (zzs) null);
    }
}
