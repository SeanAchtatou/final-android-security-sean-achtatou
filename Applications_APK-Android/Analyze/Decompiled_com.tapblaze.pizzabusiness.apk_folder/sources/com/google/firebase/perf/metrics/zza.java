package com.google.firebase.perf.metrics;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class zza implements Parcelable {
    public static final Parcelable.Creator<zza> CREATOR = new zzb();
    private final String mName;
    private AtomicLong zzfw;

    public zza(String str) {
        this.mName = str;
        this.zzfw = new AtomicLong(0);
    }

    public int describeContents() {
        return 0;
    }

    private zza(Parcel parcel) {
        this.mName = parcel.readString();
        this.zzfw = new AtomicLong(parcel.readLong());
    }

    public final void zzr(long j) {
        this.zzfw.addAndGet(j);
    }

    /* access modifiers changed from: package-private */
    public final String getName() {
        return this.mName;
    }

    /* access modifiers changed from: package-private */
    public final long getCount() {
        return this.zzfw.get();
    }

    /* access modifiers changed from: package-private */
    public final void zzs(long j) {
        this.zzfw.set(j);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mName);
        parcel.writeLong(this.zzfw.get());
    }

    /* synthetic */ zza(Parcel parcel, zzb zzb) {
        this(parcel);
    }
}
