package com.google.firebase.perf.network;

import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.android.gms.internal.p000firebaseperf.zzby;
import com.google.firebase.perf.internal.zzf;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import javax.net.ssl.HttpsURLConnection;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class FirebasePerfUrlConnection {
    private FirebasePerfUrlConnection() {
    }

    public static InputStream openStream(URL url) throws IOException {
        return zza(new zzby(url), zzf.zzbs(), new zzbt());
    }

    private static InputStream zza(zzby zzby, zzf zzf, zzbt zzbt) throws IOException {
        zzbt.reset();
        long zzcz = zzbt.zzcz();
        zzbg zzb = zzbg.zzb(zzf);
        try {
            URLConnection openConnection = zzby.openConnection();
            if (openConnection instanceof HttpsURLConnection) {
                return new zze((HttpsURLConnection) openConnection, zzbt, zzb).getInputStream();
            }
            if (openConnection instanceof HttpURLConnection) {
                return new zzb((HttpURLConnection) openConnection, zzbt, zzb).getInputStream();
            }
            return openConnection.getInputStream();
        } catch (IOException e) {
            zzb.zzk(zzcz);
            zzb.zzn(zzbt.zzda());
            zzb.zzf(zzby.toString());
            zzh.zza(zzb);
            throw e;
        }
    }

    public static Object getContent(URL url) throws IOException {
        return zzb(new zzby(url), zzf.zzbs(), new zzbt());
    }

    public static Object getContent(URL url, Class[] clsArr) throws IOException {
        return zza(new zzby(url), clsArr, zzf.zzbs(), new zzbt());
    }

    private static Object zzb(zzby zzby, zzf zzf, zzbt zzbt) throws IOException {
        zzbt.reset();
        long zzcz = zzbt.zzcz();
        zzbg zzb = zzbg.zzb(zzf);
        try {
            URLConnection openConnection = zzby.openConnection();
            if (openConnection instanceof HttpsURLConnection) {
                return new zze((HttpsURLConnection) openConnection, zzbt, zzb).getContent();
            }
            if (openConnection instanceof HttpURLConnection) {
                return new zzb((HttpURLConnection) openConnection, zzbt, zzb).getContent();
            }
            return openConnection.getContent();
        } catch (IOException e) {
            zzb.zzk(zzcz);
            zzb.zzn(zzbt.zzda());
            zzb.zzf(zzby.toString());
            zzh.zza(zzb);
            throw e;
        }
    }

    private static Object zza(zzby zzby, Class[] clsArr, zzf zzf, zzbt zzbt) throws IOException {
        zzbt.reset();
        long zzcz = zzbt.zzcz();
        zzbg zzb = zzbg.zzb(zzf);
        try {
            URLConnection openConnection = zzby.openConnection();
            if (openConnection instanceof HttpsURLConnection) {
                return new zze((HttpsURLConnection) openConnection, zzbt, zzb).getContent(clsArr);
            }
            if (openConnection instanceof HttpURLConnection) {
                return new zzb((HttpURLConnection) openConnection, zzbt, zzb).getContent(clsArr);
            }
            return openConnection.getContent(clsArr);
        } catch (IOException e) {
            zzb.zzk(zzcz);
            zzb.zzn(zzbt.zzda());
            zzb.zzf(zzby.toString());
            zzh.zza(zzb);
            throw e;
        }
    }

    public static Object instrument(Object obj) throws IOException {
        if (obj instanceof HttpsURLConnection) {
            return new zze((HttpsURLConnection) obj, new zzbt(), zzbg.zzb(zzf.zzbs()));
        }
        return obj instanceof HttpURLConnection ? new zzb((HttpURLConnection) obj, new zzbt(), zzbg.zzb(zzf.zzbs())) : obj;
    }
}
