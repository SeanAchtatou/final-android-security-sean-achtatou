package com.google.firebase.perf.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.p000firebaseperf.zza;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbk;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.android.gms.internal.p000firebaseperf.zzcg;
import com.google.firebase.perf.internal.SessionManager;
import com.google.firebase.perf.metrics.AppStartTrace;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class FirebasePerfProvider extends ContentProvider {
    private static final zzbt zzhb = new zzbt();
    private final Handler mHandler = new zza(Looper.getMainLooper());

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public boolean onCreate() {
        return false;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    public static zzbt zzcx() {
        return zzhb;
    }

    public void attachInfo(Context context, ProviderInfo providerInfo) {
        Preconditions.checkNotNull(providerInfo, "FirebasePerfProvider ProviderInfo cannot be null.");
        if (!"com.google.firebase.firebaseperfprovider".equals(providerInfo.authority)) {
            super.attachInfo(context, providerInfo);
            zzaf zzl = zzaf.zzl();
            zzl.zzb(getContext());
            if (zzl.zzm()) {
                com.google.firebase.perf.internal.zza.zzbf().zze(getContext());
                AppStartTrace zzcp = AppStartTrace.zzcp();
                zzcp.zze(getContext());
                this.mHandler.post(new AppStartTrace.zza(zzcp));
            }
            SessionManager.zzck().zzc(zzcg.FOREGROUND);
            return;
        }
        throw new IllegalStateException("Incorrect provider authority in manifest. Most likely due to a missing applicationId variable in application's build.gradle.");
    }

    static {
        new zzbk();
    }
}
