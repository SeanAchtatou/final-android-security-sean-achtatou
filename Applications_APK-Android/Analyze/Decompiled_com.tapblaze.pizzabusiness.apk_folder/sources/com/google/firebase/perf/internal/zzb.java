package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzcg;
import com.google.firebase.perf.internal.zza;
import java.lang.ref.WeakReference;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class zzb implements zza.C0041zza {
    private zza zzcr;
    private zzcg zzcs;
    private boolean zzct;
    private WeakReference<zza.C0041zza> zzcu;

    protected zzb() {
        this(zza.zzbf());
    }

    protected zzb(zza zza) {
        this.zzcs = zzcg.APPLICATION_PROCESS_STATE_UNKNOWN;
        this.zzct = false;
        this.zzcr = zza;
        this.zzcu = new WeakReference<>(this);
    }

    /* access modifiers changed from: protected */
    public final void zzbp() {
        if (!this.zzct) {
            this.zzcs = this.zzcr.zzbh();
            this.zzcr.zza(this.zzcu);
            this.zzct = true;
        }
    }

    /* access modifiers changed from: protected */
    public final void zzbq() {
        if (this.zzct) {
            this.zzcr.zzb(this.zzcu);
            this.zzct = false;
        }
    }

    /* access modifiers changed from: protected */
    public final void zzb(int i) {
        this.zzcr.zzb(1);
    }

    public void zzb(zzcg zzcg) {
        if (this.zzcs == zzcg.APPLICATION_PROCESS_STATE_UNKNOWN) {
            this.zzcs = zzcg;
        } else if (this.zzcs != zzcg && zzcg != zzcg.APPLICATION_PROCESS_STATE_UNKNOWN) {
            this.zzcs = zzcg.FOREGROUND_BACKGROUND;
        }
    }

    public final zzcg zzbh() {
        return this.zzcs;
    }
}
