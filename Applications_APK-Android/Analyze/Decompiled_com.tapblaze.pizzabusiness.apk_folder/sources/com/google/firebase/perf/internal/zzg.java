package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzcg;
import com.google.android.gms.internal.p000firebaseperf.zzcv;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzg implements Runnable {
    private final /* synthetic */ zzf zzcy;
    private final /* synthetic */ zzcv zzdl;
    private final /* synthetic */ zzcg zzdm;

    zzg(zzf zzf, zzcv zzcv, zzcg zzcg) {
        this.zzcy = zzf;
        this.zzdl = zzcv;
        this.zzdm = zzcg;
    }

    public final void run() {
        this.zzcy.zzb(this.zzdl, this.zzdm);
    }
}
