package com.google.firebase.perf.internal;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseIntArray;
import androidx.core.app.FrameMetricsAggregator;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbj;
import com.google.android.gms.internal.p000firebaseperf.zzbk;
import com.google.android.gms.internal.p000firebaseperf.zzbm;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.android.gms.internal.p000firebaseperf.zzbx;
import com.google.android.gms.internal.p000firebaseperf.zzcg;
import com.google.android.gms.internal.p000firebaseperf.zzdn;
import com.google.android.gms.internal.p000firebaseperf.zzfc;
import com.google.firebase.perf.metrics.Trace;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class zza implements Application.ActivityLifecycleCallbacks {
    private static volatile zza zzbv;
    private boolean mRegistered = false;
    private zzf zzbw = null;
    private zzaf zzbx;
    private final zzbk zzby;
    private boolean zzbz = true;
    private final WeakHashMap<Activity, Boolean> zzca = new WeakHashMap<>();
    private zzbt zzcb;
    private zzbt zzcc;
    private final Map<String, Long> zzcd = new HashMap();
    private AtomicInteger zzce = new AtomicInteger(0);
    private zzcg zzcf = zzcg.BACKGROUND;
    private Set<WeakReference<C0041zza>> zzcg = new HashSet();
    private boolean zzch = false;
    private FrameMetricsAggregator zzci;
    private final WeakHashMap<Activity, Trace> zzcj = new WeakHashMap<>();

    /* renamed from: com.google.firebase.perf.internal.zza$zza  reason: collision with other inner class name */
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public interface C0041zza {
        void zzb(zzcg zzcg);
    }

    public static zza zzbf() {
        if (zzbv != null) {
            return zzbv;
        }
        return zza((zzf) null);
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    private static zza zza(zzf zzf) {
        if (zzbv == null) {
            synchronized (zza.class) {
                if (zzbv == null) {
                    zzbv = new zza(null, new zzbk());
                }
            }
        }
        return zzbv;
    }

    private zza(zzf zzf, zzbk zzbk) {
        this.zzby = zzbk;
        this.zzbx = zzaf.zzl();
        this.zzch = zzbj();
        if (this.zzch) {
            this.zzci = new FrameMetricsAggregator();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zze(android.content.Context r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r0 = r1.mRegistered     // Catch:{ all -> 0x0019 }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            android.content.Context r2 = r2.getApplicationContext()     // Catch:{ all -> 0x0019 }
            boolean r0 = r2 instanceof android.app.Application     // Catch:{ all -> 0x0019 }
            if (r0 == 0) goto L_0x0017
            android.app.Application r2 = (android.app.Application) r2     // Catch:{ all -> 0x0019 }
            r2.registerActivityLifecycleCallbacks(r1)     // Catch:{ all -> 0x0019 }
            r2 = 1
            r1.mRegistered = r2     // Catch:{ all -> 0x0019 }
        L_0x0017:
            monitor-exit(r1)
            return
        L_0x0019:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.perf.internal.zza.zze(android.content.Context):void");
    }

    public final void zzb(String str, long j) {
        synchronized (this.zzcd) {
            Long l = this.zzcd.get(str);
            if (l == null) {
                this.zzcd.put(str, 1L);
            } else {
                this.zzcd.put(str, Long.valueOf(l.longValue() + 1));
            }
        }
    }

    public final void zzb(int i) {
        this.zzce.addAndGet(1);
    }

    public synchronized void onActivityStarted(Activity activity) {
        if (zza(activity) && this.zzbx.zzm()) {
            this.zzci.add(activity);
            zzbi();
            Trace trace = new Trace(zzb(activity), this.zzbw, this.zzby, this);
            trace.start();
            this.zzcj.put(activity, trace);
        }
    }

    public synchronized void onActivityStopped(Activity activity) {
        Trace trace;
        int i;
        int i2;
        int i3;
        SparseIntArray sparseIntArray;
        if (zza(activity) && this.zzcj.containsKey(activity) && (trace = this.zzcj.get(activity)) != null) {
            this.zzcj.remove(activity);
            SparseIntArray[] remove = this.zzci.remove(activity);
            if (remove == null || (sparseIntArray = remove[0]) == null) {
                i3 = 0;
                i2 = 0;
                i = 0;
            } else {
                i3 = 0;
                i2 = 0;
                i = 0;
                for (int i4 = 0; i4 < sparseIntArray.size(); i4++) {
                    int keyAt = sparseIntArray.keyAt(i4);
                    int valueAt = sparseIntArray.valueAt(i4);
                    i3 += valueAt;
                    if (keyAt > 700) {
                        i += valueAt;
                    }
                    if (keyAt > 16) {
                        i2 += valueAt;
                    }
                }
            }
            if (i3 > 0) {
                trace.putMetric(zzbj.FRAMES_TOTAL.toString(), (long) i3);
            }
            if (i2 > 0) {
                trace.putMetric(zzbj.FRAMES_SLOW.toString(), (long) i2);
            }
            if (i > 0) {
                trace.putMetric(zzbj.FRAMES_FROZEN.toString(), (long) i);
            }
            if (zzbx.zzg(activity.getApplicationContext())) {
                String zzb = zzb(activity);
                StringBuilder sb = new StringBuilder(String.valueOf(zzb).length() + 81);
                sb.append("sendScreenTrace name:");
                sb.append(zzb);
                sb.append(" _fr_tot:");
                sb.append(i3);
                sb.append(" _fr_slo:");
                sb.append(i2);
                sb.append(" _fr_fzn:");
                sb.append(i);
                Log.d("FirebasePerformance", sb.toString());
            }
            trace.stop();
        }
        if (this.zzca.containsKey(activity)) {
            this.zzca.remove(activity);
            if (this.zzca.isEmpty()) {
                this.zzcb = new zzbt();
                zza(zzcg.BACKGROUND);
                zzb(false);
                zza(zzbm.FOREGROUND_TRACE_NAME.toString(), this.zzcc, this.zzcb);
            }
        }
    }

    public synchronized void onActivityResumed(Activity activity) {
        if (this.zzca.isEmpty()) {
            this.zzcc = new zzbt();
            this.zzca.put(activity, true);
            zza(zzcg.FOREGROUND);
            zzb(true);
            if (this.zzbz) {
                this.zzbz = false;
            } else {
                zza(zzbm.BACKGROUND_TRACE_NAME.toString(), this.zzcb, this.zzcc);
            }
        } else {
            this.zzca.put(activity, true);
        }
    }

    public final boolean zzbg() {
        return this.zzbz;
    }

    public final zzcg zzbh() {
        return this.zzcf;
    }

    public final void zza(WeakReference<C0041zza> weakReference) {
        synchronized (this.zzcg) {
            this.zzcg.add(weakReference);
        }
    }

    public final void zzb(WeakReference<C0041zza> weakReference) {
        synchronized (this.zzcg) {
            this.zzcg.remove(weakReference);
        }
    }

    private final void zza(zzcg zzcg2) {
        this.zzcf = zzcg2;
        synchronized (this.zzcg) {
            Iterator<WeakReference<C0041zza>> it = this.zzcg.iterator();
            while (it.hasNext()) {
                C0041zza zza = (C0041zza) it.next().get();
                if (zza != null) {
                    zza.zzb(this.zzcf);
                } else {
                    it.remove();
                }
            }
        }
    }

    private final void zza(String str, zzbt zzbt, zzbt zzbt2) {
        if (this.zzbx.zzm()) {
            zzbi();
            zzdn.zzb zzb = zzdn.zzfx().zzah(str).zzao(zzbt.zzcz()).zzap(zzbt.zzk(zzbt2)).zzb(SessionManager.zzck().zzcl().zzcg());
            int andSet = this.zzce.getAndSet(0);
            synchronized (this.zzcd) {
                zzb.zzd(this.zzcd);
                if (andSet != 0) {
                    zzb.zzc(zzbj.TRACE_STARTED_NOT_STOPPED.toString(), (long) andSet);
                }
                this.zzcd.clear();
            }
            zzf zzf = this.zzbw;
            if (zzf != null) {
                zzf.zza((zzdn) ((zzfc) zzb.zzhp()), zzcg.FOREGROUND_BACKGROUND);
            }
        }
    }

    private final void zzb(boolean z) {
        zzbi();
        zzf zzf = this.zzbw;
        if (zzf != null) {
            zzf.zzc(z);
        }
    }

    private final void zzbi() {
        if (this.zzbw == null) {
            this.zzbw = zzf.zzbs();
        }
    }

    private final boolean zza(Activity activity) {
        return (!this.zzch || activity.getWindow() == null || (activity.getWindow().getAttributes().flags & 16777216) == 0) ? false : true;
    }

    private static boolean zzbj() {
        try {
            Class.forName("androidx.core.app.FrameMetricsAggregator");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    private static String zzb(Activity activity) {
        String valueOf = String.valueOf(activity.getClass().getSimpleName());
        return valueOf.length() != 0 ? "_st_".concat(valueOf) : new String("_st_");
    }
}
