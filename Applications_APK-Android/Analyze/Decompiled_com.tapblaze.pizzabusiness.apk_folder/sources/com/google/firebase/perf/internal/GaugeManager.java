package com.google.firebase.perf.internal;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbb;
import com.google.android.gms.internal.p000firebaseperf.zzbc;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.android.gms.internal.p000firebaseperf.zzcg;
import com.google.android.gms.internal.p000firebaseperf.zzcm;
import com.google.android.gms.internal.p000firebaseperf.zzcq;
import com.google.android.gms.internal.p000firebaseperf.zzfc;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class GaugeManager {
    private static GaugeManager zzds = new GaugeManager();
    private final zzaf zzab;
    private final ScheduledExecutorService zzdt;
    private final zzbb zzdu;
    private final zzbc zzdv;
    private zzf zzdw;
    private zzr zzdx;
    private zzcg zzdy;
    private String zzdz;
    private ScheduledFuture zzea;
    private final ConcurrentLinkedQueue<zza> zzeb;

    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    class zza {
        /* access modifiers changed from: private */
        public final zzcq zzdr;
        /* access modifiers changed from: private */
        public final zzcg zzdy;

        zza(GaugeManager gaugeManager, zzcq zzcq, zzcg zzcg) {
            this.zzdr = zzcq;
            this.zzdy = zzcg;
        }
    }

    private GaugeManager() {
        this(Executors.newSingleThreadScheduledExecutor(), null, zzaf.zzl(), null, zzbb.zzbc(), zzbc.zzbe());
    }

    private GaugeManager(ScheduledExecutorService scheduledExecutorService, zzf zzf, zzaf zzaf, zzr zzr, zzbb zzbb, zzbc zzbc) {
        this.zzdy = zzcg.APPLICATION_PROCESS_STATE_UNKNOWN;
        this.zzdz = null;
        this.zzea = null;
        this.zzeb = new ConcurrentLinkedQueue<>();
        this.zzdt = scheduledExecutorService;
        this.zzdw = null;
        this.zzab = zzaf;
        this.zzdx = null;
        this.zzdu = zzbb;
        this.zzdv = zzbc;
    }

    public final void zzc(Context context) {
        this.zzdx = new zzr(context);
    }

    public static synchronized GaugeManager zzbx() {
        GaugeManager gaugeManager;
        synchronized (GaugeManager.class) {
            gaugeManager = zzds;
        }
        return gaugeManager;
    }

    public final void zza(zzt zzt, zzcg zzcg) {
        long j;
        boolean z;
        long j2;
        zzcg zzcg2 = zzcg;
        if (this.zzdz != null) {
            zzby();
        }
        zzbt zzce = zzt.zzce();
        int i = zzp.zzeg[zzcg.ordinal()];
        if (i == 1) {
            j = this.zzab.zzu();
        } else if (i != 2) {
            j = -1;
        } else {
            j = this.zzab.zzt();
        }
        if (zzbb.zzi(j)) {
            j = -1;
        }
        boolean z2 = false;
        if (j == -1) {
            Log.d("FirebasePerformance", "Invalid Cpu Metrics collection frequency. Did not collect Cpu Metrics.");
            z = false;
        } else {
            this.zzdu.zza(j, zzce);
            z = true;
        }
        if (!z) {
            j = -1;
        }
        int i2 = zzp.zzeg[zzcg.ordinal()];
        if (i2 == 1) {
            j2 = this.zzab.zzw();
        } else if (i2 != 2) {
            j2 = -1;
        } else {
            j2 = this.zzab.zzv();
        }
        if (zzbc.zzi(j2)) {
            j2 = -1;
        }
        if (j2 == -1) {
            Log.d("FirebasePerformance", "Invalid Memory Metrics collection frequency. Did not collect Memory Metrics.");
        } else {
            this.zzdv.zza(j2, zzce);
            z2 = true;
        }
        if (z2) {
            if (j == -1) {
                j = j2;
            } else {
                j = Math.min(j, j2);
            }
        }
        if (j == -1) {
            Log.w("FirebasePerformance", "Invalid gauge collection frequency. Unable to start collecting Gauges.");
            return;
        }
        this.zzdz = zzt.zzcd();
        this.zzdy = zzcg2;
        try {
            long j3 = j * 20;
            this.zzea = this.zzdt.scheduleAtFixedRate(new zzo(this, this.zzdz, zzcg2), j3, j3, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("FirebasePerformance", valueOf.length() != 0 ? "Unable to start collecting Gauges: ".concat(valueOf) : new String("Unable to start collecting Gauges: "));
        }
    }

    public final void zzby() {
        String str = this.zzdz;
        if (str != null) {
            zzcg zzcg = this.zzdy;
            this.zzdu.zzbd();
            this.zzdv.zzbd();
            ScheduledFuture scheduledFuture = this.zzea;
            if (scheduledFuture != null) {
                scheduledFuture.cancel(false);
            }
            this.zzdt.schedule(new zzn(this, str, zzcg), 20, TimeUnit.MILLISECONDS);
            this.zzdz = null;
            this.zzdy = zzcg.APPLICATION_PROCESS_STATE_UNKNOWN;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: zza */
    public final void zzd(String str, zzcg zzcg) {
        zzcq.zza zzec = zzcq.zzec();
        while (!this.zzdu.zzbn.isEmpty()) {
            zzec.zzb(this.zzdu.zzbn.poll());
        }
        while (!this.zzdv.zzbq.isEmpty()) {
            zzec.zzb(this.zzdv.zzbq.poll());
        }
        zzec.zzad(str);
        zzc((zzcq) ((zzfc) zzec.zzhp()), zzcg);
    }

    /* access modifiers changed from: package-private */
    public final boolean zzb(String str, zzcg zzcg) {
        if (this.zzdx == null) {
            return false;
        }
        zzc((zzcq) ((zzfc) zzcq.zzec().zzad(str).zzb((zzcm) ((zzfc) zzcm.zzdt().zzab(this.zzdx.getProcessName()).zzi(this.zzdx.zzcb()).zzj(this.zzdx.zzbz()).zzk(this.zzdx.zzca()).zzhp())).zzhp()), zzcg);
        return true;
    }

    private final void zzc(zzcq zzcq, zzcg zzcg) {
        zzf zzf = this.zzdw;
        if (zzf == null) {
            zzf = zzf.zzbs();
        }
        this.zzdw = zzf;
        zzf zzf2 = this.zzdw;
        if (zzf2 != null) {
            zzf2.zza(zzcq, zzcg);
            while (!this.zzeb.isEmpty()) {
                zza poll = this.zzeb.poll();
                this.zzdw.zza(poll.zzdr, poll.zzdy);
            }
            return;
        }
        this.zzeb.add(new zza(this, zzcq, zzcg));
    }

    public final void zzj(zzbt zzbt) {
        zzbb zzbb = this.zzdu;
        zzbc zzbc = this.zzdv;
        zzbb.zza(zzbt);
        zzbc.zza(zzbt);
    }
}
