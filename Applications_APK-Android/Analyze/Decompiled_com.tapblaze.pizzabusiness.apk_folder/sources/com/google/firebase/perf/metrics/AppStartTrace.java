package com.google.firebase.perf.metrics;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.internal.p000firebaseperf.zzbk;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.firebase.perf.internal.zzf;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class AppStartTrace implements Application.ActivityLifecycleCallbacks {
    private static final long zzfm = TimeUnit.MINUTES.toMicros(1);
    private static volatile AppStartTrace zzfn;
    private boolean mRegistered = false;
    private zzf zzbw = null;
    private final zzbk zzby;
    private Context zzfo;
    private WeakReference<Activity> zzfp;
    private WeakReference<Activity> zzfq;
    private boolean zzfr = false;
    /* access modifiers changed from: private */
    public zzbt zzfs = null;
    private zzbt zzft = null;
    private zzbt zzfu = null;
    /* access modifiers changed from: private */
    public boolean zzfv = false;

    public static void setLauncherActivityOnCreateTime(String str) {
    }

    public static void setLauncherActivityOnResumeTime(String str) {
    }

    public static void setLauncherActivityOnStartTime(String str) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public static class zza implements Runnable {
        private final AppStartTrace zzfx;

        public zza(AppStartTrace appStartTrace) {
            this.zzfx = appStartTrace;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.firebase.perf.metrics.AppStartTrace.zza(com.google.firebase.perf.metrics.AppStartTrace, boolean):boolean
         arg types: [com.google.firebase.perf.metrics.AppStartTrace, int]
         candidates:
          com.google.firebase.perf.metrics.AppStartTrace.zza(com.google.firebase.perf.internal.zzf, com.google.android.gms.internal.firebase-perf.zzbk):com.google.firebase.perf.metrics.AppStartTrace
          com.google.firebase.perf.metrics.AppStartTrace.zza(com.google.firebase.perf.metrics.AppStartTrace, boolean):boolean */
        public final void run() {
            if (this.zzfx.zzfs == null) {
                boolean unused = this.zzfx.zzfv = true;
            }
        }
    }

    public static AppStartTrace zzcp() {
        if (zzfn != null) {
            return zzfn;
        }
        return zza((zzf) null, new zzbk());
    }

    private static AppStartTrace zza(zzf zzf, zzbk zzbk) {
        if (zzfn == null) {
            synchronized (AppStartTrace.class) {
                if (zzfn == null) {
                    zzfn = new AppStartTrace(null, zzbk);
                }
            }
        }
        return zzfn;
    }

    private AppStartTrace(zzf zzf, zzbk zzbk) {
        this.zzby = zzbk;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zze(android.content.Context r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            boolean r0 = r1.mRegistered     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            android.content.Context r2 = r2.getApplicationContext()     // Catch:{ all -> 0x001c }
            boolean r0 = r2 instanceof android.app.Application     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x001a
            r0 = r2
            android.app.Application r0 = (android.app.Application) r0     // Catch:{ all -> 0x001c }
            r0.registerActivityLifecycleCallbacks(r1)     // Catch:{ all -> 0x001c }
            r0 = 1
            r1.mRegistered = r0     // Catch:{ all -> 0x001c }
            r1.zzfo = r2     // Catch:{ all -> 0x001c }
        L_0x001a:
            monitor-exit(r1)
            return
        L_0x001c:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.perf.metrics.AppStartTrace.zze(android.content.Context):void");
    }

    private final synchronized void zzcq() {
        if (this.mRegistered) {
            ((Application) this.zzfo).unregisterActivityLifecycleCallbacks(this);
            this.mRegistered = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onActivityCreated(android.app.Activity r4, android.os.Bundle r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r5 = r3.zzfv     // Catch:{ all -> 0x002f }
            if (r5 != 0) goto L_0x002d
            com.google.android.gms.internal.firebase-perf.zzbt r5 = r3.zzfs     // Catch:{ all -> 0x002f }
            if (r5 == 0) goto L_0x000a
            goto L_0x002d
        L_0x000a:
            java.lang.ref.WeakReference r5 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x002f }
            r5.<init>(r4)     // Catch:{ all -> 0x002f }
            r3.zzfp = r5     // Catch:{ all -> 0x002f }
            com.google.android.gms.internal.firebase-perf.zzbt r4 = new com.google.android.gms.internal.firebase-perf.zzbt     // Catch:{ all -> 0x002f }
            r4.<init>()     // Catch:{ all -> 0x002f }
            r3.zzfs = r4     // Catch:{ all -> 0x002f }
            com.google.android.gms.internal.firebase-perf.zzbt r4 = com.google.firebase.perf.provider.FirebasePerfProvider.zzcx()     // Catch:{ all -> 0x002f }
            com.google.android.gms.internal.firebase-perf.zzbt r5 = r3.zzfs     // Catch:{ all -> 0x002f }
            long r4 = r4.zzk(r5)     // Catch:{ all -> 0x002f }
            long r0 = com.google.firebase.perf.metrics.AppStartTrace.zzfm     // Catch:{ all -> 0x002f }
            int r2 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x002b
            r4 = 1
            r3.zzfr = r4     // Catch:{ all -> 0x002f }
        L_0x002b:
            monitor-exit(r3)
            return
        L_0x002d:
            monitor-exit(r3)
            return
        L_0x002f:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.perf.metrics.AppStartTrace.onActivityCreated(android.app.Activity, android.os.Bundle):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0018, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onActivityStarted(android.app.Activity r1) {
        /*
            r0 = this;
            monitor-enter(r0)
            boolean r1 = r0.zzfv     // Catch:{ all -> 0x0019 }
            if (r1 != 0) goto L_0x0017
            com.google.android.gms.internal.firebase-perf.zzbt r1 = r0.zzft     // Catch:{ all -> 0x0019 }
            if (r1 != 0) goto L_0x0017
            boolean r1 = r0.zzfr     // Catch:{ all -> 0x0019 }
            if (r1 == 0) goto L_0x000e
            goto L_0x0017
        L_0x000e:
            com.google.android.gms.internal.firebase-perf.zzbt r1 = new com.google.android.gms.internal.firebase-perf.zzbt     // Catch:{ all -> 0x0019 }
            r1.<init>()     // Catch:{ all -> 0x0019 }
            r0.zzft = r1     // Catch:{ all -> 0x0019 }
            monitor-exit(r0)
            return
        L_0x0017:
            monitor-exit(r0)
            return
        L_0x0019:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.perf.metrics.AppStartTrace.onActivityStarted(android.app.Activity):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0143, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0145, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onActivityResumed(android.app.Activity r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            boolean r0 = r6.zzfv     // Catch:{ all -> 0x0146 }
            if (r0 != 0) goto L_0x0144
            com.google.android.gms.internal.firebase-perf.zzbt r0 = r6.zzfu     // Catch:{ all -> 0x0146 }
            if (r0 != 0) goto L_0x0144
            boolean r0 = r6.zzfr     // Catch:{ all -> 0x0146 }
            if (r0 == 0) goto L_0x000f
            goto L_0x0144
        L_0x000f:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0146 }
            r0.<init>(r7)     // Catch:{ all -> 0x0146 }
            r6.zzfq = r0     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r0 = new com.google.android.gms.internal.firebase-perf.zzbt     // Catch:{ all -> 0x0146 }
            r0.<init>()     // Catch:{ all -> 0x0146 }
            r6.zzfu = r0     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r0 = com.google.firebase.perf.provider.FirebasePerfProvider.zzcx()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbi r1 = com.google.android.gms.internal.p000firebaseperf.zzbi.zzco()     // Catch:{ all -> 0x0146 }
            java.lang.Class r7 = r7.getClass()     // Catch:{ all -> 0x0146 }
            java.lang.String r7 = r7.getName()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r2 = r6.zzfu     // Catch:{ all -> 0x0146 }
            long r2 = r0.zzk(r2)     // Catch:{ all -> 0x0146 }
            java.lang.String r4 = java.lang.String.valueOf(r7)     // Catch:{ all -> 0x0146 }
            int r4 = r4.length()     // Catch:{ all -> 0x0146 }
            int r4 = r4 + 47
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0146 }
            r5.<init>(r4)     // Catch:{ all -> 0x0146 }
            java.lang.String r4 = "onResume(): "
            r5.append(r4)     // Catch:{ all -> 0x0146 }
            r5.append(r7)     // Catch:{ all -> 0x0146 }
            java.lang.String r7 = ": "
            r5.append(r7)     // Catch:{ all -> 0x0146 }
            r5.append(r2)     // Catch:{ all -> 0x0146 }
            java.lang.String r7 = " microseconds"
            r5.append(r7)     // Catch:{ all -> 0x0146 }
            java.lang.String r7 = r5.toString()     // Catch:{ all -> 0x0146 }
            r1.zzm(r7)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r7 = com.google.android.gms.internal.p000firebaseperf.zzdn.zzfx()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbm r1 = com.google.android.gms.internal.p000firebaseperf.zzbm.APP_START_TRACE_NAME     // Catch:{ all -> 0x0146 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r7 = r7.zzah(r1)     // Catch:{ all -> 0x0146 }
            long r1 = r0.zzcz()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r7 = r7.zzao(r1)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r1 = r6.zzfu     // Catch:{ all -> 0x0146 }
            long r1 = r0.zzk(r1)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r7 = r7.zzap(r1)     // Catch:{ all -> 0x0146 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0146 }
            r2 = 3
            r1.<init>(r2)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r2 = com.google.android.gms.internal.p000firebaseperf.zzdn.zzfx()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbm r3 = com.google.android.gms.internal.p000firebaseperf.zzbm.ON_CREATE_TRACE_NAME     // Catch:{ all -> 0x0146 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r2 = r2.zzah(r3)     // Catch:{ all -> 0x0146 }
            long r3 = r0.zzcz()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r2 = r2.zzao(r3)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r3 = r6.zzfs     // Catch:{ all -> 0x0146 }
            long r3 = r0.zzk(r3)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r0 = r2.zzap(r3)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzgl r0 = r0.zzhp()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzfc r0 = (com.google.android.gms.internal.p000firebaseperf.zzfc) r0     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn r0 = (com.google.android.gms.internal.p000firebaseperf.zzdn) r0     // Catch:{ all -> 0x0146 }
            r1.add(r0)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r0 = com.google.android.gms.internal.p000firebaseperf.zzdn.zzfx()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbm r2 = com.google.android.gms.internal.p000firebaseperf.zzbm.ON_START_TRACE_NAME     // Catch:{ all -> 0x0146 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r2 = r0.zzah(r2)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r3 = r6.zzfs     // Catch:{ all -> 0x0146 }
            long r3 = r3.zzcz()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r2 = r2.zzao(r3)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r3 = r6.zzfs     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r4 = r6.zzft     // Catch:{ all -> 0x0146 }
            long r3 = r3.zzk(r4)     // Catch:{ all -> 0x0146 }
            r2.zzap(r3)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzgl r0 = r0.zzhp()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzfc r0 = (com.google.android.gms.internal.p000firebaseperf.zzfc) r0     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn r0 = (com.google.android.gms.internal.p000firebaseperf.zzdn) r0     // Catch:{ all -> 0x0146 }
            r1.add(r0)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r0 = com.google.android.gms.internal.p000firebaseperf.zzdn.zzfx()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbm r2 = com.google.android.gms.internal.p000firebaseperf.zzbm.ON_RESUME_TRACE_NAME     // Catch:{ all -> 0x0146 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r2 = r0.zzah(r2)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r3 = r6.zzft     // Catch:{ all -> 0x0146 }
            long r3 = r3.zzcz()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r2 = r2.zzao(r3)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r3 = r6.zzft     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzbt r4 = r6.zzfu     // Catch:{ all -> 0x0146 }
            long r3 = r3.zzk(r4)     // Catch:{ all -> 0x0146 }
            r2.zzap(r3)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzgl r0 = r0.zzhp()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzfc r0 = (com.google.android.gms.internal.p000firebaseperf.zzfc) r0     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn r0 = (com.google.android.gms.internal.p000firebaseperf.zzdn) r0     // Catch:{ all -> 0x0146 }
            r1.add(r0)     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn$zzb r0 = r7.zzd(r1)     // Catch:{ all -> 0x0146 }
            com.google.firebase.perf.internal.SessionManager r1 = com.google.firebase.perf.internal.SessionManager.zzck()     // Catch:{ all -> 0x0146 }
            com.google.firebase.perf.internal.zzt r1 = r1.zzcl()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzde r1 = r1.zzcg()     // Catch:{ all -> 0x0146 }
            r0.zzb(r1)     // Catch:{ all -> 0x0146 }
            com.google.firebase.perf.internal.zzf r0 = r6.zzbw     // Catch:{ all -> 0x0146 }
            if (r0 != 0) goto L_0x0128
            com.google.firebase.perf.internal.zzf r0 = com.google.firebase.perf.internal.zzf.zzbs()     // Catch:{ all -> 0x0146 }
            r6.zzbw = r0     // Catch:{ all -> 0x0146 }
        L_0x0128:
            com.google.firebase.perf.internal.zzf r0 = r6.zzbw     // Catch:{ all -> 0x0146 }
            if (r0 == 0) goto L_0x013b
            com.google.firebase.perf.internal.zzf r0 = r6.zzbw     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzgl r7 = r7.zzhp()     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzfc r7 = (com.google.android.gms.internal.p000firebaseperf.zzfc) r7     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzdn r7 = (com.google.android.gms.internal.p000firebaseperf.zzdn) r7     // Catch:{ all -> 0x0146 }
            com.google.android.gms.internal.firebase-perf.zzcg r1 = com.google.android.gms.internal.p000firebaseperf.zzcg.FOREGROUND_BACKGROUND     // Catch:{ all -> 0x0146 }
            r0.zza(r7, r1)     // Catch:{ all -> 0x0146 }
        L_0x013b:
            boolean r7 = r6.mRegistered     // Catch:{ all -> 0x0146 }
            if (r7 == 0) goto L_0x0142
            r6.zzcq()     // Catch:{ all -> 0x0146 }
        L_0x0142:
            monitor-exit(r6)
            return
        L_0x0144:
            monitor-exit(r6)
            return
        L_0x0146:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.perf.metrics.AppStartTrace.onActivityResumed(android.app.Activity):void");
    }

    public synchronized void onActivityStopped(Activity activity) {
    }
}
