package com.google.firebase.perf.internal;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzi implements Runnable {
    private final /* synthetic */ zzf zzcy;
    private final /* synthetic */ boolean zzdo;

    zzi(zzf zzf, boolean z) {
        this.zzcy = zzf;
        this.zzdo = z;
    }

    public final void run() {
        this.zzcy.zzd(this.zzdo);
    }
}
