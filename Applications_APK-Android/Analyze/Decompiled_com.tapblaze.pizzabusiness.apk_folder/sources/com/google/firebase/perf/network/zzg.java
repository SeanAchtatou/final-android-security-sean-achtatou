package com.google.firebase.perf.network;

import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import java.io.IOException;
import org.apache.http.HttpMessage;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzg<T> implements ResponseHandler<T> {
    private final zzbt zzfz;
    private final zzbg zzgo;
    private final ResponseHandler<? extends T> zzha;

    public zzg(ResponseHandler<? extends T> responseHandler, zzbt zzbt, zzbg zzbg) {
        this.zzha = responseHandler;
        this.zzfz = zzbt;
        this.zzgo = zzbg;
    }

    public final T handleResponse(HttpResponse httpResponse) throws IOException {
        this.zzgo.zzn(this.zzfz.zzda());
        this.zzgo.zzc(httpResponse.getStatusLine().getStatusCode());
        Long zza = zzh.zza((HttpMessage) httpResponse);
        if (zza != null) {
            this.zzgo.zzo(zza.longValue());
        }
        String zza2 = zzh.zza(httpResponse);
        if (zza2 != null) {
            this.zzgo.zzh(zza2);
        }
        this.zzgo.zzbo();
        return this.zzha.handleResponse(httpResponse);
    }
}
