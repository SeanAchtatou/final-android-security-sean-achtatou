package com.google.firebase.perf.internal;

import android.content.Context;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbk;
import com.google.android.gms.internal.p000firebaseperf.zzbm;
import com.google.android.gms.internal.p000firebaseperf.zzbx;
import com.google.android.gms.internal.p000firebaseperf.zzd;
import com.google.android.gms.internal.p000firebaseperf.zzda;
import com.google.android.gms.internal.p000firebaseperf.zzde;
import com.google.android.gms.internal.p000firebaseperf.zzdi;
import java.util.List;
import java.util.Random;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzv {
    private final zzaf zzab;
    private boolean zzdk;
    private final float zzex;
    private zzu zzey;
    private zzu zzez;

    public zzv(Context context, double d, long j) {
        this(100.0d, 500, new zzbk(), new Random().nextFloat(), zzaf.zzl());
        this.zzdk = zzbx.zzg(context);
    }

    private zzv(double d, long j, zzbk zzbk, float f, zzaf zzaf) {
        float f2 = f;
        boolean z = false;
        this.zzdk = false;
        this.zzey = null;
        this.zzez = null;
        if (0.0f <= f2 && f2 < 1.0f) {
            z = true;
        }
        zzd.checkArgument(z, "Sampling bucket ID should be in range [0.0f, 1.0f).");
        this.zzex = f2;
        this.zzab = zzaf;
        zzbk zzbk2 = zzbk;
        zzaf zzaf2 = zzaf;
        this.zzey = new zzu(100.0d, 500, zzbk2, zzaf2, "Trace", this.zzdk);
        this.zzez = new zzu(100.0d, 500, zzbk2, zzaf2, "Network", this.zzdk);
    }

    /* access modifiers changed from: package-private */
    public final boolean zzb(zzda zzda) {
        if (zzda.zzff()) {
            if (!(this.zzex < this.zzab.zzq()) && !zzb(zzda.zzfg().zzex())) {
                return false;
            }
        }
        if (zzda.zzfh()) {
            if (!(this.zzex < this.zzab.zzr()) && !zzb(zzda.zzfi().zzex())) {
                return false;
            }
        }
        if (!((!zzda.zzff() || ((!zzda.zzfg().getName().equals(zzbm.FOREGROUND_TRACE_NAME.toString()) && !zzda.zzfg().getName().equals(zzbm.BACKGROUND_TRACE_NAME.toString())) || zzda.zzfg().zzfq() <= 0)) && !zzda.zzfj())) {
            return true;
        }
        if (zzda.zzfh()) {
            return this.zzez.zzb(zzda);
        }
        if (zzda.zzff()) {
            return this.zzey.zzb(zzda);
        }
        return false;
    }

    private static boolean zzb(List<zzde> list) {
        if (list.size() <= 0 || list.get(0).zzfn() <= 0 || list.get(0).zzn(0) != zzdi.GAUGES_AND_SYSTEM_EVENTS) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void zzc(boolean z) {
        this.zzey.zzc(z);
        this.zzez.zzc(z);
    }
}
