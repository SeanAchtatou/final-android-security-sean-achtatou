package com.google.firebase.perf.network;

import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.firebase.perf.internal.zzf;
import java.io.IOException;
import org.apache.http.HttpHost;
import org.apache.http.HttpMessage;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class FirebasePerfHttpClient {
    private FirebasePerfHttpClient() {
    }

    public static HttpResponse execute(HttpClient httpClient, HttpUriRequest httpUriRequest) throws IOException {
        return zza(httpClient, httpUriRequest, new zzbt(), zzf.zzbs());
    }

    public static HttpResponse execute(HttpClient httpClient, HttpUriRequest httpUriRequest, HttpContext httpContext) throws IOException {
        return zza(httpClient, httpUriRequest, httpContext, new zzbt(), zzf.zzbs());
    }

    public static <T> T execute(HttpClient httpClient, HttpUriRequest httpUriRequest, ResponseHandler<T> responseHandler) throws IOException {
        return zza(httpClient, httpUriRequest, responseHandler, new zzbt(), zzf.zzbs());
    }

    public static <T> T execute(HttpClient httpClient, HttpUriRequest httpUriRequest, ResponseHandler<T> responseHandler, HttpContext httpContext) throws IOException {
        return zza(httpClient, httpUriRequest, responseHandler, httpContext, new zzbt(), zzf.zzbs());
    }

    public static HttpResponse execute(HttpClient httpClient, HttpHost httpHost, HttpRequest httpRequest) throws IOException {
        return zza(httpClient, httpHost, httpRequest, new zzbt(), zzf.zzbs());
    }

    public static HttpResponse execute(HttpClient httpClient, HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext) throws IOException {
        return zza(httpClient, httpHost, httpRequest, httpContext, new zzbt(), zzf.zzbs());
    }

    public static <T> T execute(HttpClient httpClient, HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler) throws IOException {
        return zza(httpClient, httpHost, httpRequest, responseHandler, new zzbt(), zzf.zzbs());
    }

    public static <T> T execute(HttpClient httpClient, HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) throws IOException {
        return zza(httpClient, httpHost, httpRequest, responseHandler, httpContext, new zzbt(), zzf.zzbs());
    }

    private static HttpResponse zza(HttpClient httpClient, HttpUriRequest httpUriRequest, zzbt zzbt, zzf zzf) throws IOException {
        zzbg zzb = zzbg.zzb(zzf);
        try {
            zzb.zzf(httpUriRequest.getURI().toString()).zzg(httpUriRequest.getMethod());
            Long zza = zzh.zza((HttpMessage) httpUriRequest);
            if (zza != null) {
                zzb.zzj(zza.longValue());
            }
            zzbt.reset();
            zzb.zzk(zzbt.zzcz());
            HttpResponse execute = httpClient.execute(httpUriRequest);
            zzb.zzn(zzbt.zzda());
            zzb.zzc(execute.getStatusLine().getStatusCode());
            Long zza2 = zzh.zza((HttpMessage) execute);
            if (zza2 != null) {
                zzb.zzo(zza2.longValue());
            }
            String zza3 = zzh.zza(execute);
            if (zza3 != null) {
                zzb.zzh(zza3);
            }
            zzb.zzbo();
            return execute;
        } catch (IOException e) {
            zzb.zzn(zzbt.zzda());
            zzh.zza(zzb);
            throw e;
        }
    }

    private static HttpResponse zza(HttpClient httpClient, HttpUriRequest httpUriRequest, HttpContext httpContext, zzbt zzbt, zzf zzf) throws IOException {
        zzbg zzb = zzbg.zzb(zzf);
        try {
            zzb.zzf(httpUriRequest.getURI().toString()).zzg(httpUriRequest.getMethod());
            Long zza = zzh.zza((HttpMessage) httpUriRequest);
            if (zza != null) {
                zzb.zzj(zza.longValue());
            }
            zzbt.reset();
            zzb.zzk(zzbt.zzcz());
            HttpResponse execute = httpClient.execute(httpUriRequest, httpContext);
            zzb.zzn(zzbt.zzda());
            zzb.zzc(execute.getStatusLine().getStatusCode());
            Long zza2 = zzh.zza((HttpMessage) execute);
            if (zza2 != null) {
                zzb.zzo(zza2.longValue());
            }
            String zza3 = zzh.zza(execute);
            if (zza3 != null) {
                zzb.zzh(zza3);
            }
            zzb.zzbo();
            return execute;
        } catch (IOException e) {
            zzb.zzn(zzbt.zzda());
            zzh.zza(zzb);
            throw e;
        }
    }

    private static <T> T zza(HttpClient httpClient, HttpUriRequest httpUriRequest, ResponseHandler<T> responseHandler, zzbt zzbt, zzf zzf) throws IOException {
        zzbg zzb = zzbg.zzb(zzf);
        try {
            zzb.zzf(httpUriRequest.getURI().toString()).zzg(httpUriRequest.getMethod());
            Long zza = zzh.zza((HttpMessage) httpUriRequest);
            if (zza != null) {
                zzb.zzj(zza.longValue());
            }
            zzbt.reset();
            zzb.zzk(zzbt.zzcz());
            return httpClient.execute(httpUriRequest, new zzg(responseHandler, zzbt, zzb));
        } catch (IOException e) {
            zzb.zzn(zzbt.zzda());
            zzh.zza(zzb);
            throw e;
        }
    }

    private static <T> T zza(HttpClient httpClient, HttpUriRequest httpUriRequest, ResponseHandler<T> responseHandler, HttpContext httpContext, zzbt zzbt, zzf zzf) throws IOException {
        zzbg zzb = zzbg.zzb(zzf);
        try {
            zzb.zzf(httpUriRequest.getURI().toString()).zzg(httpUriRequest.getMethod());
            Long zza = zzh.zza((HttpMessage) httpUriRequest);
            if (zza != null) {
                zzb.zzj(zza.longValue());
            }
            zzbt.reset();
            zzb.zzk(zzbt.zzcz());
            return httpClient.execute(httpUriRequest, new zzg(responseHandler, zzbt, zzb), httpContext);
        } catch (IOException e) {
            zzb.zzn(zzbt.zzda());
            zzh.zza(zzb);
            throw e;
        }
    }

    private static HttpResponse zza(HttpClient httpClient, HttpHost httpHost, HttpRequest httpRequest, zzbt zzbt, zzf zzf) throws IOException {
        zzbg zzb = zzbg.zzb(zzf);
        try {
            String valueOf = String.valueOf(httpHost.toURI());
            String valueOf2 = String.valueOf(httpRequest.getRequestLine().getUri());
            zzb.zzf(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)).zzg(httpRequest.getRequestLine().getMethod());
            Long zza = zzh.zza((HttpMessage) httpRequest);
            if (zza != null) {
                zzb.zzj(zza.longValue());
            }
            zzbt.reset();
            zzb.zzk(zzbt.zzcz());
            HttpResponse execute = httpClient.execute(httpHost, httpRequest);
            zzb.zzn(zzbt.zzda());
            zzb.zzc(execute.getStatusLine().getStatusCode());
            Long zza2 = zzh.zza((HttpMessage) execute);
            if (zza2 != null) {
                zzb.zzo(zza2.longValue());
            }
            String zza3 = zzh.zza(execute);
            if (zza3 != null) {
                zzb.zzh(zza3);
            }
            zzb.zzbo();
            return execute;
        } catch (IOException e) {
            zzb.zzn(zzbt.zzda());
            zzh.zza(zzb);
            throw e;
        }
    }

    private static HttpResponse zza(HttpClient httpClient, HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext, zzbt zzbt, zzf zzf) throws IOException {
        zzbg zzb = zzbg.zzb(zzf);
        try {
            String valueOf = String.valueOf(httpHost.toURI());
            String valueOf2 = String.valueOf(httpRequest.getRequestLine().getUri());
            zzb.zzf(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)).zzg(httpRequest.getRequestLine().getMethod());
            Long zza = zzh.zza((HttpMessage) httpRequest);
            if (zza != null) {
                zzb.zzj(zza.longValue());
            }
            zzbt.reset();
            zzb.zzk(zzbt.zzcz());
            HttpResponse execute = httpClient.execute(httpHost, httpRequest, httpContext);
            zzb.zzn(zzbt.zzda());
            zzb.zzc(execute.getStatusLine().getStatusCode());
            Long zza2 = zzh.zza((HttpMessage) execute);
            if (zza2 != null) {
                zzb.zzo(zza2.longValue());
            }
            String zza3 = zzh.zza(execute);
            if (zza3 != null) {
                zzb.zzh(zza3);
            }
            zzb.zzbo();
            return execute;
        } catch (IOException e) {
            zzb.zzn(zzbt.zzda());
            zzh.zza(zzb);
            throw e;
        }
    }

    private static <T> T zza(HttpClient httpClient, HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler, zzbt zzbt, zzf zzf) throws IOException {
        zzbg zzb = zzbg.zzb(zzf);
        try {
            String valueOf = String.valueOf(httpHost.toURI());
            String valueOf2 = String.valueOf(httpRequest.getRequestLine().getUri());
            zzb.zzf(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)).zzg(httpRequest.getRequestLine().getMethod());
            Long zza = zzh.zza((HttpMessage) httpRequest);
            if (zza != null) {
                zzb.zzj(zza.longValue());
            }
            zzbt.reset();
            zzb.zzk(zzbt.zzcz());
            return httpClient.execute(httpHost, httpRequest, new zzg(responseHandler, zzbt, zzb));
        } catch (IOException e) {
            zzb.zzn(zzbt.zzda());
            zzh.zza(zzb);
            throw e;
        }
    }

    private static <T> T zza(HttpClient httpClient, HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext, zzbt zzbt, zzf zzf) throws IOException {
        zzbg zzb = zzbg.zzb(zzf);
        try {
            String valueOf = String.valueOf(httpHost.toURI());
            String valueOf2 = String.valueOf(httpRequest.getRequestLine().getUri());
            zzb.zzf(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)).zzg(httpRequest.getRequestLine().getMethod());
            Long zza = zzh.zza((HttpMessage) httpRequest);
            if (zza != null) {
                zzb.zzj(zza.longValue());
            }
            zzbt.reset();
            zzb.zzk(zzbt.zzcz());
            return httpClient.execute(httpHost, httpRequest, new zzg(responseHandler, zzbt, zzb), httpContext);
        } catch (IOException e) {
            zzb.zzn(zzbt.zzda());
            zzh.zza(zzb);
            throw e;
        }
    }
}
