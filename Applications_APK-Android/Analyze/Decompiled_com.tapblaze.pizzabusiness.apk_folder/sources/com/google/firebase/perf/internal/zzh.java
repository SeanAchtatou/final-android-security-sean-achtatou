package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzcg;
import com.google.android.gms.internal.p000firebaseperf.zzdn;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzh implements Runnable {
    private final /* synthetic */ zzf zzcy;
    private final /* synthetic */ zzcg zzdm;
    private final /* synthetic */ zzdn zzdn;

    zzh(zzf zzf, zzdn zzdn2, zzcg zzcg) {
        this.zzcy = zzf;
        this.zzdn = zzdn2;
        this.zzdm = zzcg;
    }

    public final void run() {
        this.zzcy.zzb(this.zzdn, this.zzdm);
    }
}
