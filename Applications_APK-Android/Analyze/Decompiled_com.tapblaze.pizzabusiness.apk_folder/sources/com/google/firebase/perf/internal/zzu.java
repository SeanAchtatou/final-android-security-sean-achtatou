package com.google.firebase.perf.internal;

import android.util.Log;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbk;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.android.gms.internal.p000firebaseperf.zzda;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzu {
    private static final long zzeo = TimeUnit.SECONDS.toMicros(1);
    private final zzbk zzby;
    private final boolean zzdk;
    private long zzep;
    private double zzeq;
    private zzbt zzer = new zzbt();
    private long zzes;
    private double zzet;
    private long zzeu;
    private double zzev;
    private long zzew;

    zzu(double d, long j, zzbk zzbk, zzaf zzaf, String str, boolean z) {
        long j2;
        long j3;
        this.zzby = zzbk;
        this.zzep = j;
        this.zzeq = d;
        this.zzes = j;
        long zzac = zzaf.zzac();
        if (str == "Trace") {
            j2 = zzaf.zzy();
        } else {
            j2 = zzaf.zzaa();
        }
        double d2 = (double) j2;
        double d3 = (double) zzac;
        Double.isNaN(d2);
        Double.isNaN(d3);
        this.zzet = d2 / d3;
        this.zzeu = j2;
        if (z) {
            Log.d("FirebasePerformance", String.format("Foreground %s logging rate:%f, burst capacity:%d", str, Double.valueOf(this.zzet), Long.valueOf(this.zzeu)));
        }
        long zzac2 = zzaf.zzac();
        if (str == "Trace") {
            j3 = zzaf.zzz();
        } else {
            j3 = zzaf.zzab();
        }
        double d4 = (double) j3;
        double d5 = (double) zzac2;
        Double.isNaN(d4);
        Double.isNaN(d5);
        this.zzev = d4 / d5;
        this.zzew = j3;
        if (z) {
            Log.d("FirebasePerformance", String.format("Background %s logging rate:%f, capacity:%d", str, Double.valueOf(this.zzev), Long.valueOf(this.zzew)));
        }
        this.zzdk = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: package-private */
    public final synchronized boolean zzb(zzda zzda) {
        zzbt zzbt = new zzbt();
        double zzk = (double) this.zzer.zzk(zzbt);
        double d = this.zzeq;
        Double.isNaN(zzk);
        double d2 = zzk * d;
        double d3 = (double) zzeo;
        Double.isNaN(d3);
        this.zzes = Math.min(this.zzes + Math.max(0L, (long) (d2 / d3)), this.zzep);
        if (this.zzes > 0) {
            this.zzes--;
            this.zzer = zzbt;
            return true;
        }
        if (this.zzdk) {
            Log.w("FirebasePerformance", "Exceeded log rate limit, dropping the log.");
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void zzc(boolean z) {
        this.zzeq = z ? this.zzet : this.zzev;
        this.zzep = z ? this.zzeu : this.zzew;
    }
}
