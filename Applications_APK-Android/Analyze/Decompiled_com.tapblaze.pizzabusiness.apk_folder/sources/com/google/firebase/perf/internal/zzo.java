package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzcg;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final /* synthetic */ class zzo implements Runnable {
    private final GaugeManager zzed;
    private final String zzee;
    private final zzcg zzef;

    zzo(GaugeManager gaugeManager, String str, zzcg zzcg) {
        this.zzed = gaugeManager;
        this.zzee = str;
        this.zzef = zzcg;
    }

    public final void run() {
        this.zzed.zzd(this.zzee, this.zzef);
    }
}
