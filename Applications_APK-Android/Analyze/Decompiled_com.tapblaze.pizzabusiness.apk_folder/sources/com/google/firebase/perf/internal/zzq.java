package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzbj;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.Locale;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public abstract class zzq {
    public abstract boolean zzbr();

    public static String zzk(String str) {
        if (str == null) {
            return "Metric name must not be null";
        }
        if (str.length() > 100) {
            return String.format(Locale.US, "Metric name must not exceed %d characters", 100);
        } else if (!str.startsWith(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR)) {
            return null;
        } else {
            for (zzbj zzbj : zzbj.values()) {
                if (zzbj.toString().equals(str)) {
                    return null;
                }
            }
            return "Metric name must not start with '_'";
        }
    }

    public static String zza(Map.Entry<String, String> entry) {
        String key = entry.getKey();
        String value = entry.getValue();
        if (key == null) {
            return "Attribute key must not be null";
        }
        if (value == null) {
            return "Attribute value must not be null";
        }
        if (key.length() > 40) {
            return String.format(Locale.US, "Attribute key length must not exceed %d characters", 40);
        } else if (value.length() > 100) {
            return String.format(Locale.US, "Attribute value length must not exceed %d characters", 100);
        } else if (!key.matches("^(?!(firebase_|google_|ga_))[A-Za-z][A-Za-z_0-9]*")) {
            return "Attribute key must start with letter, must only contain alphanumeric characters and underscore and must not start with \"firebase_\", \"google_\" and \"ga_";
        } else {
            return null;
        }
    }
}
