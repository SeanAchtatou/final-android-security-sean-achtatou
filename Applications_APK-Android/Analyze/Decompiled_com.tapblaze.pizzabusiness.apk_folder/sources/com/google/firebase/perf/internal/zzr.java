package com.google.firebase.perf.internal;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Process;
import com.google.android.gms.internal.p000firebaseperf.zzbn;
import com.google.android.gms.internal.p000firebaseperf.zzx;
import java.util.Iterator;
import java.util.List;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzr {
    private final Runtime zzbr;
    private final ActivityManager zzeh;
    private final ActivityManager.MemoryInfo zzei;
    private final String zzej;
    private final Context zzek;

    zzr(Context context) {
        this(Runtime.getRuntime(), context);
    }

    private zzr(Runtime runtime, Context context) {
        String str;
        this.zzbr = runtime;
        this.zzek = context;
        this.zzeh = (ActivityManager) context.getSystemService("activity");
        this.zzei = new ActivityManager.MemoryInfo();
        this.zzeh.getMemoryInfo(this.zzei);
        int myPid = Process.myPid();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = this.zzeh.getRunningAppProcesses();
        if (runningAppProcesses != null) {
            Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it.next();
                if (next.pid == myPid) {
                    str = next.processName;
                    break;
                }
            }
            this.zzej = str;
        }
        str = this.zzek.getPackageName();
        this.zzej = str;
    }

    public final String getProcessName() {
        return this.zzej;
    }

    public final int zzbz() {
        return zzx.zza(zzbn.BYTES.zzt(this.zzbr.maxMemory()));
    }

    public final int zzca() {
        return zzx.zza(zzbn.MEGABYTES.zzt((long) this.zzeh.getMemoryClass()));
    }

    public final int zzcb() {
        return zzx.zza(zzbn.BYTES.zzt(this.zzei.totalMem));
    }
}
