package com.google.firebase.perf.metrics;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbk;
import com.google.android.gms.internal.p000firebaseperf.zzbm;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.firebase.perf.internal.GaugeManager;
import com.google.firebase.perf.internal.SessionManager;
import com.google.firebase.perf.internal.zza;
import com.google.firebase.perf.internal.zzb;
import com.google.firebase.perf.internal.zzf;
import com.google.firebase.perf.internal.zzq;
import com.google.firebase.perf.internal.zzt;
import com.google.firebase.perf.internal.zzx;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.lang.ref.WeakReference;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class Trace extends zzb implements Parcelable, zzx {
    public static final Parcelable.Creator<Trace> CREATOR = new zzc();
    private static final Map<String, Trace> zzgd = new ConcurrentHashMap();
    private static final Parcelable.Creator<Trace> zzgl = new zze();
    private final String name;
    private final List<zzt> zzck;
    private final GaugeManager zzcl;
    private final WeakReference<zzx> zzcq;
    private final zzf zzcz;
    private final Trace zzge;
    private final List<Trace> zzgf;
    private final Map<String, zza> zzgg;
    private final zzbk zzgh;
    private final Map<String, String> zzgi;
    private zzbt zzgj;
    private zzbt zzgk;

    public int describeContents() {
        return 0;
    }

    public final void zza(zzt zzt) {
        if (hasStarted() && !isStopped()) {
            this.zzck.add(zzt);
        }
    }

    public static Trace zzn(String str) {
        return new Trace(str);
    }

    private Trace(String str) {
        this(str, zzf.zzbs(), new zzbk(), zza.zzbf(), GaugeManager.zzbx());
    }

    public Trace(String str, zzf zzf, zzbk zzbk, zza zza) {
        this(str, zzf, zzbk, zza, GaugeManager.zzbx());
    }

    private Trace(String str, zzf zzf, zzbk zzbk, zza zza, GaugeManager gaugeManager) {
        super(zza);
        this.zzcq = new WeakReference<>(this);
        this.zzge = null;
        this.name = str.trim();
        this.zzgf = new ArrayList();
        this.zzgg = new ConcurrentHashMap();
        this.zzgi = new ConcurrentHashMap();
        this.zzgh = zzbk;
        this.zzcz = zzf;
        this.zzck = new ArrayList();
        this.zzcl = gaugeManager;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private Trace(Parcel parcel, boolean z) {
        super(z ? null : zza.zzbf());
        Class<Trace> cls = Trace.class;
        this.zzcq = new WeakReference<>(this);
        this.zzge = (Trace) parcel.readParcelable(cls.getClassLoader());
        this.name = parcel.readString();
        this.zzgf = new ArrayList();
        parcel.readList(this.zzgf, cls.getClassLoader());
        this.zzgg = new ConcurrentHashMap();
        this.zzgi = new ConcurrentHashMap();
        parcel.readMap(this.zzgg, zza.class.getClassLoader());
        this.zzgj = (zzbt) parcel.readParcelable(zzbt.class.getClassLoader());
        this.zzgk = (zzbt) parcel.readParcelable(zzbt.class.getClassLoader());
        this.zzck = new ArrayList();
        parcel.readList(this.zzck, zzt.class.getClassLoader());
        if (z) {
            this.zzcz = null;
            this.zzgh = null;
            this.zzcl = null;
            return;
        }
        this.zzcz = zzf.zzbs();
        this.zzgh = new zzbk();
        this.zzcl = GaugeManager.zzbx();
    }

    public void start() {
        String str;
        if (!zzaf.zzl().zzm()) {
            Log.i("FirebasePerformance", "Trace feature is disabled.");
            return;
        }
        String str2 = this.name;
        if (str2 == null) {
            str = "Trace name must not be null";
        } else if (str2.length() > 100) {
            str = String.format(Locale.US, "Trace name must not exceed %d characters", 100);
        } else {
            if (str2.startsWith(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR)) {
                zzbm[] values = zzbm.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i < length) {
                        if (values[i].toString().equals(str2)) {
                            break;
                        }
                        i++;
                    } else if (!str2.startsWith("_st_")) {
                        str = "Trace name must not start with '_'";
                    }
                }
            }
            str = null;
        }
        if (str != null) {
            Log.e("FirebasePerformance", String.format("Cannot start trace %s. Trace name is invalid.(%s)", this.name, str));
        } else if (this.zzgj != null) {
            Log.e("FirebasePerformance", String.format("Trace '%s' has already started, should not start again!", this.name));
        } else {
            zzbp();
            zzt zzcl2 = SessionManager.zzck().zzcl();
            SessionManager.zzck().zzc(this.zzcq);
            this.zzck.add(zzcl2);
            this.zzgj = new zzbt();
            if (zzcl2.zzcf()) {
                this.zzcl.zzj(zzcl2.zzce());
            }
        }
    }

    public void stop() {
        if (!hasStarted()) {
            Log.e("FirebasePerformance", String.format("Trace '%s' has not been started so unable to stop!", this.name));
        } else if (isStopped()) {
            Log.e("FirebasePerformance", String.format("Trace '%s' has already stopped, should not stop again!", this.name));
        } else {
            SessionManager.zzck().zzd(this.zzcq);
            zzbq();
            this.zzgk = new zzbt();
            if (this.zzge == null) {
                zzbt zzbt = this.zzgk;
                if (!this.zzgf.isEmpty()) {
                    Trace trace = this.zzgf.get(this.zzgf.size() - 1);
                    if (trace.zzgk == null) {
                        trace.zzgk = zzbt;
                    }
                }
                if (!this.name.isEmpty()) {
                    zzf zzf = this.zzcz;
                    if (zzf != null) {
                        zzf.zza(new zzd(this).zzcv(), zzbh());
                        if (SessionManager.zzck().zzcl().zzcf()) {
                            this.zzcl.zzj(SessionManager.zzck().zzcl().zzce());
                            return;
                        }
                        return;
                    }
                    return;
                }
                Log.e("FirebasePerformance", "Trace name is empty, no log is sent to server");
            }
        }
    }

    private final zza zzo(String str) {
        zza zza = this.zzgg.get(str);
        if (zza != null) {
            return zza;
        }
        zza zza2 = new zza(str);
        this.zzgg.put(str, zza2);
        return zza2;
    }

    public void incrementMetric(String str, long j) {
        String zzk = zzq.zzk(str);
        if (zzk != null) {
            Log.e("FirebasePerformance", String.format("Cannot increment metric %s. Metric name is invalid.(%s)", str, zzk));
        } else if (!hasStarted()) {
            Log.w("FirebasePerformance", String.format("Cannot increment metric '%s' for trace '%s' because it's not started", str, this.name));
        } else if (isStopped()) {
            Log.w("FirebasePerformance", String.format("Cannot increment metric '%s' for trace '%s' because it's been stopped", str, this.name));
        } else {
            zzo(str.trim()).zzr(j);
        }
    }

    public long getLongMetric(String str) {
        zza zza = str != null ? this.zzgg.get(str.trim()) : null;
        if (zza == null) {
            return 0;
        }
        return zza.getCount();
    }

    public void putMetric(String str, long j) {
        String zzk = zzq.zzk(str);
        if (zzk != null) {
            Log.e("FirebasePerformance", String.format("Cannot set value for metric %s. Metric name is invalid.(%s)", str, zzk));
        } else if (!hasStarted()) {
            Log.w("FirebasePerformance", String.format("Cannot set value for metric '%s' for trace '%s' because it's not started", str, this.name));
        } else if (isStopped()) {
            Log.w("FirebasePerformance", String.format("Cannot set value for metric '%s' for trace '%s' because it's been stopped", str, this.name));
        } else {
            zzo(str.trim()).zzs(j);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            if (hasStarted() && !isStopped()) {
                Log.w("FirebasePerformance", String.format("Trace '%s' is started but not stopped when it is destructed!", this.name));
                zzb(1);
            }
        } finally {
            super.finalize();
        }
    }

    /* access modifiers changed from: package-private */
    public final String getName() {
        return this.name;
    }

    /* access modifiers changed from: package-private */
    public final Map<String, zza> zzcr() {
        return this.zzgg;
    }

    /* access modifiers changed from: package-private */
    public final zzbt zzcs() {
        return this.zzgj;
    }

    /* access modifiers changed from: package-private */
    public final zzbt zzct() {
        return this.zzgk;
    }

    /* access modifiers changed from: package-private */
    public final List<Trace> zzcu() {
        return this.zzgf;
    }

    private final boolean isStopped() {
        return this.zzgk != null;
    }

    private final boolean hasStarted() {
        return this.zzgj != null;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.zzge, 0);
        parcel.writeString(this.name);
        parcel.writeList(this.zzgf);
        parcel.writeMap(this.zzgg);
        parcel.writeParcelable(this.zzgj, 0);
        parcel.writeParcelable(this.zzgk, 0);
        parcel.writeList(this.zzck);
    }

    public void putAttribute(String str, String str2) {
        boolean z = false;
        try {
            str = str.trim();
            str2 = str2.trim();
            if (!isStopped()) {
                if (!this.zzgi.containsKey(str)) {
                    if (this.zzgi.size() >= 5) {
                        throw new IllegalArgumentException(String.format(Locale.US, "Exceeds max limit of number of attributes - %d", 5));
                    }
                }
                String zza = zzq.zza(new AbstractMap.SimpleEntry(str, str2));
                if (zza == null) {
                    z = true;
                    if (z) {
                        this.zzgi.put(str, str2);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException(zza);
            }
            throw new IllegalArgumentException(String.format(Locale.US, "Trace %s has been stopped", this.name));
        } catch (Exception e) {
            Log.e("FirebasePerformance", String.format("Can not set attribute %s with value %s (%s)", str, str2, e.getMessage()));
        }
    }

    public void removeAttribute(String str) {
        if (isStopped()) {
            Log.e("FirebasePerformance", "Can't remove a attribute from a Trace that's stopped.");
        } else {
            this.zzgi.remove(str);
        }
    }

    public String getAttribute(String str) {
        return this.zzgi.get(str);
    }

    public Map<String, String> getAttributes() {
        return new HashMap(this.zzgi);
    }

    /* access modifiers changed from: package-private */
    public final List<zzt> getSessions() {
        return this.zzck;
    }

    /* synthetic */ Trace(Parcel parcel, boolean z, zzc zzc) {
        this(parcel, z);
    }
}
