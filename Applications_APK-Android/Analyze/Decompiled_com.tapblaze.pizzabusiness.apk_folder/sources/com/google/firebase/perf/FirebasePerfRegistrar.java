package com.google.firebase.perf;

import com.google.firebase.FirebaseApp;
import com.google.firebase.components.Component;
import com.google.firebase.components.ComponentRegistrar;
import com.google.firebase.components.Dependency;
import com.google.firebase.perf.internal.zzd;
import com.google.firebase.platforminfo.LibraryVersionComponent;
import com.google.firebase.remoteconfig.RemoteConfigComponent;
import java.util.Arrays;
import java.util.List;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class FirebasePerfRegistrar implements ComponentRegistrar {
    public List<Component<?>> getComponents() {
        return Arrays.asList(Component.builder(FirebasePerformance.class).add(Dependency.required(FirebaseApp.class)).add(Dependency.required(RemoteConfigComponent.class)).factory(zza.zzae).eagerInDefaultApp().build(), LibraryVersionComponent.create("fire-perf", zzd.VERSION_NAME));
    }
}
