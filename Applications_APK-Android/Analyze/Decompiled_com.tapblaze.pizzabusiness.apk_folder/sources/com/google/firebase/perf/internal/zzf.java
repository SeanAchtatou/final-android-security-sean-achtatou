package com.google.firebase.perf.internal;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.clearcut.ClearcutLogger;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbj;
import com.google.android.gms.internal.p000firebaseperf.zzbx;
import com.google.android.gms.internal.p000firebaseperf.zzca;
import com.google.android.gms.internal.p000firebaseperf.zzce;
import com.google.android.gms.internal.p000firebaseperf.zzcg;
import com.google.android.gms.internal.p000firebaseperf.zzcq;
import com.google.android.gms.internal.p000firebaseperf.zzcv;
import com.google.android.gms.internal.p000firebaseperf.zzda;
import com.google.android.gms.internal.p000firebaseperf.zzdn;
import com.google.android.gms.internal.p000firebaseperf.zzfc;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.perf.FirebasePerformance;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class zzf {
    private static volatile zzf zzcz;
    private zzaf zzab = null;
    private final ExecutorService zzda = new ThreadPoolExecutor(0, 1, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
    private FirebaseApp zzdb;
    private FirebasePerformance zzdc;
    private FirebaseInstanceId zzdd = null;
    private Context zzde;
    private ClearcutLogger zzdf = null;
    private String zzdg;
    private final zzce.zzb zzdh = zzce.zzdm();
    private zzv zzdi = null;
    private zza zzdj = null;
    private boolean zzdk;

    public static zzf zzbs() {
        if (zzcz == null) {
            synchronized (zzf.class) {
                if (zzcz == null) {
                    try {
                        FirebaseApp.getInstance();
                        zzcz = new zzf(null, null, null, null, null, null);
                    } catch (IllegalStateException unused) {
                        return null;
                    }
                }
            }
        }
        return zzcz;
    }

    private zzf(ExecutorService executorService, ClearcutLogger clearcutLogger, zzv zzv, zza zza, FirebaseInstanceId firebaseInstanceId, zzaf zzaf) {
        this.zzda.execute(new zze(this));
    }

    public final void zza(zzdn zzdn, zzcg zzcg) {
        this.zzda.execute(new zzh(this, zzdn, zzcg));
        SessionManager.zzck().zzcm();
    }

    public final void zza(zzcv zzcv, zzcg zzcg) {
        this.zzda.execute(new zzg(this, zzcv, zzcg));
        SessionManager.zzck().zzcm();
    }

    public final void zza(zzcq zzcq, zzcg zzcg) {
        this.zzda.execute(new zzj(this, zzcq, zzcg));
        SessionManager.zzck().zzcm();
    }

    public final void zzc(boolean z) {
        this.zzda.execute(new zzi(this, z));
    }

    /* access modifiers changed from: private */
    public final void zzbt() {
        this.zzdb = FirebaseApp.getInstance();
        this.zzdc = FirebasePerformance.getInstance();
        this.zzde = this.zzdb.getApplicationContext();
        this.zzdg = this.zzdb.getOptions().getApplicationId();
        this.zzdh.zzy(this.zzdg).zza(zzca.zzdc().zzt(this.zzde.getPackageName()).zzu(zzd.VERSION_NAME).zzv(zzf(this.zzde)));
        zzbu();
        zzv zzv = this.zzdi;
        if (zzv == null) {
            zzv = new zzv(this.zzde, 100.0d, 500);
        }
        this.zzdi = zzv;
        zza zza = this.zzdj;
        if (zza == null) {
            zza = zza.zzbf();
        }
        this.zzdj = zza;
        zzaf zzaf = this.zzab;
        if (zzaf == null) {
            zzaf = zzaf.zzl();
        }
        this.zzab = zzaf;
        this.zzab.zzc(this.zzde);
        this.zzdk = zzbx.zzg(this.zzde);
        if (this.zzdf == null) {
            try {
                this.zzdf = ClearcutLogger.anonymousLogger(this.zzde, this.zzab.zzad());
            } catch (SecurityException e) {
                String valueOf = String.valueOf(e.getMessage());
                Log.w("FirebasePerformance", valueOf.length() != 0 ? "Caught SecurityException while init ClearcutLogger: ".concat(valueOf) : new String("Caught SecurityException while init ClearcutLogger: "));
                this.zzdf = null;
            }
        }
    }

    private final void zza(zzda zzda2) {
        if (this.zzdf == null || !zzbv()) {
            return;
        }
        if (!zzda2.zzfe().hasAppInstanceId()) {
            Log.w("FirebasePerformance", "App Instance ID is null or empty, dropping the log");
            return;
        }
        Context context = this.zzde;
        ArrayList arrayList = new ArrayList();
        if (zzda2.zzff()) {
            arrayList.add(new zzm(zzda2.zzfg()));
        }
        if (zzda2.zzfh()) {
            arrayList.add(new zzk(zzda2.zzfi(), context));
        }
        if (zzda2.zzfd()) {
            arrayList.add(new zzc(zzda2.zzfe()));
        }
        if (zzda2.zzfj()) {
            arrayList.add(new zzl(zzda2.zzfk()));
        }
        boolean z = false;
        if (!arrayList.isEmpty()) {
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    z = true;
                    break;
                }
                Object obj = arrayList2.get(i);
                i++;
                if (!((zzq) obj).zzbr()) {
                    break;
                }
            }
        } else {
            Log.d("FirebasePerformance", "No validators found for PerfMetric.");
        }
        if (!z) {
            Log.w("FirebasePerformance", "Unable to process the PerfMetric due to missing or invalid values. See earlier log statements for additional information on the specific missing/invalid values.");
        } else if (!this.zzdi.zzb(zzda2)) {
            if (zzda2.zzfh()) {
                this.zzdj.zzb(zzbj.NETWORK_TRACE_EVENT_RATE_LIMITED.toString(), 1);
            } else if (zzda2.zzff()) {
                this.zzdj.zzb(zzbj.TRACE_EVENT_RATE_LIMITED.toString(), 1);
            }
            if (!this.zzdk) {
                return;
            }
            if (zzda2.zzfh()) {
                String valueOf = String.valueOf(zzda2.zzfi().getUrl());
                Log.i("FirebasePerformance", valueOf.length() != 0 ? "Rate Limited NetworkRequestMetric - ".concat(valueOf) : new String("Rate Limited NetworkRequestMetric - "));
            } else if (zzda2.zzff()) {
                String valueOf2 = String.valueOf(zzda2.zzfg().getName());
                Log.i("FirebasePerformance", valueOf2.length() != 0 ? "Rate Limited TraceMetric - ".concat(valueOf2) : new String("Rate Limited TraceMetric - "));
            }
        } else {
            try {
                this.zzdf.newEvent(zzda2.toByteArray()).log();
            } catch (SecurityException unused) {
            }
        }
    }

    /* access modifiers changed from: private */
    public final void zzb(zzcq zzcq, zzcg zzcg) {
        if (zzbv()) {
            if (this.zzdk) {
                Log.d("FirebasePerformance", String.format("Logging GaugeMetric. Cpu Metrics: %d, Memory Metrics: %d, Has Metadata: %b, Session ID: %s", Integer.valueOf(zzcq.zzea()), Integer.valueOf(zzcq.zzeb()), Boolean.valueOf(zzcq.zzdy()), zzcq.zzdx()));
            }
            zzda.zza zzfl = zzda.zzfl();
            zzbu();
            zzfl.zza(this.zzdh.zzf(zzcg)).zzb(zzcq);
            zza((zzda) ((zzfc) zzfl.zzhp()));
        }
    }

    /* access modifiers changed from: private */
    public final void zzb(zzdn zzdn, zzcg zzcg) {
        Map<String, String> map;
        if (zzbv()) {
            if (this.zzdk) {
                Log.d("FirebasePerformance", String.format("Logging TraceMetric - %s %dms", zzdn.getName(), Long.valueOf(zzdn.getDurationUs() / 1000)));
            }
            zzbu();
            zzda.zza zzfl = zzda.zzfl();
            zzce.zzb zzf = ((zzce.zzb) ((zzfc.zzb) this.zzdh.clone())).zzf(zzcg);
            zzbw();
            FirebasePerformance firebasePerformance = this.zzdc;
            if (firebasePerformance != null) {
                map = firebasePerformance.getAttributes();
            } else {
                map = Collections.emptyMap();
            }
            zza((zzda) ((zzfc) zzfl.zza(zzf.zzb(map)).zzb(zzdn).zzhp()));
        }
    }

    /* access modifiers changed from: private */
    public final void zzb(zzcv zzcv, zzcg zzcg) {
        long j;
        if (zzbv()) {
            if (this.zzdk) {
                long j2 = 0;
                if (!zzcv.zzev()) {
                    j = 0;
                } else {
                    j = zzcv.zzew();
                }
                if (zzcv.zzel()) {
                    j2 = zzcv.zzem();
                }
                Log.d("FirebasePerformance", String.format("Logging NetworkRequestMetric - %s %db %dms,", zzcv.getUrl(), Long.valueOf(j2), Long.valueOf(j / 1000)));
            }
            zzbu();
            zza((zzda) ((zzfc) zzda.zzfl().zza(this.zzdh.zzf(zzcg)).zzd(zzcv).zzhp()));
        }
    }

    public final void zzd(boolean z) {
        this.zzdi.zzc(z);
    }

    private static String zzf(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException unused) {
            return "";
        }
    }

    private final void zzbu() {
        if (!this.zzdh.hasAppInstanceId() && zzbv()) {
            if (this.zzdd == null) {
                this.zzdd = FirebaseInstanceId.getInstance();
            }
            String id = this.zzdd.getId();
            if (id != null && !id.isEmpty()) {
                this.zzdh.zzz(id);
            }
        }
    }

    private final boolean zzbv() {
        zzbw();
        if (this.zzab == null) {
            this.zzab = zzaf.zzl();
        }
        FirebasePerformance firebasePerformance = this.zzdc;
        return firebasePerformance != null && firebasePerformance.isPerformanceCollectionEnabled() && this.zzab.zzp();
    }

    private final void zzbw() {
        if (this.zzdc == null) {
            this.zzdc = this.zzdb != null ? FirebasePerformance.getInstance() : null;
        }
    }
}
