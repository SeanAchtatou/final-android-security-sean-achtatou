package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzbi;
import com.google.android.gms.internal.p000firebaseperf.zzbo;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class RemoteConfigManager {
    private static final RemoteConfigManager zzfb = new RemoteConfigManager();
    private static final long zzfc = TimeUnit.HOURS.toMillis(12);
    private final Executor executor;
    private zzbi zzai;
    private long zzfd;
    private FirebaseRemoteConfig zzfe;

    private RemoteConfigManager() {
        this(new ThreadPoolExecutor(0, 1, 0, TimeUnit.SECONDS, new LinkedBlockingQueue()), null);
    }

    private RemoteConfigManager(Executor executor2, FirebaseRemoteConfig firebaseRemoteConfig) {
        this.zzfd = 0;
        this.executor = executor2;
        this.zzfe = null;
        this.zzai = zzbi.zzco();
    }

    public static RemoteConfigManager zzch() {
        return zzfb;
    }

    public final void zza(FirebaseRemoteConfig firebaseRemoteConfig) {
        this.zzfe = firebaseRemoteConfig;
    }

    public final zzbo<Float> zzd(String str) {
        if (str == null) {
            this.zzai.zzm("The key to get Remote Config float value is null.");
            return zzbo.zzcy();
        }
        FirebaseRemoteConfigValue zzl = zzl(str);
        if (zzl != null) {
            try {
                return zzbo.zzb(Float.valueOf(Double.valueOf(zzl.asDouble()).floatValue()));
            } catch (IllegalArgumentException unused) {
                if (!zzl.asString().isEmpty()) {
                    this.zzai.zzm(String.format("Could not parse value: '%s' for key: '%s'.", zzl.asString(), str));
                }
            }
        }
        return zzbo.zzcy();
    }

    public final zzbo<Long> zze(String str) {
        if (str == null) {
            this.zzai.zzm("The key to get Remote Config long value is null.");
            return zzbo.zzcy();
        }
        FirebaseRemoteConfigValue zzl = zzl(str);
        if (zzl != null) {
            try {
                return zzbo.zzb(Long.valueOf(zzl.asLong()));
            } catch (IllegalArgumentException unused) {
                if (!zzl.asString().isEmpty()) {
                    this.zzai.zzm(String.format("Could not parse value: '%s' for key: '%s'.", zzl.asString(), str));
                }
            }
        }
        return zzbo.zzcy();
    }

    public final zzbo<Boolean> zzb(String str) {
        if (str == null) {
            this.zzai.zzm("The key to get Remote Config boolean value is null.");
            return zzbo.zzcy();
        }
        FirebaseRemoteConfigValue zzl = zzl(str);
        if (zzl != null) {
            try {
                return zzbo.zzb(Boolean.valueOf(zzl.asBoolean()));
            } catch (IllegalArgumentException unused) {
                if (!zzl.asString().isEmpty()) {
                    this.zzai.zzm(String.format("Could not parse value: '%s' for key: '%s'.", zzl.asString(), str));
                }
            }
        }
        return zzbo.zzcy();
    }

    public final zzbo<String> zzc(String str) {
        if (str == null) {
            this.zzai.zzm("The key to get Remote Config String value is null.");
            return zzbo.zzcy();
        }
        FirebaseRemoteConfigValue zzl = zzl(str);
        if (zzl != null) {
            return zzbo.zzb(zzl.asString());
        }
        return zzbo.zzcy();
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> T zza(java.lang.String r8, T r9) {
        /*
            r7 = this;
            com.google.firebase.remoteconfig.FirebaseRemoteConfigValue r0 = r7.zzl(r8)
            if (r0 == 0) goto L_0x007e
            r1 = 1
            r2 = 0
            boolean r3 = r9 instanceof java.lang.Boolean     // Catch:{ IllegalArgumentException -> 0x005d }
            if (r3 == 0) goto L_0x0016
            boolean r3 = r0.asBoolean()     // Catch:{ IllegalArgumentException -> 0x005d }
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x005d }
            goto L_0x007e
        L_0x0016:
            boolean r3 = r9 instanceof java.lang.Float     // Catch:{ IllegalArgumentException -> 0x005d }
            if (r3 == 0) goto L_0x002b
            double r3 = r0.asDouble()     // Catch:{ IllegalArgumentException -> 0x005d }
            java.lang.Double r3 = java.lang.Double.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x005d }
            float r3 = r3.floatValue()     // Catch:{ IllegalArgumentException -> 0x005d }
            java.lang.Float r9 = java.lang.Float.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x005d }
            goto L_0x007e
        L_0x002b:
            boolean r3 = r9 instanceof java.lang.Long     // Catch:{ IllegalArgumentException -> 0x005d }
            if (r3 != 0) goto L_0x0054
            boolean r3 = r9 instanceof java.lang.Integer     // Catch:{ IllegalArgumentException -> 0x005d }
            if (r3 == 0) goto L_0x0034
            goto L_0x0054
        L_0x0034:
            boolean r3 = r9 instanceof java.lang.String     // Catch:{ IllegalArgumentException -> 0x005d }
            if (r3 == 0) goto L_0x003d
            java.lang.String r9 = r0.asString()     // Catch:{ IllegalArgumentException -> 0x005d }
            goto L_0x007e
        L_0x003d:
            java.lang.String r3 = r0.asString()     // Catch:{ IllegalArgumentException -> 0x005d }
            com.google.android.gms.internal.firebase-perf.zzbi r4 = r7.zzai     // Catch:{ IllegalArgumentException -> 0x0052 }
            java.lang.String r5 = "No matching type found for the defaultValue: '%s', using String."
            java.lang.Object[] r6 = new java.lang.Object[r1]     // Catch:{ IllegalArgumentException -> 0x0052 }
            r6[r2] = r9     // Catch:{ IllegalArgumentException -> 0x0052 }
            java.lang.String r9 = java.lang.String.format(r5, r6)     // Catch:{ IllegalArgumentException -> 0x0052 }
            r4.zzm(r9)     // Catch:{ IllegalArgumentException -> 0x0052 }
            r9 = r3
            goto L_0x007e
        L_0x0052:
            r9 = r3
            goto L_0x005e
        L_0x0054:
            long r3 = r0.asLong()     // Catch:{ IllegalArgumentException -> 0x005d }
            java.lang.Long r9 = java.lang.Long.valueOf(r3)     // Catch:{ IllegalArgumentException -> 0x005d }
            goto L_0x007e
        L_0x005d:
        L_0x005e:
            java.lang.String r3 = r0.asString()
            boolean r3 = r3.isEmpty()
            if (r3 != 0) goto L_0x007e
            com.google.android.gms.internal.firebase-perf.zzbi r3 = r7.zzai
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]
            java.lang.String r0 = r0.asString()
            r4[r2] = r0
            r4[r1] = r8
            java.lang.String r8 = "Could not parse value: '%s' for key: '%s'."
            java.lang.String r8 = java.lang.String.format(r8, r4)
            r3.zzm(r8)
        L_0x007e:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.perf.internal.RemoteConfigManager.zza(java.lang.String, java.lang.Object):java.lang.Object");
    }

    private final FirebaseRemoteConfigValue zzl(String str) {
        if (zzcj()) {
            if (System.currentTimeMillis() - this.zzfd > zzfc) {
                this.zzfd = System.currentTimeMillis();
                this.zzfe.fetchAndActivate().addOnFailureListener(this.executor, new zzw(this));
            }
        }
        if (!zzcj()) {
            return null;
        }
        FirebaseRemoteConfigValue value = this.zzfe.getValue(str);
        if (value.getSource() != 2) {
            return null;
        }
        this.zzai.zzm(String.format("Fetched value: '%s' for key: '%s' from Firebase Remote Config.", value.asString(), str));
        return value;
    }

    public final boolean zzci() {
        FirebaseRemoteConfig firebaseRemoteConfig = this.zzfe;
        if (firebaseRemoteConfig == null || firebaseRemoteConfig.getInfo().getLastFetchStatus() == 1) {
            return true;
        }
        return false;
    }

    private final boolean zzcj() {
        return this.zzfe != null;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(Exception exc) {
        this.zzfd = 0;
    }
}
