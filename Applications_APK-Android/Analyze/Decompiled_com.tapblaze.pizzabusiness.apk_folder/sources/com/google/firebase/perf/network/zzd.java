package com.google.firebase.perf.network;

import android.util.Log;
import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.security.Permission;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzd {
    private final zzbt zzfz;
    private final zzbg zzgo;
    private long zzgq = -1;
    private final HttpURLConnection zzgv;
    private long zzgw = -1;

    public zzd(HttpURLConnection httpURLConnection, zzbt zzbt, zzbg zzbg) {
        this.zzgv = httpURLConnection;
        this.zzgo = zzbg;
        this.zzfz = zzbt;
        this.zzgo.zzf(this.zzgv.getURL().toString());
    }

    public final void connect() throws IOException {
        if (this.zzgw == -1) {
            this.zzfz.reset();
            this.zzgw = this.zzfz.zzcz();
            this.zzgo.zzk(this.zzgw);
        }
        try {
            this.zzgv.connect();
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final void disconnect() {
        this.zzgo.zzn(this.zzfz.zzda());
        this.zzgo.zzbo();
        this.zzgv.disconnect();
    }

    public final Object getContent() throws IOException {
        zzcw();
        this.zzgo.zzc(this.zzgv.getResponseCode());
        try {
            Object content = this.zzgv.getContent();
            if (content instanceof InputStream) {
                this.zzgo.zzh(this.zzgv.getContentType());
                return new zza((InputStream) content, this.zzgo, this.zzfz);
            }
            this.zzgo.zzh(this.zzgv.getContentType());
            this.zzgo.zzo((long) this.zzgv.getContentLength());
            this.zzgo.zzn(this.zzfz.zzda());
            this.zzgo.zzbo();
            return content;
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final Object getContent(Class[] clsArr) throws IOException {
        zzcw();
        this.zzgo.zzc(this.zzgv.getResponseCode());
        try {
            Object content = this.zzgv.getContent(clsArr);
            if (content instanceof InputStream) {
                this.zzgo.zzh(this.zzgv.getContentType());
                return new zza((InputStream) content, this.zzgo, this.zzfz);
            }
            this.zzgo.zzh(this.zzgv.getContentType());
            this.zzgo.zzo((long) this.zzgv.getContentLength());
            this.zzgo.zzn(this.zzfz.zzda());
            this.zzgo.zzbo();
            return content;
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final InputStream getInputStream() throws IOException {
        zzcw();
        this.zzgo.zzc(this.zzgv.getResponseCode());
        this.zzgo.zzh(this.zzgv.getContentType());
        try {
            return new zza(this.zzgv.getInputStream(), this.zzgo, this.zzfz);
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final long getLastModified() {
        zzcw();
        return this.zzgv.getLastModified();
    }

    public final OutputStream getOutputStream() throws IOException {
        try {
            return new zzc(this.zzgv.getOutputStream(), this.zzgo, this.zzfz);
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final Permission getPermission() throws IOException {
        try {
            return this.zzgv.getPermission();
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final int getResponseCode() throws IOException {
        zzcw();
        if (this.zzgq == -1) {
            this.zzgq = this.zzfz.zzda();
            this.zzgo.zzm(this.zzgq);
        }
        try {
            int responseCode = this.zzgv.getResponseCode();
            this.zzgo.zzc(responseCode);
            return responseCode;
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final String getResponseMessage() throws IOException {
        zzcw();
        if (this.zzgq == -1) {
            this.zzgq = this.zzfz.zzda();
            this.zzgo.zzm(this.zzgq);
        }
        try {
            String responseMessage = this.zzgv.getResponseMessage();
            this.zzgo.zzc(this.zzgv.getResponseCode());
            return responseMessage;
        } catch (IOException e) {
            this.zzgo.zzn(this.zzfz.zzda());
            zzh.zza(this.zzgo);
            throw e;
        }
    }

    public final long getExpiration() {
        zzcw();
        return this.zzgv.getExpiration();
    }

    public final String getHeaderField(int i) {
        zzcw();
        return this.zzgv.getHeaderField(i);
    }

    public final String getHeaderField(String str) {
        zzcw();
        return this.zzgv.getHeaderField(str);
    }

    public final long getHeaderFieldDate(String str, long j) {
        zzcw();
        return this.zzgv.getHeaderFieldDate(str, j);
    }

    public final int getHeaderFieldInt(String str, int i) {
        zzcw();
        return this.zzgv.getHeaderFieldInt(str, i);
    }

    public final long getHeaderFieldLong(String str, long j) {
        zzcw();
        return this.zzgv.getHeaderFieldLong(str, j);
    }

    public final String getHeaderFieldKey(int i) {
        zzcw();
        return this.zzgv.getHeaderFieldKey(i);
    }

    public final Map<String, List<String>> getHeaderFields() {
        zzcw();
        return this.zzgv.getHeaderFields();
    }

    public final String getContentEncoding() {
        zzcw();
        return this.zzgv.getContentEncoding();
    }

    public final int getContentLength() {
        zzcw();
        return this.zzgv.getContentLength();
    }

    public final long getContentLengthLong() {
        zzcw();
        return this.zzgv.getContentLengthLong();
    }

    public final String getContentType() {
        zzcw();
        return this.zzgv.getContentType();
    }

    public final long getDate() {
        zzcw();
        return this.zzgv.getDate();
    }

    public final void addRequestProperty(String str, String str2) {
        this.zzgv.addRequestProperty(str, str2);
    }

    public final boolean equals(Object obj) {
        return this.zzgv.equals(obj);
    }

    public final boolean getAllowUserInteraction() {
        return this.zzgv.getAllowUserInteraction();
    }

    public final int getConnectTimeout() {
        return this.zzgv.getConnectTimeout();
    }

    public final boolean getDefaultUseCaches() {
        return this.zzgv.getDefaultUseCaches();
    }

    public final boolean getDoInput() {
        return this.zzgv.getDoInput();
    }

    public final boolean getDoOutput() {
        return this.zzgv.getDoOutput();
    }

    public final InputStream getErrorStream() {
        zzcw();
        try {
            this.zzgo.zzc(this.zzgv.getResponseCode());
        } catch (IOException unused) {
            Log.d("FirebasePerformance", "IOException thrown trying to obtain the response code");
        }
        InputStream errorStream = this.zzgv.getErrorStream();
        return errorStream != null ? new zza(errorStream, this.zzgo, this.zzfz) : errorStream;
    }

    public final long getIfModifiedSince() {
        return this.zzgv.getIfModifiedSince();
    }

    public final boolean getInstanceFollowRedirects() {
        return this.zzgv.getInstanceFollowRedirects();
    }

    public final int getReadTimeout() {
        return this.zzgv.getReadTimeout();
    }

    public final String getRequestMethod() {
        return this.zzgv.getRequestMethod();
    }

    public final Map<String, List<String>> getRequestProperties() {
        return this.zzgv.getRequestProperties();
    }

    public final String getRequestProperty(String str) {
        return this.zzgv.getRequestProperty(str);
    }

    public final URL getURL() {
        return this.zzgv.getURL();
    }

    public final boolean getUseCaches() {
        return this.zzgv.getUseCaches();
    }

    public final int hashCode() {
        return this.zzgv.hashCode();
    }

    public final void setAllowUserInteraction(boolean z) {
        this.zzgv.setAllowUserInteraction(z);
    }

    public final void setChunkedStreamingMode(int i) {
        this.zzgv.setChunkedStreamingMode(i);
    }

    public final void setConnectTimeout(int i) {
        this.zzgv.setConnectTimeout(i);
    }

    public final void setDefaultUseCaches(boolean z) {
        this.zzgv.setDefaultUseCaches(z);
    }

    public final void setDoInput(boolean z) {
        this.zzgv.setDoInput(z);
    }

    public final void setDoOutput(boolean z) {
        this.zzgv.setDoOutput(z);
    }

    public final void setFixedLengthStreamingMode(int i) {
        this.zzgv.setFixedLengthStreamingMode(i);
    }

    public final void setFixedLengthStreamingMode(long j) {
        this.zzgv.setFixedLengthStreamingMode(j);
    }

    public final void setIfModifiedSince(long j) {
        this.zzgv.setIfModifiedSince(j);
    }

    public final void setInstanceFollowRedirects(boolean z) {
        this.zzgv.setInstanceFollowRedirects(z);
    }

    public final void setReadTimeout(int i) {
        this.zzgv.setReadTimeout(i);
    }

    public final void setRequestMethod(String str) throws ProtocolException {
        this.zzgv.setRequestMethod(str);
    }

    public final void setRequestProperty(String str, String str2) {
        this.zzgv.setRequestProperty(str, str2);
    }

    public final void setUseCaches(boolean z) {
        this.zzgv.setUseCaches(z);
    }

    public final String toString() {
        return this.zzgv.toString();
    }

    public final boolean usingProxy() {
        return this.zzgv.usingProxy();
    }

    private final void zzcw() {
        if (this.zzgw == -1) {
            this.zzfz.reset();
            this.zzgw = this.zzfz.zzcz();
            this.zzgo.zzk(this.zzgw);
        }
        String requestMethod = this.zzgv.getRequestMethod();
        if (requestMethod != null) {
            this.zzgo.zzg(requestMethod);
        } else if (this.zzgv.getDoOutput()) {
            this.zzgo.zzg("POST");
        } else {
            this.zzgo.zzg("GET");
        }
    }
}
