package com.google.firebase.perf.metrics;

import com.google.android.gms.internal.p000firebaseperf.zzde;
import com.google.android.gms.internal.p000firebaseperf.zzdn;
import com.google.android.gms.internal.p000firebaseperf.zzfc;
import com.google.firebase.perf.internal.zzt;
import java.util.Arrays;
import java.util.List;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
final class zzd {
    private final Trace zzgm;

    zzd(Trace trace) {
        this.zzgm = trace;
    }

    /* access modifiers changed from: package-private */
    public final zzdn zzcv() {
        zzdn.zzb zzap = zzdn.zzfx().zzah(this.zzgm.getName()).zzao(this.zzgm.zzcs().zzcz()).zzap(this.zzgm.zzcs().zzk(this.zzgm.zzct()));
        for (zza next : this.zzgm.zzcr().values()) {
            zzap.zzc(next.getName(), next.getCount());
        }
        List<Trace> zzcu = this.zzgm.zzcu();
        if (!zzcu.isEmpty()) {
            for (Trace zzd : zzcu) {
                zzap.zzf(new zzd(zzd).zzcv());
            }
        }
        zzap.zze(this.zzgm.getAttributes());
        zzde[] zza = zzt.zza(this.zzgm.getSessions());
        if (zza != null) {
            zzap.zze(Arrays.asList(zza));
        }
        return (zzdn) ((zzfc) zzap.zzhp());
    }
}
