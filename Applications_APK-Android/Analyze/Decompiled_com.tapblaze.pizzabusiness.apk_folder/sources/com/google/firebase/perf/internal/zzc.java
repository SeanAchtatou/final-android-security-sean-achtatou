package com.google.firebase.perf.internal;

import com.google.android.gms.internal.p000firebaseperf.zzce;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public final class zzc extends zzq {
    private final zzce zzcv;

    zzc(zzce zzce) {
        this.zzcv = zzce;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006c A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzbr() {
        /*
            r4 = this;
            com.google.android.gms.internal.firebase-perf.zzce r0 = r4.zzcv
            r1 = 1
            r2 = 0
            java.lang.String r3 = "FirebasePerformance"
            if (r0 != 0) goto L_0x000f
            java.lang.String r0 = "ApplicationInfo is null"
            android.util.Log.w(r3, r0)
        L_0x000d:
            r0 = 0
            goto L_0x0064
        L_0x000f:
            boolean r0 = r0.zzdh()
            if (r0 != 0) goto L_0x001b
            java.lang.String r0 = "GoogleAppId is null"
            android.util.Log.w(r3, r0)
            goto L_0x000d
        L_0x001b:
            com.google.android.gms.internal.firebase-perf.zzce r0 = r4.zzcv
            boolean r0 = r0.hasAppInstanceId()
            if (r0 != 0) goto L_0x0029
            java.lang.String r0 = "AppInstanceId is null"
            android.util.Log.w(r3, r0)
            goto L_0x000d
        L_0x0029:
            com.google.android.gms.internal.firebase-perf.zzce r0 = r4.zzcv
            boolean r0 = r0.zzdk()
            if (r0 != 0) goto L_0x0037
            java.lang.String r0 = "ApplicationProcessState is null"
            android.util.Log.w(r3, r0)
            goto L_0x000d
        L_0x0037:
            com.google.android.gms.internal.firebase-perf.zzce r0 = r4.zzcv
            boolean r0 = r0.zzdi()
            if (r0 == 0) goto L_0x0063
            com.google.android.gms.internal.firebase-perf.zzce r0 = r4.zzcv
            com.google.android.gms.internal.firebase-perf.zzca r0 = r0.zzdj()
            boolean r0 = r0.hasPackageName()
            if (r0 != 0) goto L_0x0051
            java.lang.String r0 = "AndroidAppInfo.packageName is null"
            android.util.Log.w(r3, r0)
            goto L_0x000d
        L_0x0051:
            com.google.android.gms.internal.firebase-perf.zzce r0 = r4.zzcv
            com.google.android.gms.internal.firebase-perf.zzca r0 = r0.zzdj()
            boolean r0 = r0.hasSdkVersion()
            if (r0 != 0) goto L_0x0063
            java.lang.String r0 = "AndroidAppInfo.sdkVersion is null"
            android.util.Log.w(r3, r0)
            goto L_0x000d
        L_0x0063:
            r0 = 1
        L_0x0064:
            if (r0 != 0) goto L_0x006c
            java.lang.String r0 = "ApplicationInfo is invalid"
            android.util.Log.w(r3, r0)
            return r2
        L_0x006c:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.perf.internal.zzc.zzbr():boolean");
    }
}
