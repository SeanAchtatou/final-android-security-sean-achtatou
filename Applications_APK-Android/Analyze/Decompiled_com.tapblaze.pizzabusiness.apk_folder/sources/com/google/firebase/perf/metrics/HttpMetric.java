package com.google.firebase.perf.metrics;

import android.util.Log;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.firebase.perf.internal.zzf;
import com.google.firebase.perf.internal.zzq;
import java.net.URL;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class HttpMetric {
    private zzbg zzfy;
    private zzbt zzfz;
    private final Map<String, String> zzga;
    private boolean zzgb;
    private boolean zzgc;

    public HttpMetric(String str, String str2, zzf zzf, zzbt zzbt) {
        this.zzgb = false;
        this.zzgc = false;
        this.zzga = new ConcurrentHashMap();
        this.zzfz = zzbt;
        this.zzfy = zzbg.zzb(zzf).zzf(str).zzg(str2);
        this.zzfy.zzbk();
        if (!zzaf.zzl().zzm()) {
            Log.i("FirebasePerformance", String.format("HttpMetric feature is disabled. URL %s", str));
            this.zzgc = true;
        }
    }

    public HttpMetric(URL url, String str, zzf zzf, zzbt zzbt) {
        this(url.toString(), str, zzf, zzbt);
    }

    public void setHttpResponseCode(int i) {
        this.zzfy.zzc(i);
    }

    public void setRequestPayloadSize(long j) {
        this.zzfy.zzj(j);
    }

    public void setResponsePayloadSize(long j) {
        this.zzfy.zzo(j);
    }

    public void setResponseContentType(String str) {
        this.zzfy.zzh(str);
    }

    public void start() {
        this.zzfz.reset();
        this.zzfy.zzk(this.zzfz.zzcz());
    }

    public void stop() {
        if (!this.zzgc) {
            this.zzfy.zzn(this.zzfz.zzda()).zza(this.zzga).zzbo();
            this.zzgb = true;
        }
    }

    public void putAttribute(String str, String str2) {
        boolean z = false;
        try {
            str = str.trim();
            str2 = str2.trim();
            if (this.zzgb) {
                throw new IllegalArgumentException("HttpMetric has been logged already so unable to modify attributes");
            } else if (str == null || str2 == null) {
                throw new IllegalArgumentException("Attribute must not have null key or value.");
            } else {
                if (!this.zzga.containsKey(str)) {
                    if (this.zzga.size() >= 5) {
                        throw new IllegalArgumentException(String.format(Locale.US, "Exceeds max limit of number of attributes - %d", 5));
                    }
                }
                String zza = zzq.zza(new AbstractMap.SimpleEntry(str, str2));
                if (zza == null) {
                    z = true;
                    if (z) {
                        this.zzga.put(str, str2);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException(zza);
            }
        } catch (Exception e) {
            Log.e("FirebasePerformance", String.format("Can not set attribute %s with value %s (%s)", str, str2, e.getMessage()));
        }
    }

    public void removeAttribute(String str) {
        if (this.zzgb) {
            Log.e("FirebasePerformance", "Can't remove a attribute from a HttpMetric that's stopped.");
        } else {
            this.zzga.remove(str);
        }
    }

    public String getAttribute(String str) {
        return this.zzga.get(str);
    }

    public Map<String, String> getAttributes() {
        return new HashMap(this.zzga);
    }
}
