package com.google.firebase.perf.network;

import com.google.android.gms.internal.p000firebaseperf.zzbg;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.firebase.perf.internal.zzf;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class FirebasePerfOkHttpClient {
    private FirebasePerfOkHttpClient() {
    }

    public static Response execute(Call call) throws IOException {
        zzbg zzb = zzbg.zzb(zzf.zzbs());
        zzbt zzbt = new zzbt();
        long zzcz = zzbt.zzcz();
        try {
            Response execute = call.execute();
            zza(execute, zzb, zzcz, zzbt.zzda());
            return execute;
        } catch (IOException e) {
            Request request = call.request();
            if (request != null) {
                HttpUrl url = request.url();
                if (url != null) {
                    zzb.zzf(url.url().toString());
                }
                if (request.method() != null) {
                    zzb.zzg(request.method());
                }
            }
            zzb.zzk(zzcz);
            zzb.zzn(zzbt.zzda());
            zzh.zza(zzb);
            throw e;
        }
    }

    public static void enqueue(Call call, Callback callback) {
        zzbt zzbt = new zzbt();
        Callback callback2 = callback;
        call.enqueue(new zzf(callback2, zzf.zzbs(), zzbt, zzbt.zzcz()));
    }

    static void zza(Response response, zzbg zzbg, long j, long j2) throws IOException {
        Request request = response.request();
        if (request != null) {
            zzbg.zzf(request.url().url().toString());
            zzbg.zzg(request.method());
            if (request.body() != null) {
                long contentLength = request.body().contentLength();
                if (contentLength != -1) {
                    zzbg.zzj(contentLength);
                }
            }
            ResponseBody body = response.body();
            if (body != null) {
                long contentLength2 = body.contentLength();
                if (contentLength2 != -1) {
                    zzbg.zzo(contentLength2);
                }
                MediaType contentType = body.contentType();
                if (contentType != null) {
                    zzbg.zzh(contentType.toString());
                }
            }
            zzbg.zzc(response.code());
            zzbg.zzk(j);
            zzbg.zzn(j2);
            zzbg.zzbo();
        }
    }
}
