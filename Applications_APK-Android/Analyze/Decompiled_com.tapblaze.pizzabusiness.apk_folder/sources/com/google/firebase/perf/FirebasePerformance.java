package com.google.firebase.perf;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.internal.p000firebaseperf.zzaf;
import com.google.android.gms.internal.p000firebaseperf.zzbl;
import com.google.android.gms.internal.p000firebaseperf.zzbt;
import com.google.firebase.FirebaseApp;
import com.google.firebase.perf.internal.GaugeManager;
import com.google.firebase.perf.internal.RemoteConfigManager;
import com.google.firebase.perf.internal.zzf;
import com.google.firebase.perf.metrics.HttpMetric;
import com.google.firebase.perf.metrics.Trace;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
public class FirebasePerformance {
    public static final int MAX_TRACE_NAME_LENGTH = 100;
    private static volatile FirebasePerformance zzz;
    private final Map<String, String> zzaa;
    private final zzaf zzab;
    private final zzbl zzac;
    private Boolean zzad;

    @Retention(RetentionPolicy.SOURCE)
    /* compiled from: com.google.firebase:firebase-perf@@19.0.5 */
    public @interface HttpMethod {
        public static final String CONNECT = "CONNECT";
        public static final String DELETE = "DELETE";
        public static final String GET = "GET";
        public static final String HEAD = "HEAD";
        public static final String OPTIONS = "OPTIONS";
        public static final String PATCH = "PATCH";
        public static final String POST = "POST";
        public static final String PUT = "PUT";
        public static final String TRACE = "TRACE";
    }

    public static FirebasePerformance getInstance() {
        Class<FirebasePerformance> cls = FirebasePerformance.class;
        if (zzz == null) {
            synchronized (cls) {
                if (zzz == null) {
                    zzz = (FirebasePerformance) FirebaseApp.getInstance().get(cls);
                }
            }
        }
        return zzz;
    }

    FirebasePerformance(FirebaseApp firebaseApp, FirebaseRemoteConfig firebaseRemoteConfig) {
        this(firebaseApp, firebaseRemoteConfig, RemoteConfigManager.zzch(), zzaf.zzl(), GaugeManager.zzbx());
    }

    private FirebasePerformance(FirebaseApp firebaseApp, FirebaseRemoteConfig firebaseRemoteConfig, RemoteConfigManager remoteConfigManager, zzaf zzaf, GaugeManager gaugeManager) {
        this.zzaa = new ConcurrentHashMap();
        this.zzad = null;
        if (firebaseApp == null) {
            this.zzad = false;
            this.zzab = zzaf;
            this.zzac = new zzbl(new Bundle());
            return;
        }
        Context applicationContext = firebaseApp.getApplicationContext();
        this.zzac = zza(applicationContext);
        remoteConfigManager.zza(firebaseRemoteConfig);
        this.zzab = zzaf;
        this.zzab.zza(this.zzac);
        this.zzab.zzc(applicationContext);
        gaugeManager.zzc(applicationContext);
        this.zzad = zzaf.zzn();
    }

    public static Trace startTrace(String str) {
        Trace zzn = Trace.zzn(str);
        zzn.start();
        return zzn;
    }

    public void setPerformanceCollectionEnabled(boolean z) {
        try {
            FirebaseApp.getInstance();
            if (this.zzab.zzo().booleanValue()) {
                Log.i("FirebasePerformance", "Firebase Performance is permanently disabled");
                return;
            }
            this.zzad = Boolean.valueOf(z);
            this.zzab.zza(z);
            if (z) {
                Log.i("FirebasePerformance", "Firebase Performance is Enabled");
            } else {
                Log.i("FirebasePerformance", "Firebase Performance is Disabled");
            }
        } catch (IllegalStateException unused) {
        }
    }

    public boolean isPerformanceCollectionEnabled() {
        Boolean bool = this.zzad;
        if (bool != null) {
            return bool.booleanValue();
        }
        return FirebaseApp.getInstance().isDataCollectionDefaultEnabled();
    }

    public final Map<String, String> getAttributes() {
        return new HashMap(this.zzaa);
    }

    public Trace newTrace(String str) {
        return Trace.zzn(str);
    }

    public HttpMetric newHttpMetric(String str, String str2) {
        return new HttpMetric(str, str2, zzf.zzbs(), new zzbt());
    }

    public HttpMetric newHttpMetric(URL url, String str) {
        return new HttpMetric(url, str, zzf.zzbs(), new zzbt());
    }

    private static zzbl zza(Context context) {
        Bundle bundle;
        try {
            bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (PackageManager.NameNotFoundException | NullPointerException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.d("isEnabled", valueOf.length() != 0 ? "No perf enable meta data found ".concat(valueOf) : new String("No perf enable meta data found "));
            bundle = null;
        }
        return bundle != null ? new zzbl(bundle) : new zzbl();
    }
}
