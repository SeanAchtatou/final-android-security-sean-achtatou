package com.google.firebase.analytics;

import android.os.Bundle;
import com.google.android.gms.internal.measurement.zzx;
import com.google.android.gms.measurement.internal.zzhf;
import com.google.android.gms.measurement.internal.zzhi;
import com.google.android.gms.measurement.internal.zzif;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-api@@17.2.2 */
final class zzb implements zzif {
    private final /* synthetic */ zzx zza;

    zzb(zzx zzx) {
        this.zza = zzx;
    }

    public final void zza(String str, String str2, Bundle bundle) {
        this.zza.zza(str, str2, bundle);
    }

    public final void zza(String str, String str2, Bundle bundle, long j) {
        this.zza.zza(str, str2, bundle, j);
    }

    public final void zza(String str, String str2, Object obj) {
        this.zza.zza(str, str2, obj);
    }

    public final void zza(boolean z) {
        this.zza.zza(z);
    }

    public final void zzb(boolean z) {
        this.zza.zzb(z);
    }

    public final Map<String, Object> zza(String str, String str2, boolean z) {
        return this.zza.zza(str, str2, z);
    }

    public final void zza(zzhf zzhf) {
        this.zza.zza(zzhf);
    }

    public final void zza(zzhi zzhi) {
        this.zza.zza(zzhi);
    }

    public final void zzb(zzhi zzhi) {
        this.zza.zzb(zzhi);
    }

    public final String zza() {
        return this.zza.zzf();
    }

    public final String zzb() {
        return this.zza.zzg();
    }

    public final String zzc() {
        return this.zza.zzd();
    }

    public final String zzd() {
        return this.zza.zzc();
    }

    public final long zze() {
        return this.zza.zze();
    }

    public final void zza(String str) {
        this.zza.zzb(str);
    }

    public final void zzb(String str) {
        this.zza.zzc(str);
    }

    public final void zza(Bundle bundle) {
        this.zza.zza(bundle);
    }

    public final void zzb(String str, String str2, Bundle bundle) {
        this.zza.zzb(str, str2, bundle);
    }

    public final List<Bundle> zza(String str, String str2) {
        return this.zza.zzb(str, str2);
    }

    public final int zzc(String str) {
        return this.zza.zzd(str);
    }

    public final Object zza(int i) {
        return this.zza.zza(i);
    }
}
