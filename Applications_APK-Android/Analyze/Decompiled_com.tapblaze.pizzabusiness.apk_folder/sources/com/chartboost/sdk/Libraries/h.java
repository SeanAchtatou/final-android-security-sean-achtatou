package com.chartboost.sdk.Libraries;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.chartboost.sdk.e;
import java.io.File;
import org.json.JSONObject;

public class h {
    private a a;
    private final e b;
    private String c;
    private float d = 1.0f;

    public static class a {
        private int a;
        private final String b;
        private final File c;
        private Bitmap d;
        private final f e;
        private int f = -1;
        private int g = -1;

        public a(String str, File file, f fVar) {
            this.c = file;
            this.b = str;
            this.d = null;
            this.a = 1;
            this.e = fVar;
        }

        public Bitmap a() {
            if (this.d == null) {
                b();
            }
            return this.d;
        }

        public void b() {
            if (this.d == null) {
                CBLogging.a("MemoryBitmap", "Loading image '" + this.b + "' from cache");
                byte[] a2 = this.e.a(this.c);
                if (a2 == null) {
                    CBLogging.b("MemoryBitmap", "decode() - bitmap not found");
                    return;
                }
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(a2, 0, a2.length, options);
                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inJustDecodeBounds = false;
                options2.inDither = false;
                options2.inPurgeable = true;
                options2.inInputShareable = true;
                options2.inTempStorage = new byte[32768];
                options2.inSampleSize = 1;
                while (true) {
                    if (options2.inSampleSize >= 32) {
                        break;
                    }
                    try {
                        this.d = BitmapFactory.decodeByteArray(a2, 0, a2.length, options2);
                        break;
                    } catch (OutOfMemoryError e2) {
                        CBLogging.a("MemoryBitmap", "OutOfMemoryError suppressed - trying larger sample size", e2);
                        options2.inSampleSize *= 2;
                    } catch (Exception e3) {
                        CBLogging.a("MemoryBitmap", "Exception raised decoding bitmap", e3);
                        com.chartboost.sdk.Tracking.a.a(getClass(), "decodeByteArray", e3);
                    }
                }
                this.a = options2.inSampleSize;
            }
            return;
            if (this.d == null) {
                this.c.delete();
                throw new RuntimeException("Unable to decode " + this.b);
            }
            this.a = options2.inSampleSize;
        }

        public int c() {
            return this.a;
        }

        public void d() {
            try {
                if (this.d != null && !this.d.isRecycled()) {
                    this.d.recycle();
                }
            } catch (Exception e2) {
                com.chartboost.sdk.Tracking.a.a(getClass(), "recycle", e2);
            }
            this.d = null;
        }

        public int e() {
            Bitmap bitmap = this.d;
            if (bitmap != null) {
                return bitmap.getWidth();
            }
            int i = this.f;
            if (i >= 0) {
                return i;
            }
            g();
            return this.f;
        }

        public int f() {
            Bitmap bitmap = this.d;
            if (bitmap != null) {
                return bitmap.getHeight();
            }
            int i = this.g;
            if (i >= 0) {
                return i;
            }
            g();
            return this.g;
        }

        private void g() {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(this.c.getAbsolutePath(), options);
                this.f = options.outWidth;
                this.g = options.outHeight;
            } catch (Exception e2) {
                CBLogging.a("MemoryBitmap", "Error decoding file size", e2);
                com.chartboost.sdk.Tracking.a.a(getClass(), "decodeSize", e2);
            }
        }
    }

    public h(e eVar) {
        this.b = eVar;
    }

    public int a() {
        return this.a.e() * this.a.c();
    }

    public int b() {
        return this.a.f() * this.a.c();
    }

    public boolean a(String str) {
        return a(this.b.g(), str);
    }

    public boolean a(JSONObject jSONObject, String str) {
        JSONObject a2 = e.a(jSONObject, str);
        this.c = str;
        if (a2 == null) {
            return true;
        }
        String optString = a2.optString("url");
        this.d = (float) a2.optDouble("scale", 1.0d);
        if (optString.isEmpty()) {
            return true;
        }
        String optString2 = a2.optString("checksum");
        if (optString2.isEmpty()) {
            return false;
        }
        this.a = this.b.e.j.a(optString2);
        if (this.a != null) {
            return true;
        }
        return false;
    }

    public void c() {
        a aVar = this.a;
        if (aVar != null) {
            aVar.d();
        }
    }

    public boolean d() {
        return this.a != null;
    }

    public Bitmap e() {
        a aVar = this.a;
        if (aVar != null) {
            return aVar.a();
        }
        return null;
    }

    public float f() {
        return this.d;
    }
}
