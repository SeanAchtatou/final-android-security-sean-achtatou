package com.chartboost.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.j;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.impl.ai;
import com.chartboost.sdk.impl.aq;
import com.chartboost.sdk.impl.bc;
import com.chartboost.sdk.impl.s;
import java.util.HashSet;

public class c {
    final h a;
    final Handler b;
    public final d c;
    j d;
    CBImpressionActivity e = null;
    com.chartboost.sdk.Model.c f = null;
    Runnable g;
    final Application.ActivityLifecycleCallbacks h;
    private final ai i;
    private final com.chartboost.sdk.Tracking.a j;
    private boolean k = false;
    private final HashSet<Integer> l = new HashSet<>();
    private j m;

    private void b(j jVar, boolean z) {
    }

    public c(Activity activity, ai aiVar, h hVar, com.chartboost.sdk.Tracking.a aVar, Handler handler, d dVar) {
        this.i = aiVar;
        this.a = hVar;
        this.j = aVar;
        this.b = handler;
        this.c = dVar;
        this.d = a(activity);
        aq.a("CBUIManager.assignHostActivityRef", this.d);
        this.g = new b();
        if (s.a().a(14)) {
            this.h = new a();
        } else {
            this.h = null;
        }
    }

    /* access modifiers changed from: package-private */
    public j a(Activity activity) {
        j jVar = this.m;
        if (jVar == null || jVar.a != activity.hashCode()) {
            this.m = new j(activity);
        }
        return this.m;
    }

    public Activity a() {
        j jVar = this.d;
        if (jVar != null) {
            return (Activity) jVar.get();
        }
        return null;
    }

    public Activity b() {
        return this.e;
    }

    public d c() {
        if (b() == null) {
            return null;
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public com.chartboost.sdk.Model.c d() {
        bc bcVar;
        d c2 = c();
        if (c2 == null) {
            bcVar = null;
        } else {
            bcVar = c2.a();
        }
        if (bcVar == null || !bcVar.f()) {
            return null;
        }
        return bcVar.e();
    }

    public boolean e() {
        return d() != null;
    }

    /* access modifiers changed from: package-private */
    public void a(CBImpressionActivity cBImpressionActivity) {
        aq.a("CBUIManager.setImpressionActivity", cBImpressionActivity);
        if (this.e == null) {
            i.m = cBImpressionActivity.getApplicationContext();
            this.e = cBImpressionActivity;
        }
        this.b.removeCallbacks(this.g);
    }

    /* access modifiers changed from: package-private */
    public void f() {
        aq.a("CBUIManager.clearImpressionActivity");
        this.e = null;
    }

    private void a(int i2, boolean z) {
        if (z) {
            this.l.add(Integer.valueOf(i2));
        } else {
            this.l.remove(Integer.valueOf(i2));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar, boolean z) {
        if (jVar != null) {
            a(jVar.a, z);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, boolean z) {
        if (activity != null) {
            a(activity.hashCode(), z);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(j jVar) {
        if (jVar == null) {
            return false;
        }
        return this.l.contains(Integer.valueOf(jVar.a));
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return a(this.d);
    }

    private boolean l(Activity activity) {
        return this.e == activity;
    }

    private boolean c(j jVar) {
        if (jVar == null) {
            return this.e == null;
        }
        return jVar.a(this.e);
    }

    public void a(com.chartboost.sdk.Model.c cVar) {
        aq.a("CBUIManager.queueDisplayView", cVar);
        if (e()) {
            cVar.a(CBError.CBImpressionError.IMPRESSION_ALREADY_VISIBLE);
        } else if (this.e != null) {
            this.c.a(cVar);
        } else if (!g()) {
            cVar.a(CBError.CBImpressionError.NO_HOST_ACTIVITY);
        } else {
            Activity a2 = a();
            if (a2 == null) {
                CBLogging.b("CBUIManager", "Failed to display impression as the host activity reference has been lost!");
                cVar.a(CBError.CBImpressionError.NO_HOST_ACTIVITY);
                return;
            }
            com.chartboost.sdk.Model.c cVar2 = this.f;
            if (cVar2 == null || cVar2 == cVar) {
                this.f = cVar;
                if (i.c != null) {
                    if (cVar.n == 1 || cVar.n == 2) {
                        i.c.willDisplayVideo(cVar.m);
                    } else if (cVar.n == 0) {
                        i.c.willDisplayInterstitial(cVar.m);
                    }
                }
                if (i.d != null) {
                    C0007c cVar3 = new C0007c(9);
                    cVar3.b = a2;
                    cVar3.d = cVar;
                    this.b.postDelayed(cVar3, (long) 1);
                    return;
                }
                a(a2, cVar);
                return;
            }
            cVar.a(CBError.CBImpressionError.IMPRESSION_ALREADY_VISIBLE);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(Activity activity, com.chartboost.sdk.Model.c cVar) {
        Intent intent = new Intent(activity, CBImpressionActivity.class);
        boolean z = false;
        boolean z2 = (activity.getWindow().getAttributes().flags & 1024) != 0;
        boolean z3 = (activity.getWindow().getAttributes().flags & 2048) != 0;
        if (z2 && !z3) {
            z = true;
        }
        intent.putExtra("paramFullscreen", z);
        intent.putExtra("isChartboost", true);
        try {
            activity.startActivity(intent);
            this.k = true;
        } catch (ActivityNotFoundException unused) {
            CBLogging.b("CBUIManager", "Please add CBImpressionActivity in AndroidManifest.xml following README.md instructions.");
            this.f = null;
            cVar.a(CBError.CBImpressionError.ACTIVITY_MISSING_IN_MANIFEST);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void
     arg types: [com.chartboost.sdk.Libraries.j, int]
     candidates:
      com.chartboost.sdk.c.a(int, boolean):void
      com.chartboost.sdk.c.a(android.app.Activity, com.chartboost.sdk.Model.c):void
      com.chartboost.sdk.c.a(android.app.Activity, boolean):void
      com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(j jVar) {
        aq.a("CBUIManager.onStop", jVar);
        if (!(jVar.get() instanceof CBImpressionActivity)) {
            a(jVar, false);
        }
        this.a.c();
    }

    /* access modifiers changed from: package-private */
    public void b(Activity activity) {
        aq.a("CBUIManager.onCreateCallback", activity);
        if (b.b() && b.a(activity)) {
            C0007c cVar = new C0007c(0);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void
     arg types: [com.chartboost.sdk.Libraries.j, int]
     candidates:
      com.chartboost.sdk.c.a(int, boolean):void
      com.chartboost.sdk.c.a(android.app.Activity, com.chartboost.sdk.Model.c):void
      com.chartboost.sdk.c.a(android.app.Activity, boolean):void
      com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void */
    /* access modifiers changed from: package-private */
    public void c(Activity activity) {
        aq.a("CBUIManager.onCreateImpl", activity);
        j jVar = this.d;
        if (jVar != null && !jVar.a(activity) && g()) {
            b(this.d);
            a(this.d, false);
        }
        this.b.removeCallbacks(this.g);
        this.d = a(activity);
        aq.a("CBUIManager.assignHostActivityRef", this.d);
    }

    /* access modifiers changed from: package-private */
    public void d(Activity activity) {
        aq.a("CBUIManager.onStartCallback", activity);
        if (b.b() && b.a(activity)) {
            C0007c cVar = new C0007c(1);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void
     arg types: [com.chartboost.sdk.Libraries.j, int]
     candidates:
      com.chartboost.sdk.c.a(int, boolean):void
      com.chartboost.sdk.c.a(android.app.Activity, com.chartboost.sdk.Model.c):void
      com.chartboost.sdk.c.a(android.app.Activity, boolean):void
      com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.c.b(com.chartboost.sdk.Libraries.j, boolean):void
     arg types: [com.chartboost.sdk.Libraries.j, int]
     candidates:
      com.chartboost.sdk.c.b(android.app.Activity, com.chartboost.sdk.Model.c):boolean
      com.chartboost.sdk.c.b(com.chartboost.sdk.Libraries.j, boolean):void */
    /* access modifiers changed from: package-private */
    public void e(Activity activity) {
        aq.a("CBUIManager.onStartImpl", activity);
        i.m = activity.getApplicationContext();
        boolean z = activity instanceof CBImpressionActivity;
        if (!z) {
            this.d = a(activity);
            aq.a("CBUIManager.assignHostActivityRef", this.d);
            a(this.d, true);
        } else {
            a((CBImpressionActivity) activity);
        }
        this.b.removeCallbacks(this.g);
        boolean z2 = i.d != null && i.d.doesWrapperUseCustomBackgroundingBehavior();
        if (activity == null) {
            return;
        }
        if (z2 || l(activity)) {
            b(a(activity), true);
            if (z) {
                this.k = false;
            }
            if (b(activity, this.f)) {
                this.f = null;
            }
            com.chartboost.sdk.Model.c d2 = d();
            if (d2 != null) {
                d2.s();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f(Activity activity) {
        aq.a("CBUIManager.onResumeCallback", activity);
        if (b.b() && b.a(activity)) {
            this.a.e();
            C0007c cVar = new C0007c(2);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        aq.a("CBUIManager.onResumeImpl", (String) null);
        this.i.b(i.m);
        com.chartboost.sdk.Model.c d2 = d();
        if (CBUtility.a(Chartboost.CBFramework.CBFrameworkUnity)) {
            this.a.b();
        }
        if (d2 != null) {
            d2.r();
        }
    }

    /* access modifiers changed from: package-private */
    public void g(Activity activity) {
        aq.a("CBUIManager.onPauseCallback", activity);
        if (b.b() && b.a(activity)) {
            C0007c cVar = new C0007c(3);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        aq.a("CBUIManager.onPauseImpl", (String) null);
        com.chartboost.sdk.Model.c d2 = d();
        if (d2 != null) {
            d2.t();
        }
        this.i.c(i.m);
    }

    /* access modifiers changed from: package-private */
    public void h(Activity activity) {
        aq.a("CBUIManager.onStopCallback", activity);
        if (b.b() && b.a(activity)) {
            C0007c cVar = new C0007c(4);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.c.b(com.chartboost.sdk.Libraries.j, boolean):void
     arg types: [com.chartboost.sdk.Libraries.j, int]
     candidates:
      com.chartboost.sdk.c.b(android.app.Activity, com.chartboost.sdk.Model.c):boolean
      com.chartboost.sdk.c.b(com.chartboost.sdk.Libraries.j, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void
     arg types: [com.chartboost.sdk.Libraries.j, int]
     candidates:
      com.chartboost.sdk.c.a(int, boolean):void
      com.chartboost.sdk.c.a(android.app.Activity, com.chartboost.sdk.Model.c):void
      com.chartboost.sdk.c.a(android.app.Activity, boolean):void
      com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void */
    /* access modifiers changed from: package-private */
    public void i(Activity activity) {
        j a2 = a(activity);
        aq.a("CBUIManager.onStopImpl", a2);
        com.chartboost.sdk.Model.c d2 = d();
        if (d2 != null && d2.p.b == 0) {
            d c2 = c();
            if (c(a2) && c2 != null) {
                c2.c(d2);
                this.f = d2;
                b(a2, false);
            }
            if (!(a2.get() instanceof CBImpressionActivity)) {
                a(a2, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        aq.a("CBUIManager.onBackPressedCallback");
        if (!b.b()) {
            return false;
        }
        if (this.d == null) {
            CBLogging.b("CBUIManager", "The Chartboost methods onCreate(), onStart(), onStop(), and onDestroy() must be called in the corresponding methods of your activity in order for Chartboost to function properly.");
            return false;
        } else if (!this.k) {
            return false;
        } else {
            this.k = false;
            k();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean k() {
        aq.a("CBUIManager.onBackPressedImpl");
        return m();
    }

    private boolean m() {
        aq.a("CBUIManager.closeImpressionImpl");
        com.chartboost.sdk.Model.c d2 = d();
        if (d2 == null || d2.l != 2) {
            return false;
        }
        if (d2.q()) {
            return true;
        }
        h.b(new C0007c(7));
        return true;
    }

    /* access modifiers changed from: package-private */
    public void j(Activity activity) {
        aq.a("CBUIManager.onDestroyCallback", activity);
        if (b.b() && b.a(activity)) {
            C0007c cVar = new C0007c(5);
            cVar.b = activity;
            h.b(cVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.c.b(com.chartboost.sdk.Libraries.j, boolean):void
     arg types: [com.chartboost.sdk.Libraries.j, int]
     candidates:
      com.chartboost.sdk.c.b(android.app.Activity, com.chartboost.sdk.Model.c):boolean
      com.chartboost.sdk.c.b(com.chartboost.sdk.Libraries.j, boolean):void */
    /* access modifiers changed from: package-private */
    public void k(Activity activity) {
        com.chartboost.sdk.Model.c cVar;
        aq.a("CBUIManager.onDestroyImpl", activity);
        b(a(activity), false);
        com.chartboost.sdk.Model.c d2 = d();
        if (!(d2 == null && activity == this.e && (cVar = this.f) != null)) {
            cVar = d2;
        }
        d c2 = c();
        if (!(c2 == null || cVar == null)) {
            c2.d(cVar);
        }
        this.f = null;
    }

    public void b(com.chartboost.sdk.Model.c cVar) {
        d c2;
        if (cVar.l == 2) {
            d c3 = c();
            if (c3 != null) {
                c3.b(cVar);
            }
        } else if (cVar.p.b == 1 && cVar.l == 1 && (c2 = c()) != null) {
            c2.d(cVar);
        }
        if (cVar.v()) {
            this.j.d(cVar.a.a(cVar.p.b), cVar.m, cVar.o());
        } else {
            this.j.e(cVar.a.a(cVar.p.b), cVar.m, cVar.o());
        }
    }

    /* access modifiers changed from: package-private */
    public boolean l() {
        com.chartboost.sdk.Model.c d2 = d();
        if (d2 == null) {
            return false;
        }
        d2.z = true;
        b(d2);
        return true;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000d, code lost:
        if (r1 != 3) goto L_0x004d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(android.app.Activity r4, com.chartboost.sdk.Model.c r5) {
        /*
            r3 = this;
            r0 = 1
            if (r5 == 0) goto L_0x004d
            int r1 = r5.l
            if (r1 == 0) goto L_0x004d
            if (r1 == r0) goto L_0x004a
            r2 = 2
            if (r1 == r2) goto L_0x0010
            r4 = 3
            if (r1 == r4) goto L_0x004a
            goto L_0x004d
        L_0x0010:
            boolean r1 = r5.g()
            if (r1 != 0) goto L_0x004d
            com.chartboost.sdk.Chartboost$CBFramework r1 = com.chartboost.sdk.i.d
            if (r1 == 0) goto L_0x0028
            com.chartboost.sdk.Chartboost$CBFramework r1 = com.chartboost.sdk.i.d
            boolean r1 = r1.doesWrapperUseCustomBackgroundingBehavior()
            if (r1 == 0) goto L_0x0028
            boolean r4 = r4 instanceof com.chartboost.sdk.CBImpressionActivity
            if (r4 != 0) goto L_0x0028
            r4 = 0
            return r4
        L_0x0028:
            com.chartboost.sdk.d r4 = r3.c()
            if (r4 == 0) goto L_0x004d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error onActivityStart "
            r1.append(r2)
            int r2 = r5.l
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "CBUIManager"
            com.chartboost.sdk.Libraries.CBLogging.b(r2, r1)
            r4.d(r5)
            goto L_0x004d
        L_0x004a:
            r3.a(r5)
        L_0x004d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.c.b(android.app.Activity, com.chartboost.sdk.Model.c):boolean");
    }

    class b implements Runnable {
        private final int b;
        private final int c;
        private final int d;

        private a a() {
            return i.c;
        }

        b() {
            a a2 = a();
            int i = -1;
            this.b = c.this.e == null ? -1 : c.this.e.hashCode();
            this.c = c.this.d == null ? -1 : c.this.d.hashCode();
            this.d = a2 != null ? a2.hashCode() : i;
        }

        public void run() {
            aq.a("ClearMemoryRunnable.run");
            a a2 = a();
            if (c.this.d != null && c.this.d.hashCode() == this.c) {
                c.this.d = null;
                aq.a("CBUIManager.clearHostActivityRef");
            }
            if (a2 != null && a2.hashCode() == this.d) {
                i.c = null;
                aq.a("SdkSettings.clearDelegate");
            }
        }
    }

    private class a implements Application.ActivityLifecycleCallbacks {
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        private a() {
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityCreated", activity);
            CBLogging.a("CBUIManager", "######## onActivityCreated callback called");
            if (!(activity instanceof CBImpressionActivity)) {
                c.this.b(activity);
            }
        }

        public void onActivityStarted(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityStarted", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityStarted callback called from developer side");
                c.this.d(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityStarted callback called from CBImpressionactivity");
            c.this.e(activity);
        }

        public void onActivityResumed(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityResumed", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityResumed callback called from developer side");
                c.this.f(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityResumed callback called from CBImpressionactivity");
            c.this.a(activity);
            c.this.h();
        }

        public void onActivityPaused(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityPaused", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityPaused callback called from developer side");
                c.this.g(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityPaused callback called from CBImpressionactivity");
            c.this.a(activity);
            c.this.i();
        }

        public void onActivityStopped(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityStopped", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityStopped callback called from developer side");
                c.this.h(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityStopped callback called from CBImpressionactivity");
            c.this.i(activity);
        }

        public void onActivityDestroyed(Activity activity) {
            aq.a("CBUIManager.ActivityCallbackListener.onActivityDestroyed", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                CBLogging.a("CBUIManager", "######## onActivityDestroyed callback called from developer side");
                c.this.j(activity);
                return;
            }
            CBLogging.a("CBUIManager", "######## onActivityDestroyed callback called from CBImpressionactivity");
            c.this.k(activity);
        }
    }

    /* renamed from: com.chartboost.sdk.c$c  reason: collision with other inner class name */
    public class C0007c implements Runnable {
        public final int a;
        Activity b = null;
        boolean c = false;
        public com.chartboost.sdk.Model.c d = null;

        public C0007c(int i) {
            this.a = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void
         arg types: [com.chartboost.sdk.Libraries.j, int]
         candidates:
          com.chartboost.sdk.c.a(int, boolean):void
          com.chartboost.sdk.c.a(android.app.Activity, com.chartboost.sdk.Model.c):void
          com.chartboost.sdk.c.a(android.app.Activity, boolean):void
          com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.c.a(android.app.Activity, boolean):void
         arg types: [android.app.Activity, int]
         candidates:
          com.chartboost.sdk.c.a(int, boolean):void
          com.chartboost.sdk.c.a(android.app.Activity, com.chartboost.sdk.Model.c):void
          com.chartboost.sdk.c.a(com.chartboost.sdk.Libraries.j, boolean):void
          com.chartboost.sdk.c.a(android.app.Activity, boolean):void */
        public void run() {
            try {
                switch (this.a) {
                    case 0:
                        c.this.c(this.b);
                        return;
                    case 1:
                        c.this.b.removeCallbacks(c.this.g);
                        if (c.this.d != null && !c.this.d.a(this.b) && c.this.g()) {
                            c.this.b(c.this.d);
                            c.this.a(c.this.d, false);
                        }
                        c.this.a(this.b, true);
                        c.this.d = c.this.a(this.b);
                        c.this.a.b();
                        c.this.a.a(this.b);
                        c.this.e(this.b);
                        return;
                    case 2:
                        if (c.this.a(c.this.a(this.b))) {
                            c.this.h();
                            return;
                        } else if (CBUtility.a(Chartboost.CBFramework.CBFrameworkUnity)) {
                            c.this.a.b();
                            return;
                        } else {
                            return;
                        }
                    case 3:
                        if (c.this.a(c.this.a(this.b))) {
                            c.this.i();
                            return;
                        }
                        return;
                    case 4:
                        j a2 = c.this.a(this.b);
                        if (c.this.a(a2)) {
                            c.this.b(a2);
                            return;
                        }
                        return;
                    case 5:
                        if (c.this.d == null || c.this.d.a(this.b)) {
                            c.this.g = new b();
                            c.this.g.run();
                        }
                        c.this.k(this.b);
                        return;
                    case 6:
                        if (c.this.e == null) {
                            return;
                        }
                        if (this.c) {
                            c.this.e.forwardTouchEvents(c.this.a());
                            return;
                        } else {
                            c.this.e.forwardTouchEvents(null);
                            return;
                        }
                    case 7:
                        c.this.l();
                        return;
                    case 8:
                    default:
                        return;
                    case 9:
                        c.this.a(this.b, this.d);
                        return;
                    case 10:
                        if (this.d.a()) {
                            this.d.u().b();
                            return;
                        }
                        return;
                    case 11:
                        d c2 = c.this.c();
                        if (this.d.l == 2 && c2 != null) {
                            c2.b(this.d);
                            return;
                        }
                        return;
                    case 12:
                        this.d.n();
                        return;
                    case 13:
                        c.this.c.a(this.d, this.b);
                        return;
                    case 14:
                        c.this.c.d(this.d);
                        return;
                }
            } catch (Exception e2) {
                com.chartboost.sdk.Tracking.a.a(C0007c.class, "run (" + this.a + ")", e2);
            }
        }
    }
}
