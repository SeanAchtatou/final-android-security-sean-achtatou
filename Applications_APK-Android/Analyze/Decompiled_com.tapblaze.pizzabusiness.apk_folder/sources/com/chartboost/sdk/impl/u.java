package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.core.view.ViewCompat;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.h;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.e;
import com.chartboost.sdk.g;
import org.json.JSONObject;

public class u extends e {
    h j = new h(this);
    h k = new h(this);
    h l = new h(this);
    h m = new h(this);
    h n = new h(this);
    h o = new h(this);
    protected float p = 1.0f;
    private final String q = "ImageViewProtocol";

    public class a extends e.a {
        protected ay c;
        protected az d;
        protected az e;
        protected ImageView f;
        private boolean h = false;

        protected a(Context context) {
            super(context);
            setBackgroundColor(0);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            g a = g.a();
            this.c = (ay) a.a(new ay(context));
            addView(this.c, new RelativeLayout.LayoutParams(-1, -1));
            this.e = (az) a.a(new az(context, u.this) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    a.this.a(motionEvent.getX(), motionEvent.getY(), (float) a.this.e.getWidth(), (float) a.this.e.getHeight());
                }
            });
            a(this.e);
            this.e.setContentDescription("CBAd");
            this.f = (ImageView) a.a(new ImageView(context));
            this.f.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            addView(this.f);
            addView(this.e);
        }

        /* access modifiers changed from: protected */
        public void c() {
            this.d = new az(getContext()) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    a.this.d();
                }
            };
            this.d.setContentDescription("CBClose");
            addView(this.d);
        }

        /* access modifiers changed from: protected */
        public void a(float f2, float f3, float f4, float f5) {
            u.this.b(com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("x", Float.valueOf(f2)), com.chartboost.sdk.Libraries.e.a("y", Float.valueOf(f3)), com.chartboost.sdk.Libraries.e.a("w", Float.valueOf(f4)), com.chartboost.sdk.Libraries.e.a("h", Float.valueOf(f5))));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [float, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        /* access modifiers changed from: protected */
        public void a(int i, int i2) {
            int i3;
            int i4;
            if (!this.h) {
                c();
                this.h = true;
            }
            boolean a = CBUtility.a(u.this.a());
            u uVar = u.this;
            h hVar = a ? uVar.j : uVar.k;
            u uVar2 = u.this;
            h hVar2 = a ? uVar2.l : uVar2.m;
            if (!hVar.d()) {
                if (hVar == u.this.j) {
                    hVar = u.this.k;
                } else {
                    hVar = u.this.j;
                }
            }
            if (!hVar2.d()) {
                if (hVar2 == u.this.l) {
                    hVar2 = u.this.m;
                } else {
                    hVar2 = u.this.l;
                }
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            u.this.a(layoutParams, hVar, 1.0f);
            u.this.p = Math.min(Math.min(((float) i) / ((float) layoutParams.width), ((float) i2) / ((float) layoutParams.height)), 1.0f);
            layoutParams.width = (int) (((float) layoutParams.width) * u.this.p);
            layoutParams.height = (int) (((float) layoutParams.height) * u.this.p);
            Point b = u.this.b(a ? "frame-portrait" : "frame-landscape");
            layoutParams.leftMargin = Math.round((((float) (i - layoutParams.width)) / 2.0f) + ((((float) b.x) / hVar.f()) * u.this.p));
            layoutParams.topMargin = Math.round((((float) (i2 - layoutParams.height)) / 2.0f) + ((((float) b.y) / hVar.f()) * u.this.p));
            u.this.a(layoutParams2, hVar2, 1.0f);
            Point b2 = u.this.b(a ? "close-portrait" : "close-landscape");
            if (b2.x == 0 && b2.y == 0) {
                i4 = layoutParams.leftMargin + layoutParams.width + Math.round(((float) (-layoutParams2.width)) / 2.0f);
                i3 = layoutParams.topMargin + Math.round(((float) (-layoutParams2.height)) / 2.0f);
            } else {
                int round = Math.round(((((float) layoutParams.leftMargin) + (((float) layoutParams.width) / 2.0f)) + ((float) b2.x)) - (((float) layoutParams2.width) / 2.0f));
                i3 = Math.round(((((float) layoutParams.topMargin) + (((float) layoutParams.height) / 2.0f)) + ((float) b2.y)) - (((float) layoutParams2.height) / 2.0f));
                i4 = round;
            }
            layoutParams2.leftMargin = Math.min(Math.max(0, i4), i - layoutParams2.width);
            layoutParams2.topMargin = Math.min(Math.max(0, i3), i2 - layoutParams2.height);
            this.c.setLayoutParams(layoutParams);
            this.d.setLayoutParams(layoutParams2);
            this.c.setScaleType(ImageView.ScaleType.FIT_CENTER);
            this.c.a(hVar);
            this.d.a(hVar2);
            u uVar3 = u.this;
            h hVar3 = a ? uVar3.n : uVar3.o;
            if (!hVar3.d()) {
                if (hVar3 == u.this.n) {
                    hVar3 = u.this.o;
                } else {
                    hVar3 = u.this.n;
                }
            }
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            u uVar4 = u.this;
            uVar4.a(layoutParams3, hVar3, uVar4.p);
            Point b3 = u.this.b(a ? "ad-portrait" : "ad-landscape");
            layoutParams3.leftMargin = Math.round((((float) (i - layoutParams3.width)) / 2.0f) + ((((float) b3.x) / hVar3.f()) * u.this.p));
            layoutParams3.topMargin = Math.round((((float) (i2 - layoutParams3.height)) / 2.0f) + ((((float) b3.y) / hVar3.f()) * u.this.p));
            this.f.setLayoutParams(layoutParams3);
            this.e.setLayoutParams(layoutParams3);
            this.e.a(ImageView.ScaleType.FIT_CENTER);
            this.e.a(hVar3);
        }

        /* access modifiers changed from: protected */
        public void d() {
            u.this.h();
        }

        public void b() {
            super.b();
            this.c = null;
            this.d = null;
            this.e = null;
            this.f = null;
        }
    }

    public u(c cVar, Handler handler, com.chartboost.sdk.c cVar2) {
        super(cVar, handler, cVar2);
    }

    /* access modifiers changed from: protected */
    public e.a b(Context context) {
        return new a(context);
    }

    public boolean a(JSONObject jSONObject) {
        if (!super.a(jSONObject)) {
            return false;
        }
        if (this.d.isNull("frame-portrait") || this.d.isNull("close-portrait")) {
            this.h = false;
        }
        if (this.d.isNull("frame-landscape") || this.d.isNull("close-landscape")) {
            this.i = false;
        }
        if (this.d.isNull("ad-portrait")) {
            this.h = false;
        }
        if (this.d.isNull("ad-landscape")) {
            this.i = false;
        }
        if (this.k.a("frame-landscape") && this.j.a("frame-portrait") && this.m.a("close-landscape") && this.l.a("close-portrait") && this.o.a("ad-landscape") && this.n.a("ad-portrait")) {
            return true;
        }
        CBLogging.b("ImageViewProtocol", "Error while downloading the assets");
        a(CBError.CBImpressionError.ASSETS_DOWNLOAD_FAILURE);
        return false;
    }

    /* access modifiers changed from: protected */
    public Point b(String str) {
        JSONObject a2 = com.chartboost.sdk.Libraries.e.a(this.d, str, "offset");
        if (a2 != null) {
            return new Point(a2.optInt("x"), a2.optInt("y"));
        }
        return new Point(0, 0);
    }

    public void a(ViewGroup.LayoutParams layoutParams, h hVar, float f) {
        if (hVar != null && hVar.d()) {
            layoutParams.width = (int) ((((float) hVar.a()) / hVar.f()) * f);
            layoutParams.height = (int) ((((float) hVar.b()) / hVar.f()) * f);
        }
    }

    public void d() {
        super.d();
        this.k.c();
        this.j.c();
        this.m.c();
        this.l.c();
        this.o.c();
        this.n.c();
        this.k = null;
        this.j = null;
        this.m = null;
        this.l = null;
        this.o = null;
        this.n = null;
    }
}
