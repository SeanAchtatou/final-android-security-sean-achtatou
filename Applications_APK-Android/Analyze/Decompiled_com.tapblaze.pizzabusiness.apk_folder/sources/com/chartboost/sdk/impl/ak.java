package com.chartboost.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.i;
import com.google.android.gms.drive.DriveFile;
import cz.msebera.android.httpclient.HttpHost;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Executor;

public class ak {
    final ai a;
    final Handler b;
    private final Executor c;
    private final ah d;

    public ak(Executor executor, ah ahVar, ai aiVar, Handler handler) {
        this.c = executor;
        this.d = ahVar;
        this.a = aiVar;
        this.b = handler;
    }

    public void a(c cVar, boolean z, String str, CBError.CBClickError cBClickError, aj ajVar) {
        if (cVar != null) {
            cVar.x = false;
            if (cVar.b()) {
                cVar.l = 4;
            }
        }
        if (!z) {
            if (i.c != null) {
                i.c.didFailToRecordClick(str, cBClickError);
            }
        } else if (cVar != null && cVar.w != null) {
            this.d.a(cVar.w);
        } else if (ajVar != null) {
            this.d.a(ajVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.impl.ak.a(com.chartboost.sdk.Model.c, java.lang.String, android.content.Context, com.chartboost.sdk.impl.aj):void
     arg types: [com.chartboost.sdk.Model.c, java.lang.String, android.app.Activity, com.chartboost.sdk.impl.aj]
     candidates:
      com.chartboost.sdk.impl.ak.a(com.chartboost.sdk.Model.c, java.lang.String, android.app.Activity, com.chartboost.sdk.impl.aj):void
      com.chartboost.sdk.impl.ak.a(com.chartboost.sdk.Model.c, java.lang.String, android.content.Context, com.chartboost.sdk.impl.aj):void */
    public void a(c cVar, String str, Activity activity, aj ajVar) {
        try {
            String scheme = new URI(str).getScheme();
            if (scheme == null) {
                a(cVar, false, str, CBError.CBClickError.URI_INVALID, ajVar);
            } else if (scheme.equals(HttpHost.DEFAULT_SCHEME_NAME) || scheme.equals("https")) {
                final String str2 = str;
                final c cVar2 = cVar;
                final Activity activity2 = activity;
                final aj ajVar2 = ajVar;
                this.c.execute(new Runnable() {
                    /* JADX WARNING: Removed duplicated region for block: B:24:0x004f A[SYNTHETIC, Splitter:B:24:0x004f] */
                    /* JADX WARNING: Removed duplicated region for block: B:27:0x0055 A[Catch:{ Exception -> 0x005d }] */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void run() {
                        /*
                            r6 = this;
                            java.lang.String r0 = r3     // Catch:{ Exception -> 0x005d }
                            com.chartboost.sdk.impl.ak r1 = com.chartboost.sdk.impl.ak.this     // Catch:{ Exception -> 0x005d }
                            com.chartboost.sdk.impl.ai r1 = r1.a     // Catch:{ Exception -> 0x005d }
                            boolean r1 = r1.b()     // Catch:{ Exception -> 0x005d }
                            if (r1 == 0) goto L_0x0059
                            r1 = 0
                            java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0045 }
                            java.lang.String r3 = r3     // Catch:{ Exception -> 0x0045 }
                            r2.<init>(r3)     // Catch:{ Exception -> 0x0045 }
                            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x0045 }
                            java.lang.Object r2 = com.google.firebase.perf.network.FirebasePerfUrlConnection.instrument(r2)     // Catch:{ Exception -> 0x0045 }
                            java.net.URLConnection r2 = (java.net.URLConnection) r2     // Catch:{ Exception -> 0x0045 }
                            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Exception -> 0x0045 }
                            r1 = 0
                            r2.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
                            r1 = 10000(0x2710, float:1.4013E-41)
                            r2.setConnectTimeout(r1)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
                            r2.setReadTimeout(r1)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
                            java.lang.String r1 = "Location"
                            java.lang.String r1 = r2.getHeaderField(r1)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
                            if (r1 == 0) goto L_0x0035
                            r0 = r1
                        L_0x0035:
                            if (r2 == 0) goto L_0x0059
                            r2.disconnect()     // Catch:{ Exception -> 0x005d }
                            goto L_0x0059
                        L_0x003b:
                            r0 = move-exception
                            r1 = r2
                            goto L_0x0053
                        L_0x003e:
                            r1 = move-exception
                            r5 = r2
                            r2 = r1
                            r1 = r5
                            goto L_0x0046
                        L_0x0043:
                            r0 = move-exception
                            goto L_0x0053
                        L_0x0045:
                            r2 = move-exception
                        L_0x0046:
                            java.lang.String r3 = "CBURLOpener"
                            java.lang.String r4 = "Exception raised while opening a HTTP Conection"
                            com.chartboost.sdk.Libraries.CBLogging.a(r3, r4, r2)     // Catch:{ all -> 0x0043 }
                            if (r1 == 0) goto L_0x0059
                            r1.disconnect()     // Catch:{ Exception -> 0x005d }
                            goto L_0x0059
                        L_0x0053:
                            if (r1 == 0) goto L_0x0058
                            r1.disconnect()     // Catch:{ Exception -> 0x005d }
                        L_0x0058:
                            throw r0     // Catch:{ Exception -> 0x005d }
                        L_0x0059:
                            r6.a(r0)     // Catch:{ Exception -> 0x005d }
                            goto L_0x0065
                        L_0x005d:
                            r0 = move-exception
                            java.lang.Class<com.chartboost.sdk.impl.ak> r1 = com.chartboost.sdk.impl.ak.class
                            java.lang.String r2 = "open followTask"
                            com.chartboost.sdk.Tracking.a.a(r1, r2, r0)
                        L_0x0065:
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.ak.AnonymousClass1.run():void");
                    }

                    private void a(final String str) {
                        AnonymousClass1 r0 = new Runnable() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.chartboost.sdk.impl.ak.a(com.chartboost.sdk.Model.c, java.lang.String, android.content.Context, com.chartboost.sdk.impl.aj):void
                             arg types: [com.chartboost.sdk.Model.c, java.lang.String, android.app.Activity, com.chartboost.sdk.impl.aj]
                             candidates:
                              com.chartboost.sdk.impl.ak.a(com.chartboost.sdk.Model.c, java.lang.String, android.app.Activity, com.chartboost.sdk.impl.aj):void
                              com.chartboost.sdk.impl.ak.a(com.chartboost.sdk.Model.c, java.lang.String, android.content.Context, com.chartboost.sdk.impl.aj):void */
                            public void run() {
                                try {
                                    ak.this.a(cVar2, str, (Context) activity2, ajVar2);
                                } catch (Exception e) {
                                    a.a(ak.class, "open openOnUiThread Runnable.run", e);
                                }
                            }
                        };
                        Activity activity = activity2;
                        if (activity != null) {
                            activity.runOnUiThread(r0);
                        } else {
                            ak.this.b.post(r0);
                        }
                    }
                });
            } else {
                a(cVar, str, (Context) activity, ajVar);
            }
        } catch (URISyntaxException unused) {
            a(cVar, false, str, CBError.CBClickError.URI_INVALID, ajVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar, String str, Context context, aj ajVar) {
        if (cVar != null && cVar.b()) {
            cVar.l = 5;
        }
        if (context == null) {
            context = i.m;
        }
        if (context == null) {
            a(cVar, false, str, CBError.CBClickError.NO_HOST_ACTIVITY, ajVar);
            return;
        }
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(DriveFile.MODE_READ_ONLY);
            }
            intent.setData(Uri.parse(str));
            context.startActivity(intent);
        } catch (Exception unused) {
            if (str.startsWith("market://")) {
                try {
                    str = "http://market.android.com/" + str.substring(9);
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    if (!(context instanceof Activity)) {
                        intent2.addFlags(DriveFile.MODE_READ_ONLY);
                    }
                    intent2.setData(Uri.parse(str));
                    context.startActivity(intent2);
                } catch (Exception e) {
                    CBLogging.a("CBURLOpener", "Exception raised openeing an inavld playstore URL", e);
                    a(cVar, false, str, CBError.CBClickError.URI_UNRECOGNIZED, ajVar);
                    return;
                }
            } else {
                a(cVar, false, str, CBError.CBClickError.URI_UNRECOGNIZED, ajVar);
            }
        }
        a(cVar, true, str, null, ajVar);
    }

    public boolean a(String str) {
        try {
            Context context = i.m;
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(DriveFile.MODE_READ_ONLY);
            }
            intent.setData(Uri.parse(str));
            if (context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            CBLogging.a("CBURLOpener", "Cannot open URL", e);
            a.a(ak.class, "canOpenURL", e);
            return false;
        }
    }

    public void a(c cVar, String str, aj ajVar) {
        a(cVar, str, cVar != null ? cVar.g.a() : null, ajVar);
    }
}
