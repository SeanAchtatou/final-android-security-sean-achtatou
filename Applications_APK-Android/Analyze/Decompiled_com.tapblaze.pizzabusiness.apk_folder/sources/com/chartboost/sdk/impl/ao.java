package com.chartboost.sdk.impl;

import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class ao {
    /* access modifiers changed from: package-private */
    public HttpURLConnection a(ad<?> adVar) throws IOException {
        return (HttpURLConnection) ((URLConnection) FirebasePerfUrlConnection.instrument(new URL(adVar.c).openConnection()));
    }
}
