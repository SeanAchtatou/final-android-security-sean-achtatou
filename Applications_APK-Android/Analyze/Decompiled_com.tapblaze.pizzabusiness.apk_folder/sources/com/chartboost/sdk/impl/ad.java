package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

public class ad<T> {
    public final String b;
    public final String c;
    public final int d;
    public final AtomicInteger e = new AtomicInteger();
    public final File f;
    public long g;
    public long h;
    public long i;
    public int j;

    public void a(CBError cBError, ag agVar) {
    }

    public void a(Object obj, ag agVar) {
    }

    public ad(String str, String str2, int i2, File file) {
        this.b = str;
        this.c = str2;
        this.d = i2;
        this.f = file;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
    }

    public ae a() {
        return new ae(null, null, null);
    }

    public af<T> a(ag agVar) {
        return af.a((Object) null);
    }

    public boolean b() {
        return this.e.compareAndSet(0, -1);
    }
}
