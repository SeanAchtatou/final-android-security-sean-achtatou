package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;

public class af<T> {
    public final T a;
    public final CBError b;

    public static <T> af<T> a(Object obj) {
        return new af<>(obj, null);
    }

    public static <T> af<T> a(CBError cBError) {
        return new af<>(null, cBError);
    }

    private af(T t, CBError cBError) {
        this.a = t;
        this.b = cBError;
    }
}
