package com.chartboost.sdk.impl;

import android.app.Application;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public class o {
    static List<String> a = new ArrayList();
    static HashMap<String, p> b = new HashMap<>();

    static boolean b() {
        return true;
    }

    public static void a() {
        a.clear();
        if (b()) {
            a.add("moat");
        }
    }

    public static void a(List<String> list) {
        for (String next : list) {
            if (a.contains(next) && !b.containsKey(next)) {
                b.put(next, null);
            }
        }
    }

    public static void a(Application application, boolean z, boolean z2, boolean z3) {
        for (String next : b.keySet()) {
            if (next.contains("moat")) {
                if (b.get(next) != null) {
                    b.get(next).b();
                }
                r rVar = new r();
                rVar.a(application, z, z2, z3);
                b.put("moat", rVar);
            }
        }
    }

    public static void a(WebView webView, HashSet<String> hashSet) {
        Iterator<String> it = hashSet.iterator();
        while (it.hasNext()) {
            p pVar = b.get(it.next());
            if (pVar != null) {
                pVar.a(webView);
            }
        }
    }

    public static void c() {
        for (p next : b.values()) {
            if (next != null) {
                next.a();
            }
        }
    }

    public static void d() {
        for (String str : b.keySet()) {
            p pVar = b.get(str);
            if (pVar != null) {
                pVar.b();
            }
        }
    }

    public static JSONArray e() {
        JSONArray jSONArray = new JSONArray();
        List<String> list = a;
        if (list != null) {
            for (String put : list) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static JSONArray f() {
        JSONArray jSONArray = new JSONArray();
        HashMap<String, p> hashMap = b;
        if (hashMap != null) {
            for (String put : hashMap.keySet()) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static String a(HashSet<String> hashSet) {
        JSONArray jSONArray = new JSONArray();
        Iterator<String> it = hashSet.iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (b.get(next) != null) {
                jSONArray.put(next);
            }
        }
        return jSONArray.toString();
    }
}
