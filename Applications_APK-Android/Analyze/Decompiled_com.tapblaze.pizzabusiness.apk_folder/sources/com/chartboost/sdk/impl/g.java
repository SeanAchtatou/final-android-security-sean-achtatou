package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.aj;
import org.json.JSONObject;

public class g implements aj.a {
    private final e a;
    private final String b;

    public g(e eVar, String str) {
        this.a = eVar;
        this.b = str;
    }

    public void a(aj ajVar, JSONObject jSONObject) {
        if (this.a.f.h || i.t) {
            synchronized (this.a) {
                this.a.b(this.b);
            }
        }
    }

    public void a(aj ajVar, CBError cBError) {
        if (this.a.f.h) {
            synchronized (this.a) {
                this.a.b(this.b);
            }
        }
    }
}
