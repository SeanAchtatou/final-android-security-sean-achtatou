package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.core.app.NotificationCompat;
import androidx.core.view.ViewCompat;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.h;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.e;
import com.chartboost.sdk.g;
import com.chartboost.sdk.impl.u;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.ironsource.sdk.constants.Constants;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.io.File;
import org.json.JSONObject;

public class v extends u {
    protected boolean A;
    protected boolean B;
    protected boolean C = false;
    protected int D = 0;
    protected h E;
    protected h F;
    protected h G;
    protected h H;
    protected h I;
    protected h J;
    protected h K;
    protected h L;
    protected boolean M = false;
    protected boolean N = false;
    protected boolean O = false;
    private boolean P = false;
    private boolean Q = false;
    private boolean R = false;
    final f q;
    protected int r = 0;
    protected int s;
    protected String t;
    protected String u;
    protected int v = 0;
    protected int w = 0;
    JSONObject x;
    protected boolean y;
    protected boolean z;

    public class a extends u.a {
        final ab h;
        y i;
        final t j;
        final w k;
        private final az m;
        private View n;
        private final az o;

        private a(Context context) {
            super(context);
            JSONObject optJSONObject;
            JSONObject optJSONObject2;
            g a = g.a();
            if (v.this.N) {
                this.n = new View(context);
                this.n.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                this.n.setVisibility(8);
                addView(this.n);
            }
            if (v.this.e.n == 2) {
                this.i = (y) a.a(new y(context, v.this));
                this.i.setVisibility(8);
                addView(this.i);
            }
            this.h = (ab) a.a(new ab(context, v.this));
            a(this.h.g);
            this.h.setVisibility(8);
            addView(this.h);
            this.j = (t) a.a(new t(context, v.this));
            this.j.setVisibility(8);
            addView(this.j);
            if (v.this.e.n == 2) {
                this.k = (w) a.a(new w(context, v.this));
                this.k.setVisibility(8);
                addView(this.k);
            } else {
                this.k = null;
            }
            this.m = new az(getContext(), v.this) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    if (v.this.e.n == 2) {
                        a.this.k.a(false);
                    }
                    if (v.this.r == 1) {
                        a.this.c(false);
                    }
                    a.this.b(true);
                }
            };
            this.m.setVisibility(8);
            addView(this.m);
            this.o = new az(getContext(), v.this) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    a.this.d();
                }
            };
            this.o.setVisibility(8);
            this.o.setContentDescription("CBClose");
            addView(this.o);
            JSONObject optJSONObject3 = v.this.x.optJSONObject(NotificationCompat.CATEGORY_PROGRESS);
            JSONObject optJSONObject4 = v.this.x.optJSONObject("video-controls-background");
            if (optJSONObject3 != null && !optJSONObject3.isNull("background-color") && !optJSONObject3.isNull("border-color") && !optJSONObject3.isNull("progress-color") && !optJSONObject3.isNull("radius")) {
                v.this.M = true;
                x c = this.h.c();
                c.a(e.a(optJSONObject3.optString("background-color")));
                c.b(e.a(optJSONObject3.optString("border-color")));
                c.c(e.a(optJSONObject3.optString("progress-color")));
                c.b((float) optJSONObject3.optDouble("radius", FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE));
            }
            if (optJSONObject4 != null && !optJSONObject4.isNull(Constants.ParametersKeys.COLOR)) {
                this.h.a(e.a(optJSONObject4.optString(Constants.ParametersKeys.COLOR)));
            }
            if (v.this.e.n == 2 && v.this.A && (optJSONObject2 = v.this.x.optJSONObject("post-video-toaster")) != null) {
                this.j.a(optJSONObject2.optString("title"), optJSONObject2.optString("tagline"));
            }
            if (v.this.e.n == 2 && v.this.z && (optJSONObject = v.this.x.optJSONObject("confirmation")) != null) {
                this.i.a(optJSONObject.optString(ViewHierarchyConstants.TEXT_KEY), e.a(optJSONObject.optString(Constants.ParametersKeys.COLOR)));
            }
            String str = "";
            if (v.this.e.n == 2 && v.this.B) {
                JSONObject a2 = com.chartboost.sdk.Libraries.e.a(v.this.x, "post-video-reward-toaster");
                this.k.a((a2 == null || !a2.optString(Constants.ParametersKeys.POSITION).equals("inside-top")) ? 1 : 0);
                this.k.a(a2 != null ? a2.optString(ViewHierarchyConstants.TEXT_KEY) : str);
                if (v.this.J.d()) {
                    this.k.a(v.this.L);
                }
            }
            JSONObject g = v.this.g();
            if (g == null || g.isNull("video-click-button")) {
                this.h.d();
            }
            this.h.d(v.this.x.optBoolean("video-progress-timer-enabled"));
            if (v.this.O || v.this.N) {
                this.f.setVisibility(4);
            }
            String[] strArr = new String[1];
            strArr[0] = CBUtility.a(v.this.a()) ? "video-portrait" : "video-landscape";
            JSONObject a3 = com.chartboost.sdk.Libraries.e.a(g, strArr);
            v.this.u = a3 != null ? a3.optString("id") : str;
            if (v.this.u.isEmpty()) {
                v.this.a(CBError.CBImpressionError.VIDEO_ID_MISSING);
                return;
            }
            if (v.this.t == null) {
                v.this.t = v.this.q.a(v.this.u);
            }
            if (v.this.t == null) {
                v.this.a(CBError.CBImpressionError.VIDEO_UNAVAILABLE);
            } else {
                this.h.a(v.this.t);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.impl.v.a.a(int, boolean):void
         arg types: [int, int]
         candidates:
          com.chartboost.sdk.impl.v.a.a(int, int):void
          com.chartboost.sdk.impl.u.a.a(int, int):void
          com.chartboost.sdk.e.a.a(int, int):void
          com.chartboost.sdk.impl.v.a.a(int, boolean):void */
        /* access modifiers changed from: protected */
        public void c() {
            super.c();
            if (v.this.r != 0 || (v.this.z && !v.this.o())) {
                a(v.this.r, false);
            } else {
                b(false);
            }
        }

        public void e() {
            c(true);
            this.h.h();
            v.this.s++;
            if (v.this.s <= 1 && !v.this.u() && v.this.v >= 1) {
                v.this.e.e();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.impl.v.a.a(int, boolean):void
         arg types: [int, int]
         candidates:
          com.chartboost.sdk.impl.v.a.a(int, int):void
          com.chartboost.sdk.impl.u.a.a(int, int):void
          com.chartboost.sdk.e.a.a(int, int):void
          com.chartboost.sdk.impl.v.a.a(int, boolean):void */
        /* access modifiers changed from: protected */
        public void a(int i2, int i3) {
            super.a(i2, i3);
            a(v.this.r, false);
            boolean a = CBUtility.a(v.this.a());
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams6 = (RelativeLayout.LayoutParams) this.c.getLayoutParams();
            v vVar = v.this;
            vVar.a(layoutParams2, a ? vVar.F : vVar.E, 1.0f);
            Point b = v.this.b(a ? "replay-portrait" : "replay-landscape");
            int round = Math.round(((((float) layoutParams6.leftMargin) + (((float) layoutParams6.width) / 2.0f)) + ((float) b.x)) - (((float) layoutParams2.width) / 2.0f));
            int round2 = Math.round(((((float) layoutParams6.topMargin) + (((float) layoutParams6.height) / 2.0f)) + ((float) b.y)) - (((float) layoutParams2.height) / 2.0f));
            layoutParams2.leftMargin = Math.min(Math.max(0, round), i2 - layoutParams2.width);
            layoutParams2.topMargin = Math.min(Math.max(0, round2), i3 - layoutParams2.height);
            this.m.bringToFront();
            if (a) {
                this.m.a(v.this.F);
            } else {
                this.m.a(v.this.E);
            }
            RelativeLayout.LayoutParams layoutParams7 = (RelativeLayout.LayoutParams) this.e.getLayoutParams();
            if (!v.this.s()) {
                layoutParams3.width = layoutParams7.width;
                layoutParams3.height = layoutParams7.height;
                layoutParams3.leftMargin = layoutParams7.leftMargin;
                layoutParams3.topMargin = layoutParams7.topMargin;
                layoutParams4.width = layoutParams7.width;
                layoutParams4.height = layoutParams7.height;
                layoutParams4.leftMargin = layoutParams7.leftMargin;
                layoutParams4.topMargin = layoutParams7.topMargin;
            } else {
                RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(-2, -2);
                h hVar = a ? v.this.l : v.this.m;
                v.this.a(layoutParams8, hVar, 1.0f);
                layoutParams8.leftMargin = 0;
                layoutParams8.topMargin = 0;
                layoutParams8.addRule(11);
                this.o.setLayoutParams(layoutParams8);
                this.o.a(hVar);
            }
            layoutParams5.width = layoutParams7.width;
            layoutParams5.height = 72;
            layoutParams5.leftMargin = layoutParams7.leftMargin;
            layoutParams5.topMargin = (layoutParams7.topMargin + layoutParams7.height) - 72;
            if (v.this.N) {
                this.n.setLayoutParams(layoutParams);
            }
            if (v.this.e.n == 2) {
                this.i.setLayoutParams(layoutParams3);
            }
            this.h.setLayoutParams(layoutParams4);
            this.j.setLayoutParams(layoutParams5);
            this.m.setLayoutParams(layoutParams2);
            if (v.this.e.n == 2) {
                this.i.a();
            }
            this.h.a();
        }

        /* access modifiers changed from: package-private */
        public void b(boolean z) {
            if (v.this.r != 1) {
                if (v.this.z) {
                    a(0, z);
                    return;
                }
                a(1, z);
                JSONObject a = com.chartboost.sdk.Libraries.e.a(v.this.x, "timer");
                if (v.this.s >= 1 || a == null || a.isNull(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_DELAY)) {
                    this.h.b(!v.this.y);
                } else {
                    Object[] objArr = new Object[1];
                    objArr[0] = v.this.y ? "visible" : "hidden";
                    CBLogging.c("InterstitialVideoViewProtocol", String.format("controls starting %s, setting timer", objArr));
                    this.h.b(v.this.y);
                    v.this.a(this.h, new Runnable() {
                        public void run() {
                            Object[] objArr = new Object[1];
                            objArr[0] = v.this.y ? "hidden" : "shown";
                            CBLogging.c("InterstitialVideoViewProtocol", String.format("controls %s automatically from timer", objArr));
                            a.this.h.a(!v.this.y, true);
                            synchronized (v.this.g) {
                                v.this.g.remove(a.this.h);
                            }
                        }
                    }, Math.round(a.optDouble(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_DELAY, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) * 1000.0d));
                }
                this.h.e();
                if (v.this.s <= 1) {
                    v.this.e.f();
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.impl.v.a.a(int, boolean):void
         arg types: [int, int]
         candidates:
          com.chartboost.sdk.impl.v.a.a(int, int):void
          com.chartboost.sdk.impl.u.a.a(int, int):void
          com.chartboost.sdk.e.a.a(int, int):void
          com.chartboost.sdk.impl.v.a.a(int, boolean):void */
        /* access modifiers changed from: package-private */
        public void c(boolean z) {
            this.h.f();
            if (v.this.r == 1 && z) {
                if (v.this.s < 1 && v.this.x != null && !v.this.x.isNull("post-video-reward-toaster") && v.this.B && v.this.J.d() && v.this.K.d()) {
                    e(true);
                }
                a(2, true);
                if (CBUtility.a(CBUtility.a())) {
                    requestLayout();
                }
            }
        }

        private void e(boolean z) {
            if (z) {
                this.k.a(true);
            } else {
                this.k.setVisibility(0);
            }
            v.this.a.postDelayed(new Runnable() {
                public void run() {
                    a.this.k.a(false);
                }
            }, 2500);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void
         arg types: [int, com.chartboost.sdk.impl.az, boolean]
         candidates:
          com.chartboost.sdk.impl.u.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.Libraries.h, float):void
          com.chartboost.sdk.e.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void
         arg types: [int, com.chartboost.sdk.impl.y, boolean]
         candidates:
          com.chartboost.sdk.impl.u.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.Libraries.h, float):void
          com.chartboost.sdk.e.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void
         arg types: [int, android.view.View, boolean]
         candidates:
          com.chartboost.sdk.impl.u.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.Libraries.h, float):void
          com.chartboost.sdk.e.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void
         arg types: [int, com.chartboost.sdk.impl.ab, boolean]
         candidates:
          com.chartboost.sdk.impl.u.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.Libraries.h, float):void
          com.chartboost.sdk.e.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void
         arg types: [int, com.chartboost.sdk.impl.t, boolean]
         candidates:
          com.chartboost.sdk.impl.u.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.Libraries.h, float):void
          com.chartboost.sdk.e.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.e.a(boolean, android.view.View, boolean):void */
        private void a(int i2, boolean z) {
            v vVar = v.this;
            vVar.r = i2;
            boolean z2 = true;
            if (i2 == 0) {
                vVar.a(!vVar.s(), this.e, z);
                if (v.this.e.n == 2) {
                    v.this.a(true, (View) this.i, z);
                }
                if (v.this.N) {
                    v.this.a(false, this.n, z);
                }
                v.this.a(false, (View) this.h, z);
                v.this.a(false, (View) this.m, z);
                v.this.a(false, (View) this.j, z);
                this.e.setEnabled(false);
                this.m.setEnabled(false);
                this.h.setEnabled(false);
            } else if (i2 == 1) {
                vVar.a(false, (View) this.e, z);
                if (v.this.e.n == 2) {
                    v.this.a(false, (View) this.i, z);
                }
                if (v.this.N) {
                    v.this.a(true, this.n, z);
                }
                v.this.a(true, (View) this.h, z);
                v.this.a(false, (View) this.m, z);
                v.this.a(false, (View) this.j, z);
                this.e.setEnabled(true);
                this.m.setEnabled(false);
                this.h.setEnabled(true);
            } else if (i2 == 2) {
                vVar.a(true, (View) this.e, z);
                if (v.this.e.n == 2) {
                    v.this.a(false, (View) this.i, z);
                }
                if (v.this.N) {
                    v.this.a(false, this.n, z);
                }
                v.this.a(false, (View) this.h, z);
                v.this.a(true, (View) this.m, z);
                v.this.a(v.this.K.d() && v.this.J.d() && v.this.A, this.j, z);
                this.m.setEnabled(true);
                this.e.setEnabled(true);
                this.h.setEnabled(false);
                if (v.this.C) {
                    e(false);
                }
            }
            boolean f = f();
            az d = d(true);
            d.setEnabled(f);
            v.this.a(f, d, z);
            az d2 = d(false);
            d2.setEnabled(false);
            v.this.a(false, (View) d2, z);
            if (v.this.O || v.this.N) {
                v vVar2 = v.this;
                vVar2.a(!vVar2.s(), this.f, z);
            }
            v vVar3 = v.this;
            vVar3.a(!vVar3.s(), this.c, z);
            if (i2 == 0) {
                z2 = false;
            }
            a(z2);
        }

        /* access modifiers changed from: protected */
        public boolean f() {
            if (v.this.r == 1 && v.this.s < 1) {
                StringBuilder sb = new StringBuilder();
                sb.append("close-");
                sb.append(CBUtility.a(v.this.a()) ? Constants.ParametersKeys.ORIENTATION_PORTRAIT : Constants.ParametersKeys.ORIENTATION_LANDSCAPE);
                JSONObject a = com.chartboost.sdk.Libraries.e.a(v.this.g(), sb.toString());
                float optDouble = a != null ? (float) a.optDouble(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_DELAY, -1.0d) : -1.0f;
                int round = optDouble >= 0.0f ? Math.round(optDouble * 1000.0f) : -1;
                v.this.D = round;
                if (round < 0 || round > this.h.b().d()) {
                    return false;
                }
            }
            return true;
        }

        public void b() {
            v.this.n();
            super.b();
        }

        /* access modifiers changed from: protected */
        public void d() {
            if (v.this.r != 1 || v.this.e.a.a != 1) {
                if (v.this.r == 1) {
                    c(false);
                    this.h.h();
                    if (v.this.s < 1) {
                        v.this.s++;
                        v.this.e.e();
                    }
                }
                v.this.a.post(new Runnable() {
                    public void run() {
                        try {
                            v.this.h();
                        } catch (Exception e) {
                            com.chartboost.sdk.Tracking.a.a(a.class, "onCloseButton Runnable.run", e);
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: protected */
        public void a(float f, float f2, float f3, float f4) {
            if ((!v.this.y || v.this.r != 1) && v.this.r != 0) {
                b(f, f2, f3, f4);
            }
        }

        /* access modifiers changed from: protected */
        public void b(float f, float f2, float f3, float f4) {
            if (v.this.r == 1) {
                c(false);
            }
            v.this.b(com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("x", Float.valueOf(f)), com.chartboost.sdk.Libraries.e.a("y", Float.valueOf(f2)), com.chartboost.sdk.Libraries.e.a("w", Float.valueOf(f3)), com.chartboost.sdk.Libraries.e.a("h", Float.valueOf(f4))));
        }

        /* access modifiers changed from: protected */
        public void g() {
            v.this.z = false;
            b(true);
        }

        public az d(boolean z) {
            return ((!v.this.s() || !z) && (v.this.s() || z)) ? this.d : this.o;
        }
    }

    public v(c cVar, f fVar, Handler handler, com.chartboost.sdk.c cVar2) {
        super(cVar, handler, cVar2);
        this.q = fVar;
        this.r = 0;
        this.E = new h(this);
        this.F = new h(this);
        this.G = new h(this);
        this.H = new h(this);
        this.I = new h(this);
        this.J = new h(this);
        this.K = new h(this);
        this.L = new h(this);
        this.s = 0;
    }

    public boolean o() {
        return this.e.n == 1;
    }

    /* access modifiers changed from: protected */
    public e.a b(Context context) {
        return new a(context);
    }

    public boolean l() {
        e().d();
        return true;
    }

    public void m() {
        super.m();
        if (this.r == 1 && this.P) {
            e().h.b().a(this.v);
            e().h.e();
        }
        this.P = false;
    }

    public void n() {
        super.n();
        if (this.r == 1 && !this.P) {
            this.P = true;
            e().h.g();
        }
    }

    public boolean a(JSONObject jSONObject) {
        if (!super.a(jSONObject)) {
            return false;
        }
        this.x = jSONObject.optJSONObject("ux");
        if (this.x == null) {
            this.x = com.chartboost.sdk.Libraries.e.a(new e.a[0]);
        }
        if (this.d.isNull("video-landscape") || this.d.isNull("replay-landscape")) {
            this.i = false;
        }
        if (!this.E.a("replay-landscape") || !this.F.a("replay-portrait") || !this.I.a("video-click-button") || !this.J.a("post-video-reward-icon") || !this.K.a("post-video-button") || !this.G.a("video-confirmation-button") || !this.H.a("video-confirmation-icon") || !this.L.a("post-video-reward-icon")) {
            CBLogging.b("InterstitialVideoViewProtocol", "Error while downloading the assets");
            a(CBError.CBImpressionError.ASSETS_DOWNLOAD_FAILURE);
            return false;
        }
        this.y = this.x.optBoolean("video-controls-togglable");
        this.N = jSONObject.optBoolean("fullscreen");
        this.O = jSONObject.optBoolean("preroll_popup_fullscreen");
        if (this.e.n == 2) {
            JSONObject optJSONObject = this.x.optJSONObject("confirmation");
            JSONObject optJSONObject2 = this.x.optJSONObject("post-video-toaster");
            if (optJSONObject2 != null && !optJSONObject2.isNull("title") && !optJSONObject2.isNull("tagline")) {
                this.A = true;
            }
            if (optJSONObject != null && !optJSONObject.isNull(ViewHierarchyConstants.TEXT_KEY) && !optJSONObject.isNull(Constants.ParametersKeys.COLOR)) {
                this.z = true;
            }
            if (!this.x.isNull("post-video-reward-toaster")) {
                this.B = true;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.z && (!this.G.d() || !this.H.d())) {
            this.z = false;
        }
        super.i();
    }

    public void d() {
        super.d();
        this.E.c();
        this.F.c();
        this.I.c();
        this.J.c();
        this.K.c();
        this.G.c();
        this.H.c();
        this.L.c();
        this.E = null;
        this.F = null;
        this.I = null;
        this.J = null;
        this.K = null;
        this.G = null;
        this.H = null;
        this.L = null;
    }

    public boolean p() {
        return this.r == 1;
    }

    /* renamed from: q */
    public a e() {
        return (a) super.e();
    }

    /* access modifiers changed from: protected */
    public void r() {
        this.e.p();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x000b A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean s() {
        /*
            r4 = this;
            int r0 = r4.r
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x001c
            if (r0 == r2) goto L_0x000d
            r3 = 2
            if (r0 == r3) goto L_0x002b
        L_0x000b:
            r1 = 1
            goto L_0x002b
        L_0x000d:
            boolean r0 = r4.N
            if (r0 != 0) goto L_0x000b
            int r0 = com.chartboost.sdk.Libraries.CBUtility.a()
            boolean r0 = com.chartboost.sdk.Libraries.CBUtility.a(r0)
            if (r0 == 0) goto L_0x002b
            goto L_0x000b
        L_0x001c:
            boolean r0 = r4.O
            if (r0 != 0) goto L_0x000b
            int r0 = com.chartboost.sdk.Libraries.CBUtility.a()
            boolean r0 = com.chartboost.sdk.Libraries.CBUtility.a(r0)
            if (r0 == 0) goto L_0x002b
            goto L_0x000b
        L_0x002b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.v.s():boolean");
    }

    public boolean t() {
        return this.Q;
    }

    public void a(boolean z2) {
        this.Q = z2;
    }

    public boolean u() {
        return this.R;
    }

    public void v() {
        String str = this.t;
        if (str != null) {
            new File(str).delete();
        }
        this.R = true;
        a(CBError.CBImpressionError.ERROR_PLAYING_VIDEO);
    }

    public float j() {
        return (float) this.w;
    }

    public float k() {
        return (float) this.v;
    }
}
