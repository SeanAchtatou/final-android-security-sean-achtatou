package com.chartboost.sdk.impl;

import java.io.Serializable;
import java.io.Writer;

public class bk extends Writer implements Serializable {
    private final StringBuilder a;

    public void close() {
    }

    public void flush() {
    }

    public bk() {
        this.a = new StringBuilder();
    }

    public bk(int i) {
        this.a = new StringBuilder(i);
    }

    public Writer append(char c) {
        this.a.append(c);
        return this;
    }

    public Writer append(CharSequence charSequence) {
        this.a.append(charSequence);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.CharSequence, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public Writer append(CharSequence charSequence, int i, int i2) {
        this.a.append(charSequence, i, i2);
        return this;
    }

    public void write(String str) {
        if (str != null) {
            this.a.append(str);
        }
    }

    public void write(char[] cArr, int i, int i2) {
        if (cArr != null) {
            this.a.append(cArr, i, i2);
        }
    }

    public String toString() {
        return this.a.toString();
    }
}
