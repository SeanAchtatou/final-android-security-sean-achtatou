package com.chartboost.sdk.impl;

import android.os.Handler;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Tracking.a;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.Executor;

public class an<T> implements Comparable<an>, Runnable {
    public final ad<T> a;
    private final Executor b;
    private final ao c;
    private final ai d;
    private final i e;
    private final Handler f;
    private af<T> g;
    private ag h;

    private static boolean a(int i) {
        return ((100 <= i && i < 200) || i == 204 || i == 304) ? false : true;
    }

    an(Executor executor, ao aoVar, ai aiVar, i iVar, Handler handler, ad<T> adVar) {
        this.b = executor;
        this.c = aoVar;
        this.d = aiVar;
        this.e = iVar;
        this.f = handler;
        this.a = adVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.impl.ad.a(java.lang.Object, com.chartboost.sdk.impl.ag):void
     arg types: [T, com.chartboost.sdk.impl.ag]
     candidates:
      com.chartboost.sdk.impl.ad.a(com.chartboost.sdk.Model.CBError, com.chartboost.sdk.impl.ag):void
      com.chartboost.sdk.impl.ad.a(java.lang.Object, com.chartboost.sdk.impl.ag):void */
    public void run() {
        af<T> afVar = this.g;
        if (afVar != null) {
            try {
                if (afVar.b == null) {
                    this.a.a((Object) this.g.a, this.h);
                } else {
                    this.a.a(this.g.b, this.h);
                }
            } catch (Exception e2) {
                a.a(getClass(), "deliver result", e2);
            }
        } else if (this.a.e.compareAndSet(0, 1)) {
            long b2 = this.e.b();
            try {
                if (this.d.b()) {
                    this.h = a(this.a);
                    int i = this.h.a;
                    if (i < 200 || i >= 300) {
                        CBError.a aVar = CBError.a.NETWORK_FAILURE;
                        this.g = af.a(new CBError(aVar, "Failure due to HTTP status code " + i));
                    } else {
                        this.g = this.a.a(this.h);
                    }
                } else {
                    this.g = af.a(new CBError(CBError.a.INTERNET_UNAVAILABLE, "Internet Unavailable"));
                }
                this.a.g = this.e.b() - b2;
                int i2 = this.a.j;
                if (i2 != 0) {
                    if (i2 != 1) {
                        return;
                    }
                    this.b.execute(this);
                    return;
                }
            } catch (Throwable th) {
                this.a.g = this.e.b() - b2;
                int i3 = this.a.j;
                if (i3 == 0) {
                    this.f.post(this);
                } else if (i3 == 1) {
                    this.b.execute(this);
                }
                throw th;
            }
            this.f.post(this);
        }
    }

    private ag a(ad<T> adVar) throws IOException {
        int i = 10000;
        int i2 = 0;
        while (true) {
            try {
                return a(adVar, i);
            } catch (SocketTimeoutException e2) {
                if (i2 < 1) {
                    i *= 2;
                    i2++;
                } else {
                    throw e2;
                }
            }
        }
    }

    /* JADX WARN: Type inference failed for: r4v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r4v4 */
    /* JADX WARN: Type inference failed for: r4v5, types: [java.io.DataOutputStream] */
    /* JADX WARN: Type inference failed for: r4v6 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:80|81) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:79|(2:89|90)|91|92) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:20|21|(2:24|25)|26|27) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:62|63|(2:68|69)|(2:72|73)|74|75) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:36|37|(2:39|(13:41|42|43|44|45|46|47|(2:49|50)|51|52|53|54|(2:56|(2:58|59)(2:60|61)))(5:76|77|78|(1:83)(1:84)|(2:86|87)))(1:93)|94|95|96|97) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:46|47|(2:49|50)|51|52|53|54|(2:56|(2:58|59)(2:60|61))) */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        r0 = r2.getErrorStream();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x007d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00d4 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x00d7 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:74:0x014a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:80:0x0153 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:91:0x0171 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:94:0x0174 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007a A[SYNTHETIC, Splitter:B:24:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00df A[Catch:{ all -> 0x0151, all -> 0x0186 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0140 A[SYNTHETIC, Splitter:B:68:0x0140] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0147 A[SYNTHETIC, Splitter:B:72:0x0147] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:91:0x0171=Splitter:B:91:0x0171, B:53:0x00d7=Splitter:B:53:0x00d7, B:74:0x014a=Splitter:B:74:0x014a} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:94:0x0174=Splitter:B:94:0x0174, B:26:0x007d=Splitter:B:26:0x007d} */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.chartboost.sdk.impl.ag a(com.chartboost.sdk.impl.ad<T> r10, int r11) throws java.io.IOException {
        /*
            r9 = this;
            com.chartboost.sdk.impl.ae r0 = r10.a()
            java.util.Map<java.lang.String, java.lang.String> r1 = r0.a
            com.chartboost.sdk.impl.ao r2 = r9.c
            java.net.HttpURLConnection r2 = r2.a(r10)
            r2.setConnectTimeout(r11)
            r2.setReadTimeout(r11)
            r11 = 0
            r2.setUseCaches(r11)
            r3 = 1
            r2.setDoInput(r3)
            if (r1 == 0) goto L_0x003a
            java.util.Set r4 = r1.keySet()     // Catch:{ all -> 0x01a4 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x01a4 }
        L_0x0024:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x01a4 }
            if (r5 == 0) goto L_0x003a
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x01a4 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x01a4 }
            java.lang.Object r6 = r1.get(r5)     // Catch:{ all -> 0x01a4 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x01a4 }
            r2.addRequestProperty(r5, r6)     // Catch:{ all -> 0x01a4 }
            goto L_0x0024
        L_0x003a:
            java.lang.String r1 = r10.b     // Catch:{ all -> 0x01a4 }
            r2.setRequestMethod(r1)     // Catch:{ all -> 0x01a4 }
            java.lang.String r1 = r10.b     // Catch:{ all -> 0x01a4 }
            java.lang.String r4 = "POST"
            boolean r1 = r1.equals(r4)     // Catch:{ all -> 0x01a4 }
            r4 = 0
            if (r1 == 0) goto L_0x007e
            byte[] r1 = r0.b     // Catch:{ all -> 0x01a4 }
            if (r1 == 0) goto L_0x007e
            r2.setDoOutput(r3)     // Catch:{ all -> 0x01a4 }
            byte[] r1 = r0.b     // Catch:{ all -> 0x01a4 }
            int r1 = r1.length     // Catch:{ all -> 0x01a4 }
            r2.setFixedLengthStreamingMode(r1)     // Catch:{ all -> 0x01a4 }
            java.lang.String r1 = r0.c     // Catch:{ all -> 0x01a4 }
            if (r1 == 0) goto L_0x0062
            java.lang.String r1 = "Content-Type"
            java.lang.String r3 = r0.c     // Catch:{ all -> 0x01a4 }
            r2.addRequestProperty(r1, r3)     // Catch:{ all -> 0x01a4 }
        L_0x0062:
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ all -> 0x0077 }
            java.io.OutputStream r3 = r2.getOutputStream()     // Catch:{ all -> 0x0077 }
            r1.<init>(r3)     // Catch:{ all -> 0x0077 }
            byte[] r0 = r0.b     // Catch:{ all -> 0x0074 }
            r1.write(r0)     // Catch:{ all -> 0x0074 }
            r1.close()     // Catch:{ IOException -> 0x007e }
            goto L_0x007e
        L_0x0074:
            r10 = move-exception
            r4 = r1
            goto L_0x0078
        L_0x0077:
            r10 = move-exception
        L_0x0078:
            if (r4 == 0) goto L_0x007d
            r4.close()     // Catch:{ IOException -> 0x007d }
        L_0x007d:
            throw r10     // Catch:{ all -> 0x01a4 }
        L_0x007e:
            com.chartboost.sdk.Libraries.i r0 = r9.e     // Catch:{ all -> 0x01a4 }
            long r0 = r0.b()     // Catch:{ all -> 0x01a4 }
            int r3 = r2.getResponseCode()     // Catch:{ all -> 0x0199 }
            com.chartboost.sdk.Libraries.i r5 = r9.e     // Catch:{ all -> 0x01a4 }
            long r5 = r5.b()     // Catch:{ all -> 0x01a4 }
            long r0 = r5 - r0
            r10.h = r0     // Catch:{ all -> 0x01a4 }
            r0 = -1
            if (r3 == r0) goto L_0x0191
            boolean r0 = a(r3)     // Catch:{ all -> 0x0186 }
            if (r0 == 0) goto L_0x0172
            java.io.File r0 = r10.f     // Catch:{ all -> 0x0186 }
            if (r0 == 0) goto L_0x014b
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0186 }
            java.io.File r1 = r10.f     // Catch:{ all -> 0x0186 }
            java.io.File r1 = r1.getParentFile()     // Catch:{ all -> 0x0186 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0186 }
            r7.<init>()     // Catch:{ all -> 0x0186 }
            java.io.File r8 = r10.f     // Catch:{ all -> 0x0186 }
            java.lang.String r8 = r8.getName()     // Catch:{ all -> 0x0186 }
            r7.append(r8)     // Catch:{ all -> 0x0186 }
            java.lang.String r8 = ".tmp"
            r7.append(r8)     // Catch:{ all -> 0x0186 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0186 }
            r0.<init>(r1, r7)     // Catch:{ all -> 0x0186 }
            byte[] r11 = new byte[r11]     // Catch:{ all -> 0x0186 }
            java.io.InputStream r1 = r2.getInputStream()     // Catch:{ all -> 0x013c }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ all -> 0x013a }
            r7.<init>(r0)     // Catch:{ all -> 0x013a }
            com.chartboost.sdk.impl.bi.a(r1, r7)     // Catch:{ all -> 0x0137 }
            if (r1 == 0) goto L_0x00d4
            r1.close()     // Catch:{ IOException -> 0x00d4 }
        L_0x00d4:
            r7.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x00d7:
            java.io.File r1 = r10.f     // Catch:{ all -> 0x0186 }
            boolean r1 = r0.renameTo(r1)     // Catch:{ all -> 0x0186 }
            if (r1 != 0) goto L_0x0174
            boolean r11 = r0.delete()     // Catch:{ all -> 0x0186 }
            if (r11 != 0) goto L_0x010e
            java.io.IOException r11 = new java.io.IOException     // Catch:{ all -> 0x0186 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0186 }
            r1.<init>()     // Catch:{ all -> 0x0186 }
            java.lang.String r3 = "Unable to delete "
            r1.append(r3)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0186 }
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = " after failing to rename to "
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.io.File r0 = r10.f     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0186 }
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0186 }
            r11.<init>(r0)     // Catch:{ all -> 0x0186 }
            throw r11     // Catch:{ all -> 0x0186 }
        L_0x010e:
            java.io.IOException r11 = new java.io.IOException     // Catch:{ all -> 0x0186 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0186 }
            r1.<init>()     // Catch:{ all -> 0x0186 }
            java.lang.String r3 = "Unable to move "
            r1.append(r3)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0186 }
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = " to "
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.io.File r0 = r10.f     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0186 }
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0186 }
            r11.<init>(r0)     // Catch:{ all -> 0x0186 }
            throw r11     // Catch:{ all -> 0x0186 }
        L_0x0137:
            r11 = move-exception
            r4 = r7
            goto L_0x013e
        L_0x013a:
            r11 = move-exception
            goto L_0x013e
        L_0x013c:
            r11 = move-exception
            r1 = r4
        L_0x013e:
            if (r1 == 0) goto L_0x0145
            r1.close()     // Catch:{ IOException -> 0x0144 }
            goto L_0x0145
        L_0x0144:
        L_0x0145:
            if (r4 == 0) goto L_0x014a
            r4.close()     // Catch:{ IOException -> 0x014a }
        L_0x014a:
            throw r11     // Catch:{ all -> 0x0186 }
        L_0x014b:
            java.io.InputStream r0 = r2.getInputStream()     // Catch:{ IOException -> 0x0153 }
        L_0x014f:
            r4 = r0
            goto L_0x0158
        L_0x0151:
            r11 = move-exception
            goto L_0x016c
        L_0x0153:
            java.io.InputStream r0 = r2.getErrorStream()     // Catch:{ all -> 0x0151 }
            goto L_0x014f
        L_0x0158:
            if (r4 == 0) goto L_0x0164
            java.io.BufferedInputStream r11 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0151 }
            r11.<init>(r4)     // Catch:{ all -> 0x0151 }
            byte[] r11 = com.chartboost.sdk.impl.bi.b(r11)     // Catch:{ all -> 0x0151 }
            goto L_0x0166
        L_0x0164:
            byte[] r11 = new byte[r11]     // Catch:{ all -> 0x0151 }
        L_0x0166:
            if (r4 == 0) goto L_0x0174
            r4.close()     // Catch:{ IOException -> 0x0174 }
            goto L_0x0174
        L_0x016c:
            if (r4 == 0) goto L_0x0171
            r4.close()     // Catch:{ IOException -> 0x0171 }
        L_0x0171:
            throw r11     // Catch:{ all -> 0x0186 }
        L_0x0172:
            byte[] r11 = new byte[r11]     // Catch:{ all -> 0x0186 }
        L_0x0174:
            com.chartboost.sdk.Libraries.i r0 = r9.e     // Catch:{ all -> 0x01a4 }
            long r0 = r0.b()     // Catch:{ all -> 0x01a4 }
            long r0 = r0 - r5
            r10.i = r0     // Catch:{ all -> 0x01a4 }
            com.chartboost.sdk.impl.ag r10 = new com.chartboost.sdk.impl.ag     // Catch:{ all -> 0x01a4 }
            r10.<init>(r3, r11)     // Catch:{ all -> 0x01a4 }
            r2.disconnect()
            return r10
        L_0x0186:
            r11 = move-exception
            com.chartboost.sdk.Libraries.i r0 = r9.e     // Catch:{ all -> 0x01a4 }
            long r0 = r0.b()     // Catch:{ all -> 0x01a4 }
            long r0 = r0 - r5
            r10.i = r0     // Catch:{ all -> 0x01a4 }
            throw r11     // Catch:{ all -> 0x01a4 }
        L_0x0191:
            java.io.IOException r10 = new java.io.IOException     // Catch:{ all -> 0x01a4 }
            java.lang.String r11 = "Could not retrieve response code from HttpUrlConnection."
            r10.<init>(r11)     // Catch:{ all -> 0x01a4 }
            throw r10     // Catch:{ all -> 0x01a4 }
        L_0x0199:
            r11 = move-exception
            com.chartboost.sdk.Libraries.i r3 = r9.e     // Catch:{ all -> 0x01a4 }
            long r3 = r3.b()     // Catch:{ all -> 0x01a4 }
            long r3 = r3 - r0
            r10.h = r3     // Catch:{ all -> 0x01a4 }
            throw r11     // Catch:{ all -> 0x01a4 }
        L_0x01a4:
            r10 = move-exception
            r2.disconnect()
            goto L_0x01aa
        L_0x01a9:
            throw r10
        L_0x01aa:
            goto L_0x01a9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.impl.an.a(com.chartboost.sdk.impl.ad, int):com.chartboost.sdk.impl.ag");
    }

    /* renamed from: a */
    public int compareTo(an anVar) {
        return this.a.d - anVar.a.d;
    }
}
