package com.tapblaze.pizzabusiness;

import android.app.Activity;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import io.fabric.sdk.android.Fabric;

public class CrashlyticsWrapper {
    private static boolean initialized = false;

    public static void Init(Activity activity) {
        Fabric.with(activity, new Crashlytics(), new CrashlyticsNdk());
        initialized = true;
    }

    public static void log(String str) {
        if (initialized) {
            Crashlytics.log(str);
            Crashlytics.logException(new Throwable(str));
        }
    }

    public static void setBool(String str, boolean z) {
        if (initialized) {
            Crashlytics.setBool(str, z);
        }
    }

    public static void setFloat(String str, float f) {
        if (initialized) {
            Crashlytics.setFloat(str, f);
        }
    }

    public static void setInt(String str, int i) {
        if (initialized) {
            Crashlytics.setInt(str, i);
        }
    }

    public static void setUserID(String str) {
        if (initialized) {
            Crashlytics.setUserIdentifier(str);
        }
    }
}
