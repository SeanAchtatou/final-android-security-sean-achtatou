package com.tapblaze.pizzabusiness;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.kochava.base.Tracker;
import java.util.Arrays;
import java.util.HashMap;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

public class AppActivity extends Cocos2dxActivity {
    private static final int RC_ACHIEVEMENT_UI = 9003;
    private static final int RC_LEADERBOARD_UI = 9004;
    /* access modifiers changed from: private */
    public static int RC_SIGN_IN = 9001;
    /* access modifiers changed from: private */
    public static Activity activity = null;
    private static Context context = null;
    /* access modifiers changed from: private */
    public static String gameCenterID = "";
    private static Cocos2dxGLSurfaceView glSurfaceView = null;
    /* access modifiers changed from: private */
    public static GoogleSignInClient mGoogleSignInClient = null;
    private static boolean mSignInClicked = false;
    private static String serverAuthCode;

    public static native String getABTestGroupFromJava();

    public static String getPlatformCode() {
        return BuildConfig.FLAVOR;
    }

    /* access modifiers changed from: private */
    public static native void onLoginGooglePlay();

    /* access modifiers changed from: private */
    public static native void onLogoutGooglePlay();

    public static AppActivity getInstance() {
        return (AppActivity) activity;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.setEnableVirtualButton(false);
        super.onCreate(bundle);
        Log.d("cocos2d", "onCreate");
        if (isTaskRoot()) {
            activity = this;
            context = getContext();
            if (!isEUUser()) {
                CrashlyticsWrapper.Init(this);
            }
            activity = this;
            context = getContext();
            getWindow().setFlags(1024, 1024);
            PurchasesManager.initialize(this);
            if (!isEUUser()) {
                Tracker.configure(new Tracker.Configuration(getApplicationContext()).setAppGuid("kogood-pizza-great-pizza-google-play-an00k"));
            }
            getGLSurfaceView().setMultipleTouchEnabled(false);
            mGoogleSignInClient = GoogleSignIn.getClient(activity, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN).requestServerAuthCode(context.getString(R.string.webclient_id)).requestScopes(new Scope(Scopes.GAMES), new Scope[0]).requestProfile().build());
            serverAuthCode = "";
        }
    }

    public Cocos2dxGLSurfaceView onCreateView() {
        glSurfaceView = new Cocos2dxGLSurfaceView(this);
        uiHide();
        glSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 1);
        return glSurfaceView;
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            uiHide();
        }
    }

    private void uiHide() {
        if (Build.VERSION.SDK_INT >= 19) {
            glSurfaceView.setSystemUiVisibility(5894);
        }
    }

    public void onStart() {
        super.onStart();
    }

    public void onResume() {
        super.onResume();
        if (getPlatformCode().equalsIgnoreCase(BuildConfig.FLAVOR)) {
            uiHide();
        }
        IronSourceWrapper.onResume();
        PurchasesManager.onResume();
    }

    public void onPause() {
        super.onPause();
        IronSourceWrapper.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        PurchasesManager.release();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == RC_SIGN_IN) {
            GoogleSignInResult signInResultFromIntent = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
            if (signInResultFromIntent == null) {
                return;
            }
            if (signInResultFromIntent.isSuccess()) {
                onSignedIn(signInResultFromIntent.getSignInAccount());
            } else {
                getInstance().runOnGLThread(new Runnable() {
                    public void run() {
                        AppActivity.onLogoutGooglePlay();
                    }
                });
            }
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    public static void ShowToast(final String str, final int i) {
        getInstance().runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(AppActivity.getInstance(), str, i).show();
            }
        });
    }

    private static boolean findTwitterClient() {
        try {
            activity.getPackageManager().getApplicationInfo("com.twitter.android", 0);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static void shareTwitter() {
        if (!findTwitterClient()) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AppActivity.activity);
                    builder.setTitle("Good Pizza, Great Pizza").setMessage("Sorry, you need to install Twitter application first!").setNeutralButton("Ok", (DialogInterface.OnClickListener) null);
                    builder.show();
                }
            });
        } else if (isConnectedToInternet()) {
            Intent intent = new Intent("android.intent.action.SEND", Uri.parse("twitter:"));
            intent.setType("image/*");
            intent.putExtra("android.intent.extra.TEXT", activity.getResources().getString(R.string.twitter_share_message));
            intent.setPackage("com.twitter.android");
            activity.startActivity(intent);
        } else {
            ShowToast("No internet connection", 1);
        }
    }

    public static boolean sendMail(String str, String str2) {
        Intent intent = new Intent("android.intent.action.SEND", Uri.parse("mailto:"));
        intent.putExtra("android.intent.extra.SUBJECT", str2);
        intent.putExtra("android.intent.extra.EMAIL", new String[]{str});
        intent.setType("message/rfc822");
        try {
            activity.startActivity(Intent.createChooser(intent, "Select application"));
            return true;
        } catch (ActivityNotFoundException unused) {
            return false;
        }
    }

    public static String getVersionString() {
        return BuildConfig.VERSION_NAME + " (" + ((int) BuildConfig.VERSION_CODE) + ")";
    }

    public static boolean isConnectedToInternet() {
        NetworkInfo[] allNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) getInstance().getApplicationContext().getSystemService("connectivity");
        if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
            for (NetworkInfo state : allNetworkInfo) {
                if (state.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getServerAuthCode() {
        return serverAuthCode;
    }

    public static String getGameCenterID() {
        return gameCenterID;
    }

    public static String getCountryAndroid() {
        return context.getResources().getConfiguration().locale.getCountry();
    }

    public static boolean isEUUser() {
        return Arrays.asList(new String[]{"BE", "BG", "CZ", "DK", "DE", "EE", "IE", "EL", "ES", "FR", "HR", "IT", "CY", "LV", "LT", "LU", "HU", "MT", "NL", "AT", "PL", "PT", "RO", "SI", "SK", "FI", "SE", "UK"}).contains(getCountryAndroid());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x001a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void followInstagram(java.lang.String r2, java.lang.String r3) {
        /*
            android.net.Uri r2 = android.net.Uri.parse(r2)
            android.net.Uri r3 = android.net.Uri.parse(r3)
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "android.intent.action.VIEW"
            r0.<init>(r1, r2)
            java.lang.String r2 = "com.instagram.android"
            r0.setPackage(r2)
            android.app.Activity r2 = com.tapblaze.pizzabusiness.AppActivity.activity     // Catch:{ ActivityNotFoundException -> 0x001a }
            r2.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x001a }
            goto L_0x0024
        L_0x001a:
            android.app.Activity r2 = com.tapblaze.pizzabusiness.AppActivity.activity     // Catch:{ ActivityNotFoundException -> 0x0024 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x0024 }
            r0.<init>(r1, r3)     // Catch:{ ActivityNotFoundException -> 0x0024 }
            r2.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x0024 }
        L_0x0024:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapblaze.pizzabusiness.AppActivity.followInstagram(java.lang.String, java.lang.String):void");
    }

    public static void logoutGooglePlay() {
        handleSignOut();
    }

    public static void loginGooglePlay() {
        getInstance().runOnUiThread(new Runnable() {
            public void run() {
                AppActivity.signInSilently();
            }
        });
    }

    private static void handleSignOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            public void onComplete(Task<Void> task) {
                AppActivity.getInstance().runOnGLThread(new Runnable() {
                    public void run() {
                        AppActivity.onLogoutGooglePlay();
                    }
                });
            }
        });
    }

    private static boolean isSignedIn() {
        return GoogleSignIn.getLastSignedInAccount(activity) != null;
    }

    /* access modifiers changed from: private */
    public static void signInSilently() {
        mGoogleSignInClient.silentSignIn().addOnCompleteListener(activity, new OnCompleteListener<GoogleSignInAccount>() {
            public void onComplete(Task<GoogleSignInAccount> task) {
                if (task.isSuccessful()) {
                    AppActivity.onSignedIn(task.getResult());
                    return;
                }
                AppActivity.activity.startActivityForResult(AppActivity.mGoogleSignInClient.getSignInIntent(), AppActivity.RC_SIGN_IN);
            }
        });
    }

    public static void onSignedIn(GoogleSignInAccount googleSignInAccount) {
        Log.d("GPGS", "onConnected() called. Sign in successful!");
        serverAuthCode = googleSignInAccount.getServerAuthCode();
        Games.getPlayersClient(activity, googleSignInAccount).getCurrentPlayer().addOnCompleteListener(new OnCompleteListener<Player>() {
            public void onComplete(Task<Player> task) {
                try {
                    String unused = AppActivity.gameCenterID = task.getResult(ApiException.class).getPlayerId();
                    AppActivity.getInstance().runOnGLThread(new Runnable() {
                        public void run() {
                            AppActivity.onLoginGooglePlay();
                        }
                    });
                } catch (ApiException e) {
                    Log.e("GooglePlayGames", "Error getting player: " + e);
                }
            }
        });
    }

    public static void postScore(int i, String str) {
        if (isSignedIn()) {
            Activity activity2 = activity;
            Games.getLeaderboardsClient(activity2, GoogleSignIn.getLastSignedInAccount(activity2)).submitScore(str, (long) i);
        }
    }

    public static boolean showLeaderboard() {
        if (!isSignedIn()) {
            return false;
        }
        Activity activity2 = activity;
        Games.getLeaderboardsClient(activity2, GoogleSignIn.getLastSignedInAccount(activity2)).getAllLeaderboardsIntent().addOnSuccessListener(new OnSuccessListener<Intent>() {
            public void onSuccess(Intent intent) {
                AppActivity.activity.startActivityForResult(intent, 9004);
            }
        });
        return true;
    }

    public static boolean showAchievements() {
        if (!isSignedIn()) {
            return true;
        }
        Activity activity2 = activity;
        Games.getAchievementsClient(activity2, GoogleSignIn.getLastSignedInAccount(activity2)).getAchievementsIntent().addOnSuccessListener(new OnSuccessListener<Intent>() {
            public void onSuccess(Intent intent) {
                AppActivity.activity.startActivityForResult(intent, 9003);
            }
        });
        return true;
    }

    public static void postAchievement(String str, int i) {
        if (i >= 100 && isSignedIn()) {
            Activity activity2 = activity;
            Games.getAchievementsClient(activity2, GoogleSignIn.getLastSignedInAccount(activity2)).unlock(str);
        }
    }

    public static void unlockAchievement(String str) {
        if (isSignedIn()) {
            Activity activity2 = activity;
            Games.getAchievementsClient(activity2, GoogleSignIn.getLastSignedInAccount(activity2)).unlock(str);
        }
    }

    public static void submitEvent(String str, int i) {
        if (isSignedIn()) {
            Activity activity2 = activity;
            Games.getEventsClient(activity2, GoogleSignIn.getLastSignedInAccount(activity2)).increment(str, i);
        }
    }

    public static int getAndroidAPI() {
        return Build.VERSION.SDK_INT;
    }

    public static void initFlurry() {
        if (!isEUUser()) {
            FlurryAgent.Builder withContinueSessionMillis = new FlurryAgent.Builder().withLogEnabled(true).withCaptureUncaughtExceptions(true).withContinueSessionMillis(10000);
            Activity activity2 = activity;
            withContinueSessionMillis.build(activity2, activity2.getString(R.string.flurry_api_key));
            FlurryAgent.setVersionName(getVersionString() + " " + getABTestGroupFromJava());
        }
    }

    public static void logEvent(String str, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("Day", Integer.toString(i));
        FlurryAgent.logEvent(str, hashMap);
    }

    public static boolean isGooglePlayGamesInstalled() {
        try {
            activity.getPackageManager().getApplicationInfo(GooglePlayServicesUtilLight.GOOGLE_PLAY_GAMES_PACKAGE, 0);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }
}
