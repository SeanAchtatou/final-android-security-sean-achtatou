package com.tapblaze.pizzabusiness;

import android.content.Context;
import android.util.Log;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.kochava.base.Tracker;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

public class PurchasesManager implements PurchasesUpdatedListener, BillingClientStateListener, SkuDetailsResponseListener, ConsumeResponseListener {
    private static PurchasesManager instance;
    private boolean bConnected = false;
    /* access modifiers changed from: private */
    public BillingClient billingClient = null;
    private Context context = null;
    /* access modifiers changed from: private */
    public HashMap<String, SkuDetails> products;

    public static void onResume() {
    }

    /* access modifiers changed from: private */
    public static native void reportPrice(String str, String str2);

    /* access modifiers changed from: private */
    public static native void reportPurchaseFailed();

    /* access modifiers changed from: private */
    public static native void reportPurchaseWithJson(String str, String str2, String str3);

    public void onConsumeResponse(BillingResult billingResult, String str) {
    }

    public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> list) {
        if (billingResult.getResponseCode() == 0 && list != null) {
            for (Purchase handlePurchase : list) {
                handlePurchase(handlePurchase);
            }
        } else if (billingResult.getResponseCode() == 1) {
            AppActivity.getInstance().runOnGLThread(new Runnable() {
                public void run() {
                    PurchasesManager.reportPurchaseFailed();
                }
            });
        } else {
            AppActivity.getInstance().runOnGLThread(new Runnable() {
                public void run() {
                    PurchasesManager.reportPurchaseFailed();
                }
            });
        }
    }

    public void onBillingSetupFinished(BillingResult billingResult) {
        if (billingResult.getResponseCode() == 0) {
            this.bConnected = true;
        }
    }

    public void onBillingServiceDisconnected() {
        this.bConnected = false;
    }

    public void onSkuDetailsResponse(BillingResult billingResult, final List<SkuDetails> list) {
        if (billingResult.getResponseCode() == 0 && list != null) {
            AppActivity.getInstance().runOnGLThread(new Runnable() {
                public void run() {
                    for (SkuDetails skuDetails : list) {
                        PurchasesManager.this.products.put(skuDetails.getSku(), skuDetails);
                        Log.e("PurchasesManager", "SKU details for ID = " + skuDetails.getSku() + " received, price = " + skuDetails.getPrice());
                        PurchasesManager.reportPrice(PurchasesManager.this.purchaseIDFromSystemToGame(skuDetails.getSku()), skuDetails.getPrice());
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void handlePurchase(final Purchase purchase) {
        if (purchase.getPurchaseState() == 1) {
            this.billingClient.consumeAsync(ConsumeParams.newBuilder().setPurchaseToken(purchase.getPurchaseToken()).build(), this);
            AppActivity.getInstance().runOnGLThread(new Runnable() {
                public void run() {
                    String signature = purchase.getSignature();
                    PurchasesManager.reportPurchaseWithJson(PurchasesManager.this.purchaseIDFromSystemToGame(purchase.getSku()), purchase.getOriginalJson(), signature);
                }
            });
            TrackPurchase(purchase.getSku(), purchase.getSignature(), purchase.getOriginalJson(), purchase.getPurchaseToken());
            return;
        }
        purchase.getPurchaseState();
    }

    /* access modifiers changed from: private */
    public static PurchasesManager getInstance() {
        if (instance == null) {
            instance = new PurchasesManager();
        }
        return instance;
    }

    /* access modifiers changed from: private */
    public void initializeService(Context context2) {
        this.context = context2;
        this.products = new HashMap<>();
        this.billingClient = BillingClient.newBuilder(AppActivity.getInstance()).setListener(this).enablePendingPurchases().build();
        this.billingClient.startConnection(this);
    }

    private void releaseService() {
        BillingClient billingClient2 = this.billingClient;
        if (billingClient2 != null) {
            billingClient2.endConnection();
        }
    }

    private void purchaseInternal(final String str) {
        if (this.bConnected) {
            AppActivity.getInstance().runOnUiThread(new Runnable() {
                public void run() {
                    SkuDetails skuDetails = (SkuDetails) PurchasesManager.this.products.get(PurchasesManager.this.purchaseIDFromGameToSystem(str));
                    if (skuDetails != null) {
                        if (PurchasesManager.this.billingClient.launchBillingFlow(AppActivity.getInstance(), BillingFlowParams.newBuilder().setSkuDetails(skuDetails).build()).getResponseCode() != 0) {
                            AppActivity.getInstance().runOnGLThread(new Runnable() {
                                public void run() {
                                    PurchasesManager.reportPurchaseFailed();
                                }
                            });
                        }
                    }
                }
            });
        }
    }

    private void queryPurchasesInternal() {
        if (this.bConnected) {
            AppActivity.getInstance().runOnUiThread(new Runnable() {
                public void run() {
                    Purchase.PurchasesResult queryPurchases = PurchasesManager.this.billingClient.queryPurchases(BillingClient.SkuType.INAPP);
                    if (queryPurchases.getBillingResult().getResponseCode() == 0) {
                        for (Purchase next : queryPurchases.getPurchasesList()) {
                            if (next.getPurchaseState() == 1) {
                                PurchasesManager.this.handlePurchase(next);
                            }
                        }
                    }
                }
            });
        }
    }

    private void restorePurchasesInternal() {
        if (this.bConnected) {
            queryPurchasesInternal();
        }
    }

    private void requestPricesInternal(final String[] strArr) {
        if (this.bConnected) {
            AppActivity.getInstance().runOnUiThread(new Runnable() {
                public void run() {
                    ArrayList arrayList = new ArrayList();
                    for (String access$500 : strArr) {
                        arrayList.add(PurchasesManager.this.purchaseIDFromGameToSystem(access$500));
                    }
                    SkuDetailsParams.Builder newBuilder = SkuDetailsParams.newBuilder();
                    newBuilder.setSkusList(arrayList).setType(BillingClient.SkuType.INAPP);
                    PurchasesManager.this.billingClient.querySkuDetailsAsync(newBuilder.build(), PurchasesManager.getInstance());
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public String purchaseIDFromGameToSystem(String str) {
        if (str.equals("funds01")) {
            return this.context.getResources().getString(R.string.addfunds1_purchase);
        }
        if (str.equals("funds02")) {
            return this.context.getResources().getString(R.string.addfunds2_purchase);
        }
        if (str.equals("funds03")) {
            return this.context.getResources().getString(R.string.addfunds3_purchase);
        }
        if (str.equals("funds04")) {
            return this.context.getResources().getString(R.string.addfunds4_purchase);
        }
        if (str.equals("funds05")) {
            return this.context.getResources().getString(R.string.addfunds5_purchase);
        }
        if (str.equals("funds06")) {
            return this.context.getResources().getString(R.string.addfunds6_purchase);
        }
        if (str.equals("funds07")) {
            return this.context.getResources().getString(R.string.addfunds7_purchase);
        }
        if (str.equals("funds08")) {
            return this.context.getResources().getString(R.string.addfunds8_purchase);
        }
        if (str.equals("funds09")) {
            return this.context.getResources().getString(R.string.addfunds9_purchase);
        }
        if (str.equals("starterpack")) {
            return this.context.getResources().getString(R.string.starterpack);
        }
        if (str.equals("removeads")) {
            return this.context.getResources().getString(R.string.removeads_purchase);
        }
        if (str.equals("unlockall")) {
            return this.context.getResources().getString(R.string.unlockall_purchase);
        }
        if (str.equals("crescentcoins01")) {
            return this.context.getResources().getString(R.string.halloween_coins1_purchase);
        }
        if (str.equals("crescentcoins02")) {
            return this.context.getResources().getString(R.string.halloween_coins2_purchase);
        }
        if (str.equals("crescentcoins03")) {
            return this.context.getResources().getString(R.string.halloween_coins3_purchase);
        }
        return str.equals("draculainvitationletters") ? this.context.getResources().getString(R.string.halloween_tickets_purchase) : str;
    }

    /* access modifiers changed from: private */
    public String purchaseIDFromSystemToGame(String str) {
        if (str.equals(this.context.getResources().getString(R.string.addfunds1_purchase))) {
            return "funds01";
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds2_purchase))) {
            return "funds02";
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds3_purchase))) {
            return "funds03";
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds4_purchase))) {
            return "funds04";
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds5_purchase))) {
            return "funds05";
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds6_purchase))) {
            return "funds06";
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds7_purchase))) {
            return "funds07";
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds8_purchase))) {
            return "funds08";
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds9_purchase))) {
            return "funds09";
        }
        if (str.equals(this.context.getResources().getString(R.string.starterpack))) {
            return "starterpack";
        }
        if (str.equals(this.context.getResources().getString(R.string.removeads_purchase))) {
            return "removeads";
        }
        if (str.equals(this.context.getResources().getString(R.string.unlockall_purchase))) {
            return "unlockall";
        }
        if (str.equals(this.context.getResources().getString(R.string.halloween_coins1_purchase))) {
            return "crescentcoins01";
        }
        if (str.equals(this.context.getResources().getString(R.string.halloween_coins2_purchase))) {
            return "crescentcoins02";
        }
        if (str.equals(this.context.getResources().getString(R.string.halloween_coins3_purchase))) {
            return "crescentcoins03";
        }
        return str.equals(this.context.getResources().getString(R.string.halloween_tickets_purchase)) ? "draculainvitationletters" : str;
    }

    /* access modifiers changed from: private */
    public double getPriceFromProductId(String str) {
        if (str.equals(this.context.getResources().getString(R.string.addfunds1_purchase))) {
            return 0.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds2_purchase))) {
            return 1.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds3_purchase))) {
            return 4.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds4_purchase))) {
            return 2.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds5_purchase))) {
            return 9.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds6_purchase))) {
            return 2.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds7_purchase))) {
            return 4.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds8_purchase))) {
            return 9.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.addfunds9_purchase))) {
            return 24.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.starterpack)) || str.equals(this.context.getResources().getString(R.string.removeads_purchase))) {
            return 1.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.unlockall_purchase))) {
            return 4.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.halloween_coins1_purchase))) {
            return 1.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.halloween_coins2_purchase))) {
            return 2.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.halloween_coins3_purchase))) {
            return 4.99d;
        }
        if (str.equals(this.context.getResources().getString(R.string.halloween_tickets_purchase))) {
            return 0.99d;
        }
        return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
    }

    private void TrackPurchase(String str, String str2, String str3, String str4) {
        if (!AppActivity.isEUUser()) {
            final String str5 = str;
            final String str6 = str4;
            final String str7 = str3;
            final String str8 = str2;
            new Thread(new Runnable() {
                public void run() {
                    try {
                        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) new URL("https://www.autxo.com/verification/play.php?package_name=com.tapblaze.pizzabusiness&product_id=" + str5 + "&purchase_token=" + str6).openConnection();
                        httpsURLConnection.setRequestMethod("GET");
                        httpsURLConnection.setReadTimeout(10000);
                        httpsURLConnection.connect();
                        if (httpsURLConnection.getResponseCode() == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
                            String str = "";
                            while (true) {
                                String readLine = bufferedReader.readLine();
                                if (readLine != null) {
                                    str = str + readLine;
                                } else {
                                    bufferedReader.close();
                                    try {
                                        new JSONObject(str);
                                        Tracker.sendEvent(new Tracker.Event(6).setPrice(PurchasesManager.this.getPriceFromProductId(str5)).setName(str5).setGooglePlayReceipt(str7, str8));
                                        return;
                                    } catch (JSONException e) {
                                        Log.e("SYNC getUpdate", "json error", e);
                                        return;
                                    }
                                }
                            }
                        } else {
                            httpsURLConnection.disconnect();
                        }
                    } catch (MalformedURLException e2) {
                        Log.e("SYNC getUpdate", "malformed url error", e2);
                    } catch (IOException e3) {
                        Log.e("SYNC getUpdate", "io error", e3);
                    } catch (SecurityException e4) {
                        Log.e("SYNC getUpdate", "security error", e4);
                    }
                }
            }).start();
        }
    }

    public static void initialize(final Context context2) {
        AppActivity.getInstance().runOnUiThread(new Runnable() {
            public void run() {
                PurchasesManager.getInstance().initializeService(context2);
            }
        });
    }

    public static void release() {
        getInstance().releaseService();
    }

    public static void purchase(String str) {
        getInstance().purchaseInternal(str);
    }

    public static void restorePurchases() {
        if (!AppActivity.isConnectedToInternet()) {
            AppActivity.ShowToast("No internet connection", 1);
        } else {
            getInstance().restorePurchasesInternal();
        }
    }

    public static void requestPrices(String[] strArr) {
        getInstance().requestPricesInternal(strArr);
    }
}
