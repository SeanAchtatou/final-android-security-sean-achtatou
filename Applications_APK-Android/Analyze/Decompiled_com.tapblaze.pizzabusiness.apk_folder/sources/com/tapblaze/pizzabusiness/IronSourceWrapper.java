package com.tapblaze.pizzabusiness;

import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;

public class IronSourceWrapper {
    /* access modifiers changed from: private */
    public static native void onInitialized();

    /* access modifiers changed from: private */
    public static native void onInterstitialDidFailToShow();

    /* access modifiers changed from: private */
    public static native void onRewardedVideoDidFailToShow();

    /* access modifiers changed from: private */
    public static native void onVideoEnded();

    /* access modifiers changed from: private */
    public static native void onVideoReady();

    /* access modifiers changed from: private */
    public static native void onVideoStarted();

    /* access modifiers changed from: private */
    public static native void onVideoWatched(String str);

    public static boolean isRewardedVideoReady() {
        return IronSource.isRewardedVideoAvailable();
    }

    public static void showRewardedVideo(final String str) {
        if (isRewardedVideoReady()) {
            AppActivity.getInstance().runOnUiThread(new Runnable() {
                public void run() {
                    IronSource.showRewardedVideo(str);
                }
            });
        } else {
            AppActivity.getInstance().runOnGLThread(new Runnable() {
                public void run() {
                    IronSourceWrapper.onVideoEnded();
                }
            });
        }
    }

    public static void loadInterstitialVideo() {
        IronSource.loadInterstitial();
    }

    public static boolean isInterstitialVideoReady() {
        return IronSource.isInterstitialReady();
    }

    public static void showInterstitialVideo(final String str) {
        if (isInterstitialVideoReady()) {
            AppActivity.getInstance().runOnUiThread(new Runnable() {
                public void run() {
                    IronSource.showInterstitial(str);
                }
            });
        } else {
            AppActivity.getInstance().runOnGLThread(new Runnable() {
                public void run() {
                    IronSourceWrapper.onVideoEnded();
                }
            });
        }
    }

    public static void Initialize(final String str, final boolean z, final int i) {
        AppActivity.getInstance().runOnUiThread(new Runnable() {
            public void run() {
                IronSource.setConsent(!z);
                IronSource.setAge(i);
                IronSource.setRewardedVideoListener(new RewardedVideoListener() {
                    public void onRewardedVideoAdClicked(Placement placement) {
                    }

                    public void onRewardedVideoAdEnded() {
                    }

                    public void onRewardedVideoAdStarted() {
                    }

                    public void onRewardedVideoAdOpened() {
                        AppActivity.getInstance().runOnGLThread(new Runnable() {
                            public void run() {
                                IronSourceWrapper.onVideoStarted();
                            }
                        });
                    }

                    public void onRewardedVideoAdClosed() {
                        AppActivity.getInstance().runOnGLThread(new Runnable() {
                            public void run() {
                                IronSourceWrapper.onVideoEnded();
                            }
                        });
                    }

                    public void onRewardedVideoAvailabilityChanged(boolean z) {
                        if (z) {
                            AppActivity.getInstance().runOnGLThread(new Runnable() {
                                public void run() {
                                    IronSourceWrapper.onVideoReady();
                                }
                            });
                        }
                    }

                    public void onRewardedVideoAdRewarded(final Placement placement) {
                        AppActivity.getInstance().runOnGLThread(new Runnable() {
                            public void run() {
                                IronSourceWrapper.onVideoWatched(placement.getPlacementName());
                            }
                        });
                    }

                    public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {
                        AppActivity.getInstance().runOnGLThread(new Runnable() {
                            public void run() {
                                IronSourceWrapper.onRewardedVideoDidFailToShow();
                            }
                        });
                    }
                });
                IronSource.setInterstitialListener(new InterstitialListener() {
                    public void onInterstitialAdClicked() {
                    }

                    public void onInterstitialAdClosed() {
                    }

                    public void onInterstitialAdLoadFailed(IronSourceError ironSourceError) {
                    }

                    public void onInterstitialAdOpened() {
                    }

                    public void onInterstitialAdReady() {
                    }

                    public void onInterstitialAdShowSucceeded() {
                    }

                    public void onInterstitialAdShowFailed(IronSourceError ironSourceError) {
                        AppActivity.getInstance().runOnGLThread(new Runnable() {
                            public void run() {
                                IronSourceWrapper.onInterstitialDidFailToShow();
                            }
                        });
                    }
                });
                IronSource.init(AppActivity.getInstance(), str, IronSource.AD_UNIT.REWARDED_VIDEO, IronSource.AD_UNIT.INTERSTITIAL);
                AppActivity.getInstance().runOnGLThread(new Runnable() {
                    public void run() {
                        IronSourceWrapper.onInitialized();
                    }
                });
            }
        });
    }

    public static void onPause() {
        IronSource.onPause(AppActivity.getInstance());
    }

    public static void onResume() {
        IronSource.onResume(AppActivity.getInstance());
    }
}
