package com.tapblaze.pizzabusiness;

import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;

public class FirebaseWrapper {
    private static boolean firebaseinitialized = false;
    private static FirebaseAnalytics mFirebaseAnalytics;

    public static void InitFirebase() {
        if (!firebaseinitialized) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(AppActivity.getContext());
            mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
            firebaseinitialized = true;
        }
    }

    public static void SetUserID(String str) {
        if (firebaseinitialized) {
            mFirebaseAnalytics.setUserId(str);
        }
    }

    public static void LogFirebaseEvent(String str, String str2, String str3) {
        if (firebaseinitialized) {
            Bundle bundle = new Bundle();
            bundle.putString(str2, str3);
            mFirebaseAnalytics.logEvent(str, bundle);
        }
    }

    public static void LogFirebaseEvent(String str, String str2, String str3, String str4, String str5) {
        if (firebaseinitialized) {
            Bundle bundle = new Bundle();
            bundle.putString(str2, str3);
            bundle.putString(str4, str5);
            mFirebaseAnalytics.logEvent(str, bundle);
        }
    }

    public static void LogFirebaseEvent(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        if (firebaseinitialized) {
            Bundle bundle = new Bundle();
            bundle.putString(str2, str3);
            bundle.putString(str4, str5);
            bundle.putString(str6, str7);
            mFirebaseAnalytics.logEvent(str, bundle);
        }
    }

    public static void LogFirebaseLevelUp(int i) {
        if (firebaseinitialized) {
            Bundle bundle = new Bundle();
            bundle.putLong("level", (long) i);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_UP, bundle);
        }
    }

    public static void LogFirebaseContentSelect(String str, String str2) {
        if (firebaseinitialized) {
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, str);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str2);
            bundle.putString("item_id", str2);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        }
    }

    public static void LogFirebaseSpendCurrency(String str, int i) {
        if (firebaseinitialized) {
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME, str);
            bundle.putFloat("value", (float) (i / 100));
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY, bundle);
        }
    }
}
