package com.tapblaze.pizzabusiness;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.tapblaze.pizzabusiness";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "google";
    public static final int VERSION_CODE = 337;
    public static final String VERSION_NAME = "3.3.7";
}
