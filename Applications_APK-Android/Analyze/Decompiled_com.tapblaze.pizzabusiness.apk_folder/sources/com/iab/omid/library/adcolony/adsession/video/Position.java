package com.iab.omid.library.adcolony.adsession.video;

import com.vungle.warren.model.Advertisement;

public enum Position {
    PREROLL("preroll"),
    MIDROLL("midroll"),
    POSTROLL(Advertisement.KEY_POSTROLL),
    STANDALONE("standalone");
    
    private final String a;

    private Position(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
