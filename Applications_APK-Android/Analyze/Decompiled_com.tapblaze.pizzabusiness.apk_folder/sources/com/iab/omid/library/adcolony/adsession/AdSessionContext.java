package com.iab.omid.library.adcolony.adsession;

import android.webkit.WebView;
import com.iab.omid.library.adcolony.d.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class AdSessionContext {
    private final Partner a;
    private final WebView b;
    private final List<VerificationScriptResource> c = new ArrayList();
    private final String d;
    private final String e;
    private final AdSessionContextType f;

    private AdSessionContext(Partner partner, WebView webView, String str, List<VerificationScriptResource> list, String str2) {
        AdSessionContextType adSessionContextType;
        this.a = partner;
        this.b = webView;
        this.d = str;
        if (list != null) {
            this.c.addAll(list);
            adSessionContextType = AdSessionContextType.NATIVE;
        } else {
            adSessionContextType = AdSessionContextType.HTML;
        }
        this.f = adSessionContextType;
        this.e = str2;
    }

    public static AdSessionContext createHtmlAdSessionContext(Partner partner, WebView webView, String str) {
        e.a(partner, "Partner is null");
        e.a(webView, "WebView is null");
        if (str != null) {
            e.a(str, 256, "CustomReferenceData is greater than 256 characters");
        }
        return new AdSessionContext(partner, webView, null, null, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.iab.omid.library.adcolony.d.e.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.iab.omid.library.adcolony.d.e.a(java.lang.String, java.lang.String):void
      com.iab.omid.library.adcolony.d.e.a(java.lang.Object, java.lang.String):void */
    public static AdSessionContext createNativeAdSessionContext(Partner partner, String str, List<VerificationScriptResource> list, String str2) {
        e.a(partner, "Partner is null");
        e.a((Object) str, "OM SDK JS script content is null");
        e.a(list, "VerificationScriptResources is null");
        if (str2 != null) {
            e.a(str2, 256, "CustomReferenceData is greater than 256 characters");
        }
        return new AdSessionContext(partner, null, str, list, str2);
    }

    public AdSessionContextType getAdSessionContextType() {
        return this.f;
    }

    public String getCustomReferenceData() {
        return this.e;
    }

    public String getOmidJsScriptContent() {
        return this.d;
    }

    public Partner getPartner() {
        return this.a;
    }

    public List<VerificationScriptResource> getVerificationScriptResources() {
        return Collections.unmodifiableList(this.c);
    }

    public WebView getWebView() {
        return this.b;
    }
}
