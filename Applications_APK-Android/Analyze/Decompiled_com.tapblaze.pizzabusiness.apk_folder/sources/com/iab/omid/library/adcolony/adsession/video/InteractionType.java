package com.iab.omid.library.adcolony.adsession.video;

public enum InteractionType {
    CLICK("click"),
    INVITATION_ACCEPTED("invitationAccept");
    
    String a;

    private InteractionType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
