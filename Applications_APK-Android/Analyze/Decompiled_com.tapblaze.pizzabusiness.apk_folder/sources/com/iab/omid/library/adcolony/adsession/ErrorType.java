package com.iab.omid.library.adcolony.adsession;

import com.facebook.share.internal.MessengerShareContentUtility;

public enum ErrorType {
    GENERIC(MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE),
    VIDEO("video");
    
    private final String a;

    private ErrorType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
