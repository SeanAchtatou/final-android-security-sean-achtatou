package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.b;
import com.iab.omid.library.adcolony.d.e;
import org.json.JSONObject;

public class AdSessionConfiguration {
    private final Owner a;
    private final Owner b;
    private final boolean c;

    private AdSessionConfiguration(Owner owner, Owner owner2, boolean z) {
        this.a = owner;
        if (owner2 == null) {
            this.b = Owner.NONE;
        } else {
            this.b = owner2;
        }
        this.c = z;
    }

    @Deprecated
    public static AdSessionConfiguration createAdSessionConfiguration(Owner owner, Owner owner2) {
        return createAdSessionConfiguration(owner, owner2, true);
    }

    public static AdSessionConfiguration createAdSessionConfiguration(Owner owner, Owner owner2, boolean z) {
        e.a(owner, "Impression owner is null");
        e.a(owner);
        return new AdSessionConfiguration(owner, owner2, z);
    }

    public boolean isNativeImpressionOwner() {
        return Owner.NATIVE == this.a;
    }

    public boolean isNativeVideoEventsOwner() {
        return Owner.NATIVE == this.b;
    }

    public JSONObject toJsonObject() {
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "impressionOwner", this.a);
        b.a(jSONObject, "videoEventsOwner", this.b);
        b.a(jSONObject, "isolateVerificationScripts", Boolean.valueOf(this.c));
        return jSONObject;
    }
}
