package com.iab.omid.library.adcolony.adsession;

import com.facebook.internal.AnalyticsEvents;

public enum AdSessionContextType {
    HTML("html"),
    NATIVE(AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_NATIVE);
    
    private final String a;

    private AdSessionContextType(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
