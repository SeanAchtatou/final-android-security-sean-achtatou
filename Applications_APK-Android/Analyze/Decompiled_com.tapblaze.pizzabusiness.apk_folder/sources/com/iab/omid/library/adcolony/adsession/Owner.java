package com.iab.omid.library.adcolony.adsession;

import com.facebook.internal.AnalyticsEvents;
import com.ironsource.sdk.constants.Constants;

public enum Owner {
    NATIVE(AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_NATIVE),
    JAVASCRIPT("javascript"),
    NONE(Constants.ParametersKeys.ORIENTATION_NONE);
    
    private final String a;

    private Owner(String str) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }
}
