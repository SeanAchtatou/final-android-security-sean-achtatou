package com.iab.omid.library.adcolony;

import android.content.Context;

public final class Omid {
    private static a a = new a();

    private Omid() {
    }

    public static boolean activateWithOmidApiVersion(String str, Context context) {
        a.a(context.getApplicationContext());
        return true;
    }

    public static String getVersion() {
        return a.a();
    }

    public static boolean isActive() {
        return a.b();
    }

    public static boolean isCompatibleWithOmidApiVersion(String str) {
        return a.a(str);
    }
}
