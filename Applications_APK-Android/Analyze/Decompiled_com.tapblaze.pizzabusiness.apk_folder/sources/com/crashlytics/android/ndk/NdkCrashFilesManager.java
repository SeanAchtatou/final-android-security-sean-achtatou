package com.crashlytics.android.ndk;

import io.fabric.sdk.android.services.common.SystemCurrentTimeProvider;
import io.fabric.sdk.android.services.persistence.FileStore;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;

class NdkCrashFilesManager implements CrashFilesManager {
    private static final String NATIVE_ROOT_DIRECTORY_SUFFIX = "native";
    private static final FileFilter ONLY_DIRECTORIES_FILTER = new FileFilter() {
        public boolean accept(File file) {
            return file.isDirectory();
        }
    };
    private final FileStore fileStore;

    public NdkCrashFilesManager(FileStore fileStore2) {
        this.fileStore = fileStore2;
    }

    private File getNativeRootFileDirectory() {
        return new File(this.fileStore.getFilesDir(), "native");
    }

    public File getNewNativeDirectory() {
        File nativeRootFileDirectory = getNativeRootFileDirectory();
        if (!nativeRootFileDirectory.isDirectory() && !nativeRootFileDirectory.mkdir()) {
            return null;
        }
        File file = new File(nativeRootFileDirectory, Long.toString(new SystemCurrentTimeProvider().getCurrentTimeMillis()));
        if (file.mkdir()) {
            return file;
        }
        return null;
    }

    private static TreeSet<File> getAllTimestampedDirectories(File file) {
        if (file == null || !file.isDirectory()) {
            return new TreeSet<>();
        }
        File[] listFiles = file.listFiles(ONLY_DIRECTORIES_FILTER);
        TreeSet<File> treeSet = new TreeSet<>(new Comparator<File>() {
            public int compare(File file, File file2) {
                return file2.getName().compareTo(file.getName());
            }
        });
        treeSet.addAll(Arrays.asList(listFiles));
        return treeSet;
    }

    public TreeSet<File> getAllNativeDirectories() {
        return getAllTimestampedDirectories(getNativeRootFileDirectory());
    }
}
