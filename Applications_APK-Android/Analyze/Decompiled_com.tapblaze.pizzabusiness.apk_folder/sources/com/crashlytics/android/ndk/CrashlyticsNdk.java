package com.crashlytics.android.ndk;

import com.crashlytics.android.core.CrashlyticsCore;
import com.crashlytics.android.core.CrashlyticsKitBinder;
import com.crashlytics.android.core.CrashlyticsNdkData;
import com.crashlytics.android.core.CrashlyticsNdkDataProvider;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import io.fabric.sdk.android.services.persistence.FileStoreImpl;
import java.io.IOException;

public class CrashlyticsNdk extends Kit<Void> implements CrashlyticsNdkDataProvider {
    static final String TAG = "CrashlyticsNdk";
    private NdkKitController controller;
    private CrashlyticsNdkData crashlyticsNdkData;

    public String getIdentifier() {
        return "com.crashlytics.sdk.android.crashlytics-ndk";
    }

    public String getVersion() {
        return "2.1.1.36";
    }

    public static CrashlyticsNdk getInstance() {
        return (CrashlyticsNdk) Fabric.getKit(CrashlyticsNdk.class);
    }

    /* access modifiers changed from: protected */
    public boolean onPreExecute() {
        CrashlyticsCore crashlyticsCore = (CrashlyticsCore) Fabric.getKit(CrashlyticsCore.class);
        if (crashlyticsCore != null) {
            return onPreExecute(new BreakpadController(getContext(), new JniNativeApi(), new NdkCrashFilesManager(new FileStoreImpl(this))), crashlyticsCore, new CrashlyticsKitBinder());
        }
        throw new UnmetDependencyException("CrashlyticsNdk requires Crashlytics");
    }

    /* access modifiers changed from: package-private */
    public boolean onPreExecute(NdkKitController ndkKitController, CrashlyticsCore crashlyticsCore, CrashlyticsKitBinder crashlyticsKitBinder) {
        this.controller = ndkKitController;
        boolean initialize = ndkKitController.initialize();
        if (initialize) {
            crashlyticsKitBinder.bindCrashEventDataProvider(crashlyticsCore, this);
        }
        Logger logger = Fabric.getLogger();
        StringBuilder sb = new StringBuilder();
        sb.append("Crashlytics NDK initialization ");
        sb.append(initialize ? "successful" : "FAILED");
        logger.d(TAG, sb.toString());
        return initialize;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground() {
        try {
            this.crashlyticsNdkData = this.controller.getNativeData();
            return null;
        } catch (IOException e) {
            Fabric.getLogger().e(TAG, "Could not process ndk data; ", e);
            return null;
        }
    }

    public CrashlyticsNdkData getCrashlyticsNdkData() {
        return this.crashlyticsNdkData;
    }
}
