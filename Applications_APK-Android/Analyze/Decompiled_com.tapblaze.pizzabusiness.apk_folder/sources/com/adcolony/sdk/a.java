package com.adcolony.sdk;

import android.content.Context;
import com.adcolony.sdk.w;
import java.lang.ref.WeakReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class a {
    static boolean a;
    static boolean b;
    private static WeakReference<Context> c;
    /* access modifiers changed from: private */
    public static j d;

    a() {
    }

    static void a(final Context context, AdColonyAppOptions adColonyAppOptions, boolean z) {
        a(context);
        b = true;
        j jVar = d;
        if (jVar == null) {
            d = new j();
            d.a(adColonyAppOptions, z);
        } else {
            jVar.a(adColonyAppOptions);
        }
        at.b.execute(new Runnable() {
            public void run() {
                a.d.a(context, (ab) null);
            }
        });
        new w.a().a("Configuring AdColony").a(w.c);
        d.b(false);
        d.k().d(true);
        d.k().e(true);
        d.k().f(false);
        j jVar2 = d;
        jVar2.f = true;
        jVar2.k().a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.j.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void
     arg types: [com.adcolony.sdk.AdColonyAppOptions, int]
     candidates:
      com.adcolony.sdk.j.a(com.adcolony.sdk.j, com.iab.omid.library.adcolony.adsession.Partner):com.iab.omid.library.adcolony.adsession.Partner
      com.adcolony.sdk.j.a(com.adcolony.sdk.j, com.adcolony.sdk.ab):void
      com.adcolony.sdk.j.a(com.adcolony.sdk.j, boolean):boolean
      com.adcolony.sdk.j.a(boolean, boolean):boolean
      com.adcolony.sdk.j.a(android.content.Context, com.adcolony.sdk.ab):boolean
      com.adcolony.sdk.j.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void */
    static j a() {
        if (!b()) {
            Context c2 = c();
            if (c2 == null) {
                return new j();
            }
            d = new j();
            JSONObject c3 = u.c(c2.getFilesDir().getAbsolutePath() + "/adc3/AppInfo");
            JSONArray g = u.g(c3, "zoneIds");
            d.a(new AdColonyAppOptions().a(u.b(c3, "appId")).a(u.a(g)), false);
        }
        return d;
    }

    static boolean b() {
        return d != null;
    }

    static void a(Context context) {
        if (context == null) {
            c.clear();
        } else {
            c = new WeakReference<>(context);
        }
    }

    static Context c() {
        WeakReference<Context> weakReference = c;
        if (weakReference == null) {
            return null;
        }
        return weakReference.get();
    }

    static boolean d() {
        WeakReference<Context> weakReference = c;
        return (weakReference == null || weakReference.get() == null) ? false : true;
    }

    static boolean e() {
        return a;
    }

    static void a(String str, ad adVar) {
        a().q().a(str, adVar);
    }

    static ad a(String str, ad adVar, boolean z) {
        a().q().a(str, adVar);
        return adVar;
    }

    static void b(String str, ad adVar) {
        a().q().b(str, adVar);
    }

    static void f() {
        a().q().b();
    }

    static void a(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = u.a();
        }
        u.a(jSONObject, "m_type", str);
        a().q().a(jSONObject);
    }

    static void a(String str) {
        try {
            ab abVar = new ab("CustomMessage.send", 0);
            abVar.c().put("message", str);
            abVar.b();
        } catch (JSONException e) {
            new w.a().a("JSON error from ADC.java's send_custom_message(): ").a(e.toString()).a(w.h);
        }
    }
}
