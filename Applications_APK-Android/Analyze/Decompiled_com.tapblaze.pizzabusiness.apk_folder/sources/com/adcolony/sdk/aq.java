package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Vibrator;
import androidx.core.app.NotificationCompat;
import com.adcolony.sdk.w;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.google.android.gms.drive.DriveFile;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpStatus;
import cz.msebera.android.httpclient.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

class aq {
    aq() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a.a("System.open_store", new ad() {
            public void a(ab abVar) {
                aq.this.b(abVar);
            }
        });
        a.a("System.save_screenshot", new ad() {
            public void a(ab abVar) {
                aq.this.c(abVar);
            }
        });
        a.a("System.telephone", new ad() {
            public void a(ab abVar) {
                aq.this.d(abVar);
            }
        });
        a.a("System.sms", new ad() {
            public void a(ab abVar) {
                aq.this.e(abVar);
            }
        });
        a.a("System.vibrate", new ad() {
            public void a(ab abVar) {
                aq.this.f(abVar);
            }
        });
        a.a("System.open_browser", new ad() {
            public void a(ab abVar) {
                aq.this.g(abVar);
            }
        });
        a.a("System.mail", new ad() {
            public void a(ab abVar) {
                aq.this.h(abVar);
            }
        });
        a.a("System.launch_app", new ad() {
            public void a(ab abVar) {
                aq.this.i(abVar);
            }
        });
        a.a("System.create_calendar_event", new ad() {
            public void a(ab abVar) {
                aq.this.j(abVar);
            }
        });
        a.a("System.check_app_presence", new ad() {
            public void a(ab abVar) {
                aq.this.k(abVar);
            }
        });
        a.a("System.check_social_presence", new ad() {
            public void a(ab abVar) {
                aq.this.l(abVar);
            }
        });
        a.a("System.social_post", new ad() {
            public void a(ab abVar) {
                aq.this.m(abVar);
            }
        });
        a.a("System.make_in_app_purchase", new ad() {
            public void a(ab abVar) {
                boolean unused = aq.this.q(abVar);
            }
        });
        a.a("System.close", new ad() {
            public void a(ab abVar) {
                boolean unused = aq.this.p(abVar);
            }
        });
        a.a("System.expand", new ad() {
            public void a(ab abVar) {
                aq.this.a(abVar);
            }
        });
        a.a("System.use_custom_close", new ad() {
            public void a(ab abVar) {
                boolean unused = aq.this.o(abVar);
            }
        });
        a.a("System.set_orientation_properties", new ad() {
            public void a(ab abVar) {
                boolean unused = aq.this.n(abVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean n(ab abVar) {
        JSONObject c = abVar.c();
        String b = u.b(c, "ad_session_id");
        int c2 = u.c(c, "orientation");
        d l = a.a().l();
        AdColonyAdView adColonyAdView = l.e().get(b);
        AdColonyInterstitial adColonyInterstitial = l.c().get(b);
        Context c3 = a.c();
        if (adColonyAdView != null) {
            adColonyAdView.setOrientation(c2);
        } else if (adColonyInterstitial != null) {
            adColonyInterstitial.b(c2);
        }
        if (!(c3 instanceof b)) {
            return true;
        }
        ((b) c3).a(adColonyAdView == null ? adColonyInterstitial.e() : adColonyAdView.getOrientation());
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(ab abVar) {
        JSONObject c = abVar.c();
        Context c2 = a.c();
        if (c2 != null && a.b()) {
            String b = u.b(c, "ad_session_id");
            j a = a.a();
            AdColonyAdView adColonyAdView = a.l().e().get(b);
            if (adColonyAdView != null && ((adColonyAdView.getTrustedDemandSource() || adColonyAdView.getUserInteraction()) && a.u() != adColonyAdView)) {
                adColonyAdView.setExpandMessage(abVar);
                adColonyAdView.setExpandedWidth(u.c(c, "width"));
                adColonyAdView.setExpandedHeight(u.c(c, "height"));
                adColonyAdView.setOrientation(u.a(c, "orientation", -1));
                adColonyAdView.setNoCloseButton(u.d(c, "use_custom_close"));
                a.a(adColonyAdView);
                a.a(adColonyAdView.getContainer());
                Intent intent = new Intent(c2, AdColonyAdViewActivity.class);
                if (c2 instanceof Application) {
                    intent.addFlags(DriveFile.MODE_READ_ONLY);
                }
                c(b);
                b(b);
                c2.startActivity(intent);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean o(ab abVar) {
        AdColonyAdView adColonyAdView = a.a().l().e().get(u.b(abVar.c(), "ad_session_id"));
        if (adColonyAdView == null) {
            return false;
        }
        adColonyAdView.setNoCloseButton(u.d(abVar.c(), "use_custom_close"));
        return true;
    }

    /* access modifiers changed from: private */
    public boolean p(ab abVar) {
        String b = u.b(abVar.c(), "ad_session_id");
        Activity activity = a.c() instanceof Activity ? (Activity) a.c() : null;
        boolean z = activity instanceof AdColonyAdViewActivity;
        if (!(activity instanceof b)) {
            return false;
        }
        if (z) {
            ((AdColonyAdViewActivity) activity).b();
            return true;
        }
        JSONObject a = u.a();
        u.a(a, "id", b);
        new ab("AdSession.on_request_close", ((b) activity).f, a).b();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean q(ab abVar) {
        JSONObject c = abVar.c();
        d l = a.a().l();
        String b = u.b(c, "ad_session_id");
        AdColonyInterstitial adColonyInterstitial = l.c().get(b);
        AdColonyAdView adColonyAdView = l.e().get(b);
        if ((adColonyInterstitial == null || adColonyInterstitial.getListener() == null || adColonyInterstitial.d() == null) && (adColonyAdView == null || adColonyAdView.getListener() == null)) {
            return false;
        }
        if (adColonyAdView == null) {
            new ab("AdUnit.make_in_app_purchase", adColonyInterstitial.d().c()).b();
        }
        b(b);
        c(b);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean b(ab abVar) {
        JSONObject a = u.a();
        JSONObject c = abVar.c();
        String b = u.b(c, "product_id");
        String b2 = u.b(c, "ad_session_id");
        if (b.equals("")) {
            b = u.b(c, "handle");
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(b));
        d(b);
        if (at.a(intent)) {
            u.b(a, "success", true);
            abVar.a(a).b();
            a(b2);
            b(b2);
            c(b2);
            return true;
        }
        at.a("Unable to open.", 0);
        u.b(a, "success", false);
        abVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|9|10|11|12|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        com.adcolony.sdk.at.a("Error saving screenshot.", 0);
        com.adcolony.sdk.u.b(r4, "success", false);
        r13.a(r4).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00e1, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00e2, code lost:
        com.adcolony.sdk.at.a("Error saving screenshot.", 0);
        com.adcolony.sdk.u.b(r4, "success", false);
        r13.a(r4).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00ef, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x00af */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x00d4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean c(final com.adcolony.sdk.ab r13) {
        /*
            r12 = this;
            java.lang.String r0 = "success"
            java.lang.String r1 = "Error saving screenshot."
            android.content.Context r2 = com.adcolony.sdk.a.c()
            r3 = 0
            if (r2 == 0) goto L_0x0113
            boolean r4 = r2 instanceof android.app.Activity
            if (r4 != 0) goto L_0x0011
            goto L_0x0113
        L_0x0011:
            java.lang.String r4 = "android.permission.WRITE_EXTERNAL_STORAGE"
            int r4 = androidx.core.app.ActivityCompat.checkSelfPermission(r2, r4)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            if (r4 != 0) goto L_0x00f0
            org.json.JSONObject r4 = r13.c()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r5 = "ad_session_id"
            java.lang.String r4 = com.adcolony.sdk.u.b(r4, r5)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r12.b(r4)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            org.json.JSONObject r4 = com.adcolony.sdk.u.a()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r5.<init>()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.io.File r6 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r6 = r6.toString()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r5.append(r6)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r6 = "/Pictures/AdColony_Screenshots/AdColony_Screenshot_"
            r5.append(r6)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r5.append(r6)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r6 = ".jpg"
            r5.append(r6)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r5 = r5.toString()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r6 = r2
            android.app.Activity r6 = (android.app.Activity) r6     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.view.Window r6 = r6.getWindow()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.view.View r6 = r6.getDecorView()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.view.View r6 = r6.getRootView()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r7 = 1
            r6.setDrawingCacheEnabled(r7)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.graphics.Bitmap r8 = r6.getDrawingCache()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.graphics.Bitmap r8 = android.graphics.Bitmap.createBitmap(r8)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r6.setDrawingCacheEnabled(r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x00af }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00af }
            r9.<init>()     // Catch:{ Exception -> 0x00af }
            java.io.File r10 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00af }
            java.lang.String r10 = r10.getPath()     // Catch:{ Exception -> 0x00af }
            r9.append(r10)     // Catch:{ Exception -> 0x00af }
            java.lang.String r10 = "/Pictures"
            r9.append(r10)     // Catch:{ Exception -> 0x00af }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x00af }
            r6.<init>(r9)     // Catch:{ Exception -> 0x00af }
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x00af }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00af }
            r10.<init>()     // Catch:{ Exception -> 0x00af }
            java.io.File r11 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00af }
            java.lang.String r11 = r11.getPath()     // Catch:{ Exception -> 0x00af }
            r10.append(r11)     // Catch:{ Exception -> 0x00af }
            java.lang.String r11 = "/Pictures/AdColony_Screenshots"
            r10.append(r11)     // Catch:{ Exception -> 0x00af }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00af }
            r9.<init>(r10)     // Catch:{ Exception -> 0x00af }
            r6.mkdirs()     // Catch:{ Exception -> 0x00af }
            r9.mkdirs()     // Catch:{ Exception -> 0x00af }
        L_0x00af:
            java.io.File r6 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r6.<init>(r5)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r9.<init>(r6)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            android.graphics.Bitmap$CompressFormat r6 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r10 = 90
            r8.compress(r6, r10, r9)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r9.flush()     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r9.close()     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            java.lang.String[] r6 = new java.lang.String[r7]     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r6[r3] = r5     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r5 = 0
            com.adcolony.sdk.aq$10 r8 = new com.adcolony.sdk.aq$10     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r8.<init>(r4, r13)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            android.media.MediaScannerConnection.scanFile(r2, r6, r5, r8)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            return r7
        L_0x00d4:
            com.adcolony.sdk.at.a(r1, r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.u.b(r4, r0, r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.ab r2 = r13.a(r4)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r2.b()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            return r3
        L_0x00e2:
            com.adcolony.sdk.at.a(r1, r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.u.b(r4, r0, r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.ab r2 = r13.a(r4)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r2.b()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            return r3
        L_0x00f0:
            com.adcolony.sdk.at.a(r1, r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            org.json.JSONObject r2 = r13.c()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.u.b(r2, r0, r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.ab r2 = r13.a(r2)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r2.b()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            return r3
        L_0x0102:
            com.adcolony.sdk.at.a(r1, r3)
            org.json.JSONObject r1 = r13.c()
            com.adcolony.sdk.u.b(r1, r0, r3)
            com.adcolony.sdk.ab r13 = r13.a(r1)
            r13.b()
        L_0x0113:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.aq.c(com.adcolony.sdk.ab):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean d(ab abVar) {
        JSONObject a = u.a();
        JSONObject c = abVar.c();
        Intent intent = new Intent("android.intent.action.DIAL");
        Intent data = intent.setData(Uri.parse("tel:" + u.b(c, "phone_number")));
        String b = u.b(c, "ad_session_id");
        if (at.a(data)) {
            u.b(a, "success", true);
            abVar.a(a).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        at.a("Failed to dial number.", 0);
        u.b(a, "success", false);
        abVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean e(ab abVar) {
        JSONObject c = abVar.c();
        JSONObject a = u.a();
        String b = u.b(c, "ad_session_id");
        JSONArray g = u.g(c, "recipients");
        String str = "";
        for (int i = 0; i < g.length(); i++) {
            if (i != 0) {
                str = str + ";";
            }
            str = str + u.c(g, i);
        }
        if (at.a(new Intent("android.intent.action.VIEW", Uri.parse("smsto:" + str)).putExtra("sms_body", u.b(c, "body")))) {
            u.b(a, "success", true);
            abVar.a(a).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        at.a("Failed to create sms.", 0);
        u.b(a, "success", false);
        abVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean f(ab abVar) {
        Context c = a.c();
        if (c == null) {
            return false;
        }
        int a = u.a(abVar.c(), "length_ms", (int) HttpStatus.SC_INTERNAL_SERVER_ERROR);
        JSONObject a2 = u.a();
        JSONArray d = at.d(c);
        boolean z = false;
        for (int i = 0; i < d.length(); i++) {
            if (u.c(d, i).equals("android.permission.VIBRATE")) {
                z = true;
            }
        }
        if (!z) {
            new w.a().a("No vibrate permission detected.").a(w.e);
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
        try {
            ((Vibrator) c.getSystemService("vibrator")).vibrate((long) a);
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return true;
        } catch (Exception unused) {
            new w.a().a("Vibrate command failed.").a(w.e);
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean g(ab abVar) {
        JSONObject a = u.a();
        JSONObject c = abVar.c();
        String b = u.b(c, "url");
        String b2 = u.b(c, "ad_session_id");
        AdColonyAdView adColonyAdView = a.a().l().e().get(b2);
        if (adColonyAdView != null && !adColonyAdView.getTrustedDemandSource() && !adColonyAdView.getUserInteraction()) {
            return false;
        }
        if (b.startsWith("browser")) {
            b = b.replaceFirst("browser", HttpHost.DEFAULT_SCHEME_NAME);
        }
        if (b.startsWith("safari")) {
            b = b.replaceFirst("safari", HttpHost.DEFAULT_SCHEME_NAME);
        }
        d(b);
        if (at.a(new Intent("android.intent.action.VIEW", Uri.parse(b)))) {
            u.b(a, "success", true);
            abVar.a(a).b();
            a(b2);
            b(b2);
            c(b2);
            return true;
        }
        at.a("Failed to launch browser.", 0);
        u.b(a, "success", false);
        abVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean h(ab abVar) {
        JSONObject a = u.a();
        JSONObject c = abVar.c();
        JSONArray g = u.g(c, "recipients");
        boolean d = u.d(c, "html");
        String b = u.b(c, "subject");
        String b2 = u.b(c, "body");
        String b3 = u.b(c, "ad_session_id");
        String[] strArr = new String[g.length()];
        for (int i = 0; i < g.length(); i++) {
            strArr[i] = u.c(g, i);
        }
        Intent intent = new Intent("android.intent.action.SEND");
        if (!d) {
            intent.setType("plain/text");
        }
        intent.putExtra("android.intent.extra.SUBJECT", b).putExtra("android.intent.extra.TEXT", b2).putExtra("android.intent.extra.EMAIL", strArr);
        if (at.a(intent)) {
            u.b(a, "success", true);
            abVar.a(a).b();
            a(b3);
            b(b3);
            c(b3);
            return true;
        }
        at.a("Failed to send email.", 0);
        u.b(a, "success", false);
        abVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean i(ab abVar) {
        JSONObject a = u.a();
        JSONObject c = abVar.c();
        String b = u.b(c, "ad_session_id");
        if (u.d(c, "deep_link")) {
            return b(abVar);
        }
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        if (at.a(c2.getPackageManager().getLaunchIntentForPackage(u.b(c, "handle")))) {
            u.b(a, "success", true);
            abVar.a(a).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        at.a("Failed to launch external application.", 0);
        u.b(a, "success", false);
        abVar.a(a).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01ed  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean j(com.adcolony.sdk.ab r25) {
        /*
            r24 = this;
            r0 = r24
            r1 = r25
            org.json.JSONObject r2 = com.adcolony.sdk.u.a()
            org.json.JSONObject r3 = r25.c()
            java.lang.String r4 = "ad_session_id"
            java.lang.String r4 = com.adcolony.sdk.u.b(r3, r4)
            java.lang.String r5 = "params"
            org.json.JSONObject r3 = com.adcolony.sdk.u.f(r3, r5)
            java.lang.String r5 = "recurrence"
            org.json.JSONObject r5 = com.adcolony.sdk.u.f(r3, r5)
            org.json.JSONArray r6 = com.adcolony.sdk.u.b()
            org.json.JSONArray r7 = com.adcolony.sdk.u.b()
            org.json.JSONArray r8 = com.adcolony.sdk.u.b()
            java.lang.String r9 = "description"
            java.lang.String r10 = com.adcolony.sdk.u.b(r3, r9)
            java.lang.String r11 = "location"
            com.adcolony.sdk.u.b(r3, r11)
            java.lang.String r11 = "start"
            java.lang.String r11 = com.adcolony.sdk.u.b(r3, r11)
            java.lang.String r12 = "end"
            java.lang.String r12 = com.adcolony.sdk.u.b(r3, r12)
            java.lang.String r13 = "summary"
            java.lang.String r3 = com.adcolony.sdk.u.b(r3, r13)
            java.lang.String r13 = ""
            if (r5 == 0) goto L_0x0078
            int r14 = r5.length()
            if (r14 <= 0) goto L_0x0078
            java.lang.String r6 = "expires"
            java.lang.String r6 = com.adcolony.sdk.u.b(r5, r6)
            java.lang.String r7 = "frequency"
            java.lang.String r7 = com.adcolony.sdk.u.b(r5, r7)
            java.util.Locale r8 = java.util.Locale.getDefault()
            java.lang.String r7 = r7.toUpperCase(r8)
            java.lang.String r8 = "daysInWeek"
            org.json.JSONArray r8 = com.adcolony.sdk.u.g(r5, r8)
            java.lang.String r14 = "daysInMonth"
            org.json.JSONArray r14 = com.adcolony.sdk.u.g(r5, r14)
            java.lang.String r15 = "daysInYear"
            org.json.JSONArray r15 = com.adcolony.sdk.u.g(r5, r15)
            goto L_0x007d
        L_0x0078:
            r14 = r7
            r15 = r8
            r7 = r13
            r8 = r6
            r6 = r7
        L_0x007d:
            boolean r13 = r3.equals(r13)
            if (r13 == 0) goto L_0x0084
            r3 = r10
        L_0x0084:
            java.util.Date r11 = com.adcolony.sdk.at.h(r11)
            java.util.Date r12 = com.adcolony.sdk.at.h(r12)
            java.util.Date r6 = com.adcolony.sdk.at.h(r6)
            java.lang.String r13 = "success"
            if (r11 == 0) goto L_0x0206
            if (r12 != 0) goto L_0x0098
            goto L_0x0206
        L_0x0098:
            long r0 = r11.getTime()
            r16 = r13
            long r12 = r12.getTime()
            r17 = 0
            if (r6 == 0) goto L_0x00b5
            long r19 = r6.getTime()
            long r21 = r11.getTime()
            long r19 = r19 - r21
            r21 = 1000(0x3e8, double:4.94E-321)
            long r19 = r19 / r21
            goto L_0x00b7
        L_0x00b5:
            r19 = r17
        L_0x00b7:
            java.lang.String r6 = "DAILY"
            boolean r6 = r7.equals(r6)
            r21 = 1
            if (r6 == 0) goto L_0x00cd
            r17 = 86400(0x15180, double:4.26873E-319)
            long r19 = r19 / r17
        L_0x00c6:
            long r17 = r19 + r21
        L_0x00c8:
            r19 = r12
            r11 = r17
            goto L_0x00f7
        L_0x00cd:
            java.lang.String r6 = "WEEKLY"
            boolean r6 = r7.equals(r6)
            if (r6 == 0) goto L_0x00db
            r17 = 604800(0x93a80, double:2.98811E-318)
            long r19 = r19 / r17
            goto L_0x00c6
        L_0x00db:
            java.lang.String r6 = "MONTHLY"
            boolean r6 = r7.equals(r6)
            if (r6 == 0) goto L_0x00e9
            r17 = 2629800(0x2820a8, double:1.299294E-317)
            long r19 = r19 / r17
            goto L_0x00c6
        L_0x00e9:
            java.lang.String r6 = "YEARLY"
            boolean r6 = r7.equals(r6)
            if (r6 == 0) goto L_0x00c8
            r17 = 31557600(0x1e187e0, double:1.5591526E-316)
            long r19 = r19 / r17
            goto L_0x00c6
        L_0x00f7:
            java.lang.String r6 = "endTime"
            java.lang.String r13 = "beginTime"
            r17 = r4
            java.lang.String r4 = "title"
            r18 = r2
            java.lang.String r2 = "vnd.android.cursor.item/event"
            r21 = r6
            java.lang.String r6 = "android.intent.action.EDIT"
            if (r5 == 0) goto L_0x01aa
            int r5 = r5.length()
            if (r5 <= 0) goto L_0x01aa
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r22 = r0
            java.lang.String r0 = "FREQ="
            r5.append(r0)
            r5.append(r7)
            java.lang.String r0 = ";COUNT="
            r5.append(r0)
            r5.append(r11)
            java.lang.String r0 = r5.toString()
            int r1 = r8.length()     // Catch:{ JSONException -> 0x0184 }
            if (r1 == 0) goto L_0x0148
            java.lang.String r1 = com.adcolony.sdk.at.a(r8)     // Catch:{ JSONException -> 0x0184 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0184 }
            r5.<init>()     // Catch:{ JSONException -> 0x0184 }
            r5.append(r0)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r7 = ";BYDAY="
            r5.append(r7)     // Catch:{ JSONException -> 0x0184 }
            r5.append(r1)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r0 = r5.toString()     // Catch:{ JSONException -> 0x0184 }
        L_0x0148:
            int r1 = r14.length()     // Catch:{ JSONException -> 0x0184 }
            if (r1 == 0) goto L_0x0166
            java.lang.String r1 = com.adcolony.sdk.at.b(r14)     // Catch:{ JSONException -> 0x0184 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0184 }
            r5.<init>()     // Catch:{ JSONException -> 0x0184 }
            r5.append(r0)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r7 = ";BYMONTHDAY="
            r5.append(r7)     // Catch:{ JSONException -> 0x0184 }
            r5.append(r1)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r0 = r5.toString()     // Catch:{ JSONException -> 0x0184 }
        L_0x0166:
            int r1 = r15.length()     // Catch:{ JSONException -> 0x0184 }
            if (r1 == 0) goto L_0x0184
            java.lang.String r1 = com.adcolony.sdk.at.b(r15)     // Catch:{ JSONException -> 0x0184 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0184 }
            r5.<init>()     // Catch:{ JSONException -> 0x0184 }
            r5.append(r0)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r7 = ";BYYEARDAY="
            r5.append(r7)     // Catch:{ JSONException -> 0x0184 }
            r5.append(r1)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r0 = r5.toString()     // Catch:{ JSONException -> 0x0184 }
        L_0x0184:
            android.content.Intent r1 = new android.content.Intent
            r1.<init>(r6)
            android.content.Intent r1 = r1.setType(r2)
            android.content.Intent r1 = r1.putExtra(r4, r3)
            android.content.Intent r1 = r1.putExtra(r9, r10)
            r7 = r22
            android.content.Intent r1 = r1.putExtra(r13, r7)
            r11 = r19
            r5 = r21
            android.content.Intent r1 = r1.putExtra(r5, r11)
            java.lang.String r2 = "rrule"
            android.content.Intent r0 = r1.putExtra(r2, r0)
            goto L_0x01c8
        L_0x01aa:
            r7 = r0
            r11 = r19
            r5 = r21
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r6)
            android.content.Intent r0 = r0.setType(r2)
            android.content.Intent r0 = r0.putExtra(r4, r3)
            android.content.Intent r0 = r0.putExtra(r9, r10)
            android.content.Intent r0 = r0.putExtra(r13, r7)
            android.content.Intent r0 = r0.putExtra(r5, r11)
        L_0x01c8:
            boolean r0 = com.adcolony.sdk.at.a(r0)
            if (r0 == 0) goto L_0x01ed
            r0 = 1
            r2 = r16
            r1 = r18
            com.adcolony.sdk.u.b(r1, r2, r0)
            r3 = r25
            com.adcolony.sdk.ab r1 = r3.a(r1)
            r1.b()
            r4 = r24
            r1 = r17
            r4.a(r1)
            r4.b(r1)
            r4.c(r1)
            return r0
        L_0x01ed:
            r4 = r24
            r3 = r25
            r2 = r16
            r1 = r18
            r0 = 0
            java.lang.String r5 = "Unable to create Calendar Event."
            com.adcolony.sdk.at.a(r5, r0)
            com.adcolony.sdk.u.b(r1, r2, r0)
            com.adcolony.sdk.ab r1 = r3.a(r1)
            r1.b()
            return r0
        L_0x0206:
            r4 = r24
            r3 = r1
            r1 = r2
            r2 = r13
            r0 = 0
            java.lang.String r5 = "Unable to create Calendar Event"
            com.adcolony.sdk.at.a(r5, r0)
            com.adcolony.sdk.u.b(r1, r2, r0)
            com.adcolony.sdk.ab r1 = r3.a(r1)
            r1.b()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.aq.j(com.adcolony.sdk.ab):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean k(ab abVar) {
        JSONObject a = u.a();
        String b = u.b(abVar.c(), "name");
        boolean a2 = at.a(b);
        u.b(a, "success", true);
        u.b(a, IronSourceConstants.EVENTS_RESULT, a2);
        u.a(a, "name", b);
        u.a(a, NotificationCompat.CATEGORY_SERVICE, b);
        abVar.a(a).b();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean l(ab abVar) {
        return k(abVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.at.a(android.content.Intent, boolean):boolean
     arg types: [android.content.Intent, int]
     candidates:
      com.adcolony.sdk.at.a(double, int):java.lang.String
      com.adcolony.sdk.at.a(java.lang.String, int):boolean
      com.adcolony.sdk.at.a(java.lang.String, java.io.File):boolean
      com.adcolony.sdk.at.a(java.lang.String[], java.lang.String[]):boolean
      com.adcolony.sdk.at.a(android.content.Intent, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean m(ab abVar) {
        JSONObject a = u.a();
        JSONObject c = abVar.c();
        Intent type = new Intent("android.intent.action.SEND").setType(HTTP.PLAIN_TEXT_TYPE);
        Intent putExtra = type.putExtra("android.intent.extra.TEXT", u.b(c, ViewHierarchyConstants.TEXT_KEY) + " " + u.b(c, "url"));
        String b = u.b(c, "ad_session_id");
        if (at.a(putExtra, true)) {
            u.b(a, "success", true);
            abVar.a(a).b();
            a(b);
            b(b);
            c(b);
            return true;
        }
        at.a("Unable to create social post.", 0);
        u.b(a, "success", false);
        abVar.a(a).b();
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        d l = a.a().l();
        AdColonyInterstitial adColonyInterstitial = l.c().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.getListener() == null) {
            AdColonyAdView adColonyAdView = l.e().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (adColonyAdView != null && listener != null) {
                listener.onLeftApplication(adColonyAdView);
                return;
            }
            return;
        }
        adColonyInterstitial.getListener().onLeftApplication(adColonyInterstitial);
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        d l = a.a().l();
        AdColonyInterstitial adColonyInterstitial = l.c().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.getListener() == null) {
            AdColonyAdView adColonyAdView = l.e().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (adColonyAdView != null && listener != null) {
                listener.onClicked(adColonyAdView);
                return;
            }
            return;
        }
        adColonyInterstitial.getListener().onClicked(adColonyInterstitial);
    }

    private boolean c(String str) {
        if (a.a().l().e().get(str) == null) {
            return false;
        }
        JSONObject a = u.a();
        u.a(a, "ad_session_id", str);
        new ab("MRAID.on_event", 1, a).b();
        return true;
    }

    private void d(final String str) {
        at.b.execute(new Runnable() {
            public void run() {
                JSONObject a2 = u.a();
                u.a(a2, "type", "open_hook");
                u.a(a2, "message", str);
                new ab("CustomMessage.controller_send", 0, a2).b();
            }
        });
    }
}
