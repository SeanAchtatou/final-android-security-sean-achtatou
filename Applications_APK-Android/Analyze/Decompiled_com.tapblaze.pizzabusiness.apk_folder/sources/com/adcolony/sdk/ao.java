package com.adcolony.sdk;

import android.content.Context;
import android.os.StatFs;
import com.adcolony.sdk.w;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.io.File;

class ao {
    private String a;
    private String b;
    private String c;
    private String d;
    private File e;
    private File f;
    private File g;

    ao() {
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        new w.a().a("Configuring storage").a(w.d);
        j a2 = a.a();
        this.a = c() + "/adc3/";
        this.b = this.a + "media/";
        this.e = new File(this.b);
        if (!this.e.isDirectory()) {
            this.e.delete();
            this.e.mkdirs();
        }
        if (!this.e.isDirectory()) {
            a2.a(true);
            return false;
        } else if (a(this.b) < 2.097152E7d) {
            new w.a().a("Not enough memory available at media path, disabling AdColony.").a(w.e);
            a2.a(true);
            return false;
        } else {
            this.c = c() + "/adc3/data/";
            this.f = new File(this.c);
            if (!this.f.isDirectory()) {
                this.f.delete();
            }
            this.f.mkdirs();
            this.d = this.a + "tmp/";
            this.g = new File(this.d);
            if (!this.g.isDirectory()) {
                this.g.delete();
                this.g.mkdirs();
            }
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        File file = this.e;
        if (file == null || this.f == null || this.g == null) {
            return false;
        }
        if (!file.isDirectory()) {
            this.e.delete();
        }
        if (!this.f.isDirectory()) {
            this.f.delete();
        }
        if (!this.g.isDirectory()) {
            this.g.delete();
        }
        this.e.mkdirs();
        this.f.mkdirs();
        this.g.mkdirs();
        return true;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        return c2.getFilesDir().getAbsolutePath();
    }

    /* access modifiers changed from: package-private */
    public double a(String str) {
        try {
            StatFs statFs = new StatFs(str);
            return (double) (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize()));
        } catch (RuntimeException unused) {
            return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        }
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.a;
    }
}
