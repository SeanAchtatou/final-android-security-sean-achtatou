package com.adcolony.sdk;

import com.ironsource.mediationsdk.utils.IronSourceConstants;
import cz.msebera.android.httpclient.HttpStatus;

public class AdColonyAdSize {
    public static final AdColonyAdSize BANNER = new AdColonyAdSize(320, 50);
    public static final AdColonyAdSize LEADERBOARD = new AdColonyAdSize(728, 90);
    public static final AdColonyAdSize MEDIUM_RECTANGLE = new AdColonyAdSize(HttpStatus.SC_MULTIPLE_CHOICES, IronSourceConstants.INTERSTITIAL_DAILY_CAPPED);
    public static final AdColonyAdSize SKYSCRAPER = new AdColonyAdSize(160, 600);
    int a;
    int b;

    public AdColonyAdSize(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public int getWidth() {
        return this.a;
    }

    public int getHeight() {
        return this.b;
    }
}
