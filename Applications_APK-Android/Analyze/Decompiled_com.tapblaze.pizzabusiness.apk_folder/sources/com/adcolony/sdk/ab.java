package com.adcolony.sdk;

import com.adcolony.sdk.w;
import org.json.JSONException;
import org.json.JSONObject;

class ab {
    private String a;
    private JSONObject b;

    ab(JSONObject jSONObject) {
        try {
            this.b = jSONObject;
            this.a = jSONObject.getString("m_type");
        } catch (JSONException e) {
            new w.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(w.h);
        }
    }

    ab(String str, int i) {
        try {
            this.a = str;
            this.b = new JSONObject();
            this.b.put("m_target", i);
        } catch (JSONException e) {
            new w.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(w.h);
        }
    }

    ab(String str, int i, String str2) {
        try {
            this.a = str;
            this.b = u.a(str2);
            this.b.put("m_target", i);
        } catch (JSONException e) {
            new w.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(w.h);
        }
    }

    ab(String str, int i, JSONObject jSONObject) {
        try {
            this.a = str;
            this.b = jSONObject == null ? new JSONObject() : jSONObject;
            this.b.put("m_target", i);
        } catch (JSONException e) {
            new w.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(w.h);
        }
    }

    /* access modifiers changed from: package-private */
    public ab a() {
        return a((JSONObject) null);
    }

    /* access modifiers changed from: package-private */
    public ab a(String str) {
        return a(u.a(str));
    }

    /* access modifiers changed from: package-private */
    public ab a(JSONObject jSONObject) {
        try {
            ab abVar = new ab("reply", this.b.getInt("m_origin"), jSONObject);
            abVar.b.put("m_id", this.b.getInt("m_id"));
            return abVar;
        } catch (JSONException e) {
            new w.a().a("JSON error in ADCMessage's createReply(): ").a(e.toString()).a(w.h);
            return new ab("JSONException", 0);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a.a(this.a, this.b);
    }

    /* access modifiers changed from: package-private */
    public JSONObject c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void b(JSONObject jSONObject) {
        this.b = jSONObject;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.a = str;
    }
}
