package com.adcolony.sdk;

import com.adcolony.sdk.n;
import com.adcolony.sdk.w;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

class o implements n.a {
    private BlockingQueue<Runnable> a = new LinkedBlockingQueue();
    private ThreadPoolExecutor b = new ThreadPoolExecutor(4, 16, 60, TimeUnit.SECONDS, this.a);
    private LinkedList<n> c = new LinkedList<>();
    private String d = a.a().m().G();

    o() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a.a("WebServices.download", new ad() {
            public void a(ab abVar) {
                o oVar = o.this;
                oVar.a(new n(abVar, oVar));
            }
        });
        a.a("WebServices.get", new ad() {
            public void a(ab abVar) {
                o oVar = o.this;
                oVar.a(new n(abVar, oVar));
            }
        });
        a.a("WebServices.post", new ad() {
            public void a(ab abVar) {
                o oVar = o.this;
                oVar.a(new n(abVar, oVar));
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(n nVar) {
        if (this.d.equals("")) {
            this.c.push(nVar);
            return;
        }
        try {
            this.b.execute(nVar);
        } catch (RejectedExecutionException unused) {
            w.a a2 = new w.a().a("RejectedExecutionException: ThreadPoolExecutor unable to ");
            a2.a("execute download for url " + nVar.a).a(w.h);
            a(nVar, nVar.a(), null);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.d = str;
        while (!this.c.isEmpty()) {
            a(this.c.removeLast());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.b.setCorePoolSize(i);
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b.getCorePoolSize();
    }

    public void a(n nVar, ab abVar, Map<String, List<String>> map) {
        JSONObject a2 = u.a();
        u.a(a2, "url", nVar.a);
        u.b(a2, "success", nVar.c);
        u.b(a2, "status", nVar.e);
        u.a(a2, "body", nVar.b);
        u.b(a2, "size", nVar.d);
        if (map != null) {
            JSONObject a3 = u.a();
            for (Map.Entry next : map.entrySet()) {
                String obj = ((List) next.getValue()).toString();
                String substring = obj.substring(1, obj.length() - 1);
                if (next.getKey() != null) {
                    u.a(a3, (String) next.getKey(), substring);
                }
            }
            u.a(a2, "headers", a3);
        }
        abVar.a(a2).b();
    }
}
