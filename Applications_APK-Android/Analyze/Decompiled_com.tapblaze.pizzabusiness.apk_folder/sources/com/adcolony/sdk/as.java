package com.adcolony.sdk;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.ArrayList;

class as {
    ak a;
    z b = new z().b();
    float[] c = new float[16];
    z d = new z().b();
    z e = new z().b();
    z f = new z().b();
    ArrayList<z> g = new ArrayList<>();
    ArrayList<z> h = new ArrayList<>();
    boolean i;
    boolean j;
    boolean k = true;

    as(ak akVar) {
        this.a = akVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.a.d();
        this.e.b();
        this.i = true;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a();
        do {
        } while (d());
    }

    /* access modifiers changed from: package-private */
    public z c() {
        int size = this.h.size();
        if (size == 0) {
            return new z();
        }
        return this.h.remove(size - 1);
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        int size = this.g.size();
        if (size == 0) {
            return false;
        }
        this.a.d();
        this.j = true;
        this.h.add(this.g.remove(size - 1));
        this.e.b();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.a.d();
        this.g.add(c().b(this.e));
        this.e.b();
        this.j = true;
        this.i = true;
    }

    /* access modifiers changed from: package-private */
    public void a(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10) {
        double d11;
        double d12;
        boolean z;
        double d13 = d4;
        double d14 = d5;
        this.a.d();
        double d15 = d13 / d9;
        double d16 = d14 / d10;
        double d17 = 1.0d;
        boolean z2 = true;
        if (d15 >= FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
            d11 = d15;
            d12 = 1.0d;
            z = false;
        } else {
            d11 = -d15;
            d12 = -1.0d;
            z = true;
        }
        if (d16 >= FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
            z2 = false;
        } else {
            d16 = -d16;
            d17 = -1.0d;
        }
        double d18 = d6 * d13;
        double d19 = d7 * d14;
        if (z || z2) {
            d18 -= d13 / 2.0d;
            d19 -= d14 / 2.0d;
            b((-d13) / 2.0d, (-d14) / 2.0d);
        }
        double cos = Math.cos(d8);
        double sin = Math.sin(d8);
        double d20 = d18 * d11;
        double d21 = cos * d16;
        this.e.a(cos * d11 * d12, d11 * sin * d12, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, (-sin) * d16 * d17, d21 * d17, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, 1.0d, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, (d2 - (d20 * cos)) + (sin * d16 * d19), (d3 - (d20 * sin)) - (d21 * d19), FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, 1.0d);
    }

    /* access modifiers changed from: package-private */
    public void a(double d2) {
        this.a.d();
        this.e.b(d2);
    }

    /* access modifiers changed from: package-private */
    public void b(double d2) {
        this.a.d();
        this.e.a(d2);
    }

    /* access modifiers changed from: package-private */
    public void a(double d2, double d3) {
        this.a.d();
        this.e.a(d2, d3, 1.0d);
    }

    /* access modifiers changed from: package-private */
    public void a(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12, double d13, double d14, double d15, double d16, double d17) {
        this.a.d();
        this.e.b(d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17);
        this.i = true;
    }

    /* access modifiers changed from: package-private */
    public void a(z zVar) {
        this.a.d();
        this.d.b(zVar);
        this.k = true;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        b();
    }

    /* access modifiers changed from: package-private */
    public void b(double d2, double d3) {
        this.e.b(d2, d3, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (this.j || this.k) {
            this.k = false;
            if (this.j) {
                this.j = false;
                this.f.b();
                for (int size = this.g.size() - 1; size >= 0; size--) {
                    this.f.a(this.g.get(size));
                }
            }
            this.b.b();
            this.b.a(this.e);
            this.b.a(this.f);
            this.b.a(this.d);
            this.b.a(this.c);
        }
    }
}
