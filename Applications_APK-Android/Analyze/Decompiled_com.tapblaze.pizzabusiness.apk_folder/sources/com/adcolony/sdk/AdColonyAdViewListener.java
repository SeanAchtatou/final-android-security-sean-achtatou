package com.adcolony.sdk;

public abstract class AdColonyAdViewListener {
    String a = "";
    AdColonyAdSize b;
    ag c;

    public void onClicked(AdColonyAdView adColonyAdView) {
    }

    public void onClosed(AdColonyAdView adColonyAdView) {
    }

    public void onLeftApplication(AdColonyAdView adColonyAdView) {
    }

    public void onOpened(AdColonyAdView adColonyAdView) {
    }

    public abstract void onRequestFilled(AdColonyAdView adColonyAdView);

    public void onRequestNotFilled(AdColonyZone adColonyZone) {
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public AdColonyAdSize b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public ag c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(ag agVar) {
        this.c = agVar;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyAdSize adColonyAdSize) {
        this.b = adColonyAdSize;
    }
}
