package com.adcolony.sdk;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import androidx.core.view.MotionEventCompat;
import com.ironsource.sdk.constants.Constants;
import org.json.JSONObject;

class f extends View {
    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private String f;
    private String g;
    private c h;
    private ab i;

    private f(Context context) {
        super(context);
    }

    f(Context context, ab abVar, int i2, c cVar) {
        super(context);
        this.h = cVar;
        this.i = abVar;
        this.a = i2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(ab abVar) {
        JSONObject c2 = abVar.c();
        return u.c(c2, "id") == this.a && u.c(c2, "container_id") == this.h.d() && u.b(c2, "ad_session_id").equals(this.h.b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.f$1, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.f$2, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.f$3, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* access modifiers changed from: package-private */
    public void a() {
        JSONObject c2 = this.i.c();
        this.g = u.b(c2, "ad_session_id");
        this.b = u.c(c2, "x");
        this.c = u.c(c2, "y");
        this.d = u.c(c2, "width");
        this.e = u.c(c2, "height");
        this.f = u.b(c2, Constants.ParametersKeys.COLOR);
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.d, this.e);
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.gravity = 0;
        this.h.addView(this, layoutParams);
        setBackgroundColor(at.g(this.f));
        this.h.m().add(a.a("ColorView.set_bounds", (ad) new ad() {
            public void a(ab abVar) {
                if (f.this.a(abVar)) {
                    f.this.b(abVar);
                }
            }
        }, true));
        this.h.m().add(a.a("ColorView.set_visible", (ad) new ad() {
            public void a(ab abVar) {
                if (f.this.a(abVar)) {
                    f.this.d(abVar);
                }
            }
        }, true));
        this.h.m().add(a.a("ColorView.set_color", (ad) new ad() {
            public void a(ab abVar) {
                if (f.this.a(abVar)) {
                    f.this.c(abVar);
                }
            }
        }, true));
        this.h.n().add("ColorView.set_bounds");
        this.h.n().add("ColorView.set_visible");
        this.h.n().add("ColorView.set_color");
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        j a2 = a.a();
        d l = a2.l();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        JSONObject a3 = u.a();
        u.b(a3, "view_id", this.a);
        u.a(a3, "ad_session_id", this.g);
        u.b(a3, "container_x", this.b + x);
        u.b(a3, "container_y", this.c + y);
        u.b(a3, "view_x", x);
        u.b(a3, "view_y", y);
        u.b(a3, "id", this.h.d());
        if (action == 0) {
            new ab("AdContainer.on_touch_began", this.h.c(), a3).b();
        } else if (action == 1) {
            if (!this.h.q()) {
                a2.a(l.e().get(this.g));
            }
            new ab("AdContainer.on_touch_ended", this.h.c(), a3).b();
        } else if (action == 2) {
            new ab("AdContainer.on_touch_moved", this.h.c(), a3).b();
        } else if (action == 3) {
            new ab("AdContainer.on_touch_cancelled", this.h.c(), a3).b();
        } else if (action == 5) {
            int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
            u.b(a3, "container_x", ((int) motionEvent2.getX(action2)) + this.b);
            u.b(a3, "container_y", ((int) motionEvent2.getY(action2)) + this.c);
            u.b(a3, "view_x", (int) motionEvent2.getX(action2));
            u.b(a3, "view_y", (int) motionEvent2.getY(action2));
            new ab("AdContainer.on_touch_began", this.h.c(), a3).b();
        } else if (action == 6) {
            int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
            u.b(a3, "container_x", ((int) motionEvent2.getX(action3)) + this.b);
            u.b(a3, "container_y", ((int) motionEvent2.getY(action3)) + this.c);
            u.b(a3, "view_x", (int) motionEvent2.getX(action3));
            u.b(a3, "view_y", (int) motionEvent2.getY(action3));
            if (!this.h.q()) {
                a2.a(l.e().get(this.g));
            }
            new ab("AdContainer.on_touch_ended", this.h.c(), a3).b();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(ab abVar) {
        JSONObject c2 = abVar.c();
        this.b = u.c(c2, "x");
        this.c = u.c(c2, "y");
        this.d = u.c(c2, "width");
        this.e = u.c(c2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.width = this.d;
        layoutParams.height = this.e;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void c(ab abVar) {
        setBackgroundColor(at.g(u.b(abVar.c(), Constants.ParametersKeys.COLOR)));
    }

    /* access modifiers changed from: package-private */
    public void d(ab abVar) {
        if (u.d(abVar.c(), "visible")) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }
}
