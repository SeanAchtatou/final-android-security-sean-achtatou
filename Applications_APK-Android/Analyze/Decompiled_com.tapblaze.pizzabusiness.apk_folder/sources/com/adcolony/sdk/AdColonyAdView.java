package com.adcolony.sdk;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.adcolony.sdk.w;
import java.io.File;
import org.json.JSONObject;

public class AdColonyAdView extends FrameLayout {
    /* access modifiers changed from: private */
    public c a = a.a().l().b().get(this.d);
    private AdColonyAdViewListener b;
    private AdColonyAdSize c;
    /* access modifiers changed from: private */
    public String d;
    private String e;
    private String f;
    private ImageView g;
    private ag h;
    private ab i;
    private boolean j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private JSONObject t;

    AdColonyAdView(Context context, ab abVar, AdColonyAdViewListener adColonyAdViewListener) {
        super(context);
        this.b = adColonyAdViewListener;
        this.e = adColonyAdViewListener.a();
        JSONObject c2 = abVar.c();
        this.t = c2;
        this.d = u.b(c2, "id");
        this.f = u.b(c2, "close_button_filepath");
        this.j = u.d(c2, "trusted_demand_source");
        this.n = u.d(c2, "close_button_snap_to_webview");
        this.r = u.c(c2, "close_button_width");
        this.s = u.c(c2, "close_button_height");
        this.c = adColonyAdViewListener.b();
        setLayoutParams(new FrameLayout.LayoutParams(this.a.p(), this.a.o()));
        setBackgroundColor(0);
        addView(this.a);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.h != null) {
            getWebView().k();
        }
    }

    public boolean destroy() {
        if (this.k) {
            new w.a().a("Ignoring duplicate call to destroy().").a(w.e);
            return false;
        }
        this.k = true;
        ag agVar = this.h;
        if (!(agVar == null || agVar.e() == null)) {
            this.h.a();
        }
        at.a(new Runnable() {
            public void run() {
                Context c = a.c();
                if (c instanceof AdColonyAdViewActivity) {
                    ((AdColonyAdViewActivity) c).b();
                }
                d l = a.a().l();
                l.e().remove(AdColonyAdView.this.d);
                l.a(AdColonyAdView.this.a);
                JSONObject a2 = u.a();
                u.a(a2, "id", AdColonyAdView.this.d);
                new ab("AdSession.on_ad_view_destroyed", 1, a2).b();
            }
        });
        return true;
    }

    public String getZoneId() {
        return this.e;
    }

    public void setListener(AdColonyAdViewListener adColonyAdViewListener) {
        this.b = adColonyAdViewListener;
    }

    public AdColonyAdViewListener getListener() {
        return this.b;
    }

    public AdColonyAdSize getAdSize() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean b() {
        if (this.j || this.m) {
            l m2 = a.a().m();
            int q2 = m2.q();
            int r2 = m2.r();
            int i2 = this.p;
            if (i2 <= 0) {
                i2 = q2;
            }
            int i3 = this.q;
            if (i3 <= 0) {
                i3 = r2;
            }
            int i4 = (q2 - i2) / 2;
            int i5 = (r2 - i3) / 2;
            this.a.setLayoutParams(new FrameLayout.LayoutParams(q2, r2));
            av webView = getWebView();
            if (webView != null) {
                ab abVar = new ab("WebView.set_bounds", 0);
                JSONObject a2 = u.a();
                u.b(a2, "x", i4);
                u.b(a2, "y", i5);
                u.b(a2, "width", i2);
                u.b(a2, "height", i3);
                abVar.b(a2);
                webView.b(abVar);
                float p2 = m2.p();
                JSONObject a3 = u.a();
                u.b(a3, "app_orientation", at.j(at.h()));
                u.b(a3, "width", (int) (((float) i2) / p2));
                u.b(a3, "height", (int) (((float) i3) / p2));
                u.b(a3, "x", at.a(webView));
                u.b(a3, "y", at.b(webView));
                u.a(a3, "ad_session_id", this.d);
                new ab("MRAID.on_size_change", this.a.c(), a3).b();
            }
            ImageView imageView = this.g;
            if (imageView != null) {
                this.a.removeView(imageView);
            }
            final Context c2 = a.c();
            if (!(c2 == null || this.l || webView == null)) {
                float p3 = a.a().m().p();
                int i6 = (int) (((float) this.r) * p3);
                int i7 = (int) (((float) this.s) * p3);
                if (this.n) {
                    q2 = webView.u() + webView.s();
                }
                int v = this.n ? webView.v() : 0;
                this.g = new ImageView(c2.getApplicationContext());
                this.g.setImageURI(Uri.fromFile(new File(this.f)));
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i6, i7);
                layoutParams.setMargins(q2 - i6, v, 0, 0);
                this.g.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Context context = c2;
                        if (context instanceof AdColonyAdViewActivity) {
                            ((AdColonyAdViewActivity) context).b();
                        }
                    }
                });
                this.a.addView(this.g, layoutParams);
            }
            if (this.i != null) {
                JSONObject a4 = u.a();
                u.b(a4, "success", true);
                this.i.a(a4).b();
                this.i = null;
            }
            return true;
        }
        if (this.i != null) {
            JSONObject a5 = u.a();
            u.b(a5, "success", false);
            this.i.a(a5).b();
            this.i = null;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.j || this.m) {
            float p2 = a.a().m().p();
            this.a.setLayoutParams(new FrameLayout.LayoutParams((int) (((float) this.c.getWidth()) * p2), (int) (((float) this.c.getHeight()) * p2)));
            av webView = getWebView();
            if (webView != null) {
                ab abVar = new ab("WebView.set_bounds", 0);
                JSONObject a2 = u.a();
                u.b(a2, "x", webView.o());
                u.b(a2, "y", webView.p());
                u.b(a2, "width", webView.q());
                u.b(a2, "height", webView.r());
                abVar.b(a2);
                webView.b(abVar);
                JSONObject a3 = u.a();
                u.a(a3, "ad_session_id", this.d);
                new ab("MRAID.on_close", this.a.c(), a3).b();
            }
            ImageView imageView = this.g;
            if (imageView != null) {
                this.a.removeView(imageView);
            }
            addView(this.a);
            AdColonyAdViewListener adColonyAdViewListener = this.b;
            if (adColonyAdViewListener != null) {
                adColonyAdViewListener.onClosed(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ag getOmidManager() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void setOmidManager(ag agVar) {
        this.h = agVar;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public String getAdSessionId() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public c getContainer() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void setNoCloseButton(boolean z) {
        this.l = this.j && z;
    }

    /* access modifiers changed from: package-private */
    public void setUserInteraction(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: package-private */
    public boolean getUserInteraction() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public boolean getTrustedDemandSource() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void setExpandedWidth(int i2) {
        this.p = (int) (((float) i2) * a.a().m().p());
    }

    /* access modifiers changed from: package-private */
    public void setExpandedHeight(int i2) {
        this.q = (int) (((float) i2) * a.a().m().p());
    }

    /* access modifiers changed from: package-private */
    public int getOrientation() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public void setOrientation(int i2) {
        this.o = i2;
    }

    /* access modifiers changed from: package-private */
    public av getWebView() {
        c cVar = this.a;
        if (cVar == null) {
            return null;
        }
        return cVar.g().get(2);
    }

    /* access modifiers changed from: package-private */
    public void setExpandMessage(ab abVar) {
        this.i = abVar;
    }
}
