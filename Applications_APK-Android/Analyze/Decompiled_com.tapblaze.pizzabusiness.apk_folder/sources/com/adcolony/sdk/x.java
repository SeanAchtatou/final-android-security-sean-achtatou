package com.adcolony.sdk;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class x {
    static final SimpleDateFormat l = new SimpleDateFormat("yyyyMMdd'T'HHmmss.SSSZ", Locale.US);
    static final String m = "message";
    static final String n = "timestamp";
    /* access modifiers changed from: private */
    public Date a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public t c;
    protected String o;

    x() {
    }

    /* access modifiers changed from: package-private */
    public void a(t tVar) {
        this.c = tVar;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.b = i;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        int i = this.b;
        if (i == -1) {
            return "Fatal";
        }
        if (i == 0) {
            return "Error";
        }
        if (i == 1) {
            return "Warn";
        }
        if (i != 2) {
            return i != 3 ? "UNKNOWN LOG LEVEL" : "Debug";
        }
        return "Info";
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return l.format(this.a);
    }

    /* access modifiers changed from: package-private */
    public t f() {
        return this.c;
    }

    public String toString() {
        return e() + " " + b() + "/" + f().d() + ": " + d();
    }

    static class a {
        protected x b = new x();

        a() {
        }

        /* access modifiers changed from: package-private */
        public a a(int i) {
            int unused = this.b.b = i;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(t tVar) {
            t unused = this.b.c = tVar;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(String str) {
            this.b.o = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(Date date) {
            Date unused = this.b.a = date;
            return this;
        }

        /* access modifiers changed from: package-private */
        public x a() {
            if (this.b.a == null) {
                Date unused = this.b.a = new Date(System.currentTimeMillis());
            }
            return this.b;
        }
    }
}
