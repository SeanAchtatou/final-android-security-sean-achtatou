package com.adcolony.sdk;

import android.media.SoundPool;
import com.adcolony.sdk.w;
import java.util.HashMap;
import org.json.JSONObject;

class an {
    final String a;
    private final int b;
    private HashMap<Integer, Integer> c = new HashMap<>();
    private HashMap<Integer, Integer> d = new HashMap<>();
    private HashMap<Integer, Boolean> e = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, Integer> f = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, Integer> g = new HashMap<>();
    private HashMap<String, Integer> h = new HashMap<>();
    private SoundPool i;

    an(final String str, final int i2) {
        this.a = str;
        this.b = i2;
        this.i = new SoundPool(50, 3, 0);
        this.i.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            public void onLoadComplete(SoundPool soundPool, int i, int i2) {
                JSONObject a2 = u.a();
                u.b(a2, "id", ((Integer) an.this.f.get(Integer.valueOf(i))).intValue());
                u.a(a2, "ad_session_id", str);
                if (i2 == 0) {
                    new ab("AudioPlayer.on_ready", i2, a2).b();
                    an.this.g.put(an.this.f.get(Integer.valueOf(i)), Integer.valueOf(i));
                    return;
                }
                new ab("AudioPlayer.on_error", i2, a2).b();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(ab abVar) {
        JSONObject c2 = abVar.c();
        int load = this.i.load(u.b(c2, "filepath"), 1);
        int i2 = u.d(c2, "repeats") ? -1 : 0;
        this.f.put(Integer.valueOf(load), Integer.valueOf(u.c(c2, "id")));
        new w.a().a("Load audio with id = ").a(load).a(w.d);
        this.d.put(Integer.valueOf(load), Integer.valueOf(i2));
        this.e.put(Integer.valueOf(load), false);
    }

    /* access modifiers changed from: package-private */
    public void b(ab abVar) {
        this.i.unload(this.g.get(Integer.valueOf(u.c(abVar.c(), "id"))).intValue());
    }

    /* access modifiers changed from: package-private */
    public void c(ab abVar) {
        int intValue = this.g.get(Integer.valueOf(u.c(abVar.c(), "id"))).intValue();
        if (!this.e.get(Integer.valueOf(intValue)).booleanValue()) {
            int play = this.i.play(intValue, 1.0f, 1.0f, 0, this.d.get(Integer.valueOf(intValue)).intValue(), 1.0f);
            if (play != 0) {
                this.c.put(Integer.valueOf(intValue), Integer.valueOf(play));
                return;
            }
            JSONObject a2 = u.a();
            u.b(a2, "id", u.c(abVar.c(), "id"));
            u.a(a2, "ad_session_id", this.a);
            new ab("AudioPlayer.on_error", this.b, a2).b();
            return;
        }
        this.i.resume(this.c.get(Integer.valueOf(intValue)).intValue());
    }

    /* access modifiers changed from: package-private */
    public void d(ab abVar) {
        int intValue = this.g.get(Integer.valueOf(u.c(abVar.c(), "id"))).intValue();
        this.i.pause(this.c.get(Integer.valueOf(intValue)).intValue());
        this.e.put(Integer.valueOf(intValue), true);
    }

    /* access modifiers changed from: package-private */
    public void e(ab abVar) {
        this.i.stop(this.c.get(this.g.get(Integer.valueOf(u.c(abVar.c(), "id")))).intValue());
    }

    /* access modifiers changed from: package-private */
    public SoundPool a() {
        return this.i;
    }
}
