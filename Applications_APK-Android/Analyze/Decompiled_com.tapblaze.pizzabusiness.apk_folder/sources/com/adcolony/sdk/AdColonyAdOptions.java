package com.adcolony.sdk;

import org.json.JSONObject;

public class AdColonyAdOptions {
    boolean a;
    boolean b;
    AdColonyUserMetadata c;
    JSONObject d = u.a();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    public AdColonyAdOptions enableConfirmationDialog(boolean z) {
        this.a = z;
        u.b(this.d, "confirmation_enabled", true);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    public AdColonyAdOptions enableResultsDialog(boolean z) {
        this.b = z;
        u.b(this.d, "results_enabled", true);
        return this;
    }

    public AdColonyAdOptions setOption(String str, boolean z) {
        if (at.d(str)) {
            u.b(this.d, str, z);
        }
        return this;
    }

    public Object getOption(String str) {
        return u.a(this.d, str);
    }

    public AdColonyAdOptions setOption(String str, double d2) {
        if (at.d(str)) {
            u.a(this.d, str, d2);
        }
        return this;
    }

    public AdColonyAdOptions setOption(String str, String str2) {
        if (str != null && at.d(str) && at.d(str2)) {
            u.a(this.d, str, str2);
        }
        return this;
    }

    public AdColonyAdOptions setUserMetadata(AdColonyUserMetadata adColonyUserMetadata) {
        this.c = adColonyUserMetadata;
        u.a(this.d, "user_metadata", adColonyUserMetadata.c);
        return this;
    }

    public AdColonyUserMetadata getUserMetadata() {
        return this.c;
    }
}
