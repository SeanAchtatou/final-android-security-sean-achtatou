package com.adcolony.sdk;

import android.content.Context;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.FrameLayout;
import androidx.core.view.GravityCompat;
import androidx.core.view.MotionEventCompat;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import org.json.JSONObject;

class p extends EditText {
    private c A;
    private ab B;
    private final int a = 0;
    private final int b = 1;
    private final int c = 2;
    private final int d = 3;
    private final int e = 1;
    private final int f = 2;
    private final int g = 3;
    private final int h = 0;
    private final int i = 1;
    private final int j = 2;
    private final int k = 1;
    private final int l = 2;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private String w;
    private String x;
    private String y;
    private String z;

    /* access modifiers changed from: package-private */
    public int a(boolean z2, int i2) {
        if (i2 == 0) {
            return z2 ? 1 : 16;
        }
        if (i2 != 1) {
            if (i2 != 2) {
                return 17;
            }
            if (z2) {
                return GravityCompat.END;
            }
            return 80;
        } else if (z2) {
            return GravityCompat.START;
        } else {
            return 48;
        }
    }

    private p(Context context) {
        super(context);
    }

    p(Context context, ab abVar, int i2, c cVar) {
        super(context);
        this.m = i2;
        this.B = abVar;
        this.A = cVar;
    }

    /* access modifiers changed from: package-private */
    public void a(ab abVar) {
        JSONObject c2 = abVar.c();
        this.u = u.c(c2, "x");
        this.v = u.c(c2, "y");
        setGravity(a(true, this.u) | a(false, this.v));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        j a2 = a.a();
        d l2 = a2.l();
        int action = motionEvent.getAction() & 255;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject a3 = u.a();
        u.b(a3, "view_id", this.m);
        u.a(a3, "ad_session_id", this.w);
        u.b(a3, "container_x", this.n + x2);
        u.b(a3, "container_y", this.o + y2);
        u.b(a3, "view_x", x2);
        u.b(a3, "view_y", y2);
        u.b(a3, "id", this.A.d());
        if (action == 0) {
            new ab("AdContainer.on_touch_began", this.A.c(), a3).b();
        } else if (action == 1) {
            if (!this.A.q()) {
                a2.a(l2.e().get(this.w));
            }
            new ab("AdContainer.on_touch_ended", this.A.c(), a3).b();
        } else if (action == 2) {
            new ab("AdContainer.on_touch_moved", this.A.c(), a3).b();
        } else if (action == 3) {
            new ab("AdContainer.on_touch_cancelled", this.A.c(), a3).b();
        } else if (action == 5) {
            int action2 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
            u.b(a3, "container_x", ((int) motionEvent2.getX(action2)) + this.n);
            u.b(a3, "container_y", ((int) motionEvent2.getY(action2)) + this.o);
            u.b(a3, "view_x", (int) motionEvent2.getX(action2));
            u.b(a3, "view_y", (int) motionEvent2.getY(action2));
            new ab("AdContainer.on_touch_began", this.A.c(), a3).b();
        } else if (action == 6) {
            int action3 = (motionEvent.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
            u.b(a3, "container_x", ((int) motionEvent2.getX(action3)) + this.n);
            u.b(a3, "container_y", ((int) motionEvent2.getY(action3)) + this.o);
            u.b(a3, "view_x", (int) motionEvent2.getX(action3));
            u.b(a3, "view_y", (int) motionEvent2.getY(action3));
            if (!this.A.q()) {
                a2.a(l2.e().get(this.w));
            }
            new ab("AdContainer.on_touch_ended", this.A.c(), a3).b();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b(ab abVar) {
        JSONObject c2 = abVar.c();
        return u.c(c2, "id") == this.m && u.c(c2, "container_id") == this.A.d() && u.b(c2, "ad_session_id").equals(this.A.b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$1, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$3, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$4, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$5, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$6, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$7, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$8, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$9, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$10, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.p$2, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* access modifiers changed from: package-private */
    public void a() {
        JSONObject c2 = this.B.c();
        this.w = u.b(c2, "ad_session_id");
        this.n = u.c(c2, "x");
        this.o = u.c(c2, "y");
        this.p = u.c(c2, "width");
        this.q = u.c(c2, "height");
        this.s = u.c(c2, "font_family");
        this.r = u.c(c2, "font_style");
        this.t = u.c(c2, ViewHierarchyConstants.TEXT_SIZE);
        this.x = u.b(c2, "background_color");
        this.y = u.b(c2, "font_color");
        this.z = u.b(c2, ViewHierarchyConstants.TEXT_KEY);
        this.u = u.c(c2, "align_x");
        this.v = u.c(c2, "align_y");
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.p, this.q);
        layoutParams.setMargins(this.n, this.o, 0, 0);
        layoutParams.gravity = 0;
        this.A.addView(this, layoutParams);
        int i2 = this.s;
        if (i2 == 0) {
            setTypeface(Typeface.DEFAULT);
        } else if (i2 == 1) {
            setTypeface(Typeface.SERIF);
        } else if (i2 == 2) {
            setTypeface(Typeface.SANS_SERIF);
        } else if (i2 == 3) {
            setTypeface(Typeface.MONOSPACE);
        }
        int i3 = this.r;
        if (i3 == 0) {
            setTypeface(getTypeface(), 0);
        } else if (i3 == 1) {
            setTypeface(getTypeface(), 1);
        } else if (i3 == 2) {
            setTypeface(getTypeface(), 2);
        } else if (i3 == 3) {
            setTypeface(getTypeface(), 3);
        }
        setText(this.z);
        setTextSize((float) this.t);
        setGravity(a(true, this.u) | a(false, this.v));
        if (!this.x.equals("")) {
            setBackgroundColor(at.g(this.x));
        }
        if (!this.y.equals("")) {
            setTextColor(at.g(this.y));
        }
        this.A.m().add(a.a("TextView.set_visible", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.k(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_bounds", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.d(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_font_color", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.f(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_background_color", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.e(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_typeface", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.j(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_font_size", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.g(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_font_style", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.h(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.get_text", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.c(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.set_text", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.i(abVar);
                }
            }
        }, true));
        this.A.m().add(a.a("TextView.align", (ad) new ad() {
            public void a(ab abVar) {
                if (p.this.b(abVar)) {
                    p.this.a(abVar);
                }
            }
        }, true));
        this.A.n().add("TextView.set_visible");
        this.A.n().add("TextView.set_bounds");
        this.A.n().add("TextView.set_font_color");
        this.A.n().add("TextView.set_background_color");
        this.A.n().add("TextView.set_typeface");
        this.A.n().add("TextView.set_font_size");
        this.A.n().add("TextView.set_font_style");
        this.A.n().add("TextView.get_text");
        this.A.n().add("TextView.set_text");
        this.A.n().add("TextView.align");
    }

    /* access modifiers changed from: package-private */
    public void c(ab abVar) {
        JSONObject a2 = u.a();
        u.a(a2, ViewHierarchyConstants.TEXT_KEY, getText().toString());
        abVar.a(a2).b();
    }

    /* access modifiers changed from: package-private */
    public void d(ab abVar) {
        JSONObject c2 = abVar.c();
        this.n = u.c(c2, "x");
        this.o = u.c(c2, "y");
        this.p = u.c(c2, "width");
        this.q = u.c(c2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.n, this.o, 0, 0);
        layoutParams.width = this.p;
        layoutParams.height = this.q;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void e(ab abVar) {
        this.x = u.b(abVar.c(), "background_color");
        setBackgroundColor(at.g(this.x));
    }

    /* access modifiers changed from: package-private */
    public void f(ab abVar) {
        this.y = u.b(abVar.c(), "font_color");
        setTextColor(at.g(this.y));
    }

    /* access modifiers changed from: package-private */
    public void g(ab abVar) {
        this.t = u.c(abVar.c(), ViewHierarchyConstants.TEXT_SIZE);
        setTextSize((float) this.t);
    }

    /* access modifiers changed from: package-private */
    public void h(ab abVar) {
        int c2 = u.c(abVar.c(), "font_style");
        this.r = c2;
        if (c2 == 0) {
            setTypeface(getTypeface(), 0);
        } else if (c2 == 1) {
            setTypeface(getTypeface(), 1);
        } else if (c2 == 2) {
            setTypeface(getTypeface(), 2);
        } else if (c2 == 3) {
            setTypeface(getTypeface(), 3);
        }
    }

    /* access modifiers changed from: package-private */
    public void i(ab abVar) {
        this.z = u.b(abVar.c(), ViewHierarchyConstants.TEXT_KEY);
        setText(this.z);
    }

    /* access modifiers changed from: package-private */
    public void j(ab abVar) {
        int c2 = u.c(abVar.c(), "font_family");
        this.s = c2;
        if (c2 == 0) {
            setTypeface(Typeface.DEFAULT);
        } else if (c2 == 1) {
            setTypeface(Typeface.SERIF);
        } else if (c2 == 2) {
            setTypeface(Typeface.SANS_SERIF);
        } else if (c2 == 3) {
            setTypeface(Typeface.MONOSPACE);
        }
    }

    /* access modifiers changed from: package-private */
    public void k(ab abVar) {
        if (u.d(abVar.c(), "visible")) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }
}
