package com.adcolony.sdk;

import android.content.Context;
import com.adcolony.sdk.w;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import com.vungle.warren.model.Advertisement;
import cz.msebera.android.httpclient.HttpStatus;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

class n implements Runnable {
    String a = "";
    String b = "";
    boolean c;
    int d;
    int e;
    private HttpURLConnection f;
    private InputStream g;
    private ab h;
    private a i;
    private final int j = 4096;
    private String k;
    private int l = 0;
    private boolean m = false;
    private Map<String, List<String>> n;
    private String o = "";
    private String p = "";

    interface a {
        void a(n nVar, ab abVar, Map<String, List<String>> map);
    }

    n(ab abVar, a aVar) {
        this.h = abVar;
        this.i = aVar;
    }

    public void run() {
        boolean z = false;
        this.c = false;
        try {
            if (b()) {
                this.c = c();
                if (this.h.d().equals("WebServices.post") && this.e != 200) {
                    this.c = false;
                }
            }
        } catch (MalformedURLException e2) {
            new w.a().a("MalformedURLException: ").a(e2.toString()).a(w.h);
            this.c = true;
        } catch (OutOfMemoryError unused) {
            w.a a2 = new w.a().a("Out of memory error - disabling AdColony. (").a(this.d).a("/").a(this.l);
            a2.a("): " + this.a).a(w.g);
            a.a().a(true);
        } catch (IOException e3) {
            new w.a().a("Download of ").a(this.a).a(" failed: ").a(e3.toString()).a(w.f);
            int i2 = this.e;
            if (i2 == 0) {
                i2 = HttpStatus.SC_GATEWAY_TIMEOUT;
            }
            this.e = i2;
        } catch (IllegalStateException e4) {
            new w.a().a("okhttp error: ").a(e4.toString()).a(w.g);
            e4.printStackTrace();
        } catch (Exception e5) {
            new w.a().a("Exception: ").a(e5.toString()).a(w.g);
            e5.printStackTrace();
        }
        z = true;
        if (this.c) {
            new w.a().a("Downloaded ").a(this.a).a(w.d);
        }
        if (z) {
            if (this.h.d().equals("WebServices.download")) {
                a(this.p, this.o);
            }
            this.i.a(this, this.h, this.n);
        }
    }

    private boolean b() throws IOException {
        JSONObject c2 = this.h.c();
        String b2 = u.b(c2, FirebaseAnalytics.Param.CONTENT_TYPE);
        String b3 = u.b(c2, "content");
        boolean d2 = u.d(c2, "no_redirect");
        this.a = u.b(c2, "url");
        this.o = u.b(c2, "filepath");
        StringBuilder sb = new StringBuilder();
        sb.append(a.a().o().f());
        String str = this.o;
        sb.append(str.substring(str.lastIndexOf("/") + 1));
        this.p = sb.toString();
        this.k = u.b(c2, "encoding");
        this.l = u.a(c2, "max_size", 0);
        this.m = this.l != 0;
        this.d = 0;
        this.g = null;
        this.f = null;
        this.n = null;
        if (!this.a.startsWith(Advertisement.FILE_SCHEME)) {
            this.f = (HttpURLConnection) ((URLConnection) FirebasePerfUrlConnection.instrument(new URL(this.a).openConnection()));
            this.f.setInstanceFollowRedirects(!d2);
            this.f.setRequestProperty("Accept-Charset", "UTF-8");
            this.f.setRequestProperty("User-Agent", a.a().m().G());
            if (!b2.equals("")) {
                this.f.setRequestProperty("Content-Type", b2);
            }
            if (this.h.d().equals("WebServices.post")) {
                this.f.setDoOutput(true);
                this.f.setFixedLengthStreamingMode(b3.getBytes("UTF-8").length);
                new PrintStream(this.f.getOutputStream()).print(b3);
            }
        } else if (this.a.startsWith("file:///android_asset/")) {
            Context c3 = a.c();
            if (c3 != null) {
                this.g = c3.getAssets().open(this.a.substring(22));
            }
        } else {
            this.g = new FileInputStream(this.a.substring(7));
        }
        if (this.f == null && this.g == null) {
            return false;
        }
        return true;
    }

    private boolean c() throws Exception {
        OutputStream outputStream;
        String d2 = this.h.d();
        if (this.g != null) {
            outputStream = this.o.length() == 0 ? new ByteArrayOutputStream(4096) : new FileOutputStream(new File(this.o).getAbsolutePath());
        } else if (d2.equals("WebServices.download")) {
            this.g = this.f.getInputStream();
            outputStream = new FileOutputStream(this.p);
        } else if (d2.equals("WebServices.get")) {
            this.g = this.f.getInputStream();
            outputStream = new ByteArrayOutputStream(4096);
        } else if (d2.equals("WebServices.post")) {
            this.f.connect();
            this.g = this.f.getResponseCode() == 200 ? this.f.getInputStream() : this.f.getErrorStream();
            outputStream = new ByteArrayOutputStream(4096);
        } else {
            outputStream = null;
        }
        HttpURLConnection httpURLConnection = this.f;
        if (httpURLConnection != null) {
            this.e = httpURLConnection.getResponseCode();
            this.n = this.f.getHeaderFields();
        }
        return a(this.g, outputStream);
    }

    private boolean a(InputStream inputStream, OutputStream outputStream) throws Exception {
        BufferedInputStream bufferedInputStream;
        Exception e2;
        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            try {
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = bufferedInputStream.read(bArr, 0, 4096);
                    if (read != -1) {
                        this.d += read;
                        if (this.m) {
                            if (this.d > this.l) {
                                throw new Exception("Data exceeds expected maximum (" + this.d + "/" + this.l + "): " + this.f.getURL().toString());
                            }
                        }
                        outputStream.write(bArr, 0, read);
                    } else {
                        String str = "UTF-8";
                        if (this.k != null && !this.k.isEmpty()) {
                            str = this.k;
                        }
                        if (outputStream instanceof ByteArrayOutputStream) {
                            this.b = ((ByteArrayOutputStream) outputStream).toString(str);
                        }
                        if (outputStream != null) {
                            outputStream.close();
                        }
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        bufferedInputStream.close();
                        return true;
                    }
                }
            } catch (Exception e3) {
                e2 = e3;
                try {
                    throw e2;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (Exception e4) {
            Exception exc = e4;
            bufferedInputStream = null;
            e2 = exc;
            throw e2;
        } catch (Throwable th2) {
            Throwable th3 = th2;
            bufferedInputStream = null;
            th = th3;
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (bufferedInputStream != null) {
                bufferedInputStream.close();
            }
            throw th;
        }
    }

    private void a(String str, String str2) {
        try {
            String substring = str2.substring(0, str2.lastIndexOf("/") + 1);
            if (str2 != null && !"".equals(str2) && !substring.equals(a.a().o().f()) && !new File(str).renameTo(new File(str2))) {
                w.a a2 = new w.a().a("Moving of ");
                if (str == null) {
                    str = "temp folder's asset file";
                }
                a2.a(str).a(" failed.").a(w.f);
            }
        } catch (Exception e2) {
            new w.a().a("Exception: ").a(e2.toString()).a(w.g);
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public ab a() {
        return this.h;
    }
}
