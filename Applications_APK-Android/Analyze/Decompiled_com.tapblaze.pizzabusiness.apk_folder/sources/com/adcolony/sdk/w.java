package com.adcolony.sdk;

class w {
    static w a = new w(3, false);
    static w b = new w(3, true);
    static w c = new w(2, false);
    static w d = new w(2, true);
    static w e = new w(1, false);
    static w f = new w(1, true);
    static w g = new w(0, false);
    static w h = new w(0, true);
    private int i;
    private boolean j;

    w(int i2, boolean z) {
        this.i = i2;
        this.j = z;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        y.a(this.i, str, this.j);
    }

    static class a {
        StringBuilder a = new StringBuilder();

        a() {
        }

        /* access modifiers changed from: package-private */
        public a a(char c) {
            if (c != 10) {
                this.a.append(c);
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(String str) {
            this.a.append(str);
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(Object obj) {
            if (obj != null) {
                this.a.append(obj.toString());
            } else {
                this.a.append("null");
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(double d) {
            at.a(d, 2, this.a);
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(int i) {
            this.a.append(i);
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(boolean z) {
            this.a.append(z);
            return this;
        }

        /* access modifiers changed from: package-private */
        public void a(w wVar) {
            wVar.a(this.a.toString());
        }
    }
}
