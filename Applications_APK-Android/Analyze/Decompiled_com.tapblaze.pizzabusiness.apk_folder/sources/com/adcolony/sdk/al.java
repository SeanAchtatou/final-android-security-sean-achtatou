package com.adcolony.sdk;

import android.content.Context;
import com.adcolony.sdk.at;
import com.adcolony.sdk.w;
import com.facebook.login.widget.ToolTipPopup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Executors;
import org.json.JSONObject;

class al implements Runnable {
    private final long a = 30000;
    private final int b = 17;
    private final int c = 15000;
    private final int d = 1000;
    private long e;
    private long f;
    private long g;
    private long h;
    private long i;
    private long j;
    private long k;
    private long l;
    private boolean m = true;
    private boolean n = true;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    /* access modifiers changed from: private */
    public boolean s;
    private boolean t;

    al() {
    }

    public void a() {
        a.a("SessionInfo.stopped", new ad() {
            public void a(ab abVar) {
                boolean unused = al.this.s = true;
            }
        });
    }

    public void run() {
        long j2;
        while (!this.r) {
            this.h = System.currentTimeMillis();
            a.f();
            if (this.f >= 30000) {
                break;
            }
            if (!this.m) {
                if (this.o && !this.n) {
                    this.o = false;
                    f();
                }
                long j3 = this.f;
                if (this.l == 0) {
                    j2 = 0;
                } else {
                    j2 = System.currentTimeMillis() - this.l;
                }
                this.f = j3 + j2;
                this.l = System.currentTimeMillis();
            } else {
                if (this.o && this.n) {
                    this.o = false;
                    g();
                }
                this.f = 0;
                this.l = 0;
            }
            this.g = 17;
            a(this.g);
            this.i = System.currentTimeMillis() - this.h;
            long j4 = this.i;
            if (j4 > 0 && j4 < ToolTipPopup.DEFAULT_POPUP_DISPLAY_TIME) {
                this.e += j4;
            }
            j a2 = a.a();
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.k > 15000) {
                this.k = currentTimeMillis;
            }
            if (a.d() && currentTimeMillis - this.j > 1000) {
                this.j = currentTimeMillis;
                String c2 = a2.d.c();
                if (!c2.equals(a2.w())) {
                    a2.a(c2);
                    JSONObject a3 = u.a();
                    u.a(a3, "network_type", a2.w());
                    new ab("Network.on_status_change", 1, a3).b();
                }
            }
        }
        new w.a().a("AdColony session ending, releasing Context.").a(w.c);
        a.a().b(true);
        a.a((Context) null);
        this.q = true;
        this.t = true;
        b();
        at.a aVar = new at.a(10.0d);
        while (!this.s && !aVar.b() && this.t) {
            a.f();
            a(100);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (!this.p) {
            if (this.q) {
                a.a().b(false);
                this.q = false;
            }
            this.e = 0;
            this.f = 0;
            this.p = true;
            this.m = true;
            this.s = false;
            new Thread(this).start();
            if (z) {
                JSONObject a2 = u.a();
                u.a(a2, "id", at.e());
                new ab("SessionInfo.on_start", 1, a2).b();
                av avVar = (av) a.a().q().e().get(1);
                if (avVar != null) {
                    avVar.j();
                }
            }
            if (AdColony.a.isShutdown()) {
                AdColony.a = Executors.newSingleThreadExecutor();
            }
            y.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.p = false;
        this.m = false;
        if (y.l != null) {
            y.l.a();
        }
        JSONObject a2 = u.a();
        double d2 = (double) this.e;
        Double.isNaN(d2);
        u.a(a2, "session_length", d2 / 1000.0d);
        new ab("SessionInfo.on_stop", 1, a2).b();
        a.f();
        AdColony.a.shutdown();
    }

    private void f() {
        b(false);
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        ArrayList<ae> c2 = a.a().q().c();
        synchronized (c2) {
            Iterator<ae> it = c2.iterator();
            while (it.hasNext()) {
                JSONObject a2 = u.a();
                u.b(a2, "from_window_focus", z);
                new ab("SessionInfo.on_pause", it.next().a(), a2).b();
            }
        }
        this.n = true;
        a.f();
    }

    private void g() {
        c(false);
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z) {
        ArrayList<ae> c2 = a.a().q().c();
        synchronized (c2) {
            Iterator<ae> it = c2.iterator();
            while (it.hasNext()) {
                JSONObject a2 = u.a();
                u.b(a2, "from_window_focus", z);
                new ab("SessionInfo.on_resume", it.next().a(), a2).b();
            }
        }
        y.a();
        this.n = false;
    }

    /* access modifiers changed from: package-private */
    public void d(boolean z) {
        this.m = z;
    }

    /* access modifiers changed from: package-private */
    public void e(boolean z) {
        this.o = z;
    }

    /* access modifiers changed from: package-private */
    public void f(boolean z) {
        this.t = z;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.p;
    }

    private void a(long j2) {
        try {
            Thread.sleep(j2);
        } catch (InterruptedException unused) {
        }
    }
}
