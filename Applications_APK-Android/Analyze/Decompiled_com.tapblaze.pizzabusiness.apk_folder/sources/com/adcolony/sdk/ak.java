package com.adcolony.sdk;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.util.Log;
import com.adcolony.sdk.am;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

class ak {
    static final int a = 1;
    static final int b = 2;
    static final int c = 4;
    static final int d = 1;
    static final int e = 2;
    static final int f = 512;
    static int g = 1;
    static ByteBuffer h;
    static IntBuffer i;
    static BitmapFactory.Options j = new BitmapFactory.Options();
    static int[] k = new int[1];
    am A;
    am B;
    am C;
    am D;
    am E;
    am F;
    int l;
    int m;
    int n;
    int o;
    ArrayList<a> p = new ArrayList<>();
    int q;
    int r;
    boolean s = true;
    boolean t = true;
    a u;
    int v;
    FloatBuffer w;
    FloatBuffer x;
    IntBuffer y;
    as z = new as(this);

    ak() {
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(4096);
        allocateDirect.order(ByteOrder.nativeOrder());
        this.w = allocateDirect.asFloatBuffer();
        this.w.rewind();
        ByteBuffer allocateDirect2 = ByteBuffer.allocateDirect(4096);
        allocateDirect2.order(ByteOrder.nativeOrder());
        this.x = allocateDirect2.asFloatBuffer();
        this.x.rewind();
        ByteBuffer allocateDirect3 = ByteBuffer.allocateDirect(Math.max(2048, 4194304));
        allocateDirect3.order(ByteOrder.nativeOrder());
        this.y = allocateDirect3.asIntBuffer();
        this.y.rewind();
    }

    /* access modifiers changed from: package-private */
    public void a(double d2, double d3, double d4, double d5, int i2) {
        int i3 = (i2 >> 24) & 255;
        int i4 = (i2 >> 16) & 255;
        int i5 = (i2 >> 8) & 255;
        int i6 = i2 & 255;
        if (i3 > 0) {
            this.t = false;
        }
        if (i3 < 255) {
            this.s = false;
        }
        this.w.put((float) d2);
        this.w.put((float) d3);
        this.x.put((float) d4);
        this.x.put((float) d5);
        this.y.put((((i6 * i3) / 255) << 16) | (i3 << 24) | (((i5 * i3) / 255) << 8) | ((i4 * i3) / 255));
        this.v++;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        b((a) null);
        this.z.b();
        b();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, double d2, int i4) {
        if (i2 != 0) {
            int i5 = 0;
            d();
            if ((i2 & 1) != 0) {
                i5 = 16384;
                GLES20.glClearColor(((float) ((i3 >> 16) & 255)) / 255.0f, ((float) ((i3 >> 8) & 255)) / 255.0f, ((float) (i3 & 255)) / 255.0f, ((float) ((i3 >> 24) & 255)) / 255.0f);
            }
            if ((i2 & 2) != 0) {
                i5 |= 256;
                GLES20.glClearDepthf((float) d2);
            }
            if ((i2 & 4) != 0) {
                i5 |= 1024;
                GLES20.glClearStencil(i4);
            }
            GLES20.glClear(i5);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        d();
        GLES20.glDisable(3089);
    }

    /* access modifiers changed from: package-private */
    public a a(Bitmap bitmap) {
        d();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int i2 = 1;
        int i3 = 1;
        while (i3 < width) {
            i3 <<= 1;
        }
        while (i2 < height) {
            i2 <<= 1;
        }
        int i4 = i3 * i2 * 4;
        ByteBuffer byteBuffer = h;
        if (byteBuffer == null || byteBuffer.capacity() < i4) {
            int i5 = 4194304;
            if (i4 >= 4194304) {
                i5 = i4;
            }
            h = ByteBuffer.allocateDirect(i5);
            h.order(ByteOrder.nativeOrder());
            i = h.asIntBuffer();
        }
        h.rewind();
        bitmap.copyPixelsToBuffer(h);
        bitmap.recycle();
        a aVar = new a();
        this.p.add(aVar);
        return aVar.a(i, width, height);
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.p.remove(aVar);
        k[0] = aVar.b;
        GLES20.glDeleteTextures(1, k, 0);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        d();
        b();
    }

    /* access modifiers changed from: package-private */
    public a a(int i2) {
        for (int i3 = 0; i3 < this.p.size(); i3++) {
            a aVar = this.p.get(i3);
            if (aVar.b == i2) {
                return aVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003b, code lost:
        if (r8 != 3) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d() {
        /*
            r9 = this;
            int r0 = r9.v
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            com.adcolony.sdk.as r0 = r9.z
            r0.g()
            int r0 = r9.q
            r1 = r0 & 1
            r2 = 3042(0xbe2, float:4.263E-42)
            r3 = 2
            r4 = 0
            r5 = 1
            if (r1 == 0) goto L_0x004a
            int r0 = r0 >> 8
            r0 = r0 & 15
            r1 = 771(0x303, float:1.08E-42)
            r6 = 770(0x302, float:1.079E-42)
            r7 = 3
            if (r0 == 0) goto L_0x002e
            if (r0 == r5) goto L_0x0026
            if (r0 == r3) goto L_0x002b
            if (r0 == r7) goto L_0x0028
        L_0x0026:
            r0 = 1
            goto L_0x002f
        L_0x0028:
            r0 = 771(0x303, float:1.08E-42)
            goto L_0x002f
        L_0x002b:
            r0 = 770(0x302, float:1.079E-42)
            goto L_0x002f
        L_0x002e:
            r0 = 0
        L_0x002f:
            int r8 = r9.q
            int r8 = r8 >> 12
            r8 = r8 & 15
            if (r8 == 0) goto L_0x003d
            if (r8 == r5) goto L_0x0042
            if (r8 == r3) goto L_0x003f
            if (r8 == r7) goto L_0x0043
        L_0x003d:
            r1 = 0
            goto L_0x0043
        L_0x003f:
            r1 = 770(0x302, float:1.079E-42)
            goto L_0x0043
        L_0x0042:
            r1 = 1
        L_0x0043:
            android.opengl.GLES20.glBlendFunc(r0, r1)
            android.opengl.GLES20.glEnable(r2)
            goto L_0x004d
        L_0x004a:
            android.opengl.GLES20.glDisable(r2)
        L_0x004d:
            com.adcolony.sdk.ak$a r0 = r9.u
            r1 = 3553(0xde1, float:4.979E-42)
            if (r0 == 0) goto L_0x00e0
            android.opengl.GLES20.glEnable(r1)
            r0 = 33984(0x84c0, float:4.7622E-41)
            android.opengl.GLES20.glActiveTexture(r0)
            com.adcolony.sdk.ak$a r0 = r9.u
            int r0 = r0.a
            android.opengl.GLES20.glBindTexture(r1, r0)
            int r0 = r9.q
            r0 = r0 & 12
            r2 = 10243(0x2803, float:1.4354E-41)
            r6 = 10242(0x2802, float:1.4352E-41)
            if (r0 == 0) goto L_0x0077
            r0 = 1176765440(0x46240400, float:10497.0)
            android.opengl.GLES20.glTexParameterf(r1, r6, r0)
            android.opengl.GLES20.glTexParameterf(r1, r2, r0)
            goto L_0x0080
        L_0x0077:
            r0 = 1191259904(0x47012f00, float:33071.0)
            android.opengl.GLES20.glTexParameterf(r1, r6, r0)
            android.opengl.GLES20.glTexParameterf(r1, r2, r0)
        L_0x0080:
            int r0 = r9.q
            r0 = r0 & 16
            r2 = 10241(0x2801, float:1.435E-41)
            if (r0 == 0) goto L_0x0095
            r0 = 1175977984(0x46180000, float:9728.0)
            android.opengl.GLES20.glTexParameterf(r1, r2, r0)
            r0 = 10240(0x2800, float:1.4349E-41)
            r2 = 1175977984(0x46180000, float:9728.0)
            android.opengl.GLES20.glTexParameterf(r1, r0, r2)
            goto L_0x00a3
        L_0x0095:
            r0 = 1175979008(0x46180400, float:9729.0)
            android.opengl.GLES20.glTexParameterf(r1, r2, r0)
            r0 = 10240(0x2800, float:1.4349E-41)
            r2 = 1175979008(0x46180400, float:9729.0)
            android.opengl.GLES20.glTexParameterf(r1, r0, r2)
        L_0x00a3:
            int r0 = r9.q
            r1 = 16711680(0xff0000, float:2.3418052E-38)
            r0 = r0 & r1
            r1 = 65536(0x10000, float:9.18355E-41)
            if (r0 == r1) goto L_0x00da
            r1 = 131072(0x20000, float:1.83671E-40)
            if (r0 == r1) goto L_0x00d4
            r1 = 196608(0x30000, float:2.75506E-40)
            if (r0 == r1) goto L_0x00ba
            com.adcolony.sdk.am r0 = r9.B
            r0.a()
            goto L_0x00e8
        L_0x00ba:
            boolean r0 = r9.s
            if (r0 == 0) goto L_0x00c4
            com.adcolony.sdk.am r0 = r9.E
            r0.a()
            goto L_0x00e8
        L_0x00c4:
            boolean r0 = r9.t
            if (r0 == 0) goto L_0x00ce
            com.adcolony.sdk.am r0 = r9.B
            r0.a()
            goto L_0x00e8
        L_0x00ce:
            com.adcolony.sdk.am r0 = r9.F
            r0.a()
            goto L_0x00e8
        L_0x00d4:
            com.adcolony.sdk.am r0 = r9.D
            r0.a()
            goto L_0x00e8
        L_0x00da:
            com.adcolony.sdk.am r0 = r9.C
            r0.a()
            goto L_0x00e8
        L_0x00e0:
            android.opengl.GLES20.glDisable(r1)
            com.adcolony.sdk.am r0 = r9.A
            r0.a()
        L_0x00e8:
            int r0 = r9.r
            if (r0 == r5) goto L_0x00f6
            if (r0 == r3) goto L_0x00ef
            goto L_0x0100
        L_0x00ef:
            r0 = 4
            int r1 = r9.v
            android.opengl.GLES20.glDrawArrays(r0, r4, r1)
            goto L_0x0100
        L_0x00f6:
            com.adcolony.sdk.am r0 = r9.A
            r0.a()
            int r0 = r9.v
            android.opengl.GLES20.glDrawArrays(r5, r4, r0)
        L_0x0100:
            r9.v = r4
            java.nio.FloatBuffer r0 = r9.w
            r0.rewind()
            java.nio.FloatBuffer r0 = r9.x
            r0.rewind()
            java.nio.IntBuffer r0 = r9.y
            r0.rewind()
            r9.s = r5
            r9.t = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.ak.d():void");
    }

    /* access modifiers changed from: package-private */
    public a a(String str) {
        d();
        BitmapFactory.Options options = j;
        options.inScaled = false;
        Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
        if (decodeFile == null) {
            PrintStream printStream = System.out;
            printStream.println("Failed to load " + str);
            decodeFile = Bitmap.createBitmap(16, 16, Bitmap.Config.ARGB_8888);
        }
        return a(decodeFile);
    }

    /* access modifiers changed from: package-private */
    public a a(InputStream inputStream) {
        d();
        BitmapFactory.Options options = j;
        options.inScaled = false;
        Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, null, options);
        if (decodeStream == null) {
            Log.w("ADC3", "Failed to decode input stream.");
            decodeStream = Bitmap.createBitmap(16, 16, Bitmap.Config.ARGB_8888);
        }
        return a(decodeStream);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        System.out.println("ADCRenderer on_surface_created()");
        this.A = new am.a(this);
        this.B = new am.b(this);
        this.C = new am.d(this);
        this.D = new am.c(this);
        this.E = new am.e(this);
        this.F = new am.f(this);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        PrintStream printStream = System.out;
        printStream.println("ADCRenderer on_surface_changed " + i2 + "x" + i3);
        this.l = i2;
        this.m = i3;
        this.n = i2;
        this.o = i3;
        GLES20.glViewport(0, 0, i2, i3);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, int i5) {
        d();
        double d2 = (double) this.l;
        double d3 = (double) this.n;
        Double.isNaN(d2);
        Double.isNaN(d3);
        double d4 = d2 / d3;
        int i6 = this.m;
        double d5 = (double) i6;
        double d6 = (double) this.o;
        Double.isNaN(d5);
        Double.isNaN(d6);
        double d7 = d5 / d6;
        double d8 = (double) i4;
        Double.isNaN(d8);
        int i7 = (int) (d8 * d4);
        double d9 = (double) i5;
        Double.isNaN(d9);
        int i8 = (int) (d9 * d7);
        double d10 = (double) i2;
        Double.isNaN(d10);
        double d11 = (double) i3;
        Double.isNaN(d11);
        GLES20.glScissor((int) (d10 * d4), i6 - (((int) (d11 * d7)) + i8), i7, i8);
        GLES20.glEnable(3089);
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.r != 1) {
            d();
            this.r = 1;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, double d2) {
        this.n = i2;
        this.o = i3;
        as asVar = this.z;
        asVar.a(asVar.d.a(i2, i3, d2));
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        if (i2 != this.q) {
            d();
            this.q = i2;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3, int i4, int i5) {
        b(i2 | 2 | (i3 << 8) | (i4 << 12) | (i5 << 16));
    }

    /* access modifiers changed from: package-private */
    public void b(a aVar) {
        if (aVar != this.u) {
            d();
            this.u = aVar;
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (this.r != 2) {
            d();
            this.r = 2;
        }
    }

    class a {
        int a = ak.k[0];
        int b;
        int c;
        int d;
        int e;
        int f;
        double g;
        double h;

        a() {
            int i2 = ak.g;
            ak.g = i2 + 1;
            this.b = i2;
            GLES20.glGenTextures(1, ak.k, 0);
        }

        /* access modifiers changed from: package-private */
        public a a(IntBuffer intBuffer, int i2, int i3) {
            int i4 = 1;
            int i5 = 1;
            while (i5 < i2) {
                i5 <<= 1;
            }
            while (i4 < i3) {
                i4 <<= 1;
            }
            this.c = i2;
            this.d = i3;
            this.e = i5;
            this.f = i4;
            double d2 = (double) this.c;
            double d3 = (double) this.e;
            Double.isNaN(d2);
            Double.isNaN(d3);
            this.g = d2 / d3;
            double d4 = (double) this.d;
            double d5 = (double) this.f;
            Double.isNaN(d4);
            Double.isNaN(d5);
            this.h = d4 / d5;
            intBuffer.rewind();
            int i6 = i2 * i3;
            while (true) {
                i6--;
                if (i6 < 0) {
                    break;
                }
                int i7 = intBuffer.get(i6);
                intBuffer.put(i6, ((i7 << 16) & 16711680) | (-16711936 & i7) | ((i7 >> 16) & 255));
            }
            intBuffer.rewind();
            int i8 = this.c;
            int i9 = this.e;
            if (i8 < i9) {
                int i10 = this.d;
                int i11 = ((i10 - 1) * i9) + i8;
                int i12 = i8 * i10;
                int i13 = i9 - i8;
                while (true) {
                    i10--;
                    if (i10 < 0) {
                        break;
                    }
                    intBuffer.put(i11, intBuffer.get(i12 - 1));
                    int i14 = this.c;
                    while (true) {
                        i14--;
                        if (i14 < 0) {
                            break;
                        }
                        i11--;
                        i12--;
                        intBuffer.put(i11, intBuffer.get(i12));
                    }
                    i11 -= i13;
                }
            }
            intBuffer.rewind();
            int i15 = this.d;
            if (i15 < this.f) {
                int i16 = this.e;
                int i17 = i15 * i16;
                int i18 = i17 + i16;
                while (true) {
                    i16--;
                    if (i16 < 0) {
                        break;
                    }
                    i18--;
                    i17--;
                    intBuffer.put(i18, intBuffer.get(i17));
                }
            }
            GLES20.glBindTexture(3553, this.a);
            intBuffer.rewind();
            GLES20.glTexImage2D(3553, 0, 6408, this.e, this.f, 0, 6408, 5121, intBuffer);
            System.out.println("ADC3 Texture::set gl_texture_id:" + this.a + " texture_id:" + this.b + " w:" + i2 + " h:" + i3);
            return this;
        }
    }
}
