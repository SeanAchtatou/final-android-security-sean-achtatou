package com.adcolony.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import com.adcolony.sdk.w;
import org.json.JSONObject;

class m {
    static AlertDialog a;
    /* access modifiers changed from: private */
    public ab b;
    /* access modifiers changed from: private */
    public AlertDialog c;
    /* access modifiers changed from: private */
    public boolean d;

    m() {
        a.a("Alert.show", new ad() {
            public void a(ab abVar) {
                if (!a.d() || !(a.c() instanceof Activity)) {
                    new w.a().a("Missing Activity reference, can't build AlertDialog.").a(w.g);
                } else if (u.d(abVar.c(), "on_resume")) {
                    ab unused = m.this.b = abVar;
                } else {
                    m.this.a(abVar);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(final ab abVar) {
        Context c2 = a.c();
        if (c2 != null) {
            final AlertDialog.Builder builder = Build.VERSION.SDK_INT >= 21 ? new AlertDialog.Builder(c2, 16974374) : new AlertDialog.Builder(c2, 16974126);
            JSONObject c3 = abVar.c();
            String b2 = u.b(c3, "message");
            String b3 = u.b(c3, "title");
            String b4 = u.b(c3, "positive");
            String b5 = u.b(c3, "negative");
            builder.setMessage(b2);
            builder.setTitle(b3);
            builder.setPositiveButton(b4, new DialogInterface.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
                 arg types: [org.json.JSONObject, java.lang.String, int]
                 candidates:
                  com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
                  com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.m.a(com.adcolony.sdk.m, boolean):boolean
                 arg types: [com.adcolony.sdk.m, int]
                 candidates:
                  com.adcolony.sdk.m.a(com.adcolony.sdk.m, android.app.AlertDialog):android.app.AlertDialog
                  com.adcolony.sdk.m.a(com.adcolony.sdk.m, com.adcolony.sdk.ab):com.adcolony.sdk.ab
                  com.adcolony.sdk.m.a(com.adcolony.sdk.m, boolean):boolean */
                public void onClick(DialogInterface dialogInterface, int i) {
                    AlertDialog unused = m.this.c = (AlertDialog) null;
                    dialogInterface.dismiss();
                    JSONObject a2 = u.a();
                    u.b(a2, "positive", true);
                    boolean unused2 = m.this.d = false;
                    abVar.a(a2).b();
                }
            });
            if (!b5.equals("")) {
                builder.setNegativeButton(b5, new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
                     arg types: [org.json.JSONObject, java.lang.String, int]
                     candidates:
                      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
                      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.adcolony.sdk.m.a(com.adcolony.sdk.m, boolean):boolean
                     arg types: [com.adcolony.sdk.m, int]
                     candidates:
                      com.adcolony.sdk.m.a(com.adcolony.sdk.m, android.app.AlertDialog):android.app.AlertDialog
                      com.adcolony.sdk.m.a(com.adcolony.sdk.m, com.adcolony.sdk.ab):com.adcolony.sdk.ab
                      com.adcolony.sdk.m.a(com.adcolony.sdk.m, boolean):boolean */
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog unused = m.this.c = (AlertDialog) null;
                        dialogInterface.dismiss();
                        JSONObject a2 = u.a();
                        u.b(a2, "positive", false);
                        boolean unused2 = m.this.d = false;
                        abVar.a(a2).b();
                    }
                });
            }
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.m.a(com.adcolony.sdk.m, boolean):boolean
                 arg types: [com.adcolony.sdk.m, int]
                 candidates:
                  com.adcolony.sdk.m.a(com.adcolony.sdk.m, android.app.AlertDialog):android.app.AlertDialog
                  com.adcolony.sdk.m.a(com.adcolony.sdk.m, com.adcolony.sdk.ab):com.adcolony.sdk.ab
                  com.adcolony.sdk.m.a(com.adcolony.sdk.m, boolean):boolean */
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
                 arg types: [org.json.JSONObject, java.lang.String, int]
                 candidates:
                  com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
                  com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
                public void onCancel(DialogInterface dialogInterface) {
                    AlertDialog unused = m.this.c = (AlertDialog) null;
                    boolean unused2 = m.this.d = false;
                    JSONObject a2 = u.a();
                    u.b(a2, "positive", false);
                    abVar.a(a2).b();
                }
            });
            at.a(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.m.a(com.adcolony.sdk.m, boolean):boolean
                 arg types: [com.adcolony.sdk.m, int]
                 candidates:
                  com.adcolony.sdk.m.a(com.adcolony.sdk.m, android.app.AlertDialog):android.app.AlertDialog
                  com.adcolony.sdk.m.a(com.adcolony.sdk.m, com.adcolony.sdk.ab):com.adcolony.sdk.ab
                  com.adcolony.sdk.m.a(com.adcolony.sdk.m, boolean):boolean */
                public void run() {
                    boolean unused = m.this.d = true;
                    AlertDialog unused2 = m.this.c = builder.show();
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        ab abVar = this.b;
        if (abVar != null) {
            a(abVar);
            this.b = null;
        }
    }

    /* access modifiers changed from: package-private */
    public AlertDialog b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(AlertDialog alertDialog) {
        this.c = alertDialog;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.d;
    }
}
