package com.adcolony.sdk;

import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

public class AdColonyCustomMessage {
    String a;
    String b;

    public AdColonyCustomMessage(String str, String str2) {
        if (at.d(str) || at.d(str2)) {
            this.a = str;
            this.b = str2;
        }
    }

    public String getMessage() {
        return this.b;
    }

    public String getType() {
        return this.a;
    }

    public AdColonyCustomMessage set(String str, String str2) {
        this.a = str;
        this.b = str2;
        return this;
    }

    public void send() {
        try {
            AdColony.a.execute(new Runnable() {
                public void run() {
                    AdColony.a();
                    JSONObject a2 = u.a();
                    u.a(a2, "type", AdColonyCustomMessage.this.a);
                    u.a(a2, "message", AdColonyCustomMessage.this.b);
                    new ab("CustomMessage.native_send", 1, a2).b();
                }
            });
        } catch (RejectedExecutionException unused) {
        }
    }
}
