package com.adcolony.sdk;

import android.util.Log;
import com.adcolony.sdk.x;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ai {
    static final String a = "adcolony_android";
    static final String b = "adcolony_fatal_reports";
    v c;
    ScheduledExecutorService d;
    List<x> e = new ArrayList();
    List<x> f = new ArrayList();
    HashMap<String, Object> g;
    private t h = new t(a, "4.1.0", "Production");
    private t i = new t(b, "4.1.0", "Production");

    ai(v vVar, ScheduledExecutorService scheduledExecutorService, HashMap<String, Object> hashMap) {
        this.c = vVar;
        this.d = scheduledExecutorService;
        this.g = hashMap;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(String str) {
        this.g.put("controllerVersion", str);
    }

    /* access modifiers changed from: package-private */
    public synchronized void b(String str) {
        this.g.put("sessionId", str);
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(long j, TimeUnit timeUnit) {
        try {
            if (!this.d.isShutdown() && !this.d.isTerminated()) {
                this.d.scheduleAtFixedRate(new Runnable() {
                    public void run() {
                        ai.this.b();
                    }
                }, j, j, timeUnit);
            }
        } catch (RuntimeException unused) {
            Log.e("ADCLogError", "Internal error when submitting remote log to executor service");
        }
        return;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:9|10) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r4.d.shutdownNow();
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0040 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a() {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.concurrent.ScheduledExecutorService r0 = r4.d     // Catch:{ all -> 0x004e }
            r0.shutdown()     // Catch:{ all -> 0x004e }
            java.util.concurrent.ScheduledExecutorService r0 = r4.d     // Catch:{ InterruptedException -> 0x0040 }
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ InterruptedException -> 0x0040 }
            r2 = 1
            boolean r0 = r0.awaitTermination(r2, r1)     // Catch:{ InterruptedException -> 0x0040 }
            if (r0 != 0) goto L_0x004c
            java.util.concurrent.ScheduledExecutorService r0 = r4.d     // Catch:{ InterruptedException -> 0x0040 }
            r0.shutdownNow()     // Catch:{ InterruptedException -> 0x0040 }
            java.util.concurrent.ScheduledExecutorService r0 = r4.d     // Catch:{ InterruptedException -> 0x0040 }
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ InterruptedException -> 0x0040 }
            boolean r0 = r0.awaitTermination(r2, r1)     // Catch:{ InterruptedException -> 0x0040 }
            if (r0 != 0) goto L_0x004c
            java.io.PrintStream r0 = java.lang.System.err     // Catch:{ InterruptedException -> 0x0040 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0040 }
            r1.<init>()     // Catch:{ InterruptedException -> 0x0040 }
            java.lang.Class r2 = r4.getClass()     // Catch:{ InterruptedException -> 0x0040 }
            java.lang.String r2 = r2.getSimpleName()     // Catch:{ InterruptedException -> 0x0040 }
            r1.append(r2)     // Catch:{ InterruptedException -> 0x0040 }
            java.lang.String r2 = ": ScheduledExecutorService did not terminate"
            r1.append(r2)     // Catch:{ InterruptedException -> 0x0040 }
            java.lang.String r1 = r1.toString()     // Catch:{ InterruptedException -> 0x0040 }
            r0.println(r1)     // Catch:{ InterruptedException -> 0x0040 }
            goto L_0x004c
        L_0x0040:
            java.util.concurrent.ScheduledExecutorService r0 = r4.d     // Catch:{ all -> 0x004e }
            r0.shutdownNow()     // Catch:{ all -> 0x004e }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x004e }
            r0.interrupt()     // Catch:{ all -> 0x004e }
        L_0x004c:
            monitor-exit(r4)
            return
        L_0x004e:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.ai.a():void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0039 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b() {
        /*
            r2 = this;
            monitor-enter(r2)
            monitor-enter(r2)     // Catch:{ all -> 0x0049 }
            java.util.List<com.adcolony.sdk.x> r0 = r2.e     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            int r0 = r0.size()     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            if (r0 <= 0) goto L_0x001c
            com.adcolony.sdk.t r0 = r2.h     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.util.List<com.adcolony.sdk.x> r1 = r2.e     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.lang.String r0 = r2.a(r0, r1)     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            com.adcolony.sdk.v r1 = r2.c     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            r1.a(r0)     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.util.List<com.adcolony.sdk.x> r0 = r2.e     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            r0.clear()     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
        L_0x001c:
            java.util.List<com.adcolony.sdk.x> r0 = r2.f     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            int r0 = r0.size()     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            if (r0 <= 0) goto L_0x0044
            com.adcolony.sdk.t r0 = r2.i     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.util.List<com.adcolony.sdk.x> r1 = r2.f     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.lang.String r0 = r2.a(r0, r1)     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            com.adcolony.sdk.v r1 = r2.c     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            r1.a(r0)     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.util.List<com.adcolony.sdk.x> r0 = r2.f     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            r0.clear()     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            goto L_0x0044
        L_0x0037:
            r0 = move-exception
            goto L_0x0047
        L_0x0039:
            java.util.List<com.adcolony.sdk.x> r0 = r2.e     // Catch:{ all -> 0x0037 }
            r0.clear()     // Catch:{ all -> 0x0037 }
            goto L_0x0044
        L_0x003f:
            java.util.List<com.adcolony.sdk.x> r0 = r2.e     // Catch:{ all -> 0x0037 }
            r0.clear()     // Catch:{ all -> 0x0037 }
        L_0x0044:
            monitor-exit(r2)     // Catch:{ all -> 0x0037 }
            monitor-exit(r2)
            return
        L_0x0047:
            monitor-exit(r2)     // Catch:{ all -> 0x0037 }
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.ai.b():void");
    }

    /* access modifiers changed from: package-private */
    public synchronized void c(String str) {
        a(new x.a().a(3).a(this.h).a(str).a());
    }

    /* access modifiers changed from: package-private */
    public synchronized void d(String str) {
        a(new x.a().a(2).a(this.h).a(str).a());
    }

    /* access modifiers changed from: package-private */
    public synchronized void e(String str) {
        a(new x.a().a(1).a(this.h).a(str).a());
    }

    /* access modifiers changed from: package-private */
    public synchronized void f(String str) {
        a(new x.a().a(0).a(this.h).a(str).a());
    }

    /* access modifiers changed from: package-private */
    public void a(q qVar) {
        qVar.a(this.i);
        qVar.a(-1);
        b(qVar);
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(final x xVar) {
        try {
            if (!this.d.isShutdown() && !this.d.isTerminated()) {
                this.d.submit(new Runnable() {
                    public void run() {
                        ai.this.e.add(xVar);
                    }
                });
            }
        } catch (RejectedExecutionException unused) {
            Log.e("ADCLogError", "Internal error when submitting remote log to executor service");
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public synchronized void b(x xVar) {
        this.f.add(xVar);
    }

    /* access modifiers changed from: package-private */
    public String a(t tVar, List<x> list) throws IOException, JSONException {
        String c2 = a.a().m().c();
        String str = this.g.get("advertiserId") != null ? (String) this.g.get("advertiserId") : "unknown";
        if (c2 != null && c2.length() > 0 && !c2.equals(str)) {
            this.g.put("advertiserId", c2);
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(FirebaseAnalytics.Param.INDEX, tVar.b());
        jSONObject.put("environment", tVar.d());
        jSONObject.put("version", tVar.c());
        JSONArray jSONArray = new JSONArray();
        for (x c3 : list) {
            jSONArray.put(c(c3));
        }
        jSONObject.put("logs", jSONArray);
        return jSONObject.toString();
    }

    private synchronized JSONObject c(x xVar) throws JSONException {
        JSONObject jSONObject;
        jSONObject = new JSONObject(this.g);
        jSONObject.put("environment", xVar.f().d());
        jSONObject.put("level", xVar.b());
        jSONObject.put("message", xVar.d());
        jSONObject.put("clientTimestamp", xVar.e());
        JSONObject mediationInfo = a.a().d().getMediationInfo();
        JSONObject pluginInfo = a.a().d().getPluginInfo();
        double t = a.a().m().t();
        jSONObject.put("mediation_network", u.b(mediationInfo, "name"));
        jSONObject.put("mediation_network_version", u.b(mediationInfo, "version"));
        jSONObject.put("plugin", u.b(pluginInfo, "name"));
        jSONObject.put("plugin_version", u.b(pluginInfo, "version"));
        jSONObject.put("batteryInfo", t);
        if (xVar instanceof q) {
            jSONObject = u.a(jSONObject, ((q) xVar).a());
            jSONObject.put("platform", "android");
        }
        return jSONObject;
    }
}
