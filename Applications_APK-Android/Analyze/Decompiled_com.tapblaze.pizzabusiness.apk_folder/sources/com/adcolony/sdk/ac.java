package com.adcolony.sdk;

import com.adcolony.sdk.w;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ac {
    private ArrayList<ae> a = new ArrayList<>();
    private HashMap<Integer, ae> b = new HashMap<>();
    private int c = 2;
    private HashMap<String, ArrayList<ad>> d = new HashMap<>();
    private JSONArray e = u.b();
    private int f = 1;

    static /* synthetic */ int a(ac acVar) {
        int i = acVar.f;
        acVar.f = i + 1;
        return i;
    }

    ac() {
    }

    /* access modifiers changed from: package-private */
    public void a(String str, ad adVar) {
        ArrayList arrayList = this.d.get(str);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.d.put(str, arrayList);
        }
        arrayList.add(adVar);
    }

    /* access modifiers changed from: package-private */
    public void b(String str, ad adVar) {
        synchronized (this.d) {
            ArrayList arrayList = this.d.get(str);
            if (arrayList != null) {
                arrayList.remove(adVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        r0 = com.adcolony.sdk.a.c();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r2 = this;
            com.adcolony.sdk.j r0 = com.adcolony.sdk.a.a()
            boolean r1 = r0.g()
            if (r1 != 0) goto L_0x001f
            boolean r0 = r0.h()
            if (r0 != 0) goto L_0x001f
            android.content.Context r0 = com.adcolony.sdk.a.c()
            if (r0 != 0) goto L_0x0017
            goto L_0x001f
        L_0x0017:
            com.adcolony.sdk.ac$1 r1 = new com.adcolony.sdk.ac$1
            r1.<init>(r0)
            com.adcolony.sdk.at.a(r1)
        L_0x001f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.ac.a():void");
    }

    /* access modifiers changed from: package-private */
    public ae a(ae aeVar) {
        synchronized (this.a) {
            int b2 = aeVar.b();
            if (b2 <= 0) {
                b2 = aeVar.a();
            }
            this.a.add(aeVar);
            this.b.put(Integer.valueOf(b2), aeVar);
        }
        return aeVar;
    }

    /* access modifiers changed from: package-private */
    public ae a(int i) {
        synchronized (this.a) {
            ae aeVar = this.b.get(Integer.valueOf(i));
            if (aeVar == null) {
                return null;
            }
            this.a.remove(aeVar);
            this.b.remove(Integer.valueOf(i));
            aeVar.c();
            return aeVar;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void b() {
        synchronized (this.a) {
            for (int size = this.a.size() - 1; size >= 0; size--) {
                this.a.get(size).d();
            }
        }
        JSONArray jSONArray = null;
        if (this.e.length() > 0) {
            jSONArray = this.e;
            this.e = u.b();
        }
        if (jSONArray != null) {
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    final JSONObject jSONObject = jSONArray.getJSONObject(i);
                    final String string = jSONObject.getString("m_type");
                    if (jSONObject.getInt("m_origin") >= 2) {
                        at.a(new Runnable() {
                            public void run() {
                                ac.this.a(string, jSONObject);
                            }
                        });
                    } else {
                        a(string, jSONObject);
                    }
                } catch (JSONException e2) {
                    new w.a().a("JSON error from message dispatcher's updateModules(): ").a(e2.toString()).a(w.h);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, JSONObject jSONObject) {
        synchronized (this.d) {
            ArrayList arrayList = this.d.get(str);
            if (arrayList != null) {
                ab abVar = new ab(jSONObject);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    try {
                        ((ad) it.next()).a(abVar);
                    } catch (RuntimeException e2) {
                        new w.a().a(e2).a(w.h);
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        try {
            if (!jSONObject.has("m_id")) {
                int i = this.f;
                this.f = i + 1;
                jSONObject.put("m_id", i);
            }
            if (!jSONObject.has("m_origin")) {
                jSONObject.put("m_origin", 0);
            }
            int i2 = jSONObject.getInt("m_target");
            if (i2 == 0) {
                synchronized (this) {
                    this.e.put(jSONObject);
                }
                return;
            }
            ae aeVar = this.b.get(Integer.valueOf(i2));
            if (aeVar != null) {
                aeVar.a(jSONObject);
            }
        } catch (JSONException e2) {
            new w.a().a("JSON error in ADCMessageDispatcher's sendMessage(): ").a(e2.toString()).a(w.h);
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<ae> c() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        int i = this.c;
        this.c = i + 1;
        return i;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, ae> e() {
        return this.b;
    }
}
