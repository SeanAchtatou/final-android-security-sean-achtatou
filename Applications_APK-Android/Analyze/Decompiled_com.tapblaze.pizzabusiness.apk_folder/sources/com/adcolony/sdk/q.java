package com.adcolony.sdk;

import com.adcolony.sdk.x;
import java.util.Date;
import org.json.JSONObject;

class q extends x {
    static final t a = new t("adcolony_fatal_reports", "4.1.0", "Production");
    static final String b = "sourceFile";
    static final String c = "lineNumber";
    static final String d = "methodName";
    static final String e = "stackTrace";
    static final String f = "isAdActive";
    static final String g = "activeAdId";
    static final String h = "active_creative_ad_id";
    static final String i = "listOfCachedAds";
    static final String j = "listOfCreativeAdIds";
    static final String k = "adCacheSize";
    /* access modifiers changed from: private */
    public JSONObject p;

    q() {
    }

    /* access modifiers changed from: package-private */
    public q a(JSONObject jSONObject) {
        a aVar = new a();
        aVar.a(jSONObject);
        aVar.a(u.b(jSONObject, "message"));
        try {
            aVar.a(new Date(Long.parseLong(u.b(jSONObject, "timestamp"))));
        } catch (NumberFormatException unused) {
        }
        aVar.a(a);
        aVar.a(-1);
        return (q) aVar.a();
    }

    /* access modifiers changed from: package-private */
    public JSONObject a() {
        return this.p;
    }

    private class a extends x.a {
        a() {
            this.b = new q();
        }

        /* access modifiers changed from: package-private */
        public a a(JSONObject jSONObject) {
            JSONObject unused = ((q) this.b).p = jSONObject;
            return this;
        }

        /* access modifiers changed from: package-private */
        public x.a a(Date date) {
            u.a(((q) this.b).p, "timestamp", x.l.format(date));
            return super.a(date);
        }
    }
}
