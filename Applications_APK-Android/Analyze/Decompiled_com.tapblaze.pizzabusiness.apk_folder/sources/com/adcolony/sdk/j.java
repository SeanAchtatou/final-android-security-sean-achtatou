package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.adcolony.sdk.n;
import com.adcolony.sdk.w;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.iab.omid.library.adcolony.adsession.Partner;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

class j implements n.a {
    private static volatile String H = "";
    static final String a = "026ae9c9824b3e483fa6c71fa88f57ae27816141";
    static final String b = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5";
    static String e = "https://adc3-launch.adcolony.com/v4/launch";
    private HashMap<String, AdColonyZone> A = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, av> B = new HashMap<>();
    private String C;
    private String D;
    private String E;
    private String F;
    private String G = "";
    private boolean I;
    /* access modifiers changed from: private */
    public boolean J;
    private boolean K;
    /* access modifiers changed from: private */
    public boolean L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    /* access modifiers changed from: private */
    public boolean Q;
    /* access modifiers changed from: private */
    public boolean R;
    /* access modifiers changed from: private */
    public boolean S;
    private int T;
    /* access modifiers changed from: private */
    public int U = 1;
    private final int V = 120;
    private Application.ActivityLifecycleCallbacks W;
    /* access modifiers changed from: private */
    public Partner X = null;
    l c;
    af d;
    boolean f;
    private k g;
    /* access modifiers changed from: private */
    public ac h;
    /* access modifiers changed from: private */
    public o i;
    /* access modifiers changed from: private */
    public al j;
    private d k;
    /* access modifiers changed from: private */
    public m l;
    private r m;
    private aq n;
    /* access modifiers changed from: private */
    public ao o;
    private y p;
    private c q;
    private AdColonyAdView r;
    private AdColonyInterstitial s;
    /* access modifiers changed from: private */
    public AdColonyRewardListener t;
    private HashMap<String, AdColonyCustomMessageListener> u = new HashMap<>();
    /* access modifiers changed from: private */
    public AdColonyAppOptions v;
    /* access modifiers changed from: private */
    public ab w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public ab y;
    private JSONObject z;

    j() {
    }

    /* access modifiers changed from: package-private */
    public Context a() {
        return a.c();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0113  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.adcolony.sdk.AdColonyAppOptions r5, boolean r6) {
        /*
            r4 = this;
            r4.K = r6
            r4.v = r5
            com.adcolony.sdk.ac r0 = new com.adcolony.sdk.ac
            r0.<init>()
            r4.h = r0
            com.adcolony.sdk.k r0 = new com.adcolony.sdk.k
            r0.<init>()
            r4.g = r0
            com.adcolony.sdk.o r0 = new com.adcolony.sdk.o
            r0.<init>()
            r4.i = r0
            com.adcolony.sdk.o r0 = r4.i
            r0.a()
            com.adcolony.sdk.al r0 = new com.adcolony.sdk.al
            r0.<init>()
            r4.j = r0
            com.adcolony.sdk.al r0 = r4.j
            r0.a()
            com.adcolony.sdk.d r0 = new com.adcolony.sdk.d
            r0.<init>()
            r4.k = r0
            com.adcolony.sdk.d r0 = r4.k
            r0.a()
            com.adcolony.sdk.m r0 = new com.adcolony.sdk.m
            r0.<init>()
            r4.l = r0
            com.adcolony.sdk.r r0 = new com.adcolony.sdk.r
            r0.<init>()
            r4.m = r0
            com.adcolony.sdk.r r0 = r4.m
            r0.a()
            com.adcolony.sdk.y r0 = new com.adcolony.sdk.y
            r0.<init>()
            r4.p = r0
            com.adcolony.sdk.y r0 = r4.p
            com.adcolony.sdk.y.c()
            com.adcolony.sdk.ao r0 = new com.adcolony.sdk.ao
            r0.<init>()
            r4.o = r0
            com.adcolony.sdk.ao r0 = r4.o
            r0.a()
            com.adcolony.sdk.aq r0 = new com.adcolony.sdk.aq
            r0.<init>()
            r4.n = r0
            com.adcolony.sdk.aq r0 = r4.n
            r0.a()
            com.adcolony.sdk.l r0 = new com.adcolony.sdk.l
            r0.<init>()
            r4.c = r0
            com.adcolony.sdk.l r0 = r4.c
            r0.e()
            com.adcolony.sdk.af r0 = new com.adcolony.sdk.af
            r0.<init>()
            r4.d = r0
            com.adcolony.sdk.af r0 = r4.d
            java.lang.String r0 = r0.c()
            r4.C = r0
            android.content.Context r0 = com.adcolony.sdk.a.c()
            com.adcolony.sdk.AdColony.a(r0, r5)
            r5 = 0
            r0 = 1
            if (r6 != 0) goto L_0x013b
            java.io.File r6 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.adcolony.sdk.ao r2 = r4.o
            java.lang.String r2 = r2.g()
            r1.append(r2)
            java.lang.String r2 = "026ae9c9824b3e483fa6c71fa88f57ae27816141"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r6.<init>(r1)
            boolean r6 = r6.exists()
            r4.N = r6
            java.io.File r6 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.adcolony.sdk.ao r3 = r4.o
            java.lang.String r3 = r3.g()
            r1.append(r3)
            java.lang.String r3 = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5"
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r6.<init>(r1)
            boolean r6 = r6.exists()
            r4.O = r6
            boolean r6 = r4.N
            if (r6 == 0) goto L_0x010c
            boolean r6 = r4.O
            if (r6 == 0) goto L_0x010c
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            com.adcolony.sdk.ao r1 = r4.o
            java.lang.String r1 = r1.g()
            r6.append(r1)
            r6.append(r2)
            java.lang.String r6 = r6.toString()
            org.json.JSONObject r6 = com.adcolony.sdk.u.c(r6)
            java.lang.String r1 = "sdkVersion"
            java.lang.String r6 = com.adcolony.sdk.u.b(r6, r1)
            com.adcolony.sdk.l r1 = r4.c
            java.lang.String r1 = r1.F()
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x010c
            r6 = 1
            goto L_0x010d
        L_0x010c:
            r6 = 0
        L_0x010d:
            r4.M = r6
            boolean r6 = r4.N
            if (r6 == 0) goto L_0x0133
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            com.adcolony.sdk.ao r1 = r4.o
            java.lang.String r1 = r1.g()
            r6.append(r1)
            r6.append(r2)
            java.lang.String r6 = r6.toString()
            org.json.JSONObject r6 = com.adcolony.sdk.u.c(r6)
            r4.z = r6
            org.json.JSONObject r6 = r4.z
            r4.b(r6)
        L_0x0133:
            boolean r6 = r4.M
            r4.e(r6)
            r4.I()
        L_0x013b:
            com.adcolony.sdk.j$1 r6 = new com.adcolony.sdk.j$1
            r6.<init>()
            java.lang.String r1 = "Module.load"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$12 r6 = new com.adcolony.sdk.j$12
            r6.<init>()
            java.lang.String r1 = "Module.unload"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$14 r6 = new com.adcolony.sdk.j$14
            r6.<init>()
            java.lang.String r1 = "AdColony.on_configured"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$15 r6 = new com.adcolony.sdk.j$15
            r6.<init>()
            java.lang.String r1 = "AdColony.get_app_info"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$16 r6 = new com.adcolony.sdk.j$16
            r6.<init>()
            java.lang.String r1 = "AdColony.v4vc_reward"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$17 r6 = new com.adcolony.sdk.j$17
            r6.<init>()
            java.lang.String r1 = "AdColony.zone_info"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$18 r6 = new com.adcolony.sdk.j$18
            r6.<init>()
            java.lang.String r1 = "AdColony.probe_launch_server"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$19 r6 = new com.adcolony.sdk.j$19
            r6.<init>()
            java.lang.String r1 = "Crypto.sha1"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$20 r6 = new com.adcolony.sdk.j$20
            r6.<init>()
            java.lang.String r1 = "Crypto.crc32"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$2 r6 = new com.adcolony.sdk.j$2
            r6.<init>()
            java.lang.String r1 = "Crypto.uuid"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$3 r6 = new com.adcolony.sdk.j$3
            r6.<init>()
            java.lang.String r1 = "Device.query_advertiser_info"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.j$4 r6 = new com.adcolony.sdk.j$4
            r6.<init>()
            java.lang.String r1 = "AdColony.controller_version"
            com.adcolony.sdk.a.a(r1, r6)
            com.adcolony.sdk.ao r6 = r4.o
            int r6 = com.adcolony.sdk.at.a(r6)
            if (r6 != r0) goto L_0x01bd
            r1 = 1
            goto L_0x01be
        L_0x01bd:
            r1 = 0
        L_0x01be:
            r4.Q = r1
            r1 = 2
            if (r6 != r1) goto L_0x01c4
            r5 = 1
        L_0x01c4:
            r4.R = r5
            com.adcolony.sdk.j$5 r5 = new com.adcolony.sdk.j$5
            r5.<init>()
            com.adcolony.sdk.at.a(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.j.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void");
    }

    /* access modifiers changed from: private */
    public void E() {
        JSONObject a2 = u.a();
        u.a(a2, "type", "AdColony.on_configuration_completed");
        JSONArray jSONArray = new JSONArray();
        for (String put : f().keySet()) {
            jSONArray.put(put);
        }
        JSONObject a3 = u.a();
        u.a(a3, "zone_ids", jSONArray);
        u.a(a2, "message", a3);
        new ab("CustomMessage.controller_send", 0, a2).b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.j.a(boolean, boolean):boolean
     arg types: [boolean, int]
     candidates:
      com.adcolony.sdk.j.a(com.adcolony.sdk.j, com.iab.omid.library.adcolony.adsession.Partner):com.iab.omid.library.adcolony.adsession.Partner
      com.adcolony.sdk.j.a(com.adcolony.sdk.j, com.adcolony.sdk.ab):void
      com.adcolony.sdk.j.a(com.adcolony.sdk.j, boolean):boolean
      com.adcolony.sdk.j.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.j.a(android.content.Context, com.adcolony.sdk.ab):boolean
      com.adcolony.sdk.j.a(boolean, boolean):boolean */
    private boolean e(boolean z2) {
        return a(z2, false);
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.G;
    }

    /* access modifiers changed from: package-private */
    public JSONObject c() {
        return this.z;
    }

    /* access modifiers changed from: private */
    public boolean a(boolean z2, boolean z3) {
        if (!a.d()) {
            return false;
        }
        this.P = z3;
        this.M = z2;
        if (z2 && !z3 && !H()) {
            return false;
        }
        F();
        return true;
    }

    /* access modifiers changed from: private */
    public void F() {
        new Thread(new Runnable() {
            public void run() {
                JSONObject a2 = u.a();
                u.a(a2, "url", j.e);
                u.a(a2, FirebaseAnalytics.Param.CONTENT_TYPE, "application/json");
                u.a(a2, "content", j.this.m().J().toString());
                new w.a().a("Launch: ").a(j.this.m().J().toString()).a(w.b);
                new w.a().a("Saving Launch to ").a(j.this.o.g()).a(j.a).a(w.d);
                j.this.i.a(new n(new ab("WebServices.post", 0, a2), j.this));
            }
        }).start();
    }

    private boolean a(JSONObject jSONObject) {
        if (!this.M) {
            new w.a().a("Non-standard launch. Downloading new controller.").a(w.f);
            return true;
        }
        JSONObject jSONObject2 = this.z;
        if (jSONObject2 != null && u.b(u.f(jSONObject2, "controller"), "sha1").equals(u.b(u.f(jSONObject, "controller"), "sha1"))) {
            return false;
        }
        new w.a().a("Controller sha1 does not match, downloading new controller.").a(w.f);
        return true;
    }

    /* access modifiers changed from: private */
    public void f(ab abVar) {
        a(u.c(abVar.c(), "id"));
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.K = z2;
    }

    /* access modifiers changed from: private */
    public void g(ab abVar) {
        JSONObject jSONObject = this.v.d;
        u.a(jSONObject, "app_id", this.v.a);
        u.a(jSONObject, "zone_ids", this.v.c);
        JSONObject a2 = u.a();
        u.a(a2, "options", jSONObject);
        abVar.a(a2).b();
    }

    /* access modifiers changed from: package-private */
    public boolean a(final ab abVar) {
        final Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        try {
            int c3 = abVar.c().has("id") ? u.c(abVar.c(), "id") : 0;
            if (c3 <= 0) {
                c3 = this.h.d();
            }
            a(c3);
            final boolean d2 = u.d(abVar.c(), "is_display_module");
            at.a(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.adcolony.sdk.av.a(boolean, com.adcolony.sdk.ab):void
                 arg types: [int, com.adcolony.sdk.ab]
                 candidates:
                  com.adcolony.sdk.av.a(java.lang.String, java.lang.String):java.lang.String
                  com.adcolony.sdk.av.a(com.adcolony.sdk.av, org.json.JSONArray):org.json.JSONArray
                  com.adcolony.sdk.av.a(com.adcolony.sdk.av, java.lang.String):void
                  com.adcolony.sdk.av.a(org.json.JSONObject, java.lang.String):void
                  com.adcolony.sdk.av.a(com.adcolony.sdk.av, boolean):boolean
                  com.adcolony.sdk.av.a(boolean, com.adcolony.sdk.ab):void */
                public void run() {
                    av avVar = new av(c2.getApplicationContext(), j.this.h.d(), d2);
                    avVar.a(true, abVar);
                    j.this.B.put(Integer.valueOf(avVar.a()), avVar);
                }
            });
            return true;
        } catch (RuntimeException e2) {
            w.a aVar = new w.a();
            aVar.a(e2.toString() + ": during WebView initialization.").a(" Disabling AdColony.").a(w.g);
            AdColony.disable();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ab abVar) {
        this.w = abVar;
    }

    /* access modifiers changed from: package-private */
    public void c(ab abVar) {
        this.y = abVar;
    }

    private void G() {
        if (a.a().k().e()) {
            this.T++;
            int i2 = this.U;
            int i3 = this.T;
            int i4 = 120;
            if (i2 * i3 <= 120) {
                i4 = i2 * i3;
            }
            this.U = i4;
            at.a(new Runnable() {
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if (a.a().k().e()) {
                                j.this.F();
                            }
                        }
                    }, (long) (j.this.U * 1000));
                }
            });
            return;
        }
        new w.a().a("Max launch server download attempts hit, or AdColony is no longer").a(" active.").a(w.f);
    }

    public void a(n nVar, ab abVar, Map<String, List<String>> map) {
        if (nVar.a.equals(e)) {
            if (nVar.c) {
                new w.a().a("Launch: ").a(nVar.b).a(w.b);
                JSONObject a2 = u.a(nVar.b, "Parsing launch response");
                u.a(a2, "sdkVersion", m().F());
                u.h(a2, this.o.g() + a);
                if (c(a2)) {
                    if (a(a2)) {
                        new w.a().a("Controller missing or out of date. Downloading controller").a(w.d);
                        JSONObject a3 = u.a();
                        u.a(a3, "url", this.D);
                        u.a(a3, "filepath", this.o.g() + b);
                        this.i.a(new n(new ab("WebServices.download", 0, a3), this));
                    }
                    this.z = a2;
                } else if (!this.M) {
                    new w.a().a("Incomplete or disabled launch server response. ").a("Disabling AdColony until next launch.").a(w.g);
                    a(true);
                }
            } else {
                G();
            }
        } else if (!nVar.a.equals(this.D)) {
        } else {
            if (!b(this.E) && !av.a) {
                new w.a().a("Downloaded controller sha1 does not match, retrying.").a(w.e);
                G();
            } else if (!this.M && !this.P) {
                at.a(new Runnable() {
                    public void run() {
                        boolean l = j.this.H();
                        w.a aVar = new w.a();
                        aVar.a("Loaded library. Success=" + l).a(w.b);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean H() {
        this.h.a();
        return true;
    }

    private boolean b(String str) {
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        File file = new File(c2.getFilesDir().getAbsolutePath() + "/adc3/" + b);
        if (file.exists()) {
            return at.a(str, file);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Context context, ab abVar) {
        if (context == null) {
            return false;
        }
        AdvertisingIdClient.Info info = null;
        try {
            info = AdvertisingIdClient.getAdvertisingIdInfo(context);
        } catch (NoClassDefFoundError unused) {
            new w.a().a("Google Play Services ads dependencies are missing. Collecting ").a("Android ID instead of Advertising ID.").a(w.e);
            return false;
        } catch (NoSuchMethodError unused2) {
            new w.a().a("Google Play Services is out of date, please update to GPS 4.0+. ").a("Collecting Android ID instead of Advertising ID.").a(w.e);
        } catch (Exception e2) {
            e2.printStackTrace();
            if (Build.MANUFACTURER.equals("Amazon")) {
                return false;
            }
            new w.a().a("Advertising ID is not available. Collecting Android ID instead of").a(" Advertising ID.").a(w.e);
            return false;
        }
        if (info == null) {
            return false;
        }
        m().a(info.getId());
        y.l.g.put("advertisingId", m().c());
        m().b(info.isLimitAdTrackingEnabled());
        m().a(true);
        if (abVar != null) {
            JSONObject a2 = u.a();
            u.a(a2, "advertiser_id", m().c());
            u.b(a2, "limit_ad_tracking", m().g());
            abVar.a(a2).b();
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.j.a(boolean, boolean):boolean
     arg types: [int, int]
     candidates:
      com.adcolony.sdk.j.a(com.adcolony.sdk.j, com.iab.omid.library.adcolony.adsession.Partner):com.iab.omid.library.adcolony.adsession.Partner
      com.adcolony.sdk.j.a(com.adcolony.sdk.j, com.adcolony.sdk.ab):void
      com.adcolony.sdk.j.a(com.adcolony.sdk.j, boolean):boolean
      com.adcolony.sdk.j.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.j.a(android.content.Context, com.adcolony.sdk.ab):boolean
      com.adcolony.sdk.j.a(boolean, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void a(AdColonyAppOptions adColonyAppOptions) {
        synchronized (this.k.c()) {
            for (Map.Entry<String, AdColonyInterstitial> value : this.k.c().entrySet()) {
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) value.getValue();
                AdColonyInterstitialListener listener = adColonyInterstitial.getListener();
                adColonyInterstitial.a(true);
                if (listener != null) {
                    listener.onExpiring(adColonyInterstitial);
                }
            }
            this.k.c().clear();
        }
        this.L = false;
        a(1);
        this.A.clear();
        this.v = adColonyAppOptions;
        this.h.a();
        a(true, true);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        ae a2 = this.h.a(i2);
        final av remove = this.B.remove(Integer.valueOf(i2));
        boolean z2 = false;
        if (a2 == null) {
            return false;
        }
        if (remove != null && remove.l()) {
            z2 = true;
        }
        AnonymousClass10 r2 = new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.adcolony.sdk.j.d(com.adcolony.sdk.j, boolean):boolean
             arg types: [com.adcolony.sdk.j, int]
             candidates:
              com.adcolony.sdk.j.d(com.adcolony.sdk.j, com.adcolony.sdk.ab):com.adcolony.sdk.ab
              com.adcolony.sdk.j.d(com.adcolony.sdk.j, boolean):boolean */
            public void run() {
                av avVar = remove;
                if (avVar != null && avVar.m()) {
                    remove.loadUrl("about:blank");
                    remove.clearCache(true);
                    remove.removeAllViews();
                    remove.a(true);
                    remove.destroy();
                }
                if (j.this.y != null) {
                    j.this.y.b();
                    ab unused = j.this.y = (ab) null;
                    boolean unused2 = j.this.x = false;
                }
            }
        };
        if (z2) {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            new Handler().postDelayed(r2, 1000);
        } else {
            r2.run();
        }
        return true;
    }

    private void b(JSONObject jSONObject) {
        if (!av.a) {
            JSONObject f2 = u.f(jSONObject, "logging");
            y.k = u.a(f2, "send_level", 1);
            y.a = u.d(f2, "log_private");
            y.i = u.a(f2, "print_level", 3);
            this.p.a(u.g(f2, "modules"));
        }
        m().a(u.f(jSONObject, "metadata"));
        this.G = u.b(u.f(jSONObject, "controller"), "version");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        new java.io.File(r3.o.g() + com.adcolony.sdk.j.a).delete();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0047 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean c(org.json.JSONObject r4) {
        /*
            r3 = this;
            r0 = 0
            if (r4 != 0) goto L_0x0014
            com.adcolony.sdk.w$a r4 = new com.adcolony.sdk.w$a
            r4.<init>()
            java.lang.String r1 = "Launch response verification failed - response is null or unknown"
            com.adcolony.sdk.w$a r4 = r4.a(r1)
            com.adcolony.sdk.w r1 = com.adcolony.sdk.w.d
            r4.a(r1)
            return r0
        L_0x0014:
            java.lang.String r1 = "controller"
            org.json.JSONObject r1 = com.adcolony.sdk.u.f(r4, r1)     // Catch:{ Exception -> 0x0047 }
            java.lang.String r2 = "url"
            java.lang.String r2 = com.adcolony.sdk.u.b(r1, r2)     // Catch:{ Exception -> 0x0047 }
            r3.D = r2     // Catch:{ Exception -> 0x0047 }
            java.lang.String r2 = "sha1"
            java.lang.String r1 = com.adcolony.sdk.u.b(r1, r2)     // Catch:{ Exception -> 0x0047 }
            r3.E = r1     // Catch:{ Exception -> 0x0047 }
            java.lang.String r1 = "status"
            java.lang.String r1 = com.adcolony.sdk.u.b(r4, r1)     // Catch:{ Exception -> 0x0047 }
            r3.F = r1     // Catch:{ Exception -> 0x0047 }
            java.lang.String r1 = "pie"
            java.lang.String r1 = com.adcolony.sdk.u.b(r4, r1)     // Catch:{ Exception -> 0x0047 }
            com.adcolony.sdk.j.H = r1     // Catch:{ Exception -> 0x0047 }
            boolean r1 = com.adcolony.sdk.AdColonyEventTracker.b()     // Catch:{ Exception -> 0x0047 }
            if (r1 == 0) goto L_0x0043
            com.adcolony.sdk.AdColonyEventTracker.a()     // Catch:{ Exception -> 0x0047 }
        L_0x0043:
            r3.b(r4)     // Catch:{ Exception -> 0x0047 }
            goto L_0x0068
        L_0x0047:
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0067 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0067 }
            r1.<init>()     // Catch:{ Exception -> 0x0067 }
            com.adcolony.sdk.ao r2 = r3.o     // Catch:{ Exception -> 0x0067 }
            java.lang.String r2 = r2.g()     // Catch:{ Exception -> 0x0067 }
            r1.append(r2)     // Catch:{ Exception -> 0x0067 }
            java.lang.String r2 = "026ae9c9824b3e483fa6c71fa88f57ae27816141"
            r1.append(r2)     // Catch:{ Exception -> 0x0067 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0067 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0067 }
            r4.delete()     // Catch:{ Exception -> 0x0067 }
            goto L_0x0068
        L_0x0067:
        L_0x0068:
            java.lang.String r4 = r3.F
            java.lang.String r1 = "disable"
            boolean r4 = r4.equals(r1)
            if (r4 == 0) goto L_0x00af
            boolean r4 = com.adcolony.sdk.av.a
            if (r4 != 0) goto L_0x00af
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0095 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0095 }
            r1.<init>()     // Catch:{ Exception -> 0x0095 }
            com.adcolony.sdk.ao r2 = r3.o     // Catch:{ Exception -> 0x0095 }
            java.lang.String r2 = r2.g()     // Catch:{ Exception -> 0x0095 }
            r1.append(r2)     // Catch:{ Exception -> 0x0095 }
            java.lang.String r2 = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5"
            r1.append(r2)     // Catch:{ Exception -> 0x0095 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0095 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0095 }
            r4.delete()     // Catch:{ Exception -> 0x0095 }
        L_0x0095:
            com.adcolony.sdk.w$a r4 = new com.adcolony.sdk.w$a
            r4.<init>()
            java.lang.String r1 = "Launch server response with disabled status. Disabling AdColony "
            com.adcolony.sdk.w$a r4 = r4.a(r1)
            java.lang.String r1 = "until next launch."
            com.adcolony.sdk.w$a r4 = r4.a(r1)
            com.adcolony.sdk.w r1 = com.adcolony.sdk.w.f
            r4.a(r1)
            com.adcolony.sdk.AdColony.disable()
            return r0
        L_0x00af:
            java.lang.String r4 = r3.D
            java.lang.String r1 = ""
            boolean r4 = r4.equals(r1)
            if (r4 != 0) goto L_0x00c1
            java.lang.String r4 = r3.F
            boolean r4 = r4.equals(r1)
            if (r4 == 0) goto L_0x00dc
        L_0x00c1:
            boolean r4 = com.adcolony.sdk.av.a
            if (r4 != 0) goto L_0x00dc
            com.adcolony.sdk.w$a r4 = new com.adcolony.sdk.w$a
            r4.<init>()
            java.lang.String r1 = "Missing controller status or URL. Disabling AdColony until next "
            com.adcolony.sdk.w$a r4 = r4.a(r1)
            java.lang.String r1 = "launch."
            com.adcolony.sdk.w$a r4 = r4.a(r1)
            com.adcolony.sdk.w r1 = com.adcolony.sdk.w.g
            r4.a(r1)
            return r0
        L_0x00dc:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.j.c(org.json.JSONObject):boolean");
    }

    /* access modifiers changed from: package-private */
    public boolean d(final ab abVar) {
        if (this.t == null) {
            return false;
        }
        at.a(new Runnable() {
            public void run() {
                j.this.t.onReward(new AdColonyReward(abVar));
            }
        });
        return true;
    }

    /* access modifiers changed from: package-private */
    public void e(ab abVar) {
        AdColonyZone adColonyZone;
        if (this.K) {
            new w.a().a("AdColony is disabled. Ignoring zone_info message.").a(w.f);
            return;
        }
        String b2 = u.b(abVar.c(), "zone_id");
        if (this.A.containsKey(b2)) {
            adColonyZone = this.A.get(b2);
        } else {
            AdColonyZone adColonyZone2 = new AdColonyZone(b2);
            this.A.put(b2, adColonyZone2);
            adColonyZone = adColonyZone2;
        }
        adColonyZone.a(abVar);
    }

    private void I() {
        Application application;
        Context c2 = a.c();
        if (c2 != null && this.W == null && Build.VERSION.SDK_INT > 14) {
            this.W = new Application.ActivityLifecycleCallbacks() {
                public void onActivityDestroyed(Activity activity) {
                }

                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                }

                public void onActivityStarted(Activity activity) {
                }

                public void onActivityStopped(Activity activity) {
                }

                public void onActivityResumed(Activity activity) {
                    a.b = true;
                    a.a(activity);
                    Context c = a.c();
                    if (c == null || !j.this.j.c() || !(c instanceof b) || ((b) c).g) {
                        new w.a().a("onActivityResumed() Activity Lifecycle Callback").a(w.d);
                        a.a(activity);
                        if (j.this.w != null) {
                            j.this.w.a(j.this.w.c()).b();
                            ab unused = j.this.w = (ab) null;
                        }
                        boolean unused2 = j.this.J = false;
                        j.this.j.d(true);
                        j.this.j.e(true);
                        j.this.j.f(false);
                        if (j.this.f && !j.this.j.e()) {
                            j.this.j.a(true);
                        }
                        j.this.l.a();
                        if (y.l == null || y.l.d == null || y.l.d.isShutdown() || y.l.d.isTerminated()) {
                            AdColony.a(activity, a.a().v);
                            return;
                        }
                        return;
                    }
                    new w.a().a("Ignoring onActivityResumed").a(w.d);
                }

                public void onActivityPaused(Activity activity) {
                    a.b = false;
                    j.this.j.d(false);
                    j.this.j.e(true);
                    a.a().m().I();
                }

                public void onActivityCreated(Activity activity, Bundle bundle) {
                    if (!j.this.j.e()) {
                        j.this.j.a(true);
                    }
                    a.a(activity);
                }
            };
            if (c2 instanceof Application) {
                application = (Application) c2;
            } else {
                application = ((Activity) c2).getApplication();
            }
            application.registerActivityLifecycleCallbacks(this.W);
        }
    }

    /* access modifiers changed from: package-private */
    public AdColonyAppOptions d() {
        if (this.v == null) {
            this.v = new AdColonyAppOptions();
        }
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.v != null;
    }

    /* access modifiers changed from: package-private */
    public void b(AdColonyAppOptions adColonyAppOptions) {
        this.v = adColonyAppOptions;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyZone> f() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.J = z2;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return this.J;
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        return this.K;
    }

    /* access modifiers changed from: package-private */
    public AdColonyRewardListener i() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyRewardListener adColonyRewardListener) {
        this.t = adColonyRewardListener;
    }

    /* access modifiers changed from: package-private */
    public r j() {
        if (this.m == null) {
            this.m = new r();
            this.m.a();
        }
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public al k() {
        if (this.j == null) {
            this.j = new al();
            this.j.a();
        }
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public d l() {
        if (this.k == null) {
            this.k = new d();
            this.k.a();
        }
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public l m() {
        if (this.c == null) {
            this.c = new l();
            this.c.e();
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public aq n() {
        if (this.n == null) {
            this.n = new aq();
            this.n.a();
        }
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public ao o() {
        if (this.o == null) {
            this.o = new ao();
            this.o.a();
        }
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public af p() {
        if (this.d == null) {
            this.d = new af();
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public ac q() {
        if (this.h == null) {
            this.h = new ac();
            this.h.a();
        }
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public m r() {
        if (this.l == null) {
            this.l = new m();
        }
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public o s() {
        if (this.i == null) {
            this.i = new o();
        }
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public c t() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        this.q = cVar;
    }

    /* access modifiers changed from: package-private */
    public AdColonyAdView u() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyAdView adColonyAdView) {
        this.r = adColonyAdView;
    }

    /* access modifiers changed from: package-private */
    public AdColonyInterstitial v() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyInterstitial adColonyInterstitial) {
        this.s = adColonyInterstitial;
    }

    /* access modifiers changed from: package-private */
    public String w() {
        return this.C;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.C = str;
    }

    /* access modifiers changed from: package-private */
    public boolean x() {
        return this.I;
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z2) {
        this.I = z2;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, av> y() {
        return this.B;
    }

    /* access modifiers changed from: package-private */
    public boolean z() {
        return this.x;
    }

    /* access modifiers changed from: package-private */
    public void d(boolean z2) {
        this.x = z2;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyCustomMessageListener> A() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public boolean B() {
        return this.L;
    }

    static String C() {
        return H;
    }

    /* access modifiers changed from: package-private */
    public Partner D() {
        return this.X;
    }
}
