package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import com.adcolony.sdk.at;
import com.adcolony.sdk.w;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.sdk.constants.Constants;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

public class AdColony {
    static ExecutorService a = Executors.newSingleThreadExecutor();

    public static boolean disable() {
        if (!a.e()) {
            return false;
        }
        Context c = a.c();
        if (c != null && (c instanceof b)) {
            ((Activity) c).finish();
        }
        final j a2 = a.a();
        for (final AdColonyInterstitial next : a2.l().c().values()) {
            at.a(new Runnable() {
                public void run() {
                    AdColonyInterstitialListener listener = next.getListener();
                    next.a(true);
                    if (listener != null) {
                        listener.onExpiring(next);
                    }
                }
            });
        }
        at.a(new Runnable() {
            public void run() {
                ArrayList arrayList = new ArrayList();
                Iterator<ae> it = a2.q().c().iterator();
                while (it.hasNext()) {
                    arrayList.add(it.next());
                }
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    ae aeVar = (ae) it2.next();
                    a2.a(aeVar.a());
                    if (aeVar instanceof av) {
                        av avVar = (av) aeVar;
                        if (!avVar.m()) {
                            avVar.loadUrl("about:blank");
                            avVar.clearCache(true);
                            avVar.removeAllViews();
                            avVar.a(true);
                        }
                    }
                }
            }
        });
        a.a().a(true);
        return true;
    }

    public static boolean configure(Activity activity, String str, String... strArr) {
        return a(activity, null, str, strArr);
    }

    public static boolean configure(Activity activity, AdColonyAppOptions adColonyAppOptions, String str, String... strArr) {
        return a(activity, adColonyAppOptions, str, strArr);
    }

    public static boolean configure(Application application, String str, String... strArr) {
        return configure(application, (AdColonyAppOptions) null, str, strArr);
    }

    public static boolean configure(Application application, AdColonyAppOptions adColonyAppOptions, String str, String... strArr) {
        return a(application, adColonyAppOptions, str, strArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
     arg types: [android.content.Context, com.adcolony.sdk.AdColonyAppOptions, int]
     candidates:
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.a(org.json.JSONArray, java.lang.String[], boolean):org.json.JSONArray
     arg types: [org.json.JSONArray, java.lang.String[], int]
     candidates:
      com.adcolony.sdk.u.a(org.json.JSONObject, java.lang.String, int):int
      com.adcolony.sdk.u.a(org.json.JSONObject, java.lang.String, double):boolean
      com.adcolony.sdk.u.a(org.json.JSONObject, java.lang.String, long):boolean
      com.adcolony.sdk.u.a(org.json.JSONObject, java.lang.String, java.lang.String):boolean
      com.adcolony.sdk.u.a(org.json.JSONObject, java.lang.String, org.json.JSONArray):boolean
      com.adcolony.sdk.u.a(org.json.JSONObject, java.lang.String, org.json.JSONObject):boolean
      com.adcolony.sdk.u.a(org.json.JSONObject, java.lang.String, boolean):boolean
      com.adcolony.sdk.u.a(org.json.JSONArray, java.lang.String[], boolean):org.json.JSONArray */
    private static boolean a(Context context, AdColonyAppOptions adColonyAppOptions, String str, String... strArr) {
        if (ah.a(0, null)) {
            new w.a().a("Cannot configure AdColony; configuration mechanism requires 5 ").a("seconds between attempts.").a(w.e);
            return false;
        }
        if (context == null) {
            context = a.c();
        }
        if (context == null) {
            new w.a().a("Ignoring call to AdColony.configure() as the provided Activity or ").a("Application context is null and we do not currently hold a ").a("reference to either for our use.").a(w.e);
            return false;
        }
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        if (adColonyAppOptions == null) {
            adColonyAppOptions = new AdColonyAppOptions();
        }
        if (a.b() && !u.d(a.a().d().d(), "reconfigurable")) {
            j a2 = a.a();
            if (!a2.d().a().equals(str)) {
                new w.a().a("Ignoring call to AdColony.configure() as the app id does not ").a("match what was used during the initial configuration.").a(w.e);
                return false;
            } else if (at.a(strArr, a2.d().b())) {
                new w.a().a("Ignoring call to AdColony.configure() as the same zone ids ").a("were used during the previous configuration.").a(w.e);
                return true;
            }
        }
        adColonyAppOptions.a(str);
        adColonyAppOptions.a(strArr);
        adColonyAppOptions.f();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss:SSS", Locale.US);
        long currentTimeMillis = System.currentTimeMillis();
        String format = simpleDateFormat.format(new Date(currentTimeMillis));
        boolean z = true;
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] != null && !strArr[i].equals("")) {
                z = false;
            }
        }
        if (str.equals("") || z) {
            new w.a().a("AdColony.configure() called with an empty app or zone id String.").a(w.g);
            return false;
        }
        a.a = true;
        if (Build.VERSION.SDK_INT < 14) {
            new w.a().a("The minimum API level for the AdColony SDK is 14.").a(w.e);
            a.a(context, adColonyAppOptions, true);
        } else {
            a.a(context, adColonyAppOptions, false);
        }
        String str2 = a.a().o().c() + "/adc3/AppInfo";
        JSONObject a3 = u.a();
        if (new File(str2).exists()) {
            a3 = u.c(str2);
        }
        JSONObject a4 = u.a();
        if (u.b(a3, "appId").equals(str)) {
            u.a(a4, "zoneIds", u.a(u.g(a3, "zoneIds"), strArr, true));
            u.a(a4, "appId", str);
        } else {
            u.a(a4, "zoneIds", u.a(strArr));
            u.a(a4, "appId", str);
        }
        u.h(a4, str2);
        new w.a().a("Configure: Total Time (ms): ").a("" + (System.currentTimeMillis() - currentTimeMillis)).a(" and started at " + format).a(w.f);
        return true;
    }

    public static AdColonyZone getZone(String str) {
        if (!a.e()) {
            new w.a().a("Ignoring call to AdColony.getZone() as AdColony has not yet been ").a("configured.").a(w.e);
            return null;
        }
        HashMap<String, AdColonyZone> f = a.a().f();
        if (f.containsKey(str)) {
            return f.get(str);
        }
        AdColonyZone adColonyZone = new AdColonyZone(str);
        a.a().f().put(str, adColonyZone);
        return adColonyZone;
    }

    public static boolean notifyIAPComplete(String str, String str2) {
        return notifyIAPComplete(str, str2, null, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE);
    }

    public static boolean notifyIAPComplete(String str, String str2, String str3, double d) {
        if (!a.e()) {
            new w.a().a("Ignoring call to notifyIAPComplete as AdColony has not yet been ").a("configured.").a(w.e);
            return false;
        } else if (!at.d(str) || !at.d(str2)) {
            new w.a().a("Ignoring call to notifyIAPComplete as one of the passed Strings ").a("is greater than ").a(128).a(" characters.").a(w.e);
            return false;
        } else {
            if (str3 != null && str3.length() > 3) {
                new w.a().a("You are trying to report an IAP event with a currency String ").a("containing more than 3 characters.").a(w.e);
            }
            final double d2 = d;
            final String str4 = str3;
            final String str5 = str;
            final String str6 = str2;
            a.execute(new Runnable() {
                public void run() {
                    AdColony.a();
                    JSONObject a2 = u.a();
                    double d2 = d2;
                    if (d2 >= FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                        u.a(a2, "price", d2);
                    }
                    String str = str4;
                    if (str != null && str.length() <= 3) {
                        u.a(a2, "currency_code", str4);
                    }
                    u.a(a2, "product_id", str5);
                    u.a(a2, "transaction_id", str6);
                    new ab("AdColony.on_iap_report", 1, a2).b();
                }
            });
            return true;
        }
    }

    public static boolean requestAdView(String str, AdColonyAdViewListener adColonyAdViewListener, AdColonyAdSize adColonyAdSize) {
        return requestAdView(str, adColonyAdViewListener, adColonyAdSize, null);
    }

    public static boolean requestAdView(final String str, final AdColonyAdViewListener adColonyAdViewListener, final AdColonyAdSize adColonyAdSize, final AdColonyAdOptions adColonyAdOptions) {
        if (!a.e()) {
            new w.a().a("Ignoring call to requestAdView as AdColony has not yet been").a(" configured.").a(w.e);
            a(adColonyAdViewListener, str);
            return false;
        } else if (adColonyAdSize.getHeight() <= 0 || adColonyAdSize.getWidth() <= 0) {
            new w.a().a("Ignoring call to requestAdView as you've provided an AdColonyAdSize").a(" object with an invalid width or height.").a(w.e);
            return false;
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("zone_id", str);
            if (ah.a(1, bundle)) {
                a(adColonyAdViewListener, str);
                return false;
            }
            try {
                a.execute(new Runnable() {
                    public void run() {
                        j a2 = a.a();
                        if (a2.g() || a2.h()) {
                            AdColony.b();
                            AdColony.a(adColonyAdViewListener, str);
                        }
                        if (!AdColony.a() && a.d()) {
                            AdColony.a(adColonyAdViewListener, str);
                        }
                        if (a2.f().get(str) == null) {
                            new AdColonyZone(str);
                            new w.a().a("Zone info for ").a(str).a(" doesn't exist in hashmap").a(w.b);
                        }
                        a2.l().a(str, adColonyAdViewListener, adColonyAdSize, adColonyAdOptions);
                    }
                });
                return true;
            } catch (RejectedExecutionException unused) {
                a(adColonyAdViewListener, str);
                return false;
            }
        }
    }

    public static boolean setAppOptions(final AdColonyAppOptions adColonyAppOptions) {
        if (!a.e()) {
            new w.a().a("Ignoring call to AdColony.setAppOptions() as AdColony has not yet").a(" been configured.").a(w.e);
            return false;
        }
        a.a().b(adColonyAppOptions);
        adColonyAppOptions.f();
        try {
            a.execute(new Runnable() {
                public void run() {
                    AdColony.a();
                    JSONObject a2 = u.a();
                    u.a(a2, "options", adColonyAppOptions.d());
                    new ab("Options.set_options", 1, a2).b();
                }
            });
            return true;
        } catch (RejectedExecutionException unused) {
            return false;
        }
    }

    public static AdColonyAppOptions getAppOptions() {
        if (!a.e()) {
            return null;
        }
        return a.a().d();
    }

    public static boolean setRewardListener(AdColonyRewardListener adColonyRewardListener) {
        if (!a.e()) {
            new w.a().a("Ignoring call to AdColony.setRewardListener() as AdColony has not").a(" yet been configured.").a(w.e);
            return false;
        }
        a.a().a(adColonyRewardListener);
        return true;
    }

    public static boolean removeRewardListener() {
        if (!a.e()) {
            new w.a().a("Ignoring call to AdColony.removeRewardListener() as AdColony has ").a("not yet been configured.").a(w.e);
            return false;
        }
        a.a().a((AdColonyRewardListener) null);
        return true;
    }

    public static String getSDKVersion() {
        if (!a.e()) {
            return "";
        }
        return a.a().m().F();
    }

    public static AdColonyRewardListener getRewardListener() {
        if (!a.e()) {
            return null;
        }
        return a.a().i();
    }

    public static boolean addCustomMessageListener(AdColonyCustomMessageListener adColonyCustomMessageListener, final String str) {
        if (!a.e()) {
            new w.a().a("Ignoring call to AdColony.addCustomMessageListener as AdColony ").a("has not yet been configured.").a(w.e);
            return false;
        } else if (!at.d(str)) {
            new w.a().a("Ignoring call to AdColony.addCustomMessageListener.").a(w.e);
            return false;
        } else {
            try {
                a.a().A().put(str, adColonyCustomMessageListener);
                a.execute(new Runnable() {
                    public void run() {
                        AdColony.a();
                        JSONObject a2 = u.a();
                        u.a(a2, "type", str);
                        new ab("CustomMessage.register", 1, a2).b();
                    }
                });
                return true;
            } catch (RejectedExecutionException unused) {
                return false;
            }
        }
    }

    public static AdColonyCustomMessageListener getCustomMessageListener(String str) {
        if (!a.e()) {
            return null;
        }
        return a.a().A().get(str);
    }

    public static boolean removeCustomMessageListener(final String str) {
        if (!a.e()) {
            new w.a().a("Ignoring call to AdColony.removeCustomMessageListener as AdColony").a(" has not yet been configured.").a(w.e);
            return false;
        }
        a.a().A().remove(str);
        a.execute(new Runnable() {
            public void run() {
                AdColony.a();
                JSONObject a2 = u.a();
                u.a(a2, "type", str);
                new ab("CustomMessage.unregister", 1, a2).b();
            }
        });
        return true;
    }

    public static boolean clearCustomMessageListeners() {
        if (!a.e()) {
            new w.a().a("Ignoring call to AdColony.clearCustomMessageListeners as AdColony").a(" has not yet been configured.").a(w.e);
            return false;
        }
        a.a().A().clear();
        a.execute(new Runnable() {
            public void run() {
                AdColony.a();
                for (String a : a.a().A().keySet()) {
                    JSONObject a2 = u.a();
                    u.a(a2, "type", a);
                    new ab("CustomMessage.unregister", 1, a2).b();
                }
            }
        });
        return true;
    }

    public static boolean requestInterstitial(String str, AdColonyInterstitialListener adColonyInterstitialListener) {
        return requestInterstitial(str, adColonyInterstitialListener, null);
    }

    public static boolean requestInterstitial(final String str, final AdColonyInterstitialListener adColonyInterstitialListener, final AdColonyAdOptions adColonyAdOptions) {
        if (!a.e()) {
            new w.a().a("Ignoring call to AdColony.requestInterstitial as AdColony has not").a(" yet been configured.").a(w.e);
            adColonyInterstitialListener.onRequestNotFilled(new AdColonyZone(str));
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putString("zone_id", str);
        if (ah.a(1, bundle)) {
            AdColonyZone adColonyZone = a.a().f().get(str);
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(str);
                w.a a2 = new w.a().a("Zone info for ");
                a2.a(str + " doesn't exist in hashmap").a(w.b);
            }
            adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
            return false;
        }
        try {
            a.execute(new Runnable() {
                public void run() {
                    j a2 = a.a();
                    if (a2.g() || a2.h()) {
                        AdColony.b();
                        AdColony.a(adColonyInterstitialListener, str);
                    } else if (AdColony.a() || !a.d()) {
                        final AdColonyZone adColonyZone = a2.f().get(str);
                        if (adColonyZone == null) {
                            adColonyZone = new AdColonyZone(str);
                            w.a a3 = new w.a().a("Zone info for ");
                            a3.a(str + " doesn't exist in hashmap").a(w.b);
                        }
                        if (adColonyZone.getZoneType() == 2 || adColonyZone.getZoneType() == 1) {
                            at.a(new Runnable() {
                                public void run() {
                                    adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
                                }
                            });
                        } else {
                            a2.l().a(str, adColonyInterstitialListener, adColonyAdOptions);
                        }
                    } else {
                        AdColony.a(adColonyInterstitialListener, str);
                    }
                }
            });
            return true;
        } catch (RejectedExecutionException unused) {
            a(adColonyInterstitialListener, str);
            return false;
        }
    }

    static boolean a() {
        at.a aVar = new at.a(15.0d);
        j a2 = a.a();
        while (!a2.B() && !aVar.b()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException unused) {
            }
        }
        return a2.B();
    }

    static boolean a(final AdColonyInterstitialListener adColonyInterstitialListener, final String str) {
        if (adColonyInterstitialListener == null || !a.d()) {
            return false;
        }
        at.a(new Runnable() {
            public void run() {
                AdColonyZone adColonyZone = a.a().f().get(str);
                if (adColonyZone == null) {
                    adColonyZone = new AdColonyZone(str);
                }
                adColonyInterstitialListener.onRequestNotFilled(adColonyZone);
            }
        });
        return false;
    }

    static boolean a(final AdColonyAdViewListener adColonyAdViewListener, final String str) {
        if (adColonyAdViewListener == null || !a.d()) {
            return false;
        }
        at.a(new Runnable() {
            public void run() {
                AdColonyZone adColonyZone;
                if (!a.b()) {
                    adColonyZone = null;
                } else {
                    adColonyZone = a.a().f().get(str);
                }
                if (adColonyZone == null) {
                    adColonyZone = new AdColonyZone(str);
                }
                adColonyAdViewListener.onRequestNotFilled(adColonyZone);
            }
        });
        return false;
    }

    static void a(Context context, AdColonyAppOptions adColonyAppOptions) {
        String str;
        if (adColonyAppOptions != null && context != null) {
            String b = at.b(context);
            String b2 = at.b();
            int c = at.c();
            String i = a.a().m().i();
            if (a.a().p().a()) {
                str = ConnectivityService.NETWORK_TYPE_WIFI;
            } else {
                str = a.a().p().b() ? "mobile" : Constants.ParametersKeys.ORIENTATION_NONE;
            }
            HashMap hashMap = new HashMap();
            hashMap.put("sessionId", "unknown");
            hashMap.put("advertiserId", "unknown");
            hashMap.put("countryLocale", Locale.getDefault().getDisplayLanguage() + " (" + Locale.getDefault().getDisplayCountry() + ")");
            hashMap.put("countryLocalShort", a.a().m().w());
            hashMap.put("manufacturer", a.a().m().z());
            hashMap.put("model", a.a().m().A());
            hashMap.put("osVersion", a.a().m().B());
            hashMap.put("carrierName", i);
            hashMap.put("networkType", str);
            hashMap.put("platform", "android");
            hashMap.put("appName", b);
            hashMap.put("appVersion", b2);
            hashMap.put("appBuildNumber", Integer.valueOf(c));
            hashMap.put("appId", "" + adColonyAppOptions.a());
            hashMap.put("apiLevel", Integer.valueOf(Build.VERSION.SDK_INT));
            hashMap.put("sdkVersion", a.a().m().F());
            hashMap.put("controllerVersion", "unknown");
            hashMap.put("zoneIds", adColonyAppOptions.c());
            JSONObject mediationInfo = adColonyAppOptions.getMediationInfo();
            JSONObject pluginInfo = adColonyAppOptions.getPluginInfo();
            if (!u.b(mediationInfo, "mediation_network").equals("")) {
                hashMap.put("mediationNetwork", u.b(mediationInfo, "mediation_network"));
                hashMap.put("mediationNetworkVersion", u.b(mediationInfo, "mediation_network_version"));
            }
            if (!u.b(pluginInfo, "plugin").equals("")) {
                hashMap.put("plugin", u.b(pluginInfo, "plugin"));
                hashMap.put("pluginVersion", u.b(pluginInfo, "plugin_version"));
            }
            y.a(hashMap);
        }
    }

    static void b() {
        new w.a().a("The AdColony API is not available while AdColony is disabled.").a(w.g);
    }
}
