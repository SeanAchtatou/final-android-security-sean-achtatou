package com.adcolony.sdk;

import cz.msebera.android.httpclient.message.TokenParser;

class z {
    static float[] a = new float[16];
    static z b = new z();
    double[] c = new double[16];
    boolean d;

    z() {
        b();
    }

    z(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12, double d13, double d14, double d15, double d16, double d17) {
        b(d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17);
    }

    /* access modifiers changed from: package-private */
    public z a() {
        z zVar = new z();
        zVar.b(this);
        return zVar;
    }

    /* access modifiers changed from: package-private */
    public z a(z zVar) {
        z zVar2 = zVar;
        if (zVar2.d) {
            return this;
        }
        double[] dArr = zVar2.c;
        return a(dArr[0], dArr[1], dArr[2], dArr[3], dArr[4], dArr[5], dArr[6], dArr[7], dArr[8], dArr[9], dArr[10], dArr[11], dArr[12], dArr[13], dArr[14], dArr[15]);
    }

    /* access modifiers changed from: package-private */
    public z a(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12, double d13, double d14, double d15, double d16, double d17) {
        return a(d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, this);
    }

    /* access modifiers changed from: package-private */
    public z a(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12, double d13, double d14, double d15, double d16, double d17, z zVar) {
        z zVar2 = zVar;
        if (this.d) {
            return zVar.b(d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17);
        }
        double[] dArr = this.c;
        double d18 = dArr[0];
        double d19 = dArr[1];
        double d20 = dArr[2];
        double d21 = dArr[3];
        double d22 = dArr[4];
        double d23 = dArr[5];
        double d24 = dArr[6];
        double d25 = dArr[7];
        double d26 = dArr[8];
        double d27 = dArr[9];
        double d28 = dArr[10];
        double d29 = dArr[11];
        double d30 = dArr[12];
        double d31 = dArr[13];
        double d32 = dArr[14];
        double d33 = dArr[15];
        double[] dArr2 = zVar2.c;
        dArr2[0] = (d18 * d2) + (d19 * d6) + (d20 * d10) + (d21 * d14);
        dArr2[1] = (d18 * d3) + (d19 * d7) + (d20 * d11) + (d21 * d15);
        dArr2[2] = (d18 * d4) + (d19 * d8) + (d20 * d12) + (d21 * d16);
        dArr2[3] = (d18 * d5) + (d19 * d9) + (d20 * d13) + (d21 * d17);
        dArr2[4] = (d22 * d2) + (d23 * d6) + (d24 * d10) + (d25 * d14);
        dArr2[5] = (d22 * d3) + (d23 * d7) + (d24 * d11) + (d25 * d15);
        dArr2[6] = (d22 * d4) + (d23 * d8) + (d24 * d12) + (d25 * d16);
        dArr2[7] = (d22 * d5) + (d23 * d9) + (d24 * d13) + (d25 * d17);
        dArr2[8] = (d26 * d2) + (d27 * d6) + (d28 * d10) + (d29 * d14);
        dArr2[9] = (d26 * d3) + (d27 * d7) + (d28 * d11) + (d29 * d15);
        dArr2[10] = (d26 * d4) + (d27 * d8) + (d28 * d12) + (d29 * d16);
        dArr2[11] = (d26 * d5) + (d27 * d9) + (d28 * d13) + (d29 * d17);
        dArr2[12] = (d30 * d2) + (d31 * d6) + (d32 * d10) + (d33 * d14);
        dArr2[13] = (d30 * d3) + (d31 * d7) + (d32 * d11) + (d33 * d15);
        dArr2[14] = (d30 * d4) + (d31 * d8) + (d32 * d12) + (d33 * d16);
        dArr2[15] = (d30 * d5) + (d31 * d9) + (d32 * d13) + (d33 * d17);
        this.d = false;
        return zVar2;
    }

    /* access modifiers changed from: package-private */
    public z a(z zVar, z zVar2) {
        z zVar3 = zVar;
        if (zVar3.d) {
            return zVar2.b(this);
        }
        double[] dArr = zVar3.c;
        return a(dArr[0], dArr[1], dArr[2], dArr[3], dArr[4], dArr[5], dArr[6], dArr[7], dArr[8], dArr[9], dArr[10], dArr[11], dArr[12], dArr[13], dArr[14], dArr[15], zVar2);
    }

    /* access modifiers changed from: package-private */
    public z a(double d2) {
        b.b();
        double cos = Math.cos(d2);
        double sin = Math.sin(d2);
        z zVar = b;
        double[] dArr = zVar.c;
        dArr[0] = cos;
        dArr[1] = sin;
        dArr[4] = -sin;
        dArr[5] = cos;
        zVar.d = false;
        return a(zVar);
    }

    /* access modifiers changed from: package-private */
    public z b(double d2) {
        return a((d2 * 3.141592653589793d) / 180.0d);
    }

    /* access modifiers changed from: package-private */
    public z a(double d2, double d3, double d4) {
        b.b();
        z zVar = b;
        double[] dArr = zVar.c;
        dArr[0] = d2;
        dArr[5] = d3;
        dArr[10] = d4;
        zVar.d = false;
        return a(zVar);
    }

    /* access modifiers changed from: package-private */
    public z b(z zVar) {
        for (int i = 15; i >= 0; i--) {
            this.c[i] = zVar.c[i];
        }
        this.d = zVar.d;
        return this;
    }

    /* access modifiers changed from: package-private */
    public z b(double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12, double d13, double d14, double d15, double d16, double d17) {
        this.d = false;
        double[] dArr = this.c;
        dArr[0] = d2;
        dArr[1] = d3;
        dArr[2] = d4;
        dArr[3] = d5;
        dArr[4] = d6;
        dArr[5] = d7;
        dArr[6] = d8;
        dArr[7] = d9;
        dArr[8] = d10;
        dArr[9] = d11;
        dArr[10] = d12;
        dArr[11] = d13;
        dArr[12] = d14;
        dArr[13] = d15;
        dArr[14] = d16;
        dArr[15] = d17;
        return this;
    }

    /* access modifiers changed from: package-private */
    public z b() {
        for (int i = 15; i >= 0; i--) {
            this.c[i] = 0.0d;
        }
        double[] dArr = this.c;
        dArr[0] = 1.0d;
        dArr[5] = 1.0d;
        dArr[10] = 1.0d;
        dArr[15] = 1.0d;
        this.d = true;
        return this;
    }

    /* access modifiers changed from: package-private */
    public z a(int i, int i2, double d2) {
        b();
        double[] dArr = this.c;
        double d3 = (double) i;
        Double.isNaN(d3);
        dArr[0] = 2.0d / d3;
        double d4 = (double) i2;
        Double.isNaN(d4);
        dArr[5] = -2.0d / d4;
        dArr[10] = -2.0d / d2;
        dArr[12] = -1.0d;
        dArr[13] = 1.0d;
        dArr[14] = -1.0d;
        this.d = false;
        return this;
    }

    /* access modifiers changed from: package-private */
    public z b(double d2, double d3, double d4) {
        b.b();
        z zVar = b;
        double[] dArr = zVar.c;
        dArr[12] = d2;
        dArr[13] = d3;
        dArr[14] = d4;
        zVar.d = false;
        return a(zVar);
    }

    /* access modifiers changed from: package-private */
    public z c(double d2) {
        for (int i = 15; i >= 0; i--) {
            this.c[i] = d2;
        }
        this.d = false;
        return this;
    }

    /* access modifiers changed from: package-private */
    public float[] c() {
        return a(a);
    }

    /* access modifiers changed from: package-private */
    public float[] a(float[] fArr) {
        for (int i = 15; i >= 0; i--) {
            fArr[i] = (float) this.c[i];
        }
        return fArr;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.c[0]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[1]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[2]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[3]);
        sb.append(10);
        sb.append(this.c[4]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[5]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[6]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[7]);
        sb.append(10);
        sb.append(this.c[8]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[9]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[10]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[11]);
        sb.append(10);
        sb.append(this.c[12]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[13]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[14]);
        sb.append((char) TokenParser.SP);
        sb.append(this.c[15]);
        sb.append(10);
        return sb.toString();
    }
}
