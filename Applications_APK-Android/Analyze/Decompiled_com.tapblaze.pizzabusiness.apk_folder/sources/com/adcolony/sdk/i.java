package com.adcolony.sdk;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.Settings;
import com.adcolony.sdk.w;
import org.json.JSONObject;

class i extends ContentObserver {
    private AudioManager a;
    private AdColonyInterstitial b;

    public boolean deliverSelfNotifications() {
        return false;
    }

    public i(Handler handler, AdColonyInterstitial adColonyInterstitial) {
        super(handler);
        Context c = a.c();
        if (c != null) {
            this.a = (AudioManager) c.getSystemService("audio");
            this.b = adColonyInterstitial;
            c.getApplicationContext().getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this);
        }
    }

    public void onChange(boolean z) {
        AdColonyInterstitial adColonyInterstitial;
        if (this.a != null && (adColonyInterstitial = this.b) != null && adColonyInterstitial.d() != null) {
            double streamVolume = (double) ((((float) this.a.getStreamVolume(3)) / 15.0f) * 100.0f);
            JSONObject a2 = u.a();
            u.a(a2, "audio_percentage", streamVolume);
            u.a(a2, "ad_session_id", this.b.d().b());
            u.b(a2, "id", this.b.d().d());
            new ab("AdContainer.on_audio_change", this.b.d().c(), a2).b();
            new w.a().a("Volume changed to ").a(streamVolume).a(w.d);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Context c = a.c();
        if (c != null) {
            c.getApplicationContext().getContentResolver().unregisterContentObserver(this);
        }
        this.b = null;
        this.a = null;
    }
}
