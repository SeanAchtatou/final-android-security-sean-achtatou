package com.adcolony.sdk;

import org.json.JSONObject;

public class AdColonyReward {
    private int a;
    private String b;
    private String c;
    private boolean d;

    AdColonyReward(ab abVar) {
        JSONObject c2 = abVar.c();
        this.a = u.c(c2, "reward_amount");
        this.b = u.b(c2, "reward_name");
        this.d = u.d(c2, "success");
        this.c = u.b(c2, "zone_id");
    }

    public int getRewardAmount() {
        return this.a;
    }

    public String getRewardName() {
        return this.b;
    }

    public String getZoneID() {
        return this.c;
    }

    public boolean success() {
        return this.d;
    }
}
