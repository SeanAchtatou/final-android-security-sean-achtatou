package com.adcolony.sdk;

import android.media.MediaPlayer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONObject;

class aa {
    final String a;
    /* access modifiers changed from: private */
    public final int b;
    private HashMap<Integer, MediaPlayer> c = new HashMap<>();
    private HashMap<Integer, a> d = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, Boolean> e = new HashMap<>();
    private HashMap<Integer, Boolean> f = new HashMap<>();
    private ArrayList<MediaPlayer> g = new ArrayList<>();

    aa(String str, int i) {
        this.a = str;
        this.b = i;
    }

    /* access modifiers changed from: package-private */
    public void a(ab abVar) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        JSONObject c2 = abVar.c();
        int c3 = u.c(c2, "id");
        a aVar = new a(c3, u.d(c2, "repeats"));
        this.c.put(Integer.valueOf(c3), mediaPlayer);
        this.d.put(Integer.valueOf(c3), aVar);
        this.e.put(Integer.valueOf(c3), false);
        this.f.put(Integer.valueOf(c3), false);
        mediaPlayer.setOnErrorListener(aVar);
        mediaPlayer.setOnPreparedListener(aVar);
        try {
            mediaPlayer.setDataSource(u.b(c2, "filepath"));
        } catch (Exception unused) {
            JSONObject a2 = u.a();
            u.b(a2, "id", c3);
            u.a(a2, "ad_session_id", this.a);
            new ab("AudioPlayer.on_error", this.b, a2).b();
        }
        mediaPlayer.prepareAsync();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.g.clear();
        for (MediaPlayer next : this.c.values()) {
            if (next.isPlaying()) {
                next.pause();
                this.g.add(next);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        Iterator<MediaPlayer> it = this.g.iterator();
        while (it.hasNext()) {
            it.next().start();
        }
        this.g.clear();
    }

    /* access modifiers changed from: package-private */
    public void b(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        if (this.f.get(Integer.valueOf(c2)).booleanValue()) {
            this.c.get(Integer.valueOf(c2)).pause();
        }
    }

    /* access modifiers changed from: package-private */
    public void c(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        if (this.e.get(Integer.valueOf(c2)).booleanValue()) {
            this.c.get(Integer.valueOf(c2)).start();
            this.f.put(Integer.valueOf(c2), true);
        }
    }

    /* access modifiers changed from: package-private */
    public void d(ab abVar) {
        this.c.remove(Integer.valueOf(u.c(abVar.c(), "id"))).release();
    }

    /* access modifiers changed from: package-private */
    public void e(ab abVar) {
        int c2 = u.c(abVar.c(), "id");
        if (this.f.get(Integer.valueOf(c2)).booleanValue()) {
            MediaPlayer mediaPlayer = this.c.get(Integer.valueOf(c2));
            mediaPlayer.pause();
            mediaPlayer.seekTo(0);
        }
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, MediaPlayer> c() {
        return this.c;
    }

    private class a implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
        int a;
        boolean b;

        a(int i, boolean z) {
            this.a = i;
            this.b = z;
        }

        public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            JSONObject a2 = u.a();
            u.b(a2, "id", this.a);
            u.a(a2, "ad_session_id", aa.this.a);
            new ab("AudioPlayer.on_error", aa.this.b, a2).b();
            return true;
        }

        public void onPrepared(MediaPlayer mediaPlayer) {
            mediaPlayer.setLooping(this.b);
            aa.this.e.put(Integer.valueOf(this.a), true);
            JSONObject a2 = u.a();
            u.b(a2, "id", this.a);
            u.a(a2, "ad_session_id", aa.this.a);
            new ab("AudioPlayer.on_ready", aa.this.b, a2).b();
        }
    }
}
