package com.adcolony.sdk;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.adcolony.sdk.w;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.iab.omid.library.adcolony.ScriptInjector;
import com.ironsource.sdk.constants.Constants;
import cz.msebera.android.httpclient.HttpHost;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import org.json.JSONArray;
import org.json.JSONObject;

class av extends WebView implements ae {
    static boolean a = false;
    private boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    private boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    private boolean E;
    private boolean F;
    private boolean G;
    /* access modifiers changed from: private */
    public JSONArray H = u.b();
    /* access modifiers changed from: private */
    public JSONObject I = u.a();
    private JSONObject J = u.a();
    /* access modifiers changed from: private */
    public c K;
    /* access modifiers changed from: private */
    public ab L;
    private ImageView M;
    /* access modifiers changed from: private */
    public final Object N = new Object();
    /* access modifiers changed from: private */
    public String b;
    private String c;
    private String d = "";
    private String e = "";
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g = "";
    private String h = "";
    private String i = "";
    /* access modifiers changed from: private */
    public String j = "";
    private String k = "";
    /* access modifiers changed from: private */
    public int l;
    private int m;
    private int n;
    private int o;
    private int p;
    /* access modifiers changed from: private */
    public int q;
    private int r;
    /* access modifiers changed from: private */
    public int s;
    private int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v;
    private int w;
    private int x;
    /* access modifiers changed from: private */
    public boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    public void c() {
    }

    private av(Context context) {
        super(context);
    }

    av(Context context, ab abVar, int i2, int i3, c cVar) {
        super(context);
        this.L = abVar;
        a(abVar, i2, i3, cVar);
        e();
    }

    av(Context context, int i2, boolean z2) {
        super(context);
        this.u = i2;
        this.A = z2;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.C) {
            new w.a().a("Ignoring call to execute_js as WebView has been destroyed.").a(w.b);
        } else if (Build.VERSION.SDK_INT >= 19) {
            evaluateJavascript(str, null);
        } else {
            loadUrl("javascript:" + str);
        }
    }

    public int a() {
        return this.u;
    }

    public int b() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public boolean a(ab abVar) {
        JSONObject c2 = abVar.c();
        return u.c(c2, "id") == this.l && u.c(c2, "container_id") == this.K.d() && u.b(c2, "ad_session_id").equals(this.K.b());
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        AdColonyAdView adColonyAdView;
        if (motionEvent.getAction() == 1) {
            if (this.f == null) {
                adColonyAdView = null;
            } else {
                adColonyAdView = a.a().l().e().get(this.f);
            }
            if (adColonyAdView != null && !adColonyAdView.getUserInteraction()) {
                JSONObject a2 = u.a();
                u.a(a2, "ad_session_id", this.f);
                new ab("WebView.on_first_click", 1, a2).b();
                adColonyAdView.setUserInteraction(true);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.av.a(boolean, com.adcolony.sdk.ab):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.adcolony.sdk.av.a(java.lang.String, java.lang.String):java.lang.String
      com.adcolony.sdk.av.a(com.adcolony.sdk.av, org.json.JSONArray):org.json.JSONArray
      com.adcolony.sdk.av.a(com.adcolony.sdk.av, java.lang.String):void
      com.adcolony.sdk.av.a(org.json.JSONObject, java.lang.String):void
      com.adcolony.sdk.av.a(com.adcolony.sdk.av, boolean):boolean
      com.adcolony.sdk.av.a(boolean, com.adcolony.sdk.ab):void */
    /* access modifiers changed from: package-private */
    public void e() {
        a(false, (ab) null);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2, ab abVar) {
        WebViewClient webViewClient;
        String str;
        String str2;
        if (this.L == null) {
            this.L = abVar;
        }
        final JSONObject c2 = this.L.c();
        this.z = z2;
        this.A = u.d(c2, "is_display_module");
        if (z2) {
            String b2 = u.b(c2, "filepath");
            this.k = u.b(c2, "interstitial_html");
            this.h = u.b(c2, "mraid_filepath");
            this.e = u.b(c2, "base_url");
            this.c = b2;
            this.J = u.f(c2, "iab");
            if (a && this.u == 1) {
                this.c = "android_asset/ADCController.js";
            }
            if (this.k.equals("")) {
                str2 = "file:///" + this.c;
            } else {
                str2 = "";
            }
            this.b = str2;
            this.I = u.f(c2, "info");
            this.f = u.b(c2, "ad_session_id");
            this.y = true;
        }
        setFocusable(true);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        if (Build.VERSION.SDK_INT >= 19) {
            setWebContentsDebuggingEnabled(true);
        }
        setWebChromeClient(new WebChromeClient() {
            public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                new w.a().a("JS Alert: ").a(str2).a(w.d);
                jsResult.confirm();
                return true;
            }

            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                String str;
                ConsoleMessage.MessageLevel messageLevel = consoleMessage.messageLevel();
                String message = consoleMessage.message();
                boolean z = false;
                boolean z2 = messageLevel == ConsoleMessage.MessageLevel.ERROR;
                if (consoleMessage.message().contains("Viewport argument key \"shrink-to-fit\" not recognized and ignored") || consoleMessage.message().contains("Viewport target-densitydpi is not supported.")) {
                    z = true;
                }
                if (message.contains("ADC3_update is not defined") || message.contains("NativeLayer.dispatch_messages is not a function")) {
                    av.this.a(c2, "Unable to communicate with AdColony. Please ensure that you have added an exception for our Javascript interface in your ProGuard configuration and that you do not have a faulty proxy enabled on your device.");
                }
                if (!z && (messageLevel == ConsoleMessage.MessageLevel.WARNING || z2)) {
                    AdColonyInterstitial adColonyInterstitial = null;
                    if (av.this.f != null) {
                        adColonyInterstitial = a.a().l().c().get(av.this.f);
                    }
                    if (adColonyInterstitial == null) {
                        str = "unknown";
                    } else {
                        str = adColonyInterstitial.b();
                    }
                    new w.a().a("onConsoleMessage: ").a(consoleMessage.message()).a(" with ad id: ").a(str).a(z2 ? w.h : w.f);
                }
                return true;
            }
        });
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setGeolocationEnabled(true);
        settings.setUseWideViewPort(true);
        if (Build.VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setAllowUniversalAccessFromFileURLs(true);
            settings.setAllowFileAccess(true);
        }
        if (Build.VERSION.SDK_INT >= 23) {
            webViewClient = new a() {
                public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
                    if (!av.this.B || !webResourceRequest.isForMainFrame()) {
                        return false;
                    }
                    Uri url = webResourceRequest.getUrl();
                    at.a(new Intent("android.intent.action.VIEW", url));
                    JSONObject a2 = u.a();
                    u.a(a2, "url", url.toString());
                    u.a(a2, "ad_session_id", av.this.f);
                    new ab("WebView.redirect_detected", av.this.K.c(), a2).b();
                    aq n = a.a().n();
                    n.b(av.this.f);
                    n.a(av.this.f);
                    return true;
                }

                public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
                    av.this.a(webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
                }

                public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
                    if (webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                        try {
                            return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(av.this.g.getBytes("UTF-8")));
                        } catch (UnsupportedEncodingException unused) {
                            new w.a().a("UTF-8 not supported.").a(w.h);
                        }
                    }
                    return null;
                }
            };
        } else {
            webViewClient = Build.VERSION.SDK_INT >= 21 ? new a() {
                public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
                    if (webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                        try {
                            return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(av.this.g.getBytes("UTF-8")));
                        } catch (UnsupportedEncodingException unused) {
                            new w.a().a("UTF-8 not supported.").a(w.h);
                        }
                    }
                    return null;
                }
            } : new a();
        }
        addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void push_messages(String str) {
                av.this.b(str);
            }

            @JavascriptInterface
            public void dispatch_messages(String str) {
                av.this.b(str);
            }

            @JavascriptInterface
            public String pull_messages() {
                String str;
                synchronized (av.this.N) {
                    str = "[]";
                    if (av.this.H.length() > 0) {
                        if (av.this.y) {
                            str = av.this.H.toString();
                        }
                        JSONArray unused = av.this.H = u.b();
                    }
                }
                return str;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.adcolony.sdk.av.a(com.adcolony.sdk.av, boolean):boolean
             arg types: [com.adcolony.sdk.av, int]
             candidates:
              com.adcolony.sdk.av.a(java.lang.String, java.lang.String):java.lang.String
              com.adcolony.sdk.av.a(com.adcolony.sdk.av, org.json.JSONArray):org.json.JSONArray
              com.adcolony.sdk.av.a(com.adcolony.sdk.av, java.lang.String):void
              com.adcolony.sdk.av.a(org.json.JSONObject, java.lang.String):void
              com.adcolony.sdk.av.a(boolean, com.adcolony.sdk.ab):void
              com.adcolony.sdk.av.a(com.adcolony.sdk.av, boolean):boolean */
            @JavascriptInterface
            public void enable_reverse_messaging() {
                boolean unused = av.this.D = true;
            }
        }, "NativeLayer");
        setWebViewClient(webViewClient);
        if (this.A) {
            try {
                if (this.k.equals("")) {
                    FileInputStream fileInputStream = new FileInputStream(this.c);
                    StringBuilder sb = new StringBuilder(fileInputStream.available());
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = fileInputStream.read(bArr, 0, 1024);
                        if (read < 0) {
                            break;
                        }
                        sb.append(new String(bArr, 0, read));
                    }
                    if (this.c.contains(".html")) {
                        str = sb.toString();
                    } else {
                        str = "<html><script>" + sb.toString() + "</script></html>";
                    }
                } else {
                    str = this.k.replaceFirst("script\\s*src\\s*=\\s*\"mraid.js\"", "script src=\"file://" + this.h + "\"");
                }
                String b3 = u.b(u.f(c2, "info"), "metadata");
                loadDataWithBaseURL(this.b.equals("") ? this.e : this.b, a(str, u.b(u.a(b3), "iab_filepath")).replaceFirst("var\\s*ADC_DEVICE_INFO\\s*=\\s*null\\s*;", Matcher.quoteReplacement("var ADC_DEVICE_INFO = " + b3 + ";")), "text/html", null, null);
            } catch (IOException e2) {
                a(e2);
                return;
            } catch (IllegalArgumentException e3) {
                a(e3);
                return;
            } catch (IndexOutOfBoundsException e4) {
                a(e4);
                return;
            }
        } else if (!this.b.startsWith(HttpHost.DEFAULT_SCHEME_NAME) && !this.b.startsWith(Constants.ParametersKeys.FILE)) {
            loadDataWithBaseURL(this.e, this.b, "text/html", null, null);
        } else if (this.b.contains(".html") || !this.b.startsWith(Constants.ParametersKeys.FILE)) {
            loadUrl(this.b);
        } else {
            loadDataWithBaseURL(this.b, "<html><script src=\"" + this.b + "\"></script></html>", "text/html", null, null);
        }
        if (!z2) {
            f();
            g();
        }
        if (z2 || this.y) {
            a.a().q().a(this);
        }
        if (!this.d.equals("")) {
            a(this.d);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.r.a(java.lang.String, boolean):java.lang.StringBuilder
     arg types: [java.lang.String, int]
     candidates:
      com.adcolony.sdk.r.a(com.adcolony.sdk.r, com.adcolony.sdk.ab):boolean
      com.adcolony.sdk.r.a(com.adcolony.sdk.ab, java.io.File):boolean
      com.adcolony.sdk.r.a(java.lang.String, boolean):java.lang.StringBuilder */
    private String a(String str, String str2) {
        ag agVar;
        d l2 = a.a().l();
        AdColonyInterstitial adColonyInterstitial = l2.c().get(this.f);
        AdColonyAdViewListener adColonyAdViewListener = l2.d().get(this.f);
        if (adColonyInterstitial != null && this.J.length() > 0 && !u.b(this.J, "ad_type").equals("video")) {
            adColonyInterstitial.a(this.J);
        } else if (adColonyAdViewListener != null && this.J.length() > 0) {
            adColonyAdViewListener.a(new ag(this.J, this.f));
        }
        if (adColonyInterstitial == null) {
            agVar = null;
        } else {
            agVar = adColonyInterstitial.h();
        }
        if (agVar == null && adColonyAdViewListener != null) {
            agVar = adColonyAdViewListener.c();
        }
        if (agVar != null && agVar.c() == 2) {
            this.E = true;
            if (!str2.equals("")) {
                try {
                    return ScriptInjector.injectScriptContentIntoHtml(a.a().j().a(str2, false).toString(), str);
                } catch (IOException e2) {
                    a(e2);
                }
            }
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        JSONArray b2 = u.b(str);
        for (int i2 = 0; i2 < b2.length(); i2++) {
            a.a().q().a(u.d(b2, i2));
        }
    }

    private boolean a(Exception exc) {
        AdColonyInterstitialListener listener;
        new w.a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(u.b(this.I, "metadata")).a(w.h);
        AdColonyInterstitial remove = a.a().l().c().remove(u.b(this.I, "ad_session_id"));
        if (remove == null || (listener = remove.getListener()) == null) {
            return false;
        }
        listener.onExpiring(remove);
        remove.a(true);
        return true;
    }

    private void b(Exception exc) {
        new w.a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(u.b(this.I, "metadata")).a(w.h);
        JSONObject a2 = u.a();
        u.a(a2, "id", this.f);
        new ab("AdSession.on_error", this.K.c(), a2).b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.av$7, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.av$8, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.av$9, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.av$10, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    /* access modifiers changed from: package-private */
    public void f() {
        this.K.m().add(a.a("WebView.set_visible", (ad) new ad() {
            public void a(final ab abVar) {
                if (av.this.a(abVar)) {
                    at.a(new Runnable() {
                        public void run() {
                            av.this.c(abVar);
                        }
                    });
                }
            }
        }, true));
        this.K.m().add(a.a("WebView.set_bounds", (ad) new ad() {
            public void a(final ab abVar) {
                if (av.this.a(abVar)) {
                    at.a(new Runnable() {
                        public void run() {
                            av.this.b(abVar);
                        }
                    });
                }
            }
        }, true));
        this.K.m().add(a.a("WebView.execute_js", (ad) new ad() {
            public void a(final ab abVar) {
                if (av.this.a(abVar)) {
                    at.a(new Runnable() {
                        public void run() {
                            av.this.a(u.b(abVar.c(), "custom_js"));
                        }
                    });
                }
            }
        }, true));
        this.K.m().add(a.a("WebView.set_transparent", (ad) new ad() {
            public void a(final ab abVar) {
                if (av.this.a(abVar)) {
                    at.a(new Runnable() {
                        public void run() {
                            av.this.b(u.d(abVar.c(), Constants.ParametersKeys.TRANSPARENT));
                        }
                    });
                }
            }
        }, true));
        this.K.n().add("WebView.set_visible");
        this.K.n().add("WebView.set_bounds");
        this.K.n().add("WebView.execute_js");
        this.K.n().add("WebView.set_transparent");
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        setBackgroundColor(z2 ? 0 : -1);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.q, this.s);
        layoutParams.setMargins(this.m, this.o, 0, 0);
        layoutParams.gravity = 0;
        this.K.addView(this, layoutParams);
        if (!this.i.equals("") && !this.j.equals("")) {
            w();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ab abVar, int i2, c cVar) {
        a(abVar, i2, -1, cVar);
        g();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.r.a(java.lang.String, boolean):java.lang.StringBuilder
     arg types: [java.lang.String, int]
     candidates:
      com.adcolony.sdk.r.a(com.adcolony.sdk.r, com.adcolony.sdk.ab):boolean
      com.adcolony.sdk.r.a(com.adcolony.sdk.ab, java.io.File):boolean
      com.adcolony.sdk.r.a(java.lang.String, boolean):java.lang.StringBuilder */
    /* access modifiers changed from: package-private */
    public void a(ab abVar, int i2, int i3, c cVar) {
        JSONObject c2 = abVar.c();
        this.b = u.b(c2, "url");
        if (this.b.equals("")) {
            this.b = u.b(c2, "data");
        }
        this.e = u.b(c2, "base_url");
        this.d = u.b(c2, "custom_js");
        this.f = u.b(c2, "ad_session_id");
        this.I = u.f(c2, "info");
        this.h = u.b(c2, "mraid_filepath");
        this.v = u.d(c2, "use_mraid_module") ? a.a().q().d() : this.v;
        this.i = u.b(c2, "ad_choices_filepath");
        this.j = u.b(c2, "ad_choices_url");
        this.F = u.d(c2, "disable_ad_choices");
        this.G = u.d(c2, "ad_choices_snap_to_webview");
        this.w = u.c(c2, "ad_choices_width");
        this.x = u.c(c2, "ad_choices_height");
        if (this.J.length() == 0) {
            this.J = u.f(c2, "iab");
        }
        boolean z2 = false;
        if (!this.A && !this.h.equals("")) {
            if (this.v > 0) {
                this.b = a(this.b.replaceFirst("script\\s*src\\s*=\\s*\"mraid.js\"", "script src=\"file://" + this.h + "\""), u.b(u.f(this.I, DeviceRequestsHelper.DEVICE_INFO_PARAM), "iab_filepath"));
            } else {
                try {
                    this.g = a.a().j().a(this.h, false).toString();
                    this.g = this.g.replaceFirst("bridge.os_name\\s*=\\s*\"\"\\s*;", "bridge.os_name = \"\";\nvar ADC_DEVICE_INFO = " + this.I.toString() + ";\n");
                } catch (IOException e2) {
                    b(e2);
                } catch (IllegalArgumentException e3) {
                    b(e3);
                } catch (IndexOutOfBoundsException e4) {
                    b(e4);
                }
            }
        }
        this.l = i2;
        this.K = cVar;
        if (i3 >= 0) {
            this.u = i3;
        } else {
            f();
        }
        this.q = u.c(c2, "width");
        this.s = u.c(c2, "height");
        this.m = u.c(c2, "x");
        this.o = u.c(c2, "y");
        this.r = this.q;
        this.t = this.s;
        this.p = this.o;
        this.n = this.m;
        if (u.d(c2, "enable_messages") || this.z) {
            z2 = true;
        }
        this.y = z2;
        k();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void b(ab abVar) {
        JSONObject c2 = abVar.c();
        this.m = u.c(c2, "x");
        this.o = u.c(c2, "y");
        this.q = u.c(c2, "width");
        this.s = u.c(c2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.m, this.o, 0, 0);
        layoutParams.width = this.q;
        layoutParams.height = this.s;
        setLayoutParams(layoutParams);
        if (this.z) {
            JSONObject a2 = u.a();
            u.b(a2, "success", true);
            u.b(a2, "id", this.u);
            abVar.a(a2).b();
        }
        h();
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.M != null) {
            int q2 = a.a().m().q();
            int r2 = a.a().m().r();
            if (this.G) {
                q2 = this.m + this.q;
            }
            if (this.G) {
                r2 = this.o + this.s;
            }
            float p2 = a.a().m().p();
            int i2 = (int) (((float) this.w) * p2);
            int i3 = (int) (((float) this.x) * p2);
            this.M.setLayoutParams(new AbsoluteLayout.LayoutParams(i2, i3, q2 - i2, r2 - i3));
        }
    }

    private void w() {
        Context c2 = a.c();
        if (c2 != null && this.K != null && !this.F) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(-1);
            gradientDrawable.setShape(1);
            this.M = new ImageView(c2);
            this.M.setImageURI(Uri.fromFile(new File(this.i)));
            this.M.setBackground(gradientDrawable);
            this.M.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    at.a(new Intent("android.intent.action.VIEW", Uri.parse(av.this.j)));
                    a.a().n().a(av.this.f);
                }
            });
            h();
            addView(this.M);
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        ImageView imageView = this.M;
        if (imageView != null) {
            this.K.a(imageView);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void c(ab abVar) {
        if (u.d(abVar.c(), "visible")) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
        if (this.z) {
            JSONObject a2 = u.a();
            u.b(a2, "success", true);
            u.b(a2, "id", this.u);
            abVar.a(a2).b();
        }
    }

    public void a(JSONObject jSONObject) {
        synchronized (this.N) {
            this.H.put(jSONObject);
        }
    }

    public void d() {
        if (a.d() && this.B && !this.D) {
            j();
        }
    }

    /* access modifiers changed from: package-private */
    public void j() {
        at.a(new Runnable() {
            public void run() {
                String str;
                synchronized (av.this.N) {
                    str = "";
                    if (av.this.H.length() > 0) {
                        if (av.this.y) {
                            str = av.this.H.toString();
                        }
                        JSONArray unused = av.this.H = u.b();
                    }
                }
                if (av.this.y) {
                    av avVar = av.this;
                    avVar.a("NativeLayer.dispatch_messages(ADC3_update(" + str + "));");
                }
            }
        });
    }

    private class a extends WebViewClient {
        private a() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!av.this.B) {
                return false;
            }
            at.a(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            aq n = a.a().n();
            n.b(av.this.f);
            n.a(av.this.f);
            JSONObject a = u.a();
            u.a(a, "url", str);
            u.a(a, "ad_session_id", av.this.f);
            new ab("WebView.redirect_detected", av.this.K.c(), a).b();
            return true;
        }

        public void onLoadResource(WebView webView, String str) {
            if (str.equals(av.this.b)) {
                av.this.a("if (typeof(CN) != 'undefined' && CN.div) {\n  if (typeof(cn_dispatch_on_touch_begin) != 'undefined') CN.div.removeEventListener('mousedown',  cn_dispatch_on_touch_begin, true);\n  if (typeof(cn_dispatch_on_touch_end) != 'undefined')   CN.div.removeEventListener('mouseup',  cn_dispatch_on_touch_end, true);\n  if (typeof(cn_dispatch_on_touch_move) != 'undefined')  CN.div.removeEventListener('mousemove',  cn_dispatch_on_touch_move, true);\n}\n");
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            boolean unused = av.this.B = false;
            new w.a().a("onPageStarted with URL = ").a(str).a(w.d);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
         arg types: [org.json.JSONObject, java.lang.String, int]
         candidates:
          com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
          com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
        public void onPageFinished(WebView webView, String str) {
            JSONObject a = u.a();
            u.b(a, "id", av.this.l);
            u.a(a, "url", str);
            new w.a().a("onPageFinished called with URL = ").a(str).a(w.b);
            if (av.this.K == null) {
                new ab("WebView.on_load", av.this.u, a).b();
            } else {
                u.a(a, "ad_session_id", av.this.f);
                u.b(a, "container_id", av.this.K.d());
                new ab("WebView.on_load", av.this.K.c(), a).b();
            }
            if ((av.this.y || av.this.z) && !av.this.B) {
                int m = av.this.v > 0 ? av.this.v : av.this.u;
                if (av.this.v > 0) {
                    float p = a.a().m().p();
                    u.b(av.this.I, "app_orientation", at.j(at.h()));
                    u.b(av.this.I, "x", at.a(av.this));
                    u.b(av.this.I, "y", at.b(av.this));
                    u.b(av.this.I, "width", (int) (((float) av.this.q) / p));
                    u.b(av.this.I, "height", (int) (((float) av.this.s) / p));
                    u.a(av.this.I, "ad_session_id", av.this.f);
                }
                av avVar = av.this;
                avVar.a("ADC3_init(" + m + "," + av.this.I.toString() + ");");
                boolean unused = av.this.B = true;
            }
            if (!av.this.z) {
                return;
            }
            if (av.this.u != 1 || av.this.v > 0) {
                JSONObject a2 = u.a();
                u.b(a2, "success", true);
                u.b(a2, "id", av.this.u);
                av.this.L.a(a2).b();
            }
        }

        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            if (Build.VERSION.SDK_INT < 21 && str.endsWith("mraid.js")) {
                try {
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(av.this.g.getBytes("UTF-8")));
                } catch (UnsupportedEncodingException unused) {
                    new w.a().a("UTF-8 not supported.").a(w.h);
                }
            }
            return null;
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            if (Build.VERSION.SDK_INT < 23) {
                av.this.a(i, str, str2);
            }
        }

        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            if (Build.VERSION.SDK_INT < 26) {
                return super.onRenderProcessGone(webView, renderProcessGoneDetail);
            }
            if (!renderProcessGoneDetail.didCrash()) {
                return true;
            }
            av.this.a(u.a(), "An error occurred while rendering the ad. Ad closing.");
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject, String str) {
        Context c2 = a.c();
        if (c2 != null && (c2 instanceof b)) {
            ab abVar = new ab("AdSession.finish_fullscreen_ad", 0);
            u.b(jSONObject, "status", 1);
            new w.a().a(str).a(w.g);
            ((b) c2).a(abVar);
        } else if (this.u == 1) {
            new w.a().a("Unable to communicate with controller, disabling AdColony.").a(w.g);
            AdColony.disable();
        } else if (this.v > 0) {
            this.y = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void k() {
        at.a(new Runnable() {
            public void run() {
                ag agVar;
                int i;
                try {
                    d l = a.a().l();
                    AdColonyInterstitial adColonyInterstitial = l.c().get(av.this.f);
                    AdColonyAdView adColonyAdView = l.e().get(av.this.f);
                    if (adColonyInterstitial == null) {
                        agVar = null;
                    } else {
                        agVar = adColonyInterstitial.h();
                    }
                    if (agVar == null && adColonyAdView != null) {
                        agVar = adColonyAdView.getOmidManager();
                    }
                    if (agVar == null) {
                        i = -1;
                    } else {
                        i = agVar.c();
                    }
                    if (agVar != null && i == 2) {
                        agVar.a(av.this);
                        agVar.a(av.this.K);
                    }
                } catch (IllegalArgumentException unused) {
                    new w.a().a("IllegalArgumentException when creating omid session").a(w.h);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str, String str2) {
        if (this.K != null) {
            JSONObject a2 = u.a();
            u.b(a2, "id", this.l);
            u.a(a2, "ad_session_id", this.f);
            u.b(a2, "container_id", this.K.d());
            u.b(a2, "code", i2);
            u.a(a2, "error", str);
            u.a(a2, "url", str2);
            new ab("WebView.on_error", this.K.c(), a2).b();
        }
        new w.a().a("onReceivedError: ").a(str).a(w.h);
    }

    /* access modifiers changed from: package-private */
    public boolean l() {
        return this.E;
    }

    /* access modifiers changed from: package-private */
    public boolean m() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public boolean n() {
        return this.C;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.C = z2;
    }

    /* access modifiers changed from: package-private */
    public int o() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public int p() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public int q() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public int r() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public int s() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public int t() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public int u() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public int v() {
        return this.o;
    }
}
