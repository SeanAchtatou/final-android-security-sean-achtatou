package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;
import androidx.core.view.ViewCompat;
import com.adcolony.sdk.w;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.ironsource.sdk.constants.Constants;
import cz.msebera.android.httpclient.message.TokenParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.CRC32;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class at {
    static final int a = 128;
    static ExecutorService b = Executors.newSingleThreadExecutor();
    static Handler c;

    at() {
    }

    static boolean a(String str) {
        Application application;
        Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        try {
            if (c2 instanceof Application) {
                application = (Application) c2;
            } else {
                application = ((Activity) c2).getApplication();
            }
            application.getPackageManager().getApplicationInfo(str, 0);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    static boolean a() {
        try {
            j a2 = a.a();
            File file = new File(a2.o().g() + "026ae9c9824b3e483fa6c71fa88f57ae27816141");
            File file2 = new File(a2.o().g() + "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5");
            boolean a3 = a2.j().a(file);
            boolean a4 = a2.j().a(file2);
            if (!a3 || !a4) {
                return false;
            }
            return true;
        } catch (Exception unused) {
            new w.a().a("Unable to delete controller or launch response.").a(w.h);
            return false;
        }
    }

    static String b() {
        Context c2 = a.c();
        if (c2 == null) {
            return "1.0";
        }
        try {
            return c2.getPackageManager().getPackageInfo(c2.getPackageName(), 0).versionName;
        } catch (Exception unused) {
            new w.a().a("Failed to retrieve package info.").a(w.h);
            return "1.0";
        }
    }

    static int c() {
        Context c2 = a.c();
        if (c2 == null) {
            return 0;
        }
        try {
            return c2.getPackageManager().getPackageInfo(c2.getPackageName(), 0).versionCode;
        } catch (Exception unused) {
            new w.a().a("Failed to retrieve package info.").a(w.h);
            return 0;
        }
    }

    static String d() {
        Application application;
        Context c2 = a.c();
        if (c2 == null) {
            return "";
        }
        if (c2 instanceof Application) {
            application = (Application) c2;
        } else {
            application = ((Activity) c2).getApplication();
        }
        PackageManager packageManager = application.getPackageManager();
        try {
            CharSequence applicationLabel = packageManager.getApplicationLabel(packageManager.getApplicationInfo(c2.getPackageName(), 0));
            if (applicationLabel == null) {
                return "";
            }
            return applicationLabel.toString();
        } catch (Exception unused) {
            new w.a().a("Failed to retrieve application label.").a(w.h);
            return "";
        }
    }

    static int b(String str) {
        CRC32 crc32 = new CRC32();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            crc32.update(str.charAt(i));
        }
        return (int) crc32.getValue();
    }

    static String c(String str) {
        try {
            return aw.a(str);
        } catch (Exception unused) {
            return null;
        }
    }

    static String e() {
        return UUID.randomUUID().toString();
    }

    static JSONArray a(int i) {
        JSONArray b2 = u.b();
        for (int i2 = 0; i2 < i; i2++) {
            u.a(b2, e());
        }
        return b2;
    }

    static boolean a(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length != strArr2.length) {
            return false;
        }
        Arrays.sort(strArr);
        Arrays.sort(strArr2);
        return Arrays.equals(strArr, strArr2);
    }

    static boolean a(Runnable runnable) {
        Looper mainLooper = Looper.getMainLooper();
        if (mainLooper == null) {
            return false;
        }
        if (c == null) {
            c = new Handler(mainLooper);
        }
        if (mainLooper == Looper.myLooper()) {
            runnable.run();
            return true;
        }
        c.post(runnable);
        return true;
    }

    static double f() {
        double currentTimeMillis = (double) System.currentTimeMillis();
        Double.isNaN(currentTimeMillis);
        return currentTimeMillis / 1000.0d;
    }

    static boolean d(String str) {
        if (str != null && str.length() <= 128) {
            return true;
        }
        new w.a().a("String must be non-null and the max length is 128 characters.").a(w.e);
        return false;
    }

    static boolean a(AudioManager audioManager) {
        if (audioManager == null) {
            new w.a().a("isAudioEnabled() called with a null AudioManager").a(w.h);
            return false;
        }
        try {
            if (audioManager.getStreamVolume(3) > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            new w.a().a("Exception occurred when accessing AudioManager.getStreamVolume: ").a(e.toString()).a(w.h);
            return false;
        }
    }

    static double b(AudioManager audioManager) {
        if (audioManager == null) {
            new w.a().a("getAudioVolume() called with a null AudioManager").a(w.h);
            return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        }
        try {
            double streamVolume = (double) audioManager.getStreamVolume(3);
            double streamMaxVolume = (double) audioManager.getStreamMaxVolume(3);
            if (streamMaxVolume == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
                return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
            }
            Double.isNaN(streamVolume);
            Double.isNaN(streamMaxVolume);
            return streamVolume / streamMaxVolume;
        } catch (Exception e) {
            new w.a().a("Exception occurred when accessing AudioManager: ").a(e.toString()).a(w.h);
            return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        }
    }

    static AudioManager a(Context context) {
        if (context != null) {
            return (AudioManager) context.getSystemService("audio");
        }
        new w.a().a("getAudioManager called with a null Context").a(w.h);
        return null;
    }

    static void e(String str) {
        File[] listFiles = new File(str).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isDirectory()) {
                    new w.a().a(">").a(file.getAbsolutePath()).a(w.b);
                    e(file.getAbsolutePath());
                } else {
                    new w.a().a(file.getAbsolutePath()).a(w.b);
                }
            }
        }
    }

    static String a(double d, int i) {
        StringBuilder sb = new StringBuilder();
        a(d, i, sb);
        return sb.toString();
    }

    static void a(double d, int i, StringBuilder sb) {
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            sb.append(d);
            return;
        }
        if (d < FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) {
            d = -d;
            sb.append('-');
        }
        if (i == 0) {
            sb.append(Math.round(d));
            return;
        }
        long pow = (long) Math.pow(10.0d, (double) i);
        double d2 = (double) pow;
        Double.isNaN(d2);
        long round = Math.round(d * d2);
        sb.append(round / pow);
        sb.append('.');
        long j = round % pow;
        if (j == 0) {
            for (int i2 = 0; i2 < i; i2++) {
                sb.append('0');
            }
            return;
        }
        for (long j2 = j * 10; j2 < pow; j2 *= 10) {
            sb.append('0');
        }
        sb.append(j);
    }

    static String f(String str) {
        return str == null ? "" : URLDecoder.decode(str);
    }

    static String b(Context context) {
        try {
            return context.getPackageName();
        } catch (Exception unused) {
            return "unknown";
        }
    }

    static String a(Exception exc) {
        StringWriter stringWriter = new StringWriter();
        exc.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    static int g(String str) {
        try {
            return (int) Long.parseLong(str, 16);
        } catch (NumberFormatException unused) {
            new w.a().a("Unable to parse '").a(str).a("' as a color.").a(w.f);
            return ViewCompat.MEASURED_STATE_MASK;
        }
    }

    static int c(Context context) {
        int identifier;
        if (context != null && (identifier = context.getResources().getIdentifier("status_bar_height", "dimen", "android")) > 0) {
            return context.getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    static boolean g() {
        Context c2 = a.c();
        return c2 != null && Build.VERSION.SDK_INT >= 24 && (c2 instanceof Activity) && ((Activity) c2).isInMultiWindowMode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [?, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    static boolean a(String str, File file) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr = new byte[8192];
                while (true) {
                    try {
                        int read = fileInputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        instance.update(bArr, 0, read);
                    } catch (IOException e) {
                        throw new RuntimeException("Unable to process file for MD5", e);
                    } catch (Throwable th) {
                        try {
                            fileInputStream.close();
                        } catch (IOException unused) {
                            new w.a().a("Exception on closing MD5 input stream").a(w.h);
                        }
                        throw th;
                    }
                }
                boolean equals = str.equals(String.format("%40s", new BigInteger(1, instance.digest()).toString(16)).replace((char) TokenParser.SP, '0'));
                try {
                    fileInputStream.close();
                } catch (IOException unused2) {
                    new w.a().a("Exception on closing MD5 input stream").a(w.h);
                }
                return equals;
            } catch (FileNotFoundException unused3) {
                new w.a().a("Exception while getting FileInputStream").a(w.h);
                return false;
            }
        } catch (NoSuchAlgorithmException unused4) {
            new w.a().a("Exception while getting Digest").a(w.h);
            return false;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:4|5|6) */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0024, code lost:
        return new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", java.util.Locale.US).parse(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        return new java.text.SimpleDateFormat("yyyy-MM-dd", java.util.Locale.US).parse(r5);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x0020 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0025 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.util.Date h(java.lang.String r5) {
        /*
            java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat
            java.util.Locale r1 = java.util.Locale.US
            java.lang.String r2 = "yyyy-MM-dd'T'HH:mmZ"
            r0.<init>(r2, r1)
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            java.util.Locale r2 = java.util.Locale.US
            java.lang.String r3 = "yyyy-MM-dd'T'HH:mm:ssZ"
            r1.<init>(r3, r2)
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.util.Locale r3 = java.util.Locale.US
            java.lang.String r4 = "yyyy-MM-dd"
            r2.<init>(r4, r3)
            java.util.Date r5 = r0.parse(r5)     // Catch:{ Exception -> 0x0020 }
            return r5
        L_0x0020:
            java.util.Date r5 = r1.parse(r5)     // Catch:{ Exception -> 0x0025 }
            return r5
        L_0x0025:
            java.util.Date r5 = r2.parse(r5)     // Catch:{ Exception -> 0x002a }
            return r5
        L_0x002a:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.at.h(java.lang.String):java.util.Date");
    }

    static String a(JSONArray jSONArray) throws JSONException {
        String str = "";
        for (int i = 0; i < jSONArray.length(); i++) {
            if (i > 0) {
                str = str + ",";
            }
            switch (jSONArray.getInt(i)) {
                case 1:
                    str = str + "MO";
                    break;
                case 2:
                    str = str + "TU";
                    break;
                case 3:
                    str = str + "WE";
                    break;
                case 4:
                    str = str + "TH";
                    break;
                case 5:
                    str = str + "FR";
                    break;
                case 6:
                    str = str + "SA";
                    break;
                case 7:
                    str = str + "SU";
                    break;
            }
        }
        return str;
    }

    static String b(JSONArray jSONArray) throws JSONException {
        String str = "";
        for (int i = 0; i < jSONArray.length(); i++) {
            if (i > 0) {
                str = str + ",";
            }
            str = str + jSONArray.getInt(i);
        }
        return str;
    }

    static boolean a(Intent intent, boolean z) {
        try {
            Context c2 = a.c();
            if (c2 == null) {
                return false;
            }
            AdColonyInterstitial v = a.a().v();
            if (v != null && v.g()) {
                v.h().d();
            }
            if (z) {
                c2.startActivity(Intent.createChooser(intent, "Handle this via..."));
                return true;
            }
            c2.startActivity(intent);
            return true;
        } catch (Exception e) {
            new w.a().a(e.toString()).a(w.f);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.at.a(android.content.Intent, boolean):boolean
     arg types: [android.content.Intent, int]
     candidates:
      com.adcolony.sdk.at.a(double, int):java.lang.String
      com.adcolony.sdk.at.a(java.lang.String, int):boolean
      com.adcolony.sdk.at.a(java.lang.String, java.io.File):boolean
      com.adcolony.sdk.at.a(java.lang.String[], java.lang.String[]):boolean
      com.adcolony.sdk.at.a(android.content.Intent, boolean):boolean */
    static boolean a(Intent intent) {
        return a(intent, false);
    }

    static boolean a(final String str, final int i) {
        final Context c2 = a.c();
        if (c2 == null) {
            return false;
        }
        a(new Runnable() {
            public void run() {
                Toast.makeText(c2, str, i).show();
            }
        });
        return true;
    }

    private static void k(String str) {
        Context c2 = a.c();
        if (c2 != null) {
            try {
                InputStream open = c2.getAssets().open(str);
                FileOutputStream fileOutputStream = new FileOutputStream(a.a().o().d() + str);
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = open.read(bArr);
                    if (read != -1) {
                        fileOutputStream.write(bArr, 0, read);
                    } else {
                        open.close();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        return;
                    }
                }
            } catch (Exception e) {
                new w.a().a("Failed copy hardcoded ad unit file named: ").a(str).a(" with error: ").a(e.getMessage()).a(w.h);
            }
        }
    }

    public static void i(String str) {
        Context c2 = a.c();
        if (c2 != null) {
            try {
                String[] list = c2.getAssets().list(str);
                if (list.length == 0) {
                    k(str);
                    return;
                }
                File file = new File(a.a().o().d() + str);
                if (!file.exists()) {
                    file.mkdir();
                }
                for (int i = 0; i < list.length; i++) {
                    i(str + "/" + list[i]);
                }
            } catch (IOException e) {
                new w.a().a("Failed copy hardcoded ad unit with error: ").a(e.getMessage()).a(w.h);
            }
        }
    }

    static int a(ao aoVar) {
        int i = 0;
        try {
            Context c2 = a.c();
            if (c2 != null) {
                int i2 = (int) (c2.getPackageManager().getPackageInfo(c2.getPackageName(), 0).lastUpdateTime / 1000);
                boolean z = true;
                if (new File(aoVar.g() + "AppVersion").exists()) {
                    if (u.c(u.c(aoVar.g() + "AppVersion"), "last_update") != i2) {
                        i = 1;
                    } else {
                        z = false;
                    }
                } else {
                    i = 2;
                }
                if (z) {
                    JSONObject a2 = u.a();
                    u.b(a2, "last_update", i2);
                    u.h(a2, aoVar.g() + "AppVersion");
                }
            }
        } catch (Exception unused) {
        }
        return i;
    }

    static JSONArray d(Context context) {
        JSONArray b2 = u.b();
        if (context != null) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
                if (packageInfo.requestedPermissions != null) {
                    b2 = u.b();
                    for (String put : packageInfo.requestedPermissions) {
                        b2.put(put);
                    }
                }
            } catch (Exception unused) {
            }
        }
        return b2;
    }

    static String h() {
        Context c2 = a.c();
        if (!(c2 instanceof Activity) || c2.getResources().getConfiguration().orientation != 1) {
            return Constants.ParametersKeys.ORIENTATION_LANDSCAPE;
        }
        return Constants.ParametersKeys.ORIENTATION_PORTRAIT;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int j(java.lang.String r5) {
        /*
            int r0 = r5.hashCode()
            r1 = 729267099(0x2b77bb9b, float:8.8012383E-13)
            r2 = 0
            r3 = -1
            r4 = 1
            if (r0 == r1) goto L_0x001c
            r1 = 1430647483(0x5545f2bb, float:1.36028944E13)
            if (r0 == r1) goto L_0x0012
            goto L_0x0026
        L_0x0012:
            java.lang.String r0 = "landscape"
            boolean r5 = r5.equals(r0)
            if (r5 == 0) goto L_0x0026
            r5 = 1
            goto L_0x0027
        L_0x001c:
            java.lang.String r0 = "portrait"
            boolean r5 = r5.equals(r0)
            if (r5 == 0) goto L_0x0026
            r5 = 0
            goto L_0x0027
        L_0x0026:
            r5 = -1
        L_0x0027:
            if (r5 == 0) goto L_0x002e
            if (r5 == r4) goto L_0x002d
            r2 = -1
            goto L_0x002e
        L_0x002d:
            r2 = 1
        L_0x002e:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.at.j(java.lang.String):int");
    }

    static int a(View view) {
        if (view == null) {
            return 0;
        }
        int[] iArr = {0, 0};
        view.getLocationOnScreen(iArr);
        return (int) (((float) iArr[0]) / a.a().m().p());
    }

    static int b(View view) {
        if (view == null) {
            return 0;
        }
        int[] iArr = {0, 0};
        view.getLocationOnScreen(iArr);
        return (int) (((float) iArr[1]) / a.a().m().p());
    }

    static class a {
        double a;
        double b = ((double) System.currentTimeMillis());

        a(double d) {
            a(d);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            a(this.a);
        }

        /* access modifiers changed from: package-private */
        public void a(double d) {
            this.a = d;
            double currentTimeMillis = (double) System.currentTimeMillis();
            Double.isNaN(currentTimeMillis);
            this.b = (currentTimeMillis / 1000.0d) + this.a;
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            return c() == FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        }

        /* access modifiers changed from: package-private */
        public double c() {
            double currentTimeMillis = (double) System.currentTimeMillis();
            Double.isNaN(currentTimeMillis);
            double d = this.b - (currentTimeMillis / 1000.0d);
            return d <= FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE ? FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE : d;
        }

        public String toString() {
            return at.a(c(), 2);
        }
    }
}
