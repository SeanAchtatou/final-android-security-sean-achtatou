package com.adcolony.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import androidx.core.view.ViewCompat;
import com.adcolony.sdk.w;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

class b extends Activity {
    static final int a = 0;
    static final int b = 1;
    c c;
    int d = -1;
    String e;
    int f;
    boolean g;
    boolean h;
    boolean i;
    boolean j;
    boolean k;
    boolean l;
    e m;

    b() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad
     arg types: [java.lang.String, com.adcolony.sdk.b$1, int]
     candidates:
      com.adcolony.sdk.a.a(android.content.Context, com.adcolony.sdk.AdColonyAppOptions, boolean):void
      com.adcolony.sdk.a.a(java.lang.String, com.adcolony.sdk.ad, boolean):com.adcolony.sdk.ad */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!a.b() || a.a().t() == null) {
            finish();
            return;
        }
        j a2 = a.a();
        this.i = false;
        this.c = a2.t();
        this.c.b(false);
        if (at.g()) {
            this.c.b(true);
        }
        this.e = this.c.b();
        this.f = this.c.c();
        this.m = a.a().l().f().get(this.e);
        this.j = a2.d().getMultiWindowEnabled();
        if (this.j) {
            getWindow().addFlags(2048);
            getWindow().clearFlags(1024);
        } else {
            getWindow().addFlags(1024);
            getWindow().clearFlags(2048);
        }
        requestWindowFeature(1);
        getWindow().getDecorView().setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        if (a2.d().getKeepScreenOn()) {
            getWindow().addFlags(128);
        }
        ViewParent parent = this.c.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this.c);
        }
        setContentView(this.c);
        this.c.m().add(a.a("AdSession.finish_fullscreen_ad", (ad) new ad() {
            public void a(ab abVar) {
                b.this.a(abVar);
            }
        }, true));
        this.c.n().add("AdSession.finish_fullscreen_ad");
        a(this.d);
        if (!this.c.s()) {
            JSONObject a3 = u.a();
            u.a(a3, "id", this.c.b());
            u.b(a3, "screen_width", this.c.p());
            u.b(a3, "screen_height", this.c.o());
            new w.a().a("AdSession.on_fullscreen_ad_started").a(w.b);
            new ab("AdSession.on_fullscreen_ad_started", this.c.c(), a3).b();
            this.c.c(true);
            return;
        }
        a();
    }

    public void onPause() {
        super.onPause();
        a(this.h);
        this.h = false;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.m = a.a().l().f().get(this.e);
        Iterator<Map.Entry<Integer, au>> it = this.c.e().entrySet().iterator();
        while (it.hasNext() && !isFinishing()) {
            au auVar = (au) it.next().getValue();
            if (!auVar.j() && auVar.i().isPlaying()) {
                auVar.f();
            }
        }
        e eVar = this.m;
        if (eVar != null) {
            eVar.a();
        }
        AdColonyInterstitial v = a.a().v();
        if (v != null && v.g() && v.h().e() != null && z && this.k) {
            v.h().a("pause");
        }
    }

    public void onResume() {
        super.onResume();
        a();
        b(this.h);
        this.h = true;
        this.l = true;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        for (Map.Entry<Integer, au> value : this.c.e().entrySet()) {
            au auVar = (au) value.getValue();
            if (!auVar.j() && !auVar.i().isPlaying() && !a.a().r().c()) {
                auVar.e();
            }
        }
        e eVar = this.m;
        if (eVar != null) {
            eVar.b();
        }
        AdColonyInterstitial v = a.a().v();
        if (v != null && v.g() && v.h().e() != null) {
            if ((!z || (z && !this.k)) && this.l) {
                v.h().a("resume");
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        if (z && this.h) {
            a.a().k().c(true);
            b(this.h);
            this.k = true;
        } else if (!z && this.h) {
            new w.a().a("Activity is active but window does not have focus, pausing.").a(w.d);
            a.a().k().b(true);
            a(this.h);
            this.k = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (a.b() && this.c != null && !this.g) {
            if ((Build.VERSION.SDK_INT < 24 || !at.g()) && !this.c.r()) {
                JSONObject a2 = u.a();
                u.a(a2, "id", this.c.b());
                new ab("AdSession.on_error", this.c.c(), a2).b();
                this.i = true;
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this instanceof AdColonyInterstitialActivity) {
            a();
        } else {
            ((AdColonyAdViewActivity) this).c();
        }
    }

    public void onBackPressed() {
        JSONObject a2 = u.a();
        u.a(a2, "id", this.c.b());
        new ab("AdSession.on_back_button", this.c.c(), a2).b();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        j a2 = a.a();
        if (this.c == null) {
            this.c = a2.t();
        }
        c cVar = this.c;
        if (cVar != null) {
            cVar.b(false);
            if (at.g()) {
                this.c.b(true);
            }
            int q = a2.m().q();
            int r = this.j ? a2.m().r() - at.c(a.c()) : a2.m().r();
            if (q > 0 && r > 0) {
                JSONObject a3 = u.a();
                JSONObject a4 = u.a();
                float p = a2.m().p();
                u.b(a4, "width", (int) (((float) q) / p));
                u.b(a4, "height", (int) (((float) r) / p));
                u.b(a4, "app_orientation", at.j(at.h()));
                u.b(a4, "x", 0);
                u.b(a4, "y", 0);
                u.a(a4, "ad_session_id", this.c.b());
                u.b(a3, "screen_width", q);
                u.b(a3, "screen_height", r);
                u.a(a3, "ad_session_id", this.c.b());
                u.b(a3, "id", this.c.d());
                this.c.setLayoutParams(new FrameLayout.LayoutParams(q, r));
                this.c.b(q);
                this.c.a(r);
                new ab("MRAID.on_size_change", this.c.c(), a4).b();
                new ab("AdContainer.on_orientation_change", this.c.c(), a3).b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ab abVar) {
        int c2 = u.c(abVar.c(), "status");
        if ((c2 == 5 || c2 == 0 || c2 == 6 || c2 == 1) && !this.g) {
            j a2 = a.a();
            m r = a2.r();
            a2.b(abVar);
            if (r.b() != null) {
                r.b().dismiss();
                r.a((AlertDialog) null);
            }
            if (!this.i) {
                finish();
            }
            this.g = true;
            ((ViewGroup) getWindow().getDecorView()).removeAllViews();
            a2.c(false);
            JSONObject a3 = u.a();
            u.a(a3, "id", this.c.b());
            new ab("AdSession.on_close", this.c.c(), a3).b();
            a2.a((c) null);
            a2.a((AdColonyInterstitial) null);
            a2.a((AdColonyAdView) null);
            a.a().l().c().remove(this.c.b());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (i2 == 0) {
            setRequestedOrientation(7);
        } else if (i2 != 1) {
            setRequestedOrientation(4);
        } else {
            setRequestedOrientation(6);
        }
        this.d = i2;
    }
}
