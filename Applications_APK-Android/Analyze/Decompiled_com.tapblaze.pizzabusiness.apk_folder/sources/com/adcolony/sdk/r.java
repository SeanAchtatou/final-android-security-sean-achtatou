package com.adcolony.sdk;

import com.adcolony.sdk.w;
import com.google.firebase.remoteconfig.RemoteConfigConstants;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.json.JSONArray;
import org.json.JSONObject;

class r {
    private LinkedList<Runnable> a = new LinkedList<>();
    private boolean b;

    r() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public boolean f(ab abVar) {
        JSONObject c = abVar.c();
        String b2 = u.b(c, "filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            int c2 = u.c(c, "offset");
            int c3 = u.c(c, "size");
            boolean d = u.d(c, "gunzip");
            String b3 = u.b(c, "output_filepath");
            ap apVar = new ap(new FileInputStream(b2), c2, c3);
            InputStream gZIPInputStream = d ? new GZIPInputStream(apVar, 1024) : apVar;
            if (b3.equals("")) {
                StringBuilder sb = new StringBuilder(gZIPInputStream.available());
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = gZIPInputStream.read(bArr, 0, 1024);
                    if (read < 0) {
                        break;
                    }
                    sb.append(new String(bArr, 0, read, "ISO-8859-1"));
                }
                u.b(a2, "size", sb.length());
                u.a(a2, "data", sb.toString());
            } else {
                FileOutputStream fileOutputStream = new FileOutputStream(b3);
                byte[] bArr2 = new byte[1024];
                int i = 0;
                while (true) {
                    int read2 = gZIPInputStream.read(bArr2, 0, 1024);
                    if (read2 < 0) {
                        break;
                    }
                    fileOutputStream.write(bArr2, 0, read2);
                    i += read2;
                }
                fileOutputStream.close();
                u.b(a2, "size", i);
            }
            gZIPInputStream.close();
            u.b(a2, "success", true);
            abVar.a(a2).b();
            return true;
        } catch (IOException unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        } catch (OutOfMemoryError unused2) {
            new w.a().a("Out of memory error - disabling AdColony.").a(w.g);
            a.a().a(true);
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00fd, code lost:
        new com.adcolony.sdk.w.a().a("Out of memory error - disabling AdColony.").a(com.adcolony.sdk.w.g);
        com.adcolony.sdk.a.a().a(true);
        com.adcolony.sdk.u.b(r5, "success", false);
        r0.a(r5).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0120, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0121, code lost:
        r2 = false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[ExcHandler: OutOfMemoryError (unused java.lang.OutOfMemoryError), SYNTHETIC, Splitter:B:1:0x0029] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g(com.adcolony.sdk.ab r20) {
        /*
            r19 = this;
            r0 = r20
            java.lang.String r1 = "success"
            org.json.JSONObject r2 = r20.c()
            java.lang.String r3 = "filepath"
            java.lang.String r3 = com.adcolony.sdk.u.b(r2, r3)
            java.lang.String r4 = "bundle_path"
            java.lang.String r4 = com.adcolony.sdk.u.b(r2, r4)
            java.lang.String r5 = "bundle_filenames"
            org.json.JSONArray r2 = com.adcolony.sdk.u.g(r2, r5)
            com.adcolony.sdk.j r5 = com.adcolony.sdk.a.a()
            com.adcolony.sdk.ao r5 = r5.o()
            r5.b()
            org.json.JSONObject r5 = com.adcolony.sdk.u.a()
            java.io.File r8 = new java.io.File     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r8.<init>(r4)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            java.io.RandomAccessFile r9 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            java.lang.String r10 = "r"
            r9.<init>(r8, r10)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r10 = 32
            byte[] r10 = new byte[r10]     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r9.readInt()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            int r11 = r9.readInt()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            org.json.JSONArray r12 = new org.json.JSONArray     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r12.<init>()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r13 = 1024(0x400, float:1.435E-42)
            byte[] r14 = new byte[r13]     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r15 = 0
        L_0x004a:
            if (r15 >= r11) goto L_0x00e4
            r16 = 8
            int r17 = r15 * 44
            int r6 = r16 + r17
            r17 = r8
            long r7 = (long) r6     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r9.seek(r7)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r9.read(r10)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            java.lang.String r6 = new java.lang.String     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r6.<init>(r10)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r9.readInt()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            int r6 = r9.readInt()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            int r7 = r9.readInt()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r12.put(r7)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00ba }
            r8.<init>()     // Catch:{ JSONException -> 0x00ba }
            r8.append(r3)     // Catch:{ JSONException -> 0x00ba }
            java.lang.Object r13 = r2.get(r15)     // Catch:{ JSONException -> 0x00ba }
            r8.append(r13)     // Catch:{ JSONException -> 0x00ba }
            java.lang.String r8 = r8.toString()     // Catch:{ JSONException -> 0x00ba }
            r18 = r2
            r13 = r3
            long r2 = (long) r6
            r9.seek(r2)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r2.<init>(r8)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            int r3 = r7 / 1024
            int r7 = r7 % 1024
            r6 = 0
        L_0x0092:
            if (r6 >= r3) goto L_0x00a4
            r16 = r3
            r3 = 1024(0x400, float:1.435E-42)
            r8 = 0
            r9.read(r14, r8, r3)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r2.write(r14, r8, r3)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            int r6 = r6 + 1
            r3 = r16
            goto L_0x0092
        L_0x00a4:
            r3 = 1024(0x400, float:1.435E-42)
            r8 = 0
            r9.read(r14, r8, r7)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r2.write(r14, r8, r7)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r2.close()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            int r15 = r15 + 1
            r3 = r13
            r8 = r17
            r2 = r18
            r13 = 1024(0x400, float:1.435E-42)
            goto L_0x004a
        L_0x00ba:
            com.adcolony.sdk.w$a r2 = new com.adcolony.sdk.w$a     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r2.<init>()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            java.lang.String r3 = "Could extract file name at index "
            com.adcolony.sdk.w$a r2 = r2.a(r3)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            com.adcolony.sdk.w$a r2 = r2.a(r15)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            java.lang.String r3 = " unpacking ad unit bundle at "
            com.adcolony.sdk.w$a r2 = r2.a(r3)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            com.adcolony.sdk.w$a r2 = r2.a(r4)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            com.adcolony.sdk.w r3 = com.adcolony.sdk.w.g     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r2.a(r3)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r2 = 0
            com.adcolony.sdk.u.b(r5, r1, r2)     // Catch:{ IOException -> 0x0122, OutOfMemoryError -> 0x00fd }
            com.adcolony.sdk.ab r3 = r0.a(r5)     // Catch:{ IOException -> 0x0122, OutOfMemoryError -> 0x00fd }
            r3.b()     // Catch:{ IOException -> 0x0122, OutOfMemoryError -> 0x00fd }
            return r2
        L_0x00e4:
            r17 = r8
            r9.close()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r17.delete()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r2 = 1
            com.adcolony.sdk.u.b(r5, r1, r2)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            java.lang.String r3 = "file_sizes"
            com.adcolony.sdk.u.a(r5, r3, r12)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            com.adcolony.sdk.ab r3 = r0.a(r5)     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            r3.b()     // Catch:{ IOException -> 0x0121, OutOfMemoryError -> 0x00fd }
            return r2
        L_0x00fd:
            com.adcolony.sdk.w$a r2 = new com.adcolony.sdk.w$a
            r2.<init>()
            java.lang.String r3 = "Out of memory error - disabling AdColony."
            com.adcolony.sdk.w$a r2 = r2.a(r3)
            com.adcolony.sdk.w r3 = com.adcolony.sdk.w.g
            r2.a(r3)
            com.adcolony.sdk.j r2 = com.adcolony.sdk.a.a()
            r3 = 1
            r2.a(r3)
            r2 = 0
            com.adcolony.sdk.u.b(r5, r1, r2)
            com.adcolony.sdk.ab r0 = r0.a(r5)
            r0.b()
            return r2
        L_0x0121:
            r2 = 0
        L_0x0122:
            com.adcolony.sdk.w$a r3 = new com.adcolony.sdk.w$a
            r3.<init>()
            java.lang.String r6 = "Failed to find or open ad unit bundle at path: "
            com.adcolony.sdk.w$a r3 = r3.a(r6)
            com.adcolony.sdk.w$a r3 = r3.a(r4)
            com.adcolony.sdk.w r4 = com.adcolony.sdk.w.h
            r3.a(r4)
            com.adcolony.sdk.u.b(r5, r1, r2)
            com.adcolony.sdk.ab r0 = r0.a(r5)
            r0.b()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.r.g(com.adcolony.sdk.ab):boolean");
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a.a("FileSystem.save", new ad() {
            public void a(final ab abVar) {
                r.this.a(new Runnable() {
                    public void run() {
                        r.this.a(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.delete", new ad() {
            public void a(final ab abVar) {
                r.this.a(new Runnable() {
                    public void run() {
                        r.this.a(abVar, new File(u.b(abVar.c(), "filepath")));
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.listing", new ad() {
            public void a(final ab abVar) {
                r.this.a(new Runnable() {
                    public void run() {
                        r.this.b(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.load", new ad() {
            public void a(final ab abVar) {
                r.this.a(new Runnable() {
                    public void run() {
                        r.this.c(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.rename", new ad() {
            public void a(final ab abVar) {
                r.this.a(new Runnable() {
                    public void run() {
                        r.this.d(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.exists", new ad() {
            public void a(final ab abVar) {
                r.this.a(new Runnable() {
                    public void run() {
                        r.this.e(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.extract", new ad() {
            public void a(final ab abVar) {
                r.this.a(new Runnable() {
                    public void run() {
                        boolean unused = r.this.f(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.unpack_bundle", new ad() {
            public void a(final ab abVar) {
                r.this.a(new Runnable() {
                    public void run() {
                        boolean unused = r.this.g(abVar);
                        r.this.b();
                    }
                });
            }
        });
        a.a("FileSystem.create_directory", new ad() {
            public void a(final ab abVar) {
                r.this.a(new Runnable() {
                    public void run() {
                        boolean unused = r.this.h(abVar);
                        r.this.b();
                    }
                });
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean a(ab abVar) {
        JSONObject c = abVar.c();
        String b2 = u.b(c, "filepath");
        String b3 = u.b(c, "data");
        String b4 = u.b(c, "encoding");
        boolean z = b4 != null && b4.equals("utf8");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            a(b2, b3, z);
            u.b(a2, "success", true);
            abVar.a(a2).b();
            return true;
        } catch (IOException unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, boolean z) throws IOException {
        BufferedWriter bufferedWriter = z ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(str), "UTF-8")) : new BufferedWriter(new OutputStreamWriter(new FileOutputStream(str)));
        bufferedWriter.write(str2);
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean a(ab abVar, File file) {
        a.a().o().b();
        JSONObject a2 = u.a();
        if (a(file)) {
            u.b(a2, "success", true);
            abVar.a(a2).b();
            return true;
        }
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(File file) {
        try {
            if (!file.isDirectory()) {
                return file.delete();
            }
            if (file.list().length == 0) {
                return file.delete();
            }
            String[] list = file.list();
            if (list.length > 0) {
                return a(new File(file, list[0]));
            }
            if (file.list().length == 0) {
                return file.delete();
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean b(ab abVar) {
        String b2 = u.b(abVar.c(), "filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        String[] list = new File(b2).list();
        if (list != null) {
            JSONArray b3 = u.b();
            for (String str : list) {
                JSONObject a3 = u.a();
                u.a(a3, "filename", str);
                if (new File(b2 + str).isDirectory()) {
                    u.b(a3, "is_folder", true);
                } else {
                    u.b(a3, "is_folder", false);
                }
                u.a(b3, a3);
            }
            u.b(a2, "success", true);
            u.a(a2, RemoteConfigConstants.ResponseFieldKey.ENTRIES, b3);
            abVar.a(a2).b();
            return true;
        }
        u.b(a2, "success", false);
        abVar.a(a2).b();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public String c(ab abVar) {
        JSONObject c = abVar.c();
        String b2 = u.b(c, "filepath");
        String b3 = u.b(c, "encoding");
        boolean z = b3 != null && b3.equals("utf8");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            StringBuilder a3 = a(b2, z);
            u.b(a2, "success", true);
            u.a(a2, "data", a3.toString());
            abVar.a(a2).b();
            return a3.toString();
        } catch (IOException unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public StringBuilder a(String str, boolean z) throws IOException {
        BufferedReader bufferedReader;
        File file = new File(str);
        StringBuilder sb = new StringBuilder((int) file.length());
        if (z) {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), "UTF-8"));
        } else {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath())));
        }
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
                sb.append("\n");
            } else {
                bufferedReader.close();
                return sb;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public List<String> b(String str, boolean z) throws IOException {
        BufferedReader bufferedReader;
        File file = new File(str);
        file.length();
        ArrayList arrayList = new ArrayList();
        if (z) {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), "UTF-8"));
        } else {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath())));
        }
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                arrayList.add(readLine);
            } else {
                bufferedReader.close();
                return arrayList;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean d(ab abVar) {
        JSONObject c = abVar.c();
        String b2 = u.b(c, "filepath");
        String b3 = u.b(c, "new_filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            if (new File(b2).renameTo(new File(b3))) {
                u.b(a2, "success", true);
                abVar.a(a2).b();
                return true;
            }
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        } catch (Exception unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean e(ab abVar) {
        String b2 = u.b(abVar.c(), "filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            boolean a3 = a(b2);
            u.b(a2, IronSourceConstants.EVENTS_RESULT, a3);
            u.b(a2, "success", true);
            abVar.a(a2).b();
            return a3;
        } catch (Exception e) {
            u.b(a2, IronSourceConstants.EVENTS_RESULT, false);
            u.b(a2, "success", false);
            abVar.a(a2).b();
            e.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) throws Exception {
        return new File(str).exists();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean
     arg types: [org.json.JSONObject, java.lang.String, int]
     candidates:
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, int):boolean
      com.adcolony.sdk.u.b(org.json.JSONObject, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public boolean h(ab abVar) {
        String b2 = u.b(abVar.c(), "filepath");
        a.a().o().b();
        JSONObject a2 = u.a();
        try {
            if (new File(b2).mkdir()) {
                u.b(a2, "success", true);
                abVar.a(a2).b();
                return true;
            }
            u.b(a2, "success", false);
            return false;
        } catch (Exception unused) {
            u.b(a2, "success", false);
            abVar.a(a2).b();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Runnable runnable) {
        if (!this.a.isEmpty() || this.b) {
            this.a.push(runnable);
            return;
        }
        this.b = true;
        runnable.run();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.b = false;
        if (!this.a.isEmpty()) {
            this.b = true;
            this.a.removeLast().run();
        }
    }
}
