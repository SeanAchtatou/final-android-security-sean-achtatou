package org.ormma.view;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.JsResult;
import android.webkit.URLUtil;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.widget.VideoView;
import com.google.android.gms.drive.DriveFile;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import org.ormma.controller.OrmmaController;
import org.ormma.controller.OrmmaUtilityController;
import org.ormma.controller.util.OrmmaPlayer;
import org.ormma.controller.util.OrmmaPlayerListener;
import org.ormma.controller.util.OrmmaUtils;

public class OrmmaView extends WebView implements ViewTreeObserver.OnGlobalLayoutListener {
    public static final String ACTION_KEY = "action";
    private static final String AD_PATH = "AD_PATH";
    protected static final int BACKGROUND_ID = 101;
    private static final String CURRENT_FILE = "_ormma_current";
    public static final String DIMENSIONS = "expand_dimensions";
    private static final String ERROR_ACTION = "action";
    private static final String ERROR_MESSAGE = "message";
    private static final String EXPAND_PROPERTIES = "expand_properties";
    public static final String EXPAND_URL = "expand_url";
    private static final String LOG_TAG = "OrmmaView";
    private static final int MESSAGE_CLOSE = 1001;
    private static final int MESSAGE_EXPAND = 1004;
    private static final int MESSAGE_HIDE = 1002;
    private static final int MESSAGE_OPEN = 1006;
    private static final int MESSAGE_PLAY_AUDIO = 1008;
    private static final int MESSAGE_PLAY_VIDEO = 1007;
    private static final int MESSAGE_RAISE_ERROR = 1009;
    private static final int MESSAGE_RESIZE = 1000;
    private static final int MESSAGE_SEND_EXPAND_CLOSE = 1005;
    private static final int MESSAGE_SHOW = 1003;
    public static final int ORMMA_ID = 102;
    protected static final int PLACEHOLDER_ID = 100;
    public static final String PLAYER_PROPERTIES = "player_properties";
    private static final String RESIZE_HEIGHT = "resize_height";
    private static final String RESIZE_WIDTH = "resize_width";
    private static final String TAG = "OrmmaView";
    private static int[] attrs = {16843039, 16843040};
    /* access modifiers changed from: private */
    public static String mBridgeScriptPath;
    /* access modifiers changed from: private */
    public static String mScriptPath;
    private static OrmmaPlayer player;
    private boolean bGotLayoutParams;
    private boolean bKeyboardOut;
    /* access modifiers changed from: private */
    public boolean bPageFinished;
    private int mContentViewHeight;
    /* access modifiers changed from: private */
    public int mDefaultHeight;
    /* access modifiers changed from: private */
    public int mDefaultWidth;
    /* access modifiers changed from: private */
    public float mDensity;
    private Handler mHandler = new Handler() {
        private static /* synthetic */ int[] $SWITCH_TABLE$org$ormma$view$OrmmaView$ViewState;

        static /* synthetic */ int[] $SWITCH_TABLE$org$ormma$view$OrmmaView$ViewState() {
            int[] iArr = $SWITCH_TABLE$org$ormma$view$OrmmaView$ViewState;
            if (iArr == null) {
                iArr = new int[ViewState.values().length];
                try {
                    iArr[ViewState.DEFAULT.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[ViewState.EXPANDED.ordinal()] = 3;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[ViewState.HIDDEN.ordinal()] = 4;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[ViewState.LEFT_BEHIND.ordinal()] = 5;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[ViewState.OPENED.ordinal()] = 6;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[ViewState.RESIZED.ordinal()] = 2;
                } catch (NoSuchFieldError e6) {
                }
                $SWITCH_TABLE$org$ormma$view$OrmmaView$ViewState = iArr;
            }
            return iArr;
        }

        public void handleMessage(Message msg) {
            Bundle data = msg.getData();
            switch (msg.what) {
                case 1000:
                    OrmmaView.this.mViewState = ViewState.RESIZED;
                    ViewGroup.LayoutParams lp = OrmmaView.this.getLayoutParams();
                    lp.height = data.getInt(OrmmaView.RESIZE_HEIGHT, lp.height);
                    lp.width = data.getInt(OrmmaView.RESIZE_WIDTH, lp.width);
                    OrmmaView.this.injectJavaScript("window.ormmaview.fireChangeEvent({ state: 'resized', size: { width: " + lp.width + ", " + "height: " + lp.height + "}});");
                    OrmmaView.this.requestLayout();
                    if (OrmmaView.this.mListener != null) {
                        OrmmaView.this.mListener.onResize();
                        break;
                    }
                    break;
                case 1001:
                    switch ($SWITCH_TABLE$org$ormma$view$OrmmaView$ViewState()[OrmmaView.this.mViewState.ordinal()]) {
                        case 2:
                            OrmmaView.this.closeResized();
                            break;
                        case 3:
                            OrmmaView.this.closeExpanded();
                            break;
                        default:
                            OrmmaView.this.closeDefault();
                            break;
                    }
                case 1002:
                    OrmmaView.this.setVisibility(4);
                    OrmmaView.this.injectJavaScript("window.ormmaview.fireChangeEvent({ state: 'hidden' });");
                    break;
                case OrmmaView.MESSAGE_SHOW /*1003*/:
                    OrmmaView.this.injectJavaScript("window.ormmaview.fireChangeEvent({ state: 'default' });");
                    OrmmaView.this.setVisibility(0);
                    break;
                case OrmmaView.MESSAGE_EXPAND /*1004*/:
                    OrmmaView.this.doExpand(data);
                    break;
                case OrmmaView.MESSAGE_SEND_EXPAND_CLOSE /*1005*/:
                    if (OrmmaView.this.mListener != null) {
                        OrmmaView.this.mListener.onExpandClose();
                        break;
                    }
                    break;
                case OrmmaView.MESSAGE_OPEN /*1006*/:
                    OrmmaView.this.mViewState = ViewState.LEFT_BEHIND;
                    break;
                case OrmmaView.MESSAGE_PLAY_VIDEO /*1007*/:
                    OrmmaView.this.playVideoImpl(data);
                    break;
                case OrmmaView.MESSAGE_PLAY_AUDIO /*1008*/:
                    OrmmaView.this.playAudioImpl(data);
                    break;
                case OrmmaView.MESSAGE_RAISE_ERROR /*1009*/:
                    String strMsg = data.getString(OrmmaView.ERROR_MESSAGE);
                    OrmmaView.this.injectJavaScript("window.ormmaview.fireErrorEvent(\"" + strMsg + "\", \"" + data.getString("action") + "\")");
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private int mIndex;
    private int mInitLayoutHeight;
    private int mInitLayoutWidth;
    /* access modifiers changed from: private */
    public OrmmaViewListener mListener;
    /* access modifiers changed from: private */
    public String mLocalFilePath;
    /* access modifiers changed from: private */
    public TimeOut mTimeOut;
    /* access modifiers changed from: private */
    public OrmmaUtilityController mUtilityController;
    /* access modifiers changed from: private */
    public ViewState mViewState = ViewState.DEFAULT;
    WebChromeClient mWebChromeClient = new WebChromeClient() {
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            Log.d("OrmmaView", "Message: " + message);
            return false;
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
            view.setId(OrmmaView.BACKGROUND_ID);
            view.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            ((FrameLayout) OrmmaView.this.getRootView().findViewById(16908290)).addView(view, new FrameLayout.LayoutParams(-1, -1));
            OrmmaView.this.setVisibility(4);
            if ((view instanceof ViewGroup) && ((ViewGroup) view).getChildCount() > 0 && (((ViewGroup) view).getChildAt(0) instanceof VideoView)) {
                final VideoView videoView = (VideoView) ((ViewGroup) view).getChildAt(0);
                videoView.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (keyCode == 4 && event.getAction() == 1) {
                            AnonymousClass3.this.onHideCustomView();
                            videoView.setOnKeyListener(null);
                        }
                        return true;
                    }
                });
                videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        AnonymousClass3.this.onHideCustomView();
                    }
                });
                videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        AnonymousClass3.this.onHideCustomView();
                        return true;
                    }
                });
                videoView.start();
            }
        }

        public void onHideCustomView() {
            FrameLayout background = (FrameLayout) OrmmaView.this.getRootView().findViewById(OrmmaView.BACKGROUND_ID);
            ((ViewGroup) background.getParent()).removeView(background);
            OrmmaView.this.setVisibility(0);
        }
    };
    WebViewClient mWebViewClient = new WebViewClient() {
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Log.d("OrmmaView", "error:" + description);
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        public void onPageFinished(WebView view, String url) {
            OrmmaView.this.mDefaultHeight = (int) (((float) OrmmaView.this.getHeight()) / OrmmaView.this.mDensity);
            OrmmaView.this.mDefaultWidth = (int) (((float) OrmmaView.this.getWidth()) / OrmmaView.this.mDensity);
            OrmmaView.this.mUtilityController.init(OrmmaView.this.mDensity);
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri = Uri.parse(url);
            try {
                if (OrmmaView.this.mListener != null && OrmmaView.this.isRegisteredProtocol(uri)) {
                    OrmmaView.this.mListener.handleRequest(url);
                    return true;
                } else if (url.startsWith("tel:")) {
                    Intent intent = new Intent("android.intent.action.DIAL", Uri.parse(url));
                    intent.addFlags(DriveFile.MODE_READ_ONLY);
                    OrmmaView.this.getContext().startActivity(intent);
                    return true;
                } else if (url.startsWith("mailto:")) {
                    Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(url));
                    intent2.addFlags(DriveFile.MODE_READ_ONLY);
                    OrmmaView.this.getContext().startActivity(intent2);
                    return true;
                } else {
                    Intent intent3 = new Intent();
                    intent3.setAction("android.intent.action.VIEW");
                    intent3.setData(uri);
                    intent3.addFlags(DriveFile.MODE_READ_ONLY);
                    OrmmaView.this.getContext().startActivity(intent3);
                    return true;
                }
            } catch (Exception e) {
                try {
                    Intent intent4 = new Intent();
                    intent4.setAction("android.intent.action.VIEW");
                    intent4.setData(uri);
                    intent4.addFlags(DriveFile.MODE_READ_ONLY);
                    OrmmaView.this.getContext().startActivity(intent4);
                    return true;
                } catch (Exception e2) {
                    return false;
                }
            }
        }

        public void onLoadResource(WebView view, String url) {
            Log.d("OrmmaView", "lr:" + url);
        }
    };
    private String mapAPIKey;
    private final HashSet<String> registeredProtocols = new HashSet<>();

    public enum ACTION {
        PLAY_AUDIO,
        PLAY_VIDEO
    }

    public static abstract class NewLocationReciever {
        public abstract void OnNewLocation(ViewState viewState);
    }

    public interface OrmmaViewListener {
        void handleRequest(String str);

        boolean onDefaultClose();

        boolean onEventFired();

        boolean onExpand();

        boolean onExpandClose();

        boolean onReady();

        boolean onResize();

        boolean onResizeClose();
    }

    public enum ViewState {
        DEFAULT,
        RESIZED,
        EXPANDED,
        HIDDEN,
        LEFT_BEHIND,
        OPENED
    }

    public static class SimpleOrmmaViewListener implements OrmmaViewListener {
        public boolean onReady() {
            return false;
        }

        public boolean onResize() {
            return false;
        }

        public boolean onExpand() {
            return false;
        }

        public boolean onExpandClose() {
            return false;
        }

        public boolean onResizeClose() {
            return false;
        }

        public boolean onDefaultClose() {
            return false;
        }

        public boolean onEventFired() {
            return false;
        }

        public void handleRequest(String url) {
        }
    }

    public OrmmaView(Context context, OrmmaViewListener listener) {
        super(context);
        setListener(listener);
        initialize();
    }

    public void setMapAPIKey(String key) {
        this.mapAPIKey = key;
    }

    public void setListener(OrmmaViewListener listener) {
        this.mListener = listener;
    }

    public void removeListener() {
        this.mListener = null;
    }

    public OrmmaView(Context context) {
        super(context);
        initialize();
    }

    public OrmmaView(Context context, String mapAPIKey2) {
        super(context);
        if (!(context instanceof MapActivity)) {
            throw new IllegalArgumentException("MapActivity context required");
        }
        this.mapAPIKey = mapAPIKey2;
        initialize();
    }

    public OrmmaView(Context context, String mapAPIKey2, OrmmaViewListener listener) {
        super(context);
        if (!(context instanceof MapActivity)) {
            throw new IllegalArgumentException("MapActivity context required");
        }
        this.mapAPIKey = mapAPIKey2;
        setListener(listener);
        initialize();
    }

    public void setMaxSize(int w, int h) {
        this.mUtilityController.setMaxSize(w, h);
    }

    public void registerProtocol(String protocol) {
        if (protocol != null) {
            this.registeredProtocols.add(protocol.toLowerCase());
        }
    }

    public void deregisterProtocol(String protocol) {
        if (protocol != null) {
            this.registeredProtocols.remove(protocol.toLowerCase());
        }
    }

    /* access modifiers changed from: private */
    public boolean isRegisteredProtocol(Uri uri) {
        String scheme = uri.getScheme();
        if (scheme == null) {
            return false;
        }
        Iterator<String> it = this.registeredProtocols.iterator();
        while (it.hasNext()) {
            if (it.next().equalsIgnoreCase(scheme)) {
                return true;
            }
        }
        return false;
    }

    public void injectJavaScript(String str) {
        if (str == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            super.evaluateJavascript(str, null);
        } else {
            super.loadUrl("javascript:" + str);
        }
    }

    public void loadUrl(String url, String dataToInject) {
        loadUrl(url, false, dataToInject);
    }

    public void loadUrl(String url) {
        loadUrl(url, false, null);
    }

    class TimeOut extends TimerTask {
        int mCount = 0;
        int mProgress = 0;

        TimeOut() {
        }

        public void run() {
            if (OrmmaView.this.getContext() instanceof Activity) {
                ((Activity) OrmmaView.this.getContext()).runOnUiThread(new Runnable() {
                    public void run() {
                        int progress = OrmmaView.this.getProgress();
                        if (progress == 100) {
                            TimeOut.this.cancel();
                        } else if (TimeOut.this.mProgress == progress) {
                            TimeOut.this.mCount++;
                            if (TimeOut.this.mCount == 3) {
                                try {
                                    OrmmaView.this.stopLoading();
                                } catch (Exception e) {
                                    Log.w("OrmmaView", "error in stopLoading");
                                    e.printStackTrace();
                                }
                                TimeOut.this.cancel();
                            }
                        }
                        TimeOut.this.mProgress = progress;
                    }
                });
            }
        }
    }

    public void clearView() {
        reset();
        super.clearView();
    }

    /* access modifiers changed from: private */
    public void reset() {
        if (this.mViewState == ViewState.EXPANDED) {
            closeExpanded();
        } else if (this.mViewState == ViewState.RESIZED) {
            closeResized();
        }
        invalidate();
        this.mUtilityController.deleteOldAds();
        this.mUtilityController.stopAllListeners();
        resetLayout();
    }

    public void loadUrl(final String url, boolean dontLoad, final String dataToInject) {
        if (URLUtil.isValidUrl(url)) {
            if (!dontLoad) {
                new AsyncTask<Void, Void, String>() {
                    /* access modifiers changed from: protected */
                    public void onPreExecute() {
                        OrmmaView.this.reset();
                    }

                    /* access modifiers changed from: protected */
                    public String doInBackground(Void... params) {
                        InputStream is;
                        OrmmaView.this.bPageFinished = false;
                        try {
                            URL u = new URL(url);
                            String file = u.getFile();
                            if (url.startsWith("file:///android_asset/")) {
                                is = OrmmaView.this.getContext().getAssets().open(url.replace("file:///android_asset/", ""));
                            } else {
                                is = u.openStream();
                            }
                            if (OrmmaView.this.mTimeOut != null) {
                                OrmmaView.this.mTimeOut.cancel();
                            }
                            OrmmaView.this.mTimeOut = new TimeOut();
                            try {
                                OrmmaView.this.mLocalFilePath = OrmmaView.this.mUtilityController.writeToDiskWrap(is, OrmmaView.CURRENT_FILE, true, dataToInject, OrmmaView.mBridgeScriptPath, OrmmaView.mScriptPath);
                                String fileUrl = "file://" + OrmmaView.this.mLocalFilePath + OrmmaView.CURRENT_FILE;
                                new Timer().schedule(OrmmaView.this.mTimeOut, 2000, 2000);
                                if (dataToInject != null) {
                                    OrmmaView.this.injectJavaScript(dataToInject);
                                }
                                if (is != null) {
                                    try {
                                        is.close();
                                    } catch (Exception e) {
                                    }
                                }
                                return fileUrl;
                            } catch (IllegalStateException e2) {
                                e2.printStackTrace();
                                if (is != null) {
                                    try {
                                        is.close();
                                    } catch (Exception e3) {
                                    }
                                }
                                return null;
                            } catch (IOException e4) {
                                e4.printStackTrace();
                                if (is != null) {
                                    try {
                                        is.close();
                                    } catch (Exception e5) {
                                    }
                                }
                                return null;
                            } catch (Throwable th) {
                                if (is != null) {
                                    try {
                                        is.close();
                                    } catch (Exception e6) {
                                    }
                                }
                                throw th;
                            }
                        } catch (MalformedURLException e7) {
                        } catch (IOException e8) {
                            e8.printStackTrace();
                        }
                        return null;
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(String result) {
                        if (!TextUtils.isEmpty(result)) {
                            OrmmaView.super.loadUrl(result);
                        }
                    }
                }.execute(new Void[0]);
            }
            super.loadUrl(url);
        }
    }

    public OrmmaView(Context context, AttributeSet set) {
        super(context, set);
        initialize();
        TypedArray a = getContext().obtainStyledAttributes(set, attrs);
        int w = a.getDimensionPixelSize(0, -1);
        int h = a.getDimensionPixelSize(1, -1);
        if (w > 0 && h > 0) {
            this.mUtilityController.setMaxSize(w, h);
        }
        a.recycle();
    }

    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(ev);
    }

    private FrameLayout changeContentArea(OrmmaController.Dimensions d) {
        FrameLayout contentView = (FrameLayout) getRootView().findViewById(16908290);
        ViewGroup parent = (ViewGroup) getParent();
        FrameLayout.LayoutParams fl = new FrameLayout.LayoutParams(d.width, d.height);
        fl.topMargin = d.x;
        fl.leftMargin = d.y;
        int count = parent.getChildCount();
        int index = 0;
        while (index < count && parent.getChildAt(index) != this) {
            index++;
        }
        this.mIndex = index;
        FrameLayout placeHolder = new FrameLayout(getContext());
        placeHolder.setId(100);
        parent.addView(placeHolder, index, new ViewGroup.LayoutParams(getWidth(), getHeight()));
        parent.removeView(this);
        FrameLayout backGround = new FrameLayout(getContext());
        backGround.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                Log.i("ORMMA", "background touch called");
                return true;
            }
        });
        FrameLayout.LayoutParams bgfl = new FrameLayout.LayoutParams(-1, -1);
        backGround.setId(BACKGROUND_ID);
        backGround.setPadding(d.x, d.y, 0, 0);
        backGround.addView(this, fl);
        contentView.addView(backGround, bgfl);
        return backGround;
    }

    /* access modifiers changed from: private */
    public void doExpand(Bundle data) {
        OrmmaController.Dimensions d = (OrmmaController.Dimensions) data.getParcelable(DIMENSIONS);
        String url = data.getString(EXPAND_URL);
        OrmmaController.Properties p = (OrmmaController.Properties) data.getParcelable(EXPAND_PROPERTIES);
        if (URLUtil.isValidUrl(url)) {
            loadUrl(url);
        }
        FrameLayout backGround = changeContentArea(d);
        if (p.useBackground) {
            backGround.setBackgroundColor(p.backgroundColor | (((int) (p.backgroundOpacity * 255.0f)) * DriveFile.MODE_READ_ONLY));
        }
        String injection = "window.ormmaview.fireChangeEvent({ state: 'expanded', size: { width: " + ((int) (((float) d.width) / this.mDensity)) + ", " + "height: " + ((int) (((float) d.height) / this.mDensity)) + "}" + " });";
        Log.d("OrmmaView", "doExpand: injection: " + injection);
        injectJavaScript(injection);
        if (this.mListener != null) {
            this.mListener.onExpand();
        }
        this.mViewState = ViewState.EXPANDED;
    }

    /* access modifiers changed from: private */
    public void closeResized() {
        if (this.mListener != null) {
            this.mListener.onResizeClose();
        }
        this.mViewState = ViewState.DEFAULT;
        String injection = "window.ormmaview.fireChangeEvent({ state: 'default', size: { width: " + this.mDefaultWidth + ", " + "height: " + this.mDefaultHeight + "}" + "});";
        Log.d("OrmmaView", "closeResized: injection: " + injection);
        injectJavaScript(injection);
        resetLayout();
    }

    private void initialize() {
        setScrollContainer(false);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        setBackgroundColor(0);
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        this.mDensity = metrics.density;
        this.bPageFinished = false;
        getSettings().setJavaScriptEnabled(true);
        getSettings().setPluginState(WebSettings.PluginState.ON);
        this.mUtilityController = new OrmmaUtilityController(this, getContext());
        addJavascriptInterface(this.mUtilityController, "ORMMAUtilityControllerBridge");
        setWebViewClient(this.mWebViewClient);
        setWebChromeClient(this.mWebChromeClient);
        setScriptPath();
        this.mContentViewHeight = getContentViewHeight();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    public void addJavascriptObject(Object obj, String name) {
        addJavascriptInterface(obj, name);
    }

    private int getContentViewHeight() {
        View contentView = getRootView().findViewById(16908290);
        if (contentView != null) {
            return contentView.getHeight();
        }
        return -1;
    }

    private synchronized void setScriptPath() {
        if (mScriptPath == null) {
            mScriptPath = this.mUtilityController.copyTextFromJarIntoAssetDir("/js/ormma.js", "js/ormma.js");
        }
        if (mBridgeScriptPath == null) {
            mBridgeScriptPath = this.mUtilityController.copyTextFromJarIntoAssetDir("/js/ormma_bridge.js", "js/ormma_bridge.js");
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void closeExpanded() {
        resetContents();
        String injection = "window.ormmaview.fireChangeEvent({ state: 'default', size: { width: " + this.mDefaultWidth + ", " + "height: " + this.mDefaultHeight + "}" + "});";
        Log.d("OrmmaView", "closeExpanded: injection: " + injection);
        injectJavaScript(injection);
        this.mViewState = ViewState.DEFAULT;
        this.mHandler.sendEmptyMessage(MESSAGE_SEND_EXPAND_CLOSE);
        setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void closeOpened(View openedFrame) {
        ((ViewGroup) ((Activity) getContext()).getWindow().getDecorView()).removeView(openedFrame);
        this.mViewState = ViewState.DEFAULT;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public void closeDefault() {
        if (this.mListener == null) {
            ((ViewGroup) getParent()).removeView(this);
        } else if (!this.mListener.onDefaultClose()) {
            ((ViewGroup) getParent()).removeView(this);
        }
    }

    public String getState() {
        return this.mViewState.toString().toLowerCase();
    }

    public void resize(int width, int height) {
        Message msg = this.mHandler.obtainMessage(1000);
        Bundle data = new Bundle();
        data.putInt(RESIZE_WIDTH, width);
        data.putInt(RESIZE_HEIGHT, height);
        msg.setData(data);
        this.mHandler.sendMessage(msg);
    }

    public void close() {
        this.mHandler.sendEmptyMessage(1001);
    }

    public void hide() {
        this.mHandler.sendEmptyMessage(1002);
    }

    public void show() {
        this.mHandler.sendEmptyMessage(MESSAGE_SHOW);
    }

    public ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) getContext().getSystemService("connectivity");
    }

    public void dump() {
    }

    public void expand(OrmmaController.Dimensions dimensions, String URL, OrmmaController.Properties properties) {
        Message msg = this.mHandler.obtainMessage(MESSAGE_EXPAND);
        Bundle data = new Bundle();
        data.putParcelable(DIMENSIONS, dimensions);
        data.putString(EXPAND_URL, URL);
        data.putParcelable(EXPAND_PROPERTIES, properties);
        msg.setData(data);
        this.mHandler.sendMessage(msg);
    }

    public void open(String url, boolean back, boolean forward, boolean refresh) {
        Intent i = new Intent(getContext(), Browser.class);
        Log.d("OrmmaView", "open:" + url);
        i.putExtra(Browser.URL_EXTRA, url);
        i.putExtra(Browser.SHOW_BACK_EXTRA, back);
        i.putExtra(Browser.SHOW_FORWARD_EXTRA, forward);
        i.putExtra(Browser.SHOW_REFRESH_EXTRA, refresh);
        i.addFlags(DriveFile.MODE_READ_ONLY);
        getContext().startActivity(i);
    }

    public void openMap(String POI, boolean fullscreen) {
        Log.d("OrmmaView", "Opening Map Url " + POI);
        String POI2 = OrmmaUtils.convert(POI.trim());
        if (fullscreen) {
            try {
                Intent mapIntent = new Intent("android.intent.action.VIEW", Uri.parse(POI2));
                mapIntent.setFlags(DriveFile.MODE_READ_ONLY);
                getContext().startActivity(mapIntent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        } else if (this.mapAPIKey != null) {
            try {
                new MapView(getContext(), this.mapAPIKey).setBuiltInZoomControls(true);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            Toast.makeText(getContext(), "Error: no Google Maps API Key provided for embedded map", 1).show();
        }
    }

    public void playAudioImpl(Bundle data) {
        String url = data.getString(EXPAND_URL);
        OrmmaPlayer audioPlayer = getPlayer();
        audioPlayer.setPlayData((OrmmaController.PlayerProperties) data.getParcelable(PLAYER_PROPERTIES), url);
        audioPlayer.setLayoutParams(new ViewGroup.LayoutParams(1, 1));
        ((ViewGroup) getParent()).addView(audioPlayer);
        audioPlayer.playAudio();
    }

    public void playAudio(String url, boolean autoPlay, boolean controls, boolean loop, boolean position, String startStyle, String stopStyle) {
        OrmmaController.PlayerProperties properties = new OrmmaController.PlayerProperties();
        properties.setProperties(false, autoPlay, controls, position, loop, startStyle, stopStyle);
        Bundle data = new Bundle();
        data.putString("action", ACTION.PLAY_AUDIO.toString());
        data.putString(EXPAND_URL, url);
        data.putParcelable(PLAYER_PROPERTIES, properties);
        if (properties.isFullScreen()) {
            try {
                Intent intent = new Intent(getContext(), OrmmaActionHandler.class);
                intent.putExtras(data);
                getContext().startActivity(intent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Message msg = this.mHandler.obtainMessage(MESSAGE_PLAY_AUDIO);
            msg.setData(data);
            this.mHandler.sendMessage(msg);
        }
    }

    public void playVideoImpl(Bundle data) {
        OrmmaController.Dimensions d = (OrmmaController.Dimensions) data.getParcelable(DIMENSIONS);
        String url = data.getString(EXPAND_URL);
        OrmmaPlayer videoPlayer = getPlayer();
        videoPlayer.setPlayData((OrmmaController.PlayerProperties) data.getParcelable(PLAYER_PROPERTIES), url);
        FrameLayout.LayoutParams fl = new FrameLayout.LayoutParams(d.width, d.height);
        fl.topMargin = d.x;
        fl.leftMargin = d.y;
        videoPlayer.setLayoutParams(fl);
        FrameLayout backGround = new FrameLayout(getContext());
        backGround.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                Log.i("ORMMA", "background touch called");
                return true;
            }
        });
        backGround.setId(BACKGROUND_ID);
        backGround.setPadding(d.x, d.y, 0, 0);
        ((FrameLayout) getRootView().findViewById(16908290)).addView(backGround, new FrameLayout.LayoutParams(-1, -1));
        backGround.addView(videoPlayer);
        setVisibility(4);
        videoPlayer.setListener(new OrmmaPlayerListener() {
            public void onPrepared() {
            }

            public void onError() {
                onComplete();
            }

            public void onComplete() {
                FrameLayout background = (FrameLayout) OrmmaView.this.getRootView().findViewById(OrmmaView.BACKGROUND_ID);
                ((ViewGroup) background.getParent()).removeView(background);
                OrmmaView.this.setVisibility(0);
            }
        });
        videoPlayer.playVideo();
    }

    public void playVideo(String url, boolean audioMuted, boolean autoPlay, boolean controls, boolean loop, OrmmaController.Dimensions d, String startStyle, String stopStyle) {
        Message msg = this.mHandler.obtainMessage(MESSAGE_PLAY_VIDEO);
        OrmmaController.PlayerProperties properties = new OrmmaController.PlayerProperties();
        properties.setProperties(audioMuted, autoPlay, controls, false, loop, startStyle, stopStyle);
        Bundle data = new Bundle();
        data.putString(EXPAND_URL, url);
        data.putString("action", ACTION.PLAY_VIDEO.toString());
        data.putParcelable(PLAYER_PROPERTIES, properties);
        if (d != null) {
            data.putParcelable(DIMENSIONS, d);
        }
        if (properties.isFullScreen()) {
            try {
                Intent intent = new Intent(getContext(), OrmmaActionHandler.class);
                intent.putExtras(data);
                getContext().startActivity(intent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        } else if (d != null) {
            msg.setData(data);
            this.mHandler.sendMessage(msg);
        }
    }

    public void resetContents() {
        FrameLayout placeHolder = (FrameLayout) getRootView().findViewById(100);
        FrameLayout background = (FrameLayout) getRootView().findViewById(BACKGROUND_ID);
        ViewGroup parent = (ViewGroup) placeHolder.getParent();
        background.removeView(this);
        ((FrameLayout) getRootView().findViewById(16908290)).removeView(background);
        resetLayout();
        parent.addView(this, this.mIndex);
        parent.removeView(placeHolder);
        parent.invalidate();
    }

    private void resetLayout() {
        ViewGroup.LayoutParams lp = getLayoutParams();
        if (this.bGotLayoutParams) {
            lp.height = this.mInitLayoutHeight;
            lp.width = this.mInitLayoutWidth;
        }
        setVisibility(0);
        requestLayout();
    }

    public boolean isPageFinished() {
        return this.bPageFinished;
    }

    public void onGlobalLayout() {
        boolean state = this.bKeyboardOut;
        if (!this.bKeyboardOut && this.mContentViewHeight >= 0 && getContentViewHeight() >= 0 && this.mContentViewHeight != getContentViewHeight()) {
            state = true;
            injectJavaScript("window.ormmaview.fireChangeEvent({ keyboardState: true});");
        }
        if (this.bKeyboardOut && this.mContentViewHeight >= 0 && getContentViewHeight() >= 0 && this.mContentViewHeight == getContentViewHeight()) {
            state = false;
            injectJavaScript("window.ormmaview.fireChangeEvent({ keyboardState: false});");
        }
        if (this.mContentViewHeight < 0) {
            this.mContentViewHeight = getContentViewHeight();
        }
        this.bKeyboardOut = state;
    }

    public String getSize() {
        return "{ width: " + ((int) (((float) getWidth()) / this.mDensity)) + ", " + "height: " + ((int) (((float) getHeight()) / this.mDensity)) + "}";
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (!this.bGotLayoutParams) {
            ViewGroup.LayoutParams lp = getLayoutParams();
            this.mInitLayoutHeight = lp.height;
            this.mInitLayoutWidth = lp.width;
            this.bGotLayoutParams = true;
        }
        super.onAttachedToWindow();
    }

    public WebBackForwardList saveState(Bundle outState) {
        outState.putString(AD_PATH, this.mLocalFilePath);
        return null;
    }

    public WebBackForwardList restoreState(Bundle savedInstanceState) {
        this.mLocalFilePath = savedInstanceState.getString(AD_PATH);
        super.loadUrl("file://" + this.mLocalFilePath + File.separator + CURRENT_FILE);
        return null;
    }

    class ScrollEater extends GestureDetector.SimpleOnGestureListener {
        ScrollEater() {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return true;
        }
    }

    public void raiseError(String strMsg, String action) {
        Message msg = this.mHandler.obtainMessage(MESSAGE_RAISE_ERROR);
        Bundle data = new Bundle();
        data.putString(ERROR_MESSAGE, strMsg);
        data.putString("action", action);
        msg.setData(data);
        this.mHandler.sendMessage(msg);
    }

    public boolean isExpanded() {
        return this.mViewState == ViewState.EXPANDED;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.mUtilityController.stopAllListeners();
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: package-private */
    public OrmmaPlayer getPlayer() {
        if (player != null) {
            player.releasePlayer();
        }
        player = new OrmmaPlayer(getContext());
        return player;
    }
}
