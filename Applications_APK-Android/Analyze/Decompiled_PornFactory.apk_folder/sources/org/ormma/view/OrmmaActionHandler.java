package org.ormma.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import java.util.HashMap;
import java.util.Map;
import org.ormma.controller.OrmmaController;
import org.ormma.controller.util.OrmmaPlayer;
import org.ormma.controller.util.OrmmaPlayerListener;
import org.ormma.controller.util.OrmmaUtils;
import org.ormma.view.OrmmaView;

public class OrmmaActionHandler extends Activity {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$ormma$view$OrmmaView$ACTION;
    private HashMap<OrmmaView.ACTION, Object> actionData = new HashMap<>();
    private RelativeLayout layout;

    static /* synthetic */ int[] $SWITCH_TABLE$org$ormma$view$OrmmaView$ACTION() {
        int[] iArr = $SWITCH_TABLE$org$ormma$view$OrmmaView$ACTION;
        if (iArr == null) {
            iArr = new int[OrmmaView.ACTION.values().length];
            try {
                iArr[OrmmaView.ACTION.PLAY_AUDIO.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrmmaView.ACTION.PLAY_VIDEO.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$org$ormma$view$OrmmaView$ACTION = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle data = getIntent().getExtras();
        this.layout = new RelativeLayout(this);
        this.layout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setContentView(this.layout);
        doAction(data);
    }

    private void doAction(Bundle data) {
        String actionData2 = data.getString(OrmmaView.ACTION_KEY);
        if (actionData2 != null) {
            OrmmaView.ACTION actionType = OrmmaView.ACTION.valueOf(actionData2);
            switch ($SWITCH_TABLE$org$ormma$view$OrmmaView$ACTION()[actionType.ordinal()]) {
                case 1:
                    initPlayer(data, actionType).playAudio();
                    return;
                case 2:
                    initPlayer(data, actionType).playVideo();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public OrmmaPlayer initPlayer(Bundle playData, OrmmaView.ACTION actionType) {
        RelativeLayout.LayoutParams lp;
        OrmmaController.Dimensions playDimensions = (OrmmaController.Dimensions) playData.getParcelable(OrmmaView.DIMENSIONS);
        OrmmaPlayer player = new OrmmaPlayer(this);
        player.setPlayData((OrmmaController.PlayerProperties) playData.getParcelable(OrmmaView.PLAYER_PROPERTIES), OrmmaUtils.getData(OrmmaView.EXPAND_URL, playData));
        if (playDimensions == null) {
            lp = new RelativeLayout.LayoutParams(-1, -1);
            lp.addRule(13);
        } else {
            lp = new RelativeLayout.LayoutParams(playDimensions.width, playDimensions.height);
            lp.topMargin = playDimensions.y;
            lp.leftMargin = playDimensions.x;
        }
        player.setLayoutParams(lp);
        this.layout.addView(player);
        this.actionData.put(actionType, player);
        setPlayerListener(player);
        return player;
    }

    private void setPlayerListener(OrmmaPlayer player) {
        player.setListener(new OrmmaPlayerListener() {
            public void onPrepared() {
            }

            public void onError() {
                OrmmaActionHandler.this.finish();
            }

            public void onComplete() {
                OrmmaActionHandler.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        for (Map.Entry<OrmmaView.ACTION, Object> entry : this.actionData.entrySet()) {
            switch ($SWITCH_TABLE$org$ormma$view$OrmmaView$ACTION()[((OrmmaView.ACTION) entry.getKey()).ordinal()]) {
                case 1:
                case 2:
                    ((OrmmaPlayer) entry.getValue()).releasePlayer();
                    break;
            }
        }
        super.onStop();
    }
}
