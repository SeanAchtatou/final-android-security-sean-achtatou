package org.ormma.controller.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.ormma.controller.OrmmaDisplayController;

public class OrmmaConfigurationBroadcastReceiver extends BroadcastReceiver {
    private int mLastOrientation = this.mOrmmaDisplayController.getOrientation();
    private OrmmaDisplayController mOrmmaDisplayController;

    public OrmmaConfigurationBroadcastReceiver(OrmmaDisplayController ormmaDisplayController) {
        this.mOrmmaDisplayController = ormmaDisplayController;
    }

    public void onReceive(Context context, Intent intent) {
        int orientation;
        if (intent.getAction().equals("android.intent.action.CONFIGURATION_CHANGED") && (orientation = this.mOrmmaDisplayController.getOrientation()) != this.mLastOrientation) {
            this.mLastOrientation = orientation;
            this.mOrmmaDisplayController.onOrientationChanged(this.mLastOrientation);
        }
    }
}
