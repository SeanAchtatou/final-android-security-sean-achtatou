package org.ormma.controller.util;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import org.ormma.controller.OrmmaController;

public class OrmmaPlayer extends VideoView implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private static String LOG_TAG = "Ormma Player";
    private static String transientText = "Loading. Please Wait..";
    private AudioManager aManager = ((AudioManager) getContext().getSystemService("audio"));
    private String contentURL;
    private boolean isReleased;
    private OrmmaPlayerListener listener;
    private int mutedVolume;
    private OrmmaController.PlayerProperties playProperties;
    private RelativeLayout transientLayout;

    public OrmmaPlayer(Context context) {
        super(context);
    }

    public void setPlayData(OrmmaController.PlayerProperties properties, String url) {
        this.isReleased = false;
        this.playProperties = properties;
        this.contentURL = url;
    }

    public void playAudio() {
        loadContent();
    }

    /* access modifiers changed from: package-private */
    public void displayControl() {
        if (this.playProperties.showControl()) {
            MediaController ctrl = new MediaController(getContext());
            setMediaController(ctrl);
            ctrl.setAnchorView(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void loadContent() {
        this.contentURL = this.contentURL.trim();
        this.contentURL = OrmmaUtils.convert(this.contentURL);
        if (this.contentURL != null || this.listener == null) {
            setVideoURI(Uri.parse(this.contentURL));
            displayControl();
            startContent();
            return;
        }
        removeView();
        this.listener.onError();
    }

    /* access modifiers changed from: package-private */
    public void startContent() {
        setOnCompletionListener(this);
        setOnErrorListener(this);
        setOnPreparedListener(this);
        if (!this.playProperties.inline) {
            addTransientMessage();
        }
        if (this.playProperties.isAutoPlay()) {
            start();
        }
    }

    public void playVideo() {
        if (this.playProperties.doMute()) {
            this.mutedVolume = this.aManager.getStreamVolume(3);
            this.aManager.setStreamVolume(3, 0, 4);
        }
        loadContent();
    }

    /* access modifiers changed from: package-private */
    public void unMute() {
        this.aManager.setStreamVolume(3, this.mutedVolume, 4);
    }

    public void setListener(OrmmaPlayerListener listener2) {
        this.listener = listener2;
    }

    public void onCompletion(MediaPlayer mp) {
        if (this.playProperties.doLoop()) {
            start();
        } else if (this.playProperties.exitOnComplete() || this.playProperties.inline) {
            releasePlayer();
        }
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.i(LOG_TAG, "Player error : " + what);
        clearTransientMessage();
        removeView();
        if (this.listener == null) {
            return false;
        }
        this.listener.onError();
        return false;
    }

    public void onPrepared(MediaPlayer mp) {
        clearTransientMessage();
        if (this.listener != null) {
            this.listener.onPrepared();
        }
    }

    /* access modifiers changed from: package-private */
    public void removeView() {
        ViewGroup parent = (ViewGroup) getParent();
        if (parent != null) {
            parent.removeView(this);
        }
    }

    public void releasePlayer() {
        if (!this.isReleased) {
            this.isReleased = true;
            stopPlayback();
            removeView();
            if (this.playProperties != null && this.playProperties.doMute()) {
                unMute();
            }
            if (this.listener != null) {
                this.listener.onComplete();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void addTransientMessage() {
        if (!this.playProperties.inline) {
            this.transientLayout = new RelativeLayout(getContext());
            this.transientLayout.setLayoutParams(getLayoutParams());
            TextView transientView = new TextView(getContext());
            transientView.setText(transientText);
            transientView.setTextColor(-1);
            RelativeLayout.LayoutParams msgparams = new RelativeLayout.LayoutParams(-2, -2);
            msgparams.addRule(13);
            this.transientLayout.addView(transientView, msgparams);
            ((ViewGroup) getParent()).addView(this.transientLayout);
        }
    }

    /* access modifiers changed from: package-private */
    public void clearTransientMessage() {
        if (this.transientLayout != null) {
            ((ViewGroup) getParent()).removeView(this.transientLayout);
        }
    }
}
