package org.ormma.controller.util;

public interface OrmmaPlayerListener {
    void onComplete();

    void onError();

    void onPrepared();
}
