package org.ormma.controller;

import android.content.Context;
import android.content.IntentFilter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.URLUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.ormma.controller.OrmmaController;
import org.ormma.controller.util.OrmmaConfigurationBroadcastReceiver;
import org.ormma.view.OrmmaView;

public class OrmmaDisplayController extends OrmmaController {
    private static final String LOG_TAG = "OrmmaDisplayController";
    private boolean bMaxSizeSet = false;
    private OrmmaConfigurationBroadcastReceiver mBroadCastReceiver;
    private float mDensity;
    private int mMaxHeight = -1;
    private int mMaxWidth = -1;
    private WindowManager mWindowManager;

    public OrmmaDisplayController(OrmmaView adView, Context c) {
        super(adView, c);
        DisplayMetrics metrics = new DisplayMetrics();
        this.mWindowManager = (WindowManager) c.getSystemService("window");
        this.mWindowManager.getDefaultDisplay().getMetrics(metrics);
        this.mDensity = metrics.density;
    }

    @JavascriptInterface
    public void resize(int width, int height) {
        Log.d(LOG_TAG, "resize: width: " + width + " height: " + height);
        if ((this.mMaxHeight <= 0 || height <= this.mMaxHeight) && (this.mMaxWidth <= 0 || width <= this.mMaxWidth)) {
            this.mOrmmaView.resize((int) (this.mDensity * ((float) width)), (int) (this.mDensity * ((float) height)));
        } else {
            this.mOrmmaView.raiseError("Maximum size exceeded", "resize");
        }
    }

    @JavascriptInterface
    public void open(String url, boolean back, boolean forward, boolean refresh) {
        Log.d(LOG_TAG, "open: url: " + url + " back: " + back + " forward: " + forward + " refresh: " + refresh);
        if (!URLUtil.isValidUrl(url)) {
            this.mOrmmaView.raiseError("Invalid url", "open");
        } else {
            this.mOrmmaView.open(url, back, forward, refresh);
        }
    }

    @JavascriptInterface
    public void openMap(String url, boolean fullscreen) {
        Log.d(LOG_TAG, "openMap: url: " + url);
        this.mOrmmaView.openMap(url, fullscreen);
    }

    @JavascriptInterface
    public void playAudio(String url, boolean autoPlay, boolean controls, boolean loop, boolean position, String startStyle, String stopStyle) {
        Log.d(LOG_TAG, "playAudio: url: " + url + " autoPlay: " + autoPlay + " controls: " + controls + " loop: " + loop + " position: " + position + " startStyle: " + startStyle + " stopStyle: " + stopStyle);
        if (!URLUtil.isValidUrl(url)) {
            this.mOrmmaView.raiseError("Invalid url", "playAudio");
        } else {
            this.mOrmmaView.playAudio(url, autoPlay, controls, loop, position, startStyle, stopStyle);
        }
    }

    @JavascriptInterface
    public void playVideo(String url, boolean audioMuted, boolean autoPlay, boolean controls, boolean loop, int[] position, String startStyle, String stopStyle) {
        Log.d(LOG_TAG, "playVideo: url: " + url + " audioMuted: " + audioMuted + " autoPlay: " + autoPlay + " controls: " + controls + " loop: " + loop + " x: " + position[0] + " y: " + position[1] + " width: " + position[2] + " height: " + position[3] + " startStyle: " + startStyle + " stopStyle: " + stopStyle);
        OrmmaController.Dimensions d = null;
        if (position[0] != -1) {
            OrmmaController.Dimensions d2 = new OrmmaController.Dimensions();
            d2.x = position[0];
            d2.y = position[1];
            d2.width = position[2];
            d2.height = position[3];
            d = getDeviceDimensions(d2);
        }
        if (!URLUtil.isValidUrl(url)) {
            this.mOrmmaView.raiseError("Invalid url", "playVideo");
        } else {
            this.mOrmmaView.playVideo(url, audioMuted, autoPlay, controls, loop, d, startStyle, stopStyle);
        }
    }

    private OrmmaController.Dimensions getDeviceDimensions(OrmmaController.Dimensions d) {
        d.width = (int) (((float) d.width) * this.mDensity);
        d.height = (int) (((float) d.height) * this.mDensity);
        d.x = (int) (((float) d.x) * this.mDensity);
        d.y = (int) (((float) d.y) * this.mDensity);
        if (d.height < 0) {
            d.height = this.mOrmmaView.getHeight();
        }
        if (d.width < 0) {
            d.width = this.mOrmmaView.getWidth();
        }
        int[] loc = new int[2];
        this.mOrmmaView.getLocationInWindow(loc);
        if (d.x < 0) {
            d.x = loc[0];
        }
        if (d.y < 0) {
            d.y = loc[1] - 0;
        }
        return d;
    }

    @JavascriptInterface
    public void expand(String dimensions, String URL, String properties) {
        Log.d(LOG_TAG, "expand: dimensions: " + dimensions + " url: " + URL + " properties: " + properties);
        try {
            this.mOrmmaView.expand(getDeviceDimensions((OrmmaController.Dimensions) getFromJSON(new JSONObject(dimensions), OrmmaController.Dimensions.class)), URL, (OrmmaController.Properties) getFromJSON(new JSONObject(properties), OrmmaController.Properties.class));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (NullPointerException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        } catch (InstantiationException e4) {
            e4.printStackTrace();
        } catch (JSONException e5) {
            e5.printStackTrace();
        }
    }

    @JavascriptInterface
    public void close() {
        Log.d(LOG_TAG, "close");
        this.mOrmmaView.close();
    }

    @JavascriptInterface
    public void hide() {
        Log.d(LOG_TAG, "hide");
        this.mOrmmaView.hide();
    }

    @JavascriptInterface
    public void show() {
        Log.d(LOG_TAG, "show");
        this.mOrmmaView.show();
    }

    @JavascriptInterface
    public boolean isVisible() {
        return this.mOrmmaView.getVisibility() == 0;
    }

    @JavascriptInterface
    public String dimensions() {
        return "{ \"top\" :" + ((int) (((float) this.mOrmmaView.getTop()) / this.mDensity)) + "," + "\"left\" :" + ((int) (((float) this.mOrmmaView.getLeft()) / this.mDensity)) + "," + "\"bottom\" :" + ((int) (((float) this.mOrmmaView.getBottom()) / this.mDensity)) + "," + "\"right\" :" + ((int) (((float) this.mOrmmaView.getRight()) / this.mDensity)) + "}";
    }

    @JavascriptInterface
    public int getOrientation() {
        int ret = -1;
        switch (this.mWindowManager.getDefaultDisplay().getOrientation()) {
            case 0:
                ret = 0;
                break;
            case 1:
                ret = 90;
                break;
            case 2:
                ret = 180;
                break;
            case 3:
                ret = 270;
                break;
        }
        Log.d(LOG_TAG, "getOrientation: " + ret);
        return ret;
    }

    @JavascriptInterface
    public String getScreenSize() {
        DisplayMetrics metrics = new DisplayMetrics();
        this.mWindowManager.getDefaultDisplay().getMetrics(metrics);
        return "{ width: " + ((int) (((float) metrics.widthPixels) / metrics.density)) + ", " + "height: " + ((int) (((float) metrics.heightPixels) / metrics.density)) + "}";
    }

    @JavascriptInterface
    public String getSize() {
        return this.mOrmmaView.getSize();
    }

    @JavascriptInterface
    public String getMaxSize() {
        if (this.bMaxSizeSet) {
            return "{ width: " + this.mMaxWidth + ", " + "height: " + this.mMaxHeight + "}";
        }
        return getScreenSize();
    }

    @JavascriptInterface
    public void setMaxSize(int w, int h) {
        this.bMaxSizeSet = true;
        this.mMaxWidth = w;
        this.mMaxHeight = h;
    }

    @JavascriptInterface
    public void onOrientationChanged(int orientation) {
        String script = "window.ormmaview.fireChangeEvent({ orientation: " + orientation + "});";
        Log.d(LOG_TAG, script);
        this.mOrmmaView.injectJavaScript(script);
    }

    @JavascriptInterface
    public void logHTML(String html) {
        Log.d(LOG_TAG, html);
    }

    public void stopAllListeners() {
        stopConfigurationListener();
        this.mBroadCastReceiver = null;
    }

    public void stopConfigurationListener() {
        try {
            this.mContext.unregisterReceiver(this.mBroadCastReceiver);
        } catch (Exception e) {
        }
    }

    public void startConfigurationListener() {
        try {
            if (this.mBroadCastReceiver == null) {
                this.mBroadCastReceiver = new OrmmaConfigurationBroadcastReceiver(this);
            }
            this.mContext.registerReceiver(this.mBroadCastReceiver, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"));
        } catch (Exception e) {
        }
    }
}
