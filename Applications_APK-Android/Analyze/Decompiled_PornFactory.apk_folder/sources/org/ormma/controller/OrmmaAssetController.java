package org.ormma.controller;

import android.content.Context;
import android.os.StatFs;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.jar.JarFile;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ormma.view.OrmmaView;

public class OrmmaAssetController extends OrmmaController {
    private static final char[] HEX_CHARS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public OrmmaAssetController(OrmmaView adView, Context c) {
        super(adView, c);
    }

    public String copyTextFromJarIntoAssetDir(String alias, String source) {
        InputStream in = null;
        try {
            String file = OrmmaAssetController.class.getClassLoader().getResource(source).getFile();
            if (file.startsWith("file:")) {
                file = file.substring(5);
            }
            int pos = file.indexOf("!");
            if (pos > 0) {
                file = file.substring(0, pos);
            }
            JarFile jf = new JarFile(file);
            InputStream in2 = jf.getInputStream(jf.getJarEntry(source));
            String name = writeToDisk(in2, alias, false);
            if (in2 == null) {
                return name;
            }
            try {
                in2.close();
            } catch (Exception e) {
            }
            return name;
        } catch (Exception e2) {
            e2.printStackTrace();
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e3) {
                }
            }
            return null;
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }

    public void addAsset(String alias, String url) {
        HttpEntity entity = getHttpEntity(url);
        InputStream in = null;
        try {
            in = entity.getContent();
            writeToDisk(in, alias, false);
            this.mOrmmaView.injectJavaScript("OrmmaAdController.addedAsset('" + alias + "' )");
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
        try {
            entity.consumeContent();
        } catch (Exception e5) {
            e5.printStackTrace();
        }
    }

    private HttpEntity getHttpEntity(String url) {
        try {
            return new DefaultHttpClient().execute(new HttpGet(url)).getEntity();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int cacheRemaining() {
        StatFs stats = new StatFs(this.mContext.getFilesDir().getPath());
        return stats.getFreeBlocks() * stats.getBlockSize();
    }

    public String writeToDisk(InputStream in, String file, boolean storeInHashedDirectory) throws IllegalStateException, IOException {
        int i = 0;
        byte[] buff = new byte[1024];
        MessageDigest digest = null;
        if (storeInHashedDirectory) {
            try {
                digest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        FileOutputStream out = null;
        try {
            out = getAssetOutputString(file);
            while (true) {
                int numread = in.read(buff);
                if (numread <= 0) {
                    break;
                }
                if (storeInHashedDirectory && digest != null) {
                    digest.update(buff);
                }
                out.write(buff, 0, numread);
                i++;
            }
            out.flush();
            String filesDir = getFilesDir();
            if (storeInHashedDirectory && digest != null) {
                filesDir = moveToAdDirectory(file, filesDir, asHex(digest));
            }
            return String.valueOf(filesDir) + file;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e2) {
                }
            }
        }
    }

    public String writeToDiskWrap(InputStream in, String file, boolean storeInHashedDirectory, String injection, String bridgePath, String ormmaPath) throws IllegalStateException, IOException {
        byte[] buff = new byte[1024];
        MessageDigest digest = null;
        if (storeInHashedDirectory) {
            try {
                digest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream fromFile = new ByteArrayOutputStream();
        FileOutputStream out = null;
        while (true) {
            try {
                int numread = in.read(buff);
                if (numread <= 0) {
                    break;
                }
                if (storeInHashedDirectory && digest != null) {
                    digest.update(buff);
                }
                fromFile.write(buff, 0, numread);
            } finally {
                if (fromFile != null) {
                    try {
                        fromFile.close();
                    } catch (Exception e2) {
                    }
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (Exception e3) {
                    }
                }
            }
        }
        String wholeHTML = fromFile.toString();
        Log.d("html", wholeHTML);
        boolean hasHTMLWrap = wholeHTML.indexOf("</html>") >= 0;
        StringBuilder wholeHTMLBuffer = null;
        if (hasHTMLWrap) {
            wholeHTMLBuffer = new StringBuilder(wholeHTML);
            int start = wholeHTMLBuffer.indexOf("/ormma_bridge.js");
            if (start <= 0) {
                int endHeadIndex = wholeHTMLBuffer.indexOf("</head>");
                wholeHTMLBuffer.replace(endHeadIndex, "</head>".length() + endHeadIndex, "<script type=\"text/javascript\" src=\"file://" + bridgePath + "\"></script><script type=\"text/javascript\" src=\"file://" + ormmaPath + "\"></script></head>");
            } else {
                wholeHTMLBuffer.replace(start, "/ormma_bridge.js".length() + start, "file:/" + bridgePath);
                int start2 = wholeHTMLBuffer.indexOf("/ormma.js");
                wholeHTMLBuffer.replace(start2, "/ormma.js".length() + start2, "file:/" + ormmaPath);
            }
        }
        out = getAssetOutputString(file);
        if (!hasHTMLWrap) {
            out.write("<html>".getBytes());
        }
        if (!hasHTMLWrap) {
            out.write(fromFile.toString().replace("<head>", "<head><script src=\"file://" + bridgePath + "\" type=\"text/javascript\"></script><script src=\"file://" + ormmaPath + "\" type=\"text/javascript\"></script>").getBytes());
        } else {
            out.write(wholeHTMLBuffer.toString().getBytes());
        }
        if (!hasHTMLWrap) {
            out.write("</html> ".getBytes());
        }
        out.flush();
        String filesDir = getFilesDir();
        if (!storeInHashedDirectory || digest == null) {
            return filesDir;
        }
        return moveToAdDirectory(file, filesDir, asHex(digest));
    }

    private String moveToAdDirectory(String fn, String filesDir, String subDir) {
        File file = new File(String.valueOf(filesDir) + File.separator + fn);
        new File(String.valueOf(filesDir) + File.separator + "ad").mkdir();
        File dir = new File(String.valueOf(filesDir) + File.separator + "ad" + File.separator + subDir);
        dir.mkdir();
        file.renameTo(new File(dir, file.getName()));
        return String.valueOf(dir.getPath()) + File.separator;
    }

    private String asHex(MessageDigest digest) {
        byte[] hash = digest.digest();
        char[] buf = new char[(hash.length * 2)];
        int x = 0;
        for (int i = 0; i < hash.length; i++) {
            int x2 = x + 1;
            buf[x] = HEX_CHARS[(hash[i] >>> 4) & 15];
            x = x2 + 1;
            buf[x2] = HEX_CHARS[hash[i] & 15];
        }
        return new String(buf);
    }

    private String getFilesDir() {
        return this.mContext.getFilesDir().getPath();
    }

    public FileOutputStream getAssetOutputString(String asset) throws FileNotFoundException {
        File dir = getAssetDir(getAssetPath(asset));
        dir.mkdirs();
        return new FileOutputStream(new File(dir, getAssetName(asset)));
    }

    public void removeAsset(String asset) {
        File dir = getAssetDir(getAssetPath(asset));
        dir.mkdirs();
        new File(dir, getAssetName(asset)).delete();
        this.mOrmmaView.injectJavaScript("OrmmaAdController.assetRemoved('" + asset + "' )");
    }

    private File getAssetDir(String path) {
        return new File(String.valueOf(this.mContext.getFilesDir().getPath()) + File.separator + path);
    }

    private String getAssetPath(String asset) {
        if (asset.lastIndexOf(File.separatorChar) >= 0) {
            return asset.substring(0, asset.lastIndexOf(File.separatorChar));
        }
        return "/";
    }

    private String getAssetName(String asset) {
        String name = asset;
        if (asset.lastIndexOf(File.separatorChar) >= 0) {
            return asset.substring(asset.lastIndexOf(File.separatorChar) + 1);
        }
        return name;
    }

    public String getAssetPath() {
        return "file://" + this.mContext.getFilesDir() + "/";
    }

    public static boolean deleteDirectory(String path) {
        if (path != null) {
            return deleteDirectory(new File(path));
        }
        return false;
    }

    public static boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return path.delete();
    }

    public void deleteOldAds() {
        deleteDirectory(new File(String.valueOf(getFilesDir()) + File.separator + "ad"));
    }

    public void stopAllListeners() {
    }
}
