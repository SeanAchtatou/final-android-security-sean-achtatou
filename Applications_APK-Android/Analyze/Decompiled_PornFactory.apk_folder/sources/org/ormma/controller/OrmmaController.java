package org.ormma.controller;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.dd.plist.ASCIIPropertyListParser;
import java.lang.reflect.Field;
import org.json.JSONException;
import org.json.JSONObject;
import org.ormma.controller.util.NavigationStringEnum;
import org.ormma.controller.util.TransitionStringEnum;
import org.ormma.view.OrmmaView;

public abstract class OrmmaController {
    private static final String BOOLEAN_TYPE = "boolean";
    public static final String EXIT = "exit";
    private static final String FLOAT_TYPE = "float";
    public static final String FULL_SCREEN = "fullscreen";
    private static final String INT_TYPE = "int";
    private static final String NAVIGATION_TYPE = "class com.ormma.NavigationStringEnum";
    private static final String STRING_TYPE = "class java.lang.String";
    public static final String STYLE_NORMAL = "normal";
    private static final String TRANSITION_TYPE = "class com.ormma.TransitionStringEnum";
    protected Context mContext;
    protected OrmmaView mOrmmaView;

    public abstract void stopAllListeners();

    public static class PlayerProperties extends ReflectedParcelable {
        public static final Parcelable.Creator<PlayerProperties> CREATOR = new Parcelable.Creator<PlayerProperties>() {
            public PlayerProperties createFromParcel(Parcel in) {
                return new PlayerProperties(in);
            }

            public PlayerProperties[] newArray(int size) {
                return new PlayerProperties[size];
            }
        };
        public boolean audioMuted;
        public boolean autoPlay;
        public boolean doLoop;
        public boolean inline;
        public boolean showControl;
        public String startStyle;
        public String stopStyle;

        public PlayerProperties() {
            this.showControl = true;
            this.autoPlay = true;
            this.audioMuted = false;
            this.doLoop = false;
            this.stopStyle = OrmmaController.STYLE_NORMAL;
            this.startStyle = OrmmaController.STYLE_NORMAL;
            this.inline = false;
        }

        public PlayerProperties(Parcel in) {
            super(in);
        }

        public void setStopStyle(String style) {
            this.stopStyle = style;
        }

        public void setProperties(boolean audioMuted2, boolean autoPlay2, boolean controls, boolean inline2, boolean loop, String startStyle2, String stopStyle2) {
            this.autoPlay = autoPlay2;
            this.showControl = controls;
            this.doLoop = loop;
            this.audioMuted = audioMuted2;
            this.startStyle = startStyle2;
            this.stopStyle = stopStyle2;
            this.inline = inline2;
        }

        public void muteAudio() {
            this.audioMuted = true;
        }

        public boolean isAutoPlay() {
            return this.autoPlay;
        }

        public boolean showControl() {
            return this.showControl;
        }

        public boolean doLoop() {
            return this.doLoop;
        }

        public boolean doMute() {
            return this.audioMuted;
        }

        public boolean exitOnComplete() {
            return this.stopStyle.equalsIgnoreCase(OrmmaController.EXIT);
        }

        public boolean isFullScreen() {
            return this.startStyle.equalsIgnoreCase(OrmmaController.FULL_SCREEN);
        }
    }

    public static class Dimensions extends ReflectedParcelable {
        public static final Parcelable.Creator<Dimensions> CREATOR = new Parcelable.Creator<Dimensions>() {
            public Dimensions createFromParcel(Parcel in) {
                return new Dimensions(in);
            }

            public Dimensions[] newArray(int size) {
                return new Dimensions[size];
            }
        };
        public int height;
        public int width;
        public int x;
        public int y;

        public Dimensions() {
            this.x = -1;
            this.y = -1;
            this.width = -1;
            this.height = -1;
        }

        protected Dimensions(Parcel in) {
            super(in);
        }
    }

    public static class Properties extends ReflectedParcelable {
        public static final Parcelable.Creator<Properties> CREATOR = new Parcelable.Creator<Properties>() {
            public Properties createFromParcel(Parcel in) {
                return new Properties(in);
            }

            public Properties[] newArray(int size) {
                return new Properties[size];
            }
        };
        public int backgroundColor;
        public float backgroundOpacity;
        public boolean useBackground;

        protected Properties(Parcel in) {
            super(in);
        }

        public Properties() {
            this.useBackground = false;
            this.backgroundColor = 0;
            this.backgroundOpacity = 0.0f;
        }
    }

    public OrmmaController(OrmmaView adView, Context context) {
        this.mOrmmaView = adView;
        this.mContext = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, ?]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    protected static Object getFromJSON(JSONObject json, Class<?> c) throws IllegalAccessException, InstantiationException, NumberFormatException, NullPointerException {
        int iVal;
        Field[] fields = c.getDeclaredFields();
        Object obj = c.newInstance();
        int i = 0;
        while (i < fields.length) {
            Field f = fields[i];
            String JSONName = f.getName().replace('_', (char) ASCIIPropertyListParser.DATE_DATE_FIELD_DELIMITER);
            String typeStr = f.getType().toString();
            try {
                if (typeStr.equals(INT_TYPE)) {
                    String value = json.getString(JSONName).toLowerCase();
                    if (value.startsWith("#")) {
                        iVal = -1;
                        try {
                            if (value.startsWith("#0x")) {
                                iVal = Integer.decode(value.substring(1)).intValue();
                            } else {
                                iVal = Integer.parseInt(value.substring(1), 16);
                            }
                        } catch (NumberFormatException e) {
                        }
                    } else {
                        iVal = Integer.parseInt(value);
                    }
                    f.set(obj, Integer.valueOf(iVal));
                    i++;
                } else {
                    if (typeStr.equals(STRING_TYPE)) {
                        f.set(obj, json.getString(JSONName));
                    } else if (typeStr.equals(BOOLEAN_TYPE)) {
                        f.set(obj, Boolean.valueOf(json.getBoolean(JSONName)));
                    } else if (typeStr.equals(FLOAT_TYPE)) {
                        f.set(obj, Float.valueOf(Float.parseFloat(json.getString(JSONName))));
                    } else if (typeStr.equals(NAVIGATION_TYPE)) {
                        f.set(obj, NavigationStringEnum.fromString(json.getString(JSONName)));
                    } else if (typeStr.equals(TRANSITION_TYPE)) {
                        f.set(obj, TransitionStringEnum.fromString(json.getString(JSONName)));
                    }
                    i++;
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return obj;
    }

    public static class ReflectedParcelable implements Parcelable {
        public ReflectedParcelable() {
        }

        public int describeContents() {
            return 0;
        }

        protected ReflectedParcelable(Parcel in) {
            Field[] fields = getClass().getDeclaredFields();
            int i = 0;
            while (i < fields.length) {
                try {
                    Field f = fields[i];
                    Class<?> type = f.getType();
                    if (type.isEnum()) {
                        String typeStr = type.toString();
                        if (typeStr.equals(OrmmaController.NAVIGATION_TYPE)) {
                            f.set(this, NavigationStringEnum.fromString(in.readString()));
                        } else if (typeStr.equals(OrmmaController.TRANSITION_TYPE)) {
                            f.set(this, TransitionStringEnum.fromString(in.readString()));
                        }
                    } else if (!(f.get(this) instanceof Parcelable.Creator)) {
                        f.set(this, in.readValue(null));
                    }
                    i++;
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    return;
                } catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                    return;
                }
            }
        }

        public void writeToParcel(Parcel out, int flags1) {
            Field[] fields = getClass().getDeclaredFields();
            int i = 0;
            while (i < fields.length) {
                try {
                    Field f = fields[i];
                    Class<?> type = f.getType();
                    if (type.isEnum()) {
                        String typeStr = type.toString();
                        if (typeStr.equals(OrmmaController.NAVIGATION_TYPE)) {
                            out.writeString(((NavigationStringEnum) f.get(this)).getText());
                        } else if (typeStr.equals(OrmmaController.TRANSITION_TYPE)) {
                            out.writeString(((TransitionStringEnum) f.get(this)).getText());
                        }
                    } else {
                        Object dt = f.get(this);
                        if (!(dt instanceof Parcelable.Creator)) {
                            out.writeValue(dt);
                        }
                    }
                    i++;
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    return;
                } catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                    return;
                }
            }
        }
    }
}
