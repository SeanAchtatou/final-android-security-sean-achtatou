package org.ormma.controller;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.plus.PlusShare;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.ormma.controller.Defines;
import org.ormma.view.OrmmaView;

public class OrmmaUtilityController extends OrmmaController {
    private static final String LOG_TAG = "OrmmaUtilityController";
    private OrmmaAssetController mAssetController;
    private OrmmaDisplayController mDisplayController;
    private OrmmaLocationController mLocationController;
    private OrmmaNetworkController mNetworkController;
    private OrmmaSensorController mSensorController;

    public OrmmaUtilityController(OrmmaView adView, Context context) {
        super(adView, context);
        this.mAssetController = new OrmmaAssetController(adView, context);
        this.mDisplayController = new OrmmaDisplayController(adView, context);
        this.mLocationController = new OrmmaLocationController(adView, context);
        this.mNetworkController = new OrmmaNetworkController(adView, context);
        this.mSensorController = new OrmmaSensorController(adView, context);
        adView.addJavascriptInterface(this.mAssetController, "ORMMAAssetsControllerBridge");
        adView.addJavascriptInterface(this.mDisplayController, "ORMMADisplayControllerBridge");
        adView.addJavascriptInterface(this.mLocationController, "ORMMALocationControllerBridge");
        adView.addJavascriptInterface(this.mNetworkController, "ORMMANetworkControllerBridge");
        adView.addJavascriptInterface(this.mSensorController, "ORMMASensorControllerBridge");
    }

    public void init(float density) {
        String injection = "window.ormmaview.fireChangeEvent({ state: 'default', network: '" + this.mNetworkController.getNetwork() + "'," + " size: " + this.mDisplayController.getSize() + "," + " maxSize: " + this.mDisplayController.getMaxSize() + "," + " screenSize: " + this.mDisplayController.getScreenSize() + "," + " defaultPosition: { x:" + ((int) (((float) this.mOrmmaView.getLeft()) / density)) + ", y: " + ((int) (((float) this.mOrmmaView.getTop()) / density)) + ", width: " + ((int) (((float) this.mOrmmaView.getWidth()) / density)) + ", height: " + ((int) (((float) this.mOrmmaView.getHeight()) / density)) + " }," + " orientation:" + this.mDisplayController.getOrientation() + "," + getSupports() + " });";
        Log.d(LOG_TAG, "init: injection: " + injection);
        this.mOrmmaView.injectJavaScript(injection);
    }

    private String getSupports() {
        boolean p;
        boolean p2;
        boolean p3;
        boolean p4;
        String supports = "supports: [ 'level-1', 'level-2', 'screen', 'orientation', 'network'";
        if (!this.mLocationController.allowLocationServices() || !(this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0 || this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0)) {
            p = false;
        } else {
            p = true;
        }
        if (p) {
            supports = String.valueOf(supports) + ", 'location'";
        }
        if (this.mContext.checkCallingOrSelfPermission("android.permission.SEND_SMS") == 0) {
            p2 = true;
        } else {
            p2 = false;
        }
        if (p2) {
            supports = String.valueOf(supports) + ", 'sms'";
        }
        if (this.mContext.checkCallingOrSelfPermission("android.permission.CALL_PHONE") == 0) {
            p3 = true;
        } else {
            p3 = false;
        }
        if (p3) {
            supports = String.valueOf(supports) + ", 'phone'";
        }
        if (this.mContext.checkCallingOrSelfPermission("android.permission.WRITE_CALENDAR") == 0 && this.mContext.checkCallingOrSelfPermission("android.permission.READ_CALENDAR") == 0) {
            p4 = true;
        } else {
            p4 = false;
        }
        if (p4) {
            supports = String.valueOf(supports) + ", 'calendar'";
        }
        String supports2 = String.valueOf(String.valueOf(String.valueOf(String.valueOf(supports) + ", 'video'") + ", 'audio'") + ", 'map'") + ", 'email' ]";
        Log.d(LOG_TAG, "getSupports: " + supports2);
        return supports2;
    }

    public void ready() {
        this.mOrmmaView.injectJavaScript("Ormma.setState(\"" + this.mOrmmaView.getState() + "\");");
        this.mOrmmaView.injectJavaScript("ORMMAReady();");
    }

    @JavascriptInterface
    public void sendSMS(String recipient, String body) {
        Log.d(LOG_TAG, "sendSMS: recipient: " + recipient + " body: " + body);
        Intent sendIntent = new Intent("android.intent.action.VIEW");
        sendIntent.putExtra("address", recipient);
        sendIntent.putExtra("sms_body", body);
        sendIntent.setType("vnd.android-dir/mms-sms");
        sendIntent.addFlags(DriveFile.MODE_READ_ONLY);
        this.mContext.startActivity(sendIntent);
    }

    @JavascriptInterface
    public void sendMail(String recipient, String subject, String body) {
        Log.d(LOG_TAG, "sendMail: recipient: " + recipient + " subject: " + subject + " body: " + body);
        Intent i = new Intent("android.intent.action.SEND");
        i.setType("plain/text");
        i.putExtra("android.intent.extra.EMAIL", new String[]{recipient});
        i.putExtra("android.intent.extra.SUBJECT", subject);
        i.putExtra("android.intent.extra.TEXT", body);
        i.addFlags(DriveFile.MODE_READ_ONLY);
        this.mContext.startActivity(i);
    }

    private String createTelUrl(String number) {
        if (TextUtils.isEmpty(number)) {
            return null;
        }
        return "tel:" + number;
    }

    @JavascriptInterface
    public void makeCall(String number) {
        Log.d(LOG_TAG, "makeCall: number: " + number);
        String url = createTelUrl(number);
        if (url == null) {
            this.mOrmmaView.raiseError("Bad Phone Number", "makeCall");
            return;
        }
        Intent i = new Intent("android.intent.action.CALL", Uri.parse(url.toString()));
        i.addFlags(DriveFile.MODE_READ_ONLY);
        this.mContext.startActivity(i);
    }

    @JavascriptInterface
    public void createEvent(String date, String title, String body) {
        Cursor cursor;
        Log.d(LOG_TAG, "createEvent: date: " + date + " title: " + title + " body: " + body);
        ContentResolver cr = this.mContext.getContentResolver();
        String[] cols = {DmsConstants.ID, "displayName", "_sync_account"};
        if (Integer.parseInt(Build.VERSION.SDK) == 8) {
            cursor = cr.query(Uri.parse("content://com.android.calendar/calendars"), cols, null, null, null);
        } else {
            cursor = cr.query(Uri.parse("content://calendar/calendars"), cols, null, null, null);
        }
        if (cursor == null || (cursor != null && !cursor.moveToFirst())) {
            Toast.makeText(this.mContext, "No calendar account found", 1).show();
            if (cursor != null) {
                cursor.close();
                return;
            }
            return;
        }
        if (cursor.getCount() == 1) {
            addCalendarEvent(cursor.getInt(0), date, title, body);
        } else {
            List<Map<String, String>> entries = new ArrayList<>();
            for (int i = 0; i < cursor.getCount(); i++) {
                HashMap hashMap = new HashMap();
                hashMap.put("ID", cursor.getString(0));
                hashMap.put("NAME", cursor.getString(1));
                hashMap.put("EMAILID", cursor.getString(2));
                entries.add(hashMap);
                cursor.moveToNext();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
            builder.setTitle("Choose Calendar to save event to");
            final List<Map<String, String>> list = entries;
            final String str = date;
            final String str2 = title;
            final String str3 = body;
            AlertDialog.Builder builder2 = builder;
            builder2.setSingleChoiceItems(new SimpleAdapter(this.mContext, entries, 17367053, new String[]{"NAME", "EMAILID"}, new int[]{16908308, 16908309}), -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    OrmmaUtilityController.this.addCalendarEvent(Integer.parseInt((String) ((Map) list.get(which)).get("ID")), str, str2, str3);
                    dialog.cancel();
                }
            });
            builder.create().show();
        }
        cursor.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    @JavascriptInterface
    public void addCalendarEvent(int callId, String date, String title, String body) {
        Uri newEvent;
        ContentResolver cr = this.mContext.getContentResolver();
        long dtStart = Long.parseLong(date);
        ContentValues cv = new ContentValues();
        cv.put("calendar_id", Integer.valueOf(callId));
        cv.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title);
        cv.put("description", body);
        cv.put("dtstart", Long.valueOf(dtStart));
        cv.put("hasAlarm", (Integer) 1);
        cv.put("dtend", Long.valueOf(dtStart + 3600000));
        if (Integer.parseInt(Build.VERSION.SDK) == 8) {
            newEvent = cr.insert(Uri.parse("content://com.android.calendar/events"), cv);
        } else {
            newEvent = cr.insert(Uri.parse("content://calendar/events"), cv);
        }
        if (newEvent != null) {
            long id = Long.parseLong(newEvent.getLastPathSegment());
            ContentValues values = new ContentValues();
            values.put("event_id", Long.valueOf(id));
            values.put("method", (Integer) 1);
            values.put("minutes", (Integer) 15);
            if (Integer.parseInt(Build.VERSION.SDK) == 8) {
                cr.insert(Uri.parse("content://com.android.calendar/reminders"), values);
            } else {
                cr.insert(Uri.parse("content://calendar/reminders"), values);
            }
        }
        Toast.makeText(this.mContext, "Event added to calendar", 0).show();
    }

    public String copyTextFromJarIntoAssetDir(String alias, String source) {
        return this.mAssetController.copyTextFromJarIntoAssetDir(alias, source);
    }

    @JavascriptInterface
    public void setMaxSize(int w, int h) {
        this.mDisplayController.setMaxSize(w, h);
    }

    public String writeToDiskWrap(InputStream is, String currentFile, boolean storeInHashedDirectory, String injection, String bridgePath, String ormmaPath) throws IllegalStateException, IOException {
        return this.mAssetController.writeToDiskWrap(is, currentFile, storeInHashedDirectory, injection, bridgePath, ormmaPath);
    }

    @JavascriptInterface
    public void activate(String event) {
        Log.d(LOG_TAG, "activate: " + event);
        if (event.equalsIgnoreCase(Defines.Events.NETWORK_CHANGE)) {
            this.mNetworkController.startNetworkListener();
        } else if (this.mLocationController.allowLocationServices() && event.equalsIgnoreCase(Defines.Events.LOCATION_CHANGE)) {
            this.mLocationController.startLocationListener();
        } else if (event.equalsIgnoreCase(Defines.Events.SHAKE)) {
            this.mSensorController.startShakeListener();
        } else if (event.equalsIgnoreCase(Defines.Events.TILT_CHANGE)) {
            this.mSensorController.startTiltListener();
        } else if (event.equalsIgnoreCase(Defines.Events.HEADING_CHANGE)) {
            this.mSensorController.startHeadingListener();
        } else if (event.equalsIgnoreCase(Defines.Events.ORIENTATION_CHANGE)) {
            this.mDisplayController.startConfigurationListener();
        }
    }

    @JavascriptInterface
    public void deactivate(String event) {
        Log.d(LOG_TAG, "deactivate: " + event);
        if (event.equalsIgnoreCase(Defines.Events.NETWORK_CHANGE)) {
            this.mNetworkController.stopNetworkListener();
        } else if (event.equalsIgnoreCase(Defines.Events.LOCATION_CHANGE)) {
            this.mLocationController.stopAllListeners();
        } else if (event.equalsIgnoreCase(Defines.Events.SHAKE)) {
            this.mSensorController.stopShakeListener();
        } else if (event.equalsIgnoreCase(Defines.Events.TILT_CHANGE)) {
            this.mSensorController.stopTiltListener();
        } else if (event.equalsIgnoreCase(Defines.Events.HEADING_CHANGE)) {
            this.mSensorController.stopHeadingListener();
        } else if (event.equalsIgnoreCase(Defines.Events.ORIENTATION_CHANGE)) {
            this.mDisplayController.stopConfigurationListener();
        }
    }

    @JavascriptInterface
    public void deleteOldAds() {
        this.mAssetController.deleteOldAds();
    }

    public void stopAllListeners() {
        try {
            this.mAssetController.stopAllListeners();
            this.mDisplayController.stopAllListeners();
            this.mLocationController.stopAllListeners();
            this.mNetworkController.stopAllListeners();
            this.mSensorController.stopAllListeners();
        } catch (Exception e) {
        }
    }

    @JavascriptInterface
    public void showAlert(String message) {
        Log.e(LOG_TAG, message);
    }
}
