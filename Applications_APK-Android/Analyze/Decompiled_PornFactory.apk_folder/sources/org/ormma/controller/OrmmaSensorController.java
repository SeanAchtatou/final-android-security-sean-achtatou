package org.ormma.controller;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import org.ormma.controller.listeners.AccelListener;
import org.ormma.view.OrmmaView;

public class OrmmaSensorController extends OrmmaController {
    private static final String LOG_TAG = "OrmmaSensorController";
    final int INTERVAL = 1000;
    private AccelListener mAccel;
    private float mLastX = 0.0f;
    private float mLastY = 0.0f;
    private float mLastZ = 0.0f;

    public OrmmaSensorController(OrmmaView adView, Context context) {
        super(adView, context);
        this.mAccel = new AccelListener(context, this);
    }

    @JavascriptInterface
    public void startTiltListener() {
        this.mAccel.startTrackingTilt();
    }

    @JavascriptInterface
    public void startShakeListener() {
        this.mAccel.startTrackingShake();
    }

    @JavascriptInterface
    public void stopTiltListener() {
        this.mAccel.stopTrackingTilt();
    }

    @JavascriptInterface
    public void stopShakeListener() {
        this.mAccel.stopTrackingShake();
    }

    @JavascriptInterface
    public void startHeadingListener() {
        this.mAccel.startTrackingHeading();
    }

    @JavascriptInterface
    public void stopHeadingListener() {
        this.mAccel.stopTrackingHeading();
    }

    /* access modifiers changed from: package-private */
    @JavascriptInterface
    public void stop() {
    }

    @JavascriptInterface
    public void onShake() {
        this.mOrmmaView.injectJavaScript("Ormma.gotShake()");
    }

    @JavascriptInterface
    public void onTilt(float x, float y, float z) {
        this.mLastX = x;
        this.mLastY = y;
        this.mLastZ = z;
        String script = "window.ormmaview.fireChangeEvent({ tilt: " + getTilt() + "})";
        Log.d(LOG_TAG, script);
        this.mOrmmaView.injectJavaScript(script);
    }

    @JavascriptInterface
    public String getTilt() {
        String tilt = "{ x : \"" + this.mLastX + "\", y : \"" + this.mLastY + "\", z : \"" + this.mLastZ + "\"}";
        Log.d(LOG_TAG, "getTilt: " + tilt);
        return tilt;
    }

    @JavascriptInterface
    public void onHeadingChange(float f) {
        String script = "window.ormmaview.fireChangeEvent({ heading: " + ((int) (((double) f) * 57.29577951308232d)) + "});";
        Log.d(LOG_TAG, script);
        this.mOrmmaView.injectJavaScript(script);
    }

    @JavascriptInterface
    public float getHeading() {
        Log.d(LOG_TAG, "getHeading: " + this.mAccel.getHeading());
        return this.mAccel.getHeading();
    }

    public void stopAllListeners() {
        this.mAccel.stopAllListeners();
    }
}
