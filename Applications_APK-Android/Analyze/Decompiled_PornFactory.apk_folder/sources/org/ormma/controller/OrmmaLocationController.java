package org.ormma.controller;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.webkit.JavascriptInterface;
import org.ormma.controller.listeners.LocListener;
import org.ormma.view.OrmmaView;

public class OrmmaLocationController extends OrmmaController {
    private static final String LOG_TAG = "OrmmaLocationController";
    final int INTERVAL = 1000;
    private boolean allowLocationServices = false;
    private boolean hasPermission = false;
    private LocListener mGps;
    private int mLocListenerCount;
    private LocationManager mLocationManager;
    private LocListener mNetwork;

    public OrmmaLocationController(OrmmaView adView, Context context) {
        super(adView, context);
        try {
            this.mLocationManager = (LocationManager) context.getSystemService("location");
            if (this.mLocationManager.getProvider("gps") != null) {
                this.mGps = new LocListener(context, 1000, this, "gps");
            }
            if (this.mLocationManager.getProvider("network") != null) {
                this.mNetwork = new LocListener(context, 1000, this, "network");
            }
            this.hasPermission = true;
        } catch (SecurityException e) {
        }
    }

    public void allowLocationServices(boolean flag) {
        this.allowLocationServices = flag;
    }

    public boolean allowLocationServices() {
        return this.allowLocationServices;
    }

    private static String formatLocation(Location loc) {
        return "{ lat: " + loc.getLatitude() + ", lon: " + loc.getLongitude() + ", acc: " + loc.getAccuracy() + "}";
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x004a  */
    @android.webkit.JavascriptInterface
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getLocation() {
        /*
            r7 = this;
            r4 = 0
            java.lang.String r3 = "OrmmaLocationController"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "getLocation: hasPermission: "
            r5.<init>(r6)
            boolean r6 = r7.hasPermission
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r3, r5)
            boolean r3 = r7.hasPermission
            if (r3 != 0) goto L_0x001d
            r3 = r4
        L_0x001c:
            return r3
        L_0x001d:
            android.location.LocationManager r3 = r7.mLocationManager
            r5 = 1
            java.util.List r2 = r3.getProviders(r5)
            java.util.Iterator r1 = r2.iterator()
            r0 = 0
        L_0x0029:
            boolean r3 = r1.hasNext()
            if (r3 != 0) goto L_0x004a
        L_0x002f:
            java.lang.String r3 = "OrmmaLocationController"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "getLocation: "
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r3, r5)
            if (r0 == 0) goto L_0x0059
            java.lang.String r3 = formatLocation(r0)
            goto L_0x001c
        L_0x004a:
            android.location.LocationManager r5 = r7.mLocationManager
            java.lang.Object r3 = r1.next()
            java.lang.String r3 = (java.lang.String) r3
            android.location.Location r0 = r5.getLastKnownLocation(r3)
            if (r0 == 0) goto L_0x0029
            goto L_0x002f
        L_0x0059:
            r3 = r4
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ormma.controller.OrmmaLocationController.getLocation():java.lang.String");
    }

    @JavascriptInterface
    public void startLocationListener() {
        if (this.mLocListenerCount == 0) {
            if (this.mNetwork != null) {
                this.mNetwork.start();
            }
            if (this.mGps != null) {
                this.mGps.start();
            }
        }
        this.mLocListenerCount++;
    }

    @JavascriptInterface
    public void stopLocationListener() {
        this.mLocListenerCount--;
        if (this.mLocListenerCount == 0) {
            if (this.mNetwork != null) {
                this.mNetwork.stop();
            }
            if (this.mGps != null) {
                this.mGps.stop();
            }
        }
    }

    @JavascriptInterface
    public void success(Location loc) {
        String script = "window.ormmaview.fireChangeEvent({ location: " + formatLocation(loc) + "})";
        Log.d(LOG_TAG, script);
        this.mOrmmaView.injectJavaScript(script);
    }

    @JavascriptInterface
    public void fail() {
        Log.e(LOG_TAG, "Location can't be determined");
        this.mOrmmaView.injectJavaScript("window.ormmaview.fireErrorEvent(\"Location cannot be identified\", \"OrmmaLocationController\")");
    }

    public void stopAllListeners() {
        this.mLocListenerCount = 0;
        try {
            this.mGps.stop();
        } catch (Exception e) {
        }
        try {
            this.mNetwork.stop();
        } catch (Exception e2) {
        }
    }
}
