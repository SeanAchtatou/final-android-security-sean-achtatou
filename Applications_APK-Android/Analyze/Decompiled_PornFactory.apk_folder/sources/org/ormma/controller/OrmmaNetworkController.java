package org.ormma.controller;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.os.EnvironmentCompat;
import android.util.Log;
import android.webkit.JavascriptInterface;
import org.ormma.controller.util.OrmmaNetworkBroadcastReceiver;
import org.ormma.view.OrmmaView;

public class OrmmaNetworkController extends OrmmaController {
    private static /* synthetic */ int[] $SWITCH_TABLE$android$net$NetworkInfo$State = null;
    private static final String LOG_TAG = "OrmmaNetworkController";
    private OrmmaNetworkBroadcastReceiver mBroadCastReceiver;
    private ConnectivityManager mConnectivityManager;
    private IntentFilter mFilter;
    private int mNetworkListenerCount;

    static /* synthetic */ int[] $SWITCH_TABLE$android$net$NetworkInfo$State() {
        int[] iArr = $SWITCH_TABLE$android$net$NetworkInfo$State;
        if (iArr == null) {
            iArr = new int[NetworkInfo.State.values().length];
            try {
                iArr[NetworkInfo.State.CONNECTED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[NetworkInfo.State.CONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[NetworkInfo.State.DISCONNECTED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[NetworkInfo.State.DISCONNECTING.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[NetworkInfo.State.SUSPENDED.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[NetworkInfo.State.UNKNOWN.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$android$net$NetworkInfo$State = iArr;
        }
        return iArr;
    }

    public OrmmaNetworkController(OrmmaView adView, Context context) {
        super(adView, context);
        this.mConnectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
    }

    @JavascriptInterface
    public String getNetwork() {
        NetworkInfo ni = this.mConnectivityManager.getActiveNetworkInfo();
        String networkType = EnvironmentCompat.MEDIA_UNKNOWN;
        if (ni != null) {
            switch ($SWITCH_TABLE$android$net$NetworkInfo$State()[ni.getState().ordinal()]) {
                case 3:
                    networkType = "offline";
                    break;
                case 4:
                case 5:
                default:
                    int type = ni.getType();
                    if (type != 0) {
                        if (type == 1) {
                            networkType = "wifi";
                            break;
                        }
                    } else {
                        networkType = "cell";
                        break;
                    }
                    break;
                case 6:
                    networkType = EnvironmentCompat.MEDIA_UNKNOWN;
                    break;
            }
        } else {
            networkType = "offline";
        }
        Log.d(LOG_TAG, "getNetwork: " + networkType);
        return networkType;
    }

    public void startNetworkListener() {
        if (this.mNetworkListenerCount == 0) {
            this.mBroadCastReceiver = new OrmmaNetworkBroadcastReceiver(this);
            this.mFilter = new IntentFilter();
            this.mFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        }
        this.mNetworkListenerCount++;
        this.mContext.registerReceiver(this.mBroadCastReceiver, this.mFilter);
    }

    public void stopNetworkListener() {
        this.mNetworkListenerCount--;
        if (this.mNetworkListenerCount == 0) {
            this.mContext.unregisterReceiver(this.mBroadCastReceiver);
            this.mBroadCastReceiver = null;
            this.mFilter = null;
        }
    }

    @JavascriptInterface
    public void onConnectionChanged() {
        String script = "window.ormmaview.fireChangeEvent({ network: '" + getNetwork() + "'});";
        Log.d(LOG_TAG, script);
        this.mOrmmaView.injectJavaScript(script);
    }

    public void stopAllListeners() {
        this.mNetworkListenerCount = 0;
        try {
            this.mContext.unregisterReceiver(this.mBroadCastReceiver);
        } catch (Exception e) {
        }
    }
}
