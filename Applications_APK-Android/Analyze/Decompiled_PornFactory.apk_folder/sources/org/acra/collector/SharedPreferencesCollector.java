package org.acra.collector;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.dd.plist.ASCIIPropertyListParser;
import java.util.Map;
import java.util.TreeMap;
import org.acra.ACRA;

final class SharedPreferencesCollector {
    SharedPreferencesCollector() {
    }

    public static String collect(Context context) {
        StringBuilder result = new StringBuilder();
        Map<String, SharedPreferences> shrdPrefs = new TreeMap<>();
        shrdPrefs.put("default", PreferenceManager.getDefaultSharedPreferences(context));
        String[] shrdPrefsIds = ACRA.getConfig().additionalSharedPreferences();
        if (shrdPrefsIds != null) {
            for (String shrdPrefId : shrdPrefsIds) {
                shrdPrefs.put(shrdPrefId, context.getSharedPreferences(shrdPrefId, 0));
            }
        }
        for (String prefsId : shrdPrefs.keySet()) {
            SharedPreferences prefs = (SharedPreferences) shrdPrefs.get(prefsId);
            if (prefs != null) {
                Map<String, ?> kv = prefs.getAll();
                if (kv == null || kv.size() <= 0) {
                    result.append(prefsId).append((char) ASCIIPropertyListParser.DICTIONARY_ASSIGN_TOKEN).append("empty\n");
                } else {
                    for (String key : kv.keySet()) {
                        if (!filteredKey(key)) {
                            if (kv.get(key) != null) {
                                result.append(prefsId).append('.').append(key).append((char) ASCIIPropertyListParser.DICTIONARY_ASSIGN_TOKEN).append(kv.get(key).toString()).append("\n");
                            } else {
                                result.append(prefsId).append('.').append(key).append((char) ASCIIPropertyListParser.DICTIONARY_ASSIGN_TOKEN).append("null\n");
                            }
                        }
                    }
                }
            } else {
                result.append("null\n");
            }
            result.append(10);
        }
        return result.toString();
    }

    private static boolean filteredKey(String key) {
        for (String regex : ACRA.getConfig().excludeMatchingSharedPreferencesKeys()) {
            if (key.matches(regex)) {
                return true;
            }
        }
        return false;
    }
}
