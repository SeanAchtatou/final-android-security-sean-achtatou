package org.acra.collector;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Display;
import android.view.WindowManager;
import com.dd.plist.ASCIIPropertyListParser;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.acra.ACRA;

final class DisplayManagerCollector {
    static final SparseArray<String> mDensities = new SparseArray<>();
    static final SparseArray<String> mFlagsNames = new SparseArray<>();

    DisplayManagerCollector() {
    }

    public static String collectDisplays(Context ctx) {
        Display[] displays = null;
        StringBuilder result = new StringBuilder();
        if (Compatibility.getAPILevel() < 17) {
            displays = new Display[]{((WindowManager) ctx.getSystemService("window")).getDefaultDisplay()};
        } else {
            try {
                Object displayManager = ctx.getSystemService((String) ctx.getClass().getField("DISPLAY_SERVICE").get(null));
                displays = (Display[]) displayManager.getClass().getMethod("getDisplays", new Class[0]).invoke(displayManager, new Object[0]);
            } catch (IllegalArgumentException e) {
                ACRA.log.w(ACRA.LOG_TAG, "Error while collecting DisplayManager data: ", e);
            } catch (SecurityException e2) {
                ACRA.log.w(ACRA.LOG_TAG, "Error while collecting DisplayManager data: ", e2);
            } catch (IllegalAccessException e3) {
                ACRA.log.w(ACRA.LOG_TAG, "Error while collecting DisplayManager data: ", e3);
            } catch (NoSuchFieldException e4) {
                ACRA.log.w(ACRA.LOG_TAG, "Error while collecting DisplayManager data: ", e4);
            } catch (NoSuchMethodException e5) {
                ACRA.log.w(ACRA.LOG_TAG, "Error while collecting DisplayManager data: ", e5);
            } catch (InvocationTargetException e6) {
                ACRA.log.w(ACRA.LOG_TAG, "Error while collecting DisplayManager data: ", e6);
            }
        }
        for (Display display : displays) {
            result.append(collectDisplayData(display));
        }
        return result.toString();
    }

    private static Object collectDisplayData(Display display) {
        display.getMetrics(new DisplayMetrics());
        StringBuilder result = new StringBuilder();
        result.append(collectCurrentSizeRange(display));
        result.append(collectFlags(display));
        result.append(display.getDisplayId()).append(".height=").append(display.getHeight()).append(10);
        result.append(collectMetrics(display, "getMetrics"));
        result.append(collectName(display));
        result.append(display.getDisplayId()).append(".orientation=").append(display.getOrientation()).append(10);
        result.append(display.getDisplayId()).append(".pixelFormat=").append(display.getPixelFormat()).append(10);
        result.append(collectMetrics(display, "getRealMetrics"));
        result.append(collectSize(display, "getRealSize"));
        result.append(collectRectSize(display));
        result.append(display.getDisplayId()).append(".refreshRate=").append(display.getRefreshRate()).append(10);
        result.append(collectRotation(display));
        result.append(collectSize(display, "getSize"));
        result.append(display.getDisplayId()).append(".width=").append(display.getWidth()).append(10);
        result.append(collectIsValid(display));
        return result.toString();
    }

    private static Object collectIsValid(Display display) {
        StringBuilder result = new StringBuilder();
        try {
            result.append(display.getDisplayId()).append(".isValid=").append((Boolean) display.getClass().getMethod("isValid", new Class[0]).invoke(display, new Object[0])).append(10);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
        }
        return result.toString();
    }

    private static Object collectRotation(Display display) {
        StringBuilder result = new StringBuilder();
        try {
            int rotation = ((Integer) display.getClass().getMethod("getRotation", new Class[0]).invoke(display, new Object[0])).intValue();
            result.append(display.getDisplayId()).append(".rotation=");
            switch (rotation) {
                case 0:
                    result.append("ROTATION_0");
                    result.append(10);
                    break;
                case 1:
                    result.append("ROTATION_90");
                    result.append(10);
                    break;
                case 2:
                    result.append("ROTATION_180");
                    result.append(10);
                    break;
                case 3:
                    result.append("ROTATION_270");
                    result.append(10);
                    break;
                default:
                    result.append(rotation);
                    result.append(10);
                    break;
            }
        } catch (SecurityException e) {
        } catch (NoSuchMethodException e2) {
        } catch (IllegalArgumentException e3) {
        } catch (IllegalAccessException | InvocationTargetException e4) {
        }
        return result.toString();
    }

    private static Object collectRectSize(Display display) {
        StringBuilder result = new StringBuilder();
        try {
            Method getRectSize = display.getClass().getMethod("getRectSize", Rect.class);
            Rect size = new Rect();
            getRectSize.invoke(display, size);
            result.append(display.getDisplayId()).append(".rectSize=[").append(size.top).append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN).append(size.left).append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN).append(size.width()).append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN).append(size.height()).append(']').append(10);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
        }
        return result.toString();
    }

    private static Object collectSize(Display display, String methodName) {
        StringBuilder result = new StringBuilder();
        try {
            Method getRealSize = display.getClass().getMethod(methodName, Point.class);
            Point size = new Point();
            getRealSize.invoke(display, size);
            result.append(display.getDisplayId()).append('.').append(methodName).append("=[").append(size.x).append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN).append(size.y).append(']').append(10);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
        }
        return result.toString();
    }

    private static String collectCurrentSizeRange(Display display) {
        StringBuilder result = new StringBuilder();
        try {
            Method getCurrentSizeRange = display.getClass().getMethod("getCurrentSizeRange", Point.class, Point.class);
            Point smallest = new Point();
            Point largest = new Point();
            getCurrentSizeRange.invoke(display, smallest, largest);
            result.append(display.getDisplayId()).append(".currentSizeRange.smallest=[").append(smallest.x).append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN).append(smallest.y).append(']').append(10);
            result.append(display.getDisplayId()).append(".currentSizeRange.largest=[").append(largest.x).append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN).append(largest.y).append(']').append(10);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
        }
        return result.toString();
    }

    private static String collectFlags(Display display) {
        StringBuilder result = new StringBuilder();
        try {
            int flags = ((Integer) display.getClass().getMethod("getFlags", new Class[0]).invoke(display, new Object[0])).intValue();
            for (Field field : display.getClass().getFields()) {
                if (field.getName().startsWith("FLAG_")) {
                    mFlagsNames.put(field.getInt(null), field.getName());
                }
            }
            result.append(display.getDisplayId()).append(".flags=").append(activeFlags(mFlagsNames, flags)).append(10);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
        }
        return result.toString();
    }

    private static String collectName(Display display) {
        StringBuilder result = new StringBuilder();
        try {
            result.append(display.getDisplayId()).append(".name=").append((String) display.getClass().getMethod("getName", new Class[0]).invoke(display, new Object[0])).append(10);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
        }
        return result.toString();
    }

    private static Object collectMetrics(Display display, String methodName) {
        StringBuilder result = new StringBuilder();
        try {
            DisplayMetrics metrics = (DisplayMetrics) display.getClass().getMethod(methodName, new Class[0]).invoke(display, new Object[0]);
            for (Field field : DisplayMetrics.class.getFields()) {
                if (field.getType().equals(Integer.class) && field.getName().startsWith("DENSITY_") && !field.getName().equals("DENSITY_DEFAULT")) {
                    mDensities.put(field.getInt(null), field.getName());
                }
            }
            result.append(display.getDisplayId()).append('.').append(methodName).append(".density=").append(metrics.density).append(10);
            result.append(display.getDisplayId()).append('.').append(methodName).append(".densityDpi=").append(metrics.getClass().getField("densityDpi")).append(10);
            result.append(display.getDisplayId()).append('.').append(methodName).append("scaledDensity=x").append(metrics.scaledDensity).append(10);
            result.append(display.getDisplayId()).append('.').append(methodName).append(".widthPixels=").append(metrics.widthPixels).append(10);
            result.append(display.getDisplayId()).append('.').append(methodName).append(".heightPixels=").append(metrics.heightPixels).append(10);
            result.append(display.getDisplayId()).append('.').append(methodName).append(".xdpi=").append(metrics.xdpi).append(10);
            result.append(display.getDisplayId()).append('.').append(methodName).append(".ydpi=").append(metrics.ydpi).append(10);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
        }
        return result.toString();
    }

    private static String activeFlags(SparseArray<String> valueNames, int bitfield) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < valueNames.size(); i++) {
            int value = bitfield & valueNames.keyAt(i);
            if (value > 0) {
                if (result.length() > 0) {
                    result.append('+');
                }
                result.append(valueNames.get(value));
            }
        }
        return result.toString();
    }
}
