package org.acra.collector;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;
import com.nth.analytics.android.LocalyticsProvider;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.acra.ACRA;
import org.acra.ACRAConstants;
import org.acra.ReportField;
import org.acra.annotation.ReportsCrashes;
import org.acra.util.Installation;
import org.acra.util.PackageManagerWrapper;
import org.acra.util.ReportUtils;

public final class CrashReportDataFactory {
    private final Time appStartDate;
    private final Context context;
    private final Map<String, String> customParameters = new HashMap();
    private final String initialConfiguration;
    private final SharedPreferences prefs;

    public CrashReportDataFactory(Context context2, SharedPreferences prefs2, Time appStartDate2, String initialConfiguration2) {
        this.context = context2;
        this.prefs = prefs2;
        this.appStartDate = appStartDate2;
        this.initialConfiguration = initialConfiguration2;
    }

    public String putCustomData(String key, String value) {
        return this.customParameters.put(key, value);
    }

    public String removeCustomData(String key) {
        return this.customParameters.remove(key);
    }

    public String getCustomData(String key) {
        return this.customParameters.get(key);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [org.acra.ReportField, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    public CrashReportData createCrashData(Throwable th, boolean isSilentReport, Thread brokenThread) {
        String deviceId;
        CrashReportData crashReportData = new CrashReportData();
        try {
            List<ReportField> crashReportFields = getReportFields();
            crashReportData.put((Enum) ReportField.STACK_TRACE, (Object) getStackTrace(th));
            crashReportData.put((Enum) ReportField.USER_APP_START_DATE, (Object) this.appStartDate.format3339(false));
            if (isSilentReport) {
                crashReportData.put((Enum) ReportField.IS_SILENT, (Object) "true");
            }
            if (crashReportFields.contains(ReportField.REPORT_ID)) {
                crashReportData.put((Enum) ReportField.REPORT_ID, (Object) UUID.randomUUID().toString());
            }
            if (crashReportFields.contains(ReportField.INSTALLATION_ID)) {
                crashReportData.put((Enum) ReportField.INSTALLATION_ID, (Object) Installation.id(this.context));
            }
            if (crashReportFields.contains(ReportField.INITIAL_CONFIGURATION)) {
                crashReportData.put((Enum) ReportField.INITIAL_CONFIGURATION, (Object) this.initialConfiguration);
            }
            if (crashReportFields.contains(ReportField.CRASH_CONFIGURATION)) {
                crashReportData.put((Enum) ReportField.CRASH_CONFIGURATION, (Object) ConfigurationCollector.collectConfiguration(this.context));
            }
            if (!(th instanceof OutOfMemoryError) && crashReportFields.contains(ReportField.DUMPSYS_MEMINFO)) {
                crashReportData.put((Enum) ReportField.DUMPSYS_MEMINFO, (Object) DumpSysCollector.collectMemInfo());
            }
            if (crashReportFields.contains(ReportField.PACKAGE_NAME)) {
                crashReportData.put((Enum) ReportField.PACKAGE_NAME, (Object) this.context.getPackageName());
            }
            if (crashReportFields.contains(ReportField.BUILD)) {
                crashReportData.put((Enum) ReportField.BUILD, (Object) (ReflectionCollector.collectConstants(Build.class) + ReflectionCollector.collectConstants(Build.VERSION.class, "VERSION")));
            }
            if (crashReportFields.contains(ReportField.PHONE_MODEL)) {
                crashReportData.put((Enum) ReportField.PHONE_MODEL, (Object) Build.MODEL);
            }
            if (crashReportFields.contains(ReportField.ANDROID_VERSION)) {
                crashReportData.put((Enum) ReportField.ANDROID_VERSION, (Object) Build.VERSION.RELEASE);
            }
            if (crashReportFields.contains(ReportField.BRAND)) {
                crashReportData.put((Enum) ReportField.BRAND, (Object) Build.BRAND);
            }
            if (crashReportFields.contains(ReportField.PRODUCT)) {
                crashReportData.put((Enum) ReportField.PRODUCT, (Object) Build.PRODUCT);
            }
            if (crashReportFields.contains(ReportField.TOTAL_MEM_SIZE)) {
                crashReportData.put((Enum) ReportField.TOTAL_MEM_SIZE, (Object) Long.toString(ReportUtils.getTotalInternalMemorySize()));
            }
            if (crashReportFields.contains(ReportField.AVAILABLE_MEM_SIZE)) {
                crashReportData.put((Enum) ReportField.AVAILABLE_MEM_SIZE, (Object) Long.toString(ReportUtils.getAvailableInternalMemorySize()));
            }
            if (crashReportFields.contains(ReportField.FILE_PATH)) {
                crashReportData.put((Enum) ReportField.FILE_PATH, (Object) ReportUtils.getApplicationFilePath(this.context));
            }
            if (crashReportFields.contains(ReportField.DISPLAY)) {
                crashReportData.put((Enum) ReportField.DISPLAY, (Object) DisplayManagerCollector.collectDisplays(this.context));
            }
            if (crashReportFields.contains(ReportField.USER_CRASH_DATE)) {
                Time curDate = new Time();
                curDate.setToNow();
                crashReportData.put((Enum) ReportField.USER_CRASH_DATE, (Object) curDate.format3339(false));
            }
            if (crashReportFields.contains(ReportField.CUSTOM_DATA)) {
                crashReportData.put((Enum) ReportField.CUSTOM_DATA, (Object) createCustomInfoString());
            }
            if (crashReportFields.contains(ReportField.USER_EMAIL)) {
                crashReportData.put((Enum) ReportField.USER_EMAIL, (Object) this.prefs.getString(ACRA.PREF_USER_EMAIL_ADDRESS, "N/A"));
            }
            if (crashReportFields.contains(ReportField.DEVICE_FEATURES)) {
                crashReportData.put((Enum) ReportField.DEVICE_FEATURES, (Object) DeviceFeaturesCollector.getFeatures(this.context));
            }
            if (crashReportFields.contains(ReportField.ENVIRONMENT)) {
                crashReportData.put((Enum) ReportField.ENVIRONMENT, (Object) ReflectionCollector.collectStaticGettersResults(Environment.class));
            }
            if (crashReportFields.contains(ReportField.SETTINGS_SYSTEM)) {
                crashReportData.put((Enum) ReportField.SETTINGS_SYSTEM, (Object) SettingsCollector.collectSystemSettings(this.context));
            }
            if (crashReportFields.contains(ReportField.SETTINGS_SECURE)) {
                crashReportData.put((Enum) ReportField.SETTINGS_SECURE, (Object) SettingsCollector.collectSecureSettings(this.context));
            }
            if (crashReportFields.contains(ReportField.SETTINGS_GLOBAL)) {
                crashReportData.put((Enum) ReportField.SETTINGS_GLOBAL, (Object) SettingsCollector.collectGlobalSettings(this.context));
            }
            if (crashReportFields.contains(ReportField.SHARED_PREFERENCES)) {
                crashReportData.put((Enum) ReportField.SHARED_PREFERENCES, (Object) SharedPreferencesCollector.collect(this.context));
            }
            PackageManagerWrapper pm = new PackageManagerWrapper(this.context);
            PackageInfo pi = pm.getPackageInfo();
            if (pi != null) {
                if (crashReportFields.contains(ReportField.APP_VERSION_CODE)) {
                    crashReportData.put((Enum) ReportField.APP_VERSION_CODE, (Object) Integer.toString(pi.versionCode));
                }
                if (crashReportFields.contains(ReportField.APP_VERSION_NAME)) {
                    crashReportData.put((Enum) ReportField.APP_VERSION_NAME, (Object) (pi.versionName != null ? pi.versionName : "not set"));
                }
            } else {
                crashReportData.put((Enum) ReportField.APP_VERSION_NAME, (Object) "Package info unavailable");
            }
            if (crashReportFields.contains(ReportField.DEVICE_ID) && this.prefs.getBoolean(ACRA.PREF_ENABLE_DEVICE_ID, true) && pm.hasPermission("android.permission.READ_PHONE_STATE") && (deviceId = ReportUtils.getDeviceId(this.context)) != null) {
                crashReportData.put((Enum) ReportField.DEVICE_ID, (Object) deviceId);
            }
            if ((!this.prefs.getBoolean(ACRA.PREF_ENABLE_SYSTEM_LOGS, true) || !pm.hasPermission("android.permission.READ_LOGS")) && Compatibility.getAPILevel() < 16) {
                Log.i(ACRA.LOG_TAG, "READ_LOGS not allowed. ACRA will not include LogCat and DropBox data.");
            } else {
                Log.i(ACRA.LOG_TAG, "READ_LOGS granted! ACRA can include LogCat and DropBox data.");
                if (crashReportFields.contains(ReportField.LOGCAT)) {
                    crashReportData.put((Enum) ReportField.LOGCAT, (Object) LogCatCollector.collectLogCat(null));
                }
                if (crashReportFields.contains(ReportField.EVENTSLOG)) {
                    crashReportData.put((Enum) ReportField.EVENTSLOG, (Object) LogCatCollector.collectLogCat(LocalyticsProvider.EventsDbColumns.TABLE_NAME));
                }
                if (crashReportFields.contains(ReportField.RADIOLOG)) {
                    crashReportData.put((Enum) ReportField.RADIOLOG, (Object) LogCatCollector.collectLogCat("radio"));
                }
                if (crashReportFields.contains(ReportField.DROPBOX)) {
                    crashReportData.put((Enum) ReportField.DROPBOX, (Object) DropBoxCollector.read(this.context, ACRA.getConfig().additionalDropBoxTags()));
                }
            }
            if (crashReportFields.contains(ReportField.APPLICATION_LOG)) {
                crashReportData.put((Enum) ReportField.APPLICATION_LOG, (Object) LogFileCollector.collectLogFile(this.context, ACRA.getConfig().applicationLogFile(), ACRA.getConfig().applicationLogFileLines()));
            }
            if (crashReportFields.contains(ReportField.MEDIA_CODEC_LIST)) {
                crashReportData.put((Enum) ReportField.MEDIA_CODEC_LIST, (Object) MediaCodecListCollector.collecMediaCodecList());
            }
            if (crashReportFields.contains(ReportField.THREAD_DETAILS)) {
                crashReportData.put((Enum) ReportField.THREAD_DETAILS, (Object) ThreadCollector.collect(brokenThread));
            }
            if (crashReportFields.contains(ReportField.USER_IP)) {
                crashReportData.put((Enum) ReportField.USER_IP, (Object) ReportUtils.getLocalIpAddress());
            }
        } catch (RuntimeException e) {
            Log.e(ACRA.LOG_TAG, "Error while retrieving crash data", e);
        } catch (FileNotFoundException e2) {
            Log.e(ACRA.LOG_TAG, "Error : application log file " + ACRA.getConfig().applicationLogFile() + " not found.", e2);
        } catch (IOException e3) {
            Log.e(ACRA.LOG_TAG, "Error while reading application log file " + ACRA.getConfig().applicationLogFile() + ".", e3);
        }
        return crashReportData;
    }

    private String createCustomInfoString() {
        StringBuilder customInfo = new StringBuilder();
        for (String currentKey : this.customParameters.keySet()) {
            String currentVal = this.customParameters.get(currentKey);
            customInfo.append(currentKey);
            customInfo.append(" = ");
            if (currentVal != null) {
                currentVal = currentVal.replaceAll("\n", "\\\\n");
            }
            customInfo.append(currentVal);
            customInfo.append("\n");
        }
        return customInfo.toString();
    }

    private String getStackTrace(Throwable th) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        for (Throwable cause = th; cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        String stacktraceAsString = result.toString();
        printWriter.close();
        return stacktraceAsString;
    }

    private List<ReportField> getReportFields() {
        ReportField[] fieldsList;
        ReportsCrashes config = ACRA.getConfig();
        ReportField[] customReportFields = config.customReportContent();
        if (customReportFields.length != 0) {
            Log.d(ACRA.LOG_TAG, "Using custom Report Fields");
            fieldsList = customReportFields;
        } else if (config.mailTo() == null || "".equals(config.mailTo())) {
            Log.d(ACRA.LOG_TAG, "Using default Report Fields");
            fieldsList = ACRAConstants.DEFAULT_REPORT_FIELDS;
        } else {
            Log.d(ACRA.LOG_TAG, "Using default Mail Report Fields");
            fieldsList = ACRAConstants.DEFAULT_MAIL_REPORT_FIELDS;
        }
        return Arrays.asList(fieldsList);
    }
}
