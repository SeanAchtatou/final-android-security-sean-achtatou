package ch.nth.simpleplist.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeSet;

public final class CollectionFactory {
    public static Collection getInstance(Field field) throws Exception {
        Class type = field.getType();
        if (!TypeUtils.isInstantiable(type)) {
            type = getConversion(field);
        }
        if (TypeUtils.isCollection(type)) {
            return (Collection) type.newInstance();
        }
        throw new InstantiationException(String.format("Type is not a collection %s", type));
    }

    public static Collection getInstance(Class type) throws Exception {
        if (!TypeUtils.isInstantiable(type)) {
            throw new InstantiationException(String.format("Could not instantiate class %s", type));
        } else if (TypeUtils.isCollection(type)) {
            return (Collection) type.newInstance();
        } else {
            throw new InstantiationException(String.format("Type is not a collection: %s", type));
        }
    }

    public static Class getConversion(Field field) throws Exception {
        Class type = field.getType();
        if (type.isAssignableFrom(ArrayList.class)) {
            return ArrayList.class;
        }
        if (type.isAssignableFrom(HashSet.class)) {
            return HashSet.class;
        }
        if (type.isAssignableFrom(TreeSet.class)) {
            return TreeSet.class;
        }
        throw new InstantiationException(String.format("Cannot instantiate %s", type));
    }
}
