package ch.nth.simpleplist.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Collection;

public class TypeUtils {
    public static Object getInstance(Class type) throws Exception {
        Constructor method = type.getDeclaredConstructor(new Class[0]);
        if (!method.isAccessible()) {
            method.setAccessible(true);
        }
        return method.newInstance(new Object[0]);
    }

    public static boolean isInstantiable(Class type) {
        int modifiers = type.getModifiers();
        if (!Modifier.isAbstract(modifiers) && !Modifier.isInterface(modifiers)) {
            return true;
        }
        return false;
    }

    public static boolean isCollection(Class type) {
        return Collection.class.isAssignableFrom(type);
    }
}
