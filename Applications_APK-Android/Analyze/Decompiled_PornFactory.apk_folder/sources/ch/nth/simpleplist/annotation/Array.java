package ch.nth.simpleplist.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Array {
    Class<? extends Object> clazz() default Object.class;

    boolean dictionary() default false;

    String name();

    String path() default "";

    boolean required() default true;
}
