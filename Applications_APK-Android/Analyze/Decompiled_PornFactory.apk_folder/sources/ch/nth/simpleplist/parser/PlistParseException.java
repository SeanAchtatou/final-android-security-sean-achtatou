package ch.nth.simpleplist.parser;

public class PlistParseException extends Exception {
    private static final long serialVersionUID = 8500358798332756427L;

    public PlistParseException() {
    }

    public PlistParseException(String message) {
        super(message);
    }

    public PlistParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlistParseException(Throwable cause) {
        super(cause);
    }
}
