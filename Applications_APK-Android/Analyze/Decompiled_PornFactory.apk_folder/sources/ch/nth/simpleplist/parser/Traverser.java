package ch.nth.simpleplist.parser;

import ch.nth.simpleplist.annotation.Array;
import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

interface Traverser<T> {
    void processArray(Field field, Array array, Object obj, Path path) throws PlistParseException;

    void processDictionary(Field field, Dictionary dictionary, Object obj, Path path) throws PlistParseException;

    void processProperty(Field field, Property property, Object obj, Path path) throws PlistParseException;

    <U> U read(Class<U> cls) throws PlistParseException;

    <U> U read(Class<U> cls, Path path) throws PlistParseException;

    void scan(Class cls, Object obj, Path path) throws PlistParseException;

    void scan(Field field, Annotation annotation, Object obj, Path path) throws PlistParseException;
}
