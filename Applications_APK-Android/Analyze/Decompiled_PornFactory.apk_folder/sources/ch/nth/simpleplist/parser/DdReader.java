package ch.nth.simpleplist.parser;

import com.dd.plist.NSDictionary;
import com.dd.plist.PropertyListParser;
import java.io.File;
import java.io.InputStream;

public class DdReader implements Reader {
    public <T> T read(Class<T> type, String source) throws PlistParseException {
        if (source != null) {
            return read(type, source.getBytes());
        }
        throw new PlistParseException("Source string is null");
    }

    public <T> T read(Class<T> type, byte[] source) throws PlistParseException {
        if (source == null) {
            throw new PlistParseException("Source byte array is null");
        }
        try {
            return read(type, (NSDictionary) PropertyListParser.parse(source));
        } catch (Exception e) {
            throw new PlistParseException("Source byte array parsing exception");
        }
    }

    public <T> T read(Class<T> type, File source) throws PlistParseException {
        if (source == null) {
            throw new PlistParseException("Source file is null");
        } else if (!source.exists()) {
            throw new PlistParseException("Source file does not exist");
        } else {
            try {
                return read(type, (NSDictionary) PropertyListParser.parse(source));
            } catch (Exception e) {
                throw new PlistParseException("Source file parsing exception");
            }
        }
    }

    public <T> T read(Class<T> type, InputStream source) throws PlistParseException {
        if (source == null) {
            throw new PlistParseException("Source input stream is null");
        }
        try {
            return read(type, (NSDictionary) PropertyListParser.parse(source));
        } catch (Exception e) {
            throw new PlistParseException("Source input stream parsing exception");
        }
    }

    private <T> T read(Class<T> type, NSDictionary source) throws PlistParseException {
        return new DdTraverser(source).read(type);
    }
}
