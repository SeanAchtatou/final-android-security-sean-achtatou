package ch.nth.simpleplist.parser;

interface Parser<T, S> {
    S getArray(String str, Object obj) throws PlistParseException;

    boolean getBooleanProperty(String str, Object obj) throws PlistParseException;

    T getDictionary(String str, Object obj) throws PlistParseException;

    double getDoubleProperty(String str, Object obj) throws PlistParseException;

    float getFloatProperty(String str, Object obj) throws PlistParseException;

    boolean getGenericBooleanProperty(String str, Object obj) throws PlistParseException;

    double getGenericDoubleProperty(String str, Object obj) throws PlistParseException;

    float getGenericFloatProperty(String str, Object obj) throws PlistParseException;

    int getGenericIntegerProperty(String str, Object obj) throws PlistParseException;

    int getIntegerProperty(String str, Object obj) throws PlistParseException;

    boolean getStringAsBooleanProperty(String str, Object obj) throws PlistParseException;

    double getStringAsDoubleProperty(String str, Object obj) throws PlistParseException;

    float getStringAsFloatProperty(String str, Object obj) throws PlistParseException;

    int getStringAsIntegerProperty(String str, Object obj) throws PlistParseException;

    String getStringProperty(String str, Object obj) throws PlistParseException;
}
