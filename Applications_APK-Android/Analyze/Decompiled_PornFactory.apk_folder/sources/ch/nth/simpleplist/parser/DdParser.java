package ch.nth.simpleplist.parser;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSObject;

class DdParser implements Parser<NSDictionary, NSArray> {
    DdParser() {
    }

    public NSDictionary getDictionary(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj instanceof NSDictionary) {
            return (NSDictionary) obj;
        }
        throw new PlistParseException("Error parsing dictionary with key: " + key);
    }

    public String getStringProperty(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj != null) {
            return obj.toString();
        }
        throw new PlistParseException("Error parsing property with key: " + key);
    }

    public int getGenericIntegerProperty(String key, NSDictionary root) throws PlistParseException {
        try {
            return getIntegerProperty(key, root);
        } catch (PlistParseException e) {
            try {
                return getStringAsIntegerProperty(key, root);
            } catch (PlistParseException e2) {
                throw new PlistParseException("Error parsing property with key: " + key);
            }
        }
    }

    public int getIntegerProperty(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj == null) {
            throw new PlistParseException("Error parsing property with key: " + key);
        } else if (obj instanceof NSNumber) {
            NSNumber num = (NSNumber) obj;
            if (num.type() == 0) {
                return num.intValue();
            }
            throw new PlistParseException("Property with key: " + key + " is not a integer");
        } else {
            throw new PlistParseException("Property with key: " + key + " is not a integer");
        }
    }

    public int getStringAsIntegerProperty(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj == null) {
            throw new PlistParseException("Error parsing property with key: " + key);
        }
        try {
            return Integer.parseInt(obj.toString());
        } catch (NumberFormatException e) {
            throw new PlistParseException("Property with key: " + key + " is not a integer");
        }
    }

    public boolean getGenericBooleanProperty(String key, NSDictionary root) throws PlistParseException {
        try {
            return getBooleanProperty(key, root);
        } catch (PlistParseException e) {
            try {
                return getStringAsBooleanProperty(key, root);
            } catch (PlistParseException e2) {
                throw new PlistParseException("Error parsing property with key: " + key);
            }
        }
    }

    public boolean getBooleanProperty(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj == null) {
            throw new PlistParseException("Error parsing property with key: " + key);
        } else if (obj instanceof NSNumber) {
            NSNumber num = (NSNumber) obj;
            if (num.type() == 2) {
                return num.boolValue();
            }
            throw new PlistParseException("Property with key: " + key + " is not a boolean");
        } else {
            throw new PlistParseException("Property with key: " + key + " is not a boolean");
        }
    }

    public boolean getStringAsBooleanProperty(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj != null) {
            return "true".equalsIgnoreCase(obj.toString());
        }
        throw new PlistParseException("Error parsing property with key: " + key);
    }

    public NSArray getArray(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj == null) {
            throw new PlistParseException("Error parsing array with key: " + key);
        } else if (obj instanceof NSArray) {
            return (NSArray) obj;
        } else {
            throw new PlistParseException("Array with key: " + key + " is not a collection!");
        }
    }

    public float getGenericFloatProperty(String key, NSDictionary root) throws PlistParseException {
        try {
            return getFloatProperty(key, root);
        } catch (PlistParseException e) {
            try {
                return getStringAsFloatProperty(key, root);
            } catch (PlistParseException e2) {
                throw new PlistParseException("Error parsing property with key: " + key);
            }
        }
    }

    public float getFloatProperty(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj == null) {
            throw new PlistParseException("Error parsing property with key: " + key);
        } else if (obj instanceof NSNumber) {
            NSNumber num = (NSNumber) obj;
            if (num.isReal()) {
                return num.floatValue();
            }
            throw new PlistParseException("Property with key: " + key + " is not a float");
        } else {
            throw new PlistParseException("Property with key: " + key + " is not a float");
        }
    }

    public float getStringAsFloatProperty(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj == null) {
            throw new PlistParseException("Error parsing property with key: " + key);
        }
        try {
            return Float.parseFloat(obj.toString());
        } catch (NumberFormatException e) {
            throw new PlistParseException("Property with key: " + key + " is not a float");
        }
    }

    public double getGenericDoubleProperty(String key, NSDictionary root) throws PlistParseException {
        try {
            return getDoubleProperty(key, root);
        } catch (PlistParseException e) {
            try {
                return getStringAsDoubleProperty(key, root);
            } catch (PlistParseException e2) {
                throw new PlistParseException("Error parsing property with key: " + key);
            }
        }
    }

    public double getDoubleProperty(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj == null) {
            throw new PlistParseException("Error parsing property with key: " + key);
        } else if (obj instanceof NSNumber) {
            NSNumber num = (NSNumber) obj;
            if (num.isReal()) {
                return num.doubleValue();
            }
            throw new PlistParseException("Property with key: " + key + " is not a double");
        } else {
            throw new PlistParseException("Property with key: " + key + " is not a double");
        }
    }

    public double getStringAsDoubleProperty(String key, NSDictionary root) throws PlistParseException {
        NSObject obj = root.objectForKey(key);
        if (obj == null) {
            throw new PlistParseException("Error parsing property with key: " + key);
        }
        try {
            return Double.parseDouble(obj.toString());
        } catch (NumberFormatException e) {
            throw new PlistParseException("Property with key: " + key + " is not a double");
        }
    }
}
