package ch.nth.simpleplist.parser;

import ch.nth.simpleplist.annotation.Array;
import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;
import ch.nth.simpleplist.util.CollectionFactory;
import ch.nth.simpleplist.util.TypeUtils;
import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collection;

class DdTraverser implements Traverser<NSDictionary> {
    private final DdProvider mProvider;

    DdTraverser(NSDictionary source) {
        this(new DdProvider(source));
    }

    DdTraverser(DdProvider provider) {
        this.mProvider = provider;
    }

    public <T> T read(Class<T> type) throws PlistParseException {
        return read(type, new Path(""));
    }

    public <T> T read(Class<T> type, Path path) throws PlistParseException {
        try {
            Object instance = TypeUtils.getInstance(type);
            scan(type, instance, path);
            return instance;
        } catch (Exception e) {
            throw new PlistParseException("Can't instantiate type - " + type.getCanonicalName());
        }
    }

    public void scan(Class type, Object object, Path rootPath) throws PlistParseException {
        Class current = type;
        do {
            for (Field field : current.getDeclaredFields()) {
                for (Annotation annotation : field.getAnnotations()) {
                    scan(field, annotation, object, rootPath);
                }
            }
            current = current.getSuperclass();
        } while (current != null);
    }

    public void scan(Field field, Annotation label, Object object, Path rootPath) throws PlistParseException {
        if (label instanceof Property) {
            processProperty(field, (Property) label, object, rootPath);
        } else if (label instanceof Dictionary) {
            processDictionary(field, (Dictionary) label, object, rootPath);
        } else if (label instanceof Array) {
            processArray(field, (Array) label, object, rootPath);
        }
    }

    public void processProperty(Field field, Property label, Object object, Path rootPath) throws PlistParseException {
        field.setAccessible(true);
        if (field.getType() == String.class) {
            try {
                field.set(object, this.mProvider.getString(label.name(), Path.rootForProperty(label, rootPath)));
            } catch (PlistParseException e) {
                if (label.required()) {
                    throw new PlistParseException("[Property] " + rootPath + " " + label + " " + e.getMessage(), e);
                }
                System.out.println("Can't process property: " + label.name());
            } catch (IllegalAccessException | IllegalArgumentException e2) {
            }
        } else if (field.getType() == Boolean.TYPE || field.getType() == Boolean.class) {
            try {
                field.setBoolean(object, this.mProvider.getBoolean(label.name(), Path.rootForProperty(label, rootPath), label.stringCompatibility()));
            } catch (PlistParseException e3) {
                if (label.required()) {
                    throw new PlistParseException("[Property] " + rootPath + " " + label + " " + e3.getMessage(), e3);
                }
                System.out.println("Can't process property: " + label.name());
            } catch (IllegalAccessException | IllegalArgumentException e4) {
            }
        } else if (field.getType() == Integer.TYPE || field.getType() == Integer.class) {
            try {
                field.setInt(object, this.mProvider.getInt(label.name(), Path.rootForProperty(label, rootPath), label.stringCompatibility()));
            } catch (PlistParseException e5) {
                if (label.required()) {
                    throw new PlistParseException("[Property] " + rootPath + " " + label + " " + e5.getMessage(), e5);
                }
                System.out.println("Can't process property: " + label.name());
            } catch (IllegalAccessException | IllegalArgumentException e6) {
            }
        } else if (field.getType() == Float.TYPE || field.getType() == Float.class) {
            try {
                field.setFloat(object, this.mProvider.getFloat(label.name(), Path.rootForProperty(label, rootPath), label.stringCompatibility()));
            } catch (PlistParseException e7) {
                if (label.required()) {
                    throw new PlistParseException("[Property] " + rootPath + " " + label + " " + e7.getMessage(), e7);
                }
                System.out.println("Can't process property: " + label.name());
            } catch (IllegalAccessException | IllegalArgumentException e8) {
            }
        } else if (field.getType() == Double.TYPE || field.getType() == Double.class) {
            try {
                field.setDouble(object, this.mProvider.getDouble(label.name(), Path.rootForProperty(label, rootPath), label.stringCompatibility()));
            } catch (PlistParseException e9) {
                if (label.required()) {
                    throw new PlistParseException("[Property] " + rootPath + " " + label + " " + e9.getMessage(), e9);
                }
                System.out.println("Can't process property: " + label.name());
            } catch (IllegalAccessException | IllegalArgumentException e10) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ch.nth.simpleplist.parser.DdProvider.getDictionary(java.lang.String, ch.nth.simpleplist.parser.Path):com.dd.plist.NSDictionary
     arg types: [java.lang.String, ch.nth.simpleplist.parser.Path]
     candidates:
      ch.nth.simpleplist.parser.DdProvider.getDictionary(java.lang.String, ch.nth.simpleplist.parser.Path):java.lang.Object
      ch.nth.simpleplist.parser.Provider.getDictionary(java.lang.String, ch.nth.simpleplist.parser.Path):T
      ch.nth.simpleplist.parser.DdProvider.getDictionary(java.lang.String, ch.nth.simpleplist.parser.Path):com.dd.plist.NSDictionary */
    public void processDictionary(Field field, Dictionary label, Object object, Path rootPath) throws PlistParseException {
        Object instance = null;
        try {
            this.mProvider.getDictionary(label.name(), rootPath);
            instance = TypeUtils.getInstance(field.getType());
            field.setAccessible(true);
            field.set(object, instance);
        } catch (PlistParseException e) {
            if (label.required()) {
                throw new PlistParseException("[Dictionary Missing] " + rootPath + " " + label + " " + e.getMessage(), e);
            }
            System.out.println("[Dictionary Missing] " + rootPath + " " + label + " " + e.getMessage());
        } catch (Exception e2) {
            if (label.required()) {
                throw new PlistParseException("Can't process dictionary: " + label.name() + ", info: " + e2.getMessage(), e2);
            }
            System.out.println("Can't process dictionary: " + label.name() + ", info: " + e2.getMessage());
        }
        if (instance != null) {
            try {
                scan(field.getType(), instance, Path.rootForDictionary(label, rootPath));
            } catch (PlistParseException e3) {
                if (label.required()) {
                    throw new PlistParseException("[Dictionary Parsing Error] " + rootPath + " " + label + " " + e3.getMessage(), e3);
                }
                System.out.println("[Dictionary Parsing Error] " + rootPath + " " + label + " " + e3.getMessage());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ch.nth.simpleplist.parser.DdProvider.getArray(java.lang.String, ch.nth.simpleplist.parser.Path):com.dd.plist.NSArray
     arg types: [java.lang.String, ch.nth.simpleplist.parser.Path]
     candidates:
      ch.nth.simpleplist.parser.DdProvider.getArray(java.lang.String, ch.nth.simpleplist.parser.Path):java.lang.Object
      ch.nth.simpleplist.parser.Provider.getArray(java.lang.String, ch.nth.simpleplist.parser.Path):S
      ch.nth.simpleplist.parser.DdProvider.getArray(java.lang.String, ch.nth.simpleplist.parser.Path):com.dd.plist.NSArray */
    public void processArray(Field field, Array label, Object object, Path rootPath) throws PlistParseException {
        field.setAccessible(true);
        try {
            this.mProvider.getArray(label.name(), Path.rootForProperty(label, rootPath));
        } catch (PlistParseException e) {
            if (label.required()) {
                throw new PlistParseException("[Array Missing] " + rootPath + " " + label + " " + e.getMessage(), e);
            }
            System.out.println("[Array Missing] " + rootPath + " " + label + " " + e.getMessage());
        }
        try {
            Collection collection = CollectionFactory.getInstance(field);
            NSArray array = this.mProvider.getArray(label.name(), Path.rootForProperty(label, rootPath));
            if (!label.dictionary() || label.clazz() == Object.class) {
                for (int i = 0; i < array.count(); i++) {
                    collection.add(array.objectAtIndex(i).toJavaObject());
                }
            } else {
                Class clazz = label.clazz();
                int i2 = 0;
                while (i2 < array.count()) {
                    NSObject arrayObject = array.objectAtIndex(i2);
                    if (arrayObject instanceof NSDictionary) {
                        collection.add(new DdTraverser((NSDictionary) arrayObject).read(clazz, new Path("")));
                        i2++;
                    } else {
                        throw new PlistParseException("Array element: " + label.name() + " is not a dictionary!");
                    }
                }
            }
            field.set(object, collection);
        } catch (PlistParseException e2) {
            if (label.required()) {
                throw new PlistParseException("[Array Parse Error] " + rootPath + " " + label + " " + e2.getMessage(), e2);
            }
            System.out.println("[Array Parse Error] " + rootPath + " " + label + " " + e2.getMessage());
        } catch (Exception ex) {
            if (label.required()) {
                throw new PlistParseException("Can't process array: " + label.name() + " ; info: " + ex.getMessage());
            }
            System.out.println("Can't process array: " + label.name() + " ; info: " + ex.getMessage());
        }
    }
}
