package ch.nth.simpleplist.parser;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import java.util.HashMap;
import java.util.Map;

class DdProvider implements Provider<NSDictionary, NSArray> {
    private final Parser<NSDictionary, NSArray> mParser;
    private final NSDictionary mRoot;
    private final Map<Path, NSDictionary> sDictionaryCache = new HashMap();

    public DdProvider(NSDictionary rootDictionary) {
        this.mRoot = rootDictionary;
        this.mParser = new DdParser();
    }

    public NSDictionary getDictionary(String key, Path path) throws PlistParseException {
        return this.mParser.getDictionary(key, getRootForPath(path));
    }

    public String getString(String key, Path path) throws PlistParseException {
        return this.mParser.getStringProperty(key, getRootForPath(path));
    }

    public int getInt(String key, Path path, boolean stringCompatibility) throws PlistParseException {
        if (stringCompatibility) {
            return this.mParser.getGenericIntegerProperty(key, getRootForPath(path));
        }
        return this.mParser.getIntegerProperty(key, getRootForPath(path));
    }

    public float getFloat(String key, Path path, boolean stringCompatibility) throws PlistParseException {
        if (stringCompatibility) {
            return this.mParser.getGenericFloatProperty(key, getRootForPath(path));
        }
        return this.mParser.getFloatProperty(key, getRootForPath(path));
    }

    public double getDouble(String key, Path path, boolean stringCompatibility) throws PlistParseException {
        if (stringCompatibility) {
            return this.mParser.getGenericDoubleProperty(key, getRootForPath(path));
        }
        return this.mParser.getDoubleProperty(key, getRootForPath(path));
    }

    public boolean getBoolean(String key, Path path, boolean stringCompatibility) throws PlistParseException {
        if (stringCompatibility) {
            return this.mParser.getGenericBooleanProperty(key, getRootForPath(path));
        }
        return this.mParser.getBooleanProperty(key, getRootForPath(path));
    }

    public NSArray getArray(String key, Path path) throws PlistParseException {
        return this.mParser.getArray(key, getRootForPath(path));
    }

    private NSDictionary getRootForPath(Path path) {
        if (path == null) {
            return this.mRoot;
        }
        if (!this.sDictionaryCache.containsKey(path)) {
            this.sDictionaryCache.put(path, path.getRoot(this.mRoot));
        }
        return this.sDictionaryCache.get(path);
    }
}
