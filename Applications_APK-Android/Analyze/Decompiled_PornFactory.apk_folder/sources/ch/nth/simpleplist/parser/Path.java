package ch.nth.simpleplist.parser;

import ch.nth.simpleplist.annotation.Array;
import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;
import ch.nth.simpleplist.util.TextUtils;
import com.dd.plist.NSDictionary;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class Path {
    private static final String sDelimiter = "/";
    private List<String> mPathElements;

    public Path(String fullPath) {
        this(fullPath, sDelimiter);
    }

    public Path(String fullPath, String delimiter) {
        this(getPathElements(fullPath, delimiter));
    }

    public Path(List<String> pathElements) {
        this.mPathElements = new ArrayList();
        this.mPathElements.addAll(pathElements);
    }

    public NSDictionary getRoot(NSDictionary root) {
        return getRoot(root, this.mPathElements);
    }

    private NSDictionary getRoot(NSDictionary root, List<String> pathElements) {
        NSDictionary dict = root;
        Iterator<String> it = pathElements.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String pathElement = it.next();
            if (dict == null || !dict.containsKey(pathElement)) {
                System.out.println("Couldn't find path element: " + pathElement + " in path: " + TextUtils.concatStringsWithSeparator(this.mPathElements, sDelimiter));
            } else {
                dict = (NSDictionary) dict.objectForKey(pathElement);
            }
        }
        return dict;
    }

    public Path withNewPathSegment(String newSegment) {
        if (TextUtils.isEmpty(newSegment)) {
            return new Path(this.mPathElements);
        }
        List<String> newPathElements = new ArrayList<>(this.mPathElements);
        newPathElements.addAll(getPathElements(newSegment));
        return new Path(newPathElements);
    }

    public static List<String> getPathElements(String pathString) {
        return getPathElements(pathString, sDelimiter);
    }

    public static List<String> getPathElements(String pathString, String delimiter) {
        if (TextUtils.isEmpty(pathString)) {
            return new ArrayList();
        }
        return Arrays.asList(pathString.split(delimiter));
    }

    public static Path rootForProperty(Property property, Path currentRoot) {
        if (TextUtils.isEmpty(property.path())) {
            return currentRoot;
        }
        return currentRoot.withNewPathSegment(property.path());
    }

    public static Path rootForProperty(Array property, Path currentRoot) {
        if (TextUtils.isEmpty(property.path())) {
            return currentRoot;
        }
        return currentRoot.withNewPathSegment(property.path());
    }

    public static Path rootForDictionary(Dictionary dictionary, Path currentRoot) {
        if (TextUtils.isEmpty(dictionary.path())) {
            return currentRoot.withNewPathSegment(dictionary.name());
        }
        return currentRoot.withNewPathSegment(dictionary.path()).withNewPathSegment(dictionary.name());
    }

    public String toString() {
        return "Path { " + TextUtils.concatStringsWithSeparator(this.mPathElements, sDelimiter) + " }";
    }
}
