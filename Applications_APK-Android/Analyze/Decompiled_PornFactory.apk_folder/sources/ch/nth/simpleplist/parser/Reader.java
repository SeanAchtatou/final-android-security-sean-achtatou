package ch.nth.simpleplist.parser;

import java.io.File;
import java.io.InputStream;

public interface Reader {
    <T> T read(Class cls, File file) throws PlistParseException;

    <T> T read(Class cls, InputStream inputStream) throws PlistParseException;

    <T> T read(Class cls, String str) throws PlistParseException;

    <T> T read(Class cls, byte[] bArr) throws PlistParseException;
}
