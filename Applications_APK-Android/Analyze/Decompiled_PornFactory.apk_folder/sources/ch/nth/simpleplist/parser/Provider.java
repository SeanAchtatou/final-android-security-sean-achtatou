package ch.nth.simpleplist.parser;

interface Provider<T, S> {
    S getArray(String str, Path path) throws PlistParseException;

    boolean getBoolean(String str, Path path, boolean z) throws PlistParseException;

    T getDictionary(String str, Path path) throws PlistParseException;

    double getDouble(String str, Path path, boolean z) throws PlistParseException;

    float getFloat(String str, Path path, boolean z) throws PlistParseException;

    int getInt(String str, Path path, boolean z) throws PlistParseException;

    String getString(String str, Path path) throws PlistParseException;
}
