package ch.nth.android.lang;

public enum Language {
    ENGLISH("en", "eng"),
    GERMAN("de", "deu"),
    ITALIAN("it", "ita"),
    FRENCH("fr", "fra"),
    CROATIAN("hr", "hrv"),
    SERBIAN("sr", "srp"),
    SLOVENE("sl", "slv"),
    HUNGARIAN("hu", "hun"),
    MACEDONIAN("mk", "mkd"),
    BOSNIAN("bs", "bos"),
    ROMANIAN("ro", "ron"),
    CZECH("cs", "ces"),
    SLOVAK("sk", "slk"),
    POLISH("pl", "pol"),
    MONTENEGRIN("me", "mne");
    
    private String mThreeLetterCode;
    private String mTwoLetterCode;

    private Language(String twoLetterCode, String threeLetterCode) {
        this.mTwoLetterCode = twoLetterCode;
        this.mThreeLetterCode = threeLetterCode;
    }

    public String getTwoLetterCode() {
        return this.mTwoLetterCode;
    }

    public String getThreeLetterCode() {
        return this.mThreeLetterCode;
    }

    public static Language getDefault() {
        return ENGLISH;
    }
}
