package ch.nth.android.lang;

public class StringWrapper {
    private String value;

    public StringWrapper(String value2) {
        this.value = value2;
    }

    public String toString() {
        return this.value;
    }
}
