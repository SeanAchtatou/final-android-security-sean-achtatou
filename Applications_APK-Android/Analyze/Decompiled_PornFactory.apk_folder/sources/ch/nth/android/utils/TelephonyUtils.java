package ch.nth.android.utils;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

public class TelephonyUtils {
    private static final String TAG = TelephonyUtils.class.getSimpleName();

    private TelephonyUtils() {
    }

    static TelephonyManager getTelephonyManager(Context context) {
        return (TelephonyManager) context.getSystemService("phone");
    }

    public static String getIMEI(Context context) {
        if (context == null) {
            return null;
        }
        return getTelephonyManager(context).getDeviceId();
    }

    public static String getMSISDN(Context context) {
        if (context == null) {
            return null;
        }
        return getTelephonyManager(context).getLine1Number();
    }

    public static String getMCC(Context context) {
        if (context == null) {
            return null;
        }
        try {
            String simOperator = getTelephonyManager(context).getSimOperator();
            if (!TextUtils.isEmpty(simOperator)) {
                return simOperator.substring(0, 3);
            }
            return null;
        } catch (Exception e) {
            Log.d(TAG, "Failed to get MCC value");
            return null;
        }
    }

    public static String getMNC(Context context) {
        if (context == null) {
            return null;
        }
        try {
            String simOperator = getTelephonyManager(context).getSimOperator();
            if (!TextUtils.isEmpty(simOperator)) {
                return simOperator.substring(3);
            }
            return null;
        } catch (Exception e) {
            Log.d(TAG, "Failed to get MNC value");
            return null;
        }
    }

    public static String getMCCMNCTuple(Context context) {
        if (context == null) {
            return null;
        }
        try {
            return getTelephonyManager(context).getSimOperator();
        } catch (Exception e) {
            Log.d(TAG, "Failed to get MCC/MNC tuple");
            return null;
        }
    }
}
