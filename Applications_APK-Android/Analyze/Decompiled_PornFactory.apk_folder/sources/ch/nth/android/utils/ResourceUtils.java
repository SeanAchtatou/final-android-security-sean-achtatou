package ch.nth.android.utils;

import android.content.Context;
import android.support.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ResourceUtils {
    public static final String RESOURCE_TYPE_DRAWABLE = "drawable";
    public static final String RESOURCE_TYPE_ID = "id";
    public static final String RESOURCE_TYPE_LAYOUT = "layout";
    public static final String RESOURCE_TYPE_STRING = "string";

    @StringDef({"string", "id", "layout", "drawable"})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ResourceType {
    }

    private ResourceUtils() {
    }

    public static String getString(Context context, String name) {
        int identifier;
        if (context == null || (identifier = getResourceId(context, name, RESOURCE_TYPE_STRING)) == 0) {
            return "";
        }
        return context.getString(identifier);
    }

    public static int getDrawableResourceId(Context context, String name) {
        if (context == null) {
            return 0;
        }
        return getResourceId(context, name, RESOURCE_TYPE_DRAWABLE);
    }

    public static int getResourceId(Context context, String resourceName, String resourceType) {
        if (context == null) {
            return 0;
        }
        return context.getResources().getIdentifier(resourceName, resourceType, context.getPackageName());
    }
}
