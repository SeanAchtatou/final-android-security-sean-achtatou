package ch.nth.android.utils;

import android.util.Log;
import java.io.Closeable;
import java.util.regex.Pattern;

public class JavaUtils {
    private static final String TAG = JavaUtils.class.getSimpleName();

    private enum RegexMode {
        MATCH,
        FIND
    }

    private JavaUtils() {
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
            }
        }
    }

    public static boolean equal(Object a, Object b) {
        return a == b || (a != null && a.equals(b));
    }

    public static boolean regexMatch(String regex, CharSequence text) {
        return regexCheck(regex, text, RegexMode.MATCH);
    }

    public static boolean regexMatch(Pattern pattern, CharSequence text) {
        return regexCheck(pattern, text, RegexMode.MATCH);
    }

    public static boolean regexFind(String regex, CharSequence text) {
        return regexCheck(regex, text, RegexMode.FIND);
    }

    public static boolean regexFind(Pattern pattern, CharSequence text) {
        return regexCheck(pattern, text, RegexMode.FIND);
    }

    private static boolean regexCheck(String regex, CharSequence text, RegexMode mode) {
        if (regex == null) {
            return false;
        }
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(regex);
        } catch (Exception e) {
            Log.w(TAG, "exception while compiling a regex pattern", e);
        }
        return regexCheck(pattern, text, mode);
    }

    private static boolean regexCheck(Pattern pattern, CharSequence text, RegexMode mode) {
        if (pattern == null || text == null) {
            return false;
        }
        switch (mode) {
            case MATCH:
                return pattern.matcher(text).matches();
            case FIND:
                return pattern.matcher(text).find();
            default:
                return false;
        }
    }
}
