package ch.nth.android.utils;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class ViewUtils {
    private ViewUtils() {
    }

    public static void setText(@Nullable TextView textView, int resId) {
        if (textView != null) {
            textView.setText(resId);
        }
    }

    public static void setText(@Nullable TextView textView, @Nullable CharSequence text) {
        if (textView != null) {
            textView.setText(text);
        }
    }

    public static void setViewVisible(@Nullable View view, boolean visible, boolean animate) {
        if (view != null) {
            if (visible) {
                if (view.getVisibility() != 0) {
                    if (animate) {
                        view.startAnimation(AnimationUtils.loadAnimation(view.getContext(), 17432576));
                    } else {
                        view.clearAnimation();
                    }
                    view.setVisibility(0);
                }
            } else if (view.getVisibility() != 8) {
                if (animate) {
                    view.startAnimation(AnimationUtils.loadAnimation(view.getContext(), 17432577));
                } else {
                    view.clearAnimation();
                }
                view.setVisibility(8);
            }
        }
    }
}
