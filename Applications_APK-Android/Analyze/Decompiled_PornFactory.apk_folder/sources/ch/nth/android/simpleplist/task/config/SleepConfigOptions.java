package ch.nth.android.simpleplist.task.config;

public class SleepConfigOptions extends ConfigOptions {
    private int mSleepDurationMs;

    public SleepConfigOptions(int sleepDurationMs) {
        this.mSleepDurationMs = sleepDurationMs;
    }

    public int getSleepDurationMs() {
        return this.mSleepDurationMs;
    }

    public static class Builder {
        int sleepDurationMs;

        public Builder(int sleepDurationMs2) {
            this.sleepDurationMs = sleepDurationMs2;
        }

        public SleepConfigOptions build() {
            return new SleepConfigOptions(this.sleepDurationMs);
        }
    }
}
