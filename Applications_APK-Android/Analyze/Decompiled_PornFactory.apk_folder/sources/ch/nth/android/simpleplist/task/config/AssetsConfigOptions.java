package ch.nth.android.simpleplist.task.config;

import android.text.TextUtils;
import ch.nth.android.fetcher.config.AssetsFetchOptions;

public class AssetsConfigOptions extends ConfigOptions {
    private String mAssetsDirectory = AssetsFetchOptions.PLIST_DEFAULT_ASSETS_DIRECTORY;
    private String mFilename = AssetsFetchOptions.PLIST_DEFAULT_ASSETS_FILENAME;

    public String getAssetsDirectory() {
        return this.mAssetsDirectory;
    }

    /* access modifiers changed from: private */
    public void setAssetsDirectory(String assetsDirectory) {
        if (assetsDirectory != null) {
            this.mAssetsDirectory = assetsDirectory;
        }
    }

    public String getFilename() {
        return this.mFilename;
    }

    /* access modifiers changed from: private */
    public void setFilename(String filename) {
        if (!TextUtils.isEmpty(filename)) {
            this.mFilename = filename;
        }
    }

    public static class Builder {
        private String assetsDirectory;
        private String filename;

        public Builder assetsDirectory(String assetsDirectory2) {
            this.assetsDirectory = assetsDirectory2;
            return this;
        }

        public Builder filename(String filename2) {
            this.filename = filename2;
            return this;
        }

        public AssetsConfigOptions build() {
            AssetsConfigOptions options = new AssetsConfigOptions();
            options.setAssetsDirectory(this.assetsDirectory);
            options.setFilename(this.filename);
            return options;
        }
    }
}
