package ch.nth.android.simpleplist.task.config;

public class RemoteConfigOptions extends ConfigOptionsUsingCache {
    /* access modifiers changed from: private */
    public String mCharset;
    /* access modifiers changed from: private */
    public int mNetworkTimeout;
    private String mRemoteUrl;
    /* access modifiers changed from: private */
    public boolean mShouldCacheOnSuccess;

    public /* bridge */ /* synthetic */ String getCacheDirectory() {
        return super.getCacheDirectory();
    }

    public /* bridge */ /* synthetic */ String getCachedFileName() {
        return super.getCachedFileName();
    }

    public RemoteConfigOptions(String remoteUrl) {
        this.mRemoteUrl = remoteUrl;
    }

    public String getRemoteUrl() {
        return this.mRemoteUrl;
    }

    public boolean isShouldCacheOnSuccess() {
        return this.mShouldCacheOnSuccess;
    }

    public int getNetworkTimeout() {
        return this.mNetworkTimeout;
    }

    public String getCharset() {
        return this.mCharset;
    }

    public static class Builder {
        private String cacheDirectory;
        private String cachedFileName;
        private String charset;
        private int networkTimeout;
        private String remoteUrl;
        private boolean shouldCacheOnSuccess;

        public Builder(String remoteUrl2) {
            this.remoteUrl = remoteUrl2;
        }

        public Builder cacheOnSuccess(boolean shouldCacheOnSuccess2) {
            this.shouldCacheOnSuccess = shouldCacheOnSuccess2;
            return this;
        }

        public Builder timeout(int networkTimeout2) {
            this.networkTimeout = networkTimeout2;
            return this;
        }

        public Builder cacheDirectory(String cacheDirectory2) {
            this.cacheDirectory = cacheDirectory2;
            return this;
        }

        public Builder cachedFileName(String cachedFileName2) {
            this.cachedFileName = cachedFileName2;
            return this;
        }

        public RemoteConfigOptions build() {
            RemoteConfigOptions options = new RemoteConfigOptions(this.remoteUrl);
            options.mShouldCacheOnSuccess = this.shouldCacheOnSuccess;
            options.mNetworkTimeout = this.networkTimeout;
            options.mCharset = this.charset;
            options.setCacheDirectory(this.cacheDirectory);
            options.setCachedFileName(this.cachedFileName);
            return options;
        }
    }
}
