package ch.nth.android.simpleplist.task;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.simpleplist.cache.DiskCache;
import ch.nth.android.simpleplist.debug.Debugger;
import ch.nth.android.simpleplist.debug.LogEntry;
import ch.nth.android.simpleplist.task.config.AssetsConfigOptions;
import ch.nth.android.simpleplist.task.config.CacheConfigOptions;
import ch.nth.android.simpleplist.task.config.ConfigOptions;
import ch.nth.android.simpleplist.task.config.ConfigType;
import ch.nth.android.simpleplist.task.config.RemoteConfigOptions;
import ch.nth.android.simpleplist.task.config.SleepConfigOptions;
import ch.nth.simpleplist.parser.DdReader;
import ch.nth.simpleplist.parser.PlistParseException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class PlistAsyncTask<T> extends AsyncTask<Void, Void, T> {
    private static final String TAG = PlistAsyncTask.class.getSimpleName();
    private boolean debugMode = false;
    private Debugger debugger;
    private Class<? extends T> mClass;
    private ConfigType mConfigType;
    private Context mContext;
    private TaskOptions mOptions;

    public PlistAsyncTask(Context context, TaskOptions options, Class<? extends T> clazz) {
        this.mContext = context;
        this.mOptions = options;
        this.mClass = clazz;
    }

    @TargetApi(11)
    public void executeAsync() {
        if (Build.VERSION.SDK_INT >= 11) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        } else {
            execute((Object[]) null);
        }
    }

    /* access modifiers changed from: protected */
    public T doInBackground(Void... params) {
        if (this.mOptions == null || this.mOptions.getSteps() == null) {
            return null;
        }
        if (this.mOptions.isDebugMode()) {
            this.debugMode = true;
            this.debugger = new Debugger();
        }
        T config = null;
        Iterator<ConfigOptions> it = this.mOptions.getSteps().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            ConfigOptions options = it.next();
            if (!isCancelled()) {
                if (options instanceof RemoteConfigOptions) {
                    config = getRemote((RemoteConfigOptions) options);
                    if (config == null) {
                        continue;
                    } else {
                        if (this.debugMode) {
                            addDebugMessage("remote config success");
                        }
                        this.mConfigType = ConfigType.REMOTE;
                        continue;
                    }
                } else if (options instanceof CacheConfigOptions) {
                    config = getCached((CacheConfigOptions) options);
                    if (config == null) {
                        continue;
                    } else {
                        if (this.debugMode) {
                            addDebugMessage("cached config success");
                        }
                        this.mConfigType = ConfigType.CACHE;
                        continue;
                    }
                } else if (options instanceof AssetsConfigOptions) {
                    config = getAssets((AssetsConfigOptions) options);
                    if (config == null) {
                        continue;
                    } else {
                        if (this.debugMode) {
                            addDebugMessage("assets config success");
                        }
                        this.mConfigType = ConfigType.ASSETS;
                        continue;
                    }
                } else if (options instanceof SleepConfigOptions) {
                    SleepConfigOptions sleepOptions = (SleepConfigOptions) options;
                    try {
                        if (this.debugMode) {
                            addDebugMessage("sleep started");
                        }
                        Thread.sleep((long) sleepOptions.getSleepDurationMs());
                        if (this.debugMode) {
                            addDebugMessage("sleep ended");
                            continue;
                        } else {
                            continue;
                        }
                    } catch (InterruptedException e) {
                        Log.e(TAG, "Sleep InterruptedException", e);
                        continue;
                    }
                } else {
                    continue;
                }
                if (config != null) {
                    if (this.debugMode) {
                        addDebugMessage("config instantiated - breaking and finishing");
                    }
                }
            } else if (this.debugMode) {
                addDebugMessage("task cancelled - breaking and finishing");
            }
        }
        printDebugMessages(true);
        return config;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.lang.String):T
     arg types: [java.lang.Object, java.lang.String]
     candidates:
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.io.File):T
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.io.InputStream):T
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, byte[]):T
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.lang.String):T */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private T getRemote(ch.nth.android.simpleplist.task.config.RemoteConfigOptions r16) {
        /*
            r15 = this;
            org.apache.http.impl.client.DefaultHttpClient r2 = new org.apache.http.impl.client.DefaultHttpClient
            r2.<init>()
            org.apache.http.params.HttpParams r7 = r2.getParams()
            int r12 = r16.getNetworkTimeout()
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r7, r12)
            int r12 = r16.getNetworkTimeout()
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r7, r12)
            r9 = 0
            r3 = 0
            org.apache.http.client.methods.HttpGet r6 = new org.apache.http.client.methods.HttpGet     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            java.lang.String r12 = r16.getRemoteUrl()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            r6.<init>(r12)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            boolean r12 = r15.debugMode     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            if (r12 == 0) goto L_0x002b
            java.lang.String r12 = "started remote fetch"
            r15.addDebugMessage(r12)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
        L_0x002b:
            org.apache.http.HttpResponse r8 = r2.execute(r6)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            boolean r12 = r15.debugMode     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            if (r12 == 0) goto L_0x0038
            java.lang.String r12 = "remote fetched without exceptions"
            r15.addDebugMessage(r12)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
        L_0x0038:
            org.apache.http.HttpEntity r5 = r8.getEntity()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            if (r5 != 0) goto L_0x0040
            r3 = 0
        L_0x003f:
            return r3
        L_0x0040:
            java.lang.String r12 = r16.getCharset()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            boolean r12 = android.text.TextUtils.isEmpty(r12)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            if (r12 == 0) goto L_0x0065
            java.lang.String r1 = "UTF-8"
        L_0x004c:
            java.lang.String r9 = org.apache.http.util.EntityUtils.toString(r5, r1)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            org.apache.http.StatusLine r11 = r8.getStatusLine()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            boolean r12 = r15.isCancelled()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            if (r12 == 0) goto L_0x006a
            boolean r12 = r15.debugMode     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            if (r12 == 0) goto L_0x0063
            java.lang.String r12 = "task cancelled - aborting remote data deserialization"
            r15.addDebugMessage(r12)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
        L_0x0063:
            r3 = 0
            goto L_0x003f
        L_0x0065:
            java.lang.String r1 = r16.getCharset()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            goto L_0x004c
        L_0x006a:
            int r12 = r11.getStatusCode()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            r13 = 200(0xc8, float:2.8E-43)
            if (r12 < r13) goto L_0x00e0
            int r12 = r11.getStatusCode()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            r13 = 300(0x12c, float:4.2E-43)
            if (r12 >= r13) goto L_0x00e0
            boolean r12 = r15.debugMode     // Catch:{ IllegalStateException -> 0x00bc, PlistParseException -> 0x00ce }
            if (r12 == 0) goto L_0x0083
            java.lang.String r12 = "started remote deserialize"
            r15.addDebugMessage(r12)     // Catch:{ IllegalStateException -> 0x00bc, PlistParseException -> 0x00ce }
        L_0x0083:
            ch.nth.simpleplist.parser.DdReader r10 = new ch.nth.simpleplist.parser.DdReader     // Catch:{ IllegalStateException -> 0x00bc, PlistParseException -> 0x00ce }
            r10.<init>()     // Catch:{ IllegalStateException -> 0x00bc, PlistParseException -> 0x00ce }
            java.lang.Class<? extends T> r12 = r15.mClass     // Catch:{ IllegalStateException -> 0x00bc, PlistParseException -> 0x00ce }
            java.lang.Object r3 = r10.read(r12, r9)     // Catch:{ IllegalStateException -> 0x00bc, PlistParseException -> 0x00ce }
            boolean r12 = r15.debugMode     // Catch:{ IllegalStateException -> 0x00bc, PlistParseException -> 0x00ce }
            if (r12 == 0) goto L_0x0097
            java.lang.String r12 = "ended remote deserialize"
            r15.addDebugMessage(r12)     // Catch:{ IllegalStateException -> 0x00bc, PlistParseException -> 0x00ce }
        L_0x0097:
            boolean r12 = r15.debugMode
            if (r12 == 0) goto L_0x00a0
            java.lang.String r12 = "ended remote fetch"
            r15.addDebugMessage(r12)
        L_0x00a0:
            if (r3 == 0) goto L_0x003f
            boolean r12 = r16.isShouldCacheOnSuccess()
            if (r12 == 0) goto L_0x003f
            android.content.Context r12 = r15.mContext
            if (r12 == 0) goto L_0x003f
            boolean r12 = r15.isCancelled()
            if (r12 == 0) goto L_0x0102
            boolean r12 = r15.debugMode
            if (r12 == 0) goto L_0x003f
            java.lang.String r12 = "task cancelled - aborting cache"
            r15.addDebugMessage(r12)
            goto L_0x003f
        L_0x00bc:
            r4 = move-exception
            java.lang.String r12 = ch.nth.android.simpleplist.task.PlistAsyncTask.TAG     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            java.lang.String r13 = "getRemote() - Parser IllegalStateException"
            android.util.Log.e(r12, r13, r4)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            goto L_0x0097
        L_0x00c5:
            r4 = move-exception
            java.lang.String r12 = ch.nth.android.simpleplist.task.PlistAsyncTask.TAG
            java.lang.String r13 = "getRemote() - HttpClient ClientProtocolException"
            android.util.Log.e(r12, r13, r4)
            goto L_0x0097
        L_0x00ce:
            r4 = move-exception
            java.lang.String r12 = ch.nth.android.simpleplist.task.PlistAsyncTask.TAG     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            java.lang.String r13 = "getRemote() - Parser PlistParseException"
            android.util.Log.e(r12, r13, r4)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            goto L_0x0097
        L_0x00d7:
            r4 = move-exception
            java.lang.String r12 = ch.nth.android.simpleplist.task.PlistAsyncTask.TAG
            java.lang.String r13 = "getRemote() - HttpClient/EntityUtils IOException"
            android.util.Log.e(r12, r13, r4)
            goto L_0x0097
        L_0x00e0:
            java.lang.String r12 = ch.nth.android.simpleplist.task.PlistAsyncTask.TAG     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            java.lang.String r14 = "getRemote() - Error communicating with server, status code: "
            r13.<init>(r14)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            int r14 = r11.getStatusCode()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            java.lang.String r13 = r13.toString()     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            android.util.Log.w(r12, r13)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x00d7, ParseException -> 0x00f9 }
            goto L_0x0097
        L_0x00f9:
            r4 = move-exception
            java.lang.String r12 = ch.nth.android.simpleplist.task.PlistAsyncTask.TAG
            java.lang.String r13 = "getRemote() - EntityUtils ParseException"
            android.util.Log.e(r12, r13, r4)
            goto L_0x0097
        L_0x0102:
            boolean r12 = r15.debugMode
            if (r12 == 0) goto L_0x010b
            java.lang.String r12 = "start caching"
            r15.addDebugMessage(r12)
        L_0x010b:
            ch.nth.android.simpleplist.cache.DiskCache r0 = new ch.nth.android.simpleplist.cache.DiskCache
            android.content.Context r12 = r15.mContext
            java.lang.String r13 = r16.getCacheDirectory()
            r0.<init>(r12, r13)
            java.lang.String r12 = r16.getCachedFileName()
            r0.put(r12, r9)
            boolean r12 = r15.debugMode
            if (r12 == 0) goto L_0x003f
            java.lang.String r12 = "caching completed"
            r15.addDebugMessage(r12)
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.simpleplist.task.PlistAsyncTask.getRemote(ch.nth.android.simpleplist.task.config.RemoteConfigOptions):java.lang.Object");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.lang.String):T
     arg types: [java.lang.Object, java.lang.String]
     candidates:
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.io.File):T
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.io.InputStream):T
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, byte[]):T
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.lang.String):T */
    private T getCached(CacheConfigOptions options) {
        T config = null;
        if (!(options == null || this.mContext == null)) {
            if (this.debugMode) {
                addDebugMessage("started getting cached");
            }
            String cachedContent = new DiskCache(this.mContext, options.getCacheDirectory()).get(options.getCachedFileName());
            if (!TextUtils.isEmpty(cachedContent)) {
                if (this.debugMode) {
                    addDebugMessage("started cached deserialize");
                }
                try {
                    config = new DdReader().read((Class) this.mClass, cachedContent);
                } catch (PlistParseException e) {
                    Log.e(TAG, "getCached() - Parser PlistParseException", e);
                }
                if (this.debugMode) {
                    addDebugMessage("ended cached deserialize");
                }
            } else {
                if (this.debugMode) {
                    addDebugMessage("cached entry not found");
                }
                Log.w(TAG, "getCached() - cached entry not found");
            }
        }
        return config;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.io.InputStream):T
     arg types: [java.lang.Object, java.io.InputStream]
     candidates:
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.io.File):T
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.lang.String):T
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, byte[]):T
      ch.nth.simpleplist.parser.Reader.read(java.lang.Class, java.io.InputStream):T */
    private T getAssets(AssetsConfigOptions options) {
        T config = null;
        InputStream inputStream = null;
        if (this.debugMode) {
            addDebugMessage("started getting assets");
        }
        try {
            inputStream = this.mContext.getAssets().open(String.valueOf(options.getAssetsDirectory()) + options.getFilename());
        } catch (IOException e) {
            if (this.debugMode) {
                addDebugMessage("assets entry not found");
            }
            Log.e(TAG, "getAssets() - Assets IOException", e);
        }
        if (inputStream != null) {
            if (this.debugMode) {
                addDebugMessage("started assets deserialize");
            }
            try {
                config = new DdReader().read((Class) this.mClass, inputStream);
            } catch (PlistParseException e2) {
                Log.e(TAG, "getAssets() - Parser PlistParseException", e2);
            }
            if (this.debugMode) {
                addDebugMessage("ended assets deserialize");
            }
        }
        return config;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(T result) {
        if (this.mOptions != null && this.mOptions.getListener() != null) {
            if (result != null) {
                this.mOptions.getListener().onPlistSucceeded(result, this.mConfigType, this.mOptions.getRequestTag());
            } else {
                this.mOptions.getListener().onPlistFailed(this.mOptions.getRequestTag());
            }
        }
    }

    private void addDebugMessage(String message) {
        if (this.debugger != null) {
            this.debugger.addEntry(new LogEntry(message));
        }
    }

    private void printDebugMessages(boolean cleanAfterPrint) {
        if (this.debugger != null) {
            this.debugger.printAllMessages();
            if (cleanAfterPrint) {
                this.debugger.clearAllMessages();
            }
        }
    }
}
