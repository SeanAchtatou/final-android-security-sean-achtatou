package ch.nth.android.simpleplist.debug;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class Debugger {
    private static final String TAG = Debugger.class.getSimpleName();
    private List<LogEntry> messageLog = new ArrayList();
    private long startTime = System.currentTimeMillis();

    public void addEntry(LogEntry entry) {
        this.messageLog.add(entry);
    }

    public void clearAllMessages() {
        this.messageLog.clear();
    }

    public void printAllMessages() {
        for (LogEntry entry : this.messageLog) {
            Log.d(TAG, String.valueOf(padLeft(String.valueOf(entry.getTimestamp() - this.startTime) + "ms - ", 10)) + entry.getMessage());
        }
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }
}
