package ch.nth.android.simpleplist.task;

import ch.nth.android.simpleplist.task.config.ConfigOptions;
import java.util.ArrayList;
import java.util.List;

public class TaskOptions {
    /* access modifiers changed from: private */
    public boolean mDebugMode;
    /* access modifiers changed from: private */
    public PlistListener mListener;
    /* access modifiers changed from: private */
    public int mRequestTag;
    /* access modifiers changed from: private */
    public List<ConfigOptions> mSteps;

    private TaskOptions() {
    }

    /* synthetic */ TaskOptions(TaskOptions taskOptions) {
        this();
    }

    public PlistListener getListener() {
        return this.mListener;
    }

    public List<ConfigOptions> getSteps() {
        return this.mSteps;
    }

    public int getRequestTag() {
        return this.mRequestTag;
    }

    public boolean isDebugMode() {
        return this.mDebugMode;
    }

    public static class Builder {
        private boolean debugMode;
        private final PlistListener listener;
        private int requestTag;
        private List<ConfigOptions> steps = new ArrayList();

        public Builder(PlistListener listener2) {
            this.listener = listener2;
        }

        public Builder addStep(ConfigOptions option) {
            this.steps.add(option);
            return this;
        }

        public Builder tag(int requestTag2) {
            this.requestTag = requestTag2;
            return this;
        }

        public Builder debug(boolean debugMode2) {
            this.debugMode = debugMode2;
            return this;
        }

        public TaskOptions build() {
            TaskOptions options = new TaskOptions(null);
            options.mListener = this.listener;
            options.mSteps = this.steps;
            options.mRequestTag = this.requestTag;
            options.mDebugMode = this.debugMode;
            return options;
        }
    }
}
