package ch.nth.android.simpleplist.debug;

public class LogEntry {
    private String message;
    private long timestamp = System.currentTimeMillis();

    public LogEntry(String message2) {
        this.message = message2;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getMessage() {
        return this.message;
    }
}
