package ch.nth.android.simpleplist.task.config;

public enum ConfigType {
    REMOTE,
    CACHE,
    ASSETS
}
