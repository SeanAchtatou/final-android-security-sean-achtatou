package ch.nth.android.simpleplist.cache;

import android.content.Context;
import java.io.File;

public class DiskCache {
    private static String DEFAULT_CACHE_DIR = "app_cache";
    private Context mContext;
    private File mRootDirectory;

    public DiskCache(Context context) {
        this(context, DEFAULT_CACHE_DIR);
    }

    public DiskCache(Context context, String cacheDir) {
        this.mContext = context;
        this.mRootDirectory = new File(context.getCacheDir(), cacheDir);
        if (!this.mRootDirectory.exists()) {
            this.mRootDirectory.mkdirs();
        }
    }

    public synchronized void put(String key, String entry) {
        writeFile(getFileForKey(key), entry);
    }

    public synchronized String get(String key) {
        return readFile(getFileForKey(key));
    }

    public synchronized boolean delete(String key) {
        return deleteFile(getFileForKey(key));
    }

    private String getFilenameForKey(String key) {
        int firstHalfLength = key.length() / 2;
        return String.valueOf(String.valueOf(key.substring(0, firstHalfLength).hashCode())) + String.valueOf(key.substring(firstHalfLength).hashCode());
    }

    public File getFileForKey(String key) {
        return new File(this.mRootDirectory, getFilenameForKey(key));
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0048 A[SYNTHETIC, Splitter:B:30:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x004d A[SYNTHETIC, Splitter:B:33:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0052 A[SYNTHETIC, Splitter:B:36:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x005b A[SYNTHETIC, Splitter:B:41:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0060 A[SYNTHETIC, Splitter:B:44:0x0060] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0065 A[SYNTHETIC, Splitter:B:47:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x006e A[SYNTHETIC, Splitter:B:52:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0073 A[SYNTHETIC, Splitter:B:55:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0078 A[SYNTHETIC, Splitter:B:58:0x0078] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String readFile(java.io.File r11) {
        /*
            r10 = this;
            if (r11 == 0) goto L_0x0008
            boolean r8 = r11.exists()
            if (r8 != 0) goto L_0x000a
        L_0x0008:
            r8 = 0
        L_0x0009:
            return r8
        L_0x000a:
            r2 = 0
            r4 = 0
            r0 = 0
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00ab, IOException -> 0x0058, all -> 0x006b }
            r3.<init>(r11)     // Catch:{ FileNotFoundException -> 0x00ab, IOException -> 0x0058, all -> 0x006b }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x009f, all -> 0x0093 }
            r5.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00ad, IOException -> 0x009f, all -> 0x0093 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x00b0, IOException -> 0x00a2, all -> 0x0096 }
            r1.<init>(r5)     // Catch:{ FileNotFoundException -> 0x00b0, IOException -> 0x00a2, all -> 0x0096 }
        L_0x0021:
            java.lang.String r6 = r1.readLine()     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x00a6, all -> 0x009a }
            if (r6 != 0) goto L_0x003e
            if (r3 == 0) goto L_0x002c
            r3.close()     // Catch:{ IOException -> 0x008f }
        L_0x002c:
            if (r5 == 0) goto L_0x0031
            r5.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0031:
            if (r1 == 0) goto L_0x00b4
            r1.close()     // Catch:{ IOException -> 0x007c }
            r0 = r1
            r4 = r5
            r2 = r3
        L_0x0039:
            java.lang.String r8 = r7.toString()
            goto L_0x0009
        L_0x003e:
            r7.append(r6)     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x00a6, all -> 0x009a }
            goto L_0x0021
        L_0x0042:
            r8 = move-exception
            r0 = r1
            r4 = r5
            r2 = r3
        L_0x0046:
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ IOException -> 0x0081 }
        L_0x004b:
            if (r4 == 0) goto L_0x0050
            r4.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0050:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ IOException -> 0x0056 }
            goto L_0x0039
        L_0x0056:
            r8 = move-exception
            goto L_0x0039
        L_0x0058:
            r8 = move-exception
        L_0x0059:
            if (r2 == 0) goto L_0x005e
            r2.close()     // Catch:{ IOException -> 0x0085 }
        L_0x005e:
            if (r4 == 0) goto L_0x0063
            r4.close()     // Catch:{ IOException -> 0x0087 }
        L_0x0063:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ IOException -> 0x0069 }
            goto L_0x0039
        L_0x0069:
            r8 = move-exception
            goto L_0x0039
        L_0x006b:
            r8 = move-exception
        L_0x006c:
            if (r2 == 0) goto L_0x0071
            r2.close()     // Catch:{ IOException -> 0x0089 }
        L_0x0071:
            if (r4 == 0) goto L_0x0076
            r4.close()     // Catch:{ IOException -> 0x008b }
        L_0x0076:
            if (r0 == 0) goto L_0x007b
            r0.close()     // Catch:{ IOException -> 0x008d }
        L_0x007b:
            throw r8
        L_0x007c:
            r8 = move-exception
            r0 = r1
            r4 = r5
            r2 = r3
            goto L_0x0039
        L_0x0081:
            r8 = move-exception
            goto L_0x004b
        L_0x0083:
            r8 = move-exception
            goto L_0x0050
        L_0x0085:
            r8 = move-exception
            goto L_0x005e
        L_0x0087:
            r8 = move-exception
            goto L_0x0063
        L_0x0089:
            r9 = move-exception
            goto L_0x0071
        L_0x008b:
            r9 = move-exception
            goto L_0x0076
        L_0x008d:
            r9 = move-exception
            goto L_0x007b
        L_0x008f:
            r8 = move-exception
            goto L_0x002c
        L_0x0091:
            r8 = move-exception
            goto L_0x0031
        L_0x0093:
            r8 = move-exception
            r2 = r3
            goto L_0x006c
        L_0x0096:
            r8 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x006c
        L_0x009a:
            r8 = move-exception
            r0 = r1
            r4 = r5
            r2 = r3
            goto L_0x006c
        L_0x009f:
            r8 = move-exception
            r2 = r3
            goto L_0x0059
        L_0x00a2:
            r8 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x0059
        L_0x00a6:
            r8 = move-exception
            r0 = r1
            r4 = r5
            r2 = r3
            goto L_0x0059
        L_0x00ab:
            r8 = move-exception
            goto L_0x0046
        L_0x00ad:
            r8 = move-exception
            r2 = r3
            goto L_0x0046
        L_0x00b0:
            r8 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x0046
        L_0x00b4:
            r0 = r1
            r4 = r5
            r2 = r3
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.simpleplist.cache.DiskCache.readFile(java.io.File):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0021 A[SYNTHETIC, Splitter:B:16:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x002b A[SYNTHETIC, Splitter:B:22:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0034 A[SYNTHETIC, Splitter:B:27:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean writeFile(java.io.File r7, java.lang.String r8) {
        /*
            r6 = this;
            android.content.Context r4 = r6.mContext
            if (r4 == 0) goto L_0x0006
            if (r7 != 0) goto L_0x0008
        L_0x0006:
            r3 = 0
        L_0x0007:
            return r3
        L_0x0008:
            r1 = 0
            r3 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x001d, IOException -> 0x0027, all -> 0x0031 }
            r2.<init>(r7)     // Catch:{ FileNotFoundException -> 0x001d, IOException -> 0x0027, all -> 0x0031 }
            byte[] r4 = r8.getBytes()     // Catch:{ FileNotFoundException -> 0x0043, IOException -> 0x0040, all -> 0x003d }
            r2.write(r4)     // Catch:{ FileNotFoundException -> 0x0043, IOException -> 0x0040, all -> 0x003d }
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ IOException -> 0x0038 }
            r1 = r2
            goto L_0x0007
        L_0x001d:
            r0 = move-exception
        L_0x001e:
            r3 = 0
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ IOException -> 0x0025 }
            goto L_0x0007
        L_0x0025:
            r4 = move-exception
            goto L_0x0007
        L_0x0027:
            r0 = move-exception
        L_0x0028:
            r3 = 0
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ IOException -> 0x002f }
            goto L_0x0007
        L_0x002f:
            r4 = move-exception
            goto L_0x0007
        L_0x0031:
            r4 = move-exception
        L_0x0032:
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x003b }
        L_0x0037:
            throw r4
        L_0x0038:
            r4 = move-exception
            r1 = r2
            goto L_0x0007
        L_0x003b:
            r5 = move-exception
            goto L_0x0037
        L_0x003d:
            r4 = move-exception
            r1 = r2
            goto L_0x0032
        L_0x0040:
            r0 = move-exception
            r1 = r2
            goto L_0x0028
        L_0x0043:
            r0 = move-exception
            r1 = r2
            goto L_0x001e
        L_0x0046:
            r1 = r2
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.simpleplist.cache.DiskCache.writeFile(java.io.File, java.lang.String):boolean");
    }

    private boolean deleteFile(File file) {
        if (file == null || !file.exists()) {
            return false;
        }
        return file.delete();
    }
}
