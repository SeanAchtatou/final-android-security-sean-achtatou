package ch.nth.android.simpleplist.task.config;

import android.text.TextUtils;

abstract class ConfigOptionsUsingCache extends ConfigOptions {
    private static final String DEFAULT_CACHE_DIR = "config_cache";
    private static final String DEFAULT_FILENAME = "config.plist";
    private String mCacheDirectory = DEFAULT_CACHE_DIR;
    private String mCachedFileName = DEFAULT_FILENAME;

    public String getCacheDirectory() {
        return this.mCacheDirectory;
    }

    /* access modifiers changed from: protected */
    public void setCacheDirectory(String cacheDirectory) {
        if (cacheDirectory != null) {
            this.mCacheDirectory = cacheDirectory;
        }
    }

    public String getCachedFileName() {
        return this.mCachedFileName;
    }

    /* access modifiers changed from: protected */
    public void setCachedFileName(String cachedFileName) {
        if (!TextUtils.isEmpty(cachedFileName)) {
            this.mCachedFileName = cachedFileName;
        }
    }
}
