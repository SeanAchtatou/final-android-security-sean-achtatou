package ch.nth.android.simpleplist.task.config;

public class CacheConfigOptions extends ConfigOptionsUsingCache {
    public /* bridge */ /* synthetic */ String getCacheDirectory() {
        return super.getCacheDirectory();
    }

    public /* bridge */ /* synthetic */ String getCachedFileName() {
        return super.getCachedFileName();
    }

    public static class Builder {
        private String cacheDirectory;
        private String cachedFileName;

        public Builder cacheDirectory(String cacheDirectory2) {
            this.cacheDirectory = cacheDirectory2;
            return this;
        }

        public Builder cachedFileName(String cachedFileName2) {
            this.cachedFileName = cachedFileName2;
            return this;
        }

        public CacheConfigOptions build() {
            CacheConfigOptions options = new CacheConfigOptions();
            options.setCacheDirectory(this.cacheDirectory);
            options.setCachedFileName(this.cachedFileName);
            return options;
        }
    }
}
