package ch.nth.android.simpleplist.task;

import ch.nth.android.simpleplist.task.config.ConfigType;

public interface PlistListener {
    void onPlistFailed(int i);

    void onPlistSucceeded(Object obj, ConfigType configType, int i);
}
