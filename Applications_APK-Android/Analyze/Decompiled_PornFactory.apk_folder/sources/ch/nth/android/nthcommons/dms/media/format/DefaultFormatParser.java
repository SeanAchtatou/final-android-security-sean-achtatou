package ch.nth.android.nthcommons.dms.media.format;

import ch.nth.android.nthcommons.dms.media.Size;

public class DefaultFormatParser implements FormatParser {
    public static final String TAG = DefaultFormatParser.class.getSimpleName();

    public Size parseFormat(String rawFormat) {
        String dimensionsFormat;
        int underscoreIndex = rawFormat.indexOf(95);
        if (underscoreIndex == -1) {
            dimensionsFormat = rawFormat;
        } else {
            dimensionsFormat = rawFormat.substring(0, underscoreIndex);
        }
        String[] dimensionsString = dimensionsFormat.split("x");
        try {
            return new Size(Integer.parseInt(dimensionsString[0]), Integer.parseInt(dimensionsString[1]));
        } catch (NumberFormatException e) {
            System.out.println(TAG + " - " + "Can't parse size of media format: " + rawFormat);
            return null;
        }
    }
}
