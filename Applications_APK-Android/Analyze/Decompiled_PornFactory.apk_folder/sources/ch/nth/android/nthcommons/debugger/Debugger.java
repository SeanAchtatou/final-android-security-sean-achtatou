package ch.nth.android.nthcommons.debugger;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Debugger {
    private static final String DEFAULT_TAG = Debugger.class.getSimpleName();
    private final List<LogEntry> messageLog;
    private final long startTime;
    private final String tag;

    public Debugger() {
        this(DEFAULT_TAG);
    }

    public Debugger(String tag2) {
        this.startTime = SystemClock.elapsedRealtime();
        this.messageLog = new ArrayList();
        this.tag = tag2;
    }

    public void addEntry(String entry) {
        this.messageLog.add(new LogEntry(entry));
    }

    public void addEntry(LogEntry entry) {
        this.messageLog.add(entry);
    }

    public void clearEntries() {
        this.messageLog.clear();
    }

    public void print() {
        StringBuilder builder = new StringBuilder(this.tag);
        builder.append(" - START\n");
        Iterator<LogEntry> iterator = this.messageLog.iterator();
        while (iterator.hasNext()) {
            LogEntry entry = iterator.next();
            builder.append(entry.getTimestamp() - this.startTime);
            builder.append(" ms");
            builder.append("\t\t");
            builder.append(entry.getMessage());
            if (builder.length() > 3400) {
                Log.d(this.tag, builder.toString());
                builder.setLength(0);
                builder.append(this.tag);
                builder.append(" - CONTINUED...\n");
            }
            if (iterator.hasNext()) {
                builder.append("\n");
            }
        }
        Log.d(this.tag, builder.toString());
    }
}
