package ch.nth.android.nthcommons.debugger;

import android.os.SystemClock;

public class LogEntry {
    private String message;
    private final long timestamp = SystemClock.elapsedRealtime();

    public LogEntry(String message2) {
        this.message = message2;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getMessage() {
        return this.message;
    }
}
