package ch.nth.android.nthcommons.dms.media;

import java.io.Serializable;
import java.util.Map;

public class DmsMedia implements Serializable {
    private Map<String, String> formats;
    private String original;

    public String getOriginal() {
        return this.original;
    }

    public Map<String, String> getFormats() {
        return this.formats;
    }
}
