package ch.nth.android.nthcommons.dms.media.options;

public class SizeStep implements ParseStep {
    private double heightTolerance = -1.0d;
    private double widthTolerance = -1.0d;

    public SizeStep(double widthTolerance2, double heightTolerance2) {
        this.widthTolerance = widthTolerance2;
        this.heightTolerance = heightTolerance2;
    }

    public double getWidthTolerance() {
        return this.widthTolerance;
    }

    public double getHeightTolerance() {
        return this.heightTolerance;
    }
}
