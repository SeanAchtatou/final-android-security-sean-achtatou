package ch.nth.android.nthcommons.dms.api;

public class DmsConstants {
    public static final String ADAPT_LANGS = "adapt_langs";
    public static final String ID = "_id";
    public static final String LANG = "lang";
    public static final String PAGE_NUMBER = "page_number";
    public static final String PAGE_SIZE = "page_size";

    private DmsConstants() {
    }
}
