package ch.nth.android.nthcommons.dms.media.options;

public class AspectRatioStep implements ParseStep {
    private double heightTolerance = -1.0d;
    private double ratioTolerance = -1.0d;
    private double widthTolerance = -1.0d;

    public AspectRatioStep(double ratioTolerance2) {
        this.ratioTolerance = ratioTolerance2;
    }

    public AspectRatioStep(double ratioTolerance2, double widthTolerance2, double heightTolerance2) {
        this.ratioTolerance = ratioTolerance2;
        this.widthTolerance = widthTolerance2;
        this.heightTolerance = heightTolerance2;
    }

    public double getRatioTolerance() {
        return this.ratioTolerance;
    }

    public double getWidthTolerance() {
        return this.widthTolerance;
    }

    public double getHeightTolerance() {
        return this.heightTolerance;
    }
}
