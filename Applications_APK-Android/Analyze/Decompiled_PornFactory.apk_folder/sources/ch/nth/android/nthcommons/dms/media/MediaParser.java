package ch.nth.android.nthcommons.dms.media;

import ch.nth.android.nthcommons.dms.media.format.FormatParser;
import ch.nth.android.nthcommons.dms.media.options.AspectRatioStep;
import ch.nth.android.nthcommons.dms.media.options.HeightStep;
import ch.nth.android.nthcommons.dms.media.options.Options;
import ch.nth.android.nthcommons.dms.media.options.ParseStep;
import ch.nth.android.nthcommons.dms.media.options.SizeStep;
import ch.nth.android.nthcommons.dms.media.options.WidthStep;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MediaParser {
    private static final Map<String, Size> sizeCache = new HashMap();

    public static String getBestMediaUrl(DmsMedia media, Options options) {
        if (media == null) {
            return null;
        }
        return getBestMediaUrl(media.getFormats(), media.getOriginal(), options);
    }

    public static String getBestMediaUrl(Map<String, String> formats, String original, Options options) {
        if (options == null) {
            return null;
        }
        if (formats != null) {
            String optimalSizeKey = null;
            for (ParseStep step : options.getSteps()) {
                if (step instanceof AspectRatioStep) {
                    optimalSizeKey = getBestUrlByRatioAndSize(formats, options, (AspectRatioStep) step);
                    continue;
                } else if (step instanceof SizeStep) {
                    optimalSizeKey = getBestUrlBySize(formats, options, (SizeStep) step);
                    continue;
                } else if (step instanceof WidthStep) {
                    optimalSizeKey = getBestUrlByWidth(formats, options, (WidthStep) step);
                    continue;
                } else if (step instanceof HeightStep) {
                    optimalSizeKey = getBestUrlByHeight(formats, options, (HeightStep) step);
                    continue;
                } else {
                    continue;
                }
                if (optimalSizeKey != null) {
                    break;
                }
            }
            String url = formats.get(optimalSizeKey);
            if (url != null || !options.isReturnOriginalIfNotFound()) {
                return url;
            }
            return original;
        } else if (!options.isReturnOriginalIfNotFound()) {
            return null;
        } else {
            return original;
        }
    }

    private static Size getSize(String key, Set<FormatParser> formatParsers) {
        Size size = sizeCache.get(key);
        if (size != null) {
            return size;
        }
        Iterator i$ = formatParsers.iterator();
        while (true) {
            if (i$.hasNext()) {
                size = i$.next().parseFormat(key);
                if (size != null) {
                    sizeCache.put(key, size);
                    break;
                }
            } else {
                break;
            }
        }
        return size;
    }

    private static String getBestUrlByRatioAndSize(Map<String, String> formats, Options options, AspectRatioStep step) {
        String optimalSizeKey = null;
        double minTotalDiff = Double.MAX_VALUE;
        Size targetSize = options.getTargetSize();
        for (String key : formats.keySet()) {
            Size size = getSize(key, options.getParsers());
            if (Size.isValidSize(size) && Size.isOfAcceptableAspectRatio(targetSize, size, step.getRatioTolerance()) && Size.isOfAcceptableWidth(targetSize, size, step.getWidthTolerance()) && Size.isOfAcceptableHeight(targetSize, size, step.getHeightTolerance())) {
                double totalDiff = Size.getTotalSizeDiff(targetSize, size);
                if (totalDiff < minTotalDiff) {
                    optimalSizeKey = key;
                    minTotalDiff = totalDiff;
                }
            }
        }
        return optimalSizeKey;
    }

    private static String getBestUrlBySize(Map<String, String> formats, Options options, SizeStep step) {
        String optimalSizeKey = null;
        double minTotalDiff = Double.MAX_VALUE;
        Size targetSize = options.getTargetSize();
        for (String key : formats.keySet()) {
            Size size = getSize(key, options.getParsers());
            if (Size.isValidSize(size) && Size.isOfAcceptableWidth(targetSize, size, step.getWidthTolerance()) && Size.isOfAcceptableHeight(targetSize, size, step.getHeightTolerance())) {
                double totalDiff = Size.getTotalSizeDiff(targetSize, size);
                if (totalDiff < minTotalDiff) {
                    optimalSizeKey = key;
                    minTotalDiff = totalDiff;
                }
            }
        }
        return optimalSizeKey;
    }

    private static String getBestUrlByWidth(Map<String, String> formats, Options options, WidthStep step) {
        String optimalSizeKey = null;
        double minWidthDiff = Double.MAX_VALUE;
        Size targetSize = options.getTargetSize();
        for (String key : formats.keySet()) {
            Size size = getSize(key, options.getParsers());
            if (Size.isValidSize(size) && Size.isOfAcceptableWidth(targetSize, size, step.getWidthTolerance())) {
                double widthDiff = Size.getWidthSizeDiff(targetSize, size);
                if (widthDiff < minWidthDiff) {
                    optimalSizeKey = key;
                    minWidthDiff = widthDiff;
                }
            }
        }
        return optimalSizeKey;
    }

    private static String getBestUrlByHeight(Map<String, String> formats, Options options, HeightStep step) {
        String optimalSizeKey = null;
        double minHeightDiff = Double.MAX_VALUE;
        Size targetSize = options.getTargetSize();
        for (String key : formats.keySet()) {
            Size size = getSize(key, options.getParsers());
            if (Size.isValidSize(size) && Size.isOfAcceptableHeight(targetSize, size, step.getHeightTolerance())) {
                double heightDiff = Size.getHeightSizeDiff(targetSize, size);
                if (heightDiff < minHeightDiff) {
                    optimalSizeKey = key;
                    minHeightDiff = heightDiff;
                }
            }
        }
        return optimalSizeKey;
    }
}
