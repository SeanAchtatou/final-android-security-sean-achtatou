package ch.nth.android.nthcommons.dms.media;

import android.content.Context;

public class Size {
    int heightPx;
    int widthPx;

    public Size(Context context, int widthDp, int heightDp) {
        calculateTargetSize(context, widthDp, heightDp);
    }

    public Size(int widthPx2, int heightPx2) {
        this.widthPx = widthPx2;
        this.heightPx = heightPx2;
    }

    public int getWidthPx() {
        return this.widthPx;
    }

    public int getHeightPx() {
        return this.heightPx;
    }

    /* access modifiers changed from: package-private */
    public void calculateTargetSize(Context context, int widthDp, int heightDp) {
        float density = context.getResources().getDisplayMetrics().density;
        this.widthPx = (int) (((float) widthDp) * density);
        this.heightPx = (int) (((float) heightDp) * density);
    }

    public static boolean isValidSize(Size size) {
        return size != null && size.heightPx > 0 && size.widthPx > 0;
    }

    public static boolean isOfAcceptableAspectRatio(Size targetSize, Size checkedSize, double tolerance) {
        if (tolerance < 0.0d) {
            return true;
        }
        if (Math.abs((((double) checkedSize.widthPx) / ((double) checkedSize.heightPx)) - (((double) targetSize.widthPx) / ((double) targetSize.heightPx))) > tolerance) {
            return false;
        }
        return true;
    }

    public static boolean isOfAcceptableWidth(Size targetSize, Size checkedSize, double tolerance) {
        if (tolerance >= 0.0d && ((double) Math.abs(targetSize.widthPx - checkedSize.widthPx)) / ((double) targetSize.widthPx) > tolerance) {
            return false;
        }
        return true;
    }

    public static boolean isOfAcceptableHeight(Size targetSize, Size checkedSize, double tolerance) {
        if (tolerance >= 0.0d && ((double) Math.abs(targetSize.heightPx - checkedSize.heightPx)) / ((double) targetSize.heightPx) > tolerance) {
            return false;
        }
        return true;
    }

    public static double getTotalSizeDiff(Size targetSize, Size checkedSize) {
        return (double) (Math.abs(checkedSize.widthPx - targetSize.widthPx) + Math.abs(checkedSize.heightPx - targetSize.heightPx));
    }

    public static double getWidthSizeDiff(Size targetSize, Size checkedSize) {
        return (double) Math.abs(checkedSize.widthPx - targetSize.widthPx);
    }

    public static double getHeightSizeDiff(Size targetSize, Size checkedSize) {
        return (double) Math.abs(checkedSize.heightPx - targetSize.heightPx);
    }
}
