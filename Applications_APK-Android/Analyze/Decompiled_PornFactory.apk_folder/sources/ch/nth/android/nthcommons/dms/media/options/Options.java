package ch.nth.android.nthcommons.dms.media.options;

import ch.nth.android.nthcommons.dms.media.Size;
import ch.nth.android.nthcommons.dms.media.format.DefaultFormatParser;
import ch.nth.android.nthcommons.dms.media.format.FormatParser;
import java.util.HashSet;
import java.util.Set;

public class Options {
    private Set<FormatParser> parsers;
    private boolean returnOriginalIfNotFound;
    private Set<ParseStep> steps;
    private Size targetSize;

    private Options(Builder builder) {
        this.steps = new HashSet();
        this.parsers = new HashSet();
        this.targetSize = builder.targetSize;
        this.steps = builder.steps;
        this.parsers = builder.parsers;
        this.returnOriginalIfNotFound = builder.returnOriginalIfNotFound;
    }

    public Size getTargetSize() {
        return this.targetSize;
    }

    public Set<ParseStep> getSteps() {
        return this.steps;
    }

    public Set<FormatParser> getParsers() {
        return this.parsers;
    }

    public boolean isReturnOriginalIfNotFound() {
        return this.returnOriginalIfNotFound;
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public Set<FormatParser> parsers = new HashSet();
        /* access modifiers changed from: private */
        public boolean returnOriginalIfNotFound = true;
        /* access modifiers changed from: private */
        public Set<ParseStep> steps = new HashSet();
        /* access modifiers changed from: private */
        public Size targetSize;

        public Builder(Size targetSize2) {
            if (targetSize2 == null) {
                throw new IllegalArgumentException("targetSize must not be null");
            }
            this.targetSize = targetSize2;
        }

        public Builder addStep(ParseStep step) {
            if (step != null) {
                this.steps.add(step);
            }
            return this;
        }

        public Builder addParser(FormatParser parser) {
            if (parser != null) {
                this.parsers.add(parser);
            }
            return this;
        }

        public Builder withReturnOriginalIfNotFound(boolean returnOriginalIfNotFound2) {
            this.returnOriginalIfNotFound = returnOriginalIfNotFound2;
            return this;
        }

        public Options build() {
            if (this.steps.isEmpty()) {
                this.steps.add(new AspectRatioStep(0.25d));
                this.steps.add(new SizeStep(1.0d, 1.0d));
            }
            if (this.parsers.isEmpty()) {
                this.parsers.add(new DefaultFormatParser());
            }
            return new Options(this);
        }
    }
}
