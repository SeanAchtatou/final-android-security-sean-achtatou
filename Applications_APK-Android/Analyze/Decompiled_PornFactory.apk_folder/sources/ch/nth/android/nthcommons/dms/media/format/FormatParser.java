package ch.nth.android.nthcommons.dms.media.format;

import ch.nth.android.nthcommons.dms.media.Size;

public interface FormatParser {
    Size parseFormat(String str);
}
