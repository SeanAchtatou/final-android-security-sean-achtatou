package ch.nth.android.nthcommons.dms.media.options;

public class HeightStep implements ParseStep {
    double heightTolerance = -1.0d;

    public HeightStep(double heightTolerance2) {
        this.heightTolerance = heightTolerance2;
    }

    public double getHeightTolerance() {
        return this.heightTolerance;
    }
}
