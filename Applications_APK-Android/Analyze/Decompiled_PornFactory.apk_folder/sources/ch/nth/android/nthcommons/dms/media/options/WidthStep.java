package ch.nth.android.nthcommons.dms.media.options;

public class WidthStep implements ParseStep {
    double widthTolerance = -1.0d;

    public WidthStep(double widthTolerance2) {
        this.widthTolerance = widthTolerance2;
    }

    public double getWidthTolerance() {
        return this.widthTolerance;
    }
}
