package ch.nth.android.scmsdk.listener;

import ch.nth.android.scmsdk.model.BaseScmResult;

public interface ScmResponseListener<T extends BaseScmResult> {
    void onError(int i);

    void onResponseReceived(int i, BaseScmResult baseScmResult);
}
