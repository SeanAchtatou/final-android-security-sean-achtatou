package ch.nth.android.scmsdk.listener;

import java.util.List;
import org.apache.http.Header;
import org.apache.http.cookie.Cookie;

public interface ScmStringContentListener {
    void onContentAvailable(int i, Header[] headerArr, List<Cookie> list, String str);

    void onContentNotAvailable(int i, Header[] headerArr, List<Cookie> list);
}
