package ch.nth.android.scmsdk.async;

import android.net.Uri;
import android.os.AsyncTask;
import ch.nth.android.scmsdk.listener.ScmStringContentListener;
import ch.nth.android.scmsdk.utils.Utils;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public class AsyncGetRequest extends AsyncTask<Void, Void, AsyncResponse> {
    private ScmStringContentListener mListener;
    private Map<String, String> mParams;
    private String mUrl;

    public AsyncGetRequest(String url, ScmStringContentListener listener) {
        this.mUrl = url;
        this.mListener = listener;
    }

    public AsyncGetRequest(String url, Map<String, String> params, ScmStringContentListener listener) {
        this(url, listener);
        this.mParams = params;
    }

    /* access modifiers changed from: protected */
    public AsyncResponse doInBackground(Void... params) {
        AsyncResponse aResponse = new AsyncResponse();
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
        HttpConnectionParams.setSoTimeout(httpParameters, 10000);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        try {
            Uri.Builder ub = Uri.parse(this.mUrl).buildUpon();
            if (this.mParams != null) {
                for (String key : this.mParams.keySet()) {
                    ub.appendQueryParameter(key, this.mParams.get(key));
                }
            }
            Utils.doLog("GET REQUEST: " + ub.build().toString());
            HttpResponse response = httpClient.execute(new HttpGet(ub.toString()));
            aResponse.setStatusCode(response.getStatusLine().getStatusCode());
            aResponse.setHeaders(response.getAllHeaders());
            aResponse.setCookies(httpClient.getCookieStore().getCookies());
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                aResponse.setContent(EntityUtils.toString(entity));
            }
        } catch (Exception ex) {
            Utils.doLogException(ex);
        }
        return aResponse;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(AsyncResponse aResponse) {
        if (Utils.checkSuccessfulResponse(aResponse.getStatusCode())) {
            this.mListener.onContentAvailable(aResponse.getStatusCode(), aResponse.getHeaders(), aResponse.getCookies(), aResponse.getContent());
        } else {
            this.mListener.onContentNotAvailable(aResponse.getStatusCode(), aResponse.getHeaders(), aResponse.getCookies());
        }
    }
}
