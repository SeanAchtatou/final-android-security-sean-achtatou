package ch.nth.android.scmsdk.async;

import android.content.Context;
import ch.nth.android.scmsdk.async.base.BaseRequest;
import ch.nth.android.scmsdk.listener.ScmResponseListener;
import ch.nth.android.scmsdk.listener.ScmStringContentListener;
import ch.nth.android.scmsdk.model.ScmSubscriptionSession;
import ch.nth.android.scmsdk.utils.Utils;
import com.google.gson.Gson;
import java.util.List;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.cookie.Cookie;

public class ScmVerifySubscriptionRequest extends BaseRequest<ScmSubscriptionSession> {
    public ScmVerifySubscriptionRequest(Context context, String baseEndpoint, Map<String, String> params, ScmResponseListener<ScmSubscriptionSession> listener) {
        super(context, baseEndpoint, params, listener);
    }

    public void onExecuteRequest() {
        new AsyncGetRequest(String.valueOf(this.mBaseEndPoint) + "subscription/verify", this.mParams, new ScmStringContentListener() {
            public void onContentAvailable(int code, Header[] headerList, List<Cookie> list, String content) {
                try {
                    ScmVerifySubscriptionRequest.this.notifyResponseListener(true, code, (ScmSubscriptionSession) new Gson().fromJson(content, ScmSubscriptionSession.class));
                } catch (Exception e) {
                    Utils.doLogException(e);
                    ScmVerifySubscriptionRequest.this.notifyResponseListener(false, -1, null);
                }
            }

            public void onContentNotAvailable(int responseCode, Header[] headerList, List<Cookie> list) {
                ScmVerifySubscriptionRequest.this.notifyResponseListener(false, responseCode, null);
            }
        }).execute(new Void[0]);
    }
}
