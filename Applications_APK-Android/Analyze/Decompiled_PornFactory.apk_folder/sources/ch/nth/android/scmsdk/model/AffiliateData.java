package ch.nth.android.scmsdk.model;

import com.google.gson.annotations.SerializedName;

public class AffiliateData {
    @SerializedName("frid")
    private String frid;

    public String getFrid() {
        return this.frid;
    }
}
