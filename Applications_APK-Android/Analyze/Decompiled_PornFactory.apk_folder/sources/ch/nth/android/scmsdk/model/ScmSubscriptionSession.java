package ch.nth.android.scmsdk.model;

import ch.nth.android.scmsdk.model.BaseScmResult;
import com.google.gson.annotations.SerializedName;

public class ScmSubscriptionSession extends BaseScmResult {
    @SerializedName("affiliate_data")
    private AffiliateData affiliateData;
    @SerializedName("session_id")
    private String sessionId;
    @SerializedName("status")
    private SessionStatus status;
    @SerializedName("user_id")
    private Integer userId;

    public enum SessionStatus {
        OPEN,
        CLOSED,
        LIMITED,
        DISABLED
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId2) {
        this.sessionId = sessionId2;
    }

    public SessionStatus getStatus() {
        return this.status;
    }

    public void setStatus(SessionStatus status2) {
        this.status = status2;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId2) {
        this.userId = userId2;
    }

    public AffiliateData getAffiliateData() {
        return this.affiliateData;
    }

    public BaseScmResult.ResultType getResultType() {
        return BaseScmResult.ResultType.CHECK_SUBSCRIPTION;
    }
}
