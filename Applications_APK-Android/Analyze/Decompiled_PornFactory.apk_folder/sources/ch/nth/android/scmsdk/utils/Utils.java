package ch.nth.android.scmsdk.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Xml;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class Utils {
    private static String ENTERPRISE_STORE_APPLICATION_UNIQUE_ID = null;
    private static String ENTERPRISE_STORE_SERVICE_ID = null;
    private static final String ENTSTORE_ASSET_FILE = "opportunity_data.xml";
    private static final String TAG = "SCM-SDK";

    public static void doLog(Object o) {
    }

    public static void doLogException(Exception e) {
    }

    public static void doToast(Context context, String text) {
        if (context != null) {
            Toast.makeText(context, text, 1).show();
        }
    }

    public static void doToast(Context context, int stringResId) {
        Toast.makeText(context, stringResId, 1).show();
    }

    public static void doToast(Context context, Object o) {
        Toast.makeText(context, o.toString(), 1).show();
    }

    public static String getAvailableID(Context context) {
        String availableID = getMSISDN(context);
        if (TextUtils.isEmpty(availableID)) {
            String availableID2 = getIMEI(context);
            if (!TextUtils.isEmpty(availableID2)) {
                return availableID2;
            }
            availableID = getAndroidID(context);
            if (TextUtils.isEmpty(availableID)) {
                availableID = "pw_app";
            }
        }
        return availableID;
    }

    public static String getIMEI(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static String getSimSerial(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber();
    }

    public static String getIMSI(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
    }

    public static String getMSISDN(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
    }

    public static String getAndroidID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static int getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException ex) {
            throw new RuntimeException("Could not get package name: " + ex);
        }
    }

    public static String getApplicationUniqueId(Context context) {
        if (ENTERPRISE_STORE_APPLICATION_UNIQUE_ID == null) {
            processEnterpriseStoreMetaData(context);
        }
        return ENTERPRISE_STORE_APPLICATION_UNIQUE_ID;
    }

    public static boolean checkSuccessfulResponse(int code) {
        if (code / 100 == 2) {
            return true;
        }
        return false;
    }

    private static void processEnterpriseStoreMetaData(Context context) {
        InputStream is = null;
        try {
            is = context.getAssets().open(ENTSTORE_ASSET_FILE);
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, "ISO-8859-1");
            parser.nextTag();
            parser.require(2, null, "root");
            while (parser.next() != 3) {
                if (parser.getEventType() == 2) {
                    String name = parser.getName();
                    if (name.equals("opportunity_id")) {
                        ENTERPRISE_STORE_APPLICATION_UNIQUE_ID = readText(parser);
                    } else if (name.equals("aboidentifier")) {
                        ENTERPRISE_STORE_SERVICE_ID = readText(parser);
                    }
                }
            }
            try {
                is.close();
            } catch (IOException e) {
            }
        } catch (Exception e2) {
            doLogException(e2);
            try {
                is.close();
            } catch (IOException e3) {
            }
        } catch (Throwable th) {
            try {
                is.close();
            } catch (IOException e4) {
            }
            throw th;
        }
    }

    private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        if (parser.next() != 4) {
            return "";
        }
        String result = parser.getText();
        parser.nextTag();
        return result;
    }
}
