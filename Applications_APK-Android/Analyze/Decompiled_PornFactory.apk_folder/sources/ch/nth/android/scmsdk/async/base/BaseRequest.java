package ch.nth.android.scmsdk.async.base;

import android.content.Context;
import ch.nth.android.scmsdk.listener.ScmResponseListener;
import ch.nth.android.scmsdk.model.BaseScmResult;
import java.util.Map;

public abstract class BaseRequest<T extends BaseScmResult> {
    protected String mBaseEndPoint;
    protected Context mContext;
    protected Map<String, String> mParams;
    protected ScmResponseListener<T> mResponseListener;

    public abstract void onExecuteRequest();

    public BaseRequest(Context context, String baseEndpoint, Map<String, String> params, ScmResponseListener<T> listener) {
        this.mContext = context;
        this.mParams = params;
        this.mBaseEndPoint = baseEndpoint;
        this.mResponseListener = listener;
        run();
    }

    public void run() {
        if (this.mResponseListener != null && this.mContext != null) {
            onExecuteRequest();
        }
    }

    /* access modifiers changed from: protected */
    public void notifyResponseListener(boolean success, int statusCode, T response) {
        if (this.mResponseListener == null) {
            return;
        }
        if (success) {
            this.mResponseListener.onResponseReceived(statusCode, response);
        } else {
            this.mResponseListener.onError(statusCode);
        }
    }
}
