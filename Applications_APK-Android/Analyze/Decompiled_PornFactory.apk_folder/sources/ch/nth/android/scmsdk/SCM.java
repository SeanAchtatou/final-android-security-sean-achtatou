package ch.nth.android.scmsdk;

import android.content.Context;
import android.text.TextUtils;
import ch.nth.android.scmsdk.async.ScmVerifySubscriptionRequest;
import ch.nth.android.scmsdk.listener.ScmResponseListener;
import ch.nth.android.scmsdk.model.ScmSubscriptionSession;
import ch.nth.android.scmsdk.utils.Utils;
import java.util.HashMap;
import java.util.Map;

public class SCM {
    private String mBaseEndpoint;
    private Context mContext;
    private String sid;

    public SCM(Context context) {
        this.mContext = context;
    }

    public String getmBaseEndpoint() {
        return this.mBaseEndpoint;
    }

    public void setmBaseEndpoint(String mBaseEndpoint2) {
        this.mBaseEndpoint = mBaseEndpoint2;
    }

    public String getSid() {
        return this.sid;
    }

    public void setSid(String sid2) {
        this.sid = sid2;
    }

    public void verifySubscription(String aid, String sessionId, int userId, ScmResponseListener<ScmSubscriptionSession> listener) throws Exception {
        if (TextUtils.isEmpty(aid)) {
            throw new Exception("aid must not be null");
        }
        Map<String, String> params = new HashMap<>();
        params.put("sid", this.sid);
        params.put("aid", aid);
        params.put("udid", Utils.getAndroidID(this.mContext));
        String imsi = Utils.getIMSI(this.mContext);
        String imei = Utils.getIMEI(this.mContext);
        if (!TextUtils.isEmpty(imsi)) {
            params.put("imsi", imsi);
        } else if (!TextUtils.isEmpty(imei)) {
            params.put("imsi", imei);
        }
        String msisdn = Utils.getMSISDN(this.mContext);
        if (!TextUtils.isEmpty(msisdn)) {
            params.put("msisdn", msisdn);
        }
        if (!TextUtils.isEmpty(sessionId)) {
            params.put("session_id", sessionId);
        }
        if (userId != 0) {
            params.put("uid", String.valueOf(userId));
        }
        params.put("platform", "ANDROID");
        params.put("version", String.valueOf(Utils.getAppVersion(this.mContext)));
        new ScmVerifySubscriptionRequest(this.mContext, this.mBaseEndpoint, params, listener);
    }
}
