package ch.nth.android.scmsdk.async;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.cookie.Cookie;

public class AsyncResponse {
    private String content;
    private List<Cookie> cookies = new ArrayList();
    private Header[] headers = new Header[0];
    private int statusCode;

    public int getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(int statusCode2) {
        this.statusCode = statusCode2;
    }

    public Header[] getHeaders() {
        return this.headers;
    }

    public void setHeaders(Header[] headers2) {
        this.headers = headers2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public List<Cookie> getCookies() {
        return this.cookies;
    }

    public void setCookies(List<Cookie> cookies2) {
        this.cookies = cookies2;
    }
}
