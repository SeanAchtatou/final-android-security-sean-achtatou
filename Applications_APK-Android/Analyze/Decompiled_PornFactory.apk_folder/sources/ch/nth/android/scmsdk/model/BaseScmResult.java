package ch.nth.android.scmsdk.model;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

public abstract class BaseScmResult {
    public List<Header> mHeaders = new ArrayList();

    public enum ResultType {
        CHECK_SUBSCRIPTION
    }

    public abstract ResultType getResultType();

    public List<Header> getHeaders() {
        return this.mHeaders;
    }

    public void setHeaders(List<Header> headers) {
        this.mHeaders = headers;
    }
}
