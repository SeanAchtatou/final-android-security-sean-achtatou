package ch.nth.android.paymentsdk.v2.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import ch.nth.android.paymentsdk.v2.listeners.PaymentResponseListener;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import ch.nth.android.paymentsdk.v2.view.base.BasePaymentWebView;

public class PaymentWebView extends BasePaymentWebView {
    private PaymentResponseListener mResponseListener;

    public PaymentWebView(Context context) {
        super(context);
    }

    public PaymentWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PaymentWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setResponseListener(PaymentResponseListener listener) {
        this.mResponseListener = listener;
    }

    public void onDataReceived(String result) {
        notifyListener(result);
    }

    public void onDataReceived(InfoResponse infoReponse) {
        if (this.mResponseListener != null) {
            this.mResponseListener.onRequestInfoRetrieved(infoReponse);
        }
    }

    private void notifyListener(String response) {
        if (this.mResponseListener == null) {
            return;
        }
        if (!TextUtils.isEmpty(response)) {
            this.mResponseListener.onSuccess(response);
        } else {
            this.mResponseListener.onError();
        }
    }

    public void onCheckWebUrl(InitPaymentParams url) {
        if (this.mResponseListener != null) {
            this.mResponseListener.onCheckWebUrl(url);
        }
    }
}
