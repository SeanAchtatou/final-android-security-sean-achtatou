package ch.nth.android.paymentsdk.v2.payment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import ch.nth.android.paymentsdk.v2.async.AsyncInfoRequest;
import ch.nth.android.paymentsdk.v2.listeners.GenericStringContentListener;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class PaymentWebViewClient extends WebViewClient {
    boolean mAllowPageLoad = false;
    private String mApiKey = "";
    private String mApiSecret = "";
    private String mBaseUrl = "";
    private String mCallback = "";
    boolean mReloadWebPage = false;
    private String mRequestId = "";
    boolean mVerifyEveryUrl = true;

    public abstract InitPaymentParams getAllowedPage();

    public abstract void onDataReceived(InfoResponse infoResponse);

    public abstract void onIOException(IOException iOException);

    public abstract void onReturnResult(String str);

    public abstract void onReturnUrl(InitPaymentParams initPaymentParams);

    public abstract void onWebViewReceivedError(int i, String str, String str2);

    public PaymentWebViewClient(String callback, String baseUrl, String requestId, String apiKey, String apiSecret, boolean verifyEveryUrl) {
        this.mCallback = callback;
        this.mBaseUrl = baseUrl;
        this.mRequestId = requestId;
        this.mApiKey = apiKey;
        this.mApiSecret = apiSecret;
        this.mVerifyEveryUrl = verifyEveryUrl;
    }

    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        Utils.doLog("onPageStarted >>>> " + url);
        if (!TextUtils.isEmpty(this.mCallback) && url.startsWith(this.mCallback)) {
            Utils.doLog("notify found callback");
            onReturnResult(url);
            view.stopLoading();
        } else if (this.mVerifyEveryUrl) {
            if (getAllowedPage() == null) {
                this.mAllowPageLoad = false;
            } else if (url.equalsIgnoreCase(getAllowedPage().getRedirectUrl())) {
                this.mAllowPageLoad = true;
                if (!this.mReloadWebPage) {
                    this.mReloadWebPage = true;
                } else {
                    this.mReloadWebPage = false;
                }
            } else {
                this.mAllowPageLoad = false;
            }
            if (this.mAllowPageLoad) {
                Utils.doLog("on page onPageStarted, should reload? " + this.mReloadWebPage);
                if (this.mReloadWebPage) {
                    view.loadUrl(url);
                }
            } else if (!this.mReloadWebPage) {
                onReturnUrl(new InitPaymentParams(url, this.mRequestId, this.mCallback));
                tryFetchRequestInfo();
                view.stopLoading();
            }
        } else {
            onReturnUrl(new InitPaymentParams(url, this.mRequestId, this.mCallback));
            tryFetchRequestInfo();
        }
    }

    public void onPageFinished(WebView view, String url) {
        if (TextUtils.isEmpty(this.mCallback) || !url.startsWith(this.mCallback)) {
            super.onPageFinished(view, url);
            return;
        }
        Utils.doLog("notify found callback");
        onReturnResult(url);
    }

    public boolean shouldOverrideUrlLoading(WebView view, String reqUrl) {
        if (!Utils.hasActiveNetworkConnection(view.getContext())) {
            return false;
        }
        if (!TextUtils.isEmpty(this.mCallback) && reqUrl.startsWith(this.mCallback)) {
            onReturnResult(reqUrl);
            return true;
        } else if (!Utils.shouldStartIntent(reqUrl)) {
            return super.shouldOverrideUrlLoading(view, reqUrl);
        } else {
            startIntentByUrl(view, reqUrl);
            return true;
        }
    }

    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        Utils.doLog("eror " + errorCode + " desc " + description);
        super.onReceivedError(view, errorCode, description, failingUrl);
        onWebViewReceivedError(errorCode, description, failingUrl);
    }

    private void startIntentByUrl(WebView ww, String url) {
        if (ww != null) {
            try {
                if (ww.getContext() != null) {
                    ww.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                }
            } catch (Exception e) {
                Utils.doLogException(e);
            }
        }
    }

    private void tryFetchRequestInfo() {
        Map<String, String> params = new HashMap<>();
        params.put("requestId", this.mRequestId);
        new AsyncInfoRequest(this.mBaseUrl, params, this.mApiKey, this.mApiSecret, new GenericStringContentListener() {
            public void onContentNotAvailable(int statusCode) {
                PaymentWebViewClient.this.onDataReceived(null);
            }

            public void onContentAvailable(int statusCode, String htmlContent) {
                try {
                    PaymentWebViewClient.this.onDataReceived((InfoResponse) new Gson().fromJson(htmlContent, InfoResponse.class));
                } catch (Exception e) {
                    Utils.doLogException(e);
                    PaymentWebViewClient.this.onDataReceived(null);
                }
            }
        }).execute(new Void[0]);
    }
}
