package ch.nth.android.paymentsdk.v2.async;

import android.content.Context;
import ch.nth.android.paymentsdk.v2.async.base.BaseRequest;
import ch.nth.android.paymentsdk.v2.listeners.GenericPaymentResponseListener;
import ch.nth.android.paymentsdk.v2.listeners.GenericStringContentListener;
import ch.nth.android.paymentsdk.v2.model.PaymentSessionStatus;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;

public class InitPaymentRequest extends BaseRequest {
    public InitPaymentRequest(Context context, String baseEndpoint, String apiKey, String apiSecret, Map<String, String> params, GenericPaymentResponseListener listener) {
        super(context, baseEndpoint, apiKey, apiSecret, params, listener);
    }

    public void onExecuteRequest() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", System.getProperty("http.agent"));
        new AsyncPostRequest(String.valueOf(this.mBaseEndPoint) + "initPayment", this.mApiKey, this.mApiSecret, this.params, headers, new GenericStringContentListener() {
            public void onContentAvailable(int statusCode, String htmlContent) {
                try {
                    InitPaymentRequest.this.notifyResponseListener(true, statusCode, (PaymentSessionStatus) new Gson().fromJson(htmlContent, PaymentSessionStatus.class));
                } catch (Exception e) {
                    Utils.doLogException(e);
                    InitPaymentRequest.this.notifyResponseListener(false, -1, null);
                }
            }

            public void onContentNotAvailable(int statusCode) {
                InitPaymentRequest.this.notifyResponseListener(false, statusCode, null);
            }
        }).execute(new Void[0]);
    }
}
