package ch.nth.android.paymentsdk.v2.model.base;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

public abstract class BasePaymentResult {
    public List<Header> mHeaders = new ArrayList();

    public List<Header> getHeaders() {
        return this.mHeaders;
    }

    public void setHeaders(List<Header> headers) {
        this.mHeaders = headers;
    }
}
