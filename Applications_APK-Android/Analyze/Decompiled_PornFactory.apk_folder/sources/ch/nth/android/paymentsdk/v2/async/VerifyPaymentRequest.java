package ch.nth.android.paymentsdk.v2.async;

import android.content.Context;
import ch.nth.android.paymentsdk.v2.async.base.BaseRequest;
import ch.nth.android.paymentsdk.v2.listeners.GenericPaymentResponseListener;
import ch.nth.android.paymentsdk.v2.listeners.GenericStringContentListener;
import ch.nth.android.paymentsdk.v2.model.SubscriptionPaymentSession;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import com.google.gson.Gson;
import java.util.Map;

public class VerifyPaymentRequest extends BaseRequest {
    public VerifyPaymentRequest(Context context, String baseEndpoint, String apiKey, String apiSecret, Map<String, String> params, GenericPaymentResponseListener listener) {
        super(context, baseEndpoint, apiKey, apiSecret, params, listener);
    }

    public void onExecuteRequest() {
        new AsyncPostRequest(String.valueOf(this.mBaseEndPoint) + "verifyPayment", this.mApiKey, this.mApiSecret, this.params, new GenericStringContentListener() {
            public void onContentAvailable(int statusCode, String htmlContent) {
                try {
                    VerifyPaymentRequest.this.notifyResponseListener(true, statusCode, (SubscriptionPaymentSession) new Gson().fromJson(htmlContent, SubscriptionPaymentSession.class));
                } catch (Exception e) {
                    Utils.doLogException(e);
                    VerifyPaymentRequest.this.notifyResponseListener(false, -1, null);
                }
            }

            public void onContentNotAvailable(int statusCode) {
                VerifyPaymentRequest.this.notifyResponseListener(false, statusCode, null);
            }
        }).execute(new Void[0]);
    }
}
