package ch.nth.android.paymentsdk.v2.async.base;

import android.content.Context;
import ch.nth.android.paymentsdk.v2.listeners.GenericPaymentResponseListener;
import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;
import java.util.Map;

public abstract class BaseRequest {
    protected String mApiKey;
    protected String mApiSecret;
    protected String mBaseEndPoint;
    protected Context mContext;
    protected GenericPaymentResponseListener mResponseListener;
    protected Map<String, String> params;

    public abstract void onExecuteRequest();

    public BaseRequest(Context context, String baseEndpoint, String apiKey, String apiSecret, Map<String, String> params2, GenericPaymentResponseListener listener) {
        this.mContext = context;
        this.params = params2;
        this.mApiKey = apiKey;
        this.mApiSecret = apiSecret;
        this.mBaseEndPoint = baseEndpoint;
        this.mResponseListener = listener;
        run();
    }

    public void run() {
        if (this.mResponseListener != null && this.mContext != null) {
            onExecuteRequest();
        }
    }

    /* access modifiers changed from: protected */
    public void notifyResponseListener(boolean success, int statusCode, BasePaymentResult response) {
        if (this.mResponseListener == null) {
            return;
        }
        if (success) {
            this.mResponseListener.onResponseReceived(statusCode, response);
        } else {
            this.mResponseListener.onError(statusCode);
        }
    }
}
