package ch.nth.android.paymentsdk.v2.exceptions;

public class MandatoryFieldException extends Exception {
    public MandatoryFieldException(String message) {
        super(message);
    }
}
