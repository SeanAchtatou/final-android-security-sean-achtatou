package ch.nth.android.paymentsdk.v2.model;

import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;

public class InfoResponse extends BasePaymentResult {
    private String currency;
    private String flowName;
    private String flowType;
    private String mcc;
    private String mnc;
    private String periodCount;
    private String periodType;
    private String phone;
    private String price;
    private String sessionId;
    private String sessionStatus;
    private String sessionType;
    private String smsCancelUri;
    private String smsConfirmationResponse;
    private String smsConfirmationUri;
    private String smsServiceResponse;
    private String smsServiceType;
    private String smsServiceUri;
    private String smsStopKeywordRequired;

    public String getFlowName() {
        return this.flowName;
    }

    public void setFlowName(String flowName2) {
        this.flowName = flowName2;
    }

    public String getFlowType() {
        return this.flowType;
    }

    public void setFlowType(String flowType2) {
        this.flowType = flowType2;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId2) {
        this.sessionId = sessionId2;
    }

    public String getSessionType() {
        return this.sessionType;
    }

    public void setSessionType(String sessionType2) {
        this.sessionType = sessionType2;
    }

    public String getSessionStatus() {
        return this.sessionStatus;
    }

    public void setSessionStatus(String sessionStatus2) {
        this.sessionStatus = sessionStatus2;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }

    public String getMcc() {
        return this.mcc;
    }

    public void setMcc(String mcc2) {
        this.mcc = mcc2;
    }

    public String getMnc() {
        return this.mnc;
    }

    public void setMnc(String mnc2) {
        this.mnc = mnc2;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency2) {
        this.currency = currency2;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price2) {
        this.price = price2;
    }

    public String getPeriodType() {
        return this.periodType;
    }

    public void setPeriodType(String periodType2) {
        this.periodType = periodType2;
    }

    public String getPeriodCount() {
        return this.periodCount;
    }

    public void setPeriodCount(String periodCount2) {
        this.periodCount = periodCount2;
    }

    public String getSmsServiceType() {
        return this.smsServiceType;
    }

    public void setSmsServiceType(String smsServiceType2) {
        this.smsServiceType = smsServiceType2;
    }

    public String getSmsServiceUri() {
        return this.smsServiceUri;
    }

    public void setSmsServiceUri(String smsServiceUri2) {
        this.smsServiceUri = smsServiceUri2;
    }

    public String getSmsServiceResponse() {
        return this.smsServiceResponse;
    }

    public void setSmsServiceResponse(String smsServiceResponse2) {
        this.smsServiceResponse = smsServiceResponse2;
    }

    public String getSmsConfirmationUri() {
        return this.smsConfirmationUri;
    }

    public void setSmsConfirmationUri(String smsConfirmationUri2) {
        this.smsConfirmationUri = smsConfirmationUri2;
    }

    public String getSmsConfirmationResponse() {
        return this.smsConfirmationResponse;
    }

    public void setSmsConfirmationResponse(String smsConfirmationResponse2) {
        this.smsConfirmationResponse = smsConfirmationResponse2;
    }

    public String getSmsStopKeywordRequired() {
        return this.smsStopKeywordRequired;
    }

    public void setSmsStopKeywordRequired(String smsStopKeywordRequired2) {
        this.smsStopKeywordRequired = smsStopKeywordRequired2;
    }

    public String getSmsCancelUri() {
        return this.smsCancelUri;
    }

    public void setSmsCancelUri(String smsCancelUri2) {
        this.smsCancelUri = smsCancelUri2;
    }

    public String toString() {
        return "InfoResponse ( " + super.toString() + "\n" + "currency = " + this.currency + "\n" + "flowName = " + this.flowName + "\n" + "flowType = " + this.flowType + "\n" + "mcc = " + this.mcc + "\n" + "mnc = " + this.mnc + "\n" + "periodCount = " + this.periodCount + "\n" + "periodType = " + this.periodType + "\n" + "phone = " + this.phone + "\n" + "price = " + this.price + "\n" + "sessionId = " + this.sessionId + "\n" + "sessionStatus = " + this.sessionStatus + "\n" + "sessionType = " + this.sessionType + "\n" + "smsConfirmationResponse = " + this.smsConfirmationResponse + "\n" + "smsConfirmationUri = " + this.smsConfirmationUri + "\n" + "smsServiceResponse = " + this.smsServiceResponse + "\n" + "smsServiceType = " + this.smsServiceType + "\n" + "smsServiceUri = " + this.smsServiceUri + "\n" + "smsStopKeywordRequired = " + this.smsStopKeywordRequired + "\n" + "smsCancelUri = " + this.smsCancelUri + "\n" + " )";
    }
}
