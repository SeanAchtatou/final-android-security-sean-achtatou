package ch.nth.android.paymentsdk.v2.model;

public class EndUser {
    private String msisdn;
    private String userId;

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public String getMsisdn() {
        return this.msisdn;
    }

    public void setMsisdn(String msisdn2) {
        this.msisdn = msisdn2;
    }

    public String toString() {
        return "EndUser ( " + super.toString() + "\n" + "userId = " + this.userId + "\n" + "msisdn = " + this.msisdn + "\n" + " )";
    }
}
