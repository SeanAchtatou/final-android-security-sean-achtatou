package ch.nth.android.paymentsdk.v2.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import ch.nth.android.paymentsdk.v2.fragments.base.BaseDialogWebViewFragment;
import ch.nth.android.paymentsdk.v2.listeners.PaymentResponseListener;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;

public class PaymentWebViewDialogFragment extends BaseDialogWebViewFragment {
    private static final String API_KEY = "ch.nth.paymentwidget.v2.API_KEY";
    private static final String API_SECRET = "ch.nth.paymentwidget.v2.API_SECRET";
    private static final String BASE_URL = "ch.nth.paymentwidget.v2.BASE_URL";
    private static final String CALLBACK = "ch.nth.paymentwidget.v2.CALLBACK";
    private static final String LAYOUT_RESOURCE_ID = "ch.nth.paymentwidget.v2.LAYOUT_RESOURCE_ID";
    private static final String REQUEST_ID = "ch.nth.paymentwidget.v2.REQUEST_ID";
    private static final String URL = "ch.nth.paymentwidget.v2.URL";
    private static final String VERIFY_EVERY_URL = "ch.nth.paymentwidget.v2.VERIFY_EVERY_URL";
    private static final String WEBVIEW_RESOURCE_ID = "ch.nth.paymentwidget.v2.WEBVIEW_RESOURCE_ID";
    private PaymentResponseListener mResponseListener;

    public static PaymentWebViewDialogFragment newInstance(int layoutResourceId, int webViewId, String url, String callback, String requestId, String baseUrl, String apiKey, String apiSecret, boolean verifyEveryUrl) {
        PaymentWebViewDialogFragment f = new PaymentWebViewDialogFragment();
        Bundle b = new Bundle();
        b.putInt(LAYOUT_RESOURCE_ID, layoutResourceId);
        b.putInt(WEBVIEW_RESOURCE_ID, webViewId);
        b.putString(URL, url);
        b.putString(CALLBACK, callback);
        b.putString(REQUEST_ID, requestId);
        b.putString(BASE_URL, baseUrl);
        b.putString(API_KEY, apiKey);
        b.putString(API_SECRET, apiSecret);
        b.putBoolean(VERIFY_EVERY_URL, verifyEveryUrl);
        f.setArguments(b);
        return f;
    }

    public void onDataReceived(String result) {
        notifyListener(result);
    }

    public void setResponseListener(PaymentResponseListener listener) {
        this.mResponseListener = listener;
    }

    private void notifyListener(String response) {
        if (this.mResponseListener == null) {
            return;
        }
        if (!TextUtils.isEmpty(response)) {
            this.mResponseListener.onSuccess(response);
        } else {
            this.mResponseListener.onError();
        }
    }

    public void onDataReceived(InfoResponse infoReponse) {
        if (this.mResponseListener != null) {
            this.mResponseListener.onRequestInfoRetrieved(infoReponse);
        }
    }

    public void onCheckWebUrl(InitPaymentParams url) {
        if (this.mResponseListener != null) {
            this.mResponseListener.onCheckWebUrl(url);
        }
    }
}
