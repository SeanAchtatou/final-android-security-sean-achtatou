package ch.nth.android.paymentsdk.v2.utils;

import android.text.TextUtils;
import android.util.Base64;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class PaymentUtils {
    public static final String ALGORITHM_HMAC_SHA1 = "HmacSHA1";

    public static void initWebViewAttributes(WebView ww) {
        if (ww != null) {
            try {
                WebSettings settings = ww.getSettings();
                settings.setUseWideViewPort(true);
                settings.setLoadWithOverviewMode(true);
                settings.setJavaScriptEnabled(true);
                settings.setJavaScriptCanOpenWindowsAutomatically(true);
                ww.setScrollBarStyle(0);
                ww.clearHistory();
                ww.clearFormData();
                ww.clearCache(true);
            } catch (Exception e) {
                Utils.doLogException(e);
            }
        }
    }

    public static String calculateSignature(URI uri, String secretKey) throws InvalidKeyException, NoSuchAlgorithmException {
        StringBuilder raw = new StringBuilder(uri.getPath());
        Map<String, String> params = new TreeMap<>();
        for (NameValuePair pair : URLEncodedUtils.parse(uri, "UTF-8")) {
            params.put(pair.getName(), pair.getValue());
        }
        if (!params.isEmpty()) {
            raw.append("?");
            Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                raw.append((String) entry.getKey()).append("=").append(!TextUtils.isEmpty((CharSequence) entry.getValue()) ? (String) entry.getValue() : "");
                if (iterator.hasNext()) {
                    raw.append("&");
                }
            }
        }
        return sha1(raw.toString(), secretKey);
    }

    public static String sha1(String data, String secretKey) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), ALGORITHM_HMAC_SHA1);
        Mac mac = Mac.getInstance(ALGORITHM_HMAC_SHA1);
        mac.init(signingKey);
        return Base64.encodeToString(mac.doFinal(data.getBytes()), 2);
    }

    public static String generateAuthorizationValue(String link, String apiKey, String apiSecret) {
        try {
            URI uri = new URI(link);
            return "ApiKey " + apiKey + ":" + calculateSignature(new URI(String.valueOf(uri.getPath()) + (!TextUtils.isEmpty(uri.getRawQuery()) ? "?" + uri.getRawQuery() : "")), apiSecret);
        } catch (InvalidKeyException e) {
            Utils.doLogException(e);
            return "";
        } catch (NoSuchAlgorithmException e2) {
            Utils.doLogException(e2);
            return "";
        } catch (URISyntaxException e3) {
            Utils.doLogException(e3);
            return "";
        } catch (Exception e4) {
            Utils.doLogException(e4);
            return "";
        }
    }
}
