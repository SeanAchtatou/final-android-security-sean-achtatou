package ch.nth.android.paymentsdk.v2.model;

import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;

public class GenericPageLoadResponse extends BasePaymentResult {
    private String confirmationResponse;
    private String confirmationUri;
    private String htmlContent;
    private String serviceResponse;
    private String serviceType;
    private String serviceUri;

    public String getServiceType() {
        return this.serviceType;
    }

    public void setServiceType(String serviceType2) {
        this.serviceType = serviceType2;
    }

    public String getServiceUri() {
        return this.serviceUri;
    }

    public void setServiceUri(String serviceUri2) {
        this.serviceUri = serviceUri2;
    }

    public String getServiceResponse() {
        return this.serviceResponse;
    }

    public void setServiceResponse(String serviceResponse2) {
        this.serviceResponse = serviceResponse2;
    }

    public String getConfirmationUri() {
        return this.confirmationUri;
    }

    public void setConfirmationUri(String confirmationUri2) {
        this.confirmationUri = confirmationUri2;
    }

    public String getConfirmationResponse() {
        return this.confirmationResponse;
    }

    public void setConfirmationResponse(String confirmationResponse2) {
        this.confirmationResponse = confirmationResponse2;
    }

    public String getHtmlContent() {
        return this.htmlContent;
    }

    public void setHtmlContent(String htmlContent2) {
        this.htmlContent = htmlContent2;
    }

    public String toString() {
        return "GenericPageLoadResponse ( " + super.toString() + "\n" + "serviceType = " + this.serviceType + "\n" + "serviceUri = " + this.serviceUri + "\n" + "serviceResponse = " + this.serviceResponse + "\n" + "confirmationUri = " + this.confirmationUri + "\n" + "confirmationResponse = " + this.confirmationResponse + "\n" + "htmlContent = " + this.htmlContent + "\n" + " )";
    }
}
