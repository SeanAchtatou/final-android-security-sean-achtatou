package ch.nth.android.paymentsdk.v2.listeners;

import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;

public interface GenericPaymentResponseListener {
    void onError(int i);

    void onResponseReceived(int i, BasePaymentResult basePaymentResult);
}
