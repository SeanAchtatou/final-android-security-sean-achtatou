package ch.nth.android.paymentsdk.v2.activities;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import ch.nth.android.paymentsdk.v2.async.AsyncInfoRequest;
import ch.nth.android.paymentsdk.v2.dialog.WebViewDialog;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import ch.nth.android.paymentsdk.v2.exceptions.MandatoryFieldException;
import ch.nth.android.paymentsdk.v2.listeners.GenericStringContentListener;
import ch.nth.android.paymentsdk.v2.listeners.PaymentResponseListener;
import ch.nth.android.paymentsdk.v2.listeners.PaymentResultStepListener;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.InitPayment;
import ch.nth.android.paymentsdk.v2.model.PaymentSessionStatus;
import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import ch.nth.android.paymentsdk.v2.payment.PaymentManager;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import ch.nth.android.paymentsdk.v2.view.PaymentWebView;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;

public abstract class BasePaymentActivity extends Activity {
    private boolean mDisplayWebViewDialog = true;
    private InitPayment mInitPayment;
    private PaymentManager mPaymentManager;
    private PaymentResponseListener mPaymentResponseListener = new PaymentResponseListener() {
        public void onSuccess(String data) {
            if (BasePaymentActivity.this.mPaymentSessionStatus == null) {
                BasePaymentActivity.this.mPaymentSessionStatus = new PaymentSessionStatus();
            }
            if (!TextUtils.isEmpty(data)) {
                BasePaymentActivity.this.mPaymentSessionStatus.setSessionId(Utils.extractSID(data));
            }
            BasePaymentActivity.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT, BasePaymentActivity.this.mPaymentSessionStatus);
        }

        public void onError() {
            if (BasePaymentActivity.this.mPaymentSessionStatus == null) {
                BasePaymentActivity.this.mPaymentSessionStatus = new PaymentSessionStatus();
            }
            BasePaymentActivity.this.onFailure(PaymentManagerSteps.INIT_PAYMENT, BasePaymentActivity.this.mPaymentSessionStatus);
        }

        public void onRequestInfoRetrieved(InfoResponse infoReponse) {
            BasePaymentActivity.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO, infoReponse);
        }

        public void onCheckWebUrl(InitPaymentParams url) {
            BasePaymentActivity.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT_URL, url);
        }
    };
    /* access modifiers changed from: private */
    public PaymentSessionStatus mPaymentSessionStatus;
    private PaymentResultStepListener mPaymentStepListener = new PaymentResultStepListener() {
        public void onPaymentError(int statusCode, PaymentManagerSteps step) {
            Utils.doLog("error, status code " + statusCode + ", step " + step);
            BasePaymentActivity.this.onFailure(step, Integer.valueOf(statusCode));
        }

        public void onPaymentReceived(int statusCode, BasePaymentResult status, PaymentManagerSteps step) {
            if (step == PaymentManagerSteps.INIT_PAYMENT) {
                BasePaymentActivity.this.mPaymentSessionStatus = (PaymentSessionStatus) status;
                if (!TextUtils.isEmpty(BasePaymentActivity.this.mPaymentSessionStatus.getRedirectUrl())) {
                    if (BasePaymentActivity.this.mSilentFetchInitData) {
                        BasePaymentActivity.this.fetchRedirectUrlContent(BasePaymentActivity.this.mPaymentSessionStatus.getRedirectUrl());
                    } else {
                        BasePaymentActivity.this.displayPaymentWebView();
                    }
                }
                BasePaymentActivity.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT_SHOW_WEB_VIEW, BasePaymentActivity.this.mPaymentSessionStatus);
            } else if (step == PaymentManagerSteps.VERIFY_PAYMENT) {
                BasePaymentActivity.this.onSuccess(step, status);
            } else if (step == PaymentManagerSteps.CANCEL_PAYMENT) {
                BasePaymentActivity.this.onSuccess(step, null);
            } else if (step == PaymentManagerSteps.CLOSE_PAYMENT) {
                BasePaymentActivity.this.onSuccess(step, null);
            }
        }
    };
    private PaymentWebView mPaymentWebView;
    /* access modifiers changed from: private */
    public boolean mSilentFetchInitData = false;
    private boolean mVerifyEveryUrl = false;
    private WebViewDialog wwDialog;

    /* access modifiers changed from: protected */
    public abstract int getWebViewLayoutResource();

    /* access modifiers changed from: protected */
    public abstract int getWebViewResourceId();

    /* access modifiers changed from: protected */
    public abstract void onFailure(PaymentManagerSteps paymentManagerSteps, Object obj);

    /* access modifiers changed from: protected */
    public abstract void onSuccess(PaymentManagerSteps paymentManagerSteps, Object obj);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPaymentManager = new PaymentManager(this, this.mPaymentStepListener);
    }

    /* access modifiers changed from: private */
    public void displayPaymentWebView() {
        try {
            String redirectUrl = this.mPaymentSessionStatus.getRedirectUrl();
            Utils.doLog(" display init " + redirectUrl + " " + this.mDisplayWebViewDialog);
            if (TextUtils.isEmpty(redirectUrl)) {
                onFailure(PaymentManagerSteps.INIT_PAYMENT, this.mPaymentSessionStatus);
            } else if (this.mDisplayWebViewDialog) {
                this.wwDialog = new WebViewDialog(this, getWebViewLayoutResource(), getWebViewResourceId(), this.mPaymentSessionStatus.getRedirectUrl(), this.mPaymentManager.getCallbackUrl(), this.mPaymentSessionStatus.getRequestId(), this.mPaymentManager.getBaseEndpoint(), this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), this.mVerifyEveryUrl);
                this.wwDialog.setResponseListener(this.mPaymentResponseListener);
                this.wwDialog.show();
            } else {
                this.mPaymentWebView = (PaymentWebView) findViewById(getWebViewResourceId());
                this.mPaymentWebView.setConfigData(this.mPaymentSessionStatus.getRedirectUrl(), this.mPaymentManager.getCallbackUrl(), this.mPaymentSessionStatus.getRequestId(), this.mPaymentManager.getBaseEndpoint(), this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), this.mVerifyEveryUrl);
                this.mPaymentWebView.setResponseListener(this.mPaymentResponseListener);
                this.mPaymentWebView.loadUrl();
            }
        } catch (Exception e) {
            Utils.doLogException(e);
            onFailure(PaymentManagerSteps.INIT_PAYMENT, this.mPaymentSessionStatus);
        }
    }

    private void displayPaymentWebViewWithPredefinedData(InitPaymentParams initPaymentParams) {
        try {
            if (this.mDisplayWebViewDialog) {
                this.wwDialog = new WebViewDialog(this, getWebViewLayoutResource(), getWebViewResourceId(), initPaymentParams.getRedirectUrl(), initPaymentParams.getCallbackUrl(), initPaymentParams.getRequestId(), this.mPaymentManager.getBaseEndpoint(), this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), this.mVerifyEveryUrl);
                this.wwDialog.setResponseListener(this.mPaymentResponseListener);
                this.wwDialog.show();
                return;
            }
            this.mPaymentWebView = (PaymentWebView) findViewById(getWebViewResourceId());
            this.mPaymentWebView.setResponseListener(this.mPaymentResponseListener);
            this.mPaymentWebView.setConfigData(initPaymentParams.getRedirectUrl(), initPaymentParams.getCallbackUrl(), initPaymentParams.getRequestId(), this.mPaymentManager.getBaseEndpoint(), this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), this.mVerifyEveryUrl);
            this.mPaymentWebView.loadUrl();
        } catch (Exception e) {
            Utils.doLogException(e);
            onFailure(PaymentManagerSteps.INIT_PAYMENT, this.mPaymentSessionStatus);
        }
    }

    private void dissmissWebView() {
        if (this.mDisplayWebViewDialog && this.wwDialog != null) {
            this.wwDialog.dismiss();
        }
    }

    public synchronized void initPayment(InitPayment initPaymentOptions) {
        try {
            this.mDisplayWebViewDialog = initPaymentOptions.isDisplayWebViewDialog();
            this.mSilentFetchInitData = initPaymentOptions.isSilentFetchData();
            this.mInitPayment = initPaymentOptions;
            this.mPaymentManager.initPayment(this.mInitPayment);
        } catch (MandatoryFieldException e) {
            Utils.doLogException(e);
            onFailure(PaymentManagerSteps.INIT_PAYMENT, null);
        }
        return;
    }

    public synchronized void initPayment(String pid, String referenceId) {
        if (getApplicationContext() != null) {
            this.mInitPayment = new InitPayment(getApplicationContext(), pid, Utils.getAvailableID(getApplicationContext()), referenceId);
            try {
                this.mPaymentManager.initPayment(this.mInitPayment);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.INIT_PAYMENT, null);
            }
        }
        return;
    }

    public synchronized void initPayment(String pid, String referenceId, int mcc, int mnc) {
        if (getApplicationContext() != null) {
            this.mInitPayment = new InitPayment(getApplicationContext(), pid, Utils.getAvailableID(getApplicationContext()), referenceId, mcc, mnc);
            try {
                this.mPaymentManager.initPayment(this.mInitPayment);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.INIT_PAYMENT, null);
            }
        }
        return;
    }

    public synchronized void initPayment(String pid, String referenceId, boolean displayWebViewDialog) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        initPayment(pid, referenceId);
    }

    public synchronized void initPayment(String pid, String referenceId, int mcc, int mnc, boolean displayWebViewDialog) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        initPayment(pid, referenceId, mcc, mnc);
    }

    public synchronized void initPayment(String pid, String referenceId, boolean displayWebViewDialog, boolean silentFetchData) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        this.mSilentFetchInitData = silentFetchData;
        initPayment(pid, referenceId);
    }

    public synchronized void initPayment(String pid, String referenceId, int mcc, int mnc, boolean displayWebViewDialog, boolean silentFetchData) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        this.mSilentFetchInitData = silentFetchData;
        initPayment(pid, referenceId, mcc, mnc);
    }

    public synchronized void verifyPayment(String sid) {
        if (getApplicationContext() != null) {
            try {
                this.mPaymentManager.verifyPayment(sid);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.VERIFY_PAYMENT, null);
            }
        }
        return;
    }

    public synchronized void cancelPayment(String sid) {
        if (getApplicationContext() != null) {
            try {
                this.mPaymentManager.cancelPayment(sid);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.CANCEL_PAYMENT, null);
            }
        }
        return;
    }

    public synchronized void closePayment(String sid) {
        if (getApplicationContext() != null) {
            try {
                this.mPaymentManager.closePayment(sid);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.CLOSE_PAYMENT, null);
            }
        }
        return;
    }

    public void setConfigData(String url, String apiKey, String apiSecret) {
        if (this.mPaymentManager != null) {
            this.mPaymentManager.setBaseEndpoint(url);
            this.mPaymentManager.setApiKey(apiKey);
            this.mPaymentManager.setApiSecret(apiSecret);
        }
    }

    /* access modifiers changed from: private */
    public void fetchRedirectUrlContent(String url) {
        Utils.doLog("doFetchRedirectUrlContent, url: " + url);
        Map<String, String> params = new HashMap<>();
        params.put("requestId", this.mPaymentSessionStatus.getRequestId());
        new AsyncInfoRequest(this.mPaymentManager.getBaseEndpoint(), params, this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), new GenericStringContentListener() {
            public void onContentNotAvailable(int statusCode) {
                BasePaymentActivity.this.onFailure(PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO, null);
            }

            public void onContentAvailable(int statusCode, String htmlContent) {
                try {
                    BasePaymentActivity.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO, (InfoResponse) new Gson().fromJson(htmlContent, InfoResponse.class));
                } catch (Exception e) {
                    Utils.doLogException(e);
                    BasePaymentActivity.this.onFailure(PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO, null);
                }
            }
        }).execute(new Void[0]);
        onSuccess(PaymentManagerSteps.INIT_PAYMENT_REDIRECT_URL_RESULT, new InitPaymentParams(this.mPaymentSessionStatus.getRedirectUrl(), this.mPaymentSessionStatus.getRequestId(), this.mPaymentManager.getCallbackUrl()));
    }

    public synchronized void initPaymentContinueWithPredefinedData(InitPaymentParams initPaymentParams, boolean displayWebViewDialog) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        displayPaymentWebViewWithPredefinedData(initPaymentParams);
    }

    public synchronized void dismissWebDialog() {
        dissmissWebView();
    }

    public void loadAllowedWebPage(InitPaymentParams pageParams) {
        if (this.wwDialog != null) {
            this.wwDialog.loadAllowedWebPage(pageParams);
        } else if (this.mPaymentWebView != null) {
            this.mPaymentWebView.loadAllowedWebPage(pageParams);
        }
    }

    public void setVerifyEveryUrl(boolean enabled) {
        this.mVerifyEveryUrl = enabled;
    }
}
