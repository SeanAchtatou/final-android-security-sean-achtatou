package ch.nth.android.paymentsdk.v2.dialog;

import android.content.Context;
import android.text.TextUtils;
import ch.nth.android.paymentsdk.v2.dialog.base.BaseWebViewDialog;
import ch.nth.android.paymentsdk.v2.listeners.PaymentResponseListener;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;

public class WebViewDialog extends BaseWebViewDialog {
    private PaymentResponseListener mResponseListener;

    public WebViewDialog(Context context, int resourceLayoutId, int resourceWebViewId, String url, String callback, String requestId, String baseUrl, String apiKey, String apiSecret, boolean verifyEveryUrl) {
        super(context, resourceLayoutId, resourceWebViewId, url, callback, requestId, baseUrl, apiKey, apiSecret, verifyEveryUrl);
    }

    public void onDataReceived(String result) {
        notifyListener(result);
    }

    public void setResponseListener(PaymentResponseListener listener) {
        this.mResponseListener = listener;
    }

    private void notifyListener(String response) {
        if (this.mResponseListener == null) {
            return;
        }
        if (!TextUtils.isEmpty(response)) {
            this.mResponseListener.onSuccess(response);
        } else {
            this.mResponseListener.onError();
        }
    }

    public void onDataReceived(InfoResponse infoReponse) {
        if (this.mResponseListener != null) {
            this.mResponseListener.onRequestInfoRetrieved(infoReponse);
        }
    }

    public void onCheckWebUrl(InitPaymentParams url) {
        if (this.mResponseListener != null) {
            this.mResponseListener.onCheckWebUrl(url);
        }
    }
}
