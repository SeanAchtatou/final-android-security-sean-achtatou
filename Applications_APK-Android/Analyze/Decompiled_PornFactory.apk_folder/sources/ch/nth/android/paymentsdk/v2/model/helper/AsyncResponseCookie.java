package ch.nth.android.paymentsdk.v2.model.helper;

import java.io.Serializable;

public class AsyncResponseCookie implements Serializable {
    private static final long serialVersionUID = 2684397849689656160L;
    private String comment;
    private String domain;
    private String name;
    private String path;
    private int[] ports;
    private String value;
    private int version;

    public String getDomain() {
        return this.domain;
    }

    public void setDomain(String domain2) {
        this.domain = domain2;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment2) {
        this.comment = comment2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version2) {
        this.version = version2;
    }

    public int[] getPorts() {
        return this.ports;
    }

    public void setPorts(int[] ports2) {
        this.ports = ports2;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
