package ch.nth.android.paymentsdk.v2.listeners;

public interface GenericResponseListener {
    void onError(int i);

    void onSuccess(int i);
}
