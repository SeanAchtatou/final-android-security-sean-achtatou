package ch.nth.android.paymentsdk.v2.model.helper;

import java.io.Serializable;

public class AsyncResponse implements Serializable {
    private static final long serialVersionUID = 2108094653774053855L;
    private String baseUrl;
    private String callback;
    private String htmlContent;
    private String requestId;
    private int statusCode;

    public AsyncResponse() {
    }

    public AsyncResponse(int statusCode2, String htmlContent2, String callback2, String baseUrl2, String requestId2) {
        this.statusCode = statusCode2;
        this.htmlContent = htmlContent2;
        this.callback = callback2;
        this.baseUrl = baseUrl2;
        this.requestId = requestId2;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(int statusCode2) {
        this.statusCode = statusCode2;
    }

    public String getHtmlContent() {
        return this.htmlContent;
    }

    public void setHtmlContent(String htmlContent2) {
        this.htmlContent = htmlContent2;
    }

    public String getCallback() {
        return this.callback;
    }

    public void setCallback(String callback2) {
        this.callback = callback2;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl2) {
        this.baseUrl = baseUrl2;
    }

    public String getRequestId() {
        return this.requestId;
    }

    public void setRequestId(String requestId2) {
        this.requestId = requestId2;
    }
}
