package ch.nth.android.paymentsdk.v2.fragments.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import ch.nth.android.paymentsdk.v2.async.AsyncInfoRequest;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import ch.nth.android.paymentsdk.v2.exceptions.MandatoryFieldException;
import ch.nth.android.paymentsdk.v2.fragments.PaymentWebViewDialogFragment;
import ch.nth.android.paymentsdk.v2.listeners.GenericStringContentListener;
import ch.nth.android.paymentsdk.v2.listeners.PaymentResponseListener;
import ch.nth.android.paymentsdk.v2.listeners.PaymentResultStepListener;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.InitPayment;
import ch.nth.android.paymentsdk.v2.model.PaymentSessionStatus;
import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import ch.nth.android.paymentsdk.v2.payment.PaymentManager;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import ch.nth.android.paymentsdk.v2.view.PaymentWebView;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;

public abstract class BasePaymentFragment extends Fragment {
    private boolean mDisplayWebViewDialog = true;
    public InitPayment mInitPayment;
    private PaymentManager mPaymentManager;
    private PaymentResponseListener mPaymentResponseListener = new PaymentResponseListener() {
        public void onSuccess(String data) {
            if (BasePaymentFragment.this.mPaymentSessionStatus == null) {
                BasePaymentFragment.this.mPaymentSessionStatus = new PaymentSessionStatus();
            }
            if (!TextUtils.isEmpty(data)) {
                BasePaymentFragment.this.mPaymentSessionStatus.setSessionId(Utils.extractSID(data));
            }
            BasePaymentFragment.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT, BasePaymentFragment.this.mPaymentSessionStatus);
        }

        public void onError() {
            if (BasePaymentFragment.this.mPaymentSessionStatus == null) {
                BasePaymentFragment.this.mPaymentSessionStatus = new PaymentSessionStatus();
            }
            BasePaymentFragment.this.onFailure(PaymentManagerSteps.INIT_PAYMENT, BasePaymentFragment.this.mPaymentSessionStatus);
        }

        public void onRequestInfoRetrieved(InfoResponse infoReponse) {
            BasePaymentFragment.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO, infoReponse);
        }

        public void onCheckWebUrl(InitPaymentParams url) {
            BasePaymentFragment.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT_URL, url);
        }
    };
    public PaymentSessionStatus mPaymentSessionStatus;
    private PaymentResultStepListener mPaymentStepListener = new PaymentResultStepListener() {
        public void onPaymentError(int statusCode, PaymentManagerSteps step) {
            Utils.doLog("error, status code " + statusCode + ", step " + step);
            BasePaymentFragment.this.onFailure(step, Integer.valueOf(statusCode));
        }

        public void onPaymentReceived(int statusCode, BasePaymentResult status, PaymentManagerSteps step) {
            Utils.doLog("payment step received " + step);
            if (step == PaymentManagerSteps.INIT_PAYMENT) {
                BasePaymentFragment.this.mPaymentSessionStatus = (PaymentSessionStatus) status;
                if (!TextUtils.isEmpty(BasePaymentFragment.this.mPaymentSessionStatus.getRedirectUrl())) {
                    if (BasePaymentFragment.this.mSilentFetchInitData) {
                        BasePaymentFragment.this.fetchRedirectUrlContent(BasePaymentFragment.this.mPaymentSessionStatus.getRedirectUrl());
                    } else {
                        BasePaymentFragment.this.displayPaymentWebView();
                    }
                }
                BasePaymentFragment.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT_SHOW_WEB_VIEW, BasePaymentFragment.this.mPaymentSessionStatus);
            } else if (step == PaymentManagerSteps.VERIFY_PAYMENT) {
                BasePaymentFragment.this.onSuccess(step, status);
            } else if (step == PaymentManagerSteps.CANCEL_PAYMENT) {
                BasePaymentFragment.this.onSuccess(step, null);
            } else if (step == PaymentManagerSteps.CLOSE_PAYMENT) {
                BasePaymentFragment.this.onSuccess(step, null);
            }
        }
    };
    private PaymentWebView mPaymentWebView;
    /* access modifiers changed from: private */
    public boolean mSilentFetchInitData = false;
    private boolean mVerifyEveryUrl = false;
    private PaymentWebViewDialogFragment newFragment;

    /* access modifiers changed from: protected */
    public abstract int getWebViewLayoutResource();

    /* access modifiers changed from: protected */
    public abstract int getWebViewResourceId();

    /* access modifiers changed from: protected */
    public abstract void onFailure(PaymentManagerSteps paymentManagerSteps, Object obj);

    /* access modifiers changed from: protected */
    public abstract void onSuccess(PaymentManagerSteps paymentManagerSteps, Object obj);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPaymentManager = new PaymentManager(getActivity(), this.mPaymentStepListener);
    }

    /* access modifiers changed from: private */
    public void displayPaymentWebView() {
        try {
            String redirectUrl = this.mPaymentSessionStatus.getRedirectUrl();
            Utils.doLog(" display init " + redirectUrl + " " + this.mDisplayWebViewDialog);
            if (TextUtils.isEmpty(redirectUrl)) {
                onFailure(PaymentManagerSteps.INIT_PAYMENT, this.mPaymentSessionStatus);
            } else if (this.mDisplayWebViewDialog) {
                this.newFragment = PaymentWebViewDialogFragment.newInstance(getWebViewLayoutResource(), getWebViewResourceId(), redirectUrl, this.mPaymentManager.getCallbackUrl(), this.mPaymentSessionStatus.getRequestId(), this.mPaymentManager.getBaseEndpoint(), this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), this.mVerifyEveryUrl);
                this.newFragment.setResponseListener(this.mPaymentResponseListener);
                this.newFragment.show(getFragmentManager(), "dialog");
            } else {
                this.mPaymentWebView = (PaymentWebView) getView().findViewById(getWebViewResourceId());
                this.mPaymentWebView.setResponseListener(this.mPaymentResponseListener);
                this.mPaymentWebView.setConfigData(redirectUrl, this.mPaymentManager.getCallbackUrl(), this.mPaymentSessionStatus.getRequestId(), this.mPaymentManager.getBaseEndpoint(), this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), this.mVerifyEveryUrl);
                this.mPaymentWebView.loadUrl();
            }
        } catch (Exception e) {
            Utils.doLogException(e);
            onFailure(PaymentManagerSteps.INIT_PAYMENT, this.mPaymentSessionStatus);
        }
    }

    private void displayPaymentWebViewWithPredefinedData(InitPaymentParams initPaymentParams) {
        try {
            if (this.mDisplayWebViewDialog) {
                this.newFragment = PaymentWebViewDialogFragment.newInstance(getWebViewLayoutResource(), getWebViewResourceId(), initPaymentParams.getRedirectUrl(), initPaymentParams.getCallbackUrl(), initPaymentParams.getRequestId(), this.mPaymentManager.getBaseEndpoint(), this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), this.mVerifyEveryUrl);
                this.newFragment.setResponseListener(this.mPaymentResponseListener);
                this.newFragment.show(getFragmentManager(), "dialog");
                return;
            }
            this.mPaymentWebView = (PaymentWebView) getView().findViewById(getWebViewResourceId());
            this.mPaymentWebView.setResponseListener(this.mPaymentResponseListener);
            this.mPaymentWebView.setConfigData(initPaymentParams.getRedirectUrl(), initPaymentParams.getCallbackUrl(), initPaymentParams.getRequestId(), this.mPaymentManager.getBaseEndpoint(), this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), this.mVerifyEveryUrl);
            this.mPaymentWebView.loadUrl();
        } catch (Exception e) {
            Utils.doLogException(e);
            onFailure(PaymentManagerSteps.INIT_PAYMENT, this.mPaymentSessionStatus);
        }
    }

    private void dissmissWebView() {
        if (this.mDisplayWebViewDialog && this.newFragment != null) {
            this.newFragment.dismiss();
        }
    }

    public synchronized void initPayment(InitPayment initPaymentOptions) {
        try {
            this.mDisplayWebViewDialog = initPaymentOptions.isDisplayWebViewDialog();
            this.mSilentFetchInitData = initPaymentOptions.isSilentFetchData();
            this.mInitPayment = initPaymentOptions;
            this.mPaymentManager.initPayment(this.mInitPayment);
        } catch (MandatoryFieldException e) {
            Utils.doLogException(e);
            onFailure(PaymentManagerSteps.INIT_PAYMENT, null);
        }
        return;
    }

    public synchronized void initPayment(String pid, String referenceId) {
        if (getActivity() != null) {
            this.mInitPayment = new InitPayment(getActivity(), pid, Utils.getAvailableID(getActivity()), referenceId);
            try {
                this.mPaymentManager.initPayment(this.mInitPayment);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.INIT_PAYMENT, null);
            }
        }
        return;
    }

    public synchronized void initPayment(String pid, String referenceId, int mcc, int mnc) {
        if (getActivity() != null) {
            this.mInitPayment = new InitPayment(getActivity(), pid, Utils.getAvailableID(getActivity()), referenceId, mcc, mnc);
            try {
                this.mPaymentManager.initPayment(this.mInitPayment);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.INIT_PAYMENT, null);
            }
        }
        return;
    }

    public synchronized void initPayment(String pid, String referenceId, boolean displayWebViewDialog) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        initPayment(pid, referenceId);
    }

    public synchronized void initPayment(String pid, String referenceId, int mcc, int mnc, boolean displayWebViewDialog) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        initPayment(pid, referenceId, mcc, mnc);
    }

    public synchronized void initPayment(String pid, String referenceId, boolean displayWebViewDialog, boolean silentFetchData) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        this.mSilentFetchInitData = silentFetchData;
        initPayment(pid, referenceId);
    }

    public synchronized void initPayment(String pid, String referenceId, int mcc, int mnc, boolean displayWebViewDialog, boolean silentFetchData) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        this.mSilentFetchInitData = silentFetchData;
        initPayment(pid, referenceId, mcc, mnc);
    }

    public synchronized void verifyPayment(String sid) {
        if (getActivity() != null) {
            try {
                this.mPaymentManager.verifyPayment(sid);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.VERIFY_PAYMENT, null);
            }
        }
        return;
    }

    public synchronized void cancelPayment(String sid) {
        if (getActivity() != null) {
            try {
                this.mPaymentManager.cancelPayment(sid);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.CANCEL_PAYMENT, null);
            }
        }
        return;
    }

    public synchronized void closePayment(String sid) {
        if (getActivity() != null) {
            try {
                this.mPaymentManager.closePayment(sid);
            } catch (MandatoryFieldException e) {
                Utils.doLogException(e);
                onFailure(PaymentManagerSteps.CLOSE_PAYMENT, null);
            }
        }
        return;
    }

    public void setConfigData(String url, String apiKey, String apiSecret) {
        if (this.mPaymentManager != null) {
            this.mPaymentManager.setBaseEndpoint(url);
            this.mPaymentManager.setApiKey(apiKey);
            this.mPaymentManager.setApiSecret(apiSecret);
        }
    }

    /* access modifiers changed from: private */
    public void fetchRedirectUrlContent(String url) {
        Utils.doLog("doFetchRedirectUrlContent, url: " + url);
        Map<String, String> params = new HashMap<>();
        params.put("requestId", this.mPaymentSessionStatus.getRequestId());
        new AsyncInfoRequest(this.mPaymentManager.getBaseEndpoint(), params, this.mPaymentManager.getApiKey(), this.mPaymentManager.getApiSecret(), new GenericStringContentListener() {
            public void onContentNotAvailable(int statusCode) {
                BasePaymentFragment.this.onFailure(PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO, null);
            }

            public void onContentAvailable(int statusCode, String htmlContent) {
                try {
                    BasePaymentFragment.this.onSuccess(PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO, (InfoResponse) new Gson().fromJson(htmlContent, InfoResponse.class));
                } catch (Exception e) {
                    Utils.doLogException(e);
                    BasePaymentFragment.this.onFailure(PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO, null);
                }
            }
        }).execute(new Void[0]);
        onSuccess(PaymentManagerSteps.INIT_PAYMENT_REDIRECT_URL_RESULT, new InitPaymentParams(this.mPaymentSessionStatus.getRedirectUrl(), this.mPaymentSessionStatus.getRequestId(), this.mPaymentManager.getCallbackUrl()));
    }

    public synchronized void initPaymentContinueWithPredefinedData(InitPaymentParams initPaymentParams, boolean displayWebViewDialog) {
        this.mDisplayWebViewDialog = displayWebViewDialog;
        displayPaymentWebViewWithPredefinedData(initPaymentParams);
    }

    public synchronized void dismissWebDialog() {
        dissmissWebView();
    }

    public void loadAllowedWebPage(InitPaymentParams pageParams) {
        if (this.newFragment != null) {
            this.newFragment.loadAllowedWebPage(pageParams);
        } else if (this.mPaymentWebView != null) {
            this.mPaymentWebView.loadAllowedWebPage(pageParams);
        }
    }

    public void setVerifyEveryUrl(boolean enabled) {
        this.mVerifyEveryUrl = enabled;
    }
}
