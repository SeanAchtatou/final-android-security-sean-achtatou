package ch.nth.android.paymentsdk.v2.async;

import android.net.Uri;
import android.os.AsyncTask;
import ch.nth.android.paymentsdk.v2.listeners.GenericStringContentListener;
import ch.nth.android.paymentsdk.v2.utils.PaymentUtils;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public class AsyncInfoRequest extends AsyncTask<Void, Void, String> {
    private String mApiKey;
    private String mApiSecret;
    private String mBaseUrl;
    private GenericStringContentListener mListener;
    private Map<String, String> mParams;
    private int mStatusCode = -1;

    public AsyncInfoRequest(String baseUrl, Map<String, String> params, String apiKey, String apiSecret, GenericStringContentListener listener) {
        this.mListener = listener;
        this.mBaseUrl = baseUrl;
        this.mParams = params;
        this.mApiKey = apiKey;
        this.mApiSecret = apiSecret;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Void... params) {
        HttpEntity entity;
        String content = "";
        try {
            Uri.Builder ub = Uri.parse(String.valueOf(this.mBaseUrl) + "requestInfo").buildUpon();
            if (this.mParams != null) {
                for (String key : this.mParams.keySet()) {
                    ub.appendQueryParameter(key, this.mParams.get(key));
                }
            }
            String url = ub.toString();
            String authorization = PaymentUtils.generateAuthorizationValue(url, this.mApiKey, this.mApiSecret);
            Utils.doLog("GET REQUEST: " + ub.build().toString() + " authVal " + authorization);
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
            HttpConnectionParams.setSoTimeout(httpParameters, 10000);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpGet httpGet = new HttpGet(url);
            httpGet.addHeader("Authorization", authorization);
            HttpResponse response = httpClient.execute(httpGet);
            this.mStatusCode = response.getStatusLine().getStatusCode();
            if ((this.mStatusCode == 200 || this.mStatusCode == 302) && (entity = response.getEntity()) != null) {
                content = EntityUtils.toString(entity);
            }
            Utils.doLog("status code " + this.mStatusCode);
        } catch (Exception e) {
            Utils.doLogException(e);
        }
        return content;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String htmlContent) {
        if (Utils.checkSuccessfulResponse(this.mStatusCode)) {
            this.mListener.onContentAvailable(this.mStatusCode, htmlContent);
        } else {
            this.mListener.onContentNotAvailable(this.mStatusCode);
        }
    }
}
