package ch.nth.android.paymentsdk.v2.enums;

public enum PaymentManagerSteps {
    INIT_PAYMENT,
    INIT_PAYMENT_SHOW_WEB_VIEW,
    INIT_PAYMENT_REDIRECT_URL_RESULT,
    VERIFY_PAYMENT,
    CANCEL_PAYMENT,
    CLOSE_PAYMENT,
    INIT_PAYMENT_REQUEST_INFO,
    INIT_PAYMENT_URL
}
