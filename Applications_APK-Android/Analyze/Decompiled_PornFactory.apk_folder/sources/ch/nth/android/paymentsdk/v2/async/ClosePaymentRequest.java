package ch.nth.android.paymentsdk.v2.async;

import android.content.Context;
import ch.nth.android.paymentsdk.v2.async.base.BaseRequest;
import ch.nth.android.paymentsdk.v2.listeners.GenericPaymentResponseListener;
import ch.nth.android.paymentsdk.v2.listeners.GenericResponseListener;
import java.util.Map;

public class ClosePaymentRequest extends BaseRequest {
    public ClosePaymentRequest(Context context, String baseEndpoint, String apiKey, String apiSecret, Map<String, String> params, GenericPaymentResponseListener listener) {
        super(context, baseEndpoint, apiKey, apiSecret, params, listener);
    }

    public void onExecuteRequest() {
        new AsyncPutRequest(String.valueOf(this.mBaseEndPoint) + "closePayment", this.mApiKey, this.mApiSecret, this.params, new GenericResponseListener() {
            public void onSuccess(int statusCode) {
                ClosePaymentRequest.this.notifyResponseListener(true, statusCode, null);
            }

            public void onError(int statusCode) {
                ClosePaymentRequest.this.notifyResponseListener(false, statusCode, null);
            }
        }).execute(new Void[0]);
    }
}
