package ch.nth.android.paymentsdk.v2.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.Toast;
import org.apache.http.Header;

public class Utils {
    private static final String TAG = "PaymentWidgetSDK_V2";

    public static void doLog(Object o) {
    }

    public static void doLogException(Exception e) {
    }

    public static void doToast(Context context, String text) {
        if (context != null) {
            Toast.makeText(context, text, 1).show();
        }
    }

    public static void doToast(Context context, int stringResId) {
        Toast.makeText(context, stringResId, 1).show();
    }

    public static void doToast(Context context, Object o) {
        Toast.makeText(context, o.toString(), 1).show();
    }

    public static boolean hasActiveNetworkConnection(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static String getAvailableID(Context context) {
        String availableID = getMSISDN(context);
        if (TextUtils.isEmpty(availableID)) {
            String availableID2 = getIMEI(context);
            if (!TextUtils.isEmpty(availableID2)) {
                return availableID2;
            }
            availableID = getAndroidID(context);
            if (TextUtils.isEmpty(availableID)) {
                availableID = "pw_app";
            }
        }
        return availableID;
    }

    public static String getIMEI(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static String getMSISDN(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
    }

    public static String getAndroidID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static int getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static boolean checkSuccessfulResponse(int code) {
        if (code / 100 == 2) {
            return true;
        }
        return false;
    }

    public static boolean shouldStartIntent(String url) {
        if (url.contains("tel:") || url.contains("mailto:") || url.contains("sms:")) {
            return true;
        }
        return false;
    }

    public static String extractSID(String data) {
        int index = data.indexOf("sid=");
        if (index != -1) {
            return data.substring(index, data.indexOf("&", data.indexOf("sid="))).split("=")[1];
        }
        return "";
    }

    public static String findValueInHeaders(Header[] headers, String value2find) {
        for (Header header : headers) {
            if (header.getName().equalsIgnoreCase(value2find)) {
                return header.getValue();
            }
        }
        return "";
    }
}
