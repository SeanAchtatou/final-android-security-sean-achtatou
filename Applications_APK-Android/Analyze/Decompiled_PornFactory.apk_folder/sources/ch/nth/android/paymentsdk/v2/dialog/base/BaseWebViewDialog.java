package ch.nth.android.paymentsdk.v2.dialog.base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.webkit.WebView;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.helper.AsyncResponse;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import ch.nth.android.paymentsdk.v2.payment.PaymentWebViewClient;
import ch.nth.android.paymentsdk.v2.utils.PaymentUtils;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import java.io.IOException;
import java.net.UnknownHostException;

public abstract class BaseWebViewDialog extends Dialog implements DialogInterface.OnDismissListener {
    /* access modifiers changed from: private */
    public String mApiKey = "";
    /* access modifiers changed from: private */
    public String mApiSecret = "";
    private AsyncResponse mAsyncResponse;
    /* access modifiers changed from: private */
    public String mBaseUrl = "";
    /* access modifiers changed from: private */
    public String mCallback = "";
    /* access modifiers changed from: private */
    public InitPaymentParams mInitPaymentParams;
    /* access modifiers changed from: private */
    public String mLocationToReturn = "";
    /* access modifiers changed from: private */
    public String mRequestId = "";
    private int mResourceLayoutId;
    private int mResourceWebViewId;
    private String mUrl = "";
    /* access modifiers changed from: private */
    public boolean mVerifyEveryUrl = false;
    private WebView mWebView;

    public abstract void onCheckWebUrl(InitPaymentParams initPaymentParams);

    public abstract void onDataReceived(InfoResponse infoResponse);

    public abstract void onDataReceived(String str);

    public BaseWebViewDialog(Context context, int resourceLayoutId, int resourceWebViewId, String url, String callback, String requestId, String baseUrl, String apiKey, String apiSecret, boolean verifyEveryUrl) {
        super(context);
        this.mResourceLayoutId = resourceLayoutId;
        this.mResourceWebViewId = resourceWebViewId;
        this.mUrl = url;
        this.mCallback = callback;
        this.mRequestId = requestId;
        this.mBaseUrl = baseUrl;
        this.mApiKey = apiKey;
        this.mApiSecret = apiSecret;
        this.mVerifyEveryUrl = verifyEveryUrl;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(this.mResourceLayoutId);
        this.mWebView = (WebView) findViewById(this.mResourceWebViewId);
        PaymentUtils.initWebViewAttributes(this.mWebView);
        this.mWebView.setWebViewClient(new CustomPaymentWebViewClient());
        setOnDismissListener(this);
        if (this.mAsyncResponse != null) {
            this.mWebView.loadData(this.mAsyncResponse.getHtmlContent(), "text/html", "utf-8");
        } else if (!TextUtils.isEmpty(this.mUrl)) {
            this.mWebView.loadUrl(this.mUrl);
        }
    }

    private class CustomPaymentWebViewClient extends PaymentWebViewClient {
        public CustomPaymentWebViewClient() {
            super(BaseWebViewDialog.this.mCallback, BaseWebViewDialog.this.mBaseUrl, BaseWebViewDialog.this.mRequestId, BaseWebViewDialog.this.mApiKey, BaseWebViewDialog.this.mApiSecret, BaseWebViewDialog.this.mVerifyEveryUrl);
        }

        public void onIOException(IOException exception) {
            if (exception instanceof UnknownHostException) {
                new Handler(BaseWebViewDialog.this.getContext().getMainLooper()).post(new Runnable() {
                    public void run() {
                        Utils.doToast(BaseWebViewDialog.this.getContext(), "Unable to resolve host");
                    }
                });
            }
        }

        public void onDataReceived(InfoResponse infoReponse) {
            BaseWebViewDialog.this.onDataReceived(infoReponse);
        }

        public void onReturnResult(String location) {
            BaseWebViewDialog.this.mLocationToReturn = location;
            BaseWebViewDialog.this.dismiss();
        }

        public void onWebViewReceivedError(int errorCode, String description, String failingUrl) {
            StringBuilder append = new StringBuilder("onWebViewReceivedError: ").append(errorCode).append(" ");
            if (TextUtils.isEmpty(description)) {
                description = "";
            }
            StringBuilder append2 = append.append(description).append(" ");
            if (TextUtils.isEmpty(failingUrl)) {
                failingUrl = "";
            }
            Utils.doLog(append2.append(failingUrl).toString());
            Utils.doToast(BaseWebViewDialog.this.getContext(), "There is problem with loading web site!");
        }

        public void onReturnUrl(InitPaymentParams url) {
            BaseWebViewDialog.this.onCheckWebUrl(url);
        }

        public InitPaymentParams getAllowedPage() {
            return BaseWebViewDialog.this.mInitPaymentParams;
        }
    }

    public void onDismiss(DialogInterface di) {
        onDataReceived(this.mLocationToReturn);
    }

    public void loadAllowedWebPage(InitPaymentParams pageParams) {
        this.mInitPaymentParams = pageParams;
        this.mWebView.loadUrl(pageParams.getRedirectUrl());
    }
}
