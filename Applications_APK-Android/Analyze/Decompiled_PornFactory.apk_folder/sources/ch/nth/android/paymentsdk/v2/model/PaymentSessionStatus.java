package ch.nth.android.paymentsdk.v2.model;

import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;

public class PaymentSessionStatus extends BasePaymentResult {
    private InitPayment originalPaymentItem;
    private String redirectUrl;
    private String requestId;
    private String sessionId;
    private String status;
    private String type;

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId2) {
        this.sessionId = sessionId2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public String getRedirectUrl() {
        return this.redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl2) {
        this.redirectUrl = redirectUrl2;
    }

    public InitPayment getOriginalPaymentItem() {
        return this.originalPaymentItem;
    }

    public void setOriginalPaymentItem(InitPayment originalPaymentItem2) {
        this.originalPaymentItem = originalPaymentItem2;
    }

    public String getRequestId() {
        return this.requestId;
    }

    public void setRequestId(String requestId2) {
        this.requestId = requestId2;
    }

    public String toString() {
        return "PaymentSessionStatus ( " + super.toString() + "\n" + "sessionId = " + this.sessionId + "\n" + "type = " + this.type + "\n" + "status = " + this.status + "\n" + "redirectUrl = " + this.redirectUrl + "\n" + "requestId = " + this.requestId + "\n" + " )";
    }
}
