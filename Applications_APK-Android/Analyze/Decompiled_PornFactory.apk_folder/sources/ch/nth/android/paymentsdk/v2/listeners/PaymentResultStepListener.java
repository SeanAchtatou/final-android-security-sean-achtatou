package ch.nth.android.paymentsdk.v2.listeners;

import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;

public interface PaymentResultStepListener {
    void onPaymentError(int i, PaymentManagerSteps paymentManagerSteps);

    void onPaymentReceived(int i, BasePaymentResult basePaymentResult, PaymentManagerSteps paymentManagerSteps);
}
