package ch.nth.android.paymentsdk.v2.model.header;

import java.io.Serializable;
import java.util.Map;

public class PaymentHeaders implements Serializable {
    private static final long serialVersionUID = 3273788597704555022L;
    private Map<String, String> map;

    public PaymentHeaders(Map<String, String> map2) {
        this.map = map2;
    }

    public Map<String, String> getMap() {
        return this.map;
    }

    public void setMap(Map<String, String> map2) {
        this.map = map2;
    }

    public String toString() {
        String toString = "";
        for (Map.Entry<String, String> ent : this.map.entrySet()) {
            toString = "header " + ((String) ent.getKey()) + " -> " + ((String) ent.getValue()) + "\n";
        }
        return toString;
    }
}
