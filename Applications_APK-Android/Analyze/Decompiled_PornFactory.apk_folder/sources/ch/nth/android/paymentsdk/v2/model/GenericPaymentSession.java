package ch.nth.android.paymentsdk.v2.model;

import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;

public class GenericPaymentSession extends BasePaymentResult {
    private int amount;
    private String billingText;
    private String currencyCode;
    private EndUser endUser;
    private String sessionId;
    private String status;
    private String timeAuthorized;
    private String timeInit;
    private String timePayment;
    private String type;

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId2) {
        this.sessionId = sessionId2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public EndUser getEndUser() {
        return this.endUser;
    }

    public void setEndUser(EndUser endUser2) {
        this.endUser = endUser2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount2) {
        this.amount = amount2;
    }

    public String getCurrencyCode() {
        return this.currencyCode;
    }

    public void setCurrencyCode(String currencyCode2) {
        this.currencyCode = currencyCode2;
    }

    public String getBillingText() {
        return this.billingText;
    }

    public void setBillingText(String billingText2) {
        this.billingText = billingText2;
    }

    public String getTimeInit() {
        return this.timeInit;
    }

    public void setTimeInit(String timeInit2) {
        this.timeInit = timeInit2;
    }

    public String getTimeAuthorized() {
        return this.timeAuthorized;
    }

    public void setTimeAuthorized(String timeAuthorized2) {
        this.timeAuthorized = timeAuthorized2;
    }

    public String getTimePayment() {
        return this.timePayment;
    }

    public void setTimePayment(String timePayment2) {
        this.timePayment = timePayment2;
    }

    public String toString() {
        return "GenericPaymentSession ( " + super.toString() + "\n" + "sessionId = " + this.sessionId + "\n" + "type = " + this.type + "\n" + "endUser = " + this.endUser.toString() + "\n" + "status = " + this.status + "\n" + "amount = " + this.amount + "\n" + "currencyCode = " + this.currencyCode + "\n" + "billingText = " + this.billingText + "\n" + "timeInit = " + this.timeInit + "\n" + "timeAuthorized = " + this.timeAuthorized + "\n" + "timePayment = " + this.timePayment + "\n" + " )";
    }
}
