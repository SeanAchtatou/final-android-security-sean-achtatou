package ch.nth.android.paymentsdk.v2.async;

import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import ch.nth.android.paymentsdk.v2.listeners.GenericStringContentListener;
import ch.nth.android.paymentsdk.v2.model.helper.AsyncResponse;
import ch.nth.android.paymentsdk.v2.utils.PaymentUtils;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public class AsyncPostRequest extends AsyncTask<Void, Void, AsyncResponse> {
    private String mApiKey;
    private String mApiSecret;
    private Map<String, String> mHeaders;
    private GenericStringContentListener mListener;
    private Map<String, String> mParams;
    private String mUrl;

    public AsyncPostRequest(String url, String apiKey, String apiSecret, GenericStringContentListener listener) {
        this.mUrl = url;
        this.mApiKey = apiKey;
        this.mApiSecret = apiSecret;
        this.mListener = listener;
    }

    public AsyncPostRequest(String url, String apiKey, String apiSecret, Map<String, String> params, GenericStringContentListener listener) {
        this(url, apiKey, apiSecret, listener);
        this.mParams = params;
    }

    public AsyncPostRequest(String url, String apiKey, String apiSecret, Map<String, String> params, Map<String, String> headers, GenericStringContentListener listener) {
        this(url, apiKey, apiSecret, params, listener);
        this.mHeaders = headers;
    }

    /* access modifiers changed from: protected */
    public AsyncResponse doInBackground(Void... params) {
        AsyncResponse aResponse = new AsyncResponse();
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
        HttpConnectionParams.setSoTimeout(httpParameters, 10000);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        try {
            Uri.Builder ub = Uri.parse(this.mUrl).buildUpon();
            if (this.mParams != null) {
                for (String key : this.mParams.keySet()) {
                    ub.appendQueryParameter(key, this.mParams.get(key));
                }
            }
            String authVal = generateAuthorizationValue(ub.toString(), this.mApiKey, this.mApiSecret);
            HttpPost httpPost = new HttpPost(ub.toString());
            httpPost.addHeader("Authorization", authVal);
            if (this.mHeaders != null) {
                for (String key2 : this.mHeaders.keySet()) {
                    httpPost.addHeader(key2, this.mHeaders.get(key2));
                }
            }
            Utils.doLog("POST REQUEST: " + ub.build().toString() + " authVal " + authVal);
            HttpResponse response = httpClient.execute(httpPost);
            aResponse.setStatusCode(response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                aResponse.setHtmlContent(EntityUtils.toString(entity));
            }
        } catch (Exception e) {
            Utils.doLogException(e);
        }
        return aResponse;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(AsyncResponse aResponse) {
        if (Utils.checkSuccessfulResponse(aResponse.getStatusCode())) {
            this.mListener.onContentAvailable(aResponse.getStatusCode(), aResponse.getHtmlContent());
        } else {
            this.mListener.onContentNotAvailable(aResponse.getStatusCode());
        }
    }

    private String generateAuthorizationValue(String link, String apiKey, String apiSecret) {
        try {
            URI uri = new URI(link);
            return "ApiKey " + apiKey + ":" + PaymentUtils.calculateSignature(new URI(String.valueOf(uri.getPath()) + (!TextUtils.isEmpty(uri.getRawQuery()) ? "?" + uri.getRawQuery() : "")), apiSecret);
        } catch (InvalidKeyException e) {
            Utils.doLogException(e);
            return "";
        } catch (NoSuchAlgorithmException e2) {
            Utils.doLogException(e2);
            return "";
        } catch (URISyntaxException e3) {
            Utils.doLogException(e3);
            return "";
        } catch (Exception e4) {
            Utils.doLogException(e4);
            return "";
        }
    }
}
