package ch.nth.android.paymentsdk.v2.async;

import android.os.AsyncTask;
import android.text.TextUtils;
import ch.nth.android.paymentsdk.v2.listeners.GenericResponseListener;
import ch.nth.android.paymentsdk.v2.utils.PaymentUtils;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class AsyncPutRequest extends AsyncTask<Void, Void, Integer> {
    private String mApiKey;
    private String mApiSecret;
    private GenericResponseListener mListener;
    private Map<String, String> mParams;
    private String mUrl;

    public AsyncPutRequest(String url, String apiKey, String apiSecret, GenericResponseListener listener) {
        this.mUrl = url;
        this.mApiKey = apiKey;
        this.mApiSecret = apiSecret;
        this.mListener = listener;
    }

    public AsyncPutRequest(String url, String apiKey, String apiSecret, Map<String, String> params, GenericResponseListener listener) {
        this(url, apiKey, apiSecret, listener);
        this.mParams = params;
    }

    /* JADX WARN: Type inference failed for: r10v15, types: [java.net.URLConnection] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00be, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00bf, code lost:
        ch.nth.android.paymentsdk.v2.utils.Utils.doLogException(r3);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer doInBackground(java.lang.Void... r14) {
        /*
            r13 = this;
            r10 = -1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r10)
            java.lang.String r10 = r13.mUrl     // Catch:{ Exception -> 0x00a7 }
            android.net.Uri r10 = android.net.Uri.parse(r10)     // Catch:{ Exception -> 0x00a7 }
            android.net.Uri$Builder r7 = r10.buildUpon()     // Catch:{ Exception -> 0x00a7 }
            java.util.Map<java.lang.String, java.lang.String> r10 = r13.mParams     // Catch:{ Exception -> 0x00a7 }
            if (r10 == 0) goto L_0x0023
            java.util.Map<java.lang.String, java.lang.String> r10 = r13.mParams     // Catch:{ Exception -> 0x00a7 }
            java.util.Set r10 = r10.keySet()     // Catch:{ Exception -> 0x00a7 }
            java.util.Iterator r5 = r10.iterator()     // Catch:{ Exception -> 0x00a7 }
        L_0x001d:
            boolean r10 = r5.hasNext()     // Catch:{ Exception -> 0x00a7 }
            if (r10 != 0) goto L_0x0094
        L_0x0023:
            java.lang.String r10 = r7.toString()     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r11 = r13.mApiKey     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r12 = r13.mApiSecret     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r2 = r13.generateAuthorizationValue(r10, r11, r12)     // Catch:{ Exception -> 0x00a7 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r11 = "PUT REQUEST: "
            r10.<init>(r11)     // Catch:{ Exception -> 0x00a7 }
            android.net.Uri r11 = r7.build()     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x00a7 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r11 = " authVal "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x00a7 }
            java.lang.StringBuilder r10 = r10.append(r2)     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00a7 }
            ch.nth.android.paymentsdk.v2.utils.Utils.doLog(r10)     // Catch:{ Exception -> 0x00a7 }
            r8 = 0
            r4 = 0
            java.net.URL r9 = new java.net.URL     // Catch:{ Exception -> 0x00ac }
            android.net.Uri r10 = r7.build()     // Catch:{ Exception -> 0x00ac }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00ac }
            r9.<init>(r10)     // Catch:{ Exception -> 0x00ac }
            java.net.URLConnection r10 = r9.openConnection()     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r0 = r10
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r4 = r0
            r10 = 10000(0x2710, float:1.4013E-41)
            r4.setConnectTimeout(r10)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r10 = 10000(0x2710, float:1.4013E-41)
            r4.setReadTimeout(r10)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            java.lang.String r10 = "Authorization"
            r4.setRequestProperty(r10, r2)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r10 = 1
            r4.setDoOutput(r10)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            java.lang.String r10 = "PUT"
            r4.setRequestMethod(r10)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r4.getResponseCode()     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r4.getInputStream()     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            int r10 = r4.getResponseCode()     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r4.disconnect()     // Catch:{ Exception -> 0x00c3 }
        L_0x0093:
            return r1
        L_0x0094:
            java.lang.Object r6 = r5.next()     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ Exception -> 0x00a7 }
            java.util.Map<java.lang.String, java.lang.String> r10 = r13.mParams     // Catch:{ Exception -> 0x00a7 }
            java.lang.Object r10 = r10.get(r6)     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ Exception -> 0x00a7 }
            r7.appendQueryParameter(r6, r10)     // Catch:{ Exception -> 0x00a7 }
            goto L_0x001d
        L_0x00a7:
            r3 = move-exception
            ch.nth.android.paymentsdk.v2.utils.Utils.doLogException(r3)
            goto L_0x0093
        L_0x00ac:
            r3 = move-exception
        L_0x00ad:
            ch.nth.android.paymentsdk.v2.utils.Utils.doLogException(r3)     // Catch:{ all -> 0x00b9 }
            r4.disconnect()     // Catch:{ Exception -> 0x00b4 }
            goto L_0x0093
        L_0x00b4:
            r3 = move-exception
            ch.nth.android.paymentsdk.v2.utils.Utils.doLogException(r3)     // Catch:{ Exception -> 0x00a7 }
            goto L_0x0093
        L_0x00b9:
            r10 = move-exception
        L_0x00ba:
            r4.disconnect()     // Catch:{ Exception -> 0x00be }
        L_0x00bd:
            throw r10     // Catch:{ Exception -> 0x00a7 }
        L_0x00be:
            r3 = move-exception
            ch.nth.android.paymentsdk.v2.utils.Utils.doLogException(r3)     // Catch:{ Exception -> 0x00a7 }
            goto L_0x00bd
        L_0x00c3:
            r3 = move-exception
            ch.nth.android.paymentsdk.v2.utils.Utils.doLogException(r3)     // Catch:{ Exception -> 0x00a7 }
            goto L_0x0093
        L_0x00c8:
            r10 = move-exception
            r8 = r9
            goto L_0x00ba
        L_0x00cb:
            r3 = move-exception
            r8 = r9
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.paymentsdk.v2.async.AsyncPutRequest.doInBackground(java.lang.Void[]):java.lang.Integer");
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer statusCode) {
        if (Utils.checkSuccessfulResponse(statusCode.intValue())) {
            this.mListener.onSuccess(statusCode.intValue());
        } else {
            this.mListener.onError(statusCode.intValue());
        }
    }

    private String generateAuthorizationValue(String link, String apiKey, String apiSecret) {
        try {
            URI uri = new URI(link);
            return "ApiKey " + apiKey + ":" + PaymentUtils.calculateSignature(new URI(String.valueOf(uri.getPath()) + (!TextUtils.isEmpty(uri.getRawQuery()) ? "?" + uri.getRawQuery() : "")), apiSecret);
        } catch (InvalidKeyException e) {
            Utils.doLogException(e);
            return "";
        } catch (NoSuchAlgorithmException e2) {
            Utils.doLogException(e2);
            return "";
        } catch (URISyntaxException e3) {
            Utils.doLogException(e3);
            return "";
        } catch (Exception e4) {
            Utils.doLogException(e4);
            return "";
        }
    }
}
