package ch.nth.android.paymentsdk.v2.listeners;

import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;

public interface PaymentResponseListener {
    void onCheckWebUrl(InitPaymentParams initPaymentParams);

    void onError();

    void onRequestInfoRetrieved(InfoResponse infoResponse);

    void onSuccess(String str);
}
