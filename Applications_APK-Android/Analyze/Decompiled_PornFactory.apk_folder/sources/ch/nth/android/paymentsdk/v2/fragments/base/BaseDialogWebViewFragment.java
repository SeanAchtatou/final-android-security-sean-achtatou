package ch.nth.android.paymentsdk.v2.fragments.base;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.helper.AsyncResponse;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import ch.nth.android.paymentsdk.v2.payment.PaymentWebViewClient;
import ch.nth.android.paymentsdk.v2.utils.PaymentUtils;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import java.io.IOException;
import java.net.UnknownHostException;

public abstract class BaseDialogWebViewFragment extends DialogFragment {
    private static final String API_KEY = "ch.nth.paymentwidget.v2.API_KEY";
    private static final String API_SECRET = "ch.nth.paymentwidget.v2.API_SECRET";
    private static final String BASE_URL = "ch.nth.paymentwidget.v2.BASE_URL";
    private static final String CALLBACK = "ch.nth.paymentwidget.v2.CALLBACK";
    private static final String LAYOUT_RESOURCE_ID = "ch.nth.paymentwidget.v2.LAYOUT_RESOURCE_ID";
    private static final String REQUEST_ID = "ch.nth.paymentwidget.v2.REQUEST_ID";
    private static final String URL = "ch.nth.paymentwidget.v2.URL";
    private static final String VERIFY_EVERY_URL = "ch.nth.paymentwidget.v2.VERIFY_EVERY_URL";
    private static final String WEBVIEW_RESOURCE_ID = "ch.nth.paymentwidget.v2.WEBVIEW_RESOURCE_ID";
    /* access modifiers changed from: private */
    public String mApiKey = "";
    /* access modifiers changed from: private */
    public String mApiSecret = "";
    private AsyncResponse mAsyncResponse;
    /* access modifiers changed from: private */
    public String mBaseUrl = "";
    /* access modifiers changed from: private */
    public String mCallback = "";
    /* access modifiers changed from: private */
    public InitPaymentParams mInitPaymentParams;
    private int mLayoutResourceId;
    /* access modifiers changed from: private */
    public String mLocationToReturn = "";
    public WebView mPaymentWebView;
    /* access modifiers changed from: private */
    public String mRequestId = "";
    private String mUrl = "";
    /* access modifiers changed from: private */
    public boolean mVerifyEveryUrl = false;
    private int mWebViewResourceId;

    public abstract void onCheckWebUrl(InitPaymentParams initPaymentParams);

    public abstract void onDataReceived(InfoResponse infoResponse);

    public abstract void onDataReceived(String str);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(URL)) {
                this.mUrl = getArguments().getString(URL);
            }
            if (getArguments().containsKey(CALLBACK)) {
                this.mCallback = getArguments().getString(CALLBACK);
            }
            if (getArguments().containsKey(LAYOUT_RESOURCE_ID)) {
                this.mLayoutResourceId = getArguments().getInt(LAYOUT_RESOURCE_ID);
            }
            if (getArguments().containsKey(WEBVIEW_RESOURCE_ID)) {
                this.mWebViewResourceId = getArguments().getInt(WEBVIEW_RESOURCE_ID);
            }
            if (getArguments().containsKey(BASE_URL)) {
                this.mBaseUrl = getArguments().getString(BASE_URL);
            }
            if (getArguments().containsKey(REQUEST_ID)) {
                this.mRequestId = getArguments().getString(REQUEST_ID);
            }
            if (getArguments().containsKey(API_KEY)) {
                this.mApiKey = getArguments().getString(API_KEY);
            }
            if (getArguments().containsKey(API_SECRET)) {
                this.mApiSecret = getArguments().getString(API_SECRET);
            }
            if (getArguments().containsKey(VERIFY_EVERY_URL)) {
                this.mVerifyEveryUrl = getArguments().getBoolean(VERIFY_EVERY_URL);
            }
        }
    }

    public void onDismiss(DialogInterface dialog) {
        onDataReceived(this.mLocationToReturn);
        super.onDismiss(dialog);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mLayoutResourceId, container, false);
        hideDialogTitle();
        this.mPaymentWebView = (WebView) view.findViewById(this.mWebViewResourceId);
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PaymentUtils.initWebViewAttributes(this.mPaymentWebView);
        this.mPaymentWebView.setWebViewClient(new CustomPaymentWebViewClient());
        if (this.mAsyncResponse != null) {
            this.mPaymentWebView.loadData(this.mAsyncResponse.getHtmlContent(), "text/html", "utf-8");
        } else if (!TextUtils.isEmpty(this.mUrl)) {
            this.mPaymentWebView.loadUrl(this.mUrl);
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    private class CustomPaymentWebViewClient extends PaymentWebViewClient {
        public CustomPaymentWebViewClient() {
            super(BaseDialogWebViewFragment.this.mCallback, BaseDialogWebViewFragment.this.mBaseUrl, BaseDialogWebViewFragment.this.mRequestId, BaseDialogWebViewFragment.this.mApiKey, BaseDialogWebViewFragment.this.mApiSecret, BaseDialogWebViewFragment.this.mVerifyEveryUrl);
        }

        public void onIOException(IOException exception) {
            if ((exception instanceof UnknownHostException) && BaseDialogWebViewFragment.this.getActivity() != null) {
                BaseDialogWebViewFragment.this.getActivity().runOnUiThread(new Runnable() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ch.nth.android.paymentsdk.v2.utils.Utils.doToast(android.content.Context, java.lang.String):void
                     arg types: [android.support.v4.app.FragmentActivity, java.lang.String]
                     candidates:
                      ch.nth.android.paymentsdk.v2.utils.Utils.doToast(android.content.Context, int):void
                      ch.nth.android.paymentsdk.v2.utils.Utils.doToast(android.content.Context, java.lang.Object):void
                      ch.nth.android.paymentsdk.v2.utils.Utils.doToast(android.content.Context, java.lang.String):void */
                    public void run() {
                        Utils.doToast((Context) BaseDialogWebViewFragment.this.getActivity(), "Unable to resolve host");
                    }
                });
            }
        }

        public void onDataReceived(InfoResponse infoReponse) {
            BaseDialogWebViewFragment.this.onDataReceived(infoReponse);
        }

        public void onReturnResult(String location) {
            BaseDialogWebViewFragment.this.mLocationToReturn = location;
            if (BaseDialogWebViewFragment.this.getDialog() != null) {
                BaseDialogWebViewFragment.this.getDialog().dismiss();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ch.nth.android.paymentsdk.v2.utils.Utils.doToast(android.content.Context, java.lang.String):void
         arg types: [android.support.v4.app.FragmentActivity, java.lang.String]
         candidates:
          ch.nth.android.paymentsdk.v2.utils.Utils.doToast(android.content.Context, int):void
          ch.nth.android.paymentsdk.v2.utils.Utils.doToast(android.content.Context, java.lang.Object):void
          ch.nth.android.paymentsdk.v2.utils.Utils.doToast(android.content.Context, java.lang.String):void */
        public void onWebViewReceivedError(int errorCode, String description, String failingUrl) {
            StringBuilder append = new StringBuilder("onWebViewReceivedError: ").append(errorCode).append(" ");
            if (TextUtils.isEmpty(description)) {
                description = "";
            }
            StringBuilder append2 = append.append(description).append(" ");
            if (TextUtils.isEmpty(failingUrl)) {
                failingUrl = "";
            }
            Utils.doLog(append2.append(failingUrl).toString());
            Utils.doToast((Context) BaseDialogWebViewFragment.this.getActivity(), "There is problem with loading web site!");
        }

        public void onReturnUrl(InitPaymentParams url) {
            BaseDialogWebViewFragment.this.onCheckWebUrl(url);
        }

        public InitPaymentParams getAllowedPage() {
            return BaseDialogWebViewFragment.this.mInitPaymentParams;
        }
    }

    private void hideDialogTitle() {
        try {
            getDialog().getWindow().requestFeature(1);
        } catch (Exception e) {
            Utils.doLogException(e);
        }
    }

    public void loadAllowedWebPage(InitPaymentParams pageParams) {
        this.mInitPaymentParams = pageParams;
        this.mPaymentWebView.loadUrl(pageParams.getRedirectUrl());
    }
}
