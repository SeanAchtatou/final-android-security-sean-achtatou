package ch.nth.android.paymentsdk.v2.model;

import android.content.Context;
import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;

public class InitPayment extends BasePaymentResult {
    private String country;
    private boolean displayWebViewDialog;
    private String imageUrl;
    private String lang;
    private int mcc;
    private int mnc;
    private String notifyUrl;
    private String phone;
    private String pid;
    private String referencedId;
    private boolean silentFetchData;
    private String userId;

    public InitPayment() {
    }

    public InitPayment(InitPaymentBuilder builder) {
        this.pid = builder.pid;
        this.userId = builder.userId;
        this.referencedId = builder.referencedId;
        this.mcc = builder.mcc;
        this.mnc = builder.mnc;
        this.country = builder.country;
        this.phone = builder.phone;
        this.lang = builder.lang;
        this.notifyUrl = builder.notifyUrl;
        this.imageUrl = builder.imageUrl;
        this.displayWebViewDialog = builder.displayWebViewDialog;
        this.silentFetchData = builder.silentFetchData;
    }

    public InitPayment(Context context, String pid2, String userId2, String referencedId2) {
        this.pid = pid2;
        this.userId = userId2;
        this.referencedId = referencedId2;
    }

    public InitPayment(Context context, String pid2, String userId2, String referencedId2, int mcc2, int mnc2) {
        this(context, pid2, userId2, referencedId2);
        this.mcc = mcc2;
        this.mnc = mnc2;
    }

    public String getPid() {
        return this.pid;
    }

    public void setPid(String pid2) {
        this.pid = pid2;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public String getReferencedId() {
        return this.referencedId;
    }

    public void setReferencedId(String referencedId2) {
        this.referencedId = referencedId2;
    }

    public int getMcc() {
        return this.mcc;
    }

    public void setMcc(int mcc2) {
        this.mcc = mcc2;
    }

    public int getMnc() {
        return this.mnc;
    }

    public void setMnc(int mnc2) {
        this.mnc = mnc2;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country2) {
        this.country = country2;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }

    public String getLang() {
        return this.lang;
    }

    public void setLang(String lang2) {
        this.lang = lang2;
    }

    public String getNotifyUrl() {
        return this.notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl2) {
        this.notifyUrl = notifyUrl2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public boolean isDisplayWebViewDialog() {
        return this.displayWebViewDialog;
    }

    public void setDisplayWebViewDialog(boolean displayWebViewDialog2) {
        this.displayWebViewDialog = displayWebViewDialog2;
    }

    public boolean isSilentFetchData() {
        return this.silentFetchData;
    }

    public void setSilentFetchData(boolean silentFetchData2) {
        this.silentFetchData = silentFetchData2;
    }

    public String toString() {
        return "InitPayment ( " + super.toString() + "\n" + "pid = " + this.pid + "\n" + "mcc = " + this.mcc + "\n" + "mnc = " + this.mnc + "\n" + "country = " + this.country + "\n" + "userId = " + this.userId + "\n" + "referencedId = " + this.referencedId + "\n" + "phone = " + this.phone + "\n" + "lang = " + this.lang + "\n" + "notifyUrl = " + this.notifyUrl + "\n" + "imageUrl = " + this.imageUrl + "\n" + "displayWebViewDialog = " + this.displayWebViewDialog + "\n" + "silentFetchData = " + this.silentFetchData + "\n" + " )";
    }

    public static class InitPaymentBuilder {
        /* access modifiers changed from: private */
        public String country;
        /* access modifiers changed from: private */
        public boolean displayWebViewDialog;
        /* access modifiers changed from: private */
        public String imageUrl;
        /* access modifiers changed from: private */
        public String lang;
        /* access modifiers changed from: private */
        public int mcc;
        /* access modifiers changed from: private */
        public int mnc;
        /* access modifiers changed from: private */
        public String notifyUrl;
        /* access modifiers changed from: private */
        public String phone;
        /* access modifiers changed from: private */
        public String pid;
        /* access modifiers changed from: private */
        public String referencedId;
        /* access modifiers changed from: private */
        public boolean silentFetchData;
        /* access modifiers changed from: private */
        public String userId;

        public InitPaymentBuilder(String pid2) {
            this.pid = pid2;
        }

        public InitPaymentBuilder country(String country2) {
            this.country = country2;
            return this;
        }

        public InitPaymentBuilder userId(String userId2) {
            this.userId = userId2;
            return this;
        }

        public InitPaymentBuilder referencedId(String referencedId2) {
            this.referencedId = referencedId2;
            return this;
        }

        public InitPaymentBuilder mcc(int mcc2) {
            this.mcc = mcc2;
            return this;
        }

        public InitPaymentBuilder mnc(int mnc2) {
            this.mnc = mnc2;
            return this;
        }

        public InitPaymentBuilder phone(String phone2) {
            this.phone = phone2;
            return this;
        }

        public InitPaymentBuilder lang(String lang2) {
            this.lang = lang2;
            return this;
        }

        public InitPaymentBuilder notifyUrl(String notifyUrl2) {
            this.notifyUrl = notifyUrl2;
            return this;
        }

        public InitPaymentBuilder imageUrl(String imageUrl2) {
            this.imageUrl = imageUrl2;
            return this;
        }

        public InitPaymentBuilder displayWebViewDialog(boolean enabled) {
            this.displayWebViewDialog = enabled;
            return this;
        }

        public InitPaymentBuilder silentFetchData(boolean enabled) {
            this.silentFetchData = enabled;
            return this;
        }

        public InitPayment build() {
            return new InitPayment(this);
        }
    }
}
