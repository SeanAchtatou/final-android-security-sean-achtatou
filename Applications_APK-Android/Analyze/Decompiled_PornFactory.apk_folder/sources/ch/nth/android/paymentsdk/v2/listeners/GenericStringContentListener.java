package ch.nth.android.paymentsdk.v2.listeners;

public interface GenericStringContentListener {
    void onContentAvailable(int i, String str);

    void onContentNotAvailable(int i);
}
