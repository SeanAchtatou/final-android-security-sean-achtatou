package ch.nth.android.paymentsdk.v2.model.helper;

import java.io.Serializable;

public class InitPaymentParams implements Serializable {
    private static final long serialVersionUID = 4715020071435387536L;
    private String callbackUrl;
    private String redirectUrl;
    private String requestId;

    public InitPaymentParams(String redirectUrl2, String requestId2, String callbackUrl2) {
        this.redirectUrl = redirectUrl2;
        this.callbackUrl = callbackUrl2;
        this.requestId = requestId2;
    }

    public String getRedirectUrl() {
        return this.redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl2) {
        this.redirectUrl = redirectUrl2;
    }

    public String getCallbackUrl() {
        return this.callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl2) {
        this.callbackUrl = callbackUrl2;
    }

    public String getRequestId() {
        return this.requestId;
    }

    public void setRequestId(String requestId2) {
        this.requestId = requestId2;
    }
}
