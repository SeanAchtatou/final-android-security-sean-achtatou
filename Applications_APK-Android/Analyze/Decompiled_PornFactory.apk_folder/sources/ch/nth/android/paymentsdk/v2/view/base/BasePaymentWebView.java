package ch.nth.android.paymentsdk.v2.view.base;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.webkit.WebView;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.helper.AsyncResponse;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import ch.nth.android.paymentsdk.v2.payment.PaymentWebViewClient;
import ch.nth.android.paymentsdk.v2.utils.PaymentUtils;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import java.io.IOException;
import java.net.UnknownHostException;

public abstract class BasePaymentWebView extends WebView {
    private AsyncResponse mAsyncResponse;
    /* access modifiers changed from: private */
    public InitPaymentParams mInitPaymentParams;
    private String mUrl = "";

    public abstract void onCheckWebUrl(InitPaymentParams initPaymentParams);

    public abstract void onDataReceived(InfoResponse infoResponse);

    public abstract void onDataReceived(String str);

    public BasePaymentWebView(Context context) {
        super(context);
    }

    public BasePaymentWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BasePaymentWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setConfigData(String url, String callback, String requestId, String baseUrl, String apiKey, String apiSecret, boolean verifyEveryUrl) {
        this.mUrl = url;
        super.setWebViewClient(new CustomPaymentWebViewClient(callback, baseUrl, requestId, apiKey, apiSecret, verifyEveryUrl));
        PaymentUtils.initWebViewAttributes(this);
    }

    public void loadUrl() {
        if (this.mAsyncResponse != null) {
            super.loadData(this.mAsyncResponse.getHtmlContent(), "text/html", "utf-8");
        } else {
            super.loadUrl(this.mUrl);
        }
    }

    private class CustomPaymentWebViewClient extends PaymentWebViewClient {
        public CustomPaymentWebViewClient(String callback, String baseUrl, String requestId, String apiKey, String apiSecret, boolean verifyEveryUrl) {
            super(callback, baseUrl, requestId, apiKey, apiSecret, verifyEveryUrl);
        }

        public void onIOException(IOException exception) {
            if ((exception instanceof UnknownHostException) && BasePaymentWebView.this.getContext() != null) {
                Utils.doToast(BasePaymentWebView.this.getContext(), "Unable to resolve host");
            }
        }

        public void onDataReceived(InfoResponse infoReponse) {
            BasePaymentWebView.this.onDataReceived(infoReponse);
        }

        public void onReturnResult(String location) {
            BasePaymentWebView.this.onDataReceived(location);
        }

        public void onWebViewReceivedError(int errorCode, String description, String failingUrl) {
            StringBuilder append = new StringBuilder("onWebViewReceivedError: ").append(errorCode).append(" ");
            if (TextUtils.isEmpty(description)) {
                description = "";
            }
            StringBuilder append2 = append.append(description).append(" ");
            if (TextUtils.isEmpty(failingUrl)) {
                failingUrl = "";
            }
            Utils.doLog(append2.append(failingUrl).toString());
            Utils.doToast(BasePaymentWebView.this.getContext(), "There is problem with loading web site!");
        }

        public void onReturnUrl(InitPaymentParams url) {
            BasePaymentWebView.this.onCheckWebUrl(url);
        }

        public InitPaymentParams getAllowedPage() {
            return BasePaymentWebView.this.mInitPaymentParams;
        }
    }

    public void loadAllowedWebPage(InitPaymentParams pageParams) {
        this.mInitPaymentParams = pageParams;
        loadUrl(pageParams.getRedirectUrl());
    }
}
