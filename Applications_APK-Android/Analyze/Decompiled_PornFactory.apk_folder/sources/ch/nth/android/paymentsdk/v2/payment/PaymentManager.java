package ch.nth.android.paymentsdk.v2.payment;

import android.content.Context;
import android.text.TextUtils;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import ch.nth.android.paymentsdk.v2.async.AsyncInfoRequest;
import ch.nth.android.paymentsdk.v2.async.CancelPaymentRequest;
import ch.nth.android.paymentsdk.v2.async.ClosePaymentRequest;
import ch.nth.android.paymentsdk.v2.async.InitPaymentRequest;
import ch.nth.android.paymentsdk.v2.async.VerifyPaymentRequest;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import ch.nth.android.paymentsdk.v2.exceptions.MandatoryFieldException;
import ch.nth.android.paymentsdk.v2.listeners.GenericPaymentResponseListener;
import ch.nth.android.paymentsdk.v2.listeners.GenericStringContentListener;
import ch.nth.android.paymentsdk.v2.listeners.PaymentResultStepListener;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.InitPayment;
import ch.nth.android.paymentsdk.v2.model.base.BasePaymentResult;
import ch.nth.android.paymentsdk.v2.utils.Utils;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public class PaymentManager {
    private String callbackUrl;
    private String mApiKey;
    private String mApiSecret;
    private String mBaseEndpoint;
    private Context mContext;
    private PaymentResultStepListener mListener;

    public PaymentManager(Context context) {
        this.mContext = context;
    }

    public PaymentManager(Context context, PaymentResultStepListener listener) {
        this(context);
        this.mListener = listener;
        this.callbackUrl = "http://localhost/" + System.currentTimeMillis();
    }

    public void initPayment(InitPayment initPayment) throws MandatoryFieldException {
        checkApiKeySecret();
        if (initPayment == null) {
            throw new MandatoryFieldException("InitPayment object must not be null");
        } else if (TextUtils.isEmpty(initPayment.getPid())) {
            throw new MandatoryFieldException("pid must not be null");
        } else {
            Map<String, String> params = new LinkedHashMap<>();
            params.put("callbackUrl", this.callbackUrl);
            params.put("pid", initPayment.getPid());
            if (initPayment.getMcc() > 0) {
                params.put("mcc", String.format(Locale.US, "%03d", Integer.valueOf(initPayment.getMcc())));
            }
            if (initPayment.getMnc() > 0) {
                params.put("mnc", String.format(Locale.US, "%02d", Integer.valueOf(initPayment.getMnc())));
            }
            if (!TextUtils.isEmpty(initPayment.getCountry())) {
                params.put("country", initPayment.getCountry());
            }
            if (!TextUtils.isEmpty(initPayment.getUserId())) {
                params.put("userId", initPayment.getUserId());
            }
            if (!TextUtils.isEmpty(initPayment.getReferencedId())) {
                params.put("referenceId", initPayment.getReferencedId());
            }
            if (!TextUtils.isEmpty(initPayment.getPhone())) {
                params.put("phone", initPayment.getPhone());
            }
            if (!TextUtils.isEmpty(initPayment.getLang())) {
                params.put(DmsConstants.LANG, initPayment.getLang());
            }
            if (!TextUtils.isEmpty(initPayment.getNotifyUrl())) {
                params.put("notifyUrl", initPayment.getNotifyUrl());
            }
            if (!TextUtils.isEmpty(initPayment.getImageUrl())) {
                params.put("imageUrl", initPayment.getImageUrl());
            }
            new InitPaymentRequest(this.mContext, this.mBaseEndpoint, this.mApiKey, this.mApiSecret, params, new GenericPaymentResponseListener() {
                public void onResponseReceived(int statusCode, BasePaymentResult status) {
                    PaymentManager.this.notifyListener(statusCode, status, PaymentManagerSteps.INIT_PAYMENT);
                }

                public void onError(int statusCode) {
                    PaymentManager.this.notifyListener(statusCode, null, PaymentManagerSteps.INIT_PAYMENT);
                }
            });
        }
    }

    public void verifyPayment(String sid) throws MandatoryFieldException {
        checkApiKeySecret();
        if (TextUtils.isEmpty(sid)) {
            throw new MandatoryFieldException("sid must not be null");
        }
        Map<String, String> params = new HashMap<>();
        params.put("sid", sid);
        new VerifyPaymentRequest(this.mContext, this.mBaseEndpoint, this.mApiKey, this.mApiSecret, params, new GenericPaymentResponseListener() {
            public void onResponseReceived(int statusCode, BasePaymentResult status) {
                PaymentManager.this.notifyListener(statusCode, status, PaymentManagerSteps.VERIFY_PAYMENT);
            }

            public void onError(int statusCode) {
                PaymentManager.this.notifyListener(statusCode, null, PaymentManagerSteps.VERIFY_PAYMENT);
            }
        });
    }

    public void closePayment(String sid) throws MandatoryFieldException {
        checkApiKeySecret();
        if (TextUtils.isEmpty(sid)) {
            throw new MandatoryFieldException("sid must not be null");
        }
        Map<String, String> params = new HashMap<>();
        params.put("sid", sid);
        new ClosePaymentRequest(this.mContext, this.mBaseEndpoint, this.mApiKey, this.mApiSecret, params, new GenericPaymentResponseListener() {
            public void onResponseReceived(int statusCode, BasePaymentResult status) {
                PaymentManager.this.notifyListener(statusCode, status, PaymentManagerSteps.CLOSE_PAYMENT);
            }

            public void onError(int statusCode) {
                PaymentManager.this.notifyListener(statusCode, null, PaymentManagerSteps.CLOSE_PAYMENT);
            }
        });
    }

    public void cancelPayment(String sid) throws MandatoryFieldException {
        checkApiKeySecret();
        if (TextUtils.isEmpty(sid)) {
            throw new MandatoryFieldException("sid must not be null");
        }
        Map<String, String> params = new HashMap<>();
        params.put("sid", sid);
        new CancelPaymentRequest(this.mContext, this.mBaseEndpoint, this.mApiKey, this.mApiSecret, params, new GenericPaymentResponseListener() {
            public void onResponseReceived(int statusCode, BasePaymentResult status) {
                PaymentManager.this.notifyListener(statusCode, status, PaymentManagerSteps.CANCEL_PAYMENT);
            }

            public void onError(int statusCode) {
                PaymentManager.this.notifyListener(statusCode, null, PaymentManagerSteps.CANCEL_PAYMENT);
            }
        });
    }

    public void getRequestInfoByRequestId(String requestId) throws MandatoryFieldException {
        checkApiKeySecret();
        if (TextUtils.isEmpty(requestId)) {
            throw new MandatoryFieldException("requestId must not be null");
        }
        Map<String, String> params = new HashMap<>();
        params.put("requestId", requestId);
        genericRequestInfo(params);
    }

    public void getRequestInfoBySessionId(String sessionId) throws MandatoryFieldException {
        checkApiKeySecret();
        if (TextUtils.isEmpty(sessionId)) {
            throw new MandatoryFieldException("sessionId must not be null");
        }
        Map<String, String> params = new HashMap<>();
        params.put("sid", sessionId);
        genericRequestInfo(params);
    }

    private void genericRequestInfo(Map<String, String> params) {
        new AsyncInfoRequest(this.mBaseEndpoint, params, this.mApiKey, this.mApiSecret, new GenericStringContentListener() {
            public void onContentNotAvailable(int statusCode) {
                PaymentManager.this.notifyListener(statusCode, null, PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO);
            }

            public void onContentAvailable(int statusCode, String htmlContent) {
                try {
                    PaymentManager.this.notifyListener(statusCode, (InfoResponse) new Gson().fromJson(htmlContent, InfoResponse.class), PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO);
                } catch (Exception e) {
                    Utils.doLogException(e);
                    PaymentManager.this.notifyListener(statusCode, null, PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO);
                }
            }
        }).execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void notifyListener(int statusCode, BasePaymentResult status, PaymentManagerSteps paymentStep) {
        if (this.mListener == null) {
            return;
        }
        if (Utils.checkSuccessfulResponse(statusCode)) {
            this.mListener.onPaymentReceived(statusCode, status, paymentStep);
        } else {
            this.mListener.onPaymentError(statusCode, paymentStep);
        }
    }

    private void checkApiKeySecret() throws MandatoryFieldException {
        if (TextUtils.isEmpty(this.mApiKey)) {
            throw new MandatoryFieldException("API Key must not be null");
        } else if (TextUtils.isEmpty(this.mApiSecret)) {
            throw new MandatoryFieldException("API Secret must not be null");
        }
    }

    public void setPaymentListener(PaymentResultStepListener listener) {
        this.mListener = listener;
    }

    public String getBaseEndpoint() {
        return this.mBaseEndpoint;
    }

    public void setBaseEndpoint(String baseEndpoint) {
        this.mBaseEndpoint = baseEndpoint;
    }

    public String getApiKey() {
        return this.mApiKey;
    }

    public void setApiKey(String apiKey) {
        this.mApiKey = apiKey;
    }

    public String getApiSecret() {
        return this.mApiSecret;
    }

    public void setApiSecret(String apiSecret) {
        this.mApiSecret = apiSecret;
    }

    public String getCallbackUrl() {
        return this.callbackUrl;
    }
}
