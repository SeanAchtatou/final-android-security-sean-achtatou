package ch.nth.android.paymentsdk.v2.model;

public class SubscriptionPaymentSession extends GenericPaymentSession {
    private int expiresIn;
    private boolean recurrent;
    private String timeClosed;

    public boolean isRecurrent() {
        return this.recurrent;
    }

    public void setRecurrent(boolean recurrent2) {
        this.recurrent = recurrent2;
    }

    public int getExpiresIn() {
        return this.expiresIn;
    }

    public void setExpiresIn(int expiresIn2) {
        this.expiresIn = expiresIn2;
    }

    public String getTimeClosed() {
        return this.timeClosed;
    }

    public void setTimeClosed(String timeClosed2) {
        this.timeClosed = timeClosed2;
    }

    public String toString() {
        return "SubscriptionPaymentSession ( " + super.toString() + "\n" + "recurrent = " + this.recurrent + "\n" + "expiresIn = " + this.expiresIn + "\n" + "timeClosed = " + this.timeClosed + "\n" + " )";
    }
}
