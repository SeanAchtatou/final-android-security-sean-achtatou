package ch.nth.android.polyglot.translator.merger;

import ch.nth.android.polyglot.translator.TranslationEntry;

public class PermissiveEntryValidator implements EntryValidator {
    public boolean validate(TranslationEntry entry) {
        return TranslationEntry.isValid(entry);
    }
}
