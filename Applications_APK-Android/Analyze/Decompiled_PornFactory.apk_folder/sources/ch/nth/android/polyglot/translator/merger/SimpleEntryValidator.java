package ch.nth.android.polyglot.translator.merger;

import ch.nth.android.polyglot.translator.TranslationEntry;

public class SimpleEntryValidator implements EntryValidator {
    public boolean validate(TranslationEntry entry) {
        if (!TranslationEntry.isValid(entry)) {
            return false;
        }
        return entry.containsEmptyTranslations();
    }
}
