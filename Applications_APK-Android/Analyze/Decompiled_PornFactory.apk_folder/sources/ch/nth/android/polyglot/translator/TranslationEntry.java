package ch.nth.android.polyglot.translator;

import android.text.TextUtils;
import ch.nth.android.lang.Language;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TranslationEntry {
    public static final transient TranslationEntry STUB = new TranslationEntry();
    private String cachedFallback = "";
    private String key = "";
    private Map<String, String> values = new HashMap();

    public String getKey() {
        return this.key;
    }

    /* access modifiers changed from: package-private */
    public void setKey(String key2) {
        this.key = key2;
    }

    public Map<String, String> getValues() {
        return this.values;
    }

    public String getTranslation(Language language) {
        return getTranslation(language.getTwoLetterCode());
    }

    public String getTranslation(String language) {
        String value = this.values.get(language);
        return value != null ? value : "";
    }

    public String getAnyTranslation() {
        for (String translation : this.values.values()) {
            if (!TextUtils.isEmpty(translation)) {
                return translation;
            }
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public String getCachedTranslation() {
        return this.cachedFallback != null ? this.cachedFallback : "";
    }

    /* access modifiers changed from: package-private */
    public void setCachedTranslation(String value) {
        if (!TextUtils.isEmpty(value)) {
            this.cachedFallback = value;
        }
    }

    public static boolean isValid(TranslationEntry entry) {
        return (entry == null || STUB == entry) ? false : true;
    }

    public boolean containsEmptyTranslations() {
        for (String translation : this.values.values()) {
            if (!TextUtils.isEmpty(translation)) {
                return true;
            }
        }
        return false;
    }

    public Set<String> getLanguages(boolean strict) {
        if (!strict) {
            return this.values.keySet();
        }
        Set<String> languages = new HashSet<>();
        for (Map.Entry<String, String> translation : this.values.entrySet()) {
            if (!TextUtils.isEmpty((CharSequence) translation.getValue())) {
                languages.add(translation.getKey());
            }
        }
        return languages;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TranslationEntry that = (TranslationEntry) o;
        if (this.key != null) {
            if (this.key.equals(that.key)) {
                return true;
            }
        } else if (that.key == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.key != null) {
            return this.key.hashCode();
        }
        return 0;
    }
}
