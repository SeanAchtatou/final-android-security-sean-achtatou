package ch.nth.android.polyglot.translator.merger;

import android.util.Log;
import ch.nth.android.polyglot.translator.TranslationModule;
import ch.nth.android.polyglot.translator.TranslationModuleCollection;
import java.util.List;
import java.util.Set;

public class FullCollectionMerger implements CollectionMerger {
    private static final String TAG = FullCollectionMerger.class.getSimpleName();

    public TranslationModuleCollection merge(TranslationModuleCollection primaryCollection, TranslationModuleCollection fallbackCollection, List<ModuleMerger> mergers, List<TargetedModuleMerger> targetedMergers) {
        TranslationModuleCollection result = new InnerCollectionMerger().merge(primaryCollection, fallbackCollection, mergers, targetedMergers);
        Set<TranslationModule> xOr = TranslationModuleCollection.xOr(primaryCollection, fallbackCollection);
        result.addAllModules(xOr);
        Log.d(TAG, "adding-xor-modules: " + xOr);
        return result;
    }
}
