package ch.nth.android.polyglot.translator.fetcher;

import ch.nth.android.fetcher.TaskOptions;

public class ModuleCollectionTaskOptions {
    private TaskOptions mFallbackTaskOptions;
    private TaskOptions mPrimaryTaskOptions;

    public ModuleCollectionTaskOptions(TaskOptions primaryTaskOptions) {
        this(primaryTaskOptions, null);
    }

    public ModuleCollectionTaskOptions(TaskOptions primaryTaskOptions, TaskOptions fallbackTaskOptions) {
        this.mPrimaryTaskOptions = primaryTaskOptions;
        this.mFallbackTaskOptions = fallbackTaskOptions;
    }

    public TaskOptions getPrimaryTaskOptions() {
        return this.mPrimaryTaskOptions;
    }

    public TaskOptions getFallbackTaskOptions() {
        return this.mFallbackTaskOptions;
    }
}
