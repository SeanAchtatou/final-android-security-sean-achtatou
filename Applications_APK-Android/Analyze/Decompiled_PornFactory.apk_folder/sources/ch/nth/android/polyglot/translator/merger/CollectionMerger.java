package ch.nth.android.polyglot.translator.merger;

import ch.nth.android.polyglot.translator.TranslationModuleCollection;
import java.util.List;

public interface CollectionMerger {
    TranslationModuleCollection merge(TranslationModuleCollection translationModuleCollection, TranslationModuleCollection translationModuleCollection2, List<ModuleMerger> list, List<TargetedModuleMerger> list2);
}
