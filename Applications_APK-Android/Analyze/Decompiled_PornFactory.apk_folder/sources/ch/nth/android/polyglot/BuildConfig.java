package ch.nth.android.polyglot;

public final class BuildConfig {
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final String PACKAGE_NAME = "ch.nth.android.polyglot";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.3.1";
}
