package ch.nth.android.polyglot.translator.merger;

import android.util.Log;
import ch.nth.android.polyglot.translator.TranslationModule;
import ch.nth.android.polyglot.translator.TranslationModuleCollection;
import java.util.List;
import java.util.Set;

public class LeftCollectionMerger implements CollectionMerger {
    private static final String TAG = LeftCollectionMerger.class.getSimpleName();

    public TranslationModuleCollection merge(TranslationModuleCollection primaryCollection, TranslationModuleCollection fallbackCollection, List<ModuleMerger> mergers, List<TargetedModuleMerger> targetedMergers) {
        TranslationModuleCollection result = new InnerCollectionMerger().merge(primaryCollection, fallbackCollection, mergers, targetedMergers);
        Set<TranslationModule> onlyLeft = TranslationModuleCollection.xOrLeft(primaryCollection, fallbackCollection);
        result.addAllModules(onlyLeft);
        Log.d(TAG, "adding-left-xor-modules: " + onlyLeft);
        return result;
    }
}
