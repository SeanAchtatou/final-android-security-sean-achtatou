package ch.nth.android.polyglot.translator.merger;

import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.polyglot.translator.TranslationEntry;
import ch.nth.android.polyglot.translator.TranslationModule;
import java.lang.reflect.Field;
import java.util.Set;
import java.util.concurrent.Callable;

public class ModuleChecker implements Callable<TranslationModule> {
    private static final String TAG = ModuleChecker.class.getSimpleName();
    private Set<String> mLanguages;
    private Class mStringsClass;
    private TranslationModule mTranslations;

    private ModuleChecker(Builder builder) {
        this.mTranslations = builder.mTranslations;
        this.mStringsClass = builder.mStringsClass;
        this.mLanguages = builder.mLanguages;
    }

    public TranslationModule call() throws Exception {
        for (Field field : this.mStringsClass.getDeclaredFields()) {
            if (String.class.isAssignableFrom(field.getType())) {
                String fieldName = field.getName();
                TranslationEntry entry = this.mTranslations.getEntry(fieldName);
                if (entry == null || TranslationEntry.STUB.equals(entry)) {
                    Log.w(TAG, "translation entry not found: " + fieldName);
                } else if (this.mLanguages != null) {
                    for (String language : this.mLanguages) {
                        if (TextUtils.isEmpty(entry.getTranslation(language))) {
                            Log.w(TAG, "translation not found for language: " + language + " in entry " + entry.getKey());
                        }
                    }
                }
            }
        }
        return this.mTranslations;
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public Set<String> mLanguages;
        /* access modifiers changed from: private */
        public Class mStringsClass;
        /* access modifiers changed from: private */
        public TranslationModule mTranslations;

        public Builder(TranslationModule translations, Class stringsClass) {
            this.mTranslations = translations;
            this.mStringsClass = stringsClass;
        }

        public Builder withLanguages(Set<String> languages) {
            this.mLanguages = languages;
            return this;
        }
    }
}
