package ch.nth.android.polyglot.translator;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import ch.nth.android.utils.JavaUtils;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;

public class TranslationModuleCollection {
    private static final transient String TAG = TranslationModuleCollection.class.getSimpleName();
    private Set<TranslationModule> data = new HashSet(4);

    public TranslationModuleCollection() {
    }

    private TranslationModuleCollection(Set<TranslationModule> data2) {
        this.data = data2;
    }

    @NonNull
    public static TranslationModuleCollection fromModules(Collection<TranslationModule> modules) {
        if (modules == null || modules.isEmpty()) {
            return new TranslationModuleCollection();
        }
        Set<TranslationModule> collectionModules = new HashSet<>(modules.size());
        collectionModules.addAll(modules);
        return new TranslationModuleCollection(collectionModules);
    }

    public Set<TranslationModule> getData() {
        return this.data;
    }

    @NonNull
    public TranslationModule getTranslationModule(String moduleName) {
        return getTranslationModule(moduleName, false);
    }

    @NonNull
    public TranslationModule getTranslationModule(String moduleName, boolean regexFallback) {
        if (TextUtils.isEmpty(moduleName)) {
            return TranslationModule.STUB;
        }
        for (TranslationModule module : this.data) {
            if (TranslationModule.isValid(module) && module.getModuleName().equals(moduleName)) {
                return module;
            }
        }
        if (regexFallback) {
            for (TranslationModule module2 : this.data) {
                if (TranslationModule.isValid(module2) && JavaUtils.regexMatch(module2.getModuleName(), moduleName)) {
                    return module2;
                }
            }
        }
        return TranslationModule.STUB;
    }

    @NonNull
    public TranslationModule getTranslationModule(Pattern pattern) {
        if (pattern == null) {
            return TranslationModule.STUB;
        }
        for (TranslationModule translations : this.data) {
            if (translations.moduleNameMatches(pattern)) {
                return translations;
            }
        }
        return TranslationModule.STUB;
    }

    public void addAllModules(TranslationModuleCollection from) {
        if (from != null) {
            addAllModules(from.getData());
        }
    }

    public void addAllModules(Collection<TranslationModule> modules) {
        if (modules != null) {
            this.data.addAll(modules);
        }
    }

    public void replaceModule(TranslationModule module) {
        if (module != null && !TextUtils.isEmpty(module.getModuleName())) {
            String moduleName = module.getModuleName();
            Iterator<TranslationModule> iterator = this.data.iterator();
            while (iterator.hasNext()) {
                if (moduleName.equals(iterator.next().getModuleName())) {
                    iterator.remove();
                }
            }
            this.data.add(module);
        }
    }

    @NonNull
    public static Set<TranslationModule> xOrLeft(TranslationModuleCollection left, TranslationModuleCollection right) {
        return xOrHelper(left, right, true, false);
    }

    @NonNull
    public static Set<TranslationModule> xOrRight(TranslationModuleCollection left, TranslationModuleCollection right) {
        return xOrHelper(left, right, false, true);
    }

    @NonNull
    public static Set<TranslationModule> xOr(TranslationModuleCollection left, TranslationModuleCollection right) {
        return xOrHelper(left, right, true, true);
    }

    @NonNull
    private static Set<TranslationModule> xOrHelper(TranslationModuleCollection left, TranslationModuleCollection right, boolean includeLeft, boolean includeRight) {
        Set<TranslationModule> modules = new HashSet<>();
        if (left != null && right != null) {
            Set<TranslationModule> union = new HashSet<>();
            union.addAll(left.data);
            union.addAll(right.data);
            for (TranslationModule module : union) {
                if (!includeLeft || !includeRight) {
                    if (includeLeft) {
                        if (left.data.contains(module) && !right.data.contains(module)) {
                            modules.add(module);
                        }
                    } else if (includeRight && !left.data.contains(module) && right.data.contains(module)) {
                        modules.add(module);
                    }
                } else if ((left.data.contains(module) && !right.data.contains(module)) || (!left.data.contains(module) && right.data.contains(module))) {
                    modules.add(module);
                }
            }
            return modules;
        } else if (left != null) {
            return left.data;
        } else {
            if (right != null) {
                return right.data;
            }
            return modules;
        }
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("TranslationModuleCollection { ");
        Iterator<TranslationModule> iterator = this.data.iterator();
        while (iterator.hasNext()) {
            TranslationModule translations = iterator.next();
            if (translations != null) {
                builder.append(translations.toString());
                if (iterator.hasNext()) {
                    builder.append(", ");
                }
            }
        }
        builder.append(" }");
        return builder.toString();
    }
}
