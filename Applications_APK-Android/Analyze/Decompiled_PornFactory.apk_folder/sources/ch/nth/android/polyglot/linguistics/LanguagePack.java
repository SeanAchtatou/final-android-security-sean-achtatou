package ch.nth.android.polyglot.linguistics;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

public class LanguagePack {
    private String mAlpha2CountryCode = "";
    private String mDefaultLanguageName = "";
    private int mId;
    private boolean mLanguageEnabled = true;
    private Map<String, String> mLanguageNames = new HashMap();
    private String mLocalFlagFileName = "";
    private String mRemoteFlagUrl = "";

    LanguagePack() {
    }

    public LanguagePack(String alpha2CountryCode, String defaultLanguageName, String localFlagFileName, String remoteFlagUrl, boolean languageEnabled) {
        setAlpha2CountryCode(alpha2CountryCode);
        this.mDefaultLanguageName = defaultLanguageName;
        this.mLocalFlagFileName = localFlagFileName;
        this.mRemoteFlagUrl = remoteFlagUrl;
        this.mLanguageEnabled = languageEnabled;
    }

    public String getAlpha2CountryCode() {
        return this.mAlpha2CountryCode;
    }

    /* access modifiers changed from: package-private */
    public void setAlpha2CountryCode(String alpha2CountryCode) {
        this.mAlpha2CountryCode = alpha2CountryCode;
    }

    public String getDefaultLanguageName() {
        return this.mDefaultLanguageName;
    }

    public void setDefaultLanguageName(String defaultLanguageName) {
        this.mDefaultLanguageName = defaultLanguageName;
    }

    public String getLocalFlagFileName() {
        return this.mLocalFlagFileName;
    }

    /* access modifiers changed from: package-private */
    public void setLocalFlagFileName(String localFlagFileName) {
        this.mLocalFlagFileName = localFlagFileName;
    }

    public String getRemoteFlagUrl() {
        return this.mRemoteFlagUrl;
    }

    /* access modifiers changed from: package-private */
    public void setRemoteFlagUrl(String remoteFlagUrl) {
        this.mRemoteFlagUrl = remoteFlagUrl;
    }

    public boolean isLanguageEnabled() {
        return this.mLanguageEnabled;
    }

    /* access modifiers changed from: package-private */
    public void setLanguageEnabled(boolean languageEnabled) {
        this.mLanguageEnabled = languageEnabled;
    }

    public int getId() {
        return this.mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public void putLanguageName(String language, String name) {
        if (!TextUtils.isEmpty(language) && !TextUtils.isEmpty(name)) {
            this.mLanguageNames.put(language, name);
        }
    }

    public String getLanguageName(String language) {
        String languageName = this.mLanguageNames.get(language);
        return languageName == null ? "" : languageName;
    }

    public String getNativeLanguageName() {
        return getLanguageName(this.mAlpha2CountryCode);
    }

    public static boolean isValid(LanguagePack entry) {
        return entry != null;
    }

    public String toString() {
        return "LanguagePack { mAlpha2CountryCode=" + this.mAlpha2CountryCode + ", mId: " + this.mId + ", mDefaultLanguageName: " + this.mDefaultLanguageName + ", mLocalFlagFileName: " + this.mLocalFlagFileName + ", mRemoteFlagUrl: " + this.mRemoteFlagUrl + ", mLanguageEnabled: " + this.mLanguageEnabled + ", mLanguageNames: " + this.mLanguageNames + " }";
    }
}
