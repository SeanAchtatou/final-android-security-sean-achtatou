package ch.nth.android.polyglot.translator.merger;

import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.polyglot.translator.TranslationEntry;
import java.util.Map;

public class SimpleEntryMerger implements EntryMerger {
    private static final String TAG = SimpleEntryMerger.class.getSimpleName();

    public TranslationEntry merge(TranslationEntry primary, TranslationEntry fallback) {
        if (primary == null) {
            if (fallback == null) {
                return TranslationEntry.STUB;
            }
            return fallback;
        } else if (fallback == null) {
            return primary;
        } else {
            for (Map.Entry<String, String> languageEntry : primary.getValues().entrySet()) {
                if (TextUtils.isEmpty((CharSequence) languageEntry.getValue())) {
                    String fallbackTranslation = fallback.getTranslation((String) languageEntry.getKey());
                    if (!TextUtils.isEmpty(fallbackTranslation)) {
                        languageEntry.setValue(fallbackTranslation);
                        Log.d(TAG, "setting fallback translation for language \"" + ((String) languageEntry.getKey()) + "\" with value \"" + fallbackTranslation + "\", [entry=" + primary.getKey() + "]");
                    } else {
                        Log.d(TAG, "can't find fallback translation for language \"" + ((String) languageEntry.getKey()) + "\", [entry=" + primary.getKey() + "]");
                    }
                }
            }
            return primary;
        }
    }
}
