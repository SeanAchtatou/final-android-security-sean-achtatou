package ch.nth.android.polyglot.translator.merger;

import ch.nth.android.polyglot.translator.TranslationEntry;

public interface EntryValidator {
    boolean validate(TranslationEntry translationEntry);
}
