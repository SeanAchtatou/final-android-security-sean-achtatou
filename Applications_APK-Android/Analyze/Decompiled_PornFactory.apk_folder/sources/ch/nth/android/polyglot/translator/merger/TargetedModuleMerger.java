package ch.nth.android.polyglot.translator.merger;

public interface TargetedModuleMerger extends ModuleMerger {
    boolean isValidTarget(String str);
}
