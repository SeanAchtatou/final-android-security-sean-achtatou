package ch.nth.android.polyglot.translator.merger;

import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.polyglot.translator.TranslationEntry;
import ch.nth.android.polyglot.translator.TranslationModule;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SimpleModuleMerger implements ModuleMerger {
    private static final String TAG = SimpleModuleMerger.class.getSimpleName();
    private boolean mCheckAllLanguages;
    private Set<String> mLanguages;
    private EntryValidator mMissingEntryValidator;
    private EntryMerger mMissingLanguageMerger;

    private SimpleModuleMerger(Builder builder) {
        this.mMissingEntryValidator = builder.mMissingEntryValidator;
        this.mMissingLanguageMerger = builder.mMissingLanguageMerger;
        this.mLanguages = builder.mLanguages;
        this.mCheckAllLanguages = builder.mCheckAllLanguages;
    }

    public TranslationModule merge(TranslationModule primaryTranslations, TranslationModule fallbackTranslations) {
        String str;
        if (primaryTranslations == null) {
            if (!TranslationModule.isValid(fallbackTranslations)) {
                Log.i(TAG, "primary translations are NULL, fallback translations are also NULL (or stub)! returning stub...");
                return TranslationModule.STUB;
            }
            Log.i(TAG, "primary translations are NULL, returning fallback translations [module=" + fallbackTranslations.getModuleName() + "]");
            return fallbackTranslations;
        } else if (!TranslationModule.isValid(fallbackTranslations)) {
            Log.i(TAG, "primary translations are valid, fallback translations are NULL (or stub)! returning primary translations... [module=" + primaryTranslations.getModuleName() + "]");
            return primaryTranslations;
        } else {
            String moduleName = primaryTranslations.getModuleName();
            if (this.mCheckAllLanguages) {
                if (this.mLanguages == null) {
                    this.mLanguages = new HashSet();
                }
                this.mLanguages.addAll(primaryTranslations.getAllLanguages(false));
            }
            Set<String> entryNames = primaryTranslations.getEntryNames();
            String str2 = TAG;
            StringBuilder append = new StringBuilder().append("MERGING STARTED... [module=").append(moduleName).append("]");
            if (this.mLanguages == null) {
                str = "; no language check";
            } else {
                str = "; [languages=" + this.mLanguages + "]";
            }
            Log.d(str2, append.append(str).append(" [entryNames=").append(entryNames).append("]").toString());
            for (String entryName : entryNames) {
                TranslationEntry entry = primaryTranslations.getEntry(entryName);
                if (!TranslationEntry.isValid(entry)) {
                    if (this.mMissingEntryValidator != null) {
                        TranslationEntry fallbackEntry = fallbackTranslations.getEntry(entryName);
                        if (this.mMissingEntryValidator.validate(fallbackEntry)) {
                            primaryTranslations.putEntry(fallbackEntry);
                            Log.d(TAG, "translation entry missing, setting fallback entry... [entry=" + entryName + "]");
                        } else {
                            Log.w(TAG, "translation entry missing, fallback entry not found (or is not acceptable according to validator) - skipping... [entry=" + entryName + "]");
                        }
                    } else {
                        Log.w(TAG, "translation entry missing, but no entry validator was set, skipping... [entry=" + entryName + "]");
                    }
                } else if (this.mLanguages != null) {
                    boolean missingLanguageFound = false;
                    for (String language : this.mLanguages) {
                        if (TextUtils.isEmpty(entry.getTranslation(language))) {
                            missingLanguageFound = true;
                        }
                    }
                    if (missingLanguageFound) {
                        if (this.mMissingLanguageMerger != null) {
                            TranslationEntry mergedEntry = this.mMissingLanguageMerger.merge(entry, fallbackTranslations.getEntry(entryName));
                            if (TranslationEntry.isValid(mergedEntry)) {
                                primaryTranslations.putEntry(mergedEntry);
                                Log.d(TAG, "missing language(s) found, setting merged entry [entry=" + entryName + "]");
                            } else {
                                Log.w(TAG, "missing language(s) found but merged entry is not acceptable [entry=" + entryName + "]");
                            }
                        } else {
                            Log.w(TAG, "missing language(s) found but no language merger was set, skipping... [entry=" + entryName + "]");
                        }
                    }
                }
            }
            Log.d(TAG, "MERGING FINISHED... [module=" + moduleName + "]");
            return primaryTranslations;
        }
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public boolean mCheckAllLanguages;
        /* access modifiers changed from: private */
        public Set<String> mLanguages = new HashSet();
        /* access modifiers changed from: private */
        public EntryValidator mMissingEntryValidator;
        /* access modifiers changed from: private */
        public EntryMerger mMissingLanguageMerger;

        public Builder addLanguageToCheck(String language) {
            this.mLanguages.add(language);
            return this;
        }

        public Builder addLanguagesToCheck(Collection<String> languages) {
            this.mLanguages.addAll(languages);
            return this;
        }

        public Builder withCheckAllLanguages(boolean checkAllLanguages) {
            this.mCheckAllLanguages = checkAllLanguages;
            return this;
        }

        public Builder withMissingEntryValidator(EntryValidator validator) {
            this.mMissingEntryValidator = validator;
            return this;
        }

        public Builder withMissingLanguageMerger(EntryMerger merger) {
            this.mMissingLanguageMerger = merger;
            return this;
        }

        public SimpleModuleMerger build() {
            return new SimpleModuleMerger(this);
        }
    }
}
