package ch.nth.android.polyglot.translator.merger;

import ch.nth.android.polyglot.translator.TranslationModuleCollection;
import java.util.List;

public class RightMerger implements CollectionMerger {
    public TranslationModuleCollection merge(TranslationModuleCollection primaryCollection, TranslationModuleCollection fallbackCollection, List<ModuleMerger> mergers, List<TargetedModuleMerger> targetedMergers) {
        return new LeftCollectionMerger().merge(fallbackCollection, primaryCollection, mergers, targetedMergers);
    }
}
