package ch.nth.android.polyglot.translator.merger;

import ch.nth.android.polyglot.translator.TranslationModule;

public interface ModuleMerger {
    TranslationModule merge(TranslationModule translationModule, TranslationModule translationModule2);
}
