package ch.nth.android.polyglot.translator.merger;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import ch.nth.android.polyglot.translator.TranslationModule;
import ch.nth.android.polyglot.translator.TranslationModuleCollection;
import java.util.List;
import java.util.Set;

public class RightCollectionMerger implements CollectionMerger {
    private static final String TAG = RightCollectionMerger.class.getSimpleName();

    public TranslationModuleCollection merge(@NonNull TranslationModuleCollection primaryCollection, @Nullable TranslationModuleCollection fallbackCollection, @NonNull List<ModuleMerger> mergers, @NonNull List<TargetedModuleMerger> targetedMergers) {
        TranslationModuleCollection result = new InnerCollectionMerger().merge(primaryCollection, fallbackCollection, mergers, targetedMergers);
        Set<TranslationModule> onlyRight = TranslationModuleCollection.xOrRight(primaryCollection, fallbackCollection);
        result.addAllModules(onlyRight);
        Log.d(TAG, "adding-right-xor-modules: " + onlyRight);
        return result;
    }
}
