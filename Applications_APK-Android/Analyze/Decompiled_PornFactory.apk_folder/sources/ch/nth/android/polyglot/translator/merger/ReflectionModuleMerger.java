package ch.nth.android.polyglot.translator.merger;

import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.polyglot.translator.TranslationEntry;
import ch.nth.android.polyglot.translator.TranslationModule;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ReflectionModuleMerger implements TargetedModuleMerger {
    private static final String TAG = ReflectionModuleMerger.class.getSimpleName();
    private boolean mCheckAllLanguages;
    private Class mClass;
    private Set<String> mLanguages;
    private EntryValidator mMissingEntryValidator;
    private EntryMerger mMissingLanguageMerger;
    private Set<String> mTargetModules;

    protected ReflectionModuleMerger(Builder builder) {
        this.mClass = builder.mClass;
        this.mTargetModules = builder.mTargetModules;
        this.mMissingEntryValidator = builder.mMissingEntryValidator;
        this.mMissingLanguageMerger = builder.mMissingLanguageMerger;
        this.mLanguages = builder.mLanguages;
        this.mCheckAllLanguages = builder.mCheckAllLanguages;
    }

    public TranslationModule merge(TranslationModule primaryTranslations, TranslationModule fallbackTranslations) {
        String str;
        if (primaryTranslations == null) {
            if (!TranslationModule.isValid(fallbackTranslations)) {
                Log.i(TAG, "primary translations are NULL, fallback translations are also NULL (or stub)! returning stub...");
                return TranslationModule.STUB;
            }
            Log.i(TAG, "primary translations are NULL, returning fallback translations [module=" + fallbackTranslations.getModuleName() + "]");
            return fallbackTranslations;
        } else if (!TranslationModule.isValid(fallbackTranslations)) {
            Log.i(TAG, "primary translations are valid, fallback translations are NULL (or stub)! returning primary translations... [module=" + primaryTranslations.getModuleName() + "]");
            return primaryTranslations;
        } else {
            String moduleName = primaryTranslations.getModuleName();
            if (!this.mTargetModules.contains(moduleName)) {
                Log.d(TAG, "module not in the target list for this merger! returning primary translations... [module=" + moduleName + "]");
                return primaryTranslations;
            }
            if (this.mCheckAllLanguages) {
                if (this.mLanguages == null) {
                    this.mLanguages = new HashSet();
                }
                this.mLanguages.addAll(primaryTranslations.getAllLanguages(false));
            }
            String str2 = TAG;
            StringBuilder append = new StringBuilder().append("MERGING STARTED... [module=").append(moduleName).append("]");
            if (this.mLanguages == null) {
                str = "; no language check";
            } else {
                str = "; [languages=" + this.mLanguages + "]";
            }
            Log.d(str2, append.append(str).toString());
            for (Field field : this.mClass.getDeclaredFields()) {
                if (String.class.isAssignableFrom(field.getType())) {
                    String fieldName = field.getName();
                    TranslationEntry entry = primaryTranslations.getEntry(fieldName);
                    if (!TranslationEntry.isValid(entry)) {
                        if (this.mMissingEntryValidator != null) {
                            TranslationEntry fallbackEntry = fallbackTranslations.getEntry(fieldName);
                            if (this.mMissingEntryValidator.validate(fallbackEntry)) {
                                primaryTranslations.putEntry(fallbackEntry);
                                Log.d(TAG, "translation entry missing, setting fallback entry... [entry=" + fieldName + "]");
                            } else {
                                Log.w(TAG, "translation entry missing, fallback entry not found (or is not acceptable according to validator) - skipping... [entry=" + fieldName + "]");
                            }
                        } else {
                            Log.w(TAG, "translation entry missing, but no entry validator was set, skipping... [entry=" + fieldName + "]");
                        }
                    } else if (this.mLanguages != null) {
                        boolean missingLanguageFound = false;
                        for (String language : this.mLanguages) {
                            if (TextUtils.isEmpty(entry.getTranslation(language))) {
                                missingLanguageFound = true;
                            }
                        }
                        if (missingLanguageFound) {
                            if (this.mMissingLanguageMerger != null) {
                                TranslationEntry mergedEntry = this.mMissingLanguageMerger.merge(entry, fallbackTranslations.getEntry(fieldName));
                                if (TranslationEntry.isValid(mergedEntry)) {
                                    primaryTranslations.putEntry(mergedEntry);
                                    Log.d(TAG, "missing language(s) found, setting merged entry [entry=" + fieldName + "]");
                                } else {
                                    Log.w(TAG, "missing language(s) found but merged entry is not acceptable [entry=" + fieldName + "]");
                                }
                            } else {
                                Log.w(TAG, "missing language(s) found but no language merger was set, skipping... [entry=" + fieldName + "]");
                            }
                        }
                    }
                }
            }
            Log.d(TAG, "MERGING FINISHED... [module=" + moduleName + "]");
            return primaryTranslations;
        }
    }

    public boolean isValidTarget(String moduleName) {
        return this.mTargetModules.contains(moduleName);
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public boolean mCheckAllLanguages;
        /* access modifiers changed from: private */
        public Class mClass;
        /* access modifiers changed from: private */
        public Set<String> mLanguages = new HashSet();
        /* access modifiers changed from: private */
        public EntryValidator mMissingEntryValidator;
        /* access modifiers changed from: private */
        public EntryMerger mMissingLanguageMerger;
        /* access modifiers changed from: private */
        public Set<String> mTargetModules = new HashSet();

        public Builder(String targetModule, Class stringsClass) {
            addAcceptableMergeModule(targetModule);
            this.mClass = stringsClass;
        }

        public Builder addAcceptableMergeModule(String module) {
            if (!TextUtils.isEmpty(module)) {
                this.mTargetModules.add(module);
            }
            return this;
        }

        public Builder addLanguageToCheck(String language) {
            this.mLanguages.add(language);
            return this;
        }

        public Builder addLanguagesToCheck(Collection<String> languages) {
            this.mLanguages.addAll(languages);
            return this;
        }

        public Builder withCheckAllLanguages(boolean checkAllLanguages) {
            this.mCheckAllLanguages = checkAllLanguages;
            return this;
        }

        public Builder withMissingEntryValidator(EntryValidator validator) {
            this.mMissingEntryValidator = validator;
            return this;
        }

        public Builder withMissingLanguageMerger(EntryMerger merger) {
            this.mMissingLanguageMerger = merger;
            return this;
        }

        public ReflectionModuleMerger build() {
            return new ReflectionModuleMerger(this);
        }
    }
}
