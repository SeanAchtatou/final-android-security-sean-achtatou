package ch.nth.android.polyglot.translator;

public final class Modules {
    public static final String MODULE_APP = "app";
    public static final String MODULE_EPG = "module_epg";
    public static final String MODULE_LANGS = "langs";
    public static final String MODULE_LIVESTREAM = "module_livestream";
    public static final String MODULE_NEWS = "module_news";
    public static final String MODULE_REPORTER = "module_reporter";
    public static final String MODULE_TELETEXT = "module_teletext";
    public static final String MODULE_VIDEO_ON_DEMAND = "module_videoondemand";
    public static final String MODULE_WEATHER = "module_weather";
}
