package ch.nth.android.polyglot.translator;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.utils.JavaUtils;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class TranslationStorage {
    private static final String TAG = TranslationStorage.class.getSimpleName();

    public static void serializeCollection(Context context, TranslationModuleCollection collection, String fileName) {
        if (TextUtils.isEmpty(fileName)) {
            Log.d(TAG, "cache file name not specified... will not cache multi-lang module collection");
        } else {
            serialize(context, collection, TranslationModuleCollection.class, fileName);
        }
    }

    public static TranslationModuleCollection deserializeCollection(Context context, String moduleName) {
        if (TextUtils.isEmpty(moduleName)) {
            return null;
        }
        return (TranslationModuleCollection) deserialize(context, TranslationModuleCollection.class, moduleName);
    }

    public static void serializeModule(Context context, TranslationModule module) {
        if (TranslationModule.isValid(module)) {
            serialize(context, module, TranslationModule.class, module.getModuleName());
        }
    }

    public static TranslationModule deserializeModule(Context context, String moduleName) {
        if (TextUtils.isEmpty(moduleName)) {
            return null;
        }
        return (TranslationModule) deserialize(context, TranslationModule.class, moduleName);
    }

    public static <T> void serialize(Context context, T object, Class<T> tClass, String fileName) {
        JsonWriter jsonWriter;
        if (context == null || object == null || tClass == null || TextUtils.isEmpty(fileName)) {
            Log.d(TAG, "unable to serialize... object: " + ((Object) object) + ", class: " + tClass + ", fileName: " + fileName);
            return;
        }
        Writer fileWriter = null;
        Writer bufferedWriter = null;
        JsonWriter jsonWriter2 = null;
        try {
            Writer fileWriter2 = new FileWriter(new File(context.getFilesDir(), fileName));
            try {
                Writer bufferedWriter2 = new BufferedWriter(fileWriter2);
                try {
                    jsonWriter = new JsonWriter(fileWriter2);
                } catch (IOException e) {
                    e = e;
                    bufferedWriter = bufferedWriter2;
                    fileWriter = fileWriter2;
                    try {
                        Log.d(TAG, "Exception while writing to file", e);
                        JavaUtils.closeQuietly(jsonWriter2);
                        JavaUtils.closeQuietly(bufferedWriter);
                        JavaUtils.closeQuietly(fileWriter);
                    } catch (Throwable th) {
                        th = th;
                        JavaUtils.closeQuietly(jsonWriter2);
                        JavaUtils.closeQuietly(bufferedWriter);
                        JavaUtils.closeQuietly(fileWriter);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedWriter = bufferedWriter2;
                    fileWriter = fileWriter2;
                    JavaUtils.closeQuietly(jsonWriter2);
                    JavaUtils.closeQuietly(bufferedWriter);
                    JavaUtils.closeQuietly(fileWriter);
                    throw th;
                }
                try {
                    new Gson().toJson(object, tClass, jsonWriter);
                    jsonWriter.flush();
                    Log.d(TAG, "serialized " + ((Object) object) + " to " + fileName);
                    JavaUtils.closeQuietly(jsonWriter);
                    JavaUtils.closeQuietly(bufferedWriter2);
                    JavaUtils.closeQuietly(fileWriter2);
                } catch (IOException e2) {
                    e = e2;
                    jsonWriter2 = jsonWriter;
                    bufferedWriter = bufferedWriter2;
                    fileWriter = fileWriter2;
                    Log.d(TAG, "Exception while writing to file", e);
                    JavaUtils.closeQuietly(jsonWriter2);
                    JavaUtils.closeQuietly(bufferedWriter);
                    JavaUtils.closeQuietly(fileWriter);
                } catch (Throwable th3) {
                    th = th3;
                    jsonWriter2 = jsonWriter;
                    bufferedWriter = bufferedWriter2;
                    fileWriter = fileWriter2;
                    JavaUtils.closeQuietly(jsonWriter2);
                    JavaUtils.closeQuietly(bufferedWriter);
                    JavaUtils.closeQuietly(fileWriter);
                    throw th;
                }
            } catch (IOException e3) {
                e = e3;
                fileWriter = fileWriter2;
                Log.d(TAG, "Exception while writing to file", e);
                JavaUtils.closeQuietly(jsonWriter2);
                JavaUtils.closeQuietly(bufferedWriter);
                JavaUtils.closeQuietly(fileWriter);
            } catch (Throwable th4) {
                th = th4;
                fileWriter = fileWriter2;
                JavaUtils.closeQuietly(jsonWriter2);
                JavaUtils.closeQuietly(bufferedWriter);
                JavaUtils.closeQuietly(fileWriter);
                throw th;
            }
        } catch (IOException e4) {
            e = e4;
            Log.d(TAG, "Exception while writing to file", e);
            JavaUtils.closeQuietly(jsonWriter2);
            JavaUtils.closeQuietly(bufferedWriter);
            JavaUtils.closeQuietly(fileWriter);
        }
    }

    public static <T> T deserialize(Context context, Class<T> tClass, String fileName) {
        JsonReader jsonReader;
        if (context == null || tClass == null || TextUtils.isEmpty(fileName)) {
            Log.d(TAG, "unable to deserialize... class: " + tClass + ", fileName: " + fileName);
            return null;
        }
        Reader fileReader = null;
        Reader bufferedReader = null;
        JsonReader jsonReader2 = null;
        try {
            Reader fileReader2 = new FileReader(new File(context.getFilesDir(), fileName));
            try {
                Reader bufferedReader2 = new BufferedReader(fileReader2);
                try {
                    jsonReader = new JsonReader(bufferedReader2);
                } catch (FileNotFoundException e) {
                    e = e;
                    bufferedReader = bufferedReader2;
                    fileReader = fileReader2;
                    try {
                        Log.d(TAG, "Exception while reading from file", e);
                        JavaUtils.closeQuietly(jsonReader2);
                        JavaUtils.closeQuietly(bufferedReader);
                        JavaUtils.closeQuietly(fileReader);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        JavaUtils.closeQuietly(jsonReader2);
                        JavaUtils.closeQuietly(bufferedReader);
                        JavaUtils.closeQuietly(fileReader);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedReader = bufferedReader2;
                    fileReader = fileReader2;
                    JavaUtils.closeQuietly(jsonReader2);
                    JavaUtils.closeQuietly(bufferedReader);
                    JavaUtils.closeQuietly(fileReader);
                    throw th;
                }
                try {
                    T instance = new Gson().fromJson(jsonReader, tClass);
                    JavaUtils.closeQuietly(jsonReader);
                    JavaUtils.closeQuietly(bufferedReader2);
                    JavaUtils.closeQuietly(fileReader2);
                    return instance;
                } catch (FileNotFoundException e2) {
                    e = e2;
                    jsonReader2 = jsonReader;
                    bufferedReader = bufferedReader2;
                    fileReader = fileReader2;
                    Log.d(TAG, "Exception while reading from file", e);
                    JavaUtils.closeQuietly(jsonReader2);
                    JavaUtils.closeQuietly(bufferedReader);
                    JavaUtils.closeQuietly(fileReader);
                    return null;
                } catch (Throwable th3) {
                    th = th3;
                    jsonReader2 = jsonReader;
                    bufferedReader = bufferedReader2;
                    fileReader = fileReader2;
                    JavaUtils.closeQuietly(jsonReader2);
                    JavaUtils.closeQuietly(bufferedReader);
                    JavaUtils.closeQuietly(fileReader);
                    throw th;
                }
            } catch (FileNotFoundException e3) {
                e = e3;
                fileReader = fileReader2;
                Log.d(TAG, "Exception while reading from file", e);
                JavaUtils.closeQuietly(jsonReader2);
                JavaUtils.closeQuietly(bufferedReader);
                JavaUtils.closeQuietly(fileReader);
                return null;
            } catch (Throwable th4) {
                th = th4;
                fileReader = fileReader2;
                JavaUtils.closeQuietly(jsonReader2);
                JavaUtils.closeQuietly(bufferedReader);
                JavaUtils.closeQuietly(fileReader);
                throw th;
            }
        } catch (FileNotFoundException e4) {
            e = e4;
            Log.d(TAG, "Exception while reading from file", e);
            JavaUtils.closeQuietly(jsonReader2);
            JavaUtils.closeQuietly(bufferedReader);
            JavaUtils.closeQuietly(fileReader);
            return null;
        }
    }

    public static boolean isModuleCached(Context context, String moduleName) {
        if (context == null || TextUtils.isEmpty(moduleName)) {
            return false;
        }
        return new File(context.getFilesDir(), moduleName).exists();
    }
}
