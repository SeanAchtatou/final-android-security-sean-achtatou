package ch.nth.android.polyglot.translator;

import android.text.TextUtils;
import ch.nth.android.utils.JavaUtils;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class TranslationModule {
    public static final transient TranslationModule STUB = new TranslationModule();
    private Set<TranslationEntry> entries = new HashSet();
    private transient Map<String, String> mCacheMap;
    private transient Map<String, TranslationEntry> mEntryMap;
    private transient boolean mIsOptimized = false;
    @SerializedName("module")
    private String moduleName = "";

    public String getModuleName() {
        return this.moduleName;
    }

    public TranslationEntry getEntry(String key) {
        ensureEntryMap();
        TranslationEntry entry = this.mEntryMap.get(key);
        return entry != null ? entry : TranslationEntry.STUB;
    }

    public void putEntry(TranslationEntry entry) {
        if (entry != null && entry.getKey() != null) {
            TranslationEntry previousEntry = this.mEntryMap.put(entry.getKey(), entry);
            if (previousEntry == null) {
                this.entries.add(entry);
                return;
            }
            Iterator<TranslationEntry> iterator = this.entries.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().getKey().equals(previousEntry.getKey())) {
                    iterator.remove();
                }
            }
            this.entries.add(entry);
        }
    }

    public Set<String> getEntryNames() {
        ensureEntryMap();
        return this.mEntryMap.keySet();
    }

    /* access modifiers changed from: package-private */
    public String getCachedEntry(String key) {
        if (TextUtils.isEmpty(key)) {
            return "";
        }
        ensureCacheMap();
        String result = this.mCacheMap.get(key);
        return result == null ? "" : result;
    }

    /* access modifiers changed from: package-private */
    public void setCachedEntry(String key, String value) {
        if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(value)) {
            ensureCacheMap();
            this.mCacheMap.put(key, value);
        }
    }

    private void ensureEntryMap() {
        if (!this.mIsOptimized) {
            this.mEntryMap = new HashMap(this.entries.size());
            for (TranslationEntry entry : this.entries) {
                this.mEntryMap.put(entry.getKey(), entry);
            }
            this.mIsOptimized = true;
        }
    }

    private void ensureCacheMap() {
        if (this.mCacheMap == null) {
            this.mCacheMap = new HashMap();
        }
    }

    public static boolean isValid(TranslationModule entry) {
        return (entry == null || STUB == entry) ? false : true;
    }

    public boolean moduleNameMatches(Pattern pattern) {
        return JavaUtils.regexMatch(pattern, this.moduleName);
    }

    public Set<String> getAllLanguages(boolean exhaustive) {
        return getAllLanguages(exhaustive, false);
    }

    public Set<String> getAllLanguages(boolean exhaustive, boolean strict) {
        ensureEntryMap();
        Set<String> languages = new HashSet<>();
        if (exhaustive) {
            for (TranslationEntry entry : this.mEntryMap.values()) {
                languages.addAll(entry.getLanguages(strict));
            }
            return languages;
        }
        Iterator i$ = this.mEntryMap.values().iterator();
        if (i$.hasNext()) {
            return i$.next().getLanguages(strict);
        }
        return languages;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TranslationModule that = (TranslationModule) o;
        if (this.moduleName != null) {
            if (this.moduleName.equals(that.moduleName)) {
                return true;
            }
        } else if (that.moduleName == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.moduleName != null) {
            return this.moduleName.hashCode();
        }
        return 0;
    }

    public String toString() {
        return "TranslationModule { moduleName=" + this.moduleName + ", entries: " + this.entries.size() + " }";
    }
}
