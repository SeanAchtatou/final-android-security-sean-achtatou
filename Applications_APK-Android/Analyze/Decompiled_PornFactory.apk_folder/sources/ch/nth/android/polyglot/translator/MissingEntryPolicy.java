package ch.nth.android.polyglot.translator;

import android.text.TextUtils;

public enum MissingEntryPolicy {
    RESOURCE_FALLBACK,
    NONE;

    public static MissingEntryPolicy valueForNameOrDefault(String name) {
        if (TextUtils.isEmpty(name)) {
            return getDefault();
        }
        for (MissingEntryPolicy policy : values()) {
            if (policy.name().equals(name)) {
                return policy;
            }
        }
        return getDefault();
    }

    public static MissingEntryPolicy getDefault() {
        return RESOURCE_FALLBACK;
    }
}
