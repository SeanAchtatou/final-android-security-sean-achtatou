package ch.nth.android.polyglot.translator.merger;

public enum CollectionMergeType {
    LEFT_MERGE,
    RIGHT_MERGE,
    INNER_MERGE,
    FULL_MERGE
}
