package ch.nth.android.polyglot.translator.provider;

import ch.nth.android.polyglot.translator.TranslationModule;

public interface TranslationProvider {
    void changeLanguage(String str);

    TranslationModule getModule(String str);
}
