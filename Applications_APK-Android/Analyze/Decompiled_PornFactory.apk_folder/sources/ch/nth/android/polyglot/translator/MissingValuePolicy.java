package ch.nth.android.polyglot.translator;

import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public enum MissingValuePolicy {
    RESOURCE_FALLBACK,
    LANGUAGE_FALLBACK,
    ANY_LANGUAGE,
    NONE;
    
    private static final String DELIMITER = ";";

    public static MissingValuePolicy valueForNameOrNull(String name) {
        if (name == null) {
            return null;
        }
        for (MissingValuePolicy policy : values()) {
            if (policy.name().equals(name)) {
                return policy;
            }
        }
        return null;
    }

    public static String getConcatenatedName(Set<MissingValuePolicy> missingValuePolicies) {
        if (missingValuePolicies == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        Iterator<MissingValuePolicy> iterator = missingValuePolicies.iterator();
        while (iterator.hasNext()) {
            builder.append(iterator.next().name());
            if (iterator.hasNext()) {
                builder.append(DELIMITER);
            }
        }
        return builder.toString();
    }

    public static Set<MissingValuePolicy> valuesForConcatenatedNameOrDefault(String concatenatedName) {
        if (concatenatedName == null) {
            return getDefault();
        }
        String[] names = concatenatedName.split(DELIMITER);
        Set<MissingValuePolicy> set = new LinkedHashSet<>();
        for (String name : names) {
            MissingValuePolicy policy = valueForNameOrNull(name);
            if (policy != null) {
                set.add(policy);
            }
        }
        return set;
    }

    public static Set<MissingValuePolicy> getDefault() {
        return EnumSet.of(RESOURCE_FALLBACK);
    }
}
