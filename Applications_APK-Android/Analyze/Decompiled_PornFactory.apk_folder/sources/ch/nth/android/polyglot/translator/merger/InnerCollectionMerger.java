package ch.nth.android.polyglot.translator.merger;

import android.util.Log;
import ch.nth.android.polyglot.translator.TranslationModule;
import ch.nth.android.polyglot.translator.TranslationModuleCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InnerCollectionMerger implements CollectionMerger {
    private static final String TAG = InnerCollectionMerger.class.getSimpleName();

    public TranslationModuleCollection merge(TranslationModuleCollection primaryCollection, TranslationModuleCollection fallbackCollection, List<ModuleMerger> mergers, List<TargetedModuleMerger> targetedMergers) {
        List<TranslationModule> resultingModules = new ArrayList<>();
        for (TranslationModule primaryModule : primaryCollection.getData()) {
            if (TranslationModule.isValid(primaryModule)) {
                String moduleName = primaryModule.getModuleName();
                TranslationModule fallbackModule = fallbackCollection.getTranslationModule(moduleName);
                if (!TranslationModule.isValid(fallbackModule)) {
                    Log.d(TAG, "no-fallback-module-to-merge: " + moduleName);
                } else {
                    boolean mergeCompleted = false;
                    for (TargetedModuleMerger targetedMerger : targetedMergers) {
                        if (targetedMerger.isValidTarget(moduleName)) {
                            Log.d(TAG, "performing-targeted-merge: " + moduleName + ", merger: " + targetedMerger);
                            TranslationModule mergedModule = targetedMerger.merge(primaryModule, fallbackModule);
                            if (TranslationModule.isValid(mergedModule)) {
                                resultingModules.add(mergedModule);
                                mergeCompleted = true;
                            } else {
                                Log.d(TAG, "targeted-merge-result-invalid: " + moduleName + ", merger: " + targetedMerger);
                            }
                        }
                    }
                    if (!mergeCompleted) {
                        Iterator i$ = mergers.iterator();
                        while (true) {
                            if (!i$.hasNext()) {
                                break;
                            }
                            ModuleMerger merger = i$.next();
                            Log.d(TAG, "performing-generic-merge: " + moduleName + ", merger: " + merger);
                            TranslationModule mergedModule2 = merger.merge(primaryModule, fallbackModule);
                            if (TranslationModule.isValid(mergedModule2)) {
                                resultingModules.add(mergedModule2);
                                break;
                            }
                            Log.d(TAG, "generic-merge-result-invalid: " + moduleName + ", merger: " + merger);
                        }
                    }
                }
            }
        }
        return TranslationModuleCollection.fromModules(resultingModules);
    }
}
