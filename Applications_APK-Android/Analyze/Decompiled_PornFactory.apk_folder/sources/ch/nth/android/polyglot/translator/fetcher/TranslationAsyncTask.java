package ch.nth.android.polyglot.translator.fetcher;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.fetcher.FetchAsyncTask;
import ch.nth.android.fetcher.FetchListener;
import ch.nth.android.fetcher.TaskOptions;
import ch.nth.android.fetcher.config.FetchSource;
import ch.nth.android.polyglot.translator.TranslationModule;
import ch.nth.android.polyglot.translator.TranslationModuleCollection;
import ch.nth.android.polyglot.translator.TranslationStorage;
import ch.nth.android.polyglot.translator.merger.CollectionMergeType;
import ch.nth.android.polyglot.translator.merger.FullCollectionMerger;
import ch.nth.android.polyglot.translator.merger.InnerCollectionMerger;
import ch.nth.android.polyglot.translator.merger.LeftCollectionMerger;
import ch.nth.android.polyglot.translator.merger.ModuleMerger;
import ch.nth.android.polyglot.translator.merger.RightCollectionMerger;
import ch.nth.android.polyglot.translator.merger.TargetedModuleMerger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class TranslationAsyncTask extends AsyncTask<Void, Void, Object> {
    private static final String TAG = TranslationAsyncTask.class.getSimpleName();
    private String mCacheFilename;
    private List<ModuleCollectionTaskOptions> mCollectionConfigs;
    private CollectionMergeType mCollectionMergeType;
    private Context mContext;
    private FetchSource mFetchSource;
    private FetchListener mListener;
    private List<ModuleMerger> mMergers;
    private boolean mPreferCachedIfRemoteFailed;
    private int mRequestTag;
    private Set<String> mSeparatelyCachedModules;
    private List<TargetedModuleMerger> mTargetedMergers;
    private boolean mUseCachedIfAvailable = false;

    protected TranslationAsyncTask(Builder builder) {
        this.mContext = builder.mContext;
        this.mMergers = builder.mMergers;
        this.mTargetedMergers = builder.mTargetedMergers;
        this.mListener = builder.mListener;
        this.mCacheFilename = builder.mCacheFilename;
        this.mCollectionConfigs = builder.mCollectionConfigs;
        this.mSeparatelyCachedModules = builder.mSeparatelyCachedModules;
        this.mCollectionMergeType = builder.mCollectionMergeType;
        this.mPreferCachedIfRemoteFailed = builder.mPreferCachedIfRemoteFailed;
    }

    @TargetApi(11)
    public void executeAsync() {
        if (Build.VERSION.SDK_INT >= 11) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        } else {
            execute((Object[]) null);
        }
    }

    /* access modifiers changed from: protected */
    public TranslationModuleCollection doInBackground(Void... voids) {
        TranslationModuleCollection finalResult = null;
        for (ModuleCollectionTaskOptions collectionConfig : this.mCollectionConfigs) {
            TranslationModuleCollection result = getCollection(collectionConfig);
            if (!(result == null || result.getData() == null || result.getData().isEmpty())) {
                if (finalResult == null) {
                    finalResult = new TranslationModuleCollection();
                }
                finalResult.addAllModules(result);
            }
        }
        boolean reallyUseCached = false;
        TranslationModuleCollection cacheResult = null;
        if (!(!(this.mPreferCachedIfRemoteFailed && this.mUseCachedIfAvailable) || (cacheResult = TranslationStorage.deserializeCollection(this.mContext, this.mCacheFilename)) == null || finalResult == null)) {
            int cachedModules = cacheResult.getData().size();
            reallyUseCached = cachedModules > 0 && cachedModules >= finalResult.getData().size();
        }
        if (reallyUseCached) {
            finalResult = cacheResult;
            if (this.mSeparatelyCachedModules != null) {
                for (String moduleName : this.mSeparatelyCachedModules) {
                    if (!TranslationStorage.isModuleCached(this.mContext, moduleName)) {
                        TranslationModule module = finalResult.getTranslationModule(moduleName);
                        if (TranslationModule.isValid(module)) {
                            TranslationStorage.serializeModule(this.mContext, module);
                        }
                    }
                }
            }
        } else {
            TranslationStorage.serializeCollection(this.mContext, finalResult, this.mCacheFilename);
            if (this.mSeparatelyCachedModules != null) {
                for (String moduleName2 : this.mSeparatelyCachedModules) {
                    TranslationModule module2 = finalResult.getTranslationModule(moduleName2);
                    if (TranslationModule.isValid(module2)) {
                        TranslationStorage.serializeModule(this.mContext, module2);
                    }
                }
            }
        }
        return finalResult;
    }

    private TranslationModuleCollection getCollection(ModuleCollectionTaskOptions collectionConfig) {
        if (collectionConfig == null) {
            return null;
        }
        TaskOptions mPrimaryTaskOptions = collectionConfig.getPrimaryTaskOptions();
        TaskOptions mFallbackTaskOptions = collectionConfig.getFallbackTaskOptions();
        Object primaryResult = null;
        Object fallbackResult = null;
        FetchSource primarySource = null;
        FetchSource fallbackSource = null;
        if (mPrimaryTaskOptions != null) {
            try {
                FetchAsyncTask primaryTask = new FetchAsyncTask(this.mContext, mPrimaryTaskOptions);
                primaryTask.executeAsync();
                primaryResult = primaryTask.get();
                primarySource = primaryTask.getFetchSource();
            } catch (InterruptedException e) {
                Log.d(TAG, "InterruptedException", e);
            } catch (ExecutionException e2) {
                Log.d(TAG, "ExecutionException", e2);
            }
        }
        if (mFallbackTaskOptions != null) {
            FetchAsyncTask fallbackTask = new FetchAsyncTask(this.mContext, mFallbackTaskOptions);
            fallbackTask.executeAsync();
            fallbackResult = fallbackTask.get();
            fallbackSource = fallbackTask.getFetchSource();
        }
        if (!(primaryResult instanceof TranslationModuleCollection)) {
            this.mUseCachedIfAvailable = true;
            if (!(fallbackResult instanceof TranslationModuleCollection)) {
                return null;
            }
            this.mFetchSource = fallbackSource;
            this.mRequestTag = mFallbackTaskOptions.getRequestTag();
            return (TranslationModuleCollection) fallbackResult;
        }
        this.mFetchSource = primarySource;
        this.mRequestTag = mPrimaryTaskOptions.getRequestTag();
        if (!(fallbackResult instanceof TranslationModuleCollection)) {
            return (TranslationModuleCollection) primaryResult;
        }
        if (this.mMergers.isEmpty() && this.mTargetedMergers.isEmpty()) {
            return (TranslationModuleCollection) primaryResult;
        }
        TranslationModuleCollection primaryCollection = (TranslationModuleCollection) primaryResult;
        TranslationModuleCollection fallbackCollection = (TranslationModuleCollection) fallbackResult;
        switch (this.mCollectionMergeType) {
            case LEFT_MERGE:
                return new LeftCollectionMerger().merge(primaryCollection, fallbackCollection, this.mMergers, this.mTargetedMergers);
            case RIGHT_MERGE:
                return new RightCollectionMerger().merge(primaryCollection, fallbackCollection, this.mMergers, this.mTargetedMergers);
            case INNER_MERGE:
                return new InnerCollectionMerger().merge(primaryCollection, fallbackCollection, this.mMergers, this.mTargetedMergers);
            case FULL_MERGE:
                return new FullCollectionMerger().merge(primaryCollection, fallbackCollection, this.mMergers, this.mTargetedMergers);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object result) {
        if (this.mListener == null) {
            return;
        }
        if (result != null) {
            this.mListener.onFetchSucceeded(result, this.mFetchSource, this.mRequestTag);
        } else {
            this.mListener.onFetchFailed(this.mRequestTag);
        }
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public String mCacheFilename;
        /* access modifiers changed from: private */
        public List<ModuleCollectionTaskOptions> mCollectionConfigs = new ArrayList(2);
        /* access modifiers changed from: private */
        public CollectionMergeType mCollectionMergeType = CollectionMergeType.FULL_MERGE;
        /* access modifiers changed from: private */
        public Context mContext;
        /* access modifiers changed from: private */
        public FetchListener mListener;
        /* access modifiers changed from: private */
        public List<ModuleMerger> mMergers = new ArrayList();
        /* access modifiers changed from: private */
        public boolean mPreferCachedIfRemoteFailed = true;
        /* access modifiers changed from: private */
        public Set<String> mSeparatelyCachedModules;
        /* access modifiers changed from: private */
        public List<TargetedModuleMerger> mTargetedMergers = new ArrayList();

        public Builder(Context context, ModuleCollectionTaskOptions collectionConfig, FetchListener listener) {
            this.mContext = context == null ? null : context.getApplicationContext();
            this.mListener = listener;
            addCollectionConfig(collectionConfig);
        }

        public Builder addCollectionConfig(ModuleCollectionTaskOptions collectionConfig) {
            if (collectionConfig != null) {
                this.mCollectionConfigs.add(collectionConfig);
            }
            return this;
        }

        public Builder addMerger(ModuleMerger merger) {
            if (merger != null) {
                this.mMergers.add(merger);
            }
            return this;
        }

        public Builder addTargetedMerger(TargetedModuleMerger merger) {
            if (merger != null) {
                this.mTargetedMergers.add(merger);
            }
            return this;
        }

        public Builder withModuleCollectionCacheFilename(String fileName) {
            this.mCacheFilename = fileName;
            return this;
        }

        public Builder addSeparatelyCachedModule(String moduleName) {
            if (!TextUtils.isEmpty(moduleName)) {
                if (this.mSeparatelyCachedModules == null) {
                    this.mSeparatelyCachedModules = new HashSet(4);
                }
                this.mSeparatelyCachedModules.add(moduleName);
            }
            return this;
        }

        public Builder withCollectionMergeType(CollectionMergeType collectionMergeType) {
            if (collectionMergeType != null) {
                this.mCollectionMergeType = collectionMergeType;
            }
            return this;
        }

        public Builder withPreferCachedIfRemoteFailed(boolean preferCached) {
            this.mPreferCachedIfRemoteFailed = preferCached;
            return this;
        }

        public TranslationAsyncTask build() {
            return new TranslationAsyncTask(this);
        }
    }
}
