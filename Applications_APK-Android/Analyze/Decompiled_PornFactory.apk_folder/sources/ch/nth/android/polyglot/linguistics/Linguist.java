package ch.nth.android.polyglot.linguistics;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import ch.nth.android.polyglot.translator.TranslationEntry;
import ch.nth.android.polyglot.translator.TranslationModule;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Linguist {
    public static final String KEY_ENABLED = "enabled";
    public static final String KEY_FLAGS_ASSETS = "flags_assets";
    public static final String KEY_FLAGS_REMOTE = "flags_remote";
    public static final String KEY_NAME_DEFAULT = "name_default";
    public static final String KEY_NAME_PREFIX = "name_";
    private Map<String, LanguagePack> mAvailableLanguagePacks;
    private TranslationModule mModule;

    public Linguist(TranslationModule module) {
        if (module != null) {
            this.mModule = module;
        } else {
            this.mModule = TranslationModule.STUB;
        }
    }

    @Nullable
    public LanguagePack getLanguagePack(String language) {
        return getAvailableLanguagePacksMap().get(language);
    }

    @NonNull
    public List<LanguagePack> getAvailableLanguagePacks() {
        return getAvailableLanguagePacks(true);
    }

    @NonNull
    public List<LanguagePack> getAvailableLanguagePacks(boolean onlyEnabledLanguages) {
        return new ArrayList(getAvailableLanguagePacksMap(onlyEnabledLanguages).values());
    }

    @NonNull
    public Map<String, LanguagePack> getAvailableLanguagePacksMap() {
        return getAvailableLanguagePacksMap(true);
    }

    @NonNull
    public Map<String, LanguagePack> getAvailableLanguagePacksMap(boolean onlyEnabledLanguages) {
        if (this.mAvailableLanguagePacks == null) {
            this.mAvailableLanguagePacks = extractAvailableLanguagePacks(onlyEnabledLanguages);
        }
        return this.mAvailableLanguagePacks;
    }

    @NonNull
    private Map<String, LanguagePack> extractAvailableLanguagePacks(boolean onlyEnabledLanguages) {
        Map<String, LanguagePack> packs = new HashMap<>();
        if (TranslationModule.isValid(this.mModule)) {
            TranslationEntry defaultNameEntry = this.mModule.getEntry(KEY_NAME_DEFAULT);
            TranslationEntry localFlagsEntry = this.mModule.getEntry(KEY_FLAGS_ASSETS);
            TranslationEntry remoteFlagsEntry = this.mModule.getEntry(KEY_FLAGS_REMOTE);
            TranslationEntry enabledEntry = this.mModule.getEntry(KEY_ENABLED);
            Set<String> possibleLanguages = this.mModule.getAllLanguages(true);
            for (String targetLanguage : possibleLanguages) {
                boolean enabled = true;
                if (TranslationEntry.isValid(enabledEntry)) {
                    enabled = "true".equals(enabledEntry.getTranslation(targetLanguage));
                }
                if (enabled || !onlyEnabledLanguages) {
                    LanguagePack pack = new LanguagePack();
                    pack.setAlpha2CountryCode(targetLanguage);
                    pack.setLanguageEnabled(enabled);
                    if (TranslationEntry.isValid(localFlagsEntry)) {
                        pack.setLocalFlagFileName(localFlagsEntry.getTranslation(targetLanguage));
                    }
                    if (TranslationEntry.isValid(remoteFlagsEntry)) {
                        pack.setRemoteFlagUrl(remoteFlagsEntry.getTranslation(targetLanguage));
                    }
                    if (TranslationEntry.isValid(defaultNameEntry)) {
                        pack.setDefaultLanguageName(defaultNameEntry.getTranslation(targetLanguage));
                    }
                    for (String lang : possibleLanguages) {
                        TranslationEntry nameEntry = this.mModule.getEntry(KEY_NAME_PREFIX + lang);
                        if (TranslationEntry.isValid(nameEntry)) {
                            pack.putLanguageName(lang, nameEntry.getTranslation(targetLanguage));
                        }
                    }
                    packs.put(targetLanguage, pack);
                }
            }
        }
        return packs;
    }
}
