package ch.nth.android.polyglot.translator;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import ch.nth.android.lang.Language;
import ch.nth.android.polyglot.linguistics.Linguist;
import ch.nth.android.utils.ResourceUtils;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class Polyglot {
    private static final String DEFAULT_POLYGLOT_PREFERENCES = "polyglot_preferences.xml";
    private static final String KEY_CURRENT_LANG = "key_current_lang";
    private static final String KEY_FALLBACK_LANG = "key_fallback_lang";
    private static final String KEY_MISSING_ENTRY_POLICY = "key_missing_entry_policy";
    private static final String KEY_MISSING_VALUE_POLICY = "key_missing_value_policy";
    private static final String KEY_PERSIST_VALUES = "key_persist_values";
    private Context mContext;
    private String mCurrentLanguage;
    private String mFallbackLanguage;
    private MissingEntryPolicy mMissingEntryPolicy;
    private Set<MissingValuePolicy> mMissingValuePolicies;
    private boolean mPersistValues;
    private boolean mSystemLanguageFallback;
    private TranslationModule mTranslations;

    public Polyglot(TranslationModule translations) {
        this(translations, null);
    }

    public Polyglot(TranslationModule translations, Context context) {
        this.mMissingEntryPolicy = MissingEntryPolicy.getDefault();
        this.mMissingValuePolicies = MissingValuePolicy.getDefault();
        this.mCurrentLanguage = Language.ENGLISH.getTwoLetterCode();
        this.mFallbackLanguage = Language.ENGLISH.getTwoLetterCode();
        this.mPersistValues = true;
        this.mSystemLanguageFallback = true;
        if (translations != null) {
            this.mTranslations = translations;
        } else {
            this.mTranslations = TranslationModule.STUB;
        }
        setContext(context);
        initFromPrefsIfEnabled();
    }

    public Polyglot(TranslationModule translations, Context context, boolean persistValues, boolean systemLanguageFallback) {
        this.mMissingEntryPolicy = MissingEntryPolicy.getDefault();
        this.mMissingValuePolicies = MissingValuePolicy.getDefault();
        this.mCurrentLanguage = Language.ENGLISH.getTwoLetterCode();
        this.mFallbackLanguage = Language.ENGLISH.getTwoLetterCode();
        this.mPersistValues = true;
        this.mSystemLanguageFallback = true;
        if (translations != null) {
            this.mTranslations = translations;
        } else {
            this.mTranslations = TranslationModule.STUB;
        }
        setContext(context);
        setPersistValues(persistValues);
        setSystemLanguageFallback(systemLanguageFallback);
        initFromPrefsIfEnabled();
    }

    public void setContext(Context context) {
        if (context != null) {
            this.mContext = context.getApplicationContext();
        }
    }

    private void initFromPrefsIfEnabled() {
        if (this.mContext != null) {
            SharedPreferences prefs = this.mContext.getSharedPreferences(getPreferencesName(), 0);
            this.mPersistValues = prefs.getBoolean(KEY_PERSIST_VALUES, true);
            if (this.mPersistValues) {
                String currentLanguage = prefs.getString(KEY_CURRENT_LANG, null);
                String fallbackLanguage = prefs.getString(KEY_FALLBACK_LANG, null);
                String missingEntryPolicyString = prefs.getString(KEY_MISSING_ENTRY_POLICY, null);
                String missingValuePolicyString = prefs.getString(KEY_MISSING_VALUE_POLICY, null);
                if (!TextUtils.isEmpty(currentLanguage)) {
                    setCurrentLanguage(currentLanguage, false);
                } else if (this.mSystemLanguageFallback) {
                    String defaultLanguage = Locale.getDefault().getLanguage().toUpperCase();
                    if (!TextUtils.isEmpty(defaultLanguage) && new Linguist(this.mTranslations).getAvailableLanguagePacksMap().keySet().contains(defaultLanguage)) {
                        setCurrentLanguage(defaultLanguage);
                    }
                }
                if (!TextUtils.isEmpty(fallbackLanguage)) {
                    setFallbackLanguage(fallbackLanguage, false);
                }
                if (!TextUtils.isEmpty(missingEntryPolicyString)) {
                    setMissingEntryPolicy(MissingEntryPolicy.valueForNameOrDefault(missingEntryPolicyString), false);
                }
                if (!TextUtils.isEmpty(missingValuePolicyString)) {
                    setMissingValuePolicies(MissingValuePolicy.valuesForConcatenatedNameOrDefault(missingValuePolicyString), false);
                }
            }
        }
    }

    public String getCurrentLanguage() {
        return this.mCurrentLanguage;
    }

    public void setCurrentLanguage(Language currentLanguage) {
        if (currentLanguage != null) {
            setCurrentLanguage(currentLanguage.getTwoLetterCode());
        }
    }

    public void setCurrentLanguage(String currentLanguage) {
        setCurrentLanguage(currentLanguage, this.mPersistValues);
    }

    private void setCurrentLanguage(String currentLanguage, boolean persistValues) {
        if (currentLanguage != null) {
            this.mCurrentLanguage = currentLanguage;
            if (persistValues && this.mContext != null) {
                this.mContext.getSharedPreferences(getPreferencesName(), 0).edit().putString(KEY_CURRENT_LANG, currentLanguage).commit();
            }
        }
    }

    public String getFallbackLanguage() {
        return this.mFallbackLanguage;
    }

    public void setFallbackLanguage(Language fallbackLanguage) {
        if (fallbackLanguage != null) {
            setFallbackLanguage(fallbackLanguage.getTwoLetterCode());
        }
    }

    public void setFallbackLanguage(String fallbackLanguage) {
        setFallbackLanguage(fallbackLanguage, this.mPersistValues);
    }

    private void setFallbackLanguage(String fallbackLanguage, boolean persistValues) {
        if (fallbackLanguage != null) {
            this.mFallbackLanguage = fallbackLanguage;
            if (persistValues && this.mContext != null) {
                this.mContext.getSharedPreferences(getPreferencesName(), 0).edit().putString(KEY_FALLBACK_LANG, fallbackLanguage).commit();
            }
        }
    }

    public boolean isPersistValues() {
        return this.mPersistValues;
    }

    public void setPersistValues(boolean persistValues) {
        this.mPersistValues = persistValues;
        if (this.mContext != null) {
            this.mContext.getSharedPreferences(getPreferencesName(), 0).edit().putBoolean(KEY_PERSIST_VALUES, persistValues).commit();
        }
    }

    private void setSystemLanguageFallback(boolean systemLanguageFallback) {
        this.mSystemLanguageFallback = systemLanguageFallback;
    }

    public MissingEntryPolicy getMissingEntryPolicy() {
        return this.mMissingEntryPolicy;
    }

    public void setMissingEntryPolicy(MissingEntryPolicy missingEntryPolicy) {
        setMissingEntryPolicy(missingEntryPolicy, this.mPersistValues);
    }

    private void setMissingEntryPolicy(MissingEntryPolicy missingEntryPolicy, boolean persistValues) {
        if (missingEntryPolicy != null) {
            this.mMissingEntryPolicy = missingEntryPolicy;
            if (persistValues && this.mContext != null) {
                this.mContext.getSharedPreferences(getPreferencesName(), 0).edit().putString(KEY_MISSING_ENTRY_POLICY, missingEntryPolicy.name()).commit();
            }
        }
    }

    public Set<MissingValuePolicy> getMissingValuePolicies() {
        return this.mMissingValuePolicies;
    }

    public void setMissingValuePolicies(Set<MissingValuePolicy> missingValuePolicies) {
        setMissingValuePolicies(missingValuePolicies, this.mPersistValues);
    }

    private void setMissingValuePolicies(Set<MissingValuePolicy> missingValuePolicies, boolean persistValues) {
        if (missingValuePolicies != null) {
            this.mMissingValuePolicies = missingValuePolicies;
            if (persistValues && this.mContext != null) {
                SharedPreferences prefs = this.mContext.getSharedPreferences(getPreferencesName(), 0);
                prefs.edit().putString(KEY_MISSING_VALUE_POLICY, MissingValuePolicy.getConcatenatedName(missingValuePolicies)).commit();
            }
        }
    }

    /* access modifiers changed from: protected */
    public String getPreferencesName() {
        return DEFAULT_POLYGLOT_PREFERENCES;
    }

    public String getString(String key) {
        return getString(key, this.mCurrentLanguage);
    }

    public String getString(String key, String language) {
        TranslationEntry entry = this.mTranslations.getEntry(key);
        if (TranslationEntry.isValid(entry)) {
            String result = entry.getTranslation(language);
            if (!TextUtils.isEmpty(result)) {
                return result;
            }
            Iterator i$ = this.mMissingValuePolicies.iterator();
            while (i$.hasNext()) {
                switch (i$.next()) {
                    case ANY_LANGUAGE:
                        result = entry.getAnyTranslation();
                        break;
                    case LANGUAGE_FALLBACK:
                        result = entry.getTranslation(this.mFallbackLanguage);
                        break;
                    case RESOURCE_FALLBACK:
                        result = getFromEntryCacheOrResources(key, entry);
                        break;
                }
                if (!TextUtils.isEmpty(result)) {
                    return result;
                }
            }
            return result;
        }
        String result2 = null;
        switch (this.mMissingEntryPolicy) {
            case RESOURCE_FALLBACK:
                result2 = getFromCacheOrResources(key);
                break;
            case NONE:
                result2 = "";
                break;
        }
        return result2;
    }

    private String getFromCacheOrResources(String key) {
        String result = this.mTranslations.getCachedEntry(key);
        if (!TextUtils.isEmpty(result)) {
            return result;
        }
        String result2 = ResourceUtils.getString(this.mContext, key);
        this.mTranslations.setCachedEntry(key, result2);
        return result2;
    }

    private String getFromEntryCacheOrResources(String key, TranslationEntry entry) {
        if (entry == null) {
            return ResourceUtils.getString(this.mContext, key);
        }
        String result = entry.getCachedTranslation();
        if (!TextUtils.isEmpty(result)) {
            return result;
        }
        String result2 = ResourceUtils.getString(this.mContext, key);
        entry.setCachedTranslation(result2);
        return result2;
    }
}
