package ch.nth.android.polyglot.translator.fetcher;

import android.net.Uri;
import android.text.TextUtils;
import ch.nth.android.fetcher.Parser;
import ch.nth.android.fetcher.TaskOptions;
import ch.nth.android.fetcher.config.AssetsFetchOptions;
import ch.nth.android.fetcher.config.RemoteFetchOptions;
import ch.nth.android.nthcommons.dms.api.DmsConstants;

public class TranslationTaskOptions {
    private TranslationTaskOptions() {
    }

    public static class RemoteBuilder {
        private boolean mAppendAdaptLangs = true;
        private String mCredential;
        private boolean mDebug;
        private Parser mParser;
        private int mRequestTag;
        private String mSecret;
        private String mUrl;

        public RemoteBuilder(String url, Parser parser) {
            this.mUrl = url;
            this.mParser = parser;
        }

        public RemoteBuilder withApiKeyAuthHeader(String credential, String secret) {
            this.mCredential = credential;
            this.mSecret = secret;
            return this;
        }

        public RemoteBuilder withDebugMode(boolean debugMode) {
            this.mDebug = debugMode;
            return this;
        }

        public RemoteBuilder withRequestTag(int requestTag) {
            this.mRequestTag = requestTag;
            return this;
        }

        public RemoteBuilder withAppendAdaptLangs(boolean appendAdaptLangs) {
            this.mAppendAdaptLangs = appendAdaptLangs;
            return this;
        }

        public TaskOptions build() {
            Uri.Builder uriBuilder = Uri.parse(this.mUrl).buildUpon();
            if (this.mAppendAdaptLangs && !this.mUrl.contains(DmsConstants.ADAPT_LANGS)) {
                uriBuilder.appendQueryParameter(DmsConstants.ADAPT_LANGS, "false");
            }
            RemoteFetchOptions.Builder remoteOptionsBuilder = new RemoteFetchOptions.Builder(uriBuilder.build().toString());
            if (!TextUtils.isEmpty(this.mCredential) && !TextUtils.isEmpty(this.mSecret)) {
                remoteOptionsBuilder.withApiKeyAuthHeader(this.mCredential, this.mSecret);
            }
            return new TaskOptions.Builder(this.mParser).addStep(remoteOptionsBuilder.build()).withDebugMode(this.mDebug).withRequestTag(this.mRequestTag).build();
        }
    }

    public static class FallbackBuilder {
        private boolean mDebug;
        private String mDirectory;
        private String mFilename;
        private Parser mParser;
        private int mRequestTag;

        public FallbackBuilder(Parser parser) {
            this.mParser = parser;
        }

        public FallbackBuilder withDirectory(String directory) {
            this.mDirectory = directory;
            return this;
        }

        public FallbackBuilder withFilename(String filename) {
            this.mFilename = filename;
            return this;
        }

        public FallbackBuilder withDebugMode(boolean debugMode) {
            this.mDebug = debugMode;
            return this;
        }

        public FallbackBuilder withRequestTag(int requestTag) {
            this.mRequestTag = requestTag;
            return this;
        }

        public TaskOptions build() {
            return new TaskOptions.Builder(this.mParser).addStep(new AssetsFetchOptions.Builder().withDirectory(this.mDirectory != null ? this.mDirectory : AssetsFetchOptions.TRANSLATIONS_DEFAULT_ASSETS_DIRECTORY).withFilename(this.mFilename != null ? this.mFilename : AssetsFetchOptions.STRINGS_DEFAULT_ASSETS_FILENAME).build()).withDebugMode(this.mDebug).withRequestTag(this.mRequestTag).build();
        }
    }
}
