package ch.nth.android.fetcher.toolbox;

import android.util.Log;
import ch.nth.android.fetcher.Parser;
import ch.nth.android.utils.JavaUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.ParseException;

public class GsonParser implements Parser {
    private static final String TAG = GsonParser.class.getSimpleName();
    private Class mClass;
    private Gson mGson;
    private Type mType;

    public GsonParser(Class aClass) {
        this.mClass = aClass;
    }

    public GsonParser(Type type) {
        this.mType = type;
    }

    /* access modifiers changed from: package-private */
    public void setGson(Gson gson) {
        this.mGson = gson;
    }

    public Object parse(String string) throws ParseException {
        if (this.mGson == null) {
            this.mGson = new Gson();
        }
        try {
            if (this.mClass != null) {
                return this.mGson.fromJson(string, this.mClass);
            }
            if (this.mType != null) {
                return this.mGson.fromJson(string, this.mType);
            }
            return null;
        } catch (JsonSyntaxException e) {
            Log.e(TAG, "JsonSyntaxException", e);
        }
    }

    public Object parse(InputStream stream) throws ParseException {
        if (this.mGson == null) {
            this.mGson = new Gson();
        }
        JsonReader jsonReader = null;
        InputStreamReader inputStreamReader = null;
        try {
            InputStreamReader inputStreamReader2 = new InputStreamReader(stream);
            try {
                JsonReader jsonReader2 = new JsonReader(inputStreamReader2);
                try {
                    if (this.mClass != null) {
                        Object fromJson = this.mGson.fromJson(jsonReader2, this.mClass);
                        JavaUtils.closeQuietly(jsonReader2);
                        JavaUtils.closeQuietly(inputStreamReader2);
                        return fromJson;
                    } else if (this.mType != null) {
                        Object fromJson2 = this.mGson.fromJson(jsonReader2, this.mType);
                        JavaUtils.closeQuietly(jsonReader2);
                        JavaUtils.closeQuietly(inputStreamReader2);
                        return fromJson2;
                    } else {
                        JavaUtils.closeQuietly(jsonReader2);
                        JavaUtils.closeQuietly(inputStreamReader2);
                        return null;
                    }
                } catch (JsonSyntaxException e) {
                    e = e;
                    inputStreamReader = inputStreamReader2;
                    jsonReader = jsonReader2;
                    try {
                        Log.e(TAG, "JsonSyntaxException", e);
                        JavaUtils.closeQuietly(jsonReader);
                        JavaUtils.closeQuietly(inputStreamReader);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        JavaUtils.closeQuietly(jsonReader);
                        JavaUtils.closeQuietly(inputStreamReader);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    inputStreamReader = inputStreamReader2;
                    jsonReader = jsonReader2;
                    JavaUtils.closeQuietly(jsonReader);
                    JavaUtils.closeQuietly(inputStreamReader);
                    throw th;
                }
            } catch (JsonSyntaxException e2) {
                e = e2;
                inputStreamReader = inputStreamReader2;
                Log.e(TAG, "JsonSyntaxException", e);
                JavaUtils.closeQuietly(jsonReader);
                JavaUtils.closeQuietly(inputStreamReader);
                return null;
            } catch (Throwable th3) {
                th = th3;
                inputStreamReader = inputStreamReader2;
                JavaUtils.closeQuietly(jsonReader);
                JavaUtils.closeQuietly(inputStreamReader);
                throw th;
            }
        } catch (JsonSyntaxException e3) {
            e = e3;
            Log.e(TAG, "JsonSyntaxException", e);
            JavaUtils.closeQuietly(jsonReader);
            JavaUtils.closeQuietly(inputStreamReader);
            return null;
        }
    }
}
