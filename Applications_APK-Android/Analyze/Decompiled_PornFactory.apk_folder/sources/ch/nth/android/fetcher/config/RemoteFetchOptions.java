package ch.nth.android.fetcher.config;

import ch.nth.android.lang.StringWrapper;
import java.util.HashMap;
import java.util.Map;

public class RemoteFetchOptions extends FetchOptions {
    public static final long DEFAULT_NETWORK_TIMEOUT = 5000;
    private long mNetworkTimeout;
    private String mRemoteUrl;
    private Map<StringWrapper, String> mRequestHeaders;
    private boolean mSkipCache;

    private RemoteFetchOptions(Builder builder) {
        this.mRemoteUrl = builder.mRemoteUrl;
        this.mNetworkTimeout = builder.mNetworkTimeout;
        this.mSkipCache = builder.mSkipCache;
        this.mRequestHeaders = builder.mRequestHeaders;
    }

    public String getRemoteUrl() {
        return this.mRemoteUrl;
    }

    public long getNetworkTimeout() {
        return this.mNetworkTimeout;
    }

    public boolean isSkipCache() {
        return this.mSkipCache;
    }

    public Map<StringWrapper, String> getRequestHeaders() {
        return this.mRequestHeaders;
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public long mNetworkTimeout = 5000;
        /* access modifiers changed from: private */
        public String mRemoteUrl;
        /* access modifiers changed from: private */
        public Map<StringWrapper, String> mRequestHeaders;
        /* access modifiers changed from: private */
        public boolean mSkipCache;

        public Builder(String remoteUrl) {
            this.mRemoteUrl = remoteUrl;
        }

        public Builder withNetworkTimeout(long networkTimeout) {
            this.mNetworkTimeout = networkTimeout;
            return this;
        }

        public Builder skipCache(boolean skipCache) {
            this.mSkipCache = skipCache;
            return this;
        }

        public Builder addRequestHeader(String key, String value) {
            if (!(key == null || value == null)) {
                if (this.mRequestHeaders == null) {
                    this.mRequestHeaders = new HashMap();
                }
                this.mRequestHeaders.put(new StringWrapper(key), value);
            }
            return this;
        }

        public Builder withApiKeyAuthHeader(String credential, String secret) {
            return addRequestHeader("Authorization", "ApiKey " + credential + ":" + secret);
        }

        public RemoteFetchOptions build() {
            return new RemoteFetchOptions(this);
        }
    }
}
