package ch.nth.android.fetcher.config;

public class CacheFetchOptions extends LocalFetchOptions {
    public static final long MAX_STALE_DEFAULT = 14515200;
    private long mMaxStale;
    private String mRemoteUrl;

    private CacheFetchOptions(Builder builder) {
        this.mRemoteUrl = builder.mRemoteUrl;
        this.mMaxStale = builder.mMaxStale;
    }

    public String getRemoteUrl() {
        return this.mRemoteUrl;
    }

    public long getMaxStale() {
        return this.mMaxStale;
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public long mMaxStale = CacheFetchOptions.MAX_STALE_DEFAULT;
        /* access modifiers changed from: private */
        public String mRemoteUrl;

        public Builder(String remoteUrl) {
            this.mRemoteUrl = remoteUrl;
        }

        public Builder withMaxStale(long maxStale) {
            this.mMaxStale = maxStale;
            return this;
        }

        public CacheFetchOptions build() {
            return new CacheFetchOptions(this);
        }
    }
}
