package ch.nth.android.fetcher;

import ch.nth.android.fetcher.config.FetchSource;

public interface FetchListener {
    void onFetchFailed(int i);

    void onFetchSucceeded(Object obj, FetchSource fetchSource, int i);
}
