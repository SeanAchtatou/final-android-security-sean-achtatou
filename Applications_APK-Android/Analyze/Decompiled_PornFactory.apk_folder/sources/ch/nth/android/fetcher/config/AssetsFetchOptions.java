package ch.nth.android.fetcher.config;

public class AssetsFetchOptions extends LocalFetchOptions {
    public static final String DISCLAIMERS_DEFAULT_ASSETS_FILENAME = "disclaimers.json";
    public static final String PLIST_DEFAULT_ASSETS_DIRECTORY = "plist/";
    public static final String PLIST_DEFAULT_ASSETS_FILENAME = "application-settings.plist";
    public static final String STRINGS_DEFAULT_ASSETS_FILENAME = "strings.json";
    public static final String TRANSLATIONS_DEFAULT_ASSETS_DIRECTORY = "translations/";
    /* access modifiers changed from: private */
    public String mDirectory;
    /* access modifiers changed from: private */
    public String mFilename;

    private AssetsFetchOptions() {
    }

    public static AssetsFetchOptions newDefaultPlistInstance() {
        return new Builder().withDirectory(PLIST_DEFAULT_ASSETS_DIRECTORY).withFilename(PLIST_DEFAULT_ASSETS_FILENAME).build();
    }

    public static AssetsFetchOptions newDefaultStringsInstance() {
        return new Builder().withDirectory(TRANSLATIONS_DEFAULT_ASSETS_DIRECTORY).withFilename(STRINGS_DEFAULT_ASSETS_FILENAME).build();
    }

    public static AssetsFetchOptions newDefaultDisclaimersInstance() {
        return new Builder().withDirectory(TRANSLATIONS_DEFAULT_ASSETS_DIRECTORY).withFilename(DISCLAIMERS_DEFAULT_ASSETS_FILENAME).build();
    }

    public String getDirectory() {
        return this.mDirectory;
    }

    public String getFilename() {
        return this.mFilename;
    }

    public static class Builder {
        private String mDirectory;
        private String mFilename;

        public Builder withDirectory(String directory) {
            this.mDirectory = directory;
            return this;
        }

        public Builder withFilename(String filename) {
            this.mFilename = filename;
            return this;
        }

        public AssetsFetchOptions build() {
            AssetsFetchOptions options = new AssetsFetchOptions();
            String unused = options.mDirectory = this.mDirectory;
            String unused2 = options.mFilename = this.mFilename;
            return options;
        }
    }
}
