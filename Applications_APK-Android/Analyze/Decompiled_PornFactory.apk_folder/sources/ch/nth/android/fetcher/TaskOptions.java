package ch.nth.android.fetcher;

import ch.nth.android.fetcher.config.FetchOptions;
import java.util.ArrayList;
import java.util.List;

public class TaskOptions {
    private boolean mDebugMode;
    private Parser mParser;
    private int mRequestTag;
    private List<FetchOptions> mSteps;

    private TaskOptions(Builder builder) {
        this.mSteps = builder.mSteps;
        this.mRequestTag = builder.mRequestTag;
        this.mDebugMode = builder.mDebugMode;
        this.mParser = builder.mParser;
    }

    public List<FetchOptions> getSteps() {
        return this.mSteps;
    }

    public int getRequestTag() {
        return this.mRequestTag;
    }

    public boolean isDebugMode() {
        return this.mDebugMode;
    }

    public Parser getParser() {
        return this.mParser;
    }

    public Builder buildUpon() {
        Builder builder = new Builder(this.mParser).withDebugMode(this.mDebugMode).withRequestTag(this.mRequestTag);
        if (this.mSteps != null) {
            for (FetchOptions step : this.mSteps) {
                builder.addStep(step);
            }
        }
        return builder;
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasRemoteOrCacheSteps() {
        /*
            r3 = this;
            java.util.List<ch.nth.android.fetcher.config.FetchOptions> r2 = r3.mSteps
            java.util.Iterator r0 = r2.iterator()
        L_0x0006:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x001c
            java.lang.Object r1 = r0.next()
            ch.nth.android.fetcher.config.FetchOptions r1 = (ch.nth.android.fetcher.config.FetchOptions) r1
            boolean r2 = r1 instanceof ch.nth.android.fetcher.config.RemoteFetchOptions
            if (r2 != 0) goto L_0x001a
            boolean r2 = r1 instanceof ch.nth.android.fetcher.config.CacheFetchOptions
            if (r2 == 0) goto L_0x0006
        L_0x001a:
            r2 = 1
        L_0x001b:
            return r2
        L_0x001c:
            r2 = 0
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.fetcher.TaskOptions.hasRemoteOrCacheSteps():boolean");
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public boolean mDebugMode;
        /* access modifiers changed from: private */
        public Parser mParser;
        /* access modifiers changed from: private */
        public int mRequestTag;
        /* access modifiers changed from: private */
        public List<FetchOptions> mSteps = new ArrayList();

        public Builder(Parser parser) {
            this.mParser = parser;
        }

        public Builder withParser(Parser parser) {
            this.mParser = parser;
            return this;
        }

        public Builder addStep(FetchOptions option) {
            this.mSteps.add(option);
            return this;
        }

        public Builder withRequestTag(int requestTag) {
            this.mRequestTag = requestTag;
            return this;
        }

        public Builder withDebugMode(boolean debugMode) {
            this.mDebugMode = debugMode;
            return this;
        }

        public TaskOptions build() {
            return new TaskOptions(this);
        }
    }
}
