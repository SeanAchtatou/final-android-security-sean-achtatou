package ch.nth.android.fetcher.toolbox;

import ch.nth.android.fetcher.Parser;
import com.google.gson.Gson;
import java.lang.reflect.Type;

public final class Parsers {
    private Parsers() {
    }

    public static Parser newSimplePlistParser(Class aClass) {
        return new SimplePlistParser(aClass);
    }

    public static Parser newGsonParser(Class aClass) {
        return new GsonParser(aClass);
    }

    public static Parser newGsonParser(Type type) {
        return new GsonParser(type);
    }

    public static Parser newGsonParser(Class aClass, Gson gson) {
        GsonParser parser = new GsonParser(aClass);
        parser.setGson(gson);
        return parser;
    }

    public static Parser newGsonParser(Type type, Gson gson) {
        GsonParser parser = new GsonParser(type);
        parser.setGson(gson);
        return parser;
    }
}
