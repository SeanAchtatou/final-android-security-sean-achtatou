package ch.nth.android.fetcher;

import java.io.InputStream;
import java.text.ParseException;

public interface Parser<T> {
    T parse(InputStream inputStream) throws ParseException;

    T parse(String str) throws ParseException;
}
