package ch.nth.android.fetcher.config;

public enum FetchSource {
    REMOTE,
    CACHE,
    ASSETS
}
