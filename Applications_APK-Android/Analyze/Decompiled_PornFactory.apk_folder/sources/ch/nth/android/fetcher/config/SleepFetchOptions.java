package ch.nth.android.fetcher.config;

public class SleepFetchOptions extends LocalFetchOptions {
    private int mSleepDurationMs;

    public SleepFetchOptions(int sleepDurationMs) {
        this.mSleepDurationMs = sleepDurationMs;
    }

    public int getSleepDurationMs() {
        return this.mSleepDurationMs;
    }
}
