package ch.nth.android.fetcher.toolbox;

import android.util.Log;
import ch.nth.android.fetcher.Parser;
import ch.nth.simpleplist.parser.DdReader;
import java.io.InputStream;
import java.text.ParseException;

public class SimplePlistParser implements Parser {
    private static final String TAG = SimplePlistParser.class.getSimpleName();
    private Class mClass;

    public SimplePlistParser(Class aClass) {
        this.mClass = aClass;
    }

    public Object parse(String string) throws ParseException {
        try {
            return new DdReader().read(this.mClass, string);
        } catch (Exception e) {
            Log.e(TAG, "Parse Exception", e);
            return null;
        }
    }

    public Object parse(InputStream stream) throws ParseException {
        try {
            return new DdReader().read(this.mClass, stream);
        } catch (Exception e) {
            Log.e(TAG, "Parse Exception", e);
            return null;
        }
    }
}
