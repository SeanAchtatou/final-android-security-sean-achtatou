package ch.nth.android.fetcher;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.fetcher.config.AssetsFetchOptions;
import ch.nth.android.fetcher.config.CacheFetchOptions;
import ch.nth.android.fetcher.config.FetchOptions;
import ch.nth.android.fetcher.config.FetchSource;
import ch.nth.android.fetcher.config.RemoteFetchOptions;
import ch.nth.android.fetcher.config.SleepFetchOptions;
import ch.nth.android.lang.StringWrapper;
import ch.nth.android.nthcommons.debugger.Debugger;
import ch.nth.android.nthcommons.debugger.LogEntry;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class FetchAsyncTask extends AsyncTask<Void, Void, Object> {
    private static final String TAG = FetchAsyncTask.class.getSimpleName();
    private Context mContext;
    private boolean mDebugMode;
    private Debugger mDebugger;
    private FetchSource mFetchSource;
    private FetchListener mListener;
    private OkHttpClient mOkHttpClient;
    private TaskOptions mOptions;
    private int mRequestTag;

    public FetchAsyncTask(Context context, TaskOptions options) {
        this(context, options, null);
    }

    public FetchAsyncTask(Context context, TaskOptions options, FetchListener listener) {
        this.mDebugMode = false;
        this.mContext = context == null ? null : context.getApplicationContext();
        this.mOptions = options;
        this.mListener = listener;
        if (this.mOptions != null) {
            this.mRequestTag = this.mOptions.getRequestTag();
            if (this.mOptions.isDebugMode()) {
                this.mDebugMode = true;
                this.mDebugger = new Debugger();
            }
            if (this.mOptions.hasRemoteOrCacheSteps()) {
                this.mOkHttpClient = new OkHttpClient();
                try {
                    this.mOkHttpClient.setCache(new Cache(new File(context.getCacheDir(), "fetcher_cache"), (long) 2097152));
                } catch (IOException e) {
                    addDebugMessage("IOException while trying to set cache");
                }
            }
        }
    }

    @TargetApi(11)
    public void executeAsync() {
        if (Build.VERSION.SDK_INT >= 11) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        } else {
            execute((Object[]) null);
        }
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Void... params) {
        if (this.mOptions == null || this.mOptions.getSteps() == null) {
            return null;
        }
        Object result = null;
        Iterator i$ = this.mOptions.getSteps().iterator();
        while (true) {
            if (!i$.hasNext()) {
                break;
            }
            FetchOptions options = i$.next();
            if (!isCancelled()) {
                if (options instanceof RemoteFetchOptions) {
                    result = getRemote((RemoteFetchOptions) options);
                    if (result != null) {
                        addDebugMessage("remote-fetch-success");
                        this.mFetchSource = FetchSource.REMOTE;
                        continue;
                    } else {
                        continue;
                    }
                } else if (options instanceof CacheFetchOptions) {
                    result = getCached((CacheFetchOptions) options);
                    if (result != null) {
                        addDebugMessage("cached-fetch-success");
                        this.mFetchSource = FetchSource.CACHE;
                        continue;
                    } else {
                        continue;
                    }
                } else if (options instanceof AssetsFetchOptions) {
                    result = getAssets((AssetsFetchOptions) options);
                    if (result != null) {
                        addDebugMessage("assets-fetch-success");
                        this.mFetchSource = FetchSource.ASSETS;
                        continue;
                    } else {
                        continue;
                    }
                } else if (options instanceof SleepFetchOptions) {
                    SleepFetchOptions sleepOptions = (SleepFetchOptions) options;
                    try {
                        addDebugMessage("sleep-started");
                        Thread.sleep((long) sleepOptions.getSleepDurationMs());
                        addDebugMessage("sleep-ended");
                        continue;
                    } catch (InterruptedException e) {
                        Log.e(TAG, "sleep-InterruptedException", e);
                        continue;
                    }
                } else {
                    continue;
                }
                if (result != null) {
                    addDebugMessage("fetch-success---finishing");
                    break;
                }
            } else {
                addDebugMessage("task-cancelled---finishing");
                break;
            }
        }
        printDebugMessages();
        return result;
    }

    private Object getRemote(RemoteFetchOptions options) {
        Request.Builder builder = new Request.Builder().url(options.getRemoteUrl());
        if (options.getRequestHeaders() != null) {
            for (Map.Entry<StringWrapper, String> entry : options.getRequestHeaders().entrySet()) {
                builder.addHeader(((StringWrapper) entry.getKey()).toString(), (String) entry.getValue());
            }
        }
        if (options.isSkipCache()) {
            builder.addHeader("Cache-Control", "no-cache");
        }
        Request request = builder.build();
        OkHttpClient client = this.mOkHttpClient.clone();
        client.setConnectTimeout(options.getNetworkTimeout(), TimeUnit.MILLISECONDS);
        client.setWriteTimeout(options.getNetworkTimeout(), TimeUnit.MILLISECONDS);
        client.setReadTimeout(options.getNetworkTimeout(), TimeUnit.MILLISECONDS);
        Object result = null;
        try {
            addDebugMessage("remote-fetch-started");
            result = processResponse(client.newCall(request).execute());
        } catch (Exception e) {
            addDebugMessage("remote-fetch-exception " + e);
        }
        addDebugMessage("remote-fetch-end");
        return result;
    }

    private Object getCached(CacheFetchOptions options) {
        Request.Builder builder = new Request.Builder().url(options.getRemoteUrl());
        builder.addHeader("Cache-Control", "only-if-cached");
        builder.addHeader("Cache-Control", "max-stale=" + options.getMaxStale());
        Request request = builder.build();
        Object result = null;
        try {
            addDebugMessage("cache-fetch-started");
            result = processResponse(this.mOkHttpClient.newCall(request).execute());
        } catch (Exception e) {
            addDebugMessage("cache-fetch-exception" + e);
        }
        addDebugMessage("cache-fetch-ended");
        return result;
    }

    private Object getAssets(AssetsFetchOptions options) {
        InputStream inputStream = null;
        addDebugMessage("assets-fetch-started");
        try {
            inputStream = this.mContext.getAssets().open(options.getDirectory() + options.getFilename());
        } catch (IOException e) {
            addDebugMessage("assets-fetch-exception " + e);
        }
        Object result = null;
        if (inputStream != null) {
            result = parse(inputStream);
        }
        addDebugMessage("assets-fetch-ended");
        return result;
    }

    private Object processResponse(Response response) {
        Headers requestHeaders;
        if (response == null) {
            addDebugMessage("fetch-response-null!");
            return null;
        }
        if (this.mDebugMode) {
            addDebugMessage("fetch-response " + response);
            Request request = response.request();
            if (!(request == null || (requestHeaders = request.headers()) == null)) {
                addDebugMessage("fetch-request-headers >>>");
                for (int i = 0; i < requestHeaders.size(); i++) {
                    addDebugMessage("    " + requestHeaders.name(i) + ": " + requestHeaders.value(i));
                }
            }
            Headers responseHeaders = response.headers();
            if (responseHeaders != null) {
                addDebugMessage("fetch-response-headers <<<");
                for (int i2 = 0; i2 < responseHeaders.size(); i2++) {
                    addDebugMessage("        " + responseHeaders.name(i2) + ": " + responseHeaders.value(i2));
                }
            }
        }
        if (!response.isSuccessful()) {
            addDebugMessage("fetch-unexpected-response-code! returning...");
            return null;
        }
        String responseBody = null;
        try {
            if (response.body() != null) {
                String temp = response.body().string();
                if (!TextUtils.isEmpty(temp)) {
                    responseBody = temp;
                }
            }
        } catch (IOException e) {
            addDebugMessage("process-response-IOException");
            Log.e(TAG, "process-response-IOException", e);
        }
        return parse(responseBody);
    }

    private Object parse(String string) {
        Parser parser = this.mOptions.getParser();
        if (parser == null) {
            return null;
        }
        try {
            return parser.parse(string);
        } catch (ParseException e) {
            addDebugMessage("String-deserialization-ParseException");
            Log.e(TAG, "String-deserialization-ParseException", e);
            return null;
        }
    }

    private Object parse(InputStream inputStream) {
        Parser parser = this.mOptions.getParser();
        if (parser == null) {
            return null;
        }
        try {
            return parser.parse(inputStream);
        } catch (ParseException e) {
            addDebugMessage("InputStream-deserialization-ParseException");
            Log.e(TAG, "InputStream-deserialization-ParseException", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object result) {
        if (this.mListener == null) {
            return;
        }
        if (result != null) {
            this.mListener.onFetchSucceeded(result, this.mFetchSource, this.mRequestTag);
        } else {
            this.mListener.onFetchFailed(this.mRequestTag);
        }
    }

    public FetchSource getFetchSource() {
        return this.mFetchSource;
    }

    private void addDebugMessage(String message) {
        if (this.mDebugger != null) {
            this.mDebugger.addEntry(new LogEntry(message));
        }
    }

    private void printDebugMessages() {
        if (this.mDebugger != null) {
            this.mDebugger.print();
        }
    }
}
