package ch.nth.android.contentabo_l01.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import ch.nth.android.contentabo.config.modules.VideoDetailsModule;
import ch.nth.android.contentabo.fragments.VideoDetailsCommentsDialogAbstractFragment;
import ch.nth.android.contentabo.fragments.interfaces.CommentSubmitListener;
import ch.nth.android.contentabo.models.content.CommentList;
import ch.nth.android.contentabo.models.utils.CommentsActionsManager;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo_l01.R;

public class VideoDetailsCommentsDialogFragment extends VideoDetailsCommentsDialogAbstractFragment implements View.OnClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsDialogAbstractFragment$CommentFailReason;
    private ImageButton mCloseButton;
    private EditText mCommentsContent;
    private LinearLayout mCommentsEntryLayout;
    private EditText mCommentsNickname;
    private Button mCommentsSubmit;
    private CommentSubmitListener mListener;

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsDialogAbstractFragment$CommentFailReason() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsDialogAbstractFragment$CommentFailReason;
        if (iArr == null) {
            iArr = new int[VideoDetailsCommentsDialogAbstractFragment.CommentFailReason.values().length];
            try {
                iArr[VideoDetailsCommentsDialogAbstractFragment.CommentFailReason.ALREADY_SENDING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[VideoDetailsCommentsDialogAbstractFragment.CommentFailReason.MISSING_NICKNAME_OR_CONTENT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[VideoDetailsCommentsDialogAbstractFragment.CommentFailReason.NETWORK_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsDialogAbstractFragment$CommentFailReason = iArr;
        }
        return iArr;
    }

    public static VideoDetailsCommentsDialogFragment newInstance(Bundle args, CommentSubmitListener listener) {
        VideoDetailsCommentsDialogFragment fragment = new VideoDetailsCommentsDialogFragment();
        args.putInt(VideoDetailsCommentsDialogAbstractFragment.EXTRA_DIALOG_LAYOUT, R.layout.dialog_video_comments);
        fragment.setArguments(args);
        fragment.mListener = listener;
        return fragment;
    }

    /* access modifiers changed from: protected */
    public void createLayoutHooks(View v, LayoutInflater inflater) {
        this.mCloseButton = (ImageButton) v.findViewById(16908327);
        if (this.mCloseButton != null) {
            this.mCloseButton.setOnClickListener(this);
        }
        TextView title = (TextView) v.findViewById(R.id.text_dialog_title);
        if (title != null) {
            title.setText(Translations.getPolyglot().getString(T.string.post_comment));
        }
        this.mCommentsEntryLayout = (LinearLayout) v.findViewById(R.id.comment_entry_layout);
        this.mCommentsNickname = (EditText) v.findViewById(R.id.comment_entry_nickname);
        this.mCommentsNickname.setHint(Translations.getPolyglot().getString(T.string.nickname));
        this.mCommentsContent = (EditText) v.findViewById(R.id.comment_entry_content);
        this.mCommentsContent.setHint(Translations.getPolyglot().getString(T.string.comment));
        this.mCommentsSubmit = (Button) v.findViewById(R.id.comment_entry_submit);
        this.mCommentsSubmit.setText(Translations.getPolyglot().getString(T.string.post_comment));
        this.mCommentsSubmit.setOnClickListener(this);
        if (!(VideoDetailsModule.CommentType.ON.equals(this.mCommentType) && !CommentsActionsManager.getInstance().hasEntry(this.mContentId))) {
            this.mCommentsEntryLayout.setVisibility(8);
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        resetPagingAndFetch();
    }

    public void onNetworkFetchStarted() {
    }

    public void onCommentsFetched(CommentList response, int page) {
    }

    public void onContentError(String message) {
    }

    public void onClick(View v) {
        if (v.getId() == R.id.comment_entry_submit) {
            submitComment(this.mCommentsNickname.getText().toString(), this.mCommentsContent.getText().toString());
        } else if (v.getId() == 16908327) {
            dismiss();
        }
    }

    public void onCommentSubmitSuccess() {
        CommentsActionsManager.getInstance().addEntry(this.mContentId);
        showToastSafe(Translations.getPolyglot().getString(T.string.comment_submit_success));
        if (this.mListener != null) {
            this.mListener.onCommentSubmitSuccess();
        }
        dismiss();
    }

    public void onCommentSubmitError(VideoDetailsCommentsDialogAbstractFragment.CommentFailReason reason) {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsDialogAbstractFragment$CommentFailReason()[reason.ordinal()]) {
            case 1:
                showToastSafe(Translations.getPolyglot().getString(T.string.comment_submit_in_progress));
                return;
            case 2:
                showToastSafe(Translations.getPolyglot().getString(T.string.comments_enter_nickname_and_content));
                return;
            case 3:
                showToastSafe(Translations.getPolyglot().getString(T.string.comment_submit_failed));
                return;
            default:
                showToastSafe(Translations.getPolyglot().getString(T.string.comment_submit_failed));
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean getCanceledOnTouchOutside() {
        return false;
    }
}
