package ch.nth.android.contentabo_l01.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import ch.nth.android.contentabo.activities.MainAbstractActivity;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.fragments.common.YesNoDialogFragment;
import ch.nth.android.contentabo.models.content.Category;
import ch.nth.android.contentabo.navigation.NavigationItem;
import ch.nth.android.contentabo.translations.D;
import ch.nth.android.contentabo.translations.Disclaimers;
import ch.nth.android.contentabo.translations.LanguageChangedEvent;
import ch.nth.android.contentabo.translations.Languages;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.contentabo_l01.fragments.MySubscriptionFragment;
import ch.nth.android.contentabo_l01.fragments.VideoListFragment;
import ch.nth.android.contentabo_l01.fragments.WebFragment;
import ch.nth.android.polyglot.linguistics.LanguagePack;
import ch.nth.android.utils.ResourceUtils;
import com.nth.analytics.android.LocalyticsProvider;
import de.greenrobot.event.EventBus;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends MainAbstractActivity implements YesNoDialogFragment.YesNoDialogListener {
    private static final String FRAGMENT_TAG_VIDEO_LIST = "fragment_tag_video_list";
    private static final int LANG_ITEM_BASE = 17408;
    private LanguagePack mCurrentLanguagePack;
    private VideoListFragment mVideoListFragment;

    public int getContentView() {
        return R.layout.activity_main;
    }

    /* access modifiers changed from: protected */
    public int getNavigationDrawerItemLayoutResource() {
        return R.layout.drawer_list_item;
    }

    /* access modifiers changed from: protected */
    public int getNavigationDrawerHeaderLayoutResource() {
        return R.layout.drawer_list_item_header;
    }

    /* access modifiers changed from: protected */
    public int getNavigationDrawerCategoryLayoutResource() {
        return R.layout.drawer_list_item_category;
    }

    /* access modifiers changed from: protected */
    public View getNavigationDrawerContainer() {
        return findViewById(R.id.left_drawer);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ch.nth.android.contentabo.navigation.NavigationItem.<init>(int, java.lang.String, boolean, ch.nth.android.contentabo.models.content.Category):void
     arg types: [int, java.lang.String, int, ch.nth.android.contentabo.models.content.Category]
     candidates:
      ch.nth.android.contentabo.navigation.NavigationItem.<init>(int, java.lang.String, boolean, ch.nth.android.contentabo.config.menu.RemoteMenu):void
      ch.nth.android.contentabo.navigation.NavigationItem.<init>(int, java.lang.String, boolean, ch.nth.android.contentabo.models.content.Category):void */
    /* access modifiers changed from: protected */
    public List<NavigationItem> createCategoriesNavigationObjects() {
        List<NavigationItem> categoryItems = new ArrayList<>();
        for (Category category : this.mCategories.getData()) {
            categoryItems.add(new NavigationItem(R.drawable.navigation_categories_image, category.getName(), false, category));
        }
        return categoryItems;
    }

    /* access modifiers changed from: protected */
    public void onSelectNavigationItem(int position) {
        NavigationItem item = (NavigationItem) this.mNavigationList.get(position);
        if (!item.isHeader()) {
            int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
            Fragment fragment = null;
            String fragmentTag = null;
            if (item.getTitle().equalsIgnoreCase(Translations.getPolyglot().getString(T.string.app_component_home))) {
                while (backStackCount > 0) {
                    getSupportFragmentManager().popBackStack();
                    backStackCount--;
                }
                return;
            }
            if (item.getTitle().equalsIgnoreCase(Translations.getPolyglot().getString(T.string.app_component_my_subscriptions))) {
                fragment = MySubscriptionFragment.newInstance();
                fragmentTag = MySubscriptionFragment.FRAGMENT_TAG;
            }
            if (fragment == null && item.getRemoteMenu() != null) {
                fragment = WebFragment.newInstance(item.getRemoteMenu());
                fragmentTag = WebFragment.FRAGMENT_TAG;
            }
            while (backStackCount > 0) {
                getSupportFragmentManager().popBackStack();
                backStackCount--;
            }
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().add(R.id.content_frame, fragment, fragmentTag).addToBackStack(fragmentTag).commit();
                getSupportActionBar().setNavigationMode(0);
                AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.SCREEN_SELECT_EVENT, LocalyticsProvider.EventHistoryDbColumns.NAME, fragmentTag);
            } else if (item.getCategory() != null) {
                this.mVideoListFragment.changeCategory(item.getCategory());
                if (backStackCount > 0) {
                    selectNavigationHome();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleIntent(getIntent());
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                if (MainActivity.this.getSupportFragmentManager().getBackStackEntryCount() < 1) {
                    MainActivity.this.getSupportActionBar().setNavigationMode(2);
                }
            }
        });
        this.mVideoListFragment = (VideoListFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_VIDEO_LIST);
        if (this.mVideoListFragment == null) {
            this.mVideoListFragment = VideoListFragment.newInstance(null);
            getSupportFragmentManager().beginTransaction().add(R.id.content_frame, this.mVideoListFragment, FRAGMENT_TAG_VIDEO_LIST).commit();
        }
    }

    public void onResume() {
        super.onResume();
        doVerifySCMSubscription();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.mCurrentLanguagePack = Languages.getLinguist().getLanguagePack(Translations.getPolyglot().getCurrentLanguage());
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        int resId;
        MenuItem langItem = menu.findItem(R.id.menu_item_language);
        List<LanguagePack> availableLanguagePacks = Languages.getLinguist().getAvailableLanguagePacks();
        if (langItem != null && availableLanguagePacks != null && !availableLanguagePacks.isEmpty()) {
            SubMenu subMenu = langItem.getSubMenu();
            subMenu.clear();
            int baseId = 0;
            for (LanguagePack languagePack : availableLanguagePacks) {
                languagePack.setId(baseId);
                MenuItem item = subMenu.add(0, baseId + LANG_ITEM_BASE, 0, languagePack.getDefaultLanguageName());
                int flagResId = ResourceUtils.getResourceId(this, languagePack.getLocalFlagFileName().toLowerCase(), ResourceUtils.RESOURCE_TYPE_DRAWABLE);
                Log.d("MainActivity", "lang: " + languagePack.getLocalFlagFileName().toLowerCase() + ", flag resid=" + flagResId);
                item.setIcon(flagResId);
                baseId++;
            }
        } else if (langItem != null) {
            langItem.setVisible(false);
            langItem.setEnabled(false);
        }
        if (langItem == null || this.mCurrentLanguagePack == null || (resId = ResourceUtils.getResourceId(this, this.mCurrentLanguagePack.getLocalFlagFileName().toLowerCase(), ResourceUtils.RESOURCE_TYPE_DRAWABLE)) == 0) {
            return super.onPrepareOptionsMenu(menu);
        }
        langItem.setIcon(resId);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int langPackId = item.getItemId() - 17408;
        for (LanguagePack languagePack : Languages.getLinguist().getAvailableLanguagePacks()) {
            if (languagePack.getId() == langPackId) {
                setCurrentLanguagePack(languagePack);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setCurrentLanguagePack(LanguagePack languagePack) {
        this.mCurrentLanguagePack = languagePack;
        if (this.mCurrentLanguagePack != null) {
            Translations.getPolyglot().setCurrentLanguage(this.mCurrentLanguagePack.getAlpha2CountryCode());
            EventBus.getDefault().post(new LanguageChangedEvent());
        }
        supportInvalidateOptionsMenu();
    }

    public void onEvent(LanguageChangedEvent event) {
        super.onEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            this.mVideoListFragment.doSearch(intent.getStringExtra("query"));
        }
    }

    /* access modifiers changed from: protected */
    public Class<? extends Activity> getVideoDetailsActivityClass() {
        return VideoDetailsActivity.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ch.nth.android.contentabo.navigation.NavigationItem.<init>(int, java.lang.String, boolean):void
     arg types: [int, java.lang.String, int]
     candidates:
      ch.nth.android.contentabo.navigation.NavigationItem.<init>(int, java.lang.String, java.lang.String):void
      ch.nth.android.contentabo.navigation.NavigationItem.<init>(int, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public boolean createNavigationHeaders(List<NavigationItem> navigationList) {
        navigationList.add(new NavigationItem(R.drawable.navigation_titles_header_image, Translations.getPolyglot().getString(T.string.navigation_titles_header), true));
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ch.nth.android.contentabo.navigation.NavigationItem.<init>(int, java.lang.String, boolean):void
     arg types: [int, java.lang.String, int]
     candidates:
      ch.nth.android.contentabo.navigation.NavigationItem.<init>(int, java.lang.String, java.lang.String):void
      ch.nth.android.contentabo.navigation.NavigationItem.<init>(int, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public boolean createCategoriesNavigationHeaders(List<NavigationItem> navigationList) {
        navigationList.add(new NavigationItem(R.drawable.navigation_categories_header_image, Translations.getPolyglot().getString(T.string.navigation_categories_header), true));
        return false;
    }

    /* access modifiers changed from: protected */
    public int getWebViewLayoutResource() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getWebViewResourceId() {
        return R.id.webview_generic_payment;
    }

    /* access modifiers changed from: protected */
    public boolean shouldCheckSpendingLimit() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void showSpendingLimitDialog() {
        YesNoDialogFragment.newInstance(this, 83, R.layout.dialog_spending_limit, true, getSpendingLimitDialogTranslations()).show(getSupportFragmentManager(), (String) null);
        AnalyticsUtils.tagEvent(this, getAnalyticsSession(), AnalyticsUtils.SPENDING_LIMIT_DIALOG_SHOWN);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"UseSparseArrays"})
    public HashMap<Integer, String> getSpendingLimitDialogTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), getString(R.string.app_name));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.spending_limit_message_text));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString(T.string.spending_limit_button_ok));
        translations.put(Integer.valueOf(R.id.text_dialog_custom), Disclaimers.getPolyglot().getString(D.string.limit_disclaimer));
        return translations;
    }

    public void onDialogPositiveButtonClick(int alertDialogTag) {
        if (alertDialogTag == 83) {
            trySendSpendingLimitSms();
            AnalyticsUtils.tagEvent(this, getAnalyticsSession(), AnalyticsUtils.SPENDING_LIMIT_DIALOG_ACCEPTED);
        }
    }

    public void onDialogNegativeButtonClick(int alertDialogTag) {
    }
}
