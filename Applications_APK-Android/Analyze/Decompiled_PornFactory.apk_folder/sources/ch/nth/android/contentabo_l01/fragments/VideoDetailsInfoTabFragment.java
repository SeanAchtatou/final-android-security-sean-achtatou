package ch.nth.android.contentabo_l01.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import ch.nth.android.contentabo.common.utils.FormatUtils;
import ch.nth.android.contentabo.config.modules.VideoDetailsModule;
import ch.nth.android.contentabo.fragments.VideoDetailsInfoAbstractFragment;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.utils.VotingActionsManager;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo.ui.utils.UiVisible;
import ch.nth.android.contentabo_l01.R;

public class VideoDetailsInfoTabFragment extends VideoDetailsInfoAbstractFragment implements View.OnClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$config$modules$VideoDetailsModule$RatingsType;
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible;
    private TextView mCategoryTextview;
    private View mDescriptionDivider;
    private TextView mDescriptionLabel;
    private TextView mDescriptionTextview;
    private TextView mDurationTextview;
    private View mFirstDivider;
    private RatingBar mRatingBar;
    private View mRatingDivider;
    private TextView mRatingLabel;
    private View mRatingLayout;
    private TextView mRatingTextview;
    private TextView mTagsTextview;
    private TextView mTitleTextview;
    private View mVideoDetailsContent;
    private TextView mVideoDetailsInfoText;
    private View mVideoDetailsProgress;
    private TextView mViewedTextview;

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$config$modules$VideoDetailsModule$RatingsType() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$config$modules$VideoDetailsModule$RatingsType;
        if (iArr == null) {
            iArr = new int[VideoDetailsModule.RatingsType.values().length];
            try {
                iArr[VideoDetailsModule.RatingsType.OFF.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[VideoDetailsModule.RatingsType.ON.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[VideoDetailsModule.RatingsType.ONLY_VIEW.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$config$modules$VideoDetailsModule$RatingsType = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible;
        if (iArr == null) {
            iArr = new int[UiVisible.values().length];
            try {
                iArr[UiVisible.CONTENT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[UiVisible.PROGRESS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[UiVisible.TEXT_MESSAGE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible = iArr;
        }
        return iArr;
    }

    public static VideoDetailsInfoTabFragment newInstance(Bundle args) {
        VideoDetailsInfoTabFragment fragment = new VideoDetailsInfoTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video_info_tab, container, false);
        ((TextView) v.findViewById(R.id.video_details_label_duration)).setText(Translations.getPolyglot().getString(T.string.video_details_label_duration));
        ((TextView) v.findViewById(R.id.video_details_label_category)).setText(Translations.getPolyglot().getString(T.string.video_details_label_category));
        ((TextView) v.findViewById(R.id.video_details_label_viewed)).setText(Translations.getPolyglot().getString(T.string.video_details_label_viewed));
        ((TextView) v.findViewById(R.id.video_details_label_tags)).setText(Translations.getPolyglot().getString(T.string.video_details_label_tags));
        this.mVideoDetailsContent = v.findViewById(R.id.video_details_content);
        this.mVideoDetailsProgress = v.findViewById(R.id.video_details_progress);
        this.mVideoDetailsInfoText = (TextView) v.findViewById(R.id.video_details_info_text);
        this.mTitleTextview = (TextView) v.findViewById(R.id.video_details_title);
        this.mDurationTextview = (TextView) v.findViewById(R.id.video_details_duration);
        this.mCategoryTextview = (TextView) v.findViewById(R.id.video_details_category);
        this.mViewedTextview = (TextView) v.findViewById(R.id.video_details_viewed);
        this.mTagsTextview = (TextView) v.findViewById(R.id.video_details_tag);
        this.mDescriptionTextview = (TextView) v.findViewById(R.id.video_details_description);
        this.mRatingTextview = (TextView) v.findViewById(R.id.rate_video);
        this.mRatingTextview.setText(Translations.getPolyglot().getString(T.string.rate_video));
        this.mRatingBar = (RatingBar) v.findViewById(R.id.video_details_rating);
        this.mDescriptionLabel = (TextView) v.findViewById(R.id.video_details_description_label);
        this.mDescriptionLabel.setText(Translations.getPolyglot().getString(T.string.video_details_label_description));
        this.mDescriptionDivider = v.findViewById(R.id.description_divider);
        this.mRatingDivider = v.findViewById(R.id.rating_divider);
        this.mRatingLabel = (TextView) v.findViewById(R.id.rating_label);
        this.mRatingLabel.setText(Translations.getPolyglot().getString(T.string.video_details_label_rating));
        this.mRatingLayout = v.findViewById(R.id.rating_layout);
        this.mFirstDivider = v.findViewById(R.id.first_divider);
        if (!this.mVideoTitleEnabled) {
            this.mTitleTextview.setVisibility(8);
            this.mFirstDivider.setVisibility(8);
        }
        if (!this.mVideoDescriptionEnabled) {
            this.mDescriptionDivider.setVisibility(8);
            this.mDescriptionLabel.setVisibility(8);
            this.mDescriptionTextview.setVisibility(8);
        }
        updateRatingLayout();
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchContent();
    }

    public void onNetworkFetchStarted() {
        updateUi(UiVisible.PROGRESS);
    }

    public void onContentFetched(Content content) {
        this.mTitleTextview.setText(content.getName());
        this.mDurationTextview.setText(FormatUtils.formatContentDuration(content.getDuration()));
        this.mCategoryTextview.setText(FormatUtils.formatCategoriesList(content.getCategories()));
        this.mViewedTextview.setText(String.valueOf(content.getDownloadCount()));
        this.mTagsTextview.setText(FormatUtils.formatTagList(content.getTags()));
        this.mDescriptionTextview.setText(content.getDescription());
        this.mRatingBar.setRating(content.getAvgRating());
        updateUi(UiVisible.CONTENT);
    }

    public void onContentError(String message) {
        updateUi(UiVisible.TEXT_MESSAGE);
    }

    private void updateUi(UiVisible visible) {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible()[visible.ordinal()]) {
            case 1:
                this.mVideoDetailsContent.setVisibility(0);
                this.mVideoDetailsProgress.setVisibility(8);
                this.mVideoDetailsInfoText.setVisibility(8);
                return;
            case 2:
                this.mVideoDetailsContent.setVisibility(8);
                this.mVideoDetailsProgress.setVisibility(0);
                this.mVideoDetailsInfoText.setVisibility(8);
                return;
            case 3:
                this.mVideoDetailsContent.setVisibility(8);
                this.mVideoDetailsProgress.setVisibility(8);
                this.mVideoDetailsInfoText.setVisibility(0);
                this.mVideoDetailsInfoText.setText(Translations.getPolyglot().getString(T.string.error_fetching_video_details));
                return;
            default:
                return;
        }
    }

    private void updateRatingLayout() {
        int visibility = 0;
        boolean ratingsVisible = true;
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$config$modules$VideoDetailsModule$RatingsType()[this.mRatingsType.ordinal()]) {
            case 1:
                this.mRatingTextview.setOnClickListener(this);
                this.mRatingTextview.setVisibility(0);
                break;
            case 2:
                this.mRatingTextview.setVisibility(8);
                break;
            case 3:
                ratingsVisible = false;
                break;
        }
        if (!ratingsVisible) {
            visibility = 8;
        }
        this.mRatingDivider.setVisibility(visibility);
        this.mRatingLabel.setVisibility(visibility);
        this.mRatingLayout.setVisibility(visibility);
    }

    public void onClick(View v) {
        if (v.getId() != R.id.rate_video) {
            return;
        }
        if (VotingActionsManager.getInstance().hasEntry(this.mContentId)) {
            showToastSafe(Translations.getPolyglot().getString(T.string.rating_dialog_already_voted));
        } else {
            VotingDialogFragment.newInstance(this.mContentId).show(getChildFragmentManager(), (String) null);
        }
    }

    public void updateTitle() {
    }

    /* access modifiers changed from: protected */
    public void onUpdateProgress(int progress) {
    }

    public void onUserNotSubscribed() {
    }

    public void onUserSubscribed() {
    }

    /* access modifiers changed from: protected */
    public void onSubscriptionNextStage() {
    }

    /* access modifiers changed from: protected */
    public Class<?> getWebViewActivity() {
        return null;
    }
}
