package ch.nth.android.contentabo_l01.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;
import ch.nth.android.contentabo.config.menu.RemoteMenu;
import ch.nth.android.contentabo.fragments.StaticWebAbstractFragment;
import ch.nth.android.contentabo_l01.R;

public class WebFragment extends StaticWebAbstractFragment {
    public static final String FRAGMENT_TAG = WebFragment.class.getSimpleName();
    private static final String PARAM_KEY_REMOTE_MENU = "remote_menu";
    private RemoteMenu mRemoteMenu;

    public static WebFragment newInstance(RemoteMenu remoteMenu) {
        WebFragment fragment = new WebFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PARAM_KEY_REMOTE_MENU, remoteMenu);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            this.mRemoteMenu = (RemoteMenu) bundle.getSerializable(PARAM_KEY_REMOTE_MENU);
        }
    }

    public void onStart() {
        super.onStart();
        ((TextView) getView().findViewById(R.id.web_title)).setText(this.mRemoteMenu.getTitle());
    }

    public String getUrl() {
        return this.mRemoteMenu.getRemoteUrl();
    }

    public String getFilename() {
        return this.mRemoteMenu.getLocalFilename();
    }

    public String getRemoteContent() {
        return this.mRemoteMenu.getHtmlContent();
    }

    public int getViewId() {
        return R.layout.fragment_web;
    }

    public void updateTitle() {
    }

    public boolean useUrl() {
        if (this.mRemoteMenu.getType().equals(RemoteMenu.Type.REMOTE_ONLY) || this.mRemoteMenu.getType().equals(RemoteMenu.Type.REMOTE_WITH_LOCAL_FALLBACK)) {
            return true;
        }
        return false;
    }

    public boolean useFilename() {
        if (this.mRemoteMenu.getType().equals(RemoteMenu.Type.LOCAL_ONLY) || this.mRemoteMenu.getType().equals(RemoteMenu.Type.REMOTE_WITH_LOCAL_FALLBACK)) {
            return true;
        }
        return false;
    }

    public boolean useRemoteContent() {
        if (!this.mRemoteMenu.getType().equals(RemoteMenu.Type.HTML) || TextUtils.isEmpty(this.mRemoteMenu.getHtmlContent())) {
            return false;
        }
        return true;
    }
}
