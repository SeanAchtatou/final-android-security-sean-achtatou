package ch.nth.android.contentabo_l01.adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import ch.nth.android.contentabo.config.modules.VideoDetailsModule;
import ch.nth.android.contentabo.fragments.interfaces.UserSubscriptionStatusListener;
import ch.nth.android.contentabo.models.content.Content;

public class VideoDetailsFragmentAdapter extends FragmentPagerAdapter implements UserSubscriptionStatusListener {
    private VideoDetailsModule.CommentType mCommentType = VideoDetailsModule.CommentType.ON;
    private Content mContent;
    private String mContentId;
    private boolean mRelatedVideosEnabled;
    private boolean mUserSubscribed = true;

    public VideoDetailsFragmentAdapter(FragmentManager fm, Context context, String contentId, Content content, VideoDetailsModule.CommentType commentType, boolean relatedVideosEnabled) {
        super(fm);
        this.mContentId = contentId;
        this.mContent = content;
        this.mCommentType = commentType;
        this.mRelatedVideosEnabled = relatedVideosEnabled;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.support.v4.app.Fragment getItem(int r3) {
        /*
            r2 = this;
            switch(r3) {
                case 0: goto L_0x0005;
                case 1: goto L_0x0012;
                case 2: goto L_0x0036;
                default: goto L_0x0003;
            }
        L_0x0003:
            r0 = 0
        L_0x0004:
            return r0
        L_0x0005:
            java.lang.String r0 = r2.mContentId
            ch.nth.android.contentabo.models.content.Content r1 = r2.mContent
            android.os.Bundle r0 = ch.nth.android.contentabo_l01.fragments.VideoDetailsInfoTabFragment.getArgumentBundle(r0, r1)
            ch.nth.android.contentabo_l01.fragments.VideoDetailsInfoTabFragment r0 = ch.nth.android.contentabo_l01.fragments.VideoDetailsInfoTabFragment.newInstance(r0)
            goto L_0x0004
        L_0x0012:
            boolean r0 = r2.isCommentsTabEnabled()
            if (r0 == 0) goto L_0x0023
            java.lang.String r0 = r2.mContentId
            android.os.Bundle r0 = ch.nth.android.contentabo_l01.fragments.VideoDetailsCommentsTabFragment.getArgumentBundle(r0)
            ch.nth.android.contentabo_l01.fragments.VideoDetailsCommentsTabFragment r0 = ch.nth.android.contentabo_l01.fragments.VideoDetailsCommentsTabFragment.newInstance(r0)
            goto L_0x0004
        L_0x0023:
            boolean r0 = r2.isRelatedVideosTabEnabled()
            if (r0 == 0) goto L_0x0036
            java.lang.String r0 = r2.mContentId
            ch.nth.android.contentabo.models.content.Content r1 = r2.mContent
            android.os.Bundle r0 = ch.nth.android.contentabo_l01.fragments.VideoDetailsRelatedTabFragment.getArgumentBundle(r0, r1)
            ch.nth.android.contentabo_l01.fragments.VideoDetailsRelatedTabFragment r0 = ch.nth.android.contentabo_l01.fragments.VideoDetailsRelatedTabFragment.newInstance(r0)
            goto L_0x0004
        L_0x0036:
            boolean r0 = r2.isRelatedVideosTabEnabled()
            if (r0 == 0) goto L_0x0003
            java.lang.String r0 = r2.mContentId
            ch.nth.android.contentabo.models.content.Content r1 = r2.mContent
            android.os.Bundle r0 = ch.nth.android.contentabo_l01.fragments.VideoDetailsRelatedTabFragment.getArgumentBundle(r0, r1)
            ch.nth.android.contentabo_l01.fragments.VideoDetailsRelatedTabFragment r0 = ch.nth.android.contentabo_l01.fragments.VideoDetailsRelatedTabFragment.newInstance(r0)
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.contentabo_l01.adapters.VideoDetailsFragmentAdapter.getItem(int):android.support.v4.app.Fragment");
    }

    public int getCount() {
        if (!this.mUserSubscribed) {
            return 1;
        }
        if (isCommentsTabEnabled() && isRelatedVideosTabEnabled()) {
            return 3;
        }
        if (isCommentsTabEnabled() || isRelatedVideosTabEnabled()) {
            return 2;
        }
        return 1;
    }

    public int getItemPosition(Object object) {
        if (object instanceof UserSubscriptionStatusListener) {
            if (this.mUserSubscribed) {
                ((UserSubscriptionStatusListener) object).onUserSubscribed();
            } else {
                ((UserSubscriptionStatusListener) object).onUserNotSubscribed();
            }
        }
        return super.getItemPosition(object);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.CharSequence getPageTitle(int r3) {
        /*
            r2 = this;
            switch(r3) {
                case 0: goto L_0x0006;
                case 1: goto L_0x0011;
                case 2: goto L_0x0033;
                default: goto L_0x0003;
            }
        L_0x0003:
            java.lang.String r0 = ""
        L_0x0005:
            return r0
        L_0x0006:
            ch.nth.android.polyglot.translator.Polyglot r0 = ch.nth.android.contentabo.translations.Translations.getPolyglot()
            java.lang.String r1 = "tab_title_video_details"
            java.lang.String r0 = r0.getString(r1)
            goto L_0x0005
        L_0x0011:
            boolean r0 = r2.isCommentsTabEnabled()
            if (r0 == 0) goto L_0x0022
            ch.nth.android.polyglot.translator.Polyglot r0 = ch.nth.android.contentabo.translations.Translations.getPolyglot()
            java.lang.String r1 = "tab_title_comments"
            java.lang.String r0 = r0.getString(r1)
            goto L_0x0005
        L_0x0022:
            boolean r0 = r2.isRelatedVideosTabEnabled()
            if (r0 == 0) goto L_0x0033
            ch.nth.android.polyglot.translator.Polyglot r0 = ch.nth.android.contentabo.translations.Translations.getPolyglot()
            java.lang.String r1 = "tab_title_related_videos"
            java.lang.String r0 = r0.getString(r1)
            goto L_0x0005
        L_0x0033:
            boolean r0 = r2.isRelatedVideosTabEnabled()
            if (r0 == 0) goto L_0x0003
            ch.nth.android.polyglot.translator.Polyglot r0 = ch.nth.android.contentabo.translations.Translations.getPolyglot()
            java.lang.String r1 = "tab_title_related_videos"
            java.lang.String r0 = r0.getString(r1)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.contentabo_l01.adapters.VideoDetailsFragmentAdapter.getPageTitle(int):java.lang.CharSequence");
    }

    public void onUserNotSubscribed() {
        this.mUserSubscribed = false;
    }

    public void onUserSubscribed() {
        this.mUserSubscribed = true;
    }

    private boolean isCommentsTabEnabled() {
        return !VideoDetailsModule.CommentType.OFF.equals(this.mCommentType);
    }

    private boolean isRelatedVideosTabEnabled() {
        return this.mRelatedVideosEnabled;
    }
}
