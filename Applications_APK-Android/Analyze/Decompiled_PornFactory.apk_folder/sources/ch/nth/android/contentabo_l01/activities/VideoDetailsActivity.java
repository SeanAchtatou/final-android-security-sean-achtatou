package ch.nth.android.contentabo_l01.activities;

import android.os.Bundle;
import ch.nth.android.contentabo.activities.VideoDetailsAbstractActivity;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.contentabo_l01.fragments.VideoDetailsMainFragment;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;

public class VideoDetailsActivity extends VideoDetailsAbstractActivity {
    public int getContentView() {
        return R.layout.activity_video_details;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction().add(R.id.video_details_fragment_container, VideoDetailsMainFragment.newInstance(VideoDetailsMainFragment.getArgumentBundle(this.mContentId, this.mContent)), null).commit();
    }

    /* access modifiers changed from: protected */
    public int getWebViewLayoutResource() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getWebViewResourceId() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onSuccess(PaymentManagerSteps step, Object item) {
    }

    /* access modifiers changed from: protected */
    public void onFailure(PaymentManagerSteps step, Object error) {
    }
}
