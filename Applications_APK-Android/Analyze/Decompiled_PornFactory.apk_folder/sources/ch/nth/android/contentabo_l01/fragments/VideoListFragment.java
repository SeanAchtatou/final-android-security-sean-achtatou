package ch.nth.android.contentabo_l01.fragments;

import android.app.SearchManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.fragments.VideoListAbstractFragment;
import ch.nth.android.contentabo.models.content.Category;
import ch.nth.android.contentabo.translations.LanguageChangedEvent;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.contentabo_l01.ui.adapters.FragmentsPagerAdapter;
import com.nth.analytics.android.LocalyticsProvider;
import de.greenrobot.event.EventBus;

public class VideoListFragment extends VideoListAbstractFragment implements ActionBar.TabListener {
    private FragmentsPagerAdapter mFragmentsPagerAdapter;
    private ViewPager mViewPager;
    ViewPager.SimpleOnPageChangeListener pagerChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        public void onPageSelected(int position) {
            ActionBar actionBar = VideoListFragment.this.getActionBar();
            if (actionBar != null) {
                actionBar.setSelectedNavigationItem(position);
            }
        }
    };

    public static VideoListFragment newInstance(Category category) {
        VideoListFragment fragment = new VideoListFragment();
        Bundle params = new Bundle();
        params.putSerializable("video_list_param_category", category);
        fragment.setArguments(params);
        return fragment;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("");
        actionBar.setNavigationMode(2);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(2);
        }
        setHasOptionsMenu(true);
        this.mFragmentsPagerAdapter = new FragmentsPagerAdapter(getChildFragmentManager(), null, true, this.mCategory);
        for (int i = 0; i < this.mFragmentsPagerAdapter.getCount(); i++) {
            if (actionBar != null) {
                actionBar.addTab(actionBar.newTab().setText(this.mFragmentsPagerAdapter.getPageTitle(i)).setTabListener(this));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_list, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mViewPager = (ViewPager) view.findViewById(R.id.pager);
        this.mViewPager.setOffscreenPageLimit(3);
        this.mViewPager.setAdapter(this.mFragmentsPagerAdapter);
        this.mViewPager.setOnPageChangeListener(this.pagerChangeListener);
        this.mViewPager.invalidate();
        setupMasLayout(getFloatingMasConfig(), (ViewGroup) view.findViewById(R.id.mas_layout_container));
    }

    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(LanguageChangedEvent event) {
        ActionBar actionBar = getActionBar();
        if (actionBar != null && this.mFragmentsPagerAdapter != null) {
            for (int i = 0; i < this.mFragmentsPagerAdapter.getCount(); i++) {
                actionBar.getTabAt(i).setText(this.mFragmentsPagerAdapter.getPageTitle(i));
            }
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem searchItem = menu.findItem(R.id.search);
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            public boolean onMenuItemActionExpand(MenuItem arg0) {
                return true;
            }

            public boolean onMenuItemActionCollapse(MenuItem arg0) {
                VideoListFragment.this.closeSearchFragment();
                return true;
            }
        });
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setSearchableInfo(((SearchManager) getActivity().getSystemService("search")).getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(false);
    }

    public void changeCategory(Category category) {
        AnalyticsUtils.tagEvent(getActivity(), getAnalyticsSession(), AnalyticsUtils.CATEGORY_SELECT_EVENT, LocalyticsProvider.EventHistoryDbColumns.NAME, category.getName());
        this.mCategory = category;
        updateFragmentPages(null);
        getActionBar().setTitle(this.mCategory.getName());
    }

    public void doSearch(String query) {
        updateFragmentPages(query);
        AnalyticsUtils.tagEvent(getActivity(), getAnalyticsSession(), AnalyticsUtils.SEARCH_EVENT, LocalyticsProvider.EventHistoryDbColumns.NAME, query);
    }

    /* access modifiers changed from: private */
    public void closeSearchFragment() {
        if (getFragmentManager() != null) {
            for (int i = 0; i < this.mFragmentsPagerAdapter.getCount(); i++) {
                VideoListPageFragment viewPagerFragment = (VideoListPageFragment) this.mFragmentsPagerAdapter.getItem(i);
                if (viewPagerFragment != null && viewPagerFragment.isResumed()) {
                    viewPagerFragment.closeSearch();
                }
            }
        }
    }

    private void updateFragmentPages(String searchQuery) {
        if (getFragmentManager() != null) {
            for (int i = 0; i < this.mFragmentsPagerAdapter.getCount(); i++) {
                VideoListPageFragment viewPagerFragment = (VideoListPageFragment) this.mFragmentsPagerAdapter.getItem(i);
                if (viewPagerFragment != null) {
                    if (searchQuery != null) {
                        viewPagerFragment.doSearch(searchQuery);
                    } else {
                        viewPagerFragment.changeCategory(this.mCategory);
                    }
                }
            }
        }
    }

    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        if (this.mViewPager != null) {
            this.mViewPager.setCurrentItem(tab.getPosition());
            closeNavigationDrawer();
        }
    }

    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public void updateTitle() {
    }
}
