package ch.nth.android.contentabo_l01.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import ch.nth.android.contentabo.common.adapter.InfiniteBaseAdapter;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.modules.VideoDetailsModule;
import ch.nth.android.contentabo.fragments.VideoDetailsCommentsAbstractFragment;
import ch.nth.android.contentabo.fragments.interfaces.CommentSubmitListener;
import ch.nth.android.contentabo.models.content.CommentList;
import ch.nth.android.contentabo.models.utils.CommentsActionsManager;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo.ui.adapters.CommentListInfiniteAdapter;
import ch.nth.android.contentabo.ui.utils.UiVisible;
import ch.nth.android.contentabo_l01.R;

public class VideoDetailsCommentsTabFragment extends VideoDetailsCommentsAbstractFragment implements InfiniteBaseAdapter.Listener, View.OnClickListener, CommentSubmitListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsAbstractFragment$CommentFailReason;
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible;
    private View listviewHeader;
    private CommentListInfiniteAdapter mAdapter;
    private EditText mCommentsContent;
    private EditText mCommentsNickname;
    private Button mCommentsToggle;
    private ProgressBar mFooterProgressBar;
    private ListView mListview;
    private TextView mVideoDetailsInfoText;
    private View mVideoDetailsProgress;

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsAbstractFragment$CommentFailReason() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsAbstractFragment$CommentFailReason;
        if (iArr == null) {
            iArr = new int[VideoDetailsCommentsAbstractFragment.CommentFailReason.values().length];
            try {
                iArr[VideoDetailsCommentsAbstractFragment.CommentFailReason.ALREADY_SENDING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[VideoDetailsCommentsAbstractFragment.CommentFailReason.MISSING_NICKNAME_OR_CONTENT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[VideoDetailsCommentsAbstractFragment.CommentFailReason.NETWORK_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsAbstractFragment$CommentFailReason = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible;
        if (iArr == null) {
            iArr = new int[UiVisible.values().length];
            try {
                iArr[UiVisible.CONTENT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[UiVisible.PROGRESS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[UiVisible.TEXT_MESSAGE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible = iArr;
        }
        return iArr;
    }

    public static VideoDetailsCommentsTabFragment newInstance(Bundle args) {
        VideoDetailsCommentsTabFragment fragment = new VideoDetailsCommentsTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        boolean commentEntryEnabled = false;
        View v = inflater.inflate(R.layout.fragment_video_comments_tab, container, false);
        this.mListview = (ListView) v.findViewById(R.id.video_details_content);
        this.mVideoDetailsProgress = v.findViewById(R.id.video_details_progress);
        this.mVideoDetailsInfoText = (TextView) v.findViewById(R.id.video_details_info_text);
        if (VideoDetailsModule.CommentType.ON.equals(this.mCommentType) && !CommentsActionsManager.getInstance().hasEntry(this.mContentId)) {
            commentEntryEnabled = true;
        }
        if (commentEntryEnabled) {
            View listviewHeaderRoot = inflater.inflate(R.layout.comments_listview_header, (ViewGroup) null);
            this.listviewHeader = listviewHeaderRoot.findViewById(R.id.comment_header_layout);
            this.mCommentsToggle = (Button) this.listviewHeader.findViewById(R.id.comment_entry_toggle);
            this.mCommentsToggle.setText(Translations.getPolyglot().getString(T.string.add_comment));
            this.mCommentsToggle.setOnClickListener(this);
            this.mListview.addHeaderView(listviewHeaderRoot);
        }
        View listviewFooter = inflater.inflate(R.layout.listview_progress_footer, (ViewGroup) null);
        this.mFooterProgressBar = (ProgressBar) listviewFooter.findViewById(R.id.progress_bar);
        this.mListview.addFooterView(listviewFooter);
        this.mAdapter = new CommentListInfiniteAdapter(getActivity(), R.layout.comment_list_item, null, this);
        this.mListview.setAdapter((ListAdapter) this.mAdapter);
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        resetPagingAndFetch();
    }

    public void onNetworkFetchStarted() {
        this.mFooterProgressBar.setVisibility(0);
        if (getNextPage() == 0) {
            updateUi(UiVisible.PROGRESS);
        }
    }

    public void onCommentsFetched(CommentList response, int page) {
        this.mFooterProgressBar.setVisibility(8);
        if (page != 0) {
            this.mAdapter.appendData(response);
        } else if (response.getData() == null || response.getData().size() == 0) {
            this.mVideoDetailsInfoText.setText(Translations.getPolyglot().getString(T.string.no_comments));
            this.mListview.setVisibility(0);
            this.mVideoDetailsProgress.setVisibility(8);
            this.mVideoDetailsInfoText.setVisibility(0);
            return;
        } else {
            this.mAdapter.setData(response);
        }
        updateUi(UiVisible.CONTENT);
    }

    public void onContentError(String message) {
        this.mFooterProgressBar.setVisibility(8);
        Utils.doLog("error fetching comments: " + message);
        if (getNextPage() <= 1) {
            this.mVideoDetailsInfoText.setText(Translations.getPolyglot().getString(T.string.error_fetching_video_details));
            updateUi(UiVisible.TEXT_MESSAGE);
        }
    }

    private void updateUi(UiVisible visible) {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible()[visible.ordinal()]) {
            case 1:
                this.mListview.setVisibility(0);
                this.mVideoDetailsProgress.setVisibility(8);
                this.mVideoDetailsInfoText.setVisibility(8);
                return;
            case 2:
                this.mListview.setVisibility(8);
                this.mVideoDetailsProgress.setVisibility(0);
                this.mVideoDetailsInfoText.setVisibility(8);
                return;
            case 3:
                this.mListview.setVisibility(8);
                this.mVideoDetailsProgress.setVisibility(8);
                this.mVideoDetailsInfoText.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public void onLastItemReached() {
        fetchNextPage();
    }

    public void onClick(View v) {
        if (v.getId() == R.id.comment_entry_toggle) {
            VideoDetailsCommentsDialogFragment.newInstance(VideoDetailsCommentsDialogFragment.getArgumentBundle(this.mContentId), this).show(getChildFragmentManager(), (String) null);
        } else if (v.getId() == R.id.comment_entry_submit) {
            submitComment(this.mCommentsNickname.getText().toString(), this.mCommentsContent.getText().toString());
        }
    }

    public void onCommentSubmitSuccess() {
        CommentsActionsManager.getInstance().addEntry(this.mContentId);
        this.listviewHeader.setVisibility(8);
        showToastSafe(Translations.getPolyglot().getString(T.string.comment_submit_success));
        resetPagingAndFetch();
    }

    public void onCommentSubmitError(VideoDetailsCommentsAbstractFragment.CommentFailReason reason) {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$fragments$VideoDetailsCommentsAbstractFragment$CommentFailReason()[reason.ordinal()]) {
            case 1:
                showToastSafe(Translations.getPolyglot().getString(T.string.comment_submit_in_progress));
                return;
            case 2:
                showToastSafe(Translations.getPolyglot().getString(T.string.comments_enter_nickname_and_content));
                return;
            case 3:
                showToastSafe(Translations.getPolyglot().getString(T.string.comment_submit_failed));
                return;
            default:
                showToastSafe(Translations.getPolyglot().getString(T.string.comment_submit_failed));
                return;
        }
    }

    public void updateTitle() {
    }
}
