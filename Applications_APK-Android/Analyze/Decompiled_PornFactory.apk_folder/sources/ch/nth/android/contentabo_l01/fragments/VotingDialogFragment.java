package ch.nth.android.contentabo_l01.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import ch.nth.android.contentabo.fragments.VideoDetailsCommentsDialogAbstractFragment;
import ch.nth.android.contentabo.fragments.VotingDialogAbstractFragment;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo_l01.R;

public class VotingDialogFragment extends VotingDialogAbstractFragment {
    public static VotingDialogFragment newInstance(String contentId) {
        VotingDialogFragment dialogFragment = new VotingDialogFragment();
        Bundle args = new Bundle();
        args.putInt(VideoDetailsCommentsDialogAbstractFragment.EXTRA_DIALOG_LAYOUT, R.layout.dialog_rating);
        args.putString("alert_dialog_content_id", contentId);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    /* access modifiers changed from: protected */
    public void createLayoutHooks(View rootView) {
        setRatingBar((RatingBar) rootView.findViewById(R.id.rating_bar));
        TextView title = (TextView) rootView.findViewById(R.id.text_dialog_title);
        if (title != null) {
            title.setText(Translations.getPolyglot().getString(T.string.rating_dialog_rate_video));
        }
    }

    /* access modifiers changed from: protected */
    public void onVoteSuccessfull() {
        showToastSafe(Translations.getPolyglot().getString(T.string.rating_dialog_success));
        dismiss();
    }

    /* access modifiers changed from: protected */
    public void onVoteFailed(String logMessage, int errorReason) {
        switch (errorReason) {
            case 0:
                showToastSafe(Translations.getPolyglot().getString(T.string.rating_dialog_failure));
                return;
            case 1:
                showToastSafe(Translations.getPolyglot().getString(T.string.rating_dialog_failure));
                return;
            case 2:
                showToastSafe(Translations.getPolyglot().getString(T.string.rating_dialog_pending));
                return;
            case 3:
                showToastSafe(Translations.getPolyglot().getString(T.string.rating_dialog_already_voted));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean submitVoteOnRatingBarChanged() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean getCanceledOnTouchOutside() {
        return true;
    }
}
