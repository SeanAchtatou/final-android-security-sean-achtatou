package ch.nth.android.contentabo_l01.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import ch.nth.android.contentabo.common.adapter.InfiniteBaseAdapter;
import ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.content.ContentList;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo.ui.utils.UiVisible;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.contentabo_l01.activities.VideoDetailsActivity;
import ch.nth.android.contentabo_l01.ui.adapters.RelatedVideosListInfiniteAdapter;

public class VideoDetailsRelatedTabFragment extends VideoDetailsRelatedAbstractFragment implements AdapterView.OnItemClickListener, InfiniteBaseAdapter.Listener {
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible;
    private RelatedVideosListInfiniteAdapter mAdapter;
    private ProgressBar mFooterProgressBar;
    private ListView mListView;
    private TextView mVideoDetailsInfoText;
    private View mVideoDetailsProgress;

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible;
        if (iArr == null) {
            iArr = new int[UiVisible.values().length];
            try {
                iArr[UiVisible.CONTENT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[UiVisible.PROGRESS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[UiVisible.TEXT_MESSAGE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible = iArr;
        }
        return iArr;
    }

    public static VideoDetailsRelatedTabFragment newInstance(Bundle args) {
        VideoDetailsRelatedTabFragment fragment = new VideoDetailsRelatedTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAdapter = new RelatedVideosListInfiniteAdapter(getActivity(), R.layout.video_list_item, null, this, getListviewMasConfig());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video_related_tab, container, false);
        this.mVideoDetailsProgress = v.findViewById(R.id.video_details_progress);
        this.mVideoDetailsInfoText = (TextView) v.findViewById(R.id.video_details_info_text);
        this.mListView = (ListView) v.findViewById(16908298);
        this.mListView.setOnItemClickListener(this);
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        View listviewFooter = inflater.inflate(R.layout.listview_progress_footer, (ViewGroup) null);
        this.mFooterProgressBar = (ProgressBar) listviewFooter.findViewById(R.id.progress_bar);
        this.mListView.addFooterView(listviewFooter);
        return v;
    }

    public void onNetworkFetchStarted() {
        this.mFooterProgressBar.setVisibility(0);
        if (getNextPage() == 0) {
            updateUi(UiVisible.PROGRESS);
        }
    }

    public void onRelatedVideosFetched(ContentList relatedVideosList, int page) {
        this.mFooterProgressBar.setVisibility(8);
        if (page != 0) {
            this.mAdapter.appendData(relatedVideosList);
        } else if (relatedVideosList.getData() == null || relatedVideosList.getData().size() == 0) {
            this.mVideoDetailsInfoText.setText(Translations.getPolyglot().getString(T.string.no_related_videos));
            updateUi(UiVisible.TEXT_MESSAGE);
        } else {
            this.mAdapter.setData(relatedVideosList);
        }
        updateUi(UiVisible.CONTENT);
    }

    public void onRelatedVideosError(String message) {
        this.mFooterProgressBar.setVisibility(8);
        if (getNextPage() <= 1) {
            this.mVideoDetailsInfoText.setText(Translations.getPolyglot().getString(T.string.error_fetching_video_details));
            updateUi(UiVisible.TEXT_MESSAGE);
        }
    }

    public void onLastItemReached() {
        fetchNextPage();
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == 16908298) {
            Content clickedContent = this.mAdapter.getItem(position);
            String clickedContentId = clickedContent.getId();
            Intent intent = new Intent(getActivity(), VideoDetailsActivity.class);
            intent.putExtras(VideoDetailsActivity.getExtrasBundle(clickedContentId, clickedContent));
            startActivity(intent);
        }
    }

    private void updateUi(UiVisible visible) {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$ui$utils$UiVisible()[visible.ordinal()]) {
            case 1:
                this.mListView.setVisibility(0);
                this.mVideoDetailsProgress.setVisibility(8);
                this.mVideoDetailsInfoText.setVisibility(8);
                return;
            case 2:
                this.mListView.setVisibility(8);
                this.mVideoDetailsProgress.setVisibility(0);
                this.mVideoDetailsInfoText.setVisibility(8);
                return;
            case 3:
                this.mListView.setVisibility(8);
                this.mVideoDetailsProgress.setVisibility(8);
                this.mVideoDetailsInfoText.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public void updateTitle() {
    }

    /* access modifiers changed from: protected */
    public void onUpdateProgress(int progress) {
    }

    public void onUserNotSubscribed() {
    }

    public void onUserSubscribed() {
    }

    /* access modifiers changed from: protected */
    public void onSubscriptionNextStage() {
    }

    /* access modifiers changed from: protected */
    public Class<?> getWebViewActivity() {
        return null;
    }
}
