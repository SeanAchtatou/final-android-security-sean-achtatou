package ch.nth.android.contentabo_l01.activities;

import android.view.View;
import ch.nth.android.contentabo.activities.SplashAbstractActivity;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;

public class SplashActivity extends SplashAbstractActivity {
    /* access modifiers changed from: protected */
    public View getAnchorView() {
        return findViewById(R.id.fullscreen_content);
    }

    /* access modifiers changed from: protected */
    public int getImageResource() {
        return R.drawable.img_splashscreen_logo;
    }

    /* access modifiers changed from: protected */
    public Class<?> getNextClass() {
        return MainActivity.class;
    }

    public int getContentView() {
        return R.layout.activity_splash;
    }

    /* access modifiers changed from: protected */
    public int getAgeVerificationView() {
        return R.layout.dialog_age_verification;
    }

    /* access modifiers changed from: protected */
    public int getConectivityView() {
        return R.layout.dialog_connection_alert;
    }

    /* access modifiers changed from: protected */
    public int getTemporarilyDisabledView() {
        return R.layout.dialog_temporarily_disabled;
    }

    /* access modifiers changed from: protected */
    public int getAppUnavailableView() {
        return R.layout.dialog_app_unavailable;
    }

    /* access modifiers changed from: protected */
    public int getWebViewLayoutResource() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getWebViewResourceId() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onSuccess(PaymentManagerSteps step, Object item) {
    }

    /* access modifiers changed from: protected */
    public void onFailure(PaymentManagerSteps step, Object error) {
    }

    /* access modifiers changed from: protected */
    public int getUpdateDialogView() {
        return R.layout.dialog_new_update_available;
    }

    /* access modifiers changed from: protected */
    public int getMandatoryUpdateDialogView() {
        return R.layout.dialog_mandatory_update_available;
    }
}
