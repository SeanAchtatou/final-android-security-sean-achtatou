package ch.nth.android.contentabo_l01.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.MasConfigFactory;
import ch.nth.android.contentabo.fragments.MySubscriptionAbstractFragment;
import ch.nth.android.contentabo.translations.D;
import ch.nth.android.contentabo.translations.Disclaimers;
import ch.nth.android.contentabo.translations.LanguageChangedEvent;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.paymentsdk.v2.model.SubscriptionPaymentSession;

public class MySubscriptionFragment extends MySubscriptionAbstractFragment implements View.OnClickListener {
    public static final String FRAGMENT_TAG = MySubscriptionFragment.class.getSimpleName();
    private Button mAction;
    private View mMembershipContainer;
    private TextView mMembershipPriceLabel;
    private TextView mMembershipPriceText;
    private TextView mTimeCreated;
    private View mTimeCreatedContainer;
    private TextView mTimeCreatedLabel;
    private TextView mTimeExpire;
    private View mTimeExpireContainer;
    private TextView mTimeExpireLabel;
    private TextView mTimeExpired;
    private View mTimeExpiredContainer;
    private TextView mTimeExpiredLabel;
    private TextView mTitleLabel;

    public static MySubscriptionFragment newInstance() {
        return new MySubscriptionFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mysubscription, container, false);
        this.mTitleLabel = (TextView) view.findViewById(R.id.mysubscription_label_title);
        this.mTimeCreatedLabel = (TextView) view.findViewById(R.id.mysubscription_time_created_label);
        this.mTimeCreated = (TextView) view.findViewById(R.id.mysubscription_time_created_value);
        this.mTimeExpireLabel = (TextView) view.findViewById(R.id.mysubscription_time_expire_label);
        this.mTimeExpire = (TextView) view.findViewById(R.id.mysubscription_time_expire_value);
        this.mTimeExpiredLabel = (TextView) view.findViewById(R.id.mysubscription_time_expired_label);
        this.mTimeExpired = (TextView) view.findViewById(R.id.mysubscription_time_expired_value);
        this.mMembershipPriceLabel = (TextView) view.findViewById(R.id.mysubscription_membership_price_label);
        this.mTimeCreatedContainer = view.findViewById(R.id.mysubscription_contener_time_created);
        this.mTimeExpireContainer = view.findViewById(R.id.mysubscription_contener_time_expire);
        this.mTimeExpiredContainer = view.findViewById(R.id.mysubscription_contener_time_expired);
        this.mMembershipContainer = view.findViewById(R.id.mysubscription_contener_membership_price);
        this.mMembershipPriceText = (TextView) this.mMembershipContainer.findViewById(R.id.mysubscription_membership_price_text);
        this.mAction = (Button) view.findViewById(R.id.mysubscription_action);
        this.mAction.setOnClickListener(this);
        this.mTimeCreatedContainer.setVisibility(4);
        this.mTimeExpireContainer.setVisibility(4);
        this.mTimeExpiredContainer.setVisibility(4);
        this.mMembershipContainer.setVisibility(4);
        this.mAction.setVisibility(4);
        setupMasLayout(MasConfigFactory.getMySubscriptionMasConfig(), (ViewGroup) view.findViewById(R.id.mas_layout_container));
        initTextViews();
        refreshLabels();
        return view;
    }

    public void onEvent(LanguageChangedEvent event) {
        super.onEvent(event);
        refreshLabels();
        refreshSubscriptionInfo();
    }

    private void initTextViews() {
        this.mTimeCreated.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_time_default));
        this.mTimeExpire.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_time_default));
        this.mTimeExpired.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_time_default));
    }

    private void refreshLabels() {
        this.mTitleLabel.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_title));
        this.mTimeCreatedLabel.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_time_created));
        this.mTimeExpireLabel.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_time_expire));
        this.mTimeExpiredLabel.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_time_expired));
        this.mMembershipPriceLabel.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_membership_price));
        this.mAction.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_button_abort));
        this.mMembershipPriceText.setText(Disclaimers.getPolyglot().getString(D.string.subscription_price_text));
    }

    public int getAbortSubscriptionDialogView() {
        return R.layout.dialog_abort_subscription;
    }

    public int getSubscriptionExpiredDialogView() {
        return R.layout.dialog_subscription_expired;
    }

    public void onClick(View v) {
        doShowActionDialog();
    }

    public void updateTitle() {
    }

    public void onRefreshUserInfo(SubscriptionPaymentSession session) {
        String timeCreated = getFormatedDate(session.getTimeAuthorized());
        String timeExpire = getExpireDate();
        String timeExpired = getFormatedDate(session.getTimeClosed());
        this.mTimeCreated.setText(timeCreated);
        this.mTimeExpire.setText(timeExpire);
        this.mTimeExpired.setText(timeExpired);
        this.mTimeCreatedContainer.setVisibility(0);
        this.mMembershipContainer.setVisibility(0);
        refreshSubscriptionInfo();
    }

    public void refreshSubscriptionInfo() {
        int i = 0;
        if (isUserActiveFree()) {
            this.mTimeExpireContainer.setVisibility(0);
            this.mTimeExpiredContainer.setVisibility(8);
            this.mTimeExpireLabel.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_time_expire_free));
            this.mAction.setBackgroundResource(R.drawable.btn_mysubscription_abort);
        } else if (isUserActive() && !isUserActiveFree()) {
            this.mTimeExpireContainer.setVisibility(0);
            this.mTimeExpiredContainer.setVisibility(8);
            Utils.doLog("user is active and not in free period");
            this.mTimeExpireLabel.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_time_expire));
            this.mAction.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_button_abort));
            this.mAction.setBackgroundResource(R.drawable.btn_mysubscription_abort);
        } else if (isUserPassive()) {
            this.mTimeExpireContainer.setVisibility(0);
            this.mTimeExpiredContainer.setVisibility(8);
            this.mTimeExpireLabel.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_time_expire_passive));
        } else {
            this.mTimeExpireContainer.setVisibility(8);
            this.mTimeExpiredContainer.setVisibility(0);
            this.mAction.setText(Translations.getPolyglot().getString(T.string.mysubscription_label_button_subscribe));
            this.mAction.setBackground(getResources().getDrawable(R.drawable.btn_mysubscription_subscribe));
        }
        Button button = this.mAction;
        if (!showActionButton()) {
            i = 4;
        }
        button.setVisibility(i);
    }
}
