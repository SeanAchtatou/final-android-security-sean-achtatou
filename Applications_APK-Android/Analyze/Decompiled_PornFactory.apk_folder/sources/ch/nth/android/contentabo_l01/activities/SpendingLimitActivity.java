package ch.nth.android.contentabo_l01.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import ch.nth.android.contentabo.activities.base.BaseAbstractActivity;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.fragments.common.YesNoDialogFragment;
import ch.nth.android.contentabo.translations.D;
import ch.nth.android.contentabo.translations.Disclaimers;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import java.util.HashMap;

public class SpendingLimitActivity extends BaseAbstractActivity implements YesNoDialogFragment.YesNoDialogListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showSpendingLimitDialog();
    }

    /* access modifiers changed from: protected */
    public void showSpendingLimitDialog() {
        YesNoDialogFragment dialog = YesNoDialogFragment.newInstance(this, 83, R.layout.dialog_spending_limit, true, getSpendingLimitDialogTranslations());
        dialog.setNotifyDismissAsNegative(true);
        dialog.show(getSupportFragmentManager(), (String) null);
        AnalyticsUtils.tagEvent(this, getAnalyticsSession(), AnalyticsUtils.SPENDING_LIMIT_DIALOG_SHOWN);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"UseSparseArrays"})
    public HashMap<Integer, String> getSpendingLimitDialogTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), getString(R.string.app_name));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.spending_limit_message_text));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString(T.string.spending_limit_button_ok));
        translations.put(Integer.valueOf(R.id.text_dialog_custom), Disclaimers.getPolyglot().getString(D.string.limit_disclaimer));
        return translations;
    }

    public void onDialogPositiveButtonClick(int alertDialogTag) {
        if (alertDialogTag == 83) {
            trySendSpendingLimitSms();
            AnalyticsUtils.tagEvent(this, getAnalyticsSession(), AnalyticsUtils.SPENDING_LIMIT_DIALOG_ACCEPTED);
        }
        finish();
    }

    public void onDialogNegativeButtonClick(int alertDialogTag) {
        finish();
    }

    public int getContentView() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getWebViewLayoutResource() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getWebViewResourceId() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onSuccess(PaymentManagerSteps step, Object item) {
    }

    /* access modifiers changed from: protected */
    public void onFailure(PaymentManagerSteps step, Object error) {
    }
}
