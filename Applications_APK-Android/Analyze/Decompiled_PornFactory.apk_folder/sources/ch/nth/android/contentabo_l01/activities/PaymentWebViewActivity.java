package ch.nth.android.contentabo_l01.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import ch.nth.android.contentabo.activities.PaymentWebViewAbstractActivity;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.paymentsdk.v2.view.PaymentWebView;

@SuppressLint({"SetJavaScriptEnabled"})
public class PaymentWebViewActivity extends PaymentWebViewAbstractActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PaymentWebView view = (PaymentWebView) findViewById(R.id.webview_generic_payment);
        view.setInitialScale(1);
        view.getSettings().setJavaScriptEnabled(true);
        view.getSettings().setUseWideViewPort(true);
        view.getSettings().setLoadWithOverviewMode(true);
        view.getSettings().setSupportZoom(false);
    }

    public int getContentView() {
        return R.layout.generic_webview_layout;
    }

    /* access modifiers changed from: protected */
    public int getWebViewLayoutResource() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getWebViewResourceId() {
        return R.id.webview_generic_payment;
    }

    public void onBackendCallFinish(int jobId) {
    }
}
