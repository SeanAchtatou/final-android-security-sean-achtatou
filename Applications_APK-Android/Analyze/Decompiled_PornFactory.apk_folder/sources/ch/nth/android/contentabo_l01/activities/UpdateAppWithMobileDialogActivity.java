package ch.nth.android.contentabo_l01.activities;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import ch.nth.android.contentabo.activities.base.BaseAbstractActivity;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.fragments.common.YesNoDialogFragment;
import ch.nth.android.contentabo.service.AlarmReceiver;
import ch.nth.android.contentabo.service.DownloadAppService;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import com.google.android.gms.drive.DriveFile;
import java.util.HashMap;

public class UpdateAppWithMobileDialogActivity extends BaseAbstractActivity implements YesNoDialogFragment.YesNoDialogListener {
    protected static final int TAG_UPDATE_APP_WITH_WLAN_CALLBACK = 581;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showDownloadWithWlanDialog();
    }

    /* access modifiers changed from: protected */
    public void showDownloadWithWlanDialog() {
        YesNoDialogFragment dialog = YesNoDialogFragment.newInstance(this, TAG_UPDATE_APP_WITH_WLAN_CALLBACK, R.layout.dialog_update_with_mobile, true, getDownloadWithWlanTranslations());
        dialog.setNotifyDismissAsNegative(true);
        dialog.show(getSupportFragmentManager(), (String) null);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"UseSparseArrays"})
    public HashMap<Integer, String> getDownloadWithWlanTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), getString(R.string.app_name));
        translations.put(Integer.valueOf(R.id.btn_dialog_negative), Translations.getPolyglot().getString("no"));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString("yes"));
        translations.put(Integer.valueOf(R.id.text_dialog_custom), Translations.getPolyglot().getString(T.string.update_with_mobile_dialog_text));
        return translations;
    }

    public void onDialogPositiveButtonClick(int alertDialogTag) {
        if (alertDialogTag == TAG_UPDATE_APP_WITH_WLAN_CALLBACK) {
            downloadAppWithMobileInterface();
        }
        finish();
    }

    private void downloadAppWithMobileInterface() {
        startService(DownloadAppService.getLaunchIntent(this, false));
        finish();
    }

    public void onDialogNegativeButtonClick(int alertDialogTag) {
        ((AlarmManager) getSystemService("alarm")).set(2, SystemClock.elapsedRealtime() + ((long) (AppConfig.getInstance().getConfig().getEntStore().getUpdateRetryTimeoutSeconds() * 1000)), PendingIntent.getBroadcast(this, 0, new Intent(AlarmReceiver.ACTION_START_APP_UPDATE), DriveFile.MODE_READ_ONLY));
        finish();
    }

    public int getContentView() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getWebViewLayoutResource() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getWebViewResourceId() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onSuccess(PaymentManagerSteps step, Object item) {
    }

    /* access modifiers changed from: protected */
    public void onFailure(PaymentManagerSteps step, Object error) {
    }
}
