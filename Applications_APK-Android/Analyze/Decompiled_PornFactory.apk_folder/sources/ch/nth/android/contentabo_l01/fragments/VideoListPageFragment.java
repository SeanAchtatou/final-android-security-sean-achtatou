package ch.nth.android.contentabo_l01.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import ch.nth.android.contentabo.activities.MainAbstractActivity;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.fragments.VideoListPageAbstractFragment;
import ch.nth.android.contentabo.models.content.Category;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.content.ContentList;
import ch.nth.android.contentabo.models.utils.VideoSorting;
import ch.nth.android.contentabo.translations.D;
import ch.nth.android.contentabo.translations.Disclaimers;
import ch.nth.android.contentabo.translations.LanguageChangedEvent;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.contentabo_l01.ui.adapters.VideoListAdapter;
import ch.nth.android.utils.ViewUtils;
import de.greenrobot.event.EventBus;
import java.util.ArrayList;

public class VideoListPageFragment extends VideoListPageAbstractFragment implements AdapterView.OnItemClickListener {
    private static final String PARAM_KEY_CATEGORY = "video_list_param_category";
    private static final String PARAM_KEY_DATA = "video_list_param_data";
    private static final String PARAM_KEY_SORTING = "video_list_param_sorting";
    private static final String PARAM_KEY_WAITING_RESPONSE = "video_list_param_waiting_response";
    private VideoListAdapter adapter;
    private ContentList mData;
    private TextView mDisclaimerTextView;
    private TextView mEmptyMessage;
    private ProgressBar mFooterProgressBar;
    private ListView mListView;
    private View mListViewDisclaimerHeaderView;
    private ProgressBar mProgress;
    private boolean mWaitingResponse;
    private BroadcastReceiver scmStatusChangeReciever = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Utils.doLog("payment", "videolistpagefragment scmStatusChangeReciever");
            VideoListPageFragment.this.setupDisclaimerView();
        }
    };

    public static VideoListPageFragment newInstance(Category category, VideoSorting sorting, ContentList data, boolean waitingResponse) {
        VideoListPageFragment instance = new VideoListPageFragment();
        Bundle params = new Bundle();
        params.putSerializable(PARAM_KEY_CATEGORY, category);
        params.putSerializable(PARAM_KEY_SORTING, sorting);
        params.putSerializable(PARAM_KEY_DATA, data);
        params.putBoolean(PARAM_KEY_WAITING_RESPONSE, waitingResponse);
        instance.setArguments(params);
        return instance;
    }

    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            extractData(getArguments());
        } else {
            extractData(savedInstanceState);
        }
        super.onCreate(savedInstanceState);
    }

    private void extractData(Bundle bundle) {
        this.mCategory = (Category) bundle.getSerializable(PARAM_KEY_CATEGORY);
        this.mSorting = (VideoSorting) bundle.getSerializable(PARAM_KEY_SORTING);
        this.mData = (ContentList) bundle.getSerializable(PARAM_KEY_DATA);
        this.mWaitingResponse = bundle.getBoolean(PARAM_KEY_WAITING_RESPONSE);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_video_list_page, container, false);
        this.mListView = (ListView) fragmentView.findViewById(R.id.videos_listview);
        this.mEmptyMessage = (TextView) fragmentView.findViewById(R.id.empty_message);
        this.mProgress = (ProgressBar) fragmentView.findViewById(R.id.progressbar);
        View listviewFooter = inflater.inflate(R.layout.listview_progress_footer, (ViewGroup) null);
        this.mFooterProgressBar = (ProgressBar) listviewFooter.findViewById(R.id.progress_bar);
        this.mFooterProgressBar.setVisibility(8);
        this.mListViewDisclaimerHeaderView = inflater.inflate(R.layout.listview_disclaimer_header, (ViewGroup) null);
        this.mDisclaimerTextView = (TextView) this.mListViewDisclaimerHeaderView.findViewById(R.id.text);
        this.mListView.addHeaderView(this.mListViewDisclaimerHeaderView);
        setupDisclaimerView();
        this.mListView.addFooterView(listviewFooter, null, false);
        this.mListView.setOnItemClickListener(this);
        initTextViews();
        invalidate();
        return fragmentView;
    }

    public void onResume() {
        super.onResume();
        setupDisclaimerView();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(this.scmStatusChangeReciever, new IntentFilter(PaymentUtils.ACTION_SCM_STATUS_CHANGE));
        EventBus.getDefault().register(this);
    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(this.scmStatusChangeReciever);
        EventBus.getDefault().unregister(this);
    }

    /* access modifiers changed from: protected */
    public void initTextViews() {
        ViewUtils.setText(this.mEmptyMessage, Translations.getPolyglot().getString(T.string.no_content));
        ViewUtils.setText(this.mDisclaimerTextView, Disclaimers.getPolyglot().getString(D.string.video_list_disclaimer));
        if (this.adapter != null) {
            this.adapter.notifyDataSetChanged();
        } else {
            invalidate();
        }
    }

    public void onEvent(LanguageChangedEvent event) {
        initTextViews();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PARAM_KEY_CATEGORY, this.mCategory);
        outState.putSerializable(PARAM_KEY_SORTING, this.mSorting);
        outState.putSerializable(PARAM_KEY_DATA, this.mData);
        outState.putBoolean(PARAM_KEY_WAITING_RESPONSE, this.mWaitingResponse);
    }

    public void changeCategory(Category category) {
        super.changeCategory(category);
        this.mData = null;
        this.mWaitingResponse = true;
        invalidate();
        updateTitle();
    }

    /* access modifiers changed from: private */
    public void setupDisclaimerView() {
        if (!PaymentUtils.isSubscribed() && this.mDisclaimerTextView != null) {
            this.mDisclaimerTextView.setVisibility(0);
        }
        if (PaymentUtils.isSubscribed() && this.mDisclaimerTextView != null) {
            this.mDisclaimerTextView.setVisibility(8);
        }
    }

    public void setData(ContentList data, boolean waitingResponse) {
        this.mData = data;
        this.mWaitingResponse = waitingResponse;
        invalidate();
    }

    private void invalidate() {
        if (this.mWaitingSearchResults) {
            this.mWaitingSearchResults = false;
            if (this.adapter == null) {
                Context context = getActivity();
                if (context != null) {
                    this.adapter = new VideoListAdapter(context, R.layout.video_list_item, new ArrayList(), 0, this, getListviewMasConfig());
                    this.mListView.setAdapter((ListAdapter) this.adapter);
                }
            } else {
                this.adapter.clear();
            }
            this.mProgress.setVisibility(0);
            this.mEmptyMessage.setVisibility(8);
        } else if (this.mData != null && this.mData.getData() != null && this.mData.getData().size() != 0) {
            this.mProgress.setVisibility(8);
            this.mEmptyMessage.setVisibility(8);
            if (this.mWaitingResponse) {
                this.mFooterProgressBar.setVisibility(0);
            } else {
                this.mFooterProgressBar.setVisibility(8);
            }
            if (this.adapter == null) {
                Context context2 = getActivity();
                if (context2 != null) {
                    this.adapter = new VideoListAdapter(context2, R.layout.video_list_item, this.mData.getData(), this.mData.getTotal(), this, getListviewMasConfig());
                    this.mListView.setAdapter((ListAdapter) this.adapter);
                }
            } else {
                this.adapter.setData(this.mData.getData(), this.mData.getTotal());
            }
            if (this.mResetPositionOnDataFetched) {
                this.mResetPositionOnDataFetched = false;
                this.mListView.setSelection(0);
            }
        } else if (this.mWaitingResponse) {
            this.mProgress.setVisibility(0);
            this.mEmptyMessage.setVisibility(8);
        } else {
            if (this.adapter != null) {
                this.adapter.clear();
            }
            this.mProgress.setVisibility(8);
            this.mEmptyMessage.setVisibility(0);
        }
    }

    public void onLastItemReached() {
        if (this.mCategory != null || this.mSearchItem != null) {
            this.mFooterProgressBar.setVisibility(0);
            doLoadNextPage();
        }
    }

    /* access modifiers changed from: protected */
    public void onContentError() {
        this.mWaitingResponse = false;
        invalidate();
    }

    public void updateTitle() {
        if (this.mCategory != null) {
            getActionBar().setTitle(this.mCategory.getName());
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Content selectedItem = this.adapter.getItem((int) id);
        if (getActivity() instanceof MainAbstractActivity) {
            ((MainAbstractActivity) getActivity()).onVideoItemClicked(selectedItem);
        }
    }
}
