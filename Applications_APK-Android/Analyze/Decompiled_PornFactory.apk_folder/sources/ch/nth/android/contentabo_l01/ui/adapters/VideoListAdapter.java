package ch.nth.android.contentabo_l01.ui.adapters;

import android.content.Context;
import ch.nth.android.contentabo.common.adapter.InfiniteBaseAdapter;
import ch.nth.android.contentabo.config.mas.ListviewMasConfig;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.ui.adapters.VideoListEndlessAdapter;
import java.util.List;

public class VideoListAdapter extends VideoListEndlessAdapter {
    public VideoListAdapter(Context context, int layoutResource, List<Content> content, int total, InfiniteBaseAdapter.Listener listener, ListviewMasConfig masConfig) {
        super(context, layoutResource, content, total, listener, masConfig);
    }

    /* access modifiers changed from: protected */
    public Content.PictureFlavor getPictureFlavor() {
        return Content.PictureFlavor.FULL_PICTURE;
    }

    /* access modifiers changed from: protected */
    public Content.PictureSelectMode getPictureSelectMode() {
        return super.getPictureSelectMode();
    }
}
