package ch.nth.android.contentabo_l01.service;

import android.app.Activity;
import ch.nth.android.contentabo_l01.activities.SpendingLimitActivity;

public class AlarmReceiver extends ch.nth.android.contentabo.service.AlarmReceiver {
    /* access modifiers changed from: protected */
    public Class<? extends Activity> getSpendingLimitActivityClass() {
        return SpendingLimitActivity.class;
    }
}
