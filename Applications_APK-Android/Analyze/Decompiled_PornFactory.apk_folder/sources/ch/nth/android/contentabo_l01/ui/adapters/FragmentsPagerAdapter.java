package ch.nth.android.contentabo_l01.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import ch.nth.android.contentabo.models.content.Category;
import ch.nth.android.contentabo.models.content.ContentList;
import ch.nth.android.contentabo.models.utils.VideoSorting;
import ch.nth.android.contentabo_l01.fragments.VideoListPageFragment;
import java.util.LinkedList;
import java.util.List;

public class FragmentsPagerAdapter extends FragmentPagerAdapter {
    private Category mCategory;
    private ContentList mData;
    private List<VideoListPageFragment> mFragments = new LinkedList();
    private boolean mWaitingResponse;

    public FragmentsPagerAdapter(FragmentManager fm, ContentList data, boolean waitingResponse, Category category) {
        super(fm);
        this.mWaitingResponse = waitingResponse;
        this.mData = data;
        this.mCategory = category;
        VideoListPageFragment topRatedPage = VideoListPageFragment.newInstance(this.mCategory, VideoSorting.TOP_RATED, this.mData, this.mWaitingResponse);
        VideoListPageFragment newestPage = VideoListPageFragment.newInstance(this.mCategory, VideoSorting.NEWEST, this.mData, this.mWaitingResponse);
        VideoListPageFragment popularityPage = VideoListPageFragment.newInstance(this.mCategory, VideoSorting.POPULARITY, this.mData, this.mWaitingResponse);
        this.mFragments.add(topRatedPage);
        this.mFragments.add(newestPage);
        this.mFragments.add(popularityPage);
    }

    public Fragment getItem(int position) {
        return this.mFragments.get(position);
    }

    public int getCount() {
        return VideoSorting.values().length;
    }

    public void setmCategory(Category category) {
        this.mCategory = category;
    }

    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return VideoSorting.TOP_RATED.toString();
            case 1:
                return VideoSorting.NEWEST.toString();
            case 2:
                return VideoSorting.POPULARITY.toString();
            default:
                return "";
        }
    }
}
