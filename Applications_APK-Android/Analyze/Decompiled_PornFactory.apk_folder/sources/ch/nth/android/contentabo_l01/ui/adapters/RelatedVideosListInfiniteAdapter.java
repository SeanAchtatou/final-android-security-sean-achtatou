package ch.nth.android.contentabo_l01.ui.adapters;

import android.content.Context;
import ch.nth.android.contentabo.common.adapter.InfiniteBaseAdapter;
import ch.nth.android.contentabo.config.mas.ListviewMasConfig;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.content.ContentList;
import ch.nth.android.contentabo.ui.adapters.RelatedVideosListInfiniteAbstractAdapter;

public class RelatedVideosListInfiniteAdapter extends RelatedVideosListInfiniteAbstractAdapter {
    public RelatedVideosListInfiniteAdapter(Context context, int layoutResource, ContentList contentList, InfiniteBaseAdapter.Listener listener, ListviewMasConfig masConfig) {
        super(context, layoutResource, contentList, listener, masConfig);
    }

    /* access modifiers changed from: protected */
    public Content.PictureFlavor getPictureFlavor() {
        return Content.PictureFlavor.THUMB;
    }

    /* access modifiers changed from: protected */
    public Content.PictureSelectMode getPictureSelectMode() {
        return Content.PictureSelectMode.FIRST;
    }

    /* access modifiers changed from: protected */
    public boolean showProgressBarForImageLoading() {
        return true;
    }
}
