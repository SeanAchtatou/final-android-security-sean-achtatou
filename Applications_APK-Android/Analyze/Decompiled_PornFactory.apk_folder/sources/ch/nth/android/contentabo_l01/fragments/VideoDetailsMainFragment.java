package ch.nth.android.contentabo_l01.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.common.utils.FormatUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.MasConfigFactory;
import ch.nth.android.contentabo.fragments.VideoDetailsMainAbstractFragment;
import ch.nth.android.contentabo.fragments.common.DoubleMOFlowDialogFragment;
import ch.nth.android.contentabo.fragments.common.SingleMOFlowDialogFragment;
import ch.nth.android.contentabo.fragments.common.TermsAndConditionsDialogFragment;
import ch.nth.android.contentabo.fragments.common.YesNoDialogFragment;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.payment.PaymentOption;
import ch.nth.android.contentabo.translations.D;
import ch.nth.android.contentabo.translations.Disclaimers;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo_l01.R;
import ch.nth.android.contentabo_l01.activities.PaymentWebViewActivity;
import ch.nth.android.contentabo_l01.adapters.VideoDetailsFragmentAdapter;
import ch.nth.android.utils.JavaUtils;
import com.viewpagerindicator.TabPageIndicator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class VideoDetailsMainFragment extends VideoDetailsMainAbstractFragment implements YesNoDialogFragment.YesNoDialogListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor = null;
    private static final int DOUBLE_MO_DIALOG_FRAGMENT_TAG = 596;
    private static final int FLOW_TYPE_UNAVAILABLE_FRAGMENT_TAG = 770;
    private static final int SHOW_DOUBLE_MO_DIALOG_MESSAGE = 1058;
    private static final int SINGLE_MO_DIALOG_FRAGMENT_TAG = 597;
    private static final String TAG = VideoDetailsMainFragment.class.getSimpleName();
    private static final int TERMS_AND_CONDITIONS_FRAGMENT_TAG = 839;
    private TextView mCurrentTimeTextView;
    private Handler mDialogHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == VideoDetailsMainFragment.SHOW_DOUBLE_MO_DIALOG_MESSAGE) {
                VideoDetailsMainFragment.this.showDoubleMoFlowDialogFragment();
            } else {
                super.handleMessage(msg);
            }
        }
    };
    private ViewPager mPager;
    private VideoDetailsFragmentAdapter mPagerAdapter;
    private boolean mShouldUseTermsAndConditions = false;
    private ImageButton mSubscriptionButtonSubscribe;
    private TextView mSubscriptionDisclaimer;
    private ProgressBar mSubscriptionProgressBar;
    private ProgressBar mSubscriptionProgressBarIndeterminate;
    private RelativeLayout mSubscriptionProgressContainer;
    private View mSubscriptionSmsContainer;
    private RelativeLayout mSubscriptionSubscribeContainer;
    private RelativeLayout mSubscriptionTryAgainContainer;
    private TabPageIndicator mTitleIndicator;
    /* access modifiers changed from: private */
    public boolean mUserAcceptedTermsAndConditions = false;
    private FrameLayout mVideoContainer;
    private View mVideoControlsLayout;
    private ViewGroup mViewPagerContainer;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() == R.id.video_details_subscription_button_subscribe || v.getId() == R.id.video_details_subscription_button_renew) {
                PaymentOption.PaymentFlowType flowType = PaymentUtils.getPaymentOption(VideoDetailsMainFragment.this.getActivity()).getPaymentFlowType();
                if (flowType == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN) {
                    if (VideoDetailsMainFragment.this.handleFlowWithDialog()) {
                        VideoDetailsMainFragment.this.showSingleMoFlowDialogFragment();
                    } else {
                        VideoDetailsMainFragment.this.startSubscriptionButtonClick();
                    }
                } else if (flowType != PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
                    YesNoDialogFragment dialog = YesNoDialogFragment.newInstance(VideoDetailsMainFragment.this, VideoDetailsMainFragment.FLOW_TYPE_UNAVAILABLE_FRAGMENT_TAG, R.layout.dialog_flow_type_unavailable, true, VideoDetailsMainFragment.this.getFlowTypeUnavailableTranslations());
                    dialog.setTargetFragment(VideoDetailsMainFragment.this, VideoDetailsMainFragment.FLOW_TYPE_UNAVAILABLE_FRAGMENT_TAG);
                    dialog.show(VideoDetailsMainFragment.this.getFragmentManager(), (String) null);
                } else if (VideoDetailsMainFragment.this.handleFlowWithDialog()) {
                    VideoDetailsMainFragment.this.showDoubleMoFlowDialogFragment();
                } else {
                    VideoDetailsMainFragment.this.startSubscriptionButtonClick();
                }
            }
        }
    };

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor;
        if (iArr == null) {
            iArr = new int[Content.VideoFlavor.values().length];
            try {
                iArr[Content.VideoFlavor.HQ.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Content.VideoFlavor.LQ.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor = iArr;
        }
        return iArr;
    }

    public static VideoDetailsMainFragment newInstance(Bundle args) {
        VideoDetailsMainFragment fragment = new VideoDetailsMainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video_details, container, false);
        this.mPager = (ViewPager) v.findViewById(R.id.pager);
        this.mCurrentTimeTextView = (TextView) v.findViewById(R.id.video_view_progress_text);
        this.mVideoControlsLayout = v.findViewById(R.id.video_controls_layout);
        this.mViewPagerContainer = (ViewGroup) v.findViewById(R.id.viewpager_contener);
        setVideoViewAndAttachTimeoutListener((VideoView) v.findViewById(R.id.video_view));
        ImageButton settingsButton = (ImageButton) v.findViewById(R.id.video_details_settings);
        if (this.mConfigurationButtonEnabled) {
            setVideoQualitySwitchView(settingsButton);
        } else {
            settingsButton.setVisibility(8);
        }
        setPlayPauseSwitchView((ImageButton) v.findViewById(R.id.video_details_play_pause));
        setFullscreenSwitchView((ImageButton) v.findViewById(R.id.video_details_fullscreen));
        setBufferingProgressBar((ProgressBar) v.findViewById(R.id.video_view_buffering_progressbar));
        setPreviewImageView((ImageView) v.findViewById(R.id.video_view_preview_imageview));
        setSeekBar((SeekBar) v.findViewById(R.id.video_view_seek_bar));
        setCurrentTimeTextView(this.mCurrentTimeTextView);
        this.mPagerAdapter = new VideoDetailsFragmentAdapter(getChildFragmentManager(), getActivity(), this.mContentId, this.mContent, this.mCommentType, this.mRelatedVideosEnabled);
        this.mPager.setOffscreenPageLimit(2);
        this.mPager.setAdapter(this.mPagerAdapter);
        this.mTitleIndicator = (TabPageIndicator) v.findViewById(R.id.titles);
        this.mTitleIndicator.setViewPager(this.mPager);
        this.mShouldUseTermsAndConditions = termsAndConditionsActive();
        initSubscriptionViews(v, inflater);
        return v;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupMasLayout(MasConfigFactory.getVideoDetailsFloatingMasConfig(), (ViewGroup) view.findViewById(R.id.mas_layout_container));
        onSubscriptionNextStage();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchContent();
    }

    public void onContentFetched(Content content) {
        loadPreviewImage(content.getPictureUrl(Content.PictureFlavor.FULL_PICTURE, Content.PictureSelectMode.FIRST));
    }

    public void onContentError(String message) {
        showToastSafe(Translations.getPolyglot().getString(T.string.error_fetching_video_details));
    }

    public void onNetworkFetchStarted() {
    }

    private void initSubscriptionViews(View parent, LayoutInflater inflater) {
        this.mVideoContainer = (FrameLayout) parent.findViewById(R.id.video_contener);
        this.mSubscriptionDisclaimer = (TextView) parent.findViewById(R.id.video_details_subscription_disclaimer);
        this.mSubscriptionSmsContainer = inflater.inflate(R.layout.fragment_sms, (ViewGroup) null);
        ((TextView) this.mSubscriptionSmsContainer.findViewById(R.id.video_status)).setText(Translations.getPolyglot().getString(T.string.video_details_subscription_video_status));
        ((TextView) this.mSubscriptionSmsContainer.findViewById(R.id.video_status_try_again)).setText(Translations.getPolyglot().getString(T.string.video_details_subscription_try_again));
        ((TextView) this.mSubscriptionSmsContainer.findViewById(R.id.video_details_subscription_start_video)).setText(Translations.getPolyglot().getString(T.string.video_details_subscription_start_video));
        this.mSubscriptionSubscribeContainer = (RelativeLayout) this.mSubscriptionSmsContainer.findViewById(R.id.video_details_subscription_button_container);
        this.mSubscriptionProgressContainer = (RelativeLayout) this.mSubscriptionSmsContainer.findViewById(R.id.subscription_status_container_progress);
        this.mSubscriptionTryAgainContainer = (RelativeLayout) this.mSubscriptionSmsContainer.findViewById(R.id.subscription_status_container_try_again);
        this.mSubscriptionButtonSubscribe = (ImageButton) this.mSubscriptionSmsContainer.findViewById(R.id.video_details_subscription_button_subscribe);
        this.mSubscriptionProgressBar = (ProgressBar) this.mSubscriptionSmsContainer.findViewById(R.id.progress_bar);
        this.mSubscriptionProgressBarIndeterminate = (ProgressBar) this.mSubscriptionSmsContainer.findViewById(R.id.progress_bar_indeterminate);
        this.mSubscriptionProgressContainer.setVisibility(4);
        this.mSubscriptionTryAgainContainer.setVisibility(4);
        this.mVideoContainer.addView(this.mSubscriptionSmsContainer);
        this.mSubscriptionButtonSubscribe.setOnClickListener(this.onClickListener);
        ((ImageButton) this.mSubscriptionSmsContainer.findViewById(R.id.video_details_subscription_button_renew)).setOnClickListener(this.onClickListener);
        this.mSubscriptionSmsContainer.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        String disclaimerText = Disclaimers.getPolyglot().getString(D.string.video_details_disclaimer);
        String startInfoText = Disclaimers.getPolyglot().getString(D.string.video_details_button);
        this.mSubscriptionDisclaimer.setText(disclaimerText);
        ((TextView) this.mSubscriptionSmsContainer.findViewById(R.id.video_details_subscription_sms_info)).setText(startInfoText);
        this.mSubscriptionDisclaimer.setVisibility(8);
        this.mSubscriptionSmsContainer.setVisibility(4);
    }

    public void onDialogPositiveButtonClick(int alertDialogTag) {
        Activity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    public void onDialogNegativeButtonClick(int alertDialogTag) {
    }

    /* access modifiers changed from: private */
    public boolean handleFlowWithDialog() {
        if (PaymentUtils.getPaymentOption(getActivity()).getPaymentFlowType() == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN && TextUtils.isEmpty(PaymentUtils.getPaymentOption(getActivity()).getBodyFromConfirmationUrl())) {
            return false;
        }
        String mccMnc = PaymentUtils.getMCCMNCTuple(getActivity());
        String dialogAffectedNwc = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getSubscriptionDialogNwc();
        if (TextUtils.isEmpty(dialogAffectedNwc) || !JavaUtils.regexMatch(dialogAffectedNwc, mccMnc)) {
            String dialogAffectedNwc1 = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getSubscriptionDialogNwc1();
            if (TextUtils.isEmpty(dialogAffectedNwc1) || !JavaUtils.regexMatch(dialogAffectedNwc1, mccMnc)) {
                String dialogAffectedNwc2 = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getSubscriptionDialogNwc2();
                if (TextUtils.isEmpty(dialogAffectedNwc2) || !JavaUtils.regexMatch(dialogAffectedNwc2, mccMnc)) {
                    Log.d(TAG, "mccmnc does not match the patterns - mccmnc=" + mccMnc);
                    return false;
                }
                Log.d(TAG, "mccmnc matches the pattern - pattern=" + dialogAffectedNwc2);
                return true;
            }
            Log.d(TAG, "mccmnc matches the pattern - pattern=" + dialogAffectedNwc1);
            return true;
        }
        Log.d(TAG, "mccmnc matches the pattern - pattern=" + dialogAffectedNwc);
        return true;
    }

    /* access modifiers changed from: private */
    public void showSingleMoFlowDialogFragment() {
        SingleMOFlowDialogFragment fragment = SingleMOFlowDialogFragment.newInstance(new YesNoDialogFragment.YesNoDialogListener() {
            public void onDialogPositiveButtonClick(int alertDialogTag) {
                VideoDetailsMainFragment.this.startSubscriptionButtonClick();
            }

            public void onDialogNegativeButtonClick(int alertDialogTag) {
                if (VideoDetailsMainFragment.this.getActivity() != null) {
                    VideoDetailsMainFragment.this.getActivity().finish();
                }
            }
        }, SINGLE_MO_DIALOG_FRAGMENT_TAG, R.layout.dialog_single_mo_flow, true, getSingleMoFlowTranslations());
        try {
            fragment.show(getFragmentManager(), fragment.getTag());
        } catch (Exception e) {
            Log.d(TAG, "error while trying to display the dialog", e);
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"UseSparseArrays"})
    public HashMap<Integer, String> getSingleMoFlowTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Disclaimers.getPolyglot().getString(D.string.optin_dialog_title));
        translations.put(Integer.valueOf(R.id.btn_dialog_negative), Disclaimers.getPolyglot().getString("no"));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Disclaimers.getPolyglot().getString("yes"));
        translations.put(Integer.valueOf(R.id.text_header_dialog_custom), Disclaimers.getPolyglot().getString(D.string.video_details_confirm_dialog_header));
        translations.put(Integer.valueOf(R.id.text_footer_dialog_custom), Disclaimers.getPolyglot().getString(D.string.video_details_confirm_dialog_footer));
        translations.put(Integer.valueOf(R.id.webview), Disclaimers.getPolyglot().getString(D.string.ro_web_view_flow));
        return translations;
    }

    /* access modifiers changed from: private */
    public void showDoubleMoFlowDialogFragment() {
        DoubleMOFlowDialogFragment fragment = DoubleMOFlowDialogFragment.newInstance(new DoubleMOFlowDialogFragment.DoubleMoFlowDialogListener() {
            public void onDoubleMoFlowDialogPlayButtonClick(int alertDialogTag) {
                VideoDetailsMainFragment.this.startSubscriptionButtonClick();
            }

            public void onDoubleMoFlowDialogDismissed(int alertDialogTag) {
                Activity activity = VideoDetailsMainFragment.this.getActivity();
                if (activity != null) {
                    activity.finish();
                }
            }
        }, DOUBLE_MO_DIALOG_FRAGMENT_TAG, R.layout.dialog_double_mo_flow, PaymentUtils.getPaymentOption(getActivity()).getBodyFromConfirmationUrl(), getDoubleMoFlowTranslations());
        try {
            fragment.show(getFragmentManager(), fragment.getTag());
        } catch (Exception e) {
            Log.d(TAG, "error while trying to display the dialog", e);
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"UseSparseArrays"})
    public HashMap<Integer, String> getDoubleMoFlowTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Disclaimers.getPolyglot().getString(D.string.optin_dialog_title));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Disclaimers.getPolyglot().getString(D.string.play));
        translations.put(Integer.valueOf(R.id.text_header_dialog_custom), Disclaimers.getPolyglot().getString(D.string.video_details_confirm_dialog_header));
        translations.put(Integer.valueOf(R.id.text_footer_dialog_custom), Disclaimers.getPolyglot().getString(D.string.video_details_confirm_dialog_footer));
        translations.put(Integer.valueOf(R.id.edittext_dialog_custom), Disclaimers.getPolyglot().getString(D.string.video_details_confirm_edittext_error));
        return translations;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"UseSparseArrays"})
    public HashMap<Integer, String> getFlowTypeUnavailableTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.temporarily_disabled_label_title));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.temporarily_disabled_message_text));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString(T.string.temporarily_disabled_button_ok));
        return translations;
    }

    /* access modifiers changed from: protected */
    public List<View> getViewsToToggleOnFullscreen() {
        return new ArrayList(Arrays.asList(this.mViewPagerContainer));
    }

    /* access modifiers changed from: protected */
    public List<View> getViewsToToggleOnVideoClickInactive() {
        return new ArrayList(Arrays.asList(this.mVideoControlsLayout, this.mSeekBar, this.mCurrentTimeTextView));
    }

    /* access modifiers changed from: protected */
    public void onVideoFlavorChanged(Content.VideoFlavor flavor) {
        if (this.mChangeQualityImageButton != null) {
            switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor()[flavor.ordinal()]) {
                case 1:
                    this.mChangeQualityImageButton.setImageResource(R.drawable.btn_settings_lq_selector);
                    return;
                case 2:
                    this.mChangeQualityImageButton.setImageResource(R.drawable.btn_settings_hq_selector);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPlayPauseUpdateNeeded(boolean isPlaying) {
        if (isPlaying) {
            this.mPlayPauseImageButton.setImageResource(R.drawable.btn_pause_selector);
        } else {
            this.mPlayPauseImageButton.setImageResource(R.drawable.btn_play_selector);
        }
    }

    /* access modifiers changed from: protected */
    public void setVideoControlsEnabled(boolean enabled) {
        this.mPlayPauseImageButton.setEnabled(enabled);
        this.mFullscreenImageButton.setEnabled(enabled);
        this.mChangeQualityImageButton.setEnabled(enabled);
        if (enabled) {
            this.mPlayPauseImageButton.setImageResource(R.drawable.btn_play_selector);
            this.mFullscreenImageButton.setImageResource(R.drawable.btn_fs_selector);
            onVideoFlavorChanged(getVideoFlavor());
            return;
        }
        this.mPlayPauseImageButton.setImageResource(R.drawable.btn_play_disabled_selector);
        this.mFullscreenImageButton.setImageResource(R.drawable.btn_fs_disabled_selector);
        this.mChangeQualityImageButton.setImageResource(R.drawable.btn_settings_disabled_selector);
    }

    public void updateTitle() {
    }

    public void onUserNotSubscribed() {
        if (this.mSubscriptionDisclaimer != null) {
            this.mSubscriptionDisclaimer.setVisibility(0);
        }
        if (this.mSubscriptionSmsContainer != null) {
            this.mSubscriptionSmsContainer.setVisibility(0);
            if (PaymentUtils.getPaymentOption(App.getInstance()).getPaymentFlowType() == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN && PaymentUtils.getPaymentFlowStage() == PaymentUtils.PaymentFlowStage.ANGEBOT_MESSAGE_RECIVED && handleFlowWithDialog()) {
                this.mDialogHandler.sendEmptyMessage(SHOW_DOUBLE_MO_DIALOG_MESSAGE);
            }
        }
        Utils.doLog("Payment", "shouldShowProgress() " + shouldShowProgress());
        Utils.doLog("Payment", "shouldShowStartButton() " + shouldShowStartButton());
        Utils.doLog("Payment", "shouldShowError() " + shouldShowError());
        if (shouldShowProgress()) {
            showProgressView();
            startProgress();
        } else if (shouldShowStartButton()) {
            showStartButtonView();
        } else if (shouldShowError()) {
            showErrorView();
        }
        setVideoControlsEnabled(false);
        setViewPagerEnabled(false);
        this.mPagerAdapter.onUserNotSubscribed();
        this.mPagerAdapter.notifyDataSetChanged();
    }

    public void onUserSubscribed() {
        if (this.mSubscriptionDisclaimer != null) {
            this.mSubscriptionDisclaimer.setVisibility(8);
        }
        if (!(this.mVideoContainer == null || this.mSubscriptionSmsContainer == null)) {
            this.mVideoContainer.removeView(this.mSubscriptionSmsContainer);
        }
        setVideoControlsEnabled(true);
        setViewPagerEnabled(true);
        this.mPagerAdapter.onUserSubscribed();
        this.mPagerAdapter.notifyDataSetChanged();
        showVideoControlsAndScheduleHide();
        startVideo();
    }

    private void setViewPagerEnabled(boolean enabled) {
        if (enabled) {
            this.mTitleIndicator.setVisibility(0);
        } else {
            this.mTitleIndicator.setVisibility(8);
        }
    }

    private void showProgressView() {
        initSubscriptionProgressBar();
        this.mSubscriptionSubscribeContainer.setVisibility(8);
        this.mSubscriptionTryAgainContainer.setVisibility(8);
        this.mSubscriptionProgressContainer.setVisibility(0);
        this.mSubscriptionProgressBarIndeterminate.setVisibility(0);
    }

    private void showStartButtonView() {
        this.mSubscriptionSubscribeContainer.setVisibility(0);
        this.mSubscriptionTryAgainContainer.setVisibility(8);
        this.mSubscriptionProgressContainer.setVisibility(8);
        this.mSubscriptionProgressBarIndeterminate.setVisibility(8);
    }

    private void showErrorView() {
        initSubscriptionProgressBar();
        this.mSubscriptionSubscribeContainer.setVisibility(8);
        this.mSubscriptionProgressContainer.setVisibility(8);
        this.mSubscriptionProgressBarIndeterminate.setVisibility(8);
        this.mSubscriptionTryAgainContainer.setVisibility(0);
    }

    private boolean termsAndConditionsActive() {
        String termsNwc = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getOptinFlowTremsNwc();
        String mccMnc = PaymentUtils.getMCCMNCTuple(getActivity());
        if (TextUtils.isEmpty(termsNwc) || !JavaUtils.regexMatch(termsNwc, mccMnc)) {
            Log.d(TAG, "T&C DISABLED > mccmnc does not match the pattern=" + termsNwc);
            return false;
        }
        Log.d(TAG, "T&C ENABLED > mccmnc matches the pattern=" + termsNwc);
        return true;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"UseSparseArrays"})
    public HashMap<Integer, String> getTermsAndConditionsTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.app_name));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString(T.string.proceed_btn));
        translations.put(Integer.valueOf(R.id.video_details_terms_and_conditions), Disclaimers.getPolyglot().getString(D.string.termsco));
        return translations;
    }

    private void initSubscriptionProgressBar() {
        TextView videoName = (TextView) getView().findViewById(R.id.video_name);
        TextView videoPrice = (TextView) getView().findViewById(R.id.video_lenght);
        TextView videoNameTryAgain = (TextView) getView().findViewById(R.id.video_name_try_again);
        TextView videoPriceTryAgain = (TextView) getView().findViewById(R.id.video_lenght_try_again);
        if (this.mContent != null) {
            String name = String.format(Translations.getPolyglot().getString(T.string.video_details_subscription_video_name), this.mContent.getName());
            String price = String.format(Translations.getPolyglot().getString(T.string.video_details_subscription_video_name), FormatUtils.formatContentDuration(this.mContent.getDuration()));
            videoName.setText(name);
            videoNameTryAgain.setText(name);
            videoPrice.setText(price);
            videoPriceTryAgain.setText(price);
        }
    }

    /* access modifiers changed from: private */
    public void startSubscriptionButtonClick() {
        if (this.mShouldUseTermsAndConditions) {
            if (!this.mUserAcceptedTermsAndConditions) {
                Log.d(TAG, "user didn't accept T&C yet, showing dialog...");
                TermsAndConditionsDialogFragment dialog = TermsAndConditionsDialogFragment.newInstance(new YesNoDialogFragment.YesNoDialogListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ch.nth.android.contentabo_l01.fragments.VideoDetailsMainFragment.access$5(ch.nth.android.contentabo_l01.fragments.VideoDetailsMainFragment, boolean):void
                     arg types: [ch.nth.android.contentabo_l01.fragments.VideoDetailsMainFragment, int]
                     candidates:
                      ch.nth.android.contentabo.fragments.VideoDetailsMainAbstractFragment.access$5(ch.nth.android.contentabo.fragments.VideoDetailsMainAbstractFragment, boolean):void
                      ch.nth.android.contentabo_l01.fragments.VideoDetailsMainFragment.access$5(ch.nth.android.contentabo_l01.fragments.VideoDetailsMainFragment, boolean):void */
                    public void onDialogPositiveButtonClick(int alertDialogTag) {
                        VideoDetailsMainFragment.this.mUserAcceptedTermsAndConditions = true;
                        VideoDetailsMainFragment.this.startSubscriptionButtonClick();
                    }

                    public void onDialogNegativeButtonClick(int alertDialogTag) {
                    }
                }, TERMS_AND_CONDITIONS_FRAGMENT_TAG, R.layout.dialog_accept_terms, true, getTermsAndConditionsTranslations());
                dialog.setNotifyDismissAsNegative(true);
                try {
                    dialog.show(getFragmentManager(), dialog.getTag());
                    return;
                } catch (Exception e) {
                    return;
                }
            } else {
                Log.d(TAG, "user accepted T&C, continuing flow...");
            }
        }
        String name = String.format(Translations.getPolyglot().getString(T.string.video_details_subscription_video_name), this.mContent.getName());
        String price = String.format(Translations.getPolyglot().getString(T.string.video_details_subscription_video_name), FormatUtils.formatContentDuration(this.mContent.getDuration()));
        ((TextView) getView().findViewById(R.id.video_name)).setText(name);
        ((TextView) getView().findViewById(R.id.video_name_try_again)).setText(name);
        ((TextView) getView().findViewById(R.id.video_lenght)).setText(price);
        ((TextView) getView().findViewById(R.id.video_lenght_try_again)).setText(price);
        this.mSubscriptionSubscribeContainer.setVisibility(8);
        this.mSubscriptionTryAgainContainer.setVisibility(4);
        this.mSubscriptionProgressContainer.setVisibility(0);
        this.mSubscriptionProgressBarIndeterminate.setVisibility(0);
        userConfirmsNextStep();
    }

    /* access modifiers changed from: protected */
    public void onUpdateProgress(int progress) {
        this.mSubscriptionProgressBar.setProgress(progress);
    }

    /* access modifiers changed from: protected */
    public void onSubscriptionNextStage() {
        if (isSubscribed()) {
            onUserSubscribed();
        } else {
            onUserNotSubscribed();
        }
    }

    /* access modifiers changed from: protected */
    public Class<?> getWebViewActivity() {
        return PaymentWebViewActivity.class;
    }
}
