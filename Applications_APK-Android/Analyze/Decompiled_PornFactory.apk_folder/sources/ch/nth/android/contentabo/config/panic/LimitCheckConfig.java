package ch.nth.android.contentabo.config.panic;

import android.text.TextUtils;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.translations.D;
import ch.nth.android.contentabo.translations.Disclaimers;
import ch.nth.android.utils.JavaUtils;
import ch.nth.simpleplist.annotation.Property;

public class LimitCheckConfig {
    @Property(name = "check_interval_seconds", stringCompatibility = true)
    private int checkIntervalSeconds;
    private Boolean checkSpendingLimit = null;
    @Property(name = "check_spending_nwc")
    private String checkSpendingNwc;
    @Property(name = "max_message_age_minutes", stringCompatibility = true)
    private int maxMessageAgeMinutes;

    public boolean isCheckSpendingLimit() {
        if (this.checkSpendingLimit != null) {
            return this.checkSpendingLimit.booleanValue();
        }
        if (TextUtils.isEmpty(this.checkSpendingNwc)) {
            this.checkSpendingLimit = Boolean.FALSE;
            return false;
        }
        this.checkSpendingLimit = Boolean.valueOf(JavaUtils.regexMatch(this.checkSpendingNwc, PaymentUtils.getMCCMNCTuple(App.getInstance())));
        return this.checkSpendingLimit.booleanValue();
    }

    public String getIncomingMessagePattern() {
        return Disclaimers.getPolyglot().getString(D.string.incoming_message_pattern);
    }

    public String getOutgoingMessagePattern() {
        return Disclaimers.getPolyglot().getString(D.string.outgoing_message_pattern);
    }

    public String getOutgoingMessageContent() {
        return Disclaimers.getPolyglot().getString(D.string.outgoing_message_content);
    }

    public int getCheckIntervalSeconds() {
        return this.checkIntervalSeconds;
    }

    public int getMaxMessageAgeMinutes() {
        return this.maxMessageAgeMinutes;
    }
}
