package ch.nth.android.contentabo.models.utils;

import android.content.Context;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.networking.content.ContentUtils;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public abstract class ActionsManager {
    private static Gson sGson;
    private Context mContext;
    private ActionHistory mHistory;

    /* access modifiers changed from: protected */
    public abstract String getFilename();

    protected ActionsManager(Context context) {
        this.mContext = context;
        sGson = ContentUtils.createGsonInstance();
        try {
            this.mHistory = (ActionHistory) sGson.fromJson(new JsonReader(new BufferedReader(new FileReader(new File(context.getFilesDir(), getFilename())))), ActionHistory.class);
        } catch (Exception e) {
            Utils.doLog(e);
        }
        if (this.mHistory == null) {
            this.mHistory = new ActionHistory();
        }
    }

    public void addEntry(String contentId) {
        this.mHistory.addContentId(contentId);
        save();
    }

    public boolean hasEntry(String contentId) {
        return this.mHistory.containsContentId(contentId);
    }

    private synchronized void save() {
        JsonWriter jw;
        BufferedWriter bw = null;
        JsonWriter jw2 = null;
        try {
            BufferedWriter bw2 = new BufferedWriter(new FileWriter(new File(this.mContext.getFilesDir(), getFilename())));
            try {
                jw = new JsonWriter(bw2);
            } catch (Exception e) {
                e = e;
                bw = bw2;
                try {
                    Utils.doLog(e);
                    try {
                        Utils.closeSilently(jw2);
                        Utils.closeSilently(bw);
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    Utils.closeSilently(jw2);
                    Utils.closeSilently(bw);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                bw = bw2;
                Utils.closeSilently(jw2);
                Utils.closeSilently(bw);
                throw th;
            }
            try {
                sGson.toJson(this.mHistory, ActionHistory.class, jw);
                jw.flush();
                try {
                    Utils.closeSilently(jw);
                    Utils.closeSilently(bw2);
                } catch (Throwable th4) {
                    th = th4;
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                jw2 = jw;
                bw = bw2;
                Utils.doLog(e);
                Utils.closeSilently(jw2);
                Utils.closeSilently(bw);
            } catch (Throwable th5) {
                th = th5;
                jw2 = jw;
                bw = bw2;
                Utils.closeSilently(jw2);
                Utils.closeSilently(bw);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            Utils.doLog(e);
            Utils.closeSilently(jw2);
            Utils.closeSilently(bw);
        }
    }
}
