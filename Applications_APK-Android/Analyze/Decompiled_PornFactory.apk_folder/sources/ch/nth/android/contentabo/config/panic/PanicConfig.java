package ch.nth.android.contentabo.config.panic;

import android.text.TextUtils;
import ch.nth.android.contentabo.translations.D;
import ch.nth.simpleplist.annotation.Property;

public class PanicConfig {
    @Property(name = "angebot_check_delay_seconds", required = false, stringCompatibility = true)
    private int angebotCheckDelaySeconds = 12;
    @Property(name = "angebot_message_pattern", required = false)
    private String angebotMessagePattern;
    @Property(name = "angebot_message_validity_seconds", required = false, stringCompatibility = true)
    private int angebotMessageValiditySeconds = 1200;
    @Property(name = D.string.incoming_message_pattern)
    private String incomingMessagePattern;

    public String getIncomingMessagePattern() {
        if (TextUtils.isEmpty(this.incomingMessagePattern)) {
            return "";
        }
        return this.incomingMessagePattern;
    }

    public String getAngebotMessagePattern() {
        if (TextUtils.isEmpty(this.angebotMessagePattern)) {
            return getIncomingMessagePattern();
        }
        return this.angebotMessagePattern;
    }

    public int getAngebotCheckDelaySeconds() {
        return this.angebotCheckDelaySeconds;
    }

    public int getAngebotMessageValiditySeconds() {
        return this.angebotMessageValiditySeconds;
    }
}
