package ch.nth.android.contentabo.fragments;

import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;
import ch.nth.android.contentabo.common.utils.FormatUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.PreferenceConnector;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.PlistConfig;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.networking.content.GsonRequest;
import ch.nth.android.contentabo.networking.content.RequestUtils;
import com.android.volley.Request;
import com.squareup.picasso.Picasso;
import java.util.List;

public abstract class VideoDetailsMainAbstractFragment extends VideoDetailsAbstractFragment {
    private static final int HANDLER_HIDE = 0;
    private static final int HANDLER_UPDATE_PROGRESS = 1;
    private static final String KEY_SELECTED_VIDEO_FLAVOR = "user_selected_video_flavor";
    private static final int VIDEO_CONTROLS_HIDE_DEFAULT_TIMEOUT = 3500;
    protected ProgressBar mBufferingProgressBar;
    protected ImageButton mChangeQualityImageButton;
    protected TextView mCurrentTimeTextView;
    /* access modifiers changed from: private */
    public boolean mDragging;
    protected ImageButton mFullscreenImageButton;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    VideoDetailsMainAbstractFragment.this.updateViewsVisibility(VideoDetailsMainAbstractFragment.this.getViewsToToggleOnVideoClickInactive(), 8);
                    return;
                case 1:
                    int pos = VideoDetailsMainAbstractFragment.this.updateVideoProgress();
                    if (!VideoDetailsMainAbstractFragment.this.mDragging && VideoDetailsMainAbstractFragment.this.mVideoView.isPlaying()) {
                        sendMessageDelayed(obtainMessage(1), (long) (1000 - (pos % 1000)));
                    }
                    VideoDetailsMainAbstractFragment.this.onPlayPauseUpdateNeeded(VideoDetailsMainAbstractFragment.this.mVideoView.isPlaying());
                    return;
                default:
                    return;
            }
        }
    };
    protected ImageButton mPlayPauseImageButton;
    protected ImageView mPreviewImageView;
    protected SeekBar mSeekBar;
    /* access modifiers changed from: private */
    public Content.VideoFlavor mVideoFlavor;
    private String mVideoPathSet;
    protected VideoView mVideoView;
    private View.OnTouchListener videoViewTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            VideoDetailsMainAbstractFragment.this.showVideoControlsAndScheduleHide();
            return false;
        }
    };

    /* access modifiers changed from: protected */
    public abstract List<View> getViewsToToggleOnFullscreen();

    /* access modifiers changed from: protected */
    public abstract List<View> getViewsToToggleOnVideoClickInactive();

    public abstract void onContentError(String str);

    public abstract void onContentFetched(Content content);

    /* access modifiers changed from: protected */
    public abstract void onPlayPauseUpdateNeeded(boolean z);

    /* access modifiers changed from: protected */
    public abstract void onVideoFlavorChanged(Content.VideoFlavor videoFlavor);

    /* access modifiers changed from: protected */
    public abstract void setVideoControlsEnabled(boolean z);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mVideoFlavor = Content.VideoFlavor.valueForNameOrNull(PreferenceConnector.readString(getActivity(), KEY_SELECTED_VIDEO_FLAVOR, null));
        if (this.mVideoFlavor == null) {
            try {
                this.mVideoFlavor = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getDefaultVideoQuality();
                if (this.mVideoFlavor == null) {
                    this.mVideoFlavor = Content.VideoFlavor.getDefault();
                }
            } catch (Exception e) {
                Utils.doLogException(e);
                if (this.mVideoFlavor == null) {
                    this.mVideoFlavor = Content.VideoFlavor.getDefault();
                }
            } catch (Throwable th) {
                if (this.mVideoFlavor == null) {
                    this.mVideoFlavor = Content.VideoFlavor.getDefault();
                }
                throw th;
            }
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        incrementContentDownloadCount();
    }

    public void onResume() {
        super.onResume();
        if (PaymentUtils.isSubscribed()) {
            scheduleVideoControlsHide();
        }
        onVideoFlavorChanged(this.mVideoFlavor);
    }

    private void incrementContentDownloadCount() {
        GsonRequest<Content> request = new GsonRequest<>(getActivity(), RequestUtils.getContentIncrementDownloadCountUrl(getActivity(), this.mContentId), Content.class, null, null, null);
        request.setPriority(Request.Priority.LOW);
        addRequest(request);
    }

    /* access modifiers changed from: protected */
    public boolean hasRequiredFields(Content content) {
        if (content == null || content.getUrlLQ() == null || content.getUrlHQ() == null) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void setPlayPauseSwitchView(ImageButton playPauseSwitch) {
        this.mPlayPauseImageButton = playPauseSwitch;
        playPauseSwitch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoDetailsMainAbstractFragment.this.scheduleVideoControlsHide();
                if (VideoDetailsMainAbstractFragment.this.mVideoView.isPlaying()) {
                    VideoDetailsMainAbstractFragment.this.mVideoView.pause();
                } else {
                    VideoDetailsMainAbstractFragment.this.startVideo();
                }
                VideoDetailsMainAbstractFragment.this.updatePausePlayUi();
            }
        });
    }

    /* access modifiers changed from: protected */
    public boolean updateUrlIfNeeded() {
        if (this.mContent != null) {
            String urlToPlay = this.mContent.getVideoUrl(this.mVideoFlavor);
            if (TextUtils.isEmpty(this.mVideoPathSet) || !this.mVideoPathSet.equals(urlToPlay)) {
                this.mVideoPathSet = urlToPlay;
                this.mVideoView.setVideoPath(urlToPlay);
                return true;
            }
        } else {
            fetchContent();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void startVideo() {
        if (updateUrlIfNeeded()) {
            this.mBufferingProgressBar.setVisibility(0);
        }
        this.mVideoView.start();
    }

    /* access modifiers changed from: protected */
    public void setVideoQualitySwitchView(ImageButton videoQualitySwitch) {
        this.mChangeQualityImageButton = videoQualitySwitch;
        videoQualitySwitch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoDetailsMainAbstractFragment.this.scheduleVideoControlsHide();
                Content.VideoFlavor nextFlavor = VideoDetailsMainAbstractFragment.this.mVideoFlavor.getNext();
                if (VideoDetailsMainAbstractFragment.this.mVideoView.isPlaying()) {
                    VideoDetailsMainAbstractFragment.this.setVideoFlavor(nextFlavor);
                    VideoDetailsMainAbstractFragment.this.startVideo();
                } else {
                    VideoDetailsMainAbstractFragment.this.setVideoFlavor(nextFlavor);
                }
                PreferenceConnector.writeString(VideoDetailsMainAbstractFragment.this.getActivity(), VideoDetailsMainAbstractFragment.KEY_SELECTED_VIDEO_FLAVOR, nextFlavor.name());
            }
        });
    }

    /* access modifiers changed from: protected */
    public void setFullscreenSwitchView(ImageButton fullscreenSwitch) {
        this.mFullscreenImageButton = fullscreenSwitch;
        fullscreenSwitch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VideoDetailsMainAbstractFragment.this.scheduleVideoControlsHide();
                VideoDetailsMainAbstractFragment.this.toggleOrientation();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void setBufferingProgressBar(ProgressBar progressBar) {
        this.mBufferingProgressBar = progressBar;
    }

    /* access modifiers changed from: protected */
    public void setPreviewImageView(ImageView imageView) {
        this.mPreviewImageView = imageView;
    }

    /* access modifiers changed from: protected */
    public void loadPreviewImage(String url) {
        Picasso.with(getActivity()).load(url).into(this.mPreviewImageView);
    }

    /* access modifiers changed from: protected */
    public void setSeekBar(SeekBar seekBar) {
        this.mSeekBar = seekBar;
        this.mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
                VideoDetailsMainAbstractFragment.this.mDragging = true;
                VideoDetailsMainAbstractFragment.this.mHandler.removeMessages(0);
                VideoDetailsMainAbstractFragment.this.mHandler.removeMessages(1);
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    long newposition = (((long) progress) * ((long) VideoDetailsMainAbstractFragment.this.mVideoView.getDuration())) / 1000;
                    VideoDetailsMainAbstractFragment.this.mVideoView.seekTo((int) newposition);
                    if (VideoDetailsMainAbstractFragment.this.mCurrentTimeTextView != null) {
                        VideoDetailsMainAbstractFragment.this.mCurrentTimeTextView.setText(FormatUtils.formatContentDuration(((int) newposition) / 1000));
                    }
                }
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                VideoDetailsMainAbstractFragment.this.mDragging = false;
                VideoDetailsMainAbstractFragment.this.scheduleVideoControlsHide();
                VideoDetailsMainAbstractFragment.this.mHandler.sendEmptyMessage(1);
            }
        });
    }

    /* access modifiers changed from: private */
    public void updatePausePlayUi() {
        if (this.mVideoView != null && this.mVideoView != null) {
            onPlayPauseUpdateNeeded(this.mVideoView.isPlaying());
        }
    }

    /* access modifiers changed from: protected */
    public Content.VideoFlavor getVideoFlavor() {
        return this.mVideoFlavor;
    }

    /* access modifiers changed from: protected */
    public void setVideoFlavor(Content.VideoFlavor newFlavor) {
        this.mVideoFlavor = newFlavor;
        onVideoFlavorChanged(this.mVideoFlavor);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 2) {
            updateViewsVisibility(getViewsToToggleOnFullscreen(), 8);
            getActionBar().hide();
            return;
        }
        updateViewsVisibility(getViewsToToggleOnFullscreen(), 0);
        getActionBar().show();
    }

    public void toggleOrientation() {
        if (getActivity() == null) {
            return;
        }
        if (getResources().getConfiguration().orientation == 1) {
            getActivity().setRequestedOrientation(0);
        } else {
            getActivity().setRequestedOrientation(1);
        }
    }

    /* access modifiers changed from: protected */
    public void setVideoViewAndAttachTimeoutListener(VideoView videoView) {
        this.mVideoView = videoView;
        this.mVideoView.setOnTouchListener(this.videoViewTouchListener);
        this.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                VideoDetailsMainAbstractFragment.this.mBufferingProgressBar.setVisibility(8);
                VideoDetailsMainAbstractFragment.this.mPreviewImageView.setVisibility(8);
                VideoDetailsMainAbstractFragment.this.mHandler.sendEmptyMessage(1);
            }
        });
        this.mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                VideoDetailsMainAbstractFragment.this.updatePausePlayUi();
                return false;
            }
        });
        this.mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                VideoDetailsMainAbstractFragment.this.updatePausePlayUi();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void setCurrentTimeTextView(TextView textView) {
        this.mCurrentTimeTextView = textView;
    }

    /* access modifiers changed from: private */
    public void updateViewsVisibility(List<View> views, int visibility) {
        if (views != null) {
            for (View view : views) {
                view.setVisibility(visibility);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void scheduleVideoControlsHide() {
        scheduleVideoControlsHide(getVideoControlsHideTimeout());
    }

    /* access modifiers changed from: protected */
    public void scheduleVideoControlsHide(int timeout) {
        this.mHandler.removeMessages(0);
        this.mHandler.sendEmptyMessageDelayed(0, (long) timeout);
    }

    /* access modifiers changed from: protected */
    public void showVideoControlsAndScheduleHide() {
        scheduleVideoControlsHide(getVideoControlsHideTimeout());
        updateViewsVisibility(getViewsToToggleOnVideoClickInactive(), 0);
    }

    /* access modifiers changed from: private */
    public int updateVideoProgress() {
        if (this.mVideoView == null || this.mDragging) {
            return 0;
        }
        int position = this.mVideoView.getCurrentPosition();
        int duration = this.mVideoView.getDuration();
        if (this.mSeekBar != null) {
            if (duration > 0) {
                this.mSeekBar.setProgress((int) ((1000 * ((long) position)) / ((long) duration)));
            }
            this.mSeekBar.setSecondaryProgress(this.mVideoView.getBufferPercentage() * 10);
        }
        if (this.mCurrentTimeTextView == null) {
            return position;
        }
        this.mCurrentTimeTextView.setText(FormatUtils.formatContentDuration(position / 1000));
        return position;
    }

    /* access modifiers changed from: protected */
    public int getVideoControlsHideTimeout() {
        return VIDEO_CONTROLS_HIDE_DEFAULT_TIMEOUT;
    }

    /* access modifiers changed from: protected */
    public int getDelayAfterSmsDelivered() {
        PlistConfig config = AppConfig.getInstance().getConfig();
        if (config != null) {
            return config.getDelayAfterSmsDelivered();
        }
        return 1000;
    }
}
