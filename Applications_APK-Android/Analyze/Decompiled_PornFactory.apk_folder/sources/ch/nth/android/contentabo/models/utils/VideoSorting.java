package ch.nth.android.contentabo.models.utils;

import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public enum VideoSorting {
    TOP_RATED,
    NEWEST,
    POPULARITY;
    
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$utils$VideoSorting;

    public Comparator<Content> getSortingComparator() {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$utils$VideoSorting()[ordinal()]) {
            case 1:
                return new VideoSortTopRatedComparator();
            case 2:
                return new VideoSortTimeImported();
            case 3:
                return new VideoSortPopularityComparator();
            default:
                return new VideoSortTimeImported();
        }
    }

    public static ArrayList<String> getSortingOptionsAsList() {
        ArrayList<String> allSortItems = new ArrayList<>();
        allSortItems.addAll(Arrays.asList(TOP_RATED.toString(), NEWEST.toString(), POPULARITY.toString()));
        return allSortItems;
    }

    public String getSortingUrl() {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$utils$VideoSorting()[ordinal()]) {
            case 1:
                return App.getInstance().getString(R.string.search_content_pattern_popularity_desc);
            case 2:
                return App.getInstance().getString(R.string.search_content_pattern_time_imported_desc);
            case 3:
                return App.getInstance().getString(R.string.search_content_pattern_download_count_desc);
            default:
                return super.toString();
        }
    }

    public String getPatternUrl() {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$utils$VideoSorting()[ordinal()]) {
            case 1:
                return App.getInstance().getString(R.string.content_by_category_pattern_popularity_desc);
            case 2:
                return App.getInstance().getString(R.string.content_by_category_pattern_time_imported_desc);
            case 3:
                return App.getInstance().getString(R.string.content_by_category_pattern_download_count_desc);
            default:
                return super.toString();
        }
    }

    public String getDefaultPatternUrl() {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$utils$VideoSorting()[ordinal()]) {
            case 1:
                return App.getInstance().getString(R.string.default_content_pattern_popularity_desc);
            case 2:
                return App.getInstance().getString(R.string.default_content_pattern_time_imported_desc);
            case 3:
                return App.getInstance().getString(R.string.default_content_pattern_download_count_desc);
            default:
                return super.toString();
        }
    }

    public String getEmbeddedAssetsFilename() {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$utils$VideoSorting()[ordinal()]) {
            case 1:
                return "top_rated.json";
            case 2:
                return "newest.json";
            case 3:
                return "popularity.json";
            default:
                return "";
        }
    }

    public String toString() {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$utils$VideoSorting()[ordinal()]) {
            case 1:
                return Translations.getPolyglot().getString(T.string.sorting_top_rated);
            case 2:
                return Translations.getPolyglot().getString(T.string.sorting_newest);
            case 3:
                return Translations.getPolyglot().getString(T.string.sorting_popularity);
            default:
                return super.toString();
        }
    }
}
