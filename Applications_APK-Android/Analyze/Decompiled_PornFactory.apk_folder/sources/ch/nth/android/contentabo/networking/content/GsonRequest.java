package ch.nth.android.contentabo.networking.content;

import android.content.Context;
import ch.nth.android.contentabo.networking.content.ContentUtils;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.io.UnsupportedEncodingException;
import java.util.EnumSet;
import java.util.Map;

public class GsonRequest<T> extends Request<T> {
    private static final String PROTOCOL_CHARSET = "utf-8";
    private static final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", PROTOCOL_CHARSET);
    private final Gson gson;
    private final Class<T> mClazz;
    private Context mContext;
    private Map<String, String> mHeaders;
    private final Response.Listener<T> mListener;
    private Request.Priority mPriority;
    private final String mRequestBody;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public GsonRequest(Context context, String url, Class<T> clazz, String jsonRequestBody, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(context, jsonRequestBody != null, url, clazz, jsonRequestBody, listener, errorListener);
    }

    public GsonRequest(Context context, int method, String url, Class<T> clazz, String jsonRequestBody, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        setShouldCache(true);
        this.mContext = context;
        this.mListener = listener;
        this.mRequestBody = jsonRequestBody;
        this.mClazz = clazz;
        setRetryPolicy(new DefaultRetryPolicy(12000, 3, 1.25f));
        this.mHeaders = RequestUtils.createHeaders(this.mContext);
        this.gson = ContentUtils.createGsonInstance(EnumSet.of(ContentUtils.GsonTypeAdapter.DATE));
    }

    public void appendHeaders(Map<String, String> headers) {
        this.mHeaders.putAll(headers);
    }

    public void appendHeader(String key, String value) {
        this.mHeaders.put(key, value);
    }

    public void setHeaders(Map<String, String> headers) {
        this.mHeaders = headers;
    }

    /* access modifiers changed from: protected */
    public void deliverResponse(T response) {
        if (this.mListener != null) {
            this.mListener.onResponse(response);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
     arg types: [java.lang.String, java.lang.Class<T>]
     candidates:
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.Class):T
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(com.google.gson.stream.JsonReader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T */
    /* access modifiers changed from: protected */
    public Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            return Response.success(this.gson.fromJson(new String(response.data, HttpHeaderParser.parseCharset(response.headers)), (Class) this.mClazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e2) {
            return Response.error(new ParseError(e2));
        }
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        return this.mHeaders;
    }

    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    public byte[] getBody() {
        try {
            if (this.mRequestBody == null) {
                return null;
            }
            return this.mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException e) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", this.mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    public Request.Priority getPriority() {
        return this.mPriority == null ? super.getPriority() : this.mPriority;
    }

    public void setPriority(Request.Priority priority) {
        this.mPriority = priority;
    }
}
