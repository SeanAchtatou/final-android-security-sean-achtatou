package ch.nth.android.contentabo.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ProgressBar;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.common.adapter.InfiniteBaseAdapter;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.ConfigFactory;
import ch.nth.android.contentabo.config.content.EmbeddedContentConfig;
import ch.nth.android.contentabo.config.mas.ListviewMasConfig;
import ch.nth.android.contentabo.fragments.base.BaseFragment;
import ch.nth.android.contentabo.models.content.Category;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.content.ContentList;
import ch.nth.android.contentabo.models.task.ContentListTask;
import ch.nth.android.contentabo.models.task.ContentSource;
import ch.nth.android.contentabo.models.utils.ListDisplayMode;
import ch.nth.android.contentabo.models.utils.VideoListPager;
import ch.nth.android.contentabo.models.utils.VideoSorting;
import ch.nth.android.contentabo.networking.content.GsonRequest;
import ch.nth.android.contentabo.networking.content.RequestUtils;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import java.util.ArrayList;
import java.util.List;

public abstract class VideoListPageAbstractFragment extends BaseFragment implements Response.Listener<ContentList>, Response.ErrorListener, InfiniteBaseAdapter.Listener {
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource = null;
    private static final String KEY_INSTANCE_STATE_PAGER = "video_list_fragment_pager";
    protected static final int PAGE_SIZE = 6;
    private static final Object SEARCH_REQUEST_TAG = new Object();
    protected Category mCategory = null;
    protected ContentList mDataList;
    protected EmbeddedContentConfig mEmbeddedContentConfig;
    protected ProgressBar mFooterProgressBar;
    protected IVideoClickListener mListener;
    protected boolean mResetPositionOnDataFetched;
    protected ContentList mSearchDataList;
    protected String mSearchItem = null;
    protected VideoSorting mSorting;
    protected boolean mWaitingSearchResults;
    protected VideoListPager pager;
    Response.Listener<ContentList> searchListener = new Response.Listener<ContentList>() {
        public void onResponse(ContentList data) {
            VideoListPageAbstractFragment.this.onSearchFetched(data);
        }
    };
    private ContentListTask.ErrorListener taskErrorListener = new ContentListTask.ErrorListener() {
        public void onError() {
            Utils.doLog("ContentListTask", "no local content data - " + VideoListPageAbstractFragment.this.mSorting);
            VideoListPageAbstractFragment.this.fetchDefaultContent(ContentSource.REMOTE);
        }
    };
    private ContentListTask.Listener taskListener = new ContentListTask.Listener() {
        public void onSuccess(ContentList data) {
            Utils.doLog("ContentListTask", "delivered content data - " + VideoListPageAbstractFragment.this.mSorting);
            VideoListPageAbstractFragment.this.onContentFetched(data);
        }
    };

    public interface IVideoClickListener {
        void onVideoItemClicked(Content content);
    }

    /* access modifiers changed from: protected */
    public abstract void onContentError();

    /* access modifiers changed from: protected */
    public abstract void setData(ContentList contentList, boolean z);

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource;
        if (iArr == null) {
            iArr = new int[ContentSource.values().length];
            try {
                iArr[ContentSource.CACHE_ASSETS_REMOTE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ContentSource.CACHE_REMOTE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ContentSource.REMOTE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource = iArr;
        }
        return iArr;
    }

    public VideoSorting getSorting() {
        return this.mSorting;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (IVideoClickListener) activity;
        } catch (ClassCastException e) {
            Utils.doLogException(e);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mEmbeddedContentConfig = ConfigFactory.getEmbeddedContentConfigOrDefault();
        if (savedInstanceState != null) {
            this.pager = (VideoListPager) savedInstanceState.getSerializable(KEY_INSTANCE_STATE_PAGER);
            return;
        }
        this.pager = new VideoListPager();
        fetchDefaultContent();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_INSTANCE_STATE_PAGER, this.pager);
    }

    public void changeCategory(Category category) {
        this.mDataList = null;
        this.mSearchDataList = null;
        this.mCategory = category;
        this.mResetPositionOnDataFetched = true;
        this.pager = new VideoListPager();
        fetchNetworkContentByCategoryId(0, 6);
    }

    public void doLoadNextPage() {
        if (this.pager.getDisplayMode() == ListDisplayMode.CONTENT) {
            doLoadNextPage(this.pager.getPageNumber());
        } else if (this.pager.getDisplayMode() == ListDisplayMode.SEARCH) {
            doSearchNextPage(this.pager.getPageNumber());
        }
    }

    public void doLoadNextPage(int pageNumber) {
        if (this.mCategory != null) {
            fetchNetworkContentByCategoryId(pageNumber, 6);
        }
    }

    public void doSearch(String searchItem) {
        this.mWaitingSearchResults = true;
        setData(new ContentList(0, new ArrayList()), true);
        this.pager.clearSearch();
        this.pager.setDisplayMode(ListDisplayMode.SEARCH);
        this.mSearchItem = searchItem;
        this.mResetPositionOnDataFetched = true;
        searchNetworkContent(searchItem, 0, 6);
    }

    public void doSearchNextPage(int pageNumber) {
        searchNetworkContent(this.mSearchItem, pageNumber, 6);
    }

    public void closeSearch() {
        getRequestQueue().cancelAll(SEARCH_REQUEST_TAG);
        this.mSearchItem = null;
        this.mSearchDataList = null;
        this.pager.clearSearch();
        this.pager.changeDisplayMode();
        if (this.mDataList != null) {
            onContentFetched(this.mDataList, true);
        } else {
            this.pager = new VideoListPager();
        }
    }

    /* access modifiers changed from: protected */
    public void fetchDefaultContent() {
        if (this.mEmbeddedContentConfig == null || !this.mEmbeddedContentConfig.isEnabled()) {
            fetchDefaultContent(ContentSource.REMOTE);
        } else if (App.getInstance().getVersionCode() >= this.mEmbeddedContentConfig.getAppVersionRequired()) {
            fetchDefaultContent(ContentSource.CACHE_ASSETS_REMOTE);
        } else {
            fetchDefaultContent(ContentSource.CACHE_REMOTE);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public void fetchDefaultContent(ContentSource contentSource) {
        String requestUrl = RequestUtils.getDefaultContent(getActivity(), 0, 6, this.mSorting);
        boolean tryAssets = false;
        Utils.doLog("ContentListTask", "requested data - " + contentSource + " - " + this.mSorting);
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource()[contentSource.ordinal()]) {
            case 1:
                tryAssets = true;
                break;
            case 2:
                break;
            case 3:
                addRequest(new GsonRequest<>(getActivity(), requestUrl, ContentList.class, null, this, this));
                return;
            default:
                return;
        }
        new ContentListTask(getActivity(), requestUrl, this.mSorting, tryAssets, this.taskListener, this.taskErrorListener).executeAsync();
        GsonRequest<ContentList> requestForCache = new GsonRequest<>(getActivity(), requestUrl, ContentList.class, null, null, null);
        requestForCache.setPriority(Request.Priority.LOW);
        addRequest(requestForCache);
    }

    /* access modifiers changed from: protected */
    public void fetchNetworkContentByCategoryId(int pageNumber, int pageSize) {
        addRequest(new GsonRequest<>(getActivity(), RequestUtils.getContentByCategoryUrl(getActivity(), this.mCategory.getId(), pageNumber, pageSize, this.mSorting), ContentList.class, null, this, this));
    }

    /* access modifiers changed from: protected */
    public void searchNetworkContent(String searchItem, int pageNumber, int pageSize) {
        GsonRequest<ContentList> request = new GsonRequest<>(getActivity(), RequestUtils.getSearchUrl(getActivity(), searchItem, pageNumber, pageSize, this.mSorting), ContentList.class, null, this.searchListener, this);
        request.setTag(SEARCH_REQUEST_TAG);
        addRequest(request);
    }

    /* access modifiers changed from: protected */
    public void onSearchFetched(ContentList searchDataList) {
        if (this.pager.getPageNumber() <= 0) {
            this.mSearchDataList = searchDataList;
        } else if (this.mSearchDataList == null) {
            this.mSearchDataList = searchDataList;
        } else {
            this.mSearchDataList.getData().addAll(searchDataList.getData());
        }
        this.pager.success();
        setData(this.mSearchDataList, false);
    }

    /* access modifiers changed from: protected */
    public void onContentFetched(ContentList dataList) {
        onContentFetched(dataList, false);
    }

    /* access modifiers changed from: protected */
    public void onContentFetched(ContentList dataList, boolean replaceData) {
        if (this.pager.getPageNumber() <= 0) {
            if (this.mDataList == null) {
                this.mDataList = dataList;
            }
            List<Content> listToAdd = new ArrayList<>(dataList.getData());
            this.mDataList.getData().clear();
            this.mDataList.getData().addAll(listToAdd);
        } else if (this.mDataList == null) {
            this.mDataList = dataList;
        } else {
            List<Content> listToAdd2 = new ArrayList<>(dataList.getData());
            if (replaceData) {
                this.mDataList.getData().clear();
            }
            this.mDataList.getData().addAll(listToAdd2);
        }
        this.pager.success();
        setData(this.mDataList, false);
    }

    public void onResponse(ContentList contentList) {
        onContentFetched(contentList);
    }

    public void onErrorResponse(VolleyError error) {
        onContentError();
        showToastSafe(Translations.getPolyglot().getString(T.string.error_has_occurred));
    }

    /* access modifiers changed from: protected */
    public ListviewMasConfig getListviewMasConfig() {
        try {
            ListviewMasConfig masConfig = AppConfig.getInstance().getConfig().getModules().getVideoListModule().getListMasConfig();
            if (masConfig == null) {
                return ListviewMasConfig.newDefaultInstance();
            }
            return masConfig;
        } catch (Exception e) {
            Utils.doLogException(e);
            if (0 == 0) {
                return ListviewMasConfig.newDefaultInstance();
            }
            return null;
        } catch (Throwable th) {
            if (0 == 0) {
                ListviewMasConfig masConfig2 = ListviewMasConfig.newDefaultInstance();
            }
            throw th;
        }
    }
}
