package ch.nth.android.contentabo.models.content;

import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class Category implements Serializable {
    private static final long serialVersionUID = -8333897428591939181L;
    private int count;
    @SerializedName(DmsConstants.ID)
    private String id;
    private String name;
    @SerializedName("picture")
    private String urlPicture;

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getUrlPicture() {
        return this.urlPicture;
    }

    public int getCount() {
        return this.count;
    }
}
