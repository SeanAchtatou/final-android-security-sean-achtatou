package ch.nth.android.contentabo.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.modules.VideoDetailsModule;
import ch.nth.android.contentabo.fragments.base.BaseDialogFragment;
import ch.nth.android.contentabo.models.content.Comment;
import ch.nth.android.contentabo.models.content.CommentList;
import ch.nth.android.contentabo.networking.content.GsonRequest;
import ch.nth.android.contentabo.networking.content.RequestUtils;
import ch.nth.android.contentabo.translations.T;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import java.io.Serializable;
import java.util.HashMap;

public abstract class VideoDetailsCommentsDialogAbstractFragment extends BaseDialogFragment implements Response.Listener<CommentList>, Response.ErrorListener {
    private static final int DEFAULT_PAGE_SIZE = 10;
    public static final String EXTRA_COMMENT_TYPE = "video.details.commentType";
    public static final String EXTRA_CONTENT_ID = "video.details.contentId";
    public static final String EXTRA_DIALOG_LAYOUT = "alert_dialog_layout";
    /* access modifiers changed from: private */
    public boolean mCommentSubmissionInProgress;
    protected VideoDetailsModule.CommentType mCommentType = VideoDetailsModule.CommentType.ON;
    protected CommentList mComments;
    protected String mContentId;
    private int mDialogLayout;
    private boolean mFetchingInProgress = false;
    private int mNextPage = 0;
    private int mPageSize = 0;
    private Response.ErrorListener submitCommentErrorListener = new Response.ErrorListener() {
        public void onErrorResponse(VolleyError error) {
            VideoDetailsCommentsDialogAbstractFragment.this.mCommentSubmissionInProgress = false;
            VideoDetailsCommentsDialogAbstractFragment.this.onCommentSubmitError(CommentFailReason.NETWORK_ERROR);
        }
    };
    private Response.Listener<Comment> submitCommentListener = new Response.Listener<Comment>() {
        public void onResponse(Comment response) {
            VideoDetailsCommentsDialogAbstractFragment.this.mCommentSubmissionInProgress = false;
            VideoDetailsCommentsDialogAbstractFragment.this.onCommentSubmitSuccess();
        }
    };

    public enum CommentFailReason {
        ALREADY_SENDING,
        MISSING_NICKNAME_OR_CONTENT,
        NETWORK_ERROR
    }

    /* access modifiers changed from: protected */
    public abstract void createLayoutHooks(View view, LayoutInflater layoutInflater);

    /* access modifiers changed from: protected */
    public abstract boolean getCanceledOnTouchOutside();

    public abstract void onCommentSubmitError(CommentFailReason commentFailReason);

    public abstract void onCommentSubmitSuccess();

    public abstract void onCommentsFetched(CommentList commentList, int i);

    public abstract void onContentError(String str);

    public abstract void onNetworkFetchStarted();

    public static Bundle getArgumentBundle(String contentId) {
        Bundle args = new Bundle();
        args.putString("video.details.contentId", contentId);
        return args;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            String extraContentId = args.getString("video.details.contentId");
            if (!TextUtils.isEmpty(extraContentId)) {
                this.mContentId = extraContentId;
            }
            this.mDialogLayout = args.getInt(EXTRA_DIALOG_LAYOUT);
            Serializable obj = args.getSerializable("video.details.commentType");
            if (obj instanceof VideoDetailsModule.CommentType) {
                this.mCommentType = (VideoDetailsModule.CommentType) obj;
            }
        }
        try {
            VideoDetailsModule module = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule();
            if (module.getCommentType() != null) {
                this.mCommentType = module.getCommentType();
            }
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mDialogLayout, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().setCanceledOnTouchOutside(getCanceledOnTouchOutside());
        createLayoutHooks(view, inflater);
        return view;
    }

    /* access modifiers changed from: protected */
    public final void resetPagingAndFetch() {
        this.mNextPage = 0;
        fetchNextPage();
    }

    /* access modifiers changed from: protected */
    public final void fetchNextPage() {
        fetchNetworkCommentsById(this.mContentId);
    }

    public int getNextPage() {
        return this.mNextPage;
    }

    private void fetchNetworkCommentsById(String contentId) {
        if (!this.mFetchingInProgress) {
            this.mFetchingInProgress = true;
            onNetworkFetchStarted();
            addRequest(new GsonRequest<>(getActivity(), RequestUtils.getCommentsForContentIdUrl(getActivity(), contentId, this.mNextPage, getPageSize()), CommentList.class, null, this, this));
        }
    }

    public void onResponse(CommentList response) {
        if (response.getData() != null) {
            int i = this.mNextPage;
            this.mNextPage = i + 1;
            onCommentsFetched(response, i);
        } else {
            onContentError("comments list null");
        }
        this.mFetchingInProgress = false;
    }

    public void onErrorResponse(VolleyError error) {
        onContentError(error.getMessage());
        this.mFetchingInProgress = false;
    }

    public void submitComment(String nickname, String comment) {
        if (TextUtils.isEmpty(nickname) || TextUtils.isEmpty(comment)) {
            onCommentSubmitError(CommentFailReason.MISSING_NICKNAME_OR_CONTENT);
        } else if (this.mCommentSubmissionInProgress) {
            onCommentSubmitError(CommentFailReason.ALREADY_SENDING);
        } else {
            this.mCommentSubmissionInProgress = true;
            GsonRequest<Comment> request = new GsonRequest<>(getActivity(), RequestUtils.getSubmitCommentUrl(getActivity()), Comment.class, RequestUtils.getSubmitCommentPostBody(getActivity(), this.mContentId, nickname, comment), this.submitCommentListener, this.submitCommentErrorListener);
            request.setHeaders(RequestUtils.createHeaders(getActivity(), false));
            addRequest(request);
            HashMap<String, String> attributes = new HashMap<>();
            attributes.put("contentId", this.mContentId);
            attributes.put(T.string.nickname, nickname);
            AnalyticsUtils.tagEvent(getActivity(), getAnalyticsSession(), AnalyticsUtils.ADD_COMMENT_EVENT, attributes);
        }
    }

    /* access modifiers changed from: protected */
    public int getPageSize() {
        if (this.mPageSize <= 0) {
            try {
                this.mPageSize = AppConfig.getInstance().getConfig().getModules().getCommentsModule().getPageSize();
                if (this.mPageSize <= 0) {
                    this.mPageSize = 10;
                }
            } catch (Exception e) {
                Utils.doLogException(e);
                if (this.mPageSize <= 0) {
                    this.mPageSize = 10;
                }
            } catch (Throwable th) {
                if (this.mPageSize <= 0) {
                    this.mPageSize = 10;
                }
                throw th;
            }
        }
        return this.mPageSize;
    }
}
