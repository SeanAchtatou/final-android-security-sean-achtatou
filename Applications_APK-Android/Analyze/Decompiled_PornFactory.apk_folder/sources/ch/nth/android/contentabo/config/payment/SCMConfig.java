package ch.nth.android.contentabo.config.payment;

import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;
import com.google.android.gms.plus.PlusShare;

public class SCMConfig {
    @Dictionary(name = "properties")
    private SCMProperties properties;
    @Property(name = PlusShare.KEY_CALL_TO_ACTION_URL)
    private String url;

    public String getUrl() {
        return this.url;
    }

    public SCMProperties getProperties() {
        if (this.properties == null) {
            return new SCMProperties();
        }
        return this.properties;
    }
}
