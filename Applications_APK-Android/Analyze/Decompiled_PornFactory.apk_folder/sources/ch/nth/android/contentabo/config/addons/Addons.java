package ch.nth.android.contentabo.config.addons;

import ch.nth.simpleplist.annotation.Dictionary;

public class Addons {
    @Dictionary(name = "analytics")
    private Analytics analytics;

    public Analytics getAnalytics() {
        if (this.analytics == null) {
            return new Analytics();
        }
        return this.analytics;
    }
}
