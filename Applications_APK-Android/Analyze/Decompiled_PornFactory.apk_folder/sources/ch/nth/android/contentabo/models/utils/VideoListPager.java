package ch.nth.android.contentabo.models.utils;

import java.io.Serializable;

public class VideoListPager implements Serializable {
    private static final long serialVersionUID = -1168934236893958975L;
    private int currentPageNumberContent = 0;
    private int currentPageNumberSearch = 0;
    private ListDisplayMode displayMode = ListDisplayMode.CONTENT;

    public ListDisplayMode getDisplayMode() {
        return this.displayMode;
    }

    public void setDisplayMode(ListDisplayMode mode) {
        this.displayMode = mode;
    }

    public void changeDisplayMode() {
        this.displayMode = this.displayMode == ListDisplayMode.CONTENT ? ListDisplayMode.SEARCH : ListDisplayMode.CONTENT;
    }

    public int getPageNumber() {
        if (this.displayMode == ListDisplayMode.CONTENT) {
            return this.currentPageNumberContent;
        }
        if (this.displayMode == ListDisplayMode.SEARCH) {
            return this.currentPageNumberSearch;
        }
        return 0;
    }

    public void clearSearch() {
        this.currentPageNumberSearch = 0;
    }

    public void success() {
        if (this.displayMode == ListDisplayMode.CONTENT) {
            this.currentPageNumberContent++;
        } else if (this.displayMode == ListDisplayMode.SEARCH) {
            this.currentPageNumberSearch++;
        }
    }
}
