package ch.nth.android.contentabo.models.utils;

import ch.nth.android.contentabo.models.content.Content;
import java.util.Comparator;

public class VideoSortTopRatedComparator implements Comparator<Content> {
    public int compare(Content lhs, Content rhs) {
        if (lhs.getAvgRating() < rhs.getAvgRating()) {
            return 1;
        }
        return lhs.getAvgRating() > rhs.getAvgRating() ? -1 : 0;
    }
}
