package ch.nth.android.contentabo.navigation;

public enum NavigationTarget {
    HOME,
    MY_SUBSCRIPTIONS,
    LEGAL_NOTES,
    HELP
}
