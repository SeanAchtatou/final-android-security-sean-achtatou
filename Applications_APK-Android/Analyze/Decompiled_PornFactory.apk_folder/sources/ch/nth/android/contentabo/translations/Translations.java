package ch.nth.android.contentabo.translations;

import android.content.Context;
import ch.nth.android.contentabo.App;
import ch.nth.android.polyglot.translator.MissingEntryPolicy;
import ch.nth.android.polyglot.translator.MissingValuePolicy;
import ch.nth.android.polyglot.translator.Polyglot;
import ch.nth.android.polyglot.translator.TranslationModule;
import ch.nth.android.polyglot.translator.TranslationStorage;
import java.util.LinkedHashSet;
import java.util.Set;

public class Translations {
    public static final String CACHE_FILENAME = "translations.json";
    private static volatile Translations instance = null;
    private Polyglot mPolyglot;

    private Translations() {
    }

    public static Polyglot getPolyglot() {
        if (instance == null) {
            synchronized (Translations.class) {
                if (instance == null) {
                    instance = new Translations();
                    regenerate();
                    initialize();
                }
            }
        }
        return instance.mPolyglot;
    }

    public static Polyglot initPolyglot(TranslationModule translations, Context context) {
        if (instance == null) {
            synchronized (Translations.class) {
                if (instance == null) {
                    instance = new Translations();
                }
            }
        }
        instance.mPolyglot = new Polyglot(translations, context);
        initialize();
        return instance.mPolyglot;
    }

    private static void regenerate() {
        TranslationModule translations = TranslationStorage.deserializeModule(App.getInstance(), "app");
        instance.mPolyglot = new Polyglot(translations, App.getInstance());
    }

    private static void initialize() {
        if (instance.mPolyglot != null) {
            Set<MissingValuePolicy> missingValuePolicies = new LinkedHashSet<>();
            missingValuePolicies.add(MissingValuePolicy.LANGUAGE_FALLBACK);
            missingValuePolicies.add(MissingValuePolicy.RESOURCE_FALLBACK);
            instance.mPolyglot.setMissingValuePolicies(missingValuePolicies);
            instance.mPolyglot.setMissingEntryPolicy(MissingEntryPolicy.RESOURCE_FALLBACK);
        }
    }
}
