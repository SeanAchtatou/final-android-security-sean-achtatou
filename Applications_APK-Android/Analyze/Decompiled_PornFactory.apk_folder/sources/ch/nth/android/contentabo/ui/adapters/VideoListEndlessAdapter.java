package ch.nth.android.contentabo.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.common.adapter.InfiniteBaseAdapter;
import ch.nth.android.contentabo.common.utils.FormatUtils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.PlistConfig;
import ch.nth.android.contentabo.config.mas.ListviewMasConfig;
import ch.nth.android.contentabo.config.modules.VideoListModule;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.utils.ViewUtils;
import com.nth.android.mas.MASLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class VideoListEndlessAdapter extends InfiniteBaseAdapter {
    protected static final int VIEW_TYPE_AD = 1;
    protected static final int VIEW_TYPE_VIDEO = 0;
    private int everyXPositions = 0;
    private PlistConfig mConfig;
    private final List<Content> mContent;
    private Context mContext;
    private final LayoutInflater mInflater;
    private final int mLayoutResource;
    private ListviewMasConfig mMasConfig;
    private int mTotal;
    private VideoListModule mVideoListModule;

    public VideoListEndlessAdapter(Context context, int layoutResource, List<Content> content, int total, InfiniteBaseAdapter.Listener listener, ListviewMasConfig masConfig) {
        this.mContext = context;
        this.mLayoutResource = layoutResource;
        this.mInflater = LayoutInflater.from(context);
        this.mContent = new ArrayList();
        this.mConfig = AppConfig.getInstance().getConfig();
        this.mVideoListModule = this.mConfig.getModules().getVideoListModule();
        this.mMasConfig = masConfig;
        if (this.mMasConfig == null) {
            this.mMasConfig = ListviewMasConfig.newDefaultInstance();
        }
        setInfiniteAdapterListener(listener);
        setData(content, total);
    }

    public void setData(List<Content> content, int total) {
        if (content != null) {
            this.mContent.clear();
            this.mContent.addAll(content);
            this.mTotal = calculateNewTotal(total);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        this.mContent.clear();
        this.mTotal = 0;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.mContent.size();
    }

    public Content getItem(int position) {
        return this.mContent.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemViewType(int position) {
        if (!showAdViewType() || !isAdViewType(position)) {
            return 0;
        }
        return 1;
    }

    public int getViewTypeCount() {
        if (showAdViewType()) {
            return 2;
        }
        return 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        final VideoItemHolder holder;
        final boolean showProgressBar = true;
        View view = convertView;
        int itemViewType = getItemViewType(position);
        if (itemViewType == 0) {
            if (view == null) {
                view = this.mInflater.inflate(this.mLayoutResource, parent, false);
                holder = new VideoItemHolder();
                holder.imbPlay = (ImageView) view.findViewById(R.id.video_list_item_play);
                holder.imbPlay.setTag(Integer.valueOf(position));
                holder.imgPreviewImage = (ImageView) view.findViewById(R.id.video_list_item_image);
                holder.txtTitle = (TextView) view.findViewById(R.id.video_list_item_title);
                holder.txtDuration = (TextView) view.findViewById(R.id.video_list_item_duration);
                holder.vTimeImportedContainer = view.findViewById(R.id.video_list_item_time_imported_container);
                holder.vTimeImportedDivider = view.findViewById(R.id.video_list_item_time_imported_divider);
                holder.txtTimeImported = (TextView) view.findViewById(R.id.video_list_item_time_imported);
                holder.vViewsContainer = view.findViewById(R.id.video_list_item_views_container);
                holder.vViewsDivider = view.findViewById(R.id.video_list_item_views_divider);
                holder.txtViews = (TextView) view.findViewById(R.id.video_list_item_views);
                holder.vRatingContainer = view.findViewById(R.id.video_list_item_rating_container);
                holder.vRatingDivider = view.findViewById(R.id.video_list_item_rating_divider);
                holder.rbRating = (RatingBar) view.findViewById(R.id.video_list_item_rating);
                holder.progressBar = (ProgressBar) view.findViewById(R.id.video_list_item_progressbar);
                holder.durationLabel = (TextView) view.findViewById(R.id.video_list_item_duration_label);
                holder.timeImportedLabel = (TextView) view.findViewById(R.id.video_list_item_time_imported_label);
                holder.viewsLabel = (TextView) view.findViewById(R.id.video_list_item_views_label);
                holder.ratingLabel = (TextView) view.findViewById(R.id.video_list_item_rating_label);
                configureVisibilities(holder);
                view.setTag(holder);
            } else {
                holder = (VideoItemHolder) view.getTag();
                holder.imbPlay.setTag(Integer.valueOf(position));
            }
            Content item = getItem(position);
            if (!showProgressBarForImageLoading() || holder.progressBar == null) {
                showProgressBar = false;
            }
            if (showProgressBar) {
                holder.imbPlay.setVisibility(4);
                holder.progressBar.setVisibility(0);
            }
            if (!(holder == null || item == null)) {
                Picasso.with(this.mContext).load(item.getPictureUrl(getPictureFlavor(), getPictureSelectMode())).into(holder.imgPreviewImage, new Callback() {
                    public void onSuccess() {
                        if (showProgressBar) {
                            holder.imbPlay.setVisibility(0);
                            holder.progressBar.setVisibility(8);
                        }
                    }

                    public void onError() {
                        if (showProgressBar) {
                            holder.imbPlay.setVisibility(0);
                            holder.progressBar.setVisibility(8);
                        }
                    }
                });
                ViewUtils.setText(holder.durationLabel, Translations.getPolyglot().getString(T.string.video_list_item_duration));
                ViewUtils.setText(holder.timeImportedLabel, Translations.getPolyglot().getString(T.string.video_list_item_uploaded));
                ViewUtils.setText(holder.viewsLabel, Translations.getPolyglot().getString(T.string.video_list_item_views));
                ViewUtils.setText(holder.ratingLabel, Translations.getPolyglot().getString(T.string.video_list_item_rating));
                holder.txtTitle.setText(item.getName());
                holder.txtDuration.setText(FormatUtils.formatContentDuration(item.getDuration()));
                holder.txtTimeImported.setText(FormatUtils.formatLocalDate(this.mContext, item.getTimeImported()));
                holder.txtViews.setText(String.valueOf(item.getDownloadCount()));
                holder.rbRating.setRating(item.getAvgRating());
            }
        } else if (itemViewType == 1 && view == null && (this.mContext instanceof Activity)) {
            view = new MASLayout((Activity) this.mContext, this.mMasConfig.getKey(), this.mMasConfig.getConfigUrl());
        }
        checkAndReportLastPosition(position);
        return view;
    }

    public int getTotalItems() {
        return this.mTotal;
    }

    private void configureVisibilities(VideoItemHolder holder) {
        if (holder != null && this.mVideoListModule != null) {
            if (this.mVideoListModule.isTitleVisibile()) {
                holder.txtTitle.setVisibility(0);
            } else {
                holder.txtTitle.setVisibility(8);
            }
            if (this.mVideoListModule.isCategoryVisibile()) {
                if (holder.vTimeImportedContainer != null) {
                    holder.vTimeImportedContainer.setVisibility(0);
                }
                holder.vTimeImportedDivider.setVisibility(0);
                holder.txtTimeImported.setVisibility(0);
            } else {
                if (holder.vTimeImportedContainer != null) {
                    holder.vTimeImportedContainer.setVisibility(8);
                }
                holder.vTimeImportedDivider.setVisibility(8);
                holder.txtTimeImported.setVisibility(8);
            }
            if (this.mVideoListModule.isNumberOfViewsVisibile()) {
                if (holder.vViewsContainer != null) {
                    holder.vViewsContainer.setVisibility(0);
                }
                holder.vViewsDivider.setVisibility(0);
                holder.txtViews.setVisibility(0);
            } else {
                if (holder.vViewsContainer != null) {
                    holder.vViewsContainer.setVisibility(8);
                }
                holder.vViewsDivider.setVisibility(8);
                holder.txtViews.setVisibility(8);
            }
            if (this.mVideoListModule.isRatingVisibile()) {
                if (holder.vRatingContainer != null) {
                    holder.vRatingContainer.setVisibility(0);
                }
                holder.vRatingDivider.setVisibility(0);
                holder.rbRating.setVisibility(0);
                return;
            }
            if (holder.vRatingContainer != null) {
                holder.vRatingContainer.setVisibility(8);
            }
            holder.vRatingDivider.setVisibility(8);
            holder.rbRating.setVisibility(8);
        }
    }

    static class VideoItemHolder {
        TextView durationLabel;
        ImageView imbPlay;
        ImageView imgPreviewImage;
        ProgressBar progressBar;
        TextView ratingLabel;
        RatingBar rbRating;
        TextView timeImportedLabel;
        TextView txtDuration;
        TextView txtTimeImported;
        TextView txtTitle;
        TextView txtViews;
        View vRatingContainer;
        View vRatingDivider;
        View vTimeImportedContainer;
        View vTimeImportedDivider;
        View vViewsContainer;
        View vViewsDivider;
        TextView viewsLabel;

        VideoItemHolder() {
        }
    }

    /* access modifiers changed from: protected */
    public boolean showProgressBarForImageLoading() {
        return true;
    }

    /* access modifiers changed from: protected */
    public Content.PictureFlavor getPictureFlavor() {
        return Content.PictureFlavor.THUMB;
    }

    /* access modifiers changed from: protected */
    public Content.PictureSelectMode getPictureSelectMode() {
        return Content.PictureSelectMode.FIRST;
    }

    /* access modifiers changed from: protected */
    public final boolean showAdViewType() {
        return this.mMasConfig.isEnabled();
    }

    /* access modifiers changed from: protected */
    public boolean isAdViewType(int position) {
        if (this.everyXPositions == 0) {
            this.everyXPositions = this.mMasConfig.getRepeatEveryXItems();
        }
        return this.everyXPositions > 4 && position != 0 && position % this.everyXPositions == 0;
    }
}
