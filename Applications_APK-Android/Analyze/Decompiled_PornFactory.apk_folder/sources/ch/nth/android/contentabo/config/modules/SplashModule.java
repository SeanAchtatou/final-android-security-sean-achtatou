package ch.nth.android.contentabo.config.modules;

import android.text.TextUtils;
import ch.nth.simpleplist.annotation.Property;

public class SplashModule {
    @Property(name = "show_age_verification")
    private String showAgeVerification;

    public enum AgeVerificationVisibility {
        ON,
        OFF
    }

    public AgeVerificationVisibility getAgeVerificationVisibility() {
        if (TextUtils.isEmpty(this.showAgeVerification)) {
            return AgeVerificationVisibility.ON;
        }
        if (this.showAgeVerification.equalsIgnoreCase(AgeVerificationVisibility.ON.name())) {
            return AgeVerificationVisibility.ON;
        }
        if (this.showAgeVerification.equalsIgnoreCase(AgeVerificationVisibility.OFF.name())) {
            return AgeVerificationVisibility.OFF;
        }
        return AgeVerificationVisibility.ON;
    }
}
