package ch.nth.android.contentabo.config.content;

import ch.nth.android.polyglot.linguistics.Linguist;
import ch.nth.simpleplist.annotation.Property;

public class EmbeddedContentConfig {
    @Property(name = "app_version_required", stringCompatibility = true)
    private int appVersionRequired;
    @Property(name = Linguist.KEY_ENABLED)
    private boolean enabled;

    public static EmbeddedContentConfig newDefaultInstance() {
        return new EmbeddedContentConfig(false, 1);
    }

    public EmbeddedContentConfig() {
    }

    public EmbeddedContentConfig(boolean enabled2, int appVersionRequired2) {
        this.enabled = enabled2;
        this.appVersionRequired = appVersionRequired2;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public int getAppVersionRequired() {
        return this.appVersionRequired;
    }
}
