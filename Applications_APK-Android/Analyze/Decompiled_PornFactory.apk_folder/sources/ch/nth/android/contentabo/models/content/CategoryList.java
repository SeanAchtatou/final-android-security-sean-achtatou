package ch.nth.android.contentabo.models.content;

import java.io.Serializable;
import java.util.List;

public class CategoryList implements Serializable {
    private static final long serialVersionUID = 183278457861799690L;
    private List<Category> data;
    private int total;

    public int getTotal() {
        return this.total;
    }

    public List<Category> getData() {
        return this.data;
    }
}
