package ch.nth.android.contentabo.models.utils;

public enum ListDisplayMode {
    CONTENT,
    SEARCH
}
