package ch.nth.android.contentabo.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

public abstract class WakefulIntentService extends IntentService {
    static final String NAME = "ch.nth.android.contentabo.update.WakefulIntentService";
    private static volatile PowerManager.WakeLock lockStatic = null;

    /* access modifiers changed from: protected */
    public abstract void doWakefulWork(Intent intent);

    private static synchronized PowerManager.WakeLock getLock(Context context) {
        PowerManager.WakeLock wakeLock;
        synchronized (WakefulIntentService.class) {
            if (lockStatic == null) {
                lockStatic = ((PowerManager) context.getSystemService("power")).newWakeLock(1, NAME);
                lockStatic.setReferenceCounted(true);
            }
            wakeLock = lockStatic;
        }
        return wakeLock;
    }

    public static void sendWakefulWork(Context context, Class<?> classService) {
        sendWakefulWork(context, new Intent(context, classService));
    }

    public static void sendWakefulWork(Context context, Intent i) {
        getLock(context.getApplicationContext()).acquire();
        context.startService(i);
    }

    public WakefulIntentService(String name) {
        super(name);
        setIntentRedelivery(true);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        PowerManager.WakeLock lock = getLock(getApplicationContext());
        if (!lock.isHeld() || (flags & 1) != 0) {
            lock.acquire();
        }
        super.onStartCommand(intent, flags, startId);
        return 3;
    }

    /* access modifiers changed from: protected */
    public final void onHandleIntent(Intent intent) {
        try {
            doWakefulWork(intent);
        } finally {
            PowerManager.WakeLock lock = getLock(getApplicationContext());
            if (lock.isHeld()) {
                lock.release();
            }
        }
    }
}
