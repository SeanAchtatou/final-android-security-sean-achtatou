package ch.nth.android.contentabo.models.utils;

import android.content.Context;
import ch.nth.android.contentabo.App;

public class CommentsActionsManager extends ActionsManager {
    private static final String FILENAME = "comments_history.xml";
    private static CommentsActionsManager sInstance;

    public static CommentsActionsManager getInstance() {
        if (sInstance == null) {
            sInstance = new CommentsActionsManager(App.getInstance());
        }
        return sInstance;
    }

    protected CommentsActionsManager(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public String getFilename() {
        return FILENAME;
    }
}
