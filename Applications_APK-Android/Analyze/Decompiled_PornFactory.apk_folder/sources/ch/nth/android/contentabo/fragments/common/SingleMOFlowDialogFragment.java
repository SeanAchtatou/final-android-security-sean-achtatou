package ch.nth.android.contentabo.fragments.common;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.fragments.common.YesNoDialogFragment;
import java.io.Serializable;
import java.util.HashMap;

public class SingleMOFlowDialogFragment extends DialogFragment {
    private static final String ARG_ALERT_DIALOG_LAYOUT = "edittext_alert_dialog_layout";
    private static final String ARG_ALERT_DIALOG_TAG = "edittext_alert_dialog_tag";
    private static final String ARG_ALERT_DIALOG_TRANSLATION_MAPPINGS = "alert_dialog_translation_keys";
    private static final String ARG_DISMISS_DIALOG_ON_CLICK = "arg_dismiss_dialog_on_click";
    /* access modifiers changed from: private */
    public boolean dismissDialogOnClick;
    /* access modifiers changed from: private */
    public YesNoDialogFragment.YesNoDialogListener mAlertDialogListener;
    /* access modifiers changed from: private */
    public int mAlertDialogTag;
    private int mDialogLayout;
    private HashMap<Integer, String> mTranslationMappings;

    public static SingleMOFlowDialogFragment newInstance(YesNoDialogFragment.YesNoDialogListener listener, int tag, int layoutId, boolean dismissDialogOnClick2, HashMap<Integer, String> translationMappings) {
        SingleMOFlowDialogFragment dialogFragment = new SingleMOFlowDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ALERT_DIALOG_TAG, tag);
        args.putInt(ARG_ALERT_DIALOG_LAYOUT, layoutId);
        args.putBoolean(ARG_DISMISS_DIALOG_ON_CLICK, dismissDialogOnClick2);
        args.putSerializable(ARG_ALERT_DIALOG_TRANSLATION_MAPPINGS, translationMappings);
        dialogFragment.setArguments(args);
        dialogFragment.setYesNoDialogListener(listener);
        return dialogFragment;
    }

    private void setYesNoDialogListener(YesNoDialogFragment.YesNoDialogListener listener) {
        this.mAlertDialogListener = listener;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            handleBundleArgs(args);
        }
    }

    @SuppressLint({"UseSparseArrays"})
    private void handleBundleArgs(Bundle args) {
        if (args.containsKey(ARG_ALERT_DIALOG_TAG)) {
            this.mAlertDialogTag = args.getInt(ARG_ALERT_DIALOG_TAG);
        }
        if (args.containsKey(ARG_ALERT_DIALOG_LAYOUT)) {
            this.mDialogLayout = args.getInt(ARG_ALERT_DIALOG_LAYOUT);
        }
        if (args.containsKey(ARG_DISMISS_DIALOG_ON_CLICK)) {
            this.dismissDialogOnClick = args.getBoolean(ARG_DISMISS_DIALOG_ON_CLICK);
        }
        Serializable translationMappingsSerializable = getArguments().getSerializable(ARG_ALERT_DIALOG_TRANSLATION_MAPPINGS);
        if (translationMappingsSerializable instanceof HashMap) {
            try {
                this.mTranslationMappings = (HashMap) translationMappingsSerializable;
            } catch (Exception e) {
            }
        }
        if (this.mTranslationMappings == null) {
            this.mTranslationMappings = new HashMap<>();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mDialogLayout, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().setCanceledOnTouchOutside(false);
        Button yesButton = (Button) view.findViewById(R.id.btn_dialog_positive);
        if (yesButton != null) {
            String positiveButtonText = this.mTranslationMappings.get(Integer.valueOf(R.id.btn_dialog_positive));
            if (!TextUtils.isEmpty(positiveButtonText)) {
                yesButton.setText(positiveButtonText);
            }
            yesButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SingleMOFlowDialogFragment.this.mAlertDialogListener.onDialogPositiveButtonClick(SingleMOFlowDialogFragment.this.mAlertDialogTag);
                    if (SingleMOFlowDialogFragment.this.dismissDialogOnClick) {
                        SingleMOFlowDialogFragment.this.dismiss();
                    }
                }
            });
        }
        Button noButton = (Button) view.findViewById(R.id.btn_dialog_negative);
        if (noButton != null) {
            String negativeButtonText = this.mTranslationMappings.get(Integer.valueOf(R.id.btn_dialog_negative));
            if (!TextUtils.isEmpty(negativeButtonText)) {
                noButton.setText(negativeButtonText);
            }
            noButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SingleMOFlowDialogFragment.this.mAlertDialogListener.onDialogNegativeButtonClick(SingleMOFlowDialogFragment.this.mAlertDialogTag);
                    if (SingleMOFlowDialogFragment.this.dismissDialogOnClick) {
                        SingleMOFlowDialogFragment.this.dismiss();
                    }
                }
            });
        }
        TextView titleTextView = (TextView) view.findViewById(R.id.text_dialog_title);
        if (titleTextView != null) {
            String titleText = this.mTranslationMappings.get(Integer.valueOf(R.id.text_dialog_title));
            if (!TextUtils.isEmpty(titleText)) {
                titleTextView.setText(titleText);
            }
        }
        TextView customHeaderTextView = (TextView) view.findViewById(R.id.text_header_dialog_custom);
        if (customHeaderTextView != null) {
            String headerText = this.mTranslationMappings.get(Integer.valueOf(R.id.text_header_dialog_custom));
            if (!TextUtils.isEmpty(headerText) || !TextUtils.isEmpty(customHeaderTextView.getText())) {
                customHeaderTextView.setText(headerText);
            } else {
                customHeaderTextView.setVisibility(8);
            }
        }
        TextView customFooterTextView = (TextView) view.findViewById(R.id.text_footer_dialog_custom);
        if (customFooterTextView != null) {
            String footerText = this.mTranslationMappings.get(Integer.valueOf(R.id.text_footer_dialog_custom));
            if (!TextUtils.isEmpty(footerText) || !TextUtils.isEmpty(customFooterTextView.getText())) {
                customFooterTextView.setText(footerText);
            } else {
                customFooterTextView.setVisibility(8);
            }
        }
        return view;
    }
}
