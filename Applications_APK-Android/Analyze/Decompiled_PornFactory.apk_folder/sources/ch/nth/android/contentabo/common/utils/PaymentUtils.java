package ch.nth.android.contentabo.common.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.models.payment.PaymentOption;
import ch.nth.android.paymentsdk.v2.model.SubscriptionPaymentSession;
import ch.nth.android.paymentsdk.v2.model.helper.AsyncResponse;
import ch.nth.android.scmsdk.model.ScmSubscriptionSession;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PaymentUtils {
    public static final String ACTION_SCM_STATUS_CHANGE = "ch.nth.android.contentabo.common.utils.scmStatusChange";
    public static final String EXTERNAL_STATUS_PENDING = "EXTERNAL_STATUS_PENDING";
    public static final String KEY_PREFERENCES_FIRST_MO_EXTERNAL_STATUS = "ch.nth.android.contentabo.FIRST_MO_EXTERNAL_STATUS";
    public static final String KEY_PREFERENCES_FIRST_MO_LAST_FLOW_STAGE = "ch.nth.android.contentabo.FIRST_MO_LAST_FLOW_STAGE";
    public static final String KEY_PREFERENCES_FIRST_MO_SINGLE_EXTERNAL_STATUS = "ch.nth.android.contentabo.FIRST_MO_SINGLE_EXTERNAL_STATUS";
    public static final String KEY_PREFERENCES_FIRST_MO_SINGLE_LAST_FLOW_STAGE = "ch.nth.android.contentabo.FIRST_MO_SINGLE_LAST_FLOW_STAGE";
    public static final String KEY_PREFERENCES_FIRST_MO_SINGLE_TIME = "ch.nth.android.contentabo.FIRST_MO_SINGLE_TIME";
    public static final String KEY_PREFERENCES_FIRST_MO_TIME = "ch.nth.android.contentabo.FIRST_MO_TIME";
    public static final String KEY_PREFERENCES_SECOND_MO_EXTERNAL_STATUS = "ch.nth.android.contentabo.SECOND_MO_EXTERNAL_STATUS";
    public static final String KEY_PREFERENCES_SECOND_MO_LAST_FLOW_STAGE = "ch.nth.android.contentabo.SECOND_MO_LAST_FLOW_STAGE";
    public static final String KEY_PREFERENCES_SECOND_MO_TIME = "ch.nth.android.contentabo.SECOND_MO_TIME";
    public static final String KEY_PREFERENCES_SHORT_ID = "ch.nth.android.contentabo.common.utils.KEY_PREFERENCES_SHORT_ID";
    public static final String KEY_PREFERENCES_USE_IMSI_FALLBACK = "ch.nth.android.contentabo.USE_IMSI_FALLBACK";
    public static final String PACKAGE = App.getInstance().getPackageName();
    public static final long PAYMENT_FLOW_STAGE_CACHE_TIME_SECONDS = 1800;
    public static final String PREFS_FRID = (String.valueOf(PACKAGE) + ".frid");
    public static final String PREFS_KEY_PAYMENT_FLOW_STAGE = (String.valueOf(PACKAGE) + ".paymentFlowStage");
    public static final String PREFS_KEY_PAYMENT_FLOW_STAGE_TIMEOUT = (String.valueOf(PACKAGE) + ".paymentFlowStageTimeout");
    public static final String PREFS_KEY_SCM_USER_ID = (String.valueOf(PACKAGE) + ".scmUserId");
    public static final long SCM_SESSION_CLOSE_CACHE_TIME_SECONDS = 1800;
    public static final long SCM_SESSION_OPEN_CACHE_TIME_SECONDS = 10800;
    public static final long SCM_SESSION_TEMPORARILY_CACHE_TIME_SECONDS = 1800;
    private static PaymentOption paymentOption;
    private static AsyncResponse savedPaymentData;
    private static ScmSubscriptionSession.SessionStatus scmSessionStatus;
    private static long scmSessionStatusTimeout;

    public enum PaymentTransactionStatus {
        INITIALIZED,
        RESERVEPENDING,
        RESERVED,
        RESERVERFAILED,
        RESERVERERROR,
        CHARGEPENDING,
        CHARGED,
        CHARGEFAILED,
        ChargeError,
        REFUNDPENDING,
        REFUNDED,
        REFUNDFAILED,
        REFUNDERROR,
        CANCELPENDING,
        CANCELLED,
        CANCELFAILED,
        CANCELERROR,
        ERROR,
        PAYMENTCOMPLETE;

        public static PaymentTransactionStatus valueOfOrDefault(String value) {
            try {
                return valueOf(value.toUpperCase());
            } catch (Exception e) {
                return ERROR;
            }
        }
    }

    public enum PaymentStatus {
        INITIALIZED,
        IDENTIFIED,
        IDENTIFYFAILED,
        IDENTIFYERROR,
        IDENTIFYEXPIRED,
        IDENTIFYDUPLICATE,
        AUTHORIZED,
        AUTHORIZEFAILED,
        AUTHORIZEERROR,
        AUTHORIZEEXPIRED,
        AUTHORIZEREJECTED,
        PAYMENTCOMPLETE,
        PAYMENTPARTIAL,
        PAYMENTFAILED,
        CLOSED,
        ERROR;

        public static PaymentStatus valueOfOrDefault(String value) {
            try {
                return valueOf(value.toUpperCase());
            } catch (Exception e) {
                return CLOSED;
            }
        }
    }

    public enum PaymentFlowStage {
        NONE,
        INIT_PAYMENT_IN_PROGRESS,
        INIT_PAYMENT_FINISHED,
        INIT_PAYMENT_ERROR,
        FIRST_MO_SEND_IN_PROGRESS,
        FIRST_MO_SENT_SUCCESSFULLY,
        FIRST_MO_SEND_ERROR,
        ANGEBOT_MESSAGE_RECIVED,
        SECOND_MO_SEND_IN_PROGRESS,
        SECOND_MO_SENT_SUCCESSFULLY,
        SECOND_MO_SEND_ERROR,
        WELCOME_MESSAGE_RECEIVED;

        public static PaymentFlowStage valueOfOrDefault(String value) {
            try {
                return valueOf(value);
            } catch (Exception e) {
                return NONE;
            }
        }
    }

    public static PaymentOption getPaymentOption(Context context) {
        if (paymentOption == null) {
            try {
                ObjectInputStream is = new ObjectInputStream(context.openFileInput("paymentOption"));
                paymentOption = (PaymentOption) is.readObject();
                is.close();
            } catch (Exception e) {
                paymentOption = new PaymentOption();
                Utils.doLogException(e);
            }
        }
        return paymentOption;
    }

    public static void savePaymentOption(Context context) {
        try {
            ObjectOutputStream os = new ObjectOutputStream(context.openFileOutput("paymentOption", 0));
            os.writeObject(paymentOption);
            os.close();
            Utils.doLog("Payment", "Saving payment option");
            String paymentShortId = paymentOption.getNumberFromServiceUrl().replaceFirst("^00", "");
            if (!TextUtils.isEmpty(paymentShortId)) {
                PreferenceConnector.writeString(context, KEY_PREFERENCES_SHORT_ID, paymentShortId);
            }
        } catch (Exception e) {
            Utils.doLogException(e);
        }
    }

    public static void resetPaymentOption(Context context) {
        String sessionId = paymentOption.getSessionId();
        paymentOption = new PaymentOption();
        paymentOption.setSessionId(sessionId);
        savePaymentOption(context);
        setPaymentFlowStage(context, PaymentFlowStage.NONE);
    }

    public static PaymentFlowStage getPaymentFlowStage() {
        if (System.currentTimeMillis() <= PreferenceConnector.readLong(App.getInstance(), PREFS_KEY_PAYMENT_FLOW_STAGE_TIMEOUT, 0)) {
            return PaymentFlowStage.valueOfOrDefault(PreferenceConnector.readString(App.getInstance(), PREFS_KEY_PAYMENT_FLOW_STAGE, PaymentFlowStage.NONE.name()));
        }
        return PaymentFlowStage.NONE;
    }

    public static void setPaymentFlowStage(Context context, PaymentFlowStage paymentFlowStage) {
        boolean savePaymentOption = false;
        PaymentOption paymentOption2 = getPaymentOption(context);
        Utils.doLog("Payment", "From " + getPaymentFlowStage() + " to " + paymentFlowStage);
        if (paymentFlowStage == PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY || paymentFlowStage == PaymentFlowStage.FIRST_MO_SEND_IN_PROGRESS || paymentFlowStage == PaymentFlowStage.FIRST_MO_SEND_ERROR) {
            savePaymentOption = true;
            paymentOption2.setFirstMoStatus(paymentFlowStage);
        } else if (paymentFlowStage == PaymentFlowStage.SECOND_MO_SENT_SUCCESSFULLY || paymentFlowStage == PaymentFlowStage.SECOND_MO_SEND_IN_PROGRESS || paymentFlowStage == PaymentFlowStage.SECOND_MO_SEND_ERROR) {
            savePaymentOption = true;
            paymentOption2.setSecondMoStatus(paymentFlowStage);
        } else if (paymentFlowStage == PaymentFlowStage.ANGEBOT_MESSAGE_RECIVED) {
            savePaymentOption = true;
            paymentOption2.setAngebotMessageStatus(paymentFlowStage);
        } else if (paymentFlowStage == PaymentFlowStage.WELCOME_MESSAGE_RECEIVED) {
            savePaymentOption = true;
            paymentOption2.setWelcomeMessageStatus(paymentFlowStage);
        }
        if (savePaymentOption) {
            savePaymentOption(context);
        }
        PreferenceConnector.writeLong(App.getInstance(), PREFS_KEY_PAYMENT_FLOW_STAGE_TIMEOUT, System.currentTimeMillis() + 1800000);
        PreferenceConnector.writeString(App.getInstance(), PREFS_KEY_PAYMENT_FLOW_STAGE, paymentFlowStage.name());
    }

    public static int getSCMUserId() {
        return PreferenceConnector.readInteger(App.getInstance(), PREFS_KEY_SCM_USER_ID, 0);
    }

    public static void setSCMUserId(int userId) {
        PreferenceConnector.writeInteger(App.getInstance(), PREFS_KEY_SCM_USER_ID, userId);
    }

    public static String getFrid() {
        return PreferenceConnector.readString(App.getInstance(), PREFS_FRID, "");
    }

    public static void setFrid(String frid) {
        PreferenceConnector.writeString(App.getInstance(), PREFS_FRID, frid);
    }

    public static ScmSubscriptionSession.SessionStatus getSCMStatusOrNull() {
        if (System.currentTimeMillis() <= scmSessionStatusTimeout) {
            return scmSessionStatus;
        }
        return null;
    }

    public static void setSCMStatus(Context context, ScmSubscriptionSession.SessionStatus status, long cacheSeconds) {
        Utils.doLog("SCM", "From " + getSCMStatusOrNull() + " to " + status);
        scmSessionStatusTimeout = System.currentTimeMillis() + (1000 * cacheSeconds);
        scmSessionStatus = status;
        sendSCMStatusChangeBroadcast();
    }

    public static boolean isSubscribed() {
        ScmSubscriptionSession.SessionStatus sessionStatus = getSCMStatusOrNull();
        if (sessionStatus == null || (sessionStatus != ScmSubscriptionSession.SessionStatus.OPEN && sessionStatus != ScmSubscriptionSession.SessionStatus.LIMITED)) {
            return false;
        }
        return true;
    }

    public static boolean getIfUsingImsiFallback(Context context) {
        if (context == null) {
            return false;
        }
        return PreferenceConnector.readBoolean(context, KEY_PREFERENCES_USE_IMSI_FALLBACK, false);
    }

    public static void setIfUsingImsiFallback(Context context, SubscriptionPaymentSession session) {
        Log.d("IMSI_FALLBACK", "Trying to set...");
        if (context != null && session != null) {
            String timeAuthorized = session.getTimeAuthorized();
            if (!TextUtils.isEmpty(timeAuthorized)) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat(App.getInstance().getString(R.string.payment_date_format));
                    SimpleDateFormat sdfPlist = new SimpleDateFormat("yyyy-MM-dd");
                    Date dateAuthorized = sdf.parse(timeAuthorized);
                    Date imsiDate = sdfPlist.parse(AppConfig.getInstance().getConfig().getImsiDate());
                    if (imsiDate == null || !imsiDate.after(dateAuthorized)) {
                        PreferenceConnector.writeBoolean(context, KEY_PREFERENCES_USE_IMSI_FALLBACK, false);
                        Log.d("IMSI_FALLBACK", "set to false...");
                        return;
                    }
                    PreferenceConnector.writeBoolean(context, KEY_PREFERENCES_USE_IMSI_FALLBACK, true);
                    Log.d("IMSI_FALLBACK", "set to TRUE...");
                } catch (Exception e) {
                }
            }
        }
    }

    public static AsyncResponse getPaymnetData(Context context) {
        return savedPaymentData;
    }

    public static void setPaymentData(Context context, AsyncResponse response) {
        savedPaymentData = response;
    }

    public static boolean isPaymentSessionValid(PaymentStatus sessionStatus) {
        if (sessionStatus == PaymentStatus.AUTHORIZED || sessionStatus == PaymentStatus.PAYMENTCOMPLETE || sessionStatus == PaymentStatus.PAYMENTPARTIAL) {
            return true;
        }
        return false;
    }

    public static boolean isPaymentSessionInError(PaymentStatus sessionStatus) {
        if (sessionStatus == PaymentStatus.IDENTIFYFAILED || sessionStatus == PaymentStatus.IDENTIFYERROR || sessionStatus == PaymentStatus.IDENTIFYEXPIRED || sessionStatus == PaymentStatus.IDENTIFYDUPLICATE || sessionStatus == PaymentStatus.AUTHORIZEFAILED || sessionStatus == PaymentStatus.AUTHORIZEERROR || sessionStatus == PaymentStatus.AUTHORIZEEXPIRED || sessionStatus == PaymentStatus.AUTHORIZEREJECTED || sessionStatus == PaymentStatus.CLOSED || sessionStatus == PaymentStatus.ERROR) {
            return true;
        }
        return false;
    }

    private static void sendSCMStatusChangeBroadcast() {
        Utils.doLog("sendSCMStatusChangeBroadcast");
        LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(new Intent(ACTION_SCM_STATUS_CHANGE));
    }

    public static int getMCC(Context context) {
        try {
            String simOperator = ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
            if (!TextUtils.isEmpty(simOperator)) {
                return Integer.parseInt(simOperator.substring(0, 3));
            }
        } catch (Exception e) {
            Utils.doLogException(e);
        }
        return -1;
    }

    public static int getMNC(Context context) {
        try {
            String simOperator = ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
            if (!TextUtils.isEmpty(simOperator)) {
                return Integer.parseInt(simOperator.substring(3));
            }
        } catch (Exception e) {
            Utils.doLogException(e);
        }
        return -1;
    }

    public static String getMCCMNCTuple(Context context) {
        if (context == null) {
            return null;
        }
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getSimOperator();
        } catch (Exception e) {
            Utils.doLogException(e);
            return null;
        }
    }
}
