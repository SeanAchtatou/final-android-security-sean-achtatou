package ch.nth.android.contentabo.config.payment;

import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;
import com.google.android.gms.plus.PlusShare;
import com.nth.analytics.android.LocalyticsProvider;

public class PMConfig {
    @Property(name = LocalyticsProvider.ApiKeysDbColumns.API_KEY)
    private String apiKey;
    @Property(name = "api_secret")
    private String apiSecret;
    @Dictionary(name = "cancel_subscription", required = false)
    private CancelSubscriptionConfig cancelSubscriptionConfig;
    @Dictionary(name = "nwc_modify", required = false)
    private NwcModifyConfig nwcModifyConfig;
    @Property(name = "service_code")
    private String serviceCode;
    @Property(name = PlusShare.KEY_CALL_TO_ACTION_URL)
    private String url;

    public String getUrl() {
        return this.url;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public String getApiSecret() {
        return this.apiSecret;
    }

    public String getServiceCode() {
        return this.serviceCode;
    }

    public CancelSubscriptionConfig getCancelSubscriptionConfig() {
        if (this.cancelSubscriptionConfig == null) {
            this.cancelSubscriptionConfig = new CancelSubscriptionConfig();
        }
        return this.cancelSubscriptionConfig;
    }

    public NwcModifyConfig getNwcModifyConfig() {
        if (this.nwcModifyConfig == null) {
            this.nwcModifyConfig = new NwcModifyConfig();
        }
        return this.nwcModifyConfig;
    }
}
