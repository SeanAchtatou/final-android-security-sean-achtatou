package ch.nth.android.contentabo.config.modules;

import ch.nth.android.contentabo.config.mas.MasConfig;
import ch.nth.simpleplist.annotation.Dictionary;

public class MySubscriptionModule {
    @Dictionary(name = "mas_config", required = false)
    private MasConfig masConfig;

    public MasConfig getMasConfig() {
        if (this.masConfig == null) {
            return MasConfig.newDefaultInstance();
        }
        return this.masConfig;
    }
}
