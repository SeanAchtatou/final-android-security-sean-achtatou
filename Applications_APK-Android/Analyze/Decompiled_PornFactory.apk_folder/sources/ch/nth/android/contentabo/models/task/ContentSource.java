package ch.nth.android.contentabo.models.task;

public enum ContentSource {
    CACHE_ASSETS_REMOTE,
    CACHE_REMOTE,
    REMOTE
}
