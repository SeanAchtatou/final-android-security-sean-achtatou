package ch.nth.android.contentabo.fragments.common;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.fragments.common.YesNoDialogFragment;
import java.io.Serializable;
import java.util.HashMap;

public class TermsAndConditionsDialogFragment extends DialogFragment implements CompoundButton.OnCheckedChangeListener {
    private static final String ARG_ALERT_DIALOG_DISMISS_ON_BUTTON_CLICK = "alert_dialog_dismiss_on_button_click";
    private static final String ARG_ALERT_DIALOG_LAYOUT = "alert_dialog_layout";
    private static final String ARG_ALERT_DIALOG_TAG = "alert_dialog_tag";
    private static final String ARG_ALERT_DIALOG_TRANSLATION_MAPPINGS = "alert_dialog_translation_keys";
    /* access modifiers changed from: private */
    public YesNoDialogFragment.YesNoDialogListener mAlertDialogListener;
    /* access modifiers changed from: private */
    public int mAlertDialogTag;
    private int mDialogLayout;
    /* access modifiers changed from: private */
    public boolean mDismissOnButtonCLick = true;
    private boolean mNotifyDismissAsNegative;
    private Button mPositiveButton;
    /* access modifiers changed from: private */
    public CheckBox mSubscriptionTermsAndConditions;
    private HashMap<Integer, String> mTranslationMappings;

    public static TermsAndConditionsDialogFragment newInstance(YesNoDialogFragment.YesNoDialogListener listener, int tag, int layoutId, boolean dismissOnButtonCLick, HashMap<Integer, String> translationMappings) {
        TermsAndConditionsDialogFragment dialogFragment = new TermsAndConditionsDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ALERT_DIALOG_TAG, tag);
        args.putInt("alert_dialog_layout", layoutId);
        args.putBoolean(ARG_ALERT_DIALOG_DISMISS_ON_BUTTON_CLICK, dismissOnButtonCLick);
        args.putSerializable(ARG_ALERT_DIALOG_TRANSLATION_MAPPINGS, translationMappings);
        dialogFragment.mAlertDialogListener = listener;
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @SuppressLint({"UseSparseArrays"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAlertDialogTag = getArguments().getInt(ARG_ALERT_DIALOG_TAG);
        this.mDialogLayout = getArguments().getInt("alert_dialog_layout");
        this.mDismissOnButtonCLick = getArguments().getBoolean(ARG_ALERT_DIALOG_DISMISS_ON_BUTTON_CLICK);
        Serializable translationMappingsSerializable = getArguments().getSerializable(ARG_ALERT_DIALOG_TRANSLATION_MAPPINGS);
        if (translationMappingsSerializable instanceof HashMap) {
            try {
                this.mTranslationMappings = (HashMap) translationMappingsSerializable;
            } catch (Exception e) {
            }
        }
        if (this.mTranslationMappings == null) {
            this.mTranslationMappings = new HashMap<>();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mDialogLayout, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().setCanceledOnTouchOutside(false);
        this.mPositiveButton = (Button) view.findViewById(R.id.btn_dialog_positive);
        if (this.mPositiveButton != null) {
            String positiveButtonText = this.mTranslationMappings.get(Integer.valueOf(R.id.btn_dialog_positive));
            if (!TextUtils.isEmpty(positiveButtonText)) {
                this.mPositiveButton.setText(positiveButtonText);
            }
            this.mPositiveButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (TermsAndConditionsDialogFragment.this.mSubscriptionTermsAndConditions.isChecked()) {
                        if (TermsAndConditionsDialogFragment.this.mAlertDialogListener != null) {
                            TermsAndConditionsDialogFragment.this.mAlertDialogListener.onDialogPositiveButtonClick(TermsAndConditionsDialogFragment.this.mAlertDialogTag);
                        }
                        if (TermsAndConditionsDialogFragment.this.mDismissOnButtonCLick) {
                            TermsAndConditionsDialogFragment.this.dismiss();
                        }
                    }
                }
            });
        }
        Button negativeButton = (Button) view.findViewById(R.id.btn_dialog_negative);
        if (negativeButton != null) {
            String negativeButtonText = this.mTranslationMappings.get(Integer.valueOf(R.id.btn_dialog_negative));
            if (!TextUtils.isEmpty(negativeButtonText)) {
                negativeButton.setText(negativeButtonText);
            }
            negativeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (TermsAndConditionsDialogFragment.this.mAlertDialogListener != null) {
                        TermsAndConditionsDialogFragment.this.mAlertDialogListener.onDialogNegativeButtonClick(TermsAndConditionsDialogFragment.this.mAlertDialogTag);
                    }
                    if (TermsAndConditionsDialogFragment.this.mDismissOnButtonCLick) {
                        TermsAndConditionsDialogFragment.this.dismiss();
                    }
                }
            });
        }
        TextView titleTextView = (TextView) view.findViewById(R.id.text_dialog_title);
        if (titleTextView != null) {
            String titleText = this.mTranslationMappings.get(Integer.valueOf(R.id.text_dialog_title));
            if (!TextUtils.isEmpty(titleText)) {
                titleTextView.setText(titleText);
            }
        }
        this.mSubscriptionTermsAndConditions = (CheckBox) view.findViewById(R.id.video_details_terms_and_conditions);
        if (this.mSubscriptionTermsAndConditions != null) {
            this.mSubscriptionTermsAndConditions.setText(this.mTranslationMappings.get(Integer.valueOf(R.id.video_details_terms_and_conditions)));
        }
        this.mSubscriptionTermsAndConditions.setChecked(AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().isOptinFlowTremsPreselected());
        updatePositiveButton();
        this.mSubscriptionTermsAndConditions.setOnCheckedChangeListener(this);
        return view;
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.video_details_terms_and_conditions) {
            updatePositiveButton();
        }
    }

    private void updatePositiveButton() {
        this.mPositiveButton.setEnabled(this.mSubscriptionTermsAndConditions.isChecked());
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (this.mNotifyDismissAsNegative && this.mAlertDialogListener != null) {
            this.mAlertDialogListener.onDialogNegativeButtonClick(this.mAlertDialogTag);
        }
    }

    public boolean isNotifyDismissAsNegative() {
        return this.mNotifyDismissAsNegative;
    }

    public void setNotifyDismissAsNegative(boolean notifyDismissAsNegative) {
        this.mNotifyDismissAsNegative = notifyDismissAsNegative;
    }
}
