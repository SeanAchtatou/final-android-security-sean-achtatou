package ch.nth.android.contentabo.activities.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.activities.VideoDetailsAbstractActivity;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.ConfigFactory;
import ch.nth.android.contentabo.config.PlistConfig;
import ch.nth.android.contentabo.config.content.EmbeddedContentConfig;
import ch.nth.android.contentabo.config.menu.RemoteMenu;
import ch.nth.android.contentabo.config.modules.MenuModule;
import ch.nth.android.contentabo.models.content.CategoryList;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.task.CategoryListTask;
import ch.nth.android.contentabo.models.task.ContentSource;
import ch.nth.android.contentabo.navigation.NavigationDrawerAdapter;
import ch.nth.android.contentabo.navigation.NavigationItem;
import ch.nth.android.contentabo.networking.content.GsonRequest;
import ch.nth.android.contentabo.networking.content.RequestUtils;
import ch.nth.android.contentabo.translations.LanguageChangedEvent;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class HomeAbstractActivity extends PaymentAbstractActivity implements Response.Listener<CategoryList>, Response.ErrorListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource = null;
    private static final String INSTANCE_VARIABLE_CATEGORIES = "instanceVariableCategories";
    public static final int SUBSCRIBE_OFFER_REQUEST = 1;
    private NavigationDrawerAdapter adapter;
    protected CategoryList mCategories;
    protected View mDrawerContainer;
    protected DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    protected EmbeddedContentConfig mEmbeddedContentConfig;
    private int mNavigationHomeIndex;
    private int[] mNavigationImages;
    protected List<NavigationItem> mNavigationList;
    private String[] mNavigationTitles;
    private BroadcastReceiver scmStatusChangeReciever = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Utils.doLog("HomeAbstractActivity - scmStatusChangeReciever");
            HomeAbstractActivity.this.setupNavigationDrawer();
        }
    };
    private CategoryListTask.ErrorListener taskErrorListener = new CategoryListTask.ErrorListener() {
        public void onError() {
            Utils.doLog("CategoryListTask", "no local category data");
            HomeAbstractActivity.this.getCategories(ContentSource.REMOTE);
        }
    };
    private CategoryListTask.Listener taskListener = new CategoryListTask.Listener() {
        public void onSuccess(CategoryList data) {
            HomeAbstractActivity.this.onResponse(data);
        }
    };

    /* access modifiers changed from: protected */
    public abstract boolean createCategoriesNavigationHeaders(List<NavigationItem> list);

    /* access modifiers changed from: protected */
    public abstract List<NavigationItem> createCategoriesNavigationObjects();

    /* access modifiers changed from: protected */
    public abstract boolean createNavigationHeaders(List<NavigationItem> list);

    /* access modifiers changed from: protected */
    public abstract int getNavigationDrawerCategoryLayoutResource();

    /* access modifiers changed from: protected */
    public abstract View getNavigationDrawerContainer();

    /* access modifiers changed from: protected */
    public abstract int getNavigationDrawerHeaderLayoutResource();

    /* access modifiers changed from: protected */
    public abstract int getNavigationDrawerItemLayoutResource();

    /* access modifiers changed from: protected */
    public abstract Class<? extends Activity> getVideoDetailsActivityClass();

    /* access modifiers changed from: protected */
    public abstract void onSelectNavigationItem(int i);

    /* access modifiers changed from: protected */
    public abstract boolean shouldCreateNavigationDrawer();

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource;
        if (iArr == null) {
            iArr = new int[ContentSource.values().length];
            try {
                iArr[ContentSource.CACHE_ASSETS_REMOTE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ContentSource.CACHE_REMOTE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ContentSource.REMOTE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mEmbeddedContentConfig = ConfigFactory.getEmbeddedCategoriesConfigOrDefault();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (savedInstanceState == null) {
            getCategories();
        } else {
            this.mCategories = (CategoryList) savedInstanceState.getSerializable(INSTANCE_VARIABLE_CATEGORIES);
        }
        if (shouldCreateNavigationDrawer()) {
            setupNavigationDrawer();
        }
        AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.HOME_SCREEN_SHOWEN_EVENT);
    }

    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(this.scmStatusChangeReciever, new IntentFilter(PaymentUtils.ACTION_SCM_STATUS_CHANGE));
        if (shouldCreateNavigationDrawer()) {
            setupNavigationDrawer();
        }
    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.scmStatusChangeReciever);
    }

    public void onEvent(LanguageChangedEvent event) {
        supportInvalidateOptionsMenu();
        setupNavigationDrawer();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (this.mDrawerToggle != null) {
            this.mDrawerToggle.syncState();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (getAnalyticsSession() != null) {
            getAnalyticsSession().close();
            getAnalyticsSession().upload();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(INSTANCE_VARIABLE_CATEGORIES, this.mCategories);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.search).setTitle(Translations.getPolyglot().getString(T.string.search_title));
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            if (this.mDrawerLayout.isDrawerOpen(this.mDrawerContainer)) {
                this.mDrawerLayout.closeDrawer(this.mDrawerContainer);
            } else {
                this.mDrawerLayout.openDrawer(this.mDrawerContainer);
            }
            return true;
        }
        closeNavigationDrawer();
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == -1) {
            setupNavigationDrawer();
        }
    }

    /* access modifiers changed from: protected */
    public void setupNavigationDrawer() {
        Utils.doLog("Setting up navigationDrawer");
        ActionBar actionBar = getSupportActionBar();
        this.mDrawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer_layout);
        this.mDrawerList = (ListView) findViewById(R.id.navigation_drawer_list);
        this.mDrawerContainer = getNavigationDrawerContainer();
        this.mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, 8388611);
        this.mNavigationList = createNavigationObjects(null);
        this.adapter = new NavigationDrawerAdapter(this, getNavigationDrawerItemLayoutResource(), getNavigationDrawerHeaderLayoutResource(), getNavigationDrawerCategoryLayoutResource(), this.mNavigationList);
        this.mDrawerList.setAdapter((ListAdapter) this.adapter);
        this.mDrawerList.setOnItemClickListener(new DrawerItemClickListener(this, null));
        this.mDrawerList.setSelection(0);
        this.mDrawerList.setItemChecked(0, true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        this.mDrawerToggle = new ActionBarDrawerToggle(this, this.mDrawerLayout, R.drawable.ic_drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                ActivityCompat.invalidateOptionsMenu(HomeAbstractActivity.this);
            }

            public void onDrawerOpened(View drawerView) {
                ActivityCompat.invalidateOptionsMenu(HomeAbstractActivity.this);
            }
        };
        this.mDrawerLayout.setDrawerListener(this.mDrawerToggle);
    }

    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        private DrawerItemClickListener() {
        }

        /* synthetic */ DrawerItemClickListener(HomeAbstractActivity homeAbstractActivity, DrawerItemClickListener drawerItemClickListener) {
            this();
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            HomeAbstractActivity.this.selectNavigationItem(position);
        }
    }

    /* access modifiers changed from: private */
    public void selectNavigationItem(int position) {
        this.mDrawerList.setItemChecked(position, true);
        this.mDrawerList.setSelection(position);
        onSelectNavigationItem(position);
        closeNavigationDrawer();
    }

    /* access modifiers changed from: protected */
    public List<NavigationItem> createNavigationObjects(List<NavigationItem> navigationList) {
        Utils.doLog("ContentAbo", "createNavigationObjects");
        if (navigationList == null) {
            navigationList = new LinkedList<>();
        }
        navigationList.clear();
        if (!(this.mCategories == null || this.mCategories.getData() == null || this.mCategories.getData().size() <= 0)) {
            createCategories(navigationList);
        }
        String appComponentHome = Translations.getPolyglot().getString(T.string.app_component_home);
        this.mNavigationTitles = new String[2];
        this.mNavigationTitles[0] = appComponentHome;
        this.mNavigationTitles[1] = Translations.getPolyglot().getString(T.string.app_component_my_subscriptions);
        TypedArray layoutArray = getResources().obtainTypedArray(R.array.navigation_images_array);
        this.mNavigationImages = new int[layoutArray.length()];
        String[] hideItemsFromUnsubscribedUser = getResources().getStringArray(R.array.navigation_unsubscribe_hidden);
        String[] keysArray = getResources().getStringArray(R.array.navigation_keys_array);
        TypedValue tv = new TypedValue();
        createNavigationHeaders(navigationList);
        PlistConfig config = AppConfig.getInstance().getConfig();
        MenuModule menuModule = config.getModules().getMenuModule();
        for (int i = 0; i < this.mNavigationTitles.length; i++) {
            layoutArray.getValue(i, tv);
            this.mNavigationImages[i] = tv.resourceId;
            NavigationItem navItem = new NavigationItem(this.mNavigationImages[i], this.mNavigationTitles[i], keysArray[i]);
            if (Arrays.binarySearch(hideItemsFromUnsubscribedUser, navItem.getKey()) >= 0 && !PaymentUtils.isSubscribed()) {
                Utils.doLog("not subscribed, skipping menu item: " + navItem.getTitle());
            } else if (menuModule.isVisibileByComponenetKey(navItem.getKey())) {
                navigationList.add(navItem);
                if (navItem.getTitle().equals(appComponentHome)) {
                    this.mNavigationHomeIndex = navigationList.size() - 1;
                }
            }
        }
        for (RemoteMenu remoteMenu : config.getRemoteMenus()) {
            if ((!remoteMenu.isSubscribedOnly() || PaymentUtils.isSubscribed()) && remoteMenu.isVisible()) {
                navigationList.add(new NavigationItem(remoteMenu.isHeader() ? R.drawable.navigation_custom_header_image : R.drawable.navigation_custom_item_image, remoteMenu.getTitle(), remoteMenu.isHeader(), remoteMenu));
            }
        }
        layoutArray.recycle();
        return navigationList;
    }

    /* access modifiers changed from: protected */
    public void getCategories() {
        if (this.mEmbeddedContentConfig == null || !this.mEmbeddedContentConfig.isEnabled()) {
            getCategories(ContentSource.REMOTE);
        } else if (App.getInstance().getVersionCode() >= this.mEmbeddedContentConfig.getAppVersionRequired()) {
            getCategories(ContentSource.CACHE_ASSETS_REMOTE);
        } else {
            getCategories(ContentSource.CACHE_REMOTE);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void getCategories(ContentSource contentSource) {
        String requestUrl = RequestUtils.getCategoriesUrl(this);
        boolean tryAssets = false;
        Utils.doLog("CategoryListTask", "requested category data - " + contentSource);
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$task$ContentSource()[contentSource.ordinal()]) {
            case 1:
                tryAssets = true;
                break;
            case 2:
                break;
            case 3:
                GsonRequest<CategoryList> request = new GsonRequest<>(this, requestUrl, CategoryList.class, null, this, this);
                request.setRetryPolicy(new DefaultRetryPolicy(10000, 3, 1.25f));
                addRequest(request);
                return;
            default:
                return;
        }
        new CategoryListTask(this, requestUrl, tryAssets, this.taskListener, this.taskErrorListener).executeAsync();
        GsonRequest<CategoryList> requestForCache = new GsonRequest<>(this, requestUrl, CategoryList.class, null, null, null);
        requestForCache.setPriority(Request.Priority.LOW);
        addRequest(requestForCache);
    }

    public void onResponse(CategoryList categories) {
        this.mCategories = categories;
        this.mNavigationList = createNavigationObjects(this.mNavigationList);
        this.adapter.notifyDataSetChanged();
    }

    private void createCategories(List<NavigationItem> navigationList) {
        if (navigationList == null) {
            navigationList = new ArrayList<>();
        }
        createCategoriesNavigationHeaders(navigationList);
        navigationList.addAll(createCategoriesNavigationObjects());
    }

    public void onErrorResponse(VolleyError arg0) {
    }

    public void startContentDetailsActivity(String contentId, Content content) {
        Intent intent = new Intent(this, getVideoDetailsActivityClass());
        intent.putExtras(VideoDetailsAbstractActivity.getExtrasBundle(contentId, content));
        if (!PaymentUtils.isSubscribed()) {
            startActivityForResult(intent, 1);
        } else {
            startActivity(intent);
        }
    }

    public void closeNavigationDrawer() {
        if (this.mDrawerLayout != null && this.mDrawerLayout.isDrawerOpen(this.mDrawerContainer)) {
            this.mDrawerLayout.closeDrawer(this.mDrawerContainer);
        }
    }

    public void selectNavigationHome() {
        selectNavigationItem(this.mNavigationHomeIndex);
    }
}
