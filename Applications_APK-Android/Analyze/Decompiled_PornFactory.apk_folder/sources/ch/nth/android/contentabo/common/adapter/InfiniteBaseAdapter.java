package ch.nth.android.contentabo.common.adapter;

import android.widget.BaseAdapter;

public abstract class InfiniteBaseAdapter extends BaseAdapter {
    private Listener mListener;

    public interface Listener {
        void onLastItemReached();
    }

    public abstract int getTotalItems();

    public void setInfiniteAdapterListener(Listener infiniteAdapterListener) {
        this.mListener = infiniteAdapterListener;
    }

    /* access modifiers changed from: protected */
    public boolean isLastPosition(int position) {
        if (position <= 0 || getCount() - 1 != position) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void checkAndReportLastPosition(int position) {
        if (isLastPosition(position) && getCount() < getTotalItems() && this.mListener != null) {
            this.mListener.onLastItemReached();
        }
    }

    /* access modifiers changed from: protected */
    public final int calculateNewTotal(int totalItems) {
        int currentItems = getCount();
        return totalItems > currentItems ? totalItems : currentItems;
    }
}
