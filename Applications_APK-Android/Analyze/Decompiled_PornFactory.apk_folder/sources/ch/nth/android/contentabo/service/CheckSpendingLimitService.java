package ch.nth.android.contentabo.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.PreferenceConnector;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.google.android.gms.drive.DriveFile;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public class CheckSpendingLimitService extends WakefulIntentService {
    public static final String EXTRA_STARTED_FROM_APP = "ch.nth.android.contentabo.service.EXTRA_STARTED_FROM_APP";
    public static final String LAST_LIMIT_CHECK_TIMESTAMP = "ch.nth.android.contentabo.service.LAST_LIMIT_CHECK_TIMESTAMP";
    protected static final int NOTIFICATION_ID = 100;
    private static final String[] PROJECTION = {DmsConstants.ID, "address", "date", "body"};
    private static final String SORT_ORDER = "date desc";
    private static final String TAG = CheckSpendingLimitService.class.getSimpleName();

    public CheckSpendingLimitService() {
        super(null);
    }

    public CheckSpendingLimitService(String name) {
        super(name);
    }

    /* access modifiers changed from: protected */
    public void doWakefulWork(Intent intent) {
        String sentSelection;
        scheduleNextCheck(this);
        if (AppConfig.getInstance().getConfig().getLimitCheck().isCheckSpendingLimit()) {
            boolean startedFromApp = false;
            Bundle extras = intent.getExtras();
            if (extras != null && extras.getBoolean(EXTRA_STARTED_FROM_APP)) {
                startedFromApp = true;
            }
            String inboxSelection = "address like '%" + PaymentUtils.getPaymentOption(this).getNumberFromServiceUrl().replaceFirst("^00", "").replace("+", "") + "%'";
            long validitySeconds = (long) (AppConfig.getInstance().getConfig().getLimitCheck().getMaxMessageAgeMinutes() * 60);
            String inboxPatternString = AppConfig.getInstance().getConfig().getLimitCheck().getIncomingMessagePattern();
            Pattern inboxPattern = null;
            if (!TextUtils.isEmpty(inboxPatternString)) {
                inboxPattern = Pattern.compile(inboxPatternString);
            }
            Result inboxResult = checkSpendingMessage(Uri.parse("content://sms/inbox"), inboxSelection, new Config(validitySeconds, null, inboxPattern, true));
            if (!inboxResult.foundMessage) {
                Log.i(TAG, "no spending limit inbox message found, all is well...");
                return;
            }
            String sentPatternString = AppConfig.getInstance().getConfig().getLimitCheck().getOutgoingMessageContent();
            Pattern sentPattern = null;
            if (!TextUtils.isEmpty(sentPatternString)) {
                sentPattern = Pattern.compile(sentPatternString);
            }
            if (!TextUtils.isEmpty(inboxResult.messageAddress)) {
                sentSelection = "address like '%" + inboxResult.messageAddress + "%'";
            } else {
                sentSelection = inboxSelection;
            }
            if (!checkSpendingMessage(Uri.parse("content://sms/sent"), sentSelection, new Config(validitySeconds, inboxResult.messageDate, sentPattern, false)).foundMessage) {
                Log.i(TAG, "found only inbox spending limit message, we should do something!");
                if (startedFromApp) {
                    Log.i(TAG, "SEND A BROADCAST!");
                    return;
                }
                Log.i(TAG, "DISPLAY A NOTIFICATION!");
                sendBroadcast(new Intent(AlarmReceiver.ACTION_SHOW_SPENDING_LIMIT_NOTIFICATION));
                return;
            }
            Log.i(TAG, "found both inbox and sent spending limit messages, all is well...");
        }
    }

    private Result checkSpendingMessage(Uri uri, String selection, Config config) {
        Result result = new Result(null);
        if (!(uri == null || config.messagePattern == null)) {
            Cursor cursor = null;
            try {
                cursor = getContentResolver().query(uri, PROJECTION, selection, null, SORT_ORDER);
                String[] body = new String[cursor.getCount()];
                String[] date = new String[cursor.getCount()];
                String[] address = new String[cursor.getCount()];
                if (cursor.moveToFirst()) {
                    try {
                        int bodyColumnIndex = cursor.getColumnIndexOrThrow("body");
                        int dateColumnIndex = cursor.getColumnIndexOrThrow("date");
                        int addressColumnIndex = cursor.getColumnIndexOrThrow("address");
                        for (int i = 0; i < cursor.getCount(); i++) {
                            body[i] = cursor.getString(bodyColumnIndex);
                            date[i] = cursor.getString(dateColumnIndex);
                            address[i] = cursor.getString(addressColumnIndex);
                            cursor.moveToNext();
                        }
                    } catch (Exception e) {
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                }
                int count = body.length;
                Date now = Calendar.getInstance().getTime();
                int i2 = 0;
                while (true) {
                    if (i2 < count) {
                        Date messageDate = null;
                        try {
                            messageDate = new Date(Long.parseLong(date[i2]));
                        } catch (NumberFormatException e2) {
                        }
                        if (messageDate != null) {
                            if ((now.getTime() - messageDate.getTime()) / 1000 <= config.messageValiditySeconds) {
                                Log.d(TAG, "checking: " + messageDate + " - " + body[i2]);
                                if (config.maxMessageAgeDate != null) {
                                    if (messageDate.before(config.maxMessageAgeDate)) {
                                        continue;
                                    }
                                }
                                if (config.messagePattern.matcher(body[i2]).matches()) {
                                    Log.i(TAG, "found matching message on date " + date[i2] + " : " + body[i2]);
                                    result.foundMessage = true;
                                    result.messageDate = messageDate;
                                    result.messageAddress = address[i2];
                                    if (config.isInboxCheck && !TextUtils.isEmpty(result.messageAddress)) {
                                        PreferenceConnector.writeString(this, PaymentUtils.KEY_PREFERENCES_SHORT_ID, result.messageAddress);
                                    }
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                        i2++;
                    } else if (cursor != null) {
                        cursor.close();
                    }
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return result;
    }

    public static final synchronized boolean shouldCheckMessages(Context context) {
        boolean z = false;
        synchronized (CheckSpendingLimitService.class) {
            if (context != null) {
                long previousCheck = PreferenceConnector.readLong(context, LAST_LIMIT_CHECK_TIMESTAMP, 0);
                if (previousCheck + ((long) (AppConfig.getInstance().getConfig().getLimitCheck().getCheckIntervalSeconds() * 1000)) + 5000 < System.currentTimeMillis()) {
                    z = true;
                }
            }
        }
        return z;
    }

    public static final void writeCurrentTimeToCheckFlag(Context context) {
        if (context != null) {
            PreferenceConnector.writeLong(context, LAST_LIMIT_CHECK_TIMESTAMP, System.currentTimeMillis());
        }
    }

    public static final void scheduleNextCheck(Context context) {
        if (context != null && AppConfig.getInstance().getConfig().getLimitCheck().isCheckSpendingLimit()) {
            ((AlarmManager) context.getSystemService("alarm")).set(2, SystemClock.elapsedRealtime() + ((long) Math.max(120000, AppConfig.getInstance().getConfig().getLimitCheck().getCheckIntervalSeconds() * 1000)), PendingIntent.getBroadcast(context, 0, new Intent(AlarmReceiver.ACTION_CHECK_SPENDING_LIMIT), DriveFile.MODE_READ_ONLY));
        }
    }

    private static class Config {
        /* access modifiers changed from: private */
        public boolean isInboxCheck;
        /* access modifiers changed from: private */
        public Date maxMessageAgeDate;
        /* access modifiers changed from: private */
        public Pattern messagePattern;
        /* access modifiers changed from: private */
        public long messageValiditySeconds;

        public Config(long maxMessageAgeSeconds, Date maxMessageAgeDate2, Pattern messagePattern2, boolean isInboxCheck2) {
            this.messageValiditySeconds = maxMessageAgeSeconds;
            this.maxMessageAgeDate = maxMessageAgeDate2;
            this.messagePattern = messagePattern2;
            this.isInboxCheck = isInboxCheck2;
        }
    }

    private static class Result {
        /* access modifiers changed from: private */
        public boolean foundMessage;
        /* access modifiers changed from: private */
        public String messageAddress;
        /* access modifiers changed from: private */
        public Date messageDate;

        private Result() {
        }

        /* synthetic */ Result(Result result) {
            this();
        }
    }
}
