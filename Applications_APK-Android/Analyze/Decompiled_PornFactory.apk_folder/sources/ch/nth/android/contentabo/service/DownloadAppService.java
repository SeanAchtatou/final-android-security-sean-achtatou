package ch.nth.android.contentabo.service;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.common.utils.NetworkUtils;
import ch.nth.android.contentabo.common.utils.PreferenceConnector;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.PlistConfig;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import com.google.android.gms.drive.DriveFile;
import java.io.File;

public class DownloadAppService extends WakefulIntentService {
    private static final String DOWNLOAD_APP_WITH_WLAN_DIALOG_INTENT = "ch.nth.contentabo.action.install";
    public static final String FILENAME = "app_update.apk";
    private static final int NOTIFICATION_ID = 83;
    private static final String PARAM_APPLICATION_ID = "aid";
    private static final String PARAM_CURRENT_VERSION = "ver";
    public static final String PREFERENCE_PROPERTY_DOWNLOAD_COUNT = "preference_property_download_count";
    public static final String TAG = DownloadAppService.class.getSimpleName();
    private static final String TAG_UPDATE_WITH_WLAN_ONLY = "tag_ignore_update_with_wlan_only";

    public DownloadAppService() {
        this(null);
    }

    public DownloadAppService(String name) {
        super(name);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    /* access modifiers changed from: protected */
    public void doWakefulWork(Intent intent) {
        long start = System.currentTimeMillis();
        Log.d(TAG, "Starting work...");
        PlistConfig config = getPlistConfig();
        StringBuilder downloadUrl = new StringBuilder(config.getEntStore().getUpdateUrl());
        if (TextUtils.isEmpty(downloadUrl)) {
            Log.d(TAG, "No app download link found... exiting...");
            scheduleDownloadRetry();
            return;
        }
        boolean updateWithWlanOnly = false;
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey(TAG_UPDATE_WITH_WLAN_ONLY)) {
            updateWithWlanOnly = extras.getBoolean(TAG_UPDATE_WITH_WLAN_ONLY);
        }
        int updateAppCountLimit = config.getModules().getUpdateAppModule().getUpdateCountLimit();
        int currentUpdateAppCount = getDownloadRetryCount();
        if (updateAppCountLimit <= currentUpdateAppCount) {
            Log.d(TAG, "Update app count is greater than current update count, exiting from service!");
            return;
        }
        if (updateWithWlanOnly) {
            Log.d(TAG, "Update app with wlan only flag: " + updateWithWlanOnly);
            if (NetworkUtils.getConnectivityStatus(getApplicationContext()) == NetworkUtils.TYPE_MOBILE) {
                Log.d(TAG, "Mobile interface on, dispaching Wifi required for update app notification.");
                dispachWifiRequiredNotification();
                updateDownloadRetryCountValue(currentUpdateAppCount + 1);
                return;
            }
        }
        downloadApp(downloadUrl.toString());
        if (!getDownloadFile().exists()) {
            Log.d(TAG, "No downloaded file found... exiting...");
            scheduleDownloadRetry();
            return;
        }
        showNotification();
        Log.d(TAG, "Download duration: " + (System.currentTimeMillis() - start) + "ms");
        Log.d(TAG, "Finishing work...");
    }

    private File getDownloadFile() {
        return new File(getExternalFilesDir(null), FILENAME);
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x010a A[SYNTHETIC, Splitter:B:36:0x010a] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x011b A[SYNTHETIC, Splitter:B:42:0x011b] */
    /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void downloadApp(java.lang.String r20) {
        /*
            r19 = this;
            r10 = 0
            r4 = 0
            java.io.File r8 = r19.getDownloadFile()
            r2 = 0
            r6 = 0
            android.content.Context r16 = r19.getBaseContext()     // Catch:{ Exception -> 0x0132 }
            java.lang.String r2 = ch.nth.android.scmsdk.utils.Utils.getApplicationUniqueId(r16)     // Catch:{ Exception -> 0x0132 }
            android.content.Context r16 = r19.getBaseContext()     // Catch:{ Exception -> 0x0132 }
            int r16 = ch.nth.android.scmsdk.utils.Utils.getAppVersion(r16)     // Catch:{ Exception -> 0x0132 }
            java.lang.String r6 = java.lang.String.valueOf(r16)     // Catch:{ Exception -> 0x0132 }
        L_0x001c:
            boolean r16 = android.text.TextUtils.isEmpty(r2)
            if (r16 != 0) goto L_0x0028
            boolean r16 = android.text.TextUtils.isEmpty(r6)
            if (r16 == 0) goto L_0x002c
        L_0x0028:
            r8.delete()
        L_0x002b:
            return
        L_0x002c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r0 = r20
            r3.<init>(r0)
            java.lang.String r16 = "/"
            r0 = r20
            r1 = r16
            boolean r16 = r0.endsWith(r1)
            if (r16 != 0) goto L_0x0046
            java.lang.String r16 = "/"
            r0 = r16
            r3.append(r0)
        L_0x0046:
            java.lang.String r16 = "aid"
            r0 = r16
            java.lang.StringBuilder r16 = r3.append(r0)
            java.lang.String r17 = "="
            java.lang.StringBuilder r16 = r16.append(r17)
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r2)
            java.lang.String r17 = "&"
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r17 = "ver"
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r17 = "="
            java.lang.StringBuilder r16 = r16.append(r17)
            r0 = r16
            r0.append(r6)
            java.lang.String r20 = r3.toString()
            r11 = 0
            r13 = 0
            java.lang.String r16 = ch.nth.android.contentabo.service.DownloadAppService.TAG
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            java.lang.String r18 = "Downloading url: "
            r17.<init>(r18)
            r0 = r17
            r1 = r20
            java.lang.StringBuilder r17 = r0.append(r1)
            java.lang.String r18 = " to file: "
            java.lang.StringBuilder r17 = r17.append(r18)
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r8)
            java.lang.String r17 = r17.toString()
            android.util.Log.d(r16, r17)
            java.net.URL r15 = new java.net.URL     // Catch:{ Exception -> 0x012d }
            r0 = r20
            r15.<init>(r0)     // Catch:{ Exception -> 0x012d }
            java.net.URLConnection r5 = r15.openConnection()     // Catch:{ Exception -> 0x012d }
            r5.connect()     // Catch:{ Exception -> 0x012d }
            java.io.BufferedInputStream r12 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x012d }
            java.io.InputStream r16 = r15.openStream()     // Catch:{ Exception -> 0x012d }
            r0 = r16
            r12.<init>(r0)     // Catch:{ Exception -> 0x012d }
            java.io.BufferedOutputStream r14 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x012f, all -> 0x0126 }
            java.io.FileOutputStream r16 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x012f, all -> 0x0126 }
            r0 = r16
            r0.<init>(r8)     // Catch:{ Exception -> 0x012f, all -> 0x0126 }
            r0 = r16
            r14.<init>(r0)     // Catch:{ Exception -> 0x012f, all -> 0x0126 }
            r16 = 1024(0x400, float:1.435E-42)
            r0 = r16
            byte[] r7 = new byte[r0]     // Catch:{ Exception -> 0x00f4, all -> 0x0129 }
        L_0x00c8:
            int r4 = r12.read(r7)     // Catch:{ Exception -> 0x00f4, all -> 0x0129 }
            r16 = -1
            r0 = r16
            if (r4 != r0) goto L_0x00ec
            r14.flush()     // Catch:{ Exception -> 0x00f4, all -> 0x0129 }
            r10 = 1
            java.lang.String r16 = ch.nth.android.contentabo.service.DownloadAppService.TAG     // Catch:{ Exception -> 0x00f4, all -> 0x0129 }
            java.lang.String r17 = "App download finished successfully"
            android.util.Log.d(r16, r17)     // Catch:{ Exception -> 0x00f4, all -> 0x0129 }
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r14)
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r12)
            if (r10 != 0) goto L_0x0135
            r8.delete()     // Catch:{ Exception -> 0x011f }
            r13 = r14
            r11 = r12
            goto L_0x002b
        L_0x00ec:
            r16 = 0
            r0 = r16
            r14.write(r7, r0, r4)     // Catch:{ Exception -> 0x00f4, all -> 0x0129 }
            goto L_0x00c8
        L_0x00f4:
            r9 = move-exception
            r13 = r14
            r11 = r12
        L_0x00f7:
            java.lang.String r16 = ch.nth.android.contentabo.service.DownloadAppService.TAG     // Catch:{ all -> 0x0112 }
            java.lang.String r17 = "App download error"
            r0 = r16
            r1 = r17
            android.util.Log.e(r0, r1, r9)     // Catch:{ all -> 0x0112 }
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r13)
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r11)
            if (r10 != 0) goto L_0x002b
            r8.delete()     // Catch:{ Exception -> 0x010f }
            goto L_0x002b
        L_0x010f:
            r16 = move-exception
            goto L_0x002b
        L_0x0112:
            r16 = move-exception
        L_0x0113:
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r13)
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r11)
            if (r10 != 0) goto L_0x011e
            r8.delete()     // Catch:{ Exception -> 0x0124 }
        L_0x011e:
            throw r16
        L_0x011f:
            r16 = move-exception
            r13 = r14
            r11 = r12
            goto L_0x002b
        L_0x0124:
            r17 = move-exception
            goto L_0x011e
        L_0x0126:
            r16 = move-exception
            r11 = r12
            goto L_0x0113
        L_0x0129:
            r16 = move-exception
            r13 = r14
            r11 = r12
            goto L_0x0113
        L_0x012d:
            r9 = move-exception
            goto L_0x00f7
        L_0x012f:
            r9 = move-exception
            r11 = r12
            goto L_0x00f7
        L_0x0132:
            r16 = move-exception
            goto L_0x001c
        L_0x0135:
            r13 = r14
            r11 = r12
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.contentabo.service.DownloadAppService.downloadApp(java.lang.String):void");
    }

    private void dispachWifiRequiredNotification() {
        Intent i = new Intent(DOWNLOAD_APP_WITH_WLAN_DIALOG_INTENT);
        i.setFlags(DriveFile.MODE_READ_ONLY);
        ((NotificationManager) getSystemService("notification")).notify(NOTIFICATION_ID, new NotificationCompat.Builder(getApplicationContext()).setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, i, 134217728)).setAutoCancel(true).setContentTitle(getString(R.string.app_name)).setContentText(Translations.getPolyglot().getString(T.string.app_update_notification_message)).setTicker(Translations.getPolyglot().getString(T.string.app_update_notification_ticker)).setSmallIcon(R.drawable.ic_launcher).build());
    }

    private void showNotification() {
        Intent i = new Intent("android.intent.action.VIEW");
        i.setDataAndType(Uri.fromFile(getDownloadFile()), "application/vnd.android.package-archive");
        ((NotificationManager) getSystemService("notification")).notify(NOTIFICATION_ID, new NotificationCompat.Builder(getApplicationContext()).setAutoCancel(true).setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, i, 0)).setContentTitle(getString(R.string.app_name)).setContentText(Translations.getPolyglot().getString(T.string.app_update_notification_message)).setTicker(Translations.getPolyglot().getString(T.string.app_update_notification_ticker)).setSmallIcon(R.drawable.ic_launcher).build());
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v17, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: ch.nth.android.contentabo.config.PlistConfig} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v24, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: ch.nth.android.contentabo.config.PlistConfig} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private ch.nth.android.contentabo.config.PlistConfig getPlistConfig() {
        /*
            r12 = this;
            r10 = 0
            r11 = 0
            ch.nth.android.simpleplist.task.TaskOptions$Builder r8 = new ch.nth.android.simpleplist.task.TaskOptions$Builder
            r8.<init>(r10)
            ch.nth.android.simpleplist.task.config.CacheConfigOptions r9 = new ch.nth.android.simpleplist.task.config.CacheConfigOptions
            r9.<init>()
            ch.nth.android.simpleplist.task.TaskOptions$Builder r8 = r8.addStep(r9)
            ch.nth.android.simpleplist.task.TaskOptions r4 = r8.build()
            r1 = 0
            ch.nth.android.simpleplist.task.PlistAsyncTask r5 = new ch.nth.android.simpleplist.task.PlistAsyncTask
            java.lang.Class<ch.nth.android.contentabo.config.PlistConfig> r8 = ch.nth.android.contentabo.config.PlistConfig.class
            r5.<init>(r12, r4, r8)
            r8 = 0
            java.lang.Void[] r8 = new java.lang.Void[r8]     // Catch:{ InterruptedException -> 0x0059, ExecutionException -> 0x005e }
            android.os.AsyncTask r8 = r5.execute(r8)     // Catch:{ InterruptedException -> 0x0059, ExecutionException -> 0x005e }
            java.lang.Object r8 = r8.get()     // Catch:{ InterruptedException -> 0x0059, ExecutionException -> 0x005e }
            r0 = r8
            ch.nth.android.contentabo.config.PlistConfig r0 = (ch.nth.android.contentabo.config.PlistConfig) r0     // Catch:{ InterruptedException -> 0x0059, ExecutionException -> 0x005e }
            r1 = r0
        L_0x002b:
            if (r1 == 0) goto L_0x0063
            ch.nth.android.contentabo.config.update.EntStoreConfig r8 = r1.getEntStore()
            java.lang.String r8 = r8.getUpdateUrl()
            boolean r8 = android.text.TextUtils.isEmpty(r8)
            if (r8 != 0) goto L_0x0063
            java.lang.String r8 = ch.nth.android.contentabo.service.DownloadAppService.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "Download url from cached plist: "
            r9.<init>(r10)
            ch.nth.android.contentabo.config.update.EntStoreConfig r10 = r1.getEntStore()
            java.lang.String r10 = r10.getUpdateUrl()
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            android.util.Log.d(r8, r9)
            r2 = r1
        L_0x0058:
            return r2
        L_0x0059:
            r3 = move-exception
            ch.nth.android.contentabo.common.utils.Utils.doLogException(r3)
            goto L_0x002b
        L_0x005e:
            r3 = move-exception
            ch.nth.android.contentabo.common.utils.Utils.doLogException(r3)
            goto L_0x002b
        L_0x0063:
            java.lang.String r8 = ch.nth.android.contentabo.service.DownloadAppService.TAG
            java.lang.String r9 = "Downloading remote plist..."
            android.util.Log.d(r8, r9)
            int r8 = ch.nth.android.contentabo.R.string.url_settings
            java.lang.String r7 = r12.getString(r8)
            ch.nth.android.simpleplist.task.config.RemoteConfigOptions$Builder r8 = new ch.nth.android.simpleplist.task.config.RemoteConfigOptions$Builder
            r8.<init>(r7)
            r9 = 5000(0x1388, float:7.006E-42)
            ch.nth.android.simpleplist.task.config.RemoteConfigOptions$Builder r8 = r8.timeout(r9)
            r9 = 1
            ch.nth.android.simpleplist.task.config.RemoteConfigOptions$Builder r8 = r8.cacheOnSuccess(r9)
            ch.nth.android.simpleplist.task.config.RemoteConfigOptions r6 = r8.build()
            ch.nth.android.simpleplist.task.TaskOptions$Builder r8 = new ch.nth.android.simpleplist.task.TaskOptions$Builder
            r8.<init>(r10)
            ch.nth.android.simpleplist.task.TaskOptions$Builder r8 = r8.addStep(r6)
            ch.nth.android.simpleplist.task.config.AssetsConfigOptions$Builder r9 = new ch.nth.android.simpleplist.task.config.AssetsConfigOptions$Builder
            r9.<init>()
            java.lang.String r10 = "plist/"
            ch.nth.android.simpleplist.task.config.AssetsConfigOptions$Builder r9 = r9.assetsDirectory(r10)
            java.lang.String r10 = "application-settings.plist"
            ch.nth.android.simpleplist.task.config.AssetsConfigOptions$Builder r9 = r9.filename(r10)
            ch.nth.android.simpleplist.task.config.AssetsConfigOptions r9 = r9.build()
            ch.nth.android.simpleplist.task.TaskOptions$Builder r8 = r8.addStep(r9)
            ch.nth.android.simpleplist.task.TaskOptions$Builder r8 = r8.debug(r11)
            ch.nth.android.simpleplist.task.TaskOptions r4 = r8.build()
            ch.nth.android.simpleplist.task.PlistAsyncTask r5 = new ch.nth.android.simpleplist.task.PlistAsyncTask
            java.lang.Class<ch.nth.android.contentabo.config.PlistConfig> r8 = ch.nth.android.contentabo.config.PlistConfig.class
            r5.<init>(r12, r4, r8)
            r8 = 0
            java.lang.Void[] r8 = new java.lang.Void[r8]     // Catch:{ InterruptedException -> 0x00cd, ExecutionException -> 0x00d2 }
            android.os.AsyncTask r8 = r5.execute(r8)     // Catch:{ InterruptedException -> 0x00cd, ExecutionException -> 0x00d2 }
            java.lang.Object r8 = r8.get()     // Catch:{ InterruptedException -> 0x00cd, ExecutionException -> 0x00d2 }
            r0 = r8
            ch.nth.android.contentabo.config.PlistConfig r0 = (ch.nth.android.contentabo.config.PlistConfig) r0     // Catch:{ InterruptedException -> 0x00cd, ExecutionException -> 0x00d2 }
            r1 = r0
        L_0x00c4:
            if (r1 != 0) goto L_0x00cb
            ch.nth.android.contentabo.config.PlistConfig r1 = new ch.nth.android.contentabo.config.PlistConfig
            r1.<init>()
        L_0x00cb:
            r2 = r1
            goto L_0x0058
        L_0x00cd:
            r3 = move-exception
            ch.nth.android.contentabo.common.utils.Utils.doLogException(r3)
            goto L_0x00c4
        L_0x00d2:
            r3 = move-exception
            ch.nth.android.contentabo.common.utils.Utils.doLogException(r3)
            goto L_0x00c4
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.contentabo.service.DownloadAppService.getPlistConfig():ch.nth.android.contentabo.config.PlistConfig");
    }

    private void scheduleDownloadRetry() {
        long periodMillis = (long) (AppConfig.getInstance().getConfig().getEntStore().getUpdateRetryTimeoutSeconds() * 1000);
        ((AlarmManager) getSystemService("alarm")).set(2, SystemClock.elapsedRealtime() + periodMillis, PendingIntent.getBroadcast(this, 0, new Intent(AlarmReceiver.ACTION_START_APP_UPDATE), DriveFile.MODE_READ_ONLY));
        Log.d(TAG, "Scheduling update download retry in... " + periodMillis + "ms");
    }

    public static Intent getLaunchIntent(Context context, boolean updateWithWlanOnly) {
        Intent intent = new Intent(context, DownloadAppService.class);
        intent.putExtra(TAG_UPDATE_WITH_WLAN_ONLY, updateWithWlanOnly);
        return intent;
    }

    /* access modifiers changed from: protected */
    public int getDownloadRetryCount() {
        return PreferenceConnector.readInteger(getApplicationContext(), PREFERENCE_PROPERTY_DOWNLOAD_COUNT, 0);
    }

    /* access modifiers changed from: protected */
    public void updateDownloadRetryCountValue(int newValue) {
        Log.d(TAG, "Updating update app count, new value: " + newValue);
        PreferenceConnector.writeInteger(getApplicationContext(), PREFERENCE_PROPERTY_DOWNLOAD_COUNT, newValue);
    }
}
