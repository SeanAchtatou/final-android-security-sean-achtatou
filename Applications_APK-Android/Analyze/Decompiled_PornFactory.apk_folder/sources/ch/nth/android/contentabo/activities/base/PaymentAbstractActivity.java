package ch.nth.android.contentabo.activities.base;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.contentabo.common.recivers.SMSSentBroadcastReceiver;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.common.utils.MessageUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.PreferenceConnector;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.payment.PMConfig;
import ch.nth.android.contentabo.models.payment.PaymentOption;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.PaymentSessionStatus;
import ch.nth.android.paymentsdk.v2.model.SubscriptionPaymentSession;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import ch.nth.android.scmsdk.listener.ScmResponseListener;
import ch.nth.android.scmsdk.model.ScmSubscriptionSession;
import ch.nth.android.utils.JavaUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class PaymentAbstractActivity extends BaseAbstractActivity {
    public static int BACKEND_JOB_INIT_PAYMENT = 1;
    public static int BACKEND_JOB_MO_MESSAGE_RECEIVER = 3;
    public static int BACKEND_JOB_SCM_VERIFY_SUBSCRIPTION = 2;
    private static final String TAG = PaymentAbstractActivity.class.getSimpleName();
    private Runnable checkIfSmsSentFromEditorRunnable = new Runnable() {
        public void run() {
            PaymentAbstractActivity.this.checkIfSmsSentFromEditor();
        }
    };
    private boolean isInitPaymentUrlFresh;
    private ProgressDialog mDialog;
    private Handler mHandler = new Handler();
    private SmsManager mSmsManager;
    private boolean mWorking;
    private ScmResponseListener<ScmSubscriptionSession> scmResponseListener = new ScmResponseListener<ScmSubscriptionSession>() {
        public void onResponseReceived(int statusCode, ScmSubscriptionSession response) {
            PaymentAbstractActivity.this.setWorking(false);
            if (response.getStatus() == ScmSubscriptionSession.SessionStatus.DISABLED) {
                PaymentAbstractActivity.this.finish();
            }
            PaymentUtils.setSCMUserId(response.getUserId().intValue());
            if (response.getStatus() == ScmSubscriptionSession.SessionStatus.OPEN || response.getStatus() == ScmSubscriptionSession.SessionStatus.LIMITED) {
                PaymentUtils.setSCMStatus(null, response.getStatus(), PaymentUtils.SCM_SESSION_OPEN_CACHE_TIME_SECONDS);
                PaymentAbstractActivity.this.onBackendCallFinish(PaymentAbstractActivity.BACKEND_JOB_SCM_VERIFY_SUBSCRIPTION);
            } else if (response.getStatus() == ScmSubscriptionSession.SessionStatus.CLOSED) {
                PaymentUtils.setSCMStatus(null, response.getStatus(), 1800);
                PaymentAbstractActivity.this.doInitPayment();
            }
        }

        public void onError(int statusCode) {
            Utils.doLog("SCM", "Error status " + statusCode);
            PaymentAbstractActivity.this.setWorking(false);
        }
    };
    /* access modifiers changed from: private */
    public AtomicBoolean sendingMo = new AtomicBoolean();
    private BroadcastReceiver smsSentReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Utils.doLog("Payment", "smsSentReceiver resultCode: " + getResultCode());
            PaymentAbstractActivity.this.setWorking(false);
            PaymentAbstractActivity.this.sendingMo.set(false);
            PaymentAbstractActivity.this.onBackendCallFinish(PaymentAbstractActivity.BACKEND_JOB_MO_MESSAGE_RECEIVER);
        }
    };

    public abstract void onBackendCallFinish(int i);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PMConfig config = AppConfig.getInstance().getConfig().getPm();
        setConfigData(config.getUrl(), config.getApiKey(), config.getApiSecret());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        registerReceiver(this.smsSentReceiver, new IntentFilter(SMSSentBroadcastReceiver.FIRST_SMS_SENT));
        if (PaymentUtils.EXTERNAL_STATUS_PENDING.equals(PreferenceConnector.readString(this, PaymentUtils.KEY_PREFERENCES_FIRST_MO_EXTERNAL_STATUS, ""))) {
            showProgressDialog();
            this.mHandler.postDelayed(this.checkIfSmsSentFromEditorRunnable, 2500);
        }
    }

    /* access modifiers changed from: private */
    public void checkIfSmsSentFromEditor() {
        Log.d(TAG, "checking 'sms sent' to see if first MO was sent");
        PaymentOption paymentOption = PaymentUtils.getPaymentOption(getBaseContext());
        if (paymentOption != null && paymentOption.getPaymentFlowType() == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
            String number = paymentOption.getNumberFromServiceUrl();
            String body = paymentOption.getBodyFromServiceUrl();
            Cursor cur = getBaseContext().getContentResolver().query(Uri.parse("content://sms/sent"), null, null, null, null);
            int messagesChecked = 0;
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                messagesChecked++;
                String contentBody = cur.getString(cur.getColumnIndex("body"));
                String contentNumber = cur.getString(cur.getColumnIndex("address"));
                if (!TextUtils.equals(contentBody, body) || !TextUtils.equals(number, contentNumber)) {
                    cur.moveToNext();
                } else {
                    PreferenceConnector.writeString(this, PaymentUtils.KEY_PREFERENCES_FIRST_MO_EXTERNAL_STATUS, "");
                    PaymentUtils.setPaymentFlowStage(this, PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY);
                    PaymentUtils.savePaymentOption(this);
                    Log.d(TAG, "FOUND SENT FIRST MO MESSAGE! setting PaymentFlowStage to FIRST_MO_SENT_SUCCESSFULLY and calling onBackendCallFinish, messages checked: " + messagesChecked);
                    stopProgressDialog();
                    onBackendCallFinish(BACKEND_JOB_MO_MESSAGE_RECEIVER);
                    return;
                }
            }
            if (PaymentUtils.isSubscribed()) {
                PreferenceConnector.writeString(this, PaymentUtils.KEY_PREFERENCES_FIRST_MO_EXTERNAL_STATUS, "");
            }
            Log.d(TAG, "DIDN'T FIND SENT FIRST MO MESSAGE! messages checked: " + messagesChecked);
        }
        stopProgressDialog();
    }

    public void onPause() {
        super.onPause();
        unregisterReceiver(this.smsSentReceiver);
    }

    /* access modifiers changed from: protected */
    public void doVerifySCMSubscription() {
        ScmSubscriptionSession.SessionStatus sessionStatus = PaymentUtils.getSCMStatusOrNull();
        if (sessionStatus != null) {
            if (sessionStatus != ScmSubscriptionSession.SessionStatus.OPEN && sessionStatus != ScmSubscriptionSession.SessionStatus.LIMITED) {
                if (sessionStatus == ScmSubscriptionSession.SessionStatus.DISABLED) {
                    finish();
                    return;
                } else if (sessionStatus == ScmSubscriptionSession.SessionStatus.CLOSED) {
                    doInitPayment();
                    return;
                }
            } else {
                return;
            }
        }
        try {
            PaymentOption paymentOption = PaymentUtils.getPaymentOption(getBaseContext());
            setWorking(true);
            getSCM().verifySubscription(ch.nth.android.scmsdk.utils.Utils.getApplicationUniqueId(getBaseContext()), paymentOption.getSessionId(), PaymentUtils.getSCMUserId(), this.scmResponseListener);
        } catch (Exception e) {
            Utils.doLogException(e);
            setWorking(false);
        }
    }

    /* access modifiers changed from: protected */
    public void doInitPayment() {
        PaymentUtils.PaymentFlowStage flowStage = PaymentUtils.getPaymentFlowStage();
        Utils.doLog("Payment", "doInitPayment " + flowStage);
        if (flowStage == PaymentUtils.PaymentFlowStage.NONE || flowStage == PaymentUtils.PaymentFlowStage.INIT_PAYMENT_ERROR) {
            setWorking(true);
            PMConfig config = AppConfig.getInstance().getConfig().getPm();
            setVerifyEveryUrl(true);
            PaymentUtils.setPaymentFlowStage(getBaseContext(), PaymentUtils.PaymentFlowStage.INIT_PAYMENT_IN_PROGRESS);
            initPayment(config.getServiceCode(), ch.nth.android.scmsdk.utils.Utils.getApplicationUniqueId(getBaseContext()), false, false);
            AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.SUBSCRIPTION_INIT_EVENT);
            return;
        }
        String sessionId = PaymentUtils.getPaymentOption(getBaseContext()).getSessionId();
        if (sessionId != null) {
            setWorking(true);
            verifyPayment(sessionId);
        }
    }

    /* access modifiers changed from: protected */
    public void onSuccess(PaymentManagerSteps step, Object item) {
        PaymentOption paymentOption = PaymentUtils.getPaymentOption(getBaseContext());
        Utils.doLog("Payment", "on success (activity), step " + step);
        if (step == PaymentManagerSteps.INIT_PAYMENT_REQUEST_INFO) {
            InfoResponse infoResponse = (InfoResponse) item;
            Log.i("Payment", infoResponse.toString());
            paymentOption.processInfoResponse(infoResponse);
            PaymentUtils.savePaymentOption(getBaseContext());
            if (paymentOption.getPaymentFlowType() == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
                setWorking(false);
                onBackendCallFinish(BACKEND_JOB_INIT_PAYMENT);
            } else if (paymentOption.getPaymentFlowType() == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN) {
                setWorking(false);
                onBackendCallFinish(BACKEND_JOB_INIT_PAYMENT);
            } else if (paymentOption.getPaymentFlowType() == PaymentOption.PaymentFlowType.WEB_FLOW && paymentOption.getPaymentSessionStatus() == PaymentUtils.PaymentStatus.INITIALIZED && this.isInitPaymentUrlFresh) {
                InitPaymentParams initPaymentParams = new InitPaymentParams(paymentOption.getInitRedirectUrl(), paymentOption.getInitRequestId(), paymentOption.getInitCallbackUrl());
                this.isInitPaymentUrlFresh = false;
                loadAllowedWebPage(initPaymentParams);
            } else {
                setWorking(false);
                onBackendCallFinish(BACKEND_JOB_INIT_PAYMENT);
            }
        }
        if (step == PaymentManagerSteps.INIT_PAYMENT_SHOW_WEB_VIEW) {
            PaymentSessionStatus paymentSessionStatus = (PaymentSessionStatus) item;
            Log.i("Payment", "paymentSessionStatus " + paymentSessionStatus.toString());
            if (paymentSessionStatus.getSessionId() != null && paymentSessionStatus.getRedirectUrl() == null) {
                paymentOption.setSessionId(paymentSessionStatus.getSessionId());
                PaymentUtils.savePaymentOption(getBaseContext());
                verifyPayment(paymentSessionStatus.getSessionId());
            }
        } else if (step == PaymentManagerSteps.VERIFY_PAYMENT) {
            Utils.doLog("Payment", "onSuccess (activity) " + item);
            SubscriptionPaymentSession session = (SubscriptionPaymentSession) item;
            PaymentUtils.setIfUsingImsiFallback(this, session);
            PaymentUtils.PaymentStatus paymentStatus = PaymentUtils.PaymentStatus.valueOfOrDefault(session.getStatus());
            if (PaymentUtils.isPaymentSessionValid(paymentStatus)) {
                PaymentUtils.setSCMStatus(null, ScmSubscriptionSession.SessionStatus.OPEN, 1800);
            } else if (PaymentUtils.isPaymentSessionInError(paymentStatus)) {
                PaymentUtils.setPaymentFlowStage(getBaseContext(), PaymentUtils.PaymentFlowStage.INIT_PAYMENT_ERROR);
                PaymentUtils.resetPaymentOption(getBaseContext());
                doInitPayment();
            }
            setWorking(false);
        }
        if (step == PaymentManagerSteps.CLOSE_PAYMENT) {
            PaymentUtils.setPaymentFlowStage(getBaseContext(), PaymentUtils.PaymentFlowStage.INIT_PAYMENT_ERROR);
            PaymentUtils.resetPaymentOption(getBaseContext());
            doInitPayment();
        }
        if (step == PaymentManagerSteps.INIT_PAYMENT_URL) {
            this.isInitPaymentUrlFresh = true;
            InitPaymentParams initPaymentParams2 = (InitPaymentParams) item;
            Utils.doLog("Url received, save it and wait for requestInfo " + initPaymentParams2.getRedirectUrl());
            PaymentUtils.getPaymentOption(getBaseContext()).processInitPaymentParams(initPaymentParams2);
            PaymentUtils.savePaymentOption(getBaseContext());
        }
    }

    /* access modifiers changed from: protected */
    public void onFailure(PaymentManagerSteps step, Object error) {
        Utils.doLog("Payment", "on failure (activity)  step " + step);
        if (step != PaymentManagerSteps.VERIFY_PAYMENT) {
            PaymentUtils.setPaymentFlowStage(getBaseContext(), PaymentUtils.PaymentFlowStage.INIT_PAYMENT_ERROR);
        }
        setWorking(false);
        onBackendCallFinish(BACKEND_JOB_INIT_PAYMENT);
    }

    /* access modifiers changed from: protected */
    public final SmsManager getSmsManager() {
        if (this.mSmsManager == null) {
            this.mSmsManager = SmsManager.getDefault();
        }
        return this.mSmsManager;
    }

    /* access modifiers changed from: protected */
    public void doSendFirstSMS() {
        PaymentOption paymentOption = PaymentUtils.getPaymentOption(getBaseContext());
        if (this.sendingMo.compareAndSet(false, true) && paymentOption.getFirstMoStatus() != PaymentUtils.PaymentFlowStage.SECOND_MO_SENT_SUCCESSFULLY) {
            PreferenceConnector.writeString(this, PaymentUtils.KEY_PREFERENCES_FIRST_MO_LAST_FLOW_STAGE, PaymentUtils.getPaymentFlowStage().name());
            String mccMnc = PaymentUtils.getMCCMNCTuple(this);
            String firstAndSecondFlow = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getOptinFlowOnePlusTwo();
            if (TextUtils.isEmpty(firstAndSecondFlow) || !JavaUtils.regexMatch(firstAndSecondFlow, mccMnc)) {
                PaymentUtils.setPaymentFlowStage(getBaseContext(), PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_IN_PROGRESS);
                if (!trySendSms(paymentOption.getNumberFromServiceUrl(), paymentOption.getBodyFromServiceUrl())) {
                    this.sendingMo.set(false);
                    PaymentUtils.setPaymentFlowStage(getBaseContext(), PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_ERROR);
                    setWorking(false);
                    onBackendCallFinish(BACKEND_JOB_MO_MESSAGE_RECEIVER);
                    return;
                }
                return;
            }
            String number = paymentOption.getNumberFromServiceUrl();
            String body = paymentOption.getBodyFromServiceUrl();
            PreferenceConnector.writeLong(this, PaymentUtils.KEY_PREFERENCES_FIRST_MO_TIME, Calendar.getInstance().getTime().getTime());
            PreferenceConnector.writeString(this, PaymentUtils.KEY_PREFERENCES_FIRST_MO_EXTERNAL_STATUS, PaymentUtils.EXTERNAL_STATUS_PENDING);
            MessageUtils.startSmsMessageIntent(this, number, body);
            this.sendingMo.set(false);
        }
    }

    public boolean trySendSms(String smsNumber, String messageBody) {
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();
        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<>();
        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SMSSentBroadcastReceiver.FIRST_SMS_SENT), 0);
        try {
            SmsManager smsManager = getSmsManager();
            if (TextUtils.isEmpty(messageBody)) {
                return false;
            }
            ArrayList<String> messageParts = smsManager.divideMessage(messageBody);
            for (int i = 0; i < messageParts.size(); i++) {
                sentPendingIntents.add(i, sentPI);
            }
            Utils.doLog("Payment", "Mo in progress");
            smsManager.sendMultipartTextMessage(smsNumber, null, messageParts, sentPendingIntents, deliveredPendingIntents);
            MessageUtils.saveMo2Sent(smsNumber, messageBody, getBaseContext());
            return true;
        } catch (Exception e) {
            Utils.doLogException(e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void showProgressDialog() {
        try {
            if (this.mDialog != null) {
                this.mDialog.dismiss();
            }
            this.mDialog = null;
        } catch (Exception e) {
        }
        try {
            this.mDialog = new ProgressDialog(this);
            this.mDialog.setProgressStyle(0);
            this.mDialog.setIndeterminate(true);
            this.mDialog.setCancelable(false);
            this.mDialog.setCanceledOnTouchOutside(false);
            this.mDialog.setMessage(Translations.getPolyglot().getString(T.string.progress_dialog_text));
            this.mDialog.show();
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    PaymentAbstractActivity.this.stopProgressDialog();
                }
            }, 30000);
        } catch (Exception e2) {
            Utils.doLogException(e2);
        }
    }

    /* access modifiers changed from: protected */
    public void stopProgressDialog() {
        try {
            setWorking(false);
            this.mDialog.dismiss();
        } catch (Exception e) {
            Utils.doLogException(e);
        }
    }

    /* access modifiers changed from: private */
    public void setWorking(boolean value) {
        Utils.doLog("Payment", "working: " + value);
        this.mWorking = value;
    }

    /* access modifiers changed from: protected */
    public boolean isWorking() {
        Utils.doLog("Payment", "isWorking: " + this.mWorking);
        return this.mWorking;
    }

    /* access modifiers changed from: protected */
    public boolean shouldSendFirstMoSMS() {
        ScmSubscriptionSession.SessionStatus scmStatus = PaymentUtils.getSCMStatusOrNull();
        if (scmStatus == null || scmStatus == ScmSubscriptionSession.SessionStatus.OPEN || scmStatus == ScmSubscriptionSession.SessionStatus.LIMITED) {
            return false;
        }
        PaymentOption paymentOption = PaymentUtils.getPaymentOption(getBaseContext());
        Utils.doLog("Payment", "FirstMoStatus " + paymentOption.getFirstMoStatus());
        if (paymentOption.getPaymentFlowType() != PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN || paymentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean shouldRefreshSCMStatus() {
        if (PaymentUtils.getSCMStatusOrNull() == null) {
            return true;
        }
        return false;
    }
}
