package ch.nth.android.contentabo.config;

import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.mas.MasConfig;

public class MasConfigFactory {
    private MasConfigFactory() {
    }

    public static MasConfig getMySubscriptionMasConfig() {
        try {
            return AppConfig.getInstance().getConfig().getModules().getMySubscriptionsModule().getMasConfig();
        } catch (Exception e) {
            Utils.doLogException(e);
            return MasConfig.newDefaultInstance();
        }
    }

    public static MasConfig getVideoListFloatingMasConfig() {
        try {
            return AppConfig.getInstance().getConfig().getModules().getVideoListModule().getFloatingMasConfig();
        } catch (Exception e) {
            Utils.doLogException(e);
            return MasConfig.newDefaultInstance();
        }
    }

    public static MasConfig getVideoDetailsFloatingMasConfig() {
        try {
            return AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getFloatingMasConfig();
        } catch (Exception e) {
            Utils.doLogException(e);
            return MasConfig.newDefaultInstance();
        }
    }
}
