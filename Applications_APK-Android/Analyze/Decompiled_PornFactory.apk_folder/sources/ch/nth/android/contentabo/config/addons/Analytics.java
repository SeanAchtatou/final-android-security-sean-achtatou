package ch.nth.android.contentabo.config.addons;

import ch.nth.simpleplist.annotation.Property;
import com.google.android.gms.plus.PlusShare;

public class Analytics {
    @Property(name = PlusShare.KEY_CALL_TO_ACTION_URL)
    private String url;

    public String getUrl() {
        return this.url;
    }
}
