package ch.nth.android.contentabo.translations;

import android.content.Context;
import ch.nth.android.polyglot.translator.Polyglot;
import ch.nth.android.polyglot.translator.TranslationModule;

public class DisclaimersPolyglot extends Polyglot {
    private static final String DISCLAIMERS_POLYGLOT_PREFERENCES = "disclaimers_polyglot_preferences.xml";

    public DisclaimersPolyglot(TranslationModule translations) {
        super(translations);
    }

    public DisclaimersPolyglot(TranslationModule translations, Context context) {
        super(translations, context);
    }

    public DisclaimersPolyglot(TranslationModule translations, Context context, boolean persistValues, boolean systemLanguageFallback) {
        super(translations, context, persistValues, systemLanguageFallback);
    }

    /* access modifiers changed from: protected */
    public String getPreferencesName() {
        return DISCLAIMERS_POLYGLOT_PREFERENCES;
    }
}
