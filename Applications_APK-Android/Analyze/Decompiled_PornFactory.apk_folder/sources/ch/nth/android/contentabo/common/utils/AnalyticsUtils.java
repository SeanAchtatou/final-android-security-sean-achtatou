package ch.nth.android.contentabo.common.utils;

import android.content.Context;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.scmsdk.utils.Utils;
import com.nth.analytics.android.LocalyticsSession;
import java.util.HashMap;
import java.util.Map;

public class AnalyticsUtils {
    public static final String ADD_COMMENT_EVENT = "action.addComment";
    public static final String AGE_REFUSED_EVENT = "action.AgeRefused";
    public static final String AGE_VERIFICATION_SHOWEN_EVENT = "event.ageVerificationShowen";
    public static final String AGE_VERIFIED_EVENT = "action.AgeVerified";
    public static final String APP_UNAVAILABLE = "event.appUnavailable";
    public static final String CATEGORY_SELECT_EVENT = "action.categorySelect";
    public static final String FIRST_START_EVENT = "event.firstStart";
    public static final String FIRST_START_STATUS_NONE = "first_start_status_none";
    public static final String FIRST_START_STATUS_SENDING = "first_start_status_sending";
    public static final String FIRST_START_STATUS_SUBMITED = "first_start_status_submitted";
    public static final String GENERAL_ERROR_EVENT = "event.generalError";
    public static final String HOME_SCREEN_SHOWEN_EVENT = "event.homeScreenShowen";
    public static final String MANDATORY_UPDATE_DIALOG = "event.mandatoryUpdateDialog";
    public static final String PACKAGE = App.getInstance().getPackageName();
    public static final String PAYMENT_ERROR_EVENT = "event.paymentError";
    public static final String PAYMENT_WEBVIEW_SHOWEN_EVENT = "event.paymentWebViewShowen";
    public static final String PREFS_KEY_FIRST_START_STATUS = (String.valueOf(PACKAGE) + ".firstStartStatus");
    public static final String SCREEN_SELECT_EVENT = "action.screenSelect";
    public static final String SEARCH_EVENT = "action.search";
    public static final String SPENDING_LIMIT_DIALOG_ACCEPTED = "event.spendingLimitDialogAccepted";
    public static final String SPENDING_LIMIT_DIALOG_SHOWN = "event.spendingLimitDialogShown";
    public static final String SPLASH_SHOWEN_EVENT = "event.splashShowen";
    public static final String SUBSCRIPTION_CLOSED_EVENT = "action.subscriptionClosed";
    public static final String SUBSCRIPTION_INIT_EVENT = "action.subscriptionInit";
    public static final String SUBSCRIPTION_OPENED_EVENT = "action.subscriptionOpened";
    public static final String TEMPORARILY_DISABLED_DIALOG = "event.temporarilyDisabledDialog";
    public static final String UPDATE_DIALOG = "event.updateDialog";
    public static final String VIDEO_DETAILS_SHOWEN_EVENT = "event.videoDetailsShowen";
    public static final String VOTING_EVENT = "action.voting";

    public static void tagEvent(Context context, LocalyticsSession session, String event, String key, String value) {
        HashMap<String, String> attributes = new HashMap<>();
        attributes.put(key, value);
        tagEvent(context, session, event, attributes);
    }

    public static void tagEvent(Context context, LocalyticsSession session, String event) {
        tagEvent(context, session, event, null);
    }

    public static void tagEvent(Context context, LocalyticsSession session, String event, Map<String, String> attributes) {
        if (attributes == null) {
            attributes = new HashMap<>();
        }
        if (session != null) {
            if (context != null) {
                try {
                    attributes.put("appUniqueId", Utils.getApplicationUniqueId(context));
                } catch (Exception e) {
                    Utils.doLogException(e);
                    return;
                }
            }
            attributes.put("bcdbId", AppConfig.getInstance().getConfig().getSCM().getProperties().getBcdbId());
            session.tagEvent(event, attributes);
            Utils.doLog(Constants.LOG_TAG, "Tag event " + event + " (" + attributes + ")");
            return;
        }
        Utils.doLog(Constants.LOG_TAG, "Tried to log event but session was null - event: " + event + " (" + attributes + ")");
    }

    public static String getFirstStartStatus() {
        return PreferenceConnector.readString(App.getInstance(), PREFS_KEY_FIRST_START_STATUS, FIRST_START_STATUS_NONE);
    }

    public static void setFirstStartStatus(String status) {
        Utils.doLog("First start status change from " + getFirstStartStatus() + " to " + status);
        PreferenceConnector.writeString(App.getInstance(), PREFS_KEY_FIRST_START_STATUS, status);
    }
}
