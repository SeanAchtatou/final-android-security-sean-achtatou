package ch.nth.android.contentabo.common.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import ch.nth.android.contentabo.config.AppConfig;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageUtils {
    private static final String TAG = MessageUtils.class.getSimpleName();

    private MessageUtils() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static void saveMo2Inbox(String msisdn, String text, Context context) {
        try {
            if (Build.VERSION.SDK_INT < 19) {
                ContentValues values = new ContentValues();
                values.put("address", msisdn);
                values.put("body", text);
                values.put("read", (Boolean) true);
                context.getContentResolver().insert(Uri.parse("content://sms/inbox"), values);
            }
        } catch (Exception ex) {
            Utils.doLog(TAG, "Saving SMS to inbox exception: " + ex.getMessage());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static void saveMo2Sent(String msisdn, String text, Context context) {
        try {
            if (Build.VERSION.SDK_INT < 19) {
                ContentValues values = new ContentValues();
                values.put("address", msisdn);
                values.put("body", text);
                values.put("read", (Boolean) true);
                context.getContentResolver().insert(Uri.parse("content://sms/sent"), values);
            }
        } catch (Exception ex) {
            Utils.doLog(TAG, "Saving SMS to sent exception: " + ex.getMessage());
        }
    }

    public static boolean isOurMessage(String message) {
        try {
            Log.d(TAG, "checking message: " + message);
            String incomingPatternText = AppConfig.getInstance().getConfig().getPanic().getIncomingMessagePattern();
            String angebotPatternText = AppConfig.getInstance().getConfig().getPanic().getAngebotMessagePattern();
            Pattern incomingPattern = Pattern.compile(incomingPatternText);
            Pattern angebotPattern = Pattern.compile(angebotPatternText);
            Matcher incomingMatcher = incomingPattern.matcher(message);
            Matcher angebotMatcher = angebotPattern.matcher(message);
            if (incomingMatcher.matches() || angebotMatcher.matches()) {
                Log.d(TAG, "AngebotMessagePattern match!");
                return true;
            }
            Log.d(TAG, "AngebotMessagePattern not matching - incomingPatternText=" + incomingPatternText + ", angebotPatternText=" + angebotPatternText + ", message=" + message);
            return false;
        } catch (Exception e) {
            Utils.doLogException(e);
        }
    }

    public static void startSmsMessageIntent(Context context, String phone, String message) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("sms:" + phone));
        intent.putExtra("sms_body", message);
        context.startActivity(intent);
    }
}
