package ch.nth.android.contentabo.models.content;

import java.util.List;

public class CommentList {
    private List<Comment> data;
    private int total;

    public int getTotal() {
        return this.total;
    }

    public List<Comment> getData() {
        return this.data;
    }

    public String toString() {
        return "data total: " + this.total + " [ " + this.data + " ]";
    }
}
