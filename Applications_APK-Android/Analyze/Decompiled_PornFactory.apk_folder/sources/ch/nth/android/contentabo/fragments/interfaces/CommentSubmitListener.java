package ch.nth.android.contentabo.fragments.interfaces;

import ch.nth.android.contentabo.fragments.VideoDetailsCommentsAbstractFragment;

public interface CommentSubmitListener {
    void onCommentSubmitError(VideoDetailsCommentsAbstractFragment.CommentFailReason commentFailReason);

    void onCommentSubmitSuccess();
}
