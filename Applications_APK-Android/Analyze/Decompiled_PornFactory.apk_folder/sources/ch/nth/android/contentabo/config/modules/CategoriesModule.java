package ch.nth.android.contentabo.config.modules;

import ch.nth.android.contentabo.config.content.EmbeddedContentConfig;
import ch.nth.simpleplist.annotation.Dictionary;

public class CategoriesModule {
    @Dictionary(name = "embedded_content_config", required = false)
    private EmbeddedContentConfig embeddedContentConfig;

    public EmbeddedContentConfig getEmbeddedContentConfig() {
        if (this.embeddedContentConfig == null) {
            this.embeddedContentConfig = EmbeddedContentConfig.newDefaultInstance();
        }
        return this.embeddedContentConfig;
    }
}
