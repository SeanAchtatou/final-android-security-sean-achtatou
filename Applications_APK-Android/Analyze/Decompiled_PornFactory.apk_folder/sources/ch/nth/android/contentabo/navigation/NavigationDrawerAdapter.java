package ch.nth.android.contentabo.navigation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import ch.nth.android.contentabo.R;
import java.util.List;

public class NavigationDrawerAdapter extends ArrayAdapter<NavigationItem> {
    private static final int VIEW_TYPE_CATEGORY = 2;
    private static final int VIEW_TYPE_HEADER = 1;
    private static final int VIEW_TYPE_NORMAL = 0;
    private final int mLayoutCategoryId;
    private final int mLayoutHeaderId;
    private final int mLayoutId;

    public NavigationDrawerAdapter(Context context, int resource, int headerResource, int categoryResource, List<NavigationItem> items) {
        super(context, resource, items);
        this.mLayoutId = resource;
        this.mLayoutHeaderId = headerResource;
        this.mLayoutCategoryId = categoryResource;
    }

    public int getViewTypeCount() {
        if ((this.mLayoutId == this.mLayoutHeaderId && this.mLayoutId == this.mLayoutCategoryId) || (this.mLayoutHeaderId == 0 && this.mLayoutCategoryId == 0)) {
            return 1;
        }
        return 3;
    }

    public int getItemViewType(int position) {
        NavigationItem item = (NavigationItem) getItem(position);
        if (item.isHeader()) {
            return 1;
        }
        if (item.getCategory() != null) {
            return 2;
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
            if (getViewTypeCount() != 1) {
                switch (getItemViewType(position)) {
                    case 1:
                        view = inflater.inflate(this.mLayoutHeaderId, parent, false);
                        break;
                    case 2:
                        view = inflater.inflate(this.mLayoutCategoryId, parent, false);
                        break;
                    default:
                        view = inflater.inflate(this.mLayoutId, parent, false);
                        break;
                }
            } else {
                view = inflater.inflate(this.mLayoutId, parent, false);
            }
        }
        TextView titleTextView = (TextView) view.findViewById(R.id.navigation_item_title);
        ImageView imageView = (ImageView) view.findViewById(R.id.navigation_item_image);
        NavigationItem item = (NavigationItem) getItem(position);
        if (item != null) {
            titleTextView.setText(item.getTitle());
            imageView.setImageResource(item.getImageId());
        }
        return view;
    }
}
