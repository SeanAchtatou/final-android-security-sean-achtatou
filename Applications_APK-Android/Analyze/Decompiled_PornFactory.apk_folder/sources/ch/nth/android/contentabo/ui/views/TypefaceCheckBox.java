package ch.nth.android.contentabo.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.CheckBox;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.ui.utils.Typefaces;

public class TypefaceCheckBox extends CheckBox {
    public TypefaceCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public TypefaceCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context context, AttributeSet attrs) {
        Typeface tf;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomFont);
        String assetPath = a.getString(R.styleable.CustomFont_fontName);
        a.recycle();
        if (!TextUtils.isEmpty(assetPath) && (tf = Typefaces.get(context, assetPath)) != null) {
            setTypeface(tf);
        }
    }

    public void setTextAppearance(Context context, int resid) {
        Typeface tf;
        super.setTextAppearance(context, resid);
        TypedArray a = context.obtainStyledAttributes(resid, R.styleable.CustomFont);
        String assetPath = a.getString(R.styleable.CustomFont_fontName);
        a.recycle();
        if (!TextUtils.isEmpty(assetPath) && (tf = Typefaces.get(context, assetPath)) != null) {
            setTypeface(tf);
        }
    }
}
