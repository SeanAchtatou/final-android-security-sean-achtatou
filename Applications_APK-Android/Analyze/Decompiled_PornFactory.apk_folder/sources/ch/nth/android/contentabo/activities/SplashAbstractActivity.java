package ch.nth.android.contentabo.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.activities.base.BaseAbstractActivity;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.common.utils.ContentAboUtils;
import ch.nth.android.contentabo.common.utils.EntStoreUtils;
import ch.nth.android.contentabo.common.utils.NetworkUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.PreferenceConnector;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.PlistConfig;
import ch.nth.android.contentabo.config.modules.SplashModule;
import ch.nth.android.contentabo.config.update.EntStoreConfig;
import ch.nth.android.contentabo.fragments.common.YesNoDialogFragment;
import ch.nth.android.contentabo.models.utils.CommentsActionsManager;
import ch.nth.android.contentabo.models.utils.VotingActionsManager;
import ch.nth.android.contentabo.service.DownloadAppService;
import ch.nth.android.contentabo.translations.Disclaimers;
import ch.nth.android.contentabo.translations.Languages;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.contentabo.ui.utils.SystemUiHider;
import ch.nth.android.fetcher.FetchListener;
import ch.nth.android.fetcher.Parser;
import ch.nth.android.fetcher.config.AssetsFetchOptions;
import ch.nth.android.fetcher.config.FetchSource;
import ch.nth.android.fetcher.toolbox.Parsers;
import ch.nth.android.polyglot.translator.Modules;
import ch.nth.android.polyglot.translator.TranslationModuleCollection;
import ch.nth.android.polyglot.translator.fetcher.ModuleCollectionTaskOptions;
import ch.nth.android.polyglot.translator.fetcher.TranslationAsyncTask;
import ch.nth.android.polyglot.translator.fetcher.TranslationTaskOptions;
import ch.nth.android.polyglot.translator.merger.PermissiveEntryValidator;
import ch.nth.android.polyglot.translator.merger.ReflectionModuleMerger;
import ch.nth.android.polyglot.translator.merger.SimpleEntryMerger;
import ch.nth.android.polyglot.translator.merger.SimpleModuleMerger;
import ch.nth.android.polyglot.translator.merger.TargetedModuleMerger;
import ch.nth.android.scmsdk.listener.ScmResponseListener;
import ch.nth.android.scmsdk.model.AffiliateData;
import ch.nth.android.scmsdk.model.ScmSubscriptionSession;
import ch.nth.android.simpleplist.task.PlistAsyncTask;
import ch.nth.android.simpleplist.task.PlistListener;
import ch.nth.android.simpleplist.task.TaskOptions;
import ch.nth.android.simpleplist.task.config.AssetsConfigOptions;
import ch.nth.android.simpleplist.task.config.CacheConfigOptions;
import ch.nth.android.simpleplist.task.config.ConfigType;
import ch.nth.android.simpleplist.task.config.RemoteConfigOptions;
import ch.nth.android.utils.JavaUtils;
import ch.nth.android.utils.TelephonyUtils;
import com.nth.analytics.android.LocalyticsSession;
import java.util.HashMap;
import java.util.Map;
import org.acra.ACRAConstants;

@SuppressLint({"UseSparseArrays"})
public abstract class SplashAbstractActivity extends BaseAbstractActivity implements PlistListener, YesNoDialogFragment.YesNoDialogListener, FetchListener {
    private static final int HIDER_FLAGS = 6;
    private static final long SPLASH_MIN_DURATION = 3000;
    private static final int TAG_AGE_VERIFICATION_DIALOG_CALLBACK = 64;
    private static final String TAG_AGE_VERIFICATION_DIALOG_FRAGMENT = "tag_age_verification_dialog_fragment";
    private static final int TAG_APP_UNAVAILABLE_DIALOG_CALLBACK = 72;
    private static final String TAG_APP_UNAVAILABLE_DIALOG_FRAGMENT = "tag_app_unavailable_dialog_fragment";
    private static final int TAG_CONNECTIVITY_DIALOG_CALLBACK = 66;
    private static final String TAG_CONNECTIVITY_DIALOG_FRAGMENT = "tag_connectivity_dialog_fragment";
    private static final int TAG_MANDATORY_UPDATE_DIALOG_CALLBACK = 70;
    private static final String TAG_MANDATORY_UPDATE_DIALOG_FRAGMENT = "tag_mandatory_update_dialog_fragment";
    private static final int TAG_TEMPORARILY_DISABLED_DIALOG_CALLBACK = 68;
    private static final String TAG_TEMPORARILY_DISABLED_DIALOG_FRAGMENT = "tag_temporarily_disabled_dialog_fragment";
    private static final int TAG_UPDATE_DIALOG_CALLBACK = 69;
    private static final String TAG_UPDATE_DIALOG_FRAGMENT = "tag_update_dialog_fragment";
    Runnable ageVerificationRunnable = new Runnable() {
        public void run() {
            SplashAbstractActivity.this.verifyAgeDialog();
        }
    };
    private boolean mAgeVerified;
    private YesNoDialogFragment mConnectivityDialog;
    private Handler mHandler = new Handler();
    private long mSplashStart;
    private SystemUiHider mSystemUiHider;
    Runnable nextActivityRunnable = new Runnable() {
        public void run() {
            SplashAbstractActivity.this.startActivity(new Intent(SplashAbstractActivity.this, SplashAbstractActivity.this.getNextClass()));
            SplashAbstractActivity.this.finish();
        }
    };
    private PlistAsyncTask<PlistConfig> plistTask;
    private ScmResponseListener<ScmSubscriptionSession> scmResponseListener = new ScmResponseListener<ScmSubscriptionSession>() {
        public void onResponseReceived(int statusCode, ScmSubscriptionSession response) {
            Utils.doLog("SCM", "Session status " + response.getStatus());
            if (response.getStatus() == ScmSubscriptionSession.SessionStatus.DISABLED) {
                SplashAbstractActivity.this.temporarilyDisabledDialog();
                return;
            }
            AffiliateData affiliateData = response.getAffiliateData();
            if (affiliateData != null) {
                PaymentUtils.setFrid(affiliateData.getFrid());
            }
            PaymentUtils.setSCMUserId(response.getUserId().intValue());
            if (response.getSessionId() != null) {
                PaymentUtils.getPaymentOption(SplashAbstractActivity.this.getBaseContext()).setSessionId(response.getSessionId());
                PaymentUtils.savePaymentOption(SplashAbstractActivity.this.getBaseContext());
            }
            if (response.getStatus() == ScmSubscriptionSession.SessionStatus.OPEN || response.getStatus() == ScmSubscriptionSession.SessionStatus.LIMITED) {
                PaymentUtils.setSCMStatus(SplashAbstractActivity.this.getBaseContext(), response.getStatus(), PaymentUtils.SCM_SESSION_OPEN_CACHE_TIME_SECONDS);
            } else {
                PaymentUtils.setSCMStatus(SplashAbstractActivity.this.getBaseContext(), response.getStatus(), 1800);
            }
            if (!SplashAbstractActivity.this.showDialogIfUpdateAvailable()) {
                SplashAbstractActivity.this.verifyAgeCheck();
            }
        }

        public void onError(int statusCode) {
            Utils.doLog("SCM", "Error status " + statusCode);
            SplashAbstractActivity.this.temporarilyDisabledDialog();
        }
    };

    /* access modifiers changed from: protected */
    public abstract int getAgeVerificationView();

    /* access modifiers changed from: protected */
    public abstract View getAnchorView();

    /* access modifiers changed from: protected */
    public abstract int getAppUnavailableView();

    /* access modifiers changed from: protected */
    public abstract int getConectivityView();

    /* access modifiers changed from: protected */
    public abstract int getImageResource();

    /* access modifiers changed from: protected */
    public abstract int getMandatoryUpdateDialogView();

    /* access modifiers changed from: protected */
    public abstract Class<?> getNextClass();

    /* access modifiers changed from: protected */
    public abstract int getTemporarilyDisabledView();

    /* access modifiers changed from: protected */
    public abstract int getUpdateDialogView();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSplashStart = System.currentTimeMillis();
        this.mSystemUiHider = SystemUiHider.getInstance(this, getAnchorView(), 6);
        this.mSystemUiHider.setup();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.mSystemUiHider.hide();
        if (verifyConnectivity()) {
            fetchPlist();
            VotingActionsManager.getInstance();
            CommentsActionsManager.getInstance();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        int status = NetworkUtils.getConnectivityStatus(this);
        if ((status == NetworkUtils.TYPE_WIFI || status == NetworkUtils.TYPE_MOBILE) && this.mConnectivityDialog != null && this.mConnectivityDialog.isVisible()) {
            this.mConnectivityDialog.dismiss();
            fetchPlist();
            VotingActionsManager.getInstance();
            CommentsActionsManager.getInstance();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.plistTask != null) {
            this.plistTask.cancel(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mHandler.removeCallbacks(this.nextActivityRunnable);
        this.mHandler.removeCallbacks(this.ageVerificationRunnable);
        this.mSystemUiHider.show();
        if (getAnalyticsSession() != null) {
            getAnalyticsSession().close();
            getAnalyticsSession().upload();
        }
        super.onDestroy();
    }

    public void onLowMemory() {
        Toast.makeText(getApplicationContext(), "LOW MEMORY", 0).show();
        super.onLowMemory();
    }

    private void fetchPlist() {
        TaskOptions options = new TaskOptions.Builder(this).addStep(new RemoteConfigOptions.Builder(getString(R.string.url_settings)).timeout(ACRAConstants.DEFAULT_SOCKET_TIMEOUT).cacheOnSuccess(true).build()).addStep(new CacheConfigOptions()).addStep(new AssetsConfigOptions.Builder().assetsDirectory(AssetsFetchOptions.PLIST_DEFAULT_ASSETS_DIRECTORY).filename(AssetsFetchOptions.PLIST_DEFAULT_ASSETS_FILENAME).build()).debug(false).build();
        if (this.plistTask != null) {
            this.plistTask.cancel(false);
        }
        this.plistTask = new PlistAsyncTask<>(this, options, PlistConfig.class);
        this.plistTask.executeAsync();
    }

    public void onPlistSucceeded(Object instance, ConfigType type, int tag) {
        if (instance instanceof PlistConfig) {
            AppConfig.getInstance().setConfig((PlistConfig) instance);
            AppConfig.getInstance().setType(type);
            fetchTranslations();
            return;
        }
        onPlistFailed(tag);
    }

    public void onPlistFailed(int tag) {
        Utils.doLog("Something went wrong with the plist");
        temporarilyDisabledDialog();
    }

    private void fetchTranslations() {
        String urlTranslations = AppConfig.getInstance().getConfig().getDmsTranslationsUrl();
        String urlDisclaimers = AppConfig.getInstance().getConfig().getDmsDisclaimersUrl();
        String dmsTranslationsCredential = AppConfig.getInstance().getConfig().getDmsTranslationsAccessKey();
        String dmsTranslationsSecret = AppConfig.getInstance().getConfig().getDmsTranslationsAccessCode();
        String dmsDisclaimersCredential = AppConfig.getInstance().getConfig().getDmsDisclaimersAccessKey();
        String dmsDisclaimersSecret = AppConfig.getInstance().getConfig().getDmsDisclaimersAccessCode();
        Parser<TranslationModuleCollection> parser = Parsers.newGsonParser(TranslationModuleCollection.class);
        ModuleCollectionTaskOptions moduleCollectionTaskOptions = new ModuleCollectionTaskOptions(new TranslationTaskOptions.RemoteBuilder(urlTranslations, parser).withApiKeyAuthHeader(dmsTranslationsCredential, dmsTranslationsSecret).withDebugMode(false).build(), new TranslationTaskOptions.FallbackBuilder(parser).withDirectory(AssetsFetchOptions.TRANSLATIONS_DEFAULT_ASSETS_DIRECTORY).withFilename(Translations.CACHE_FILENAME).build());
        TargetedModuleMerger appMerger = new ReflectionModuleMerger.Builder("app", T.string.class).withCheckAllLanguages(true).withMissingLanguageMerger(new SimpleEntryMerger()).withMissingEntryValidator(new PermissiveEntryValidator()).build();
        new TranslationAsyncTask.Builder(this, moduleCollectionTaskOptions, this).addCollectionConfig(new ModuleCollectionTaskOptions(new TranslationTaskOptions.RemoteBuilder(urlDisclaimers, parser).withApiKeyAuthHeader(dmsDisclaimersCredential, dmsDisclaimersSecret).withDebugMode(false).build(), new TranslationTaskOptions.FallbackBuilder(parser).withDirectory(AssetsFetchOptions.TRANSLATIONS_DEFAULT_ASSETS_DIRECTORY).withFilename(AssetsFetchOptions.DISCLAIMERS_DEFAULT_ASSETS_FILENAME).build())).addTargetedMerger(appMerger).addMerger(new SimpleModuleMerger.Builder().withCheckAllLanguages(true).withMissingLanguageMerger(new SimpleEntryMerger()).withMissingEntryValidator(new PermissiveEntryValidator()).build()).withModuleCollectionCacheFilename(Translations.CACHE_FILENAME).addSeparatelyCachedModule("app").addSeparatelyCachedModule(Modules.MODULE_LANGS).build().executeAsync();
    }

    public void onFetchSucceeded(Object instance, FetchSource source, int tag) {
        if (instance instanceof TranslationModuleCollection) {
            TranslationModuleCollection appModules = (TranslationModuleCollection) instance;
            Translations.initPolyglot(appModules.getTranslationModule("app"), this);
            Languages.initLinguist(appModules.getTranslationModule(Modules.MODULE_LANGS));
            Disclaimers.initPolyglot(appModules.getTranslationModule(PaymentUtils.getMCCMNCTuple(this), true), this);
            checkMccMnc();
            return;
        }
        onFetchFailed(tag);
    }

    public void onFetchFailed(int tag) {
        Utils.doLog("Something went wrong with the translations/disclaimers");
        checkMccMnc();
    }

    private void checkMccMnc() {
        String mccMnc = TelephonyUtils.getMCCMNCTuple(this);
        String allowedMccMnc = AppConfig.getInstance().getConfig().getAllowedMccMnc();
        String allowedMccMnc1 = AppConfig.getInstance().getConfig().getAllowedMccMnc1();
        String allowedMccMnc2 = AppConfig.getInstance().getConfig().getAllowedMccMnc2();
        if (JavaUtils.regexMatch(allowedMccMnc, mccMnc) || JavaUtils.regexMatch(allowedMccMnc1, mccMnc) || JavaUtils.regexMatch(allowedMccMnc2, mccMnc)) {
            checkScmAndContinue();
        } else {
            showAppUnavailableDialog();
        }
    }

    private void checkScmAndContinue() {
        PlistConfig config = AppConfig.getInstance().getConfig();
        getSCM().setmBaseEndpoint(config.getSCM().getUrl());
        getSCM().setSid(config.getSCM().getProperties().getBcdbId());
        try {
            getSCM().verifySubscription(ch.nth.android.scmsdk.utils.Utils.getApplicationUniqueId(getBaseContext()), PaymentUtils.getPaymentOption(getBaseContext()).getSessionId(), PaymentUtils.getSCMUserId(), this.scmResponseListener);
        } catch (Exception e) {
            Utils.doLogException(e);
            this.scmResponseListener.onError(0);
        }
        if (AnalyticsUtils.getFirstStartStatus().equals(AnalyticsUtils.FIRST_START_STATUS_NONE)) {
            AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.FIRST_START_EVENT);
            AnalyticsUtils.setFirstStartStatus(AnalyticsUtils.FIRST_START_STATUS_SUBMITED);
        }
        AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.SPLASH_SHOWEN_EVENT);
        checkAndUpdateApplicationUpdateRetryCounter();
    }

    private boolean verifyConnectivity() {
        if (NetworkUtils.getConnectivityStatus(this) != NetworkUtils.TYPE_NOT_CONNECTED) {
            return true;
        }
        this.mConnectivityDialog = YesNoDialogFragment.newInstance(this, TAG_CONNECTIVITY_DIALOG_CALLBACK, getConectivityView(), false, getConnectivityTranslations());
        this.mConnectivityDialog.show(getSupportFragmentManager(), TAG_CONNECTIVITY_DIALOG_FRAGMENT);
        return false;
    }

    /* access modifiers changed from: private */
    public boolean showDialogIfUpdateAvailable() {
        EntStoreConfig entStore = AppConfig.getInstance().getConfig().getEntStore();
        int appVersion = ch.nth.android.scmsdk.utils.Utils.getAppVersion(getBaseContext());
        if (appVersion < entStore.getProperties().getMinimumVersion()) {
            showUpdateDialog(true);
            return true;
        } else if (appVersion >= entStore.getProperties().getCurrentVersion() || !EntStoreUtils.shouldShowReminder((long) entStore.getProperties().getReminderDays())) {
            return false;
        } else {
            showUpdateDialog(false);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void verifyAgeCheck() {
        long splashShownDuration = System.currentTimeMillis() - this.mSplashStart;
        if (AppConfig.getInstance().getConfig().getModules().getSplashModule().getAgeVerificationVisibility() != SplashModule.AgeVerificationVisibility.ON) {
            this.mHandler.post(this.nextActivityRunnable);
        } else if (splashShownDuration >= SPLASH_MIN_DURATION) {
            this.mHandler.post(this.ageVerificationRunnable);
        } else {
            this.mHandler.postDelayed(this.ageVerificationRunnable, SPLASH_MIN_DURATION - splashShownDuration);
        }
    }

    /* access modifiers changed from: private */
    public void verifyAgeDialog() {
        this.mAgeVerified = ContentAboUtils.isAgeVerified();
        if (!this.mAgeVerified) {
            YesNoDialogFragment.newInstance(this, 64, getAgeVerificationView(), true, getAgeVerificationTranslations()).show(getSupportFragmentManager(), TAG_AGE_VERIFICATION_DIALOG_FRAGMENT);
            AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.AGE_VERIFICATION_SHOWEN_EVENT);
            return;
        }
        this.mHandler.post(this.nextActivityRunnable);
    }

    /* access modifiers changed from: private */
    public void temporarilyDisabledDialog() {
        YesNoDialogFragment.newInstance(this, TAG_TEMPORARILY_DISABLED_DIALOG_CALLBACK, getTemporarilyDisabledView(), true, getTemporarilyDisabledTranslations()).show(getSupportFragmentManager(), TAG_TEMPORARILY_DISABLED_DIALOG_FRAGMENT);
        AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.TEMPORARILY_DISABLED_DIALOG);
    }

    private void checkAndUpdateApplicationUpdateRetryCounter() {
        if (ch.nth.android.scmsdk.utils.Utils.getAppVersion(getBaseContext()) >= AppConfig.getInstance().getConfig().getEntStore().getProperties().getMinimumVersion()) {
            PreferenceConnector.writeInteger(getApplicationContext(), DownloadAppService.PREFERENCE_PROPERTY_DOWNLOAD_COUNT, 0);
        }
    }

    private void showUpdateDialog(boolean isMandatory) {
        String str;
        String str2;
        YesNoDialogFragment dialog = YesNoDialogFragment.newInstance(this, isMandatory ? TAG_MANDATORY_UPDATE_DIALOG_CALLBACK : TAG_UPDATE_DIALOG_CALLBACK, isMandatory ? getMandatoryUpdateDialogView() : getUpdateDialogView(), true, isMandatory ? getMandatoryUpdateDialogTranslations() : getUpdateDialogTranslations());
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        if (isMandatory) {
            str = TAG_MANDATORY_UPDATE_DIALOG_FRAGMENT;
        } else {
            str = TAG_UPDATE_DIALOG_FRAGMENT;
        }
        dialog.show(supportFragmentManager, str);
        EntStoreUtils.setReminderTime();
        Context baseContext = getBaseContext();
        LocalyticsSession analyticsSession = getAnalyticsSession();
        if (isMandatory) {
            str2 = AnalyticsUtils.MANDATORY_UPDATE_DIALOG;
        } else {
            str2 = AnalyticsUtils.UPDATE_DIALOG;
        }
        AnalyticsUtils.tagEvent(baseContext, analyticsSession, str2);
    }

    private void showAppUnavailableDialog() {
        YesNoDialogFragment dialog = YesNoDialogFragment.newInstance(this, TAG_APP_UNAVAILABLE_DIALOG_CALLBACK, getAppUnavailableView(), true, getAppUnavailableTranslations());
        dialog.setNotifyDismissAsNegative(true);
        try {
            dialog.show(getSupportFragmentManager(), TAG_APP_UNAVAILABLE_DIALOG_FRAGMENT);
        } catch (Exception e) {
        }
        String mccmnc = TelephonyUtils.getMCCMNCTuple(this);
        Map<String, String> attributes = new HashMap<>();
        if (!TextUtils.isEmpty(mccmnc)) {
            attributes.put("mccmnc", mccmnc);
        }
        AnalyticsUtils.tagEvent(App.getInstance(), getAnalyticsSession(), AnalyticsUtils.APP_UNAVAILABLE, attributes);
    }

    public void onDialogPositiveButtonClick(int alertDialogTag) {
        switch (alertDialogTag) {
            case 64:
                ContentAboUtils.setAgeVerified();
                AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.AGE_VERIFIED_EVENT);
                this.mHandler.post(this.nextActivityRunnable);
                return;
            case 65:
            case 67:
            case 71:
            default:
                return;
            case TAG_CONNECTIVITY_DIALOG_CALLBACK /*66*/:
                startActivity(new Intent("android.settings.SETTINGS"));
                return;
            case TAG_TEMPORARILY_DISABLED_DIALOG_CALLBACK /*68*/:
                finish();
                return;
            case TAG_UPDATE_DIALOG_CALLBACK /*69*/:
            case TAG_MANDATORY_UPDATE_DIALOG_CALLBACK /*70*/:
                openUpdatePage();
                finish();
                return;
            case TAG_APP_UNAVAILABLE_DIALOG_CALLBACK /*72*/:
                finish();
                return;
        }
    }

    private void openUpdatePage() {
        Uri.Builder uriBuilder = Uri.parse(AppConfig.getInstance().getConfig().getEntStore().getUpdatePageUrl()).buildUpon();
        uriBuilder.appendQueryParameter("aid", ch.nth.android.scmsdk.utils.Utils.getApplicationUniqueId(getBaseContext()));
        uriBuilder.appendQueryParameter("ver", String.valueOf(ch.nth.android.scmsdk.utils.Utils.getAppVersion(getBaseContext())));
        startActivity(new Intent("android.intent.action.VIEW", uriBuilder.build()));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onDialogNegativeButtonClick(int alertDialogTag) {
        switch (alertDialogTag) {
            case 64:
                AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.AGE_REFUSED_EVENT);
                finish();
                return;
            case TAG_UPDATE_DIALOG_CALLBACK /*69*/:
                verifyAgeCheck();
                return;
            case TAG_APP_UNAVAILABLE_DIALOG_CALLBACK /*72*/:
                finish();
                break;
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public HashMap<Integer, String> getAgeVerificationTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.age_verification_dialog_title));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.age_verification_dialog_message));
        translations.put(Integer.valueOf(R.id.btn_dialog_negative), Translations.getPolyglot().getString("no"));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString("yes"));
        return translations;
    }

    /* access modifiers changed from: protected */
    public HashMap<Integer, String> getConnectivityTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.check_connectivity_title));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.check_connectivity_message));
        translations.put(Integer.valueOf(R.id.btn_dialog_negative), Translations.getPolyglot().getString("no"));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString("yes"));
        return translations;
    }

    /* access modifiers changed from: protected */
    public HashMap<Integer, String> getTemporarilyDisabledTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.temporarily_disabled_label_title));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.temporarily_disabled_message_text));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString(T.string.temporarily_disabled_button_ok));
        return translations;
    }

    /* access modifiers changed from: protected */
    public HashMap<Integer, String> getUpdateDialogTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.new_update_available_title));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.new_update_available_message));
        translations.put(Integer.valueOf(R.id.btn_dialog_negative), Translations.getPolyglot().getString("no"));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString("yes"));
        return translations;
    }

    /* access modifiers changed from: protected */
    public HashMap<Integer, String> getMandatoryUpdateDialogTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.new_update_available_title));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.mandatory_update_available_message));
        translations.put(Integer.valueOf(R.id.btn_dialog_negative), Translations.getPolyglot().getString("no"));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString(T.string.temporarily_disabled_button_ok));
        return translations;
    }

    /* access modifiers changed from: protected */
    public HashMap<Integer, String> getAppUnavailableTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.app_unavailable_title));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.app_unavailable_message_text));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString(T.string.ok));
        return translations;
    }
}
