package ch.nth.android.contentabo.models.task;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.models.content.CategoryList;
import ch.nth.android.contentabo.networking.content.ContentUtils;
import com.android.volley.Cache;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import java.util.EnumSet;

public class CategoryListTask extends AsyncTask<Void, Void, CategoryList> {
    private static final String ASSETS_DIR = "default_categories/";
    private static final String ASSETS_FILENAME = "categories.json";
    private Context mContext;
    private ErrorListener mErrorListener;
    private final Gson mGson = ContentUtils.createGsonInstance(EnumSet.of(ContentUtils.GsonTypeAdapter.DATE));
    private Listener mListener;
    private boolean mTryAssets;
    private String mUrl;

    public interface ErrorListener {
        void onError();
    }

    public interface Listener {
        void onSuccess(CategoryList categoryList);
    }

    public CategoryListTask(Context context, String url, boolean tryAssets, Listener listener, ErrorListener errorListener) {
        this.mContext = context;
        this.mUrl = url;
        this.mTryAssets = tryAssets;
        this.mListener = listener;
        this.mErrorListener = errorListener;
    }

    @TargetApi(11)
    public void executeAsync() {
        if (Build.VERSION.SDK_INT >= 11) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        } else {
            execute(new Void[0]);
        }
    }

    /* access modifiers changed from: protected */
    public CategoryList doInBackground(Void... params) {
        CategoryList result = tryFromCache();
        if (result != null || !this.mTryAssets) {
            return result;
        }
        return tryFromAssets();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(CategoryList result) {
        if (result != null) {
            if (this.mListener != null) {
                this.mListener.onSuccess(result);
            }
        } else if (this.mErrorListener != null) {
            this.mErrorListener.onError();
        }
    }

    private CategoryList tryFromCache() {
        if (this.mUrl == null) {
            return null;
        }
        try {
            Cache.Entry entry = App.getInstance().getQueue().getCache().get(this.mUrl);
            if (entry != null) {
                return (CategoryList) this.mGson.fromJson(new String(entry.data, HttpHeaderParser.parseCharset(entry.responseHeaders)), CategoryList.class);
            }
            return null;
        } catch (Exception e) {
            Utils.doLogException(e);
            return null;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: ch.nth.android.contentabo.models.content.CategoryList} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
     arg types: [java.io.BufferedReader, java.lang.Class]
     candidates:
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.Class):T
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(com.google.gson.stream.JsonReader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private ch.nth.android.contentabo.models.content.CategoryList tryFromAssets() {
        /*
            r7 = this;
            r4 = 0
            r2 = 0
            android.content.Context r5 = r7.mContext     // Catch:{ Exception -> 0x002a }
            android.content.res.AssetManager r5 = r5.getAssets()     // Catch:{ Exception -> 0x002a }
            java.lang.String r6 = "default_categories/categories.json"
            java.io.InputStream r2 = r5.open(r6)     // Catch:{ Exception -> 0x002a }
            if (r2 == 0) goto L_0x0026
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x002a }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x002a }
            r5.<init>(r2)     // Catch:{ Exception -> 0x002a }
            r3.<init>(r5)     // Catch:{ Exception -> 0x002a }
            com.google.gson.Gson r5 = r7.mGson     // Catch:{ Exception -> 0x002a }
            java.lang.Class<ch.nth.android.contentabo.models.content.CategoryList> r6 = ch.nth.android.contentabo.models.content.CategoryList.class
            java.lang.Object r5 = r5.fromJson(r3, r6)     // Catch:{ Exception -> 0x002a }
            r0 = r5
            ch.nth.android.contentabo.models.content.CategoryList r0 = (ch.nth.android.contentabo.models.content.CategoryList) r0     // Catch:{ Exception -> 0x002a }
            r4 = r0
        L_0x0026:
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r2)
        L_0x0029:
            return r4
        L_0x002a:
            r1 = move-exception
            ch.nth.android.contentabo.common.utils.Utils.doLogException(r1)     // Catch:{ all -> 0x0032 }
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r2)
            goto L_0x0029
        L_0x0032:
            r5 = move-exception
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r2)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.contentabo.models.task.CategoryListTask.tryFromAssets():ch.nth.android.contentabo.models.content.CategoryList");
    }
}
