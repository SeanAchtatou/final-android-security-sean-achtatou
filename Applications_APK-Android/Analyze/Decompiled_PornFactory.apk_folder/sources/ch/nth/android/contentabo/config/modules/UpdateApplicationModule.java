package ch.nth.android.contentabo.config.modules;

import ch.nth.simpleplist.annotation.Property;

public class UpdateApplicationModule {
    @Property(name = "update_count_limit", stringCompatibility = true)
    private int updateCountLimit;

    public int getUpdateCountLimit() {
        return this.updateCountLimit;
    }
}
