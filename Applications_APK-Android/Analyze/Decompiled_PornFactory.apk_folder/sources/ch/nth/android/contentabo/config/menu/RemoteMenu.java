package ch.nth.android.contentabo.config.menu;

import ch.nth.simpleplist.annotation.Property;
import com.nth.analytics.android.LocalyticsProvider;
import java.io.Serializable;

public class RemoteMenu implements Serializable {
    private static final long serialVersionUID = -7747414896715888131L;
    @Property(name = "header", stringCompatibility = true)
    private boolean header;
    private String htmlContent;
    @Property(name = "local_filename", required = false)
    private String localFilename;
    @Property(name = "mode", required = false)
    private String mode = Mode.getDefault().toString();
    @Property(name = "remote_url", required = false)
    private String remoteUrl;
    @Property(name = "subscribed_only", required = false, stringCompatibility = true)
    private boolean subscribedOnly;
    private String title;
    @Property(name = LocalyticsProvider.EventHistoryDbColumns.TYPE, required = false)
    private String type = Type.getDefault().toString();
    @Property(name = "visible", required = false, stringCompatibility = true)
    private boolean visible = true;

    public enum Type {
        HTML,
        REMOTE_ONLY,
        REMOTE_WITH_LOCAL_FALLBACK,
        LOCAL_ONLY;

        public static Type getFromText(String value) {
            for (Type type : values()) {
                if (type.name().equalsIgnoreCase(value)) {
                    return type;
                }
            }
            return getDefault();
        }

        public static Type getDefault() {
            return REMOTE_WITH_LOCAL_FALLBACK;
        }
    }

    public enum Mode {
        INTERNAL,
        ACTIVITY,
        BROWSER;

        public static Mode getFromText(String value) {
            for (Mode mode : values()) {
                if (mode.name().equalsIgnoreCase(value)) {
                    return mode;
                }
            }
            return getDefault();
        }

        public static Mode getDefault() {
            return INTERNAL;
        }
    }

    public String getLocalFilename() {
        return this.localFilename;
    }

    public String getRemoteUrl() {
        return this.remoteUrl;
    }

    public Type getType() {
        return Type.getFromText(this.type);
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public boolean isHeader() {
        return this.header;
    }

    public Mode getMode() {
        return Mode.getFromText(this.mode);
    }

    public boolean isVisible() {
        return this.visible;
    }

    public boolean isSubscribedOnly() {
        return this.subscribedOnly;
    }

    public String getHtmlContent() {
        return this.htmlContent;
    }

    public void setHtmlContent(String htmlContent2) {
        this.htmlContent = htmlContent2;
    }
}
