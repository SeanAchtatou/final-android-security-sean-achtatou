package ch.nth.android.contentabo.networking.content;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.models.utils.CommentPostBody;
import ch.nth.android.contentabo.models.utils.VideoSorting;
import ch.nth.android.contentabo.networking.content.ContentUtils;
import ch.nth.android.contentabo.translations.Translations;
import com.google.gson.Gson;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

public class RequestUtils {
    private static String getDmsBaseUrl() {
        return AppConfig.getInstance().getConfig().getDmsBaseUrl();
    }

    private static String getDmsPluginBaseUrl() {
        return AppConfig.getInstance().getConfig().getDmsPluginBaseUrl();
    }

    private static String getDefaultContentId() {
        return AppConfig.getInstance().getConfig().getDefaultContentId();
    }

    private static String getAccountId() {
        return AppConfig.getInstance().getConfig().getAccountId();
    }

    public static String getContentByIdUrl(Context context, String id) {
        return String.valueOf(getDmsBaseUrl()) + String.format(context.getString(R.string.content_by_id_pattern), id, getACUserId());
    }

    public static String getDefaultContent(Context context, int pageNumber, int pageSize, VideoSorting sorting) {
        String patternUrl = sorting.getDefaultPatternUrl();
        return String.valueOf(getDmsBaseUrl()) + String.format(patternUrl, getDefaultContentId(), getAccountId(), Integer.valueOf(pageNumber), Integer.valueOf(pageSize), getACUserId());
    }

    public static String getContentUrl(Context context) {
        return String.valueOf(getDmsBaseUrl()) + context.getString(R.string.content_pattern);
    }

    public static String getContentByCategoryUrl(Context context, String categoryId, int pageNumber, int pageSize, VideoSorting sorting) {
        String patternUrl = sorting.getPatternUrl();
        Utils.doLog("ContentAbo", String.valueOf(getDmsBaseUrl()) + String.format(patternUrl, categoryId, Integer.valueOf(pageNumber), Integer.valueOf(pageSize), getACUserId()));
        return String.valueOf(getDmsBaseUrl()) + String.format(patternUrl, categoryId, Integer.valueOf(pageNumber), Integer.valueOf(pageSize), getACUserId());
    }

    public static String getSearchUrl(Context context, String searchItem, int pageNumber, int pageSize, VideoSorting sorting) {
        String patternUrl = sorting.getSortingUrl();
        return String.valueOf(getDmsBaseUrl()) + String.format(patternUrl, searchItem, Integer.valueOf(pageNumber), Integer.valueOf(pageSize), getACUserId());
    }

    public static String getCategoriesUrl(Context context) {
        return String.valueOf(getDmsBaseUrl()) + String.format(context.getString(R.string.categories_pattern), getAccountId());
    }

    public static String getCommentsForContentIdUrl(Context context, String contentId) {
        return getCommentsForContentIdUrl(context, contentId, 0, 5);
    }

    public static String getCommentsForContentIdUrl(Context context, String contentId, int pageNumber, int pageSize) {
        return String.valueOf(getDmsBaseUrl()) + String.format(context.getString(R.string.comment_by_content_id_pattern), contentId, Integer.valueOf(pageNumber), Integer.valueOf(pageSize), getCommentCountry(context));
    }

    public static String getCommentCountForContentIdUrl(Context context, String contentId) {
        return String.valueOf(getDmsBaseUrl()) + String.format(context.getString(R.string.comment_count_stub_by_content_id_pattern), contentId, getCommentCountry(context));
    }

    public static String getRelatedVideosForParentContentTagUrl(Context context, String parentContentTag) {
        return getRelatedVideosForParentContentTagUrl(context, parentContentTag, 0, 5);
    }

    public static String getRelatedVideosForParentContentTagUrl(Context context, String parentContentTag, int pageNumber, int pageSize) {
        return String.valueOf(getDmsBaseUrl()) + String.format(context.getString(R.string.related_videos_for_parent_content_tag_pattern), parentContentTag, Integer.valueOf(pageNumber), Integer.valueOf(pageSize), getACUserId());
    }

    public static String getUpdateContentRatingUrl(Context context, String contentId, float rating) {
        return String.valueOf(getDmsPluginBaseUrl()) + String.format(Locale.ENGLISH, context.getString(R.string.update_content_rating_url_pattern), contentId, Float.valueOf(rating));
    }

    public static String getSubmitCommentUrl(Context context) {
        return String.valueOf(getDmsBaseUrl()) + String.format(context.getString(R.string.submit_comment_pattern), new Object[0]);
    }

    public static String getSubmitCommentPostBody(Context context, String contentId, String username, String commentText) {
        Gson gson = ContentUtils.createGsonInstance(new HashSet(EnumSet.of(ContentUtils.GsonTypeAdapter.DATE)));
        String commentsDmsCountry = context.getString(R.string.comments_dms_country);
        Map<String, String> comments = new HashMap<>();
        comments.put(commentsDmsCountry, commentText);
        return gson.toJson(new CommentPostBody(contentId, username, comments, "active", new Date(), getCommentCountry(context)));
    }

    private static String getCommentCountry(Context context) {
        String commentCountry = Translations.getPolyglot().getCurrentLanguage();
        if (TextUtils.isEmpty(commentCountry)) {
            return context.getString(R.string.comments_country);
        }
        return commentCountry;
    }

    public static String getContentIncrementDownloadCountUrl(Context context, String contentId) {
        return String.valueOf(getDmsPluginBaseUrl()) + String.format(context.getString(R.string.increment_content_download_count_pattern), contentId);
    }

    public static Map<String, String> createHeaders(Context context) {
        return createHeaders(context, true);
    }

    public static Map<String, String> createHeaders(Context context, boolean includeContentType) {
        Map<String, String> headers = new HashMap<>();
        String dmsAccessKey = AppConfig.getInstance().getConfig().getDmsAccessKey();
        headers.put("Authorization", "ApiKey " + dmsAccessKey + ":" + AppConfig.getInstance().getConfig().getDmsAccessCode());
        if (includeContentType) {
            headers.put("Content-Type", "application/json");
        }
        return headers;
    }

    private static TelephonyManager getTelephonyManager(Context context) {
        return (TelephonyManager) context.getSystemService("phone");
    }

    private static String getIMSI(Context context) {
        if (context == null) {
            return null;
        }
        return getTelephonyManager(context).getSubscriberId();
    }

    private static String getMSISDN(Context context) {
        if (context == null) {
            return null;
        }
        return getTelephonyManager(context).getLine1Number();
    }

    public static String getACUserId() {
        Context context = App.getInstance();
        String availableID = PaymentUtils.getFrid();
        if (!TextUtils.isEmpty(availableID) || !TextUtils.isEmpty(availableID)) {
            return availableID;
        }
        String availableID2 = ch.nth.android.scmsdk.utils.Utils.getApplicationUniqueId(context);
        if (TextUtils.isEmpty(availableID2)) {
            return "pw_app";
        }
        return availableID2;
    }

    private static String getFallbackACUserId() {
        Context context = App.getInstance();
        String availableID = ch.nth.android.scmsdk.utils.Utils.getSimSerial(context);
        if (TextUtils.isEmpty(availableID)) {
            String availableID2 = ch.nth.android.scmsdk.utils.Utils.getIMEI(context);
            if (!TextUtils.isEmpty(availableID2)) {
                return availableID2;
            }
            availableID = ch.nth.android.scmsdk.utils.Utils.getApplicationUniqueId(context);
            if (TextUtils.isEmpty(availableID)) {
                availableID = "pw_app";
            }
        }
        return availableID;
    }
}
