package ch.nth.android.contentabo.config.modules;

import android.text.TextUtils;
import ch.nth.android.contentabo.config.mas.MasConfig;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;

public class VideoDetailsModule {
    @Property(name = "comments_type")
    private String commentsType;
    private CommentType commentsTypeEnum;
    @Property(name = "default_video_quality")
    private String defaultVideoQuality;
    private Content.VideoFlavor defaultVideoQualityEnum;
    @Dictionary(name = "floating_mas_config", required = false)
    private MasConfig floatingMasConfig;
    @Property(name = "optin_flow_one", required = false)
    private String optinFlowOne;
    @Property(name = "optin_flow_one_plus_two", required = false)
    private String optinFlowOnePlusTwo;
    @Property(name = "optin_flow_terms_nwc", required = false)
    private String optinFlowTremsNwc;
    @Property(name = "optin_flow_terms_preselected", required = false, stringCompatibility = true)
    private boolean optinFlowTremsPreselected;
    @Property(name = "optin_flow_two", required = false)
    private String optinFlowTwo;
    @Property(name = "ratings_type")
    private String ratingsType;
    private RatingsType ratingsTypeEnum;
    @Property(name = "show_configuration_button")
    private boolean showConfigurationButton = false;
    @Property(name = "show_related_videos")
    private boolean showRelatedVideos = true;
    @Property(name = "show_video_description")
    private boolean showVideoDescription = true;
    @Property(name = "show_video_title")
    private boolean showVideoTitle = true;
    @Property(name = "subscription_dialog_nwc", required = false)
    private String subscriptionDialogNwc;
    @Property(name = "subscription_dialog_nwc_1", required = false)
    private String subscriptionDialogNwc1;
    @Property(name = "subscription_dialog_nwc_2", required = false)
    private String subscriptionDialogNwc2;

    public enum CommentType {
        ON,
        ONLY_VIEW,
        OFF;

        public static CommentType valueForNameOrDefault(String name) {
            CommentType type = valueForNameOrNull(name);
            return type != null ? type : getDefault();
        }

        public static CommentType valueForNameOrNull(String name) {
            if (TextUtils.isEmpty(name)) {
                return null;
            }
            for (CommentType type : values()) {
                if (type.name().equalsIgnoreCase(name)) {
                    return type;
                }
            }
            return null;
        }

        public static CommentType getDefault() {
            return ON;
        }
    }

    public enum RatingsType {
        ON,
        ONLY_VIEW,
        OFF;

        public static RatingsType valueForNameOrDefault(String name) {
            RatingsType type = valueForNameOrNull(name);
            return type != null ? type : getDefault();
        }

        public static RatingsType valueForNameOrNull(String name) {
            if (TextUtils.isEmpty(name)) {
                return null;
            }
            for (RatingsType type : values()) {
                if (type.name().equalsIgnoreCase(name)) {
                    return type;
                }
            }
            return null;
        }

        public static RatingsType getDefault() {
            return ON;
        }
    }

    public String getOptinFlowOne() {
        return this.optinFlowOne;
    }

    public String getOptinFlowTwo() {
        return this.optinFlowTwo;
    }

    public String getOptinFlowOnePlusTwo() {
        return this.optinFlowOnePlusTwo;
    }

    public CommentType getCommentType() {
        if (this.commentsTypeEnum == null) {
            this.commentsTypeEnum = CommentType.valueForNameOrDefault(this.commentsType);
        }
        return this.commentsTypeEnum;
    }

    public RatingsType getRatingsType() {
        if (this.ratingsTypeEnum == null) {
            this.ratingsTypeEnum = RatingsType.valueForNameOrDefault(this.ratingsType);
        }
        return this.ratingsTypeEnum;
    }

    public Content.VideoFlavor getDefaultVideoQuality() {
        if (this.defaultVideoQualityEnum == null) {
            this.defaultVideoQualityEnum = Content.VideoFlavor.valueForNameOrDefault(this.defaultVideoQuality);
        }
        return this.defaultVideoQualityEnum;
    }

    public boolean isShowRelatedVideos() {
        return this.showRelatedVideos;
    }

    public boolean isShowConfigurationButton() {
        return this.showConfigurationButton;
    }

    public boolean isShowVideoTitle() {
        return this.showVideoTitle;
    }

    public boolean isShowVideoDescription() {
        return this.showVideoDescription;
    }

    public String getSubscriptionDialogNwc() {
        return this.subscriptionDialogNwc;
    }

    public String getSubscriptionDialogNwc1() {
        return this.subscriptionDialogNwc1;
    }

    public String getSubscriptionDialogNwc2() {
        return this.subscriptionDialogNwc2;
    }

    public MasConfig getFloatingMasConfig() {
        if (this.floatingMasConfig == null) {
            this.floatingMasConfig = MasConfig.newDefaultInstance();
        }
        return this.floatingMasConfig;
    }

    public String getOptinFlowTremsNwc() {
        return this.optinFlowTremsNwc;
    }

    public boolean isOptinFlowTremsPreselected() {
        return this.optinFlowTremsPreselected;
    }
}
