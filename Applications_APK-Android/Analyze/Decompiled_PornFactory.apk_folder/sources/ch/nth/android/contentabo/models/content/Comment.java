package ch.nth.android.contentabo.models.content;

import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private static final long serialVersionUID = -176322950160290384L;
    @SerializedName(DmsConstants.ID)
    private String id;
    private String status;
    private String text;
    @SerializedName("time_created")
    private Date timeCreated;
    private String username;

    public enum Status {
        ACTIVE
    }

    public String getId() {
        return this.id;
    }

    public Date getTimeCreated() {
        return this.timeCreated;
    }

    public String getUsername() {
        return this.username;
    }

    public String getText() {
        return this.text;
    }

    public String getStatus() {
        return this.status;
    }
}
