package ch.nth.android.contentabo.models.utils;

import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.Map;

public class CommentPostBody {
    @SerializedName("text")
    private Map<String, String> commentTexts;
    @SerializedName("content")
    private String contentId;
    private String country;
    private String status;
    @SerializedName("time_created")
    private Date timeCreated;
    private String username;

    public CommentPostBody(String contentId2, String username2, Map<String, String> commentTexts2, String status2, Date timeCreated2, String country2) {
        this.contentId = contentId2;
        this.username = username2;
        this.commentTexts = commentTexts2;
        this.status = status2;
        this.timeCreated = timeCreated2;
        this.country = country2;
    }

    public String getContentId() {
        return this.contentId;
    }

    public String getUsername() {
        return this.username;
    }

    public Map<String, String> getCommentTexts() {
        return this.commentTexts;
    }

    public String getStatus() {
        return this.status;
    }

    public Date getTimeCreated() {
        return this.timeCreated;
    }

    public String getCountry() {
        return this.country;
    }
}
