package ch.nth.android.contentabo.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.fragments.base.BaseFragment;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class StaticWebAbstractFragment extends BaseFragment {
    private static final String ASSETS_FILE_LOCATION = "file:///android_asset/static/%s";
    private static String mBaseHtml;
    /* access modifiers changed from: private */
    public ProgressBar mProgressbar;
    /* access modifiers changed from: private */
    public WebView mWebView;

    public abstract String getFilename();

    public abstract String getRemoteContent();

    public abstract String getUrl();

    public abstract int getViewId();

    public abstract boolean useFilename();

    public abstract boolean useRemoteContent();

    public abstract boolean useUrl();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @SuppressLint({"SetJavaScriptEnabled"})
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getViewId(), container, false);
        this.mProgressbar = (ProgressBar) view.findViewById(R.id.progress_bar);
        this.mWebView = (WebView) view.findViewById(R.id.webview);
        this.mWebView.setBackgroundColor(0);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.setWebViewClient(new WebClient());
        this.mProgressbar.setVisibility(0);
        try {
            loadContent();
        } catch (Exception e) {
            Utils.doLogException(e);
        }
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(0);
        }
    }

    private void loadContent() throws IOException {
        if (useUrl()) {
            this.mWebView.loadUrl(getUrl());
        } else if (useFilename()) {
            this.mWebView.loadUrl(String.format(ASSETS_FILE_LOCATION, getFilename()));
        } else if (useRemoteContent()) {
            this.mWebView.loadDataWithBaseURL(null, getHtml(getRemoteContent()), "text/html", "utf-8", null);
        }
    }

    private synchronized String getHtml(String htmlBody) {
        if (TextUtils.isEmpty(mBaseHtml)) {
            InputStream is = getResources().openRawResource(R.raw.html_template);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            while (true) {
                try {
                    int read = is.read(buffer);
                    if (read == -1) {
                        break;
                    }
                    baos.write(buffer, 0, read);
                } catch (IOException e) {
                    mBaseHtml = "";
                }
            }
            mBaseHtml = baos.toString();
        }
        if (!TextUtils.isEmpty(mBaseHtml)) {
            htmlBody = mBaseHtml.replace("%XXX_BODY_XXX%", htmlBody);
        }
        return htmlBody;
    }

    class WebClient extends WebViewClient {
        WebClient() {
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            StaticWebAbstractFragment.this.mWebView.loadUrl(String.format(StaticWebAbstractFragment.ASSETS_FILE_LOCATION, StaticWebAbstractFragment.this.getFilename()));
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            StaticWebAbstractFragment.this.mProgressbar.setVisibility(8);
        }
    }
}
