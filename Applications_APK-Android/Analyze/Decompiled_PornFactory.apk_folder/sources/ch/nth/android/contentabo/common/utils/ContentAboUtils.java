package ch.nth.android.contentabo.common.utils;

import ch.nth.android.contentabo.App;

public class ContentAboUtils {
    public static final String PACKAGE = App.getInstance().getPackageName();
    public static final String PREFS_KEY_AGE_VERIFIED = (String.valueOf(PACKAGE) + ".ageVerified");
    public static final String PREFS_KEY_DUMMY_SUBSCRIBE_STATUS = (String.valueOf(PACKAGE) + ".dummySubscribeStatus");
    public static final String PREFS_KEY_DUMMY_SUBSCRIBE_STATUS_PASSIVE = (String.valueOf(PACKAGE) + ".dummySubscribeStatusPassive");
    public static final String PREFS_KEY_DUMMY_SUBSCRIBE_TIME = (String.valueOf(PACKAGE) + ".dummySubscribeTime");

    public static boolean isAgeVerified() {
        return PreferenceConnector.readBoolean(App.getInstance(), PREFS_KEY_AGE_VERIFIED, false);
    }

    public static void setAgeVerified() {
        PreferenceConnector.writeBoolean(App.getInstance(), PREFS_KEY_AGE_VERIFIED, true);
    }
}
