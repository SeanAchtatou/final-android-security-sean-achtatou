package ch.nth.android.contentabo.fragments;

import android.os.Bundle;
import ch.nth.android.contentabo.models.content.Content;

public abstract class VideoDetailsInfoAbstractFragment extends VideoDetailsAbstractFragment {
    public static final String EXTRA_COMMENTS_TYPE = "video.details.commentsType";
    public static final String EXTRA_RATINGS_TYPE = "video.details.ratingsType";
    public static final String EXTRA_VIDEO_DESCRIPTION_ENABLED = "video.details.videoDescriptionEnabled";
    public static final String EXTRA_VIDEO_TITLE_ENABLED = "video.details.videoTitleEnabled";

    public static Bundle getArgumentBundle(String contentId, Content content) {
        return VideoDetailsAbstractFragment.getArgumentBundle(contentId, content);
    }
}
