package ch.nth.android.contentabo.ui.utils;

import android.graphics.Typeface;
import java.util.Hashtable;

public class Typefaces {
    private static final String FONTS_FOLDER = "fonts/";
    private static final Hashtable<String, Typeface> cache = new Hashtable<>();

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Typeface get(android.content.Context r6, java.lang.String r7) {
        /*
            java.util.Hashtable<java.lang.String, android.graphics.Typeface> r3 = ch.nth.android.contentabo.ui.utils.Typefaces.cache
            monitor-enter(r3)
            java.util.Hashtable<java.lang.String, android.graphics.Typeface> r2 = ch.nth.android.contentabo.ui.utils.Typefaces.cache     // Catch:{ all -> 0x0035 }
            boolean r2 = r2.containsKey(r7)     // Catch:{ all -> 0x0035 }
            if (r2 != 0) goto L_0x0027
            android.content.res.AssetManager r2 = r6.getAssets()     // Catch:{ Exception -> 0x0031 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0031 }
            java.lang.String r5 = "fonts/"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0031 }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x0031 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0031 }
            android.graphics.Typeface r1 = android.graphics.Typeface.createFromAsset(r2, r4)     // Catch:{ Exception -> 0x0031 }
            java.util.Hashtable<java.lang.String, android.graphics.Typeface> r2 = ch.nth.android.contentabo.ui.utils.Typefaces.cache     // Catch:{ Exception -> 0x0031 }
            r2.put(r7, r1)     // Catch:{ Exception -> 0x0031 }
        L_0x0027:
            java.util.Hashtable<java.lang.String, android.graphics.Typeface> r2 = ch.nth.android.contentabo.ui.utils.Typefaces.cache     // Catch:{ all -> 0x0035 }
            java.lang.Object r2 = r2.get(r7)     // Catch:{ all -> 0x0035 }
            android.graphics.Typeface r2 = (android.graphics.Typeface) r2     // Catch:{ all -> 0x0035 }
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
        L_0x0030:
            return r2
        L_0x0031:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
            r2 = 0
            goto L_0x0030
        L_0x0035:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.contentabo.ui.utils.Typefaces.get(android.content.Context, java.lang.String):android.graphics.Typeface");
    }
}
