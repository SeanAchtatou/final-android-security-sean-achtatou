package ch.nth.android.contentabo.activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.view.MenuItem;
import ch.nth.android.contentabo.activities.base.BaseAbstractActivity;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.models.content.Content;
import java.io.Serializable;

public abstract class VideoDetailsAbstractActivity extends BaseAbstractActivity {
    public static final String EXTRA_CONTENT = "video.details.content";
    public static final String EXTRA_CONTENT_ID = "video.details.contentId";
    protected Content mContent;
    protected String mContentId;

    public static Bundle getExtrasBundle(String contentId, Content content) {
        Bundle args = new Bundle();
        args.putString("video.details.contentId", contentId);
        if (content != null) {
            args.putSerializable("video.details.content", content);
        }
        return args;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Serializable obj = extras.getSerializable("video.details.content");
            if (obj instanceof Content) {
                this.mContent = (Content) obj;
            }
            String extraContentId = extras.getString("video.details.contentId");
            if (!TextUtils.isEmpty(extraContentId)) {
                this.mContentId = extraContentId;
            }
        }
        getSupportActionBar().setTitle("");
        AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.VIDEO_DETAILS_SHOWEN_EVENT);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            return onUpPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public boolean onUpPressed() {
        NavUtils.navigateUpFromSameTask(this);
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
