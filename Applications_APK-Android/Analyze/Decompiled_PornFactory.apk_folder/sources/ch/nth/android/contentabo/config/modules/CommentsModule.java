package ch.nth.android.contentabo.config.modules;

import ch.nth.android.nthcommons.dms.api.DmsConstants;
import ch.nth.simpleplist.annotation.Property;

public class CommentsModule {
    @Property(name = DmsConstants.PAGE_SIZE, stringCompatibility = true)
    private int pageSize = 10;

    public int getPageSize() {
        return this.pageSize;
    }
}
