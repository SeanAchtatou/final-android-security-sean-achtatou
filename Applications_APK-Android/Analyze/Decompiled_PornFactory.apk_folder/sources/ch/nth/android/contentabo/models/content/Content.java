package ch.nth.android.contentabo.models.content;

import android.text.TextUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Content implements Serializable {
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureFlavor = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureSelectMode = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor = null;
    private static final long serialVersionUID = -9044019703968363321L;
    private Account account;
    @SerializedName("average_rating")
    private float avgRating;
    @SerializedName("category")
    private List<Category> categories;
    private String description;
    @SerializedName("download_count")
    private int downloadCount;
    private int duration;
    @SerializedName(DmsConstants.ID)
    private String id;
    private String name;
    @SerializedName("preview_picture_link")
    private List<String> previewPictures;
    @SerializedName("preview_thumb_link")
    private List<String> previewThumbnails;
    @SerializedName("tag")
    private List<Tag> tags;
    @SerializedName("time_imported")
    private Date timeImported;
    @SerializedName("download_link_hq")
    private String urlHQ;
    @SerializedName("download_link_lq")
    private String urlLQ;
    @SerializedName("votes_count")
    private int votesCount;

    public enum PictureFlavor {
        THUMB,
        FULL_PICTURE
    }

    public enum PictureSelectMode {
        FIRST,
        RANDOM
    }

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureFlavor() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureFlavor;
        if (iArr == null) {
            iArr = new int[PictureFlavor.values().length];
            try {
                iArr[PictureFlavor.FULL_PICTURE.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PictureFlavor.THUMB.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureFlavor = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureSelectMode() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureSelectMode;
        if (iArr == null) {
            iArr = new int[PictureSelectMode.values().length];
            try {
                iArr[PictureSelectMode.FIRST.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PictureSelectMode.RANDOM.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureSelectMode = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor;
        if (iArr == null) {
            iArr = new int[VideoFlavor.values().length];
            try {
                iArr[VideoFlavor.HQ.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[VideoFlavor.LQ.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor = iArr;
        }
        return iArr;
    }

    public enum VideoFlavor {
        LQ,
        HQ;

        public VideoFlavor getNext() {
            return values()[(ordinal() + 1) % values().length];
        }

        public static VideoFlavor valueForNameOrDefault(String name) {
            VideoFlavor flavor = valueForNameOrNull(name);
            return flavor != null ? flavor : getDefault();
        }

        public static VideoFlavor valueForNameOrNull(String name) {
            if (TextUtils.isEmpty(name)) {
                return null;
            }
            for (VideoFlavor flavor : values()) {
                if (flavor.name().equalsIgnoreCase(name)) {
                    return flavor;
                }
            }
            return null;
        }

        public static VideoFlavor getDefault() {
            return HQ;
        }
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public Date getTimeImported() {
        return this.timeImported;
    }

    public int getDuration() {
        return this.duration;
    }

    public String getUrlHQ() {
        return this.urlHQ;
    }

    public String getUrlLQ() {
        return this.urlLQ;
    }

    public int getDownloadCount() {
        return this.downloadCount;
    }

    public int getVotesCount() {
        return this.votesCount;
    }

    public float getAvgRating() {
        return this.avgRating;
    }

    public List<Tag> getTags() {
        return this.tags;
    }

    public Account getAccount() {
        return this.account;
    }

    public List<Category> getCategories() {
        return this.categories;
    }

    public List<String> getPreviewPictures() {
        return this.previewPictures;
    }

    public List<String> getPreviewThumbnails() {
        return this.previewThumbnails;
    }

    public String getPictureUrl(PictureFlavor flavor, PictureSelectMode mode) {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureFlavor()[flavor.ordinal()]) {
            case 1:
                return getThumbnailPicture(mode);
            case 2:
                return getPreviewPicture(mode);
            default:
                return null;
        }
    }

    public String getPreviewPicture(PictureSelectMode mode) {
        if (this.previewPictures == null || this.previewPictures.size() == 0) {
            return null;
        }
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureSelectMode()[mode.ordinal()]) {
            case 1:
                return this.previewPictures.get(0);
            case 2:
                return this.previewPictures.get(Utils.getPseudorandom(this.previewPictures.size()));
            default:
                return null;
        }
    }

    public String getThumbnailPicture(PictureSelectMode mode) {
        if (this.previewThumbnails == null || this.previewThumbnails.size() == 0) {
            return null;
        }
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$PictureSelectMode()[mode.ordinal()]) {
            case 1:
                return this.previewThumbnails.get(0);
            case 2:
                return this.previewThumbnails.get(Utils.getPseudorandom(this.previewThumbnails.size()));
            default:
                return null;
        }
    }

    public String getVideoUrl(VideoFlavor flavor) {
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$content$Content$VideoFlavor()[flavor.ordinal()]) {
            case 1:
                return this.urlLQ;
            case 2:
                return this.urlHQ;
            default:
                return null;
        }
    }

    public String toString() {
        boolean z;
        boolean z2 = true;
        StringBuilder append = new StringBuilder("id: ").append(this.id).append(" ; name: ").append(this.name).append(" ; duration: ").append(this.duration).append(" ; categories null: ");
        if (this.categories == null) {
            z = true;
        } else {
            z = false;
        }
        StringBuilder append2 = append.append(z).append(" ; tags null: ");
        if (this.tags != null) {
            z2 = false;
        }
        return append2.append(z2).toString();
    }
}
