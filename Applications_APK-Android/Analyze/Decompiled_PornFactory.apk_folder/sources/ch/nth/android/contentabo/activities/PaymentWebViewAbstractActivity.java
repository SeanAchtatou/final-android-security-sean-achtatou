package ch.nth.android.contentabo.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import ch.nth.android.contentabo.activities.base.PaymentAbstractActivity;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.payment.PMConfig;
import ch.nth.android.contentabo.models.payment.PaymentOption;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import ch.nth.android.paymentsdk.v2.model.GenericPaymentSession;
import ch.nth.android.paymentsdk.v2.model.PaymentSessionStatus;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import ch.nth.android.scmsdk.model.ScmSubscriptionSession;

public abstract class PaymentWebViewAbstractActivity extends PaymentAbstractActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }
        AnalyticsUtils.tagEvent(getBaseContext(), getAnalyticsSession(), AnalyticsUtils.PAYMENT_WEBVIEW_SHOWEN_EVENT);
        PaymentUtils.PaymentFlowStage flowStage = PaymentUtils.getPaymentFlowStage();
        if (flowStage == PaymentUtils.PaymentFlowStage.NONE || flowStage == PaymentUtils.PaymentFlowStage.INIT_PAYMENT_ERROR) {
            doInitPayment();
            return;
        }
        PaymentOption paymentOption = PaymentUtils.getPaymentOption(getBaseContext());
        if (paymentOption != null) {
            initPaymentContinueWithPredefinedData(new InitPaymentParams(paymentOption.getInitRedirectUrl(), paymentOption.getInitRequestId(), paymentOption.getInitCallbackUrl()), false);
        }
    }

    /* access modifiers changed from: protected */
    public void doInitPayment() {
        Utils.doLog("Payment", "doInitPayment()");
        PMConfig config = AppConfig.getInstance().getConfig().getPm();
        PaymentUtils.setPaymentFlowStage(getBaseContext(), PaymentUtils.PaymentFlowStage.INIT_PAYMENT_IN_PROGRESS);
        initPayment(config.getServiceCode(), ch.nth.android.scmsdk.utils.Utils.getApplicationUniqueId(getBaseContext()), false);
    }

    /* access modifiers changed from: protected */
    public void onSuccess(PaymentManagerSteps step, Object item) {
        Utils.doLog("Payment", "on success (activity), step " + step);
        if (step == PaymentManagerSteps.INIT_PAYMENT) {
            PaymentSessionStatus paymentSessionStatus = (PaymentSessionStatus) item;
            PaymentUtils.getPaymentOption(getBaseContext()).setSessionId(paymentSessionStatus.getSessionId());
            PaymentUtils.savePaymentOption(getBaseContext());
            verifyPayment(paymentSessionStatus.getSessionId());
        } else if (step == PaymentManagerSteps.VERIFY_PAYMENT) {
            GenericPaymentSession session = (GenericPaymentSession) item;
            Utils.doLog("Payment", "onSuccess" + session);
            if (PaymentUtils.isPaymentSessionValid(PaymentUtils.PaymentStatus.valueOfOrDefault(session.getStatus()))) {
                PaymentUtils.setSCMStatus(getBaseContext(), ScmSubscriptionSession.SessionStatus.OPEN, 1800);
            } else {
                PaymentUtils.setPaymentFlowStage(getBaseContext(), PaymentUtils.PaymentFlowStage.INIT_PAYMENT_ERROR);
            }
            setResult(-1);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onFailure(PaymentManagerSteps step, Object error) {
        super.onFailure(step, error);
        setResult(0);
        finish();
    }
}
