package ch.nth.android.contentabo.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.common.adapter.InfiniteBaseAdapter;
import ch.nth.android.contentabo.common.utils.FormatUtils;
import ch.nth.android.contentabo.models.content.Comment;
import ch.nth.android.contentabo.models.content.CommentList;
import java.util.ArrayList;
import java.util.List;

public class CommentListInfiniteAdapter extends InfiniteBaseAdapter {
    private final List<Comment> mComments = new ArrayList();
    private final LayoutInflater mInflater;
    private final int mLayoutResource;
    private int mTotal;

    public CommentListInfiniteAdapter(Context context, int layoutResource, CommentList commentList, InfiniteBaseAdapter.Listener listener) {
        this.mLayoutResource = layoutResource;
        this.mInflater = LayoutInflater.from(context);
        setInfiniteAdapterListener(listener);
        setData(commentList);
    }

    public void setData(CommentList commentList) {
        if (commentList != null && commentList.getData() != null) {
            this.mComments.clear();
            this.mComments.addAll(commentList.getData());
            this.mTotal = calculateNewTotal(commentList.getTotal());
            notifyDataSetChanged();
        }
    }

    public void appendData(CommentList commentList) {
        if (commentList != null && commentList.getData() != null) {
            this.mComments.addAll(commentList.getData());
            this.mTotal = calculateNewTotal(commentList.getTotal());
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return this.mComments.size();
    }

    public Comment getItem(int position) {
        return this.mComments.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate(this.mLayoutResource, parent, false);
            holder = new ViewHolder(null);
            holder.username = (TextView) convertView.findViewById(R.id.comment_list_item_username);
            holder.date = (TextView) convertView.findViewById(R.id.comment_list_item_date);
            holder.content = (TextView) convertView.findViewById(R.id.comment_list_item_content);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Comment comment = getItem(position);
        holder.username.setText(comment.getUsername());
        holder.date.setText(FormatUtils.formatLocalDate(App.getInstance(), comment.getTimeCreated()));
        holder.content.setText(comment.getText());
        checkAndReportLastPosition(position);
        return convertView;
    }

    private static class ViewHolder {
        public TextView content;
        public TextView date;
        public TextView username;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }

    public int getTotalItems() {
        return this.mTotal;
    }
}
