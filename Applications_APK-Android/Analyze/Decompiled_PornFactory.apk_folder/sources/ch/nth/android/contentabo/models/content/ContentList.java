package ch.nth.android.contentabo.models.content;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ContentList implements Serializable {
    private static final long serialVersionUID = 8434403095499996851L;
    private List<Content> data;
    private int total;

    public ContentList() {
    }

    public ContentList(int total2, List<Content> data2) {
        this.total = total2;
        this.data = data2;
    }

    public int getTotal() {
        return this.total;
    }

    public List<Content> getData() {
        if (this.data == null) {
            this.data = new ArrayList();
        }
        return this.data;
    }
}
