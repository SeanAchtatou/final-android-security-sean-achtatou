package ch.nth.android.contentabo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.fragments.base.BaseDialogFragment;
import ch.nth.android.contentabo.models.content.CommentList;
import ch.nth.android.contentabo.models.utils.VotingActionsManager;
import ch.nth.android.contentabo.networking.content.GsonRequest;
import ch.nth.android.contentabo.networking.content.RequestUtils;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import java.util.HashMap;

public abstract class VotingDialogAbstractFragment extends BaseDialogFragment implements Response.Listener<CommentList>, Response.ErrorListener, RatingBar.OnRatingBarChangeListener {
    protected static final String ARG_ALERT_DIALOG_CONTENT_ID = "alert_dialog_content_id";
    protected static final String ARG_ALERT_DIALOG_LAYOUT = "alert_dialog_layout";
    protected static final int FAIL_REASON_ALREADY_VOTED = 3;
    protected static final int FAIL_REASON_RESPONSE = 1;
    protected static final int FAIL_REASON_VOLLEY = 0;
    protected static final int FAIL_REASON_VOTE_PENDING = 2;
    protected String mContentId;
    private int mDialogLayout;
    private boolean mDidAlreadyVoteSuccessfully;
    private boolean mIsVoteSendingInProgress;
    protected RatingBar mRatingBar;
    private float mSelectedRating;

    /* access modifiers changed from: protected */
    public abstract void createLayoutHooks(View view);

    /* access modifiers changed from: protected */
    public abstract boolean getCanceledOnTouchOutside();

    /* access modifiers changed from: protected */
    public abstract void onVoteFailed(String str, int i);

    /* access modifiers changed from: protected */
    public abstract void onVoteSuccessfull();

    /* access modifiers changed from: protected */
    public abstract boolean submitVoteOnRatingBarChanged();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mDialogLayout = getArguments().getInt("alert_dialog_layout");
        this.mContentId = getArguments().getString(ARG_ALERT_DIALOG_CONTENT_ID);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mDialogLayout, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().setCanceledOnTouchOutside(getCanceledOnTouchOutside());
        createLayoutHooks(view);
        return view;
    }

    /* access modifiers changed from: protected */
    public void setRatingBar(RatingBar ratingBar) {
        if (ratingBar != null) {
            this.mRatingBar = ratingBar;
            this.mRatingBar.setOnRatingBarChangeListener(this);
        }
    }

    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        this.mSelectedRating = rating;
        if (submitVoteOnRatingBarChanged()) {
            submitVote();
        }
    }

    /* access modifiers changed from: protected */
    public void submitVote() {
        submitVote(this.mSelectedRating);
        HashMap<String, String> attributes = new HashMap<>();
        attributes.put("contentId", this.mContentId);
        attributes.put("rating", String.valueOf(this.mSelectedRating));
        AnalyticsUtils.tagEvent(getActivity(), getAnalyticsSession(), AnalyticsUtils.VOTING_EVENT, attributes);
    }

    /* access modifiers changed from: protected */
    public void submitVote(float rating) {
        if (this.mIsVoteSendingInProgress) {
            onVoteFailed("vote sending in progress", 2);
        } else if (this.mDidAlreadyVoteSuccessfully || VotingActionsManager.getInstance().hasEntry(this.mContentId)) {
            onVoteFailed("already voted", 3);
        } else {
            this.mIsVoteSendingInProgress = true;
            this.mRatingBar.setIsIndicator(true);
            addRequest(new GsonRequest<>(getActivity(), RequestUtils.getUpdateContentRatingUrl(getActivity(), this.mContentId, rating), CommentList.class, null, this, this));
        }
    }

    public void onResponse(CommentList response) {
        this.mIsVoteSendingInProgress = false;
        this.mDidAlreadyVoteSuccessfully = true;
        VotingActionsManager.getInstance().addEntry(this.mContentId);
        onVoteSuccessfull();
    }

    public void onErrorResponse(VolleyError error) {
        this.mIsVoteSendingInProgress = false;
        this.mRatingBar.setIsIndicator(false);
        onVoteFailed(error.getMessage(), 0);
    }
}
