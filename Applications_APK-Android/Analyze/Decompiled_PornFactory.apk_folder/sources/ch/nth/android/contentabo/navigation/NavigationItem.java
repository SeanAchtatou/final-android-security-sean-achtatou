package ch.nth.android.contentabo.navigation;

import ch.nth.android.contentabo.config.menu.RemoteMenu;
import ch.nth.android.contentabo.models.content.Category;

public class NavigationItem {
    private Category mCategory;
    private boolean mHeader;
    private int mImageId;
    private String mKey;
    private RemoteMenu mRemoteMenu;
    private String mTitle;

    public NavigationItem(String title) {
        this.mTitle = title;
    }

    public NavigationItem(int imageId, String title) {
        this.mImageId = imageId;
        this.mTitle = title;
    }

    public NavigationItem(int imageId, String title, String key) {
        this.mImageId = imageId;
        this.mTitle = title;
        this.mKey = key;
    }

    public NavigationItem(int imageId, String title, boolean header) {
        this.mImageId = imageId;
        this.mTitle = title;
        this.mHeader = header;
    }

    public NavigationItem(int imageId, String title, boolean header, Category category) {
        this.mImageId = imageId;
        this.mTitle = title;
        this.mHeader = header;
        this.mCategory = category;
    }

    public NavigationItem(int imageId, String title, boolean header, RemoteMenu remoteMenu) {
        this.mImageId = imageId;
        this.mTitle = title;
        this.mHeader = header;
        this.mRemoteMenu = remoteMenu;
    }

    public int getImageId() {
        return this.mImageId;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public boolean isHeader() {
        return this.mHeader;
    }

    public Category getCategory() {
        return this.mCategory;
    }

    public RemoteMenu getRemoteMenu() {
        return this.mRemoteMenu;
    }

    public String getKey() {
        return this.mKey;
    }
}
