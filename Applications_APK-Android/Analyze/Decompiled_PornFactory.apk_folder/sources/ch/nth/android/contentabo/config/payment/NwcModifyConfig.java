package ch.nth.android.contentabo.config.payment;

import android.text.TextUtils;
import ch.nth.simpleplist.annotation.Property;

public class NwcModifyConfig {
    @Property(name = "from_nwc_1", required = false)
    private String fromNwc1;
    @Property(name = "from_nwc_2", required = false)
    private String fromNwc2;
    @Property(name = "from_nwc_3", required = false)
    private String fromNwc3;
    @Property(name = "from_nwc_4", required = false)
    private String fromNwc4;
    @Property(name = "from_nwc_5", required = false)
    private String fromNwc5;
    @Property(name = "from_nwc_6", required = false)
    private String fromNwc6;
    @Property(name = "to_nwc_1", required = false)
    private String toNwc1;
    @Property(name = "to_nwc_2", required = false)
    private String toNwc2;
    @Property(name = "to_nwc_3", required = false)
    private String toNwc3;
    @Property(name = "to_nwc_4", required = false)
    private String toNwc4;
    @Property(name = "to_nwc_5", required = false)
    private String toNwc5;
    @Property(name = "to_nwc_6", required = false)
    private String toNwc6;

    public String getRealNwc(String fromNwc) {
        if (TextUtils.isEmpty(fromNwc)) {
            return fromNwc;
        }
        if (fromNwc.equals(this.fromNwc1)) {
            return this.toNwc1;
        }
        if (fromNwc.equals(this.fromNwc2)) {
            return this.toNwc2;
        }
        if (fromNwc.equals(this.fromNwc3)) {
            return this.toNwc3;
        }
        if (fromNwc.equals(this.fromNwc4)) {
            return this.toNwc4;
        }
        if (fromNwc.equals(this.fromNwc5)) {
            return this.toNwc5;
        }
        if (fromNwc.equals(this.fromNwc6)) {
            return this.toNwc6;
        }
        return fromNwc;
    }
}
