package ch.nth.android.contentabo.translations;

public final class D {

    public final class string {
        public static final String incoming_message_pattern = "incoming_message_pattern";
        public static final String limit_disclaimer = "limit_disclaimer";
        public static final String menu_entry_1_content = "menu_entry_1_content";
        public static final String menu_entry_1_title = "menu_entry_1_title";
        public static final String menu_entry_2_content = "menu_entry_2_content";
        public static final String menu_entry_2_title = "menu_entry_2_title";
        public static final String menu_entry_3_content = "menu_entry_3_content";
        public static final String menu_entry_3_title = "menu_entry_3_title";
        public static final String menu_entry_4_content = "menu_entry_4_content";
        public static final String menu_entry_4_title = "menu_entry_4_title";
        public static final String no = "no";
        public static final String optin_dialog_title = "optin_dialog_title";
        public static final String outgoing_message_content = "outgoing_message_content";
        public static final String outgoing_message_pattern = "outgoing_message_pattern";
        public static final String play = "play";
        public static final String ro_web_view_flow = "ro_web_view";
        public static final String subscription_price_text = "subscription_price_text";
        public static final String termsco = "termsco";
        public static final String video_details_button = "video_details_button";
        public static final String video_details_confirm_dialog_footer = "video_details_confirm_dialog_footer";
        public static final String video_details_confirm_dialog_header = "video_details_confirm_dialog_header";
        public static final String video_details_confirm_edittext_error = "video_details_confirm_edittext_error";
        public static final String video_details_disclaimer = "video_details_disclaimer";
        public static final String video_list_disclaimer = "video_list_disclaimer";
        public static final String yes = "yes";

        public string() {
        }
    }
}
