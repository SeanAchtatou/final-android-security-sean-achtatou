package ch.nth.android.contentabo.config.modules;

import ch.nth.android.contentabo.config.mas.ListviewMasConfig;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;

public class RelatedVideosModule {
    @Dictionary(name = "listview_mas_config", required = false)
    private ListviewMasConfig masConfig;
    @Property(name = DmsConstants.PAGE_SIZE, stringCompatibility = true)
    private int pageSize = 6;

    public int getPageSize() {
        return this.pageSize;
    }

    public ListviewMasConfig getMasConfig() {
        if (this.masConfig == null) {
            return ListviewMasConfig.newDefaultInstance();
        }
        return this.masConfig;
    }
}
