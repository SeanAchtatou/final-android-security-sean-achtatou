package ch.nth.android.contentabo.config;

import android.text.TextUtils;
import ch.nth.android.contentabo.config.addons.Addons;
import ch.nth.android.contentabo.config.menu.RemoteMenu;
import ch.nth.android.contentabo.config.modules.Modules;
import ch.nth.android.contentabo.config.panic.LimitCheckConfig;
import ch.nth.android.contentabo.config.panic.PanicConfig;
import ch.nth.android.contentabo.config.payment.PMConfig;
import ch.nth.android.contentabo.config.payment.SCMConfig;
import ch.nth.android.contentabo.config.update.EntStoreConfig;
import ch.nth.android.contentabo.translations.D;
import ch.nth.android.contentabo.translations.Disclaimers;
import ch.nth.simpleplist.annotation.Array;
import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;
import com.google.android.gms.plus.PlusShare;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlistConfig {
    @Property(name = "account_id")
    private String accountId;
    @Dictionary(name = "addons")
    private Addons addons;
    @Property(name = "allowed_mccmnc_regex")
    private String allowedMccMnc;
    @Property(name = "allowed_mccmnc_regex_1", required = false)
    private String allowedMccMnc1;
    @Property(name = "allowed_mccmnc_regex_2", required = false)
    private String allowedMccMnc2;
    @Property(name = "apikey", stringCompatibility = true)
    private String apiKey;
    @Property(name = "default_content_id")
    private String defaultContentId;
    @Property(name = "delay_after_sms_delivered", required = false, stringCompatibility = true)
    private int delayAfterSmsDelivered = 1000;
    @Property(name = "dms_access_code")
    private String dmsAccessCode;
    @Property(name = "dms_access_key")
    private String dmsAccessKey;
    @Property(name = "dms_base_url")
    private String dmsBaseUrl;
    @Property(name = "dms_disclaimers_access_code", required = false)
    private String dmsDisclaimersAccessCode;
    @Property(name = "dms_disclaimers_access_key", required = false)
    private String dmsDisclaimersAccessKey;
    @Property(name = "dms_disclaimers_url")
    private String dmsDisclaimersUrl;
    @Property(name = "dms_plugin_base_url")
    private String dmsPluginBaseUrl;
    @Property(name = "dms_translations_access_code")
    private String dmsTranslationsAccessCode;
    @Property(name = "dms_translations_access_key")
    private String dmsTranslationsAccessKey;
    @Property(name = "dms_translations_url")
    private String dmsTranslationsUrl;
    @Dictionary(name = "ent_store", required = false)
    private EntStoreConfig entStore;
    @Property(name = "gcm_project_id", required = false)
    private String gcmProjectId;
    @Property(name = "imsi_date", required = false)
    private String imsiDate;
    @Dictionary(name = "limit", required = false)
    private LimitCheckConfig limitCheck;
    @Dictionary(name = "modules")
    private Modules modules;
    @Dictionary(name = "panic")
    private PanicConfig panic;
    @Dictionary(name = "pm", required = false)
    private PMConfig pm;
    @Property(name = PlusShare.KEY_CALL_TO_ACTION_URL, path = "addons/pushnotification")
    private String pushNotificationsUrl;
    @Dictionary(name = "menu_entry_1", required = false)
    private RemoteMenu remoteMenuNo1;
    @Dictionary(name = "menu_entry_2", required = false)
    private RemoteMenu remoteMenuNo2;
    @Dictionary(name = "menu_entry_3", required = false)
    private RemoteMenu remoteMenuNo3;
    @Dictionary(name = "menu_entry_4", required = false)
    private RemoteMenu remoteMenuNo4;
    @Array(clazz = RemoteMenu.class, dictionary = true, name = "menu_entries", required = false)
    private List<RemoteMenu> remoteMenus;
    @Dictionary(name = "scm", required = false)
    private SCMConfig scm;

    public Addons getAddons() {
        if (this.addons == null) {
            return new Addons();
        }
        return this.addons;
    }

    public String getPushNotificationsUrl() {
        if (this.pushNotificationsUrl == null) {
            return "";
        }
        return this.pushNotificationsUrl;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public String getDmsBaseUrl() {
        if (this.dmsBaseUrl == null) {
            this.dmsBaseUrl = "";
        }
        return this.dmsBaseUrl;
    }

    public String getDmsPluginBaseUrl() {
        if (this.dmsPluginBaseUrl == null) {
            this.dmsPluginBaseUrl = "";
        }
        return this.dmsPluginBaseUrl;
    }

    public String getDmsAccessKey() {
        if (this.dmsAccessKey == null) {
            return "";
        }
        return this.dmsAccessKey;
    }

    public String getDmsAccessCode() {
        if (this.dmsAccessCode == null) {
            return "";
        }
        return this.dmsAccessCode;
    }

    public String getDmsTranslationsUrl() {
        return this.dmsTranslationsUrl;
    }

    public String getDmsDisclaimersUrl() {
        return this.dmsDisclaimersUrl;
    }

    public String getDmsTranslationsAccessKey() {
        return this.dmsTranslationsAccessKey;
    }

    public String getDmsTranslationsAccessCode() {
        return this.dmsTranslationsAccessCode;
    }

    public String getDmsDisclaimersAccessKey() {
        if (TextUtils.isEmpty(this.dmsDisclaimersAccessKey)) {
            return this.dmsTranslationsAccessKey;
        }
        return this.dmsDisclaimersAccessKey;
    }

    public String getDmsDisclaimersAccessCode() {
        if (TextUtils.isEmpty(this.dmsDisclaimersAccessCode)) {
            return this.dmsTranslationsAccessCode;
        }
        return this.dmsDisclaimersAccessCode;
    }

    public String getDefaultContentId() {
        if (this.defaultContentId == null) {
            this.defaultContentId = "";
        }
        return this.defaultContentId;
    }

    public String getAccountId() {
        if (this.accountId == null) {
            this.accountId = "";
        }
        return this.accountId;
    }

    public String getAllowedMccMnc() {
        if (this.allowedMccMnc == null) {
            this.allowedMccMnc = "";
        }
        return this.allowedMccMnc;
    }

    public String getAllowedMccMnc1() {
        if (this.allowedMccMnc1 == null) {
            this.allowedMccMnc1 = "";
        }
        return this.allowedMccMnc1;
    }

    public String getAllowedMccMnc2() {
        if (this.allowedMccMnc2 == null) {
            this.allowedMccMnc2 = "";
        }
        return this.allowedMccMnc2;
    }

    public String getGcmProjectId() {
        if (this.gcmProjectId == null) {
            this.gcmProjectId = "";
        }
        return this.gcmProjectId;
    }

    public String getImsiDate() {
        if (this.imsiDate == null) {
            this.imsiDate = "";
        }
        return this.imsiDate;
    }

    public int getDelayAfterSmsDelivered() {
        return this.delayAfterSmsDelivered;
    }

    public PMConfig getPm() {
        if (this.pm == null) {
            return new PMConfig();
        }
        return this.pm;
    }

    public SCMConfig getSCM() {
        if (this.scm == null) {
            return new SCMConfig();
        }
        return this.scm;
    }

    public EntStoreConfig getEntStore() {
        if (this.entStore == null) {
            return new EntStoreConfig();
        }
        return this.entStore;
    }

    public PanicConfig getPanic() {
        if (this.panic == null) {
            return new PanicConfig();
        }
        return this.panic;
    }

    public LimitCheckConfig getLimitCheck() {
        if (this.limitCheck == null) {
            return new LimitCheckConfig();
        }
        return this.limitCheck;
    }

    public Modules getModules() {
        if (this.modules == null) {
            return new Modules();
        }
        return this.modules;
    }

    public List<RemoteMenu> getRemoteMenus() {
        if (this.remoteMenus == null) {
            this.remoteMenus = new ArrayList();
            if (this.remoteMenuNo1 != null) {
                this.remoteMenuNo1.setTitle(Disclaimers.getPolyglot().getString(D.string.menu_entry_1_title));
                this.remoteMenuNo1.setHtmlContent(Disclaimers.getPolyglot().getString(D.string.menu_entry_1_content));
            }
            if (this.remoteMenuNo2 != null) {
                this.remoteMenuNo2.setTitle(Disclaimers.getPolyglot().getString(D.string.menu_entry_2_title));
                this.remoteMenuNo2.setHtmlContent(Disclaimers.getPolyglot().getString(D.string.menu_entry_2_content));
            }
            if (this.remoteMenuNo3 != null) {
                this.remoteMenuNo3.setTitle(Disclaimers.getPolyglot().getString(D.string.menu_entry_3_title));
                this.remoteMenuNo3.setHtmlContent(Disclaimers.getPolyglot().getString(D.string.menu_entry_3_content));
            }
            if (this.remoteMenuNo4 != null) {
                this.remoteMenuNo4.setTitle(Disclaimers.getPolyglot().getString(D.string.menu_entry_4_title));
                this.remoteMenuNo4.setHtmlContent(Disclaimers.getPolyglot().getString(D.string.menu_entry_4_content));
            }
            for (RemoteMenu remoteMenu : new ArrayList<>(Arrays.asList(this.remoteMenuNo1, this.remoteMenuNo2, this.remoteMenuNo3, this.remoteMenuNo4))) {
                if (remoteMenu != null) {
                    this.remoteMenus.add(remoteMenu);
                }
            }
        }
        return this.remoteMenus;
    }
}
