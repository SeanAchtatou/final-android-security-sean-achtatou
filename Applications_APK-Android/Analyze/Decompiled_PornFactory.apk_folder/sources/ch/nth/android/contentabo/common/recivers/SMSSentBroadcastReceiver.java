package ch.nth.android.contentabo.common.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.models.payment.PaymentOption;
import ch.nth.android.scmsdk.model.ScmSubscriptionSession;

public class SMSSentBroadcastReceiver extends BroadcastReceiver {
    public static final String CANCEL_PAYMENT_SENT = "ch.nth.android.action.CANCEL_PAYMENT_SENT";
    public static final String FIRST_SMS_SENT = "ch.nth.android.action.FIRST_SMS_SENT";
    public static final String SECOND_SMS_SENT = "ch.nth.android.action.SECOND_SMS_SENT";

    public void onReceive(Context context, Intent intent) {
        boolean succesfull;
        PaymentUtils.PaymentFlowStage paymentFlowStage;
        PaymentUtils.PaymentFlowStage paymentFlowStage2;
        PaymentUtils.PaymentFlowStage paymentFlowStage3;
        Utils.doLog("Payment SMS-Sent", "onReceive()");
        try {
            switch (getResultCode()) {
                case -1:
                    succesfull = true;
                    break;
                default:
                    succesfull = false;
                    break;
            }
            Utils.doLog("Payment SMS-Sent", "Activity.RESULT_OK: " + succesfull);
            PaymentOption.PaymentFlowType flowType = PaymentUtils.getPaymentOption(context).getPaymentFlowType();
            Utils.doLog("Payment SMS-Sent", "flowType: " + flowType);
            Utils.doLog("Payment SMS-Sent", "intentAction: " + intent.getAction());
            if (flowType == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
                if (intent.getAction().equals(FIRST_SMS_SENT)) {
                    Utils.doLog("Payment SMS-Sent", "DOUBLE_MO_OPTIN, FIRST_SMS_SENT");
                    if (succesfull) {
                        paymentFlowStage3 = PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY;
                    } else {
                        paymentFlowStage3 = PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_ERROR;
                    }
                    PaymentUtils.setPaymentFlowStage(context, paymentFlowStage3);
                } else if (intent.getAction().equals(SECOND_SMS_SENT)) {
                    if (succesfull) {
                        paymentFlowStage2 = PaymentUtils.PaymentFlowStage.SECOND_MO_SENT_SUCCESSFULLY;
                    } else {
                        paymentFlowStage2 = PaymentUtils.PaymentFlowStage.SECOND_MO_SEND_ERROR;
                    }
                    PaymentUtils.setPaymentFlowStage(context, paymentFlowStage2);
                    PaymentUtils.setSCMStatus(context, ScmSubscriptionSession.SessionStatus.OPEN, 1800);
                    PaymentUtils.setPaymentFlowStage(context, PaymentUtils.PaymentFlowStage.SECOND_MO_SENT_SUCCESSFULLY);
                }
            } else if (flowType == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN && intent.getAction().equals(FIRST_SMS_SENT)) {
                Utils.doLog("Payment SMS-Sent", "SINGLE_MO_OPTIN, FIRST_SMS_SENT");
                if (succesfull) {
                    paymentFlowStage = PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY;
                } else {
                    paymentFlowStage = PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_ERROR;
                }
                PaymentUtils.setPaymentFlowStage(context, paymentFlowStage);
            }
            PaymentUtils.savePaymentOption(context);
        } catch (Exception ex) {
            Utils.doLogException(ex);
        }
    }
}
