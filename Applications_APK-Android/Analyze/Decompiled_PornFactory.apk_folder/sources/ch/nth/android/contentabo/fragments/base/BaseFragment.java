package ch.nth.android.contentabo.fragments.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.Toast;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.activities.base.HomeAbstractActivity;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.mas.MasConfig;
import ch.nth.android.contentabo.config.payment.PMConfig;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import ch.nth.android.paymentsdk.v2.fragments.base.BasePaymentFragment;
import ch.nth.android.scmsdk.SCM;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.nth.analytics.android.LocalyticsSession;
import com.nth.android.mas.MASLayout;

public abstract class BaseFragment extends BasePaymentFragment {
    public abstract void updateTitle();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PMConfig config = AppConfig.getInstance().getConfig().getPm();
        setConfigData(config.getUrl(), config.getApiKey(), config.getApiSecret());
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public RequestQueue getRequestQueue() {
        return App.getInstance().getQueue();
    }

    /* access modifiers changed from: protected */
    public void addRequest(Request<?> request) {
        App.getInstance().addRequest(request);
    }

    /* access modifiers changed from: protected */
    public void showToastSafe(String message) {
        if (getActivity() != null && isVisible()) {
            Toast.makeText(getActivity(), message, 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void showToastSafe(int stringResource) {
        if (getActivity() != null && isVisible()) {
            Toast.makeText(getActivity(), getString(stringResource), 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    /* access modifiers changed from: protected */
    public void setupMasLayout(MasConfig masConfig, ViewGroup container) {
        setupMasLayout(masConfig, container, (ViewGroup.LayoutParams) null);
    }

    /* access modifiers changed from: protected */
    public void setupMasLayout(MasConfig masConfig, ViewGroup container, ViewGroup.LayoutParams layoutParams) {
        if (masConfig != null && masConfig.isEnabled()) {
            setupMasLayout(masConfig.getKey(), masConfig.getConfigUrl(), container);
        }
    }

    /* access modifiers changed from: protected */
    public void setupMasLayout(String masKey, String masConfigUrl, ViewGroup container) {
        setupMasLayout(masKey, masConfigUrl, container, null);
    }

    /* access modifiers changed from: protected */
    public void setupMasLayout(String masKey, String masConfigUrl, ViewGroup container, ViewGroup.LayoutParams layoutParams) {
        MASLayout masAd;
        if (container != null && !TextUtils.isEmpty(masKey)) {
            if (!TextUtils.isEmpty(masConfigUrl)) {
                masAd = new MASLayout(getActivity(), masKey, masConfigUrl);
            } else {
                masAd = new MASLayout(getActivity(), masKey);
            }
            if (layoutParams == null) {
                container.addView(masAd);
            } else {
                container.addView(masAd, layoutParams);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean isTestMode() {
        if (getString(R.string.is_test_mode).equalsIgnoreCase("true")) {
            return true;
        }
        return false;
    }

    public LocalyticsSession getAnalyticsSession() {
        return App.getInstance().getAnalyticsSession();
    }

    /* access modifiers changed from: protected */
    public SCM getSCM() {
        return App.getInstance().getSCM();
    }

    public void closeNavigationDrawer() {
        Activity activity = getActivity();
        if (activity instanceof HomeAbstractActivity) {
            ((HomeAbstractActivity) activity).closeNavigationDrawer();
        }
    }

    /* access modifiers changed from: protected */
    public int getWebViewLayoutResource() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getWebViewResourceId() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onSuccess(PaymentManagerSteps step, Object item) {
    }

    /* access modifiers changed from: protected */
    public void onFailure(PaymentManagerSteps step, Object error) {
    }
}
