package ch.nth.android.contentabo.common.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

@SuppressLint({"InlinedApi"})
public class PreferenceConnector {
    public static final String PREF_NAME = "CONTENTABO_PREFERENCES";

    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();
    }

    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();
    }

    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    public static synchronized void writeLong(Context context, String key, long value) {
        synchronized (PreferenceConnector.class) {
            getEditor(context).putLong(key, value).commit();
        }
    }

    public static synchronized long readLong(Context context, String key, long defValue) {
        long j;
        synchronized (PreferenceConnector.class) {
            j = getPreferences(context).getLong(key, defValue);
        }
        return j;
    }

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, Build.VERSION.SDK_INT >= 11 ? 4 : 0);
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }
}
