package ch.nth.android.contentabo.config.modules;

import ch.nth.android.contentabo.config.content.EmbeddedContentConfig;
import ch.nth.android.contentabo.config.mas.ListviewMasConfig;
import ch.nth.android.contentabo.config.mas.MasConfig;
import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;

public class VideoListModule {
    @Property(name = "show_category")
    private boolean categoryVisibile = true;
    @Dictionary(name = "embedded_content_config", required = false)
    private EmbeddedContentConfig embeddedContentConfig;
    @Dictionary(name = "floating_mas_config", required = false)
    private MasConfig floatingMasConfig;
    @Property(name = "initial_load_count", stringCompatibility = true)
    private int initialLoadCount = 6;
    @Dictionary(name = "listview_mas_config", required = false)
    private ListviewMasConfig listMasConfig;
    @Property(name = "show_number_of_views")
    private boolean numberOfViewsVisibile = true;
    @Property(name = "show_rating")
    private boolean ratingVisibile = true;
    @Property(name = "subscription_status_cache_hours", stringCompatibility = true)
    private int subscriptionStatusCacheHours = 24;
    @Property(name = "show_title")
    private boolean titleVisibile = true;

    public int getInitialLoadCount() {
        return this.initialLoadCount;
    }

    public int getSubscriptionStatusCacheHours() {
        return this.subscriptionStatusCacheHours;
    }

    public boolean isCategoryVisibile() {
        return this.categoryVisibile;
    }

    public boolean isNumberOfViewsVisibile() {
        return this.numberOfViewsVisibile;
    }

    public boolean isRatingVisibile() {
        return this.ratingVisibile;
    }

    public boolean isTitleVisibile() {
        return this.titleVisibile;
    }

    public ListviewMasConfig getListMasConfig() {
        if (this.listMasConfig == null) {
            this.listMasConfig = ListviewMasConfig.newDefaultInstance();
        }
        return this.listMasConfig;
    }

    public MasConfig getFloatingMasConfig() {
        if (this.floatingMasConfig == null) {
            this.floatingMasConfig = MasConfig.newDefaultInstance();
        }
        return this.floatingMasConfig;
    }

    public EmbeddedContentConfig getEmbeddedContentConfig() {
        if (this.embeddedContentConfig == null) {
            this.embeddedContentConfig = EmbeddedContentConfig.newDefaultInstance();
        }
        return this.embeddedContentConfig;
    }
}
