package ch.nth.android.contentabo.fragments.base;

import android.support.v4.app.DialogFragment;
import android.widget.Toast;
import ch.nth.android.contentabo.App;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.nth.analytics.android.LocalyticsSession;

public class BaseDialogFragment extends DialogFragment {
    /* access modifiers changed from: protected */
    public RequestQueue getRequestQueue() {
        return App.getInstance().getQueue();
    }

    /* access modifiers changed from: protected */
    public void addRequest(Request<?> request) {
        App.getInstance().addRequest(request);
    }

    /* access modifiers changed from: protected */
    public void showToastSafe(String message) {
        if (getActivity() != null && isVisible()) {
            Toast.makeText(getActivity(), message, 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void showToastSafe(int stringResource) {
        if (getActivity() != null && isVisible()) {
            Toast.makeText(getActivity(), getString(stringResource), 0).show();
        }
    }

    public LocalyticsSession getAnalyticsSession() {
        return App.getInstance().getAnalyticsSession();
    }
}
