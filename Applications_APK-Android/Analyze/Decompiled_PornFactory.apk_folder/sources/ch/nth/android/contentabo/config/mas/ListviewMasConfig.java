package ch.nth.android.contentabo.config.mas;

import ch.nth.simpleplist.annotation.Property;

public class ListviewMasConfig extends MasConfig {
    @Property(name = "repeat_every_x_items", stringCompatibility = true)
    private int repeatEveryXItems;

    public static ListviewMasConfig newDefaultInstance() {
        return new ListviewMasConfig(false, null, null, 0);
    }

    public ListviewMasConfig() {
    }

    public ListviewMasConfig(boolean enabled, String key, String configUrl, int repeatEveryXItems2) {
        super(enabled, key, configUrl);
        this.repeatEveryXItems = repeatEveryXItems2;
    }

    public int getRepeatEveryXItems() {
        return this.repeatEveryXItems;
    }
}
