package ch.nth.android.contentabo.models.utils;

import com.google.gson.annotations.SerializedName;
import java.util.HashSet;
import java.util.Set;

public class ActionHistory {
    @SerializedName("content_ids")
    private Set<String> contentIds = new HashSet();

    public boolean addContentId(String contentId) {
        return this.contentIds.add(contentId);
    }

    public boolean containsContentId(String contentId) {
        return this.contentIds.contains(contentId);
    }
}
