package ch.nth.android.contentabo.fragments;

import android.os.Bundle;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.mas.ListviewMasConfig;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.content.ContentList;
import ch.nth.android.contentabo.models.content.Tag;
import ch.nth.android.contentabo.networking.content.GsonRequest;
import ch.nth.android.contentabo.networking.content.RequestUtils;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import java.util.Random;

public abstract class VideoDetailsRelatedAbstractFragment extends VideoDetailsAbstractFragment {
    private static final int DEFAULT_PAGE_SIZE = 5;
    /* access modifiers changed from: private */
    public boolean mFetchingInProgress = false;
    /* access modifiers changed from: private */
    public int mNextPage = 0;
    protected String mParentContentTag;
    protected ContentList mRelatedVideos;
    private int pageSize = 0;
    private Response.ErrorListener relatedVideosErrorListener = new Response.ErrorListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment.access$2(ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment, boolean):void
         arg types: [ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment, int]
         candidates:
          ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment.access$2(ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment, boolean):void
          ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment.access$2(ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment, boolean):void */
        public void onErrorResponse(VolleyError error) {
            VideoDetailsRelatedAbstractFragment.this.onRelatedVideosError(error.getMessage());
            VideoDetailsRelatedAbstractFragment.this.mFetchingInProgress = false;
        }
    };
    private Response.Listener<ContentList> relatedVideosListener = new Response.Listener<ContentList>() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment.access$2(ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment, boolean):void
         arg types: [ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment, int]
         candidates:
          ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment.access$2(ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment, boolean):void
          ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment.access$2(ch.nth.android.contentabo.fragments.VideoDetailsRelatedAbstractFragment, boolean):void */
        public void onResponse(ContentList response) {
            if (response.getData() != null) {
                VideoDetailsRelatedAbstractFragment.this.mRelatedVideos = response;
                VideoDetailsRelatedAbstractFragment videoDetailsRelatedAbstractFragment = VideoDetailsRelatedAbstractFragment.this;
                VideoDetailsRelatedAbstractFragment videoDetailsRelatedAbstractFragment2 = VideoDetailsRelatedAbstractFragment.this;
                int access$0 = videoDetailsRelatedAbstractFragment2.mNextPage;
                videoDetailsRelatedAbstractFragment2.mNextPage = access$0 + 1;
                videoDetailsRelatedAbstractFragment.onRelatedVideosFetched(response, access$0);
            } else {
                VideoDetailsRelatedAbstractFragment.this.onRelatedVideosError("related videos list null");
            }
            VideoDetailsRelatedAbstractFragment.this.mFetchingInProgress = false;
        }
    };

    public abstract void onRelatedVideosError(String str);

    public abstract void onRelatedVideosFetched(ContentList contentList, int i);

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchContent();
    }

    /* access modifiers changed from: protected */
    public boolean hasRequiredFields(Content content) {
        return (content == null || content.getTags() == null) ? false : true;
    }

    public void onContentFetched(Content content) {
        int tagCount;
        Random rnd = new Random();
        Tag randomTag = null;
        if (content.getTags() != null && (tagCount = content.getTags().size()) > 0) {
            randomTag = content.getTags().get(rnd.nextInt(tagCount));
        }
        if (randomTag != null) {
            this.mParentContentTag = randomTag.getId();
            resetPagingAndFetch();
            return;
        }
        onRelatedVideosError("no content tag found");
    }

    public void onContentError(String message) {
        onRelatedVideosError(message);
    }

    /* access modifiers changed from: protected */
    public final void resetPagingAndFetch() {
        this.mNextPage = 0;
        fetchNextPage();
    }

    /* access modifiers changed from: protected */
    public final void fetchNextPage() {
        fetchRelatedVideosByTag(this.mParentContentTag);
    }

    public int getNextPage() {
        return this.mNextPage;
    }

    private void fetchRelatedVideosByTag(String parentContentTag) {
        fetchNetworkRelatedVideosByTag(parentContentTag, this.mNextPage, getPageSize());
    }

    /* access modifiers changed from: protected */
    public void fetchNetworkRelatedVideosByTag(String contentId, int pageNumber, int pageSize2) {
        if (!this.mFetchingInProgress) {
            this.mFetchingInProgress = true;
            onNetworkFetchStarted();
            addRequest(new GsonRequest<>(getActivity(), RequestUtils.getRelatedVideosForParentContentTagUrl(getActivity(), contentId, pageNumber, pageSize2), ContentList.class, null, this.relatedVideosListener, this.relatedVideosErrorListener));
        }
    }

    /* access modifiers changed from: protected */
    public ListviewMasConfig getListviewMasConfig() {
        try {
            ListviewMasConfig masConfig = AppConfig.getInstance().getConfig().getModules().getRelatedVideosModule().getMasConfig();
            if (masConfig == null) {
                return ListviewMasConfig.newDefaultInstance();
            }
            return masConfig;
        } catch (Exception e) {
            Utils.doLogException(e);
            if (0 == 0) {
                return ListviewMasConfig.newDefaultInstance();
            }
            return null;
        } catch (Throwable th) {
            if (0 == 0) {
                ListviewMasConfig masConfig2 = ListviewMasConfig.newDefaultInstance();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public int getPageSize() {
        if (this.pageSize <= 1) {
            try {
                this.pageSize = AppConfig.getInstance().getConfig().getModules().getRelatedVideosModule().getPageSize();
                if (this.pageSize <= 1) {
                    this.pageSize = 5;
                }
            } catch (Exception e) {
                Utils.doLogException(e);
                if (this.pageSize <= 1) {
                    this.pageSize = 5;
                }
            } catch (Throwable th) {
                if (this.pageSize <= 1) {
                    this.pageSize = 5;
                }
                throw th;
            }
        }
        return this.pageSize;
    }
}
