package ch.nth.android.contentabo.config;

import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.content.EmbeddedContentConfig;

public class ConfigFactory {
    private ConfigFactory() {
    }

    public static EmbeddedContentConfig getEmbeddedContentConfigOrDefault() {
        EmbeddedContentConfig config = null;
        try {
            config = AppConfig.getInstance().getConfig().getModules().getVideoListModule().getEmbeddedContentConfig();
        } catch (Exception e) {
            Utils.doLogException(e);
        }
        if (config == null) {
            return EmbeddedContentConfig.newDefaultInstance();
        }
        return config;
    }

    public static EmbeddedContentConfig getEmbeddedCategoriesConfigOrDefault() {
        EmbeddedContentConfig config = null;
        try {
            config = AppConfig.getInstance().getConfig().getModules().getCategoriesModule().getEmbeddedContentConfig();
        } catch (Exception e) {
            Utils.doLogException(e);
        }
        if (config == null) {
            return EmbeddedContentConfig.newDefaultInstance();
        }
        return config;
    }
}
