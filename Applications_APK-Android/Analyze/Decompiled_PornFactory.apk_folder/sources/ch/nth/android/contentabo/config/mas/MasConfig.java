package ch.nth.android.contentabo.config.mas;

import ch.nth.android.polyglot.linguistics.Linguist;
import ch.nth.simpleplist.annotation.Property;

public class MasConfig {
    @Property(name = "config_url", required = false)
    private String configUrl;
    @Property(name = Linguist.KEY_ENABLED)
    private boolean enabled;
    @Property(name = "key")
    private String key;

    public static MasConfig newDefaultInstance() {
        return new MasConfig(false, null, null);
    }

    public MasConfig() {
    }

    public MasConfig(boolean enabled2, String key2, String configUrl2) {
        this.enabled = enabled2;
        this.key = key2;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public String getKey() {
        return this.key;
    }

    public String getConfigUrl() {
        return this.configUrl;
    }
}
