package ch.nth.android.contentabo.fragments;

import android.os.Bundle;
import ch.nth.android.contentabo.config.MasConfigFactory;
import ch.nth.android.contentabo.config.mas.MasConfig;
import ch.nth.android.contentabo.fragments.base.BaseFragment;
import ch.nth.android.contentabo.models.content.Category;

public abstract class VideoListAbstractFragment extends BaseFragment {
    protected static final int PAGE_SIZE = 6;
    protected static final String PARAM_KEY_CATEGORY = "video_list_param_category";
    protected Category mCategory = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            extractData(getArguments());
        } else {
            extractData(savedInstanceState);
        }
    }

    /* access modifiers changed from: protected */
    public void extractData(Bundle bundle) {
        this.mCategory = (Category) bundle.getSerializable(PARAM_KEY_CATEGORY);
    }

    /* access modifiers changed from: protected */
    public MasConfig getFloatingMasConfig() {
        return MasConfigFactory.getVideoListFloatingMasConfig();
    }
}
