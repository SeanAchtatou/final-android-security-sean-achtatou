package ch.nth.android.contentabo.config.payment;

import android.text.TextUtils;
import ch.nth.android.utils.JavaUtils;
import ch.nth.simpleplist.annotation.Property;

public class CancelSubscriptionConfig {
    @Property(name = "keyword_1", required = false)
    private String keyword1;
    @Property(name = "keyword_10", required = false)
    private String keyword10;
    @Property(name = "keyword_11", required = false)
    private String keyword11;
    @Property(name = "keyword_12", required = false)
    private String keyword12;
    @Property(name = "keyword_13", required = false)
    private String keyword13;
    @Property(name = "keyword_14", required = false)
    private String keyword14;
    @Property(name = "keyword_15", required = false)
    private String keyword15;
    @Property(name = "keyword_16", required = false)
    private String keyword16;
    @Property(name = "keyword_17", required = false)
    private String keyword17;
    @Property(name = "keyword_18", required = false)
    private String keyword18;
    @Property(name = "keyword_19", required = false)
    private String keyword19;
    @Property(name = "keyword_2", required = false)
    private String keyword2;
    @Property(name = "keyword_20", required = false)
    private String keyword20;
    @Property(name = "keyword_21", required = false)
    private String keyword21;
    @Property(name = "keyword_22", required = false)
    private String keyword22;
    @Property(name = "keyword_23", required = false)
    private String keyword23;
    @Property(name = "keyword_24", required = false)
    private String keyword24;
    @Property(name = "keyword_25", required = false)
    private String keyword25;
    @Property(name = "keyword_3", required = false)
    private String keyword3;
    @Property(name = "keyword_4", required = false)
    private String keyword4;
    @Property(name = "keyword_5", required = false)
    private String keyword5;
    @Property(name = "keyword_6", required = false)
    private String keyword6;
    @Property(name = "keyword_7", required = false)
    private String keyword7;
    @Property(name = "keyword_8", required = false)
    private String keyword8;
    @Property(name = "keyword_9", required = false)
    private String keyword9;
    @Property(name = "nwc_1", required = false)
    private String nwc1;
    @Property(name = "nwc_10", required = false)
    private String nwc10;
    @Property(name = "nwc_11", required = false)
    private String nwc11;
    @Property(name = "nwc_12", required = false)
    private String nwc12;
    @Property(name = "nwc_13", required = false)
    private String nwc13;
    @Property(name = "nwc_14", required = false)
    private String nwc14;
    @Property(name = "nwc_15", required = false)
    private String nwc15;
    @Property(name = "nwc_16", required = false)
    private String nwc16;
    @Property(name = "nwc_17", required = false)
    private String nwc17;
    @Property(name = "nwc_18", required = false)
    private String nwc18;
    @Property(name = "nwc_19", required = false)
    private String nwc19;
    @Property(name = "nwc_2", required = false)
    private String nwc2;
    @Property(name = "nwc_20", required = false)
    private String nwc20;
    @Property(name = "nwc_21", required = false)
    private String nwc21;
    @Property(name = "nwc_22", required = false)
    private String nwc22;
    @Property(name = "nwc_23", required = false)
    private String nwc23;
    @Property(name = "nwc_24", required = false)
    private String nwc24;
    @Property(name = "nwc_25", required = false)
    private String nwc25;
    @Property(name = "nwc_3", required = false)
    private String nwc3;
    @Property(name = "nwc_4", required = false)
    private String nwc4;
    @Property(name = "nwc_5", required = false)
    private String nwc5;
    @Property(name = "nwc_6", required = false)
    private String nwc6;
    @Property(name = "nwc_7", required = false)
    private String nwc7;
    @Property(name = "nwc_8", required = false)
    private String nwc8;
    @Property(name = "nwc_9", required = false)
    private String nwc9;
    @Property(name = "shid_1", required = false)
    private String shid1;
    @Property(name = "shid_10", required = false)
    private String shid10;
    @Property(name = "shid_11", required = false)
    private String shid11;
    @Property(name = "shid_12", required = false)
    private String shid12;
    @Property(name = "shid_13", required = false)
    private String shid13;
    @Property(name = "shid_14", required = false)
    private String shid14;
    @Property(name = "shid_15", required = false)
    private String shid15;
    @Property(name = "shid_16", required = false)
    private String shid16;
    @Property(name = "shid_17", required = false)
    private String shid17;
    @Property(name = "shid_18", required = false)
    private String shid18;
    @Property(name = "shid_19", required = false)
    private String shid19;
    @Property(name = "shid_2", required = false)
    private String shid2;
    @Property(name = "shid_20", required = false)
    private String shid20;
    @Property(name = "shid_21", required = false)
    private String shid21;
    @Property(name = "shid_22", required = false)
    private String shid22;
    @Property(name = "shid_23", required = false)
    private String shid23;
    @Property(name = "shid_24", required = false)
    private String shid24;
    @Property(name = "shid_25", required = false)
    private String shid25;
    @Property(name = "shid_3", required = false)
    private String shid3;
    @Property(name = "shid_4", required = false)
    private String shid4;
    @Property(name = "shid_5", required = false)
    private String shid5;
    @Property(name = "shid_6", required = false)
    private String shid6;
    @Property(name = "shid_7", required = false)
    private String shid7;
    @Property(name = "shid_8", required = false)
    private String shid8;
    @Property(name = "shid_9", required = false)
    private String shid9;
    @Property(name = "verify_delay_seconds", required = false, stringCompatibility = true)
    private int verifyDelaySeconds = 5;

    public int getVerifyDelaySeconds() {
        return this.verifyDelaySeconds;
    }

    public CancelSubscriptionResult getCancelSubscriptionResult(String nwc) {
        if (TextUtils.isEmpty(nwc)) {
            return new CancelSubscriptionResult("", "");
        }
        if (JavaUtils.regexMatch(this.nwc1, nwc)) {
            return new CancelSubscriptionResult(this.shid1, this.keyword1);
        }
        return JavaUtils.regexMatch(this.nwc2, nwc) ? new CancelSubscriptionResult(this.shid2, this.keyword2) : new CancelSubscriptionResult("", "");
    }

    public static class CancelSubscriptionResult {
        private String keyword;
        private String shid;

        public CancelSubscriptionResult(String shid2, String keyword2) {
            this.shid = shid2;
            this.keyword = keyword2;
        }

        public String getShid() {
            return this.shid;
        }

        public String getKeyword() {
            return this.keyword;
        }
    }
}
