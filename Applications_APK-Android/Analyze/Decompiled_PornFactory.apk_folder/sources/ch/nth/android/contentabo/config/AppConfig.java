package ch.nth.android.contentabo.config;

import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.fetcher.config.AssetsFetchOptions;
import ch.nth.android.simpleplist.task.PlistAsyncTask;
import ch.nth.android.simpleplist.task.TaskOptions;
import ch.nth.android.simpleplist.task.config.AssetsConfigOptions;
import ch.nth.android.simpleplist.task.config.CacheConfigOptions;
import ch.nth.android.simpleplist.task.config.ConfigType;
import java.util.concurrent.ExecutionException;

public class AppConfig {
    private static AppConfig sInstance;
    private PlistConfig config;
    private ConfigType type;

    public static AppConfig getInstance() {
        if (sInstance == null) {
            sInstance = new AppConfig();
        }
        return sInstance;
    }

    private AppConfig() {
    }

    public PlistConfig getConfig() {
        if (this.config == null) {
            try {
                this.config = (PlistConfig) new PlistAsyncTask<>(App.getInstance(), new TaskOptions.Builder(null).addStep(new CacheConfigOptions()).addStep(new AssetsConfigOptions.Builder().assetsDirectory(AssetsFetchOptions.PLIST_DEFAULT_ASSETS_DIRECTORY).filename(AssetsFetchOptions.PLIST_DEFAULT_ASSETS_FILENAME).build()).debug(false).build(), PlistConfig.class).execute(new Void[0]).get();
            } catch (InterruptedException e) {
                Utils.doLogException(e);
            } catch (ExecutionException e2) {
                Utils.doLogException(e2);
            }
            if (this.config == null) {
                this.config = new PlistConfig();
            }
        }
        return this.config;
    }

    public void setConfig(PlistConfig config2) {
        this.config = config2;
    }

    public ConfigType getType() {
        return this.type;
    }

    public void setType(ConfigType type2) {
        this.type = type2;
    }
}
