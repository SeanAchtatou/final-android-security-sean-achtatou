package ch.nth.android.contentabo.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.modules.VideoDetailsModule;
import ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment;
import ch.nth.android.contentabo.models.content.CommentList;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.content.ContentList;
import ch.nth.android.contentabo.networking.content.GsonRequest;
import ch.nth.android.contentabo.networking.content.RequestUtils;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import java.io.Serializable;

public abstract class VideoDetailsAbstractFragment extends PaymentAbstractFragment implements Response.Listener<ContentList>, Response.ErrorListener {
    public static final String EXTRA_CONTENT = "video.details.content";
    public static final String EXTRA_CONTENT_ID = "video.details.contentId";
    protected VideoDetailsModule.CommentType mCommentType = VideoDetailsModule.CommentType.ON;
    protected boolean mConfigurationButtonEnabled = false;
    protected Content mContent;
    protected String mContentId;
    private boolean mLocallyFetched;
    protected VideoDetailsModule.RatingsType mRatingsType = VideoDetailsModule.RatingsType.ON;
    protected boolean mRelatedVideosEnabled = true;
    protected boolean mVideoDescriptionEnabled = true;
    protected boolean mVideoTitleEnabled = true;

    public abstract void onContentError(String str);

    public abstract void onContentFetched(Content content);

    public abstract void onNetworkFetchStarted();

    public static Bundle getArgumentBundle(String contentId, Content content) {
        Bundle args = new Bundle();
        args.putString("video.details.contentId", contentId);
        if (content != null) {
            args.putSerializable("video.details.content", content);
        }
        return args;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            Serializable obj = args.getSerializable("video.details.content");
            if (obj instanceof Content) {
                this.mContent = (Content) obj;
            }
            String extraContentId = args.getString("video.details.contentId");
            if (!TextUtils.isEmpty(extraContentId)) {
                this.mContentId = extraContentId;
            }
        }
        try {
            VideoDetailsModule module = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule();
            if (module.getCommentType() != null) {
                this.mCommentType = module.getCommentType();
            }
            if (module.getRatingsType() != null) {
                this.mRatingsType = module.getRatingsType();
            }
            this.mRelatedVideosEnabled = module.isShowRelatedVideos();
            this.mConfigurationButtonEnabled = module.isShowConfigurationButton();
            this.mVideoTitleEnabled = module.isShowVideoTitle();
            this.mVideoDescriptionEnabled = module.isShowVideoDescription();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public final void fetchContent() {
        fetchContentById(this.mContentId);
    }

    /* access modifiers changed from: protected */
    public void fetchContentById(String contentId) {
        this.mContentId = contentId;
        if (this.mContent == null || (!this.mLocallyFetched && !hasRequiredFields(this.mContent))) {
            fetchNetworkContentById(this.mContentId);
        } else {
            onContentFetched(this.mContent);
        }
    }

    /* access modifiers changed from: protected */
    public void fetchNetworkContentById(String contentId) {
        onNetworkFetchStarted();
        addRequest(new GsonRequest<>(getActivity(), RequestUtils.getContentByIdUrl(getActivity(), contentId), ContentList.class, null, this, this));
    }

    public final void onResponse(ContentList response) {
        if (response.getData() == null || response.getData().size() <= 0) {
            onContentError("response list null or size 0");
            return;
        }
        this.mLocallyFetched = true;
        this.mContent = response.getData().get(0);
        onContentFetched(response.getData().get(0));
    }

    public final void onErrorResponse(VolleyError error) {
        onContentError(error.getMessage());
    }

    /* access modifiers changed from: protected */
    public boolean hasRequiredFields(Content content) {
        if (content == null || content.getDuration() <= 0 || content.getCategories() == null || content.getAvgRating() <= 0.0f || content.getTags() == null || content.getDescription() == null) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void fetchCommentCountByContentId(String contentId, Response.Listener<CommentList> listener, Response.ErrorListener errorListener) {
        addRequest(new GsonRequest<>(getActivity(), RequestUtils.getCommentCountForContentIdUrl(getActivity(), contentId), CommentList.class, null, listener, errorListener));
    }
}
