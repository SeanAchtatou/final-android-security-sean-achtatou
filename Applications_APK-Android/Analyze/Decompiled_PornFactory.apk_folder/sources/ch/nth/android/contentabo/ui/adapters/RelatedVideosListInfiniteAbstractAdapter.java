package ch.nth.android.contentabo.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.common.adapter.InfiniteBaseAdapter;
import ch.nth.android.contentabo.common.utils.FormatUtils;
import ch.nth.android.contentabo.config.mas.ListviewMasConfig;
import ch.nth.android.contentabo.models.content.Content;
import ch.nth.android.contentabo.models.content.ContentList;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import com.nth.android.mas.MASLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public abstract class RelatedVideosListInfiniteAbstractAdapter extends InfiniteBaseAdapter {
    public static final int VIEW_TYPE_AD = 1;
    public static final int VIEW_TYPE_VIDEO = 0;
    private int everyXPositions = 0;
    private final List<Content> mContentList;
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final int mLayoutResource;
    private ListviewMasConfig mMasConfig;
    private int mTotal;

    /* access modifiers changed from: protected */
    public abstract Content.PictureFlavor getPictureFlavor();

    /* access modifiers changed from: protected */
    public abstract Content.PictureSelectMode getPictureSelectMode();

    /* access modifiers changed from: protected */
    public abstract boolean showProgressBarForImageLoading();

    public RelatedVideosListInfiniteAbstractAdapter(Context context, int layoutResource, ContentList contentList, InfiniteBaseAdapter.Listener listener, ListviewMasConfig masConfig) {
        this.mContext = context;
        this.mLayoutResource = layoutResource;
        this.mInflater = LayoutInflater.from(context);
        this.mContentList = new ArrayList();
        this.mMasConfig = masConfig;
        if (this.mMasConfig == null) {
            this.mMasConfig = ListviewMasConfig.newDefaultInstance();
        }
        setInfiniteAdapterListener(listener);
        setData(contentList);
    }

    public void setData(ContentList contentList) {
        if (contentList != null && contentList.getData() != null) {
            this.mContentList.clear();
            this.mContentList.addAll(contentList.getData());
            this.mTotal = calculateNewTotal(contentList.getTotal());
            notifyDataSetChanged();
        }
    }

    public void appendData(ContentList contentList) {
        if (contentList != null && contentList.getData() != null) {
            this.mContentList.addAll(contentList.getData());
            this.mTotal = calculateNewTotal(contentList.getTotal());
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return this.mContentList.size();
    }

    public Content getItem(int position) {
        return this.mContentList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemViewType(int position) {
        if (!showAdViewType() || !isAdViewType(position)) {
            Log.i("jvx", "0");
            return 0;
        }
        Log.i("jvx", "1");
        return 1;
    }

    public int getViewTypeCount() {
        if (showAdViewType()) {
            return 2;
        }
        return 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final boolean showProgressBar = true;
        int itemViewType = getItemViewType(position);
        if (itemViewType == 0) {
            if (convertView == null) {
                convertView = this.mInflater.inflate(this.mLayoutResource, parent, false);
                holder = new ViewHolder(null);
                holder.btnPlay = (ImageView) convertView.findViewById(R.id.video_list_item_play);
                holder.imgPreviewImage = (ImageView) convertView.findViewById(R.id.video_list_item_image);
                holder.txtTitle = (TextView) convertView.findViewById(R.id.video_list_item_title);
                holder.txtDuration = (TextView) convertView.findViewById(R.id.video_list_item_duration);
                holder.txtTimeImported = (TextView) convertView.findViewById(R.id.video_list_item_time_imported);
                holder.txtViews = (TextView) convertView.findViewById(R.id.video_list_item_views);
                holder.ratingBar = (RatingBar) convertView.findViewById(R.id.video_list_item_rating);
                holder.progressBar = (ProgressBar) convertView.findViewById(R.id.video_list_item_progressbar);
                TextView durationLabel = (TextView) convertView.findViewById(R.id.video_list_item_duration_label);
                if (durationLabel != null) {
                    durationLabel.setText(Translations.getPolyglot().getString(T.string.video_list_item_duration));
                }
                TextView timeImportedLabel = (TextView) convertView.findViewById(R.id.video_list_item_time_imported_label);
                if (timeImportedLabel != null) {
                    timeImportedLabel.setText(Translations.getPolyglot().getString(T.string.video_list_item_uploaded));
                }
                TextView viewsLabel = (TextView) convertView.findViewById(R.id.video_list_item_views_label);
                if (viewsLabel != null) {
                    viewsLabel.setText(Translations.getPolyglot().getString(T.string.video_list_item_views));
                }
                TextView ratingLabel = (TextView) convertView.findViewById(R.id.video_list_item_rating_label);
                if (ratingLabel != null) {
                    ratingLabel.setText(Translations.getPolyglot().getString(T.string.video_list_item_rating));
                }
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Content content = getItem(position);
            if (!showProgressBarForImageLoading() || holder.progressBar == null) {
                showProgressBar = false;
            }
            if (showProgressBar) {
                holder.btnPlay.setVisibility(4);
                holder.progressBar.setVisibility(0);
            }
            Picasso.with(this.mContext).load(content.getPictureUrl(getPictureFlavor(), getPictureSelectMode())).into(holder.imgPreviewImage, new Callback() {
                public void onSuccess() {
                    if (showProgressBar) {
                        holder.btnPlay.setVisibility(0);
                        holder.progressBar.setVisibility(8);
                    }
                }

                public void onError() {
                    if (showProgressBar) {
                        holder.btnPlay.setVisibility(0);
                        holder.progressBar.setVisibility(8);
                    }
                }
            });
            holder.txtTitle.setText(content.getName());
            holder.txtDuration.setText(FormatUtils.formatContentDuration(content.getDuration()));
            holder.txtTimeImported.setText(FormatUtils.formatLocalDate(this.mContext, content.getTimeImported()));
            holder.txtViews.setText(new StringBuilder().append(content.getDownloadCount()).toString());
            holder.ratingBar.setRating(content.getAvgRating());
        } else if (itemViewType == 1 && convertView == null && (this.mContext instanceof Activity)) {
            convertView = new MASLayout((Activity) this.mContext, this.mMasConfig.getKey(), this.mMasConfig.getConfigUrl());
        }
        checkAndReportLastPosition(position);
        return convertView;
    }

    private static class ViewHolder {
        ImageView btnPlay;
        ImageView imgPreviewImage;
        ProgressBar progressBar;
        RatingBar ratingBar;
        TextView txtDuration;
        TextView txtTimeImported;
        TextView txtTitle;
        TextView txtViews;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }

    public int getTotalItems() {
        return this.mTotal;
    }

    /* access modifiers changed from: protected */
    public final boolean showAdViewType() {
        return this.mMasConfig.isEnabled();
    }

    /* access modifiers changed from: protected */
    public boolean isAdViewType(int position) {
        if (this.everyXPositions == 0) {
            this.everyXPositions = this.mMasConfig.getRepeatEveryXItems();
        }
        if (this.everyXPositions <= 1 || position == 0 || position % this.everyXPositions != 0) {
            return false;
        }
        return true;
    }
}
