package ch.nth.android.contentabo;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.PlistConfig;
import ch.nth.android.contentabo.service.AlarmReceiver;
import com.google.android.gms.drive.DriveFile;
import com.nth.gcm.GCMService;
import java.util.Random;

public class GCMIntentService extends GCMService {
    private static final String TAG = GCMIntentService.class.getSimpleName();

    /* access modifiers changed from: protected */
    public void onMessage(Context context, Intent intent) {
        long periodMillis = (long) (new Random().nextInt(AppConfig.getInstance().getConfig().getEntStore().getUpdateMaxTimeoutSeconds()) * 1000);
        Log.d(TAG, "scheduling application download in: " + periodMillis + "ms");
        ((AlarmManager) getSystemService("alarm")).set(2, SystemClock.elapsedRealtime() + periodMillis, PendingIntent.getBroadcast(this, 0, new Intent(AlarmReceiver.ACTION_START_APP_UPDATE), DriveFile.MODE_READ_ONLY));
    }

    public String getRegistrationUrl(Context context) {
        PlistConfig config = AppConfig.getInstance().getConfig();
        if (config != null) {
            return String.valueOf(config.getPushNotificationsUrl()) + "/tokens";
        }
        return "";
    }

    public String getUnregistrationUrl(Context context) {
        return "";
    }

    public String getApiKey() {
        PlistConfig config = AppConfig.getInstance().getConfig();
        if (config == null) {
            return "";
        }
        Log.d(TAG, "getApiKey: " + config.getApiKey());
        return config.getApiKey();
    }

    public boolean registerTokenWithLocation() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String[] getSenderIds(Context context) {
        return new String[]{AppConfig.getInstance().getConfig().getGcmProjectId()};
    }
}
