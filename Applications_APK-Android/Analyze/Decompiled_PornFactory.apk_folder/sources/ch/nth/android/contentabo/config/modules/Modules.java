package ch.nth.android.contentabo.config.modules;

import ch.nth.simpleplist.annotation.Dictionary;

public class Modules {
    @Dictionary(name = "module_categories")
    private CategoriesModule categoriesModule;
    @Dictionary(name = "module_comments")
    private CommentsModule commentsModule;
    @Dictionary(name = "module_menu")
    private MenuModule menuModule;
    @Dictionary(name = "module_my_subscription")
    private MySubscriptionModule mySubscriptionsModule;
    @Dictionary(name = "module_related_videos")
    private RelatedVideosModule relatedVideosModule;
    @Dictionary(name = "module_splash")
    private SplashModule splashModule;
    @Dictionary(name = "module_update_app")
    private UpdateApplicationModule updateApplicationModule;
    @Dictionary(name = "module_video_details")
    private VideoDetailsModule videoDetailsModule;
    @Dictionary(name = "module_video_list")
    private VideoListModule videoListModule;

    public SplashModule getSplashModule() {
        if (this.splashModule == null) {
            this.splashModule = new SplashModule();
        }
        return this.splashModule;
    }

    public VideoListModule getVideoListModule() {
        if (this.videoListModule == null) {
            this.videoListModule = new VideoListModule();
        }
        return this.videoListModule;
    }

    public VideoDetailsModule getVideoDetailsModule() {
        if (this.videoDetailsModule == null) {
            this.videoDetailsModule = new VideoDetailsModule();
        }
        return this.videoDetailsModule;
    }

    public MySubscriptionModule getMySubscriptionsModule() {
        if (this.mySubscriptionsModule == null) {
            this.mySubscriptionsModule = new MySubscriptionModule();
        }
        return this.mySubscriptionsModule;
    }

    public UpdateApplicationModule getUpdateAppModule() {
        if (this.updateApplicationModule == null) {
            this.updateApplicationModule = new UpdateApplicationModule();
        }
        return this.updateApplicationModule;
    }

    public MenuModule getMenuModule() {
        if (this.menuModule == null) {
            this.menuModule = new MenuModule();
        }
        return this.menuModule;
    }

    public RelatedVideosModule getRelatedVideosModule() {
        if (this.relatedVideosModule == null) {
            this.relatedVideosModule = new RelatedVideosModule();
        }
        return this.relatedVideosModule;
    }

    public CommentsModule getCommentsModule() {
        if (this.commentsModule == null) {
            this.commentsModule = new CommentsModule();
        }
        return this.commentsModule;
    }

    public CategoriesModule getCategoriesModule() {
        if (this.categoriesModule == null) {
            this.categoriesModule = new CategoriesModule();
        }
        return this.categoriesModule;
    }
}
