package ch.nth.android.contentabo.fragments.base;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.contentabo.common.recivers.SMSSentBroadcastReceiver;
import ch.nth.android.contentabo.common.utils.MessageUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.PreferenceConnector;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.fragments.interfaces.UserSubscriptionStatusListener;
import ch.nth.android.contentabo.models.payment.PaymentOption;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import ch.nth.android.paymentsdk.v2.model.SubscriptionPaymentSession;
import ch.nth.android.scmsdk.model.ScmSubscriptionSession;
import ch.nth.android.utils.JavaUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class PaymentAbstractFragment extends BaseFragment implements UserSubscriptionStatusListener, LoaderManager.LoaderCallbacks<Cursor> {
    private static final int ACTIVITY_PAYMENT_RESULT = 1;
    public static final String ANGEBOTE_SMS_RECEIVED_ACTION = "angebotes_sms_received";
    private static final int ANGEBOT_LOADER_ID = 66;
    private static final String TAG = PaymentAbstractFragment.class.getSimpleName();
    private BroadcastReceiver angeboteSMSReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Log.d("Payment SMS check", "angeboteSMSReceiver triggered");
            PaymentAbstractFragment.this.onAngebotOrWelcomeMessageReceived(false);
        }
    };
    /* access modifiers changed from: private */
    public long lastVerifyPaymentStartTime;
    private AtomicBoolean mAngebotReceived = new AtomicBoolean(false);
    private Handler mHandler = new Handler();
    private PaymentOption mPamentOption;
    private SmsManager mSmsManager;
    private TimeProgress mTimeProgress;
    private ProgressDialog mVerifyPaymentDialog;
    private BroadcastReceiver smsSentReceiver = new BroadcastReceiver() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment.access$1(ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment, boolean):void
         arg types: [ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment, int]
         candidates:
          ch.nth.android.paymentsdk.v2.fragments.base.BasePaymentFragment.access$1(ch.nth.android.paymentsdk.v2.fragments.base.BasePaymentFragment, java.lang.String):void
          ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment.access$1(ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment, boolean):void */
        public void onReceive(Context context, Intent intent) {
            Utils.doLog("Payment", "smsSentReceiver resultCode: " + getResultCode());
            switch (getResultCode()) {
                case -1:
                    PaymentAbstractFragment.this.onSmsSendResult(true);
                    return;
                default:
                    PaymentAbstractFragment.this.onSmsSendResult(false);
                    return;
            }
        }
    };
    private int verifyPaymentCount;
    private Runnable verifyPaymentRunnable = new Runnable() {
        public void run() {
            PaymentAbstractFragment.this.lastVerifyPaymentStartTime = System.currentTimeMillis();
            PaymentAbstractFragment.this.verifyPayment(PaymentUtils.getPaymentOption(PaymentAbstractFragment.this.getActivity()).getSessionId());
            Utils.doLog("Payment", "started verifyPaymentRunnable");
        }
    };

    /* access modifiers changed from: protected */
    public abstract Class<?> getWebViewActivity();

    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<Cursor>) loader, (Cursor) obj);
    }

    /* access modifiers changed from: protected */
    public abstract void onSubscriptionNextStage();

    /* access modifiers changed from: protected */
    public abstract void onUpdateProgress(int i);

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mPamentOption = PaymentUtils.getPaymentOption(activity);
    }

    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(this.smsSentReceiver, new IntentFilter(SMSSentBroadcastReceiver.SECOND_SMS_SENT));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(this.angeboteSMSReceiver, new IntentFilter(ANGEBOTE_SMS_RECEIVED_ACTION));
        if (PaymentUtils.EXTERNAL_STATUS_PENDING.equals(PreferenceConnector.readString(getActivity(), PaymentUtils.KEY_PREFERENCES_FIRST_MO_SINGLE_EXTERNAL_STATUS, "")) || PaymentUtils.EXTERNAL_STATUS_PENDING.equals(PreferenceConnector.readString(getActivity(), PaymentUtils.KEY_PREFERENCES_SECOND_MO_EXTERNAL_STATUS, ""))) {
            checkIfSmsSentFromEditor();
        }
    }

    private void checkIfSmsSentFromEditor() {
        Log.d(TAG, "checking 'sms sent' to see if first/second MO was sent");
        PaymentOption paymentOption = PaymentUtils.getPaymentOption(getActivity());
        if (paymentOption != null) {
            PaymentOption.PaymentFlowType flowType = paymentOption.getPaymentFlowType();
            if (flowType == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
                String number = paymentOption.getNumberFromConfirmationUrl();
                String body = paymentOption.getBodyFromConfirmationUrl();
                Cursor cur = getActivity().getContentResolver().query(Uri.parse("content://sms/sent"), null, null, null, null);
                int messagesChecked = 0;
                cur.moveToFirst();
                while (!cur.isAfterLast()) {
                    messagesChecked++;
                    String contentBody = cur.getString(cur.getColumnIndex("body"));
                    String contentNumber = cur.getString(cur.getColumnIndex("address"));
                    if (!TextUtils.equals(contentBody, body) || !TextUtils.equals(number, contentNumber)) {
                        cur.moveToNext();
                    } else {
                        PreferenceConnector.writeString(getActivity(), PaymentUtils.KEY_PREFERENCES_SECOND_MO_EXTERNAL_STATUS, "");
                        PaymentUtils.setPaymentFlowStage(getActivity(), PaymentUtils.PaymentFlowStage.SECOND_MO_SENT_SUCCESSFULLY);
                        PaymentUtils.savePaymentOption(getActivity());
                        Log.d(TAG, "FOUND SENT SECOND MO MESSAGE! setting PaymentFlowStage to SECOND_MO_SENT_SUCCESSFULLY and starting verifyPayment, messages checked: " + messagesChecked);
                        String sessionId = paymentOption.getSessionId();
                        this.verifyPaymentCount = 0;
                        showVerifyPaymentDialog();
                        verifyPayment(sessionId);
                        return;
                    }
                }
                if (PaymentUtils.isSubscribed()) {
                    PreferenceConnector.writeString(getActivity(), PaymentUtils.KEY_PREFERENCES_SECOND_MO_EXTERNAL_STATUS, "");
                }
                Log.d(TAG, "DIDN'T FIND SENT SECOND MO MESSAGE! messages checked: " + messagesChecked);
            } else if (flowType == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN) {
                String number2 = paymentOption.getNumberFromServiceUrl();
                String body2 = paymentOption.getBodyFromServiceUrl();
                Cursor cur2 = getActivity().getContentResolver().query(Uri.parse("content://sms/sent"), null, null, null, null);
                int messagesChecked2 = 0;
                cur2.moveToFirst();
                while (!cur2.isAfterLast()) {
                    messagesChecked2++;
                    String contentBody2 = cur2.getString(cur2.getColumnIndex("body"));
                    String contentNumber2 = cur2.getString(cur2.getColumnIndex("address"));
                    if (!TextUtils.equals(contentBody2, body2) || !TextUtils.equals(number2, contentNumber2)) {
                        cur2.moveToNext();
                    } else {
                        PreferenceConnector.writeString(getActivity(), PaymentUtils.KEY_PREFERENCES_FIRST_MO_SINGLE_EXTERNAL_STATUS, "");
                        PaymentUtils.setPaymentFlowStage(getActivity(), PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY);
                        PaymentUtils.savePaymentOption(getActivity());
                        Log.d(TAG, "FOUND SENT FIRST MO MESSAGE! setting PaymentFlowStage to FIRST_MO_SENT_SUCCESSFULLY and starting progress, messages checked: " + messagesChecked2);
                        this.verifyPaymentCount = 0;
                        scheduleVerifyPaymentRunnableIfViable();
                        return;
                    }
                }
                if (PaymentUtils.isSubscribed()) {
                    PreferenceConnector.writeString(getActivity(), PaymentUtils.KEY_PREFERENCES_SECOND_MO_EXTERNAL_STATUS, "");
                }
                Log.d(TAG, "DIDN'T FIND SENT FIRST MO MESSAGE! messages checked: " + messagesChecked2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void showVerifyPaymentDialog() {
        Activity activity = getActivity();
        if (activity != null) {
            try {
                this.mVerifyPaymentDialog = new ProgressDialog(activity);
                this.mVerifyPaymentDialog.setProgressStyle(0);
                this.mVerifyPaymentDialog.setIndeterminate(true);
                this.mVerifyPaymentDialog.setCancelable(false);
                this.mVerifyPaymentDialog.setCanceledOnTouchOutside(false);
                this.mVerifyPaymentDialog.setMessage(Translations.getPolyglot().getString(T.string.progress_dialog_text));
                this.mVerifyPaymentDialog.show();
            } catch (Exception e) {
                Utils.doLogException(e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void stopVerifyPaymentDialog() {
        try {
            if (this.mVerifyPaymentDialog != null) {
                this.mVerifyPaymentDialog.dismiss();
            }
        } catch (Exception e) {
            Utils.doLogException(e);
        }
    }

    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(this.smsSentReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(this.angeboteSMSReceiver);
        if (this.mTimeProgress != null) {
            this.mTimeProgress.stopTimer();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    getActivity().setResult(-1);
                    onSubscriptionNextStage();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldShowProgress() {
        PaymentOption paymentOption = PaymentUtils.getPaymentOption(getActivity());
        PaymentOption.PaymentFlowType flowType = paymentOption.getPaymentFlowType();
        if (flowType == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
            Log.d("Payment", "shouldShowProgress() - flowType=" + flowType + ", FirstMoStatus=" + paymentOption.getFirstMoStatus() + ", AngebotMessageStatus=" + paymentOption.getAngebotMessageStatus() + ", SecondMoStatus=" + paymentOption.getSecondMoStatus());
            if (paymentOption.getSecondMoStatus() == PaymentUtils.PaymentFlowStage.SECOND_MO_SEND_IN_PROGRESS) {
                return true;
            }
            if (paymentOption.getAngebotMessageStatus() == PaymentUtils.PaymentFlowStage.ANGEBOT_MESSAGE_RECIVED) {
                return false;
            }
            if (paymentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY || paymentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_IN_PROGRESS) {
                return true;
            }
        } else if (flowType == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN) {
            Log.d("Payment", "shouldShowProgress() - flowType=" + flowType + ", FirstMoStatus=" + paymentOption.getFirstMoStatus() + ", AngebotMessageStatus=" + paymentOption.getAngebotMessageStatus() + ", SecondMoStatus=" + paymentOption.getSecondMoStatus());
            if (paymentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_IN_PROGRESS) {
                return true;
            }
            if (paymentOption.getWelcomeMessageStatus() == PaymentUtils.PaymentFlowStage.WELCOME_MESSAGE_RECEIVED) {
                return false;
            }
            if (paymentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean shouldShowStartButton() {
        PaymentOption paymentOption = PaymentUtils.getPaymentOption(getActivity());
        PaymentOption.PaymentFlowType flowType = this.mPamentOption.getPaymentFlowType();
        if (flowType == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
            if (paymentOption.getAngebotMessageStatus() == PaymentUtils.PaymentFlowStage.ANGEBOT_MESSAGE_RECIVED) {
                return true;
            }
        } else if (flowType == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN) {
            if (paymentOption.getWelcomeMessageStatus() == PaymentUtils.PaymentFlowStage.WELCOME_MESSAGE_RECEIVED || paymentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_ERROR) {
                return false;
            }
            return true;
        } else if (flowType == PaymentOption.PaymentFlowType.WEB_FLOW) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean shouldShowError() {
        PaymentOption.PaymentFlowType flowType = this.mPamentOption.getPaymentFlowType();
        PaymentOption paymentoption = PaymentUtils.getPaymentOption(getActivity());
        if (flowType == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
            if (paymentoption.getSecondMoStatus() == PaymentUtils.PaymentFlowStage.SECOND_MO_SEND_ERROR) {
                return true;
            }
        } else if (flowType == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN && paymentoption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_ERROR) {
            return true;
        }
        if (PaymentUtils.getPaymentFlowStage() == PaymentUtils.PaymentFlowStage.NONE || PaymentUtils.getPaymentFlowStage() == PaymentUtils.PaymentFlowStage.INIT_PAYMENT_ERROR) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean isSubscribed() {
        ScmSubscriptionSession.SessionStatus scmStatus = PaymentUtils.getSCMStatusOrNull();
        if (scmStatus == null || (scmStatus != ScmSubscriptionSession.SessionStatus.OPEN && scmStatus != ScmSubscriptionSession.SessionStatus.LIMITED)) {
            return false;
        }
        return true;
    }

    public void userConfirmsNextStep() {
        PaymentOption.PaymentFlowType flowType = this.mPamentOption.getPaymentFlowType();
        if (flowType == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
            if (PaymentUtils.getPaymentOption(getActivity()).getAngebotMessageStatus() == PaymentUtils.PaymentFlowStage.ANGEBOT_MESSAGE_RECIVED) {
                String shorId = this.mPamentOption.getNumberFromConfirmationUrl();
                String body = this.mPamentOption.getBodyFromConfirmationUrl();
                String mccMnc = PaymentUtils.getMCCMNCTuple(getActivity());
                String firstFlow = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getOptinFlowOne();
                String onePlusSecondFlow = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getOptinFlowOnePlusTwo();
                boolean firstFlowActive = !TextUtils.isEmpty(firstFlow) && JavaUtils.regexMatch(firstFlow, mccMnc);
                boolean onePlusSecondFlowActive = !TextUtils.isEmpty(onePlusSecondFlow) && JavaUtils.regexMatch(onePlusSecondFlow, mccMnc);
                if (firstFlowActive || onePlusSecondFlowActive) {
                    PreferenceConnector.writeLong(getActivity(), PaymentUtils.KEY_PREFERENCES_SECOND_MO_TIME, Calendar.getInstance().getTime().getTime());
                    PreferenceConnector.writeString(getActivity(), PaymentUtils.KEY_PREFERENCES_SECOND_MO_EXTERNAL_STATUS, PaymentUtils.EXTERNAL_STATUS_PENDING);
                    if (firstFlowActive) {
                        Utils.doLog("Payment", "Flow One applied");
                    } else if (onePlusSecondFlowActive) {
                        Utils.doLog("Payment", "Flow One+Two applied");
                    }
                    MessageUtils.startSmsMessageIntent(getActivity(), shorId, body);
                } else {
                    PaymentUtils.setPaymentFlowStage(getActivity(), PaymentUtils.PaymentFlowStage.SECOND_MO_SEND_IN_PROGRESS);
                    if (!trySendSms(shorId, body)) {
                        PaymentUtils.setPaymentFlowStage(getActivity(), PaymentUtils.PaymentFlowStage.SECOND_MO_SEND_ERROR);
                    }
                }
            }
        } else if (flowType == PaymentOption.PaymentFlowType.WEB_FLOW) {
            startActivityForResult(new Intent(getActivity(), getWebViewActivity()), 1);
        } else if (flowType == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN) {
            String shorId2 = this.mPamentOption.getNumberFromServiceUrl();
            String body2 = this.mPamentOption.getBodyFromServiceUrl();
            String mccMnc2 = PaymentUtils.getMCCMNCTuple(getActivity());
            String secondFlow = AppConfig.getInstance().getConfig().getModules().getVideoDetailsModule().getOptinFlowTwo();
            if (!TextUtils.isEmpty(secondFlow) && JavaUtils.regexMatch(secondFlow, mccMnc2)) {
                Utils.doLog("Payment", "Flow Two applied");
                PreferenceConnector.writeLong(getActivity(), PaymentUtils.KEY_PREFERENCES_FIRST_MO_SINGLE_TIME, Calendar.getInstance().getTime().getTime());
                PreferenceConnector.writeString(getActivity(), PaymentUtils.KEY_PREFERENCES_FIRST_MO_SINGLE_EXTERNAL_STATUS, PaymentUtils.EXTERNAL_STATUS_PENDING);
                MessageUtils.startSmsMessageIntent(getActivity(), shorId2, body2);
            } else {
                PaymentUtils.setPaymentFlowStage(getActivity(), PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_IN_PROGRESS);
                if (!trySendSms(shorId2, body2, SMSSentBroadcastReceiver.FIRST_SMS_SENT)) {
                    PaymentUtils.setPaymentFlowStage(getActivity(), PaymentUtils.PaymentFlowStage.FIRST_MO_SEND_ERROR);
                }
            }
        }
        onSubscriptionNextStage();
    }

    /* access modifiers changed from: protected */
    public void startProgress() {
        PaymentOption.PaymentFlowType flowType = this.mPamentOption.getPaymentFlowType();
        boolean loop = false;
        if (flowType == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN) {
            if (PaymentUtils.getPaymentOption(getActivity()).getAngebotMessageStatus() != PaymentUtils.PaymentFlowStage.ANGEBOT_MESSAGE_RECIVED) {
                loop = true;
            }
        } else if (flowType == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN && PaymentUtils.getPaymentOption(getActivity()).getWelcomeMessageStatus() != PaymentUtils.PaymentFlowStage.WELCOME_MESSAGE_RECEIVED) {
            loop = true;
        }
        if (this.mTimeProgress != null) {
            this.mTimeProgress.stopTimer();
        }
        this.mTimeProgress = new TimeProgress(30, loop);
        this.mTimeProgress.start();
    }

    public boolean trySendSms(String smsNumber, String messageBody) {
        return trySendSms(smsNumber, messageBody, SMSSentBroadcastReceiver.SECOND_SMS_SENT);
    }

    public boolean trySendSms(String smsNumber, String messageBody, String intentAction) {
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();
        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<>();
        PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(intentAction), 0);
        try {
            SmsManager smsManager = getSmsManager();
            if (TextUtils.isEmpty(messageBody)) {
                return false;
            }
            ArrayList<String> messageParts = smsManager.divideMessage(messageBody);
            for (int i = 0; i < messageParts.size(); i++) {
                sentPendingIntents.add(i, sentPI);
            }
            smsManager.sendMultipartTextMessage(smsNumber, null, messageParts, sentPendingIntents, deliveredPendingIntents);
            MessageUtils.saveMo2Sent(smsNumber, messageBody, super.getActivity().getBaseContext());
            Utils.doLog("Payment", "send sms done");
            return true;
        } catch (Exception e) {
            Utils.doLogException(e);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void onSmsSendResult(boolean isSuccessful) {
        if (isSuccessful) {
            setActivityResult();
        }
        if (this.mTimeProgress != null) {
            this.mTimeProgress.stopTimer();
        }
        onSubscriptionNextStage();
    }

    /* access modifiers changed from: protected */
    public final SmsManager getSmsManager() {
        if (this.mSmsManager == null) {
            this.mSmsManager = SmsManager.getDefault();
        }
        return this.mSmsManager;
    }

    private void setActivityResult() {
        try {
            getActivity().setResult(-1);
        } catch (Exception e) {
            Utils.doLog(e);
        }
    }

    /* access modifiers changed from: private */
    public void onAngebotOrWelcomeMessageReceived(boolean detectedThroughInbox) {
        Log.d("Payment SMS check", "onAngebotMessageReceived()");
        if (this.mAngebotReceived.compareAndSet(false, true)) {
            Log.d("Payment SMS check", "ANGEBOT OK!");
            PaymentOption.PaymentFlowType flowType = this.mPamentOption.getPaymentFlowType();
            if (detectedThroughInbox && flowType == PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN && this.mPamentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY) {
                PaymentUtils.setPaymentFlowStage(getActivity(), PaymentUtils.PaymentFlowStage.ANGEBOT_MESSAGE_RECIVED);
            }
            if (flowType == PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN && this.mPamentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY) {
                PaymentUtils.setPaymentFlowStage(getActivity(), PaymentUtils.PaymentFlowStage.WELCOME_MESSAGE_RECEIVED);
                PaymentUtils.setSCMStatus(getActivity(), ScmSubscriptionSession.SessionStatus.OPEN, 1800);
            }
            if (this.mTimeProgress != null) {
                this.mTimeProgress.stopTimer();
            }
            onSubscriptionNextStage();
        }
    }

    /* access modifiers changed from: private */
    public void checkAngebotOrWelcomeMessage() {
        Log.d("Payment SMS check", "Triggering Loader...");
        getActivity().getSupportLoaderManager().initLoader(ANGEBOT_LOADER_ID, null, this);
    }

    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        if (getActivity() == null) {
            return null;
        }
        String selection = "address like '%" + this.mPamentOption.getNumberFromServiceUrl().replaceFirst("^00", "").replace("+", "") + "%'";
        Log.d("Payment SMS check", "cursor selection query: " + selection);
        Uri inboxUri = Uri.parse("content://sms/inbox");
        String[] requiredColumns = {DmsConstants.ID, "address", "date", "body"};
        switch (loaderId) {
            case ANGEBOT_LOADER_ID /*66*/:
                return new CursorLoader(getActivity(), inboxUri, requiredColumns, selection, null, "date desc");
            default:
                return null;
        }
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d("Payment SMS check", "cursor onLoadFinished()");
        String[] body = new String[data.getCount()];
        String[] date = new String[data.getCount()];
        String[] address = new String[data.getCount()];
        if (data.moveToFirst()) {
            try {
                int bodyColumnIndex = data.getColumnIndexOrThrow("body");
                int dateColumnIndex = data.getColumnIndexOrThrow("date");
                int addressColumnIndex = data.getColumnIndexOrThrow("address");
                for (int i = 0; i < data.getCount(); i++) {
                    body[i] = data.getString(bodyColumnIndex);
                    date[i] = data.getString(dateColumnIndex);
                    address[i] = data.getString(addressColumnIndex);
                    data.moveToNext();
                }
            } catch (Exception e) {
                Log.d("Payment SMS check", "Error getting column index");
                return;
            }
        }
        int count = body.length;
        Log.d("Payment SMS check", "messages count: " + count);
        Date now = Calendar.getInstance().getTime();
        int validitySeconds = AppConfig.getInstance().getConfig().getPanic().getAngebotMessageValiditySeconds();
        for (int i2 = 0; i2 < count; i2++) {
            Date messageDate = null;
            try {
                messageDate = new Date(Long.parseLong(date[i2]));
            } catch (NumberFormatException e2) {
                Log.d("Payment SMS check", "error parsing message date");
            }
            if (messageDate != null) {
                long secondsBetween = (now.getTime() - messageDate.getTime()) / 1000;
                if (secondsBetween <= ((long) validitySeconds) && MessageUtils.isOurMessage(body[i2])) {
                    Log.d("Payment SMS check", "Found our message in INBOX: " + messageDate + " : " + address[i2] + " : " + body[i2] + " ; from " + secondsBetween + " seconds ago");
                    onAngebotOrWelcomeMessageReceived(true);
                    return;
                }
            }
        }
    }

    public void onLoaderReset(Loader<Cursor> loader) {
    }

    /* access modifiers changed from: protected */
    public void onSuccess(PaymentManagerSteps step, Object item) {
        super.onSuccess(step, item);
        Utils.doLog("Payment", "on success (fragment), step " + step);
        if (step == PaymentManagerSteps.VERIFY_PAYMENT) {
            Utils.doLog("Payment", "onSuccess (fragment) " + item);
            if (PaymentUtils.isPaymentSessionValid(PaymentUtils.PaymentStatus.valueOfOrDefault(((SubscriptionPaymentSession) item).getStatus()))) {
                PaymentUtils.setSCMStatus(null, ScmSubscriptionSession.SessionStatus.OPEN, 1800);
                if (this.mTimeProgress != null) {
                    this.mTimeProgress.stopTimer();
                }
                stopVerifyPaymentDialog();
                onSubscriptionNextStage();
                return;
            }
            scheduleVerifyPaymentRunnableIfViable();
        }
    }

    /* access modifiers changed from: protected */
    public void onFailure(PaymentManagerSteps step, Object error) {
        super.onFailure(step, error);
        Utils.doLog("Payment", "on failure (fragment), step " + step);
        if (step == PaymentManagerSteps.VERIFY_PAYMENT) {
            Utils.doLog("Payment", "onFailure (fragment) " + error);
            this.verifyPaymentCount++;
            if (this.verifyPaymentCount < 3) {
                verifyPayment(PaymentUtils.getPaymentOption(getActivity()).getSessionId());
            }
        }
    }

    private void scheduleVerifyPaymentRunnableIfViable() {
        long delay;
        long elapsed = System.currentTimeMillis() - this.lastVerifyPaymentStartTime;
        if (elapsed > 2500) {
            delay = 100;
        } else {
            delay = 2600 - elapsed;
        }
        this.verifyPaymentCount++;
        if (this.verifyPaymentCount < 3) {
            Utils.doLog("Payment", "scheduling verifyPaymentRunnable - try #" + (this.verifyPaymentCount + 1));
            this.mHandler.postDelayed(this.verifyPaymentRunnable, delay);
            return;
        }
        Utils.doLog("Payment", "ran verifyPaymentRunnable too many times - assume user is not subscribed");
        stopVerifyPaymentDialog();
    }

    class TimeProgress extends Thread {
        private static final int MAX_PROGRESS = 100;
        private boolean broadcast;
        private long checkCumulative;
        private long checkPeriod;
        private long checkTime;
        private Handler handler = new Handler(Looper.getMainLooper());
        private boolean loop;
        /* access modifiers changed from: private */
        public int progressCounter = 0;
        private boolean running;
        private long sleepTime;

        public TimeProgress(int timeSeconds, boolean loop2) {
            this.sleepTime = (long) ((timeSeconds * 1000) / 100);
            this.loop = loop2;
            this.checkTime = SystemClock.elapsedRealtime();
            this.checkPeriod = (long) (AppConfig.getInstance().getConfig().getPanic().getAngebotCheckDelaySeconds() * 1000);
        }

        public void run() {
            this.running = true;
            this.broadcast = true;
            while (this.progressCounter <= 100 && this.running) {
                updateProgress(this.progressCounter);
                try {
                    Thread.sleep(this.sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                this.progressCounter++;
                if (this.progressCounter > 100 && this.loop) {
                    this.progressCounter = 0;
                }
            }
            if (this.progressCounter <= 100 || !this.running) {
                updateProgress(100);
            } else {
                onFinish();
            }
        }

        public void updateProgress(int progress) {
            if (this.broadcast && progress >= this.progressCounter) {
                if (progress > this.progressCounter) {
                    this.progressCounter = progress;
                }
                if (this.loop) {
                    long now = SystemClock.elapsedRealtime();
                    this.checkCumulative += now - this.checkTime;
                    this.checkTime = now;
                    if (this.checkCumulative > this.checkPeriod) {
                        this.checkCumulative = 0;
                        if (this.handler != null) {
                            this.handler.post(new Runnable() {
                                public void run() {
                                    PaymentAbstractFragment.this.checkAngebotOrWelcomeMessage();
                                }
                            });
                        }
                    }
                }
                this.handler.post(new Runnable() {
                    public void run() {
                        PaymentAbstractFragment.this.onUpdateProgress(TimeProgress.this.progressCounter);
                    }
                });
            }
        }

        public void stopTimer() {
            this.running = false;
            this.broadcast = false;
        }

        private void onFinish() {
            if (this.broadcast) {
                this.handler.post(new Runnable() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment.access$1(ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment, boolean):void
                     arg types: [ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment, int]
                     candidates:
                      ch.nth.android.paymentsdk.v2.fragments.base.BasePaymentFragment.access$1(ch.nth.android.paymentsdk.v2.fragments.base.BasePaymentFragment, java.lang.String):void
                      ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment.access$1(ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment, boolean):void */
                    public void run() {
                        PaymentAbstractFragment.this.onSmsSendResult(false);
                    }
                });
            }
        }
    }
}
