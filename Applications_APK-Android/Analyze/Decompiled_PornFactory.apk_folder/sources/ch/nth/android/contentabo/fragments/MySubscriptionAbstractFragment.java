package ch.nth.android.contentabo.fragments;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.common.recivers.SMSSentBroadcastReceiver;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.common.utils.MessageUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.mas.MasConfig;
import ch.nth.android.contentabo.config.payment.CancelSubscriptionConfig;
import ch.nth.android.contentabo.config.payment.PMConfig;
import ch.nth.android.contentabo.fragments.base.BaseFragment;
import ch.nth.android.contentabo.fragments.common.YesNoDialogFragment;
import ch.nth.android.contentabo.models.payment.PaymentOption;
import ch.nth.android.contentabo.translations.LanguageChangedEvent;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;
import ch.nth.android.paymentsdk.v2.async.AsyncInfoRequest;
import ch.nth.android.paymentsdk.v2.enums.PaymentManagerSteps;
import ch.nth.android.paymentsdk.v2.listeners.GenericStringContentListener;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.SubscriptionPaymentSession;
import com.google.gson.Gson;
import com.nth.android.mas.MASLayout;
import de.greenrobot.event.EventBus;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SuppressLint({"UseSparseArrays"})
public abstract class MySubscriptionAbstractFragment extends BaseFragment implements YesNoDialogFragment.YesNoDialogListener {
    private static final int HANDLER_DELAYED_VERIFY = 2385;
    private static final int TAG_ABORT_SUBSCRIPTION_DIALOG_CALLBACK = 64;
    private static final String TAG_ABORT_SUBSCRIPTION_DIALOG_FRAGMENT = "tag_abort_subscription_dialog_fragment";
    private static final int TAG_SUBSCRIPTION_EXPIRED_DIALOG_CALLBACK = 80;
    private static final String TAG_SUBSCRIPTION_EXPIRED_DIALOG_FRAGMENT = "tag_subscription_expired_dialog_fragment";
    private static final int TAG_SUBSCRIPTION_PASSIVE_DIALOG_CALLBACK = 96;
    private static final String TAG_SUBSCRIPTION_PASSIVE_DIALOG_FRAGMENT = "tag_subscription_passive_dialog_fragment";
    private DateFormat fromFormat;
    ProgressDialog mDialog;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == MySubscriptionAbstractFragment.HANDLER_DELAYED_VERIFY) {
                Utils.doLog("HANDLER_DELAYED_VERIFY received - refreshing user info");
                MySubscriptionAbstractFragment.this.refreshUserInfo();
            }
        }
    };
    private SubscriptionPaymentSession mSession;
    private GenericStringContentListener requestInfoListener = new GenericStringContentListener() {
        public void onContentNotAvailable(int statusCode) {
            MySubscriptionAbstractFragment.this.requestInfoPerformed = true;
            Utils.doLog("InfoResponse FAILED! [onContentNotAvailable(" + statusCode + "]");
            MySubscriptionAbstractFragment.this.performCancelPayment();
        }

        public void onContentAvailable(int statusCode, String htmlContent) {
            MySubscriptionAbstractFragment.this.requestInfoPerformed = true;
            try {
                InfoResponse infoResponse = (InfoResponse) new Gson().fromJson(htmlContent, InfoResponse.class);
                PaymentUtils.getPaymentOption(MySubscriptionAbstractFragment.this.getActivity()).processInfoResponseSilent(infoResponse);
                PaymentUtils.savePaymentOption(MySubscriptionAbstractFragment.this.getActivity());
                Utils.doLog("InfoResponse successfully fetched!");
                Utils.doLog(infoResponse);
            } catch (Exception e) {
                Utils.doLogException(e);
            }
            MySubscriptionAbstractFragment.this.performCancelPayment();
        }
    };
    /* access modifiers changed from: private */
    public boolean requestInfoPerformed = false;
    private DateFormat toFormat;

    public abstract int getAbortSubscriptionDialogView();

    public abstract int getSubscriptionExpiredDialogView();

    public abstract void onRefreshUserInfo(SubscriptionPaymentSession subscriptionPaymentSession);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.fromFormat = new SimpleDateFormat(App.getInstance().getString(R.string.payment_date_format));
        this.toFormat = android.text.format.DateFormat.getDateFormat(App.getInstance());
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MASLayout masLayout = (MASLayout) view.findViewById(R.id.mas_layout);
        if (masLayout != null) {
            MasConfig masConfig = null;
            try {
                masConfig = AppConfig.getInstance().getConfig().getModules().getMySubscriptionsModule().getMasConfig();
                if (masConfig == null) {
                    masConfig = MasConfig.newDefaultInstance();
                }
            } catch (Exception e) {
                Utils.doLogException(e);
                if (0 == 0) {
                    masConfig = MasConfig.newDefaultInstance();
                }
            } catch (Throwable th) {
                if (0 == 0) {
                    MasConfig masConfig2 = MasConfig.newDefaultInstance();
                }
                throw th;
            }
            if (masConfig.isEnabled()) {
                masLayout.setMasId(masConfig.getKey());
            }
        }
    }

    public void onResume() {
        super.onResume();
        refreshUserInfo();
        EventBus.getDefault().register(this);
    }

    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(LanguageChangedEvent event) {
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(0);
        }
    }

    /* access modifiers changed from: protected */
    public void refreshUserInfo() {
        String sessionId = PaymentUtils.getPaymentOption(getActivity()).getSessionId();
        showProgressDialog();
        verifyPayment(sessionId);
    }

    /* access modifiers changed from: protected */
    public boolean isUserActive() {
        if (this.mSession.getTimeAuthorized() == null || this.mSession.getTimeClosed() != null || !this.mSession.isRecurrent()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isUserPassive() {
        if (this.mSession.getTimeAuthorized() == null || this.mSession.getTimeClosed() != null || this.mSession.isRecurrent()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isUserActiveFree() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean showActionButton() {
        if (isUserActive()) {
            return true;
        }
        if (!isTestMode() || !isUserPassive()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void doShowActionDialog() {
        int viewId;
        int tagCalbackId;
        String tagFragement;
        HashMap<Integer, String> translationKeys;
        if (isUserActive()) {
            viewId = getAbortSubscriptionDialogView();
            tagCalbackId = 64;
            tagFragement = TAG_ABORT_SUBSCRIPTION_DIALOG_FRAGMENT;
            translationKeys = getAbortSubscriptionTranslations();
        } else if (isUserActive() || isUserPassive()) {
            viewId = getSubscriptionExpiredDialogView();
            tagCalbackId = TAG_SUBSCRIPTION_PASSIVE_DIALOG_CALLBACK;
            tagFragement = TAG_SUBSCRIPTION_PASSIVE_DIALOG_FRAGMENT;
            translationKeys = getSubscriptionExpiredTranslations();
        } else {
            viewId = getSubscriptionExpiredDialogView();
            tagCalbackId = TAG_SUBSCRIPTION_EXPIRED_DIALOG_CALLBACK;
            tagFragement = TAG_SUBSCRIPTION_EXPIRED_DIALOG_FRAGMENT;
            translationKeys = getSubscriptionExpiredTranslations();
        }
        YesNoDialogFragment dialog = YesNoDialogFragment.newInstance(this, tagCalbackId, viewId, true, translationKeys);
        dialog.setTargetFragment(this, 0);
        dialog.show(getActivity().getSupportFragmentManager(), tagFragement);
    }

    public void onDialogPositiveButtonClick(int alertDialogTag) {
        switch (alertDialogTag) {
            case 64:
                Utils.doLog("TAG_ABORT_SUBSCRIPTION_DIALOG_CALLBACK");
                performCancelPayment();
                return;
            case TAG_SUBSCRIPTION_EXPIRED_DIALOG_CALLBACK /*80*/:
                Utils.doLog("TODO");
                return;
            case TAG_SUBSCRIPTION_PASSIVE_DIALOG_CALLBACK /*96*/:
                PaymentOption paymentOption = PaymentUtils.getPaymentOption(getActivity());
                showProgressDialog();
                closePayment(paymentOption.getSessionId());
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void performCancelPayment() {
        Utils.doLog("starting performCancelPayment() ...");
        CancelSubscriptionConfig cancelSubscriptionConfig = AppConfig.getInstance().getConfig().getPm().getCancelSubscriptionConfig();
        showProgressDialog();
        PaymentOption paymentOption = PaymentUtils.getPaymentOption(getActivity());
        Utils.doLog(paymentOption);
        boolean cancelPaymentInfoInitialized = this.requestInfoPerformed || paymentOption.isCancelPaymentInfoInitialized();
        String sessionId = paymentOption.getSessionId();
        if (cancelPaymentInfoInitialized) {
            boolean useCancelWithSms = paymentOption.isCancelPaymentWithSms();
            String body = paymentOption.getBodyFromCancelUrl();
            String shortId = paymentOption.getNumberFromCancelUrl();
            if (!useCancelWithSms || TextUtils.isEmpty(shortId) || TextUtils.isEmpty(body)) {
                Utils.doLog("Performing cancelPayment with PW method [sessionId=" + sessionId + "]");
                cancelPayment(sessionId);
                AnalyticsUtils.tagEvent(getActivity(), getAnalyticsSession(), AnalyticsUtils.SUBSCRIPTION_CLOSED_EVENT);
                return;
            }
            Utils.doLog("Performing cancelPayment with SMS [shid=" + shortId + ", body=" + body + "]");
            if (!trySendSms(shortId, body, SMSSentBroadcastReceiver.CANCEL_PAYMENT_SENT)) {
                showToastSafe(Translations.getPolyglot().getString(T.string.error_has_occurred));
                stopProgressDialog();
                Utils.doLog("an error occured while sending unsubscribe sms");
                return;
            }
            int delayMillis = cancelSubscriptionConfig.getVerifyDelaySeconds() * 1000;
            this.mHandler.sendEmptyMessageDelayed(HANDLER_DELAYED_VERIFY, (long) delayMillis);
            Utils.doLog("scheduling HANDLER_DELAYED_VERIFY in " + delayMillis + "ms");
            AnalyticsUtils.tagEvent(getActivity(), getAnalyticsSession(), AnalyticsUtils.SUBSCRIPTION_CLOSED_EVENT);
            return;
        }
        PMConfig config = AppConfig.getInstance().getConfig().getPm();
        if (TextUtils.isEmpty(config.getUrl()) || TextUtils.isEmpty(config.getApiKey()) || TextUtils.isEmpty(config.getApiSecret())) {
            Utils.doLog("Missing info to request cancelPayment data! Performing cancelPayment with PW method [sessionId=" + sessionId + "]");
            cancelPayment(sessionId);
            AnalyticsUtils.tagEvent(getActivity(), getAnalyticsSession(), AnalyticsUtils.SUBSCRIPTION_CLOSED_EVENT);
            return;
        }
        Utils.doLog("Requesting cancelPayment info [sessionId=" + sessionId + "]");
        Map<String, String> params = new HashMap<>();
        params.put("sid", sessionId);
        new AsyncInfoRequest(config.getUrl(), params, config.getApiKey(), config.getApiSecret(), this.requestInfoListener).execute(new Void[0]);
    }

    public void onDialogNegativeButtonClick(int alertDialogTag) {
    }

    public boolean trySendSms(String smsNumber, String messageBody, String intentAction) {
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();
        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<>();
        PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(intentAction), 0);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            if (TextUtils.isEmpty(messageBody)) {
                return false;
            }
            ArrayList<String> messageParts = smsManager.divideMessage(messageBody);
            for (int i = 0; i < messageParts.size(); i++) {
                sentPendingIntents.add(i, sentPI);
            }
            smsManager.sendMultipartTextMessage(smsNumber, null, messageParts, sentPendingIntents, deliveredPendingIntents);
            MessageUtils.saveMo2Sent(smsNumber, messageBody, super.getActivity().getBaseContext());
            Utils.doLog("Payment", "send sms done");
            return true;
        } catch (Exception e) {
            Utils.doLogException(e);
            return false;
        }
    }

    public String getFormatedDate(String dateString) {
        if (dateString == null) {
            return "";
        }
        String formatedTime = "";
        try {
            formatedTime = this.toFormat.format(this.fromFormat.parse(dateString));
        } catch (ParseException e) {
            Utils.doLogException(e);
        }
        Utils.doLog("getFormatedDate " + dateString + " out " + formatedTime);
        return formatedTime;
    }

    /* access modifiers changed from: protected */
    public void showProgressDialog() {
        try {
            if (this.mDialog == null) {
                this.mDialog = new ProgressDialog(getActivity());
                this.mDialog.setProgressStyle(0);
                this.mDialog.setIndeterminate(true);
                this.mDialog.setCancelable(false);
                this.mDialog.setCanceledOnTouchOutside(false);
                this.mDialog.setMessage(Translations.getPolyglot().getString(T.string.progress_dialog_text));
            }
            if (!this.mDialog.isShowing()) {
                this.mDialog.show();
            }
        } catch (Exception e) {
            Utils.doLogException(e);
        }
    }

    /* access modifiers changed from: protected */
    public void stopProgressDialog() {
        try {
            if (this.mDialog != null) {
                this.mDialog.dismiss();
            }
        } catch (Exception e) {
            Utils.doLogException(e);
        }
    }

    /* access modifiers changed from: protected */
    public String getExpireDate() {
        return this.toFormat.format(new Date(System.currentTimeMillis() + ((long) (this.mSession.getExpiresIn() * 1000))));
    }

    /* access modifiers changed from: protected */
    public void onSuccess(PaymentManagerSteps step, Object item) {
        if (step == PaymentManagerSteps.VERIFY_PAYMENT) {
            Utils.doLog("Payment", "onSuccess " + item);
            this.mSession = (SubscriptionPaymentSession) item;
            Utils.doLog("Payment", this.mSession);
            onRefreshUserInfo(this.mSession);
            stopProgressDialog();
        } else if (step == PaymentManagerSteps.CANCEL_PAYMENT) {
            refreshUserInfo();
        } else if (step == PaymentManagerSteps.CLOSE_PAYMENT) {
            PaymentUtils.resetPaymentOption(getActivity());
            refreshUserInfo();
        } else {
            stopProgressDialog();
        }
    }

    /* access modifiers changed from: protected */
    public void onFailure(PaymentManagerSteps step, Object error) {
        stopProgressDialog();
        Utils.doLog("Payment", "error " + error);
    }

    /* access modifiers changed from: protected */
    public HashMap<Integer, String> getAbortSubscriptionTranslations() {
        String abortMessageText = String.format(Translations.getPolyglot().getString(T.string.mysubscription_dialogabort_message_text), getString(R.string.app_name));
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.mysubscription_dialogabort_label_title));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), abortMessageText);
        translations.put(Integer.valueOf(R.id.btn_dialog_negative), Translations.getPolyglot().getString(T.string.mysubscription_dialog_button_cancel));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString(T.string.mysubscription_dialogabort_button_ok));
        return translations;
    }

    /* access modifiers changed from: protected */
    public HashMap<Integer, String> getSubscriptionExpiredTranslations() {
        HashMap<Integer, String> translations = new HashMap<>();
        translations.put(Integer.valueOf(R.id.text_dialog_title), Translations.getPolyglot().getString(T.string.mysubscription_dialogabort_label_title));
        translations.put(Integer.valueOf(R.id.text_dialog_primary), Translations.getPolyglot().getString(T.string.mysubscription_dialogsubscribe_label_text));
        translations.put(Integer.valueOf(R.id.btn_dialog_negative), Translations.getPolyglot().getString(T.string.mysubscription_dialog_button_cancel));
        translations.put(Integer.valueOf(R.id.btn_dialog_positive), Translations.getPolyglot().getString(T.string.mysubscription_dialogsubscribe_button_ok));
        return translations;
    }
}
