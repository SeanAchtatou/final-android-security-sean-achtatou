package ch.nth.android.contentabo.common.utils;

import ch.nth.android.contentabo.App;
import java.util.concurrent.TimeUnit;

public class EntStoreUtils {
    public static final String PACKAGE = App.getInstance().getPackageName();
    public static final String PREFS_KEY_LAST_REMIND_TIME = (String.valueOf(PACKAGE) + ".lastRemind_Time");

    public static boolean shouldShowReminder(long remindDays) {
        if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis() - PreferenceConnector.readLong(App.getInstance(), PREFS_KEY_LAST_REMIND_TIME, 0)) >= remindDays) {
            return true;
        }
        return false;
    }

    public static void setReminderTime() {
        PreferenceConnector.writeLong(App.getInstance(), PREFS_KEY_LAST_REMIND_TIME, System.currentTimeMillis());
    }
}
