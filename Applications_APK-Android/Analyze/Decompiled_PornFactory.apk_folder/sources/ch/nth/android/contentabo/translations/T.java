package ch.nth.android.contentabo.translations;

public final class T {

    public final class string {
        public static final String action_settings = "action_settings";
        public static final String add_comment = "add_comment";
        public static final String agb_label_title = "agb_label_title";
        public static final String age_verification_dialog_message = "age_verification_dialog_message";
        public static final String age_verification_dialog_title = "age_verification_dialog_title";
        public static final String app_component_help = "app_component_help";
        public static final String app_component_home = "app_component_home";
        public static final String app_component_key_help = "app_component_key_help";
        public static final String app_component_key_home = "app_component_key_home";
        public static final String app_component_key_legal = "app_component_key_legal";
        public static final String app_component_key_my_subscriptions = "app_component_key_my_subscriptions";
        public static final String app_component_legal = "app_component_legal";
        public static final String app_component_my_subscriptions = "app_component_my_subscriptions";
        public static final String app_name = "app_name";
        public static final String app_unavailable_message_text = "app_unavailable_message_text";
        public static final String app_unavailable_title = "app_unavailable_title";
        public static final String app_update_notification_message = "app_update_notification_message";
        public static final String app_update_notification_ticker = "app_update_notification_ticker";
        public static final String cancel = "cancel";
        public static final String categories = "categories";
        public static final String check_connectivity_message = "check_connectivity_message";
        public static final String check_connectivity_title = "check_connectivity_title";
        public static final String comment = "comment";
        public static final String comment_submit_failed = "comment_submit_failed";
        public static final String comment_submit_in_progress = "comment_submit_in_progress";
        public static final String comment_submit_success = "comment_submit_success";
        public static final String comments = "comments";
        public static final String comments_dialog_already_commented = "comments_dialog_already_commented";
        public static final String comments_enter_nickname_and_content = "comments_enter_nickname_and_content";
        public static final String default_content_title = "default_content_title";
        public static final String description = "description";
        public static final String error_fetching_video_details = "error_fetching_video_details";
        public static final String error_has_occurred = "error_has_occurred";
        public static final String error_setting_up_application = "error_setting_up_application";
        public static final String label_comments = "label_comments";
        public static final String legal_label_title = "legal_label_title";
        public static final String listview_disclaimer_header_text = "listview_disclaimer_header_text";
        public static final String mandatory_update_available_message = "mandatory_update_available_message";
        public static final String mysubscription_dialog_button_cancel = "mysubscription_dialog_button_cancel";
        public static final String mysubscription_dialogabort_button_ok = "mysubscription_dialogabort_button_ok";
        public static final String mysubscription_dialogabort_label_title = "mysubscription_dialogabort_label_title";
        public static final String mysubscription_dialogabort_message_text = "mysubscription_dialogabort_message_text";
        public static final String mysubscription_dialogsubscribe_button_ok = "mysubscription_dialogsubscribe_button_ok";
        public static final String mysubscription_dialogsubscribe_label_text = "mysubscription_dialogsubscribe_label_text";
        public static final String mysubscription_dialogsubscribe_label_title = "mysubscription_dialogsubscribe_label_title";
        public static final String mysubscription_label_button_abort = "mysubscription_label_button_abort";
        public static final String mysubscription_label_button_subscribe = "mysubscription_label_button_subscribe";
        public static final String mysubscription_label_membership_price = "mysubscription_label_membership_price";
        public static final String mysubscription_label_membership_price_text = "mysubscription_label_membership_price_text";
        public static final String mysubscription_label_time_created = "mysubscription_label_time_created";
        public static final String mysubscription_label_time_default = "mysubscription_label_time_default";
        public static final String mysubscription_label_time_expire = "mysubscription_label_time_expire";
        public static final String mysubscription_label_time_expire_free = "mysubscription_label_time_expire_free";
        public static final String mysubscription_label_time_expire_passive = "mysubscription_label_time_expire_passive";
        public static final String mysubscription_label_time_expired = "mysubscription_label_time_expired";
        public static final String mysubscription_label_title = "mysubscription_label_title";
        public static final String navigation_categories_header = "navigation_categories_header";
        public static final String navigation_drawer_close = "navigation_drawer_close";
        public static final String navigation_drawer_open = "navigation_drawer_open";
        public static final String navigation_titles_header = "navigation_titles_header";
        public static final String new_update_available_message = "new_update_available_message";
        public static final String new_update_available_title = "new_update_available_title";
        public static final String nickname = "nickname";
        public static final String no = "no";
        public static final String no_comments = "no_comments";
        public static final String no_content = "no_content";
        public static final String no_related_videos = "no_related_videos";
        public static final String ok = "ok";
        public static final String post_comment = "post_comment";
        public static final String proceed_btn = "proceed_btn";
        public static final String progress_dialog_text = "progress_dialog_text";
        public static final String rate_video = "rate_video";
        public static final String rating_dialog_already_voted = "rating_dialog_already_voted";
        public static final String rating_dialog_failure = "rating_dialog_failure";
        public static final String rating_dialog_pending = "rating_dialog_pending";
        public static final String rating_dialog_rate_video = "rating_dialog_rate_video";
        public static final String rating_dialog_success = "rating_dialog_success";
        public static final String retry_on_error_button = "retry_on_error_button";
        public static final String retry_on_error_text_pattern = "retry_on_error_text_pattern";
        public static final String retry_on_error_title = "retry_on_error_title";
        public static final String search_hint = "search_hint";
        public static final String search_title = "search_title";
        public static final String sort_videos_by = "sort_videos_by";
        public static final String sorting_newest = "sorting_newest";
        public static final String sorting_popularity = "sorting_popularity";
        public static final String sorting_top_rated = "sorting_top_rated";
        public static final String spending_limit_button_ok = "spending_limit_button_ok";
        public static final String spending_limit_message_text = "spending_limit_message_text";
        public static final String spending_limit_notification_message = "spending_limit_notification_message";
        public static final String spending_limit_notification_ticker = "spending_limit_notification_ticker";
        public static final String tab_title_comments = "tab_title_comments";
        public static final String tab_title_related_videos = "tab_title_related_videos";
        public static final String tab_title_video_details = "tab_title_video_details";
        public static final String tags = "tags";
        public static final String temporarily_disabled_button_ok = "temporarily_disabled_button_ok";
        public static final String temporarily_disabled_label_title = "temporarily_disabled_label_title";
        public static final String temporarily_disabled_message_text = "temporarily_disabled_message_text";
        public static final String update_with_mobile_dialog_text = "update_with_mobile_dialog_text";
        public static final String video_details_label_category = "video_details_label_category";
        public static final String video_details_label_description = "video_details_label_description";
        public static final String video_details_label_duration = "video_details_label_duration";
        public static final String video_details_label_rating = "video_details_label_rating";
        public static final String video_details_label_tags = "video_details_label_tags";
        public static final String video_details_label_uploaded = "video_details_label_uploaded";
        public static final String video_details_label_viewed = "video_details_label_viewed";
        public static final String video_details_subscription_disclaimer = "video_details_subscription_disclaimer";
        public static final String video_details_subscription_sms_info = "video_details_subscription_sms_info";
        public static final String video_details_subscription_start_video = "video_details_subscription_start_video";
        public static final String video_details_subscription_try_again = "video_details_subscription_try_again";
        public static final String video_details_subscription_video_lenght = "video_details_subscription_video_lenght";
        public static final String video_details_subscription_video_name = "video_details_subscription_video_name";
        public static final String video_details_subscription_video_status = "video_details_subscription_video_status";
        public static final String video_details_subscription_video_status_done = "video_details_subscription_video_status_done";
        public static final String video_list_item_category = "video_list_item_category";
        public static final String video_list_item_duration = "video_list_item_duration";
        public static final String video_list_item_rating = "video_list_item_rating";
        public static final String video_list_item_uploaded = "video_list_item_uploaded";
        public static final String video_list_item_views = "video_list_item_views";
        public static final String video_name = "video_name";
        public static final String view_comments = "view_comments";
        public static final String yes = "yes";

        public string() {
        }
    }
}
