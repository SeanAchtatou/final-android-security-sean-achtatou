package ch.nth.android.contentabo.navigation;

import android.content.Intent;
import android.os.Bundle;

public interface NavigationListener {
    void onNavigate(NavigationTarget navigationTarget, Bundle bundle, boolean z);

    void onNavigate(NavigationTarget navigationTarget, boolean z);

    void startActivity(Intent intent);
}
