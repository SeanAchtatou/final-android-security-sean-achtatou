package ch.nth.android.contentabo.models.content;

import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class Account implements Serializable {
    private static final long serialVersionUID = -5370848405776398134L;
    private List<String> categories;
    @SerializedName("default_content")
    private List<String> defaultContent;
    @SerializedName(DmsConstants.ID)
    private String id;
    private String name;
    private String password;
    private String username;

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public List<String> getDefaultContent() {
        return this.defaultContent;
    }

    public List<String> getCategories() {
        return this.categories;
    }
}
