package ch.nth.android.contentabo.models.content;

import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class Tag implements Serializable {
    private static final long serialVersionUID = -202351119324492710L;
    @SerializedName(DmsConstants.ID)
    private String id;
    private String name;

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
}
