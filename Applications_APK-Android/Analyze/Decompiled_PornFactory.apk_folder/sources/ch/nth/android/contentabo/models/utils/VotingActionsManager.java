package ch.nth.android.contentabo.models.utils;

import android.content.Context;
import ch.nth.android.contentabo.App;

public class VotingActionsManager extends ActionsManager {
    private static final String FILENAME = "vote_history.xml";
    private static VotingActionsManager sInstance;

    public static VotingActionsManager getInstance() {
        if (sInstance == null) {
            sInstance = new VotingActionsManager(App.getInstance());
        }
        return sInstance;
    }

    protected VotingActionsManager(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public String getFilename() {
        return FILENAME;
    }
}
