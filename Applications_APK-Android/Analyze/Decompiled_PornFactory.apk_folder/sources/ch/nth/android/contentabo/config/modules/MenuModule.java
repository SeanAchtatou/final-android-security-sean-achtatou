package ch.nth.android.contentabo.config.modules;

import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.R;
import ch.nth.simpleplist.annotation.Property;

public class MenuModule {
    @Property(name = "show_agb", stringCompatibility = true)
    private boolean agbVisibile = true;
    @Property(name = "show_legal_notes", stringCompatibility = true)
    private boolean legalNotesVisibile = true;
    @Property(name = "show_my_subscription", stringCompatibility = true)
    private boolean mySubscriptionVisibile = true;

    public boolean isVisibileByComponenetKey(String key) {
        if (App.getInstance().getString(R.string.app_component_key_my_subscriptions).equals(key)) {
            return isMySubscriptionVisibile();
        }
        if (App.getInstance().getString(R.string.app_component_key_legal).equals(key)) {
            return isLegalNotesVisibile();
        }
        if (App.getInstance().getString(R.string.app_component_key_help).equals(key)) {
            return isAgbVisibile();
        }
        return true;
    }

    public boolean isMySubscriptionVisibile() {
        return this.mySubscriptionVisibile;
    }

    public boolean isLegalNotesVisibile() {
        return this.legalNotesVisibile;
    }

    public boolean isAgbVisibile() {
        return this.agbVisibile;
    }
}
