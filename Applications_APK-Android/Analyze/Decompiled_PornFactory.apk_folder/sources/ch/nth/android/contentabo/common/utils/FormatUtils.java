package ch.nth.android.contentabo.common.utils;

import android.content.Context;
import ch.nth.android.contentabo.models.content.Category;
import ch.nth.android.contentabo.models.content.Tag;
import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class FormatUtils {
    private static DateFormat sDateFormat;

    public static String formatStringList(List<String> strings) {
        if (strings == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        Iterator<String> it = strings.iterator();
        while (it.hasNext()) {
            builder.append(it.next());
            if (it.hasNext()) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    public static String formatTagList(List<Tag> strings) {
        if (strings == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        Iterator<Tag> it = strings.iterator();
        while (it.hasNext()) {
            builder.append(it.next().getName());
            if (it.hasNext()) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    public static String formatCategoriesList(List<Category> categories) {
        if (categories == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        Iterator<Category> it = categories.iterator();
        while (it.hasNext()) {
            builder.append(it.next().getName());
            if (it.hasNext()) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    public static String getFirstCategoryOrEmptyString(List<Category> categories) {
        if (categories == null || categories.size() == 0) {
            return "";
        }
        return categories.get(0).getName();
    }

    public static String formatContentDuration(int seconds) {
        int secondsTotal;
        int minutesTotal;
        int hoursTotal;
        if (seconds >= 60) {
            secondsTotal = seconds % 60;
        } else {
            secondsTotal = seconds;
        }
        int seconds2 = seconds / 60;
        if (seconds2 >= 60) {
            minutesTotal = seconds2 % 60;
        } else {
            minutesTotal = seconds2;
        }
        int seconds3 = seconds2 / 60;
        if (seconds3 >= 24) {
            hoursTotal = seconds3 % 24;
        } else {
            hoursTotal = seconds3;
        }
        if (hoursTotal > 0) {
            return String.valueOf(String.valueOf(hoursTotal)) + ":" + leftPad(String.valueOf(minutesTotal), 2, "0") + ":" + leftPad(String.valueOf(secondsTotal), 2, "0");
        }
        return String.valueOf(String.valueOf(minutesTotal)) + ":" + leftPad(String.valueOf(secondsTotal), 2, "0");
    }

    public static String leftPad(String str, int size, String padStr) {
        if (str == null) {
            return null;
        }
        int padLen = padStr.length();
        int pads = size - str.length();
        if (pads <= 0) {
            return str;
        }
        if (pads == padLen) {
            return padStr.concat(str);
        }
        if (pads < padLen) {
            return padStr.substring(0, pads).concat(str);
        }
        char[] padding = new char[pads];
        char[] padChars = padStr.toCharArray();
        for (int i = 0; i < pads; i++) {
            padding[i] = padChars[i % padLen];
        }
        return new String(padding).concat(str);
    }

    public static String formatLocalDate(Context context, Date date) {
        if (date == null) {
            return "";
        }
        if (sDateFormat == null) {
            if (context == null) {
                return "";
            }
            sDateFormat = android.text.format.DateFormat.getDateFormat(context);
        }
        return sDateFormat.format(date);
    }
}
