package ch.nth.android.contentabo.config;

import ch.nth.android.simpleplist.task.config.ConfigType;

public abstract class AbstractPlistConfigHolder {
    private PlistConfig mConfig;
    private ConfigType mConfigType;

    protected AbstractPlistConfigHolder() {
    }

    public boolean isInitialized() {
        return this.mConfig != null;
    }

    public PlistConfig getConfig() {
        return this.mConfig;
    }

    public void setConfig(PlistConfig config) {
        this.mConfig = config;
    }

    public ConfigType getConfigType() {
        return this.mConfigType;
    }

    public void setConfigType(ConfigType configType) {
        this.mConfigType = configType;
    }
}
