package ch.nth.android.contentabo.config.update;

import ch.nth.simpleplist.annotation.Property;

public class EntStoreProperties {
    @Property(name = "current_version", stringCompatibility = true)
    private int currentVersion;
    @Property(name = "minimum_version", stringCompatibility = true)
    private int minimumVersion;
    @Property(name = "reminder_days", stringCompatibility = true)
    private int reminderDays;

    public int getCurrentVersion() {
        return this.currentVersion;
    }

    public int getMinimumVersion() {
        return this.minimumVersion;
    }

    public int getReminderDays() {
        return this.reminderDays;
    }
}
