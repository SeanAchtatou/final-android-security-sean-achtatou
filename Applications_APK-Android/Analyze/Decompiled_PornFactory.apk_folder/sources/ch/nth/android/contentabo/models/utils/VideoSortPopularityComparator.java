package ch.nth.android.contentabo.models.utils;

import ch.nth.android.contentabo.models.content.Content;
import java.util.Comparator;

public class VideoSortPopularityComparator implements Comparator<Content> {
    public int compare(Content lhs, Content rhs) {
        if (lhs.getDownloadCount() < rhs.getDownloadCount()) {
            return 1;
        }
        return lhs.getDownloadCount() > rhs.getDownloadCount() ? -1 : 0;
    }
}
