package ch.nth.android.contentabo.activities;

import android.text.TextUtils;
import ch.nth.android.contentabo.activities.base.HomeAbstractActivity;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.PlistConfig;
import ch.nth.android.contentabo.fragments.VideoListPageAbstractFragment;
import ch.nth.android.contentabo.models.content.Content;
import com.nth.gcm.GCMManager;

public abstract class MainAbstractActivity extends HomeAbstractActivity implements VideoListPageAbstractFragment.IVideoClickListener {
    private Content mContent;
    private boolean mUserAction;

    /* access modifiers changed from: protected */
    public boolean shouldCreateNavigationDrawer() {
        return true;
    }

    public void onVideoItemClicked(Content content) {
        if (isWorking()) {
            this.mUserAction = true;
            this.mContent = content;
            Utils.doLog("Payment", "showProgressDialog 1");
            showProgressDialog();
        } else if (shouldSendFirstMoSMS()) {
            Utils.doLog("Payment", "shouldSendFirstMoSMS " + shouldSendFirstMoSMS());
            this.mUserAction = true;
            this.mContent = content;
            Utils.doLog("Payment", "showProgressDialog 2");
            showProgressDialog();
            doSendFirstSMS();
        } else if (shouldRefreshSCMStatus()) {
            this.mUserAction = true;
            this.mContent = content;
            showProgressDialog();
            doVerifySCMSubscription();
        } else {
            this.mUserAction = false;
            startContentDetailsActivity(content.getId(), content);
        }
    }

    /* access modifiers changed from: protected */
    public void onSelectNavigationItem(int position) {
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        PlistConfig config = AppConfig.getInstance().getConfig();
        if (config != null && !TextUtils.isEmpty(config.getPushNotificationsUrl())) {
            String gcmRegisterUrl = String.valueOf(config.getPushNotificationsUrl()) + "/tokens";
            String gcmProjectId = AppConfig.getInstance().getConfig().getGcmProjectId();
            if (!TextUtils.isEmpty(gcmProjectId)) {
                Utils.doLog("GCM", "trying to register on: " + gcmRegisterUrl);
                GCMManager.register(getApplicationContext(), gcmRegisterUrl, gcmProjectId, AppConfig.getInstance().getConfig().getApiKey());
                return;
            }
            Utils.doLog("GCM", "skipping register, no gcm project id specified");
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public void onBackendCallFinish(int jobId) {
        if (this.mUserAction) {
            if (jobId == BACKEND_JOB_MO_MESSAGE_RECEIVER) {
                stopProgressDialog();
            } else {
                Utils.doLog("Payment", "shouldSendFirstMoSMS " + shouldSendFirstMoSMS());
                if (shouldSendFirstMoSMS()) {
                    Utils.doLog("Payment", "showProgressDialog 3");
                    showProgressDialog();
                    doSendFirstSMS();
                    return;
                }
            }
            stopProgressDialog();
            this.mUserAction = false;
            if (this.mContent != null) {
                startContentDetailsActivity(this.mContent.getId(), this.mContent);
            }
        }
    }
}
