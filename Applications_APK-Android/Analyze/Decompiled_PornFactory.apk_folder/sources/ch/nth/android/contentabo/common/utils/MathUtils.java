package ch.nth.android.contentabo.common.utils;

public class MathUtils {
    public static final double roundToNearest(double number, double chunk) {
        return ((double) Math.round(number / chunk)) * chunk;
    }
}
