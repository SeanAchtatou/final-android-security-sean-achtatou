package ch.nth.android.contentabo.translations;

import android.content.Intent;
import android.content.IntentFilter;
import ch.nth.android.contentabo.App;
import ch.nth.android.polyglot.linguistics.Linguist;
import ch.nth.android.polyglot.translator.Modules;
import ch.nth.android.polyglot.translator.TranslationModule;
import ch.nth.android.polyglot.translator.TranslationStorage;

public class Languages {
    public static final String LANGUAGE_CHANGED_ACTION = "ch.nth.android.contentabo.translations.LANGUAGE_CHANGED_ACTION";
    private static volatile Languages instance = null;
    private Linguist mLinguist;

    private Languages() {
    }

    public static Linguist getLinguist() {
        if (instance == null) {
            synchronized (Languages.class) {
                if (instance == null) {
                    instance = new Languages();
                    initialize();
                }
            }
        }
        return instance.mLinguist;
    }

    public static Linguist initLinguist(TranslationModule translations) {
        if (instance == null) {
            synchronized (Languages.class) {
                if (instance == null) {
                    instance = new Languages();
                }
            }
        }
        instance.mLinguist = new Linguist(translations);
        return instance.mLinguist;
    }

    public static void testMethodOnlyClearInstance() {
        instance = null;
    }

    private static void initialize() {
        TranslationModule translations = TranslationStorage.deserializeModule(App.getInstance(), Modules.MODULE_LANGS);
        instance.mLinguist = new Linguist(translations);
    }

    public static Intent newLanguageChangedIntent() {
        return new Intent(LANGUAGE_CHANGED_ACTION);
    }

    public static IntentFilter newLanguageChangedIntentFilter() {
        return new IntentFilter(LANGUAGE_CHANGED_ACTION);
    }
}
