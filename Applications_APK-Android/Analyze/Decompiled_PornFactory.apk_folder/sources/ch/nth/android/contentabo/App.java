package ch.nth.android.contentabo;

import android.app.Application;
import android.content.pm.PackageManager;
import android.util.Log;
import ch.nth.android.contentabo.common.utils.AnalyticsUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.config.PlistConfig;
import ch.nth.android.scmsdk.SCM;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gcm.GCMConstants;
import com.nth.analytics.android.LocalyticsOptions;
import com.nth.analytics.android.LocalyticsSession;
import java.lang.Thread;
import java.util.Locale;

public class App extends Application {
    private static App instance;
    private LocalyticsSession mLocalyticsSession;
    private SCM mSCM;
    private RequestQueue queue;
    private String version = null;
    private int versionCode = 1;

    public App() {
        instance = this;
        Thread.setDefaultUncaughtExceptionHandler(getUncaughtExceptionHandler(Thread.getDefaultUncaughtExceptionHandler()));
    }

    public void onCreate() {
        super.onCreate();
        this.queue = Volley.newRequestQueue(this);
        this.mSCM = new SCM(this);
    }

    public Thread.UncaughtExceptionHandler getUncaughtExceptionHandler(final Thread.UncaughtExceptionHandler currentHandler) {
        return new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable ex) {
                LocalyticsSession session = App.this.getAnalyticsSession();
                AnalyticsUtils.tagEvent(App.this.getBaseContext(), session, AnalyticsUtils.GENERAL_ERROR_EVENT, GCMConstants.EXTRA_ERROR, Log.getStackTraceString(ex));
                session.close();
                session.upload();
                Utils.doLog("Thread: " + thread.getName() + " has an uncaught exception.", ex);
                currentHandler.uncaughtException(thread, ex);
            }
        };
    }

    public static App getInstance() {
        return instance;
    }

    public void addRequest(Request<?> request) {
        this.queue.add(request);
    }

    public RequestQueue getQueue() {
        return this.queue;
    }

    public SCM getSCM() {
        return this.mSCM;
    }

    public String getVersion() {
        if (this.version == null) {
            try {
                this.version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                this.version = null;
                Utils.doLog("Can not retrive app version !!!");
            }
        }
        return this.version;
    }

    public int getVersionCode() {
        if (this.versionCode <= 1) {
            try {
                this.versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                Utils.doLog("Can not retrieve app version code!");
            }
        }
        return this.versionCode;
    }

    public Locale getCurrentLocale() {
        return getResources().getConfiguration().locale;
    }

    public LocalyticsSession getAnalyticsSession() {
        String url;
        String apiKey;
        if (this.mLocalyticsSession == null) {
            try {
                PlistConfig config = AppConfig.getInstance().getConfig();
                if (config != null) {
                    url = config.getAddons().getAnalytics().getUrl();
                    apiKey = config.getApiKey();
                } else {
                    url = "";
                    apiKey = "";
                }
                this.mLocalyticsSession = new LocalyticsSession(this, LocalyticsOptions.newInstance(url, apiKey));
            } catch (Exception e) {
                Utils.doLogException(e);
            }
        }
        return this.mLocalyticsSession;
    }
}
