package ch.nth.android.contentabo.fragments.common;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import ch.nth.android.contentabo.R;
import java.io.Serializable;
import java.util.HashMap;

public class DoubleMOFlowDialogFragment extends DialogFragment {
    private static final String ARG_ALERT_DIALOG_LAYOUT = "edittext_alert_dialog_layout";
    private static final String ARG_ALERT_DIALOG_TAG = "edittext_alert_dialog_tag";
    private static final String ARG_ALERT_DIALOG_TEXT_TO_VALIDATE = "edittext_alert_dialog_text_to_validate";
    private static final String ARG_ALERT_DIALOG_TRANSLATION_MAPPINGS = "alert_dialog_translation_keys";
    /* access modifiers changed from: private */
    public int mAlertDialogTag;
    /* access modifiers changed from: private */
    public boolean mCancelDismissListener;
    private int mDialogLayout;
    /* access modifiers changed from: private */
    public DoubleMoFlowDialogListener mFlowDialogListener;
    /* access modifiers changed from: private */
    public String mTextToValidate;
    /* access modifiers changed from: private */
    public HashMap<Integer, String> mTranslationMappings;

    public interface DoubleMoFlowDialogListener {
        void onDoubleMoFlowDialogDismissed(int i);

        void onDoubleMoFlowDialogPlayButtonClick(int i);
    }

    public static DoubleMOFlowDialogFragment newInstance(DoubleMoFlowDialogListener listener, int tag, int layoutId, String textToValidate, HashMap<Integer, String> translationMappings) {
        DoubleMOFlowDialogFragment dialogFragment = new DoubleMOFlowDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ALERT_DIALOG_TAG, tag);
        args.putInt(ARG_ALERT_DIALOG_LAYOUT, layoutId);
        args.putString(ARG_ALERT_DIALOG_TEXT_TO_VALIDATE, textToValidate);
        args.putSerializable(ARG_ALERT_DIALOG_TRANSLATION_MAPPINGS, translationMappings);
        dialogFragment.setArguments(args);
        dialogFragment.setDoubleMoFlowEditTextListener(listener);
        return dialogFragment;
    }

    private void setDoubleMoFlowEditTextListener(DoubleMoFlowDialogListener listener) {
        this.mFlowDialogListener = listener;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            handleBundleArgs(args);
        }
    }

    @SuppressLint({"UseSparseArrays"})
    private void handleBundleArgs(Bundle args) {
        if (args.containsKey(ARG_ALERT_DIALOG_TAG)) {
            this.mAlertDialogTag = args.getInt(ARG_ALERT_DIALOG_TAG);
        }
        if (args.containsKey(ARG_ALERT_DIALOG_LAYOUT)) {
            this.mDialogLayout = args.getInt(ARG_ALERT_DIALOG_LAYOUT);
        }
        if (args.containsKey(ARG_ALERT_DIALOG_TEXT_TO_VALIDATE)) {
            this.mTextToValidate = args.getString(ARG_ALERT_DIALOG_TEXT_TO_VALIDATE);
        }
        Serializable translationMappingsSerializable = getArguments().getSerializable(ARG_ALERT_DIALOG_TRANSLATION_MAPPINGS);
        if (translationMappingsSerializable instanceof HashMap) {
            try {
                this.mTranslationMappings = (HashMap) translationMappingsSerializable;
            } catch (Exception e) {
            }
        }
        if (this.mTranslationMappings == null) {
            this.mTranslationMappings = new HashMap<>();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mDialogLayout, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().setCanceledOnTouchOutside(false);
        final EditText inputText = (EditText) view.findViewById(R.id.edittext_dialog_custom);
        Button playButton = (Button) view.findViewById(R.id.btn_dialog_positive);
        if (playButton != null) {
            playButton.setText(this.mTranslationMappings.get(Integer.valueOf(R.id.btn_dialog_positive)));
            playButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String editTextContent = inputText != null ? inputText.getText().toString() : null;
                    if (TextUtils.isEmpty(editTextContent) || TextUtils.isEmpty(DoubleMOFlowDialogFragment.this.mTextToValidate)) {
                        inputText.setError((String) DoubleMOFlowDialogFragment.this.mTranslationMappings.get(Integer.valueOf(R.id.edittext_dialog_custom)));
                    } else if (editTextContent.equalsIgnoreCase(DoubleMOFlowDialogFragment.this.mTextToValidate)) {
                        DoubleMOFlowDialogFragment.this.mFlowDialogListener.onDoubleMoFlowDialogPlayButtonClick(DoubleMOFlowDialogFragment.this.mAlertDialogTag);
                        DoubleMOFlowDialogFragment.this.mCancelDismissListener = true;
                        DoubleMOFlowDialogFragment.this.dismiss();
                    } else {
                        inputText.setError((String) DoubleMOFlowDialogFragment.this.mTranslationMappings.get(Integer.valueOf(R.id.edittext_dialog_custom)));
                    }
                }
            });
        }
        TextView titleTextView = (TextView) view.findViewById(R.id.text_dialog_title);
        if (titleTextView != null) {
            String titleText = this.mTranslationMappings.get(Integer.valueOf(R.id.text_dialog_title));
            if (!TextUtils.isEmpty(titleText)) {
                titleTextView.setText(titleText);
            }
        }
        TextView customHeaderTextView = (TextView) view.findViewById(R.id.text_header_dialog_custom);
        if (customHeaderTextView != null) {
            String headerText = this.mTranslationMappings.get(Integer.valueOf(R.id.text_header_dialog_custom));
            if (!TextUtils.isEmpty(headerText) || !TextUtils.isEmpty(customHeaderTextView.getText())) {
                customHeaderTextView.setText(headerText);
            } else {
                customHeaderTextView.setVisibility(8);
            }
        }
        TextView customFooterTextView = (TextView) view.findViewById(R.id.text_footer_dialog_custom);
        if (customFooterTextView != null) {
            String footerText = this.mTranslationMappings.get(Integer.valueOf(R.id.text_footer_dialog_custom));
            if (!TextUtils.isEmpty(footerText) || !TextUtils.isEmpty(customFooterTextView.getText())) {
                customFooterTextView.setText(footerText);
            } else {
                customFooterTextView.setVisibility(8);
            }
        }
        WebView webView = (WebView) view.findViewById(R.id.webview);
        if (webView != null) {
            String contentUrl = this.mTranslationMappings.get(Integer.valueOf(R.id.webview));
            if (!TextUtils.isEmpty(contentUrl)) {
                webView.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        return true;
                    }
                });
                webView.loadUrl(contentUrl);
                webView.setVisibility(0);
            }
        }
        return view;
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (!this.mCancelDismissListener && this.mFlowDialogListener != null) {
            this.mFlowDialogListener.onDoubleMoFlowDialogDismissed(this.mAlertDialogTag);
        }
    }
}
