package ch.nth.android.contentabo.models.payment;

import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.paymentsdk.v2.model.InfoResponse;
import ch.nth.android.paymentsdk.v2.model.helper.InitPaymentParams;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PaymentOption implements Serializable {
    public static final String X_PAYMENT_FLOW_DIRECT_OPERATOR_BILLING = "Direct Operator Billing";
    public static final String X_PAYMENT_FLOW_TYPE = "X-Payment-Flow-Type";
    public static final String X_PAYMENT_FLOW_TYPE_DOUBLE_MO = "Double MO Optin";
    public static final String X_PAYMENT_FLOW_TYPE_SINGLE_MO = "Single MO Optin";
    public static final String X_PAYMENT_SESSION_ID = "X-Payment-Session-Id";
    public static final String X_PAYMENT_SESSION_STATUS = "X-Payment-Session-Status";
    public static final String X_SMS_CONFIRMATION_URI = "X-Sms-Confirmation-Uri";
    public static final String X_SMS_SERVICE_TYPE = "X-Sms-Service-Type";
    public static final String X_SMS_SERVICE_URI = "X-Sms-Service-Uri";
    private static final long serialVersionUID = -4010241142429742728L;
    private PaymentUtils.PaymentFlowStage angebotMessageStatus;
    private String cancelUri;
    private String confirmationUri;
    private PaymentUtils.PaymentFlowStage firstMoStatus;
    private String initCallbackUrl;
    private String initRedirectUrl;
    private String initRequestId;
    private transient boolean initializedThroughParsing;
    private PaymentFlowType paymentFlowType;
    private PaymentUtils.PaymentStatus paymentSessionStatus;
    private PaymentUtils.PaymentFlowStage secondMoStatus;
    private ServiceType serviceType;
    private String serviceUri;
    private String sessionId;
    private String stopKeywordRequired;
    private Pattern uriPattern = Pattern.compile("sms:\\/\\/([0-9]+)\\?body=([\\d\\w]*)(.*)?");
    private PaymentUtils.PaymentFlowStage welcomeMessageStatus;

    public enum PaymentFlowType {
        DOUBLE_MO_OPTIN,
        SINGLE_MO_OPTIN,
        WEB_FLOW,
        UNKNOWN
    }

    public enum ServiceType {
        SILENT,
        TRANSPARENT
    }

    public PaymentFlowType getPaymentFlowType() {
        return this.paymentFlowType;
    }

    public void setPaymentFlowType(PaymentFlowType paymentFlowType2) {
        Log.i("ContentAbo", "Flow: " + paymentFlowType2);
        this.paymentFlowType = paymentFlowType2;
    }

    public String getServiceUri() {
        if (this.serviceUri == null) {
            return "";
        }
        return this.serviceUri;
    }

    public void setServiceUri(String serviceUri2) {
        this.serviceUri = serviceUri2;
    }

    public String getConfirmationUri() {
        if (this.confirmationUri == null) {
            return "";
        }
        return this.confirmationUri;
    }

    public void setConfirmationUri(String confirmationUri2) {
        this.confirmationUri = confirmationUri2;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId2) {
        this.sessionId = sessionId2;
    }

    public ServiceType getServiceType() {
        return this.serviceType;
    }

    public void setServiceType(ServiceType serviceType2) {
        this.serviceType = serviceType2;
    }

    public String getNumberFromServiceUrl() {
        String serviceUri2 = getServiceUri();
        if (TextUtils.isEmpty(serviceUri2)) {
            return "";
        }
        Matcher m = this.uriPattern.matcher(serviceUri2);
        if (m.matches()) {
            return m.group(1);
        }
        return "";
    }

    public String getNumberFromConfirmationUrl() {
        Matcher m = this.uriPattern.matcher(getConfirmationUri());
        if (m.matches()) {
            return m.group(1);
        }
        return "";
    }

    public String getNumberFromCancelUrl() {
        Matcher m = this.uriPattern.matcher(getCancelUri());
        if (m.matches()) {
            return m.group(1);
        }
        return "";
    }

    public String getBodyFromServiceUrl() {
        String serviceUri2 = getServiceUri();
        if (TextUtils.isEmpty(serviceUri2)) {
            return "";
        }
        Matcher m = this.uriPattern.matcher(serviceUri2);
        if (!m.matches()) {
            return "";
        }
        try {
            return String.valueOf(String.valueOf("") + (TextUtils.isEmpty(m.group(2)) ? "" : m.group(2))) + (TextUtils.isEmpty(m.group(3)) ? "" : m.group(3));
        } catch (Exception e) {
            return "";
        }
    }

    public String getBodyFromConfirmationUrl() {
        Matcher m = this.uriPattern.matcher(getConfirmationUri());
        if (!m.matches()) {
            return "";
        }
        try {
            return String.valueOf(String.valueOf("") + (TextUtils.isEmpty(m.group(2)) ? "" : m.group(2))) + (TextUtils.isEmpty(m.group(3)) ? "" : m.group(3));
        } catch (Exception e) {
            return "";
        }
    }

    public String getBodyFromCancelUrl() {
        Matcher m = this.uriPattern.matcher(getCancelUri());
        if (!m.matches()) {
            return "";
        }
        try {
            return String.valueOf(String.valueOf("") + (TextUtils.isEmpty(m.group(2)) ? "" : m.group(2))) + (TextUtils.isEmpty(m.group(3)) ? "" : m.group(3));
        } catch (Exception e) {
            return "";
        }
    }

    public String getKeywordFromServiceUrl() {
        Matcher m = this.uriPattern.matcher(getServiceUri());
        String keyword = "";
        if (m.matches()) {
            keyword = String.valueOf(keyword) + (TextUtils.isEmpty(m.group(2)) ? "" : m.group(2));
        }
        return keyword.trim();
    }

    public String getInitRedirectUrl() {
        return this.initRedirectUrl;
    }

    public String getInitCallbackUrl() {
        return this.initCallbackUrl;
    }

    public String getInitRequestId() {
        return this.initRequestId;
    }

    public boolean isCancelPaymentInfoInitialized() {
        return this.initializedThroughParsing || (!TextUtils.isEmpty(getBodyFromCancelUrl()) && !TextUtils.isEmpty(getNumberFromCancelUrl()) && !TextUtils.isEmpty(this.stopKeywordRequired));
    }

    public boolean isCancelPaymentWithSms() {
        if (TextUtils.isEmpty(this.stopKeywordRequired)) {
            return true;
        }
        return "YES".equalsIgnoreCase(this.stopKeywordRequired);
    }

    public void processInitPaymentParams(InitPaymentParams initPaymentParams) {
        this.initRedirectUrl = initPaymentParams.getRedirectUrl();
        this.initCallbackUrl = initPaymentParams.getCallbackUrl();
        this.initRequestId = initPaymentParams.getRequestId();
    }

    public void processInfoResponse(InfoResponse infoResponse) {
        if (infoResponse == null || TextUtils.isEmpty(infoResponse.getFlowType())) {
            setPaymentFlowType(PaymentFlowType.UNKNOWN);
            return;
        }
        this.initializedThroughParsing = true;
        if (infoResponse.getFlowType().equalsIgnoreCase(X_PAYMENT_FLOW_TYPE_DOUBLE_MO)) {
            setPaymentFlowType(PaymentFlowType.DOUBLE_MO_OPTIN);
            this.firstMoStatus = PaymentUtils.PaymentFlowStage.NONE;
            this.secondMoStatus = PaymentUtils.PaymentFlowStage.NONE;
            this.angebotMessageStatus = PaymentUtils.PaymentFlowStage.NONE;
        } else if (infoResponse.getFlowType().equalsIgnoreCase(X_PAYMENT_FLOW_TYPE_SINGLE_MO)) {
            setPaymentFlowType(PaymentFlowType.SINGLE_MO_OPTIN);
        } else if (infoResponse.getFlowType().equalsIgnoreCase(X_PAYMENT_FLOW_DIRECT_OPERATOR_BILLING)) {
            setPaymentFlowType(PaymentFlowType.WEB_FLOW);
        } else {
            setPaymentFlowType(PaymentFlowType.WEB_FLOW);
        }
        setSessionId(infoResponse.getSessionId());
        setServiceUri(infoResponse.getSmsServiceUri());
        setConfirmationUri(infoResponse.getSmsConfirmationUri());
        setCancelUri(infoResponse.getSmsCancelUri());
        setStopKeywordRequired(infoResponse.getSmsStopKeywordRequired());
        this.paymentSessionStatus = PaymentUtils.PaymentStatus.valueOfOrDefault(infoResponse.getSessionStatus());
        if (infoResponse.getSmsServiceType().equalsIgnoreCase(X_SMS_SERVICE_TYPE)) {
            setServiceType(ServiceType.SILENT);
        } else {
            setServiceType(ServiceType.TRANSPARENT);
        }
    }

    public void processInfoResponseSilent(InfoResponse infoResponse) {
        this.initializedThroughParsing = true;
        if (TextUtils.isEmpty(getSessionId())) {
            setSessionId(infoResponse.getSessionId());
        }
        if (TextUtils.isEmpty(getServiceUri())) {
            setServiceUri(infoResponse.getSmsServiceUri());
        }
        if (TextUtils.isEmpty(getConfirmationUri())) {
            setConfirmationUri(infoResponse.getSmsConfirmationUri());
        }
        if (TextUtils.isEmpty(getCancelUri())) {
            setCancelUri(infoResponse.getSmsCancelUri());
        }
        if (TextUtils.isEmpty(getStopKeywordRequired())) {
            setStopKeywordRequired(infoResponse.getSmsStopKeywordRequired());
        }
        this.paymentSessionStatus = PaymentUtils.PaymentStatus.valueOfOrDefault(infoResponse.getSessionStatus());
        if (infoResponse.getSmsServiceType().equalsIgnoreCase(X_SMS_SERVICE_TYPE)) {
            setServiceType(ServiceType.SILENT);
        } else {
            setServiceType(ServiceType.TRANSPARENT);
        }
    }

    public PaymentUtils.PaymentStatus getPaymentSessionStatus() {
        return this.paymentSessionStatus;
    }

    public void setPaymentSessionStatus(PaymentUtils.PaymentStatus paymentSessionStatus2) {
        this.paymentSessionStatus = paymentSessionStatus2;
    }

    public PaymentUtils.PaymentFlowStage getFirstMoStatus() {
        return this.firstMoStatus;
    }

    public void setFirstMoStatus(PaymentUtils.PaymentFlowStage firstMoStatus2) {
        this.firstMoStatus = firstMoStatus2;
    }

    public PaymentUtils.PaymentFlowStage getSecondMoStatus() {
        return this.secondMoStatus;
    }

    public void setSecondMoStatus(PaymentUtils.PaymentFlowStage secondMoStatus2) {
        this.secondMoStatus = secondMoStatus2;
    }

    public PaymentUtils.PaymentFlowStage getAngebotMessageStatus() {
        return this.angebotMessageStatus;
    }

    public void setAngebotMessageStatus(PaymentUtils.PaymentFlowStage angebotMessageStatus2) {
        this.angebotMessageStatus = angebotMessageStatus2;
    }

    public PaymentUtils.PaymentFlowStage getWelcomeMessageStatus() {
        return this.welcomeMessageStatus;
    }

    public void setWelcomeMessageStatus(PaymentUtils.PaymentFlowStage welcomeMessageStatus2) {
        this.welcomeMessageStatus = welcomeMessageStatus2;
    }

    public String getCancelUri() {
        if (TextUtils.isEmpty(this.cancelUri)) {
            return "";
        }
        return this.cancelUri;
    }

    public void setCancelUri(String cancelUri2) {
        this.cancelUri = cancelUri2;
    }

    public String getStopKeywordRequired() {
        return this.stopKeywordRequired;
    }

    public void setStopKeywordRequired(String stopKeywordRequired2) {
        this.stopKeywordRequired = stopKeywordRequired2;
    }

    public String toString() {
        return "PaymentOption [paymentFlowType=" + this.paymentFlowType + ", serviceUri=" + this.serviceUri + ", confirmationUri=" + this.confirmationUri + ", cancelUri=" + this.cancelUri + ", stopKeywordRequired=" + this.stopKeywordRequired + ", sessionId=" + this.sessionId + ", serviceType=" + this.serviceType + ", paymentSessionStatus=" + this.paymentSessionStatus + ", firstMoStatus=" + this.firstMoStatus + ", secondMoStatus=" + this.secondMoStatus + ", angebotMessageStatus=" + this.angebotMessageStatus + ", welcomeMessageStatus=" + this.welcomeMessageStatus + "]";
    }
}
