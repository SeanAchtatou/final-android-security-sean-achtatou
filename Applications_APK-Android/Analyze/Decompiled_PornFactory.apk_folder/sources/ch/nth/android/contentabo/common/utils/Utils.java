package ch.nth.android.contentabo.common.utils;

import java.io.Closeable;
import java.util.Random;

public class Utils {
    public static final String TAG = "ContentABO";
    private static final Random mRandom = new Random();

    public static void doLog(Object o) {
    }

    public static void doLog(String tag, Object o) {
    }

    public static void doLogException(Exception e) {
    }

    public static long parseLong(String s, long def) {
        try {
            return Long.parseLong(s);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    public static int getPseudorandom(int n) {
        return mRandom.nextInt(n);
    }

    public static void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
            }
        }
    }
}
