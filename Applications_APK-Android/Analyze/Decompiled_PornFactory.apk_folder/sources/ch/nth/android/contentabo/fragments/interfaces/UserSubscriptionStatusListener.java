package ch.nth.android.contentabo.fragments.interfaces;

public interface UserSubscriptionStatusListener {
    void onUserNotSubscribed();

    void onUserSubscribed();
}
