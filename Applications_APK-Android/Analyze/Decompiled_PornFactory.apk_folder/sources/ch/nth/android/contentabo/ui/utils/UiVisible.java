package ch.nth.android.contentabo.ui.utils;

public enum UiVisible {
    CONTENT,
    PROGRESS,
    TEXT_MESSAGE
}
