package ch.nth.android.contentabo.translations;

import android.content.Context;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.polyglot.translator.MissingEntryPolicy;
import ch.nth.android.polyglot.translator.MissingValuePolicy;
import ch.nth.android.polyglot.translator.Polyglot;
import ch.nth.android.polyglot.translator.TranslationModule;
import ch.nth.android.polyglot.translator.TranslationModuleCollection;
import ch.nth.android.polyglot.translator.TranslationStorage;
import java.util.LinkedHashSet;
import java.util.Set;

public class Disclaimers {
    private static volatile Disclaimers instance = null;
    private Polyglot mPolyglot;

    private Disclaimers() {
    }

    public static Polyglot getPolyglot() {
        if (instance == null) {
            synchronized (Disclaimers.class) {
                if (instance == null) {
                    instance = new Disclaimers();
                    regenerate();
                    initialize();
                }
            }
        }
        return instance.mPolyglot;
    }

    public static Polyglot initPolyglot(TranslationModule translations, Context context) {
        if (instance == null) {
            synchronized (Disclaimers.class) {
                if (instance == null) {
                    instance = new Disclaimers();
                }
            }
        }
        instance.mPolyglot = new DisclaimersPolyglot(translations, context);
        initialize();
        return instance.mPolyglot;
    }

    private static void regenerate() {
        TranslationModuleCollection translationsBatch = TranslationStorage.deserializeCollection(App.getInstance(), Translations.CACHE_FILENAME);
        instance.mPolyglot = new DisclaimersPolyglot(translationsBatch != null ? translationsBatch.getTranslationModule(PaymentUtils.getMCCMNCTuple(App.getInstance()), true) : null, App.getInstance());
    }

    private static void initialize() {
        if (instance.mPolyglot != null) {
            Set<MissingValuePolicy> missingValuePolicies = new LinkedHashSet<>();
            missingValuePolicies.add(MissingValuePolicy.ANY_LANGUAGE);
            missingValuePolicies.add(MissingValuePolicy.RESOURCE_FALLBACK);
            instance.mPolyglot.setMissingValuePolicies(missingValuePolicies);
            instance.mPolyglot.setMissingEntryPolicy(MissingEntryPolicy.RESOURCE_FALLBACK);
        }
    }
}
