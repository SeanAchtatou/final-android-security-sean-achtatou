package ch.nth.android.contentabo.config.update;

import ch.nth.simpleplist.annotation.Dictionary;
import ch.nth.simpleplist.annotation.Property;

public class EntStoreConfig {
    @Dictionary(name = "properties")
    private EntStoreProperties properties;
    @Property(name = "update_max_timeout_seconds", required = false, stringCompatibility = true)
    private int updateMaxTimeoutSeconds = 3600;
    @Property(name = "update_page_url")
    private String updatePageUrl;
    @Property(name = "update_retry_timeout_seconds", required = false, stringCompatibility = true)
    private int updateRetryTimeoutSeconds = 3600;
    @Property(name = "update_url")
    private String updateUrl;

    public String getUpdatePageUrl() {
        return this.updatePageUrl;
    }

    public String getUpdateUrl() {
        return this.updateUrl;
    }

    public int getUpdateRetryTimeoutSeconds() {
        return this.updateRetryTimeoutSeconds;
    }

    public int getUpdateMaxTimeoutSeconds() {
        return this.updateMaxTimeoutSeconds;
    }

    public EntStoreProperties getProperties() {
        if (this.properties == null) {
            return new EntStoreProperties();
        }
        return this.properties;
    }
}
