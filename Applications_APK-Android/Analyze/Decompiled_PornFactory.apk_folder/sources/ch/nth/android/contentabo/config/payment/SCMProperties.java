package ch.nth.android.contentabo.config.payment;

import ch.nth.simpleplist.annotation.Property;

public class SCMProperties {
    @Property(name = "bcdb_id")
    private String bcdbId;

    public String getBcdbId() {
        return this.bcdbId;
    }
}
