package ch.nth.android.contentabo.models.task;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.models.content.ContentList;
import ch.nth.android.contentabo.models.utils.VideoSorting;
import ch.nth.android.contentabo.networking.content.ContentUtils;
import com.android.volley.Cache;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import java.util.EnumSet;

public class ContentListTask extends AsyncTask<Void, Void, ContentList> {
    private static final String ASSETS_DIR = "default_content/";
    private Context mContext;
    private ErrorListener mErrorListener;
    private final Gson mGson = ContentUtils.createGsonInstance(EnumSet.of(ContentUtils.GsonTypeAdapter.DATE));
    private Listener mListener;
    private boolean mTryAssets;
    private String mUrl;
    private VideoSorting mVideoSorting;

    public interface ErrorListener {
        void onError();
    }

    public interface Listener {
        void onSuccess(ContentList contentList);
    }

    public ContentListTask(Context context, String url, VideoSorting videoSorting, boolean tryAssets, Listener listener, ErrorListener errorListener) {
        this.mContext = context;
        this.mUrl = url;
        this.mVideoSorting = videoSorting;
        this.mTryAssets = tryAssets;
        this.mListener = listener;
        this.mErrorListener = errorListener;
    }

    @TargetApi(11)
    public void executeAsync() {
        if (Build.VERSION.SDK_INT >= 11) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        } else {
            execute(new Void[0]);
        }
    }

    /* access modifiers changed from: protected */
    public ContentList doInBackground(Void... params) {
        ContentList result = tryFromCache();
        if (result != null || !this.mTryAssets) {
            return result;
        }
        return tryFromAssets();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(ContentList result) {
        if (result != null) {
            if (this.mListener != null) {
                this.mListener.onSuccess(result);
            }
        } else if (this.mErrorListener != null) {
            this.mErrorListener.onError();
        }
    }

    private ContentList tryFromCache() {
        if (this.mUrl == null) {
            return null;
        }
        try {
            Cache.Entry entry = App.getInstance().getQueue().getCache().get(this.mUrl);
            if (entry != null) {
                return (ContentList) this.mGson.fromJson(new String(entry.data, HttpHeaderParser.parseCharset(entry.responseHeaders)), ContentList.class);
            }
            return null;
        } catch (Exception e) {
            Utils.doLogException(e);
            return null;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: ch.nth.android.contentabo.models.content.ContentList} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
     arg types: [java.io.BufferedReader, java.lang.Class]
     candidates:
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.Class):T
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(com.google.gson.stream.JsonReader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private ch.nth.android.contentabo.models.content.ContentList tryFromAssets() {
        /*
            r8 = this;
            ch.nth.android.contentabo.models.utils.VideoSorting r5 = r8.mVideoSorting
            if (r5 != 0) goto L_0x0006
            r4 = 0
        L_0x0005:
            return r4
        L_0x0006:
            r4 = 0
            r2 = 0
            android.content.Context r5 = r8.mContext     // Catch:{ Exception -> 0x0043 }
            android.content.res.AssetManager r5 = r5.getAssets()     // Catch:{ Exception -> 0x0043 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0043 }
            java.lang.String r7 = "default_content/"
            r6.<init>(r7)     // Catch:{ Exception -> 0x0043 }
            ch.nth.android.contentabo.models.utils.VideoSorting r7 = r8.mVideoSorting     // Catch:{ Exception -> 0x0043 }
            java.lang.String r7 = r7.getEmbeddedAssetsFilename()     // Catch:{ Exception -> 0x0043 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0043 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0043 }
            java.io.InputStream r2 = r5.open(r6)     // Catch:{ Exception -> 0x0043 }
            if (r2 == 0) goto L_0x003f
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0043 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0043 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x0043 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x0043 }
            com.google.gson.Gson r5 = r8.mGson     // Catch:{ Exception -> 0x0043 }
            java.lang.Class<ch.nth.android.contentabo.models.content.ContentList> r6 = ch.nth.android.contentabo.models.content.ContentList.class
            java.lang.Object r5 = r5.fromJson(r3, r6)     // Catch:{ Exception -> 0x0043 }
            r0 = r5
            ch.nth.android.contentabo.models.content.ContentList r0 = (ch.nth.android.contentabo.models.content.ContentList) r0     // Catch:{ Exception -> 0x0043 }
            r4 = r0
        L_0x003f:
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r2)
            goto L_0x0005
        L_0x0043:
            r1 = move-exception
            ch.nth.android.contentabo.common.utils.Utils.doLogException(r1)     // Catch:{ all -> 0x004b }
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r2)
            goto L_0x0005
        L_0x004b:
            r5 = move-exception
            ch.nth.android.contentabo.common.utils.Utils.closeSilently(r2)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: ch.nth.android.contentabo.models.task.ContentListTask.tryFromAssets():ch.nth.android.contentabo.models.content.ContentList");
    }
}
