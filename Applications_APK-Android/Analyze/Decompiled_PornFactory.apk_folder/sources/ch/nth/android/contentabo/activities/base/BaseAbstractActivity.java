package ch.nth.android.contentabo.activities.base;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import ch.nth.android.contentabo.App;
import ch.nth.android.contentabo.common.utils.MessageUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.PreferenceConnector;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.config.AppConfig;
import ch.nth.android.contentabo.service.AlarmReceiver;
import ch.nth.android.contentabo.service.CheckSpendingLimitService;
import ch.nth.android.contentabo.translations.LanguageChangedEvent;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import ch.nth.android.scmsdk.SCM;
import com.android.volley.Request;
import com.nth.analytics.android.LocalyticsSession;
import de.greenrobot.event.EventBus;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public abstract class BaseAbstractActivity extends BasePaymentActivitySupportV7 implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int HANDLER_SHOW_DIALOG_MESSAGE = 257;
    private static final int HANDLER_START_SPENDING_CHECK = 256;
    private static final int INBOX_LOADER_ID = 152;
    protected static final String LAST_LIMIT_CHECK_TIMESTAMP = "LAST_LIMIT_CHECK_TIMESTAMP";
    private static final int OUTBOX_LOADER_ID = 153;
    protected static final String SPENDING_LIMIT_SMS_INTENT = "SPENDING_LIMIT_SMS_INTENT";
    private static final String TAG = BaseAbstractActivity.class.getSimpleName();
    protected static final int TAG_SPENDING_LIMIT_DIALOG_CALLBACK = 83;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 257) {
                BaseAbstractActivity.this.showSpendingLimitDialog();
            } else if (msg.what == 256) {
                BaseAbstractActivity.this.checkSpendingLimitIfNeeded();
            } else {
                super.handleMessage(msg);
            }
        }
    };
    private String mInboxSpendingLimitAddress;
    private Date mInboxSpendingLimitTime;
    private LocalyticsSession mLocalyticsSession;

    public abstract int getContentView();

    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<Cursor>) loader, (Cursor) obj);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int contentId = getContentView();
        if (contentId != 0) {
            setContentView(contentId);
        }
        if (shouldCheckSpendingLimit()) {
            this.mHandler.removeMessages(256);
            this.mHandler.sendEmptyMessageDelayed(256, 200);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(LanguageChangedEvent event) {
    }

    /* access modifiers changed from: protected */
    public void showSimpleToastMessage(Context context, String message) {
        Toast toast = Toast.makeText(context, message, 1);
        toast.setGravity(17, 0, 0);
        toast.show();
    }

    /* access modifiers changed from: protected */
    public SCM getSCM() {
        return App.getInstance().getSCM();
    }

    /* access modifiers changed from: protected */
    public void addRequest(Request<?> request) {
        App.getInstance().addRequest(request);
    }

    public LocalyticsSession getAnalyticsSession() {
        if (this.mLocalyticsSession == null) {
            this.mLocalyticsSession = App.getInstance().getAnalyticsSession();
        }
        return this.mLocalyticsSession;
    }

    /* access modifiers changed from: protected */
    public boolean shouldCheckSpendingLimit() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void checkSpendingLimitIfNeeded() {
        if (shouldCheckSpendingLimit() && AppConfig.getInstance().getConfig().getLimitCheck().isCheckSpendingLimit() && CheckSpendingLimitService.shouldCheckMessages(this)) {
            Log.d(TAG, "checking from: " + getClass().getSimpleName());
            CheckSpendingLimitService.scheduleNextCheck(this);
            CheckSpendingLimitService.writeCurrentTimeToCheckFlag(this);
            this.mInboxSpendingLimitTime = null;
            this.mInboxSpendingLimitAddress = null;
            getSupportLoaderManager().initLoader(INBOX_LOADER_ID, null, this);
        }
    }

    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        String[] requiredColumns = {DmsConstants.ID, "address", "date", "body"};
        switch (loaderId) {
            case INBOX_LOADER_ID /*152*/:
                String inboxSelection = "address like '%" + PaymentUtils.getPaymentOption(this).getNumberFromServiceUrl().replaceFirst("^00", "").replace("+", "") + "%'";
                Log.d(TAG, "starting INBOX_LOADER_ID ; query selection: " + inboxSelection);
                return new CursorLoader(this, Uri.parse("content://sms/inbox"), requiredColumns, inboxSelection, null, "date desc");
            case OUTBOX_LOADER_ID /*153*/:
                Log.d(TAG, "starting OUTBOX_LOADER_ID");
                Uri outboxUri = Uri.parse("content://sms/sent");
                if (TextUtils.isEmpty(this.mInboxSpendingLimitAddress)) {
                    return null;
                }
                return new CursorLoader(this, outboxUri, requiredColumns, "address like '%" + this.mInboxSpendingLimitAddress + "%'", null, "date desc");
            default:
                return null;
        }
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        String[] body = new String[data.getCount()];
        String[] date = new String[data.getCount()];
        String[] address = new String[data.getCount()];
        if (data.moveToFirst()) {
            try {
                int bodyColumnIndex = data.getColumnIndexOrThrow("body");
                int dateColumnIndex = data.getColumnIndexOrThrow("date");
                int addressColumnIndex = data.getColumnIndexOrThrow("address");
                for (int i = 0; i < data.getCount(); i++) {
                    body[i] = data.getString(bodyColumnIndex);
                    date[i] = data.getString(dateColumnIndex);
                    address[i] = data.getString(addressColumnIndex);
                    data.moveToNext();
                }
            } catch (Exception e) {
                return;
            }
        }
        int count = body.length;
        Log.d(TAG, "spending limit messages length: " + count);
        Date now = Calendar.getInstance().getTime();
        long validitySeconds = (long) (AppConfig.getInstance().getConfig().getLimitCheck().getMaxMessageAgeMinutes() * 60);
        String inboxPatternString = AppConfig.getInstance().getConfig().getLimitCheck().getIncomingMessagePattern();
        boolean inboxPatternUndefined = TextUtils.isEmpty(inboxPatternString);
        Pattern inboxPattern = Pattern.compile(inboxPatternString);
        String outboxPatternString = AppConfig.getInstance().getConfig().getLimitCheck().getOutgoingMessageContent();
        boolean outboxPatternUndefined = TextUtils.isEmpty(outboxPatternString);
        Pattern outboxPattern = Pattern.compile(outboxPatternString);
        boolean foundSpendingSmsInInbox = false;
        boolean foundSpendingResponseSmsInOutbox = false;
        for (int i2 = 0; i2 < count && !foundSpendingSmsInInbox && !foundSpendingResponseSmsInOutbox; i2++) {
            Date messageDate = null;
            try {
                messageDate = new Date(Long.parseLong(date[i2]));
            } catch (NumberFormatException e2) {
            }
            if (messageDate != null && (now.getTime() - messageDate.getTime()) / 1000 <= validitySeconds) {
                Log.d(TAG, "checking: " + messageDate + " - " + body[i2]);
                switch (loader.getId()) {
                    case INBOX_LOADER_ID /*152*/:
                        if (!inboxPatternUndefined && inboxPattern.matcher(body[i2]).matches()) {
                            Log.i(TAG, "found matching spending inbox message on date " + date[i2] + " : " + body[i2]);
                            foundSpendingSmsInInbox = true;
                            this.mInboxSpendingLimitTime = messageDate;
                            this.mInboxSpendingLimitAddress = address[i2];
                            if (TextUtils.isEmpty(this.mInboxSpendingLimitAddress)) {
                                break;
                            } else {
                                PreferenceConnector.writeString(this, PaymentUtils.KEY_PREFERENCES_SHORT_ID, this.mInboxSpendingLimitAddress);
                                break;
                            }
                        }
                    case OUTBOX_LOADER_ID /*153*/:
                        if (!outboxPatternUndefined) {
                            if (this.mInboxSpendingLimitTime != null) {
                                if (messageDate.before(this.mInboxSpendingLimitTime)) {
                                    break;
                                }
                            }
                            if (!outboxPattern.matcher(body[i2]).matches()) {
                                break;
                            } else {
                                Log.i(TAG, "found matching spending outbox message on date " + date[i2] + " : " + body[i2]);
                                foundSpendingResponseSmsInOutbox = true;
                                break;
                            }
                        } else {
                            continue;
                        }
                }
            }
        }
        switch (loader.getId()) {
            case INBOX_LOADER_ID /*152*/:
                if (foundSpendingSmsInInbox) {
                    getSupportLoaderManager().initLoader(OUTBOX_LOADER_ID, null, this);
                    Log.d(TAG, "Found spending limit SMS in inbox! Starting OUTBOX loader");
                    return;
                }
                Log.d(TAG, "No spending limit SMS in inbox");
                return;
            case OUTBOX_LOADER_ID /*153*/:
                if (foundSpendingResponseSmsInOutbox) {
                    Log.d(TAG, "Found spending limit response SMS in inbox! No further action is necessary.");
                    return;
                }
                Log.d(TAG, "No spending limit response SMS in inbox! Notifying the user...");
                this.mHandler.sendEmptyMessageDelayed(257, 10);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void showSpendingLimitDialog() {
    }

    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public boolean trySendSpendingLimitSms() {
        String smsNumber = PaymentUtils.getPaymentOption(this).getNumberFromServiceUrl();
        if (TextUtils.isEmpty(smsNumber)) {
            smsNumber = this.mInboxSpendingLimitAddress;
        }
        if (TextUtils.isEmpty(smsNumber)) {
            smsNumber = PreferenceConnector.readString(this, PaymentUtils.KEY_PREFERENCES_SHORT_ID, null);
        }
        boolean success = trySendSms(smsNumber, AppConfig.getInstance().getConfig().getLimitCheck().getOutgoingMessageContent(), SPENDING_LIMIT_SMS_INTENT);
        if (success) {
            AlarmReceiver.dismissSpendingLimitNotification(this);
        }
        return success;
    }

    public boolean trySendSms(String smsNumber, String messageBody, String intentAction) {
        Log.d(TAG, "trySendSms() >>> smsNumber: " + smsNumber + " , messageBody: " + messageBody);
        if (TextUtils.isEmpty(smsNumber) || TextUtils.isEmpty(messageBody)) {
            return false;
        }
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();
        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<>();
        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(intentAction), 0);
        try {
            if (TextUtils.isEmpty(messageBody)) {
                return false;
            }
            SmsManager smsManager = SmsManager.getDefault();
            ArrayList<String> messageParts = smsManager.divideMessage(messageBody);
            for (int i = 0; i < messageParts.size(); i++) {
                sentPendingIntents.add(i, sentPI);
            }
            smsManager.sendMultipartTextMessage(smsNumber, null, messageParts, sentPendingIntents, deliveredPendingIntents);
            MessageUtils.saveMo2Sent(smsNumber, messageBody, getBaseContext());
            return true;
        } catch (Exception e) {
            Utils.doLogException(e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public String getInboxSpendingLimitAddress() {
        return this.mInboxSpendingLimitAddress;
    }
}
