package ch.nth.android.contentabo.service;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import ch.nth.android.contentabo.R;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.contentabo.translations.Translations;

public abstract class AlarmReceiver extends BroadcastReceiver {
    public static final String ACTION_CHECK_SPENDING_LIMIT = "ch.nth.android.action.CHECK_SPENDING_LIMIT";
    public static final String ACTION_SHOW_SPENDING_LIMIT_NOTIFICATION = "ch.nth.android.action.ACTION_SHOW_SPENDING_LIMIT_NOTIFICATION";
    public static final String ACTION_START_APP_UPDATE = "ch.nth.android.action.START_APP_UPDATE";
    protected static final int NOTIFICATION_ID = 136;

    /* access modifiers changed from: protected */
    public abstract Class<? extends Activity> getSpendingLimitActivityClass();

    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            CheckSpendingLimitService.scheduleNextCheck(context);
        } else if (ACTION_START_APP_UPDATE.equals(intent.getAction())) {
            WakefulIntentService.sendWakefulWork(context, DownloadAppService.getLaunchIntent(context, true));
        } else if (ACTION_CHECK_SPENDING_LIMIT.equals(intent.getAction())) {
            WakefulIntentService.sendWakefulWork(context, new Intent(context, CheckSpendingLimitService.class));
        } else if (ACTION_SHOW_SPENDING_LIMIT_NOTIFICATION.equals(intent.getAction())) {
            showSpendingLimitNotification(context);
        }
    }

    /* access modifiers changed from: protected */
    public void showSpendingLimitNotification(Context context) {
        Class<? extends Activity> clazz = getSpendingLimitActivityClass();
        if (context != null && clazz != null) {
            ((NotificationManager) context.getSystemService("notification")).notify(NOTIFICATION_ID, new NotificationCompat.Builder(context).setAutoCancel(true).setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, clazz), 0)).setContentTitle(context.getString(R.string.app_name)).setContentText(Translations.getPolyglot().getString(T.string.spending_limit_notification_message)).setTicker(Translations.getPolyglot().getString(T.string.spending_limit_notification_ticker)).setSmallIcon(R.drawable.ic_launcher).build());
        }
    }

    public static void dismissSpendingLimitNotification(Context context) {
        if (context != null) {
            ((NotificationManager) context.getSystemService("notification")).cancel(NOTIFICATION_ID);
        }
    }
}
