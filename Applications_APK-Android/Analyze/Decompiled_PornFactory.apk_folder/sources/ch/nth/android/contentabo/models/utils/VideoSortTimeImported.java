package ch.nth.android.contentabo.models.utils;

import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.models.content.Content;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

public class VideoSortTimeImported implements Comparator<Content> {
    private static final SimpleDateFormat DISPLAY_FORMAT_DATE_TIME = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
    private static final String TAG = VideoSortTimeImported.class.getSimpleName();

    public int compare(Content lhs, Content rhs) {
        Date lhsDate = lhs.getTimeImported();
        Date rhsDate = rhs.getTimeImported();
        if (lhsDate.before(rhsDate)) {
            return -1;
        }
        return lhsDate.after(rhsDate) ? 1 : 0;
    }

    public static Date parseDate(String date) {
        try {
            return DISPLAY_FORMAT_DATE_TIME.parse(date);
        } catch (Throwable th) {
            Utils.doLog(TAG, "Error parsing date '" + date + "'");
            return null;
        }
    }
}
