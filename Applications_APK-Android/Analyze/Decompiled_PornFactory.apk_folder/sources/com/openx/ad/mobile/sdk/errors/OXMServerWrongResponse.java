package com.openx.ad.mobile.sdk.errors;

public class OXMServerWrongResponse extends OXMError {
    private static final long serialVersionUID = 1872034209394425891L;

    public OXMServerWrongResponse() {
        super.setMessage("Server response parsing error");
    }
}
