package com.openx.ad.mobile.sdk;

public class OXMEvent {
    private Object mArgs;
    private OXMEventType mEventType;
    private Object mSender;

    public enum OXMEventType {
        MODAL_WINDOW_CLOSED,
        CLOSE_MODAL_WINDOW,
        ACTION_HAS_BEGAN,
        ACTION_HAS_DONE,
        LOG
    }

    public OXMEvent(OXMEventType event, Object sender, Object args) {
        this.mEventType = event;
        this.mSender = sender;
        this.mArgs = args;
    }

    public OXMEventType getEventType() {
        return this.mEventType;
    }

    public Object getSender() {
        return this.mSender;
    }

    public Object getArgs() {
        return this.mArgs;
    }
}
