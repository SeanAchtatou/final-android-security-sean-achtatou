package com.openx.ad.mobile.sdk.models;

import com.openx.ad.mobile.sdk.interfaces.OXMAdCreative;
import org.json.JSONException;
import org.json.JSONObject;

class OXMAdCreativeImpl implements OXMAdCreative {
    private static final long serialVersionUID = -9159993881218344311L;
    private String mAlt;
    private boolean mHasParseError;
    private int mHeight;
    private String mMedia;
    private String mMime;
    private String mTarget;
    private OXMAdTrackingImpl mTracking;
    private int mWidth;

    OXMAdCreativeImpl() {
    }

    /* access modifiers changed from: package-private */
    public void setParseError(boolean hasError) {
        this.mHasParseError = hasError;
    }

    /* access modifiers changed from: package-private */
    public boolean hasParseError() {
        return this.mHasParseError;
    }

    private void setMedia(String media) {
        this.mMedia = media;
    }

    public String getMedia() {
        return this.mMedia;
    }

    private void setTracking(OXMAdTrackingImpl tracking) {
        this.mTracking = tracking;
    }

    public OXMAdTrackingImpl getTracking() {
        return this.mTracking;
    }

    private void setMime(String mime) {
        this.mMime = mime;
    }

    public String getMime() {
        return this.mMime;
    }

    private void setWidth(int width) {
        this.mWidth = width;
    }

    public int getWidth() {
        return this.mWidth;
    }

    private void setHeight(int height) {
        this.mHeight = height;
    }

    public int getHeight() {
        return this.mHeight;
    }

    private void setAlt(String alt) {
        this.mAlt = alt;
    }

    public String getAlt() {
        return this.mAlt;
    }

    private void setTarget(String target) {
        this.mTarget = target;
    }

    public String getTarget() {
        return this.mTarget;
    }

    /* access modifiers changed from: package-private */
    public void parse(String expression) {
        if (expression != null) {
            try {
                if (!expression.equals("")) {
                    JSONObject creative = new JSONObject(expression);
                    OXMAdTrackingImpl tracking = new OXMAdTrackingImpl();
                    tracking.parse(creative.optString("tracking"));
                    if (tracking.hasParseError()) {
                        setParseError(true);
                        return;
                    }
                    setTracking(tracking);
                    setAlt(creative.optString("alt"));
                    setHeight(creative.optInt("height"));
                    setMedia(creative.optString("media"));
                    setMime(creative.optString("mime"));
                    setTarget(creative.optString("target"));
                    setWidth(creative.optInt("width"));
                }
            } catch (JSONException e) {
                setParseError(true);
            }
        }
    }
}
