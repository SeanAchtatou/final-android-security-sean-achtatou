package com.openx.ad.mobile.sdk.managers;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.openx.ad.mobile.sdk.interfaces.OXMLocationManager;
import java.util.List;
import java.util.Locale;

class OXMLocationManagerImpl extends OXMBaseManager implements OXMLocationManager {
    private static final int TWO_MINUTES = 120000;
    private String mAdminArea;
    private String mCountryName;
    private Geocoder mGeocoder;
    private LocationListener mLocListenerGps;
    private LocationListener mLocListenerNtw;
    private LocationManager mLocManager;
    private Location mLocation;
    private String mPostalCode;
    private String mSubAdminArea;

    OXMLocationManagerImpl() {
    }

    private LocationListener getGPSLocationListener() {
        return this.mLocListenerGps;
    }

    private void setGPSLocationListener(LocationListener listener) {
        this.mLocListenerGps = listener;
    }

    private LocationListener getNetworkLocationListener() {
        return this.mLocListenerNtw;
    }

    private void setNetworkLocationListener(LocationListener listener) {
        this.mLocListenerNtw = listener;
    }

    private LocationManager getLocationManager() {
        return this.mLocManager;
    }

    private void setLocationManager(LocationManager locManager) {
        this.mLocManager = locManager;
    }

    private Geocoder getGeocoder() {
        return this.mGeocoder;
    }

    private void setGeocoder(Geocoder geocoder) {
        this.mGeocoder = geocoder;
    }

    /* access modifiers changed from: private */
    public void updateAddress() {
        List<Address> addresses;
        try {
            if (getGeocoder() != null && (addresses = getGeocoder().getFromLocation(getLatitude().doubleValue(), getLongtitude().doubleValue(), 1)) != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                setCountry(address.getCountryName());
                setState(address.getAdminArea());
                setCity(address.getSubAdminArea());
                setZipCode(address.getPostalCode());
            }
        } catch (Exception e) {
        }
    }

    private class DevLocationListener implements LocationListener {
        private DevLocationListener() {
        }

        /* synthetic */ DevLocationListener(OXMLocationManagerImpl oXMLocationManagerImpl, DevLocationListener devLocationListener) {
            this();
        }

        public void onLocationChanged(Location loc) {
            if (OXMLocationManagerImpl.this.getLocation() == null) {
                OXMLocationManagerImpl.this.setLocation(loc);
                OXMLocationManagerImpl.this.updateAddress();
            } else if (OXMLocationManagerImpl.this.isBetterLocation(loc, OXMLocationManagerImpl.this.getLocation())) {
                OXMLocationManagerImpl.this.setLocation(loc);
                OXMLocationManagerImpl.this.updateAddress();
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    public void init(Context context) {
        super.init(context);
        if (super.isInit()) {
            try {
                setLocationManager((LocationManager) getContext().getSystemService("location"));
                setGPSLocationListener(new DevLocationListener(this, null));
                setNetworkLocationListener(new DevLocationListener(this, null));
                getLocationManager().requestLocationUpdates("gps", 0, 0.0f, getGPSLocationListener());
                getLocationManager().requestLocationUpdates("network", 0, 0.0f, getNetworkLocationListener());
                setGeocoder(new Geocoder(getContext(), Locale.getDefault()));
                Location gpsLastKnownLocation = getLocationManager().getLastKnownLocation("gps");
                Location ntwLastKnownLocation = getLocationManager().getLastKnownLocation("network");
                if (gpsLastKnownLocation != null) {
                    setLocation(gpsLastKnownLocation);
                    if (ntwLastKnownLocation != null && isBetterLocation(ntwLastKnownLocation, getLocation())) {
                        setLocation(ntwLastKnownLocation);
                        updateAddress();
                    }
                } else if (ntwLastKnownLocation != null) {
                    setLocation(ntwLastKnownLocation);
                    updateAddress();
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            return true;
        }
        if (location == null) {
            return false;
        }
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > 120000;
        boolean isSignificantlyOlder = timeDelta < -120000;
        boolean isNewer = timeDelta > 0;
        if (isSignificantlyNewer) {
            return true;
        }
        if (isSignificantlyOlder) {
            return false;
        }
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());
        if (isMoreAccurate) {
            return true;
        }
        if (isNewer && !isLessAccurate) {
            return true;
        }
        if (!isNewer || isSignificantlyLessAccurate || !isFromSameProvider) {
            return false;
        }
        return true;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    /* access modifiers changed from: private */
    public void setLocation(Location location) {
        this.mLocation = location;
    }

    /* access modifiers changed from: private */
    public Location getLocation() {
        return this.mLocation;
    }

    public Double getLatitude() {
        if (getLocation() != null) {
            return Double.valueOf(getLocation().getLatitude());
        }
        return null;
    }

    public Double getLongtitude() {
        if (getLocation() != null) {
            return Double.valueOf(getLocation().getLongitude());
        }
        return null;
    }

    private void setCountry(String country) {
        this.mCountryName = country;
    }

    public String getCountry() {
        return this.mCountryName;
    }

    private void setState(String state) {
        this.mAdminArea = state;
    }

    public String getState() {
        return this.mAdminArea;
    }

    private void setCity(String city) {
        this.mSubAdminArea = city;
    }

    public String getCity() {
        return this.mSubAdminArea;
    }

    private void setZipCode(String zipCode) {
        this.mPostalCode = zipCode;
    }

    public String getZipCode() {
        return this.mPostalCode;
    }

    public void dispose() {
        super.dispose();
        getLocationManager().removeUpdates(getGPSLocationListener());
        getLocationManager().removeUpdates(getNetworkLocationListener());
        setLocationManager(null);
        setGeocoder(null);
        setLocation(null);
        setGPSLocationListener(null);
        setNetworkLocationListener(null);
    }
}
