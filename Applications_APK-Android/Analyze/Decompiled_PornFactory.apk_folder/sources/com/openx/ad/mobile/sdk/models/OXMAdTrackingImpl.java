package com.openx.ad.mobile.sdk.models;

import com.openx.ad.mobile.sdk.interfaces.OXMAdTracking;
import org.json.JSONException;
import org.json.JSONObject;

class OXMAdTrackingImpl implements OXMAdTracking {
    private static final long serialVersionUID = -5983147989608371247L;
    private String mClickURL;
    private boolean mHasParseError;
    private String mImpressionURL;

    OXMAdTrackingImpl() {
    }

    /* access modifiers changed from: package-private */
    public void setParseError(boolean hasError) {
        this.mHasParseError = hasError;
    }

    /* access modifiers changed from: package-private */
    public boolean hasParseError() {
        return this.mHasParseError;
    }

    private void setImpressionURL(String impressionURL) {
        this.mImpressionURL = impressionURL;
    }

    public String getImpressionURL() {
        return this.mImpressionURL;
    }

    private void setClickURL(String clickURL) {
        this.mClickURL = clickURL;
    }

    public String getClickURL() {
        return this.mClickURL;
    }

    /* access modifiers changed from: package-private */
    public void parse(String expression) {
        if (expression != null) {
            try {
                if (!expression.equals("")) {
                    JSONObject tracking = new JSONObject(expression);
                    setImpressionURL(tracking.optString("impression"));
                    setClickURL(tracking.optString("click"));
                }
            } catch (JSONException e) {
                setParseError(true);
            }
        }
    }
}
