package com.openx.ad.mobile.sdk.controllers;

import android.content.Context;
import com.openx.ad.mobile.sdk.OXMEvent;
import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;
import com.openx.ad.mobile.sdk.errors.OXMAndroidSDKVersionNotSupported;
import com.openx.ad.mobile.sdk.interfaces.OXMAdBannerView;
import com.openx.ad.mobile.sdk.interfaces.OXMAdInterstitialControllerEventsListener;

public final class OXMAdInterstitialController extends OXMAdBaseController {
    public OXMAdInterstitialController(Context context, String domain) throws OXMAndroidSDKVersionNotSupported {
        super(context, domain);
    }

    public void initForAdUnitIds(long pauid, long lauid) {
        getCore().initForAdUnitIds(pauid, lauid);
    }

    public void setAdControllerEventsListener(OXMAdInterstitialControllerEventsListener listener) {
        getCore().setAdControllerEventsListener(listener);
    }

    public void onModalWindowClosing(OXMEvent event) {
        if (getCore().getAdControllerEventsListener() != null) {
            ((OXMAdInterstitialControllerEventsListener) getCore().getAdControllerEventsListener()).adControllerInterstitialDidUnloadAd(this);
        }
        startLoading();
    }

    public OXMAdBannerView getAdBannerView() {
        return getCore().getAdBannerView();
    }

    public OXMAdBaseController.OXMAdControllerType getControllerType() {
        return OXMAdBaseController.OXMAdControllerType.INTERSTITIAL;
    }
}
