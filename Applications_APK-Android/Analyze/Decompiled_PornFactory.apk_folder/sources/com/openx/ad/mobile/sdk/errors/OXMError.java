package com.openx.ad.mobile.sdk.errors;

public class OXMError extends Exception {
    private static final long serialVersionUID = -5221998429404202540L;
    private String mMessage;

    /* access modifiers changed from: protected */
    public void setMessage(String msg) {
        this.mMessage = msg;
    }

    public String getMessage() {
        return this.mMessage;
    }
}
