package com.openx.ad.mobile.sdk.controllers;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.openx.ad.mobile.sdk.OXMEvent;
import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;
import com.openx.ad.mobile.sdk.errors.OXMAndroidSDKVersionNotSupported;
import com.openx.ad.mobile.sdk.interfaces.OXMAd;
import com.openx.ad.mobile.sdk.interfaces.OXMAdBannerView;
import com.openx.ad.mobile.sdk.interfaces.OXMAdControllerEventsListener;
import java.util.Hashtable;
import java.util.Vector;

public final class OXMAdController extends OXMAdBaseController {
    public OXMAdController(Context context, String domain) throws OXMAndroidSDKVersionNotSupported {
        super(context, domain);
        getCore().setAdChangeInterval(30000);
    }

    public void initForAdUnitIds(long pauid, long lauid) {
        getCore().initForAdUnitIds(pauid, lauid);
    }

    public void initForAdUnitGroupIds(long ppgid, long lpgid) {
        getCore().initForAdUnitGroupIds(ppgid, lpgid);
    }

    public void setAdControllerEventsListener(OXMAdControllerEventsListener listener) {
        getCore().setAdControllerEventsListener(listener);
    }

    public void onModalWindowClosing(OXMEvent event) {
        OXMAdBannerView bannerView = getCore().findBannerViewByAd((!(event.getArgs() != null && (event.getArgs() instanceof Hashtable)) || !((Hashtable) event.getArgs()).containsKey("Ad")) ? null : (OXMAd) ((Hashtable) event.getArgs()).get("Ad"));
        if (bannerView != null) {
            bannerView.isExpanded(new Handler() {
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    Bundle dataBundle = (msg.getData() == null || msg.getData().size() <= 0) ? null : msg.getData();
                    if (dataBundle != null && !dataBundle.getString("value").equals("expanded")) {
                        OXMAdController.this.queueUIThreadTask(new Runnable() {
                            public void run() {
                                OXMAdController.this.getCore().resumeLoading();
                            }
                        });
                    }
                }
            });
        }
    }

    public OXMAdBannerView getAdBannerView() {
        return getCore().getAdBannerView();
    }

    public Vector<OXMAdBannerView> getAdBannerViews() {
        return getCore().getAdBannerViews();
    }

    public void setAdChangeInterval(int milliseconds) {
        getCore().setAdChangeInterval(milliseconds);
    }

    public OXMAdBaseController.OXMAdControllerType getControllerType() {
        return OXMAdBaseController.OXMAdControllerType.INLINE;
    }
}
