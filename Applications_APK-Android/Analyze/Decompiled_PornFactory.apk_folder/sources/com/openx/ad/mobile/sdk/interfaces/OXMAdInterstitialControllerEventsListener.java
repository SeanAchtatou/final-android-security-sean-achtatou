package com.openx.ad.mobile.sdk.interfaces;

import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;

public interface OXMAdInterstitialControllerEventsListener extends OXMAdControllerEventsListener {
    void adControllerInterstitialDidUnloadAd(OXMAdBaseController oXMAdBaseController);
}
