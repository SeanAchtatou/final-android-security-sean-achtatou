package com.openx.ad.mobile.sdk.interfaces;

import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;
import com.openx.ad.mobile.sdk.errors.OXMError;

public interface OXMAdControllerEventsListener {
    void adControllerActionDidFinish(OXMAdBaseController oXMAdBaseController);

    boolean adControllerActionShouldBegin(OXMAdBaseController oXMAdBaseController, boolean z);

    void adControllerActionUnableToBegin(OXMAdBaseController oXMAdBaseController);

    void adControllerDidFailToReceiveAdWithError(OXMAdBaseController oXMAdBaseController, Throwable th);

    void adControllerDidFailWithNonCriticalError(OXMAdBaseController oXMAdBaseController, OXMError oXMError);

    void adControllerDidLoadAd(OXMAdBaseController oXMAdBaseController);

    void adControllerWillLoadAd(OXMAdBaseController oXMAdBaseController);
}
