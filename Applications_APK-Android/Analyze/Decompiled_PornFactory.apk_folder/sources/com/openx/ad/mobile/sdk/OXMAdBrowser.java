package com.openx.ad.mobile.sdk;

import android.app.Activity;
import android.webkit.WebView;
import com.openx.ad.mobile.sdk.OXMEvent;
import com.openx.ad.mobile.sdk.controllers.OXMAdController;
import com.openx.ad.mobile.sdk.interfaces.OXMAd;
import com.openx.ad.mobile.sdk.interfaces.OXMEventsManager;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;
import java.util.Hashtable;

public final class OXMAdBrowser extends Activity {
    private OXMAd mAd;
    private int mControllerUID;
    private Object mCreator;
    private boolean mIsBrowser;
    private boolean mIsLongClosingCycle;
    private WebView mWebView;

    private void setIsLongClosingCycle(boolean isLongClosingCycle) {
        this.mIsLongClosingCycle = isLongClosingCycle;
    }

    private boolean getIsLongClosingCycle() {
        return this.mIsLongClosingCycle;
    }

    private void setIsBrowser(boolean isBrowser) {
        this.mIsBrowser = isBrowser;
    }

    private boolean getIsBrowser() {
        return this.mIsBrowser;
    }

    /* access modifiers changed from: private */
    public void setCreator(Object creator) {
        this.mCreator = creator;
    }

    private Object getCreator() {
        return this.mCreator;
    }

    private int getControllerUID() {
        return this.mControllerUID;
    }

    private void setControllerUID(int controllerUID) {
        this.mControllerUID = controllerUID;
    }

    private OXMAd getAd() {
        return this.mAd;
    }

    private void setAd(OXMAd ad) {
        this.mAd = ad;
    }

    private void setWebView(WebView webView) {
        this.mWebView = webView;
    }

    /* access modifiers changed from: private */
    public WebView getWebView() {
        return this.mWebView;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v0, resolved type: android.webkit.WebView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: android.widget.FrameLayout} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: android.webkit.WebView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: android.webkit.WebView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v5, resolved type: android.webkit.WebView} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r26) {
        /*
            r25 = this;
            super.onCreate(r26)
            android.content.Intent r21 = r25.getIntent()
            android.os.Bundle r21 = r21.getExtras()
            java.lang.String r22 = "Ad"
            java.io.Serializable r14 = r21.getSerializable(r22)
            android.content.Intent r21 = r25.getIntent()
            android.os.Bundle r21 = r21.getExtras()
            java.lang.String r22 = "AdUrl"
            java.lang.String r15 = r21.getString(r22)
            android.content.Intent r21 = r25.getIntent()
            android.os.Bundle r21 = r21.getExtras()
            java.lang.String r22 = "ControllerUID"
            int r10 = r21.getInt(r22)
            android.content.Intent r21 = r25.getIntent()
            android.os.Bundle r21 = r21.getExtras()
            java.lang.String r22 = "HasCutomCloseButton"
            boolean r16 = r21.getBoolean(r22)
            android.content.Intent r21 = r25.getIntent()
            android.os.Bundle r21 = r21.getExtras()
            java.lang.String r22 = "LongClosingCycle"
            boolean r21 = r21.getBoolean(r22)
            r0 = r25
            r1 = r21
            r0.setIsLongClosingCycle(r1)
            r7 = 0
            r5 = 1
            r0 = r25
            r0.setControllerUID(r10)
            if (r14 == 0) goto L_0x0060
            com.openx.ad.mobile.sdk.interfaces.OXMAd r14 = (com.openx.ad.mobile.sdk.interfaces.OXMAd) r14
            r0 = r25
            r0.setAd(r14)
        L_0x0060:
            com.openx.ad.mobile.sdk.OXMBrowserControls r12 = new com.openx.ad.mobile.sdk.OXMBrowserControls
            com.openx.ad.mobile.sdk.OXMAdBrowser$1 r21 = new com.openx.ad.mobile.sdk.OXMAdBrowser$1
            r0 = r21
            r1 = r25
            r0.<init>()
            r0 = r25
            r1 = r21
            r12.<init>(r0, r1)
            java.lang.String r21 = ""
            r0 = r21
            boolean r21 = r15.equals(r0)
            if (r21 != 0) goto L_0x0165
            r21 = 1
            r0 = r25
            r1 = r21
            r0.setIsBrowser(r1)
            android.webkit.WebView r21 = new android.webkit.WebView
            r0 = r21
            r1 = r25
            r0.<init>(r1)
            r0 = r25
            r1 = r21
            r0.setWebView(r1)
            android.webkit.WebView r21 = r25.getWebView()
            r22 = 100
            r21.setInitialScale(r22)
            android.webkit.WebView r21 = r25.getWebView()
            android.webkit.WebSettings r21 = r21.getSettings()
            r22 = 1
            r21.setJavaScriptEnabled(r22)
            android.webkit.WebView r21 = r25.getWebView()
            android.webkit.WebSettings r21 = r21.getSettings()
            r22 = 0
            r21.setJavaScriptCanOpenWindowsAutomatically(r22)
            android.webkit.WebView r21 = r25.getWebView()
            android.webkit.WebSettings r21 = r21.getSettings()
            r22 = 0
            r21.setPluginsEnabled(r22)
            android.webkit.WebView r21 = r25.getWebView()
            r22 = 0
            r21.setHorizontalScrollBarEnabled(r22)
            android.webkit.WebView r21 = r25.getWebView()
            r22 = 0
            r21.setVerticalScrollBarEnabled(r22)
            android.webkit.WebView r21 = r25.getWebView()
            android.webkit.WebSettings r21 = r21.getSettings()
            r22 = 2
            r21.setCacheMode(r22)
            android.webkit.WebView r21 = r25.getWebView()
            android.webkit.WebSettings r21 = r21.getSettings()
            r22 = 1
            r21.setBuiltInZoomControls(r22)
            android.webkit.WebView r21 = r25.getWebView()
            r0 = r21
            r0.loadUrl(r15)
            android.webkit.WebView r21 = r25.getWebView()
            com.openx.ad.mobile.sdk.OXMAdBrowser$2 r22 = new com.openx.ad.mobile.sdk.OXMAdBrowser$2
            r0 = r22
            r1 = r25
            r0.<init>(r12)
            r21.setWebViewClient(r22)
            r12.showNavigationControls()
            android.webkit.WebView r7 = r25.getWebView()
        L_0x0111:
            android.widget.LinearLayout r17 = new android.widget.LinearLayout
            r0 = r17
            r1 = r25
            r0.<init>(r1)
            r21 = 1
            r0 = r17
            r1 = r21
            r0.setOrientation(r1)
            android.view.ViewGroup$LayoutParams r8 = new android.view.ViewGroup$LayoutParams
            r21 = -1
            r22 = -1
            r0 = r21
            r1 = r22
            r8.<init>(r0, r1)
            if (r5 == 0) goto L_0x0142
            android.view.ViewGroup$LayoutParams r21 = new android.view.ViewGroup$LayoutParams
            r22 = -1
            r23 = -2
            r21.<init>(r22, r23)
            r0 = r17
            r1 = r21
            r0.addView(r12, r1)
        L_0x0142:
            if (r7 == 0) goto L_0x0149
            r0 = r17
            r0.addView(r7, r8)
        L_0x0149:
            r21 = 1
            r0 = r25
            r1 = r21
            r0.requestWindowFeature(r1)
            android.view.Window r21 = r25.getWindow()
            r22 = 1024(0x400, float:1.435E-42)
            r23 = 1024(0x400, float:1.435E-42)
            r21.setFlags(r22, r23)
            r0 = r25
            r1 = r17
            r0.setContentView(r1)
            return
        L_0x0165:
            com.openx.ad.mobile.sdk.interfaces.OXMAd r21 = r25.getAd()
            if (r21 == 0) goto L_0x0111
            com.openx.ad.mobile.sdk.managers.OXMManagersResolver r21 = com.openx.ad.mobile.sdk.managers.OXMManagersResolver.getInstance()
            com.openx.ad.mobile.sdk.interfaces.OXMDeviceManager r13 = r21.getDeviceManager()
            int r18 = r13.getDeviceOrientation()
            com.openx.ad.mobile.sdk.views.OXMAdViewFactory$OXMViewOrientation r20 = com.openx.ad.mobile.sdk.views.OXMAdViewFactory.OXMViewOrientation.UNDEFINED
            r21 = 1
            r0 = r18
            r1 = r21
            if (r0 != r1) goto L_0x01ec
            r21 = 1
            r0 = r25
            r1 = r21
            r0.setRequestedOrientation(r1)
            com.openx.ad.mobile.sdk.views.OXMAdViewFactory$OXMViewOrientation r20 = com.openx.ad.mobile.sdk.views.OXMAdViewFactory.OXMViewOrientation.PORTRAIT
        L_0x018c:
            com.openx.ad.mobile.sdk.managers.OXMManagersResolver r21 = com.openx.ad.mobile.sdk.managers.OXMManagersResolver.getInstance()
            com.openx.ad.mobile.sdk.interfaces.OXMControllersManager r11 = r21.getControllersManager()
            com.openx.ad.mobile.sdk.views.OXMAdViewFactory r21 = com.openx.ad.mobile.sdk.views.OXMAdViewFactory.getInstance()
            java.lang.String r22 = "interstitial"
            r0 = r21
            r1 = r25
            r2 = r22
            r3 = r25
            r4 = r20
            com.openx.ad.mobile.sdk.interfaces.OXMAdBannerView r19 = r0.createNewView(r1, r2, r3, r4)
            int r21 = r25.getControllerUID()
            java.lang.Integer r21 = java.lang.Integer.valueOf(r21)
            r0 = r21
            com.openx.ad.mobile.sdk.controllers.OXMAdBaseController r9 = r11.getController(r0)
            r0 = r19
            r0.setParentController(r9)
            r0 = r19
            r9.bindToView(r0)
            r7 = r19
            android.view.View r7 = (android.view.View) r7
            com.openx.ad.mobile.sdk.interfaces.OXMAd r21 = r25.getAd()
            r0 = r19
            r1 = r21
            r0.reloadAd(r1)
            android.widget.FrameLayout r6 = new android.widget.FrameLayout
            r0 = r25
            r6.<init>(r0)
            android.widget.FrameLayout$LayoutParams r21 = new android.widget.FrameLayout$LayoutParams
            r22 = -2
            r23 = -2
            r24 = 17
            r21.<init>(r22, r23, r24)
            r0 = r21
            r6.addView(r7, r0)
            r7 = r6
            if (r16 == 0) goto L_0x0111
            r5 = 0
            goto L_0x0111
        L_0x01ec:
            r21 = 2
            r0 = r18
            r1 = r21
            if (r0 != r1) goto L_0x0200
            r21 = 0
            r0 = r25
            r1 = r21
            r0.setRequestedOrientation(r1)
            com.openx.ad.mobile.sdk.views.OXMAdViewFactory$OXMViewOrientation r20 = com.openx.ad.mobile.sdk.views.OXMAdViewFactory.OXMViewOrientation.LANDSCAPE
            goto L_0x018c
        L_0x0200:
            com.openx.ad.mobile.sdk.views.OXMAdViewFactory$OXMViewOrientation r20 = com.openx.ad.mobile.sdk.views.OXMAdViewFactory.OXMViewOrientation.BOTH
            goto L_0x018c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.openx.ad.mobile.sdk.OXMAdBrowser.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Object creator = getCreator();
        if (creator == null || (creator instanceof OXMAdController)) {
            if (getIsBrowser()) {
                actionHasDone();
                if (getIsLongClosingCycle()) {
                    interstitialDidUnload();
                }
            } else {
                interstitialDidUnload();
            }
        }
        super.onDestroy();
    }

    private void actionHasDone() {
        OXMManagersResolver.getInstance().getEventsManager().fireEvent(new OXMEvent(OXMEvent.OXMEventType.ACTION_HAS_DONE, null, OXMManagersResolver.getInstance().getControllersManager().getController(Integer.valueOf(getControllerUID()))));
    }

    private void interstitialDidUnload() {
        OXMEventsManager eventsManager = OXMManagersResolver.getInstance().getEventsManager();
        Hashtable<String, Object> args = new Hashtable<>();
        args.put("Ad", getAd());
        args.put("ControllerUID", Integer.valueOf(getControllerUID()));
        eventsManager.fireEvent(new OXMEvent(OXMEvent.OXMEventType.MODAL_WINDOW_CLOSED, null, args));
    }
}
