package com.openx.ad.mobile.sdk.interfaces;

import java.io.Serializable;

public interface OXMAdCreative extends Serializable {
    String getAlt();

    int getHeight();

    String getMedia();

    String getMime();

    String getTarget();

    OXMAdTracking getTracking();

    int getWidth();
}
