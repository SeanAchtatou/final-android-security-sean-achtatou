package com.openx.ad.mobile.sdk.models;

import com.openx.ad.mobile.sdk.interfaces.OXMAd;
import com.openx.ad.mobile.sdk.interfaces.OXMAdGroup;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class OXMAdGroupImpl implements OXMAdGroup {
    private static final long serialVersionUID = 3129941895219271556L;
    private Vector<OXMAd> mAds = new Vector<>();
    private int mCount = 0;
    private boolean mHasParseError;
    private String mVersion = "";

    OXMAdGroupImpl() {
    }

    /* access modifiers changed from: package-private */
    public void setParseError(boolean hasError) {
        this.mHasParseError = hasError;
    }

    /* access modifiers changed from: package-private */
    public boolean hasParseError() {
        return this.mHasParseError;
    }

    public Vector<OXMAd> getAds() {
        return this.mAds;
    }

    private void setVersion(String version) {
        this.mVersion = version;
    }

    public String getVersion() {
        return this.mVersion;
    }

    private void setCount(int count) {
        this.mCount = count;
    }

    public int getCount() {
        return this.mCount;
    }

    /* access modifiers changed from: package-private */
    public void parse(String expression) {
        if (expression != null) {
            try {
                if (!expression.equals("")) {
                    JSONObject ads = new JSONObject(expression).optJSONObject("ads");
                    JSONArray adArray = ads.optJSONArray("ad");
                    int i = 0;
                    while (true) {
                        if (i >= adArray.length()) {
                            break;
                        }
                        OXMAdImpl ad = new OXMAdImpl();
                        ad.parse(adArray.optJSONObject(i).toString());
                        if (ad.hasParseError()) {
                            setParseError(true);
                            break;
                        } else {
                            getAds().add(ad);
                            i++;
                        }
                    }
                    if (!hasParseError()) {
                        setVersion(ads.optString("version"));
                        setCount(ads.optInt("count"));
                    }
                }
            } catch (JSONException e) {
                setParseError(true);
            }
        }
    }
}
