package com.openx.ad.mobile.sdk.errors;

public class OXMPermissionDeniedForApplication extends OXMError {
    private static final long serialVersionUID = -3471915029151828378L;

    public OXMPermissionDeniedForApplication(String permission) {
        super.setMessage("Permission " + permission + " denied. Please include this permission in your manifest file.");
    }
}
