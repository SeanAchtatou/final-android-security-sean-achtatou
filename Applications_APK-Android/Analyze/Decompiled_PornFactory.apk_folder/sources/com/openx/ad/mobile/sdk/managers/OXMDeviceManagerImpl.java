package com.openx.ad.mobile.sdk.managers;

import android.content.Context;
import android.content.res.Configuration;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.WindowManager;
import com.dd.plist.ASCIIPropertyListParser;
import com.openx.ad.mobile.sdk.interfaces.OXMDeviceManager;

class OXMDeviceManagerImpl extends OXMBaseManager implements OXMDeviceManager {
    private TelephonyManager mTelephonyManager;
    private WindowManager mWindowManager;

    OXMDeviceManagerImpl() {
    }

    private WindowManager getWindowManager() {
        return this.mWindowManager;
    }

    private void setWindowManager(WindowManager windowManager) {
        this.mWindowManager = windowManager;
    }

    private TelephonyManager getTelephonyManager() {
        return this.mTelephonyManager;
    }

    private void setTelephonyManager(TelephonyManager telephonyManager) {
        this.mTelephonyManager = telephonyManager;
    }

    public void init(Context context) {
        super.init(context);
        if (super.isInit()) {
            setTelephonyManager((TelephonyManager) getContext().getSystemService("phone"));
            setWindowManager((WindowManager) getContext().getSystemService("window"));
        }
    }

    public String getCarrier() {
        String operatorISO;
        if (!isInit() || (operatorISO = getTelephonyManager().getNetworkOperator()) == null || operatorISO.equals("") || operatorISO.length() <= 3) {
            return null;
        }
        return String.valueOf(operatorISO.substring(0, 3)) + ((char) ASCIIPropertyListParser.DATE_DATE_FIELD_DELIMITER) + operatorISO.substring(3);
    }

    public String getDeviceId() {
        if (isInit()) {
            return Settings.System.getString(getContext().getContentResolver(), "android_id");
        }
        return null;
    }

    public boolean isPermissionGranted(String permission) {
        Boolean bool;
        if (isInit()) {
            bool = Boolean.valueOf(getContext().checkCallingOrSelfPermission(permission) == 0);
        } else {
            bool = null;
        }
        return bool.booleanValue();
    }

    public int getDeviceOrientation() {
        Configuration config = isInit() ? getContext().getResources().getConfiguration() : null;
        if (config != null) {
            return config.orientation;
        }
        return 0;
    }

    public void dispose() {
        super.dispose();
        setTelephonyManager(null);
    }

    public int getScreenWidth() {
        return getWindowManager().getDefaultDisplay().getWidth();
    }

    public int getScreenHeight() {
        return getWindowManager().getDefaultDisplay().getHeight();
    }
}
