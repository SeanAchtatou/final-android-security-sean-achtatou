package com.openx.ad.mobile.sdk.interfaces;

import java.io.Serializable;
import java.util.Vector;

public interface OXMAd extends Serializable {
    int getAdGroupId();

    int getAdId();

    int getAdUnitId();

    Vector<OXMAdCreative> getCreatives();

    String getHTML();

    String getType();
}
