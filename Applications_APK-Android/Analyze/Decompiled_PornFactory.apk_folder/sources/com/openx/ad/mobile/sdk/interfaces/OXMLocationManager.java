package com.openx.ad.mobile.sdk.interfaces;

public interface OXMLocationManager {
    String getCity();

    String getCountry();

    Double getLatitude();

    Double getLongtitude();

    String getState();

    String getZipCode();
}
