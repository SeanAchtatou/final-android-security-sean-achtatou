package com.openx.ad.mobile.sdk.interfaces;

import com.openx.ad.mobile.sdk.errors.OXMError;

public interface OXMAdViewEventsListener {
    void adViewActionDidFinish(String str);

    boolean adViewActionShouldBegin(String str, boolean z);

    void adViewActionUnableToBegin(String str);

    void adViewDidLoad();

    void adViewNonCriticalError(OXMError oXMError);

    void adViewOnRemove(OXMAdBannerView oXMAdBannerView);

    void adViewOnTrackingEvent(String str, String str2);

    void adViewResumeReloading();
}
