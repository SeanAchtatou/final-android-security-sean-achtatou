package com.openx.ad.mobile.sdk;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import com.openx.ad.mobile.sdk.OXMEvent;
import com.openx.ad.mobile.sdk.interfaces.OXMEventsManager;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class OXMUtils {
    public static String md5(String exp) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(exp.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : messageDigest) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            log(null, e.getMessage());
            return "";
        }
    }

    public static BitmapDrawable getDrawable(String nameWithExtension) {
        InputStream is = OXMUtils.class.getClassLoader().getResourceAsStream("com/openx/ad/mobile/sdk/drawable/" + nameWithExtension);
        if (is != null) {
            return new BitmapDrawable(BitmapFactory.decodeStream(is));
        }
        return null;
    }

    public static String loadJavaScriptFile(String name) {
        InputStream scriptStream = OXMUtils.class.getClassLoader().getResourceAsStream("com/openx/ad/mobile/sdk/" + name + ".js");
        StringBuilder retVal = new StringBuilder();
        if (scriptStream != null) {
            BufferedReader buffer = new BufferedReader(new InputStreamReader(scriptStream));
            while (true) {
                try {
                    String line = buffer.readLine();
                    if (line == null) {
                        break;
                    }
                    retVal.append(line);
                } catch (IOException e) {
                    log(null, e.getMessage());
                }
            }
        }
        return retVal.toString();
    }

    public static void log(Object sender, Object args) {
        OXMEventsManager eventsManager = OXMManagersResolver.getInstance().getEventsManager();
        if (eventsManager != null) {
            eventsManager.fireEvent(new OXMEvent(OXMEvent.OXMEventType.LOG, sender, args));
        }
    }
}
