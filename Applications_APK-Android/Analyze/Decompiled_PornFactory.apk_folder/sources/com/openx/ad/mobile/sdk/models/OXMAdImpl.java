package com.openx.ad.mobile.sdk.models;

import com.nth.analytics.android.LocalyticsProvider;
import com.openx.ad.mobile.sdk.interfaces.OXMAd;
import com.openx.ad.mobile.sdk.interfaces.OXMAdCreative;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class OXMAdImpl implements OXMAd {
    private static final long serialVersionUID = -7396825100961259293L;
    private int mAdGroupId;
    private int mAdId;
    private int mAdUnitId;
    private Vector<OXMAdCreative> mCreatives = new Vector<>();
    private String mHTML;
    private boolean mHasParseError;
    private String mType;

    OXMAdImpl() {
    }

    /* access modifiers changed from: package-private */
    public void setParseError(boolean hasError) {
        this.mHasParseError = hasError;
    }

    /* access modifiers changed from: package-private */
    public boolean hasParseError() {
        return this.mHasParseError;
    }

    private void setHTML(String html) {
        this.mHTML = html;
    }

    public String getHTML() {
        return this.mHTML;
    }

    public Vector<OXMAdCreative> getCreatives() {
        return this.mCreatives;
    }

    private void setAdGroupId(int adGroupId) {
        this.mAdGroupId = adGroupId;
    }

    public int getAdGroupId() {
        return this.mAdGroupId;
    }

    private void setAdUnitId(int adUnitId) {
        this.mAdUnitId = adUnitId;
    }

    public int getAdUnitId() {
        return this.mAdUnitId;
    }

    private void setAdId(int adId) {
        this.mAdId = adId;
    }

    public int getAdId() {
        return this.mAdId;
    }

    private void setType(String type) {
        this.mType = type;
    }

    public String getType() {
        return this.mType;
    }

    /* access modifiers changed from: package-private */
    public void parse(String expression) {
        if (expression != null) {
            try {
                if (!expression.equals("")) {
                    JSONObject ad = new JSONObject(expression);
                    JSONArray creativesArray = ad.optJSONArray("creative");
                    int i = 0;
                    while (true) {
                        if (i >= creativesArray.length()) {
                            break;
                        }
                        OXMAdCreativeImpl creative = new OXMAdCreativeImpl();
                        creative.parse(creativesArray.optJSONObject(i).toString());
                        if (creative.hasParseError()) {
                            setParseError(true);
                            break;
                        } else {
                            getCreatives().add(creative);
                            i++;
                        }
                    }
                    if (!hasParseError()) {
                        setAdId(ad.optInt("adid"));
                        setAdUnitId(ad.optInt("adunitid"));
                        setAdGroupId(ad.optInt("adunitgroup"));
                        setHTML(ad.optString("html"));
                        setType(ad.optString(LocalyticsProvider.EventHistoryDbColumns.TYPE));
                    }
                }
            } catch (JSONException e) {
                setParseError(true);
            }
        }
    }
}
