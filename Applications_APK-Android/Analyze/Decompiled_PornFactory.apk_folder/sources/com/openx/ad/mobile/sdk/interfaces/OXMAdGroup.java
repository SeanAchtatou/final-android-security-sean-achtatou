package com.openx.ad.mobile.sdk.interfaces;

import java.io.Serializable;
import java.util.Vector;

public interface OXMAdGroup extends Serializable {
    Vector<OXMAd> getAds();

    int getCount();

    String getVersion();
}
