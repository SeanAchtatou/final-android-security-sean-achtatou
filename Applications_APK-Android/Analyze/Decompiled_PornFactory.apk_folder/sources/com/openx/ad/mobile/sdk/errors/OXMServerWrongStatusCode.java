package com.openx.ad.mobile.sdk.errors;

public class OXMServerWrongStatusCode extends OXMError {
    private static final long serialVersionUID = -4918332368118548336L;

    public OXMServerWrongStatusCode(int code) {
        super.setMessage("Server returned " + code + " status code");
    }
}
