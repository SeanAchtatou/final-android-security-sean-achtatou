package com.openx.ad.mobile.sdk.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import com.openx.ad.mobile.sdk.OXMAdCallParams;
import com.openx.ad.mobile.sdk.interfaces.OXMNetworkManager;

class OXMNetworkManagerImpl extends OXMBaseManager implements OXMNetworkManager {
    private ConnectivityManager mConnectivityManager;
    private TelephonyManager mTelephonyManager;

    OXMNetworkManagerImpl() {
    }

    private TelephonyManager getTelephonyManager() {
        return this.mTelephonyManager;
    }

    private void setTelephonyManager(TelephonyManager telephonyManager) {
        this.mTelephonyManager = telephonyManager;
    }

    private ConnectivityManager getConnectivityManager() {
        return this.mConnectivityManager;
    }

    private void setConnectivityManager(ConnectivityManager connectivityManager) {
        this.mConnectivityManager = connectivityManager;
    }

    public void init(Context context) {
        super.init(context);
        if (super.isInit()) {
            setTelephonyManager((TelephonyManager) getContext().getSystemService("phone"));
            setConnectivityManager((ConnectivityManager) getContext().getSystemService("connectivity"));
        }
    }

    public OXMAdCallParams.OXMConnectionType getConnectionType() {
        if (isInit()) {
            NetworkInfo info = getConnectivityManager().getActiveNetworkInfo();
            if (info == null) {
                return OXMAdCallParams.OXMConnectionType.OFFLINE;
            }
            int netType = info.getType();
            int netSubtype = info.getSubtype();
            if (netType == 1) {
                if (info.isConnected()) {
                    return OXMAdCallParams.OXMConnectionType.WIFI;
                }
                return OXMAdCallParams.OXMConnectionType.OFFLINE;
            } else if (netType == 0 && netSubtype == 3 && !getTelephonyManager().isNetworkRoaming()) {
                if (info.isConnected()) {
                    return OXMAdCallParams.OXMConnectionType.CELL;
                }
                return OXMAdCallParams.OXMConnectionType.OFFLINE;
            }
        }
        return OXMAdCallParams.OXMConnectionType.OFFLINE;
    }
}
