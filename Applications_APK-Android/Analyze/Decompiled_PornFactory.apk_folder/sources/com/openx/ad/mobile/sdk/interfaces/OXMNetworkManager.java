package com.openx.ad.mobile.sdk.interfaces;

import com.openx.ad.mobile.sdk.OXMAdCallParams;

public interface OXMNetworkManager {
    OXMAdCallParams.OXMConnectionType getConnectionType();
}
