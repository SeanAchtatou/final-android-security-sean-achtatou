package com.openx.ad.mobile.sdk.managers;

import android.content.Context;
import com.openx.ad.mobile.sdk.interfaces.OXMManager;

class OXMBaseManager implements OXMManager {
    private Context mContext;
    private boolean mIsInit;

    OXMBaseManager() {
    }

    public boolean isInit() {
        return this.mIsInit;
    }

    public void init(Context context) {
        if (context != null) {
            this.mContext = context;
            this.mIsInit = true;
        }
    }

    public Context getContext() {
        return this.mContext;
    }

    public void dispose() {
        this.mIsInit = false;
        this.mContext = null;
    }
}
