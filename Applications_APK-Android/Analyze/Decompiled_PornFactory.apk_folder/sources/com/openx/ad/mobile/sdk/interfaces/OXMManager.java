package com.openx.ad.mobile.sdk.interfaces;

import android.content.Context;

public interface OXMManager {
    void dispose();

    Context getContext();

    void init(Context context);

    boolean isInit();
}
