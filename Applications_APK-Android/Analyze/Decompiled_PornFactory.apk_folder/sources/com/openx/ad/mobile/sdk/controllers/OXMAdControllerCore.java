package com.openx.ad.mobile.sdk.controllers;

import android.os.Handler;
import com.openx.ad.mobile.sdk.OXMAdCallParams;
import com.openx.ad.mobile.sdk.OXMUtils;
import com.openx.ad.mobile.sdk.errors.OXMError;
import com.openx.ad.mobile.sdk.interfaces.OXMAd;
import com.openx.ad.mobile.sdk.interfaces.OXMAdBannerView;
import com.openx.ad.mobile.sdk.interfaces.OXMAdControllerEventsListener;
import com.openx.ad.mobile.sdk.interfaces.OXMAdGroup;
import com.openx.ad.mobile.sdk.interfaces.OXMAdModel;
import com.openx.ad.mobile.sdk.interfaces.OXMAdModelEventsListener;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;
import com.openx.ad.mobile.sdk.models.OXMAdModelFactory;
import com.openx.ad.mobile.sdk.views.OXMAdViewFactory;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

final class OXMAdControllerCore {
    private Hashtable<String, OXMAdBannerView> mAdBannerViewsAll;
    private Vector<OXMAdBannerView> mAdBannerViewsLandscape;
    private Vector<OXMAdBannerView> mAdBannerViewsPortrait;
    private int mAdChangeInterval = 0;
    private Timer mAdChangeTimer;
    private OXMAdControllerEventsListener mAdControllerEventsListener;
    private OXMAdBaseController mController;
    private int mCountOfLoadedViews;
    private final int mIntSDKVersion = 1;
    private final String mMarketingSDKVersion = Constants.LOCALYTICS_CLIENT_LIBRARY_VERSION;
    private OXMAdModel mModel;
    private Handler mUIHandler;

    private void initUIHandler() {
        this.mUIHandler = new Handler();
    }

    private Handler getUIHandler() {
        return this.mUIHandler;
    }

    public OXMAdControllerCore(OXMAdBaseController controller, String domain) {
        initUIHandler();
        setController(controller);
        setAdModel(OXMAdModelFactory.getInstance().createNewModel());
        getAdModel().setAdDomain(domain);
        getAdModel().setAsyncCallbacksListener(new OXMAdModelEventsListener() {
            public void adModelLoadAdSuccess(OXMAdGroup group) {
                OXMAdViewFactory.OXMViewOrientation devOrientation;
                if (OXMAdControllerCore.this.getAdBannerViewsAll() == null) {
                    OXMUtils.log(this, "Creating new banner view sets");
                    OXMAdControllerCore.this.setAdBannerViewsAll(new Hashtable());
                    OXMAdControllerCore.this.setAdBannerViewsPortrait(new Vector());
                    OXMAdControllerCore.this.setAdBannerViewsLandscape(new Vector());
                }
                Enumeration<OXMAd> ads = group.getAds().elements();
                String idp = OXMAdControllerCore.this.getAdModel().getPortraitId();
                String idl = OXMAdControllerCore.this.getAdModel().getLandscapeId();
                int orientation = OXMManagersResolver.getInstance().getDeviceManager().getDeviceOrientation();
                OXMUtils.log(this, "Filling / Updating banner view sets");
                if (orientation == 1) {
                    devOrientation = OXMAdViewFactory.OXMViewOrientation.PORTRAIT;
                } else if (orientation == 2) {
                    devOrientation = OXMAdViewFactory.OXMViewOrientation.LANDSCAPE;
                } else if (orientation == 3) {
                    devOrientation = OXMAdViewFactory.OXMViewOrientation.BOTH;
                } else {
                    devOrientation = OXMAdViewFactory.OXMViewOrientation.UNDEFINED;
                }
                OXMAdControllerCore.this.setCountOfLoadedViews(0);
                while (ads.hasMoreElements() && !OXMAdControllerCore.this.getController().isDisposed()) {
                    OXMAd ad = ads.nextElement();
                    String key = OXMUtils.md5(String.valueOf(String.valueOf(ad.getAdGroupId())) + String.valueOf(ad.getAdUnitId()));
                    if (!OXMAdControllerCore.this.getAdBannerViewsAll().containsKey(key)) {
                        OXMAdViewFactory.OXMViewOrientation viewOrientation = OXMAdViewFactory.OXMViewOrientation.UNDEFINED;
                        boolean isPortrait = OXMAdControllerCore.this.isApplicableAd(ad, idp);
                        boolean isLandscape = OXMAdControllerCore.this.isApplicableAd(ad, idl);
                        if (isPortrait && isLandscape) {
                            viewOrientation = OXMAdViewFactory.OXMViewOrientation.BOTH;
                        } else if (isPortrait) {
                            viewOrientation = OXMAdViewFactory.OXMViewOrientation.PORTRAIT;
                        } else if (isLandscape) {
                            viewOrientation = OXMAdViewFactory.OXMViewOrientation.LANDSCAPE;
                        }
                        OXMAdBannerView view = OXMAdViewFactory.getInstance().createNewView(OXMAdControllerCore.this.getController().getContext(), "inline", this, viewOrientation);
                        OXMAdControllerCore.this.getController().bindToView(view);
                        view.setParentController(OXMAdControllerCore.this.getController());
                        OXMAdControllerCore.this.getAdBannerViewsAll().put(key, view);
                        if (viewOrientation == OXMAdViewFactory.OXMViewOrientation.BOTH) {
                            OXMAdControllerCore.this.getAdBannerViewsPortrait().add(view);
                            OXMAdControllerCore.this.getAdBannerViewsLandscape().add(view);
                            view.reloadAd(ad);
                        } else if (viewOrientation == OXMAdViewFactory.OXMViewOrientation.PORTRAIT) {
                            OXMAdControllerCore.this.getAdBannerViewsPortrait().add(view);
                            if (orientation == 1) {
                                view.reloadAd(ad);
                            }
                        } else if (viewOrientation == OXMAdViewFactory.OXMViewOrientation.LANDSCAPE) {
                            OXMAdControllerCore.this.getAdBannerViewsLandscape().add(view);
                            if (orientation == 2) {
                                view.reloadAd(ad);
                            }
                        }
                    } else if (devOrientation == ((OXMAdBannerView) OXMAdControllerCore.this.getAdBannerViewsAll().get(key)).getViewOrientation()) {
                        ((OXMAdBannerView) OXMAdControllerCore.this.getAdBannerViewsAll().get(key)).reloadAd(ad);
                    }
                }
                if (!OXMAdControllerCore.this.getController().isDisposed()) {
                    OXMUtils.log(this, "Schedule new task for advertisement update");
                    OXMAdControllerCore.this.resumeLoading();
                    if (OXMAdControllerCore.this.getAdControllerEventsListener() != null) {
                        OXMUtils.log(this, "Perform callback of loading completion");
                        OXMAdControllerCore.this.getAdControllerEventsListener().adControllerWillLoadAd(OXMAdControllerCore.this.getController());
                    }
                }
            }

            public void adModelLoadAdFail(OXMError ex) {
                if (OXMAdControllerCore.this.getAdControllerEventsListener() != null) {
                    OXMAdControllerCore.this.getAdControllerEventsListener().adControllerDidFailToReceiveAdWithError(OXMAdControllerCore.this.getController(), ex);
                }
            }

            public void adModelNonCriticalError(OXMError err) {
                if (OXMAdControllerCore.this.getAdControllerEventsListener() != null) {
                    OXMAdControllerCore.this.getAdControllerEventsListener().adControllerDidFailWithNonCriticalError(OXMAdControllerCore.this.getController(), err);
                }
            }
        });
    }

    private int getIntSDKVersion() {
        return 1;
    }

    private String getMarketingSDKVersion() {
        return Constants.LOCALYTICS_CLIENT_LIBRARY_VERSION;
    }

    /* access modifiers changed from: private */
    public boolean isApplicableAd(OXMAd ad, String id) {
        if (ad == null) {
            return false;
        }
        int idNum = Integer.parseInt(id);
        if (getAdModel().isGroupIds()) {
            if (ad.getAdGroupId() == idNum) {
                return true;
            }
            return false;
        } else if (ad.getAdUnitId() == idNum) {
            return true;
        } else {
            return false;
        }
    }

    private void setController(OXMAdBaseController controller) {
        this.mController = controller;
    }

    /* access modifiers changed from: private */
    public OXMAdBaseController getController() {
        return this.mController;
    }

    private void setAdChangeTimer(Timer timer) {
        this.mAdChangeTimer = timer;
    }

    private Timer getAdChangeTimer() {
        if (this.mAdChangeTimer == null) {
            this.mAdChangeTimer = new Timer();
        }
        return this.mAdChangeTimer;
    }

    /* access modifiers changed from: private */
    public void setAdBannerViewsPortrait(Vector<OXMAdBannerView> adBannerViews) {
        this.mAdBannerViewsPortrait = adBannerViews;
    }

    /* access modifiers changed from: private */
    public Vector<OXMAdBannerView> getAdBannerViewsPortrait() {
        return this.mAdBannerViewsPortrait;
    }

    /* access modifiers changed from: private */
    public void setAdBannerViewsLandscape(Vector<OXMAdBannerView> adBannerViews) {
        this.mAdBannerViewsLandscape = adBannerViews;
    }

    /* access modifiers changed from: private */
    public Vector<OXMAdBannerView> getAdBannerViewsLandscape() {
        return this.mAdBannerViewsLandscape;
    }

    private void setAdModel(OXMAdModel adModel) {
        this.mModel = adModel;
    }

    /* access modifiers changed from: private */
    public OXMAdModel getAdModel() {
        return this.mModel;
    }

    private int getAdChangeInterval() {
        return this.mAdChangeInterval;
    }

    /* access modifiers changed from: private */
    public void setAdBannerViewsAll(Hashtable<String, OXMAdBannerView> adBannerViews) {
        this.mAdBannerViewsAll = adBannerViews;
    }

    /* access modifiers changed from: private */
    public Hashtable<String, OXMAdBannerView> getAdBannerViewsAll() {
        return this.mAdBannerViewsAll;
    }

    private void stopAdChangeTimer() {
        if (getAdChangeTimer() != null) {
            getAdChangeTimer().cancel();
            getAdChangeTimer().purge();
            setAdChangeTimer(null);
        }
    }

    private void removeBannerElement(Vector<OXMAdBannerView> views, OXMAdBannerView view) {
        if (views != null && views.contains(view)) {
            views.remove(view);
        }
    }

    public void queueUIThreadTask(Runnable task) {
        getUIHandler().post(task);
    }

    public void removeAdBannerView(OXMAdBannerView viewToDelete) {
        Hashtable<String, OXMAdBannerView> allViews = getAdBannerViewsAll();
        if (allViews != null) {
            Enumeration<String> allViewsEn = allViews.keys();
            while (true) {
                if (!allViewsEn.hasMoreElements()) {
                    break;
                }
                String viewKey = allViewsEn.nextElement();
                OXMAdBannerView view = allViews.get(viewKey);
                if (view == viewToDelete) {
                    allViews.remove(viewKey);
                    removeBannerElement(getAdBannerViewsPortrait(), view);
                    removeBannerElement(getAdBannerViewsLandscape(), view);
                    break;
                }
            }
            if (allViews.size() == 0) {
                setAdBannerViewsAll(null);
            }
        }
    }

    public void removeAllAdBannerViews() {
        Hashtable<String, OXMAdBannerView> allViews = getAdBannerViewsAll();
        if (allViews != null) {
            Enumeration<OXMAdBannerView> allViewsEn = allViews.elements();
            while (allViewsEn.hasMoreElements()) {
                allViewsEn.nextElement().dispose();
            }
        }
    }

    public void setCountOfLoadedViews(int countOfLoadedViews) {
        this.mCountOfLoadedViews = countOfLoadedViews;
    }

    public int getCountOfLoadedViews() {
        return this.mCountOfLoadedViews;
    }

    public void trackEvent(String name, String url) {
        getAdModel().trackEvent(name, url);
    }

    public boolean isLoaded() {
        return getAdModel().isLoaded();
    }

    public void setAdCallParams(OXMAdCallParams clParams) {
        getAdModel().setAdCallParams(clParams);
    }

    public OXMAdCallParams getAdCallParams() {
        return getAdModel().getAdCallParams();
    }

    public void startLoading() {
        OXMUtils.log(this, "Stop loading if there is one and clear update timer for advertisement reload");
        getController().stopLoading();
        OXMUtils.log(this, "Start loading");
        getAdModel().processData();
    }

    public void stopLoading() {
        stopAdChangeTimer();
        getAdModel().stopLoading();
    }

    public void setAdControllerEventsListener(OXMAdControllerEventsListener listener) {
        this.mAdControllerEventsListener = listener;
    }

    public void initForAdUnitIds(long pauid, long lauid) {
        getAdModel().setIsGroupIds(false);
        getAdModel().setLandscapeId(String.valueOf(lauid));
        getAdModel().setPortraitId(String.valueOf(pauid));
    }

    public void initForAdUnitGroupIds(long ppgid, long lpgid) {
        getAdModel().setIsGroupIds(true);
        getAdModel().setLandscapeId(String.valueOf(lpgid));
        getAdModel().setPortraitId(String.valueOf(ppgid));
    }

    public OXMAdBannerView getAdBannerView() {
        Vector<OXMAdBannerView> views = getAdBannerViews();
        OXMUtils.log(this, "Get first banner view for current orientation");
        if (views != null && views.size() > 0) {
            return views.elementAt(0);
        }
        OXMUtils.log(this, "First banner view was not found");
        return null;
    }

    public Vector<OXMAdBannerView> getAdBannerViews() {
        int orientation = OXMManagersResolver.getInstance().getDeviceManager().getDeviceOrientation();
        if (orientation == 1) {
            return getAdBannerViewsPortrait();
        }
        if (orientation == 2) {
            return getAdBannerViewsLandscape();
        }
        return null;
    }

    public void setAdChangeInterval(int milliseconds) {
        this.mAdChangeInterval = milliseconds;
    }

    public OXMAdControllerEventsListener getAdControllerEventsListener() {
        return this.mAdControllerEventsListener;
    }

    public void resumeLoading() {
        if (getAdChangeInterval() > 0) {
            getAdChangeTimer().schedule(new TimerTask() {
                public void run() {
                    OXMAdControllerCore.this.queueUIThreadTask(new Runnable() {
                        public void run() {
                            OXMAdControllerCore.this.startLoading();
                        }
                    });
                }
            }, (long) getAdChangeInterval());
        }
    }

    public OXMAdBannerView findBannerViewByAd(OXMAd ad) {
        Hashtable<String, OXMAdBannerView> allViews = getAdBannerViewsAll();
        if (!(allViews == null || ad == null)) {
            for (String key : allViews.keySet()) {
                OXMAdBannerView view = allViews.get(key);
                if (view.getAd() != null && view.getAd().getAdId() == ad.getAdId()) {
                    return view;
                }
            }
        }
        return null;
    }

    public void dispose() {
        if (getController().isDisposed()) {
            removeAllAdBannerViews();
        }
    }
}
