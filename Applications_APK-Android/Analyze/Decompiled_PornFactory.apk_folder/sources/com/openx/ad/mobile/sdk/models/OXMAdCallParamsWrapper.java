package com.openx.ad.mobile.sdk.models;

import com.openx.ad.mobile.sdk.OXMAdCallParams;
import com.openx.ad.mobile.sdk.OXMUtils;
import com.openx.ad.mobile.sdk.errors.OXMPermissionDeniedForApplication;
import com.openx.ad.mobile.sdk.errors.OXMUnknownError;
import com.openx.ad.mobile.sdk.interfaces.OXMAdModelEventsListener;
import com.openx.ad.mobile.sdk.interfaces.OXMDeviceManager;
import com.openx.ad.mobile.sdk.interfaces.OXMLocationManager;
import com.openx.ad.mobile.sdk.interfaces.OXMNetworkManager;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;

class OXMAdCallParamsWrapper {
    private transient OXMAdCallParams mAdCallParams;
    private String mDomain;
    private boolean mIsGroupIds;
    private String mLandscapeId;
    private transient OXMAdModelEventsListener mModelEventsListener;
    private String mPortraitId;

    public OXMAdCallParamsWrapper(OXMAdCallParams clParams, String domain, String pid, String lid, boolean groupIds, OXMAdModelEventsListener listener) {
        setAdCallParams(clParams);
        setAdDomain(domain);
        setPortraitId(pid);
        setLandscapeId(lid);
        setIsGroupIds(groupIds);
        setAsyncCallbacksListener(listener);
    }

    private OXMAdModelEventsListener getAsyncCallbacksListener() {
        return this.mModelEventsListener;
    }

    private void setAsyncCallbacksListener(OXMAdModelEventsListener listener) {
        this.mModelEventsListener = listener;
    }

    private void setIsGroupIds(boolean isGroupIds) {
        this.mIsGroupIds = isGroupIds;
    }

    private boolean isGroupIds() {
        return this.mIsGroupIds;
    }

    private void setAdCallParams(OXMAdCallParams clParams) {
        this.mAdCallParams = clParams;
    }

    private OXMAdCallParams getAdCallParams() {
        return this.mAdCallParams;
    }

    private String getPortraitId() {
        return this.mPortraitId;
    }

    private String getLandscapeId() {
        return this.mLandscapeId;
    }

    private void setPortraitId(String id) {
        this.mPortraitId = id;
    }

    private void setLandscapeId(String id) {
        this.mLandscapeId = id;
    }

    private void setAdDomain(String domain) {
        this.mDomain = domain;
    }

    private String getAdDomain() {
        return this.mDomain;
    }

    public String generateURL() {
        StringBuilder template = new StringBuilder();
        if (getAdCallParams() == null) {
            setAdCallParams(new OXMAdCallParams());
        }
        Hashtable<String, String> params = getAdCallParams().getParams();
        Hashtable<String, String> cParams = getAdCallParams().getCustomParams();
        template.append(getAdCallParams().isSSL() ? "https://" : "http://");
        if (getAdDomain() == null || getPortraitId() == null || getLandscapeId() == null || getAdDomain().equals("")) {
            return null;
        }
        template.append(getAdDomain());
        template.append("/ma/1.0/arj?");
        if (!isGroupIds()) {
            if (!getPortraitId().equals(getLandscapeId())) {
                template.append("auid=" + getPortraitId() + "," + getLandscapeId());
            } else {
                template.append("auid=" + getPortraitId());
            }
        } else if (!getPortraitId().equals(getLandscapeId())) {
            template.append("pgid=" + getPortraitId() + "," + getLandscapeId());
        } else {
            template.append("pgid=" + getPortraitId());
        }
        detectUnspecifiedParams(params);
        Enumeration<String> en = params.keys();
        while (en.hasMoreElements()) {
            String key = en.nextElement();
            String value = params.get(key);
            if (value != null && !value.equals("")) {
                try {
                    value = URLEncoder.encode(value, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    OXMUnknownError err = new OXMUnknownError(e.getMessage());
                    OXMUtils.log(this, err.getMessage());
                    if (getAsyncCallbacksListener() != null) {
                        getAsyncCallbacksListener().adModelNonCriticalError(err);
                    }
                }
                template.append("&" + key + "=" + value);
            }
        }
        Enumeration<String> en2 = cParams.keys();
        while (en2.hasMoreElements()) {
            String key2 = en2.nextElement();
            String value2 = cParams.get(key2);
            if (value2 != null && !value2.equals("")) {
                try {
                    value2 = URLEncoder.encode(value2, "utf-8");
                } catch (UnsupportedEncodingException e2) {
                    OXMUnknownError err2 = new OXMUnknownError(e2.getMessage());
                    OXMUtils.log(this, err2.getMessage());
                    if (getAsyncCallbacksListener() != null) {
                        getAsyncCallbacksListener().adModelNonCriticalError(err2);
                    }
                }
                template.append("&" + key2 + "=" + value2);
            }
        }
        return template.toString();
    }

    private void detectUnspecifiedParams(Hashtable<String, String> params) {
        String autoDetectedValue;
        String autoDetectedValue2;
        String autoDetectedValue3;
        String autoDetectedValue4;
        String autoDetectedValue5;
        String autoDetectedValue6;
        Double autoDetectedValue7;
        Double autoDetectedValue8;
        OXMDeviceManager deviceManager = OXMManagersResolver.getInstance().getDeviceManager();
        OXMLocationManager locationManager = OXMManagersResolver.getInstance().getLocationManager();
        OXMNetworkManager networkManager = OXMManagersResolver.getInstance().getNetworkManager();
        if (deviceManager.isPermissionGranted("android.permission.ACCESS_FINE_LOCATION")) {
            if (!params.containsKey("lat") && (autoDetectedValue8 = locationManager.getLatitude()) != null) {
                params.put("lat", String.valueOf(autoDetectedValue8.doubleValue()));
            }
            if (!params.containsKey("lon") && (autoDetectedValue7 = locationManager.getLongtitude()) != null) {
                params.put("lon", String.valueOf(autoDetectedValue7.doubleValue()));
            }
        } else {
            OXMPermissionDeniedForApplication permDenied = new OXMPermissionDeniedForApplication("android.permission.ACCESS_FINE_LOCATION");
            OXMUtils.log(this, permDenied.getMessage());
            if (getAsyncCallbacksListener() != null) {
                getAsyncCallbacksListener().adModelNonCriticalError(permDenied);
            }
        }
        if (!params.containsKey("crr") && (autoDetectedValue6 = deviceManager.getCarrier()) != null && !autoDetectedValue6.equals("")) {
            params.put("crr", autoDetectedValue6);
        }
        if (!params.containsKey("cnt") && (autoDetectedValue5 = locationManager.getCountry()) != null && !autoDetectedValue5.equals("")) {
            params.put("cnt", autoDetectedValue5);
        }
        if (!params.containsKey("cty") && (autoDetectedValue4 = locationManager.getCity()) != null && !autoDetectedValue4.equals("")) {
            params.put("cty", autoDetectedValue4);
        }
        if (!params.containsKey("stt") && (autoDetectedValue3 = locationManager.getState()) != null && !autoDetectedValue3.equals("")) {
            params.put("stt", autoDetectedValue3);
        }
        if (!params.containsKey("zip") && (autoDetectedValue2 = locationManager.getZipCode()) != null && !autoDetectedValue2.equals("")) {
            params.put("zip", autoDetectedValue2);
        }
        if (!params.containsKey("did") && (autoDetectedValue = deviceManager.getDeviceId()) != null && !autoDetectedValue.equals("")) {
            params.put("did", autoDetectedValue);
        }
        if (!deviceManager.isPermissionGranted("android.permission.ACCESS_NETWORK_STATE")) {
            OXMPermissionDeniedForApplication permDenied2 = new OXMPermissionDeniedForApplication("android.permission.ACCESS_NETWORK_STATE");
            OXMUtils.log(this, permDenied2.getMessage());
            if (getAsyncCallbacksListener() != null) {
                getAsyncCallbacksListener().adModelNonCriticalError(permDenied2);
            }
        } else if (!params.containsKey("net")) {
            OXMAdCallParams.OXMConnectionType autoDetectedValue9 = networkManager.getConnectionType();
            if (autoDetectedValue9 == OXMAdCallParams.OXMConnectionType.WIFI) {
                params.put("net", "wifi");
            } else if (autoDetectedValue9 == OXMAdCallParams.OXMConnectionType.CELL) {
                params.put("net", "cell");
            }
        }
    }
}
