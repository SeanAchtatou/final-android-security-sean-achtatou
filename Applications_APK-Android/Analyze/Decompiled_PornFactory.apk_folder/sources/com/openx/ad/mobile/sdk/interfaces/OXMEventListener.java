package com.openx.ad.mobile.sdk.interfaces;

import com.openx.ad.mobile.sdk.OXMEvent;

public interface OXMEventListener {
    void onPerform(OXMEvent oXMEvent);
}
