package com.openx.ad.mobile.sdk.interfaces;

import com.openx.ad.mobile.sdk.errors.OXMError;

public interface OXMAdModelEventsListener {
    void adModelLoadAdFail(OXMError oXMError);

    void adModelLoadAdSuccess(OXMAdGroup oXMAdGroup);

    void adModelNonCriticalError(OXMError oXMError);
}
