package com.openx.ad.mobile.sdk;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import com.google.android.gms.drive.DriveFile;
import com.openx.ad.mobile.sdk.OXMEvent;
import com.openx.ad.mobile.sdk.interfaces.OXMBrowserControlsEventsListener;
import com.openx.ad.mobile.sdk.interfaces.OXMEventListener;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;

final class OXMBrowserControls extends TableLayout {
    private Button mBack;
    private OXMBrowserControlsEventsListener mBrowserControlsEventsListener;
    private Button mClose;
    private OXMEventListener mCloseEventsListener;
    private Button mForth;
    private LinearLayout mLeftPart;
    private Button mOpenInExternalBrowser;
    private Button mRefresh;
    private LinearLayout mRightPart;
    private Handler mUIHandler;

    private void setBrowserControlsEventsListener(OXMBrowserControlsEventsListener listener) {
        this.mBrowserControlsEventsListener = listener;
    }

    /* access modifiers changed from: private */
    public OXMBrowserControlsEventsListener getBrowserControlsEventsListener() {
        return this.mBrowserControlsEventsListener;
    }

    private void initUIHandler() {
        this.mUIHandler = new Handler();
    }

    private Handler getUIHandler() {
        return this.mUIHandler;
    }

    private void setCloseEventListener(OXMEventListener closeEventsListener) {
        this.mCloseEventsListener = closeEventsListener;
    }

    private OXMEventListener getCloseEventListener() {
        return this.mCloseEventsListener;
    }

    private void setLeftPart(LinearLayout part) {
        this.mLeftPart = part;
    }

    private LinearLayout getLeftPart() {
        return this.mLeftPart;
    }

    private void setRightPart(LinearLayout part) {
        this.mRightPart = part;
    }

    private LinearLayout getRightPart() {
        return this.mRightPart;
    }

    private void setCloseButton(Button button) {
        this.mClose = button;
    }

    private Button getCloseButton() {
        return this.mClose;
    }

    private void setBackButton(Button button) {
        this.mBack = button;
    }

    /* access modifiers changed from: private */
    public Button getBackButton() {
        return this.mBack;
    }

    private void setForthButton(Button button) {
        this.mForth = button;
    }

    /* access modifiers changed from: private */
    public Button getForthButton() {
        return this.mForth;
    }

    private void setRefreshButton(Button button) {
        this.mRefresh = button;
    }

    private Button getRefreshButton() {
        return this.mRefresh;
    }

    private void setOpenInExternalBrowserButton(Button button) {
        this.mOpenInExternalBrowser = button;
    }

    private Button getOpenInExternalBrowserButton() {
        return this.mOpenInExternalBrowser;
    }

    public OXMBrowserControls(Context context, OXMBrowserControlsEventsListener listener) {
        super(context);
        init(listener);
    }

    public void updateNavigationButtonsState() {
        getUIHandler().post(new Runnable() {
            public void run() {
                if (OXMBrowserControls.this.getBrowserControlsEventsListener() != null) {
                    if (OXMBrowserControls.this.getBrowserControlsEventsListener().canGoBack()) {
                        Drawable icon = OXMUtils.getDrawable("back_active.png");
                        if (icon != null) {
                            OXMBrowserControls.this.getBackButton().setBackgroundDrawable(icon);
                        }
                    } else {
                        Drawable icon2 = OXMUtils.getDrawable("back_inactive.png");
                        if (icon2 != null) {
                            OXMBrowserControls.this.getBackButton().setBackgroundDrawable(icon2);
                        }
                    }
                    if (OXMBrowserControls.this.getBrowserControlsEventsListener().canGoForward()) {
                        Drawable icon3 = OXMUtils.getDrawable("forth_active.png");
                        if (icon3 != null) {
                            OXMBrowserControls.this.getForthButton().setBackgroundDrawable(icon3);
                            return;
                        }
                        return;
                    }
                    Drawable icon4 = OXMUtils.getDrawable("forth_inactive.png");
                    if (icon4 != null) {
                        OXMBrowserControls.this.getForthButton().setBackgroundDrawable(icon4);
                    }
                }
            }
        });
    }

    private void bindEventListeners() {
        getCloseButton().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (OXMBrowserControls.this.getBrowserControlsEventsListener() != null) {
                    OXMBrowserControls.this.getBrowserControlsEventsListener().closeBrowser();
                }
            }
        });
        getBackButton().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (OXMBrowserControls.this.getBrowserControlsEventsListener() != null) {
                    OXMBrowserControls.this.getBrowserControlsEventsListener().onGoBack();
                }
            }
        });
        getForthButton().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (OXMBrowserControls.this.getBrowserControlsEventsListener() != null) {
                    OXMBrowserControls.this.getBrowserControlsEventsListener().onGoForward();
                }
            }
        });
        getRefreshButton().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (OXMBrowserControls.this.getBrowserControlsEventsListener() != null) {
                    OXMBrowserControls.this.getBrowserControlsEventsListener().onRelaod();
                }
            }
        });
        getOpenInExternalBrowserButton().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = null;
                if (OXMBrowserControls.this.getBrowserControlsEventsListener() != null) {
                    url = OXMBrowserControls.this.getBrowserControlsEventsListener().getCurrentURL();
                }
                if (url != null) {
                    OXMUtils.log(this, "Starting external browser with: " + url);
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                    intent.addFlags(DriveFile.MODE_READ_ONLY);
                    try {
                        OXMBrowserControls.this.getContext().startActivity(intent);
                        OXMUtils.log(this, "Browser has started");
                    } catch (ActivityNotFoundException e) {
                        OXMUtils.log(this, "Could not handle intent with URL: " + url + ". Is this intent unsupported on your device?");
                    }
                }
            }
        });
    }

    private void setButtonDefaultSize(Button button) {
        button.setHeight(50);
        button.setWidth(50);
    }

    private void init(OXMBrowserControlsEventsListener listener) {
        initUIHandler();
        setBrowserControlsEventsListener(listener);
        TableRow controlsSet = new TableRow(getContext());
        setLeftPart(new LinearLayout(getContext()));
        setRightPart(new LinearLayout(getContext()));
        getLeftPart().setVisibility(4);
        getRightPart().setGravity(5);
        setBackgroundColor(Color.rgb(43, 47, 50));
        setCloseButton(new Button(getContext()));
        setButtonDefaultSize(getCloseButton());
        Drawable icon = OXMUtils.getDrawable("close.png");
        if (icon != null) {
            getCloseButton().setBackgroundDrawable(icon);
        }
        setBackButton(new Button(getContext()));
        setButtonDefaultSize(getBackButton());
        if (OXMUtils.getDrawable("back_inactive.png") != null) {
            getBackButton().setBackgroundDrawable(OXMUtils.getDrawable("back_inactive.png"));
        }
        setForthButton(new Button(getContext()));
        setButtonDefaultSize(getForthButton());
        Drawable icon2 = OXMUtils.getDrawable("forth_inactive.png");
        if (icon2 != null) {
            getForthButton().setBackgroundDrawable(icon2);
        }
        setRefreshButton(new Button(getContext()));
        setButtonDefaultSize(getRefreshButton());
        Drawable icon3 = OXMUtils.getDrawable("refresh.png");
        if (icon3 != null) {
            getRefreshButton().setBackgroundDrawable(icon3);
        }
        setOpenInExternalBrowserButton(new Button(getContext()));
        setButtonDefaultSize(getOpenInExternalBrowserButton());
        Drawable icon4 = OXMUtils.getDrawable("open_in_browser.png");
        if (icon4 != null) {
            getOpenInExternalBrowserButton().setBackgroundDrawable(icon4);
        }
        bindEventListeners();
        getLeftPart().addView(getBackButton());
        getLeftPart().addView(getForthButton());
        getLeftPart().addView(getRefreshButton());
        getLeftPart().addView(getOpenInExternalBrowserButton());
        getRightPart().addView(getCloseButton());
        controlsSet.addView(getLeftPart(), new TableRow.LayoutParams(-1, -1, 3.0f));
        controlsSet.addView(getRightPart(), new TableRow.LayoutParams(-1, -1, 5.0f));
        addView(controlsSet);
        setCloseEventListener(new OXMEventListener() {
            public void onPerform(OXMEvent event) {
                OXMManagersResolver.getInstance().getEventsManager().unregisterEventListener(OXMEvent.OXMEventType.CLOSE_MODAL_WINDOW, this);
                if (OXMBrowserControls.this.getBrowserControlsEventsListener() != null) {
                    OXMBrowserControls.this.getBrowserControlsEventsListener().setCreatorOfView(event.getArgs());
                    OXMBrowserControls.this.getBrowserControlsEventsListener().closeBrowser();
                }
            }
        });
        OXMManagersResolver.getInstance().getEventsManager().registerEventListener(OXMEvent.OXMEventType.CLOSE_MODAL_WINDOW, getCloseEventListener());
    }

    public void showNavigationControls() {
        getLeftPart().setVisibility(0);
    }

    public void hideNavigationControls() {
        getLeftPart().setVisibility(4);
    }
}
