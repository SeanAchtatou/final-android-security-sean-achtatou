package com.openx.ad.mobile.sdk.interfaces;

public interface OXMDeviceManager {
    String getCarrier();

    String getDeviceId();

    int getDeviceOrientation();

    int getScreenHeight();

    int getScreenWidth();

    boolean isPermissionGranted(String str);
}
