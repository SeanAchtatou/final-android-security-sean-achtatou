package com.openx.ad.mobile.sdk;

import com.google.android.gms.plus.PlusShare;
import com.nth.analytics.android.JsonObjects;
import java.util.Enumeration;
import java.util.Hashtable;

public class OXMAdCallParams {
    private Hashtable<String, String> mCustomParams = new Hashtable<>();
    private boolean mIsSSL;
    private Hashtable<String, String> mParams = new Hashtable<>();

    public enum OXMConnectionType {
        OFFLINE,
        WIFI,
        CELL
    }

    public enum OXMEthnicity {
        AFRICAN_AMERICAN,
        ASIAN,
        HISPANIC,
        WHITE,
        OTHER
    }

    public enum OXMGender {
        MALE,
        FEMALE,
        OTHER
    }

    public enum OXMMaritalStatus {
        SINGLE,
        MARRIED,
        DIVORCED
    }

    public Hashtable<String, String> getParams() {
        return this.mParams;
    }

    public Hashtable<String, String> getCustomParams() {
        return this.mCustomParams;
    }

    public boolean isSSL() {
        return this.mIsSSL;
    }

    public void setSSL(boolean ssl) {
        this.mIsSSL = ssl;
    }

    public void setUserAge(int age) {
        if (this.mParams.containsKey("age")) {
            this.mParams.remove("age");
        }
        this.mParams.put("age", String.valueOf(age));
    }

    public void setUserGender(OXMGender gender) {
        if (this.mParams.containsKey("gen")) {
            this.mParams.remove("gen");
        }
        this.mParams.put("gen", String.valueOf(gender));
    }

    public void setUserMaritalStatus(OXMMaritalStatus maritalStatus) {
        if (this.mParams.containsKey("mar")) {
            this.mParams.remove("mar");
        }
        this.mParams.put("mar", String.valueOf(maritalStatus));
    }

    public void setUserEthnicity(OXMEthnicity ethnicity) {
        if (this.mParams.containsKey("eth")) {
            this.mParams.remove("eth");
        }
        this.mParams.put("eth", String.valueOf(ethnicity));
    }

    public void setUserAnnualIncomeInUs(int annualIncomeInUs) {
        if (this.mParams.containsKey("inc")) {
            this.mParams.remove("inc");
        }
        this.mParams.put("inc", String.valueOf(annualIncomeInUs));
    }

    public void setAppStoreMarketUrl(String appStoreMarketUrl) {
        if (this.mParams.containsKey(PlusShare.KEY_CALL_TO_ACTION_URL)) {
            this.mParams.remove(PlusShare.KEY_CALL_TO_ACTION_URL);
        }
        this.mParams.put(PlusShare.KEY_CALL_TO_ACTION_URL, appStoreMarketUrl);
    }

    public void setDma(String dma) {
        if (this.mParams.containsKey(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MANUFACTURER)) {
            this.mParams.remove(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MANUFACTURER);
        }
        this.mParams.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MANUFACTURER, dma);
    }

    public void setCustomParameters(Hashtable<String, String> params) {
        if (params != null && params.size() != 0) {
            this.mCustomParams.clear();
            Enumeration<String> en = params.keys();
            while (en.hasMoreElements()) {
                String key = en.nextElement();
                if (!key.contains("c.")) {
                    key = "c." + key;
                }
                this.mCustomParams.put(key, params.get(key));
            }
        }
    }

    public void setCustomParam(String key, String value) {
        if (this.mCustomParams.containsKey("c." + key)) {
            this.mCustomParams.remove("c." + key);
        }
        this.mCustomParams.put("c." + key, value);
    }

    public void setCoordinates(double lat, double lon) {
        if (this.mParams.containsKey("lat")) {
            this.mParams.remove("lat");
        }
        this.mParams.put("lat", String.valueOf(lat));
        if (this.mParams.containsKey("lon")) {
            this.mParams.remove("lon");
        }
        this.mParams.put("lon", String.valueOf(lon));
    }

    public void setDeviceCarrier(String carrier) {
        if (this.mParams.containsKey("crr")) {
            this.mParams.remove("crr");
        }
        this.mParams.put("crr", carrier);
    }

    public void setCountry(String country) {
        if (this.mParams.containsKey("cnt")) {
            this.mParams.remove("cnt");
        }
        this.mParams.put("cnt", country);
    }

    public void setCity(String city) {
        if (this.mParams.containsKey("cty")) {
            this.mParams.remove("cty");
        }
        this.mParams.put("cty", city);
    }

    public void setState(String state) {
        if (this.mParams.containsKey("stt")) {
            this.mParams.remove("stt");
        }
        this.mParams.put("stt", state);
    }

    public void setZipCode(String zipCode) {
        if (this.mParams.containsKey("zip")) {
            this.mParams.remove("zip");
        }
        this.mParams.put("zip", zipCode);
    }

    public void setDeviceId(String deviceId) {
        if (this.mParams.containsKey("did")) {
            this.mParams.remove("did");
        }
        this.mParams.put("did", deviceId);
    }

    public void setUserId(String userId) {
        if (this.mParams.containsKey("xid")) {
            this.mParams.remove("xid");
        }
        this.mParams.put("xid", userId);
    }

    public void setConnectionType(OXMConnectionType connectionType) {
        if (this.mParams.containsKey("net")) {
            this.mParams.remove("net");
        }
        this.mParams.put("net", String.valueOf(connectionType));
    }
}
