package com.openx.ad.mobile.sdk.interfaces;

public interface OXMBrowserControlsEventsListener {
    boolean canGoBack();

    boolean canGoForward();

    void closeBrowser();

    String getCurrentURL();

    void onGoBack();

    void onGoForward();

    void onRelaod();

    void setCreatorOfView(Object obj);
}
