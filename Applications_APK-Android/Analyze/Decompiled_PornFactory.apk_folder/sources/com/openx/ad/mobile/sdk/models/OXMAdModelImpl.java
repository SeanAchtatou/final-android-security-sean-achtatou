package com.openx.ad.mobile.sdk.models;

import com.openx.ad.mobile.sdk.OXMAdCallParams;
import com.openx.ad.mobile.sdk.OXMUtils;
import com.openx.ad.mobile.sdk.errors.OXMDeviceIsOffline;
import com.openx.ad.mobile.sdk.errors.OXMPermissionDeniedForApplication;
import com.openx.ad.mobile.sdk.errors.OXMUnknownError;
import com.openx.ad.mobile.sdk.interfaces.OXMAdModel;
import com.openx.ad.mobile.sdk.interfaces.OXMAdModelEventsListener;
import com.openx.ad.mobile.sdk.interfaces.OXMNetworkManager;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;
import java.io.Serializable;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

class OXMAdModelImpl implements OXMAdModel, Serializable {
    private static final long serialVersionUID = 1;
    private transient OXMAdCallParams mAdCallParams;
    private String mDomain;
    private transient OXMAdDownloadTask mDownloadTask = null;
    private boolean mIsGroupIds;
    private String mLandscapeId;
    private transient OXMAdModelEventsListener mModelEventsListener;
    private String mPortraitId;

    OXMAdModelImpl() {
    }

    public void processData() {
        OXMNetworkManager networkManager = OXMManagersResolver.getInstance().getNetworkManager();
        if (!OXMManagersResolver.getInstance().getDeviceManager().isPermissionGranted("android.permission.INTERNET")) {
            OXMPermissionDeniedForApplication permDenied = new OXMPermissionDeniedForApplication("android.permission.INTERNET");
            OXMUtils.log(this, permDenied.getMessage());
            if (getAsyncCallbacksListener() != null) {
                getAsyncCallbacksListener().adModelLoadAdFail(permDenied);
            }
        } else if (networkManager.getConnectionType() != OXMAdCallParams.OXMConnectionType.OFFLINE) {
            OXMUtils.log(this, "Create async task");
            setCurrentDownloadTask(new OXMAdDownloadTask(getAsyncCallbacksListener()));
            OXMUtils.log(this, "Generating URL");
            String generatedUrl = new OXMAdCallParamsWrapper(getAdCallParams(), getAdDomain(), getPortraitId(), getLandscapeId(), isGroupIds(), getAsyncCallbacksListener()).generateURL();
            if (generatedUrl == null || generatedUrl.equals("")) {
                OXMUtils.log(this, "Error. URL was not generated");
                return;
            }
            OXMUtils.log(this, "URL was successfully generated: " + generatedUrl);
            getCurrentDownloadTask().execute(generatedUrl);
        } else {
            OXMDeviceIsOffline deviceOffline = new OXMDeviceIsOffline();
            OXMUtils.log(this, deviceOffline.getMessage());
            if (getAsyncCallbacksListener() != null) {
                getAsyncCallbacksListener().adModelLoadAdFail(deviceOffline);
            }
        }
    }

    public void trackEvent(String name, String url) {
        try {
            new DefaultHttpClient().execute(new HttpGet(url));
        } catch (Exception e) {
            OXMUnknownError err = new OXMUnknownError(e.getMessage());
            if (getAsyncCallbacksListener() != null) {
                getAsyncCallbacksListener().adModelLoadAdFail(err);
            }
        }
    }

    private void setCurrentDownloadTask(OXMAdDownloadTask task) {
        this.mDownloadTask = task;
    }

    private OXMAdDownloadTask getCurrentDownloadTask() {
        return this.mDownloadTask;
    }

    private OXMAdModelEventsListener getAsyncCallbacksListener() {
        return this.mModelEventsListener;
    }

    private String getAdDomain() {
        return this.mDomain;
    }

    public void setAsyncCallbacksListener(OXMAdModelEventsListener listener) {
        this.mModelEventsListener = listener;
    }

    public void setAdCallParams(OXMAdCallParams clParams) {
        this.mAdCallParams = clParams;
    }

    public void setPortraitId(String id) {
        this.mPortraitId = id;
    }

    public void setLandscapeId(String id) {
        this.mLandscapeId = id;
    }

    public void setIsGroupIds(boolean isGroupIds) {
        this.mIsGroupIds = isGroupIds;
    }

    public String getPortraitId() {
        return this.mPortraitId;
    }

    public String getLandscapeId() {
        return this.mLandscapeId;
    }

    public boolean isGroupIds() {
        return this.mIsGroupIds;
    }

    public void setAdDomain(String domain) {
        this.mDomain = domain;
    }

    public OXMAdCallParams getAdCallParams() {
        return this.mAdCallParams;
    }

    public void stopLoading() {
        if (getCurrentDownloadTask() != null) {
            getCurrentDownloadTask().cancel(true);
            setCurrentDownloadTask(null);
        }
    }

    public boolean isLoaded() {
        if (getCurrentDownloadTask() != null) {
            return getCurrentDownloadTask().isLoaded();
        }
        return true;
    }
}
