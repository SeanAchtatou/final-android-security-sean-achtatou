package com.openx.ad.mobile.sdk.controllers;

import android.content.Context;
import android.os.Build;
import com.openx.ad.mobile.sdk.OXMAdCallParams;
import com.openx.ad.mobile.sdk.OXMEvent;
import com.openx.ad.mobile.sdk.OXMUtils;
import com.openx.ad.mobile.sdk.errors.OXMAndroidSDKVersionNotSupported;
import com.openx.ad.mobile.sdk.errors.OXMError;
import com.openx.ad.mobile.sdk.interfaces.OXMAdBannerView;
import com.openx.ad.mobile.sdk.interfaces.OXMAdViewEventsListener;
import com.openx.ad.mobile.sdk.interfaces.OXMControllersManager;
import com.openx.ad.mobile.sdk.interfaces.OXMEventListener;
import com.openx.ad.mobile.sdk.interfaces.OXMEventsManager;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;
import java.util.Hashtable;

public abstract class OXMAdBaseController {
    private OXMEventListener mActionHasBeganListener;
    private OXMEventListener mActionHasDoneListener;
    private boolean mActionInProgress;
    private OXMAdControllerCore mAdControllerCore;
    private Context mContext;
    private boolean mDisposed;
    private OXMEventListener mModalWindowClosedListener;

    public enum OXMAdControllerType {
        INLINE,
        INTERSTITIAL
    }

    public abstract OXMAdControllerType getControllerType();

    /* access modifiers changed from: protected */
    public abstract void onModalWindowClosing(OXMEvent oXMEvent);

    public OXMAdBaseController(Context context, String domain) throws OXMAndroidSDKVersionNotSupported {
        if (!(Build.VERSION.SDK_INT >= 7)) {
            throw new OXMAndroidSDKVersionNotSupported();
        }
        initCore(domain);
        OXMEventsManager eventsManager = OXMManagersResolver.getInstance().getEventsManager();
        OXMControllersManager controllersManager = OXMManagersResolver.getInstance().getControllersManager();
        OXMEventListener modalWindowClosedListener = new OXMEventListener() {
            public void onPerform(OXMEvent event) {
                if (OXMAdBaseController.this.isCurrentControllerEvent(event)) {
                    OXMAdBaseController.this.onModalWindowClosing(event);
                }
            }
        };
        setModalWindowClosedListener(modalWindowClosedListener);
        eventsManager.registerEventListener(OXMEvent.OXMEventType.MODAL_WINDOW_CLOSED, modalWindowClosedListener);
        OXMEventListener actionHasBeganListener = new OXMEventListener() {
            public void onPerform(OXMEvent event) {
                if (event.getArgs() == OXMAdBaseController.this) {
                    OXMUtils.log(OXMAdBaseController.this, "Action in progress");
                    OXMAdBaseController.this.setActionInProgress(true);
                }
            }
        };
        setActionHasBeganListener(actionHasBeganListener);
        eventsManager.registerEventListener(OXMEvent.OXMEventType.ACTION_HAS_BEGAN, actionHasBeganListener);
        OXMEventListener actionHasDoneListener = new OXMEventListener() {
            public void onPerform(OXMEvent event) {
                if (event.getArgs() == OXMAdBaseController.this) {
                    OXMUtils.log(OXMAdBaseController.this, "Action has done");
                    OXMAdBaseController.this.setActionInProgress(false);
                    if (OXMAdBaseController.this.getCore().getAdControllerEventsListener() != null) {
                        OXMAdBaseController.this.getCore().getAdControllerEventsListener().adControllerActionDidFinish(OXMAdBaseController.this);
                    }
                }
            }
        };
        setActionHasDoneListener(actionHasDoneListener);
        eventsManager.registerEventListener(OXMEvent.OXMEventType.ACTION_HAS_DONE, actionHasDoneListener);
        setContext(context);
        controllersManager.registerController(Integer.valueOf(System.identityHashCode(this)), this);
    }

    private void initCore(String domain) {
        this.mAdControllerCore = new OXMAdControllerCore(this, domain);
    }

    /* access modifiers changed from: protected */
    public OXMAdControllerCore getCore() {
        return this.mAdControllerCore;
    }

    /* access modifiers changed from: private */
    public boolean isCurrentControllerEvent(OXMEvent event) {
        boolean hasArgs;
        int controllerUID;
        if (event.getArgs() == null || !(event.getArgs() instanceof Hashtable)) {
            hasArgs = false;
        } else {
            hasArgs = true;
        }
        if (!hasArgs || !((Hashtable) event.getArgs()).containsKey("ControllerUID")) {
            controllerUID = -1;
        } else {
            controllerUID = ((Integer) ((Hashtable) event.getArgs()).get("ControllerUID")).intValue();
        }
        int controllerUIDThis = System.identityHashCode(this);
        if (controllerUID == -1 || controllerUID != controllerUIDThis) {
            return false;
        }
        return true;
    }

    private void setActionHasBeganListener(OXMEventListener listener) {
        this.mActionHasBeganListener = listener;
    }

    private OXMEventListener getActionHasBeganListener() {
        return this.mActionHasBeganListener;
    }

    private void setActionHasDoneListener(OXMEventListener listener) {
        this.mActionHasDoneListener = listener;
    }

    private OXMEventListener getActionHasDoneListener() {
        return this.mActionHasDoneListener;
    }

    public boolean isDisposed() {
        return this.mDisposed;
    }

    private void setIsDisposed(boolean isDisposed) {
        this.mDisposed = isDisposed;
    }

    private void setContext(Context context) {
        this.mContext = context;
    }

    public void queueUIThreadTask(Runnable task) {
        getCore().queueUIThreadTask(task);
    }

    public Context getContext() {
        return this.mContext;
    }

    public void bindToView(OXMAdBannerView view) {
        view.setAdViewEventsListener(new OXMAdViewEventsListener() {
            public boolean adViewActionShouldBegin(String name, boolean willLeaveApp) {
                if (OXMAdBaseController.this.getCore().getAdControllerEventsListener() != null) {
                    return OXMAdBaseController.this.getCore().getAdControllerEventsListener().adControllerActionShouldBegin(OXMAdBaseController.this, willLeaveApp);
                }
                return true;
            }

            public void adViewActionDidFinish(String name) {
                if (OXMAdBaseController.this.getCore().getAdControllerEventsListener() != null) {
                    OXMAdBaseController.this.getCore().getAdControllerEventsListener().adControllerActionDidFinish(OXMAdBaseController.this);
                }
            }

            public void adViewOnTrackingEvent(String name, String url) {
                OXMAdBaseController.this.getCore().trackEvent(name, url);
            }

            public void adViewActionUnableToBegin(String name) {
                if (OXMAdBaseController.this.getCore().getAdControllerEventsListener() != null) {
                    OXMAdBaseController.this.getCore().getAdControllerEventsListener().adControllerActionUnableToBegin(OXMAdBaseController.this);
                }
            }

            public void adViewNonCriticalError(OXMError err) {
                if (OXMAdBaseController.this.getCore().getAdControllerEventsListener() != null) {
                    OXMAdBaseController.this.getCore().getAdControllerEventsListener().adControllerDidFailWithNonCriticalError(OXMAdBaseController.this, err);
                }
            }

            public void adViewResumeReloading() {
                OXMAdBaseController.this.getCore().resumeLoading();
            }

            public void adViewDidLoad() {
                if (OXMAdBaseController.this.getCore().getAdBannerViews() != null) {
                    synchronized (OXMAdBaseController.this.getCore().getAdBannerViews()) {
                        OXMAdBaseController.this.getCore().setCountOfLoadedViews(OXMAdBaseController.this.getCore().getCountOfLoadedViews() + 1);
                        if (OXMAdBaseController.this.getCore().getAdBannerViews().size() == OXMAdBaseController.this.getCore().getCountOfLoadedViews() && OXMAdBaseController.this.getCore().getAdControllerEventsListener() != null) {
                            OXMAdBaseController.this.getCore().getAdControllerEventsListener().adControllerDidLoadAd(OXMAdBaseController.this);
                            OXMAdBaseController.this.getCore().setCountOfLoadedViews(0);
                        }
                    }
                }
            }

            public void adViewOnRemove(OXMAdBannerView view) {
                OXMAdBaseController.this.getCore().removeAdBannerView(view);
            }
        });
    }

    public void removeAllAdBannerViews() {
        getCore().removeAllAdBannerViews();
    }

    public void startLoading() {
        getCore().startLoading();
    }

    public void stopLoading() {
        getCore().stopLoading();
    }

    public void setAdCallParams(OXMAdCallParams clParams) {
        getCore().setAdCallParams(clParams);
    }

    public OXMAdCallParams getAdCallParams() {
        return getCore().getAdCallParams();
    }

    public boolean isLoaded() {
        return getCore().isLoaded();
    }

    /* access modifiers changed from: private */
    public void setActionInProgress(boolean actionInProgress) {
        this.mActionInProgress = actionInProgress;
    }

    public boolean isActionInProgress() {
        return this.mActionInProgress;
    }

    private void setModalWindowClosedListener(OXMEventListener listener) {
        this.mModalWindowClosedListener = listener;
    }

    private OXMEventListener getModalWindowClosedListener() {
        return this.mModalWindowClosedListener;
    }

    public void dispose() {
        setIsDisposed(true);
        OXMEventsManager eventsManager = OXMManagersResolver.getInstance().getEventsManager();
        eventsManager.unregisterEventListener(OXMEvent.OXMEventType.ACTION_HAS_BEGAN, getActionHasBeganListener());
        eventsManager.unregisterEventListener(OXMEvent.OXMEventType.ACTION_HAS_DONE, getActionHasDoneListener());
        eventsManager.unregisterEventListener(OXMEvent.OXMEventType.MODAL_WINDOW_CLOSED, getModalWindowClosedListener());
        stopLoading();
        getCore().dispose();
        OXMManagersResolver.getInstance().getControllersManager().unregisterController(Integer.valueOf(System.identityHashCode(this)));
    }
}
