package com.openx.ad.mobile.sdk.errors;

public class OXMAndroidSDKVersionNotSupported extends OXMError {
    private static final long serialVersionUID = 3271306888549715099L;

    public OXMAndroidSDKVersionNotSupported() {
        super.setMessage("OpenX SDK does not support of this Android firmware version. Android firmware should be 2.1 or above.");
    }
}
