package com.openx.ad.mobile.sdk.models;

import com.openx.ad.mobile.sdk.interfaces.OXMAdModel;

public class OXMAdModelFactory {
    private OXMAdModelFactory() {
    }

    /* synthetic */ OXMAdModelFactory(OXMAdModelFactory oXMAdModelFactory) {
        this();
    }

    private static class OXMAdModelFactoryHolder {
        public static final OXMAdModelFactory instance = new OXMAdModelFactory(null);

        private OXMAdModelFactoryHolder() {
        }
    }

    public static OXMAdModelFactory getInstance() {
        return OXMAdModelFactoryHolder.instance;
    }

    public OXMAdModel createNewModel() {
        return new OXMAdModelImpl();
    }
}
