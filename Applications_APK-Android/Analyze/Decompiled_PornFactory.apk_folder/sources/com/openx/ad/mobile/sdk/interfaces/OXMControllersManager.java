package com.openx.ad.mobile.sdk.interfaces;

import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;

public interface OXMControllersManager {
    OXMAdBaseController getController(Integer num);

    void registerController(Integer num, OXMAdBaseController oXMAdBaseController);

    void unregisterController(Integer num);
}
