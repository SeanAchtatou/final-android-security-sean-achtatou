package com.openx.ad.mobile.sdk.models;

import android.os.AsyncTask;
import com.openx.ad.mobile.sdk.OXMUtils;
import com.openx.ad.mobile.sdk.errors.OXMServerWrongResponse;
import com.openx.ad.mobile.sdk.errors.OXMServerWrongStatusCode;
import com.openx.ad.mobile.sdk.errors.OXMUnknownError;
import com.openx.ad.mobile.sdk.interfaces.OXMAdModelEventsListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;

class OXMAdDownloadTask extends AsyncTask<String, Void, String> {
    private transient DefaultHttpClient mDefHTTPClient = null;
    private boolean mIsLoaded;
    private transient OXMAdModelEventsListener mModelEventsListener;

    public OXMAdDownloadTask(OXMAdModelEventsListener listener) {
        setAsyncCallbacksListener(listener);
    }

    private OXMAdModelEventsListener getAsyncCallbacksListener() {
        return this.mModelEventsListener;
    }

    private void setAsyncCallbacksListener(OXMAdModelEventsListener listener) {
        this.mModelEventsListener = listener;
    }

    private void setIsLoaded(boolean isLoaded) {
        this.mIsLoaded = isLoaded;
    }

    private DefaultHttpClient getThreadSafeClient() {
        DefaultHttpClient client = new DefaultHttpClient();
        ClientConnectionManager mgr = client.getConnectionManager();
        HttpParams params = client.getParams();
        return new DefaultHttpClient(new ThreadSafeClientConnManager(params, mgr.getSchemeRegistry()), params);
    }

    private void setHTTPClient(DefaultHttpClient client) {
        if (this.mDefHTTPClient != client) {
            this.mDefHTTPClient = client;
        }
    }

    private DefaultHttpClient getHTTPClient() {
        if (this.mDefHTTPClient == null) {
            this.mDefHTTPClient = getThreadSafeClient();
        }
        return this.mDefHTTPClient;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        setIsLoaded(true);
        if (getHTTPClient() != null) {
            getHTTPClient().getConnectionManager().shutdown();
            setHTTPClient(null);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        setIsLoaded(false);
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... urls) {
        StringBuilder response = new StringBuilder();
        HttpGet httpGet = new HttpGet(urls[0]);
        try {
            OXMUtils.log(this, "Trying to establish connection");
            HttpResponse httpResponse = getHTTPClient().execute(httpGet);
            int status = httpResponse.getStatusLine().getStatusCode();
            if (status == 200) {
                OXMUtils.log(this, "Trying to obtain data");
                BufferedReader buffer = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
                while (true) {
                    String line = buffer.readLine();
                    if (line == null) {
                        break;
                    }
                    response.append(line);
                }
                OXMUtils.log(this, "Data obtaining complete");
                return response.toString();
            }
            OXMServerWrongStatusCode serverError = new OXMServerWrongStatusCode(status);
            OXMUtils.log(this, serverError.getMessage());
            if (getAsyncCallbacksListener() != null) {
                getAsyncCallbacksListener().adModelLoadAdFail(serverError);
            }
            return response.toString();
        } catch (Exception e) {
            OXMUnknownError err = new OXMUnknownError(e.getMessage());
            OXMUtils.log(this, err.getMessage());
            if (getAsyncCallbacksListener() != null) {
                getAsyncCallbacksListener().adModelLoadAdFail(err);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        OXMUtils.log(this, "Data post processing");
        if (!isLoaded()) {
            setIsLoaded(true);
            OXMUtils.log(this, "Parsing of data / Performing async callback");
            if (getAsyncCallbacksListener() != null) {
                OXMAdGroupImpl adGroup = new OXMAdGroupImpl();
                adGroup.parse(result);
                if (result.equals("") || adGroup.hasParseError()) {
                    OXMServerWrongResponse err = new OXMServerWrongResponse();
                    OXMUtils.log(this, err.getMessage());
                    getAsyncCallbacksListener().adModelLoadAdFail(err);
                    return;
                }
                getAsyncCallbacksListener().adModelLoadAdSuccess(adGroup);
                return;
            }
            OXMUtils.log(this, "Async callback listener not found. Parsing of data has been canceled.");
            return;
        }
        OXMUtils.log(this, "Loading has been canceled");
    }

    public boolean isLoaded() {
        return this.mIsLoaded;
    }
}
