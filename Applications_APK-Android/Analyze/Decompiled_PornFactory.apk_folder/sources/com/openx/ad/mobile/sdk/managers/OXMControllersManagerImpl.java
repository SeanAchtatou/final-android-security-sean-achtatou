package com.openx.ad.mobile.sdk.managers;

import android.content.Context;
import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;
import com.openx.ad.mobile.sdk.interfaces.OXMControllersManager;
import java.util.Enumeration;
import java.util.Hashtable;

class OXMControllersManagerImpl extends OXMBaseManager implements OXMControllersManager {
    private Hashtable<Integer, OXMAdBaseController> mControllers = new Hashtable<>();

    OXMControllersManagerImpl() {
    }

    public void init(Context context) {
        super.init(context);
        if (super.isInit()) {
            setControllers(new Hashtable());
        }
    }

    private void setControllers(Hashtable<Integer, OXMAdBaseController> controlles) {
        this.mControllers = controlles;
    }

    private Hashtable<Integer, OXMAdBaseController> getControllers() {
        return this.mControllers;
    }

    public void registerController(Integer key, OXMAdBaseController controller) {
        if (getControllers().containsKey(key)) {
            getControllers().remove(key);
        }
        getControllers().put(key, controller);
    }

    public void unregisterController(Integer key) {
        if (getControllers().containsKey(key)) {
            getControllers().remove(key);
        }
    }

    public OXMAdBaseController getController(Integer key) {
        if (getControllers().containsKey(key)) {
            return getControllers().get(key);
        }
        return null;
    }

    public void dispose() {
        super.dispose();
        Enumeration<OXMAdBaseController> en = getControllers().elements();
        while (en.hasMoreElements()) {
            en.nextElement().dispose();
        }
        if (getControllers().size() > 0) {
            getControllers().clear();
        }
    }
}
