package com.openx.ad.mobile.sdk.views;

import android.content.Context;
import com.openx.ad.mobile.sdk.interfaces.OXMAdBannerView;

public class OXMAdViewFactory {

    public enum OXMViewOrientation {
        PORTRAIT,
        LANDSCAPE,
        BOTH,
        UNDEFINED
    }

    private OXMAdViewFactory() {
    }

    /* synthetic */ OXMAdViewFactory(OXMAdViewFactory oXMAdViewFactory) {
        this();
    }

    private static class OXMAdViewFactoryHolder {
        public static final OXMAdViewFactory instance = new OXMAdViewFactory(null);

        private OXMAdViewFactoryHolder() {
        }
    }

    public static OXMAdViewFactory getInstance() {
        return OXMAdViewFactoryHolder.instance;
    }

    public OXMAdBannerView createNewView(Context context, String placementType, Object creator, OXMViewOrientation orientation) {
        return new OXMAdBannerViewImpl(context, placementType, creator, orientation);
    }
}
