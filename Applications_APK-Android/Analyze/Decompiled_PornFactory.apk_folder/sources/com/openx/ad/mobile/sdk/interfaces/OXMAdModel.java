package com.openx.ad.mobile.sdk.interfaces;

import com.openx.ad.mobile.sdk.OXMAdCallParams;

public interface OXMAdModel {
    OXMAdCallParams getAdCallParams();

    String getLandscapeId();

    String getPortraitId();

    boolean isGroupIds();

    boolean isLoaded();

    void processData();

    void setAdCallParams(OXMAdCallParams oXMAdCallParams);

    void setAdDomain(String str);

    void setAsyncCallbacksListener(OXMAdModelEventsListener oXMAdModelEventsListener);

    void setIsGroupIds(boolean z);

    void setLandscapeId(String str);

    void setPortraitId(String str);

    void stopLoading();

    void trackEvent(String str, String str2);
}
