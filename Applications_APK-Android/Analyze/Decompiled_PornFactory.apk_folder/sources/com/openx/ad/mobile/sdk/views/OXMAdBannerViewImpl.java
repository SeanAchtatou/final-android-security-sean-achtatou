package com.openx.ad.mobile.sdk.views;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.PopupWindow;
import com.google.android.gms.drive.DriveFile;
import com.openx.ad.mobile.sdk.OXMAdBrowser;
import com.openx.ad.mobile.sdk.OXMEvent;
import com.openx.ad.mobile.sdk.OXMUtils;
import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;
import com.openx.ad.mobile.sdk.errors.OXMUnknownError;
import com.openx.ad.mobile.sdk.interfaces.OXMAd;
import com.openx.ad.mobile.sdk.interfaces.OXMAdBannerView;
import com.openx.ad.mobile.sdk.interfaces.OXMAdCreative;
import com.openx.ad.mobile.sdk.interfaces.OXMAdTracking;
import com.openx.ad.mobile.sdk.interfaces.OXMAdViewEventsListener;
import com.openx.ad.mobile.sdk.interfaces.OXMEventListener;
import com.openx.ad.mobile.sdk.interfaces.OXMEventsManager;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;
import com.openx.ad.mobile.sdk.views.OXMAdViewFactory;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

class OXMAdBannerViewImpl extends ViewGroup implements OXMAdBannerView {
    private OXMEventListener mActionHasDoneListener;
    private OXMAd mAd;
    private OXMAdViewEventsListener mAdViewEventsListener;
    private OXMJSBridge mBridge;
    private int mCheckViewableInterval = 500;
    private Timer mCheckViewableTimer;
    private OXMAdBaseController mController;
    private Object mCreator;
    private boolean mImpressionTracked;
    private boolean mIsMRAID;
    private String mPlacementType;
    private OXMAdViewFactory.OXMViewOrientation mViewOrientation = OXMAdViewFactory.OXMViewOrientation.UNDEFINED;
    private WebView mWebView;

    private void setViewOrientation(OXMAdViewFactory.OXMViewOrientation orientation) {
        this.mViewOrientation = orientation;
    }

    public OXMAdViewFactory.OXMViewOrientation getViewOrientation() {
        return this.mViewOrientation;
    }

    private void setCreator(Object creator) {
        this.mCreator = creator;
    }

    public Object getCreator() {
        return this.mCreator;
    }

    private int getCheckViewableInterval() {
        return this.mCheckViewableInterval;
    }

    /* access modifiers changed from: private */
    public OXMAdCreative getFirstCreative() {
        if (getAd() == null || getAd().getCreatives() == null || getAd().getCreatives().size() <= 0) {
            return null;
        }
        return getAd().getCreatives().get(0);
    }

    private void setPlacementType(String placementType) {
        this.mPlacementType = placementType;
    }

    /* access modifiers changed from: private */
    public String getPlacementType() {
        return this.mPlacementType;
    }

    private void setJSBridge(OXMJSBridge bridge) {
        this.mBridge = bridge;
    }

    /* access modifiers changed from: private */
    public OXMJSBridge getJSBridge() {
        return this.mBridge;
    }

    private void setWebView(WebView webView) {
        this.mWebView = webView;
    }

    /* access modifiers changed from: private */
    public WebView getWebView() {
        return this.mWebView;
    }

    private void setCheckViewableTimer(Timer timer) {
        this.mCheckViewableTimer = timer;
    }

    private Timer getCheckViewableTimer() {
        if (this.mCheckViewableTimer == null) {
            this.mCheckViewableTimer = new Timer();
        }
        return this.mCheckViewableTimer;
    }

    /* access modifiers changed from: private */
    public void startCheckingViewableTimer() {
        if (hasViewableStateChecking()) {
            getJSBridge().clearFlags();
            getCheckViewableTimer().schedule(new TimerTask() {
                public void run() {
                    OXMAdBannerViewImpl.this.getParentController().queueUIThreadTask(new Runnable() {
                        public void run() {
                            OXMAdBannerViewImpl.this.checkViewable();
                        }
                    });
                }
            }, (long) getCheckViewableInterval(), (long) getCheckViewableInterval());
        }
    }

    private boolean hasViewableStateChecking() {
        return getParentController().getControllerType() == OXMAdBaseController.OXMAdControllerType.INLINE;
    }

    /* access modifiers changed from: private */
    public void stopCheckingViewableTimer() {
        if (hasViewableStateChecking()) {
            if (getCheckViewableTimer() != null) {
                getCheckViewableTimer().cancel();
                getCheckViewableTimer().purge();
            }
            setCheckViewableTimer(null);
        }
    }

    public void isExpanded(Handler handler) {
        if (getJSBridge() != null) {
            getJSBridge().getState(handler);
        } else if (handler != null) {
            Message responseMessage = new Message();
            Bundle bundle = new Bundle();
            bundle.putString("method", "getState");
            bundle.putString("value", "");
            responseMessage.setData(bundle);
            handler.dispatchMessage(responseMessage);
        }
    }

    class OXMJSBridge {
        private String mCurrentState = null;
        private Boolean mCurrentViewable = null;
        private String mExpandProperties = null;
        private boolean mExpandedWithURL;
        private Hashtable<String, Handler> mHandlersQueue = new Hashtable<>();
        private PopupWindow mModalAdWindow;
        private String mURLForExpanding;
        private WebView mWebViewForExpanding;

        public OXMJSBridge() {
            setWebViewForExpanding(new WebView(OXMAdBannerViewImpl.this.getContext()));
            getWebViewForExpanding().setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
        }

        public String getPlacementType() {
            return OXMAdBannerViewImpl.this.getPlacementType();
        }

        public void close() {
            OXMUtils.log(this, "Permission granted");
            OXMAdBannerViewImpl.this.getJSBridge().getState(new Handler() {
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    String state = msg.getData().getString("value");
                    if (!state.equals("loading") && !state.equals("hidden")) {
                        if (state.equals("expanded")) {
                            OXMAdBannerViewImpl.this.getParentController().queueUIThreadTask(new Runnable() {
                                public void run() {
                                    if (OXMJSBridge.this.isModalAdWindowOpened()) {
                                        OXMJSBridge.this.closeModalAdWindow();
                                    }
                                    if (!OXMJSBridge.this.isExpandedWithURL()) {
                                        OXMAdCreative creative = OXMAdBannerViewImpl.this.getFirstCreative();
                                        OXMAdBannerViewImpl.this.getWebView().setLayoutParams(new ViewGroup.LayoutParams(creative.getWidth(), creative.getHeight()));
                                        OXMAdBannerViewImpl.this.addView(OXMAdBannerViewImpl.this.getWebView());
                                    }
                                    OXMAdBannerViewImpl.this.showAd();
                                    OXMJSBridge.this.actionDidFinish();
                                    if (OXMJSBridge.this.getPlacementType().equals("inline") && OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                                        OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewResumeReloading();
                                    }
                                }
                            });
                            OXMJSBridge.this.onStateChange("default");
                            OXMAdBannerViewImpl.this.checkViewable();
                        } else if (state.equals("default")) {
                            OXMJSBridge.this.onStateChange("hidden");
                            OXMAdBannerViewImpl.this.getParentController().queueUIThreadTask(new Runnable() {
                                public void run() {
                                    if (OXMJSBridge.this.getPlacementType().equals("inline")) {
                                        OXMAdBannerViewImpl.this.hideAd();
                                        OXMJSBridge.this.viewBecameInvisible();
                                        return;
                                    }
                                    OXMManagersResolver.getInstance().getEventsManager().fireEvent(new OXMEvent(OXMEvent.OXMEventType.CLOSE_MODAL_WINDOW, OXMAdBannerViewImpl.this.getParentController(), null));
                                }
                            });
                        }
                    }
                }
            });
        }

        public void expand() {
            expand(null);
        }

        public void expand(String url) {
            boolean z = false;
            boolean allowed = true;
            if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                allowed = OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewActionShouldBegin("expandAd", false);
            }
            if (allowed) {
                OXMUtils.log(this, "Permission granted");
                actionHasBegan();
                if (url != null) {
                    z = true;
                }
                setExpandedWithURL(z);
                if (isExpandedWithURL()) {
                    setURLForExpanding(url);
                }
                OXMAdBannerViewImpl.this.getJSBridge().getState(new Handler() {
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        String state = msg.getData().getString("value");
                        if (!state.equals("loading") && !state.equals("hidden") && !state.equals("expanded")) {
                            OXMAdBannerViewImpl.this.getJSBridge().getExpandProperties(new Handler() {
                                public void handleMessage(Message msg) {
                                    super.handleMessage(msg);
                                    OXMJSBridge.this.setExpandProperties(msg.getData().getString("value"));
                                    OXMAdBannerViewImpl.this.getParentController().queueUIThreadTask(new Runnable() {
                                        /* JADX WARNING: Removed duplicated region for block: B:11:0x0044  */
                                        /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
                                        /* JADX WARNING: Removed duplicated region for block: B:16:0x0064  */
                                        /* JADX WARNING: Removed duplicated region for block: B:19:0x008d  */
                                        /* JADX WARNING: Removed duplicated region for block: B:22:0x00f6  */
                                        /* JADX WARNING: Removed duplicated region for block: B:29:0x0167  */
                                        /* JADX WARNING: Removed duplicated region for block: B:35:0x01c3  */
                                        /* JADX WARNING: Removed duplicated region for block: B:40:0x024c  */
                                        /* JADX WARNING: Removed duplicated region for block: B:41:0x0250  */
                                        /* JADX WARNING: Removed duplicated region for block: B:42:0x0253  */
                                        /* JADX WARNING: Removed duplicated region for block: B:43:0x0257  */
                                        /* JADX WARNING: Removed duplicated region for block: B:47:0x02ad  */
                                        /* JADX WARNING: Removed duplicated region for block: B:48:0x02c5  */
                                        /* JADX WARNING: Removed duplicated region for block: B:9:0x0040  */
                                        /* Code decompiled incorrectly, please refer to instructions dump. */
                                        public void run() {
                                            /*
                                                r27 = this;
                                                r9 = 0
                                                r22 = 0
                                                r12 = 0
                                                org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ JSONException -> 0x023e }
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this     // Catch:{ JSONException -> 0x023e }
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this     // Catch:{ JSONException -> 0x023e }
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this     // Catch:{ JSONException -> 0x023e }
                                                java.lang.String r23 = r23.getExpandProperties()     // Catch:{ JSONException -> 0x023e }
                                                r0 = r23
                                                r10.<init>(r0)     // Catch:{ JSONException -> 0x023e }
                                                java.lang.String r23 = "width"
                                                r0 = r23
                                                int r22 = r10.optInt(r0)     // Catch:{ JSONException -> 0x02d2 }
                                                java.lang.String r23 = "height"
                                                r0 = r23
                                                int r12 = r10.optInt(r0)     // Catch:{ JSONException -> 0x02d2 }
                                                r9 = r10
                                            L_0x002e:
                                                com.openx.ad.mobile.sdk.managers.OXMManagersResolver r23 = com.openx.ad.mobile.sdk.managers.OXMManagersResolver.getInstance()
                                                com.openx.ad.mobile.sdk.interfaces.OXMDeviceManager r7 = r23.getDeviceManager()
                                                int r19 = r7.getScreenWidth()
                                                int r18 = r7.getScreenHeight()
                                                if (r22 != 0) goto L_0x024c
                                                r17 = r19
                                            L_0x0042:
                                                if (r12 != 0) goto L_0x0250
                                                r14 = r18
                                            L_0x0046:
                                                if (r9 == 0) goto L_0x0253
                                                java.lang.String r23 = "useCustomClose"
                                                r0 = r23
                                                boolean r20 = r9.optBoolean(r0)
                                            L_0x0050:
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                boolean r23 = r23.isModalAdWindowOpened()
                                                if (r23 == 0) goto L_0x0257
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                r23.closeModalAdWindow()
                                            L_0x0075:
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                com.openx.ad.mobile.sdk.controllers.OXMAdBaseController r23 = r23.getParentController()
                                                if (r23 == 0) goto L_0x00a6
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                com.openx.ad.mobile.sdk.controllers.OXMAdBaseController r23 = r23.getParentController()
                                                r23.stopLoading()
                                            L_0x00a6:
                                                android.widget.FrameLayout r15 = new android.widget.FrameLayout
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                android.content.Context r23 = r23.getContext()
                                                r0 = r23
                                                r15.<init>(r0)
                                                r23 = 140(0x8c, float:1.96E-43)
                                                r24 = 0
                                                r25 = 0
                                                r26 = 0
                                                int r23 = android.graphics.Color.argb(r23, r24, r25, r26)
                                                r0 = r23
                                                r15.setBackgroundColor(r0)
                                                android.widget.FrameLayout$LayoutParams r16 = new android.widget.FrameLayout$LayoutParams
                                                r0 = r16
                                                r1 = r19
                                                r2 = r18
                                                r0.<init>(r1, r2)
                                                r15.setLayoutParams(r16)
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                boolean r23 = r23.isExpandedWithURL()
                                                if (r23 == 0) goto L_0x02ad
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                android.webkit.WebView r21 = r23.getWebViewForExpanding()
                                            L_0x0108:
                                                android.widget.FrameLayout$LayoutParams r4 = new android.widget.FrameLayout$LayoutParams
                                                r23 = 17
                                                r0 = r17
                                                r1 = r23
                                                r4.<init>(r0, r14, r1)
                                                r0 = r21
                                                r15.addView(r0, r4)
                                                if (r20 == 0) goto L_0x012e
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                boolean r23 = r23.isExpandedWithURL()
                                                if (r23 == 0) goto L_0x01af
                                            L_0x012e:
                                                android.widget.Button r5 = new android.widget.Button
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                android.content.Context r23 = r23.getContext()
                                                r0 = r23
                                                r5.<init>(r0)
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                android.webkit.WebView r23 = r23.getWebView()
                                                boolean r23 = r23.canGoBack()
                                                if (r23 == 0) goto L_0x02c5
                                                java.lang.String r23 = "mraid_close.png"
                                                android.graphics.drawable.BitmapDrawable r13 = com.openx.ad.mobile.sdk.OXMUtils.getDrawable(r23)
                                                if (r13 == 0) goto L_0x0172
                                                r5.setBackgroundDrawable(r13)
                                            L_0x0172:
                                                android.widget.FrameLayout$LayoutParams r6 = new android.widget.FrameLayout$LayoutParams
                                                r23 = 50
                                                r24 = 50
                                                r25 = 53
                                                r0 = r23
                                                r1 = r24
                                                r2 = r25
                                                r6.<init>(r0, r1, r2)
                                                r23 = 0
                                                int r24 = r18 / 2
                                                int r25 = r14 / 2
                                                int r24 = r24 - r25
                                                int r25 = r19 / 2
                                                int r26 = r17 / 2
                                                int r25 = r25 - r26
                                                r26 = 0
                                                r0 = r23
                                                r1 = r24
                                                r2 = r25
                                                r3 = r26
                                                r6.setMargins(r0, r1, r2, r3)
                                                r15.addView(r5, r6)
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1$1$1 r23 = new com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1$1$1
                                                r0 = r23
                                                r1 = r27
                                                r0.<init>()
                                                r0 = r23
                                                r5.setOnClickListener(r0)
                                            L_0x01af:
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                boolean r23 = r23.isExpandedWithURL()
                                                if (r23 == 0) goto L_0x01ea
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                android.webkit.WebView r23 = r23.getWebViewForExpanding()
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r24 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r24 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r24 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                java.lang.String r24 = r24.getURLForExpanding()
                                                r23.loadUrl(r24)
                                            L_0x01ea:
                                                android.widget.PopupWindow r11 = new android.widget.PopupWindow
                                                r11.<init>(r15)
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                r0 = r23
                                                r0.setModalAdWindow(r11)
                                                r0 = r19
                                                r11.setWidth(r0)
                                                r0 = r18
                                                r11.setHeight(r0)
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1$1$2 r23 = new com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1$1$2
                                                r0 = r23
                                                r1 = r27
                                                r0.<init>()
                                                r0 = r23
                                                r11.setOnDismissListener(r0)
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                r24 = 17
                                                r25 = 0
                                                r26 = 0
                                                r0 = r23
                                                r1 = r24
                                                r2 = r25
                                                r3 = r26
                                                r11.showAtLocation(r0, r1, r2, r3)
                                                return
                                            L_0x023e:
                                                r8 = move-exception
                                            L_0x023f:
                                                java.lang.String r23 = r8.getMessage()
                                                r0 = r27
                                                r1 = r23
                                                com.openx.ad.mobile.sdk.OXMUtils.log(r0, r1)
                                                goto L_0x002e
                                            L_0x024c:
                                                r17 = r22
                                                goto L_0x0042
                                            L_0x0250:
                                                r14 = r12
                                                goto L_0x0046
                                            L_0x0253:
                                                r20 = 1
                                                goto L_0x0050
                                            L_0x0257:
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                boolean r23 = r23.isExpandedWithURL()
                                                if (r23 != 0) goto L_0x0296
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r24 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r24 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r24 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r24 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                android.webkit.WebView r24 = r24.getWebView()
                                                r23.removeView(r24)
                                            L_0x0296:
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                r23.hideAd()
                                                goto L_0x0075
                                            L_0x02ad:
                                                r0 = r27
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3$1 r0 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.this
                                                r23 = r0
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge$3 r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl$OXMJSBridge r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.this
                                                com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl r23 = com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.this
                                                android.webkit.WebView r21 = r23.getWebView()
                                                goto L_0x0108
                                            L_0x02c5:
                                                java.lang.String r23 = "mraid_close.png"
                                                android.graphics.drawable.BitmapDrawable r13 = com.openx.ad.mobile.sdk.OXMUtils.getDrawable(r23)
                                                if (r13 == 0) goto L_0x0172
                                                r5.setBackgroundDrawable(r13)
                                                goto L_0x0172
                                            L_0x02d2:
                                                r8 = move-exception
                                                r9 = r10
                                                goto L_0x023f
                                            */
                                            throw new UnsupportedOperationException("Method not decompiled: com.openx.ad.mobile.sdk.views.OXMAdBannerViewImpl.OXMJSBridge.AnonymousClass3.AnonymousClass1.AnonymousClass1.run():void");
                                        }
                                    });
                                    OXMJSBridge.this.onStateChange("expanded");
                                }
                            });
                        }
                    }
                });
                return;
            }
            OXMUtils.log(this, "Permission denied by user");
            if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewActionUnableToBegin("expandAd");
            }
        }

        public void open(String url) {
            if (url != null && (url.startsWith("tel:") || url.startsWith("voicemail:") || url.startsWith("sms:") || url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("google.streetview:"))) {
                OXMUtils.log(this, "Check permission to launch external application to process this URL");
                boolean allowed = true;
                if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                    allowed = OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewActionShouldBegin("openExternalApp", true);
                }
                if (allowed) {
                    OXMUtils.log(this, "Permission granted");
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                    intent.addFlags(DriveFile.MODE_READ_ONLY);
                    try {
                        OXMAdBannerViewImpl.this.getContext().startActivity(intent);
                        if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                            OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewActionDidFinish("openExternalApp");
                        }
                    } catch (ActivityNotFoundException e) {
                        if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                            OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewActionUnableToBegin("openExternalApp");
                        }
                        OXMUtils.log(this, "Could not handle intent with URL: " + url + ". Is this intent unsupported on your device?");
                    }
                } else {
                    OXMUtils.log(this, "Permission denied by user");
                    if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                        OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewActionUnableToBegin("openExternalApp");
                    }
                }
            } else if (url == null || (!url.startsWith("http:") && !url.startsWith("https:"))) {
                if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                    OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewActionUnableToBegin("openExternalApp");
                }
                OXMUtils.log(this, "Could not handle intent with URL: " + url + ". Is this intent unsupported on your device?");
            } else {
                showModalAd(OXMAdBannerViewImpl.this.getContext(), url);
            }
        }

        public void javaScriptCallback(String handlerHash, String method, String value) {
            Handler handler = findHandler(handlerHash);
            if (handler != null) {
                Message responseMessage = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("method", method);
                bundle.putString("value", value);
                responseMessage.setData(bundle);
                handler.dispatchMessage(responseMessage);
                removeHandler(handlerHash);
            }
        }

        /* access modifiers changed from: protected */
        public void onError(String message, String action) {
            evaluateJavaScript(String.format("mraid.onError('%1$s', '%2$s');", message, action));
        }

        /* access modifiers changed from: protected */
        public void onReady() {
            evaluateJavaScript("mraid.onReady();");
        }

        /* access modifiers changed from: protected */
        public void onStateChange(String state) {
            if (state != null) {
                if (getCurrentState() == null || !getCurrentState().equals(state)) {
                    setCurrentState(state);
                    evaluateJavaScript(String.format("mraid.onStateChange('%1$s');", state));
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onViewableChange(boolean isViewable) {
            if (getCurrentViewable() == null || getCurrentViewable().booleanValue() != isViewable) {
                setCurrentViewable(Boolean.valueOf(isViewable));
                evaluateJavaScript(String.format("mraid.onViewableChange(%1$b);", Boolean.valueOf(isViewable)));
            }
        }

        /* access modifiers changed from: protected */
        public void getExpandProperties(Handler handler) {
            callJavaScriptMethodWithResult("getExpandProperties", handler);
        }

        /* access modifiers changed from: protected */
        public void getState(Handler handler) {
            callJavaScriptMethodWithResult("getState", handler);
        }

        /* access modifiers changed from: protected */
        public void isViewable(Handler handler) {
            callJavaScriptMethodWithResult("isViewable", handler);
        }

        /* access modifiers changed from: protected */
        public void clearFlags() {
            setCurrentState(null);
            setCurrentViewable(null);
        }

        /* access modifiers changed from: private */
        public void viewBecameInvisible() {
            OXMAdBannerViewImpl.this.stopCheckingViewableTimer();
            OXMAdBannerViewImpl.this.setViewable(false);
        }

        private void actionHasBegan() {
            OXMManagersResolver.getInstance().getEventsManager().fireEvent(new OXMEvent(OXMEvent.OXMEventType.ACTION_HAS_BEGAN, null, OXMAdBannerViewImpl.this.getParentController()));
        }

        /* access modifiers changed from: private */
        public void actionDidFinish() {
            OXMManagersResolver.getInstance().getEventsManager().fireEvent(new OXMEvent(OXMEvent.OXMEventType.ACTION_HAS_DONE, null, OXMAdBannerViewImpl.this.getParentController()));
        }

        /* access modifiers changed from: private */
        public void setExpandProperties(String expandProperties) {
            this.mExpandProperties = expandProperties;
        }

        /* access modifiers changed from: private */
        public String getExpandProperties() {
            return this.mExpandProperties;
        }

        private void setCurrentState(String state) {
            this.mCurrentState = state;
        }

        private String getCurrentState() {
            return this.mCurrentState;
        }

        private void setCurrentViewable(Boolean viewable) {
            this.mCurrentViewable = viewable;
        }

        private Boolean getCurrentViewable() {
            return this.mCurrentViewable;
        }

        /* access modifiers changed from: private */
        public String getURLForExpanding() {
            return this.mURLForExpanding;
        }

        private void setURLForExpanding(String url) {
            this.mURLForExpanding = url;
        }

        /* access modifiers changed from: private */
        public boolean isExpandedWithURL() {
            return this.mExpandedWithURL;
        }

        private void setExpandedWithURL(boolean expandedWithURL) {
            this.mExpandedWithURL = expandedWithURL;
        }

        /* access modifiers changed from: private */
        public WebView getWebViewForExpanding() {
            return this.mWebViewForExpanding;
        }

        public void setWebViewForExpanding(WebView webview) {
            this.mWebViewForExpanding = webview;
        }

        private Hashtable<String, Handler> getHandlersQueue() {
            return this.mHandlersQueue;
        }

        /* access modifiers changed from: private */
        public void setModalAdWindow(PopupWindow popupWindow) {
            this.mModalAdWindow = popupWindow;
        }

        /* access modifiers changed from: private */
        public PopupWindow getModalAdWindow() {
            return this.mModalAdWindow;
        }

        /* access modifiers changed from: private */
        public boolean isModalAdWindowOpened() {
            return getModalAdWindow() != null;
        }

        /* access modifiers changed from: private */
        public void closeModalAdWindow() {
            if (OXMAdBannerViewImpl.this.getJSBridge().isModalAdWindowOpened()) {
                getModalAdWindow().dismiss();
            }
        }

        private void callJavaScriptMethodWithResult(String method, Handler handler) {
            String handlerHash = queueHandler(handler);
            if (handlerHash != null) {
                evaluateJavaScript("jsBridge.javaScriptCallback('" + handlerHash + "', '" + method + "', (function() { var retVal = mraid." + method + "(); if (typeof retVal === 'object') { retVal = JSON.stringify(retVal); } return retVal; })())");
            }
        }

        private void evaluateJavaScript(String script) {
            if (OXMAdBannerViewImpl.this.getWebView() != null) {
                OXMAdBannerViewImpl.this.getWebView().loadUrl("javascript: if (window.mraid) { " + script + " }");
            }
        }

        private String queueHandler(Handler handler) {
            if (handler == null) {
                return null;
            }
            String handlerHash = String.valueOf(System.identityHashCode(handler));
            if (getHandlersQueue().containsKey(handlerHash)) {
                getHandlersQueue().remove(handlerHash);
            }
            getHandlersQueue().put(handlerHash, handler);
            return handlerHash;
        }

        private Handler findHandler(String handlerHash) {
            if (handlerHash == null || handlerHash.equals("") || !getHandlersQueue().containsKey(handlerHash)) {
                return null;
            }
            return getHandlersQueue().get(handlerHash);
        }

        private void removeHandler(String handlerHash) {
            if (handlerHash != null && !handlerHash.equals("") && getHandlersQueue().containsKey(handlerHash)) {
                getHandlersQueue().remove(handlerHash);
            }
        }

        /* access modifiers changed from: private */
        public void showModalAd(final Context context, String url) {
            boolean z = false;
            boolean allowed = true;
            OXMUtils.log(this, "Check permission to process this URL");
            if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                allowed = OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewActionShouldBegin("openBrowser", false);
            }
            if (allowed) {
                OXMUtils.log(this, "Permission granted");
                OXMEventsManager eventsManager = OXMManagersResolver.getInstance().getEventsManager();
                if (url != null) {
                    OXMUtils.log(this, "Show In-App browser");
                    actionHasBegan();
                } else {
                    OXMUtils.log(this, "Starting interstitial Ad");
                }
                eventsManager.fireEvent(new OXMEvent(OXMEvent.OXMEventType.CLOSE_MODAL_WINDOW, OXMAdBannerViewImpl.this.getParentController(), OXMAdBannerViewImpl.this.getCreator()));
                final Intent startBrowserActivity = new Intent(context, OXMAdBrowser.class);
                startBrowserActivity.putExtra("Ad", OXMAdBannerViewImpl.this.getAd());
                if (url == null) {
                    url = "";
                }
                startBrowserActivity.putExtra("AdUrl", url);
                startBrowserActivity.putExtra("ControllerUID", OXMAdBannerViewImpl.this.getParentController() != null ? System.identityHashCode(OXMAdBannerViewImpl.this.getParentController()) : -1);
                if (OXMAdBannerViewImpl.this.getCreator() != null && (OXMAdBannerViewImpl.this.getCreator() instanceof OXMAdBrowser)) {
                    z = true;
                }
                startBrowserActivity.putExtra("LongClosingCycle", z);
                if (OXMAdBannerViewImpl.this.getIsMRAID()) {
                    OXMAdBannerViewImpl.this.getJSBridge().getExpandProperties(new Handler() {
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            boolean useCustomClose = false;
                            try {
                                JSONObject expandProperties = new JSONObject(msg.getData().getString("value"));
                                useCustomClose = expandProperties != null ? expandProperties.optBoolean("useCustomClose") : true;
                            } catch (JSONException e) {
                                OXMUtils.log(this, e.getMessage());
                            }
                            startBrowserActivity.putExtra("HasCutomCloseButton", useCustomClose);
                            OXMJSBridge.this.viewBecameInvisible();
                            context.startActivity(startBrowserActivity);
                        }
                    });
                } else {
                    context.startActivity(startBrowserActivity);
                }
                if (OXMAdBannerViewImpl.this.getParentController() != null) {
                    OXMAdBannerViewImpl.this.getParentController().stopLoading();
                    return;
                }
                return;
            }
            OXMUtils.log(this, "Permission denied by user");
            if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewActionUnableToBegin("openBrowser");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (getAd() == null || getAd().getCreatives() == null || getAd().getCreatives().size() <= 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }
        OXMAdCreative creative = getFirstCreative();
        int widthSpecMode = View.MeasureSpec.getMode(widthMeasureSpec);
        int widthSpecSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int heightSpecMode = View.MeasureSpec.getMode(heightMeasureSpec);
        int heightSpecSize = View.MeasureSpec.getSize(heightMeasureSpec);
        if (widthSpecMode == Integer.MIN_VALUE || widthSpecMode == 0) {
            widthSpecSize = creative.getWidth();
        }
        if (heightSpecMode == Integer.MIN_VALUE || heightSpecMode == 0) {
            heightSpecSize = creative.getHeight();
        }
        setMeasuredDimension(widthSpecSize, heightSpecSize);
    }

    public OXMAdBannerViewImpl(Context context, String placementType, Object creator, OXMAdViewFactory.OXMViewOrientation orientation) {
        super(context);
        setCreator(creator);
        setViewOrientation(orientation);
        setWebView(new WebView(context));
        getWebView().setInitialScale(100);
        getWebView().getSettings().setJavaScriptEnabled(true);
        getWebView().getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        getWebView().getSettings().setPluginsEnabled(false);
        getWebView().setHorizontalScrollBarEnabled(false);
        getWebView().setVerticalScrollBarEnabled(false);
        getWebView().getSettings().setCacheMode(2);
        setJSBridge(new OXMJSBridge());
        getWebView().addJavascriptInterface(getJSBridge(), "jsBridge");
        setPlacementType(placementType);
        getWebView().setWebChromeClient(new WebChromeClient());
        getWebView().setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (OXMAdBannerViewImpl.this.getIsMRAID()) {
                    OXMAdBannerViewImpl.this.getJSBridge().onReady();
                    OXMAdBannerViewImpl.this.startCheckingViewableTimer();
                }
                if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                    OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewDidLoad();
                }
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                String gottenDomain = OXMAdBannerViewImpl.this.getFirstCreative() != null ? OXMAdBannerViewImpl.this.getFirstCreative().getTracking().getClickURL() : "";
                if (gottenDomain != null && !gottenDomain.equals("")) {
                    gottenDomain = gottenDomain.substring(0, gottenDomain.indexOf("?"));
                }
                OXMUtils.log(this, "Opening URL : " + url);
                if (gottenDomain != null && !gottenDomain.equals("") && url != null && !url.equals("") && url.indexOf(gottenDomain) != -1) {
                    try {
                        HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
                        urlConnection.setInstanceFollowRedirects(false);
                        String realUrl = urlConnection.getHeaderField("Location");
                        url = realUrl;
                        if (url != null) {
                            OXMUtils.log(this, "Server redirected to : " + realUrl);
                        } else {
                            OXMUtils.log(this, "Server redirected to incorrect URL, operation canceled");
                        }
                    } catch (Exception e) {
                        OXMUnknownError err = new OXMUnknownError(e.getMessage());
                        OXMUtils.log(this, err.getMessage());
                        if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                            OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewNonCriticalError(err);
                        }
                    }
                } else if (OXMAdBannerViewImpl.this.getAdViewEventsListener() != null) {
                    OXMAdBannerViewImpl.this.getAdViewEventsListener().adViewOnTrackingEvent("click", OXMAdBannerViewImpl.this.getFirstCreative().getTracking().getClickURL());
                }
                if (!(url == null || url.equals("") || OXMAdBannerViewImpl.this.getJSBridge() == null)) {
                    OXMAdBannerViewImpl.this.getJSBridge().open(url);
                }
                return true;
            }
        });
        addView(getWebView());
        OXMEventsManager eventsManager = OXMManagersResolver.getInstance().getEventsManager();
        OXMEventListener actionHasDoneListener = new OXMEventListener() {
            public void onPerform(OXMEvent event) {
                if (event.getArgs() == OXMAdBannerViewImpl.this.getParentController() && OXMAdBannerViewImpl.this.getIsMRAID()) {
                    OXMAdBannerViewImpl.this.startCheckingViewableTimer();
                }
            }
        };
        setActionHasDoneListener(actionHasDoneListener);
        eventsManager.registerEventListener(OXMEvent.OXMEventType.ACTION_HAS_DONE, actionHasDoneListener);
        setBackgroundColor(-1);
    }

    private void setActionHasDoneListener(OXMEventListener listener) {
        this.mActionHasDoneListener = listener;
    }

    private OXMEventListener getActionHasDoneListener() {
        return this.mActionHasDoneListener;
    }

    /* access modifiers changed from: private */
    public void setViewable(final boolean newState) {
        getJSBridge().isViewable(new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (newState != (msg.getData().getString("value").equals("true"))) {
                    OXMAdBannerViewImpl.this.getJSBridge().onViewableChange(newState);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void checkViewable() {
        if (!hasViewableStateChecking()) {
            return;
        }
        if (getVisibility() == 8) {
            setViewable(false);
        } else {
            setViewable(getGlobalVisibleRect(new Rect()));
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean arg0, int arg1, int arg2, int arg3, int arg4) {
        if (getChildCount() > 0) {
            View child = getChildAt(0);
            if (child.getVisibility() != 8) {
                ViewGroup.LayoutParams params = child.getLayoutParams();
                child.layout(0, 0, params.width, params.height);
                if (!getImpressionTracked() && hasViewableStateChecking()) {
                    trackImpressionEventIfNeeded();
                    setImpressionTracked(true);
                }
            }
        }
    }

    private void trackImpressionEventIfNeeded() {
        if (getAdViewEventsListener() != null && getAd() != null) {
            String impressionURL = getFirstCreative() != null ? getFirstCreative().getTracking().getImpressionURL() : "";
            if (getAd().getHTML().indexOf(impressionURL) == -1) {
                getAdViewEventsListener().adViewOnTrackingEvent("impression", impressionURL);
            }
        }
    }

    private void setAd(OXMAd ad) {
        this.mAd = ad;
    }

    public OXMAd getAd() {
        return this.mAd;
    }

    public OXMAdCreative getCreative() {
        if (getAd() == null || getAd().getCreatives() == null || getAd().getCreatives().size() <= 0) {
            return null;
        }
        return getAd().getCreatives().elementAt(0);
    }

    public OXMAdTracking getTracking() {
        OXMAdCreative creative = getCreative();
        if (creative != null) {
            return creative.getTracking();
        }
        return null;
    }

    public void showInterstitialModalAd(Context context) {
        if (getJSBridge() != null) {
            getJSBridge().showModalAd(context, null);
        }
    }

    public void showAd() {
        setVisibility(0);
    }

    public void hideAd() {
        setVisibility(4);
    }

    public void dispose() {
        if (getAdViewEventsListener() != null) {
            getAdViewEventsListener().adViewOnRemove(this);
        }
        if (getJSBridge() != null) {
            getJSBridge().closeModalAdWindow();
        }
        OXMManagersResolver.getInstance().getEventsManager().unregisterEventListener(OXMEvent.OXMEventType.ACTION_HAS_DONE, getActionHasDoneListener());
        if (getAd() != null) {
            getContext().deleteFile(String.valueOf(getAd().getAdId()) + ".html");
        }
        ViewGroup parent = getParentContainer();
        if (parent != null) {
            if (getIsMRAID()) {
                stopCheckingViewableTimer();
            }
            parent.removeView(this);
        }
        getWebView().destroy();
        setWebView(null);
    }

    /* access modifiers changed from: private */
    public OXMAdViewEventsListener getAdViewEventsListener() {
        return this.mAdViewEventsListener;
    }

    public void setAdViewEventsListener(OXMAdViewEventsListener listener) {
        this.mAdViewEventsListener = listener;
    }

    private void setImpressionTracked(boolean impressionTracked) {
        this.mImpressionTracked = impressionTracked;
    }

    private boolean getImpressionTracked() {
        return this.mImpressionTracked;
    }

    private void setIsMRAID(boolean isMRAID) {
        this.mIsMRAID = isMRAID;
    }

    /* access modifiers changed from: private */
    public boolean getIsMRAID() {
        return this.mIsMRAID;
    }

    public void reloadAd(OXMAd ad) {
        if (ad != null) {
            if (getAd() != null) {
                getContext().deleteFile(String.valueOf(getAd().getAdId()) + ".html");
            }
            setImpressionTracked(false);
            setAd(ad);
            if (getIsMRAID()) {
                stopCheckingViewableTimer();
            }
            String adHTML = getAd().getHTML();
            if (adHTML != null && !adHTML.equals("")) {
                setIsMRAID(Pattern.compile("<script\\s+.*mraid[.]{1}js.*>.*</script>", 2).matcher(adHTML).find());
                String script = "";
                if (getIsMRAID()) {
                    script = OXMUtils.loadJavaScriptFile("mraid");
                }
                String adHTML2 = "<html><head><script type='text/javascript'>" + script + "</script><head><body style='margin: 0; padding: 0;'>" + adHTML + "</body></html>";
                FileOutputStream fosHMTL = null;
                String htmlFileName = String.valueOf(getAd().getAdId()) + ".html";
                try {
                    if (getContext().getFileStreamPath(htmlFileName).exists()) {
                        getContext().deleteFile(htmlFileName);
                    }
                    FileOutputStream fosHMTL2 = getContext().openFileOutput(htmlFileName, 0);
                    fosHMTL2.write(adHTML2.getBytes("utf-8"));
                    try {
                        fosHMTL2.close();
                    } catch (IOException e) {
                        OXMUtils.log(this, e.getMessage());
                    }
                } catch (FileNotFoundException e2) {
                    OXMUtils.log(this, e2.getMessage());
                    try {
                        fosHMTL.close();
                    } catch (IOException e3) {
                        OXMUtils.log(this, e3.getMessage());
                    }
                } catch (IOException e4) {
                    OXMUtils.log(this, e4.getMessage());
                    try {
                        fosHMTL.close();
                    } catch (IOException e5) {
                        OXMUtils.log(this, e5.getMessage());
                    }
                } catch (Throwable th) {
                    try {
                        fosHMTL.close();
                    } catch (IOException e6) {
                        OXMUtils.log(this, e6.getMessage());
                    }
                    throw th;
                }
                String filePath = getContext().getFileStreamPath(htmlFileName).getPath();
                getWebView().clearView();
                getWebView().loadUrl("file://" + filePath);
                OXMAdCreative creative = getFirstCreative();
                getWebView().setLayoutParams(new ViewGroup.LayoutParams(creative.getWidth(), creative.getHeight()));
                showAd();
            }
        }
    }

    public void setParentController(OXMAdBaseController controller) {
        this.mController = controller;
    }

    public OXMAdBaseController getParentController() {
        return this.mController;
    }

    private ViewGroup getParentContainer() {
        ViewParent parent = getParent();
        if (parent == null || !(parent instanceof ViewGroup)) {
            return null;
        }
        return (ViewGroup) parent;
    }

    public void presentAdInViewGroup(ViewGroup view) {
        ViewGroup parent = getParentContainer();
        if (parent != view) {
            if (parent != null) {
                parent.removeView(this);
            }
            view.addView(this);
        }
    }

    public void presentAdInViewGroup(ViewGroup view, ViewGroup.LayoutParams params) {
        ViewGroup parent = getParentContainer();
        if (parent != view) {
            if (parent != null) {
                parent.removeView(this);
            }
            view.addView(this, params);
        }
    }
}
