package com.openx.ad.mobile.sdk.interfaces;

import java.io.Serializable;

public interface OXMAdTracking extends Serializable {
    String getClickURL();

    String getImpressionURL();
}
