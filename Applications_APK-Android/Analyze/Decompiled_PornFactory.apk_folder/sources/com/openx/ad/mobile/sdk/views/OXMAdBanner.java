package com.openx.ad.mobile.sdk.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.openx.ad.mobile.sdk.OXMAdCallParams;
import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;
import com.openx.ad.mobile.sdk.controllers.OXMAdController;
import com.openx.ad.mobile.sdk.errors.OXMAndroidSDKVersionNotSupported;
import com.openx.ad.mobile.sdk.errors.OXMError;
import com.openx.ad.mobile.sdk.interfaces.OXMAdBannerView;
import com.openx.ad.mobile.sdk.interfaces.OXMAdControllerEventsListener;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;

public final class OXMAdBanner extends ViewGroup {
    private OXMAdController mAdController;
    private OXMAdControllerEventsListener mExternalListener;
    private OXMAdControllerEventsListener mInternalListener;

    private void setInternalListener(OXMAdControllerEventsListener listener) {
        this.mInternalListener = listener;
    }

    private OXMAdControllerEventsListener getInternalListener() {
        return this.mInternalListener;
    }

    private void setExternalListener(OXMAdControllerEventsListener listener) {
        this.mExternalListener = listener;
    }

    /* access modifiers changed from: private */
    public OXMAdControllerEventsListener getExternalListener() {
        return this.mExternalListener;
    }

    private void setAdController(OXMAdController controller) {
        this.mAdController = controller;
    }

    private OXMAdController getAdController() {
        return this.mAdController;
    }

    public OXMAdBanner(Context context) {
        super(context, null);
    }

    public OXMAdBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
        reflectAttrs(attrs);
    }

    public OXMAdBanner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        reflectAttrs(attrs);
    }

    private String getAttributeValue(AttributeSet attrs, int index, String name) {
        if (attrs.getAttributeName(index).equalsIgnoreCase(name)) {
            return attrs.getAttributeValue(index);
        }
        return null;
    }

    private void reflectAttrs(AttributeSet attrs) {
        long lpid;
        long llid = 0;
        String domain = null;
        String pid = null;
        String lid = null;
        String intervalvalue = null;
        if (attrs != null) {
            int attrsCount = attrs.getAttributeCount();
            for (int i = 0; i < attrsCount; i++) {
                if (domain == null) {
                    domain = getAttributeValue(attrs, i, "domain");
                }
                if (pid == null) {
                    pid = getAttributeValue(attrs, i, "portrait_id");
                }
                if (lid == null) {
                    lid = getAttributeValue(attrs, i, "landscape_id");
                }
                if (intervalvalue == null) {
                    intervalvalue = getAttributeValue(attrs, i, "change_interval");
                }
            }
        }
        if (pid != null) {
            lpid = Long.parseLong(pid);
        } else {
            lpid = 0;
        }
        if (lid != null) {
            llid = Long.parseLong(lid);
        }
        init(domain, lpid, llid, intervalvalue != null ? Integer.parseInt(intervalvalue) : 30);
    }

    private void init(String domain, long pid, long lid, int milliseconds) {
        try {
            setAdController(new OXMAdController(getContext(), domain));
            getAdController().initForAdUnitIds(pid, lid);
            getAdController().setAdChangeInterval(milliseconds);
            setInternalListener(new OXMAdControllerEventsListener() {
                public void adControllerWillLoadAd(OXMAdBaseController controller) {
                    OXMAdBannerView view = ((OXMAdController) controller).getAdBannerView();
                    ((View) view).setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
                    view.presentAdInViewGroup(OXMAdBanner.this);
                    if (OXMAdBanner.this.hasExternalListener()) {
                        OXMAdBanner.this.getExternalListener().adControllerWillLoadAd(controller);
                    }
                }

                public void adControllerDidLoadAd(OXMAdBaseController controller) {
                    if (OXMAdBanner.this.hasExternalListener()) {
                        OXMAdBanner.this.getExternalListener().adControllerDidLoadAd(controller);
                    }
                }

                public void adControllerDidFailWithNonCriticalError(OXMAdBaseController controller, OXMError err) {
                    if (OXMAdBanner.this.hasExternalListener()) {
                        OXMAdBanner.this.getExternalListener().adControllerDidFailWithNonCriticalError(controller, err);
                    }
                }

                public void adControllerDidFailToReceiveAdWithError(OXMAdBaseController controller, Throwable err) {
                    if (OXMAdBanner.this.hasExternalListener()) {
                        OXMAdBanner.this.getExternalListener().adControllerDidFailToReceiveAdWithError(controller, err);
                    }
                }

                public void adControllerActionUnableToBegin(OXMAdBaseController controller) {
                    if (OXMAdBanner.this.hasExternalListener()) {
                        OXMAdBanner.this.getExternalListener().adControllerActionUnableToBegin(controller);
                    }
                }

                public boolean adControllerActionShouldBegin(OXMAdBaseController controller, boolean willLeaveApp) {
                    if (OXMAdBanner.this.hasExternalListener()) {
                        return OXMAdBanner.this.getExternalListener().adControllerActionShouldBegin(controller, willLeaveApp);
                    }
                    return true;
                }

                public void adControllerActionDidFinish(OXMAdBaseController controller) {
                    if (OXMAdBanner.this.hasExternalListener()) {
                        OXMAdBanner.this.getExternalListener().adControllerActionDidFinish(controller);
                    }
                }
            });
            getAdController().setAdControllerEventsListener(getInternalListener());
        } catch (OXMAndroidSDKVersionNotSupported e) {
            e.printStackTrace();
        }
    }

    public void setAdCallParams(OXMAdCallParams clParams) {
        getAdController().setAdCallParams(clParams);
    }

    public void setAdChangeInterval(int adChangeInterval) {
        getAdController().setAdChangeInterval(adChangeInterval);
    }

    public void setAdControllerEventsListener(OXMAdControllerEventsListener listener) {
        setExternalListener(listener);
    }

    public void initForAdUnitIds(long pid, long lid) {
        getAdController().initForAdUnitIds(pid, lid);
    }

    public void startLoading() {
        getAdController().startLoading();
    }

    public void stopLoading() {
        getAdController().stopLoading();
    }

    /* access modifiers changed from: private */
    public boolean hasExternalListener() {
        return getExternalListener() != null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean arg0, int arg1, int arg2, int arg3, int arg4) {
        if (getChildCount() > 0) {
            View child = getChildAt(0);
            if (child.getVisibility() != 8) {
                child.layout(0, 0, child.getMeasuredWidth(), child.getMeasuredHeight());
            }
        }
    }

    private int createWidthMeasureSpec(int value) {
        return createMeasureSpec(value, OXMManagersResolver.getInstance().getDeviceManager().getScreenWidth());
    }

    private int createHeightMeasureSpec(int value) {
        return createMeasureSpec(value, OXMManagersResolver.getInstance().getDeviceManager().getScreenHeight());
    }

    private int createMeasureSpec(int value, int defValue) {
        if (value > 0) {
            return View.MeasureSpec.makeMeasureSpec(value, 1073741824);
        }
        if (value == -1) {
            return View.MeasureSpec.makeMeasureSpec(defValue, 1073741824);
        }
        if (value == -2) {
            return View.MeasureSpec.makeMeasureSpec(0, Integer.MIN_VALUE);
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (getChildCount() > 0) {
            View child = getChildAt(0);
            ViewGroup.LayoutParams params = child.getLayoutParams();
            child.measure(createWidthMeasureSpec(params.width), createHeightMeasureSpec(params.height));
            int childWidth = child.getMeasuredWidth();
            int childHeight = child.getMeasuredHeight();
            int widthSpecMode = View.MeasureSpec.getMode(widthMeasureSpec);
            int widthSpecSize = View.MeasureSpec.getSize(widthMeasureSpec);
            int heightSpecMode = View.MeasureSpec.getMode(heightMeasureSpec);
            int heightSpecSize = View.MeasureSpec.getSize(heightMeasureSpec);
            if (widthSpecMode == Integer.MIN_VALUE || widthSpecMode == 0) {
                widthSpecSize = childWidth;
            }
            if (heightSpecMode == Integer.MIN_VALUE || heightSpecMode == 0) {
                heightSpecSize = childHeight;
            }
            setMeasuredDimension(widthSpecSize, heightSpecSize);
            return;
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void dispose() {
        getAdController().dispose();
    }
}
