package com.openx.ad.mobile.sdk.managers;

import android.content.Context;
import com.openx.ad.mobile.sdk.OXMEvent;
import com.openx.ad.mobile.sdk.interfaces.OXMEventListener;
import com.openx.ad.mobile.sdk.interfaces.OXMEventsManager;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

class OXMEventsManagerImpl extends OXMBaseManager implements OXMEventsManager {
    private Hashtable<OXMEvent.OXMEventType, Vector<OXMEventListener>> mEventListeners = new Hashtable<>();

    OXMEventsManagerImpl() {
    }

    private Hashtable<OXMEvent.OXMEventType, Vector<OXMEventListener>> getEventsListener() {
        return this.mEventListeners;
    }

    public void init(Context context) {
        super.init(context);
    }

    public void registerEventListener(OXMEvent.OXMEventType type, OXMEventListener listener) {
        if (!getEventsListener().containsKey(type)) {
            Vector<OXMEventListener> listeners = new Vector<>();
            listeners.add(listener);
            getEventsListener().put(type, listeners);
            return;
        }
        Vector<OXMEventListener> listeners2 = getEventsListener().get(type);
        if (!listeners2.contains(listener)) {
            listeners2.add(listener);
        }
    }

    public void unregisterEventListener(OXMEvent.OXMEventType type, OXMEventListener listener) {
        if (getEventsListener().containsKey(type)) {
            Vector<OXMEventListener> listeners = getEventsListener().get(type);
            if (listeners.contains(listener)) {
                listeners.remove(listener);
            }
        }
    }

    public void unregisterAllEventListeners() {
        if (getEventsListener().size() > 0) {
            getEventsListener().clear();
        }
    }

    public void fireEvent(OXMEvent event) {
        if (event == null) {
            return;
        }
        if ((event == null || event.getEventType() != null) && getEventsListener().size() > 0) {
            Enumeration<OXMEvent.OXMEventType> en = getEventsListener().keys();
            while (en.hasMoreElements()) {
                OXMEvent.OXMEventType type = en.nextElement();
                if (type == event.getEventType()) {
                    Enumeration<OXMEventListener> enl = getEventsListener().get(type).elements();
                    while (enl.hasMoreElements()) {
                        ((OXMEventListener) enl.nextElement()).onPerform(event);
                    }
                }
            }
        }
    }

    public void dispose() {
        super.dispose();
        unregisterAllEventListeners();
    }
}
