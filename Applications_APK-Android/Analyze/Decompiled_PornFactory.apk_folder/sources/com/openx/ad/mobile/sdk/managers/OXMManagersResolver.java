package com.openx.ad.mobile.sdk.managers;

import android.content.Context;
import com.openx.ad.mobile.sdk.interfaces.OXMControllersManager;
import com.openx.ad.mobile.sdk.interfaces.OXMDeviceManager;
import com.openx.ad.mobile.sdk.interfaces.OXMEventsManager;
import com.openx.ad.mobile.sdk.interfaces.OXMLocationManager;
import com.openx.ad.mobile.sdk.interfaces.OXMManager;
import com.openx.ad.mobile.sdk.interfaces.OXMNetworkManager;
import java.util.Hashtable;

public class OXMManagersResolver {
    private Context mContext;
    private Hashtable<ManagerType, OXMManager> mRegisteredManaers;

    public enum ManagerType {
        DEVICE_MANAGER,
        EVENTS_MANAGER,
        LOCATION_MANAGER,
        NETWORK_MANAGER,
        CONTROLLERS_MANAGER
    }

    private void setContext(Context context) {
        this.mContext = context;
    }

    private Context getContext() {
        return this.mContext;
    }

    private Hashtable<ManagerType, OXMManager> getRegisteredManagers() {
        return this.mRegisteredManaers;
    }

    private OXMManagersResolver() {
        this.mRegisteredManaers = new Hashtable<>();
    }

    /* synthetic */ OXMManagersResolver(OXMManagersResolver oXMManagersResolver) {
        this();
    }

    private static class OXMManagersResolverHolder {
        public static final OXMManagersResolver instance = new OXMManagersResolver(null);

        private OXMManagersResolverHolder() {
        }
    }

    public static OXMManagersResolver getInstance() {
        return OXMManagersResolverHolder.instance;
    }

    public OXMManager getManager(ManagerType type) {
        if (getRegisteredManagers().containsKey(type)) {
            return getRegisteredManagers().get(type);
        }
        return null;
    }

    public OXMControllersManager getControllersManager() {
        return (OXMControllersManager) getManager(ManagerType.CONTROLLERS_MANAGER);
    }

    public OXMDeviceManager getDeviceManager() {
        return (OXMDeviceManager) getManager(ManagerType.DEVICE_MANAGER);
    }

    public OXMEventsManager getEventsManager() {
        return (OXMEventsManager) getManager(ManagerType.EVENTS_MANAGER);
    }

    public OXMLocationManager getLocationManager() {
        return (OXMLocationManager) getManager(ManagerType.LOCATION_MANAGER);
    }

    public OXMNetworkManager getNetworkManager() {
        return (OXMNetworkManager) getManager(ManagerType.NETWORK_MANAGER);
    }

    private boolean isReady(Context context) {
        return context == getContext();
    }

    private void registerManagers(Context context) {
        OXMManager manager = new OXMDeviceManagerImpl();
        manager.init(context);
        getRegisteredManagers().put(ManagerType.DEVICE_MANAGER, manager);
        OXMManager manager2 = new OXMEventsManagerImpl();
        manager2.init(context);
        getRegisteredManagers().put(ManagerType.EVENTS_MANAGER, manager2);
        OXMManager manager3 = new OXMLocationManagerImpl();
        manager3.init(context);
        getRegisteredManagers().put(ManagerType.LOCATION_MANAGER, manager3);
        OXMManager manager4 = new OXMNetworkManagerImpl();
        manager4.init(context);
        getRegisteredManagers().put(ManagerType.NETWORK_MANAGER, manager4);
        OXMManager manager5 = new OXMControllersManagerImpl();
        manager5.init(context);
        getRegisteredManagers().put(ManagerType.CONTROLLERS_MANAGER, manager5);
        setContext(context);
    }

    public void prepare(Context context) {
        if (!isReady(context)) {
            OXMManager manager = getInstance().getManager(ManagerType.EVENTS_MANAGER);
            if (manager != null) {
                manager.dispose();
            }
            OXMManager manager2 = getInstance().getManager(ManagerType.DEVICE_MANAGER);
            if (manager2 != null) {
                manager2.dispose();
            }
            OXMManager manager3 = getInstance().getManager(ManagerType.LOCATION_MANAGER);
            if (manager3 != null) {
                manager3.dispose();
            }
            OXMManager manager4 = getInstance().getManager(ManagerType.NETWORK_MANAGER);
            if (manager4 != null) {
                manager4.dispose();
            }
            OXMManager manager5 = getInstance().getManager(ManagerType.CONTROLLERS_MANAGER);
            if (manager5 != null) {
                manager5.dispose();
            }
            registerManagers(context);
        }
    }
}
