package com.openx.ad.mobile.sdk.interfaces;

import android.content.Context;
import android.os.Handler;
import android.view.ViewGroup;
import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;
import com.openx.ad.mobile.sdk.views.OXMAdViewFactory;

public interface OXMAdBannerView {
    void dispose();

    OXMAd getAd();

    OXMAdCreative getCreative();

    Object getCreator();

    OXMAdBaseController getParentController();

    OXMAdTracking getTracking();

    OXMAdViewFactory.OXMViewOrientation getViewOrientation();

    void hideAd();

    void isExpanded(Handler handler);

    void presentAdInViewGroup(ViewGroup viewGroup);

    void presentAdInViewGroup(ViewGroup viewGroup, ViewGroup.LayoutParams layoutParams);

    void reloadAd(OXMAd oXMAd);

    void setAdViewEventsListener(OXMAdViewEventsListener oXMAdViewEventsListener);

    void setParentController(OXMAdBaseController oXMAdBaseController);

    void showAd();

    void showInterstitialModalAd(Context context);
}
