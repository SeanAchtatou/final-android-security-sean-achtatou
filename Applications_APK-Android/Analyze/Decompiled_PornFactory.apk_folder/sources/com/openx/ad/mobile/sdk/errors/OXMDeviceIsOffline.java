package com.openx.ad.mobile.sdk.errors;

public class OXMDeviceIsOffline extends OXMError {
    private static final long serialVersionUID = -2321954806108588543L;

    public OXMDeviceIsOffline() {
        super.setMessage("Device is currently offline. Please check connection.");
    }
}
