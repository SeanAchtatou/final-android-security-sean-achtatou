package com.openx.ad.mobile.sdk.interfaces;

import com.openx.ad.mobile.sdk.OXMEvent;

public interface OXMEventsManager {
    void fireEvent(OXMEvent oXMEvent);

    void registerEventListener(OXMEvent.OXMEventType oXMEventType, OXMEventListener oXMEventListener);

    void unregisterAllEventListeners();

    void unregisterEventListener(OXMEvent.OXMEventType oXMEventType, OXMEventListener oXMEventListener);
}
