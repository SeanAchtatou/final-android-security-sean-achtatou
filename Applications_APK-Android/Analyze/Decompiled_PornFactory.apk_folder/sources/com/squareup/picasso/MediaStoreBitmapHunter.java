package com.squareup.picasso;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import java.io.IOException;

class MediaStoreBitmapHunter extends ContentStreamBitmapHunter {
    private static final String[] CONTENT_ORIENTATION = {"orientation"};

    MediaStoreBitmapHunter(Context context, Picasso picasso, Dispatcher dispatcher, Cache cache, Stats stats, Action action) {
        super(context, picasso, dispatcher, cache, stats, action);
    }

    /* access modifiers changed from: package-private */
    public Bitmap decode(Request data) throws IOException {
        Bitmap result;
        ContentResolver contentResolver = this.context.getContentResolver();
        setExifRotation(getExifOrientation(contentResolver, data.uri));
        String mimeType = contentResolver.getType(data.uri);
        boolean isVideo = mimeType != null && mimeType.startsWith("video/");
        if (data.hasSize()) {
            PicassoKind picassoKind = getPicassoKind(data.targetWidth, data.targetHeight);
            if (!isVideo && picassoKind == PicassoKind.FULL) {
                return super.decode(data);
            }
            long id = ContentUris.parseId(data.uri);
            BitmapFactory.Options options = createBitmapOptions(data);
            options.inJustDecodeBounds = true;
            calculateInSampleSize(data.targetWidth, data.targetHeight, picassoKind.width, picassoKind.height, options);
            if (isVideo) {
                result = MediaStore.Video.Thumbnails.getThumbnail(contentResolver, id, picassoKind == PicassoKind.FULL ? 1 : picassoKind.androidKind, options);
            } else {
                result = MediaStore.Images.Thumbnails.getThumbnail(contentResolver, id, picassoKind.androidKind, options);
            }
            if (result != null) {
                return result;
            }
        }
        return super.decode(data);
    }

    static PicassoKind getPicassoKind(int targetWidth, int targetHeight) {
        if (targetWidth <= PicassoKind.MICRO.width && targetHeight <= PicassoKind.MICRO.height) {
            return PicassoKind.MICRO;
        }
        if (targetWidth > PicassoKind.MINI.width || targetHeight > PicassoKind.MINI.height) {
            return PicassoKind.FULL;
        }
        return PicassoKind.MINI;
    }

    static int getExifOrientation(ContentResolver contentResolver, Uri uri) {
        Cursor cursor = null;
        try {
            cursor = contentResolver.query(uri, CONTENT_ORIENTATION, null, null, null);
            if (cursor == null || !cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return 0;
            }
            int i = cursor.getInt(0);
            if (cursor == null) {
                return i;
            }
            cursor.close();
            return i;
        } catch (RuntimeException e) {
            if (cursor != null) {
                cursor.close();
            }
            return 0;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    enum PicassoKind {
        MICRO(3, 96, 96),
        MINI(1, 512, 384),
        FULL(2, -1, -1);
        
        final int androidKind;
        final int height;
        final int width;

        private PicassoKind(int androidKind2, int width2, int height2) {
            this.androidKind = androidKind2;
            this.width = width2;
            this.height = height2;
        }
    }
}
