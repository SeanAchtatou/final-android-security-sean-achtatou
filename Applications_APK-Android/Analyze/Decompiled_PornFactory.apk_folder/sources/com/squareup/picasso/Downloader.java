package com.squareup.picasso;

import android.graphics.Bitmap;
import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;

public interface Downloader {
    Response load(Uri uri, boolean z) throws IOException;

    public static class ResponseException extends IOException {
        public ResponseException(String message) {
            super(message);
        }
    }

    public static class Response {
        final Bitmap bitmap;
        final boolean cached;
        final long contentLength;
        final InputStream stream;

        @Deprecated
        public Response(Bitmap bitmap2, boolean loadedFromCache) {
            this(bitmap2, loadedFromCache, -1);
        }

        @Deprecated
        public Response(InputStream stream2, boolean loadedFromCache) {
            this(stream2, loadedFromCache, -1);
        }

        public Response(Bitmap bitmap2, boolean loadedFromCache, long contentLength2) {
            if (bitmap2 == null) {
                throw new IllegalArgumentException("Bitmap may not be null.");
            }
            this.stream = null;
            this.bitmap = bitmap2;
            this.cached = loadedFromCache;
            this.contentLength = contentLength2;
        }

        public Response(InputStream stream2, boolean loadedFromCache, long contentLength2) {
            if (stream2 == null) {
                throw new IllegalArgumentException("Stream may not be null.");
            }
            this.stream = stream2;
            this.bitmap = null;
            this.cached = loadedFromCache;
            this.contentLength = contentLength2;
        }

        public InputStream getInputStream() {
            return this.stream;
        }

        public Bitmap getBitmap() {
            return this.bitmap;
        }

        public long getContentLength() {
            return this.contentLength;
        }
    }
}
