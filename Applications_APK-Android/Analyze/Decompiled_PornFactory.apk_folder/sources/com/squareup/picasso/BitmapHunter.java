package com.squareup.picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.ContactsContract;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

abstract class BitmapHunter implements Runnable {
    private static final Object DECODE_LOCK = new Object();
    private static final ThreadLocal<StringBuilder> NAME_BUILDER = new ThreadLocal<StringBuilder>() {
        /* access modifiers changed from: protected */
        public StringBuilder initialValue() {
            return new StringBuilder("Picasso-");
        }
    };
    Action action;
    List<Action> actions;
    final Cache cache;
    final Request data;
    final Dispatcher dispatcher;
    Exception exception;
    int exifRotation;
    Future<?> future;
    final String key;
    Picasso.LoadedFrom loadedFrom;
    final Picasso picasso;
    Bitmap result;
    final boolean skipMemoryCache;
    final Stats stats;

    /* access modifiers changed from: package-private */
    public abstract Bitmap decode(Request request) throws IOException;

    BitmapHunter(Picasso picasso2, Dispatcher dispatcher2, Cache cache2, Stats stats2, Action action2) {
        this.picasso = picasso2;
        this.dispatcher = dispatcher2;
        this.cache = cache2;
        this.stats = stats2;
        this.key = action2.getKey();
        this.data = action2.getRequest();
        this.skipMemoryCache = action2.skipCache;
        this.action = action2;
    }

    /* access modifiers changed from: protected */
    public void setExifRotation(int exifRotation2) {
        this.exifRotation = exifRotation2;
    }

    public void run() {
        String str;
        try {
            updateThreadName(this.data);
            if (this.picasso.loggingEnabled) {
                Utils.log("Hunter", "executing", Utils.getLogIdsForHunter(this));
            }
            this.result = hunt();
            if (this.result == null) {
                this.dispatcher.dispatchFailed(this);
            } else {
                this.dispatcher.dispatchComplete(this);
            }
        } catch (Downloader.ResponseException e) {
            this.exception = e;
            this.dispatcher.dispatchFailed(this);
        } catch (IOException e2) {
            this.exception = e2;
            this.dispatcher.dispatchRetry(this);
        } catch (OutOfMemoryError e3) {
            StringWriter writer = new StringWriter();
            this.stats.createSnapshot().dump(new PrintWriter(writer));
            this.exception = new RuntimeException(writer.toString(), e3);
            this.dispatcher.dispatchFailed(this);
        } catch (Exception e4) {
            this.exception = e4;
            this.dispatcher.dispatchFailed(this);
        } finally {
            str = "Picasso-Idle";
            Thread.currentThread().setName(str);
        }
    }

    /* access modifiers changed from: package-private */
    public Bitmap hunt() throws IOException {
        Bitmap bitmap;
        if (this.skipMemoryCache || (bitmap = this.cache.get(this.key)) == null) {
            Bitmap bitmap2 = decode(this.data);
            if (bitmap2 != null) {
                if (this.picasso.loggingEnabled) {
                    Utils.log("Hunter", "decoded", this.data.logId());
                }
                this.stats.dispatchBitmapDecoded(bitmap2);
                if (this.data.needsTransformation() || this.exifRotation != 0) {
                    synchronized (DECODE_LOCK) {
                        if (this.data.needsMatrixTransform() || this.exifRotation != 0) {
                            bitmap2 = transformResult(this.data, bitmap2, this.exifRotation);
                            if (this.picasso.loggingEnabled) {
                                Utils.log("Hunter", "transformed", this.data.logId());
                            }
                        }
                        if (this.data.hasCustomTransformations()) {
                            bitmap2 = applyCustomTransformations(this.data.transformations, bitmap2);
                            if (this.picasso.loggingEnabled) {
                                Utils.log("Hunter", "transformed", this.data.logId(), "from custom transformations");
                            }
                        }
                    }
                    if (bitmap2 != null) {
                        this.stats.dispatchBitmapTransformed(bitmap2);
                    }
                }
            }
            return bitmap2;
        }
        this.stats.dispatchCacheHit();
        this.loadedFrom = Picasso.LoadedFrom.MEMORY;
        if (this.picasso.loggingEnabled) {
            Utils.log("Hunter", "decoded", this.data.logId(), "from cache");
        }
        return bitmap;
    }

    /* access modifiers changed from: package-private */
    public void attach(Action action2) {
        boolean loggingEnabled = this.picasso.loggingEnabled;
        Request request = action2.request;
        if (this.action == null) {
            this.action = action2;
            if (!loggingEnabled) {
                return;
            }
            if (this.actions == null || this.actions.isEmpty()) {
                Utils.log("Hunter", "joined", request.logId(), "to empty hunter");
            } else {
                Utils.log("Hunter", "joined", request.logId(), Utils.getLogIdsForHunter(this, "to "));
            }
        } else {
            if (this.actions == null) {
                this.actions = new ArrayList(3);
            }
            this.actions.add(action2);
            if (loggingEnabled) {
                Utils.log("Hunter", "joined", request.logId(), Utils.getLogIdsForHunter(this, "to "));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void detach(Action action2) {
        if (this.action == action2) {
            this.action = null;
        } else if (this.actions != null) {
            this.actions.remove(action2);
        }
        if (this.picasso.loggingEnabled) {
            Utils.log("Hunter", "removed", action2.request.logId(), Utils.getLogIdsForHunter(this, "from "));
        }
    }

    /* access modifiers changed from: package-private */
    public boolean cancel() {
        if (this.action != null) {
            return false;
        }
        if ((this.actions == null || this.actions.isEmpty()) && this.future != null && this.future.cancel(false)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isCancelled() {
        return this.future != null && this.future.isCancelled();
    }

    /* access modifiers changed from: package-private */
    public boolean shouldSkipMemoryCache() {
        return this.skipMemoryCache;
    }

    /* access modifiers changed from: package-private */
    public boolean shouldRetry(boolean airplaneMode, NetworkInfo info) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean supportsReplay() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public Bitmap getResult() {
        return this.result;
    }

    /* access modifiers changed from: package-private */
    public String getKey() {
        return this.key;
    }

    /* access modifiers changed from: package-private */
    public Request getData() {
        return this.data;
    }

    /* access modifiers changed from: package-private */
    public Action getAction() {
        return this.action;
    }

    /* access modifiers changed from: package-private */
    public Picasso getPicasso() {
        return this.picasso;
    }

    /* access modifiers changed from: package-private */
    public List<Action> getActions() {
        return this.actions;
    }

    /* access modifiers changed from: package-private */
    public Exception getException() {
        return this.exception;
    }

    /* access modifiers changed from: package-private */
    public Picasso.LoadedFrom getLoadedFrom() {
        return this.loadedFrom;
    }

    static void updateThreadName(Request data2) {
        String name = data2.getName();
        StringBuilder builder = NAME_BUILDER.get();
        builder.ensureCapacity("Picasso-".length() + name.length());
        builder.replace("Picasso-".length(), builder.length(), name);
        Thread.currentThread().setName(builder.toString());
    }

    static BitmapHunter forRequest(Context context, Picasso picasso2, Dispatcher dispatcher2, Cache cache2, Stats stats2, Action action2, Downloader downloader) {
        if (action2.getRequest().resourceId != 0) {
            return new ResourceBitmapHunter(context, picasso2, dispatcher2, cache2, stats2, action2);
        }
        Uri uri = action2.getRequest().uri;
        String scheme = uri.getScheme();
        if ("content".equals(scheme)) {
            if (ContactsContract.Contacts.CONTENT_URI.getHost().equals(uri.getHost()) && !uri.getPathSegments().contains("photo")) {
                return new ContactsPhotoBitmapHunter(context, picasso2, dispatcher2, cache2, stats2, action2);
            }
            if ("media".equals(uri.getAuthority())) {
                return new MediaStoreBitmapHunter(context, picasso2, dispatcher2, cache2, stats2, action2);
            }
            return new ContentStreamBitmapHunter(context, picasso2, dispatcher2, cache2, stats2, action2);
        } else if ("file".equals(scheme)) {
            if (uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
                return new FileBitmapHunter(context, picasso2, dispatcher2, cache2, stats2, action2);
            }
            return new AssetBitmapHunter(context, picasso2, dispatcher2, cache2, stats2, action2);
        } else if ("android.resource".equals(scheme)) {
            return new ResourceBitmapHunter(context, picasso2, dispatcher2, cache2, stats2, action2);
        } else {
            return new NetworkBitmapHunter(picasso2, dispatcher2, cache2, stats2, action2, downloader);
        }
    }

    static BitmapFactory.Options createBitmapOptions(Request data2) {
        boolean justBounds = data2.hasSize();
        boolean hasConfig = data2.config != null;
        BitmapFactory.Options options = null;
        if (justBounds || hasConfig) {
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = justBounds;
            if (hasConfig) {
                options.inPreferredConfig = data2.config;
            }
        }
        return options;
    }

    static boolean requiresInSampleSize(BitmapFactory.Options options) {
        return options != null && options.inJustDecodeBounds;
    }

    static void calculateInSampleSize(int reqWidth, int reqHeight, BitmapFactory.Options options) {
        calculateInSampleSize(reqWidth, reqHeight, options.outWidth, options.outHeight, options);
    }

    static void calculateInSampleSize(int reqWidth, int reqHeight, int width, int height, BitmapFactory.Options options) {
        int sampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int heightRatio = (int) Math.floor((double) (((float) height) / ((float) reqHeight)));
            int widthRatio = (int) Math.floor((double) (((float) width) / ((float) reqWidth)));
            if (heightRatio < widthRatio) {
                sampleSize = heightRatio;
            } else {
                sampleSize = widthRatio;
            }
        }
        options.inSampleSize = sampleSize;
        options.inJustDecodeBounds = false;
    }

    static Bitmap applyCustomTransformations(List<Transformation> transformations, Bitmap result2) {
        int i = 0;
        int count = transformations.size();
        while (i < count) {
            final Transformation transformation = transformations.get(i);
            Bitmap newResult = transformation.transform(result2);
            if (newResult == null) {
                final StringBuilder builder = new StringBuilder().append("Transformation ").append(transformation.key()).append(" returned null after ").append(i).append(" previous transformation(s).\n\nTransformation list:\n");
                for (Transformation t : transformations) {
                    builder.append(t.key()).append(10);
                }
                Picasso.HANDLER.post(new Runnable() {
                    public void run() {
                        throw new NullPointerException(builder.toString());
                    }
                });
                return null;
            } else if (newResult == result2 && result2.isRecycled()) {
                Picasso.HANDLER.post(new Runnable() {
                    public void run() {
                        throw new IllegalStateException("Transformation " + transformation.key() + " returned input Bitmap but recycled it.");
                    }
                });
                return null;
            } else if (newResult == result2 || result2.isRecycled()) {
                result2 = newResult;
                i++;
            } else {
                Picasso.HANDLER.post(new Runnable() {
                    public void run() {
                        throw new IllegalStateException("Transformation " + transformation.key() + " mutated input Bitmap but failed to recycle the original.");
                    }
                });
                return null;
            }
        }
        return result2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    static Bitmap transformResult(Request data2, Bitmap result2, int exifRotation2) {
        float scale;
        float scale2;
        int inWidth = result2.getWidth();
        int inHeight = result2.getHeight();
        int drawX = 0;
        int drawY = 0;
        int drawWidth = inWidth;
        int drawHeight = inHeight;
        Matrix matrix = new Matrix();
        if (data2.needsMatrixTransform()) {
            int targetWidth = data2.targetWidth;
            int targetHeight = data2.targetHeight;
            float targetRotation = data2.rotationDegrees;
            if (targetRotation != 0.0f) {
                if (data2.hasRotationPivot) {
                    matrix.setRotate(targetRotation, data2.rotationPivotX, data2.rotationPivotY);
                } else {
                    matrix.setRotate(targetRotation);
                }
            }
            if (data2.centerCrop) {
                float widthRatio = ((float) targetWidth) / ((float) inWidth);
                float heightRatio = ((float) targetHeight) / ((float) inHeight);
                if (widthRatio > heightRatio) {
                    scale2 = widthRatio;
                    int newSize = (int) Math.ceil((double) (((float) inHeight) * (heightRatio / widthRatio)));
                    drawY = (inHeight - newSize) / 2;
                    drawHeight = newSize;
                } else {
                    scale2 = heightRatio;
                    int newSize2 = (int) Math.ceil((double) (((float) inWidth) * (widthRatio / heightRatio)));
                    drawX = (inWidth - newSize2) / 2;
                    drawWidth = newSize2;
                }
                matrix.preScale(scale2, scale2);
            } else if (data2.centerInside) {
                float widthRatio2 = ((float) targetWidth) / ((float) inWidth);
                float heightRatio2 = ((float) targetHeight) / ((float) inHeight);
                if (widthRatio2 < heightRatio2) {
                    scale = widthRatio2;
                } else {
                    scale = heightRatio2;
                }
                matrix.preScale(scale, scale);
            } else if (!(targetWidth == 0 || targetHeight == 0 || (targetWidth == inWidth && targetHeight == inHeight))) {
                matrix.preScale(((float) targetWidth) / ((float) inWidth), ((float) targetHeight) / ((float) inHeight));
            }
        }
        if (exifRotation2 != 0) {
            matrix.preRotate((float) exifRotation2);
        }
        Bitmap newResult = Bitmap.createBitmap(result2, drawX, drawY, drawWidth, drawHeight, matrix, true);
        if (newResult == result2) {
            return result2;
        }
        result2.recycle();
        return newResult;
    }
}
