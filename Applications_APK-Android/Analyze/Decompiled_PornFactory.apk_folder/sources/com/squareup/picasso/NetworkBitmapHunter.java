package com.squareup.picasso;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;

class NetworkBitmapHunter extends BitmapHunter {
    static final int DEFAULT_RETRY_COUNT = 2;
    private static final int MARKER = 65536;
    private final Downloader downloader;
    int retryCount = 2;

    public NetworkBitmapHunter(Picasso picasso, Dispatcher dispatcher, Cache cache, Stats stats, Action action, Downloader downloader2) {
        super(picasso, dispatcher, cache, stats, action);
        this.downloader = downloader2;
    }

    /* access modifiers changed from: package-private */
    public Bitmap decode(Request data) throws IOException {
        Downloader.Response response = this.downloader.load(data.uri, this.retryCount == 0);
        if (response == null) {
            return null;
        }
        this.loadedFrom = response.cached ? Picasso.LoadedFrom.DISK : Picasso.LoadedFrom.NETWORK;
        Bitmap result = response.getBitmap();
        if (result != null) {
            return result;
        }
        InputStream is = response.getInputStream();
        if (is == null) {
            return null;
        }
        if (response.getContentLength() == 0) {
            Utils.closeQuietly(is);
            throw new IOException("Received response with 0 content-length header.");
        }
        if (this.loadedFrom == Picasso.LoadedFrom.NETWORK && response.getContentLength() > 0) {
            this.stats.dispatchDownloadFinished(response.getContentLength());
        }
        try {
            return decodeStream(is, data);
        } finally {
            Utils.closeQuietly(is);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean shouldRetry(boolean airplaneMode, NetworkInfo info) {
        boolean hasRetries;
        if (this.retryCount > 0) {
            hasRetries = true;
        } else {
            hasRetries = false;
        }
        if (!hasRetries) {
            return false;
        }
        this.retryCount--;
        if (info == null || info.isConnected()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean supportsReplay() {
        return true;
    }

    private Bitmap decodeStream(InputStream stream, Request data) throws IOException {
        MarkableInputStream markStream = new MarkableInputStream(stream);
        InputStream stream2 = markStream;
        long mark = markStream.savePosition(65536);
        BitmapFactory.Options options = createBitmapOptions(data);
        boolean calculateSize = requiresInSampleSize(options);
        boolean isWebPFile = Utils.isWebPFile(stream2);
        markStream.reset(mark);
        if (isWebPFile) {
            byte[] bytes = Utils.toByteArray(stream2);
            if (calculateSize) {
                BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
                calculateInSampleSize(data.targetWidth, data.targetHeight, options);
            }
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        }
        if (calculateSize) {
            BitmapFactory.decodeStream(stream2, null, options);
            calculateInSampleSize(data.targetWidth, data.targetHeight, options);
            markStream.reset(mark);
        }
        Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, options);
        if (bitmap != null) {
            return bitmap;
        }
        throw new IOException("Failed to decode stream.");
    }
}
