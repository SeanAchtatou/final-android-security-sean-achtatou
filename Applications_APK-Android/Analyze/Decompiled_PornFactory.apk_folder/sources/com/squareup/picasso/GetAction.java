package com.squareup.picasso;

import android.graphics.Bitmap;
import com.squareup.picasso.Picasso;

class GetAction extends Action<Void> {
    GetAction(Picasso picasso, Request data, boolean skipCache, String key) {
        super(picasso, null, data, skipCache, false, 0, null, key);
    }

    /* access modifiers changed from: package-private */
    public void complete(Bitmap result, Picasso.LoadedFrom from) {
    }

    public void error() {
    }
}
