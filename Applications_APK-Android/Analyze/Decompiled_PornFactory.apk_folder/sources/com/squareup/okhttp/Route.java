package com.squareup.okhttp;

import com.squareup.okhttp.internal.http.RouteSelector;
import java.net.InetSocketAddress;
import java.net.Proxy;

public final class Route {
    final Address address;
    final InetSocketAddress inetSocketAddress;
    final Proxy proxy;
    final String tlsVersion;

    public Route(Address address2, Proxy proxy2, InetSocketAddress inetSocketAddress2, String tlsVersion2) {
        if (address2 == null) {
            throw new NullPointerException("address == null");
        } else if (proxy2 == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress2 == null) {
            throw new NullPointerException("inetSocketAddress == null");
        } else if (tlsVersion2 == null) {
            throw new NullPointerException("tlsVersion == null");
        } else {
            this.address = address2;
            this.proxy = proxy2;
            this.inetSocketAddress = inetSocketAddress2;
            this.tlsVersion = tlsVersion2;
        }
    }

    public Address getAddress() {
        return this.address;
    }

    public Proxy getProxy() {
        return this.proxy;
    }

    public InetSocketAddress getSocketAddress() {
        return this.inetSocketAddress;
    }

    public String getTlsVersion() {
        return this.tlsVersion;
    }

    /* access modifiers changed from: package-private */
    public boolean supportsNpn() {
        return !this.tlsVersion.equals(RouteSelector.SSL_V3);
    }

    public boolean requiresTunnel() {
        return this.address.sslSocketFactory != null && this.proxy.type() == Proxy.Type.HTTP;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Route)) {
            return false;
        }
        Route other = (Route) obj;
        if (!this.address.equals(other.address) || !this.proxy.equals(other.proxy) || !this.inetSocketAddress.equals(other.inetSocketAddress) || !this.tlsVersion.equals(other.tlsVersion)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((((((this.address.hashCode() + 527) * 31) + this.proxy.hashCode()) * 31) + this.inetSocketAddress.hashCode()) * 31) + this.tlsVersion.hashCode();
    }
}
