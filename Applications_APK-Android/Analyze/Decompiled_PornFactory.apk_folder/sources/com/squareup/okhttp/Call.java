package com.squareup.okhttp;

import com.squareup.okhttp.Request;
import com.squareup.okhttp.internal.NamedRunnable;
import com.squareup.okhttp.internal.Util;
import com.squareup.okhttp.internal.http.HttpEngine;
import com.squareup.okhttp.internal.http.HttpMethod;
import com.squareup.okhttp.internal.http.OkHeaders;
import com.squareup.okhttp.internal.http.RetryableSink;
import java.io.IOException;
import java.net.ProtocolException;
import okio.BufferedSource;

public final class Call {
    volatile boolean canceled;
    private final OkHttpClient client;
    /* access modifiers changed from: private */
    public final Dispatcher dispatcher;
    HttpEngine engine;
    private boolean executed;
    private int redirectionCount;
    /* access modifiers changed from: private */
    public Request request;

    Call(OkHttpClient client2, Dispatcher dispatcher2, Request request2) {
        this.client = client2;
        this.dispatcher = dispatcher2;
        this.request = request2;
    }

    public Response execute() throws IOException {
        synchronized (this) {
            if (this.executed) {
                throw new IllegalStateException("Already Executed");
            }
            this.executed = true;
        }
        Response result = getResponse();
        this.engine.releaseConnection();
        if (result != null) {
            return result;
        }
        throw new IOException("Canceled");
    }

    public void enqueue(Callback responseCallback) {
        synchronized (this) {
            if (this.executed) {
                throw new IllegalStateException("Already Executed");
            }
            this.executed = true;
        }
        this.dispatcher.enqueue(new AsyncCall(responseCallback));
    }

    public void cancel() {
        this.canceled = true;
        if (this.engine != null) {
            this.engine.disconnect();
        }
    }

    final class AsyncCall extends NamedRunnable {
        private final Callback responseCallback;

        private AsyncCall(Callback responseCallback2) {
            super("OkHttp %s", Call.this.request.urlString());
            this.responseCallback = responseCallback2;
        }

        /* access modifiers changed from: package-private */
        public String host() {
            return Call.this.request.url().getHost();
        }

        /* access modifiers changed from: package-private */
        public Request request() {
            return Call.this.request;
        }

        /* access modifiers changed from: package-private */
        public Object tag() {
            return Call.this.request.tag();
        }

        /* access modifiers changed from: package-private */
        public Call get() {
            return Call.this;
        }

        /* access modifiers changed from: protected */
        public void execute() {
            boolean signalledCallback = false;
            try {
                Response response = Call.this.getResponse();
                if (Call.this.canceled) {
                    this.responseCallback.onFailure(Call.this.request, new IOException("Canceled"));
                } else {
                    signalledCallback = true;
                    Call.this.engine.releaseConnection();
                    this.responseCallback.onResponse(response);
                }
            } catch (IOException e) {
                if (signalledCallback) {
                    throw new RuntimeException(e);
                }
                this.responseCallback.onFailure(Call.this.request, e);
            } finally {
                Call.this.dispatcher.finished(this);
            }
        }
    }

    /* access modifiers changed from: private */
    public Response getResponse() throws IOException {
        RequestBody body = this.request.body();
        RetryableSink requestBodyOut = null;
        if (body != null) {
            Request.Builder requestBuilder = this.request.newBuilder();
            MediaType contentType = body.contentType();
            if (contentType != null) {
                requestBuilder.header("Content-Type", contentType.toString());
            }
            long contentLength = body.contentLength();
            if (contentLength != -1) {
                requestBuilder.header("Content-Length", Long.toString(contentLength));
                requestBuilder.removeHeader("Transfer-Encoding");
            } else {
                requestBuilder.header("Transfer-Encoding", "chunked");
                requestBuilder.removeHeader("Content-Length");
            }
            this.request = requestBuilder.build();
        } else if (HttpMethod.hasRequestBody(this.request.method())) {
            requestBodyOut = Util.emptySink();
        }
        this.engine = new HttpEngine(this.client, this.request, false, null, null, requestBodyOut, null);
        while (!this.canceled) {
            try {
                this.engine.sendRequest();
                if (this.request.body() != null) {
                    this.request.body().writeTo(this.engine.getBufferedRequestBody());
                }
                this.engine.readResponse();
                Response response = this.engine.getResponse();
                Request followUp = this.engine.followUpRequest();
                if (followUp == null) {
                    this.engine.releaseConnection();
                    return response.newBuilder().body(new RealResponseBody(response, this.engine.getResponseBody())).build();
                }
                if (this.engine.getResponse().isRedirect()) {
                    int i = this.redirectionCount + 1;
                    this.redirectionCount = i;
                    if (i > 20) {
                        throw new ProtocolException("Too many redirects: " + this.redirectionCount);
                    }
                }
                if (!this.engine.sameConnection(followUp.url())) {
                    this.engine.releaseConnection();
                }
                Connection connection = this.engine.close();
                this.request = followUp;
                this.engine = new HttpEngine(this.client, this.request, false, connection, null, null, response);
            } catch (IOException e) {
                HttpEngine retryEngine = this.engine.recover(e, null);
                if (retryEngine != null) {
                    this.engine = retryEngine;
                } else {
                    throw e;
                }
            }
        }
        return null;
    }

    private static class RealResponseBody extends ResponseBody {
        private final Response response;
        private final BufferedSource source;

        RealResponseBody(Response response2, BufferedSource source2) {
            this.response = response2;
            this.source = source2;
        }

        public MediaType contentType() {
            String contentType = this.response.header("Content-Type");
            if (contentType != null) {
                return MediaType.parse(contentType);
            }
            return null;
        }

        public long contentLength() {
            return OkHeaders.contentLength(this.response);
        }

        public BufferedSource source() {
            return this.source;
        }
    }
}
