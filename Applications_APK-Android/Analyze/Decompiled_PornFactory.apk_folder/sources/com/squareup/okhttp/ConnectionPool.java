package com.squareup.okhttp;

import com.squareup.okhttp.internal.Platform;
import com.squareup.okhttp.internal.Util;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class ConnectionPool {
    private static final long DEFAULT_KEEP_ALIVE_DURATION_MS = 300000;
    private static final int MAX_CONNECTIONS_TO_CLEANUP = 2;
    private static final ConnectionPool systemDefault;
    /* access modifiers changed from: private */
    public final LinkedList<Connection> connections = new LinkedList<>();
    private final Runnable connectionsCleanupRunnable = new Runnable() {
        public void run() {
            List<Connection> expiredConnections = new ArrayList<>(2);
            int idleConnectionCount = 0;
            synchronized (ConnectionPool.this) {
                ListIterator<Connection> i = ConnectionPool.this.connections.listIterator(ConnectionPool.this.connections.size());
                while (i.hasPrevious()) {
                    Connection connection = (Connection) i.previous();
                    if (!connection.isAlive() || connection.isExpired(ConnectionPool.this.keepAliveDurationNs)) {
                        i.remove();
                        expiredConnections.add(connection);
                        if (expiredConnections.size() == 2) {
                            break;
                        }
                    } else if (connection.isIdle()) {
                        idleConnectionCount++;
                    }
                }
                ListIterator<Connection> i2 = ConnectionPool.this.connections.listIterator(ConnectionPool.this.connections.size());
                while (i2.hasPrevious() && idleConnectionCount > ConnectionPool.this.maxIdleConnections) {
                    Connection connection2 = (Connection) i2.previous();
                    if (connection2.isIdle()) {
                        expiredConnections.add(connection2);
                        i2.remove();
                        idleConnectionCount--;
                    }
                }
            }
            for (Connection expiredConnection : expiredConnections) {
                Util.closeQuietly(expiredConnection.getSocket());
            }
        }
    };
    private final ExecutorService executorService = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), Util.threadFactory("OkHttp ConnectionPool", true));
    /* access modifiers changed from: private */
    public final long keepAliveDurationNs;
    /* access modifiers changed from: private */
    public final int maxIdleConnections;

    static {
        String keepAlive = System.getProperty("http.keepAlive");
        String keepAliveDuration = System.getProperty("http.keepAliveDuration");
        String maxIdleConnections2 = System.getProperty("http.maxConnections");
        long keepAliveDurationMs = keepAliveDuration != null ? Long.parseLong(keepAliveDuration) : DEFAULT_KEEP_ALIVE_DURATION_MS;
        if (keepAlive != null && !Boolean.parseBoolean(keepAlive)) {
            systemDefault = new ConnectionPool(0, keepAliveDurationMs);
        } else if (maxIdleConnections2 != null) {
            systemDefault = new ConnectionPool(Integer.parseInt(maxIdleConnections2), keepAliveDurationMs);
        } else {
            systemDefault = new ConnectionPool(5, keepAliveDurationMs);
        }
    }

    public ConnectionPool(int maxIdleConnections2, long keepAliveDurationMs) {
        this.maxIdleConnections = maxIdleConnections2;
        this.keepAliveDurationNs = keepAliveDurationMs * 1000 * 1000;
    }

    /* access modifiers changed from: package-private */
    public List<Connection> getConnections() {
        ArrayList arrayList;
        waitForCleanupCallableToRun();
        synchronized (this) {
            arrayList = new ArrayList(this.connections);
        }
        return arrayList;
    }

    private void waitForCleanupCallableToRun() {
        try {
            this.executorService.submit(new Runnable() {
                public void run() {
                }
            }).get();
        } catch (Exception e) {
            throw new AssertionError();
        }
    }

    public static ConnectionPool getDefault() {
        return systemDefault;
    }

    public synchronized int getConnectionCount() {
        return this.connections.size();
    }

    public synchronized int getSpdyConnectionCount() {
        int total;
        total = 0;
        Iterator i$ = this.connections.iterator();
        while (i$.hasNext()) {
            if (i$.next().isSpdy()) {
                total++;
            }
        }
        return total;
    }

    public synchronized int getHttpConnectionCount() {
        int total;
        total = 0;
        Iterator i$ = this.connections.iterator();
        while (i$.hasNext()) {
            if (!i$.next().isSpdy()) {
                total++;
            }
        }
        return total;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0051, code lost:
        r2 = r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.squareup.okhttp.Connection get(com.squareup.okhttp.Address r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            r2 = 0
            java.util.LinkedList<com.squareup.okhttp.Connection> r4 = r8.connections     // Catch:{ all -> 0x008b }
            java.util.LinkedList<com.squareup.okhttp.Connection> r5 = r8.connections     // Catch:{ all -> 0x008b }
            int r5 = r5.size()     // Catch:{ all -> 0x008b }
            java.util.ListIterator r3 = r4.listIterator(r5)     // Catch:{ all -> 0x008b }
        L_0x000e:
            boolean r4 = r3.hasPrevious()     // Catch:{ all -> 0x008b }
            if (r4 == 0) goto L_0x0052
            java.lang.Object r0 = r3.previous()     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.Connection r0 = (com.squareup.okhttp.Connection) r0     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.Route r4 = r0.getRoute()     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.Address r4 = r4.getAddress()     // Catch:{ all -> 0x008b }
            boolean r4 = r4.equals(r9)     // Catch:{ all -> 0x008b }
            if (r4 == 0) goto L_0x000e
            boolean r4 = r0.isAlive()     // Catch:{ all -> 0x008b }
            if (r4 == 0) goto L_0x000e
            long r4 = java.lang.System.nanoTime()     // Catch:{ all -> 0x008b }
            long r6 = r0.getIdleStartTimeNs()     // Catch:{ all -> 0x008b }
            long r4 = r4 - r6
            long r6 = r8.keepAliveDurationNs     // Catch:{ all -> 0x008b }
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 >= 0) goto L_0x000e
            r3.remove()     // Catch:{ all -> 0x008b }
            boolean r4 = r0.isSpdy()     // Catch:{ all -> 0x008b }
            if (r4 != 0) goto L_0x0051
            com.squareup.okhttp.internal.Platform r4 = com.squareup.okhttp.internal.Platform.get()     // Catch:{ SocketException -> 0x0068 }
            java.net.Socket r5 = r0.getSocket()     // Catch:{ SocketException -> 0x0068 }
            r4.tagSocket(r5)     // Catch:{ SocketException -> 0x0068 }
        L_0x0051:
            r2 = r0
        L_0x0052:
            if (r2 == 0) goto L_0x005f
            boolean r4 = r2.isSpdy()     // Catch:{ all -> 0x008b }
            if (r4 == 0) goto L_0x005f
            java.util.LinkedList<com.squareup.okhttp.Connection> r4 = r8.connections     // Catch:{ all -> 0x008b }
            r4.addFirst(r2)     // Catch:{ all -> 0x008b }
        L_0x005f:
            java.util.concurrent.ExecutorService r4 = r8.executorService     // Catch:{ all -> 0x008b }
            java.lang.Runnable r5 = r8.connectionsCleanupRunnable     // Catch:{ all -> 0x008b }
            r4.execute(r5)     // Catch:{ all -> 0x008b }
            monitor-exit(r8)
            return r2
        L_0x0068:
            r1 = move-exception
            java.net.Socket r4 = r0.getSocket()     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.internal.Util.closeQuietly(r4)     // Catch:{ all -> 0x008b }
            com.squareup.okhttp.internal.Platform r4 = com.squareup.okhttp.internal.Platform.get()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            r5.<init>()     // Catch:{ all -> 0x008b }
            java.lang.String r6 = "Unable to tagSocket(): "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r5 = r5.append(r1)     // Catch:{ all -> 0x008b }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x008b }
            r4.logW(r5)     // Catch:{ all -> 0x008b }
            goto L_0x000e
        L_0x008b:
            r4 = move-exception
            monitor-exit(r8)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.ConnectionPool.get(com.squareup.okhttp.Address):com.squareup.okhttp.Connection");
    }

    /* access modifiers changed from: package-private */
    public void recycle(Connection connection) {
        if (connection.isSpdy() || !connection.clearOwner()) {
            return;
        }
        if (!connection.isAlive()) {
            Util.closeQuietly(connection.getSocket());
            return;
        }
        try {
            Platform.get().untagSocket(connection.getSocket());
            synchronized (this) {
                this.connections.addFirst(connection);
                connection.incrementRecycleCount();
                connection.resetIdleStartTime();
            }
            this.executorService.execute(this.connectionsCleanupRunnable);
        } catch (SocketException e) {
            Platform.get().logW("Unable to untagSocket(): " + e);
            Util.closeQuietly(connection.getSocket());
        }
    }

    /* access modifiers changed from: package-private */
    public void share(Connection connection) {
        if (!connection.isSpdy()) {
            throw new IllegalArgumentException();
        }
        this.executorService.execute(this.connectionsCleanupRunnable);
        if (connection.isAlive()) {
            synchronized (this) {
                this.connections.addFirst(connection);
            }
        }
    }

    public void evictAll() {
        List<Connection> connections2;
        synchronized (this) {
            connections2 = new ArrayList<>(this.connections);
            this.connections.clear();
        }
        int size = connections2.size();
        for (int i = 0; i < size; i++) {
            Util.closeQuietly(((Connection) connections2.get(i)).getSocket());
        }
    }
}
