package com.squareup.okhttp;

import com.squareup.okhttp.internal.Util;
import java.net.Proxy;
import java.net.UnknownHostException;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public final class Address {
    final Authenticator authenticator;
    final HostnameVerifier hostnameVerifier;
    final List<Protocol> protocols;
    final Proxy proxy;
    final SocketFactory socketFactory;
    final SSLSocketFactory sslSocketFactory;
    final String uriHost;
    final int uriPort;

    public Address(String uriHost2, int uriPort2, SocketFactory socketFactory2, SSLSocketFactory sslSocketFactory2, HostnameVerifier hostnameVerifier2, Authenticator authenticator2, Proxy proxy2, List<Protocol> protocols2) throws UnknownHostException {
        if (uriHost2 == null) {
            throw new NullPointerException("uriHost == null");
        } else if (uriPort2 <= 0) {
            throw new IllegalArgumentException("uriPort <= 0: " + uriPort2);
        } else if (authenticator2 == null) {
            throw new IllegalArgumentException("authenticator == null");
        } else if (protocols2 == null) {
            throw new IllegalArgumentException("protocols == null");
        } else {
            this.proxy = proxy2;
            this.uriHost = uriHost2;
            this.uriPort = uriPort2;
            this.socketFactory = socketFactory2;
            this.sslSocketFactory = sslSocketFactory2;
            this.hostnameVerifier = hostnameVerifier2;
            this.authenticator = authenticator2;
            this.protocols = Util.immutableList(protocols2);
        }
    }

    public String getUriHost() {
        return this.uriHost;
    }

    public int getUriPort() {
        return this.uriPort;
    }

    public SocketFactory getSocketFactory() {
        return this.socketFactory;
    }

    public SSLSocketFactory getSslSocketFactory() {
        return this.sslSocketFactory;
    }

    public HostnameVerifier getHostnameVerifier() {
        return this.hostnameVerifier;
    }

    public Authenticator getAuthenticator() {
        return this.authenticator;
    }

    public List<Protocol> getProtocols() {
        return this.protocols;
    }

    public Proxy getProxy() {
        return this.proxy;
    }

    public boolean equals(Object other) {
        if (!(other instanceof Address)) {
            return false;
        }
        Address that = (Address) other;
        if (!Util.equal(this.proxy, that.proxy) || !this.uriHost.equals(that.uriHost) || this.uriPort != that.uriPort || !Util.equal(this.sslSocketFactory, that.sslSocketFactory) || !Util.equal(this.hostnameVerifier, that.hostnameVerifier) || !Util.equal(this.authenticator, that.authenticator) || !Util.equal(this.protocols, that.protocols)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int hashCode = (((((this.uriHost.hashCode() + 527) * 31) + this.uriPort) * 31) + (this.sslSocketFactory != null ? this.sslSocketFactory.hashCode() : 0)) * 31;
        if (this.hostnameVerifier != null) {
            i = this.hostnameVerifier.hashCode();
        } else {
            i = 0;
        }
        int hashCode2 = (((hashCode + i) * 31) + this.authenticator.hashCode()) * 31;
        if (this.proxy != null) {
            i2 = this.proxy.hashCode();
        }
        return ((hashCode2 + i2) * 31) + this.protocols.hashCode();
    }
}
