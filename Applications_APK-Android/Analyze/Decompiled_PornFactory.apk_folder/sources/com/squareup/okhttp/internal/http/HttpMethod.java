package com.squareup.okhttp.internal.http;

import com.android.volley.toolbox.HttpClientStack;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public final class HttpMethod {
    public static final Set<String> METHODS = new LinkedHashSet(Arrays.asList("OPTIONS", "GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", HttpClientStack.HttpPatch.METHOD_NAME));

    public static boolean invalidatesCache(String method) {
        return method.equals("POST") || method.equals(HttpClientStack.HttpPatch.METHOD_NAME) || method.equals("PUT") || method.equals("DELETE");
    }

    public static boolean hasRequestBody(String method) {
        return method.equals("POST") || method.equals("PUT") || method.equals(HttpClientStack.HttpPatch.METHOD_NAME) || method.equals("DELETE");
    }

    private HttpMethod() {
    }
}
