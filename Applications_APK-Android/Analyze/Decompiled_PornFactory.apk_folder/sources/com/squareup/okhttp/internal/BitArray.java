package com.squareup.okhttp.internal;

import com.dd.plist.ASCIIPropertyListParser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface BitArray {
    void clear();

    boolean get(int i);

    void set(int i);

    void shiftLeft(int i);

    void toggle(int i);

    public static final class FixedCapacity implements BitArray {
        long data = 0;

        public void clear() {
            this.data = 0;
        }

        public void set(int index) {
            this.data |= 1 << checkInput(index);
        }

        public void toggle(int index) {
            this.data ^= 1 << checkInput(index);
        }

        public boolean get(int index) {
            return ((this.data >> checkInput(index)) & 1) == 1;
        }

        public void shiftLeft(int count) {
            this.data <<= checkInput(count);
        }

        public String toString() {
            return Long.toBinaryString(this.data);
        }

        public BitArray toVariableCapacity() {
            return new VariableCapacity(this);
        }

        private static int checkInput(int index) {
            if (index >= 0 && index <= 63) {
                return index;
            }
            throw new IllegalArgumentException(String.format("input must be between 0 and 63: %s", Integer.valueOf(index)));
        }
    }

    public static final class VariableCapacity implements BitArray {
        long[] data;
        private int start;

        public VariableCapacity() {
            this.data = new long[1];
        }

        private VariableCapacity(FixedCapacity small) {
            this.data = new long[]{small.data, 0};
        }

        private void growToSize(int size) {
            long[] newData = new long[size];
            if (this.data != null) {
                System.arraycopy(this.data, 0, newData, 0, this.data.length);
            }
            this.data = newData;
        }

        private int offsetOf(int index) {
            int offset = (index + this.start) / 64;
            if (offset > this.data.length - 1) {
                growToSize(offset + 1);
            }
            return offset;
        }

        private int shiftOf(int index) {
            return (this.start + index) % 64;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.Arrays.fill(long[], long):void}
         arg types: [long[], int]
         candidates:
          ClspMth{java.util.Arrays.fill(double[], double):void}
          ClspMth{java.util.Arrays.fill(byte[], byte):void}
          ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
          ClspMth{java.util.Arrays.fill(char[], char):void}
          ClspMth{java.util.Arrays.fill(short[], short):void}
          ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
          ClspMth{java.util.Arrays.fill(int[], int):void}
          ClspMth{java.util.Arrays.fill(float[], float):void}
          ClspMth{java.util.Arrays.fill(long[], long):void} */
        public void clear() {
            Arrays.fill(this.data, 0L);
        }

        public void set(int index) {
            checkInput(index);
            int offset = offsetOf(index);
            long[] jArr = this.data;
            jArr[offset] = jArr[offset] | (1 << shiftOf(index));
        }

        public void toggle(int index) {
            checkInput(index);
            int offset = offsetOf(index);
            long[] jArr = this.data;
            jArr[offset] = jArr[offset] ^ (1 << shiftOf(index));
        }

        public boolean get(int index) {
            checkInput(index);
            return (this.data[offsetOf(index)] & (1 << shiftOf(index))) != 0;
        }

        public void shiftLeft(int count) {
            this.start -= checkInput(count);
            if (this.start < 0) {
                int arrayShift = (this.start / -64) + 1;
                long[] newData = new long[(this.data.length + arrayShift)];
                System.arraycopy(this.data, 0, newData, arrayShift, this.data.length);
                this.data = newData;
                this.start = (this.start % 64) + 64;
            }
        }

        public String toString() {
            StringBuilder builder = new StringBuilder("{");
            List<Integer> ints = toIntegerList();
            int count = ints.size();
            for (int i = 0; i < count; i++) {
                if (i > 0) {
                    builder.append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN);
                }
                builder.append(ints.get(i));
            }
            return builder.append((char) ASCIIPropertyListParser.DICTIONARY_END_TOKEN).toString();
        }

        /* access modifiers changed from: package-private */
        public List<Integer> toIntegerList() {
            List<Integer> ints = new ArrayList<>();
            int count = (this.data.length * 64) - this.start;
            for (int i = 0; i < count; i++) {
                if (get(i)) {
                    ints.add(Integer.valueOf(i));
                }
            }
            return ints;
        }

        private static int checkInput(int index) {
            if (index >= 0) {
                return index;
            }
            throw new IllegalArgumentException(String.format("input must be a positive number: %s", Integer.valueOf(index)));
        }
    }
}
