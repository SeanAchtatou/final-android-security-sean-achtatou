package com.squareup.okhttp.internal.http;

import com.squareup.okhttp.Address;
import com.squareup.okhttp.Connection;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.squareup.okhttp.Route;
import com.squareup.okhttp.internal.Dns;
import com.squareup.okhttp.internal.Internal;
import com.squareup.okhttp.internal.InternalCache;
import com.squareup.okhttp.internal.Util;
import com.squareup.okhttp.internal.http.CacheStrategy;
import java.io.IOException;
import java.io.InputStream;
import java.net.CacheRequest;
import java.net.CookieHandler;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.util.Date;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocketFactory;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.GzipSource;
import okio.Okio;
import okio.Sink;
import okio.Source;

public final class HttpEngine {
    private static final ResponseBody EMPTY_BODY = new ResponseBody() {
        public MediaType contentType() {
            return null;
        }

        public long contentLength() {
            return 0;
        }

        public BufferedSource source() {
            return new Buffer();
        }
    };
    public static final int MAX_REDIRECTS = 20;
    public final boolean bufferRequestBody;
    private BufferedSink bufferedRequestBody;
    private Response cacheResponse;
    private CacheStrategy cacheStrategy;
    final OkHttpClient client;
    private Connection connection;
    private Request networkRequest;
    private Response networkResponse;
    private final Response priorResponse;
    private Sink requestBodyOut;
    private BufferedSource responseBody;
    private InputStream responseBodyBytes;
    private Source responseTransferSource;
    private Route route;
    private RouteSelector routeSelector;
    long sentRequestMillis = -1;
    private CacheRequest storeRequest;
    private boolean transparentGzip;
    private Transport transport;
    private final Request userRequest;
    private Response userResponse;

    public HttpEngine(OkHttpClient client2, Request request, boolean bufferRequestBody2, Connection connection2, RouteSelector routeSelector2, RetryableSink requestBodyOut2, Response priorResponse2) {
        this.client = client2;
        this.userRequest = request;
        this.bufferRequestBody = bufferRequestBody2;
        this.connection = connection2;
        this.routeSelector = routeSelector2;
        this.requestBodyOut = requestBodyOut2;
        this.priorResponse = priorResponse2;
        if (connection2 != null) {
            Internal.instance.setOwner(connection2, this);
            this.route = connection2.getRoute();
            return;
        }
        this.route = null;
    }

    public void sendRequest() throws IOException {
        Response cacheCandidate;
        if (this.cacheStrategy == null) {
            if (this.transport != null) {
                throw new IllegalStateException();
            }
            Request request = networkRequest(this.userRequest);
            InternalCache responseCache = Internal.instance.internalCache(this.client);
            if (responseCache != null) {
                cacheCandidate = responseCache.get(request);
            } else {
                cacheCandidate = null;
            }
            this.cacheStrategy = new CacheStrategy.Factory(System.currentTimeMillis(), request, cacheCandidate).get();
            this.networkRequest = this.cacheStrategy.networkRequest;
            this.cacheResponse = this.cacheStrategy.cacheResponse;
            if (responseCache != null) {
                responseCache.trackResponse(this.cacheStrategy);
            }
            if (cacheCandidate != null && this.cacheResponse == null) {
                Util.closeQuietly(cacheCandidate.body());
            }
            if (this.networkRequest != null) {
                if (this.connection == null) {
                    connect(this.networkRequest);
                }
                if (Internal.instance.getOwner(this.connection) == this || Internal.instance.isSpdy(this.connection)) {
                    this.transport = Internal.instance.newTransport(this.connection, this);
                    if (hasRequestBody() && this.requestBodyOut == null) {
                        this.requestBodyOut = this.transport.createRequestBody(request);
                        return;
                    }
                    return;
                }
                throw new AssertionError();
            }
            if (this.connection != null) {
                Internal.instance.recycle(this.client.getConnectionPool(), this.connection);
                this.connection = null;
            }
            if (this.cacheResponse != null) {
                this.userResponse = this.cacheResponse.newBuilder().request(this.userRequest).priorResponse(stripBody(this.priorResponse)).cacheResponse(stripBody(this.cacheResponse)).build();
            } else {
                this.userResponse = new Response.Builder().request(this.userRequest).priorResponse(stripBody(this.priorResponse)).protocol(Protocol.HTTP_1_1).code(504).message("Unsatisfiable Request (only-if-cached)").body(EMPTY_BODY).build();
            }
            if (this.userResponse.body() != null) {
                initContentStream(this.userResponse.body().source());
            }
        }
    }

    private static Response stripBody(Response response) {
        return (response == null || response.body() == null) ? response : response.newBuilder().body(null).build();
    }

    private void connect(Request request) throws IOException {
        if (this.connection != null) {
            throw new IllegalStateException();
        }
        if (this.routeSelector == null) {
            String uriHost = request.url().getHost();
            if (uriHost == null || uriHost.length() == 0) {
                throw new UnknownHostException(request.url().toString());
            }
            SSLSocketFactory sslSocketFactory = null;
            HostnameVerifier hostnameVerifier = null;
            if (request.isHttps()) {
                sslSocketFactory = this.client.getSslSocketFactory();
                hostnameVerifier = this.client.getHostnameVerifier();
            }
            this.routeSelector = new RouteSelector(new Address(uriHost, Util.getEffectivePort(request.url()), this.client.getSocketFactory(), sslSocketFactory, hostnameVerifier, this.client.getAuthenticator(), this.client.getProxy(), this.client.getProtocols()), request.uri(), this.client.getProxySelector(), this.client.getConnectionPool(), Dns.DEFAULT, Internal.instance.routeDatabase(this.client));
        }
        this.connection = this.routeSelector.next(request.method());
        Internal.instance.setOwner(this.connection, this);
        if (!Internal.instance.isConnected(this.connection)) {
            Internal.instance.connect(this.connection, this.client.getConnectTimeout(), this.client.getReadTimeout(), this.client.getWriteTimeout(), tunnelRequest(this.connection, request));
            if (Internal.instance.isSpdy(this.connection)) {
                Internal.instance.share(this.client.getConnectionPool(), this.connection);
            }
            Internal.instance.routeDatabase(this.client).connected(this.connection.getRoute());
        }
        Internal.instance.setTimeouts(this.connection, this.client.getReadTimeout(), this.client.getWriteTimeout());
        this.route = this.connection.getRoute();
    }

    public void writingRequestHeaders() {
        if (this.sentRequestMillis != -1) {
            throw new IllegalStateException();
        }
        this.sentRequestMillis = System.currentTimeMillis();
    }

    /* access modifiers changed from: package-private */
    public boolean hasRequestBody() {
        return HttpMethod.hasRequestBody(this.userRequest.method()) && !Util.emptySink().equals(this.requestBodyOut);
    }

    public Sink getRequestBody() {
        if (this.cacheStrategy != null) {
            return this.requestBodyOut;
        }
        throw new IllegalStateException();
    }

    public BufferedSink getBufferedRequestBody() {
        BufferedSink result;
        BufferedSink result2 = this.bufferedRequestBody;
        if (result2 != null) {
            return result2;
        }
        Sink requestBody = getRequestBody();
        if (requestBody != null) {
            result = Okio.buffer(requestBody);
            this.bufferedRequestBody = result;
        } else {
            result = null;
        }
        return result;
    }

    public boolean hasResponse() {
        return this.userResponse != null;
    }

    public Request getRequest() {
        return this.userRequest;
    }

    public Response getResponse() {
        if (this.userResponse != null) {
            return this.userResponse;
        }
        throw new IllegalStateException();
    }

    public BufferedSource getResponseBody() {
        if (this.userResponse != null) {
            return this.responseBody;
        }
        throw new IllegalStateException();
    }

    public InputStream getResponseBodyBytes() {
        InputStream result = this.responseBodyBytes;
        if (result != null) {
            return result;
        }
        InputStream result2 = Okio.buffer(getResponseBody()).inputStream();
        this.responseBodyBytes = result2;
        return result2;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public HttpEngine recover(IOException e, Sink requestBodyOut2) {
        if (!(this.routeSelector == null || this.connection == null)) {
            this.routeSelector.connectFailed(this.connection, e);
        }
        boolean canRetryRequestBody = requestBodyOut2 == null || (requestBodyOut2 instanceof RetryableSink);
        if ((this.routeSelector == null && this.connection == null) || ((this.routeSelector != null && !this.routeSelector.hasNext()) || !isRecoverable(e) || !canRetryRequestBody)) {
            return null;
        }
        return new HttpEngine(this.client, this.userRequest, this.bufferRequestBody, close(), this.routeSelector, (RetryableSink) requestBodyOut2, this.priorResponse);
    }

    public HttpEngine recover(IOException e) {
        return recover(e, this.requestBodyOut);
    }

    private boolean isRecoverable(IOException e) {
        boolean sslFailure;
        if (!(e instanceof SSLHandshakeException) || !(e.getCause() instanceof CertificateException)) {
            sslFailure = false;
        } else {
            sslFailure = true;
        }
        return !sslFailure && !(e instanceof ProtocolException);
    }

    public Route getRoute() {
        return this.route;
    }

    private void maybeCache() throws IOException {
        InternalCache responseCache = Internal.instance.internalCache(this.client);
        if (responseCache != null) {
            if (CacheStrategy.isCacheable(this.userResponse, this.networkRequest)) {
                this.storeRequest = responseCache.put(stripBody(this.userResponse));
            } else if (HttpMethod.invalidatesCache(this.networkRequest.method())) {
                try {
                    responseCache.remove(this.networkRequest);
                } catch (IOException e) {
                }
            }
        }
    }

    public void releaseConnection() throws IOException {
        if (!(this.transport == null || this.connection == null)) {
            this.transport.releaseConnectionOnIdle();
        }
        this.connection = null;
    }

    public void disconnect() {
        if (this.transport != null) {
            try {
                this.transport.disconnect(this);
            } catch (IOException e) {
            }
        }
    }

    public Connection close() {
        if (this.bufferedRequestBody != null) {
            Util.closeQuietly(this.bufferedRequestBody);
        } else if (this.requestBodyOut != null) {
            Util.closeQuietly(this.requestBodyOut);
        }
        if (this.responseBody == null) {
            if (this.connection != null) {
                Util.closeQuietly(this.connection.getSocket());
            }
            this.connection = null;
            return null;
        }
        Util.closeQuietly(this.responseBody);
        Util.closeQuietly(this.responseBodyBytes);
        if (this.transport == null || this.connection == null || this.transport.canReuseConnection()) {
            if (this.connection != null && !Internal.instance.clearOwner(this.connection)) {
                this.connection = null;
            }
            Connection connection2 = this.connection;
            this.connection = null;
            return connection2;
        }
        Util.closeQuietly(this.connection.getSocket());
        this.connection = null;
        return null;
    }

    private void initContentStream(Source transferSource) throws IOException {
        this.responseTransferSource = transferSource;
        if (!this.transparentGzip || !"gzip".equalsIgnoreCase(this.userResponse.header("Content-Encoding"))) {
            this.responseBody = Okio.buffer(transferSource);
            return;
        }
        this.userResponse = this.userResponse.newBuilder().removeHeader("Content-Encoding").removeHeader("Content-Length").build();
        this.responseBody = Okio.buffer(new GzipSource(transferSource));
    }

    public boolean hasResponseBody() {
        if (this.userRequest.method().equals("HEAD")) {
            return false;
        }
        int responseCode = this.userResponse.code();
        if ((responseCode < 100 || responseCode >= 200) && responseCode != 204 && responseCode != 304) {
            return true;
        }
        if (OkHeaders.contentLength(this.networkResponse) != -1 || "chunked".equalsIgnoreCase(this.networkResponse.header("Transfer-Encoding"))) {
            return true;
        }
        return false;
    }

    private Request networkRequest(Request request) throws IOException {
        Request.Builder result = request.newBuilder();
        if (request.header("Host") == null) {
            result.header("Host", hostHeader(request.url()));
        }
        if ((this.connection == null || this.connection.getProtocol() != Protocol.HTTP_1_0) && request.header("Connection") == null) {
            result.header("Connection", "Keep-Alive");
        }
        if (request.header("Accept-Encoding") == null) {
            this.transparentGzip = true;
            result.header("Accept-Encoding", "gzip");
        }
        CookieHandler cookieHandler = this.client.getCookieHandler();
        if (cookieHandler != null) {
            OkHeaders.addCookies(result, cookieHandler.get(request.uri(), OkHeaders.toMultimap(result.build().headers(), null)));
        }
        return result.build();
    }

    public static String hostHeader(URL url) {
        return Util.getEffectivePort(url) != Util.getDefaultPort(url.getProtocol()) ? url.getHost() + ":" + url.getPort() : url.getHost();
    }

    public void readResponse() throws IOException {
        if (this.userResponse == null) {
            if (this.networkRequest == null && this.cacheResponse == null) {
                throw new IllegalStateException("call sendRequest() first!");
            } else if (this.networkRequest != null) {
                if (this.bufferedRequestBody != null && this.bufferedRequestBody.buffer().size() > 0) {
                    this.bufferedRequestBody.flush();
                }
                if (this.sentRequestMillis == -1) {
                    if (OkHeaders.contentLength(this.networkRequest) == -1 && (this.requestBodyOut instanceof RetryableSink)) {
                        this.networkRequest = this.networkRequest.newBuilder().header("Content-Length", Long.toString(((RetryableSink) this.requestBodyOut).contentLength())).build();
                    }
                    this.transport.writeRequestHeaders(this.networkRequest);
                }
                if (this.requestBodyOut != null) {
                    if (this.bufferedRequestBody != null) {
                        this.bufferedRequestBody.close();
                    } else {
                        this.requestBodyOut.close();
                    }
                    if ((this.requestBodyOut instanceof RetryableSink) && !Util.emptySink().equals(this.requestBodyOut)) {
                        this.transport.writeRequestBody((RetryableSink) this.requestBodyOut);
                    }
                }
                this.transport.flushRequest();
                this.networkResponse = this.transport.readResponseHeaders().request(this.networkRequest).handshake(this.connection.getHandshake()).header(OkHeaders.SENT_MILLIS, Long.toString(this.sentRequestMillis)).header(OkHeaders.RECEIVED_MILLIS, Long.toString(System.currentTimeMillis())).build();
                Internal.instance.setProtocol(this.connection, this.networkResponse.protocol());
                receiveHeaders(this.networkResponse.headers());
                if (this.cacheResponse != null) {
                    if (validate(this.cacheResponse, this.networkResponse)) {
                        this.userResponse = this.cacheResponse.newBuilder().request(this.userRequest).priorResponse(stripBody(this.priorResponse)).headers(combine(this.cacheResponse.headers(), this.networkResponse.headers())).cacheResponse(stripBody(this.cacheResponse)).networkResponse(stripBody(this.networkResponse)).build();
                        this.transport.emptyTransferStream();
                        releaseConnection();
                        InternalCache responseCache = Internal.instance.internalCache(this.client);
                        responseCache.trackConditionalCacheHit();
                        responseCache.update(this.cacheResponse, stripBody(this.userResponse));
                        if (this.cacheResponse.body() != null) {
                            initContentStream(this.cacheResponse.body().source());
                            return;
                        }
                        return;
                    }
                    Util.closeQuietly(this.cacheResponse.body());
                }
                this.userResponse = this.networkResponse.newBuilder().request(this.userRequest).priorResponse(stripBody(this.priorResponse)).cacheResponse(stripBody(this.cacheResponse)).networkResponse(stripBody(this.networkResponse)).build();
                if (!hasResponseBody()) {
                    this.responseTransferSource = this.transport.getTransferStream(this.storeRequest);
                    this.responseBody = Okio.buffer(this.responseTransferSource);
                    return;
                }
                maybeCache();
                initContentStream(this.transport.getTransferStream(this.storeRequest));
            }
        }
    }

    private static boolean validate(Response cached, Response network) {
        Date networkLastModified;
        if (network.code() == 304) {
            return true;
        }
        Date lastModified = cached.headers().getDate("Last-Modified");
        if (lastModified == null || (networkLastModified = network.headers().getDate("Last-Modified")) == null || networkLastModified.getTime() >= lastModified.getTime()) {
            return false;
        }
        return true;
    }

    private static Headers combine(Headers cachedHeaders, Headers networkHeaders) throws IOException {
        Headers.Builder result = new Headers.Builder();
        for (int i = 0; i < cachedHeaders.size(); i++) {
            String fieldName = cachedHeaders.name(i);
            String value = cachedHeaders.value(i);
            if ((!"Warning".equals(fieldName) || !value.startsWith("1")) && (!OkHeaders.isEndToEnd(fieldName) || networkHeaders.get(fieldName) == null)) {
                result.add(fieldName, value);
            }
        }
        for (int i2 = 0; i2 < networkHeaders.size(); i2++) {
            String fieldName2 = networkHeaders.name(i2);
            if (OkHeaders.isEndToEnd(fieldName2)) {
                result.add(fieldName2, networkHeaders.value(i2));
            }
        }
        return result.build();
    }

    private Request tunnelRequest(Connection connection2, Request request) throws IOException {
        if (!connection2.getRoute().requiresTunnel()) {
            return null;
        }
        String host = request.url().getHost();
        int port = Util.getEffectivePort(request.url());
        Request.Builder result = new Request.Builder().url(new URL("https", host, port, "/")).header("Host", port == Util.getDefaultPort("https") ? host : host + ":" + port).header("Proxy-Connection", "Keep-Alive");
        String userAgent = request.header("User-Agent");
        if (userAgent != null) {
            result.header("User-Agent", userAgent);
        }
        String proxyAuthorization = request.header("Proxy-Authorization");
        if (proxyAuthorization != null) {
            result.header("Proxy-Authorization", proxyAuthorization);
        }
        return result.build();
    }

    public void receiveHeaders(Headers headers) throws IOException {
        CookieHandler cookieHandler = this.client.getCookieHandler();
        if (cookieHandler != null) {
            cookieHandler.put(this.userRequest.uri(), OkHeaders.toMultimap(headers, null));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.squareup.okhttp.Request followUpRequest() throws java.io.IOException {
        /*
            r9 = this;
            r6 = 0
            com.squareup.okhttp.Response r7 = r9.userResponse
            if (r7 != 0) goto L_0x000b
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            r6.<init>()
            throw r6
        L_0x000b:
            com.squareup.okhttp.Route r7 = r9.getRoute()
            if (r7 == 0) goto L_0x0023
            com.squareup.okhttp.Route r7 = r9.getRoute()
            java.net.Proxy r4 = r7.getProxy()
        L_0x0019:
            com.squareup.okhttp.Response r7 = r9.userResponse
            int r2 = r7.code()
            switch(r2) {
                case 300: goto L_0x0063;
                case 301: goto L_0x0063;
                case 302: goto L_0x0063;
                case 303: goto L_0x0063;
                case 307: goto L_0x0047;
                case 401: goto L_0x003a;
                case 407: goto L_0x002a;
                default: goto L_0x0022;
            }
        L_0x0022:
            return r6
        L_0x0023:
            com.squareup.okhttp.OkHttpClient r7 = r9.client
            java.net.Proxy r4 = r7.getProxy()
            goto L_0x0019
        L_0x002a:
            java.net.Proxy$Type r6 = r4.type()
            java.net.Proxy$Type r7 = java.net.Proxy.Type.HTTP
            if (r6 == r7) goto L_0x003a
            java.net.ProtocolException r6 = new java.net.ProtocolException
            java.lang.String r7 = "Received HTTP_PROXY_AUTH (407) code while not using proxy"
            r6.<init>(r7)
            throw r6
        L_0x003a:
            com.squareup.okhttp.OkHttpClient r6 = r9.client
            com.squareup.okhttp.Authenticator r6 = r6.getAuthenticator()
            com.squareup.okhttp.Response r7 = r9.userResponse
            com.squareup.okhttp.Request r6 = com.squareup.okhttp.internal.http.OkHeaders.processAuthHeader(r6, r7, r4)
            goto L_0x0022
        L_0x0047:
            com.squareup.okhttp.Request r7 = r9.userRequest
            java.lang.String r7 = r7.method()
            java.lang.String r8 = "GET"
            boolean r7 = r7.equals(r8)
            if (r7 != 0) goto L_0x0063
            com.squareup.okhttp.Request r7 = r9.userRequest
            java.lang.String r7 = r7.method()
            java.lang.String r8 = "HEAD"
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x0022
        L_0x0063:
            com.squareup.okhttp.Response r7 = r9.userResponse
            java.lang.String r8 = "Location"
            java.lang.String r0 = r7.header(r8)
            if (r0 == 0) goto L_0x0022
            java.net.URL r5 = new java.net.URL
            com.squareup.okhttp.Request r7 = r9.userRequest
            java.net.URL r7 = r7.url()
            r5.<init>(r7, r0)
            java.lang.String r7 = r5.getProtocol()
            java.lang.String r8 = "https"
            boolean r7 = r7.equals(r8)
            if (r7 != 0) goto L_0x0090
            java.lang.String r7 = r5.getProtocol()
            java.lang.String r8 = "http"
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x0022
        L_0x0090:
            java.lang.String r7 = r5.getProtocol()
            com.squareup.okhttp.Request r8 = r9.userRequest
            java.net.URL r8 = r8.url()
            java.lang.String r8 = r8.getProtocol()
            boolean r3 = r7.equals(r8)
            if (r3 != 0) goto L_0x00ac
            com.squareup.okhttp.OkHttpClient r7 = r9.client
            boolean r7 = r7.getFollowSslRedirects()
            if (r7 == 0) goto L_0x0022
        L_0x00ac:
            com.squareup.okhttp.Request r7 = r9.userRequest
            com.squareup.okhttp.Request$Builder r1 = r7.newBuilder()
            com.squareup.okhttp.Request r7 = r9.userRequest
            java.lang.String r7 = r7.method()
            boolean r7 = com.squareup.okhttp.internal.http.HttpMethod.hasRequestBody(r7)
            if (r7 == 0) goto L_0x00d2
            java.lang.String r7 = "GET"
            r1.method(r7, r6)
            java.lang.String r6 = "Transfer-Encoding"
            r1.removeHeader(r6)
            java.lang.String r6 = "Content-Length"
            r1.removeHeader(r6)
            java.lang.String r6 = "Content-Type"
            r1.removeHeader(r6)
        L_0x00d2:
            boolean r6 = r9.sameConnection(r5)
            if (r6 != 0) goto L_0x00dd
            java.lang.String r6 = "Authorization"
            r1.removeHeader(r6)
        L_0x00dd:
            com.squareup.okhttp.Request$Builder r6 = r1.url(r5)
            com.squareup.okhttp.Request r6 = r6.build()
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.http.HttpEngine.followUpRequest():com.squareup.okhttp.Request");
    }

    public boolean sameConnection(URL followUp) {
        URL url = this.userRequest.url();
        return url.getHost().equals(followUp.getHost()) && Util.getEffectivePort(url) == Util.getEffectivePort(followUp) && url.getProtocol().equals(followUp.getProtocol());
    }
}
