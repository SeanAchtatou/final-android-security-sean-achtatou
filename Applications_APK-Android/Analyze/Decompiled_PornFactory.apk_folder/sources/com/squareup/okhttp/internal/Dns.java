package com.squareup.okhttp.internal;

import java.net.InetAddress;
import java.net.UnknownHostException;

public interface Dns {
    public static final Dns DEFAULT = new Dns() {
        public InetAddress[] getAllByName(String host) throws UnknownHostException {
            if (host != null) {
                return InetAddress.getAllByName(host);
            }
            throw new UnknownHostException("host == null");
        }
    };

    InetAddress[] getAllByName(String str) throws UnknownHostException;
}
