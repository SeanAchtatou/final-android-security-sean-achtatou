package com.squareup.okhttp.internal;

import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.http.RouteSelector;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;
import okio.Buffer;

public class Platform {
    private static final Platform PLATFORM = findPlatform();

    public static Platform get() {
        return PLATFORM;
    }

    public String getPrefix() {
        return "OkHttp";
    }

    public void logW(String warning) {
        System.out.println(warning);
    }

    public void tagSocket(Socket socket) throws SocketException {
    }

    public void untagSocket(Socket socket) throws SocketException {
    }

    public URI toUriLenient(URL url) throws URISyntaxException {
        return url.toURI();
    }

    public void configureTls(SSLSocket socket, String uriHost, String tlsVersion) {
        if (tlsVersion.equals(RouteSelector.SSL_V3)) {
            socket.setEnabledProtocols(new String[]{RouteSelector.SSL_V3});
        }
    }

    public String getSelectedProtocol(SSLSocket socket) {
        return null;
    }

    public void setProtocols(SSLSocket socket, List<Protocol> list) {
    }

    public void connectSocket(Socket socket, InetSocketAddress address, int connectTimeout) throws IOException {
        socket.connect(address, connectTimeout);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0070, code lost:
        r3 = java.lang.Class.forName("org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0078, code lost:
        r15 = "org.eclipse.jetty.alpn.ALPN";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r14 = java.lang.Class.forName(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r16 = java.lang.Class.forName(r15 + "$Provider");
        r11 = java.lang.Class.forName(r15 + "$ClientProvider");
        r18 = java.lang.Class.forName(r15 + "$ServerProvider");
        r17 = r14.getMethod("put", javax.net.ssl.SSLSocket.class, r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00f8, code lost:
        r15 = "org.eclipse.jetty.npn.NextProtoNego";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r14 = java.lang.Class.forName(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return new com.squareup.okhttp.internal.Platform.JdkWithJettyBootPlatform(r17, r14.getMethod("get", javax.net.ssl.SSLSocket.class), r11, r18);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0077 A[ExcHandler: NoSuchMethodException (e java.lang.NoSuchMethodException), Splitter:B:1:0x0002] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ff A[ExcHandler: NoSuchMethodException (e java.lang.NoSuchMethodException), Splitter:B:18:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0101 A[ExcHandler: ClassNotFoundException (e java.lang.ClassNotFoundException), Splitter:B:4:0x0009] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.squareup.okhttp.internal.Platform findPlatform() {
        /*
            java.lang.String r2 = "com.android.org.conscrypt.OpenSSLSocketImpl"
            java.lang.Class r3 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException -> 0x006f, NoSuchMethodException -> 0x0077 }
        L_0x0006:
            java.lang.String r2 = "setUseSessionTickets"
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
            r20 = 0
            java.lang.Class r21 = java.lang.Boolean.TYPE     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
            r10[r20] = r21     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
            java.lang.reflect.Method r4 = r3.getMethod(r2, r10)     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
            java.lang.String r2 = "setHostname"
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
            r20 = 0
            java.lang.Class<java.lang.String> r21 = java.lang.String.class
            r10[r20] = r21     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
            java.lang.reflect.Method r5 = r3.getMethod(r2, r10)     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
            r6 = 0
            r7 = 0
            java.lang.String r2 = "android.net.TrafficStats"
            java.lang.Class r19 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x0107 }
            java.lang.String r2 = "tagSocket"
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x0107 }
            r20 = 0
            java.lang.Class<java.net.Socket> r21 = java.net.Socket.class
            r10[r20] = r21     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x0107 }
            r0 = r19
            java.lang.reflect.Method r6 = r0.getMethod(r2, r10)     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x0107 }
            java.lang.String r2 = "untagSocket"
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x0107 }
            r20 = 0
            java.lang.Class<java.net.Socket> r21 = java.net.Socket.class
            r10[r20] = r21     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x0107 }
            r0 = r19
            java.lang.reflect.Method r7 = r0.getMethod(r2, r10)     // Catch:{ ClassNotFoundException -> 0x010a, NoSuchMethodException -> 0x0107 }
        L_0x004e:
            r8 = 0
            r9 = 0
            java.lang.String r2 = "setNpnProtocols"
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ NoSuchMethodException -> 0x0104, ClassNotFoundException -> 0x0101 }
            r20 = 0
            java.lang.Class<byte[]> r21 = byte[].class
            r10[r20] = r21     // Catch:{ NoSuchMethodException -> 0x0104, ClassNotFoundException -> 0x0101 }
            java.lang.reflect.Method r8 = r3.getMethod(r2, r10)     // Catch:{ NoSuchMethodException -> 0x0104, ClassNotFoundException -> 0x0101 }
            java.lang.String r2 = "getNpnSelectedProtocol"
            r10 = 0
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ NoSuchMethodException -> 0x0104, ClassNotFoundException -> 0x0101 }
            java.lang.reflect.Method r9 = r3.getMethod(r2, r10)     // Catch:{ NoSuchMethodException -> 0x0104, ClassNotFoundException -> 0x0101 }
        L_0x0068:
            com.squareup.okhttp.internal.Platform$Android r2 = new com.squareup.okhttp.internal.Platform$Android     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
            r10 = 0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
        L_0x006e:
            return r2
        L_0x006f:
            r13 = move-exception
            java.lang.String r2 = "org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl"
            java.lang.Class r3 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException -> 0x0101, NoSuchMethodException -> 0x0077 }
            goto L_0x0006
        L_0x0077:
            r2 = move-exception
        L_0x0078:
            java.lang.String r15 = "org.eclipse.jetty.alpn.ALPN"
            java.lang.Class r14 = java.lang.Class.forName(r15)     // Catch:{ ClassNotFoundException -> 0x00f7, NoSuchMethodException -> 0x00ff }
        L_0x007e:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            r2.<init>()     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.StringBuilder r2 = r2.append(r15)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.String r10 = "$Provider"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.String r2 = r2.toString()     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.Class r16 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            r2.<init>()     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.StringBuilder r2 = r2.append(r15)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.String r10 = "$ClientProvider"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.String r2 = r2.toString()     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.Class r11 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            r2.<init>()     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.StringBuilder r2 = r2.append(r15)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.String r10 = "$ServerProvider"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.String r2 = r2.toString()     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.Class r18 = java.lang.Class.forName(r2)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.String r2 = "put"
            r10 = 2
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            r20 = 0
            java.lang.Class<javax.net.ssl.SSLSocket> r21 = javax.net.ssl.SSLSocket.class
            r10[r20] = r21     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            r20 = 1
            r10[r20] = r16     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.reflect.Method r17 = r14.getMethod(r2, r10)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.String r2 = "get"
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            r20 = 0
            java.lang.Class<javax.net.ssl.SSLSocket> r21 = javax.net.ssl.SSLSocket.class
            r10[r20] = r21     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            java.lang.reflect.Method r12 = r14.getMethod(r2, r10)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            com.squareup.okhttp.internal.Platform$JdkWithJettyBootPlatform r2 = new com.squareup.okhttp.internal.Platform$JdkWithJettyBootPlatform     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            r0 = r17
            r1 = r18
            r2.<init>(r0, r12, r11, r1)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            goto L_0x006e
        L_0x00ef:
            r2 = move-exception
        L_0x00f0:
            com.squareup.okhttp.internal.Platform r2 = new com.squareup.okhttp.internal.Platform
            r2.<init>()
            goto L_0x006e
        L_0x00f7:
            r13 = move-exception
            java.lang.String r15 = "org.eclipse.jetty.npn.NextProtoNego"
            java.lang.Class r14 = java.lang.Class.forName(r15)     // Catch:{ ClassNotFoundException -> 0x00ef, NoSuchMethodException -> 0x00ff }
            goto L_0x007e
        L_0x00ff:
            r2 = move-exception
            goto L_0x00f0
        L_0x0101:
            r2 = move-exception
            goto L_0x0078
        L_0x0104:
            r2 = move-exception
            goto L_0x0068
        L_0x0107:
            r2 = move-exception
            goto L_0x004e
        L_0x010a:
            r2 = move-exception
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.internal.Platform.findPlatform():com.squareup.okhttp.internal.Platform");
    }

    private static class Android extends Platform {
        private final Method getNpnSelectedProtocol;
        protected final Class<?> openSslSocketClass;
        private final Method setHostname;
        private final Method setNpnProtocols;
        private final Method setUseSessionTickets;
        private final Method trafficStatsTagSocket;
        private final Method trafficStatsUntagSocket;

        private Android(Class<?> openSslSocketClass2, Method setUseSessionTickets2, Method setHostname2, Method trafficStatsTagSocket2, Method trafficStatsUntagSocket2, Method setNpnProtocols2, Method getNpnSelectedProtocol2) {
            this.openSslSocketClass = openSslSocketClass2;
            this.setUseSessionTickets = setUseSessionTickets2;
            this.setHostname = setHostname2;
            this.trafficStatsTagSocket = trafficStatsTagSocket2;
            this.trafficStatsUntagSocket = trafficStatsUntagSocket2;
            this.setNpnProtocols = setNpnProtocols2;
            this.getNpnSelectedProtocol = getNpnSelectedProtocol2;
        }

        public void connectSocket(Socket socket, InetSocketAddress address, int connectTimeout) throws IOException {
            try {
                socket.connect(address, connectTimeout);
            } catch (SecurityException se) {
                IOException ioException = new IOException("Exception in connect");
                ioException.initCause(se);
                throw ioException;
            }
        }

        public void configureTls(SSLSocket socket, String uriHost, String tlsVersion) {
            Platform.super.configureTls(socket, uriHost, tlsVersion);
            if (tlsVersion.equals(RouteSelector.TLS_V1) && this.openSslSocketClass.isInstance(socket)) {
                try {
                    this.setUseSessionTickets.invoke(socket, true);
                    this.setHostname.invoke(socket, uriHost);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e2) {
                    throw new AssertionError(e2);
                }
            }
        }

        public void setProtocols(SSLSocket socket, List<Protocol> protocols) {
            if (this.setNpnProtocols != null && this.openSslSocketClass.isInstance(socket)) {
                try {
                    this.setNpnProtocols.invoke(socket, concatLengthPrefixed(protocols));
                } catch (IllegalAccessException e) {
                    throw new AssertionError(e);
                } catch (InvocationTargetException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }

        public String getSelectedProtocol(SSLSocket socket) {
            if (this.getNpnSelectedProtocol == null) {
                return null;
            }
            if (!this.openSslSocketClass.isInstance(socket)) {
                return null;
            }
            try {
                byte[] npnResult = (byte[]) this.getNpnSelectedProtocol.invoke(socket, new Object[0]);
                if (npnResult == null) {
                    return null;
                }
                return new String(npnResult, Util.UTF_8);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        public void tagSocket(Socket socket) throws SocketException {
            if (this.trafficStatsTagSocket != null) {
                try {
                    this.trafficStatsTagSocket.invoke(null, socket);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }

        public void untagSocket(Socket socket) throws SocketException {
            if (this.trafficStatsUntagSocket != null) {
                try {
                    this.trafficStatsUntagSocket.invoke(null, socket);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }
    }

    private static class JdkWithJettyBootPlatform extends Platform {
        private final Class<?> clientProviderClass;
        private final Method getMethod;
        private final Method putMethod;
        private final Class<?> serverProviderClass;

        public JdkWithJettyBootPlatform(Method putMethod2, Method getMethod2, Class<?> clientProviderClass2, Class<?> serverProviderClass2) {
            this.putMethod = putMethod2;
            this.getMethod = getMethod2;
            this.clientProviderClass = clientProviderClass2;
            this.serverProviderClass = serverProviderClass2;
        }

        public void setProtocols(SSLSocket socket, List<Protocol> protocols) {
            try {
                List<String> names = new ArrayList<>(protocols.size());
                int size = protocols.size();
                for (int i = 0; i < size; i++) {
                    Protocol protocol = protocols.get(i);
                    if (protocol != Protocol.HTTP_1_0) {
                        names.add(protocol.toString());
                    }
                }
                Object provider = Proxy.newProxyInstance(Platform.class.getClassLoader(), new Class[]{this.clientProviderClass, this.serverProviderClass}, new JettyNegoProvider(names));
                this.putMethod.invoke(null, socket, provider);
            } catch (InvocationTargetException e) {
                throw new AssertionError(e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        public String getSelectedProtocol(SSLSocket socket) {
            try {
                JettyNegoProvider provider = (JettyNegoProvider) Proxy.getInvocationHandler(this.getMethod.invoke(null, socket));
                if (!provider.unsupported && provider.selected == null) {
                    Logger.getLogger("com.squareup.okhttp.OkHttpClient").log(Level.INFO, "NPN/ALPN callback dropped: SPDY and HTTP/2 are disabled. Is npn-boot or alpn-boot on the boot class path?");
                    return null;
                } else if (!provider.unsupported) {
                    return provider.selected;
                } else {
                    return null;
                }
            } catch (InvocationTargetException e) {
                throw new AssertionError();
            } catch (IllegalAccessException e2) {
                throw new AssertionError();
            }
        }
    }

    private static class JettyNegoProvider implements InvocationHandler {
        private final List<String> protocols;
        /* access modifiers changed from: private */
        public String selected;
        /* access modifiers changed from: private */
        public boolean unsupported;

        public JettyNegoProvider(List<String> protocols2) {
            this.protocols = protocols2;
        }

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            String methodName = method.getName();
            Class<?> returnType = method.getReturnType();
            if (args == null) {
                args = Util.EMPTY_STRING_ARRAY;
            }
            if (methodName.equals("supports") && Boolean.TYPE == returnType) {
                return true;
            }
            if (methodName.equals("unsupported") && Void.TYPE == returnType) {
                this.unsupported = true;
                return null;
            } else if (methodName.equals("protocols") && args.length == 0) {
                return this.protocols;
            } else {
                if ((methodName.equals("selectProtocol") || methodName.equals("select")) && String.class == returnType && args.length == 1 && (args[0] instanceof List)) {
                    List<String> peerProtocols = (List) args[0];
                    int size = peerProtocols.size();
                    for (int i = 0; i < size; i++) {
                        if (this.protocols.contains(peerProtocols.get(i))) {
                            String str = (String) peerProtocols.get(i);
                            this.selected = str;
                            return str;
                        }
                    }
                    String str2 = this.protocols.get(0);
                    this.selected = str2;
                    return str2;
                } else if ((!methodName.equals("protocolSelected") && !methodName.equals("selected")) || args.length != 1) {
                    return method.invoke(this, args);
                } else {
                    this.selected = (String) args[0];
                    return null;
                }
            }
        }
    }

    static byte[] concatLengthPrefixed(List<Protocol> protocols) {
        Buffer result = new Buffer();
        int size = protocols.size();
        for (int i = 0; i < size; i++) {
            Protocol protocol = protocols.get(i);
            if (protocol != Protocol.HTTP_1_0) {
                result.writeByte(protocol.toString().length());
                result.writeUtf8(protocol.toString());
            }
        }
        return result.readByteArray();
    }
}
