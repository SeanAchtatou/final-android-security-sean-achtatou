package com.squareup.okhttp;

import com.squareup.okhttp.internal.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import okio.BufferedSink;

public final class MultipartBuilder {
    public static final MediaType ALTERNATIVE = MediaType.parse("multipart/alternative");
    public static final MediaType DIGEST = MediaType.parse("multipart/digest");
    public static final MediaType FORM = MediaType.parse("multipart/form-data");
    public static final MediaType MIXED = MediaType.parse("multipart/mixed");
    public static final MediaType PARALLEL = MediaType.parse("multipart/parallel");
    private final String boundary;
    private final List<RequestBody> partBodies;
    private final List<Headers> partHeaders;
    private MediaType type;

    public MultipartBuilder() {
        this(UUID.randomUUID().toString());
    }

    public MultipartBuilder(String boundary2) {
        this.type = MIXED;
        this.partHeaders = new ArrayList();
        this.partBodies = new ArrayList();
        this.boundary = boundary2;
    }

    public MultipartBuilder type(MediaType type2) {
        if (type2 == null) {
            throw new NullPointerException("type == null");
        } else if (!type2.type().equals("multipart")) {
            throw new IllegalArgumentException("multipart != " + type2);
        } else {
            this.type = type2;
            return this;
        }
    }

    public MultipartBuilder addPart(RequestBody body) {
        return addPart(null, body);
    }

    public MultipartBuilder addPart(Headers headers, RequestBody body) {
        if (body == null) {
            throw new NullPointerException("body == null");
        } else if (headers != null && headers.get("Content-Type") != null) {
            throw new IllegalArgumentException("Unexpected header: Content-Type");
        } else if (headers == null || headers.get("Content-Length") == null) {
            this.partHeaders.add(headers);
            this.partBodies.add(body);
            return this;
        } else {
            throw new IllegalArgumentException("Unexpected header: Content-Length");
        }
    }

    public RequestBody build() {
        if (!this.partHeaders.isEmpty()) {
            return new MultipartRequestBody(this.type, this.boundary, this.partHeaders, this.partBodies);
        }
        throw new IllegalStateException("Multipart body must have at least one part.");
    }

    private static final class MultipartRequestBody extends RequestBody {
        private final String boundary;
        private final MediaType contentType;
        private final List<RequestBody> partBodies;
        private final List<Headers> partHeaders;

        public MultipartRequestBody(MediaType type, String boundary2, List<Headers> partHeaders2, List<RequestBody> partBodies2) {
            if (type == null) {
                throw new NullPointerException("type == null");
            }
            this.boundary = boundary2;
            this.contentType = MediaType.parse(type + "; boundary=" + boundary2);
            this.partHeaders = Util.immutableList(partHeaders2);
            this.partBodies = Util.immutableList(partBodies2);
        }

        public MediaType contentType() {
            return this.contentType;
        }

        public void writeTo(BufferedSink sink) throws IOException {
            byte[] boundary2 = this.boundary.getBytes("UTF-8");
            boolean first = true;
            for (int i = 0; i < this.partHeaders.size(); i++) {
                writeBoundary(sink, boundary2, first, false);
                writePart(sink, this.partHeaders.get(i), this.partBodies.get(i));
                first = false;
            }
            writeBoundary(sink, boundary2, false, true);
        }

        private static void writeBoundary(BufferedSink sink, byte[] boundary2, boolean first, boolean last) throws IOException {
            if (!first) {
                sink.writeUtf8("\r\n");
            }
            sink.writeUtf8("--");
            sink.write(boundary2);
            if (last) {
                sink.writeUtf8("--");
            } else {
                sink.writeUtf8("\r\n");
            }
        }

        private void writePart(BufferedSink sink, Headers headers, RequestBody body) throws IOException {
            if (headers != null) {
                for (int i = 0; i < headers.size(); i++) {
                    sink.writeUtf8(headers.name(i)).writeUtf8(": ").writeUtf8(headers.value(i)).writeUtf8("\r\n");
                }
            }
            MediaType contentType2 = body.contentType();
            if (contentType2 != null) {
                sink.writeUtf8("Content-Type: ").writeUtf8(contentType2.toString()).writeUtf8("\r\n");
            }
            long contentLength = body.contentLength();
            if (contentLength != -1) {
                sink.writeUtf8("Content-Length: ").writeUtf8(Long.toString(contentLength)).writeUtf8("\r\n");
            }
            sink.writeUtf8("\r\n");
            body.writeTo(sink);
        }
    }
}
