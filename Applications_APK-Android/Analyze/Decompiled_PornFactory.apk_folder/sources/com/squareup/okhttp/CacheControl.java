package com.squareup.okhttp;

import com.squareup.okhttp.internal.http.HeaderParser;

public final class CacheControl {
    private final boolean isPublic;
    private final int maxAgeSeconds;
    private final int maxStaleSeconds;
    private final int minFreshSeconds;
    private final boolean mustRevalidate;
    private final boolean noCache;
    private final boolean noStore;
    private final boolean onlyIfCached;
    private final int sMaxAgeSeconds;

    private CacheControl(boolean noCache2, boolean noStore2, int maxAgeSeconds2, int sMaxAgeSeconds2, boolean isPublic2, boolean mustRevalidate2, int maxStaleSeconds2, int minFreshSeconds2, boolean onlyIfCached2) {
        this.noCache = noCache2;
        this.noStore = noStore2;
        this.maxAgeSeconds = maxAgeSeconds2;
        this.sMaxAgeSeconds = sMaxAgeSeconds2;
        this.isPublic = isPublic2;
        this.mustRevalidate = mustRevalidate2;
        this.maxStaleSeconds = maxStaleSeconds2;
        this.minFreshSeconds = minFreshSeconds2;
        this.onlyIfCached = onlyIfCached2;
    }

    public boolean noCache() {
        return this.noCache;
    }

    public boolean noStore() {
        return this.noStore;
    }

    public int maxAgeSeconds() {
        return this.maxAgeSeconds;
    }

    public int sMaxAgeSeconds() {
        return this.sMaxAgeSeconds;
    }

    public boolean isPublic() {
        return this.isPublic;
    }

    public boolean mustRevalidate() {
        return this.mustRevalidate;
    }

    public int maxStaleSeconds() {
        return this.maxStaleSeconds;
    }

    public int minFreshSeconds() {
        return this.minFreshSeconds;
    }

    public boolean onlyIfCached() {
        return this.onlyIfCached;
    }

    public static CacheControl parse(Headers headers) {
        String parameter;
        boolean noCache2 = false;
        boolean noStore2 = false;
        int maxAgeSeconds2 = -1;
        int sMaxAgeSeconds2 = -1;
        boolean isPublic2 = false;
        boolean mustRevalidate2 = false;
        int maxStaleSeconds2 = -1;
        int minFreshSeconds2 = -1;
        boolean onlyIfCached2 = false;
        for (int i = 0; i < headers.size(); i++) {
            if (headers.name(i).equalsIgnoreCase("Cache-Control") || headers.name(i).equalsIgnoreCase("Pragma")) {
                String string = headers.value(i);
                int pos = 0;
                while (pos < string.length()) {
                    int tokenStart = pos;
                    int pos2 = HeaderParser.skipUntil(string, pos, "=,;");
                    String directive = string.substring(tokenStart, pos2).trim();
                    if (pos2 == string.length() || string.charAt(pos2) == ',' || string.charAt(pos2) == ';') {
                        pos = pos2 + 1;
                        parameter = null;
                    } else {
                        int pos3 = HeaderParser.skipWhitespace(string, pos2 + 1);
                        if (pos3 >= string.length() || string.charAt(pos3) != '\"') {
                            int parameterStart = pos3;
                            pos = HeaderParser.skipUntil(string, pos3, ",;");
                            parameter = string.substring(parameterStart, pos).trim();
                        } else {
                            int pos4 = pos3 + 1;
                            int parameterStart2 = pos4;
                            int pos5 = HeaderParser.skipUntil(string, pos4, "\"");
                            parameter = string.substring(parameterStart2, pos5);
                            pos = pos5 + 1;
                        }
                    }
                    if ("no-cache".equalsIgnoreCase(directive)) {
                        noCache2 = true;
                    } else if ("no-store".equalsIgnoreCase(directive)) {
                        noStore2 = true;
                    } else if ("max-age".equalsIgnoreCase(directive)) {
                        maxAgeSeconds2 = HeaderParser.parseSeconds(parameter);
                    } else if ("s-maxage".equalsIgnoreCase(directive)) {
                        sMaxAgeSeconds2 = HeaderParser.parseSeconds(parameter);
                    } else if ("public".equalsIgnoreCase(directive)) {
                        isPublic2 = true;
                    } else if ("must-revalidate".equalsIgnoreCase(directive)) {
                        mustRevalidate2 = true;
                    } else if ("max-stale".equalsIgnoreCase(directive)) {
                        maxStaleSeconds2 = HeaderParser.parseSeconds(parameter);
                    } else if ("min-fresh".equalsIgnoreCase(directive)) {
                        minFreshSeconds2 = HeaderParser.parseSeconds(parameter);
                    } else if ("only-if-cached".equalsIgnoreCase(directive)) {
                        onlyIfCached2 = true;
                    }
                }
            }
        }
        return new CacheControl(noCache2, noStore2, maxAgeSeconds2, sMaxAgeSeconds2, isPublic2, mustRevalidate2, maxStaleSeconds2, minFreshSeconds2, onlyIfCached2);
    }
}
