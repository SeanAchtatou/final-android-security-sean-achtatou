package com.dd.plist;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

public class NSString extends NSObject implements Comparable<Object> {
    private static CharsetEncoder asciiEncoder;
    private static CharsetEncoder utf16beEncoder;
    private static CharsetEncoder utf8Encoder;
    private String content;

    public NSString(byte[] bytes, String encoding) throws UnsupportedEncodingException {
        this.content = new String(bytes, encoding);
    }

    public NSString(String string) {
        this.content = string;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String c) {
        this.content = c;
    }

    public void append(NSString s) {
        append(s.getContent());
    }

    public void append(String s) {
        this.content += s;
    }

    public void prepend(String s) {
        this.content = s + this.content;
    }

    public void prepend(NSString s) {
        prepend(s.getContent());
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof NSString)) {
            return false;
        }
        return this.content.equals(((NSString) obj).content);
    }

    public int hashCode() {
        return this.content.hashCode();
    }

    public String toString() {
        return this.content;
    }

    /* access modifiers changed from: package-private */
    public void toXML(StringBuilder xml, int level) {
        indent(xml, level);
        xml.append("<string>");
        synchronized (NSString.class) {
            if (utf8Encoder == null) {
                utf8Encoder = Charset.forName("UTF-8").newEncoder();
            } else {
                utf8Encoder.reset();
            }
            try {
                ByteBuffer byteBuf = utf8Encoder.encode(CharBuffer.wrap(this.content));
                byte[] bytes = new byte[byteBuf.remaining()];
                byteBuf.get(bytes);
                this.content = new String(bytes, "UTF-8");
            } catch (Exception ex) {
                throw new RuntimeException("Could not encode the NSString into UTF-8: " + String.valueOf(ex.getMessage()));
            }
        }
        if (this.content.contains("&") || this.content.contains("<") || this.content.contains(">")) {
            xml.append("<![CDATA[");
            xml.append(this.content.replaceAll("]]>", "]]]]><![CDATA[>"));
            xml.append("]]>");
        } else {
            xml.append(this.content);
        }
        xml.append("</string>");
    }

    public void toBinary(BinaryPropertyListWriter out) throws IOException {
        int kind;
        ByteBuffer byteBuf;
        CharBuffer charBuf = CharBuffer.wrap(this.content);
        synchronized (NSString.class) {
            if (asciiEncoder == null) {
                asciiEncoder = Charset.forName("ASCII").newEncoder();
            } else {
                asciiEncoder.reset();
            }
            if (asciiEncoder.canEncode(charBuf)) {
                kind = 5;
                byteBuf = asciiEncoder.encode(charBuf);
            } else {
                if (utf16beEncoder == null) {
                    utf16beEncoder = Charset.forName("UTF-16BE").newEncoder();
                } else {
                    utf16beEncoder.reset();
                }
                kind = 6;
                byteBuf = utf16beEncoder.encode(charBuf);
            }
        }
        byte[] bytes = new byte[byteBuf.remaining()];
        byteBuf.get(bytes);
        out.writeIntHeader(kind, this.content.length());
        out.write(bytes);
    }

    /* access modifiers changed from: protected */
    public void toASCII(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append("\"");
        ascii.append(escapeStringForASCII(this.content));
        ascii.append("\"");
    }

    /* access modifiers changed from: protected */
    public void toASCIIGnuStep(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append("\"");
        ascii.append(escapeStringForASCII(this.content));
        ascii.append("\"");
    }

    static String escapeStringForASCII(String s) {
        String out = "";
        char[] cArray = s.toCharArray();
        for (char c : cArray) {
            if (c > 127) {
                String out2 = out + "\\U";
                String hex = Integer.toHexString(c);
                while (hex.length() < 4) {
                    hex = "0" + hex;
                }
                out = out2 + hex;
            } else if (c == '\\') {
                out = out + "\\\\";
            } else if (c == '\"') {
                out = out + "\\\"";
            } else if (c == 8) {
                out = out + "\\b";
            } else if (c == 10) {
                out = out + "\\n";
            } else if (c == 13) {
                out = out + "\\r";
            } else if (c == 9) {
                out = out + "\\t";
            } else {
                out = out + c;
            }
        }
        return out;
    }

    public int compareTo(Object o) {
        if (o instanceof NSString) {
            return getContent().compareTo(((NSString) o).getContent());
        }
        if (o instanceof String) {
            return getContent().compareTo((String) o);
        }
        return -1;
    }
}
