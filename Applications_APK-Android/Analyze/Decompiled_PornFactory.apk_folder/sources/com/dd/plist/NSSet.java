package com.dd.plist;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class NSSet extends NSObject {
    private boolean ordered;
    private Set<NSObject> set;

    public NSSet() {
        this.ordered = false;
        this.set = new LinkedHashSet();
    }

    public NSSet(boolean ordered2) {
        this.ordered = false;
        this.ordered = ordered2;
        if (!ordered2) {
            this.set = new LinkedHashSet();
        } else {
            this.set = new TreeSet();
        }
    }

    public NSSet(NSObject... objects) {
        this.ordered = false;
        this.set = new LinkedHashSet();
        this.set.addAll(Arrays.asList(objects));
    }

    public NSSet(boolean ordered2, NSObject... objects) {
        this.ordered = false;
        this.ordered = ordered2;
        if (!ordered2) {
            this.set = new LinkedHashSet();
        } else {
            this.set = new TreeSet();
        }
        this.set.addAll(Arrays.asList(objects));
    }

    public synchronized void addObject(NSObject obj) {
        this.set.add(obj);
    }

    public synchronized void removeObject(NSObject obj) {
        this.set.remove(obj);
    }

    public synchronized NSObject[] allObjects() {
        return (NSObject[]) this.set.toArray(new NSObject[count()]);
    }

    public synchronized NSObject anyObject() {
        NSObject next;
        if (this.set.isEmpty()) {
            next = null;
        } else {
            next = this.set.iterator().next();
        }
        return next;
    }

    public boolean containsObject(NSObject obj) {
        return this.set.contains(obj);
    }

    public synchronized NSObject member(NSObject obj) {
        NSObject o;
        Iterator i$ = this.set.iterator();
        while (true) {
            if (!i$.hasNext()) {
                o = null;
                break;
            }
            o = i$.next();
            if (o.equals(obj)) {
                break;
            }
        }
        return o;
    }

    public synchronized boolean intersectsSet(NSSet otherSet) {
        boolean z;
        Iterator i$ = this.set.iterator();
        while (true) {
            if (i$.hasNext()) {
                if (otherSet.containsObject(i$.next())) {
                    z = true;
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        return z;
    }

    public synchronized boolean isSubsetOfSet(NSSet otherSet) {
        boolean z;
        Iterator i$ = this.set.iterator();
        while (true) {
            if (i$.hasNext()) {
                if (!otherSet.containsObject(i$.next())) {
                    z = false;
                    break;
                }
            } else {
                z = true;
                break;
            }
        }
        return z;
    }

    public synchronized Iterator<NSObject> objectIterator() {
        return this.set.iterator();
    }

    /* access modifiers changed from: package-private */
    public Set<NSObject> getSet() {
        return this.set;
    }

    public int hashCode() {
        return (this.set != null ? this.set.hashCode() : 0) + 203;
    }

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NSSet other = (NSSet) obj;
        if (this.set == other.set || (this.set != null && this.set.equals(other.set))) {
            return true;
        }
        return false;
    }

    public synchronized int count() {
        return this.set.size();
    }

    /* access modifiers changed from: package-private */
    public void toXML(StringBuilder xml, int level) {
        indent(xml, level);
        xml.append("<array>");
        xml.append(NSObject.NEWLINE);
        for (NSObject o : this.set) {
            o.toXML(xml, level + 1);
            xml.append(NSObject.NEWLINE);
        }
        indent(xml, level);
        xml.append("</array>");
    }

    /* access modifiers changed from: package-private */
    public void assignIDs(BinaryPropertyListWriter out) {
        super.assignIDs(out);
        for (NSObject obj : this.set) {
            obj.assignIDs(out);
        }
    }

    /* access modifiers changed from: package-private */
    public void toBinary(BinaryPropertyListWriter out) throws IOException {
        if (this.ordered) {
            out.writeIntHeader(11, this.set.size());
        } else {
            out.writeIntHeader(12, this.set.size());
        }
        for (NSObject obj : this.set) {
            out.writeID(out.getID(obj));
        }
    }

    /* access modifiers changed from: protected */
    public void toASCII(StringBuilder ascii, int level) {
        indent(ascii, level);
        NSObject[] array = allObjects();
        ascii.append((char) ASCIIPropertyListParser.ARRAY_BEGIN_TOKEN);
        int indexOfLastNewLine = ascii.lastIndexOf(NEWLINE);
        for (int i = 0; i < array.length; i++) {
            Class<?> objClass = array[i].getClass();
            if ((objClass.equals(NSDictionary.class) || objClass.equals(NSArray.class) || objClass.equals(NSData.class)) && indexOfLastNewLine != ascii.length()) {
                ascii.append(NEWLINE);
                indexOfLastNewLine = ascii.length();
                array[i].toASCII(ascii, level + 1);
            } else {
                if (i != 0) {
                    ascii.append(" ");
                }
                array[i].toASCII(ascii, 0);
            }
            if (i != array.length - 1) {
                ascii.append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN);
            }
            if (ascii.length() - indexOfLastNewLine > 80) {
                ascii.append(NEWLINE);
                indexOfLastNewLine = ascii.length();
            }
        }
        ascii.append((char) ASCIIPropertyListParser.ARRAY_END_TOKEN);
    }

    /* access modifiers changed from: protected */
    public void toASCIIGnuStep(StringBuilder ascii, int level) {
        indent(ascii, level);
        NSObject[] array = allObjects();
        ascii.append((char) ASCIIPropertyListParser.ARRAY_BEGIN_TOKEN);
        int indexOfLastNewLine = ascii.lastIndexOf(NEWLINE);
        for (int i = 0; i < array.length; i++) {
            Class<?> objClass = array[i].getClass();
            if ((objClass.equals(NSDictionary.class) || objClass.equals(NSArray.class) || objClass.equals(NSData.class)) && indexOfLastNewLine != ascii.length()) {
                ascii.append(NEWLINE);
                indexOfLastNewLine = ascii.length();
                array[i].toASCIIGnuStep(ascii, level + 1);
            } else {
                if (i != 0) {
                    ascii.append(" ");
                }
                array[i].toASCIIGnuStep(ascii, 0);
            }
            if (i != array.length - 1) {
                ascii.append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN);
            }
            if (ascii.length() - indexOfLastNewLine > 80) {
                ascii.append(NEWLINE);
                indexOfLastNewLine = ascii.length();
            }
        }
        ascii.append((char) ASCIIPropertyListParser.ARRAY_END_TOKEN);
    }
}
