package com.dd.plist;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class NSData extends NSObject {
    private byte[] bytes;

    public NSData(byte[] bytes2) {
        this.bytes = bytes2;
    }

    public NSData(String base64) throws IOException {
        this.bytes = Base64.decode(base64.replaceAll("\\s+", ""));
    }

    public NSData(File file) throws FileNotFoundException, IOException {
        this.bytes = new byte[((int) file.length())];
        RandomAccessFile raf = new RandomAccessFile(file, "r");
        raf.read(this.bytes);
        raf.close();
    }

    public byte[] bytes() {
        return this.bytes;
    }

    public int length() {
        return this.bytes.length;
    }

    public void getBytes(ByteBuffer buf, int length) {
        buf.put(this.bytes, 0, Math.min(this.bytes.length, length));
    }

    public void getBytes(ByteBuffer buf, int rangeStart, int rangeStop) {
        buf.put(this.bytes, rangeStart, Math.min(this.bytes.length, rangeStop));
    }

    public String getBase64EncodedData() {
        return Base64.encodeBytes(this.bytes);
    }

    public boolean equals(Object obj) {
        return obj.getClass().equals(getClass()) && Arrays.equals(((NSData) obj).bytes, this.bytes);
    }

    public int hashCode() {
        return Arrays.hashCode(this.bytes) + 335;
    }

    /* access modifiers changed from: package-private */
    public void toXML(StringBuilder xml, int level) {
        indent(xml, level);
        xml.append("<data>");
        xml.append(NSObject.NEWLINE);
        for (String line : getBase64EncodedData().split("\n")) {
            indent(xml, level + 1);
            xml.append(line);
            xml.append(NSObject.NEWLINE);
        }
        indent(xml, level);
        xml.append("</data>");
    }

    /* access modifiers changed from: package-private */
    public void toBinary(BinaryPropertyListWriter out) throws IOException {
        out.writeIntHeader(4, this.bytes.length);
        out.write(this.bytes);
    }

    /* access modifiers changed from: protected */
    public void toASCII(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append((char) ASCIIPropertyListParser.DATA_BEGIN_TOKEN);
        int indexOfLastNewLine = ascii.lastIndexOf(NEWLINE);
        for (int i = 0; i < this.bytes.length; i++) {
            int b = this.bytes[i] & 255;
            if (b < 16) {
                ascii.append("0");
            }
            ascii.append(Integer.toHexString(b));
            if (ascii.length() - indexOfLastNewLine > 80) {
                ascii.append(NEWLINE);
                indexOfLastNewLine = ascii.length();
            } else if ((i + 1) % 2 == 0 && i != this.bytes.length - 1) {
                ascii.append(" ");
            }
        }
        ascii.append((char) ASCIIPropertyListParser.DATA_END_TOKEN);
    }

    /* access modifiers changed from: protected */
    public void toASCIIGnuStep(StringBuilder ascii, int level) {
        toASCII(ascii, level);
    }
}
