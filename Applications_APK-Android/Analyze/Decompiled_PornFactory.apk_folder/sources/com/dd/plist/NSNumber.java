package com.dd.plist;

import java.io.IOException;

public class NSNumber extends NSObject implements Comparable<Object> {
    public static final int BOOLEAN = 2;
    public static final int INTEGER = 0;
    public static final int REAL = 1;
    private boolean boolValue;
    private double doubleValue;
    private long longValue;
    private int type;

    public NSNumber(byte[] bytes, int type2) {
        switch (type2) {
            case 0:
                long parseLong = BinaryPropertyListParser.parseLong(bytes);
                this.longValue = parseLong;
                this.doubleValue = (double) parseLong;
                break;
            case 1:
                this.doubleValue = BinaryPropertyListParser.parseDouble(bytes);
                this.longValue = Math.round(this.doubleValue);
                break;
            default:
                throw new IllegalArgumentException("Type argument is not valid.");
        }
        this.type = type2;
    }

    public NSNumber(String text) {
        boolean z = false;
        if (text == null) {
            throw new IllegalArgumentException("The given string is null and cannot be parsed as number.");
        }
        try {
            long l = Long.parseLong(text);
            this.longValue = l;
            this.doubleValue = (double) l;
            this.type = 0;
        } catch (Exception e) {
            try {
                this.doubleValue = Double.parseDouble(text);
                this.longValue = Math.round(this.doubleValue);
                this.type = 1;
            } catch (Exception e2) {
                this.boolValue = (text.toLowerCase().equals("true") || text.toLowerCase().equals("yes")) ? true : z;
                if (this.boolValue || text.toLowerCase().equals("false") || text.toLowerCase().equals("no")) {
                    this.type = 2;
                    long j = this.boolValue ? 1 : 0;
                    this.longValue = j;
                    this.doubleValue = (double) j;
                    return;
                }
                throw new Exception("not a boolean");
            } catch (Exception e3) {
                throw new IllegalArgumentException("The given string neither represents a double, an int nor a boolean value.");
            }
        }
    }

    public NSNumber(int i) {
        long j = (long) i;
        this.longValue = j;
        this.doubleValue = (double) j;
        this.type = 0;
    }

    public NSNumber(long l) {
        this.longValue = l;
        this.doubleValue = (double) l;
        this.type = 0;
    }

    public NSNumber(double d) {
        this.doubleValue = d;
        this.longValue = (long) d;
        this.type = 1;
    }

    public NSNumber(boolean b) {
        this.boolValue = b;
        long j = b ? 1 : 0;
        this.longValue = j;
        this.doubleValue = (double) j;
        this.type = 2;
    }

    public int type() {
        return this.type;
    }

    public boolean isBoolean() {
        return this.type == 2;
    }

    public boolean isInteger() {
        return this.type == 0;
    }

    public boolean isReal() {
        return this.type == 1;
    }

    public boolean boolValue() {
        if (this.type == 2) {
            return this.boolValue;
        }
        return this.longValue != 0;
    }

    public long longValue() {
        return this.longValue;
    }

    public int intValue() {
        return (int) this.longValue;
    }

    public double doubleValue() {
        return this.doubleValue;
    }

    public float floatValue() {
        return (float) this.doubleValue;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof NSNumber)) {
            return false;
        }
        NSNumber n = (NSNumber) obj;
        if (this.type == n.type && this.longValue == n.longValue && this.doubleValue == n.doubleValue && this.boolValue == n.boolValue) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((((this.type * 37) + ((int) (this.longValue ^ (this.longValue >>> 32)))) * 37) + ((int) (Double.doubleToLongBits(this.doubleValue) ^ (Double.doubleToLongBits(this.doubleValue) >>> 32)))) * 37) + (boolValue() ? 1 : 0);
    }

    public String toString() {
        switch (this.type) {
            case 0:
                return String.valueOf(longValue());
            case 1:
                return String.valueOf(doubleValue());
            case 2:
                return String.valueOf(boolValue());
            default:
                return super.toString();
        }
    }

    /* access modifiers changed from: package-private */
    public void toXML(StringBuilder xml, int level) {
        indent(xml, level);
        switch (this.type) {
            case 0:
                xml.append("<integer>");
                xml.append(longValue());
                xml.append("</integer>");
                return;
            case 1:
                xml.append("<real>");
                xml.append(doubleValue());
                xml.append("</real>");
                return;
            case 2:
                if (boolValue()) {
                    xml.append("<true/>");
                    return;
                } else {
                    xml.append("<false/>");
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void toBinary(BinaryPropertyListWriter out) throws IOException {
        int i = 8;
        switch (type()) {
            case 0:
                if (longValue() < 0) {
                    out.write(19);
                    out.writeBytes(longValue(), 8);
                    return;
                } else if (longValue() <= 255) {
                    out.write(16);
                    out.writeBytes(longValue(), 1);
                    return;
                } else if (longValue() <= 65535) {
                    out.write(17);
                    out.writeBytes(longValue(), 2);
                    return;
                } else if (longValue() <= 4294967295L) {
                    out.write(18);
                    out.writeBytes(longValue(), 4);
                    return;
                } else {
                    out.write(19);
                    out.writeBytes(longValue(), 8);
                    return;
                }
            case 1:
                out.write(35);
                out.writeDouble(doubleValue());
                return;
            case 2:
                if (boolValue()) {
                    i = 9;
                }
                out.write(i);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void toASCII(StringBuilder ascii, int level) {
        indent(ascii, level);
        if (this.type == 2) {
            ascii.append(this.boolValue ? "YES" : "NO");
        } else {
            ascii.append(toString());
        }
    }

    /* access modifiers changed from: protected */
    public void toASCIIGnuStep(StringBuilder ascii, int level) {
        indent(ascii, level);
        switch (this.type) {
            case 0:
                ascii.append("<*I");
                ascii.append(toString());
                ascii.append(">");
                return;
            case 1:
                ascii.append("<*R");
                ascii.append(toString());
                ascii.append(">");
                return;
            case 2:
                if (this.boolValue) {
                    ascii.append("<*BY>");
                    return;
                } else {
                    ascii.append("<*BN>");
                    return;
                }
            default:
                return;
        }
    }

    public int compareTo(Object o) {
        double x = doubleValue();
        if (o instanceof NSNumber) {
            double y = ((NSNumber) o).doubleValue();
            if (x < y) {
                return -1;
            }
            return x == y ? 0 : 1;
        } else if (!(o instanceof Number)) {
            return -1;
        } else {
            double y2 = ((Number) o).doubleValue();
            if (x >= y2) {
                return x == y2 ? 0 : 1;
            }
            return -1;
        }
    }
}
