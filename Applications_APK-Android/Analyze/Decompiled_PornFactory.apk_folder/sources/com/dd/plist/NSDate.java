package com.dd.plist;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class NSDate extends NSObject {
    private static final long EPOCH = 978307200000L;
    private static final SimpleDateFormat sdfDefault = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private static final SimpleDateFormat sdfGnuStep = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
    private Date date;

    static {
        sdfDefault.setTimeZone(TimeZone.getTimeZone("GMT"));
        sdfGnuStep.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    private static synchronized Date parseDateString(String textRepresentation) throws ParseException {
        Date parse;
        synchronized (NSDate.class) {
            try {
                parse = sdfDefault.parse(textRepresentation);
            } catch (ParseException e) {
                parse = sdfGnuStep.parse(textRepresentation);
            }
        }
        return parse;
    }

    private static synchronized String makeDateString(Date date2) {
        String format;
        synchronized (NSDate.class) {
            format = sdfDefault.format(date2);
        }
        return format;
    }

    private static synchronized String makeDateStringGnuStep(Date date2) {
        String format;
        synchronized (NSDate.class) {
            format = sdfGnuStep.format(date2);
        }
        return format;
    }

    public NSDate(byte[] bytes) {
        this.date = new Date(EPOCH + ((long) (1000.0d * BinaryPropertyListParser.parseDouble(bytes))));
    }

    public NSDate(String textRepresentation) throws ParseException {
        this.date = parseDateString(textRepresentation);
    }

    public NSDate(Date d) {
        if (d == null) {
            throw new IllegalArgumentException("Date cannot be null");
        }
        this.date = d;
    }

    public Date getDate() {
        return this.date;
    }

    public boolean equals(Object obj) {
        return obj.getClass().equals(getClass()) && this.date.equals(((NSDate) obj).getDate());
    }

    public int hashCode() {
        return this.date.hashCode();
    }

    /* access modifiers changed from: package-private */
    public void toXML(StringBuilder xml, int level) {
        indent(xml, level);
        xml.append("<date>");
        xml.append(makeDateString(this.date));
        xml.append("</date>");
    }

    public void toBinary(BinaryPropertyListWriter out) throws IOException {
        out.write(51);
        out.writeDouble(((double) (this.date.getTime() - EPOCH)) / 1000.0d);
    }

    public String toString() {
        return this.date.toString();
    }

    /* access modifiers changed from: protected */
    public void toASCII(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append("\"");
        ascii.append(makeDateString(this.date));
        ascii.append("\"");
    }

    /* access modifiers changed from: protected */
    public void toASCIIGnuStep(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append("<*D");
        ascii.append(makeDateStringGnuStep(this.date));
        ascii.append(">");
    }
}
