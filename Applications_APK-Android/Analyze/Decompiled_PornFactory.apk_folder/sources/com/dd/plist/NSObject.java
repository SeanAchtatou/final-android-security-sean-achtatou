package com.dd.plist;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public abstract class NSObject {
    static final int ASCII_LINE_LENGTH = 80;
    static final String INDENT = "\t";
    static final String NEWLINE = System.getProperty("line.separator");

    /* access modifiers changed from: protected */
    public abstract void toASCII(StringBuilder sb, int i);

    /* access modifiers changed from: protected */
    public abstract void toASCIIGnuStep(StringBuilder sb, int i);

    /* access modifiers changed from: package-private */
    public abstract void toBinary(BinaryPropertyListWriter binaryPropertyListWriter) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void toXML(StringBuilder sb, int i);

    /* access modifiers changed from: package-private */
    public void assignIDs(BinaryPropertyListWriter out) {
        out.assignID(this);
    }

    public String toXMLPropertyList() {
        StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        xml.append(NEWLINE);
        xml.append("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
        xml.append(NEWLINE);
        xml.append("<plist version=\"1.0\">");
        xml.append(NEWLINE);
        toXML(xml, 0);
        xml.append(NEWLINE);
        xml.append("</plist>");
        return xml.toString();
    }

    /* access modifiers changed from: package-private */
    public void indent(StringBuilder xml, int level) {
        for (int i = 0; i < level; i++) {
            xml.append(INDENT);
        }
    }

    public static NSString wrap(String value) {
        return new NSString(value);
    }

    public static NSNumber wrap(long value) {
        return new NSNumber(value);
    }

    public static NSNumber wrap(double value) {
        return new NSNumber(value);
    }

    public static NSNumber wrap(boolean value) {
        return new NSNumber(value);
    }

    public static NSDate wrap(Date value) {
        return new NSDate(value);
    }

    public static NSData wrap(byte[] value) {
        return new NSData(value);
    }

    public static NSArray wrap(Object[] value) {
        NSArray arr = new NSArray(value.length);
        for (int i = 0; i < value.length; i++) {
            arr.setValue(i, wrap(value[i]));
        }
        return arr;
    }

    public static NSDictionary wrap(Map<String, Object> value) {
        NSDictionary dict = new NSDictionary();
        for (String key : value.keySet()) {
            dict.put(key, wrap(value.get(key)));
        }
        return dict;
    }

    public static NSSet wrap(Set<Object> value) {
        NSSet set = new NSSet();
        for (Object o : value.toArray()) {
            set.addObject(wrap(o));
        }
        return set;
    }

    public static NSObject wrap(Object o) {
        if (o == null) {
            return null;
        }
        if (o instanceof NSObject) {
            return (NSObject) o;
        }
        Class<?> cls = o.getClass();
        if (Boolean.class.isAssignableFrom(cls)) {
            return wrap(((Boolean) o).booleanValue());
        }
        if (Integer.class.isAssignableFrom(cls)) {
            return wrap((long) ((Integer) o).intValue());
        }
        if (Long.class.isAssignableFrom(cls)) {
            return wrap(((Long) o).longValue());
        }
        if (Short.class.isAssignableFrom(cls)) {
            return wrap((long) ((Short) o).shortValue());
        }
        if (Byte.class.isAssignableFrom(cls)) {
            return wrap((long) ((Byte) o).byteValue());
        }
        if (Float.class.isAssignableFrom(cls)) {
            return wrap((double) ((Float) o).floatValue());
        }
        if (Double.class.isAssignableFrom(cls)) {
            return wrap(((Double) o).doubleValue());
        }
        if (String.class.equals(cls)) {
            return wrap((String) o);
        }
        if (Date.class.equals(cls)) {
            return wrap((Date) o);
        }
        if (byte[].class.equals(cls)) {
            return wrap((byte[]) ((byte[]) o));
        }
        if (Object[].class.isAssignableFrom(cls)) {
            return wrap((Object[]) ((Object[]) o));
        }
        if (Map.class.isAssignableFrom(cls)) {
            Map map = (Map) o;
            Set keys = map.keySet();
            NSDictionary dict = new NSDictionary();
            for (Object key : keys) {
                dict.put(String.valueOf(key), wrap(map.get(key)));
            }
            return dict;
        } else if (Collection.class.isAssignableFrom(cls)) {
            return wrap(((Collection) o).toArray());
        } else {
            return wrapSerialized(o);
        }
    }

    public static NSData wrapSerialized(Object o) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            new ObjectOutputStream(baos).writeObject(o);
            return new NSData(baos.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("The given object of class " + o.getClass().toString() + " could not be serialized and stored in a NSData object.");
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 18 */
    public Object toJavaObject() {
        Set<Object> setB;
        if (this instanceof NSArray) {
            NSObject[] arrayA = ((NSArray) this).getArray();
            Object[] arrayB = new Object[arrayA.length];
            for (int i = 0; i < arrayA.length; i++) {
                arrayB[i] = arrayA[i].toJavaObject();
            }
            return arrayB;
        } else if (this instanceof NSDictionary) {
            HashMap<String, NSObject> hashMapA = ((NSDictionary) this).getHashMap();
            HashMap<String, Object> hashMapB = new HashMap<>(hashMapA.size());
            for (String key : hashMapA.keySet()) {
                hashMapB.put(key, hashMapA.get(key).toJavaObject());
            }
            return hashMapB;
        } else if (this instanceof NSSet) {
            Set<NSObject> setA = ((NSSet) this).getSet();
            if (setA instanceof LinkedHashSet) {
                setB = new LinkedHashSet<>(setA.size());
            } else {
                setB = new TreeSet<>();
            }
            for (NSObject o : setA) {
                setB.add(o.toJavaObject());
            }
            return setB;
        } else if (this instanceof NSNumber) {
            NSNumber num = (NSNumber) this;
            switch (num.type()) {
                case 0:
                    long longVal = num.longValue();
                    if (longVal > 2147483647L || longVal < -2147483648L) {
                        return Long.valueOf(longVal);
                    }
                    return Integer.valueOf(num.intValue());
                case 1:
                    return Double.valueOf(num.doubleValue());
                case 2:
                    return Boolean.valueOf(num.boolValue());
                default:
                    return Double.valueOf(num.doubleValue());
            }
        } else if (this instanceof NSString) {
            return ((NSString) this).getContent();
        } else {
            if (this instanceof NSData) {
                return ((NSData) this).bytes();
            }
            if (this instanceof NSDate) {
                return ((NSDate) this).getDate();
            }
            if (this instanceof UID) {
                return ((UID) this).getBytes();
            }
            return this;
        }
    }
}
