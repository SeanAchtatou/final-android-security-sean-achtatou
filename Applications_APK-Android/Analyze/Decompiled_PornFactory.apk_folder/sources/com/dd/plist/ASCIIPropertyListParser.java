package com.dd.plist;

import android.support.v4.view.MotionEventCompat;
import android.support.v7.internal.widget.ActivityChooserView;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.ParseException;
import java.text.StringCharacterIterator;
import java.util.LinkedList;
import java.util.List;

public class ASCIIPropertyListParser {
    public static final char ARRAY_BEGIN_TOKEN = '(';
    public static final char ARRAY_END_TOKEN = ')';
    public static final char ARRAY_ITEM_DELIMITER_TOKEN = ',';
    public static final char COMMENT_BEGIN_TOKEN = '/';
    public static final char DATA_BEGIN_TOKEN = '<';
    public static final char DATA_END_TOKEN = '>';
    public static final char DATA_GSBOOL_BEGIN_TOKEN = 'B';
    public static final char DATA_GSBOOL_FALSE_TOKEN = 'N';
    public static final char DATA_GSBOOL_TRUE_TOKEN = 'Y';
    public static final char DATA_GSDATE_BEGIN_TOKEN = 'D';
    public static final char DATA_GSINT_BEGIN_TOKEN = 'I';
    public static final char DATA_GSOBJECT_BEGIN_TOKEN = '*';
    public static final char DATA_GSREAL_BEGIN_TOKEN = 'R';
    public static final char DATE_APPLE_DATE_TIME_DELIMITER = 'T';
    public static final char DATE_APPLE_END_TOKEN = 'Z';
    public static final char DATE_DATE_FIELD_DELIMITER = '-';
    public static final char DATE_GS_DATE_TIME_DELIMITER = ' ';
    public static final char DATE_TIME_FIELD_DELIMITER = ':';
    public static final char DICTIONARY_ASSIGN_TOKEN = '=';
    public static final char DICTIONARY_BEGIN_TOKEN = '{';
    public static final char DICTIONARY_END_TOKEN = '}';
    public static final char DICTIONARY_ITEM_DELIMITER_TOKEN = ';';
    public static final char MULTILINE_COMMENT_END_TOKEN = '/';
    public static final char MULTILINE_COMMENT_SECOND_TOKEN = '*';
    public static final char QUOTEDSTRING_BEGIN_TOKEN = '\"';
    public static final char QUOTEDSTRING_END_TOKEN = '\"';
    public static final char QUOTEDSTRING_ESCAPE_TOKEN = '\\';
    public static final char SINGLELINE_COMMENT_SECOND_TOKEN = '/';
    public static final char WHITESPACE_CARRIAGE_RETURN = '\r';
    public static final char WHITESPACE_NEWLINE = '\n';
    public static final char WHITESPACE_SPACE = ' ';
    public static final char WHITESPACE_TAB = '\t';
    private static CharsetEncoder asciiEncoder;
    private byte[] data;
    private int index;

    public static NSObject parse(File f) throws Exception {
        return parse(new FileInputStream(f));
    }

    public static NSObject parse(InputStream in) throws Exception {
        byte[] buf = PropertyListParser.readAll(in, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
        in.close();
        return parse(buf);
    }

    public static NSObject parse(byte[] bytes) throws Exception {
        return new ASCIIPropertyListParser(bytes).parse();
    }

    protected ASCIIPropertyListParser() {
    }

    private ASCIIPropertyListParser(byte[] propertyListContent) {
        this.data = propertyListContent;
    }

    private boolean acceptSequence(char... sequence) {
        for (int i = 0; i < sequence.length; i++) {
            if (this.data[this.index + i] != sequence[i]) {
                return false;
            }
        }
        return true;
    }

    private boolean accept(char... acceptableSymbols) {
        boolean symbolPresent = false;
        for (char c : acceptableSymbols) {
            if (this.data[this.index] == c) {
                symbolPresent = true;
            }
        }
        return symbolPresent;
    }

    private boolean accept(char acceptableSymbol) {
        return this.data[this.index] == acceptableSymbol;
    }

    private void expect(char... expectedSymbols) throws ParseException {
        if (!accept(expectedSymbols)) {
            String excString = "Expected '" + expectedSymbols[0] + "'";
            for (int i = 1; i < expectedSymbols.length; i++) {
                excString = excString + " or '" + expectedSymbols[i] + "'";
            }
            throw new ParseException(excString + " but found '" + ((char) this.data[this.index]) + "'", this.index);
        }
    }

    private void expect(char expectedSymbol) throws ParseException {
        if (!accept(expectedSymbol)) {
            throw new ParseException("Expected '" + expectedSymbol + "' but found '" + ((char) this.data[this.index]) + "'", this.index);
        }
    }

    private void read(char symbol) throws ParseException {
        expect(symbol);
        this.index++;
    }

    private void skip() {
        this.index++;
    }

    private void skip(int numSymbols) {
        this.index += numSymbols;
    }

    private void skipWhitespacesAndComments() {
        boolean commentSkipped;
        do {
            commentSkipped = false;
            while (accept(WHITESPACE_CARRIAGE_RETURN, 10, ' ', 9)) {
                skip();
            }
            if (acceptSequence('/', '/')) {
                skip(2);
                readInputUntil(WHITESPACE_CARRIAGE_RETURN, 10);
                commentSkipped = true;
                continue;
            } else if (acceptSequence('/', '*')) {
                skip(2);
                while (!acceptSequence('*', '/')) {
                    skip();
                }
                skip(2);
                commentSkipped = true;
                continue;
            } else {
                continue;
            }
        } while (commentSkipped);
    }

    private String readInputUntil(char... symbols) {
        String s = "";
        while (!accept(symbols)) {
            s = s + ((char) this.data[this.index]);
            skip();
        }
        return s;
    }

    private String readInputUntil(char symbol) {
        String s = "";
        while (!accept(symbol)) {
            s = s + ((char) this.data[this.index]);
            skip();
        }
        return s;
    }

    public NSObject parse() throws ParseException {
        this.index = 0;
        skipWhitespacesAndComments();
        expect(DICTIONARY_BEGIN_TOKEN, ARRAY_BEGIN_TOKEN, '/');
        try {
            return parseObject();
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ParseException("Reached end of input unexpectedly.", this.index);
        }
    }

    private NSObject parseObject() throws ParseException {
        switch (this.data[this.index]) {
            case MotionEventCompat.AXIS_GENERIC_3:
                String quotedString = parseQuotedString();
                if (quotedString.length() != 20 || quotedString.charAt(4) != '-') {
                    return new NSString(quotedString);
                }
                try {
                    return new NSDate(quotedString);
                } catch (Exception e) {
                    return new NSString(quotedString);
                }
            case MotionEventCompat.AXIS_GENERIC_9:
                return parseArray();
            case 60:
                return parseData();
            case 123:
                return parseDictionary();
            default:
                if (this.data[this.index] > 47 && this.data[this.index] < 58) {
                    return parseNumerical();
                }
                String parsedString = parseString();
                if (parsedString.equals("YES")) {
                    return new NSNumber(true);
                }
                if (parsedString.equals("NO")) {
                    return new NSNumber(false);
                }
                return new NSString(parsedString);
        }
    }

    private NSArray parseArray() throws ParseException {
        skip();
        skipWhitespacesAndComments();
        List<NSObject> objects = new LinkedList<>();
        while (!accept((char) ARRAY_END_TOKEN)) {
            objects.add(parseObject());
            skipWhitespacesAndComments();
            if (!accept((char) ARRAY_ITEM_DELIMITER_TOKEN)) {
                break;
            }
            skip();
            skipWhitespacesAndComments();
        }
        read(ARRAY_END_TOKEN);
        return new NSArray((NSObject[]) objects.toArray(new NSObject[objects.size()]));
    }

    private NSDictionary parseDictionary() throws ParseException {
        String keyString;
        skip();
        skipWhitespacesAndComments();
        NSDictionary dict = new NSDictionary();
        while (!accept((char) DICTIONARY_END_TOKEN)) {
            if (accept('\"')) {
                keyString = parseQuotedString();
            } else {
                keyString = parseString();
            }
            skipWhitespacesAndComments();
            read(DICTIONARY_ASSIGN_TOKEN);
            skipWhitespacesAndComments();
            dict.put(keyString, parseObject());
            skipWhitespacesAndComments();
            read(DICTIONARY_ITEM_DELIMITER_TOKEN);
            skipWhitespacesAndComments();
        }
        skip();
        return dict;
    }

    private NSObject parseData() throws ParseException {
        NSObject obj = null;
        skip();
        if (accept('*')) {
            skip();
            expect(DATA_GSBOOL_BEGIN_TOKEN, DATA_GSDATE_BEGIN_TOKEN, DATA_GSINT_BEGIN_TOKEN, DATA_GSREAL_BEGIN_TOKEN);
            if (accept((char) DATA_GSBOOL_BEGIN_TOKEN)) {
                skip();
                expect(DATA_GSBOOL_TRUE_TOKEN, DATA_GSBOOL_FALSE_TOKEN);
                if (accept((char) DATA_GSBOOL_TRUE_TOKEN)) {
                    obj = new NSNumber(true);
                } else {
                    obj = new NSNumber(false);
                }
                skip();
            } else if (accept((char) DATA_GSDATE_BEGIN_TOKEN)) {
                skip();
                obj = new NSDate(readInputUntil((char) DATA_END_TOKEN));
            } else if (accept(DATA_GSINT_BEGIN_TOKEN, DATA_GSREAL_BEGIN_TOKEN)) {
                skip();
                obj = new NSNumber(readInputUntil((char) DATA_END_TOKEN));
            }
            read(DATA_END_TOKEN);
            return obj;
        }
        String dataString = readInputUntil((char) DATA_END_TOKEN).replaceAll("\\s+", "");
        byte[] bytes = new byte[(dataString.length() / 2)];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(dataString.substring(i * 2, (i * 2) + 2), 16);
        }
        NSObject obj2 = new NSData(bytes);
        skip();
        return obj2;
    }

    private NSObject parseNumerical() {
        String numericalString = parseString();
        try {
            if (numericalString.length() <= 4 || numericalString.charAt(4) != '-') {
                return new NSNumber(numericalString);
            }
            return new NSDate(numericalString);
        } catch (Exception e) {
            return new NSString(numericalString);
        }
    }

    private String parseString() {
        return readInputUntil(' ', 9, 10, WHITESPACE_CARRIAGE_RETURN, ARRAY_ITEM_DELIMITER_TOKEN, DICTIONARY_ITEM_DELIMITER_TOKEN, DICTIONARY_ASSIGN_TOKEN, ARRAY_END_TOKEN);
    }

    private String parseQuotedString() throws ParseException {
        skip();
        String quotedString = "";
        boolean unescapedBackslash = true;
        while (true) {
            if (this.data[this.index] != 34 || (this.data[this.index - 1] == 92 && unescapedBackslash)) {
                quotedString = quotedString + ((char) this.data[this.index]);
                if (accept((char) QUOTEDSTRING_ESCAPE_TOKEN)) {
                    if (this.data[this.index - 1] != 92 || !unescapedBackslash) {
                        unescapedBackslash = true;
                    } else {
                        unescapedBackslash = false;
                    }
                }
                skip();
            } else {
                try {
                    String unescapedString = parseQuotedString(quotedString);
                    skip();
                    return unescapedString;
                } catch (Exception e) {
                    throw new ParseException("The quoted string could not be parsed.", this.index);
                }
            }
        }
    }

    public static synchronized String parseQuotedString(String s) throws Exception {
        String result;
        synchronized (ASCIIPropertyListParser.class) {
            List<Byte> strBytes = new LinkedList<>();
            StringCharacterIterator iterator = new StringCharacterIterator(s);
            char c = iterator.current();
            while (iterator.getIndex() < iterator.getEndIndex()) {
                switch (c) {
                    case '\\':
                        for (byte b : parseEscapedSequence(iterator).getBytes("UTF-8")) {
                            strBytes.add(Byte.valueOf(b));
                        }
                        break;
                    default:
                        strBytes.add((byte) 0);
                        strBytes.add(Byte.valueOf((byte) c));
                        break;
                }
                c = iterator.next();
            }
            byte[] bytArr = new byte[strBytes.size()];
            int i = 0;
            for (Byte b2 : strBytes) {
                bytArr[i] = b2.byteValue();
                i++;
            }
            result = new String(bytArr, "UTF-8");
            CharBuffer charBuf = CharBuffer.wrap(result);
            if (asciiEncoder == null) {
                asciiEncoder = Charset.forName("ASCII").newEncoder();
            }
            if (asciiEncoder.canEncode(charBuf)) {
                result = asciiEncoder.encode(charBuf).asCharBuffer().toString();
            }
        }
        return result;
    }

    private static String parseEscapedSequence(StringCharacterIterator iterator) throws UnsupportedEncodingException {
        char c = iterator.next();
        if (c == '\\') {
            return new String("\u0000\\".getBytes(), "UTF-8");
        }
        if (c == '\"') {
            return new String("\u0000\"".getBytes(), "UTF-8");
        }
        if (c == 'b') {
            return new String(new byte[]{0, 8}, "UTF-8");
        }
        if (c == 'n') {
            return new String(new byte[]{0, 10}, "UTF-8");
        }
        if (c == 'r') {
            return new String(new byte[]{0, 13}, "UTF-8");
        }
        if (c == 't') {
            return new String(new byte[]{0, 9}, "UTF-8");
        }
        if (c == 'U' || c == 'u') {
            return new String(new byte[]{(byte) Integer.parseInt(("" + iterator.next()) + iterator.next(), 16), (byte) Integer.parseInt(("" + iterator.next()) + iterator.next(), 16)}, "UTF-8");
        }
        return new String(new byte[]{0, (byte) Integer.parseInt((("" + c) + iterator.next()) + iterator.next(), 8)}, "UTF-8");
    }
}
