package com.dd.plist;

import java.io.IOException;

public class UID extends NSObject {
    private byte[] bytes;
    private String name;

    public UID(String name2, byte[] bytes2) {
        this.name = name2;
        this.bytes = bytes2;
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public String getName() {
        return this.name;
    }

    /* access modifiers changed from: package-private */
    public void toXML(StringBuilder xml, int level) {
        indent(xml, level);
        xml.append("<string>");
        for (byte b : this.bytes) {
            if (b < 16) {
                xml.append("0");
            }
            xml.append(Integer.toHexString(b));
        }
        xml.append("</string>");
    }

    /* access modifiers changed from: package-private */
    public void toBinary(BinaryPropertyListWriter out) throws IOException {
        out.write((this.bytes.length + 128) - 1);
        out.write(this.bytes);
    }

    /* access modifiers changed from: protected */
    public void toASCII(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append("\"");
        for (byte b : this.bytes) {
            if (b < 16) {
                ascii.append("0");
            }
            ascii.append(Integer.toHexString(b));
        }
        ascii.append("\"");
    }

    /* access modifiers changed from: protected */
    public void toASCIIGnuStep(StringBuilder ascii, int level) {
        toASCII(ascii, level);
    }
}
