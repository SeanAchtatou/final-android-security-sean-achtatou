package com.dd.plist;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class NSDictionary extends NSObject implements Map<String, NSObject> {
    private HashMap<String, NSObject> dict = new LinkedHashMap();

    public HashMap<String, NSObject> getHashMap() {
        return this.dict;
    }

    public NSObject objectForKey(String key) {
        return this.dict.get(key);
    }

    public void put(String key, Object obj) {
        this.dict.put(key, NSObject.wrap(obj));
    }

    public int size() {
        return this.dict.size();
    }

    public boolean isEmpty() {
        return this.dict.isEmpty();
    }

    public boolean containsKey(Object key) {
        return this.dict.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return this.dict.containsValue(NSObject.wrap(value));
    }

    public NSObject get(Object key) {
        return this.dict.get(key);
    }

    public void putAll(Map<? extends String, ? extends NSObject> values) {
        for (Map.Entry<String, NSObject> entry : values.entrySet()) {
            put((String) entry.getKey(), (NSObject) entry.getValue());
        }
    }

    public NSObject put(String key, NSObject obj) {
        return this.dict.put(key, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject
     arg types: [java.lang.String, com.dd.plist.NSString]
     candidates:
      com.dd.plist.NSDictionary.put(java.lang.String, double):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, long):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.String):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.util.Date):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, boolean):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, byte[]):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.Object, java.lang.Object):java.lang.Object
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.Object):void
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject */
    public NSObject put(String key, String obj) {
        return put(key, (NSObject) new NSString(obj));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject
     arg types: [java.lang.String, com.dd.plist.NSNumber]
     candidates:
      com.dd.plist.NSDictionary.put(java.lang.String, double):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, long):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.String):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.util.Date):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, boolean):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, byte[]):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.Object, java.lang.Object):java.lang.Object
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.Object):void
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject */
    public NSObject put(String key, long obj) {
        return put(key, (NSObject) new NSNumber(obj));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject
     arg types: [java.lang.String, com.dd.plist.NSNumber]
     candidates:
      com.dd.plist.NSDictionary.put(java.lang.String, double):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, long):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.String):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.util.Date):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, boolean):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, byte[]):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.Object, java.lang.Object):java.lang.Object
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.Object):void
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject */
    public NSObject put(String key, double obj) {
        return put(key, (NSObject) new NSNumber(obj));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject
     arg types: [java.lang.String, com.dd.plist.NSNumber]
     candidates:
      com.dd.plist.NSDictionary.put(java.lang.String, double):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, long):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.String):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.util.Date):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, boolean):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, byte[]):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.Object, java.lang.Object):java.lang.Object
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.Object):void
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject */
    public NSObject put(String key, boolean obj) {
        return put(key, (NSObject) new NSNumber(obj));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject
     arg types: [java.lang.String, com.dd.plist.NSDate]
     candidates:
      com.dd.plist.NSDictionary.put(java.lang.String, double):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, long):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.String):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.util.Date):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, boolean):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, byte[]):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.Object, java.lang.Object):java.lang.Object
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.Object):void
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject */
    public NSObject put(String key, Date obj) {
        return put(key, (NSObject) new NSDate(obj));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject
     arg types: [java.lang.String, com.dd.plist.NSData]
     candidates:
      com.dd.plist.NSDictionary.put(java.lang.String, double):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, long):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.String):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, java.util.Date):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, boolean):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.String, byte[]):com.dd.plist.NSObject
      com.dd.plist.NSDictionary.put(java.lang.Object, java.lang.Object):java.lang.Object
      com.dd.plist.NSDictionary.put(java.lang.String, java.lang.Object):void
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      com.dd.plist.NSDictionary.put(java.lang.String, com.dd.plist.NSObject):com.dd.plist.NSObject */
    public NSObject put(String key, byte[] obj) {
        return put(key, (NSObject) new NSData(obj));
    }

    public NSObject remove(String key) {
        return this.dict.remove(key);
    }

    public NSObject remove(Object key) {
        return this.dict.remove(key);
    }

    public void clear() {
        this.dict.clear();
    }

    public Set<String> keySet() {
        return this.dict.keySet();
    }

    public Collection<NSObject> values() {
        return this.dict.values();
    }

    public Set<Map.Entry<String, NSObject>> entrySet() {
        return this.dict.entrySet();
    }

    public boolean containsKey(String key) {
        return this.dict.containsKey(key);
    }

    public boolean containsValue(NSObject val) {
        return this.dict.containsValue(val);
    }

    public boolean containsValue(String val) {
        for (NSObject o : this.dict.values()) {
            if (o.getClass().equals(NSString.class) && ((NSString) o).getContent().equals(val)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsValue(long val) {
        for (NSObject o : this.dict.values()) {
            if (o.getClass().equals(NSNumber.class)) {
                NSNumber num = (NSNumber) o;
                if (num.isInteger() && ((long) num.intValue()) == val) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containsValue(double val) {
        for (NSObject o : this.dict.values()) {
            if (o.getClass().equals(NSNumber.class)) {
                NSNumber num = (NSNumber) o;
                if (num.isReal() && num.doubleValue() == val) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containsValue(boolean val) {
        for (NSObject o : this.dict.values()) {
            if (o.getClass().equals(NSNumber.class)) {
                NSNumber num = (NSNumber) o;
                if (num.isBoolean() && num.boolValue() == val) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containsValue(Date val) {
        for (NSObject o : this.dict.values()) {
            if (o.getClass().equals(NSDate.class) && ((NSDate) o).getDate().equals(val)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsValue(byte[] val) {
        for (NSObject o : this.dict.values()) {
            if (o.getClass().equals(NSData.class) && Arrays.equals(((NSData) o).bytes(), val)) {
                return true;
            }
        }
        return false;
    }

    public int count() {
        return this.dict.size();
    }

    public boolean equals(Object obj) {
        return obj.getClass().equals(getClass()) && ((NSDictionary) obj).dict.equals(this.dict);
    }

    public String[] allKeys() {
        return (String[]) this.dict.keySet().toArray(new String[0]);
    }

    public int hashCode() {
        return (this.dict != null ? this.dict.hashCode() : 0) + 581;
    }

    /* access modifiers changed from: package-private */
    public void toXML(StringBuilder xml, int level) {
        indent(xml, level);
        xml.append("<dict>");
        xml.append(NSObject.NEWLINE);
        for (String key : this.dict.keySet()) {
            NSObject val = objectForKey(key);
            indent(xml, level + 1);
            xml.append("<key>");
            if (key.contains("&") || key.contains("<") || key.contains(">")) {
                xml.append("<![CDATA[");
                xml.append(key.replaceAll("]]>", "]]]]><![CDATA[>"));
                xml.append("]]>");
            } else {
                xml.append(key);
            }
            xml.append("</key>");
            xml.append(NSObject.NEWLINE);
            val.toXML(xml, level + 1);
            xml.append(NSObject.NEWLINE);
        }
        indent(xml, level);
        xml.append("</dict>");
    }

    /* access modifiers changed from: package-private */
    public void assignIDs(BinaryPropertyListWriter out) {
        super.assignIDs(out);
        for (Map.Entry<String, NSObject> entry : this.dict.entrySet()) {
            new NSString((String) entry.getKey()).assignIDs(out);
            ((NSObject) entry.getValue()).assignIDs(out);
        }
    }

    /* access modifiers changed from: package-private */
    public void toBinary(BinaryPropertyListWriter out) throws IOException {
        out.writeIntHeader(13, this.dict.size());
        Set<Map.Entry<String, NSObject>> entries = this.dict.entrySet();
        for (Map.Entry<String, NSObject> entry : entries) {
            out.writeID(out.getID(new NSString((String) entry.getKey())));
        }
        for (Map.Entry<String, NSObject> entry2 : entries) {
            out.writeID(out.getID((NSObject) entry2.getValue()));
        }
    }

    public String toASCIIPropertyList() {
        StringBuilder ascii = new StringBuilder();
        toASCII(ascii, 0);
        ascii.append(NEWLINE);
        return ascii.toString();
    }

    public String toGnuStepASCIIPropertyList() {
        StringBuilder ascii = new StringBuilder();
        toASCIIGnuStep(ascii, 0);
        ascii.append(NEWLINE);
        return ascii.toString();
    }

    /* access modifiers changed from: protected */
    public void toASCII(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append((char) ASCIIPropertyListParser.DICTIONARY_BEGIN_TOKEN);
        ascii.append(NEWLINE);
        String[] keys = (String[]) this.dict.keySet().toArray(new String[0]);
        for (String key : keys) {
            NSObject val = objectForKey(key);
            indent(ascii, level + 1);
            ascii.append("\"");
            ascii.append(NSString.escapeStringForASCII(key));
            ascii.append("\" =");
            Class<?> objClass = val.getClass();
            if (objClass.equals(NSDictionary.class) || objClass.equals(NSArray.class) || objClass.equals(NSData.class)) {
                ascii.append(NEWLINE);
                val.toASCII(ascii, level + 2);
            } else {
                ascii.append(" ");
                val.toASCII(ascii, 0);
            }
            ascii.append((char) ASCIIPropertyListParser.DICTIONARY_ITEM_DELIMITER_TOKEN);
            ascii.append(NEWLINE);
        }
        indent(ascii, level);
        ascii.append((char) ASCIIPropertyListParser.DICTIONARY_END_TOKEN);
    }

    /* access modifiers changed from: protected */
    public void toASCIIGnuStep(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append((char) ASCIIPropertyListParser.DICTIONARY_BEGIN_TOKEN);
        ascii.append(NEWLINE);
        String[] keys = (String[]) this.dict.keySet().toArray(new String[0]);
        for (String key : keys) {
            NSObject val = objectForKey(key);
            indent(ascii, level + 1);
            ascii.append("\"");
            ascii.append(NSString.escapeStringForASCII(key));
            ascii.append("\" =");
            Class<?> objClass = val.getClass();
            if (objClass.equals(NSDictionary.class) || objClass.equals(NSArray.class) || objClass.equals(NSData.class)) {
                ascii.append(NEWLINE);
                val.toASCIIGnuStep(ascii, level + 2);
            } else {
                ascii.append(" ");
                val.toASCIIGnuStep(ascii, 0);
            }
            ascii.append((char) ASCIIPropertyListParser.DICTIONARY_ITEM_DELIMITER_TOKEN);
            ascii.append(NEWLINE);
        }
        indent(ascii, level);
        ascii.append((char) ASCIIPropertyListParser.DICTIONARY_END_TOKEN);
    }
}
