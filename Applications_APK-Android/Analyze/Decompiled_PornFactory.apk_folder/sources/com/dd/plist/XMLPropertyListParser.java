package com.dd.plist;

import ch.nth.android.utils.ResourceUtils;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

public class XMLPropertyListParser {
    private static DocumentBuilderFactory docBuilderFactory = null;

    protected XMLPropertyListParser() {
    }

    private static synchronized void initDocBuilderFactory() throws ParserConfigurationException {
        synchronized (XMLPropertyListParser.class) {
            docBuilderFactory = DocumentBuilderFactory.newInstance();
            docBuilderFactory.setIgnoringComments(true);
            docBuilderFactory.setCoalescing(true);
        }
    }

    private static synchronized DocumentBuilder getDocBuilder() throws ParserConfigurationException {
        DocumentBuilder docBuilder;
        synchronized (XMLPropertyListParser.class) {
            if (docBuilderFactory == null) {
                initDocBuilderFactory();
            }
            docBuilder = docBuilderFactory.newDocumentBuilder();
            docBuilder.setEntityResolver(new EntityResolver() {
                public InputSource resolveEntity(String publicId, String systemId) {
                    if ("-//Apple Computer//DTD PLIST 1.0//EN".equals(publicId) || "-//Apple//DTD PLIST 1.0//EN".equals(publicId)) {
                        return new InputSource(new ByteArrayInputStream(new byte[0]));
                    }
                    return null;
                }
            });
        }
        return docBuilder;
    }

    public static NSObject parse(File f) throws Exception {
        return parseDocument(getDocBuilder().parse(f));
    }

    public static NSObject parse(byte[] bytes) throws Exception {
        return parse(new ByteArrayInputStream(bytes));
    }

    public static NSObject parse(InputStream is) throws Exception {
        return parseDocument(getDocBuilder().parse(is));
    }

    private static NSObject parseDocument(Document doc) throws Exception {
        Node rootNode;
        DocumentType docType = doc.getDoctype();
        if (docType == null) {
            if (!doc.getDocumentElement().getNodeName().equals("plist")) {
                throw new UnsupportedOperationException("The given XML document is not a property list.");
            }
        } else if (!docType.getName().equals("plist")) {
            throw new UnsupportedOperationException("The given XML document is not a property list.");
        }
        if (doc.getDocumentElement().getNodeName().equals("plist")) {
            List<Node> rootNodes = filterElementNodes(doc.getDocumentElement().getChildNodes());
            if (rootNodes.isEmpty()) {
                throw new Exception("The given property list has no root element!");
            } else if (rootNodes.size() == 1) {
                rootNode = rootNodes.get(0);
            } else {
                throw new Exception("The given property list has more than one root element!");
            }
        } else {
            rootNode = doc.getDocumentElement();
        }
        return parseObject(rootNode);
    }

    private static NSObject parseObject(Node n) throws Exception {
        String type = n.getNodeName();
        if (type.equals("dict")) {
            NSDictionary dict = new NSDictionary();
            List<Node> children = filterElementNodes(n.getChildNodes());
            for (int i = 0; i < children.size(); i += 2) {
                dict.put(getNodeTextContents(children.get(i)), parseObject(children.get(i + 1)));
            }
            return dict;
        } else if (type.equals("array")) {
            List<Node> children2 = filterElementNodes(n.getChildNodes());
            NSArray array = new NSArray(children2.size());
            for (int i2 = 0; i2 < children2.size(); i2++) {
                array.setValue(i2, parseObject(children2.get(i2)));
            }
            return array;
        } else if (type.equals("true")) {
            return new NSNumber(true);
        } else {
            if (type.equals("false")) {
                return new NSNumber(false);
            }
            if (type.equals("integer")) {
                return new NSNumber(getNodeTextContents(n));
            }
            if (type.equals("real")) {
                return new NSNumber(getNodeTextContents(n));
            }
            if (type.equals(ResourceUtils.RESOURCE_TYPE_STRING)) {
                return new NSString(getNodeTextContents(n));
            }
            if (type.equals("data")) {
                return new NSData(getNodeTextContents(n));
            }
            if (type.equals("date")) {
                return new NSDate(getNodeTextContents(n));
            }
            return null;
        }
    }

    private static List<Node> filterElementNodes(NodeList list) {
        List<Node> result = new ArrayList<>(list.getLength());
        for (int i = 0; i < list.getLength(); i++) {
            if (list.item(i).getNodeType() == 1) {
                result.add(list.item(i));
            }
        }
        return result;
    }

    private static String getNodeTextContents(Node n) {
        if (n.getNodeType() == 3 || n.getNodeType() == 4) {
            String content = ((Text) n).getWholeText();
            if (content == null) {
                return "";
            }
            return content;
        } else if (!n.hasChildNodes()) {
            return "";
        } else {
            NodeList children = n.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child.getNodeType() == 3 || child.getNodeType() == 4) {
                    String content2 = ((Text) child).getWholeText();
                    if (content2 == null) {
                        return "";
                    }
                    return content2;
                }
            }
            return "";
        }
    }
}
