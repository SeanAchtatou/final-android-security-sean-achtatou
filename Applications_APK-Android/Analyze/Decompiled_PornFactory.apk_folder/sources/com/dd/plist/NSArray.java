package com.dd.plist;

import java.io.IOException;
import java.util.Arrays;

public class NSArray extends NSObject {
    private NSObject[] array;

    public NSArray(int length) {
        this.array = new NSObject[length];
    }

    public NSArray(NSObject... a) {
        this.array = a;
    }

    public NSObject objectAtIndex(int i) {
        return this.array[i];
    }

    public void remove(int i) {
        if (i >= this.array.length || i < 0) {
            throw new ArrayIndexOutOfBoundsException("invalid index:" + i + ";the array length is " + this.array.length);
        }
        NSObject[] newArray = new NSObject[(this.array.length - 1)];
        System.arraycopy(this.array, 0, newArray, 0, i);
        System.arraycopy(this.array, i + 1, newArray, i, (this.array.length - i) - 1);
        this.array = newArray;
    }

    public void setValue(int key, NSObject value) {
        this.array[key] = value;
    }

    public NSObject[] getArray() {
        return this.array;
    }

    public int count() {
        return this.array.length;
    }

    public boolean containsObject(NSObject obj) {
        for (NSObject o : this.array) {
            if (o.equals(obj)) {
                return true;
            }
        }
        return false;
    }

    public int indexOfObject(NSObject obj) {
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i].equals(obj)) {
                return i;
            }
        }
        return -1;
    }

    public int indexOfIdenticalObject(NSObject obj) {
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] == obj) {
                return i;
            }
        }
        return -1;
    }

    public NSObject lastObject() {
        return this.array[this.array.length - 1];
    }

    public NSObject[] objectsAtIndexes(int... indexes) {
        NSObject[] result = new NSObject[indexes.length];
        Arrays.sort(indexes);
        for (int i = 0; i < indexes.length; i++) {
            result[i] = this.array[indexes[i]];
        }
        return result;
    }

    public boolean equals(Object obj) {
        return obj.getClass().equals(getClass()) && Arrays.equals(((NSArray) obj).getArray(), this.array);
    }

    public int hashCode() {
        return Arrays.deepHashCode(this.array) + 623;
    }

    /* access modifiers changed from: package-private */
    public void toXML(StringBuilder xml, int level) {
        indent(xml, level);
        xml.append("<array>");
        xml.append(NSObject.NEWLINE);
        for (NSObject o : this.array) {
            o.toXML(xml, level + 1);
            xml.append(NSObject.NEWLINE);
        }
        indent(xml, level);
        xml.append("</array>");
    }

    /* access modifiers changed from: package-private */
    public void assignIDs(BinaryPropertyListWriter out) {
        super.assignIDs(out);
        for (NSObject obj : this.array) {
            obj.assignIDs(out);
        }
    }

    /* access modifiers changed from: package-private */
    public void toBinary(BinaryPropertyListWriter out) throws IOException {
        out.writeIntHeader(10, this.array.length);
        for (NSObject obj : this.array) {
            out.writeID(out.getID(obj));
        }
    }

    public String toASCIIPropertyList() {
        StringBuilder ascii = new StringBuilder();
        toASCII(ascii, 0);
        ascii.append(NEWLINE);
        return ascii.toString();
    }

    public String toGnuStepASCIIPropertyList() {
        StringBuilder ascii = new StringBuilder();
        toASCIIGnuStep(ascii, 0);
        ascii.append(NEWLINE);
        return ascii.toString();
    }

    /* access modifiers changed from: protected */
    public void toASCII(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append((char) ASCIIPropertyListParser.ARRAY_BEGIN_TOKEN);
        int indexOfLastNewLine = ascii.lastIndexOf(NEWLINE);
        for (int i = 0; i < this.array.length; i++) {
            Class<?> objClass = this.array[i].getClass();
            if ((objClass.equals(NSDictionary.class) || objClass.equals(NSArray.class) || objClass.equals(NSData.class)) && indexOfLastNewLine != ascii.length()) {
                ascii.append(NEWLINE);
                indexOfLastNewLine = ascii.length();
                this.array[i].toASCII(ascii, level + 1);
            } else {
                if (i != 0) {
                    ascii.append(" ");
                }
                this.array[i].toASCII(ascii, 0);
            }
            if (i != this.array.length - 1) {
                ascii.append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN);
            }
            if (ascii.length() - indexOfLastNewLine > 80) {
                ascii.append(NEWLINE);
                indexOfLastNewLine = ascii.length();
            }
        }
        ascii.append((char) ASCIIPropertyListParser.ARRAY_END_TOKEN);
    }

    /* access modifiers changed from: protected */
    public void toASCIIGnuStep(StringBuilder ascii, int level) {
        indent(ascii, level);
        ascii.append((char) ASCIIPropertyListParser.ARRAY_BEGIN_TOKEN);
        int indexOfLastNewLine = ascii.lastIndexOf(NEWLINE);
        for (int i = 0; i < this.array.length; i++) {
            Class<?> objClass = this.array[i].getClass();
            if ((objClass.equals(NSDictionary.class) || objClass.equals(NSArray.class) || objClass.equals(NSData.class)) && indexOfLastNewLine != ascii.length()) {
                ascii.append(NEWLINE);
                indexOfLastNewLine = ascii.length();
                this.array[i].toASCIIGnuStep(ascii, level + 1);
            } else {
                if (i != 0) {
                    ascii.append(" ");
                }
                this.array[i].toASCIIGnuStep(ascii, 0);
            }
            if (i != this.array.length - 1) {
                ascii.append((char) ASCIIPropertyListParser.ARRAY_ITEM_DELIMITER_TOKEN);
            }
            if (ascii.length() - indexOfLastNewLine > 80) {
                ascii.append(NEWLINE);
                indexOfLastNewLine = ascii.length();
            }
        }
        ascii.append((char) ASCIIPropertyListParser.ARRAY_END_TOKEN);
    }
}
