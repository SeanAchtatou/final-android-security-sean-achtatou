package com.dd.plist;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class BinaryPropertyListWriter {
    static final /* synthetic */ boolean $assertionsDisabled = (!BinaryPropertyListWriter.class.desiredAssertionStatus());
    public static final int VERSION_00 = 0;
    public static final int VERSION_10 = 10;
    public static final int VERSION_15 = 15;
    public static final int VERSION_20 = 20;
    private long count;
    private Map<NSObject, Integer> idMap = new HashMap();
    private int idSizeInBytes;
    private OutputStream out;
    private int version = 0;

    private static int getMinimumRequiredVersion(NSObject root) {
        int minVersion = 0;
        if (root == null) {
            minVersion = 10;
        }
        if (root instanceof NSDictionary) {
            for (NSObject o : ((NSDictionary) root).getHashMap().values()) {
                int v = getMinimumRequiredVersion(o);
                if (v > minVersion) {
                    minVersion = v;
                }
            }
        } else if (root instanceof NSArray) {
            for (NSObject o2 : ((NSArray) root).getArray()) {
                int v2 = getMinimumRequiredVersion(o2);
                if (v2 > minVersion) {
                    minVersion = v2;
                }
            }
        } else if (root instanceof NSSet) {
            minVersion = 10;
            for (NSObject o3 : ((NSSet) root).allObjects()) {
                int v3 = getMinimumRequiredVersion(o3);
                if (v3 > minVersion) {
                    minVersion = v3;
                }
            }
        }
        return minVersion;
    }

    public static void write(File file, NSObject root) throws IOException {
        OutputStream out2 = new FileOutputStream(file);
        write(out2, root);
        out2.close();
    }

    public static void write(OutputStream out2, NSObject root) throws IOException {
        int minVersion = getMinimumRequiredVersion(root);
        if (minVersion > 0) {
            throw new IOException("The given property list structure cannot be saved. The required version of the binary format (" + (minVersion == 10 ? "v1.0" : minVersion == 15 ? "v1.5" : minVersion == 20 ? "v2.0" : "v0.0") + ") is not yet supported.");
        }
        new BinaryPropertyListWriter(out2, minVersion).write(root);
    }

    public static byte[] writeToArray(NSObject root) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        write(bout, root);
        return bout.toByteArray();
    }

    BinaryPropertyListWriter(OutputStream outStr) throws IOException {
        this.out = new BufferedOutputStream(outStr);
    }

    BinaryPropertyListWriter(OutputStream outStr, int version2) throws IOException {
        this.version = version2;
        this.out = new BufferedOutputStream(outStr);
    }

    /* access modifiers changed from: package-private */
    public void write(NSObject root) throws IOException {
        write(new byte[]{98, 112, 108, 105, 115, 116});
        switch (this.version) {
            case 0:
                write(new byte[]{48, 48});
                break;
            case 10:
                write(new byte[]{49, 48});
                break;
            case 15:
                write(new byte[]{49, 53});
                break;
            case 20:
                write(new byte[]{50, 48});
                break;
        }
        root.assignIDs(this);
        this.idSizeInBytes = computeIdSizeInBytes(this.idMap.size());
        long[] offsets = new long[this.idMap.size()];
        for (Map.Entry<NSObject, Integer> entry : this.idMap.entrySet()) {
            NSObject obj = (NSObject) entry.getKey();
            offsets[((Integer) entry.getValue()).intValue()] = this.count;
            if (obj == null) {
                write(0);
            } else {
                obj.toBinary(this);
            }
        }
        long offsetTableOffset = this.count;
        int offsetSizeInBytes = computeOffsetSizeInBytes(this.count);
        for (long offset : offsets) {
            writeBytes(offset, offsetSizeInBytes);
        }
        if (this.version != 15) {
            write(new byte[6]);
            write(offsetSizeInBytes);
            write(this.idSizeInBytes);
            writeLong((long) this.idMap.size());
            writeLong((long) this.idMap.get(root).intValue());
            writeLong(offsetTableOffset);
        }
        this.out.flush();
    }

    /* access modifiers changed from: package-private */
    public void assignID(NSObject obj) {
        if (!this.idMap.containsKey(obj)) {
            this.idMap.put(obj, Integer.valueOf(this.idMap.size()));
        }
    }

    /* access modifiers changed from: package-private */
    public int getID(NSObject obj) {
        return this.idMap.get(obj).intValue();
    }

    private static int computeIdSizeInBytes(int numberOfIds) {
        if (numberOfIds < 256) {
            return 1;
        }
        if (numberOfIds < 65536) {
            return 2;
        }
        return 4;
    }

    private int computeOffsetSizeInBytes(long maxOffset) {
        if (maxOffset < 256) {
            return 1;
        }
        if (maxOffset < 65536) {
            return 2;
        }
        if (maxOffset < 4294967296L) {
            return 4;
        }
        return 8;
    }

    /* access modifiers changed from: package-private */
    public void writeIntHeader(int kind, int value) throws IOException {
        if (!$assertionsDisabled && value < 0) {
            throw new AssertionError();
        } else if (value < 15) {
            write((kind << 4) + value);
        } else if (value < 256) {
            write((kind << 4) + 15);
            write(16);
            writeBytes((long) value, 1);
        } else if (value < 65536) {
            write((kind << 4) + 15);
            write(17);
            writeBytes((long) value, 2);
        } else {
            write((kind << 4) + 15);
            write(18);
            writeBytes((long) value, 4);
        }
    }

    /* access modifiers changed from: package-private */
    public void write(int b) throws IOException {
        this.out.write(b);
        this.count++;
    }

    /* access modifiers changed from: package-private */
    public void write(byte[] bytes) throws IOException {
        this.out.write(bytes);
        this.count += (long) bytes.length;
    }

    /* access modifiers changed from: package-private */
    public void writeBytes(long value, int bytes) throws IOException {
        for (int i = bytes - 1; i >= 0; i--) {
            write((int) (value >> (i * 8)));
        }
    }

    /* access modifiers changed from: package-private */
    public void writeID(int id) throws IOException {
        writeBytes((long) id, this.idSizeInBytes);
    }

    /* access modifiers changed from: package-private */
    public void writeLong(long value) throws IOException {
        writeBytes(value, 8);
    }

    /* access modifiers changed from: package-private */
    public void writeDouble(double value) throws IOException {
        writeLong(Double.doubleToRawLongBits(value));
    }
}
