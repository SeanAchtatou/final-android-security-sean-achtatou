package com.dd.plist;

import android.support.v7.internal.widget.ActivityChooserView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class PropertyListParser {
    protected PropertyListParser() {
    }

    protected static byte[] readAll(InputStream in, int max) throws IOException {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        while (max > 0) {
            int n = in.read();
            if (n == -1) {
                break;
            }
            buf.write(n);
            max--;
        }
        return buf.toByteArray();
    }

    public static NSObject parse(String filePath) throws Exception {
        return parse(new File(filePath));
    }

    public static NSObject parse(File f) throws Exception {
        FileInputStream fis = new FileInputStream(f);
        String magicString = new String(readAll(fis, 8), 0, 8);
        fis.close();
        if (magicString.startsWith("bplist")) {
            return BinaryPropertyListParser.parse(f);
        }
        if (magicString.trim().startsWith("(") || magicString.trim().startsWith("{") || magicString.trim().startsWith("/")) {
            return ASCIIPropertyListParser.parse(f);
        }
        return XMLPropertyListParser.parse(f);
    }

    public static NSObject parse(byte[] bytes) throws Exception {
        String magicString = new String(bytes, 0, 8);
        if (magicString.startsWith("bplist")) {
            return BinaryPropertyListParser.parse(bytes);
        }
        if (magicString.trim().startsWith("(") || magicString.trim().startsWith("{") || magicString.trim().startsWith("/")) {
            return ASCIIPropertyListParser.parse(bytes);
        }
        return XMLPropertyListParser.parse(bytes);
    }

    public static NSObject parse(InputStream is) throws Exception {
        if (!is.markSupported()) {
            return parse(readAll(is, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED));
        }
        is.mark(10);
        String magicString = new String(readAll(is, 8), 0, 8);
        is.reset();
        if (magicString.startsWith("bplist")) {
            return BinaryPropertyListParser.parse(is);
        }
        if (magicString.trim().startsWith("(") || magicString.trim().startsWith("{") || magicString.trim().startsWith("/")) {
            return ASCIIPropertyListParser.parse(is);
        }
        return XMLPropertyListParser.parse(is);
    }

    public static void saveAsXML(NSObject root, File out) throws IOException {
        File parent = out.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }
        FileOutputStream fous = new FileOutputStream(out);
        saveAsXML(root, fous);
        fous.close();
    }

    public static void saveAsXML(NSObject root, OutputStream out) throws IOException {
        OutputStreamWriter w = new OutputStreamWriter(out, "UTF-8");
        w.write(root.toXMLPropertyList());
        w.close();
    }

    public static void convertToXml(File in, File out) throws Exception {
        saveAsXML(parse(in), out);
    }

    public static void saveAsBinary(NSObject root, File out) throws IOException {
        File parent = out.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }
        BinaryPropertyListWriter.write(out, root);
    }

    public static void saveAsBinary(NSObject root, OutputStream out) throws IOException {
        BinaryPropertyListWriter.write(out, root);
    }

    public static void convertToBinary(File in, File out) throws Exception {
        saveAsBinary(parse(in), out);
    }

    public static void saveAsASCII(NSDictionary root, File out) throws IOException {
        File parent = out.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }
        OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(out), "ASCII");
        w.write(root.toASCIIPropertyList());
        w.close();
    }

    public static void saveAsASCII(NSArray root, File out) throws IOException {
        OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(out), "ASCII");
        w.write(root.toASCIIPropertyList());
        w.close();
    }

    public static void convertToASCII(File in, File out) throws Exception {
        NSObject root = parse(in);
        try {
            saveAsASCII((NSDictionary) root, out);
        } catch (Exception e) {
            try {
                saveAsASCII((NSArray) root, out);
            } catch (Exception e2) {
                throw new Exception("The root of the given input property list is neither a Dictionary nor an Array!");
            }
        }
    }

    public static void saveAsGnuStepASCII(NSDictionary root, File out) throws IOException {
        File parent = out.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }
        OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(out), "ASCII");
        w.write(root.toGnuStepASCIIPropertyList());
        w.close();
    }

    public static void saveAsGnuStepASCII(NSArray root, File out) throws IOException {
        File parent = out.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }
        OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(out), "ASCII");
        w.write(root.toGnuStepASCIIPropertyList());
        w.close();
    }

    public static void convertToGnuStepASCII(File in, File out) throws Exception {
        NSObject root = parse(in);
        try {
            saveAsGnuStepASCII((NSDictionary) root, out);
        } catch (Exception e) {
            try {
                saveAsGnuStepASCII((NSArray) root, out);
            } catch (Exception e2) {
                throw new Exception("The root of the given input property list is neither a Dictionary nor an Array!");
            }
        }
    }
}
