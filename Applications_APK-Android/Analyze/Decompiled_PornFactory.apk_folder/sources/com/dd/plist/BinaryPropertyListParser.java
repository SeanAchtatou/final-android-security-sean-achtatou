package com.dd.plist;

import android.support.v7.internal.widget.ActivityChooserView;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;

public class BinaryPropertyListParser {
    private byte[] bytes;
    private int majorVersion;
    private int minorVersion;
    private int numObjects;
    private int objectRefSize;
    private int offsetSize;
    private int[] offsetTable;
    private int offsetTableOffset;
    private int topObject;

    protected BinaryPropertyListParser() {
    }

    public static NSObject parse(byte[] data) throws Exception {
        return new BinaryPropertyListParser().doParse(data);
    }

    private NSObject doParse(byte[] data) throws Exception {
        this.bytes = data;
        String magic = new String(copyOfRange(this.bytes, 0, 8));
        if (!magic.startsWith("bplist")) {
            throw new Exception("The given data is no binary property list. Wrong magic bytes: " + magic);
        }
        this.majorVersion = magic.charAt(6) - '0';
        this.minorVersion = magic.charAt(7) - '0';
        if (this.majorVersion > 0) {
            throw new Exception("Unsupported binary property list format: v" + this.majorVersion + "." + this.minorVersion + ". " + "Version 1.0 and later are not yet supported.");
        }
        byte[] trailer = copyOfRange(this.bytes, this.bytes.length - 32, this.bytes.length);
        this.offsetSize = (int) parseUnsignedInt(copyOfRange(trailer, 6, 7));
        this.objectRefSize = (int) parseUnsignedInt(copyOfRange(trailer, 7, 8));
        this.numObjects = (int) parseUnsignedInt(copyOfRange(trailer, 8, 16));
        this.topObject = (int) parseUnsignedInt(copyOfRange(trailer, 16, 24));
        this.offsetTableOffset = (int) parseUnsignedInt(copyOfRange(trailer, 24, 32));
        this.offsetTable = new int[this.numObjects];
        for (int i = 0; i < this.numObjects; i++) {
            this.offsetTable[i] = (int) parseUnsignedInt(copyOfRange(this.bytes, this.offsetTableOffset + (this.offsetSize * i), this.offsetTableOffset + ((i + 1) * this.offsetSize)));
        }
        return parseObject(this.topObject);
    }

    public static NSObject parse(InputStream is) throws Exception {
        byte[] buf = PropertyListParser.readAll(is, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
        is.close();
        return parse(buf);
    }

    public static NSObject parse(File f) throws Exception {
        if (f.length() <= Runtime.getRuntime().freeMemory()) {
            return parse(new FileInputStream(f));
        }
        throw new Exception("To little heap space available! Wanted to read " + f.length() + " bytes, but only " + Runtime.getRuntime().freeMemory() + " are available.");
    }

    private NSObject parseObject(int obj) throws Exception {
        int offset = this.offsetTable[obj];
        byte type = this.bytes[offset];
        int objType = (type & 240) >> 4;
        int objInfo = type & 15;
        switch (objType) {
            case 0:
                switch (objInfo) {
                    case 0:
                        return null;
                    case 8:
                        return new NSNumber(false);
                    case 9:
                        return new NSNumber(true);
                    case 15:
                        return null;
                }
            case 1:
                int length = (int) Math.pow(2.0d, (double) objInfo);
                if (((long) length) < Runtime.getRuntime().freeMemory()) {
                    return new NSNumber(copyOfRange(this.bytes, offset + 1, offset + 1 + length), 0);
                }
                throw new Exception("To little heap space available! Wanted to read " + length + " bytes, but only " + Runtime.getRuntime().freeMemory() + " are available.");
            case 2:
                int length2 = (int) Math.pow(2.0d, (double) objInfo);
                if (((long) length2) < Runtime.getRuntime().freeMemory()) {
                    return new NSNumber(copyOfRange(this.bytes, offset + 1, offset + 1 + length2), 1);
                }
                throw new Exception("To little heap space available! Wanted to read " + length2 + " bytes, but only " + Runtime.getRuntime().freeMemory() + " are available.");
            case 3:
                if (objInfo != 3) {
                    System.err.println("BinaryPropertyListParser: Unknown date type :" + objInfo + ". Attempting to parse anyway...");
                }
                return new NSDate(copyOfRange(this.bytes, offset + 1, offset + 9));
            case 4:
                int[] lenAndoffset = readLengthAndOffset(objInfo, offset);
                int length3 = lenAndoffset[0];
                int dataoffset = lenAndoffset[1];
                if (((long) length3) < Runtime.getRuntime().freeMemory()) {
                    return new NSData(copyOfRange(this.bytes, offset + dataoffset, offset + dataoffset + length3));
                }
                throw new Exception("To little heap space available! Wanted to read " + length3 + " bytes, but only " + Runtime.getRuntime().freeMemory() + " are available.");
            case 5:
                int[] lenAndoffset2 = readLengthAndOffset(objInfo, offset);
                int length4 = lenAndoffset2[0];
                int stroffset = lenAndoffset2[1];
                if (((long) length4) < Runtime.getRuntime().freeMemory()) {
                    return new NSString(copyOfRange(this.bytes, offset + stroffset, offset + stroffset + length4), "ASCII");
                }
                throw new Exception("To little heap space available! Wanted to read " + length4 + " bytes, but only " + Runtime.getRuntime().freeMemory() + " are available.");
            case 6:
                int[] lenAndoffset3 = readLengthAndOffset(objInfo, offset);
                int length5 = lenAndoffset3[0];
                int stroffset2 = lenAndoffset3[1];
                int length6 = length5 * 2;
                if (((long) length6) < Runtime.getRuntime().freeMemory()) {
                    return new NSString(copyOfRange(this.bytes, offset + stroffset2, offset + stroffset2 + length6), "UTF-16BE");
                }
                throw new Exception("To little heap space available! Wanted to read " + length6 + " bytes, but only " + Runtime.getRuntime().freeMemory() + " are available.");
            case 7:
            case 9:
            default:
                System.err.println("Unknown object type: " + objType);
                break;
            case 8:
                int length7 = objInfo + 1;
                if (((long) length7) < Runtime.getRuntime().freeMemory()) {
                    return new UID(String.valueOf(obj), copyOfRange(this.bytes, offset + 1, offset + 1 + length7));
                }
                throw new Exception("To little heap space available! Wanted to read " + length7 + " bytes, but only " + Runtime.getRuntime().freeMemory() + " are available.");
            case 10:
                int[] lenAndoffset4 = readLengthAndOffset(objInfo, offset);
                int length8 = lenAndoffset4[0];
                int arrayoffset = lenAndoffset4[1];
                if (((long) (this.objectRefSize * length8)) > Runtime.getRuntime().freeMemory()) {
                    throw new Exception("To little heap space available!");
                }
                NSArray array = new NSArray(length8);
                for (int i = 0; i < length8; i++) {
                    array.setValue(i, parseObject((int) parseUnsignedInt(copyOfRange(this.bytes, offset + arrayoffset + (this.objectRefSize * i), offset + arrayoffset + ((i + 1) * this.objectRefSize)))));
                }
                return array;
            case 11:
                int[] lenAndoffset5 = readLengthAndOffset(objInfo, offset);
                int length9 = lenAndoffset5[0];
                int contentOffset = lenAndoffset5[1];
                if (((long) (this.objectRefSize * length9)) > Runtime.getRuntime().freeMemory()) {
                    throw new Exception("To little heap space available!");
                }
                NSSet nSSet = new NSSet(true);
                for (int i2 = 0; i2 < length9; i2++) {
                    nSSet.addObject(parseObject((int) parseUnsignedInt(copyOfRange(this.bytes, offset + contentOffset + (this.objectRefSize * i2), offset + contentOffset + ((i2 + 1) * this.objectRefSize)))));
                }
                return nSSet;
            case 12:
                int[] lenAndoffset6 = readLengthAndOffset(objInfo, offset);
                int length10 = lenAndoffset6[0];
                int contentOffset2 = lenAndoffset6[1];
                if (((long) (this.objectRefSize * length10)) > Runtime.getRuntime().freeMemory()) {
                    throw new Exception("To little heap space available!");
                }
                NSSet set = new NSSet();
                for (int i3 = 0; i3 < length10; i3++) {
                    set.addObject(parseObject((int) parseUnsignedInt(copyOfRange(this.bytes, offset + contentOffset2 + (this.objectRefSize * i3), offset + contentOffset2 + ((i3 + 1) * this.objectRefSize)))));
                }
                return set;
            case 13:
                int[] lenAndoffset7 = readLengthAndOffset(objInfo, offset);
                int length11 = lenAndoffset7[0];
                int contentOffset3 = lenAndoffset7[1];
                if (((long) (length11 * 2 * this.objectRefSize)) > Runtime.getRuntime().freeMemory()) {
                    throw new Exception("To little heap space available!");
                }
                NSDictionary dict = new NSDictionary();
                for (int i4 = 0; i4 < length11; i4++) {
                    NSObject key = parseObject((int) parseUnsignedInt(copyOfRange(this.bytes, offset + contentOffset3 + (this.objectRefSize * i4), offset + contentOffset3 + ((i4 + 1) * this.objectRefSize))));
                    dict.put(key.toString(), parseObject((int) parseUnsignedInt(copyOfRange(this.bytes, offset + contentOffset3 + (this.objectRefSize * length11) + (this.objectRefSize * i4), offset + contentOffset3 + (this.objectRefSize * length11) + ((i4 + 1) * this.objectRefSize)))));
                }
                return dict;
        }
        return null;
    }

    private int[] readLengthAndOffset(int objInfo, int offset) {
        int length = objInfo;
        int stroffset = 1;
        if (objInfo == 15) {
            byte b = this.bytes[offset + 1];
            int intType = (b & 240) >> 4;
            if (intType != 1) {
                System.err.println("BinaryPropertyListParser: Length integer has an unexpected type" + intType + ". Attempting to parse anyway...");
            }
            int intLength = (int) Math.pow(2.0d, (double) (b & 15));
            stroffset = intLength + 2;
            if (intLength < 3) {
                length = (int) parseUnsignedInt(copyOfRange(this.bytes, offset + 2, offset + 2 + intLength));
            } else {
                length = new BigInteger(copyOfRange(this.bytes, offset + 2, offset + 2 + intLength)).intValue();
            }
        }
        return new int[]{length, stroffset};
    }

    public static final long parseUnsignedInt(byte[] bytes2) {
        long l = 0;
        for (byte b : bytes2) {
            l = (l << 8) | ((long) (b & 255));
        }
        return l & 4294967295L;
    }

    public static final long parseLong(byte[] bytes2) {
        long l = 0;
        for (byte b : bytes2) {
            l = (l << 8) | ((long) (b & 255));
        }
        return l;
    }

    public static final double parseDouble(byte[] bytes2) {
        if (bytes2.length == 8) {
            return Double.longBitsToDouble(parseLong(bytes2));
        }
        if (bytes2.length == 4) {
            return (double) Float.intBitsToFloat((int) parseLong(bytes2));
        }
        throw new IllegalArgumentException("bad byte array length " + bytes2.length);
    }

    public static byte[] copyOfRange(byte[] src, int startIndex, int endIndex) {
        int length = endIndex - startIndex;
        if (length < 0) {
            throw new IllegalArgumentException("startIndex (" + startIndex + ")" + " > endIndex (" + endIndex + ")");
        }
        byte[] dest = new byte[length];
        System.arraycopy(src, startIndex, dest, 0, length);
        return dest;
    }
}
