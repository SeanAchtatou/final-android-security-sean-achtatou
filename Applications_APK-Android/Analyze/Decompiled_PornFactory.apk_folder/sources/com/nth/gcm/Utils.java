package com.nth.gcm;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;

class Utils {
    Utils() {
    }

    public static void doLog(Object o) {
    }

    public static void doLogException(Exception e) {
    }

    public static String getDeviceID(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
    }

    public static Location getLastKnownLocation(Context context) {
        try {
            LocationManager lm = (LocationManager) context.getSystemService("location");
            Criteria crit = new Criteria();
            crit.setAccuracy(1);
            return lm.getLastKnownLocation(lm.getBestProvider(crit, true));
        } catch (Exception e) {
            doLogException(e);
            return null;
        }
    }
}
