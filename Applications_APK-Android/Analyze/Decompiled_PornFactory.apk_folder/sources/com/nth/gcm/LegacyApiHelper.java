package com.nth.gcm;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.dd.plist.ASCIIPropertyListParser;
import com.google.android.gcm.GCMRegistrar;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

class LegacyApiHelper {
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final int MAX_ATTEMPTS = 5;
    private static final String TAG = "GCMManager";
    private static final Random random = new Random();

    LegacyApiHelper() {
    }

    static void register(final Context context, final String registerUrl, String id, final Map<String, String> params) {
        try {
            GCMRegistrar.checkDevice(context);
            GCMRegistrar.checkManifest(context);
            final String regId = GCMRegistrar.getRegistrationId(context);
            if (regId.equals("")) {
                GCMRegistrar.register(context, id);
            } else if (GCMRegistrar.isRegisteredOnServer(context)) {
                Log.i(TAG, "Already registered");
            } else {
                new AsyncTask<Void, Void, Void>() {
                    /* access modifiers changed from: protected */
                    public Void doInBackground(Void... p) {
                        boolean registered;
                        if (params == null) {
                            registered = LegacyApiHelper.registerOnServer(context, registerUrl, regId);
                        } else {
                            params.put("token", regId);
                            registered = LegacyApiHelper.registerOnServer(context, registerUrl, params);
                        }
                        if (registered) {
                            return null;
                        }
                        GCMRegistrar.unregister(context);
                        return null;
                    }
                }.execute(null, null, null);
            }
        } catch (Exception e) {
        }
    }

    static boolean registerOnServer(Context context, String registerUrl, String regId) {
        Map<String, String> params = new HashMap<>();
        params.put("token", regId);
        params.put(DmsConstants.LANG, "en");
        params.put("category", "all");
        return registerOnServer(context, registerUrl, params);
    }

    static boolean registerOnServer(Context context, String registerUrl, Map<String, String> params) {
        Log.i(TAG, "registering device (regId = " + params.get("token") + ")");
        params.put("device_id", Utils.getDeviceID(context));
        long backoff = (long) (random.nextInt(1000) + 2000);
        int i = 1;
        while (i <= 5) {
            Log.d(TAG, "Attempt #" + i + " to register");
            try {
                send(registerUrl, params);
                GCMRegistrar.setRegisteredOnServer(context, true);
                return true;
            } catch (IOException e) {
                Log.e(TAG, "Failed to register on attempt " + i, e);
                if (i == 5) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                    backoff *= 2;
                    i++;
                } catch (InterruptedException e2) {
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return false;
                }
            }
        }
        return false;
    }

    static void unregisterOnServer(Context context) {
        if (GCMRegistrar.isRegisteredOnServer(context)) {
            GCMRegistrar.setRegisteredOnServer(context, false);
        } else {
            Log.i(TAG, "Ignoring unregister callback");
        }
        Log.i(TAG, "Unregistering device");
    }

    static void unregisterOnServer(Context context, String unregisterUrl, String regId) {
        if (GCMRegistrar.isRegisteredOnServer(context)) {
            Log.i(TAG, "Unregistering device (regId = " + regId + ")");
            Map<String, String> params = new HashMap<>();
            params.put("token", regId);
            params.put("device_id", Utils.getDeviceID(context));
            params.put(DmsConstants.LANG, Locale.getDefault().getLanguage());
            try {
                send(unregisterUrl, params);
                GCMRegistrar.setRegisteredOnServer(context, false);
            } catch (IOException e) {
            }
        } else {
            Log.i(TAG, "Ignoring unregister callback");
        }
        Log.i(TAG, "Unregistering device");
    }

    static void send(String endpoint, Map<String, String> params) throws IOException {
        HttpClient httpClient = new DefaultHttpClient();
        StringBuilder paramsBuilder = new StringBuilder();
        Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> param = iterator.next();
            paramsBuilder.append((String) param.getKey()).append((char) ASCIIPropertyListParser.DICTIONARY_ASSIGN_TOKEN).append((String) param.getValue());
            if (iterator.hasNext()) {
                paramsBuilder.append('&');
            }
        }
        try {
            int statusCode = httpClient.execute(new HttpGet(String.valueOf(endpoint) + "?" + paramsBuilder.toString())).getStatusLine().getStatusCode();
            if (statusCode != 200) {
                throw new IOException("Post failed with error code " + statusCode);
            }
            try {
                httpClient.getConnectionManager().shutdown();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                httpClient.getConnectionManager().shutdown();
            } catch (Exception e3) {
            }
        } catch (Throwable th) {
            try {
                httpClient.getConnectionManager().shutdown();
            } catch (Exception e4) {
            }
            throw th;
        }
    }
}
