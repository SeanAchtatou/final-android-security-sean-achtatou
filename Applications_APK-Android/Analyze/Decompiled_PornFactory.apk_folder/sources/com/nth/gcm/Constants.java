package com.nth.gcm;

class Constants {
    public static final long FASTEST_INTERVAL = 1000;
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    private static final int MILLISECONDS_PER_SECOND = 1000;
    public static final long UPDATE_INTERVAL = 5000;
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;

    Constants() {
    }
}
