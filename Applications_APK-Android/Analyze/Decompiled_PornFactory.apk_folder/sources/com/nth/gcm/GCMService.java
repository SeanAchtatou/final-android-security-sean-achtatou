package com.nth.gcm;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public abstract class GCMService extends GCMBaseIntentService {
    private GooglePlayServicesClient.ConnectionCallbacks mConnectionCallbacks = new GooglePlayServicesClient.ConnectionCallbacks() {
        public void onDisconnected() {
            Utils.doLog("GCM: location onDisconnected");
        }

        public void onConnected(Bundle connectionHint) {
            Utils.doLog("GCM: location onConnected");
            GCMService.this.mLocationClient.requestLocationUpdates(GCMService.this.mLocationRequest, GCMService.this.mLocationListener);
        }
    };
    /* access modifiers changed from: private */
    public LocationClient mLocationClient;
    /* access modifiers changed from: private */
    public LocationListener mLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            Utils.doLog("GCM: onLocationChanged, latitude: " + location.getLatitude() + ", longitude " + location.getLongitude());
            GCMManager.registerOnServer(GCMService.this.getApplicationContext(), GCMService.this.getRegistrationUrl(GCMService.this.getApplicationContext()), GCMRegistrar.getRegistrationId(GCMService.this.getApplicationContext()), GCMService.this.getApiKey(), location.getLatitude(), location.getLongitude());
            GCMService.this.mLocationClient.removeLocationUpdates(this);
            GCMService.this.mLocationClient.disconnect();
        }
    };
    /* access modifiers changed from: private */
    public LocationRequest mLocationRequest;
    private GooglePlayServicesClient.OnConnectionFailedListener mOnConnectionFailedListener = new GooglePlayServicesClient.OnConnectionFailedListener() {
        public void onConnectionFailed(ConnectionResult result) {
            Utils.doLog("GCM: onConnectionFailed, error code: " + result.getErrorCode());
        }
    };

    public abstract String getApiKey();

    public abstract String getRegistrationUrl(Context context);

    public abstract String getUnregistrationUrl(Context context);

    public abstract boolean registerTokenWithLocation();

    /* access modifiers changed from: protected */
    public void onError(Context context, String errorId) {
        Log.w(GCMBaseIntentService.TAG, "Received error: " + errorId);
    }

    /* access modifiers changed from: protected */
    public void onMessage(Context context, Intent intent) {
    }

    /* access modifiers changed from: protected */
    public void onRegistered(Context context, String regId) {
        Log.i(GCMBaseIntentService.TAG, "Device registered: regId = " + regId);
        String apiKey = getApiKey();
        String regUrl = getRegistrationUrl(context);
        if (registerTokenWithLocation()) {
            tryRegisterTokenWithLocation(context, regId, apiKey, regUrl);
        } else if (TextUtils.isEmpty(apiKey)) {
            GCMManager.registerOnServer(context, regUrl, regId);
        } else {
            GCMManager.registerOnServer(context, regUrl, regId, apiKey);
        }
    }

    /* access modifiers changed from: protected */
    public void onUnregistered(Context context, String regId) {
        Log.i(GCMBaseIntentService.TAG, "Device unregistered");
        GCMManager.unregisterOnServer(context);
    }

    private void tryRegisterTokenWithLocation(Context context, String regId, String apiKey, String regUrl) {
        if (TextUtils.isEmpty(apiKey)) {
            Utils.doLog("GCM: apiKey is null, cancelling location update");
            return;
        }
        GCMManager.registerOnServer(context, regUrl, regId, apiKey);
        Location lastKnownLocation = Utils.getLastKnownLocation(context);
        if (lastKnownLocation != null) {
            Utils.doLog("last known location is available, try to register on server");
            GCMManager.registerOnServer(getApplicationContext(), regUrl, regId, apiKey, lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
        } else if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) == 0) {
            this.mLocationRequest = LocationRequest.create();
            this.mLocationRequest.setPriority(100);
            this.mLocationRequest.setInterval(5000);
            this.mLocationRequest.setFastestInterval(1000);
            this.mLocationClient = new LocationClient(context, this.mConnectionCallbacks, this.mOnConnectionFailedListener);
            this.mLocationClient.connect();
        } else {
            Utils.doLog("GCM: msg google play services are not available");
        }
    }
}
