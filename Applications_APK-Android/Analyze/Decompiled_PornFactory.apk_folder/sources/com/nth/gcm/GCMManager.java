package com.nth.gcm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.google.android.gcm.GCMRegistrar;
import com.nth.analytics.android.LocalyticsProvider;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;

public class GCMManager {
    public static String TAG = "GCMManager";

    public static void register(Context context, String registerUrl, String id) {
        register(context, registerUrl, id, (Map<String, String>) null);
    }

    public static void register(Context context, String registerUrl, String id, Map<String, String> params) {
        LegacyApiHelper.register(context, registerUrl, id, params);
    }

    public static void register(Context context, String registerUrl, String id, String apiKey) {
        register(context, registerUrl, id, apiKey, null);
    }

    public static void register(Context context, String registerUrl, String id, String apiKey, Map<String, Object> params) {
        JSONApiHelper.register(context, registerUrl, id, apiKey, params);
    }

    public static void unregister(Context context) {
        GCMRegistrar.unregister(context);
    }

    public static String getMessageAlert(Intent intent) {
        Bundle b = intent.getExtras();
        if (b != null) {
            return b.getString("alert");
        }
        return "";
    }

    @Deprecated
    public static boolean registerOnServer(Context context, String registerUrl, String regId) {
        return LegacyApiHelper.registerOnServer(context, registerUrl, regId);
    }

    @Deprecated
    public static boolean registerOnServer(Context context, String registerUrl, Map<String, String> params) {
        return LegacyApiHelper.registerOnServer(context, registerUrl, params);
    }

    @Deprecated
    public static void unregisterOnServer(Context context) {
        LegacyApiHelper.unregisterOnServer(context);
    }

    @Deprecated
    public static void unregisterOnServer(Context context, String unregisterUrl, String regId) {
        LegacyApiHelper.unregisterOnServer(context, unregisterUrl, regId);
    }

    static boolean registerOnServer(Context context, String registerUrl, String regId, String apiKey) {
        Map<String, Object> params = new HashMap<>();
        params.put("token", regId);
        params.put(DmsConstants.LANG, "en");
        params.put("deviceId", Utils.getDeviceID(context));
        params.put(T.string.categories, new JSONArray((Collection) Arrays.asList("all")));
        return JSONApiHelper.registerOnServer(context, registerUrl, params, apiKey);
    }

    static void registerOnServer(Context context, String registerUrl, String regId, String apiKey, double latitude, double longitude) {
        if (TextUtils.isEmpty(regId)) {
            Utils.doLog("regId is null, cancel sending task");
            return;
        }
        Map<String, Object> params = new HashMap<>();
        params.put("token", regId);
        params.put(LocalyticsProvider.SessionsDbColumns.LATITUDE, Double.valueOf(latitude));
        params.put(LocalyticsProvider.SessionsDbColumns.LONGITUDE, Double.valueOf(longitude));
        params.put("deviceId", Utils.getDeviceID(context));
        try {
            JSONApiHelper.post(registerUrl, params, apiKey);
        } catch (IOException e) {
            Utils.doLogException(e);
        }
    }

    public static void changeLanguage(Context context, String changeLangUrl, String lang, String apiKey) {
        JSONApiHelper.changeLanguage(context, changeLangUrl, lang, apiKey);
    }

    public static void resetBadge(Context context, String resetBadgeUrl, String apiKey, int badgeValue) {
        JSONApiHelper.resetBadge(context, resetBadgeUrl, apiKey, badgeValue);
    }

    public static void addCategory(Context context, String addCategoryUrl, String categoryName, String apiKey) {
        JSONApiHelper.addCategory(context, addCategoryUrl, categoryName, apiKey);
    }

    public static void deleteCategory(Context context, String resetBadgeUrl, String category, String apiKey) {
        JSONApiHelper.deleteCategory(context, resetBadgeUrl, category, apiKey);
    }

    public static String getDeviceId(Context context) {
        return Utils.getDeviceID(context);
    }
}
