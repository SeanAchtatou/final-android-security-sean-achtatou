package com.nth.gcm;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import ch.nth.android.contentabo.translations.T;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.google.android.gcm.GCMRegistrar;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

class JSONApiHelper {
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final int MAX_ATTEMPTS = 5;
    private static final String TAG = "GCMManager";
    private static final Random random = new Random();

    JSONApiHelper() {
    }

    public static void register(Context context, String registerUrl, String id, String apiKey, Map<String, Object> params) {
        try {
            GCMRegistrar.checkDevice(context);
            GCMRegistrar.checkManifest(context);
            final String regId = GCMRegistrar.getRegistrationId(context);
            if (regId.equals("")) {
                GCMRegistrar.register(context, id);
            } else if (GCMRegistrar.isRegisteredOnServer(context)) {
                Log.i(TAG, "Already registered");
            } else {
                final Map<String, Object> map = params;
                final Context context2 = context;
                final String str = registerUrl;
                final String str2 = apiKey;
                new AsyncTask<Void, Void, Void>() {
                    /* access modifiers changed from: protected */
                    public Void doInBackground(Void... p) {
                        boolean registered;
                        if (map == null) {
                            registered = JSONApiHelper.registerOnServer(context2, str, regId, str2);
                        } else {
                            map.put("token", regId);
                            registered = JSONApiHelper.registerOnServer(context2, str, map, str2);
                        }
                        if (registered) {
                            return null;
                        }
                        GCMRegistrar.unregister(context2);
                        return null;
                    }
                }.execute(null, null, null);
            }
        } catch (Exception e) {
            Utils.doLogException(e);
        }
    }

    static boolean registerOnServer(Context context, String registerUrl, String regId, String apiKey) {
        Map<String, Object> params = new HashMap<>();
        params.put("token", regId);
        params.put(DmsConstants.LANG, "en");
        params.put("deviceId", Utils.getDeviceID(context));
        params.put(T.string.categories, new JSONArray((Collection) Arrays.asList("all")));
        return registerOnServer(context, registerUrl, params, apiKey);
    }

    static boolean registerOnServer(Context context, String registerUrl, Map<String, Object> params, String apiKey) {
        long backoff = (long) (random.nextInt(1000) + 2000);
        int i = 1;
        while (i <= 5) {
            Utils.doLog("Attempt #" + i + " to register for JSON");
            try {
                post(registerUrl, params, apiKey);
                GCMRegistrar.setRegisteredOnServer(context, true);
                return true;
            } catch (IOException e) {
                Utils.doLog("Failed to register on attempt " + i + " " + e.getMessage());
                if (i == 5) {
                    break;
                }
                try {
                    Utils.doLog("Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                    backoff *= 2;
                    i++;
                } catch (InterruptedException e2) {
                    Utils.doLog("Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return false;
                }
            }
        }
        return false;
    }

    static void post(String endpoint, Map<String, Object> params, String apiKey) throws IOException {
        params.put("apiKey", apiKey);
        HttpPost httpPost = new HttpPost(endpoint);
        try {
            new StringEntity(new JSONObject(params).toString());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        StringEntity stringEntity = new StringEntity(new JSONObject(params).toString());
        Utils.doLog(" apiKey " + apiKey + " url " + endpoint + " --> " + new JSONObject(params).toString());
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("Authorization", "ApiKey " + apiKey);
        httpPost.setEntity(stringEntity);
        HttpClient httpClient = new DefaultHttpClient();
        try {
            int statusCode = httpClient.execute(httpPost).getStatusLine().getStatusCode();
            if (statusCode != 200) {
                throw new IOException("Post failed with error code " + statusCode);
            }
            try {
                httpClient.getConnectionManager().shutdown();
            } catch (Exception e2) {
                Utils.doLogException(e2);
            }
        } catch (Exception e3) {
            Utils.doLogException(e3);
            try {
                httpClient.getConnectionManager().shutdown();
            } catch (Exception e4) {
                Utils.doLogException(e4);
            }
        } catch (Throwable th) {
            try {
                httpClient.getConnectionManager().shutdown();
            } catch (Exception e5) {
                Utils.doLogException(e5);
            }
            throw th;
        }
    }

    static void resetBadge(Context context, String resetBadgeUrl, String apiKey, int badgeValue) {
        String regId = GCMRegistrar.getRegistrationId(context);
        if (regId.equals("")) {
            Utils.doLog("resetBadge: regId is null");
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("token", regId);
        params.put("badgeValue", String.valueOf(badgeValue));
        executeAsyncTask(resetBadgeUrl, params, "PUT", apiKey);
    }

    static void deleteCategory(Context context, String resetBadgeUrl, String category, String apiKey) {
        String regId = GCMRegistrar.getRegistrationId(context);
        if (regId.equals("")) {
            Utils.doLog("resetBadge: regId is null");
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("token", regId);
        params.put("category", category);
        executeAsyncTask(resetBadgeUrl, params, "DELETE", apiKey);
    }

    static void addCategory(Context context, final String addCategoryUrl, final String categoryName, final String apiKey) {
        final String regId = GCMRegistrar.getRegistrationId(context);
        if (regId.equals("")) {
            Utils.doLog("addCategory: regId is null");
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... p) {
                Uri.Builder uBuilder = Uri.parse(addCategoryUrl).buildUpon();
                uBuilder.appendQueryParameter("token", regId);
                uBuilder.appendQueryParameter("category", categoryName);
                HttpPost httpPost = new HttpPost(uBuilder.toString());
                HttpClient httpClient = new DefaultHttpClient();
                try {
                    httpPost.setHeader("Authorization", "ApiKey " + apiKey);
                    int statusCode = httpClient.execute(httpPost).getStatusLine().getStatusCode();
                    if (statusCode != 200) {
                        throw new IOException("Post failed with error code " + statusCode);
                    }
                    try {
                        httpClient.getConnectionManager().shutdown();
                        return null;
                    } catch (Exception e) {
                        Utils.doLogException(e);
                        return null;
                    }
                } catch (Exception e2) {
                    Utils.doLogException(e2);
                    try {
                        httpClient.getConnectionManager().shutdown();
                        return null;
                    } catch (Exception e3) {
                        Utils.doLogException(e3);
                        return null;
                    }
                } catch (Throwable th) {
                    try {
                        httpClient.getConnectionManager().shutdown();
                    } catch (Exception e4) {
                        Utils.doLogException(e4);
                    }
                    throw th;
                }
            }
        }.execute(null, null, null);
    }

    static void changeLanguage(Context context, String changeLangUrl, String lang, String apiKey) {
        String regId = GCMRegistrar.getRegistrationId(context);
        if (regId.equals("")) {
            Utils.doLog("changeLanguage: regId is null");
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("token", regId);
        params.put(DmsConstants.LANG, lang);
        executeAsyncTask(changeLangUrl, params, "PUT", apiKey);
    }

    private static void executeAsyncTask(final String url, final Map<String, String> parameters, final String method, final String apiKey) {
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... p) {
                Uri.Builder uBuilder = Uri.parse(url).buildUpon();
                for (Map.Entry<String, String> mapEntry : parameters.entrySet()) {
                    uBuilder.appendQueryParameter(((String) mapEntry.getKey()).toString(), ((String) mapEntry.getValue()).toString());
                }
                Utils.doLog("executeAsyncTask : " + uBuilder.toString() + " to " + url + " apiKey " + apiKey);
                JSONApiHelper.createGenericHttpRequest(uBuilder.toString(), method, apiKey);
                return null;
            }
        }.execute(null, null, null);
    }

    /* access modifiers changed from: private */
    public static void createGenericHttpRequest(String url, String method, String apiKey) {
        HttpURLConnection httpCon = null;
        try {
            try {
                HttpURLConnection httpCon2 = (HttpURLConnection) new URL(url).openConnection();
                httpCon2.setRequestProperty("Authorization", "ApiKey " + apiKey);
                httpCon2.setDoOutput(true);
                httpCon2.setRequestMethod(method);
                httpCon2.getResponseCode();
                httpCon2.getInputStream();
                if (httpCon2.getResponseCode() != 200) {
                    throw new IOException(String.valueOf(method) + " failed with response code " + httpCon2.getResponseCode());
                }
                try {
                    httpCon2.disconnect();
                } catch (Exception e) {
                    Utils.doLogException(e);
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    Utils.doLogException(e);
                    try {
                        httpCon.disconnect();
                    } catch (Exception e3) {
                        Utils.doLogException(e3);
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        httpCon.disconnect();
                    } catch (Exception e4) {
                        Utils.doLogException(e4);
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                httpCon.disconnect();
                throw th;
            }
        } catch (Exception e5) {
            e = e5;
            Utils.doLogException(e);
            httpCon.disconnect();
        }
    }
}
