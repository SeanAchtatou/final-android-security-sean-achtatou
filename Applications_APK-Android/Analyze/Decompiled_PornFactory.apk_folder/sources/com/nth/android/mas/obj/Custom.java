package com.nth.android.mas.obj;

public class Custom {
    public int adType;
    public String contentUrl;
    public int height;
    public int launchType;
    public String position;
    public String redirectUrl;
    public String url;
    public String verifyId;
    public int webviewAnimationType;
    public int width;
}
