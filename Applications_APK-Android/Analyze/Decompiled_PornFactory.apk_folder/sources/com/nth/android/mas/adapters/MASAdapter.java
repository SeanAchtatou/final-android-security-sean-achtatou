package com.nth.android.mas.adapters;

import com.nth.android.mas.MASLayout;
import com.nth.android.mas.obj.Ration;
import java.lang.ref.WeakReference;

public abstract class MASAdapter {
    protected final WeakReference<MASLayout> masLayoutReference;
    protected Ration ration;

    public abstract void handle();

    public MASAdapter(MASLayout masLayout, Ration ration2) {
        this.masLayoutReference = new WeakReference<>(masLayout);
        this.ration = ration2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static MASAdapter getAdapter(MASLayout masLayout, Ration ration2) {
        try {
            switch (ration2.type) {
                case 3:
                    return new CustomAdapter(masLayout, ration2);
                case 4:
                    return new OpenXAdapter(masLayout, ration2);
                case 5:
                    return new OrmmaAdapter(masLayout, ration2);
                case 6:
                    return new AdMobAdapter(masLayout, ration2);
                default:
                    return unknownAdNetwork(masLayout, ration2);
            }
        } catch (VerifyError e) {
            return unknownAdNetwork(masLayout, ration2);
        }
        return unknownAdNetwork(masLayout, ration2);
    }

    private static MASAdapter unknownAdNetwork(MASLayout masLayout, Ration ration2) {
        return null;
    }

    public static void handle(MASLayout masLayout, Ration ration2) throws Throwable {
        MASAdapter adapter = getAdapter(masLayout, ration2);
        if (adapter != null) {
            adapter.handle();
            return;
        }
        throw new Exception("Invalid adapter");
    }
}
