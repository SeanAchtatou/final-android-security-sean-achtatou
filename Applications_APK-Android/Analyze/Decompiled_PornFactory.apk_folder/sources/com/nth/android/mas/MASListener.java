package com.nth.android.mas;

public interface MASListener {
    void onAdClick();

    void onAdDismiss();

    void onAdFailed();

    void onAdSuccess();
}
