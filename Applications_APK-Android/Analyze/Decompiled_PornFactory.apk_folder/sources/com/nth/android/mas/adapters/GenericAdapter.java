package com.nth.android.mas.adapters;

import com.nth.android.mas.MASLayout;
import com.nth.android.mas.obj.Ration;

public class GenericAdapter extends MASAdapter {
    public GenericAdapter(MASLayout masLayout, Ration ration) {
        super(masLayout, ration);
    }

    public void handle() {
        MASLayout masLayout = (MASLayout) this.masLayoutReference.get();
        if (masLayout != null) {
            masLayout.masManager.resetRollover();
            masLayout.rotateThreadedDelayed();
        }
    }
}
