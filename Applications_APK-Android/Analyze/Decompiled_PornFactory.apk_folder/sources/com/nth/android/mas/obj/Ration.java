package com.nth.android.mas.obj;

public class Ration implements Comparable<Ration> {
    public String key = "";
    public String key2 = "";
    public String name = "";
    public String nid = "";
    public int priority = 0;
    public int type = 0;
    public String url = "";
    public double weight = 0.0d;

    public int compareTo(Ration another) {
        int otherPriority = another.priority;
        if (this.priority < otherPriority) {
            return -1;
        }
        if (this.priority > otherPriority) {
            return 1;
        }
        return 0;
    }
}
