package com.nth.android.mas.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public class MASUtil {
    public static final int CUSTOM_TYPE_ADMOB = 6;
    public static final int CUSTOM_TYPE_BANNER = 1;
    public static final int CUSTOM_TYPE_ICON = 2;
    public static final int CUSTOM_TYPE_OPENX = 4;
    public static final int CUSTOM_TYPE_ORMMA = 5;
    public static final int CUSTOM_TYPE_WEBVIEW = 3;
    public static final String MAS = "MASSDK";
    public static final int VERSION = 300;
    private static double density = -1.0d;
    public static final String locationString = "&location=%f,%f&location_timestamp=%d";
    private static String userAgentString;

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService("connectivity");
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public static double getDensity(Activity activity) {
        if (density == -1.0d) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            density = (double) displayMetrics.density;
        }
        return density;
    }

    public static int getWidth(Context context) {
        return ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getWidth();
    }

    public static int getHeight(Context context) {
        return ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getHeight();
    }

    public static String getDeviceIdentifier(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (byte element : data) {
            int halfbyte = (element >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = element & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }

    public static int convertToScreenPixels(int dipPixels, double density2) {
        return (int) convertToScreenPixels((double) dipPixels, density2);
    }

    public static double convertToScreenPixels(double dipPixels, double density2) {
        return density2 > 0.0d ? dipPixels * density2 : dipPixels;
    }

    public static void setUserAgentString(String userAgentString2) {
        userAgentString = userAgentString2;
    }

    public static String getUserAgentString() {
        return userAgentString;
    }
}
