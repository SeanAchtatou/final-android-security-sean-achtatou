package com.nth.android.mas;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.google.android.gms.drive.DriveFile;
import com.nth.android.mas.adapters.MASAdapter;
import com.nth.android.mas.obj.Custom;
import com.nth.android.mas.obj.Extra;
import com.nth.android.mas.obj.Ration;
import com.nth.android.mas.util.MASUtil;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.ormma.view.Browser;
import org.ormma.view.OrmmaView;

public class MASLayout extends RelativeLayout {
    private static final String ATTR_APP_ID = "appId";
    private static final String ATTR_CONFIG_URL = "configUrl";
    private static final int FULLSCREEN_INTERVAL = 5000;
    public static final String MAS_APP_ID = "MAS_APP_ID";
    public static final String MAS_CONFIG_URL = "MAS_CONFIG_URL";
    private static final String SCHEME = "http://schemas.android.com/apk/lib/com.nth.android.mas";
    public Ration activeRation;
    public WeakReference<Activity> activityReference;
    public Custom custom;
    public Extra extra;
    public final Handler handler;
    /* access modifiers changed from: private */
    public boolean hasWindow;
    /* access modifiers changed from: private */
    public boolean isScheduled;
    private String masConfigUrl;
    private String masId;
    private MASListener masListener;
    public MASManager masManager;
    public Ration nextRation;
    int numAttempts;
    public final ScheduledExecutorService scheduler;
    public WeakReference<RelativeLayout> superViewReference;

    public MASLayout(Activity context, String keyMAS) {
        super(context);
        this.handler = new Handler();
        this.scheduler = Executors.newScheduledThreadPool(1);
        this.numAttempts = 0;
        String configUrl = getConfigUrl(context);
        MASUtil.setUserAgentString(new WebView(getContext()).getSettings().getUserAgentString());
        init(context, keyMAS, configUrl);
    }

    public MASLayout(Activity context, String keyMAS, String configUrl) {
        super(context);
        this.handler = new Handler();
        this.scheduler = Executors.newScheduledThreadPool(1);
        this.numAttempts = 0;
        MASUtil.setUserAgentString(new WebView(getContext()).getSettings().getUserAgentString());
        init(context, keyMAS, configUrl);
    }

    public MASLayout(Context context) {
        this(context, (AttributeSet) null);
    }

    public MASLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.handler = new Handler();
        this.scheduler = Executors.newScheduledThreadPool(1);
        this.numAttempts = 0;
        String key = null;
        String configUrl = null;
        try {
            key = attrs.getAttributeValue(SCHEME, ATTR_APP_ID);
            configUrl = attrs.getAttributeValue(SCHEME, ATTR_CONFIG_URL);
            key = key.startsWith("@") ? getResources().getString(attrs.getAttributeResourceValue(SCHEME, ATTR_APP_ID, 0)) : key;
            if (configUrl.startsWith("@")) {
                configUrl = getResources().getString(attrs.getAttributeResourceValue(SCHEME, ATTR_CONFIG_URL, 0));
            }
        } catch (Exception e) {
        }
        key = TextUtils.isEmpty(key) ? getMASKey(context) : key;
        configUrl = TextUtils.isEmpty(configUrl) ? getConfigUrl(context) : configUrl;
        MASUtil.setUserAgentString(new WebView(getContext()).getSettings().getUserAgentString());
        init((Activity) context, key, configUrl);
    }

    /* access modifiers changed from: protected */
    public String getMASKey(Context context) {
        return getStringFromMeta(context, MAS_APP_ID);
    }

    /* access modifiers changed from: protected */
    public String getConfigUrl(Context context) {
        return getStringFromMeta(context, MAS_CONFIG_URL);
    }

    /* access modifiers changed from: protected */
    public String getStringFromMeta(Context context, String metaKey) {
        String packageName = context.getPackageName();
        String activityName = context.getClass().getName();
        PackageManager pm = context.getPackageManager();
        try {
            Bundle bundle = pm.getActivityInfo(new ComponentName(packageName, activityName), 128).metaData;
            if (bundle != null) {
                String str = bundle.getString(metaKey);
                if (!TextUtils.isEmpty(str)) {
                    return str;
                }
            }
            try {
                Bundle bundle2 = pm.getApplicationInfo(packageName, 128).metaData;
                if (bundle2 != null) {
                    return bundle2.getString(metaKey);
                }
                return null;
            } catch (PackageManager.NameNotFoundException e) {
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void init(Activity context, String keyMAS) {
        init(context, keyMAS, null);
    }

    /* access modifiers changed from: protected */
    public void init(Activity context, String keyMAS, String configUrl) {
        this.activityReference = new WeakReference<>(context);
        this.superViewReference = new WeakReference<>(this);
        this.masId = keyMAS;
        this.masConfigUrl = configUrl;
        this.hasWindow = true;
        this.isScheduled = true;
        if (!TextUtils.isEmpty(keyMAS)) {
            this.scheduler.schedule(new InitRunnable(this, keyMAS, this.masConfigUrl), 0, TimeUnit.SECONDS);
        }
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        if (visibility == 0) {
            this.hasWindow = true;
            if (!this.isScheduled) {
                this.isScheduled = true;
                if (this.extra != null) {
                    rotateThreadedNow();
                } else {
                    this.scheduler.schedule(new InitRunnable(this, this.masId, this.masConfigUrl), 0, TimeUnit.SECONDS);
                }
            }
        } else {
            this.hasWindow = false;
        }
    }

    public void setMasListener(MASListener masListener2) {
        this.masListener = masListener2;
    }

    public MASListener getMasListener() {
        return this.masListener;
    }

    public String getMasId() {
        return this.masId;
    }

    public void setMasId(String masId2) {
        this.masId = masId2;
        if (!TextUtils.isEmpty(masId2)) {
            this.scheduler.schedule(new InitRunnable(this, masId2, this.masConfigUrl), 0, TimeUnit.SECONDS);
        }
    }

    /* access modifiers changed from: package-private */
    public void rotateAd() {
        if (!this.hasWindow) {
            this.isScheduled = false;
            return;
        }
        this.nextRation = this.masManager.getRation();
        this.handler.post(new HandleAdRunnable(this));
    }

    /* access modifiers changed from: private */
    public void handleAd() {
        this.numAttempts++;
        if (this.numAttempts >= 3 && this.activeRation == null) {
            return;
        }
        if (this.nextRation == null) {
            rotateThreadedDelayed();
            return;
        }
        this.numAttempts = 0;
        try {
            MASAdapter.handle(this, this.nextRation);
        } catch (Throwable th) {
            rollover();
        }
    }

    public void rotateThreadedNow() {
        this.scheduler.schedule(new RotateAdRunnable(this), 0, TimeUnit.SECONDS);
    }

    public void rotateThreadedDelayed() {
        this.scheduler.schedule(new RotateAdRunnable(this), (long) this.extra.refreshInterval, TimeUnit.SECONDS);
    }

    public void pushSubView(ViewGroup subView) {
        RelativeLayout superView = this.superViewReference.get();
        if (superView != null) {
            superView.removeAllViews();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            superView.addView(subView, layoutParams);
            this.activeRation = this.nextRation;
        }
    }

    public void rollover() {
        this.nextRation = this.masManager.getRollover();
        this.handler.post(new HandleAdRunnable(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onInterceptTouchEvent(MotionEvent event) {
        Activity activity;
        switch (event.getAction()) {
            case 0:
                if (this.activeRation != null) {
                    if (this.masListener != null) {
                        this.masListener.onAdClick();
                    }
                    if (!(this.custom == null || this.custom.redirectUrl == null)) {
                        Intent i = new Intent(getContext(), Browser.class);
                        i.putExtra(Browser.URL_EXTRA, this.custom.redirectUrl);
                        i.putExtra(Browser.SHOW_BACK_EXTRA, true);
                        i.putExtra(Browser.SHOW_FORWARD_EXTRA, true);
                        i.putExtra(Browser.SHOW_REFRESH_EXTRA, true);
                        i.addFlags(DriveFile.MODE_READ_ONLY);
                        try {
                            if (!(this.activityReference == null || (activity = this.activityReference.get()) == null)) {
                                activity.startActivity(i);
                                break;
                            }
                        } catch (Exception e) {
                            break;
                        }
                    }
                }
                break;
        }
        return false;
    }

    private static class InitRunnable implements Runnable {
        private String configUrl;
        private String keyMAS;
        private WeakReference<MASLayout> masLayoutReference;

        public InitRunnable(MASLayout masLayout, String keyMAS2, String configUrl2) {
            this.masLayoutReference = new WeakReference<>(masLayout);
            this.keyMAS = keyMAS2;
            this.configUrl = configUrl2;
        }

        public void run() {
            Activity activity;
            MASLayout masLayout = this.masLayoutReference.get();
            if (masLayout != null && (activity = masLayout.activityReference.get()) != null) {
                if (masLayout.masManager == null) {
                    masLayout.masManager = new MASManager(new WeakReference(activity.getApplicationContext()), this.keyMAS, this.configUrl);
                }
                if (!masLayout.hasWindow) {
                    masLayout.isScheduled = false;
                    return;
                }
                masLayout.masManager.fetchConfig();
                masLayout.extra = masLayout.masManager.getExtra();
                if (masLayout.extra == null) {
                    masLayout.scheduler.schedule(this, 30, TimeUnit.SECONDS);
                } else {
                    masLayout.rotateAd();
                }
            }
        }
    }

    private static class HandleAdRunnable implements Runnable {
        private WeakReference<MASLayout> masLayoutReference;

        public HandleAdRunnable(MASLayout masLayout) {
            this.masLayoutReference = new WeakReference<>(masLayout);
        }

        public void run() {
            MASLayout masLayout = this.masLayoutReference.get();
            if (masLayout != null) {
                masLayout.handleAd();
            }
        }
    }

    public static class ViewAdRunnable implements Runnable {
        private WeakReference<MASLayout> masLayoutReference;
        private ViewGroup nextView;

        public ViewAdRunnable(MASLayout masLayout, ViewGroup nextView2) {
            this.masLayoutReference = new WeakReference<>(masLayout);
            this.nextView = nextView2;
        }

        public void run() {
            MASLayout masLayout = this.masLayoutReference.get();
            if (masLayout != null) {
                masLayout.pushSubView(this.nextView);
            }
        }
    }

    private static class RotateAdRunnable implements Runnable {
        private WeakReference<MASLayout> masLayoutReference;

        public RotateAdRunnable(MASLayout masLayout) {
            this.masLayoutReference = new WeakReference<>(masLayout);
        }

        public void run() {
            MASLayout masLayout = this.masLayoutReference.get();
            if (masLayout != null) {
                masLayout.rotateAd();
            }
        }
    }

    public static void showFullscreen(Activity activity) {
        showFullscreen(activity, 5000);
    }

    public static void showFullscreen(Activity activity, int interval) {
        showFullscreen(activity, null, interval);
    }

    public static void showFullscreen(Activity activity, String key) {
        showFullscreen(activity, key, 5000);
    }

    public static void showFullscreen(Activity activity, String key, int interval) {
        showFullscreen(activity, key, interval, null);
    }

    @SuppressLint({"NewApi"})
    public static void showFullscreen(Activity activity, String key, final int interval, final MASListener listener) {
        MASLayout ad;
        if (MASUtil.isOnline(activity)) {
            if (TextUtils.isEmpty(key)) {
                ad = new MASLayout(activity);
            } else {
                ad = new MASLayout(activity, key);
            }
            final FrameLayout layout = new FrameLayout(activity);
            layout.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            layout.addView(ad, new FrameLayout.LayoutParams(-1, -1));
            ImageButton closeButton = new ImageButton(activity);
            try {
                closeButton.setImageDrawable(Drawable.createFromStream(activity.getAssets().open("ic_close_ad.png"), "ic_close_ad"));
            } catch (IOException e) {
            }
            if (Build.VERSION.SDK_INT >= 16) {
                closeButton.setBackground(null);
            } else {
                closeButton.setBackgroundDrawable(null);
            }
            FrameLayout.LayoutParams params2 = new FrameLayout.LayoutParams(-2, -2);
            params2.gravity = 53;
            params2.topMargin = (int) TypedValue.applyDimension(1, 35.0f, activity.getResources().getDisplayMetrics());
            layout.addView(closeButton, params2);
            activity.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
            final FrameLayout contentView = (FrameLayout) activity.findViewById(16908290);
            contentView.addView(layout, new FrameLayout.LayoutParams(-1, -1));
            final Handler handler2 = new Handler() {
                public void handleMessage(Message msg) {
                    if (contentView != null && layout != null) {
                        contentView.removeView(layout);
                        if (listener != null) {
                            listener.onAdDismiss();
                        }
                    }
                }
            };
            ad.setMasListener(new MASListener() {
                public void onAdSuccess() {
                    handler2.sendEmptyMessageDelayed(0, (long) interval);
                    if (listener != null) {
                        listener.onAdSuccess();
                    }
                }

                public void onAdFailed() {
                    if (listener != null) {
                        listener.onAdFailed();
                    }
                }

                public void onAdDismiss() {
                    if (listener != null) {
                        listener.onAdDismiss();
                    }
                }

                public void onAdClick() {
                    if (listener != null) {
                        listener.onAdClick();
                    }
                }
            });
            closeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (contentView != null && layout != null) {
                        contentView.removeView(layout);
                        if (listener != null) {
                            listener.onAdDismiss();
                        }
                    }
                }
            });
        } else if (listener != null) {
            listener.onAdFailed();
        }
    }

    public static View showBounceAd(Activity activity, String url) {
        return showBounceAd(activity, url, 320, 240);
    }

    public static View showBounceAd(Activity activity, String url, int adWidth, int adHeight) {
        if (!MASUtil.isOnline(activity)) {
            return null;
        }
        final MASOrmmaView ad = new MASOrmmaView(activity);
        ad.loadUrl(url);
        final RelativeLayout layout = new RelativeLayout(activity);
        layout.addView(ad, new RelativeLayout.LayoutParams(adWidth, adHeight));
        final Activity activity2 = activity;
        final int i = adWidth;
        final int i2 = adHeight;
        final Handler handler2 = new Handler() {
            int bottom = 0;
            int left = 0;
            int right = 0;
            int top = 0;
            int xDirection = 1;
            int xSpeed = 8;
            int yDirection = 1;
            int ySpeed = 8;

            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    FrameLayout contentView = (FrameLayout) activity2.findViewById(16908290);
                    if (MASUtil.isOnline(activity2)) {
                        int width = contentView.getWidth();
                        int height = contentView.getHeight();
                        this.left = 0;
                        this.top = 0;
                        this.right = width - i;
                        this.bottom = height - i2;
                        contentView.addView(layout, new FrameLayout.LayoutParams(-1, -1));
                        sendEmptyMessageDelayed(2, 33);
                    }
                } else if (msg.what == 1) {
                    removeMessages(2);
                    ((ViewGroup) layout.getParent()).removeView(layout);
                } else if (msg.what == 2) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ad.getLayoutParams();
                    params.setMargins(this.left, this.top, this.right, this.bottom);
                    ad.setLayoutParams(params);
                    this.left += this.xDirection * this.xSpeed;
                    this.right -= this.xDirection * this.xSpeed;
                    this.top += this.yDirection * this.ySpeed;
                    this.bottom -= this.yDirection * this.ySpeed;
                    if (this.left <= 0) {
                        this.xDirection = 1;
                    }
                    if (this.right <= 0) {
                        this.xDirection = -1;
                    }
                    if (this.top <= 0) {
                        this.yDirection = 1;
                    }
                    if (this.bottom <= 0) {
                        this.yDirection = -1;
                    }
                    if (this.yDirection != -1 || this.top > (layout.getHeight() * 2) / 3 || Math.abs((this.left + (i / 2)) - (layout.getWidth() / 2)) >= 20) {
                        sendEmptyMessageDelayed(2, 33);
                    }
                }
            }
        };
        handler2.sendEmptyMessageDelayed(0, 20);
        ad.setListener(new OrmmaView.SimpleOrmmaViewListener() {
            public boolean onDefaultClose() {
                handler2.removeMessages(1);
                handler2.sendEmptyMessage(1);
                return true;
            }
        });
        ad.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (!handler2.hasMessages(2)) {
                    return false;
                }
                handler2.removeMessages(2);
                return true;
            }
        });
        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                handler2.removeMessages(2);
            }
        });
        return layout;
    }

    public static View showStickyAd(Activity activity, String url, int gravity) {
        return showStickyAd(activity, url, gravity, 0, 0);
    }

    public static View showStickyAd(Activity activity, String url, int gravity, int width, int height) {
        FrameLayout.LayoutParams params;
        final MASOrmmaView masOrmmaView = new MASOrmmaView(activity);
        masOrmmaView.setListener(new OrmmaView.SimpleOrmmaViewListener() {
            public boolean onDefaultClose() {
                ((ViewGroup) MASOrmmaView.this.getParent()).removeView(MASOrmmaView.this);
                return true;
            }
        });
        masOrmmaView.loadUrl(url);
        FrameLayout contentView = (FrameLayout) activity.findViewById(16908290);
        if (width <= 0 || height <= 0) {
            params = new FrameLayout.LayoutParams(-2, -2);
        } else {
            params = new FrameLayout.LayoutParams(width, height);
        }
        params.gravity = gravity;
        contentView.addView(masOrmmaView, params);
        return masOrmmaView;
    }

    public static View showFullscreenAd(Activity activity, String url) {
        final MASOrmmaView masOrmmaView = new MASOrmmaView(activity);
        masOrmmaView.setListener(new OrmmaView.SimpleOrmmaViewListener() {
            public boolean onDefaultClose() {
                ((ViewGroup) MASOrmmaView.this.getParent()).removeView(MASOrmmaView.this);
                MASOrmmaView.this.destroy();
                return true;
            }
        });
        masOrmmaView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 4 || event.getAction() != 0) {
                    return false;
                }
                MASOrmmaView.this.setOnKeyListener(null);
                ((ViewGroup) MASOrmmaView.this.getParent()).removeView(MASOrmmaView.this);
                MASOrmmaView.this.destroy();
                return true;
            }
        });
        masOrmmaView.loadUrl(url);
        ((FrameLayout) activity.findViewById(16908290)).addView(masOrmmaView, new FrameLayout.LayoutParams(-1, -1));
        return masOrmmaView;
    }
}
