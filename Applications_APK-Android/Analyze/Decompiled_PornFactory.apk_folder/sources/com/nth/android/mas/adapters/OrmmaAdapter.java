package com.nth.android.mas.adapters;

import android.app.Activity;
import android.text.TextUtils;
import com.nth.android.mas.MASLayout;
import com.nth.android.mas.MASListener;
import com.nth.android.mas.obj.Ration;
import com.nth.android.mas.util.MASUtil;
import java.util.concurrent.TimeUnit;
import org.ormma.controller.OrmmaController;

public class OrmmaAdapter extends MASAdapter {
    public OrmmaAdapter(MASLayout masLayout, Ration ration) {
        super(masLayout, ration);
    }

    public void handle() {
        MASLayout masLayout = (MASLayout) this.masLayoutReference.get();
        if (masLayout != null) {
            masLayout.scheduler.schedule(new FetchCustomRunnable(this), 0, TimeUnit.SECONDS);
        }
    }

    public void displayCustom() {
        Activity activity;
        MASLayout masLayout = (MASLayout) this.masLayoutReference.get();
        if (masLayout != null && (activity = masLayout.activityReference.get()) != null) {
            if (masLayout.custom == null || TextUtils.isEmpty(masLayout.custom.contentUrl)) {
                MASListener listener = masLayout.getMasListener();
                if (listener != null) {
                    listener.onAdFailed();
                    return;
                }
                return;
            }
            float density = activity.getResources().getDisplayMetrics().density;
            if ("top".equalsIgnoreCase(masLayout.custom.position)) {
                MASLayout.showStickyAd(activity, masLayout.custom.contentUrl, 49, MASUtil.convertToScreenPixels(masLayout.custom.width, (double) density), MASUtil.convertToScreenPixels(masLayout.custom.height, (double) density));
            } else if ("bottom".equalsIgnoreCase(masLayout.custom.position)) {
                MASLayout.showStickyAd(activity, masLayout.custom.contentUrl, 81, MASUtil.convertToScreenPixels(masLayout.custom.width, (double) density), MASUtil.convertToScreenPixels(masLayout.custom.height, (double) density));
            } else if (OrmmaController.FULL_SCREEN.equalsIgnoreCase(masLayout.custom.position)) {
                MASLayout.showFullscreenAd(activity, masLayout.custom.contentUrl);
            } else if ("bouncing".equalsIgnoreCase(masLayout.custom.position)) {
                MASLayout.showBounceAd(activity, masLayout.custom.contentUrl, MASUtil.convertToScreenPixels(masLayout.custom.width, (double) density), MASUtil.convertToScreenPixels(masLayout.custom.height, (double) density));
            }
            MASListener listener2 = masLayout.getMasListener();
            if (listener2 != null) {
                listener2.onAdSuccess();
            }
        }
    }

    private static class FetchCustomRunnable implements Runnable {
        private OrmmaAdapter customAdapter;

        public FetchCustomRunnable(OrmmaAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            MASLayout masLayout = (MASLayout) this.customAdapter.masLayoutReference.get();
            if (masLayout != null) {
                masLayout.custom = masLayout.masManager.getCustom(this.customAdapter.ration.url);
                if (masLayout.custom != null || masLayout.masManager.getRationsList().size() <= 1) {
                    masLayout.handler.post(new DisplayCustomRunnable(this.customAdapter));
                } else {
                    masLayout.rotateThreadedNow();
                }
            }
        }
    }

    private static class DisplayCustomRunnable implements Runnable {
        private OrmmaAdapter customAdapter;

        public DisplayCustomRunnable(OrmmaAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            this.customAdapter.displayCustom();
        }
    }
}
