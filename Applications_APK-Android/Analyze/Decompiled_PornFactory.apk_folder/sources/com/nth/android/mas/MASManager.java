package com.nth.android.mas;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.plus.PlusShare;
import com.nth.analytics.android.LocalyticsProvider;
import com.nth.android.mas.obj.Custom;
import com.nth.android.mas.obj.Extra;
import com.nth.android.mas.obj.Ration;
import com.nth.android.mas.util.MASUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.acra.ACRAConstants;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MASManager {
    private static final String PREFS_STRING_CONFIG = "config";
    private static final String PREFS_STRING_TIMESTAMP = "timestamp";
    private static final String PREFS_VERIFY_ID = "verify_id";
    private static long configExpireTimeout = 1800000;
    public String configUrl;
    private WeakReference<Context> contextReference;
    public String deviceIDHash;
    private Extra extra;
    public String keyMAS;
    public String localeString;
    public Location location;
    private List<Ration> rationsList;
    Iterator<Ration> rollovers;
    private double totalWeight;

    public MASManager(WeakReference<Context> contextReference2, String keyMAS2) {
        this(contextReference2, keyMAS2, null);
    }

    public MASManager(WeakReference<Context> contextReference2, String keyMAS2, String configUrl2) {
        this.totalWeight = 0.0d;
        this.contextReference = contextReference2;
        this.keyMAS = keyMAS2;
        this.configUrl = configUrl2;
        this.localeString = Locale.getDefault().toString();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            StringBuffer deviceIDString = new StringBuffer("android_id");
            deviceIDString.append("MAS");
            this.deviceIDHash = MASUtil.convertToHex(md.digest(deviceIDString.toString().getBytes()));
        } catch (NoSuchAlgorithmException e) {
            this.deviceIDHash = "00000000000000000000000000000000";
        }
    }

    public static void setConfigExpireTimeout(long configExpireTimeout2) {
        configExpireTimeout = configExpireTimeout2;
    }

    public Extra getExtra() {
        if (this.totalWeight <= 0.0d) {
            return null;
        }
        return this.extra;
    }

    public Ration getRation() {
        double r = new Random().nextDouble() * this.totalWeight;
        double s = 0.0d;
        Iterator<Ration> it = this.rationsList.iterator();
        Ration ration = null;
        while (it.hasNext()) {
            ration = it.next();
            s += ration.weight;
            if (s >= r) {
                break;
            }
        }
        return ration;
    }

    public Ration getRollover() {
        if (this.rollovers != null && this.rollovers.hasNext()) {
            return this.rollovers.next();
        }
        return null;
    }

    public void resetRollover() {
        this.rollovers = this.rationsList.iterator();
    }

    public Custom getCustom(String baseUrl) {
        if (!MASUtil.isOnline(this.contextReference.get())) {
            return null;
        }
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, ACRAConstants.DEFAULT_SOCKET_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, ACRAConstants.DEFAULT_SOCKET_TIMEOUT);
        HttpClient httpClient = new DefaultHttpClient(params);
        if (this.extra.locationOn == 1) {
            this.location = getLocation();
            if (this.location != null) {
                String format = String.format(MASUtil.locationString, Double.valueOf(this.location.getLatitude()), Double.valueOf(this.location.getLongitude()), Long.valueOf(this.location.getTime()));
            }
        } else {
            this.location = null;
        }
        int width = MASUtil.getWidth(this.contextReference.get());
        int height = MASUtil.getHeight(this.contextReference.get());
        String verifyId = getVerifyId();
        String url = String.valueOf(baseUrl) + ("?app_api_id=" + this.keyMAS + "&viewport_width=" + width + "&viewport_height=" + height);
        Log.i(getClass().getName(), url);
        if (TextUtils.isEmpty(verifyId)) {
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("User-Agent", MASUtil.getUserAgentString());
            httpGet.setHeader("Accept-Language", "en");
            try {
                HttpEntity entity = httpClient.execute(httpGet).getEntity();
                if (entity != null) {
                    String jsonString = convertStreamToString(entity.getContent());
                    Log.i(getClass().getName(), jsonString);
                    verifyId = parseVerifyJsonString(jsonString);
                    saveVerifyId(verifyId);
                }
            } catch (ClientProtocolException e) {
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(verifyId)) {
            String url2 = String.valueOf(url) + "&verify_id=" + verifyId;
            Log.i(getClass().getName(), url2);
            HttpGet httpGet2 = new HttpGet(url2);
            httpGet2.setHeader("User-Agent", MASUtil.getUserAgentString());
            httpGet2.setHeader("Accept-Language", "en");
            try {
                HttpEntity entity2 = httpClient.execute(httpGet2).getEntity();
                if (entity2 != null) {
                    String jsonString2 = convertStreamToString(entity2.getContent());
                    Log.i(getClass().getName(), jsonString2);
                    if (TextUtils.isEmpty(jsonString2) || jsonString2.startsWith("{}")) {
                        return null;
                    }
                    return parseCustomJsonString(jsonString2);
                }
            } catch (IOException | ClientProtocolException e3) {
            }
        }
        return null;
    }

    public void fetchConfig() {
        Context context = this.contextReference.get();
        if (context != null && MASUtil.isOnline(this.contextReference.get())) {
            SharedPreferences masPrefs = context.getSharedPreferences(this.keyMAS, 0);
            String jsonString = masPrefs.getString(PREFS_STRING_CONFIG, null);
            long timestamp = masPrefs.getLong(PREFS_STRING_TIMESTAMP, -1);
            if (jsonString == null || configExpireTimeout == -1 || System.currentTimeMillis() >= configExpireTimeout + timestamp) {
                if (TextUtils.isEmpty(this.configUrl)) {
                    jsonString = convertStreamToString(getClass().getResourceAsStream("/com/nth/android/mas/util/config"));
                } else {
                    try {
                        HttpEntity entity = new DefaultHttpClient().execute(new HttpGet(this.configUrl)).getEntity();
                        if (entity != null) {
                            jsonString = convertStreamToString(entity.getContent());
                            SharedPreferences.Editor editor = masPrefs.edit();
                            editor.putString(PREFS_STRING_CONFIG, jsonString);
                            editor.putLong(PREFS_STRING_TIMESTAMP, System.currentTimeMillis());
                            editor.commit();
                        }
                    } catch (IOException | ClientProtocolException e) {
                    }
                }
            }
            parseConfigurationString(jsonString);
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        is.close();
                        return sb.toString();
                    } catch (IOException e) {
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                try {
                    is.close();
                    return null;
                } catch (IOException e3) {
                    return null;
                }
            } catch (Throwable th) {
                try {
                    is.close();
                    throw th;
                } catch (IOException e4) {
                    return null;
                }
            }
        }
    }

    private void parseConfigurationString(String jsonString) {
        try {
            JSONObject json = new JSONObject(jsonString);
            parseExtraJson(json.getJSONObject("extra"));
            parseRationsJson(json.getJSONArray("rations"));
        } catch (JSONException e) {
            this.extra = new Extra();
        } catch (NullPointerException e2) {
            this.extra = new Extra();
        }
    }

    private void parseExtraJson(JSONObject json) {
        Extra extra2 = new Extra();
        try {
            extra2.locationOn = json.getInt("location_on");
            extra2.transition = json.getInt("transition");
            extra2.bgColor = json.getString("bcg_color");
            extra2.chooseAlgorithm = json.getInt("choose_algorithm");
            extra2.closeButtonOn = json.getInt("close_button_on");
            extra2.duration = json.getInt("duration");
            extra2.refreshInterval = json.getInt("refresh_interval");
            extra2.textColor = json.getString("text_color");
        } catch (JSONException e) {
        }
        this.extra = extra2;
    }

    private void parseRationsJson(JSONArray json) {
        List<Ration> rationsList2 = new ArrayList<>();
        this.totalWeight = 0.0d;
        int i = 0;
        while (i < json.length()) {
            try {
                JSONObject jsonRation = json.getJSONObject(i);
                if (jsonRation != null) {
                    Ration ration = new Ration();
                    ration.type = jsonRation.getInt(LocalyticsProvider.EventHistoryDbColumns.TYPE);
                    ration.name = jsonRation.getString("network_name");
                    ration.weight = (double) jsonRation.getInt("weight");
                    ration.priority = jsonRation.getInt("priority");
                    ration.url = jsonRation.optString(PlusShare.KEY_CALL_TO_ACTION_URL);
                    ration.key = jsonRation.optString("key", "");
                    ration.key2 = jsonRation.optString("key2", "");
                    this.totalWeight += ration.weight;
                    rationsList2.add(ration);
                }
                i++;
            } catch (JSONException e) {
            }
        }
        Collections.sort(rationsList2);
        this.rationsList = rationsList2;
        this.rollovers = this.rationsList.iterator();
    }

    private Custom parseCustomJsonString(String jsonString) {
        Custom custom = new Custom();
        try {
            JSONObject json = new JSONObject(jsonString);
            try {
                saveVerifyId(json.getString(PREFS_VERIFY_ID));
            } catch (Exception e) {
            }
            custom.verifyId = getVerifyId();
            if (json.has("position")) {
                custom.position = json.getString("position");
            }
            if (json.has("redirect_url")) {
                custom.redirectUrl = json.getString("redirect_url");
            }
            if (json.has("content_url")) {
                custom.contentUrl = json.getString("content_url");
            }
            if (json.has("ad_type")) {
                custom.adType = json.getInt("ad_type");
            }
            if (json.has("width")) {
                custom.width = json.getInt("width");
            }
            if (json.has("height")) {
                custom.height = json.getInt("height");
            }
            if (json.has("launch_type")) {
                custom.launchType = json.getInt("launch_type");
            }
            if (json.has("webview_animation_type")) {
                custom.webviewAnimationType = json.getInt("webview_animation_type");
            }
            if (!json.has(PlusShare.KEY_CALL_TO_ACTION_URL)) {
                return custom;
            }
            custom.url = json.getString(PlusShare.KEY_CALL_TO_ACTION_URL);
            return custom;
        } catch (JSONException e2) {
            return null;
        }
    }

    private String parseVerifyJsonString(String jsonString) {
        try {
            return new JSONObject(jsonString).getString(PREFS_VERIFY_ID);
        } catch (JSONException e) {
            return null;
        }
    }

    private Drawable fetchImage(String urlString) {
        if (!MASUtil.isOnline(this.contextReference.get())) {
            return null;
        }
        try {
            return Drawable.createFromStream((InputStream) new URL(urlString).getContent(), "src");
        } catch (Exception e) {
            return null;
        }
    }

    public List<Ration> getRationsList() {
        return this.rationsList;
    }

    public Location getLocation() {
        Context context;
        if (this.contextReference == null || (context = this.contextReference.get()) == null) {
            return null;
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            return ((LocationManager) context.getSystemService("location")).getLastKnownLocation("gps");
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            return ((LocationManager) context.getSystemService("location")).getLastKnownLocation("network");
        }
        return null;
    }

    public String getVerifyId() {
        return this.contextReference.get().getSharedPreferences(this.keyMAS, 0).getString(PREFS_VERIFY_ID, null);
    }

    private void saveVerifyId(String verifyId) {
        SharedPreferences.Editor editor = this.contextReference.get().getSharedPreferences(this.keyMAS, 0).edit();
        editor.putString(PREFS_VERIFY_ID, verifyId);
        editor.commit();
    }
}
