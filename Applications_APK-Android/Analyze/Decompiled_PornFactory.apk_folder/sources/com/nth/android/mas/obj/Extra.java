package com.nth.android.mas.obj;

public class Extra {
    public String bgColor = "";
    public int chooseAlgorithm = 0;
    public int closeButtonOn = 0;
    public int duration = 100;
    public int locationOn = 1;
    public int refreshInterval = 100;
    public String textColor = "";
    public int transition = 1;
}
