package com.nth.android.mas;

import android.content.Context;
import android.util.AttributeSet;
import org.ormma.view.OrmmaView;

public class MASOrmmaView extends OrmmaView {
    public MASOrmmaView(Context context, AttributeSet set) {
        super(context, set);
    }

    public MASOrmmaView(Context context, OrmmaView.OrmmaViewListener listener) {
        super(context, listener);
    }

    public MASOrmmaView(Context context, String mapAPIKey, OrmmaView.OrmmaViewListener listener) {
        super(context, mapAPIKey, listener);
    }

    public MASOrmmaView(Context context, String mapAPIKey) {
        super(context, mapAPIKey);
    }

    public MASOrmmaView(Context context) {
        super(context);
    }
}
