package com.nth.android.mas.adapters;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.nth.android.mas.MASLayout;
import com.nth.android.mas.MASListener;
import com.nth.android.mas.obj.Ration;
import com.nth.android.mas.util.Base64;
import com.nth.android.mas.util.MASUtil;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class CustomAdapter extends MASAdapter {
    public CustomAdapter(MASLayout masLayout, Ration ration) {
        super(masLayout, ration);
    }

    public void handle() {
        MASLayout masLayout = (MASLayout) this.masLayoutReference.get();
        if (masLayout != null) {
            masLayout.scheduler.schedule(new FetchCustomRunnable(this), 0, TimeUnit.SECONDS);
        }
    }

    public void displayCustom() {
        MASLayout masLayout = (MASLayout) this.masLayoutReference.get();
        if (masLayout != null) {
            Activity activity = masLayout.activityReference.get();
            if (activity == null || masLayout.custom == null || TextUtils.isEmpty(masLayout.custom.url)) {
                MASListener listener = masLayout.getMasListener();
                if (listener != null) {
                    listener.onAdFailed();
                    return;
                }
                return;
            }
            double density = MASUtil.getDensity(activity);
            double width = (double) MASUtil.convertToScreenPixels(masLayout.custom.width, density);
            double height = (double) MASUtil.convertToScreenPixels(masLayout.custom.height, density);
            WebView bannerWebView = new WebView(activity);
            bannerWebView.setVerticalScrollBarEnabled(false);
            bannerWebView.setHorizontalScrollBarEnabled(false);
            bannerWebView.getSettings().setUserAgentString("MAS SDK");
            bannerWebView.setBackgroundColor(0);
            new LoadAd(bannerWebView).execute(masLayout.custom.url);
            RelativeLayout bannerWebViewContainter = new RelativeLayout(activity);
            RelativeLayout.LayoutParams bannerWebViewParams = new RelativeLayout.LayoutParams((int) width, (int) height);
            bannerWebViewParams.addRule(13);
            bannerWebViewContainter.addView(bannerWebView, bannerWebViewParams);
            masLayout.pushSubView(bannerWebViewContainter);
            masLayout.masManager.resetRollover();
            masLayout.rotateThreadedDelayed();
        }
    }

    private static class FetchCustomRunnable implements Runnable {
        private CustomAdapter customAdapter;

        public FetchCustomRunnable(CustomAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            MASLayout masLayout = (MASLayout) this.customAdapter.masLayoutReference.get();
            if (masLayout != null) {
                masLayout.custom = masLayout.masManager.getCustom(this.customAdapter.ration.url);
                if (masLayout.custom != null || masLayout.masManager.getRationsList().size() <= 1) {
                    masLayout.handler.post(new DisplayCustomRunnable(this.customAdapter));
                } else {
                    masLayout.rotateThreadedNow();
                }
            }
        }
    }

    private static class DisplayCustomRunnable implements Runnable {
        private CustomAdapter customAdapter;

        public DisplayCustomRunnable(CustomAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            this.customAdapter.displayCustom();
        }
    }

    private class LoadAd extends AsyncTask<String, Void, Void> {
        String contentType = "";
        String htmlContent = "";
        WebView view;

        public LoadAd(WebView bannerWebView) {
            this.view = bannerWebView;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... params) {
            String urlString = params[0];
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(urlString);
            httpGet.setHeader("User-Agent", MASUtil.getUserAgentString());
            httpGet.setHeader("Accept-Language", "en");
            try {
                HttpResponse response = httpClient.execute(httpGet);
                if (response.getStatusLine().getStatusCode() != 200) {
                    return null;
                }
                this.contentType = response.getEntity().getContentType().getValue();
                HttpEntity entity = response.getEntity();
                if (entity == null) {
                    return null;
                }
                InputStream inputStream = entity.getContent();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                while (true) {
                    int read = inputStream.read(buffer);
                    if (read == -1) {
                        break;
                    }
                    baos.write(buffer, 0, read);
                }
                if (!this.contentType.contains("image")) {
                    return null;
                }
                this.htmlContent = Base64.encodeToString(baos.toByteArray(), 0);
                return null;
            } catch (Exception e) {
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            MASLayout masLayout;
            MASListener listener;
            super.onPostExecute((Object) result);
            if (this.contentType.contains("image")) {
                this.view.loadData("<html><head><style>* {margin:0;padding:0;}</style></head><body><img " + (Build.VERSION.SDK_INT >= 14 ? "width=\"100%\" " : "") + "src=\"data:" + this.contentType + ";base64," + this.htmlContent + "\" />" + "</body>" + "</html>", "text/html", null);
            } else {
                this.view.loadData(this.htmlContent, this.contentType, null);
            }
            if (CustomAdapter.this.masLayoutReference != null && (masLayout = (MASLayout) CustomAdapter.this.masLayoutReference.get()) != null && (listener = masLayout.getMasListener()) != null) {
                listener.onAdSuccess();
            }
        }
    }
}
