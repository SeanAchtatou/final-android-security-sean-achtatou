package com.nth.android.mas.adapters;

import android.app.Activity;
import android.widget.RelativeLayout;
import com.nth.android.mas.MASLayout;
import com.nth.android.mas.MASListener;
import com.nth.android.mas.obj.OpenXParams;
import com.nth.android.mas.obj.Ration;
import com.nth.android.mas.util.MASUtil;
import com.openx.ad.mobile.sdk.controllers.OXMAdBaseController;
import com.openx.ad.mobile.sdk.controllers.OXMAdController;
import com.openx.ad.mobile.sdk.errors.OXMAndroidSDKVersionNotSupported;
import com.openx.ad.mobile.sdk.errors.OXMError;
import com.openx.ad.mobile.sdk.interfaces.OXMAdControllerEventsListener;
import com.openx.ad.mobile.sdk.managers.OXMManagersResolver;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

public class OpenXAdapter extends MASAdapter implements OXMAdControllerEventsListener {
    private Activity activity;
    private RelativeLayout bannerWebViewContainter;
    private double height;
    private double width;

    public OpenXAdapter(MASLayout masLayout, Ration ration) {
        super(masLayout, ration);
    }

    public void handle() {
        MASLayout masLayout = (MASLayout) this.masLayoutReference.get();
        if (masLayout != null) {
            this.bannerWebViewContainter = new RelativeLayout(masLayout.activityReference.get());
            masLayout.pushSubView(this.bannerWebViewContainter);
            masLayout.scheduler.schedule(new FetchCustomRunnable(this), 0, TimeUnit.SECONDS);
        }
    }

    private static class FetchCustomRunnable implements Runnable {
        private OpenXAdapter customAdapter;

        public FetchCustomRunnable(OpenXAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            MASLayout masLayout = (MASLayout) this.customAdapter.masLayoutReference.get();
            if (masLayout != null) {
                masLayout.handler.post(new DisplayCustomRunnable(this.customAdapter));
            }
        }
    }

    private static class DisplayCustomRunnable implements Runnable {
        private OpenXAdapter customAdapter;

        public DisplayCustomRunnable(OpenXAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            try {
                this.customAdapter.displayCustom();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void displayCustom() throws JSONException {
        MASLayout masLayout = (MASLayout) this.masLayoutReference.get();
        if (masLayout != null) {
            this.activity = masLayout.activityReference.get();
            if (this.activity != null) {
                OpenXParams params = OpenXParams.parseJSON(new JSONObject(this.ration.key));
                double density = MASUtil.getDensity(this.activity);
                this.width = MASUtil.convertToScreenPixels(params.getWidth(), density);
                this.height = MASUtil.convertToScreenPixels(params.getHeight(), density);
                OXMManagersResolver.getInstance().prepare(this.activity);
                try {
                    OXMAdController adController = new OXMAdController(this.activity, params.getServer());
                    if (params.hasCallParams()) {
                        adController.setAdCallParams(params.getCallParams());
                    }
                    adController.setAdControllerEventsListener(this);
                    adController.initForAdUnitGroupIds(params.getZoneId(), params.getZoneId());
                    adController.startLoading();
                } catch (OXMAndroidSDKVersionNotSupported e) {
                    e.printStackTrace();
                }
                masLayout.masManager.resetRollover();
                masLayout.rotateThreadedDelayed();
            }
        }
    }

    public void adControllerActionDidFinish(OXMAdBaseController controller) {
    }

    public boolean adControllerActionShouldBegin(OXMAdBaseController controller, boolean willLeaveApp) {
        return true;
    }

    public void adControllerActionUnableToBegin(OXMAdBaseController controller) {
    }

    public void adControllerDidFailToReceiveAdWithError(OXMAdBaseController controller, Throwable exception) {
    }

    public void adControllerDidFailWithNonCriticalError(OXMAdBaseController controller, OXMError arg1) {
    }

    public void adControllerDidLoadAd(OXMAdBaseController controller) {
    }

    public void adControllerWillLoadAd(OXMAdBaseController controller) {
        RelativeLayout.LayoutParams bannerWebViewParams = new RelativeLayout.LayoutParams((int) this.width, (int) this.height);
        bannerWebViewParams.addRule(13);
        ((OXMAdController) controller).getAdBannerView().presentAdInViewGroup(this.bannerWebViewContainter, bannerWebViewParams);
        MASListener listener = ((MASLayout) this.masLayoutReference.get()).getMasListener();
        if (listener != null) {
            listener.onAdSuccess();
        }
    }
}
