package com.nth.android.mas.adapters;

import com.nth.android.mas.MASLayout;
import com.nth.android.mas.obj.Ration;

public class AdMobAdapter extends MASAdapter {
    public AdMobAdapter(MASLayout masLayout, Ration ration) {
        super(masLayout, ration);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000b, code lost:
        r0 = r5.activityReference.get();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handle() {
        /*
            r8 = this;
            java.lang.ref.WeakReference r6 = r8.masLayoutReference
            java.lang.Object r5 = r6.get()
            com.nth.android.mas.MASLayout r5 = (com.nth.android.mas.MASLayout) r5
            if (r5 != 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            java.lang.ref.WeakReference<android.app.Activity> r6 = r5.activityReference
            java.lang.Object r0 = r6.get()
            android.app.Activity r0 = (android.app.Activity) r0
            if (r0 == 0) goto L_0x000a
            com.nth.android.mas.obj.Ration r6 = r8.ration
            java.lang.String r6 = r6.key2
            boolean r6 = android.text.TextUtils.isEmpty(r6)
            if (r6 != 0) goto L_0x004c
            com.nth.android.mas.obj.Ration r6 = r8.ration
            java.lang.String r6 = r6.key2
            java.lang.String r7 = "interstitial"
            boolean r6 = r6.equalsIgnoreCase(r7)
            if (r6 == 0) goto L_0x004c
            com.google.android.gms.ads.InterstitialAd r3 = new com.google.android.gms.ads.InterstitialAd
            r3.<init>(r0)
            com.nth.android.mas.obj.Ration r6 = r8.ration
            java.lang.String r6 = r6.key
            r3.setAdUnitId(r6)
            com.google.android.gms.ads.AdRequest$Builder r6 = new com.google.android.gms.ads.AdRequest$Builder
            r6.<init>()
            com.google.android.gms.ads.AdRequest r1 = r6.build()
            r3.loadAd(r1)
            com.nth.android.mas.adapters.AdMobAdapter$1 r6 = new com.nth.android.mas.adapters.AdMobAdapter$1
            r6.<init>(r0, r3)
            r3.setAdListener(r6)
            goto L_0x000a
        L_0x004c:
            com.google.android.gms.ads.AdView r2 = new com.google.android.gms.ads.AdView
            r2.<init>(r0)
            com.google.android.gms.ads.AdSize r6 = com.google.android.gms.ads.AdSize.BANNER
            r2.setAdSize(r6)
            com.nth.android.mas.obj.Ration r6 = r8.ration
            java.lang.String r6 = r6.key
            r2.setAdUnitId(r6)
            com.google.android.gms.ads.AdRequest$Builder r6 = new com.google.android.gms.ads.AdRequest$Builder
            r6.<init>()
            com.google.android.gms.ads.AdRequest r6 = r6.build()
            r2.loadAd(r6)
            android.widget.LinearLayout r4 = new android.widget.LinearLayout
            android.content.Context r6 = r5.getContext()
            r4.<init>(r6)
            r4.addView(r2)
            r5.pushSubView(r4)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.nth.android.mas.adapters.AdMobAdapter.handle():void");
    }
}
