package com.nth.android.mas.obj;

import com.openx.ad.mobile.sdk.OXMAdCallParams;
import java.util.Hashtable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OpenXParams {
    private static final String APP_STORE_URL = "app_store_market_url";
    private static final String CALL_PARAMS = "call_params";
    private static final String CARRIER = "carrier";
    private static final String CITY = "city";
    private static final String CONNECTION_TYPE = "connection_type";
    private static final String COUNTRY = "country";
    private static final String CUSTOM_PARAMS = "custom";
    private static final String DEVICE_ID = "device_id";
    private static final String DMA = "dma";
    private static final String IS_HTTPS = "is_https";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String STATE = "state";
    private static final String USER_AGE = "user_age";
    private static final String USER_ETHNICITY = "user_ethnicity";
    private static final String USER_GENDER = "user_gender";
    private static final String USER_ID = "user_id";
    private static final String USER_INCOME = "user_income";
    private static final String USER_MARTIAL_STATUS = "user_marital_status";
    private static final String ZIP_CODE = "zip_code";
    private OXMAdCallParams callParams = null;
    private double height;
    private String server;
    private double width;
    private long zoneId;

    private OpenXParams() {
    }

    public double getWidth() {
        return this.width;
    }

    public double getHeight() {
        return this.height;
    }

    public String getServer() {
        return this.server;
    }

    public long getZoneId() {
        return this.zoneId;
    }

    public OXMAdCallParams getCallParams() {
        return this.callParams;
    }

    public boolean hasCallParams() {
        return this.callParams != null;
    }

    public static OpenXParams parseJSON(JSONObject jsonObject) throws JSONException {
        OpenXParams res = new OpenXParams();
        res.width = jsonObject.getDouble("width");
        res.height = jsonObject.getDouble("height");
        res.server = jsonObject.getString("server");
        res.zoneId = jsonObject.getLong("zone_id");
        if (jsonObject.has(CALL_PARAMS)) {
            res.callParams = parseCallParams(jsonObject.getJSONObject(CALL_PARAMS));
        }
        return res;
    }

    private static OXMAdCallParams parseCallParams(JSONObject jsonObject) throws JSONException {
        OXMAdCallParams res = new OXMAdCallParams();
        if (jsonObject.has(APP_STORE_URL)) {
            res.setAppStoreMarketUrl(jsonObject.getString(APP_STORE_URL));
        }
        if (jsonObject.has(CITY)) {
            res.setCity(jsonObject.getString(CITY));
        }
        if (jsonObject.has(CONNECTION_TYPE)) {
            res.setConnectionType(((OXMAdCallParams.OXMConnectionType[]) OXMAdCallParams.OXMConnectionType.class.getEnumConstants())[jsonObject.getInt(CONNECTION_TYPE)]);
        }
        if (jsonObject.has("latitude") && jsonObject.has("longitude")) {
            res.setCoordinates(jsonObject.getDouble("latitude"), jsonObject.getDouble("longitude"));
        }
        if (jsonObject.has(COUNTRY)) {
            res.setCountry(jsonObject.getString(COUNTRY));
        }
        if (jsonObject.has(CUSTOM_PARAMS)) {
            res.setCustomParameters(parseCustomParams(jsonObject.getJSONObject(CUSTOM_PARAMS)));
        }
        if (jsonObject.has(CARRIER)) {
            res.setDeviceCarrier(jsonObject.getString(CARRIER));
        }
        if (jsonObject.has(DEVICE_ID)) {
            res.setDeviceId(jsonObject.getString(DEVICE_ID));
        }
        if (jsonObject.has("dma")) {
            res.setDma(jsonObject.getString("dma"));
        }
        if (jsonObject.has(IS_HTTPS)) {
            res.setSSL(jsonObject.getBoolean(IS_HTTPS));
        }
        if (jsonObject.has(STATE)) {
            res.setState(jsonObject.getString(STATE));
        }
        if (jsonObject.has(USER_AGE)) {
            res.setUserAge(jsonObject.getInt(USER_AGE));
        }
        if (jsonObject.has(USER_INCOME)) {
            res.setUserAnnualIncomeInUs(jsonObject.getInt(USER_INCOME));
        }
        if (jsonObject.has(USER_ETHNICITY)) {
            res.setUserEthnicity(((OXMAdCallParams.OXMEthnicity[]) OXMAdCallParams.OXMEthnicity.class.getEnumConstants())[jsonObject.getInt(USER_ETHNICITY)]);
        }
        if (jsonObject.has(USER_GENDER)) {
            res.setUserGender(((OXMAdCallParams.OXMGender[]) OXMAdCallParams.OXMGender.class.getEnumConstants())[jsonObject.getInt(USER_GENDER)]);
        }
        if (jsonObject.has(USER_ID)) {
            res.setUserId(jsonObject.getString(USER_ID));
        }
        if (jsonObject.has(USER_MARTIAL_STATUS)) {
            res.setUserMaritalStatus(((OXMAdCallParams.OXMMaritalStatus[]) OXMAdCallParams.OXMMaritalStatus.class.getEnumConstants())[jsonObject.getInt(USER_MARTIAL_STATUS)]);
        }
        if (jsonObject.has(ZIP_CODE)) {
            res.setZipCode(jsonObject.getString(ZIP_CODE));
        }
        return res;
    }

    private static Hashtable<String, String> parseCustomParams(JSONObject jsonObject) throws JSONException {
        Hashtable<String, String> res = new Hashtable<>();
        JSONArray keys = jsonObject.names();
        for (int i = 0; i < keys.length(); i++) {
            res.put(keys.getString(i), jsonObject.getString(keys.getString(i)));
        }
        return res;
    }
}
