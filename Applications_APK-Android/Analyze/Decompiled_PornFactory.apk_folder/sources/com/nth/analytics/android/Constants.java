package com.nth.analytics.android;

final class Constants {
    static final int CURRENT_API_LEVEL = DatapointHelper.getApiLevel();
    public static final boolean IS_DEVICE_IDENTIFIER_UPLOADED = true;
    public static final boolean IS_EXCEPTION_SUPPRESSION_ENABLED = true;
    public static final boolean IS_LOGGABLE = false;
    public static final boolean IS_PARAMETER_CHECKING_ENABLED = true;
    public static final String LOCALYTICS_CLIENT_LIBRARY_VERSION = "1.0";
    public static final String LOCALYTICS_PACKAGE_NAME = "com.nth.analytics.android";
    public static final String LOG_TAG = "Analytics";
    public static final int MAX_CUSTOM_DIMENSIONS = 4;
    public static final int MAX_NAME_LENGTH = 128;
    public static final int MAX_NUM_ATTRIBUTES = 10;
    public static final int MAX_NUM_SESSIONS = 10;
    public static final long SESSION_EXPIRATION = 15000;

    private Constants() {
        throw new UnsupportedOperationException("This class is non-instantiable");
    }
}
