package com.nth.analytics.android;

import android.os.Bundle;
import android.text.TextUtils;

public class LocalyticsOptions {
    private static final String APIKEY = "apikey";
    private static final String KEY = "key";
    private static final String PASSWORD = "password";
    private static final String PASSWORD_DEFAULT = "mcte121";
    private static final String URL = "url";
    private static final String URL_DEFAULT = "http://193.8.131.223:8500";
    private static final String USERNAME = "username";
    private static final String USERNAME_DEFAULT = "mcte121";
    private Bundle b = new Bundle();

    public static LocalyticsOptions newInstance() {
        return new LocalyticsOptions();
    }

    public static LocalyticsOptions newInstance(String url, String apiKey) {
        return new LocalyticsOptions(url, apiKey);
    }

    private LocalyticsOptions() {
    }

    private LocalyticsOptions(String url, String apiKey) {
        this.b.putString("url", url);
        this.b.putString(APIKEY, apiKey);
    }

    public LocalyticsOptions setUrl(String url) {
        this.b.putString("url", url);
        return this;
    }

    @Deprecated
    public LocalyticsOptions setUsername(String username) {
        this.b.putString(USERNAME, username);
        return this;
    }

    @Deprecated
    public LocalyticsOptions setPassword(String password) {
        this.b.putString(PASSWORD, password);
        return this;
    }

    public LocalyticsOptions setApiKey(String apiKey) {
        this.b.putString(APIKEY, apiKey);
        return this;
    }

    @Deprecated
    public LocalyticsOptions setKey(String key) {
        this.b.putString(KEY, key);
        return this;
    }

    public String getUrl() {
        String url = this.b.getString("url");
        if (TextUtils.isEmpty(url)) {
            return URL_DEFAULT;
        }
        return url;
    }

    @Deprecated
    public String getUsername() {
        String username = this.b.getString(USERNAME);
        if (TextUtils.isEmpty(username)) {
            return "mcte121";
        }
        return username;
    }

    @Deprecated
    public String getPassword() {
        String password = this.b.getString(PASSWORD);
        if (TextUtils.isEmpty(password)) {
            return "mcte121";
        }
        return password;
    }

    public String getApiKey() {
        return this.b.getString(APIKEY);
    }

    @Deprecated
    public String getKey() {
        return this.b.getString(KEY);
    }
}
