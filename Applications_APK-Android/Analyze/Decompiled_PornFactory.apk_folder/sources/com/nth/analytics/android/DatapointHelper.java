package com.nth.analytics.android;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.v4.os.EnvironmentCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

final class DatapointHelper {
    private static final Object[] HARDWARE_TELEPHONY = {"android.hardware.telephony"};
    private static final Object[] HARDWARE_WIFI = {"android.hardware.wifi"};
    private static final String INVALID_ANDROID_ID = "9774d56d682e549c";
    private static final String LEGACY_DEVICE_ID_FILE = "/analytics/device_id";
    private static final Class<?>[] STRING_CLASS_ARRAY = {String.class};

    private DatapointHelper() {
        throw new UnsupportedOperationException("This class is non-instantiable");
    }

    static int getApiLevel() {
        try {
            return Integer.parseInt((String) Build.VERSION.class.getField("SDK").get(null));
        } catch (Exception e) {
            Log.w(Constants.LOG_TAG, "Caught exception", e);
            try {
                return Build.VERSION.class.getField("SDK_INT").getInt(null);
            } catch (Exception e2) {
                return 3;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0052 A[SYNTHETIC, Splitter:B:14:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0072 A[SYNTHETIC, Splitter:B:23:0x0072] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getAndroidIdHashOrNull(android.content.Context r12) {
        /*
            java.io.File r3 = new java.io.File
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.io.File r8 = r12.getFilesDir()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "/analytics/device_id"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r3.<init>(r7)
            boolean r7 = r3.exists()
            if (r7 == 0) goto L_0x0055
            long r8 = r3.length()
            r10 = 0
            int r7 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r7 <= 0) goto L_0x0055
            r5 = 0
            r7 = 100
            char[] r1 = new char[r7]     // Catch:{ FileNotFoundException -> 0x004f, all -> 0x006f }
            java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x004f, all -> 0x006f }
            java.io.FileReader r7 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x004f, all -> 0x006f }
            r7.<init>(r3)     // Catch:{ FileNotFoundException -> 0x004f, all -> 0x006f }
            r8 = 128(0x80, float:1.794E-43)
            r6.<init>(r7, r8)     // Catch:{ FileNotFoundException -> 0x004f, all -> 0x006f }
            int r4 = r6.read(r1)     // Catch:{ FileNotFoundException -> 0x0083, all -> 0x0080 }
            r7 = 0
            java.lang.String r2 = java.lang.String.copyValueOf(r1, r7, r4)     // Catch:{ FileNotFoundException -> 0x0083, all -> 0x0080 }
            r6.close()     // Catch:{ FileNotFoundException -> 0x0083, all -> 0x0080 }
            if (r6 == 0) goto L_0x004e
            r6.close()     // Catch:{ IOException -> 0x007d }
        L_0x004e:
            return r2
        L_0x004f:
            r7 = move-exception
        L_0x0050:
            if (r5 == 0) goto L_0x0055
            r5.close()     // Catch:{ IOException -> 0x0076 }
        L_0x0055:
            android.content.ContentResolver r7 = r12.getContentResolver()
            java.lang.String r8 = "android_id"
            java.lang.String r0 = android.provider.Settings.Secure.getString(r7, r8)
            if (r0 == 0) goto L_0x006d
            java.lang.String r7 = r0.toLowerCase()
            java.lang.String r8 = "9774d56d682e549c"
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x0078
        L_0x006d:
            r2 = 0
            goto L_0x004e
        L_0x006f:
            r7 = move-exception
        L_0x0070:
            if (r5 == 0) goto L_0x0075
            r5.close()     // Catch:{ IOException -> 0x0076 }
        L_0x0075:
            throw r7     // Catch:{ IOException -> 0x0076 }
        L_0x0076:
            r7 = move-exception
            goto L_0x0055
        L_0x0078:
            java.lang.String r2 = getSha256_buggy(r0)
            goto L_0x004e
        L_0x007d:
            r7 = move-exception
            r5 = r6
            goto L_0x0055
        L_0x0080:
            r7 = move-exception
            r5 = r6
            goto L_0x0070
        L_0x0083:
            r7 = move-exception
            r5 = r6
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.nth.analytics.android.DatapointHelper.getAndroidIdHashOrNull(android.content.Context):java.lang.String");
    }

    public static String getSerialNumberHashOrNull() {
        String serialNumber = null;
        if (Constants.CURRENT_API_LEVEL >= 9) {
            try {
                serialNumber = (String) Build.class.getField("SERIAL").get(null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        if (serialNumber == null) {
            return null;
        }
        return getSha256_buggy(serialNumber);
    }

    public static String getTelephonyDeviceIdOrNull(Context context) {
        if ((Constants.CURRENT_API_LEVEL < 7 || ((Boolean) ReflectionUtils.tryInvokeInstance(context.getPackageManager(), "hasSystemFeature", STRING_CLASS_ARRAY, HARDWARE_TELEPHONY)).booleanValue()) && context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) == 0) {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        return null;
    }

    public static String getTelephonyDeviceIdHashOrNull(Context context) {
        String id = getTelephonyDeviceIdOrNull(context);
        if (id == null) {
            return null;
        }
        return getSha256_buggy(id);
    }

    public static String getWifiMacHashOrNull(Context context) {
        WifiInfo info;
        if (Constants.CURRENT_API_LEVEL >= 8 && !((Boolean) ReflectionUtils.tryInvokeInstance(context.getPackageManager(), "hasSystemFeature", STRING_CLASS_ARRAY, HARDWARE_WIFI)).booleanValue()) {
            return null;
        }
        String id = null;
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_WIFI_STATE", context.getPackageName()) == 0 && (info = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo()) != null) {
            id = info.getMacAddress();
        }
        if (id != null) {
            return getSha256_buggy(id);
        }
        return null;
    }

    public static String getNetworkType(Context context, TelephonyManager telephonyManager) {
        NetworkInfo wifiInfo;
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_WIFI_STATE", context.getPackageName()) == 0 && (wifiInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1)) != null && wifiInfo.isConnectedOrConnecting()) {
                return "wifi";
            }
        } catch (SecurityException e) {
        }
        return "android_network_type_" + telephonyManager.getNetworkType();
    }

    public static String getManufacturer() {
        if (Constants.CURRENT_API_LEVEL <= 3) {
            return EnvironmentCompat.MEDIA_UNKNOWN;
        }
        try {
            return (String) Build.class.getField("MANUFACTURER").get(null);
        } catch (Exception e) {
            return EnvironmentCompat.MEDIA_UNKNOWN;
        }
    }

    public static String getAppVersion(Context context) {
        try {
            String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (versionName == null) {
                return EnvironmentCompat.MEDIA_UNKNOWN;
            }
            return versionName;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    static String getSha256_buggy(String string) {
        if (string == null) {
            throw new IllegalArgumentException("string cannot be null");
        }
        try {
            return new BigInteger(1, MessageDigest.getInstance("SHA-256").digest(string.getBytes("UTF-8"))).toString(16);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException(e2);
        }
    }
}
