package com.nth.analytics.android;

import java.lang.reflect.InvocationTargetException;

public final class ReflectionUtils {
    private ReflectionUtils() {
        throw new UnsupportedOperationException("This class is non-instantiable");
    }

    public static <T> T tryInvokeStatic(Class<?> classObject, String methodName, Class<?>[] types, Object[] args) {
        return helper(null, classObject, null, methodName, types, args);
    }

    public static <T> T tryInvokeStatic(String className, String methodName, Class<?>[] types, Object[] args) {
        return helper(className, null, null, methodName, types, args);
    }

    public static <T> T tryInvokeInstance(Object target, String methodName, Class<?>[] types, Object[] args) {
        return helper(target, null, null, methodName, types, args);
    }

    private static <T> T helper(Object target, Class<?> classObject, String className, String methodName, Class<?>[] argTypes, Object[] args) {
        Class<?> cls;
        if (classObject != null) {
            cls = classObject;
        } else if (target != null) {
            cls = target.getClass();
        } else {
            cls = Class.forName(className);
        }
        try {
            return cls.getMethod(methodName, argTypes).invoke(target, args);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException(e2);
        } catch (InvocationTargetException e3) {
            throw new RuntimeException(e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException(e4);
        }
    }
}
