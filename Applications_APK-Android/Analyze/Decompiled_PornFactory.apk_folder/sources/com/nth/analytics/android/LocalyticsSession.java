package com.nth.analytics.android;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.CursorJoiner;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import com.nth.analytics.android.JsonObjects;
import com.nth.analytics.android.LocalyticsProvider;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class LocalyticsSession {
    static final String CLOSE_EVENT = String.format(EVENT_FORMAT, Constants.LOCALYTICS_PACKAGE_NAME, "close");
    static final String EVENT_FORMAT = "%s:%s";
    static final String FLOW_EVENT = String.format(EVENT_FORMAT, Constants.LOCALYTICS_PACKAGE_NAME, "flow");
    static final String OPEN_EVENT = String.format(EVENT_FORMAT, Constants.LOCALYTICS_PACKAGE_NAME, "open");
    static final String OPT_IN_EVENT = String.format(EVENT_FORMAT, Constants.LOCALYTICS_PACKAGE_NAME, "opt_in");
    static final String OPT_OUT_EVENT = String.format(EVENT_FORMAT, Constants.LOCALYTICS_PACKAGE_NAME, LocalyticsProvider.ApiKeysDbColumns.OPT_OUT);
    private static final int TWO_MINUTES = 120000;
    /* access modifiers changed from: private */
    public static String mAKey;
    /* access modifiers changed from: private */
    public static String mAuthPassword;
    /* access modifiers changed from: private */
    public static String mAuthUsername;
    /* access modifiers changed from: private */
    public static String mUrl;
    protected static final Map<String, Boolean> sIsUploadingMap = new HashMap();
    private static final Map<String, SessionHandler> sLocalyticsSessionHandlerMap = new HashMap();
    private static final Object[] sLocalyticsSessionIntrinsicLock = new Object[0];
    private static final HandlerThread sSessionHandlerThread = getHandlerThread(SessionHandler.class.getSimpleName());
    protected static final HandlerThread sUploadHandlerThread = getHandlerThread(UploadHandler.class.getSimpleName());
    private final Context mContext;
    private final Handler mSessionHandler;

    private static HandlerThread getHandlerThread(String name) {
        HandlerThread thread = new HandlerThread(name, 10);
        thread.start();
        return thread;
    }

    public LocalyticsSession(Context context, LocalyticsOptions options) {
        if (context == null) {
            throw new IllegalArgumentException("context cannot be null");
        } else if (options == null) {
            throw new IllegalArgumentException("options cannot be null or empty");
        } else if (!Constants.LOCALYTICS_PACKAGE_NAME.equals(context.getPackageName()) || context.getClass().getName().equals("android.test.IsolatedContext") || context.getClass().getName().equals("android.test.RenamingDelegatingContext")) {
            if (!context.getClass().getName().equals("android.test.RenamingDelegatingContext") && Constants.CURRENT_API_LEVEL >= 8) {
                context = context.getApplicationContext();
            }
            this.mContext = context;
            String key = options.getKey();
            mAuthUsername = options.getUsername();
            mAuthPassword = options.getPassword();
            mAKey = options.getApiKey();
            mUrl = options.getUrl();
            key = TextUtils.isEmpty(key) ? mAKey : key;
            synchronized (sLocalyticsSessionIntrinsicLock) {
                SessionHandler handler = sLocalyticsSessionHandlerMap.get(key);
                if (handler == null) {
                    handler = new SessionHandler(this.mContext, key, sSessionHandlerThread.getLooper());
                    sLocalyticsSessionHandlerMap.put(key, handler);
                    handler.sendMessage(handler.obtainMessage(0));
                }
                this.mSessionHandler = handler;
            }
        } else {
            throw new IllegalArgumentException(String.format("context.getPackageName() returned %s", context.getPackageName()));
        }
    }

    public void setOptOut(boolean isOptedOut) {
        int i;
        Handler handler = this.mSessionHandler;
        Handler handler2 = this.mSessionHandler;
        if (isOptedOut) {
            i = 1;
        } else {
            i = 0;
        }
        handler.sendMessage(handler2.obtainMessage(6, i, 0));
    }

    public void open() {
        open(null);
    }

    public void open(List<String> customDimensions) {
        if (customDimensions != null) {
            if (customDimensions.isEmpty()) {
            }
            if (customDimensions.size() > 4) {
            }
            for (String element : customDimensions) {
                if (element == null) {
                    throw new IllegalArgumentException("customDimensions cannot contain null elements");
                } else if (element.length() == 0) {
                    throw new IllegalArgumentException("customDimensions cannot contain empty elements");
                }
            }
        }
        if (customDimensions == null || customDimensions.isEmpty()) {
            this.mSessionHandler.sendEmptyMessage(1);
        } else {
            this.mSessionHandler.sendMessage(this.mSessionHandler.obtainMessage(1, new TreeMap(convertDimensionsToAttributes(customDimensions))));
        }
    }

    public void close() {
        close(null);
    }

    public void close(List<String> customDimensions) {
        if (customDimensions != null) {
            if (customDimensions.isEmpty()) {
            }
            if (customDimensions.size() > 4) {
            }
            for (String element : customDimensions) {
                if (element == null) {
                    throw new IllegalArgumentException("customDimensions cannot contain null elements");
                } else if (element.length() == 0) {
                    throw new IllegalArgumentException("customDimensions cannot contain empty elements");
                }
            }
        }
        if (customDimensions == null || customDimensions.isEmpty()) {
            this.mSessionHandler.sendEmptyMessage(2);
        } else {
            this.mSessionHandler.sendMessage(this.mSessionHandler.obtainMessage(2, new TreeMap(convertDimensionsToAttributes(customDimensions))));
        }
    }

    public void tagEvent(String event) {
        tagEvent(event, null);
    }

    public void tagEvent(String event, Map<String, String> attributes) {
        tagEvent(event, attributes, null);
    }

    public void tagEvent(String event, Map<String, String> attributes, List<String> customDimensions) {
        if (event == null) {
            throw new IllegalArgumentException("event cannot be null");
        } else if (event.length() == 0) {
            throw new IllegalArgumentException("event cannot be empty");
        } else {
            if (attributes != null) {
                if (attributes.isEmpty()) {
                }
                if (attributes.size() > 10) {
                }
                for (Map.Entry<String, String> entry : attributes.entrySet()) {
                    String key = (String) entry.getKey();
                    String value = (String) entry.getValue();
                    if (key == null) {
                        throw new IllegalArgumentException("attributes cannot contain null keys");
                    } else if (value == null) {
                        throw new IllegalArgumentException("attributes cannot contain null values");
                    } else if (key.length() == 0) {
                        throw new IllegalArgumentException("attributes cannot contain empty keys");
                    } else if (value.length() == 0) {
                        throw new IllegalArgumentException("attributes cannot contain empty values");
                    }
                }
            }
            if (customDimensions != null) {
                if (customDimensions.isEmpty()) {
                }
                if (customDimensions.size() > 4) {
                }
                for (String element : customDimensions) {
                    if (element == null) {
                        throw new IllegalArgumentException("customDimensions cannot contain null elements");
                    } else if (element.length() == 0) {
                        throw new IllegalArgumentException("customDimensions cannot contain empty elements");
                    }
                }
            }
            String eventString = String.format(EVENT_FORMAT, this.mContext.getPackageName(), event);
            if (attributes == null && customDimensions == null) {
                this.mSessionHandler.sendMessage(this.mSessionHandler.obtainMessage(3, new Pair(eventString, null)));
                return;
            }
            TreeMap<String, String> remappedAttributes = new TreeMap<>();
            if (attributes != null) {
                String packageName = this.mContext.getPackageName();
                for (Map.Entry<String, String> entry2 : attributes.entrySet()) {
                    remappedAttributes.put(String.format(EVENT_FORMAT, packageName, entry2.getKey()), (String) entry2.getValue());
                }
            }
            if (customDimensions != null) {
                remappedAttributes.putAll(convertDimensionsToAttributes(customDimensions));
            }
            this.mSessionHandler.sendMessage(this.mSessionHandler.obtainMessage(3, new Pair(eventString, new TreeMap((SortedMap) remappedAttributes))));
        }
    }

    public void tagScreen(String screen) {
        if (screen == null) {
            throw new IllegalArgumentException("event cannot be null");
        } else if (screen.length() == 0) {
            throw new IllegalArgumentException("event cannot be empty");
        } else {
            this.mSessionHandler.sendMessage(this.mSessionHandler.obtainMessage(7, screen));
        }
    }

    public void upload() {
        this.mSessionHandler.sendMessage(this.mSessionHandler.obtainMessage(4, null));
    }

    public static String createRangedAttribute(int actualValue, int minValue, int maxValue, int step) {
        if (step < 1 || minValue >= maxValue) {
            return null;
        }
        int stepQuantity = ((maxValue - minValue) + step) / step;
        int[] steps = new int[(stepQuantity + 1)];
        for (int currentStep = 0; currentStep <= stepQuantity; currentStep++) {
            steps[currentStep] = (currentStep * step) + minValue;
        }
        return createRangedAttribute(actualValue, steps);
    }

    public static String createRangedAttribute(int actualValue, int[] steps) {
        if (steps == null) {
            throw new IllegalArgumentException("steps cannot be null");
        } else if (steps.length == 0) {
            throw new IllegalArgumentException("steps length must be greater than 0");
        } else if (actualValue < steps[0]) {
            return "less than " + steps[0];
        } else {
            if (actualValue >= steps[steps.length - 1]) {
                return String.valueOf(steps[steps.length - 1]) + " and above";
            }
            int bucketIndex = Arrays.binarySearch(steps, actualValue);
            if (bucketIndex < 0) {
                bucketIndex = (-bucketIndex) - 2;
            }
            if (steps[bucketIndex] == steps[bucketIndex + 1] - 1) {
                return Integer.toString(steps[bucketIndex]);
            }
            return String.valueOf(steps[bucketIndex]) + "-" + (steps[bucketIndex + 1] - 1);
        }
    }

    private static Map<String, String> convertDimensionsToAttributes(List<String> customDimensions) {
        TreeMap<String, String> attributes = new TreeMap<>();
        if (customDimensions != null) {
            int index = 0;
            for (String element : customDimensions) {
                if (index == 0) {
                    attributes.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_1, element);
                } else if (1 == index) {
                    attributes.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_2, element);
                } else if (2 == index) {
                    attributes.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_3, element);
                } else if (3 == index) {
                    attributes.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_4, element);
                }
                index++;
            }
        }
        return attributes;
    }

    static final class SessionHandler extends Handler {
        private static /* synthetic */ int[] $SWITCH_TABLE$android$database$CursorJoiner$Result = null;
        private static final String EVENTS_SORT_ORDER = String.format("CAST(%s as TEXT)", DmsConstants.ID);
        private static final String[] JOINER_ARG_UPLOAD_EVENTS_COLUMNS = {DmsConstants.ID};
        public static final int MESSAGE_CLOSE = 2;
        public static final int MESSAGE_INIT = 0;
        public static final int MESSAGE_OPEN = 1;
        public static final int MESSAGE_OPT_OUT = 6;
        public static final int MESSAGE_TAG_EVENT = 3;
        public static final int MESSAGE_TAG_SCREEN = 7;
        public static final int MESSAGE_UPLOAD = 4;
        public static final int MESSAGE_UPLOAD_CALLBACK = 5;
        private static final String[] PROJECTION_FLOW_BLOBS = {"events_key_ref"};
        private static final String[] PROJECTION_FLOW_EVENTS = {DmsConstants.ID};
        private static final String[] PROJECTION_GET_INSTALLATION_ID = {"uuid"};
        private static final String[] PROJECTION_GET_NUMBER_OF_SESSIONS = {DmsConstants.ID};
        private static final String[] PROJECTION_GET_OPEN_SESSION_ID_EVENT_COUNT = {"_count"};
        private static final String[] PROJECTION_GET_OPEN_SESSION_ID_SESSION_ID = {DmsConstants.ID};
        private static final String[] PROJECTION_INIT_API_KEY = {DmsConstants.ID, LocalyticsProvider.ApiKeysDbColumns.OPT_OUT, "uuid"};
        private static final String[] PROJECTION_IS_OPTED_OUT = {LocalyticsProvider.ApiKeysDbColumns.OPT_OUT};
        private static final String[] PROJECTION_OPEN_BLOB_EVENTS = {"events_key_ref"};
        private static final String[] PROJECTION_OPEN_CLOSED_SESSION = {"session_key_ref"};
        private static final String[] PROJECTION_OPEN_DELETE_EMPTIES_EVENT_ID = {DmsConstants.ID};
        private static final String[] PROJECTION_OPEN_DELETE_EMPTIES_PROCESSED_IN_BLOB = {LocalyticsProvider.EventHistoryDbColumns.PROCESSED_IN_BLOB};
        private static final String[] PROJECTION_OPEN_EVENT_ID = {DmsConstants.ID};
        private static final String[] PROJECTION_OPEN_SESSIONS = {DmsConstants.ID, LocalyticsProvider.SessionsDbColumns.SESSION_START_WALL_TIME};
        private static final String[] PROJECTION_TAG_EVENT = {LocalyticsProvider.SessionsDbColumns.SESSION_START_WALL_TIME};
        private static final String[] PROJECTION_TAG_SCREEN = {LocalyticsProvider.EventHistoryDbColumns.NAME};
        private static final String[] PROJECTION_UPLOAD_BLOBS = {"events_key_ref"};
        private static final String[] PROJECTION_UPLOAD_EVENTS = {DmsConstants.ID, LocalyticsProvider.EventsDbColumns.EVENT_NAME, LocalyticsProvider.EventsDbColumns.WALL_TIME};
        private static final String[] SELECTION_ARGS_FLOW_EVENTS = {LocalyticsSession.FLOW_EVENT};
        private static final String SELECTION_FLOW_EVENTS = String.format("%s = ?", LocalyticsProvider.EventsDbColumns.EVENT_NAME);
        private static final String SELECTION_GET_INSTALLATION_ID = String.format("%s = ?", LocalyticsProvider.ApiKeysDbColumns.API_KEY);
        private static final String SELECTION_GET_OPEN_SESSION_ID_EVENT_COUNT = String.format("%s = ? AND %s = ?", "session_key_ref", LocalyticsProvider.EventsDbColumns.EVENT_NAME);
        private static final String SELECTION_INIT_API_KEY = String.format("%s = ?", LocalyticsProvider.ApiKeysDbColumns.API_KEY);
        private static final String SELECTION_IS_OPTED_OUT = String.format("%s = ?", LocalyticsProvider.ApiKeysDbColumns.API_KEY);
        private static final String SELECTION_OPEN = String.format("%s = ? AND %s >= ?", LocalyticsProvider.EventsDbColumns.EVENT_NAME, LocalyticsProvider.EventsDbColumns.WALL_TIME);
        private static final String SELECTION_OPEN_CLOSED_SESSION = String.format("%s = ?", DmsConstants.ID);
        private static final String SELECTION_OPEN_CLOSED_SESSION_ATTRIBUTES = String.format("%s = ?", "events_key_ref");
        private static final String SELECTION_OPEN_DELETE_EMPTIES_EVENTS_SESSION_KEY_REF = String.format("%s = ?", "session_key_ref");
        private static final String SELECTION_OPEN_DELETE_EMPTIES_EVENT_HISTORY_SESSION_KEY_REF = String.format("%s = ?", "session_key_ref");
        private static final String SELECTION_OPEN_DELETE_EMPTIES_SESSIONS_ID = String.format("%s = ?", DmsConstants.ID);
        private static final String SELECTION_OPEN_DELETE_EMPTIES_UPLOAD_BLOBS_ID = String.format("%s = ?", DmsConstants.ID);
        private static final String SELECTION_OPEN_NEW_SESSION = String.format("%s = ?", LocalyticsProvider.ApiKeysDbColumns.API_KEY);
        private static final String SELECTION_OPT_IN_OUT = String.format("%s = ?", DmsConstants.ID);
        private static final String SELECTION_TAG_EVENT = String.format("%s = ?", DmsConstants.ID);
        private static final String SELECTION_TAG_SCREEN = String.format("%s = ? AND %s = ?", LocalyticsProvider.EventHistoryDbColumns.TYPE, "session_key_ref");
        private static final String SELECTION_UPLOAD_NULL_BLOBS = String.format("%s IS NULL", LocalyticsProvider.EventHistoryDbColumns.PROCESSED_IN_BLOB);
        private static final String SORT_ORDER_TAG_SCREEN = String.format("%s DESC", DmsConstants.ID);
        private static final String UPLOAD_BLOBS_EVENTS_SORT_ORDER = String.format("CAST(%s AS TEXT)", "events_key_ref");
        private final String mApiKey;
        private long mApiKeyId;
        private final Context mContext;
        protected LocalyticsProvider mProvider;
        private Handler mUploadHandler;

        static /* synthetic */ int[] $SWITCH_TABLE$android$database$CursorJoiner$Result() {
            int[] iArr = $SWITCH_TABLE$android$database$CursorJoiner$Result;
            if (iArr == null) {
                iArr = new int[CursorJoiner.Result.values().length];
                try {
                    iArr[CursorJoiner.Result.BOTH.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[CursorJoiner.Result.LEFT.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[CursorJoiner.Result.RIGHT.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                $SWITCH_TABLE$android$database$CursorJoiner$Result = iArr;
            }
            return iArr;
        }

        public SessionHandler(Context context, String key, Looper looper) {
            super(looper);
            if (context == null) {
                throw new IllegalArgumentException("context cannot be null");
            } else if (TextUtils.isEmpty(key)) {
                throw new IllegalArgumentException("key cannot be null or empty");
            } else {
                this.mContext = context;
                this.mApiKey = key;
            }
        }

        public void handleMessage(final Message msg) {
            try {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 0:
                        init();
                        return;
                    case 1:
                        this.mProvider.runBatchTransaction(new Runnable() {
                            public void run() {
                                SessionHandler.this.open(false, (Map) msg.obj);
                            }
                        });
                        return;
                    case 2:
                        this.mProvider.runBatchTransaction(new Runnable() {
                            public void run() {
                                SessionHandler.this.close((Map) msg.obj);
                            }
                        });
                        return;
                    case 3:
                        Pair<String, Map<String, String>> pair = (Pair) msg.obj;
                        final String event = (String) pair.first;
                        final Map<String, String> attributes = pair.second;
                        this.mProvider.runBatchTransaction(new Runnable() {
                            public void run() {
                                Map<String, String> openCloseAttributes;
                                if (SessionHandler.getOpenSessionId(SessionHandler.this.mProvider) != null) {
                                    SessionHandler.this.tagEvent(event, attributes);
                                    return;
                                }
                                if (attributes == null) {
                                    openCloseAttributes = null;
                                } else if (attributes.containsKey(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_1) || attributes.containsKey(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_2) || attributes.containsKey(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_3) || attributes.containsKey(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_4)) {
                                    openCloseAttributes = new TreeMap<>();
                                    if (attributes.containsKey(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_1)) {
                                        openCloseAttributes.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_1, (String) attributes.get(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_1));
                                    }
                                    if (attributes.containsKey(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_2)) {
                                        openCloseAttributes.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_2, (String) attributes.get(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_2));
                                    }
                                    if (attributes.containsKey(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_3)) {
                                        openCloseAttributes.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_3, (String) attributes.get(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_3));
                                    }
                                    if (attributes.containsKey(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_4)) {
                                        openCloseAttributes.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_4, (String) attributes.get(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_4));
                                    }
                                } else {
                                    openCloseAttributes = null;
                                }
                                SessionHandler.this.open(false, openCloseAttributes);
                                SessionHandler.this.tagEvent(event, attributes);
                                SessionHandler.this.close(openCloseAttributes);
                            }
                        });
                        return;
                    case 4:
                        final Runnable callback = (Runnable) msg.obj;
                        this.mProvider.runBatchTransaction(new Runnable() {
                            public void run() {
                                SessionHandler.this.upload(callback);
                            }
                        });
                        return;
                    case 5:
                        LocalyticsSession.sIsUploadingMap.put(this.mApiKey, Boolean.FALSE);
                        return;
                    case 6:
                        final boolean isOptingOut = msg.arg1 != 0;
                        this.mProvider.runBatchTransaction(new Runnable() {
                            public void run() {
                                SessionHandler.this.optOut(isOptingOut);
                            }
                        });
                        return;
                    case 7:
                        final String screen = (String) msg.obj;
                        this.mProvider.runBatchTransaction(new Runnable() {
                            public void run() {
                                SessionHandler.this.tagScreen(screen);
                            }
                        });
                        return;
                    default:
                        throw new RuntimeException("Fell through switch statement");
                }
            } catch (Exception e) {
            }
        }

        /* access modifiers changed from: package-private */
        public void init() {
            this.mProvider = LocalyticsProvider.getInstance(this.mContext, this.mApiKey);
            Cursor cursor = null;
            try {
                cursor = this.mProvider.query(LocalyticsProvider.ApiKeysDbColumns.TABLE_NAME, PROJECTION_INIT_API_KEY, SELECTION_INIT_API_KEY, new String[]{this.mApiKey}, null);
                if (cursor.moveToFirst()) {
                    this.mApiKeyId = cursor.getLong(cursor.getColumnIndexOrThrow(DmsConstants.ID));
                } else {
                    ContentValues values = new ContentValues();
                    values.put(LocalyticsProvider.ApiKeysDbColumns.API_KEY, this.mApiKey);
                    values.put("uuid", UUID.randomUUID().toString());
                    values.put(LocalyticsProvider.ApiKeysDbColumns.OPT_OUT, Boolean.FALSE);
                    values.put(LocalyticsProvider.ApiKeysDbColumns.CREATED_TIME, Long.valueOf(System.currentTimeMillis()));
                    this.mApiKeyId = this.mProvider.insert(LocalyticsProvider.ApiKeysDbColumns.TABLE_NAME, values);
                }
                if (!LocalyticsSession.sIsUploadingMap.containsKey(this.mApiKey)) {
                    LocalyticsSession.sIsUploadingMap.put(this.mApiKey, Boolean.FALSE);
                }
                this.mUploadHandler = new UploadHandler(this.mContext, this, this.mApiKey, LocalyticsSession.sUploadHandlerThread.getLooper());
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void optOut(boolean isOptingOut) {
            if (isOptedOut(this.mProvider, this.mApiKey) != isOptingOut) {
                if (getOpenSessionId(this.mProvider) == null) {
                    open(true, null);
                    tagEvent(isOptingOut ? LocalyticsSession.OPT_OUT_EVENT : LocalyticsSession.OPT_IN_EVENT, null);
                    close(null);
                } else {
                    tagEvent(isOptingOut ? LocalyticsSession.OPT_OUT_EVENT : LocalyticsSession.OPT_IN_EVENT, null);
                }
                ContentValues values = new ContentValues();
                values.put(LocalyticsProvider.ApiKeysDbColumns.OPT_OUT, Boolean.valueOf(isOptingOut));
                this.mProvider.update(LocalyticsProvider.ApiKeysDbColumns.TABLE_NAME, values, SELECTION_OPT_IN_OUT, new String[]{Long.toString(this.mApiKeyId)});
            }
        }

        static Long getOpenSessionId(LocalyticsProvider provider) {
            Cursor sessionsCursor = null;
            try {
                Cursor sessionsCursor2 = provider.query(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, PROJECTION_GET_OPEN_SESSION_ID_SESSION_ID, null, null, DmsConstants.ID);
                if (sessionsCursor2.moveToLast()) {
                    Long sessionId = Long.valueOf(sessionsCursor2.getLong(sessionsCursor2.getColumnIndexOrThrow(DmsConstants.ID)));
                    if (sessionsCursor2 != null) {
                        sessionsCursor2.close();
                    }
                    Cursor eventsCursor = null;
                    try {
                        eventsCursor = provider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, PROJECTION_GET_OPEN_SESSION_ID_EVENT_COUNT, SELECTION_GET_OPEN_SESSION_ID_EVENT_COUNT, new String[]{sessionId.toString(), LocalyticsSession.CLOSE_EVENT}, null);
                        if (eventsCursor.moveToFirst() && eventsCursor.getInt(0) == 0) {
                        }
                        if (eventsCursor != null) {
                            eventsCursor.close();
                        }
                        return null;
                    } finally {
                        if (eventsCursor != null) {
                            eventsCursor.close();
                        }
                    }
                } else {
                    if (sessionsCursor2 != null) {
                        sessionsCursor2.close();
                    }
                    return null;
                }
            } finally {
                if (sessionsCursor != null) {
                    sessionsCursor.close();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void open(boolean ignoreLimits, Map<String, String> attributes) {
            Cursor eventHistory;
            if (!isOptedOut(this.mProvider, this.mApiKey)) {
                long closeEventId = -1;
                Cursor eventsCursor = null;
                Cursor blob_eventsCursor = null;
                try {
                    eventsCursor = this.mProvider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, PROJECTION_OPEN_EVENT_ID, SELECTION_OPEN, new String[]{LocalyticsSession.CLOSE_EVENT, Long.toString(System.currentTimeMillis() - Constants.SESSION_EXPIRATION)}, EVENTS_SORT_ORDER);
                    blob_eventsCursor = this.mProvider.query(LocalyticsProvider.UploadBlobEventsDbColumns.TABLE_NAME, PROJECTION_OPEN_BLOB_EVENTS, null, null, UPLOAD_BLOBS_EVENTS_SORT_ORDER);
                    int idColumn = eventsCursor.getColumnIndexOrThrow(DmsConstants.ID);
                    Iterator<CursorJoiner.Result> it = new CursorJoiner(eventsCursor, PROJECTION_OPEN_EVENT_ID, blob_eventsCursor, PROJECTION_OPEN_BLOB_EVENTS).iterator();
                    while (it.hasNext()) {
                        switch ($SWITCH_TABLE$android$database$CursorJoiner$Result()[it.next().ordinal()]) {
                            case 2:
                                if (-1 != closeEventId) {
                                    long newClose = eventsCursor.getLong(eventsCursor.getColumnIndexOrThrow(DmsConstants.ID));
                                    if (newClose > closeEventId) {
                                        closeEventId = newClose;
                                    }
                                }
                                if (-1 != closeEventId) {
                                    break;
                                } else {
                                    closeEventId = eventsCursor.getLong(idColumn);
                                    break;
                                }
                        }
                    }
                    if (eventsCursor != null) {
                        eventsCursor.close();
                    }
                    if (blob_eventsCursor != null) {
                        blob_eventsCursor.close();
                    }
                    if (-1 != closeEventId) {
                        Log.v(Constants.LOG_TAG, "Opening old closed session and reconnecting");
                        openClosedSession(closeEventId);
                        return;
                    }
                    Cursor sessionsCursor = null;
                    try {
                        sessionsCursor = this.mProvider.query(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, PROJECTION_OPEN_SESSIONS, null, null, DmsConstants.ID);
                        if (sessionsCursor.moveToLast()) {
                            if (sessionsCursor.getLong(sessionsCursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.SESSION_START_WALL_TIME)) >= System.currentTimeMillis() - Constants.SESSION_EXPIRATION) {
                                Log.v(Constants.LOG_TAG, "Opening old unclosed session and reconnecting");
                                if (sessionsCursor == null) {
                                    return;
                                }
                                return;
                            }
                            Cursor eventsCursor2 = null;
                            try {
                                String[] sessionIdSelection = {Long.toString(sessionsCursor.getLong(sessionsCursor.getColumnIndexOrThrow(DmsConstants.ID)))};
                                eventsCursor2 = this.mProvider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, PROJECTION_OPEN_DELETE_EMPTIES_EVENT_ID, SELECTION_OPEN_DELETE_EMPTIES_EVENTS_SESSION_KEY_REF, sessionIdSelection, null);
                                if (eventsCursor2.getCount() == 0) {
                                    List<Long> blobsToDelete = new LinkedList<>();
                                    eventHistory = null;
                                    eventHistory = this.mProvider.query(LocalyticsProvider.EventHistoryDbColumns.TABLE_NAME, PROJECTION_OPEN_DELETE_EMPTIES_PROCESSED_IN_BLOB, SELECTION_OPEN_DELETE_EMPTIES_EVENT_HISTORY_SESSION_KEY_REF, sessionIdSelection, null);
                                    while (eventHistory.moveToNext()) {
                                        blobsToDelete.add(Long.valueOf(eventHistory.getLong(eventHistory.getColumnIndexOrThrow(LocalyticsProvider.EventHistoryDbColumns.PROCESSED_IN_BLOB))));
                                    }
                                    if (eventHistory != null) {
                                        eventHistory.close();
                                    }
                                    this.mProvider.delete(LocalyticsProvider.EventHistoryDbColumns.TABLE_NAME, SELECTION_OPEN_DELETE_EMPTIES_EVENT_HISTORY_SESSION_KEY_REF, sessionIdSelection);
                                    for (Long longValue : blobsToDelete) {
                                        long blobId = longValue.longValue();
                                        this.mProvider.delete(LocalyticsProvider.UploadBlobsDbColumns.TABLE_NAME, SELECTION_OPEN_DELETE_EMPTIES_UPLOAD_BLOBS_ID, new String[]{Long.toString(blobId)});
                                    }
                                    this.mProvider.delete(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, SELECTION_OPEN_DELETE_EMPTIES_SESSIONS_ID, sessionIdSelection);
                                }
                                if (eventsCursor2 != null) {
                                    eventsCursor2.close();
                                }
                            } catch (Throwable th) {
                                if (eventsCursor2 != null) {
                                    eventsCursor2.close();
                                }
                                throw th;
                            }
                        }
                        if (sessionsCursor != null) {
                            sessionsCursor.close();
                        }
                        if (ignoreLimits || getNumberOfSessions(this.mProvider) < 10) {
                            openNewSession(attributes);
                        }
                    } finally {
                        if (sessionsCursor != null) {
                            sessionsCursor.close();
                        }
                    }
                } catch (Throwable th2) {
                    if (eventsCursor != null) {
                        eventsCursor.close();
                    }
                    if (blob_eventsCursor != null) {
                        blob_eventsCursor.close();
                    }
                    throw th2;
                }
            }
        }

        private void openNewSession(Map<String, String> attributes) {
            TelephonyManager telephonyManager = (TelephonyManager) this.mContext.getSystemService("phone");
            ContentValues values = new ContentValues();
            values.put(LocalyticsProvider.SessionsDbColumns.API_KEY_REF, Long.valueOf(this.mApiKeyId));
            values.put(LocalyticsProvider.SessionsDbColumns.SESSION_START_WALL_TIME, Long.valueOf(System.currentTimeMillis()));
            values.put("uuid", UUID.randomUUID().toString());
            values.put(LocalyticsProvider.SessionsDbColumns.APP_VERSION, DatapointHelper.getAppVersion(this.mContext));
            values.put(LocalyticsProvider.SessionsDbColumns.ANDROID_SDK, Integer.valueOf(Constants.CURRENT_API_LEVEL));
            values.put(LocalyticsProvider.SessionsDbColumns.ANDROID_VERSION, Build.VERSION.RELEASE);
            String deviceId = DatapointHelper.getAndroidIdHashOrNull(this.mContext);
            if (deviceId == null) {
                Cursor cursor = null;
                try {
                    Cursor cursor2 = this.mProvider.query(LocalyticsProvider.ApiKeysDbColumns.TABLE_NAME, null, SELECTION_OPEN_NEW_SESSION, new String[]{this.mApiKey}, null);
                    if (cursor2.moveToFirst()) {
                        deviceId = cursor2.getString(cursor2.getColumnIndexOrThrow("uuid"));
                    }
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            values.put(LocalyticsProvider.SessionsDbColumns.DEVICE_ANDROID_ID_HASH, deviceId);
            values.put(LocalyticsProvider.SessionsDbColumns.DEVICE_COUNTRY, telephonyManager.getSimCountryIso());
            values.put(LocalyticsProvider.SessionsDbColumns.DEVICE_MANUFACTURER, DatapointHelper.getManufacturer());
            values.put(LocalyticsProvider.SessionsDbColumns.DEVICE_MODEL, Build.MODEL);
            values.put(LocalyticsProvider.SessionsDbColumns.DEVICE_SERIAL_NUMBER_HASH, DatapointHelper.getSerialNumberHashOrNull());
            values.put(LocalyticsProvider.SessionsDbColumns.DEVICE_TELEPHONY_ID, DatapointHelper.getTelephonyDeviceIdOrNull(this.mContext));
            values.put(LocalyticsProvider.SessionsDbColumns.DEVICE_TELEPHONY_ID_HASH, DatapointHelper.getTelephonyDeviceIdHashOrNull(this.mContext));
            values.put(LocalyticsProvider.SessionsDbColumns.DEVICE_WIFI_MAC_HASH, DatapointHelper.getWifiMacHashOrNull(this.mContext));
            values.put(LocalyticsProvider.SessionsDbColumns.LOCALE_COUNTRY, Locale.getDefault().getCountry());
            values.put(LocalyticsProvider.SessionsDbColumns.LOCALE_LANGUAGE, Locale.getDefault().getLanguage());
            values.put(LocalyticsProvider.SessionsDbColumns.LOCALYTICS_LIBRARY_VERSION, Constants.LOCALYTICS_CLIENT_LIBRARY_VERSION);
            values.put("iu", getInstallationId(this.mProvider, this.mApiKey));
            values.putNull(LocalyticsProvider.SessionsDbColumns.LATITUDE);
            values.putNull(LocalyticsProvider.SessionsDbColumns.LONGITUDE);
            values.put(LocalyticsProvider.SessionsDbColumns.NETWORK_CARRIER, telephonyManager.getNetworkOperatorName());
            values.put(LocalyticsProvider.SessionsDbColumns.NETWORK_COUNTRY, telephonyManager.getNetworkCountryIso());
            values.put(LocalyticsProvider.SessionsDbColumns.NETWORK_TYPE, DatapointHelper.getNetworkType(this.mContext, telephonyManager));
            if (this.mProvider.insert(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, values) == -1) {
                throw new AssertionError("session insert failed");
            }
            tagEvent(LocalyticsSession.OPEN_EVENT, attributes);
            LocalyticsProvider.deleteOldFiles(this.mContext);
        }

        private static String getInstallationId(LocalyticsProvider provider, String apiKey) {
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.ApiKeysDbColumns.TABLE_NAME, PROJECTION_GET_INSTALLATION_ID, SELECTION_GET_INSTALLATION_ID, new String[]{apiKey}, null);
                if (cursor.moveToFirst()) {
                    String string = cursor.getString(cursor.getColumnIndexOrThrow("uuid"));
                }
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        private void openClosedSession(long closeEventId) {
            String[] selectionArgs = {Long.toString(closeEventId)};
            Cursor cursor = null;
            try {
                cursor = this.mProvider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, PROJECTION_OPEN_CLOSED_SESSION, SELECTION_OPEN_CLOSED_SESSION, selectionArgs, null);
                if (cursor.moveToFirst()) {
                    this.mProvider.delete(LocalyticsProvider.AttributesDbColumns.TABLE_NAME, SELECTION_OPEN_CLOSED_SESSION_ATTRIBUTES, selectionArgs);
                    this.mProvider.delete(LocalyticsProvider.EventsDbColumns.TABLE_NAME, SELECTION_OPEN_CLOSED_SESSION, selectionArgs);
                } else {
                    openNewSession(null);
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static long getNumberOfSessions(LocalyticsProvider provider) {
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, PROJECTION_GET_NUMBER_OF_SESSIONS, null, null, null);
                return (long) cursor.getCount();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void close(Map<String, String> attributes) {
            if (getOpenSessionId(this.mProvider) != null) {
                tagEvent(LocalyticsSession.CLOSE_EVENT, attributes);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        /* access modifiers changed from: package-private */
        public void tagEvent(String event, Map<String, String> attributes) {
            Long openSessionId = getOpenSessionId(this.mProvider);
            if (openSessionId != null) {
                ContentValues values = new ContentValues();
                values.put("session_key_ref", openSessionId);
                values.put("uuid", UUID.randomUUID().toString());
                values.put(LocalyticsProvider.EventsDbColumns.EVENT_NAME, event);
                values.put(LocalyticsProvider.EventsDbColumns.REAL_TIME, Long.valueOf(SystemClock.elapsedRealtime()));
                values.put(LocalyticsProvider.EventsDbColumns.WALL_TIME, Long.valueOf(System.currentTimeMillis()));
                if (LocalyticsSession.OPEN_EVENT.equals(event)) {
                    Cursor cursor = null;
                    try {
                        cursor = this.mProvider.query(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, PROJECTION_TAG_EVENT, SELECTION_TAG_EVENT, new String[]{openSessionId.toString()}, null);
                        if (cursor.moveToFirst()) {
                            values.put(LocalyticsProvider.EventsDbColumns.WALL_TIME, Long.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.SESSION_START_WALL_TIME))));
                            if (cursor != null) {
                                cursor.close();
                            }
                        } else {
                            throw new AssertionError("During tag of open event, session didn't exist");
                        }
                    } catch (Throwable th) {
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                long eventId = this.mProvider.insert(LocalyticsProvider.EventsDbColumns.TABLE_NAME, values);
                if (-1 == eventId) {
                    throw new RuntimeException("Inserting event failed");
                }
                if (attributes != null) {
                    ContentValues values2 = new ContentValues();
                    String applicationAttributePrefix = String.format(LocalyticsSession.EVENT_FORMAT, this.mContext.getPackageName(), "");
                    int applicationAttributeCount = 0;
                    for (Map.Entry<String, String> entry : attributes.entrySet()) {
                        if (!((String) entry.getKey()).startsWith(applicationAttributePrefix) || (applicationAttributeCount = applicationAttributeCount + 1) <= 10) {
                            values2.put("events_key_ref", Long.valueOf(eventId));
                            values2.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_KEY, (String) entry.getKey());
                            values2.put(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_VALUE, (String) entry.getValue());
                            if (-1 == this.mProvider.insert(LocalyticsProvider.AttributesDbColumns.TABLE_NAME, values2)) {
                                throw new AssertionError("Inserting attribute failed");
                            }
                            values2.clear();
                        }
                    }
                }
                if (!LocalyticsSession.OPEN_EVENT.equals(event) && !LocalyticsSession.CLOSE_EVENT.equals(event) && !LocalyticsSession.OPT_IN_EVENT.equals(event) && !LocalyticsSession.OPT_OUT_EVENT.equals(event) && !LocalyticsSession.FLOW_EVENT.equals(event)) {
                    ContentValues values3 = new ContentValues();
                    values3.put(LocalyticsProvider.EventHistoryDbColumns.NAME, event.substring(this.mContext.getPackageName().length() + 1, event.length()));
                    values3.put(LocalyticsProvider.EventHistoryDbColumns.TYPE, (Integer) 0);
                    values3.put("session_key_ref", openSessionId);
                    values3.putNull(LocalyticsProvider.EventHistoryDbColumns.PROCESSED_IN_BLOB);
                    this.mProvider.insert(LocalyticsProvider.EventHistoryDbColumns.TABLE_NAME, values3);
                    conditionallyAddFlowEvent();
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        /* access modifiers changed from: package-private */
        public void tagScreen(String screen) {
            Long openSessionId = getOpenSessionId(this.mProvider);
            if (openSessionId != null) {
                Cursor cursor = null;
                try {
                    cursor = this.mProvider.query(LocalyticsProvider.EventHistoryDbColumns.TABLE_NAME, PROJECTION_TAG_SCREEN, SELECTION_TAG_SCREEN, new String[]{Integer.toString(1), openSessionId.toString()}, SORT_ORDER_TAG_SCREEN);
                    if (!cursor.moveToFirst() || !screen.equals(cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.EventHistoryDbColumns.NAME)))) {
                        if (cursor != null) {
                            cursor.close();
                        }
                        ContentValues values = new ContentValues();
                        values.put(LocalyticsProvider.EventHistoryDbColumns.NAME, screen);
                        values.put(LocalyticsProvider.EventHistoryDbColumns.TYPE, (Integer) 1);
                        values.put("session_key_ref", openSessionId);
                        values.putNull(LocalyticsProvider.EventHistoryDbColumns.PROCESSED_IN_BLOB);
                        this.mProvider.insert(LocalyticsProvider.EventHistoryDbColumns.TABLE_NAME, values);
                        conditionallyAddFlowEvent();
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        }

        private void conditionallyAddFlowEvent() {
            boolean foundUnassociatedFlowEvent = false;
            Cursor eventsCursor = null;
            Cursor blob_eventsCursor = null;
            try {
                eventsCursor = this.mProvider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, PROJECTION_FLOW_EVENTS, SELECTION_FLOW_EVENTS, SELECTION_ARGS_FLOW_EVENTS, EVENTS_SORT_ORDER);
                blob_eventsCursor = this.mProvider.query(LocalyticsProvider.UploadBlobEventsDbColumns.TABLE_NAME, PROJECTION_FLOW_BLOBS, null, null, UPLOAD_BLOBS_EVENTS_SORT_ORDER);
                Iterator<CursorJoiner.Result> it = new CursorJoiner(eventsCursor, PROJECTION_FLOW_EVENTS, blob_eventsCursor, PROJECTION_FLOW_BLOBS).iterator();
                while (it.hasNext()) {
                    switch ($SWITCH_TABLE$android$database$CursorJoiner$Result()[it.next().ordinal()]) {
                        case 2:
                            foundUnassociatedFlowEvent = true;
                            break;
                    }
                }
                if (!foundUnassociatedFlowEvent) {
                    tagEvent(LocalyticsSession.FLOW_EVENT, null);
                }
            } finally {
                if (eventsCursor != null) {
                    eventsCursor.close();
                }
                if (blob_eventsCursor != null) {
                    blob_eventsCursor.close();
                }
            }
        }

        static void preUploadBuildBlobs(LocalyticsProvider provider) {
            Set<Long> eventIds = new HashSet<>();
            Cursor eventsCursor = null;
            Cursor blob_eventsCursor = null;
            try {
                eventsCursor = provider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, PROJECTION_UPLOAD_EVENTS, null, null, EVENTS_SORT_ORDER);
                blob_eventsCursor = provider.query(LocalyticsProvider.UploadBlobEventsDbColumns.TABLE_NAME, PROJECTION_UPLOAD_BLOBS, null, null, UPLOAD_BLOBS_EVENTS_SORT_ORDER);
                int idColumn = eventsCursor.getColumnIndexOrThrow(DmsConstants.ID);
                Iterator<CursorJoiner.Result> it = new CursorJoiner(eventsCursor, JOINER_ARG_UPLOAD_EVENTS_COLUMNS, blob_eventsCursor, PROJECTION_UPLOAD_BLOBS).iterator();
                while (it.hasNext()) {
                    switch ($SWITCH_TABLE$android$database$CursorJoiner$Result()[it.next().ordinal()]) {
                        case 2:
                            if (LocalyticsSession.CLOSE_EVENT.equals(eventsCursor.getString(eventsCursor.getColumnIndexOrThrow(LocalyticsProvider.EventsDbColumns.EVENT_NAME))) && System.currentTimeMillis() - eventsCursor.getLong(eventsCursor.getColumnIndexOrThrow(LocalyticsProvider.EventsDbColumns.WALL_TIME)) < Constants.SESSION_EXPIRATION) {
                                break;
                            } else {
                                eventIds.add(Long.valueOf(eventsCursor.getLong(idColumn)));
                                break;
                            }
                    }
                }
                if (eventIds.size() > 0) {
                    ContentValues values = new ContentValues();
                    values.put("uuid", UUID.randomUUID().toString());
                    Long blobId = Long.valueOf(provider.insert(LocalyticsProvider.UploadBlobsDbColumns.TABLE_NAME, values));
                    values.clear();
                    for (Long x : eventIds) {
                        values.put(LocalyticsProvider.UploadBlobEventsDbColumns.UPLOAD_BLOBS_KEY_REF, blobId);
                        values.put("events_key_ref", x);
                        provider.insert(LocalyticsProvider.UploadBlobEventsDbColumns.TABLE_NAME, values);
                        values.clear();
                    }
                    values.put(LocalyticsProvider.EventHistoryDbColumns.PROCESSED_IN_BLOB, blobId);
                    provider.update(LocalyticsProvider.EventHistoryDbColumns.TABLE_NAME, values, SELECTION_UPLOAD_NULL_BLOBS, null);
                    values.clear();
                }
            } finally {
                if (eventsCursor != null) {
                    eventsCursor.close();
                }
                if (blob_eventsCursor != null) {
                    blob_eventsCursor.close();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void upload(Runnable callback) {
            if (LocalyticsSession.sIsUploadingMap.get(this.mApiKey).booleanValue()) {
                this.mUploadHandler.sendMessage(this.mUploadHandler.obtainMessage(2, callback));
                return;
            }
            try {
                preUploadBuildBlobs(this.mProvider);
                LocalyticsSession.sIsUploadingMap.put(this.mApiKey, Boolean.TRUE);
                this.mUploadHandler.sendMessage(this.mUploadHandler.obtainMessage(1, callback));
            } catch (Exception e) {
                LocalyticsSession.sIsUploadingMap.put(this.mApiKey, Boolean.FALSE);
                if (callback != null) {
                    new Thread(callback, "upload_callback").start();
                }
            }
        }

        static boolean isOptedOut(LocalyticsProvider provider, String apiKey) {
            if (provider == null) {
                throw new IllegalArgumentException("provider cannot be null");
            } else if (apiKey == null) {
                throw new IllegalArgumentException("apiKey cannot be null");
            } else {
                Cursor cursor = null;
                try {
                    cursor = provider.query(LocalyticsProvider.ApiKeysDbColumns.TABLE_NAME, PROJECTION_IS_OPTED_OUT, SELECTION_IS_OPTED_OUT, new String[]{apiKey}, null);
                    if (cursor.moveToFirst()) {
                        boolean z = cursor.getInt(cursor.getColumnIndexOrThrow(LocalyticsProvider.ApiKeysDbColumns.OPT_OUT)) != 0;
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return false;
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        }
    }

    static final class UploadHandler extends Handler {
        public static final int MESSAGE_RETRY_UPLOAD_REQUEST = 2;
        public static final int MESSAGE_UPLOAD = 1;
        private static final String UPLOAD_CALLBACK_THREAD_NAME = "upload_callback";
        private final String mApiKey;
        private final Context mContext;
        protected final LocalyticsProvider mProvider;
        private final Handler mSessionHandler;

        public UploadHandler(Context context, Handler sessionHandler, String apiKey, Looper looper) {
            super(looper);
            this.mContext = context;
            this.mProvider = LocalyticsProvider.getInstance(context, apiKey);
            this.mSessionHandler = sessionHandler;
            this.mApiKey = apiKey;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleMessage(android.os.Message r8) {
            /*
                r7 = this;
                super.handleMessage(r8)     // Catch:{ Exception -> 0x0010 }
                int r3 = r8.what     // Catch:{ Exception -> 0x0010 }
                switch(r3) {
                    case 1: goto L_0x0012;
                    case 2: goto L_0x0079;
                    default: goto L_0x0008;
                }     // Catch:{ Exception -> 0x0010 }
            L_0x0008:
                java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x0010 }
                java.lang.String r4 = "Fell through switch statement"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0010 }
                throw r3     // Catch:{ Exception -> 0x0010 }
            L_0x0010:
                r3 = move-exception
            L_0x0011:
                return
            L_0x0012:
                java.lang.Object r1 = r8.obj     // Catch:{ Exception -> 0x0010 }
                java.lang.Runnable r1 = (java.lang.Runnable) r1     // Catch:{ Exception -> 0x0010 }
                android.content.Context r3 = r7.mContext     // Catch:{ all -> 0x0065 }
                com.nth.analytics.android.LocalyticsProvider r4 = r7.mProvider     // Catch:{ all -> 0x0065 }
                java.lang.String r5 = r7.mApiKey     // Catch:{ all -> 0x0065 }
                org.json.JSONObject r2 = convertDatabaseToJsonNth(r3, r4, r5)     // Catch:{ all -> 0x0065 }
                if (r2 == 0) goto L_0x0052
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0065 }
                r0.<init>()     // Catch:{ all -> 0x0065 }
                java.lang.String r3 = r2.toString()     // Catch:{ all -> 0x0065 }
                r0.append(r3)     // Catch:{ all -> 0x0065 }
                java.lang.String r3 = com.nth.analytics.android.LocalyticsSession.mUrl     // Catch:{ all -> 0x0065 }
                r4 = 1
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0065 }
                r5 = 0
                java.lang.String r6 = r7.mApiKey     // Catch:{ all -> 0x0065 }
                r4[r5] = r6     // Catch:{ all -> 0x0065 }
                java.lang.String r3 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x0065 }
                java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0065 }
                boolean r3 = uploadSessions(r3, r4)     // Catch:{ all -> 0x0065 }
                if (r3 == 0) goto L_0x0052
                com.nth.analytics.android.LocalyticsProvider r3 = r7.mProvider     // Catch:{ all -> 0x0065 }
                com.nth.analytics.android.LocalyticsSession$UploadHandler$1 r4 = new com.nth.analytics.android.LocalyticsSession$UploadHandler$1     // Catch:{ all -> 0x0065 }
                r4.<init>()     // Catch:{ all -> 0x0065 }
                r3.runBatchTransaction(r4)     // Catch:{ all -> 0x0065 }
            L_0x0052:
                if (r1 == 0) goto L_0x005e
                java.lang.Thread r3 = new java.lang.Thread     // Catch:{ Exception -> 0x0010 }
                java.lang.String r4 = "upload_callback"
                r3.<init>(r1, r4)     // Catch:{ Exception -> 0x0010 }
                r3.start()     // Catch:{ Exception -> 0x0010 }
            L_0x005e:
                android.os.Handler r3 = r7.mSessionHandler     // Catch:{ Exception -> 0x0010 }
                r4 = 5
                r3.sendEmptyMessage(r4)     // Catch:{ Exception -> 0x0010 }
                goto L_0x0011
            L_0x0065:
                r3 = move-exception
                if (r1 == 0) goto L_0x0072
                java.lang.Thread r4 = new java.lang.Thread     // Catch:{ Exception -> 0x0010 }
                java.lang.String r5 = "upload_callback"
                r4.<init>(r1, r5)     // Catch:{ Exception -> 0x0010 }
                r4.start()     // Catch:{ Exception -> 0x0010 }
            L_0x0072:
                android.os.Handler r4 = r7.mSessionHandler     // Catch:{ Exception -> 0x0010 }
                r5 = 5
                r4.sendEmptyMessage(r5)     // Catch:{ Exception -> 0x0010 }
                throw r3     // Catch:{ Exception -> 0x0010 }
            L_0x0079:
                android.os.Handler r3 = r7.mSessionHandler     // Catch:{ Exception -> 0x0010 }
                android.os.Handler r4 = r7.mSessionHandler     // Catch:{ Exception -> 0x0010 }
                r5 = 4
                java.lang.Object r6 = r8.obj     // Catch:{ Exception -> 0x0010 }
                android.os.Message r4 = r4.obtainMessage(r5, r6)     // Catch:{ Exception -> 0x0010 }
                r3.sendMessage(r4)     // Catch:{ Exception -> 0x0010 }
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: com.nth.analytics.android.LocalyticsSession.UploadHandler.handleMessage(android.os.Message):void");
        }

        /* JADX WARN: Type inference failed for: r19v34, types: [java.net.URLConnection] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:103:0x01f6 A[SYNTHETIC, Splitter:B:103:0x01f6] */
        /* JADX WARNING: Removed duplicated region for block: B:109:0x0201 A[SYNTHETIC, Splitter:B:109:0x0201] */
        /* JADX WARNING: Removed duplicated region for block: B:115:0x020c A[SYNTHETIC, Splitter:B:115:0x020c] */
        /* JADX WARNING: Removed duplicated region for block: B:121:0x0217 A[SYNTHETIC, Splitter:B:121:0x0217] */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x00f2 A[SYNTHETIC, Splitter:B:47:0x00f2] */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x0100 A[SYNTHETIC, Splitter:B:55:0x0100] */
        @android.annotation.SuppressLint({"NewApi"})
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static boolean uploadSessions(java.lang.String r24, java.lang.String r25) {
            /*
                if (r24 != 0) goto L_0x000a
                java.lang.IllegalArgumentException r19 = new java.lang.IllegalArgumentException
                java.lang.String r20 = "url cannot be null"
                r19.<init>(r20)
                throw r19
            L_0x000a:
                if (r25 != 0) goto L_0x0014
                java.lang.IllegalArgumentException r19 = new java.lang.IllegalArgumentException
                java.lang.String r20 = "body cannot be null"
                r19.<init>(r20)
                throw r19
            L_0x0014:
                int r19 = com.nth.analytics.android.DatapointHelper.getApiLevel()
                r20 = 9
                r0 = r19
                r1 = r20
                if (r0 < r1) goto L_0x0182
                r3 = 0
                java.lang.String r19 = "UTF-8"
                r0 = r25
                r1 = r19
                byte[] r12 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x00e1, IOException -> 0x00ef, all -> 0x00fd }
                java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ UnsupportedEncodingException -> 0x00e1, IOException -> 0x00ef, all -> 0x00fd }
                int r0 = r12.length     // Catch:{ UnsupportedEncodingException -> 0x00e1, IOException -> 0x00ef, all -> 0x00fd }
                r19 = r0
                r0 = r19
                r4.<init>(r0)     // Catch:{ UnsupportedEncodingException -> 0x00e1, IOException -> 0x00ef, all -> 0x00fd }
                r4.write(r12)     // Catch:{ UnsupportedEncodingException -> 0x0243, IOException -> 0x023f, all -> 0x023b }
                r4.flush()     // Catch:{ UnsupportedEncodingException -> 0x0243, IOException -> 0x023f, all -> 0x023b }
                byte[] r7 = r4.toByteArray()     // Catch:{ UnsupportedEncodingException -> 0x0243, IOException -> 0x023f, all -> 0x023b }
                if (r4 == 0) goto L_0x0249
                r4.close()     // Catch:{ IOException -> 0x0109 }
                r3 = 0
            L_0x0045:
                r6 = 0
                java.net.URL r19 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r0 = r19
                r1 = r24
                r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.net.URLConnection r19 = r19.openConnection()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r0 = r19
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r6 = r0
                r19 = 1
                r0 = r19
                r6.setDoOutput(r0)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r19 = "Content-Type"
                java.lang.String r20 = "application/json"
                r0 = r19
                r1 = r20
                r6.setRequestProperty(r0, r1)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r19 = com.nth.analytics.android.LocalyticsSession.mAKey     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                boolean r19 = android.text.TextUtils.isEmpty(r19)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                if (r19 != 0) goto L_0x010d
                java.lang.String r19 = "Authorization"
                java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r21 = "ApiKey "
                r20.<init>(r21)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r21 = com.nth.analytics.android.LocalyticsSession.mAKey     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r20 = r20.toString()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r0 = r19
                r1 = r20
                r6.setRequestProperty(r0, r1)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
            L_0x0090:
                int r0 = r7.length     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r19 = r0
                r0 = r19
                r6.setFixedLengthStreamingMode(r0)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r18 = 0
                java.io.OutputStream r18 = r6.getOutputStream()     // Catch:{ all -> 0x015a }
                r0 = r18
                r0.write(r7)     // Catch:{ all -> 0x015a }
                if (r18 == 0) goto L_0x00ad
                r18.flush()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r18.close()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r18 = 0
            L_0x00ad:
                int r15 = r6.getResponseCode()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r19 = "Analytics"
                java.lang.String r20 = "Upload complete with status %d"
                r21 = 1
                r0 = r21
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r21 = r0
                r22 = 0
                java.lang.Integer r23 = java.lang.Integer.valueOf(r15)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r21[r22] = r23     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r20 = java.lang.String.format(r20, r21)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                android.util.Log.v(r19, r20)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r19 = 500(0x1f4, float:7.0E-43)
                r0 = r19
                if (r15 < r0) goto L_0x0179
                r19 = 599(0x257, float:8.4E-43)
                r0 = r19
                if (r15 > r0) goto L_0x0179
                if (r6 == 0) goto L_0x00de
                r6.disconnect()
                r6 = 0
            L_0x00de:
                r19 = 0
            L_0x00e0:
                return r19
            L_0x00e1:
                r8 = move-exception
            L_0x00e2:
                if (r3 == 0) goto L_0x00e8
                r3.close()     // Catch:{ IOException -> 0x00eb }
                r3 = 0
            L_0x00e8:
                r19 = 0
                goto L_0x00e0
            L_0x00eb:
                r8 = move-exception
                r19 = 0
                goto L_0x00e0
            L_0x00ef:
                r8 = move-exception
            L_0x00f0:
                if (r3 == 0) goto L_0x00f6
                r3.close()     // Catch:{ IOException -> 0x00f9 }
                r3 = 0
            L_0x00f6:
                r19 = 0
                goto L_0x00e0
            L_0x00f9:
                r8 = move-exception
                r19 = 0
                goto L_0x00e0
            L_0x00fd:
                r19 = move-exception
            L_0x00fe:
                if (r3 == 0) goto L_0x0104
                r3.close()     // Catch:{ IOException -> 0x0105 }
                r3 = 0
            L_0x0104:
                throw r19
            L_0x0105:
                r8 = move-exception
                r19 = 0
                goto L_0x00e0
            L_0x0109:
                r8 = move-exception
                r19 = 0
                goto L_0x00e0
            L_0x010d:
                java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r20 = com.nth.analytics.android.LocalyticsSession.mAuthUsername     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r20 = java.lang.String.valueOf(r20)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r19.<init>(r20)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r20 = ":"
                java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r20 = com.nth.analytics.android.LocalyticsSession.mAuthPassword     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r2 = r19.toString()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r19 = "Authorization"
                java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r21 = "Basic "
                r20.<init>(r21)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                byte[] r21 = r2.getBytes()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r22 = 0
                java.lang.String r21 = android.util.Base64.encodeToString(r21, r22)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                java.lang.String r20 = r20.toString()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r0 = r19
                r1 = r20
                r6.setRequestProperty(r0, r1)     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                goto L_0x0090
            L_0x0150:
                r8 = move-exception
                if (r6 == 0) goto L_0x0157
                r6.disconnect()
                r6 = 0
            L_0x0157:
                r19 = 0
                goto L_0x00e0
            L_0x015a:
                r19 = move-exception
                if (r18 == 0) goto L_0x0165
                r18.flush()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r18.close()     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
                r18 = 0
            L_0x0165:
                throw r19     // Catch:{ MalformedURLException -> 0x0150, IOException -> 0x0166, all -> 0x0171 }
            L_0x0166:
                r8 = move-exception
                if (r6 == 0) goto L_0x016d
                r6.disconnect()
                r6 = 0
            L_0x016d:
                r19 = 0
                goto L_0x00e0
            L_0x0171:
                r19 = move-exception
                if (r6 == 0) goto L_0x0178
                r6.disconnect()
                r6 = 0
            L_0x0178:
                throw r19
            L_0x0179:
                if (r6 == 0) goto L_0x017e
                r6.disconnect()
            L_0x017e:
                r19 = 1
                goto L_0x00e0
            L_0x0182:
                org.apache.http.impl.client.DefaultHttpClient r5 = new org.apache.http.impl.client.DefaultHttpClient
                r5.<init>()
                org.apache.http.client.methods.HttpPost r11 = new org.apache.http.client.methods.HttpPost
                r0 = r24
                r11.<init>(r0)
                java.lang.String r19 = "Content-Type"
                java.lang.String r20 = "application/x-gzip"
                r0 = r19
                r1 = r20
                r11.addHeader(r0, r1)
                r9 = 0
                java.lang.String r19 = "UTF-8"
                r0 = r25
                r1 = r19
                byte[] r12 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x01f3, ClientProtocolException -> 0x01fe, IOException -> 0x0209, all -> 0x0214 }
                java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ UnsupportedEncodingException -> 0x01f3, ClientProtocolException -> 0x01fe, IOException -> 0x0209, all -> 0x0214 }
                int r0 = r12.length     // Catch:{ UnsupportedEncodingException -> 0x01f3, ClientProtocolException -> 0x01fe, IOException -> 0x0209, all -> 0x0214 }
                r19 = r0
                r0 = r19
                r3.<init>(r0)     // Catch:{ UnsupportedEncodingException -> 0x01f3, ClientProtocolException -> 0x01fe, IOException -> 0x0209, all -> 0x0214 }
                java.util.zip.GZIPOutputStream r10 = new java.util.zip.GZIPOutputStream     // Catch:{ UnsupportedEncodingException -> 0x01f3, ClientProtocolException -> 0x01fe, IOException -> 0x0209, all -> 0x0214 }
                r10.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01f3, ClientProtocolException -> 0x01fe, IOException -> 0x0209, all -> 0x0214 }
                r10.write(r12)     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                r10.finish()     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                r10.flush()     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                org.apache.http.entity.ByteArrayEntity r13 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                byte[] r19 = r3.toByteArray()     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                r0 = r19
                r13.<init>(r0)     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                r11.setEntity(r13)     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                org.apache.http.HttpResponse r14 = r5.execute(r11)     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                org.apache.http.StatusLine r16 = r14.getStatusLine()     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                int r17 = r16.getStatusCode()     // Catch:{ UnsupportedEncodingException -> 0x0238, ClientProtocolException -> 0x0235, IOException -> 0x0232, all -> 0x022f }
                r19 = 500(0x1f4, float:7.0E-43)
                r0 = r17
                r1 = r19
                if (r0 < r1) goto L_0x021c
                r19 = 599(0x257, float:8.4E-43)
                r0 = r17
                r1 = r19
                if (r0 > r1) goto L_0x021c
                if (r10 == 0) goto L_0x0247
                r10.close()     // Catch:{ IOException -> 0x01f0 }
                r9 = 0
            L_0x01ec:
                r19 = 0
                goto L_0x00e0
            L_0x01f0:
                r19 = move-exception
                r9 = r10
                goto L_0x01ec
            L_0x01f3:
                r8 = move-exception
            L_0x01f4:
                if (r9 == 0) goto L_0x01fa
                r9.close()     // Catch:{ IOException -> 0x0224 }
                r9 = 0
            L_0x01fa:
                r19 = 0
                goto L_0x00e0
            L_0x01fe:
                r8 = move-exception
            L_0x01ff:
                if (r9 == 0) goto L_0x0205
                r9.close()     // Catch:{ IOException -> 0x0226 }
                r9 = 0
            L_0x0205:
                r19 = 0
                goto L_0x00e0
            L_0x0209:
                r8 = move-exception
            L_0x020a:
                if (r9 == 0) goto L_0x0210
                r9.close()     // Catch:{ IOException -> 0x0228 }
                r9 = 0
            L_0x0210:
                r19 = 0
                goto L_0x00e0
            L_0x0214:
                r19 = move-exception
            L_0x0215:
                if (r9 == 0) goto L_0x021b
                r9.close()     // Catch:{ IOException -> 0x022a }
                r9 = 0
            L_0x021b:
                throw r19
            L_0x021c:
                if (r10 == 0) goto L_0x017e
                r10.close()     // Catch:{ IOException -> 0x022c }
                r9 = 0
                goto L_0x017e
            L_0x0224:
                r19 = move-exception
                goto L_0x01fa
            L_0x0226:
                r19 = move-exception
                goto L_0x0205
            L_0x0228:
                r19 = move-exception
                goto L_0x0210
            L_0x022a:
                r20 = move-exception
                goto L_0x021b
            L_0x022c:
                r19 = move-exception
                goto L_0x017e
            L_0x022f:
                r19 = move-exception
                r9 = r10
                goto L_0x0215
            L_0x0232:
                r8 = move-exception
                r9 = r10
                goto L_0x020a
            L_0x0235:
                r8 = move-exception
                r9 = r10
                goto L_0x01ff
            L_0x0238:
                r8 = move-exception
                r9 = r10
                goto L_0x01f4
            L_0x023b:
                r19 = move-exception
                r3 = r4
                goto L_0x00fe
            L_0x023f:
                r8 = move-exception
                r3 = r4
                goto L_0x00f0
            L_0x0243:
                r8 = move-exception
                r3 = r4
                goto L_0x00e2
            L_0x0247:
                r9 = r10
                goto L_0x01ec
            L_0x0249:
                r3 = r4
                goto L_0x0045
            */
            throw new UnsupportedOperationException("Method not decompiled: com.nth.analytics.android.LocalyticsSession.UploadHandler.uploadSessions(java.lang.String, java.lang.String):boolean");
        }

        static List<JSONObject> convertDatabaseToJson(Context context, LocalyticsProvider provider, String apiKey) {
            Cursor blobEvents;
            LinkedList linkedList = new LinkedList();
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.UploadBlobsDbColumns.TABLE_NAME, null, null, null, null);
                long creationTime = getApiKeyCreationTime(provider, apiKey);
                int idColumn = cursor.getColumnIndexOrThrow(DmsConstants.ID);
                int uuidColumn = cursor.getColumnIndexOrThrow("uuid");
                while (cursor.moveToNext()) {
                    try {
                        JSONObject blobHeader = new JSONObject();
                        blobHeader.put("dt", JsonObjects.BlobHeader.VALUE_DATA_TYPE);
                        blobHeader.put(JsonObjects.BlobHeader.KEY_PERSISTENT_STORAGE_CREATION_TIME_SECONDS, creationTime);
                        blobHeader.put(JsonObjects.BlobHeader.KEY_SEQUENCE_NUMBER, cursor.getLong(idColumn));
                        blobHeader.put("u", cursor.getString(uuidColumn));
                        blobHeader.put("attrs", getAttributesFromSession(provider, apiKey, getSessionIdForBlobId(provider, cursor.getLong(idColumn))));
                        linkedList.add(blobHeader);
                        blobEvents = null;
                        blobEvents = provider.query(LocalyticsProvider.UploadBlobEventsDbColumns.TABLE_NAME, new String[]{"events_key_ref"}, String.format("%s = ?", LocalyticsProvider.UploadBlobEventsDbColumns.UPLOAD_BLOBS_KEY_REF), new String[]{Long.toString(cursor.getLong(idColumn))}, "events_key_ref");
                        int eventIdColumn = blobEvents.getColumnIndexOrThrow("events_key_ref");
                        while (blobEvents.moveToNext()) {
                            LinkedList linkedList2 = linkedList;
                            linkedList2.add(convertEventToJson(provider, context, blobEvents.getLong(eventIdColumn), cursor.getLong(idColumn), apiKey));
                        }
                        if (blobEvents != null) {
                            blobEvents.close();
                        }
                    } catch (JSONException e) {
                    } catch (Throwable th) {
                        if (blobEvents != null) {
                            blobEvents.close();
                        }
                        throw th;
                    }
                }
                return linkedList;
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static JSONObject convertDatabaseToJsonNth(Context context, LocalyticsProvider provider, String apiKey) {
            Cursor blobEvents;
            JSONObject result = new JSONObject();
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.UploadBlobsDbColumns.TABLE_NAME, null, null, null, null);
                Location gpsLocation = null;
                Location networkLocation = null;
                LocationManager l = (LocationManager) context.getSystemService("location");
                if (context.getPackageManager().checkPermission("android.permission.ACCESS_FINE_LOCATION", context.getPackageName()) == 0) {
                    gpsLocation = l.getLastKnownLocation("gps");
                }
                if (context.getPackageManager().checkPermission("android.permission.ACCESS_COARSE_LOCATION", context.getPackageName()) == 0) {
                    networkLocation = l.getLastKnownLocation("network");
                }
                Location location = networkLocation;
                if (LocalyticsSession.isBetterLocation(gpsLocation, networkLocation)) {
                    location = gpsLocation;
                }
                int idColumn = cursor.getColumnIndexOrThrow(DmsConstants.ID);
                int uuidColumn = cursor.getColumnIndexOrThrow("uuid");
                while (cursor.moveToNext()) {
                    try {
                        JSONObject sessionsAttributes = getAttributesFromSession(provider, apiKey, getSessionIdForBlobId(provider, cursor.getLong(idColumn)));
                        JSONObject jo = new JSONObject();
                        jo.put(JsonObjects.BlobHeader.Attributes.KEY_LOCALYTICS_API_KEY, apiKey);
                        jo.put("udid", cursor.getString(uuidColumn));
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_PLATFORM)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_PLATFORM, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_PLATFORM));
                        }
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MODEL)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MODEL, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MODEL));
                        }
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MANUFACTURER)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MANUFACTURER, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MANUFACTURER));
                        }
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_OS_VERSION)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_OS_VERSION, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_OS_VERSION));
                        }
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_NETWORK_CARRIER)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_NETWORK_CARRIER, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_NETWORK_CARRIER));
                        }
                        if (location != null) {
                            jo.put("lat", (double) Double.valueOf(location.getLatitude()).floatValue());
                            jo.put("lon", (double) Double.valueOf(location.getLongitude()).floatValue());
                        }
                        jo.put("j", LocalyticsSession.isDeviceRooted() ? 1 : 0);
                        jo.put("nt", LocalyticsSession.getNetworkType(context));
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_LOCALYTICS_CLIENT_LIBRARY_VERSION)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_LOCALYTICS_CLIENT_LIBRARY_VERSION, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_LOCALYTICS_CLIENT_LIBRARY_VERSION));
                        }
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_CLIENT_APP_VERSION)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_CLIENT_APP_VERSION, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_CLIENT_APP_VERSION));
                        }
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_LOCALE_LANGUAGE)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_LOCALE_LANGUAGE, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_LOCALE_LANGUAGE));
                        }
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_LOCALE_COUNTRY)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_LOCALE_COUNTRY, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_LOCALE_COUNTRY));
                        }
                        if (sessionsAttributes.has(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_COUNTRY)) {
                            jo.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_COUNTRY, sessionsAttributes.get(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_COUNTRY));
                        }
                        jo.put("dmem", LocalyticsSession.getTotalMemory());
                        JSONArray eventArray = new JSONArray();
                        jo.put(JsonObjects.EventFlow.KEY_FLOW_OLD, eventArray);
                        JSONArray tempEventsAll = new JSONArray();
                        blobEvents = null;
                        blobEvents = provider.query(LocalyticsProvider.UploadBlobEventsDbColumns.TABLE_NAME, new String[]{"events_key_ref"}, String.format("%s = ?", LocalyticsProvider.UploadBlobEventsDbColumns.UPLOAD_BLOBS_KEY_REF), new String[]{Long.toString(cursor.getLong(idColumn))}, "events_key_ref");
                        int eventIdColumn = blobEvents.getColumnIndexOrThrow("events_key_ref");
                        while (blobEvents.moveToNext()) {
                            tempEventsAll.put(convertEventToJson(provider, context, blobEvents.getLong(eventIdColumn), cursor.getLong(idColumn), apiKey));
                        }
                        if (blobEvents != null) {
                            blobEvents.close();
                        }
                        for (int i = 0; i < tempEventsAll.length(); i++) {
                            JSONObject obj = (JSONObject) tempEventsAll.get(i);
                            if (obj.has("dt") && obj.getString("dt").equals("s")) {
                                if (!jo.has("su") && obj.has("u")) {
                                    jo.put("su", obj.getString("u"));
                                }
                                if (obj.has("ct")) {
                                    jo.put("ss", obj.getLong("ct") * 1000);
                                }
                            }
                            if (obj.has("dt") && obj.getString("dt").equals(JsonObjects.EventFlow.VALUE_DATA_TYPE) && !jo.has("su") && obj.has("u")) {
                                jo.put("su", obj.getString("u"));
                            }
                            if (!obj.has(JsonObjects.EventFlow.KEY_FLOW_OLD) && !obj.has(JsonObjects.EventFlow.KEY_FLOW_NEW) && (obj.has(JsonObjects.SessionEvent.KEY_NAME) || obj.has("attrs"))) {
                                eventArray.put(obj);
                            }
                        }
                        if (jo.has("ss")) {
                            jo.put(JsonObjects.SessionClose.KEY_SESSION_LENGTH_SECONDS, Calendar.getInstance().getTimeInMillis() - jo.getLong("ss"));
                        }
                        SharedPreferences prefs = context.getSharedPreferences("session", 0);
                        long lastSessionStart = prefs.getLong("last_session_start", 0);
                        long time = Calendar.getInstance().getTimeInMillis();
                        if (lastSessionStart >= 0) {
                            jo.put("sl", (time - lastSessionStart) / 1000);
                        }
                        prefs.edit().putLong("last_session_start", time).commit();
                        result = jo;
                    } catch (JSONException e) {
                    } catch (Throwable th) {
                        if (blobEvents != null) {
                            blobEvents.close();
                        }
                        throw th;
                    }
                }
                return result;
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static void deleteBlobsAndSessions(LocalyticsProvider provider) {
            Cursor eventCursor;
            LinkedList<Long> sessionsToDelete = new LinkedList<>();
            HashSet<Long> blobsToDelete = new HashSet<>();
            Cursor blobEvents = null;
            try {
                blobEvents = provider.query(LocalyticsProvider.UploadBlobEventsDbColumns.TABLE_NAME, new String[]{DmsConstants.ID, "events_key_ref", LocalyticsProvider.UploadBlobEventsDbColumns.UPLOAD_BLOBS_KEY_REF}, null, null, null);
                int uploadBlobIdColumn = blobEvents.getColumnIndexOrThrow(LocalyticsProvider.UploadBlobEventsDbColumns.UPLOAD_BLOBS_KEY_REF);
                int blobEventIdColumn = blobEvents.getColumnIndexOrThrow(DmsConstants.ID);
                int eventIdColumn = blobEvents.getColumnIndexOrThrow("events_key_ref");
                while (blobEvents.moveToNext()) {
                    long blobId = blobEvents.getLong(uploadBlobIdColumn);
                    long blobEventId = blobEvents.getLong(blobEventIdColumn);
                    long eventId = blobEvents.getLong(eventIdColumn);
                    provider.delete(LocalyticsProvider.UploadBlobEventsDbColumns.TABLE_NAME, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(blobEventId)});
                    blobsToDelete.add(Long.valueOf(blobId));
                    provider.delete(LocalyticsProvider.AttributesDbColumns.TABLE_NAME, String.format("%s = ?", "events_key_ref"), new String[]{Long.toString(eventId)});
                    eventCursor = null;
                    Cursor eventCursor2 = provider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, new String[]{"session_key_ref"}, String.format("%s = ? AND %s = ?", DmsConstants.ID, LocalyticsProvider.EventsDbColumns.EVENT_NAME), new String[]{Long.toString(eventId), LocalyticsSession.CLOSE_EVENT}, null);
                    if (eventCursor2.moveToFirst()) {
                        long sessionId = eventCursor2.getLong(eventCursor2.getColumnIndexOrThrow("session_key_ref"));
                        provider.delete(LocalyticsProvider.EventHistoryDbColumns.TABLE_NAME, String.format("%s = ?", "session_key_ref"), new String[]{Long.toString(sessionId)});
                        sessionsToDelete.add(Long.valueOf(eventCursor2.getLong(eventCursor2.getColumnIndexOrThrow("session_key_ref"))));
                    }
                    if (eventCursor2 != null) {
                        eventCursor2.close();
                    }
                    provider.delete(LocalyticsProvider.EventsDbColumns.TABLE_NAME, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(eventId)});
                }
                if (blobEvents != null) {
                    blobEvents.close();
                }
                Iterator it = blobsToDelete.iterator();
                while (it.hasNext()) {
                    long x = ((Long) it.next()).longValue();
                    provider.delete(LocalyticsProvider.UploadBlobsDbColumns.TABLE_NAME, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(x)});
                }
                Iterator it2 = sessionsToDelete.iterator();
                while (it2.hasNext()) {
                    long x2 = ((Long) it2.next()).longValue();
                    provider.delete(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(x2)});
                }
            } catch (Throwable th) {
                if (blobEvents != null) {
                    blobEvents.close();
                }
                throw th;
            }
        }

        static long getApiKeyCreationTime(LocalyticsProvider provider, String key) {
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.ApiKeysDbColumns.TABLE_NAME, null, String.format("%s = ?", LocalyticsProvider.ApiKeysDbColumns.API_KEY), new String[]{key}, null);
                if (cursor.moveToFirst()) {
                    return (long) Math.round(((float) cursor.getLong(cursor.getColumnIndexOrThrow(LocalyticsProvider.ApiKeysDbColumns.CREATED_TIME))) / 1000.0f);
                }
                throw new RuntimeException("API key entry couldn't be found");
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static JSONObject getAttributesFromSession(LocalyticsProvider provider, String apiKey, long sessionId) throws JSONException {
            Object string;
            Object string2;
            Object string3;
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, null, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(sessionId)}, null);
                if (cursor.moveToFirst()) {
                    JSONObject result = new JSONObject();
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_CLIENT_APP_VERSION, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.APP_VERSION)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DATA_CONNECTION, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.NETWORK_TYPE)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_ANDROID_ID_HASH, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_ANDROID_ID_HASH)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_COUNTRY, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_COUNTRY)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MANUFACTURER, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_MANUFACTURER)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_MODEL, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_MODEL)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_OS_VERSION, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.ANDROID_VERSION)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_PLATFORM, JsonObjects.BlobHeader.Attributes.VALUE_PLATFORM);
                    if (cursor.isNull(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_SERIAL_NUMBER_HASH))) {
                        string = JSONObject.NULL;
                    } else {
                        string = cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_SERIAL_NUMBER_HASH));
                    }
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_SERIAL_HASH, string);
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_SDK_LEVEL, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.ANDROID_SDK)));
                    if (cursor.isNull(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_TELEPHONY_ID))) {
                        string2 = JSONObject.NULL;
                    } else {
                        string2 = cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_TELEPHONY_ID));
                    }
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_TELEPHONY_ID, string2);
                    if (cursor.isNull(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_WIFI_MAC_HASH))) {
                        string3 = JSONObject.NULL;
                    } else {
                        string3 = cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.DEVICE_WIFI_MAC_HASH));
                    }
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_DEVICE_WIFI_MAC_HASH, string3);
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_LOCALYTICS_API_KEY, apiKey);
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_LOCALYTICS_CLIENT_LIBRARY_VERSION, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.LOCALYTICS_LIBRARY_VERSION)));
                    result.put("dt", JsonObjects.BlobHeader.Attributes.VALUE_DATA_TYPE);
                    String installationID = cursor.getString(cursor.getColumnIndexOrThrow("iu"));
                    if (installationID != null) {
                        result.put("iu", installationID);
                    }
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_LOCALE_COUNTRY, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.LOCALE_COUNTRY)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_LOCALE_LANGUAGE, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.LOCALE_LANGUAGE)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_NETWORK_CARRIER, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.NETWORK_CARRIER)));
                    result.put(JsonObjects.BlobHeader.Attributes.KEY_NETWORK_COUNTRY, cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.NETWORK_COUNTRY)));
                    return result;
                }
                throw new RuntimeException("No session exists");
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static JSONObject convertEventToJson(LocalyticsProvider provider, Context context, long eventId, long blobId, String apiKey) throws JSONException {
            Cursor attributesCursor;
            Cursor eventHistoryCursor;
            String type;
            Cursor sessionCursor;
            Cursor eventHistoryCursor2;
            Cursor attributesCursor2;
            Cursor attributesCursor3;
            JSONObject result = new JSONObject();
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, null, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(eventId)}, DmsConstants.ID);
                if (cursor.moveToFirst()) {
                    String eventName = cursor.getString(cursor.getColumnIndexOrThrow(LocalyticsProvider.EventsDbColumns.EVENT_NAME));
                    long sessionId = getSessionIdForEventId(provider, eventId);
                    String sessionUuid = getSessionUuid(provider, sessionId);
                    long sessionStartTime = getSessionStartTime(provider, sessionId);
                    if (LocalyticsSession.OPEN_EVENT.equals(eventName)) {
                        result.put("dt", "s");
                        result.put("ct", Math.round(((double) cursor.getLong(cursor.getColumnIndex(LocalyticsProvider.EventsDbColumns.WALL_TIME))) / 1000.0d));
                        result.put("u", sessionUuid);
                        result.put(JsonObjects.SessionOpen.KEY_COUNT, sessionId);
                        attributesCursor3 = null;
                        attributesCursor3 = provider.query(LocalyticsProvider.AttributesDbColumns.TABLE_NAME, null, String.format("%s = ?", "events_key_ref"), new String[]{Long.toString(eventId)}, null);
                        int keyColumn = attributesCursor3.getColumnIndexOrThrow(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_KEY);
                        int valueColumn = attributesCursor3.getColumnIndexOrThrow(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_VALUE);
                        while (attributesCursor3.moveToNext()) {
                            String key = attributesCursor3.getString(keyColumn);
                            String value = attributesCursor3.getString(valueColumn);
                            if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_1.equals(key)) {
                                result.put("c0", value);
                            } else if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_2.equals(key)) {
                                result.put("c1", value);
                            } else if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_3.equals(key)) {
                                result.put("c2", value);
                            } else if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_4.equals(key)) {
                                result.put("c3", value);
                            }
                        }
                        if (attributesCursor3 != null) {
                            attributesCursor3.close();
                        }
                    } else if (LocalyticsSession.CLOSE_EVENT.equals(eventName)) {
                        result.put("dt", JsonObjects.SessionClose.VALUE_DATA_TYPE);
                        result.put("u", cursor.getString(cursor.getColumnIndexOrThrow("uuid")));
                        result.put("su", sessionUuid);
                        result.put("ss", Math.round(((double) sessionStartTime) / 1000.0d));
                        result.put("ct", Math.round(((double) cursor.getLong(cursor.getColumnIndex(LocalyticsProvider.EventsDbColumns.WALL_TIME))) / 1000.0d));
                        sessionCursor = null;
                        sessionCursor = provider.query(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, new String[]{LocalyticsProvider.SessionsDbColumns.SESSION_START_WALL_TIME}, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(cursor.getLong(cursor.getColumnIndexOrThrow("session_key_ref")))}, null);
                        if (sessionCursor.moveToFirst()) {
                            result.put(JsonObjects.SessionClose.KEY_SESSION_LENGTH_SECONDS, Math.round(((double) cursor.getLong(cursor.getColumnIndex(LocalyticsProvider.EventsDbColumns.WALL_TIME))) / 1000.0d) - Math.round(((double) sessionCursor.getLong(sessionCursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.SESSION_START_WALL_TIME))) / 1000.0d));
                            if (sessionCursor != null) {
                                sessionCursor.close();
                            }
                            eventHistoryCursor2 = null;
                            eventHistoryCursor2 = provider.query(LocalyticsProvider.EventHistoryDbColumns.TABLE_NAME, new String[]{LocalyticsProvider.EventHistoryDbColumns.NAME}, String.format("%s = ? AND %s = ?", "session_key_ref", LocalyticsProvider.EventHistoryDbColumns.TYPE), new String[]{Long.toString(sessionId), Integer.toString(1)}, DmsConstants.ID);
                            JSONArray screens = new JSONArray();
                            while (eventHistoryCursor2.moveToNext()) {
                                screens.put(eventHistoryCursor2.getString(eventHistoryCursor2.getColumnIndexOrThrow(LocalyticsProvider.EventHistoryDbColumns.NAME)));
                            }
                            if (screens.length() > 0) {
                                result.put(JsonObjects.SessionClose.KEY_FLOW_ARRAY, screens);
                            }
                            if (eventHistoryCursor2 != null) {
                                eventHistoryCursor2.close();
                            }
                            attributesCursor2 = null;
                            attributesCursor2 = provider.query(LocalyticsProvider.AttributesDbColumns.TABLE_NAME, null, String.format("%s = ?", "events_key_ref"), new String[]{Long.toString(eventId)}, null);
                            int keyColumn2 = attributesCursor2.getColumnIndexOrThrow(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_KEY);
                            int valueColumn2 = attributesCursor2.getColumnIndexOrThrow(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_VALUE);
                            while (attributesCursor2.moveToNext()) {
                                String key2 = attributesCursor2.getString(keyColumn2);
                                String value2 = attributesCursor2.getString(valueColumn2);
                                if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_1.equals(key2)) {
                                    result.put("c0", value2);
                                } else if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_2.equals(key2)) {
                                    result.put("c1", value2);
                                } else if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_3.equals(key2)) {
                                    result.put("c2", value2);
                                } else if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_4.equals(key2)) {
                                    result.put("c3", value2);
                                }
                            }
                            if (attributesCursor2 != null) {
                                attributesCursor2.close();
                            }
                        } else {
                            throw new RuntimeException("Session didn't exist");
                        }
                    } else if (LocalyticsSession.OPT_IN_EVENT.equals(eventName) || LocalyticsSession.OPT_OUT_EVENT.equals(eventName)) {
                        result.put("dt", JsonObjects.OptEvent.VALUE_DATA_TYPE);
                        result.put("u", apiKey);
                        result.put(JsonObjects.OptEvent.KEY_OPT, LocalyticsSession.OPT_OUT_EVENT.equals(eventName) ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
                        result.put("ct", Math.round(((double) cursor.getLong(cursor.getColumnIndex(LocalyticsProvider.EventsDbColumns.WALL_TIME))) / 1000.0d));
                    } else if (LocalyticsSession.FLOW_EVENT.equals(eventName)) {
                        result.put("dt", JsonObjects.EventFlow.VALUE_DATA_TYPE);
                        result.put("u", cursor.getString(cursor.getColumnIndexOrThrow("uuid")));
                        result.put("ss", Math.round(((double) sessionStartTime) / 1000.0d));
                        eventHistoryCursor = null;
                        eventHistoryCursor = provider.query(LocalyticsProvider.EventHistoryDbColumns.TABLE_NAME, new String[]{LocalyticsProvider.EventHistoryDbColumns.TYPE, LocalyticsProvider.EventHistoryDbColumns.PROCESSED_IN_BLOB, LocalyticsProvider.EventHistoryDbColumns.NAME}, String.format("%s = ? AND %s <= ?", "session_key_ref", LocalyticsProvider.EventHistoryDbColumns.PROCESSED_IN_BLOB), new String[]{Long.toString(sessionId), Long.toString(blobId)}, DmsConstants.ID);
                        JSONArray newScreens = new JSONArray();
                        JSONArray oldScreens = new JSONArray();
                        while (eventHistoryCursor.moveToNext()) {
                            String name = eventHistoryCursor.getString(eventHistoryCursor.getColumnIndexOrThrow(LocalyticsProvider.EventHistoryDbColumns.NAME));
                            if (eventHistoryCursor.getInt(eventHistoryCursor.getColumnIndexOrThrow(LocalyticsProvider.EventHistoryDbColumns.TYPE)) == 0) {
                                type = "e";
                            } else {
                                type = "s";
                            }
                            if (blobId == eventHistoryCursor.getLong(eventHistoryCursor.getColumnIndexOrThrow(LocalyticsProvider.EventHistoryDbColumns.PROCESSED_IN_BLOB))) {
                                newScreens.put(new JSONObject().put(type, name));
                            } else {
                                oldScreens.put(new JSONObject().put(type, name));
                            }
                        }
                        result.put(JsonObjects.EventFlow.KEY_FLOW_NEW, newScreens);
                        result.put(JsonObjects.EventFlow.KEY_FLOW_OLD, oldScreens);
                        if (eventHistoryCursor != null) {
                            eventHistoryCursor.close();
                        }
                    } else {
                        result.put("dt", "e");
                        result.put("ct", Math.round(((double) cursor.getLong(cursor.getColumnIndex(LocalyticsProvider.EventsDbColumns.WALL_TIME))) / 1000.0d));
                        result.put("u", cursor.getString(cursor.getColumnIndexOrThrow("uuid")));
                        result.put("su", sessionUuid);
                        result.put(JsonObjects.SessionEvent.KEY_NAME, eventName.substring(context.getPackageName().length() + 1, eventName.length()));
                        attributesCursor = null;
                        attributesCursor = provider.query(LocalyticsProvider.AttributesDbColumns.TABLE_NAME, null, String.format("%s = ?", "events_key_ref"), new String[]{Long.toString(eventId)}, null);
                        int keyColumn3 = attributesCursor.getColumnIndexOrThrow(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_KEY);
                        int valueColumn3 = attributesCursor.getColumnIndexOrThrow(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_VALUE);
                        while (attributesCursor.moveToNext()) {
                            String key3 = attributesCursor.getString(keyColumn3);
                            String value3 = attributesCursor.getString(valueColumn3);
                            if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_1.equals(key3)) {
                                result.put("c0", value3);
                            } else if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_2.equals(key3)) {
                                result.put("c1", value3);
                            } else if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_3.equals(key3)) {
                                result.put("c2", value3);
                            } else if (LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_4.equals(key3)) {
                                result.put("c3", value3);
                            }
                        }
                        if (attributesCursor != null) {
                            attributesCursor.close();
                        }
                        JSONObject attributes = convertAttributesToJson(provider, context, eventId);
                        if (attributes != null) {
                            result.put("attrs", attributes);
                        }
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return result;
                }
                throw new RuntimeException();
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }

        static long getSessionIdForEventId(LocalyticsProvider provider, long eventId) {
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, new String[]{"session_key_ref"}, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(eventId)}, null);
                if (cursor.moveToFirst()) {
                    return cursor.getLong(cursor.getColumnIndexOrThrow("session_key_ref"));
                }
                throw new RuntimeException();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static String getSessionUuid(LocalyticsProvider provider, long sessionId) {
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, new String[]{"uuid"}, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(sessionId)}, null);
                if (cursor.moveToFirst()) {
                    return cursor.getString(cursor.getColumnIndexOrThrow("uuid"));
                }
                throw new RuntimeException();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static long getSessionStartTime(LocalyticsProvider provider, long sessionId) {
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.SessionsDbColumns.TABLE_NAME, new String[]{LocalyticsProvider.SessionsDbColumns.SESSION_START_WALL_TIME}, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(sessionId)}, null);
                if (cursor.moveToFirst()) {
                    return cursor.getLong(cursor.getColumnIndexOrThrow(LocalyticsProvider.SessionsDbColumns.SESSION_START_WALL_TIME));
                }
                throw new RuntimeException();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static JSONObject convertAttributesToJson(LocalyticsProvider provider, Context context, long eventId) throws JSONException {
            JSONObject attributes = null;
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.AttributesDbColumns.TABLE_NAME, null, String.format("%s = ? AND %s != ? AND %s != ? AND %s != ? AND %s != ?", "events_key_ref", LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_KEY, LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_KEY, LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_KEY, LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_KEY), new String[]{Long.toString(eventId), LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_1, LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_2, LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_3, LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_CUSTOM_DIMENSION_4}, null);
                if (cursor.getCount() != 0) {
                    attributes = new JSONObject();
                    int keyColumn = cursor.getColumnIndexOrThrow(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_KEY);
                    int valueColumn = cursor.getColumnIndexOrThrow(LocalyticsProvider.AttributesDbColumns.ATTRIBUTE_VALUE);
                    while (cursor.moveToNext()) {
                        String key = cursor.getString(keyColumn);
                        attributes.put(key.substring(context.getPackageName().length() + 1, key.length()), cursor.getString(valueColumn));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                }
                return attributes;
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        static long getSessionIdForBlobId(LocalyticsProvider provider, long blobId) {
            Cursor cursor = null;
            try {
                cursor = provider.query(LocalyticsProvider.UploadBlobEventsDbColumns.TABLE_NAME, new String[]{"events_key_ref"}, String.format("%s = ?", LocalyticsProvider.UploadBlobEventsDbColumns.UPLOAD_BLOBS_KEY_REF), new String[]{Long.toString(blobId)}, null);
                if (cursor.moveToFirst()) {
                    long eventId = cursor.getLong(cursor.getColumnIndexOrThrow("events_key_ref"));
                    if (cursor != null) {
                        cursor.close();
                    }
                    Cursor cursor2 = null;
                    try {
                        cursor2 = provider.query(LocalyticsProvider.EventsDbColumns.TABLE_NAME, new String[]{"session_key_ref"}, String.format("%s = ?", DmsConstants.ID), new String[]{Long.toString(eventId)}, null);
                        if (cursor2.moveToFirst()) {
                            long sessionId = cursor2.getLong(cursor2.getColumnIndexOrThrow("session_key_ref"));
                            if (cursor2 != null) {
                                cursor2.close();
                            }
                            return sessionId;
                        }
                        throw new RuntimeException("No session associated with event");
                    } catch (Throwable th) {
                        throw th;
                    }
                } else {
                    throw new RuntimeException("No events associated with blob");
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    private static final class Pair<F, S> {
        public final F first;
        public final S second;

        public Pair(F first2, S second2) {
            this.first = first2;
            this.second = second2;
        }
    }

    /* access modifiers changed from: private */
    public static boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            return true;
        }
        if (location == null) {
            return false;
        }
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > 120000;
        boolean isSignificantlyOlder = timeDelta < -120000;
        boolean isNewer = timeDelta > 0;
        if (isSignificantlyNewer) {
            return true;
        }
        if (isSignificantlyOlder) {
            return false;
        }
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());
        if (isMoreAccurate) {
            return true;
        }
        if (isNewer && !isLessAccurate) {
            return true;
        }
        if (!isNewer || isSignificantlyLessAccurate || !isFromSameProvider) {
            return false;
        }
        return true;
    }

    private static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    /* access modifiers changed from: private */
    public static String getNetworkType(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null || info.getType() != 1) {
            return "3G";
        }
        return "wifi";
    }

    /* access modifiers changed from: private */
    public static boolean isDeviceRooted() {
        if (!checkRootMethod1() && !checkRootMethod2() && !checkRootMethod3()) {
            return false;
        }
        return true;
    }

    private static boolean checkRootMethod1() {
        String buildTags = Build.TAGS;
        if (buildTags == null || !buildTags.contains("test-keys")) {
            return false;
        }
        return true;
    }

    private static boolean checkRootMethod2() {
        try {
            if (new File("/system/app/Superuser.apk").exists()) {
                return true;
            }
            return false;
        } catch (Exception e) {
        }
    }

    private static boolean checkRootMethod3() {
        if (new ExecShell().executeCommand(ExecShell.SHELL_CMD.check_su_binary) != null) {
            return true;
        }
        return false;
    }

    static class ExecShell {
        ExecShell() {
        }

        public enum SHELL_CMD {
            check_su_binary(new String[]{"/system/xbin/which", "su"});
            
            String[] command;

            private SHELL_CMD(String[] command2) {
                this.command = command2;
            }
        }

        public ArrayList<String> executeCommand(SHELL_CMD shellCmd) {
            ArrayList<String> fullResponse = new ArrayList<>();
            try {
                Process localProcess = Runtime.getRuntime().exec(shellCmd.command);
                new BufferedWriter(new OutputStreamWriter(localProcess.getOutputStream()));
                BufferedReader in = new BufferedReader(new InputStreamReader(localProcess.getInputStream()));
                while (true) {
                    try {
                        String line = in.readLine();
                        if (line == null) {
                            return fullResponse;
                        }
                        fullResponse.add(line);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return fullResponse;
                    }
                }
            } catch (Exception e2) {
                return null;
            }
        }
    }

    /* access modifiers changed from: private */
    public static long getTotalMemory() {
        try {
            BufferedReader localBufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
            String str2 = localBufferedReader.readLine();
            String[] arrayOfString = str2.split("\\s+");
            int length = arrayOfString.length;
            for (int i = 0; i < length; i++) {
                Log.i(str2, String.valueOf(arrayOfString[i]) + "\t");
            }
            localBufferedReader.close();
            return (long) (Integer.valueOf(arrayOfString[1]).intValue() * 1024);
        } catch (IOException e) {
            return -1;
        }
    }
}
