package com.nth.analytics.android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;
import ch.nth.android.nthcommons.dms.api.DmsConstants;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class LocalyticsProvider {
    static final String DATABASE_FILE = "com.nth.analytics.android.%s.sqlite";
    private static final int DATABASE_VERSION = 1;
    private static final Map<String, String> sCountProjectionMap = Collections.unmodifiableMap(getCountProjectionMap());
    private static final Object[] sLocalyticsProviderIntrinsicLock = new Object[0];
    private static final Map<String, LocalyticsProvider> sLocalyticsProviderMap = new HashMap();
    private static final Set<String> sValidTables = Collections.unmodifiableSet(getValidTables());
    private final SQLiteDatabase mDb;

    public static LocalyticsProvider getInstance(Context context, String apiKey) {
        LocalyticsProvider provider;
        if (context == null) {
            throw new IllegalArgumentException("context cannot be null");
        } else if (context.getClass().getName().equals("android.test.RenamingDelegatingContext")) {
            return new LocalyticsProvider(context, apiKey);
        } else {
            synchronized (sLocalyticsProviderIntrinsicLock) {
                provider = sLocalyticsProviderMap.get(apiKey);
                if (provider == null) {
                    provider = new LocalyticsProvider(context, apiKey);
                    sLocalyticsProviderMap.put(apiKey, provider);
                }
            }
            return provider;
        }
    }

    private LocalyticsProvider(Context context, String apiKey) {
        this.mDb = new DatabaseHelper(context, String.format(DATABASE_FILE, DatapointHelper.getSha256_buggy(apiKey)), 1).getWritableDatabase();
    }

    public long insert(String tableName, ContentValues values) {
        if (!isValidTable(tableName)) {
            throw new IllegalArgumentException(String.format("tableName %s is invalid", tableName));
        } else if (values != null) {
            return this.mDb.insertOrThrow(tableName, null, values);
        } else {
            throw new IllegalArgumentException("values cannot be null");
        }
    }

    public Cursor query(String tableName, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (!isValidTable(tableName)) {
            throw new IllegalArgumentException(String.format("tableName %s is invalid", tableName));
        }
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(tableName);
        if (projection != null && 1 == projection.length && "_count".equals(projection[0])) {
            qb.setProjectionMap(sCountProjectionMap);
        }
        return qb.query(this.mDb, projection, selection, selectionArgs, null, null, sortOrder);
    }

    public int update(String tableName, ContentValues values, String selection, String[] selectionArgs) {
        if (isValidTable(tableName)) {
            return this.mDb.update(tableName, values, selection, selectionArgs);
        }
        throw new IllegalArgumentException(String.format("tableName %s is invalid", tableName));
    }

    public int delete(String tableName, String selection, String[] selectionArgs) {
        if (!isValidTable(tableName)) {
            throw new IllegalArgumentException(String.format("tableName %s is invalid", tableName));
        } else if (selection == null) {
            return this.mDb.delete(tableName, "1", null);
        } else {
            return this.mDb.delete(tableName, selection, selectionArgs);
        }
    }

    public void runBatchTransaction(Runnable runnable) {
        if (runnable == null) {
            throw new IllegalArgumentException("runnable cannot be null");
        }
        this.mDb.beginTransaction();
        try {
            runnable.run();
            this.mDb.setTransactionSuccessful();
        } finally {
            this.mDb.endTransaction();
        }
    }

    /* access modifiers changed from: package-private */
    public void close() {
        synchronized (sLocalyticsProviderIntrinsicLock) {
            String key = null;
            Iterator<Map.Entry<String, LocalyticsProvider>> it = sLocalyticsProviderMap.entrySet().iterator();
            while (true) {
                if (it.hasNext()) {
                    Map.Entry<String, LocalyticsProvider> entry = it.next();
                    if (this == entry.getValue()) {
                        key = (String) entry.getKey();
                        break;
                    }
                } else {
                    break;
                }
            }
            if (key != null) {
                sLocalyticsProviderMap.remove(key);
            }
        }
        this.mDb.close();
    }

    private static boolean isValidTable(String table) {
        if (table == null) {
            return false;
        }
        return sValidTables.contains(table);
    }

    private static Set<String> getValidTables() {
        HashSet<String> tables = new HashSet<>();
        tables.add(ApiKeysDbColumns.TABLE_NAME);
        tables.add(AttributesDbColumns.TABLE_NAME);
        tables.add(EventsDbColumns.TABLE_NAME);
        tables.add(EventHistoryDbColumns.TABLE_NAME);
        tables.add(SessionsDbColumns.TABLE_NAME);
        tables.add(UploadBlobsDbColumns.TABLE_NAME);
        tables.add(UploadBlobEventsDbColumns.TABLE_NAME);
        return tables;
    }

    private static HashMap<String, String> getCountProjectionMap() {
        HashMap<String, String> temp = new HashMap<>();
        temp.put("_count", "COUNT(*)");
        return temp;
    }

    static void deleteOldFiles(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("context cannot be null");
        }
        deleteDirectory(new File(context.getFilesDir(), "localytics"));
    }

    private static boolean deleteDirectory(File directory) {
        if (directory.exists() && directory.isDirectory()) {
            for (String child : directory.list()) {
                if (!deleteDirectory(new File(directory, child))) {
                    return false;
                }
            }
        }
        return directory.delete();
    }

    private static final class DatabaseHelper extends SQLiteOpenHelper {
        private static final String SQLITE_BOOLEAN_FALSE = "0";
        private static final String SQLITE_BOOLEAN_TRUE = "1";
        private final Context mContext;

        public DatabaseHelper(Context context, String name, int version) {
            super(context, name, (SQLiteDatabase.CursorFactory) null, version);
            this.mContext = context;
        }

        public void onCreate(SQLiteDatabase db) {
            if (db == null) {
                throw new IllegalArgumentException("db cannot be null");
            }
            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT UNIQUE NOT NULL, %s TEXT UNIQUE NOT NULL, %s INTEGER NOT NULL CHECK (%s >= 0), %s INTEGER NOT NULL CHECK(%s IN (%s, %s)));", ApiKeysDbColumns.TABLE_NAME, DmsConstants.ID, ApiKeysDbColumns.API_KEY, "uuid", ApiKeysDbColumns.CREATED_TIME, ApiKeysDbColumns.CREATED_TIME, ApiKeysDbColumns.OPT_OUT, ApiKeysDbColumns.OPT_OUT, SQLITE_BOOLEAN_FALSE, SQLITE_BOOLEAN_TRUE));
            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER REFERENCES %s(%s) NOT NULL, %s TEXT UNIQUE NOT NULL, %s INTEGER NOT NULL CHECK (%s >= 0), %s TEXT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s INTEGER NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT NOT NULL, %s TEXT NOT NULL, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT);", SessionsDbColumns.TABLE_NAME, DmsConstants.ID, SessionsDbColumns.API_KEY_REF, ApiKeysDbColumns.TABLE_NAME, DmsConstants.ID, "uuid", SessionsDbColumns.SESSION_START_WALL_TIME, SessionsDbColumns.SESSION_START_WALL_TIME, SessionsDbColumns.LOCALYTICS_LIBRARY_VERSION, "iu", SessionsDbColumns.APP_VERSION, SessionsDbColumns.ANDROID_VERSION, SessionsDbColumns.ANDROID_SDK, SessionsDbColumns.DEVICE_MODEL, SessionsDbColumns.DEVICE_MANUFACTURER, SessionsDbColumns.DEVICE_ANDROID_ID_HASH, SessionsDbColumns.DEVICE_TELEPHONY_ID, SessionsDbColumns.DEVICE_TELEPHONY_ID_HASH, SessionsDbColumns.DEVICE_SERIAL_NUMBER_HASH, SessionsDbColumns.DEVICE_WIFI_MAC_HASH, SessionsDbColumns.LOCALE_LANGUAGE, SessionsDbColumns.LOCALE_COUNTRY, SessionsDbColumns.NETWORK_CARRIER, SessionsDbColumns.NETWORK_COUNTRY, SessionsDbColumns.NETWORK_TYPE, SessionsDbColumns.DEVICE_COUNTRY, SessionsDbColumns.LATITUDE, SessionsDbColumns.LONGITUDE));
            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER REFERENCES %s(%s) NOT NULL, %s TEXT UNIQUE NOT NULL, %s TEXT NOT NULL, %s INTEGER NOT NULL CHECK (%s >= 0), %s INTEGER NOT NULL CHECK (%s >= 0));", EventsDbColumns.TABLE_NAME, DmsConstants.ID, "session_key_ref", SessionsDbColumns.TABLE_NAME, DmsConstants.ID, "uuid", EventsDbColumns.EVENT_NAME, EventsDbColumns.REAL_TIME, EventsDbColumns.REAL_TIME, EventsDbColumns.WALL_TIME, EventsDbColumns.WALL_TIME));
            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER REFERENCES %s(%s) NOT NULL, %s TEXT NOT NULL CHECK(%s IN (%s, %s)), %s TEXT NOT NULL, %s INTEGER);", EventHistoryDbColumns.TABLE_NAME, DmsConstants.ID, "session_key_ref", SessionsDbColumns.TABLE_NAME, DmsConstants.ID, EventHistoryDbColumns.TYPE, EventHistoryDbColumns.TYPE, 0, 1, EventHistoryDbColumns.NAME, EventHistoryDbColumns.PROCESSED_IN_BLOB));
            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER REFERENCES %s(%s) NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL);", AttributesDbColumns.TABLE_NAME, DmsConstants.ID, "events_key_ref", EventsDbColumns.TABLE_NAME, DmsConstants.ID, AttributesDbColumns.ATTRIBUTE_KEY, AttributesDbColumns.ATTRIBUTE_VALUE));
            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT UNIQUE NOT NULL);", UploadBlobsDbColumns.TABLE_NAME, DmsConstants.ID, "uuid"));
            db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER REFERENCES %s(%s) NOT NULL, %s INTEGER REFERENCES %s(%s) NOT NULL);", UploadBlobEventsDbColumns.TABLE_NAME, DmsConstants.ID, UploadBlobEventsDbColumns.UPLOAD_BLOBS_KEY_REF, UploadBlobsDbColumns.TABLE_NAME, DmsConstants.ID, "events_key_ref", EventsDbColumns.TABLE_NAME, DmsConstants.ID));
        }

        public void onOpen(SQLiteDatabase db) {
            super.onOpen(db);
            if (!db.isReadOnly()) {
                db.execSQL("PRAGMA foreign_keys = ON;");
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    public static final class ApiKeysDbColumns implements BaseColumns {
        public static final String API_KEY = "api_key";
        public static final String CREATED_TIME = "created_time";
        public static final String OPT_OUT = "opt_out";
        public static final String TABLE_NAME = "api_keys";
        public static final String UUID = "uuid";

        private ApiKeysDbColumns() {
            throw new UnsupportedOperationException("This class is non-instantiable");
        }
    }

    public static final class AttributesDbColumns implements BaseColumns {
        static final String ATTRIBUTE_CUSTOM_DIMENSION_1 = String.format(ATTRIBUTE_FORMAT, Constants.LOCALYTICS_PACKAGE_NAME, "custom_dimension_0");
        static final String ATTRIBUTE_CUSTOM_DIMENSION_2 = String.format(ATTRIBUTE_FORMAT, Constants.LOCALYTICS_PACKAGE_NAME, "custom_dimension_1");
        static final String ATTRIBUTE_CUSTOM_DIMENSION_3 = String.format(ATTRIBUTE_FORMAT, Constants.LOCALYTICS_PACKAGE_NAME, "custom_dimension_2");
        static final String ATTRIBUTE_CUSTOM_DIMENSION_4 = String.format(ATTRIBUTE_FORMAT, Constants.LOCALYTICS_PACKAGE_NAME, "custom_dimension_3");
        static final String ATTRIBUTE_FORMAT = "%s:%s";
        public static final String ATTRIBUTE_KEY = "attribute_key";
        public static final String ATTRIBUTE_VALUE = "attribute_value";
        public static final String EVENTS_KEY_REF = "events_key_ref";
        public static final String TABLE_NAME = "attributes";

        private AttributesDbColumns() {
            throw new UnsupportedOperationException("This class is non-instantiable");
        }
    }

    public static final class EventsDbColumns implements BaseColumns {
        public static final String EVENT_NAME = "event_name";
        public static final String REAL_TIME = "real_time";
        public static final String SESSION_KEY_REF = "session_key_ref";
        public static final String TABLE_NAME = "events";
        public static final String UUID = "uuid";
        public static final String WALL_TIME = "wall_time";

        private EventsDbColumns() {
            throw new UnsupportedOperationException("This class is non-instantiable");
        }
    }

    public static final class EventHistoryDbColumns implements BaseColumns {
        public static final String NAME = "name";
        public static final String PROCESSED_IN_BLOB = "processed_in_blob";
        public static final String SESSION_KEY_REF = "session_key_ref";
        public static final String TABLE_NAME = "event_history";
        public static final String TYPE = "type";
        public static final int TYPE_EVENT = 0;
        public static final int TYPE_SCREEN = 1;

        private EventHistoryDbColumns() {
            throw new UnsupportedOperationException("This class is non-instantiable");
        }
    }

    public static final class SessionsDbColumns implements BaseColumns {
        public static final String ANDROID_SDK = "android_sdk";
        public static final String ANDROID_VERSION = "android_version";
        public static final String API_KEY_REF = "api_key_ref";
        public static final String APP_VERSION = "app_version";
        public static final String DEVICE_ANDROID_ID_HASH = "device_android_id_hash";
        public static final String DEVICE_COUNTRY = "device_country";
        public static final String DEVICE_MANUFACTURER = "device_manufacturer";
        public static final String DEVICE_MODEL = "device_model";
        public static final String DEVICE_SERIAL_NUMBER_HASH = "device_serial_number_hash";
        public static final String DEVICE_TELEPHONY_ID = "device_telephony_id";
        public static final String DEVICE_TELEPHONY_ID_HASH = "device_telephony_id_hash";
        public static final String DEVICE_WIFI_MAC_HASH = "device_wifi_mac_hash";
        public static final String LATITUDE = "latitude";
        public static final String LOCALE_COUNTRY = "locale_country";
        public static final String LOCALE_LANGUAGE = "locale_language";
        public static final String LOCALYTICS_INSTALLATION_ID = "iu";
        public static final String LOCALYTICS_LIBRARY_VERSION = "localytics_library_version";
        public static final String LONGITUDE = "longitude";
        public static final String NETWORK_CARRIER = "network_carrier";
        public static final String NETWORK_COUNTRY = "network_country";
        public static final String NETWORK_TYPE = "network_type";
        public static final String SESSION_START_WALL_TIME = "session_start_wall_time";
        public static final String TABLE_NAME = "sessions";
        public static final String UUID = "uuid";

        private SessionsDbColumns() {
            throw new UnsupportedOperationException("This class is non-instantiable");
        }
    }

    public static final class UploadBlobEventsDbColumns implements BaseColumns {
        public static final String EVENTS_KEY_REF = "events_key_ref";
        public static final String TABLE_NAME = "upload_blob_events";
        public static final String UPLOAD_BLOBS_KEY_REF = "upload_blobs_key_ref";

        private UploadBlobEventsDbColumns() {
            throw new UnsupportedOperationException("This class is non-instantiable");
        }
    }

    public static final class UploadBlobsDbColumns implements BaseColumns {
        public static final String TABLE_NAME = "upload_blobs";
        public static final String UUID = "uuid";

        private UploadBlobsDbColumns() {
            throw new UnsupportedOperationException("This class is non-instantiable");
        }
    }
}
