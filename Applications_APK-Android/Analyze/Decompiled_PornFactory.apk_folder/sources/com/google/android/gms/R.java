package com.google.android.gms;

public final class R {

    public static final class attr {
        public static final int adSize = 2130772109;
        public static final int adSizes = 2130772110;
        public static final int adUnitId = 2130772111;
        public static final int cameraBearing = 2130772113;
        public static final int cameraTargetLat = 2130772114;
        public static final int cameraTargetLng = 2130772115;
        public static final int cameraTilt = 2130772116;
        public static final int cameraZoom = 2130772117;
        public static final int mapType = 2130772112;
        public static final int uiCompass = 2130772118;
        public static final int uiRotateGestures = 2130772119;
        public static final int uiScrollGestures = 2130772120;
        public static final int uiTiltGestures = 2130772121;
        public static final int uiZoomControls = 2130772122;
        public static final int uiZoomGestures = 2130772123;
        public static final int useViewLifecycle = 2130772124;
        public static final int zOrderOnTop = 2130772125;
    }

    public static final class color {
        public static final int common_action_bar_splitter = 2131230749;
        public static final int common_signin_btn_dark_text_default = 2131230740;
        public static final int common_signin_btn_dark_text_disabled = 2131230742;
        public static final int common_signin_btn_dark_text_focused = 2131230743;
        public static final int common_signin_btn_dark_text_pressed = 2131230741;
        public static final int common_signin_btn_default_background = 2131230748;
        public static final int common_signin_btn_light_text_default = 2131230744;
        public static final int common_signin_btn_light_text_disabled = 2131230746;
        public static final int common_signin_btn_light_text_focused = 2131230747;
        public static final int common_signin_btn_light_text_pressed = 2131230745;
        public static final int common_signin_btn_text_dark = 2131230759;
        public static final int common_signin_btn_text_light = 2131230760;
    }

    public static final class drawable {
        public static final int common_signin_btn_icon_dark = 2130837664;
        public static final int common_signin_btn_icon_disabled_dark = 2130837665;
        public static final int common_signin_btn_icon_disabled_focus_dark = 2130837666;
        public static final int common_signin_btn_icon_disabled_focus_light = 2130837667;
        public static final int common_signin_btn_icon_disabled_light = 2130837668;
        public static final int common_signin_btn_icon_focus_dark = 2130837669;
        public static final int common_signin_btn_icon_focus_light = 2130837670;
        public static final int common_signin_btn_icon_light = 2130837671;
        public static final int common_signin_btn_icon_normal_dark = 2130837672;
        public static final int common_signin_btn_icon_normal_light = 2130837673;
        public static final int common_signin_btn_icon_pressed_dark = 2130837674;
        public static final int common_signin_btn_icon_pressed_light = 2130837675;
        public static final int common_signin_btn_text_dark = 2130837676;
        public static final int common_signin_btn_text_disabled_dark = 2130837677;
        public static final int common_signin_btn_text_disabled_focus_dark = 2130837678;
        public static final int common_signin_btn_text_disabled_focus_light = 2130837679;
        public static final int common_signin_btn_text_disabled_light = 2130837680;
        public static final int common_signin_btn_text_focus_dark = 2130837681;
        public static final int common_signin_btn_text_focus_light = 2130837682;
        public static final int common_signin_btn_text_light = 2130837683;
        public static final int common_signin_btn_text_normal_dark = 2130837684;
        public static final int common_signin_btn_text_normal_light = 2130837685;
        public static final int common_signin_btn_text_pressed_dark = 2130837686;
        public static final int common_signin_btn_text_pressed_light = 2130837687;
        public static final int ic_plusone_medium_off_client = 2130837714;
        public static final int ic_plusone_small_off_client = 2130837715;
        public static final int ic_plusone_standard_off_client = 2130837716;
        public static final int ic_plusone_tall_off_client = 2130837717;
    }

    public static final class id {
        public static final int hybrid = 2131427434;
        public static final int none = 2131427429;
        public static final int normal = 2131427411;
        public static final int satellite = 2131427435;
        public static final int terrain = 2131427436;
    }

    public static final class integer {
        public static final int google_play_services_version = 2131361798;
    }

    public static final class string {
        public static final int auth_client_needs_enabling_title = 2131492898;
        public static final int auth_client_needs_installation_title = 2131492899;
        public static final int auth_client_needs_update_title = 2131492900;
        public static final int auth_client_play_services_err_notification_msg = 2131492901;
        public static final int auth_client_requested_by_msg = 2131492902;
        public static final int auth_client_using_bad_version_title = 2131492897;
        public static final int common_google_play_services_enable_button = 2131492883;
        public static final int common_google_play_services_enable_text = 2131492882;
        public static final int common_google_play_services_enable_title = 2131492881;
        public static final int common_google_play_services_install_button = 2131492880;
        public static final int common_google_play_services_install_text_phone = 2131492878;
        public static final int common_google_play_services_install_text_tablet = 2131492879;
        public static final int common_google_play_services_install_title = 2131492877;
        public static final int common_google_play_services_invalid_account_text = 2131492889;
        public static final int common_google_play_services_invalid_account_title = 2131492888;
        public static final int common_google_play_services_network_error_text = 2131492887;
        public static final int common_google_play_services_network_error_title = 2131492886;
        public static final int common_google_play_services_unknown_issue = 2131492890;
        public static final int common_google_play_services_unsupported_date_text = 2131492893;
        public static final int common_google_play_services_unsupported_text = 2131492892;
        public static final int common_google_play_services_unsupported_title = 2131492891;
        public static final int common_google_play_services_update_button = 2131492894;
        public static final int common_google_play_services_update_text = 2131492885;
        public static final int common_google_play_services_update_title = 2131492884;
        public static final int common_signin_button_text = 2131492895;
        public static final int common_signin_button_text_long = 2131492896;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {ch.nth.android.contentabo_l01_sim_univ.R.attr.adSize, ch.nth.android.contentabo_l01_sim_univ.R.attr.adSizes, ch.nth.android.contentabo_l01_sim_univ.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] MapAttrs = {ch.nth.android.contentabo_l01_sim_univ.R.attr.mapType, ch.nth.android.contentabo_l01_sim_univ.R.attr.cameraBearing, ch.nth.android.contentabo_l01_sim_univ.R.attr.cameraTargetLat, ch.nth.android.contentabo_l01_sim_univ.R.attr.cameraTargetLng, ch.nth.android.contentabo_l01_sim_univ.R.attr.cameraTilt, ch.nth.android.contentabo_l01_sim_univ.R.attr.cameraZoom, ch.nth.android.contentabo_l01_sim_univ.R.attr.uiCompass, ch.nth.android.contentabo_l01_sim_univ.R.attr.uiRotateGestures, ch.nth.android.contentabo_l01_sim_univ.R.attr.uiScrollGestures, ch.nth.android.contentabo_l01_sim_univ.R.attr.uiTiltGestures, ch.nth.android.contentabo_l01_sim_univ.R.attr.uiZoomControls, ch.nth.android.contentabo_l01_sim_univ.R.attr.uiZoomGestures, ch.nth.android.contentabo_l01_sim_univ.R.attr.useViewLifecycle, ch.nth.android.contentabo_l01_sim_univ.R.attr.zOrderOnTop};
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraTargetLat = 2;
        public static final int MapAttrs_cameraTargetLng = 3;
        public static final int MapAttrs_cameraTilt = 4;
        public static final int MapAttrs_cameraZoom = 5;
        public static final int MapAttrs_mapType = 0;
        public static final int MapAttrs_uiCompass = 6;
        public static final int MapAttrs_uiRotateGestures = 7;
        public static final int MapAttrs_uiScrollGestures = 8;
        public static final int MapAttrs_uiTiltGestures = 9;
        public static final int MapAttrs_uiZoomControls = 10;
        public static final int MapAttrs_uiZoomGestures = 11;
        public static final int MapAttrs_useViewLifecycle = 12;
        public static final int MapAttrs_zOrderOnTop = 13;
    }
}
