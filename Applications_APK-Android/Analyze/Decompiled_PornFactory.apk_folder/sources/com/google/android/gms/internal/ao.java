package com.google.android.gms.internal;

import com.nth.analytics.android.JsonObjects;
import java.util.Map;

public final class ao implements an {
    private static boolean a(Map<String, String> map) {
        return "1".equals(map.get("custom_close"));
    }

    private static int b(Map<String, String> map) {
        String str = map.get(JsonObjects.OptEvent.VALUE_DATA_TYPE);
        if (str != null) {
            if ("p".equalsIgnoreCase(str)) {
                return co.av();
            }
            if ("l".equalsIgnoreCase(str)) {
                return co.au();
            }
        }
        return -1;
    }

    public void a(cw cwVar, Map<String, String> map) {
        String str = map.get(JsonObjects.BlobHeader.Attributes.VALUE_DATA_TYPE);
        if (str == null) {
            ct.v("Action missing from an open GMSG.");
            return;
        }
        cx aC = cwVar.aC();
        if ("expand".equalsIgnoreCase(str)) {
            if (cwVar.aF()) {
                ct.v("Cannot expand WebView that is already expanded.");
            } else {
                aC.a(a(map), b(map));
            }
        } else if ("webapp".equalsIgnoreCase(str)) {
            String str2 = map.get("u");
            if (str2 != null) {
                aC.a(a(map), b(map), str2);
            } else {
                aC.a(a(map), b(map), map.get("html"), map.get("baseurl"));
            }
        } else {
            aC.a(new bj(map.get("i"), map.get("u"), map.get("m"), map.get("p"), map.get(JsonObjects.SessionClose.VALUE_DATA_TYPE), map.get(JsonObjects.EventFlow.VALUE_DATA_TYPE), map.get("e")));
        }
    }
}
