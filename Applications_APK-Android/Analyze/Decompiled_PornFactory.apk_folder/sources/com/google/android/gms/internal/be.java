package com.google.android.gms.internal;

import android.app.Activity;
import android.os.RemoteException;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.dynamic.b;
import com.google.android.gms.dynamic.c;
import com.google.android.gms.internal.bc;

public final class be<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> extends bc.a {
    private final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> gg;
    private final NETWORK_EXTRAS gh;

    public be(MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter, NETWORK_EXTRAS network_extras) {
        this.gg = mediationAdapter;
        this.gh = network_extras;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: SERVER_PARAMETERS
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private SERVER_PARAMETERS a(java.lang.String r7, int r8, java.lang.String r9) throws android.os.RemoteException {
        /*
            r6 = this;
            if (r7 == 0) goto L_0x0055
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0028 }
            r3.<init>(r7)     // Catch:{ Throwable -> 0x0028 }
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ Throwable -> 0x0028 }
            int r1 = r3.length()     // Catch:{ Throwable -> 0x0028 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x0028 }
            java.util.Iterator r4 = r3.keys()     // Catch:{ Throwable -> 0x0028 }
        L_0x0014:
            boolean r1 = r4.hasNext()     // Catch:{ Throwable -> 0x0028 }
            if (r1 == 0) goto L_0x0034
            java.lang.Object r1 = r4.next()     // Catch:{ Throwable -> 0x0028 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Throwable -> 0x0028 }
            java.lang.String r5 = r3.getString(r1)     // Catch:{ Throwable -> 0x0028 }
            r2.put(r1, r5)     // Catch:{ Throwable -> 0x0028 }
            goto L_0x0014
        L_0x0028:
            r1 = move-exception
            java.lang.String r2 = "Could not get MediationServerParameters."
            com.google.android.gms.internal.ct.b(r2, r1)
            android.os.RemoteException r1 = new android.os.RemoteException
            r1.<init>()
            throw r1
        L_0x0034:
            r3 = r2
        L_0x0035:
            com.google.ads.mediation.MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> r1 = r6.gg     // Catch:{ Throwable -> 0x0028 }
            java.lang.Class r1 = r1.getServerParametersType()     // Catch:{ Throwable -> 0x0028 }
            r2 = 0
            if (r1 == 0) goto L_0x0048
            java.lang.Object r1 = r1.newInstance()     // Catch:{ Throwable -> 0x0028 }
            com.google.ads.mediation.MediationServerParameters r1 = (com.google.ads.mediation.MediationServerParameters) r1     // Catch:{ Throwable -> 0x0028 }
            r1.load(r3)     // Catch:{ Throwable -> 0x0028 }
            r2 = r1
        L_0x0048:
            boolean r1 = r2 instanceof com.google.ads.mediation.admob.AdMobServerParameters     // Catch:{ Throwable -> 0x0028 }
            if (r1 == 0) goto L_0x0054
            r0 = r2
            com.google.ads.mediation.admob.AdMobServerParameters r0 = (com.google.ads.mediation.admob.AdMobServerParameters) r0     // Catch:{ Throwable -> 0x0028 }
            r1 = r0
            r1.adJson = r9     // Catch:{ Throwable -> 0x0028 }
            r1.tagForChildDirectedTreatment = r8     // Catch:{ Throwable -> 0x0028 }
        L_0x0054:
            return r2
        L_0x0055:
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ Throwable -> 0x0028 }
            r2 = 0
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0028 }
            r3 = r1
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.be.a(java.lang.String, int, java.lang.String):com.google.ads.mediation.MediationServerParameters");
    }

    public void a(b bVar, v vVar, String str, bd bdVar) throws RemoteException {
        a(bVar, vVar, str, (String) null, bdVar);
    }

    public void a(b bVar, v vVar, String str, String str2, bd bdVar) throws RemoteException {
        if (!(this.gg instanceof MediationInterstitialAdapter)) {
            ct.v("MediationAdapter is not a MediationInterstitialAdapter: " + this.gg.getClass().getCanonicalName());
            throw new RemoteException();
        }
        ct.r("Requesting interstitial ad from adapter.");
        try {
            ((MediationInterstitialAdapter) this.gg).requestInterstitialAd(new bf(bdVar), (Activity) c.b(bVar), a(str, vVar.tagForChildDirectedTreatment, str2), bg.e(vVar), this.gh);
        } catch (Throwable th) {
            ct.b("Could not request interstitial ad from adapter.", th);
            throw new RemoteException();
        }
    }

    public void a(b bVar, x xVar, v vVar, String str, bd bdVar) throws RemoteException {
        a(bVar, xVar, vVar, str, null, bdVar);
    }

    public void a(b bVar, x xVar, v vVar, String str, String str2, bd bdVar) throws RemoteException {
        if (!(this.gg instanceof MediationBannerAdapter)) {
            ct.v("MediationAdapter is not a MediationBannerAdapter: " + this.gg.getClass().getCanonicalName());
            throw new RemoteException();
        }
        ct.r("Requesting banner ad from adapter.");
        try {
            ((MediationBannerAdapter) this.gg).requestBannerAd(new bf(bdVar), (Activity) c.b(bVar), a(str, vVar.tagForChildDirectedTreatment, str2), bg.b(xVar), bg.e(vVar), this.gh);
        } catch (Throwable th) {
            ct.b("Could not request banner ad from adapter.", th);
            throw new RemoteException();
        }
    }

    public void destroy() throws RemoteException {
        try {
            this.gg.destroy();
        } catch (Throwable th) {
            ct.b("Could not destroy adapter.", th);
            throw new RemoteException();
        }
    }

    public b getView() throws RemoteException {
        if (!(this.gg instanceof MediationBannerAdapter)) {
            ct.v("MediationAdapter is not a MediationBannerAdapter: " + this.gg.getClass().getCanonicalName());
            throw new RemoteException();
        }
        try {
            return c.h(((MediationBannerAdapter) this.gg).getBannerView());
        } catch (Throwable th) {
            ct.b("Could not get banner view from adapter.", th);
            throw new RemoteException();
        }
    }

    public void showInterstitial() throws RemoteException {
        if (!(this.gg instanceof MediationInterstitialAdapter)) {
            ct.v("MediationAdapter is not a MediationInterstitialAdapter: " + this.gg.getClass().getCanonicalName());
            throw new RemoteException();
        }
        ct.r("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter) this.gg).showInterstitial();
        } catch (Throwable th) {
            ct.b("Could not show interstitial from adapter.", th);
            throw new RemoteException();
        }
    }
}
