package com.google.android.gms.dynamic;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.dynamic.LifecycleDelegate;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class a<T extends LifecycleDelegate> {
    /* access modifiers changed from: private */
    public T ss;
    /* access modifiers changed from: private */
    public Bundle st;
    /* access modifiers changed from: private */
    public LinkedList<C0007a> su;
    private final d<T> sv = new d<T>() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.dynamic.a.a(com.google.android.gms.dynamic.a, com.google.android.gms.dynamic.LifecycleDelegate):com.google.android.gms.dynamic.LifecycleDelegate
         arg types: [com.google.android.gms.dynamic.a, T]
         candidates:
          com.google.android.gms.dynamic.a.a(com.google.android.gms.dynamic.a, android.os.Bundle):android.os.Bundle
          com.google.android.gms.dynamic.a.a(android.os.Bundle, com.google.android.gms.dynamic.a$a):void
          com.google.android.gms.dynamic.a.a(com.google.android.gms.dynamic.a, com.google.android.gms.dynamic.LifecycleDelegate):com.google.android.gms.dynamic.LifecycleDelegate */
        public void a(T t) {
            LifecycleDelegate unused = a.this.ss = (LifecycleDelegate) t;
            Iterator it = a.this.su.iterator();
            while (it.hasNext()) {
                ((C0007a) it.next()).b(a.this.ss);
            }
            a.this.su.clear();
            Bundle unused2 = a.this.st = (Bundle) null;
        }
    };

    /* renamed from: com.google.android.gms.dynamic.a$a  reason: collision with other inner class name */
    private interface C0007a {
        void b(LifecycleDelegate lifecycleDelegate);

        int getState();
    }

    private void a(Bundle bundle, C0007a aVar) {
        if (this.ss != null) {
            aVar.b(this.ss);
            return;
        }
        if (this.su == null) {
            this.su = new LinkedList<>();
        }
        this.su.add(aVar);
        if (bundle != null) {
            if (this.st == null) {
                this.st = (Bundle) bundle.clone();
            } else {
                this.st.putAll(bundle);
            }
        }
        a(this.sv);
    }

    private void ay(int i) {
        while (!this.su.isEmpty() && this.su.getLast().getState() >= i) {
            this.su.removeLast();
        }
    }

    public void a(FrameLayout frameLayout) {
        final Context context = frameLayout.getContext();
        final int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        String b = GooglePlayServicesUtil.b(context, isGooglePlayServicesAvailable, -1);
        String b2 = GooglePlayServicesUtil.b(context, isGooglePlayServicesAvailable);
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        TextView textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        textView.setText(b);
        linearLayout.addView(textView);
        if (b2 != null) {
            Button button = new Button(context);
            button.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
            button.setText(b2);
            linearLayout.addView(button);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    context.startActivity(GooglePlayServicesUtil.a(context, isGooglePlayServicesAvailable, -1));
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(d<T> dVar);

    public T cZ() {
        return this.ss;
    }

    public void onCreate(final Bundle savedInstanceState) {
        a(savedInstanceState, new C0007a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.ss.onCreate(savedInstanceState);
            }

            public int getState() {
                return 1;
            }
        });
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final FrameLayout frameLayout = new FrameLayout(inflater.getContext());
        final LayoutInflater layoutInflater = inflater;
        final ViewGroup viewGroup = container;
        final Bundle bundle = savedInstanceState;
        a(savedInstanceState, new C0007a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                frameLayout.removeAllViews();
                frameLayout.addView(a.this.ss.onCreateView(layoutInflater, viewGroup, bundle));
            }

            public int getState() {
                return 2;
            }
        });
        if (this.ss == null) {
            a(frameLayout);
        }
        return frameLayout;
    }

    public void onDestroy() {
        if (this.ss != null) {
            this.ss.onDestroy();
        } else {
            ay(1);
        }
    }

    public void onDestroyView() {
        if (this.ss != null) {
            this.ss.onDestroyView();
        } else {
            ay(2);
        }
    }

    public void onInflate(final Activity activity, final Bundle attrs, final Bundle savedInstanceState) {
        a(savedInstanceState, new C0007a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.ss.onInflate(activity, attrs, savedInstanceState);
            }

            public int getState() {
                return 0;
            }
        });
    }

    public void onLowMemory() {
        if (this.ss != null) {
            this.ss.onLowMemory();
        }
    }

    public void onPause() {
        if (this.ss != null) {
            this.ss.onPause();
        } else {
            ay(3);
        }
    }

    public void onResume() {
        a((Bundle) null, new C0007a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.ss.onResume();
            }

            public int getState() {
                return 3;
            }
        });
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onSaveInstanceState(android.os.Bundle r2) {
        /*
            r1 = this;
            T r0 = r1.ss
            if (r0 == 0) goto L_0x000a
            T r0 = r1.ss
            r0.onSaveInstanceState(r2)
        L_0x0009:
            return
        L_0x000a:
            android.os.Bundle r0 = r1.st
            if (r0 == 0) goto L_0x0009
            android.os.Bundle r0 = r1.st
            r2.putAll(r0)
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamic.a.onSaveInstanceState(android.os.Bundle):void");
    }
}
