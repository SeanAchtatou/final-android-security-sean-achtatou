package com.google.android.gms.internal;

import java.io.IOException;

public abstract class iz {
    protected int rw = -1;

    public static final <T extends iz> T a(T t, byte[] bArr) throws iy {
        return b(t, bArr, 0, bArr.length);
    }

    public static final void a(iz izVar, byte[] bArr, int i, int i2) {
        try {
            ix b = ix.b(bArr, i, i2);
            izVar.a(b);
            b.gf();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public static final byte[] a(iz izVar) {
        byte[] bArr = new byte[izVar.cP()];
        a(izVar, bArr, 0, bArr.length);
        return bArr;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends com.google.android.gms.internal.iz> T b(T r2, byte[] r3, int r4, int r5) throws com.google.android.gms.internal.iy {
        /*
            com.google.android.gms.internal.iw r0 = com.google.android.gms.internal.iw.a(r3, r4, r5)     // Catch:{ iy -> 0x000c, IOException -> 0x000e }
            r2.b(r0)     // Catch:{ iy -> 0x000c, IOException -> 0x000e }
            r1 = 0
            r0.bI(r1)     // Catch:{ iy -> 0x000c, IOException -> 0x000e }
            return r2
        L_0x000c:
            r0 = move-exception
            throw r0
        L_0x000e:
            r0 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Reading from a byte array threw an IOException (should never happen)."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.iz.b(com.google.android.gms.internal.iz, byte[], int, int):com.google.android.gms.internal.iz");
    }

    public abstract void a(ix ixVar) throws IOException;

    public abstract iz b(iw iwVar) throws IOException;

    public int cP() {
        this.rw = 0;
        return 0;
    }

    public String toString() {
        return ja.b(this);
    }
}
