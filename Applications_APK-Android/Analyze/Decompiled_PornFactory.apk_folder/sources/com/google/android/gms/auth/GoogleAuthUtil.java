package com.google.android.gms.auth;

import android.accounts.AccountManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.a;
import com.google.android.gms.internal.eg;
import com.google.android.gms.internal.o;
import java.io.IOException;
import java.net.URISyntaxException;

public final class GoogleAuthUtil {
    public static final String GOOGLE_ACCOUNT_TYPE = "com.google";
    public static final String KEY_ANDROID_PACKAGE_NAME = (Build.VERSION.SDK_INT >= 14 ? "androidPackageName" : "androidPackageName");
    public static final String KEY_CALLER_UID = (Build.VERSION.SDK_INT >= 11 ? "callerUid" : "callerUid");
    public static final String KEY_CLIENT_PACKAGE_NAME = "clientPackageName";
    public static final String KEY_REQUEST_ACTIONS = "request_visible_actions";
    @Deprecated
    public static final String KEY_REQUEST_VISIBLE_ACTIVITIES = "request_visible_actions";
    public static final String KEY_SUPPRESS_PROGRESS_SCREEN = "suppressProgressScreen";
    public static final String OEM_ONLY_KEY_TARGET_ANDROID_ID = "oauth2_target_device_id";
    public static final String OEM_ONLY_KEY_VERIFIER = "oauth2_authcode_verifier";
    public static final String OEM_ONLY_SCOPE_ACCOUNT_BOOTSTRAP = "_account_setup";
    private static final ComponentName kb = new ComponentName(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, "com.google.android.gms.auth.GetToken");
    private static final ComponentName kc = new ComponentName(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, "com.google.android.gms.recovery.RecoveryService");
    private static final Intent kd = new Intent().setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE).setComponent(kb);
    private static final Intent ke = new Intent().setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE).setComponent(kc);

    private GoogleAuthUtil() {
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003e  */
    private static java.lang.String a(android.content.Context r9, java.lang.String r10, java.lang.String r11, android.os.Bundle r12) throws java.io.IOException, com.google.android.gms.auth.UserRecoverableNotifiedException, com.google.android.gms.auth.GoogleAuthException {
        /*
            r8 = 0
            if (r12 != 0) goto L_0x0008
            android.os.Bundle r12 = new android.os.Bundle
            r12.<init>()
        L_0x0008:
            java.lang.String r0 = getToken(r9, r10, r11, r12)     // Catch:{ GooglePlayServicesAvailabilityException -> 0x000d, UserRecoverableAuthException -> 0x009a }
            return r0
        L_0x000d:
            r2 = move-exception
            int r0 = r2.getConnectionStatusCode()
            android.app.PendingIntent r3 = com.google.android.gms.common.GooglePlayServicesUtil.getErrorPendingIntent(r0, r9, r8)
            android.content.res.Resources r4 = r9.getResources()
            int r0 = com.google.android.gms.R.string.auth_client_play_services_err_notification_msg
            java.lang.String r0 = r4.getString(r0)
            android.app.Notification r5 = new android.app.Notification
            r1 = 17301642(0x108008a, float:2.4979642E-38)
            long r6 = java.lang.System.currentTimeMillis()
            r5.<init>(r1, r0, r6)
            int r0 = r5.flags
            r0 = r0 | 16
            r5.flags = r0
            android.content.pm.ApplicationInfo r0 = r9.getApplicationInfo()
            java.lang.String r0 = r0.name
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x005d
            java.lang.String r0 = r9.getPackageName()
            android.content.Context r1 = r9.getApplicationContext()
            android.content.pm.PackageManager r6 = r1.getPackageManager()
            java.lang.String r1 = r9.getPackageName()     // Catch:{ NameNotFoundException -> 0x008e }
            r7 = 0
            android.content.pm.ApplicationInfo r1 = r6.getApplicationInfo(r1, r7)     // Catch:{ NameNotFoundException -> 0x008e }
        L_0x0053:
            if (r1 == 0) goto L_0x005d
            java.lang.CharSequence r0 = r6.getApplicationLabel(r1)
            java.lang.String r0 = r0.toString()
        L_0x005d:
            int r1 = com.google.android.gms.R.string.auth_client_requested_by_msg
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r6[r8] = r0
            java.lang.String r1 = r4.getString(r1, r6)
            int r0 = r2.getConnectionStatusCode()
            switch(r0) {
                case 1: goto L_0x0091;
                case 2: goto L_0x0094;
                case 3: goto L_0x0097;
                default: goto L_0x006f;
            }
        L_0x006f:
            int r0 = com.google.android.gms.R.string.auth_client_using_bad_version_title
        L_0x0071:
            java.lang.String r0 = r4.getString(r0)
            r5.setLatestEventInfo(r9, r0, r1, r3)
            java.lang.String r0 = "notification"
            java.lang.Object r0 = r9.getSystemService(r0)
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0
            r1 = 39789(0x9b6d, float:5.5756E-41)
            r0.notify(r1, r5)
            com.google.android.gms.auth.UserRecoverableNotifiedException r0 = new com.google.android.gms.auth.UserRecoverableNotifiedException
            java.lang.String r1 = "User intervention required. Notification has been pushed."
            r0.<init>(r1)
            throw r0
        L_0x008e:
            r1 = move-exception
            r1 = 0
            goto L_0x0053
        L_0x0091:
            int r0 = com.google.android.gms.R.string.auth_client_needs_installation_title
            goto L_0x0071
        L_0x0094:
            int r0 = com.google.android.gms.R.string.auth_client_needs_update_title
            goto L_0x0071
        L_0x0097:
            int r0 = com.google.android.gms.R.string.auth_client_needs_enabling_title
            goto L_0x0071
        L_0x009a:
            r0 = move-exception
            com.google.android.gms.auth.UserRecoverableNotifiedException r0 = new com.google.android.gms.auth.UserRecoverableNotifiedException
            java.lang.String r1 = "User intervention required. Notification has been pushed."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.auth.GoogleAuthUtil.a(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle):java.lang.String");
    }

    private static void b(Intent intent) {
        if (intent == null) {
            throw new IllegalArgumentException("Callack cannot be null.");
        }
        try {
            Intent.parseUri(intent.toUri(1), 1);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Parameter callback contains invalid data. It must be serializable using toUri() and parseUri().");
        }
    }

    public static String getToken(Context context, String accountName, String scope) throws IOException, UserRecoverableAuthException, GoogleAuthException {
        return getToken(context, accountName, scope, new Bundle());
    }

    public static String getToken(Context context, String accountName, String scope, Bundle extras) throws IOException, UserRecoverableAuthException, GoogleAuthException {
        Context applicationContext = context.getApplicationContext();
        eg.O("Calling this from your main thread can lead to deadlock");
        m(applicationContext);
        Bundle extras2 = extras == null ? new Bundle() : new Bundle(extras);
        String str = context.getApplicationInfo().packageName;
        extras2.putString(KEY_CLIENT_PACKAGE_NAME, str);
        if (!extras2.containsKey(KEY_ANDROID_PACKAGE_NAME)) {
            extras2.putString(KEY_ANDROID_PACKAGE_NAME, str);
        }
        a aVar = new a();
        if (applicationContext.bindService(kd, aVar, 1)) {
            try {
                Bundle a = o.a.a(aVar.bg()).a(accountName, scope, extras2);
                String string = a.getString("authtoken");
                if (!TextUtils.isEmpty(string)) {
                    applicationContext.unbindService(aVar);
                    return string;
                }
                String string2 = a.getString("Error");
                Intent intent = (Intent) a.getParcelable("userRecoveryIntent");
                if (x(string2)) {
                    throw new UserRecoverableAuthException(string2, intent);
                } else if (w(string2)) {
                    throw new IOException(string2);
                } else {
                    throw new GoogleAuthException(string2);
                }
            } catch (RemoteException e) {
                Log.i("GoogleAuthUtil", "GMS remote exception ", e);
                throw new IOException("remote exception");
            } catch (InterruptedException e2) {
                throw new GoogleAuthException("Interrupted");
            } catch (Throwable th) {
                applicationContext.unbindService(aVar);
                throw th;
            }
        } else {
            throw new IOException("Could not bind to service with the given context.");
        }
    }

    public static String getTokenWithNotification(Context context, String accountName, String scope, Bundle extras) throws IOException, UserRecoverableNotifiedException, GoogleAuthException {
        if (extras == null) {
            extras = new Bundle();
        }
        extras.putBoolean("handle_notification", true);
        return a(context, accountName, scope, extras);
    }

    public static String getTokenWithNotification(Context context, String accountName, String scope, Bundle extras, Intent callback) throws IOException, UserRecoverableNotifiedException, GoogleAuthException {
        b(callback);
        if (extras == null) {
            extras = new Bundle();
        }
        extras.putParcelable("callback_intent", callback);
        extras.putBoolean("handle_notification", true);
        return a(context, accountName, scope, extras);
    }

    public static String getTokenWithNotification(Context context, String accountName, String scope, Bundle extras, String authority, Bundle syncBundle) throws IOException, UserRecoverableNotifiedException, GoogleAuthException {
        if (TextUtils.isEmpty(authority)) {
            throw new IllegalArgumentException("Authority cannot be empty or null.");
        }
        if (extras == null) {
            extras = new Bundle();
        }
        if (syncBundle == null) {
            syncBundle = new Bundle();
        }
        ContentResolver.validateSyncExtrasBundle(syncBundle);
        extras.putString("authority", authority);
        extras.putBundle("sync_extras", syncBundle);
        extras.putBoolean("handle_notification", true);
        return a(context, accountName, scope, extras);
    }

    public static void invalidateToken(Context context, String token) {
        AccountManager.get(context).invalidateAuthToken(GOOGLE_ACCOUNT_TYPE, token);
    }

    private static void m(Context context) throws GooglePlayServicesAvailabilityException, GoogleAuthException {
        try {
            GooglePlayServicesUtil.m(context);
        } catch (GooglePlayServicesRepairableException e) {
            throw new GooglePlayServicesAvailabilityException(e.getConnectionStatusCode(), e.getMessage(), e.getIntent());
        } catch (GooglePlayServicesNotAvailableException e2) {
            throw new GoogleAuthException(e2.getMessage());
        }
    }

    private static boolean w(String str) {
        return "NetworkError".equals(str) || "ServiceUnavailable".equals(str) || "Timeout".equals(str);
    }

    private static boolean x(String str) {
        return "BadAuthentication".equals(str) || "CaptchaRequired".equals(str) || "DeviceManagementRequiredOrSyncDisabled".equals(str) || "NeedPermission".equals(str) || "NeedsBrowser".equals(str) || "UserCancel".equals(str) || "AppDownloadRequired".equals(str);
    }
}
