package com.google.android.gms.internal;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.android.gms.internal.bu;
import com.google.android.gms.internal.bw;
import com.google.android.gms.internal.bz;
import com.google.android.gms.internal.cx;
import org.json.JSONException;

public final class bv extends cm implements bw.a, cx.a {
    private final bb ed;
    /* access modifiers changed from: private */
    public final Object fx = new Object();
    private au fy;
    /* access modifiers changed from: private */
    public final cw gv;
    /* access modifiers changed from: private */
    public final bu.a hb;
    private final Object hc = new Object();
    private final bz.a hd;
    private final h he;
    private cm hf;
    /* access modifiers changed from: private */
    public cb hg;
    private boolean hh = false;
    private as hi;
    private ay hj;
    private final Context mContext;

    private static final class a extends Exception {
        private final int hm;

        public a(String str, int i) {
            super(str);
            this.hm = i;
        }

        public int getErrorCode() {
            return this.hm;
        }
    }

    public bv(Context context, bz.a aVar, h hVar, cw cwVar, bb bbVar, bu.a aVar2) {
        this.ed = bbVar;
        this.hb = aVar2;
        this.gv = cwVar;
        this.mContext = context;
        this.hd = aVar;
        this.he = hVar;
    }

    private x a(bz bzVar) throws a {
        if (this.hg.hB == null) {
            throw new a("The ad response must specify one of the supported ad sizes.", 0);
        }
        String[] split = this.hg.hB.split("x");
        if (split.length != 2) {
            throw new a("Could not parse the ad size from the ad response: " + this.hg.hB, 0);
        }
        try {
            int parseInt = Integer.parseInt(split[0]);
            int parseInt2 = Integer.parseInt(split[1]);
            for (x xVar : bzVar.em.eH) {
                float f = this.mContext.getResources().getDisplayMetrics().density;
                int i = xVar.width == -1 ? (int) (((float) xVar.widthPixels) / f) : xVar.width;
                int i2 = xVar.height == -2 ? (int) (((float) xVar.heightPixels) / f) : xVar.height;
                if (parseInt == i && parseInt2 == i2) {
                    return new x(xVar, bzVar.em.eH);
                }
            }
            throw new a("The ad size from the ad response was not one of the requested sizes: " + this.hg.hB, 0);
        } catch (NumberFormatException e) {
            throw new a("Could not parse the ad size from the ad response: " + this.hg.hB, 0);
        }
    }

    private void a(bz bzVar, long j) throws a {
        synchronized (this.hc) {
            this.hi = new as(this.mContext, bzVar, this.ed, this.fy);
        }
        this.hj = this.hi.a(j, 60000);
        switch (this.hj.ga) {
            case 0:
                break;
            case 1:
                throw new a("No fill from any mediation ad networks.", 3);
            default:
                throw new a("Unexpected mediation result: " + this.hj.ga, 0);
        }
    }

    private void aj() throws a {
        if (this.hg.errorCode != -3) {
            if (TextUtils.isEmpty(this.hg.hw)) {
                throw new a("No fill from ad server.", 3);
            } else if (this.hg.hy) {
                try {
                    this.fy = new au(this.hg.hw);
                } catch (JSONException e) {
                    throw new a("Could not parse mediation config: " + this.hg.hw, 0);
                }
            }
        }
    }

    private void b(long j) throws a {
        cs.iI.post(new Runnable() {
            /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
                return;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r7 = this;
                    com.google.android.gms.internal.bv r0 = com.google.android.gms.internal.bv.this
                    java.lang.Object r6 = r0.fx
                    monitor-enter(r6)
                    com.google.android.gms.internal.bv r0 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cb r0 = r0.hg     // Catch:{ all -> 0x005f }
                    int r0 = r0.errorCode     // Catch:{ all -> 0x005f }
                    r1 = -2
                    if (r0 == r1) goto L_0x0014
                    monitor-exit(r6)     // Catch:{ all -> 0x005f }
                L_0x0013:
                    return
                L_0x0014:
                    com.google.android.gms.internal.bv r0 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cw r0 = r0.gv     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cx r0 = r0.aC()     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.bv r1 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    r0.a(r1)     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.bv r0 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cb r0 = r0.hg     // Catch:{ all -> 0x005f }
                    int r0 = r0.errorCode     // Catch:{ all -> 0x005f }
                    r1 = -3
                    if (r0 != r1) goto L_0x0062
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x005f }
                    r0.<init>()     // Catch:{ all -> 0x005f }
                    java.lang.String r1 = "Loading URL in WebView: "
                    java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.bv r1 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cb r1 = r1.hg     // Catch:{ all -> 0x005f }
                    java.lang.String r1 = r1.gL     // Catch:{ all -> 0x005f }
                    java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x005f }
                    java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.ct.u(r0)     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.bv r0 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cw r0 = r0.gv     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.bv r1 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cb r1 = r1.hg     // Catch:{ all -> 0x005f }
                    java.lang.String r1 = r1.gL     // Catch:{ all -> 0x005f }
                    r0.loadUrl(r1)     // Catch:{ all -> 0x005f }
                L_0x005d:
                    monitor-exit(r6)     // Catch:{ all -> 0x005f }
                    goto L_0x0013
                L_0x005f:
                    r0 = move-exception
                    monitor-exit(r6)     // Catch:{ all -> 0x005f }
                    throw r0
                L_0x0062:
                    java.lang.String r0 = "Loading HTML in WebView."
                    com.google.android.gms.internal.ct.u(r0)     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.bv r0 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cw r0 = r0.gv     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.bv r1 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cb r1 = r1.hg     // Catch:{ all -> 0x005f }
                    java.lang.String r1 = r1.gL     // Catch:{ all -> 0x005f }
                    java.lang.String r1 = com.google.android.gms.internal.co.o(r1)     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.bv r2 = com.google.android.gms.internal.bv.this     // Catch:{ all -> 0x005f }
                    com.google.android.gms.internal.cb r2 = r2.hg     // Catch:{ all -> 0x005f }
                    java.lang.String r2 = r2.hw     // Catch:{ all -> 0x005f }
                    java.lang.String r3 = "text/html"
                    java.lang.String r4 = "UTF-8"
                    r5 = 0
                    r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x005f }
                    goto L_0x005d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.bv.AnonymousClass3.run():void");
            }
        });
        d(j);
    }

    private void c(long j) throws a {
        while (e(j)) {
            if (this.hg != null) {
                synchronized (this.hc) {
                    this.hf = null;
                }
                if (this.hg.errorCode != -2 && this.hg.errorCode != -3) {
                    throw new a("There was a problem getting an ad response. ErrorCode: " + this.hg.errorCode, this.hg.errorCode);
                }
                return;
            }
        }
        throw new a("Timed out waiting for ad response.", 2);
    }

    private void d(long j) throws a {
        while (e(j)) {
            if (this.hh) {
                return;
            }
        }
        throw new a("Timed out waiting for WebView to finish loading.", 2);
    }

    private boolean e(long j) throws a {
        long elapsedRealtime = 60000 - (SystemClock.elapsedRealtime() - j);
        if (elapsedRealtime <= 0) {
            return false;
        }
        try {
            this.fx.wait(elapsedRealtime);
            return true;
        } catch (InterruptedException e) {
            throw new a("Ad request cancelled.", -1);
        }
    }

    public void a(cb cbVar) {
        synchronized (this.fx) {
            ct.r("Received ad response.");
            this.hg = cbVar;
            this.fx.notify();
        }
    }

    public void a(cw cwVar) {
        synchronized (this.fx) {
            ct.r("WebView finished loading.");
            this.hh = true;
            this.fx.notify();
        }
    }

    public void ai() {
        x xVar;
        synchronized (this.fx) {
            ct.r("AdLoaderBackgroundTask started.");
            bz bzVar = new bz(this.hd, this.he.g().a(this.mContext));
            x xVar2 = null;
            int i = -2;
            try {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                cm a2 = bw.a(this.mContext, bzVar, this);
                synchronized (this.hc) {
                    this.hf = a2;
                    if (this.hf == null) {
                        throw new a("Could not start the ad request service.", 0);
                    }
                }
                c(elapsedRealtime);
                aj();
                if (bzVar.em.eH != null) {
                    xVar2 = a(bzVar);
                }
                if (this.hg.hy) {
                    a(bzVar, elapsedRealtime);
                } else {
                    b(elapsedRealtime);
                }
                xVar = xVar2;
            } catch (a e) {
                i = e.getErrorCode();
                if (i == 3 || i == -1) {
                    ct.t(e.getMessage());
                } else {
                    ct.v(e.getMessage());
                }
                this.hg = new cb(i);
                cs.iI.post(new Runnable() {
                    public void run() {
                        bv.this.onStop();
                    }
                });
                xVar = null;
            }
            final cj cjVar = new cj(bzVar.hr, this.gv, this.hg.fK, i, this.hg.fL, this.hg.hA, this.hg.orientation, this.hg.fO, bzVar.hu, this.hg.hy, this.hj != null ? this.hj.gb : null, this.hj != null ? this.hj.gc : null, this.hj != null ? this.hj.gd : null, this.fy, this.hj != null ? this.hj.ge : null, this.hg.hz, xVar, this.hg.hx);
            cs.iI.post(new Runnable() {
                public void run() {
                    synchronized (bv.this.fx) {
                        bv.this.hb.a(cjVar);
                    }
                }
            });
        }
    }

    public void onStop() {
        synchronized (this.hc) {
            if (this.hf != null) {
                this.hf.cancel();
            }
            this.gv.stopLoading();
            co.a(this.gv);
            if (this.hi != null) {
                this.hi.cancel();
            }
        }
    }
}
