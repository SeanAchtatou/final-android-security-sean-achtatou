package com.google.ads.mediation.jsadapter;

import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.internal.ct;
import java.net.URI;
import java.net.URISyntaxException;

public final class BannerWebViewClient extends WebViewClient {
    private final String A;
    private boolean B = false;
    private final JavascriptAdapter r;

    public BannerWebViewClient(JavascriptAdapter adapter, String passbackUrl) {
        this.A = c(passbackUrl);
        this.r = adapter;
    }

    private boolean b(String str) {
        String c = c(str);
        if (TextUtils.isEmpty(c)) {
            return false;
        }
        try {
            URI uri = new URI(c);
            if ("passback".equals(uri.getScheme())) {
                ct.r("Passback received");
                this.r.sendAdNotReceivedUpdate();
                return true;
            } else if (TextUtils.isEmpty(this.A)) {
                return false;
            } else {
                URI uri2 = new URI(this.A);
                String host = uri2.getHost();
                String host2 = uri.getHost();
                String path = uri2.getPath();
                String path2 = uri.getPath();
                if (!equals(host, host2) || !equals(path, path2)) {
                    return false;
                }
                ct.r("Passback received");
                this.r.sendAdNotReceivedUpdate();
                return true;
            }
        } catch (URISyntaxException e) {
            ct.s(e.getMessage());
            return false;
        }
    }

    private String c(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        try {
            return str.endsWith("/") ? str.substring(0, str.length() - 1) : str;
        } catch (IndexOutOfBoundsException e) {
            ct.s(e.getMessage());
            return str;
        }
    }

    private static boolean equals(Object obj1, Object obj2) {
        return obj1 == obj2 || (obj1 != null && obj1.equals(obj2));
    }

    public void onLoadResource(WebView view, String url) {
        ct.u("onLoadResource: " + url);
        if (!b(url)) {
            super.onLoadResource(view, url);
        }
    }

    public void onPageFinished(WebView view, String url) {
        ct.u("onPageFinished: " + url);
        super.onPageFinished(view, url);
        if (!this.B) {
            this.r.startCheckingForAd();
            this.B = true;
        }
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        ct.u("shouldOverrideUrlLoading: " + url);
        if (!b(url)) {
            return false;
        }
        ct.r("shouldOverrideUrlLoading: received passback url");
        return true;
    }
}
