package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.SpinnerAdapter;

public class ActionBarImplJB extends ActionBarImplICS {
    public /* bridge */ /* synthetic */ void addOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener onMenuVisibilityListener) {
        super.addOnMenuVisibilityListener(onMenuVisibilityListener);
    }

    public /* bridge */ /* synthetic */ void addTab(ActionBar.Tab tab) {
        super.addTab(tab);
    }

    public /* bridge */ /* synthetic */ void addTab(ActionBar.Tab tab, int i) {
        super.addTab(tab, i);
    }

    public /* bridge */ /* synthetic */ void addTab(ActionBar.Tab tab, int i, boolean z) {
        super.addTab(tab, i, z);
    }

    public /* bridge */ /* synthetic */ void addTab(ActionBar.Tab tab, boolean z) {
        super.addTab(tab, z);
    }

    public /* bridge */ /* synthetic */ View getCustomView() {
        return super.getCustomView();
    }

    public /* bridge */ /* synthetic */ int getDisplayOptions() {
        return super.getDisplayOptions();
    }

    public /* bridge */ /* synthetic */ int getHeight() {
        return super.getHeight();
    }

    public /* bridge */ /* synthetic */ int getNavigationItemCount() {
        return super.getNavigationItemCount();
    }

    public /* bridge */ /* synthetic */ int getNavigationMode() {
        return super.getNavigationMode();
    }

    public /* bridge */ /* synthetic */ int getSelectedNavigationIndex() {
        return super.getSelectedNavigationIndex();
    }

    public /* bridge */ /* synthetic */ ActionBar.Tab getSelectedTab() {
        return super.getSelectedTab();
    }

    public /* bridge */ /* synthetic */ CharSequence getSubtitle() {
        return super.getSubtitle();
    }

    public /* bridge */ /* synthetic */ ActionBar.Tab getTabAt(int i) {
        return super.getTabAt(i);
    }

    public /* bridge */ /* synthetic */ int getTabCount() {
        return super.getTabCount();
    }

    public /* bridge */ /* synthetic */ Context getThemedContext() {
        return super.getThemedContext();
    }

    public /* bridge */ /* synthetic */ CharSequence getTitle() {
        return super.getTitle();
    }

    public /* bridge */ /* synthetic */ void hide() {
        super.hide();
    }

    public /* bridge */ /* synthetic */ boolean isShowing() {
        return super.isShowing();
    }

    public /* bridge */ /* synthetic */ ActionBar.Tab newTab() {
        return super.newTab();
    }

    public /* bridge */ /* synthetic */ void removeAllTabs() {
        super.removeAllTabs();
    }

    public /* bridge */ /* synthetic */ void removeOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener onMenuVisibilityListener) {
        super.removeOnMenuVisibilityListener(onMenuVisibilityListener);
    }

    public /* bridge */ /* synthetic */ void removeTab(ActionBar.Tab tab) {
        super.removeTab(tab);
    }

    public /* bridge */ /* synthetic */ void removeTabAt(int i) {
        super.removeTabAt(i);
    }

    public /* bridge */ /* synthetic */ void selectTab(ActionBar.Tab tab) {
        super.selectTab(tab);
    }

    public /* bridge */ /* synthetic */ void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
    }

    public /* bridge */ /* synthetic */ void setCustomView(int i) {
        super.setCustomView(i);
    }

    public /* bridge */ /* synthetic */ void setCustomView(View view) {
        super.setCustomView(view);
    }

    public /* bridge */ /* synthetic */ void setCustomView(View view, ActionBar.LayoutParams layoutParams) {
        super.setCustomView(view, layoutParams);
    }

    public /* bridge */ /* synthetic */ void setDisplayHomeAsUpEnabled(boolean z) {
        super.setDisplayHomeAsUpEnabled(z);
    }

    public /* bridge */ /* synthetic */ void setDisplayOptions(int i) {
        super.setDisplayOptions(i);
    }

    public /* bridge */ /* synthetic */ void setDisplayOptions(int i, int i2) {
        super.setDisplayOptions(i, i2);
    }

    public /* bridge */ /* synthetic */ void setDisplayShowCustomEnabled(boolean z) {
        super.setDisplayShowCustomEnabled(z);
    }

    public /* bridge */ /* synthetic */ void setDisplayShowHomeEnabled(boolean z) {
        super.setDisplayShowHomeEnabled(z);
    }

    public /* bridge */ /* synthetic */ void setDisplayShowTitleEnabled(boolean z) {
        super.setDisplayShowTitleEnabled(z);
    }

    public /* bridge */ /* synthetic */ void setDisplayUseLogoEnabled(boolean z) {
        super.setDisplayUseLogoEnabled(z);
    }

    public /* bridge */ /* synthetic */ void setHomeAsUpIndicator(int i) {
        super.setHomeAsUpIndicator(i);
    }

    public /* bridge */ /* synthetic */ void setHomeAsUpIndicator(Drawable drawable) {
        super.setHomeAsUpIndicator(drawable);
    }

    public /* bridge */ /* synthetic */ void setHomeButtonEnabled(boolean z) {
        super.setHomeButtonEnabled(z);
    }

    public /* bridge */ /* synthetic */ void setIcon(int i) {
        super.setIcon(i);
    }

    public /* bridge */ /* synthetic */ void setIcon(Drawable drawable) {
        super.setIcon(drawable);
    }

    public /* bridge */ /* synthetic */ void setListNavigationCallbacks(SpinnerAdapter spinnerAdapter, ActionBar.OnNavigationListener onNavigationListener) {
        super.setListNavigationCallbacks(spinnerAdapter, onNavigationListener);
    }

    public /* bridge */ /* synthetic */ void setLogo(int i) {
        super.setLogo(i);
    }

    public /* bridge */ /* synthetic */ void setLogo(Drawable drawable) {
        super.setLogo(drawable);
    }

    public /* bridge */ /* synthetic */ void setNavigationMode(int i) {
        super.setNavigationMode(i);
    }

    public /* bridge */ /* synthetic */ void setSelectedNavigationItem(int i) {
        super.setSelectedNavigationItem(i);
    }

    public /* bridge */ /* synthetic */ void setSplitBackgroundDrawable(Drawable drawable) {
        super.setSplitBackgroundDrawable(drawable);
    }

    public /* bridge */ /* synthetic */ void setStackedBackgroundDrawable(Drawable drawable) {
        super.setStackedBackgroundDrawable(drawable);
    }

    public /* bridge */ /* synthetic */ void setSubtitle(int i) {
        super.setSubtitle(i);
    }

    public /* bridge */ /* synthetic */ void setSubtitle(CharSequence charSequence) {
        super.setSubtitle(charSequence);
    }

    public /* bridge */ /* synthetic */ void setTitle(int i) {
        super.setTitle(i);
    }

    public /* bridge */ /* synthetic */ void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
    }

    public /* bridge */ /* synthetic */ void show() {
        super.show();
    }

    public ActionBarImplJB(Activity activity, ActionBar.Callback callback) {
        super(activity, callback, false);
    }
}
