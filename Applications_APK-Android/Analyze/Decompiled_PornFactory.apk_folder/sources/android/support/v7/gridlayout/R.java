package android.support.v7.gridlayout;

public final class R {

    public static final class attr {
        public static final int alignmentMode = 2130772130;
        public static final int columnCount = 2130772128;
        public static final int columnOrderPreserved = 2130772132;
        public static final int layout_column = 2130772136;
        public static final int layout_columnSpan = 2130772137;
        public static final int layout_columnWeight = 2130772138;
        public static final int layout_gravity = 2130772139;
        public static final int layout_row = 2130772133;
        public static final int layout_rowSpan = 2130772134;
        public static final int layout_rowWeight = 2130772135;
        public static final int orientation = 2130772126;
        public static final int rowCount = 2130772127;
        public static final int rowOrderPreserved = 2130772131;
        public static final int useDefaultMargins = 2130772129;
    }

    public static final class dimen {
        public static final int default_gap = 2131296290;
    }

    public static final class id {
        public static final int alignBounds = 2131427439;
        public static final int alignMargins = 2131427440;
        public static final int bottom = 2131427432;
        public static final int center = 2131427441;
        public static final int center_horizontal = 2131427442;
        public static final int center_vertical = 2131427443;
        public static final int clip_horizontal = 2131427444;
        public static final int clip_vertical = 2131427445;
        public static final int end = 2131427427;
        public static final int fill = 2131427446;
        public static final int fill_horizontal = 2131427447;
        public static final int fill_vertical = 2131427448;
        public static final int horizontal = 2131427437;
        public static final int left = 2131427449;
        public static final int right = 2131427450;
        public static final int start = 2131427451;
        public static final int top = 2131427433;
        public static final int vertical = 2131427438;
    }

    public static final class styleable {
        public static final int[] GridLayout = {ch.nth.android.contentabo_l01_sim_univ.R.attr.orientation, ch.nth.android.contentabo_l01_sim_univ.R.attr.rowCount, ch.nth.android.contentabo_l01_sim_univ.R.attr.columnCount, ch.nth.android.contentabo_l01_sim_univ.R.attr.useDefaultMargins, ch.nth.android.contentabo_l01_sim_univ.R.attr.alignmentMode, ch.nth.android.contentabo_l01_sim_univ.R.attr.rowOrderPreserved, ch.nth.android.contentabo_l01_sim_univ.R.attr.columnOrderPreserved};
        public static final int[] GridLayout_Layout = {16842996, 16842997, 16842998, 16842999, 16843000, 16843001, 16843002, ch.nth.android.contentabo_l01_sim_univ.R.attr.layout_row, ch.nth.android.contentabo_l01_sim_univ.R.attr.layout_rowSpan, ch.nth.android.contentabo_l01_sim_univ.R.attr.layout_rowWeight, ch.nth.android.contentabo_l01_sim_univ.R.attr.layout_column, ch.nth.android.contentabo_l01_sim_univ.R.attr.layout_columnSpan, ch.nth.android.contentabo_l01_sim_univ.R.attr.layout_columnWeight, ch.nth.android.contentabo_l01_sim_univ.R.attr.layout_gravity};
        public static final int GridLayout_Layout_android_layout_height = 1;
        public static final int GridLayout_Layout_android_layout_margin = 2;
        public static final int GridLayout_Layout_android_layout_marginBottom = 6;
        public static final int GridLayout_Layout_android_layout_marginLeft = 3;
        public static final int GridLayout_Layout_android_layout_marginRight = 5;
        public static final int GridLayout_Layout_android_layout_marginTop = 4;
        public static final int GridLayout_Layout_android_layout_width = 0;
        public static final int GridLayout_Layout_layout_column = 10;
        public static final int GridLayout_Layout_layout_columnSpan = 11;
        public static final int GridLayout_Layout_layout_columnWeight = 12;
        public static final int GridLayout_Layout_layout_gravity = 13;
        public static final int GridLayout_Layout_layout_row = 7;
        public static final int GridLayout_Layout_layout_rowSpan = 8;
        public static final int GridLayout_Layout_layout_rowWeight = 9;
        public static final int GridLayout_alignmentMode = 4;
        public static final int GridLayout_columnCount = 2;
        public static final int GridLayout_columnOrderPreserved = 6;
        public static final int GridLayout_orientation = 0;
        public static final int GridLayout_rowCount = 1;
        public static final int GridLayout_rowOrderPreserved = 5;
        public static final int GridLayout_useDefaultMargins = 3;
    }
}
