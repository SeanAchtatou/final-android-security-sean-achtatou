package android.support.v4.util;

import com.dd.plist.ASCIIPropertyListParser;

public class DebugUtils {
    public static void buildShortClassTag(Object cls, StringBuilder out) {
        int end;
        if (cls == null) {
            out.append("null");
            return;
        }
        String simpleName = cls.getClass().getSimpleName();
        if ((simpleName == null || simpleName.length() <= 0) && (end = (simpleName = cls.getClass().getName()).lastIndexOf(46)) > 0) {
            simpleName = simpleName.substring(end + 1);
        }
        out.append(simpleName);
        out.append((char) ASCIIPropertyListParser.DICTIONARY_BEGIN_TOKEN);
        out.append(Integer.toHexString(System.identityHashCode(cls)));
    }
}
