package ch.nth.android.contentabo.common.recivers;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import ch.nth.android.contentabo.common.utils.MessageUtils;
import ch.nth.android.contentabo.common.utils.PaymentUtils;
import ch.nth.android.contentabo.common.utils.Utils;
import ch.nth.android.contentabo.fragments.base.PaymentAbstractFragment;
import ch.nth.android.contentabo.models.payment.PaymentOption;

public class SMSBroadcastReceiver extends BroadcastReceiver {
    private static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$payment$PaymentOption$PaymentFlowType;
    private boolean isAngebotMessage;
    private boolean isWelcomeMessage;

    static /* synthetic */ int[] $SWITCH_TABLE$ch$nth$android$contentabo$models$payment$PaymentOption$PaymentFlowType() {
        int[] iArr = $SWITCH_TABLE$ch$nth$android$contentabo$models$payment$PaymentOption$PaymentFlowType;
        if (iArr == null) {
            iArr = new int[PaymentOption.PaymentFlowType.values().length];
            try {
                iArr[PaymentOption.PaymentFlowType.DOUBLE_MO_OPTIN.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PaymentOption.PaymentFlowType.SINGLE_MO_OPTIN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PaymentOption.PaymentFlowType.UNKNOWN.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PaymentOption.PaymentFlowType.WEB_FLOW.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$ch$nth$android$contentabo$models$payment$PaymentOption$PaymentFlowType = iArr;
        }
        return iArr;
    }

    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        try {
            Utils.doLog("Payment SMS-Receiver", "onReceive message");
            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                StringBuilder message = new StringBuilder();
                String fromNumber = null;
                PaymentOption paymentOption = PaymentUtils.getPaymentOption(context);
                for (Object obj : pdusObj) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) obj);
                    fromNumber = currentMessage.getDisplayOriginatingAddress();
                    message.append(currentMessage.getMessageBody());
                }
                boolean isOurShortId = isOurShortId(paymentOption, fromNumber);
                Utils.doLog("Payment SMS-Receiver", "appshid " + isOurShortId);
                if (processMessage(context, fromNumber, paymentOption, isOurShortId, message.toString())) {
                    boolean isInserted = insertMessage(context, fromNumber, message.toString());
                    Utils.doLog("Payment SMS-Receiver", "Inserted " + isInserted + " " + message.toString());
                    if (isInserted) {
                        abortBroadcast();
                    }
                    if (this.isAngebotMessage || this.isWelcomeMessage) {
                        sendLocalBroadcast(context);
                    }
                }
                if (!TextUtils.isEmpty(fromNumber) && isOurShortId) {
                    markSmsAsRead(context, fromNumber, message.toString());
                }
            }
        } catch (Exception ex) {
            Utils.doLogException(ex);
        }
    }

    private boolean processMessage(Context context, String fromNumber, PaymentOption paymentOption, boolean isOurShortId, String message) {
        PaymentOption.PaymentFlowType flowType = paymentOption.getPaymentFlowType();
        if (!isOurShortId) {
            return false;
        }
        Utils.doLog("Payment SMS-Receiver", "SMSBroadcastReceiver flowType: " + flowType);
        Utils.doLog("Payment SMS-Receiver", "SMSBroadcastReceiver firstMoStatus: " + paymentOption.getFirstMoStatus());
        switch ($SWITCH_TABLE$ch$nth$android$contentabo$models$payment$PaymentOption$PaymentFlowType()[flowType.ordinal()]) {
            case 1:
                Utils.doLog("Payment SMS-Receiver", "processMessage - DOUBLE_MO_OPTIN flowType");
                if (paymentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY) {
                    Utils.doLog("Payment SMS-Receiver", "processMessage - FIRST_MO_SENT_SUCCESSFULLY");
                    PaymentUtils.setPaymentFlowStage(context, PaymentUtils.PaymentFlowStage.ANGEBOT_MESSAGE_RECIVED);
                    this.isAngebotMessage = true;
                    Utils.doLog("Payment SMS-Receiver", "consumed by double mo angebot");
                    return true;
                }
                break;
            case 2:
                Utils.doLog("Payment SMS-Receiver", "PaymentFlowStage: " + paymentOption.getFirstMoStatus());
                if (paymentOption.getFirstMoStatus() == PaymentUtils.PaymentFlowStage.FIRST_MO_SENT_SUCCESSFULLY) {
                    Utils.doLog("Payment SMS-Receiver", "processMessage - FIRST_MO_SENT_SUCCESSFULLY");
                    PaymentUtils.setPaymentFlowStage(context, PaymentUtils.PaymentFlowStage.WELCOME_MESSAGE_RECEIVED);
                    this.isWelcomeMessage = true;
                    Utils.doLog("ContentAbo", "consumed by single mo angebot");
                    return true;
                }
                break;
            default:
                Utils.doLog("Payment SMS-Receiver", "processMessage - unknown flowType");
                break;
        }
        if (!MessageUtils.isOurMessage(message)) {
            return false;
        }
        Utils.doLog("Payment SMS-Receiver", "Consumed by pattern");
        return true;
    }

    private boolean isOurShortId(PaymentOption paymentOption, String fromNumber) {
        String paymentShortId = paymentOption.getNumberFromServiceUrl();
        String fromNumber2 = fromNumber.replace("+", "");
        String paymentShortId2 = paymentShortId.replaceFirst("^00", "");
        Utils.doLog("Payment SMS-Receiver", "from: " + fromNumber2 + "; shid: " + paymentShortId2);
        return paymentShortId2.equals(fromNumber2);
    }

    private void sendLocalBroadcast(Context context) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(PaymentAbstractFragment.ANGEBOTE_SMS_RECEIVED_ACTION));
    }

    private void markSmsAsRead(final Context context, final String from, final String body) {
        new Thread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    Utils.doLog("Payment SMS-Receiver", "Exception while sleeping markSmsAsReadThread: " + e.getMessage());
                }
                Uri uri = Uri.parse("content://sms/inbox");
                String[] selectionArgs = {from, body, "0"};
                ContentValues values = new ContentValues();
                values.put("read", (Boolean) true);
                Utils.doLog("Payment SMS-Receiver", "rows updated: " + context.getContentResolver().update(uri, values, "address = ? AND body = ? AND read = ?", selectionArgs));
            }
        }).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    private boolean insertMessage(Context context, String from, String body) {
        try {
            ContentValues values = new ContentValues();
            values.put("address", from);
            values.put("body", body);
            values.put("read", (Boolean) true);
            context.getContentResolver().insert(Uri.parse("content://sms/inbox"), values);
            return true;
        } catch (Exception e) {
            Utils.doLog("Payment SMS-Receiver", "Exception while writing message: " + e.getMessage());
            return false;
        }
    }
}
