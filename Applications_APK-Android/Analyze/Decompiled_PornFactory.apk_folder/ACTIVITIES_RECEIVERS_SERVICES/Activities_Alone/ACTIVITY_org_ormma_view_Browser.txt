package org.ormma.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Browser extends Activity {
    private static final int BackwardId = 103;
    private static final int ButtonId = 100;
    private static final int ForwardId = 102;
    public static final String SHOW_BACK_EXTRA = "open_show_back";
    public static final String SHOW_FORWARD_EXTRA = "open_show_forward";
    public static final String SHOW_REFRESH_EXTRA = "open_show_refresh";
    public static final String URL_EXTRA = "extra_url";
    private static final int WebViewId = 101;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RelativeLayout rl = new RelativeLayout(this);
        WebView webview = new WebView(this);
        getWindow().requestFeature(2);
        getWindow().setFeatureInt(2, -1);
        Intent i = getIntent();
        LinearLayout bll = new LinearLayout(this);
        bll.setOrientation(0);
        bll.setGravity(16);
        bll.setId(100);
        bll.setWeightSum(100.0f);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(-1, -1);
        lp.addRule(2, 100);
        bll.setBackgroundDrawable(new BitmapDrawable(getResources(), bitmapFromJar("bitmaps/bkgrnd.png")));
        rl.addView(webview, lp);
        RelativeLayout.LayoutParams lp2 = new RelativeLayout.LayoutParams(-1, -2);
        lp2.addRule(12);
        rl.addView(bll, lp2);
        LinearLayout.LayoutParams lp22 = new LinearLayout.LayoutParams(-2, -1);
        lp22.weight = 25.0f;
        lp22.gravity = 16;
        ImageButton backButton = new ImageButton(this);
        backButton.setBackgroundColor(17170445);
        backButton.setId(BackwardId);
        bll.addView(backButton, lp22);
        if (!i.getBooleanExtra(SHOW_BACK_EXTRA, true)) {
            backButton.setVisibility(8);
        }
        backButton.setImageBitmap(bitmapFromJar("bitmaps/leftarrow.png"));
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                WebView wv = (WebView) Browser.this.findViewById(Browser.WebViewId);
                if (wv.canGoBack()) {
                    wv.goBack();
                } else {
                    Browser.this.finish();
                }
            }
        });
        ImageButton forwardButton = new ImageButton(this);
        forwardButton.setBackgroundColor(17170445);
        forwardButton.setId(102);
        LinearLayout.LayoutParams lp23 = new LinearLayout.LayoutParams(-2, -1);
        lp23.weight = 25.0f;
        lp23.gravity = 16;
        bll.addView(forwardButton, lp23);
        if (!i.getBooleanExtra(SHOW_FORWARD_EXTRA, true)) {
            forwardButton.setVisibility(8);
        }
        forwardButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ((WebView) Browser.this.findViewById(Browser.WebViewId)).goForward();
            }
        });
        ImageButton refreshButton = new ImageButton(this);
        refreshButton.setImageBitmap(bitmapFromJar("bitmaps/refresh.png"));
        refreshButton.setBackgroundColor(17170445);
        LinearLayout.LayoutParams lp24 = new LinearLayout.LayoutParams(-2, -2);
        lp24.weight = 25.0f;
        lp24.gravity = 16;
        bll.addView(refreshButton, lp24);
        if (!i.getBooleanExtra(SHOW_REFRESH_EXTRA, true)) {
            refreshButton.setVisibility(8);
        }
        refreshButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ((WebView) Browser.this.findViewById(Browser.WebViewId)).reload();
            }
        });
        ImageButton closeButton = new ImageButton(this);
        closeButton.setImageBitmap(bitmapFromJar("bitmaps/close.png"));
        closeButton.setBackgroundColor(17170445);
        LinearLayout.LayoutParams lp25 = new LinearLayout.LayoutParams(-2, -2);
        lp25.weight = 25.0f;
        lp25.gravity = 16;
        bll.addView(closeButton, lp25);
        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Browser.this.finish();
            }
        });
        getWindow().requestFeature(2);
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().startSync();
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(i.getStringExtra(URL_EXTRA));
        webview.setId(WebViewId);
        webview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText((Activity) view.getContext(), "Ormma Error:" + description, 0).show();
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                ((ImageButton) Browser.this.findViewById(102)).setImageBitmap(Browser.this.bitmapFromJar("bitmaps/unrightarrow.png"));
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                ImageButton forwardButton = (ImageButton) Browser.this.findViewById(102);
                if (view.canGoForward()) {
                    forwardButton.setImageBitmap(Browser.this.bitmapFromJar("bitmaps/rightarrow.png"));
                } else {
                    forwardButton.setImageBitmap(Browser.this.bitmapFromJar("bitmaps/unrightarrow.png"));
                }
            }
        });
        setContentView(rl);
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                Activity a = (Activity) view.getContext();
                a.setTitle("Loading...");
                a.setProgress(progress * 100);
                if (progress == 100) {
                    a.setTitle(view.getUrl());
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        CookieSyncManager.getInstance().stopSync();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        CookieSyncManager.getInstance().startSync();
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x005d A[SYNTHETIC, Splitter:B:33:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0063 A[SYNTHETIC, Splitter:B:37:0x0063] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap bitmapFromJar(java.lang.String r12) {
        /*
            r11 = this;
            r4 = 0
            r5 = 0
            java.lang.Class<org.ormma.controller.OrmmaAssetController> r9 = org.ormma.controller.OrmmaAssetController.class
            java.lang.ClassLoader r9 = r9.getClassLoader()     // Catch:{ Exception -> 0x0048 }
            java.net.URL r8 = r9.getResource(r12)     // Catch:{ Exception -> 0x0048 }
            java.lang.String r3 = r8.getFile()     // Catch:{ Exception -> 0x0048 }
            java.lang.String r9 = "file:"
            boolean r9 = r3.startsWith(r9)     // Catch:{ Exception -> 0x0048 }
            if (r9 == 0) goto L_0x001d
            r9 = 5
            java.lang.String r3 = r3.substring(r9)     // Catch:{ Exception -> 0x0048 }
        L_0x001d:
            java.lang.String r9 = "!"
            int r7 = r3.indexOf(r9)     // Catch:{ Exception -> 0x0048 }
            if (r7 <= 0) goto L_0x002a
            r9 = 0
            java.lang.String r3 = r3.substring(r9, r7)     // Catch:{ Exception -> 0x0048 }
        L_0x002a:
            java.util.jar.JarFile r6 = new java.util.jar.JarFile     // Catch:{ Exception -> 0x0048 }
            r6.<init>(r3)     // Catch:{ Exception -> 0x0048 }
            java.util.jar.JarEntry r2 = r6.getJarEntry(r12)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            java.io.InputStream r4 = r6.getInputStream(r2)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0074 }
            if (r4 == 0) goto L_0x0041
            r4.close()     // Catch:{ Exception -> 0x0068 }
        L_0x0040:
            r4 = 0
        L_0x0041:
            if (r6 == 0) goto L_0x007a
            r6.close()     // Catch:{ Exception -> 0x006a }
        L_0x0046:
            r5 = 0
        L_0x0047:
            return r0
        L_0x0048:
            r1 = move-exception
        L_0x0049:
            r1.printStackTrace()     // Catch:{ all -> 0x005a }
            if (r4 == 0) goto L_0x0052
            r4.close()     // Catch:{ Exception -> 0x006c }
        L_0x0051:
            r4 = 0
        L_0x0052:
            if (r5 == 0) goto L_0x0058
            r5.close()     // Catch:{ Exception -> 0x006e }
        L_0x0057:
            r5 = 0
        L_0x0058:
            r0 = 0
            goto L_0x0047
        L_0x005a:
            r9 = move-exception
        L_0x005b:
            if (r4 == 0) goto L_0x0061
            r4.close()     // Catch:{ Exception -> 0x0070 }
        L_0x0060:
            r4 = 0
        L_0x0061:
            if (r5 == 0) goto L_0x0067
            r5.close()     // Catch:{ Exception -> 0x0072 }
        L_0x0066:
            r5 = 0
        L_0x0067:
            throw r9
        L_0x0068:
            r9 = move-exception
            goto L_0x0040
        L_0x006a:
            r9 = move-exception
            goto L_0x0046
        L_0x006c:
            r9 = move-exception
            goto L_0x0051
        L_0x006e:
            r9 = move-exception
            goto L_0x0057
        L_0x0070:
            r10 = move-exception
            goto L_0x0060
        L_0x0072:
            r10 = move-exception
            goto L_0x0066
        L_0x0074:
            r9 = move-exception
            r5 = r6
            goto L_0x005b
        L_0x0077:
            r1 = move-exception
            r5 = r6
            goto L_0x0049
        L_0x007a:
            r5 = r6
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ormma.view.Browser.bitmapFromJar(java.lang.String):android.graphics.Bitmap");
    }
}
