package uebd;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import java.lang.reflect.Method;

public class VvacActivity extends Activity {
    int a = Build.VERSION.SDK_INT;
    private BroadcastReceiver b = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            VvacActivity.this.finish();
        }
    };

    private Object a(Class cls) {
        return cls.getMethod("_get".toLowerCase().substring(1) + "Window", new Class[0]).invoke(this, new Object[0]);
    }

    static void a(Object obj) {
        try {
            a(obj, obj.getClass().getMethod("setSystemUiVisibility", Integer.TYPE));
        } catch (Exception unused) {
        }
    }

    static void a(Object obj, int i) {
        try {
            ((Window) obj).addFlags(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void a(Object obj, Method method) {
        method.invoke(obj, 1280);
    }

    private void b(Object obj) {
        Class<?> cls = obj.getClass();
        a(cls.getMethod("  Get".toLowerCase().substring(2) + "sDecorView".substring(1), new Class[0]).invoke(obj, new Object[0]));
        a(obj, 524288);
        Class<?> cls2 = obj.getClass();
        cls2.getMethod("Set".toLowerCase() + "StatusBarColor", Integer.TYPE).invoke(obj, 0);
    }

    static void b(Object obj, int i) {
        try {
            obj.getClass().getMethod("clearFlags", Integer.TYPE).invoke(obj, Integer.valueOf(i));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            Object a2 = a((Class) super.getClass());
            if (this.a >= 21) {
                b(a2, 67108864);
                a(a2, Integer.MIN_VALUE);
                b(a2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
