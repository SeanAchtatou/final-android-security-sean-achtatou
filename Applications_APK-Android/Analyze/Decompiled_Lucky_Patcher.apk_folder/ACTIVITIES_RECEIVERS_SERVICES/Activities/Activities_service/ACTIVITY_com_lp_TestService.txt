package com.lp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.lp.C0970;

public class TestService extends Service {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0970.C0971 f3905 = new C0970.C0971() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m5845() {
            TestService.this.stopSelf();
            return true;
        }
    };

    public IBinder onBind(Intent intent) {
        return this.f3905;
    }
}
