package com.chelpus;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import com.chelpus.C0811;
import com.google.android.finsky.billing.iab.C0821;
import com.lp.C0967;
import com.lp.C0987;
import java.io.File;
import java.io.FileNotFoundException;
import net.lingala.zip4j.util.InternalZipConstants;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class TransferFilesService extends Service {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0811.C0812 f3090 = new C0811.C0812() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m5015() {
            return true;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public ParcelFileDescriptor m5016() {
            try {
                C0987.f4436 = new C0967(C0987.m6072());
                C0987.f4436.close();
                TransferFilesService.this.getPackageName();
                String replace = TransferFilesService.this.getDir("lptest", 0).getAbsolutePath().replace("/app_lptest", BuildConfig.FLAVOR);
                return ParcelFileDescriptor.open(new File(replace + "/databases/PackagesDB"), 268435456);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Throwable th) {
                th.printStackTrace();
                return null;
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public ParcelFileDescriptor m5018() {
            try {
                new C0821(C0987.m6072());
                C0987.f4518.close();
                TransferFilesService.this.getPackageName();
                String replace = TransferFilesService.this.getDir("lptest", 0).getAbsolutePath().replace("/app_lptest", BuildConfig.FLAVOR);
                return ParcelFileDescriptor.open(new File(replace + "/databases/BillingRestoreTransactions"), 268435456);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Throwable th) {
                th.printStackTrace();
                return null;
            }
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public ParcelFileDescriptor m5019() {
            try {
                TransferFilesService.this.getPackageName();
                String replace = TransferFilesService.this.getDir("lptest", 0).getAbsolutePath().replace("/app_lptest", BuildConfig.FLAVOR);
                return ParcelFileDescriptor.open(new File(replace + "/shared_prefs/config.xml"), 268435456);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Throwable th) {
                th.printStackTrace();
                return null;
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public String m5017(String str) {
            return TransferFilesService.this.getSharedPreferences("config", 4).getString(str, BuildConfig.FLAVOR);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public ParcelFileDescriptor m5014(String str) {
            try {
                String replace = TransferFilesService.this.getDir("lptest", 0).getAbsolutePath().replace("/app_lptest", BuildConfig.FLAVOR);
                return ParcelFileDescriptor.open(new File(replace + InternalZipConstants.ZIP_FILE_SEPARATOR + str), 268435456);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Throwable th) {
                th.printStackTrace();
                return null;
            }
        }
    };

    public IBinder onBind(Intent intent) {
        return this.f3090;
    }
}
