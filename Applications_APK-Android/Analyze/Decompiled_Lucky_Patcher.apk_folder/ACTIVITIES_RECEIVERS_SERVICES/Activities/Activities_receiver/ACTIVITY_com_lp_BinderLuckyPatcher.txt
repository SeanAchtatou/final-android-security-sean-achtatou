package com.lp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.chelpus.C0815;
import com.lp.widgets.BinderWidget;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import ru.pKkcGXHI.kKSaIWSZS.BuildConfig;

public class BinderLuckyPatcher extends BroadcastReceiver {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Context f3798;

    public void onReceive(final Context context, Intent intent) {
        C0987.m6060((Object) ("LuckyPatcher Intent: " + intent.toString()));
        f3798 = context;
        C0987.m6079(context);
        if (C0987.f4474) {
            if (intent.getAction().equals("android.intent.action.MEDIA_SCANNER_FINISHED") || intent.getAction().equals("android.intent.action.MEDIA_MOUNTED")) {
                new Thread(new Runnable() {
                    public void run() {
                        File file = new File(context.getDir("binder", 0) + "/bind.txt");
                        if (file.exists() && file.length() > 0) {
                            C0987.m6060((Object) "LuckyPatcher binder start!");
                            try {
                                if (!file.exists()) {
                                    file.createNewFile();
                                }
                                FileInputStream fileInputStream = new FileInputStream(file);
                                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
                                while (true) {
                                    String readLine = bufferedReader.readLine();
                                    if (readLine == null) {
                                        break;
                                    }
                                    String[] split = readLine.split(";");
                                    if (split.length == 2) {
                                        C0815.m5178("mount", "-o bind '" + split[0] + "' '" + split[1] + "'", split[0], split[1]);
                                    }
                                }
                                Intent intent = new Intent(context, BinderWidget.class);
                                intent.setAction(BinderWidget.f3931);
                                context.sendBroadcast(intent);
                                fileInputStream.close();
                            } catch (FileNotFoundException unused) {
                                C0987.m6060((Object) "Not found bind.txt");
                            } catch (IOException e) {
                                C0987.m6060((Object) (BuildConfig.FLAVOR + e));
                            }
                            C0987.f4484 = false;
                        }
                    }
                }).start();
                C0987.f4484 = true;
            }
            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                File file = new File(context.getDir("binder", 0) + "/bind.txt");
                if (file.exists() && file.length() > 0) {
                    C0987.m6060((Object) "LuckyPatcher binder start!");
                    try {
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        FileInputStream fileInputStream = new FileInputStream(file);
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
                        while (true) {
                            String readLine = bufferedReader.readLine();
                            if (readLine != null) {
                                String[] split = readLine.split(";");
                                if (split.length == 2) {
                                    C0815.m5178("mount", "-o bind '" + split[0] + "' '" + split[1] + "'", split[0], split[1]);
                                }
                            } else {
                                Intent intent2 = new Intent(context, BinderWidget.class);
                                intent2.setAction(BinderWidget.f3931);
                                context.sendBroadcast(intent2);
                                fileInputStream.close();
                                return;
                            }
                        }
                    } catch (FileNotFoundException unused) {
                        C0987.m6060((Object) "Not found bind.txt");
                    } catch (IOException e) {
                        C0987.m6060((Object) (BuildConfig.FLAVOR + e));
                    }
                }
            }
        }
    }
}
