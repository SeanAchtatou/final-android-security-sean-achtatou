package android.support.ʼ.ʻ;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.support.v4.content.ʻ.C0108;
import android.support.v4.ʼ.C0303;
import android.util.AttributeSet;
import android.view.InflateException;
import android.view.animation.Interpolator;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: android.support.ʼ.ʻ.ˈ  reason: contains not printable characters */
/* compiled from: PathInterpolatorCompat */
public class C0758 implements Interpolator {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float[] f3016;

    /* renamed from: ʼ  reason: contains not printable characters */
    private float[] f3017;

    public C0758(Context context, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        this(context.getResources(), context.getTheme(), attributeSet, xmlPullParser);
    }

    public C0758(Resources resources, Resources.Theme theme, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        TypedArray r2 = C0108.m566(resources, theme, attributeSet, C0749.f3000);
        m4930(r2, xmlPullParser);
        r2.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.content.ʻ.ʽ.ʻ(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, float):float
     arg types: [android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, int]
     candidates:
      android.support.v4.content.ʻ.ʽ.ʻ(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, int):int
      android.support.v4.content.ʻ.ʽ.ʻ(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, boolean):boolean
      android.support.v4.content.ʻ.ʽ.ʻ(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, float):float */
    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4930(TypedArray typedArray, XmlPullParser xmlPullParser) {
        if (C0108.m569(xmlPullParser, "pathData")) {
            String r8 = C0108.m567(typedArray, xmlPullParser, "pathData", 4);
            Path r9 = C0303.m1765(r8);
            if (r9 != null) {
                m4931(r9);
                return;
            }
            throw new InflateException("The path is null, which is created from " + r8);
        } else if (!C0108.m569(xmlPullParser, "controlX1")) {
            throw new InflateException("pathInterpolator requires the controlX1 attribute");
        } else if (C0108.m569(xmlPullParser, "controlY1")) {
            float r0 = C0108.m564(typedArray, xmlPullParser, "controlX1", 0, 0.0f);
            float r1 = C0108.m564(typedArray, xmlPullParser, "controlY1", 1, 0.0f);
            boolean r4 = C0108.m569(xmlPullParser, "controlX2");
            if (r4 != C0108.m569(xmlPullParser, "controlY2")) {
                throw new InflateException("pathInterpolator requires both controlX2 and controlY2 for cubic Beziers.");
            } else if (!r4) {
                m4928(r0, r1);
            } else {
                m4929(r0, r1, C0108.m564(typedArray, xmlPullParser, "controlX2", 2, 0.0f), C0108.m564(typedArray, xmlPullParser, "controlY2", 3, 0.0f));
            }
        } else {
            throw new InflateException("pathInterpolator requires the controlY1 attribute");
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4928(float f, float f2) {
        Path path = new Path();
        path.moveTo(0.0f, 0.0f);
        path.quadTo(f, f2, 1.0f, 1.0f);
        m4931(path);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4929(float f, float f2, float f3, float f4) {
        Path path = new Path();
        path.moveTo(0.0f, 0.0f);
        path.cubicTo(f, f2, f3, f4, 1.0f, 1.0f);
        m4931(path);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4931(Path path) {
        int i = 0;
        PathMeasure pathMeasure = new PathMeasure(path, false);
        float length = pathMeasure.getLength();
        int min = Math.min(3000, ((int) (length / 0.002f)) + 1);
        if (min > 0) {
            this.f3016 = new float[min];
            this.f3017 = new float[min];
            float[] fArr = new float[2];
            for (int i2 = 0; i2 < min; i2++) {
                pathMeasure.getPosTan((((float) i2) * length) / ((float) (min - 1)), fArr, null);
                this.f3016[i2] = fArr[0];
                this.f3017[i2] = fArr[1];
            }
            if (((double) Math.abs(this.f3016[0])) <= 1.0E-5d && ((double) Math.abs(this.f3017[0])) <= 1.0E-5d) {
                int i3 = min - 1;
                if (((double) Math.abs(this.f3016[i3] - 1.0f)) <= 1.0E-5d && ((double) Math.abs(this.f3017[i3] - 1.0f)) <= 1.0E-5d) {
                    int i4 = 0;
                    float f = 0.0f;
                    while (i < min) {
                        float[] fArr2 = this.f3016;
                        int i5 = i4 + 1;
                        float f2 = fArr2[i4];
                        if (f2 >= f) {
                            fArr2[i] = f2;
                            i++;
                            f = f2;
                            i4 = i5;
                        } else {
                            throw new IllegalArgumentException("The Path cannot loop back on itself, x :" + f2);
                        }
                    }
                    if (pathMeasure.nextContour()) {
                        throw new IllegalArgumentException("The Path should be continuous, can't have 2+ contours");
                    }
                    return;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("The Path must start at (0,0) and end at (1,1) start: ");
            sb.append(this.f3016[0]);
            sb.append(",");
            sb.append(this.f3017[0]);
            sb.append(" end:");
            int i6 = min - 1;
            sb.append(this.f3016[i6]);
            sb.append(",");
            sb.append(this.f3017[i6]);
            throw new IllegalArgumentException(sb.toString());
        }
        throw new IllegalArgumentException("The Path has a invalid length " + length);
    }

    public float getInterpolation(float f) {
        if (f <= 0.0f) {
            return 0.0f;
        }
        if (f >= 1.0f) {
            return 1.0f;
        }
        int i = 0;
        int length = this.f3016.length - 1;
        while (length - i > 1) {
            int i2 = (i + length) / 2;
            if (f < this.f3016[i2]) {
                length = i2;
            } else {
                i = i2;
            }
        }
        float[] fArr = this.f3016;
        float f2 = fArr[length] - fArr[i];
        if (f2 == 0.0f) {
            return this.f3017[i];
        }
        float[] fArr2 = this.f3017;
        float f3 = fArr2[i];
        return f3 + (((f - fArr[i]) / f2) * (fArr2[length] - f3));
    }
}
