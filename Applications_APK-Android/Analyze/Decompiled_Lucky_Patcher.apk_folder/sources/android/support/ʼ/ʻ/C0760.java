package android.support.ʼ.ʻ;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.v4.content.ʻ.C0108;
import android.support.v4.ʼ.C0303;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ˈ.C0331;
import android.util.AttributeSet;
import java.util.ArrayList;
import java.util.Stack;
import net.lingala.zip4j.util.InternalZipConstants;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: android.support.ʼ.ʻ.ˊ  reason: contains not printable characters */
/* compiled from: VectorDrawableCompat */
public class C0760 extends C0759 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final PorterDuff.Mode f3019 = PorterDuff.Mode.SRC_IN;

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0766 f3020;

    /* renamed from: ʾ  reason: contains not printable characters */
    private PorterDuffColorFilter f3021;

    /* renamed from: ʿ  reason: contains not printable characters */
    private ColorFilter f3022;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f3023;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f3024;

    /* renamed from: ˉ  reason: contains not printable characters */
    private Drawable.ConstantState f3025;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final float[] f3026;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final Matrix f3027;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final Rect f3028;

    public /* bridge */ /* synthetic */ void applyTheme(Resources.Theme theme) {
        super.applyTheme(theme);
    }

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i) {
        super.setChangingConfigurations(i);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i, PorterDuff.Mode mode) {
        super.setColorFilter(i, mode);
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f, float f2) {
        super.setHotspot(f, f2);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    C0760() {
        this.f3024 = true;
        this.f3026 = new float[9];
        this.f3027 = new Matrix();
        this.f3028 = new Rect();
        this.f3020 = new C0766();
    }

    C0760(C0766 r3) {
        this.f3024 = true;
        this.f3026 = new float[9];
        this.f3027 = new Matrix();
        this.f3028 = new Rect();
        this.f3020 = r3;
        this.f3021 = m4939(this.f3021, r3.f3076, r3.f3077);
    }

    public Drawable mutate() {
        if (this.f3018 != null) {
            this.f3018.mutate();
            return this;
        }
        if (!this.f3023 && super.mutate() == this) {
            this.f3020 = new C0766(this.f3020);
            this.f3023 = true;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Object m4940(String str) {
        return this.f3020.f3075.f3066.get(str);
    }

    public Drawable.ConstantState getConstantState() {
        if (this.f3018 != null && Build.VERSION.SDK_INT >= 24) {
            return new C0767(this.f3018.getConstantState());
        }
        this.f3020.f3074 = getChangingConfigurations();
        return this.f3020;
    }

    public void draw(Canvas canvas) {
        if (this.f3018 != null) {
            this.f3018.draw(canvas);
            return;
        }
        copyBounds(this.f3028);
        if (this.f3028.width() > 0 && this.f3028.height() > 0) {
            ColorFilter colorFilter = this.f3022;
            if (colorFilter == null) {
                colorFilter = this.f3021;
            }
            canvas.getMatrix(this.f3027);
            this.f3027.getValues(this.f3026);
            float abs = Math.abs(this.f3026[0]);
            float abs2 = Math.abs(this.f3026[4]);
            float abs3 = Math.abs(this.f3026[1]);
            float abs4 = Math.abs(this.f3026[3]);
            if (!(abs3 == 0.0f && abs4 == 0.0f)) {
                abs = 1.0f;
                abs2 = 1.0f;
            }
            int min = Math.min((int) InternalZipConstants.UFT8_NAMES_FLAG, (int) (((float) this.f3028.width()) * abs));
            int min2 = Math.min((int) InternalZipConstants.UFT8_NAMES_FLAG, (int) (((float) this.f3028.height()) * abs2));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                canvas.translate((float) this.f3028.left, (float) this.f3028.top);
                if (m4937()) {
                    canvas.translate((float) this.f3028.width(), 0.0f);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.f3028.offsetTo(0, 0);
                this.f3020.m4969(min, min2);
                if (!this.f3024) {
                    this.f3020.m4966(min, min2);
                } else if (!this.f3020.m4970()) {
                    this.f3020.m4966(min, min2);
                    this.f3020.m4971();
                }
                this.f3020.m4967(canvas, colorFilter, this.f3028);
                canvas.restoreToCount(save);
            }
        }
    }

    public int getAlpha() {
        if (this.f3018 != null) {
            return C0288.m1710(this.f3018);
        }
        return this.f3020.f3075.getRootAlpha();
    }

    public void setAlpha(int i) {
        if (this.f3018 != null) {
            this.f3018.setAlpha(i);
        } else if (this.f3020.f3075.getRootAlpha() != i) {
            this.f3020.f3075.setRootAlpha(i);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f3018 != null) {
            this.f3018.setColorFilter(colorFilter);
            return;
        }
        this.f3022 = colorFilter;
        invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public PorterDuffColorFilter m4939(PorterDuffColorFilter porterDuffColorFilter, ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    public void setTint(int i) {
        if (this.f3018 != null) {
            C0288.m1701(this.f3018, i);
        } else {
            setTintList(ColorStateList.valueOf(i));
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.f3018 != null) {
            C0288.m1703(this.f3018, colorStateList);
            return;
        }
        C0766 r0 = this.f3020;
        if (r0.f3076 != colorStateList) {
            r0.f3076 = colorStateList;
            this.f3021 = m4939(this.f3021, colorStateList, r0.f3077);
            invalidateSelf();
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (this.f3018 != null) {
            C0288.m1706(this.f3018, mode);
            return;
        }
        C0766 r0 = this.f3020;
        if (r0.f3077 != mode) {
            r0.f3077 = mode;
            this.f3021 = m4939(this.f3021, r0.f3076, mode);
            invalidateSelf();
        }
    }

    public boolean isStateful() {
        C0766 r0;
        if (this.f3018 != null) {
            return this.f3018.isStateful();
        }
        return super.isStateful() || !((r0 = this.f3020) == null || r0.f3076 == null || !this.f3020.f3076.isStateful());
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        if (this.f3018 != null) {
            return this.f3018.setState(iArr);
        }
        C0766 r3 = this.f3020;
        if (r3.f3076 == null || r3.f3077 == null) {
            return false;
        }
        this.f3021 = m4939(this.f3021, r3.f3076, r3.f3077);
        invalidateSelf();
        return true;
    }

    public int getOpacity() {
        if (this.f3018 != null) {
            return this.f3018.getOpacity();
        }
        return -3;
    }

    public int getIntrinsicWidth() {
        if (this.f3018 != null) {
            return this.f3018.getIntrinsicWidth();
        }
        return (int) this.f3020.f3075.f3060;
    }

    public int getIntrinsicHeight() {
        if (this.f3018 != null) {
            return this.f3018.getIntrinsicHeight();
        }
        return (int) this.f3020.f3075.f3061;
    }

    public boolean canApplyTheme() {
        if (this.f3018 == null) {
            return false;
        }
        C0288.m1711(this.f3018);
        return false;
    }

    public boolean isAutoMirrored() {
        if (this.f3018 != null) {
            return C0288.m1708(this.f3018);
        }
        return this.f3020.f3078;
    }

    public void setAutoMirrored(boolean z) {
        if (this.f3018 != null) {
            C0288.m1707(this.f3018, z);
        } else {
            this.f3020.f3078 = z;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038 A[Catch:{ XmlPullParserException -> 0x004a, IOException -> 0x0045 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003d A[Catch:{ XmlPullParserException -> 0x004a, IOException -> 0x0045 }] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.support.ʼ.ʻ.C0760 m4934(android.content.res.Resources r6, int r7, android.content.res.Resources.Theme r8) {
        /*
            java.lang.String r0 = "parser error"
            java.lang.String r1 = "VectorDrawableCompat"
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 24
            if (r2 < r3) goto L_0x0023
            android.support.ʼ.ʻ.ˊ r0 = new android.support.ʼ.ʻ.ˊ
            r0.<init>()
            android.graphics.drawable.Drawable r6 = android.support.v4.content.ʻ.C0107.m562(r6, r7, r8)
            r0.f3018 = r6
            android.support.ʼ.ʻ.ˊ$ˈ r6 = new android.support.ʼ.ʻ.ˊ$ˈ
            android.graphics.drawable.Drawable r7 = r0.f3018
            android.graphics.drawable.Drawable$ConstantState r7 = r7.getConstantState()
            r6.<init>(r7)
            r0.f3025 = r6
            return r0
        L_0x0023:
            android.content.res.XmlResourceParser r7 = r6.getXml(r7)     // Catch:{ XmlPullParserException -> 0x004a, IOException -> 0x0045 }
            android.util.AttributeSet r2 = android.util.Xml.asAttributeSet(r7)     // Catch:{ XmlPullParserException -> 0x004a, IOException -> 0x0045 }
        L_0x002b:
            int r3 = r7.next()     // Catch:{ XmlPullParserException -> 0x004a, IOException -> 0x0045 }
            r4 = 2
            if (r3 == r4) goto L_0x0036
            r5 = 1
            if (r3 == r5) goto L_0x0036
            goto L_0x002b
        L_0x0036:
            if (r3 != r4) goto L_0x003d
            android.support.ʼ.ʻ.ˊ r6 = m4935(r6, r7, r2, r8)     // Catch:{ XmlPullParserException -> 0x004a, IOException -> 0x0045 }
            return r6
        L_0x003d:
            org.xmlpull.v1.XmlPullParserException r6 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ XmlPullParserException -> 0x004a, IOException -> 0x0045 }
            java.lang.String r7 = "No start tag found"
            r6.<init>(r7)     // Catch:{ XmlPullParserException -> 0x004a, IOException -> 0x0045 }
            throw r6     // Catch:{ XmlPullParserException -> 0x004a, IOException -> 0x0045 }
        L_0x0045:
            r6 = move-exception
            android.util.Log.e(r1, r0, r6)
            goto L_0x004e
        L_0x004a:
            r6 = move-exception
            android.util.Log.e(r1, r0, r6)
        L_0x004e:
            r6 = 0
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.ʼ.ʻ.C0760.m4934(android.content.res.Resources, int, android.content.res.Resources$Theme):android.support.ʼ.ʻ.ˊ");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0760 m4935(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        C0760 r0 = new C0760();
        r0.inflate(resources, xmlPullParser, attributeSet, theme);
        return r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m4932(int i, float f) {
        return (i & 16777215) | (((int) (((float) Color.alpha(i)) * f)) << 24);
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        if (this.f3018 != null) {
            this.f3018.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, null);
        }
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        if (this.f3018 != null) {
            C0288.m1705(this.f3018, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        C0766 r0 = this.f3020;
        r0.f3075 = new C0765();
        TypedArray r1 = C0108.m566(resources, theme, attributeSet, C0749.f2989);
        m4936(r1, xmlPullParser);
        r1.recycle();
        r0.f3074 = getChangingConfigurations();
        r0.f3084 = true;
        m4938(resources, xmlPullParser, attributeSet, theme);
        this.f3021 = m4939(this.f3021, r0.f3076, r0.f3077);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static PorterDuff.Mode m4933(int i, PorterDuff.Mode mode) {
        if (i == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return Build.VERSION.SDK_INT >= 11 ? PorterDuff.Mode.ADD : mode;
            default:
                return mode;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4936(TypedArray typedArray, XmlPullParser xmlPullParser) {
        C0766 r0 = this.f3020;
        C0765 r1 = r0.f3075;
        r0.f3077 = m4933(C0108.m565(typedArray, xmlPullParser, "tintMode", 6, -1), PorterDuff.Mode.SRC_IN);
        ColorStateList colorStateList = typedArray.getColorStateList(1);
        if (colorStateList != null) {
            r0.f3076 = colorStateList;
        }
        r0.f3078 = C0108.m568(typedArray, xmlPullParser, "autoMirrored", 5, r0.f3078);
        r1.f3062 = C0108.m564(typedArray, xmlPullParser, "viewportWidth", 7, r1.f3062);
        r1.f3063 = C0108.m564(typedArray, xmlPullParser, "viewportHeight", 8, r1.f3063);
        if (r1.f3062 <= 0.0f) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (r1.f3063 > 0.0f) {
            r1.f3060 = typedArray.getDimension(3, r1.f3060);
            r1.f3061 = typedArray.getDimension(2, r1.f3061);
            if (r1.f3060 <= 0.0f) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (r1.f3061 > 0.0f) {
                r1.setAlpha(C0108.m564(typedArray, xmlPullParser, "alpha", 4, r1.getAlpha()));
                String string = typedArray.getString(0);
                if (string != null) {
                    r1.f3065 = string;
                    r1.f3066.put(string, r1);
                }
            } else {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            }
        } else {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m4938(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        C0766 r0 = this.f3020;
        C0765 r1 = r0.f3075;
        Stack stack = new Stack();
        stack.push(r1.f3059);
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        boolean z = true;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                C0763 r8 = (C0763) stack.peek();
                if ("path".equals(name)) {
                    C0762 r3 = new C0762();
                    r3.m4948(resources, attributeSet, theme, xmlPullParser);
                    r8.f3042.add(r3);
                    if (r3.getPathName() != null) {
                        r1.f3066.put(r3.getPathName(), r3);
                    }
                    z = false;
                    r0.f3074 = r3.f3057 | r0.f3074;
                } else if ("clip-path".equals(name)) {
                    C0761 r32 = new C0761();
                    r32.m4943(resources, attributeSet, theme, xmlPullParser);
                    r8.f3042.add(r32);
                    if (r32.getPathName() != null) {
                        r1.f3066.put(r32.getPathName(), r32);
                    }
                    r0.f3074 = r32.f3057 | r0.f3074;
                } else if ("group".equals(name)) {
                    C0763 r33 = new C0763();
                    r33.m4953(resources, attributeSet, theme, xmlPullParser);
                    r8.f3042.add(r33);
                    stack.push(r33);
                    if (r33.getGroupName() != null) {
                        r1.f3066.put(r33.getGroupName(), r33);
                    }
                    r0.f3074 = r33.f3044 | r0.f3074;
                }
            } else if (eventType == 3 && "group".equals(xmlPullParser.getName())) {
                stack.pop();
            }
            eventType = xmlPullParser.next();
        }
        if (z) {
            StringBuffer stringBuffer = new StringBuffer();
            if (stringBuffer.length() > 0) {
                stringBuffer.append(" or ");
            }
            stringBuffer.append("path");
            throw new XmlPullParserException("no " + ((Object) stringBuffer) + " defined");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m4941(boolean z) {
        this.f3024 = z;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m4937() {
        if (Build.VERSION.SDK_INT < 17 || !isAutoMirrored() || C0288.m1714(this) != 1) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f3018 != null) {
            this.f3018.setBounds(rect);
        }
    }

    public int getChangingConfigurations() {
        if (this.f3018 != null) {
            return this.f3018.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f3020.getChangingConfigurations();
    }

    public void invalidateSelf() {
        if (this.f3018 != null) {
            this.f3018.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    public void scheduleSelf(Runnable runnable, long j) {
        if (this.f3018 != null) {
            this.f3018.scheduleSelf(runnable, j);
        } else {
            super.scheduleSelf(runnable, j);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.f3018 != null) {
            return this.f3018.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    public void unscheduleSelf(Runnable runnable) {
        if (this.f3018 != null) {
            this.f3018.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }

    /* renamed from: android.support.ʼ.ʻ.ˊ$ˈ  reason: contains not printable characters */
    /* compiled from: VectorDrawableCompat */
    private static class C0767 extends Drawable.ConstantState {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Drawable.ConstantState f3086;

        public C0767(Drawable.ConstantState constantState) {
            this.f3086 = constantState;
        }

        public Drawable newDrawable() {
            C0760 r0 = new C0760();
            r0.f3018 = (VectorDrawable) this.f3086.newDrawable();
            return r0;
        }

        public Drawable newDrawable(Resources resources) {
            C0760 r0 = new C0760();
            r0.f3018 = (VectorDrawable) this.f3086.newDrawable(resources);
            return r0;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            C0760 r0 = new C0760();
            r0.f3018 = (VectorDrawable) this.f3086.newDrawable(resources, theme);
            return r0;
        }

        public boolean canApplyTheme() {
            return this.f3086.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f3086.getChangingConfigurations();
        }
    }

    /* renamed from: android.support.ʼ.ʻ.ˊ$ˆ  reason: contains not printable characters */
    /* compiled from: VectorDrawableCompat */
    private static class C0766 extends Drawable.ConstantState {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f3074;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0765 f3075;

        /* renamed from: ʽ  reason: contains not printable characters */
        ColorStateList f3076;

        /* renamed from: ʾ  reason: contains not printable characters */
        PorterDuff.Mode f3077;

        /* renamed from: ʿ  reason: contains not printable characters */
        boolean f3078;

        /* renamed from: ˆ  reason: contains not printable characters */
        Bitmap f3079;

        /* renamed from: ˈ  reason: contains not printable characters */
        ColorStateList f3080;

        /* renamed from: ˉ  reason: contains not printable characters */
        PorterDuff.Mode f3081;

        /* renamed from: ˊ  reason: contains not printable characters */
        int f3082;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f3083;

        /* renamed from: ˎ  reason: contains not printable characters */
        boolean f3084;

        /* renamed from: ˏ  reason: contains not printable characters */
        Paint f3085;

        public C0766(C0766 r4) {
            this.f3076 = null;
            this.f3077 = C0760.f3019;
            if (r4 != null) {
                this.f3074 = r4.f3074;
                this.f3075 = new C0765(r4.f3075);
                if (r4.f3075.f3071 != null) {
                    Paint unused = this.f3075.f3071 = new Paint(r4.f3075.f3071);
                }
                if (r4.f3075.f3070 != null) {
                    Paint unused2 = this.f3075.f3070 = new Paint(r4.f3075.f3070);
                }
                this.f3076 = r4.f3076;
                this.f3077 = r4.f3077;
                this.f3078 = r4.f3078;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4967(Canvas canvas, ColorFilter colorFilter, Rect rect) {
            canvas.drawBitmap(this.f3079, (Rect) null, rect, m4965(colorFilter));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4968() {
            return this.f3075.getRootAlpha() < 255;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public Paint m4965(ColorFilter colorFilter) {
            if (!m4968() && colorFilter == null) {
                return null;
            }
            if (this.f3085 == null) {
                this.f3085 = new Paint();
                this.f3085.setFilterBitmap(true);
            }
            this.f3085.setAlpha(this.f3075.getRootAlpha());
            this.f3085.setColorFilter(colorFilter);
            return this.f3085;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4966(int i, int i2) {
            this.f3079.eraseColor(0);
            this.f3075.m4964(new Canvas(this.f3079), i, i2, (ColorFilter) null);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m4969(int i, int i2) {
            if (this.f3079 == null || !m4972(i, i2)) {
                this.f3079 = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
                this.f3084 = true;
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m4972(int i, int i2) {
            return i == this.f3079.getWidth() && i2 == this.f3079.getHeight();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m4970() {
            return !this.f3084 && this.f3080 == this.f3076 && this.f3081 == this.f3077 && this.f3083 == this.f3078 && this.f3082 == this.f3075.getRootAlpha();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m4971() {
            this.f3080 = this.f3076;
            this.f3081 = this.f3077;
            this.f3082 = this.f3075.getRootAlpha();
            this.f3083 = this.f3078;
            this.f3084 = false;
        }

        public C0766() {
            this.f3076 = null;
            this.f3077 = C0760.f3019;
            this.f3075 = new C0765();
        }

        public Drawable newDrawable() {
            return new C0760(this);
        }

        public Drawable newDrawable(Resources resources) {
            return new C0760(this);
        }

        public int getChangingConfigurations() {
            return this.f3074;
        }
    }

    /* renamed from: android.support.ʼ.ʻ.ˊ$ʿ  reason: contains not printable characters */
    /* compiled from: VectorDrawableCompat */
    private static class C0765 {

        /* renamed from: ˎ  reason: contains not printable characters */
        private static final Matrix f3058 = new Matrix();

        /* renamed from: ʻ  reason: contains not printable characters */
        final C0763 f3059;

        /* renamed from: ʼ  reason: contains not printable characters */
        float f3060;

        /* renamed from: ʽ  reason: contains not printable characters */
        float f3061;

        /* renamed from: ʾ  reason: contains not printable characters */
        float f3062;

        /* renamed from: ʿ  reason: contains not printable characters */
        float f3063;

        /* renamed from: ˆ  reason: contains not printable characters */
        int f3064;

        /* renamed from: ˈ  reason: contains not printable characters */
        String f3065;

        /* renamed from: ˉ  reason: contains not printable characters */
        final C0331<String, Object> f3066;

        /* renamed from: ˊ  reason: contains not printable characters */
        private final Path f3067;

        /* renamed from: ˋ  reason: contains not printable characters */
        private final Path f3068;

        /* renamed from: ˏ  reason: contains not printable characters */
        private final Matrix f3069;
        /* access modifiers changed from: private */

        /* renamed from: ˑ  reason: contains not printable characters */
        public Paint f3070;
        /* access modifiers changed from: private */

        /* renamed from: י  reason: contains not printable characters */
        public Paint f3071;

        /* renamed from: ـ  reason: contains not printable characters */
        private PathMeasure f3072;

        /* renamed from: ٴ  reason: contains not printable characters */
        private int f3073;

        /* renamed from: ʻ  reason: contains not printable characters */
        private static float m4956(float f, float f2, float f3, float f4) {
            return (f * f4) - (f2 * f3);
        }

        public C0765() {
            this.f3069 = new Matrix();
            this.f3060 = 0.0f;
            this.f3061 = 0.0f;
            this.f3062 = 0.0f;
            this.f3063 = 0.0f;
            this.f3064 = 255;
            this.f3065 = null;
            this.f3066 = new C0331<>();
            this.f3059 = new C0763();
            this.f3067 = new Path();
            this.f3068 = new Path();
        }

        public void setRootAlpha(int i) {
            this.f3064 = i;
        }

        public int getRootAlpha() {
            return this.f3064;
        }

        public void setAlpha(float f) {
            setRootAlpha((int) (f * 255.0f));
        }

        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        public C0765(C0765 r4) {
            this.f3069 = new Matrix();
            this.f3060 = 0.0f;
            this.f3061 = 0.0f;
            this.f3062 = 0.0f;
            this.f3063 = 0.0f;
            this.f3064 = 255;
            this.f3065 = null;
            this.f3066 = new C0331<>();
            this.f3059 = new C0763(r4.f3059, this.f3066);
            this.f3067 = new Path(r4.f3067);
            this.f3068 = new Path(r4.f3068);
            this.f3060 = r4.f3060;
            this.f3061 = r4.f3061;
            this.f3062 = r4.f3062;
            this.f3063 = r4.f3063;
            this.f3073 = r4.f3073;
            this.f3064 = r4.f3064;
            this.f3065 = r4.f3065;
            String str = r4.f3065;
            if (str != null) {
                this.f3066.put(str, this);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4960(C0763 r10, Matrix matrix, Canvas canvas, int i, int i2, ColorFilter colorFilter) {
            r10.f3045.set(matrix);
            r10.f3045.preConcat(r10.f3052);
            canvas.save();
            for (int i3 = 0; i3 < r10.f3042.size(); i3++) {
                Object obj = r10.f3042.get(i3);
                if (obj instanceof C0763) {
                    m4960((C0763) obj, r10.f3045, canvas, i, i2, colorFilter);
                } else if (obj instanceof C0764) {
                    m4961(r10, (C0764) obj, canvas, i, i2, colorFilter);
                }
            }
            canvas.restore();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4964(Canvas canvas, int i, int i2, ColorFilter colorFilter) {
            m4960(this.f3059, f3058, canvas, i, i2, colorFilter);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4961(C0763 r8, C0764 r9, Canvas canvas, int i, int i2, ColorFilter colorFilter) {
            float f = ((float) i) / this.f3062;
            float f2 = ((float) i2) / this.f3063;
            float min = Math.min(f, f2);
            Matrix r82 = r8.f3045;
            this.f3069.set(r82);
            this.f3069.postScale(f, f2);
            float r83 = m4957(r82);
            if (r83 != 0.0f) {
                r9.m4954(this.f3067);
                Path path = this.f3067;
                this.f3068.reset();
                if (r9.m4955()) {
                    this.f3068.addPath(path, this.f3069);
                    canvas.clipPath(this.f3068);
                    return;
                }
                C0762 r92 = (C0762) r9;
                if (!(r92.f3035 == 0.0f && r92.f3036 == 1.0f)) {
                    float f3 = (r92.f3035 + r92.f3037) % 1.0f;
                    float f4 = (r92.f3036 + r92.f3037) % 1.0f;
                    if (this.f3072 == null) {
                        this.f3072 = new PathMeasure();
                    }
                    this.f3072.setPath(this.f3067, false);
                    float length = this.f3072.getLength();
                    float f5 = f3 * length;
                    float f6 = f4 * length;
                    path.reset();
                    if (f5 > f6) {
                        this.f3072.getSegment(f5, length, path, true);
                        this.f3072.getSegment(0.0f, f6, path, true);
                    } else {
                        this.f3072.getSegment(f5, f6, path, true);
                    }
                    path.rLineTo(0.0f, 0.0f);
                }
                this.f3068.addPath(path, this.f3069);
                if (r92.f3031 != 0) {
                    if (this.f3071 == null) {
                        this.f3071 = new Paint();
                        this.f3071.setStyle(Paint.Style.FILL);
                        this.f3071.setAntiAlias(true);
                    }
                    Paint paint = this.f3071;
                    paint.setColor(C0760.m4932(r92.f3031, r92.f3034));
                    paint.setColorFilter(colorFilter);
                    this.f3068.setFillType(r92.f3033 == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    canvas.drawPath(this.f3068, paint);
                }
                if (r92.f3029 != 0) {
                    if (this.f3070 == null) {
                        this.f3070 = new Paint();
                        this.f3070.setStyle(Paint.Style.STROKE);
                        this.f3070.setAntiAlias(true);
                    }
                    Paint paint2 = this.f3070;
                    if (r92.f3039 != null) {
                        paint2.setStrokeJoin(r92.f3039);
                    }
                    if (r92.f3038 != null) {
                        paint2.setStrokeCap(r92.f3038);
                    }
                    paint2.setStrokeMiter(r92.f3040);
                    paint2.setColor(C0760.m4932(r92.f3029, r92.f3032));
                    paint2.setColorFilter(colorFilter);
                    paint2.setStrokeWidth(r92.f3030 * min * r83);
                    canvas.drawPath(this.f3068, paint2);
                }
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private float m4957(Matrix matrix) {
            float[] fArr = {0.0f, 1.0f, 1.0f, 0.0f};
            matrix.mapVectors(fArr);
            float r10 = m4956(fArr[0], fArr[1], fArr[2], fArr[3]);
            float max = Math.max((float) Math.hypot((double) fArr[0], (double) fArr[1]), (float) Math.hypot((double) fArr[2], (double) fArr[3]));
            if (max > 0.0f) {
                return Math.abs(r10) / max;
            }
            return 0.0f;
        }
    }

    /* renamed from: android.support.ʼ.ʻ.ˊ$ʽ  reason: contains not printable characters */
    /* compiled from: VectorDrawableCompat */
    private static class C0763 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final ArrayList<Object> f3042 = new ArrayList<>();

        /* renamed from: ʼ  reason: contains not printable characters */
        float f3043 = 0.0f;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f3044;
        /* access modifiers changed from: private */

        /* renamed from: ʾ  reason: contains not printable characters */
        public final Matrix f3045 = new Matrix();

        /* renamed from: ʿ  reason: contains not printable characters */
        private float f3046 = 0.0f;

        /* renamed from: ˆ  reason: contains not printable characters */
        private float f3047 = 0.0f;

        /* renamed from: ˈ  reason: contains not printable characters */
        private float f3048 = 1.0f;

        /* renamed from: ˉ  reason: contains not printable characters */
        private float f3049 = 1.0f;

        /* renamed from: ˊ  reason: contains not printable characters */
        private float f3050 = 0.0f;

        /* renamed from: ˋ  reason: contains not printable characters */
        private float f3051 = 0.0f;
        /* access modifiers changed from: private */

        /* renamed from: ˎ  reason: contains not printable characters */
        public final Matrix f3052 = new Matrix();

        /* renamed from: ˏ  reason: contains not printable characters */
        private int[] f3053;

        /* renamed from: ˑ  reason: contains not printable characters */
        private String f3054 = null;

        public C0763(C0763 r5, C0331<String, Object> r6) {
            C0764 r2;
            this.f3043 = r5.f3043;
            this.f3046 = r5.f3046;
            this.f3047 = r5.f3047;
            this.f3048 = r5.f3048;
            this.f3049 = r5.f3049;
            this.f3050 = r5.f3050;
            this.f3051 = r5.f3051;
            this.f3053 = r5.f3053;
            this.f3054 = r5.f3054;
            this.f3044 = r5.f3044;
            String str = this.f3054;
            if (str != null) {
                r6.put(str, this);
            }
            this.f3052.set(r5.f3052);
            ArrayList<Object> arrayList = r5.f3042;
            for (int i = 0; i < arrayList.size(); i++) {
                Object obj = arrayList.get(i);
                if (obj instanceof C0763) {
                    this.f3042.add(new C0763((C0763) obj, r6));
                } else {
                    if (obj instanceof C0762) {
                        r2 = new C0762((C0762) obj);
                    } else if (obj instanceof C0761) {
                        r2 = new C0761((C0761) obj);
                    } else {
                        throw new IllegalStateException("Unknown object in the tree!");
                    }
                    this.f3042.add(r2);
                    if (r2.f3056 != null) {
                        r6.put(r2.f3056, r2);
                    }
                }
            }
        }

        public C0763() {
        }

        public String getGroupName() {
            return this.f3054;
        }

        public Matrix getLocalMatrix() {
            return this.f3052;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4953(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray r2 = C0108.m566(resources, theme, attributeSet, C0749.f2990);
            m4951(r2, xmlPullParser);
            r2.recycle();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4951(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.f3053 = null;
            this.f3043 = C0108.m564(typedArray, xmlPullParser, "rotation", 5, this.f3043);
            this.f3046 = typedArray.getFloat(1, this.f3046);
            this.f3047 = typedArray.getFloat(2, this.f3047);
            this.f3048 = C0108.m564(typedArray, xmlPullParser, "scaleX", 3, this.f3048);
            this.f3049 = C0108.m564(typedArray, xmlPullParser, "scaleY", 4, this.f3049);
            this.f3050 = C0108.m564(typedArray, xmlPullParser, "translateX", 6, this.f3050);
            this.f3051 = C0108.m564(typedArray, xmlPullParser, "translateY", 7, this.f3051);
            String string = typedArray.getString(0);
            if (string != null) {
                this.f3054 = string;
            }
            m4950();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4950() {
            this.f3052.reset();
            this.f3052.postTranslate(-this.f3046, -this.f3047);
            this.f3052.postScale(this.f3048, this.f3049);
            this.f3052.postRotate(this.f3043, 0.0f, 0.0f);
            this.f3052.postTranslate(this.f3050 + this.f3046, this.f3051 + this.f3047);
        }

        public float getRotation() {
            return this.f3043;
        }

        public void setRotation(float f) {
            if (f != this.f3043) {
                this.f3043 = f;
                m4950();
            }
        }

        public float getPivotX() {
            return this.f3046;
        }

        public void setPivotX(float f) {
            if (f != this.f3046) {
                this.f3046 = f;
                m4950();
            }
        }

        public float getPivotY() {
            return this.f3047;
        }

        public void setPivotY(float f) {
            if (f != this.f3047) {
                this.f3047 = f;
                m4950();
            }
        }

        public float getScaleX() {
            return this.f3048;
        }

        public void setScaleX(float f) {
            if (f != this.f3048) {
                this.f3048 = f;
                m4950();
            }
        }

        public float getScaleY() {
            return this.f3049;
        }

        public void setScaleY(float f) {
            if (f != this.f3049) {
                this.f3049 = f;
                m4950();
            }
        }

        public float getTranslateX() {
            return this.f3050;
        }

        public void setTranslateX(float f) {
            if (f != this.f3050) {
                this.f3050 = f;
                m4950();
            }
        }

        public float getTranslateY() {
            return this.f3051;
        }

        public void setTranslateY(float f) {
            if (f != this.f3051) {
                this.f3051 = f;
                m4950();
            }
        }
    }

    /* renamed from: android.support.ʼ.ʻ.ˊ$ʾ  reason: contains not printable characters */
    /* compiled from: VectorDrawableCompat */
    private static class C0764 {

        /* renamed from: ˑ  reason: contains not printable characters */
        protected C0303.C0305[] f3055 = null;

        /* renamed from: י  reason: contains not printable characters */
        String f3056;

        /* renamed from: ـ  reason: contains not printable characters */
        int f3057;

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4955() {
            return false;
        }

        public C0764() {
        }

        public C0764(C0764 r2) {
            this.f3056 = r2.f3056;
            this.f3057 = r2.f3057;
            this.f3055 = C0303.m1770(r2.f3055);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4954(Path path) {
            path.reset();
            C0303.C0305[] r0 = this.f3055;
            if (r0 != null) {
                C0303.C0305.m1777(r0, path);
            }
        }

        public String getPathName() {
            return this.f3056;
        }

        public C0303.C0305[] getPathData() {
            return this.f3055;
        }

        public void setPathData(C0303.C0305[] r2) {
            if (!C0303.m1768(this.f3055, r2)) {
                this.f3055 = C0303.m1770(r2);
            } else {
                C0303.m1771(this.f3055, r2);
            }
        }
    }

    /* renamed from: android.support.ʼ.ʻ.ˊ$ʻ  reason: contains not printable characters */
    /* compiled from: VectorDrawableCompat */
    private static class C0761 extends C0764 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m4944() {
            return true;
        }

        public C0761() {
        }

        public C0761(C0761 r1) {
            super(r1);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4943(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            if (C0108.m569(xmlPullParser, "pathData")) {
                TypedArray r2 = C0108.m566(resources, theme, attributeSet, C0749.f2992);
                m4942(r2);
                r2.recycle();
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4942(TypedArray typedArray) {
            String string = typedArray.getString(0);
            if (string != null) {
                this.f3056 = string;
            }
            String string2 = typedArray.getString(1);
            if (string2 != null) {
                this.f3055 = C0303.m1772(string2);
            }
        }
    }

    /* renamed from: android.support.ʼ.ʻ.ˊ$ʼ  reason: contains not printable characters */
    /* compiled from: VectorDrawableCompat */
    private static class C0762 extends C0764 {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f3029 = 0;

        /* renamed from: ʼ  reason: contains not printable characters */
        float f3030 = 0.0f;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f3031 = 0;

        /* renamed from: ʾ  reason: contains not printable characters */
        float f3032 = 1.0f;

        /* renamed from: ʿ  reason: contains not printable characters */
        int f3033 = 0;

        /* renamed from: ˆ  reason: contains not printable characters */
        float f3034 = 1.0f;

        /* renamed from: ˈ  reason: contains not printable characters */
        float f3035 = 0.0f;

        /* renamed from: ˉ  reason: contains not printable characters */
        float f3036 = 1.0f;

        /* renamed from: ˊ  reason: contains not printable characters */
        float f3037 = 0.0f;

        /* renamed from: ˋ  reason: contains not printable characters */
        Paint.Cap f3038 = Paint.Cap.BUTT;

        /* renamed from: ˎ  reason: contains not printable characters */
        Paint.Join f3039 = Paint.Join.MITER;

        /* renamed from: ˏ  reason: contains not printable characters */
        float f3040 = 4.0f;

        /* renamed from: ٴ  reason: contains not printable characters */
        private int[] f3041;

        public C0762() {
        }

        public C0762(C0762 r4) {
            super(r4);
            this.f3041 = r4.f3041;
            this.f3029 = r4.f3029;
            this.f3030 = r4.f3030;
            this.f3032 = r4.f3032;
            this.f3031 = r4.f3031;
            this.f3033 = r4.f3033;
            this.f3034 = r4.f3034;
            this.f3035 = r4.f3035;
            this.f3036 = r4.f3036;
            this.f3037 = r4.f3037;
            this.f3038 = r4.f3038;
            this.f3039 = r4.f3039;
            this.f3040 = r4.f3040;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private Paint.Cap m4945(int i, Paint.Cap cap) {
            if (i == 0) {
                return Paint.Cap.BUTT;
            }
            if (i != 1) {
                return i != 2 ? cap : Paint.Cap.SQUARE;
            }
            return Paint.Cap.ROUND;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private Paint.Join m4946(int i, Paint.Join join) {
            if (i == 0) {
                return Paint.Join.MITER;
            }
            if (i != 1) {
                return i != 2 ? join : Paint.Join.BEVEL;
            }
            return Paint.Join.ROUND;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4948(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray r2 = C0108.m566(resources, theme, attributeSet, C0749.f2991);
            m4947(r2, xmlPullParser);
            r2.recycle();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m4947(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.f3041 = null;
            if (C0108.m569(xmlPullParser, "pathData")) {
                String string = typedArray.getString(0);
                if (string != null) {
                    this.f3056 = string;
                }
                String string2 = typedArray.getString(2);
                if (string2 != null) {
                    this.f3055 = C0303.m1772(string2);
                }
                this.f3031 = C0108.m570(typedArray, xmlPullParser, "fillColor", 1, this.f3031);
                this.f3034 = C0108.m564(typedArray, xmlPullParser, "fillAlpha", 12, this.f3034);
                this.f3038 = m4945(C0108.m565(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.f3038);
                this.f3039 = m4946(C0108.m565(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.f3039);
                this.f3040 = C0108.m564(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.f3040);
                this.f3029 = C0108.m570(typedArray, xmlPullParser, "strokeColor", 3, this.f3029);
                this.f3032 = C0108.m564(typedArray, xmlPullParser, "strokeAlpha", 11, this.f3032);
                this.f3030 = C0108.m564(typedArray, xmlPullParser, "strokeWidth", 4, this.f3030);
                this.f3036 = C0108.m564(typedArray, xmlPullParser, "trimPathEnd", 6, this.f3036);
                this.f3037 = C0108.m564(typedArray, xmlPullParser, "trimPathOffset", 7, this.f3037);
                this.f3035 = C0108.m564(typedArray, xmlPullParser, "trimPathStart", 5, this.f3035);
                this.f3033 = C0108.m565(typedArray, xmlPullParser, "fillType", 13, this.f3033);
            }
        }

        /* access modifiers changed from: package-private */
        public int getStrokeColor() {
            return this.f3029;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeColor(int i) {
            this.f3029 = i;
        }

        /* access modifiers changed from: package-private */
        public float getStrokeWidth() {
            return this.f3030;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeWidth(float f) {
            this.f3030 = f;
        }

        /* access modifiers changed from: package-private */
        public float getStrokeAlpha() {
            return this.f3032;
        }

        /* access modifiers changed from: package-private */
        public void setStrokeAlpha(float f) {
            this.f3032 = f;
        }

        /* access modifiers changed from: package-private */
        public int getFillColor() {
            return this.f3031;
        }

        /* access modifiers changed from: package-private */
        public void setFillColor(int i) {
            this.f3031 = i;
        }

        /* access modifiers changed from: package-private */
        public float getFillAlpha() {
            return this.f3034;
        }

        /* access modifiers changed from: package-private */
        public void setFillAlpha(float f) {
            this.f3034 = f;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathStart() {
            return this.f3035;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathStart(float f) {
            this.f3035 = f;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathEnd() {
            return this.f3036;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathEnd(float f) {
            this.f3036 = f;
        }

        /* access modifiers changed from: package-private */
        public float getTrimPathOffset() {
            return this.f3037;
        }

        /* access modifiers changed from: package-private */
        public void setTrimPathOffset(float f) {
            this.f3037 = f;
        }
    }
}
