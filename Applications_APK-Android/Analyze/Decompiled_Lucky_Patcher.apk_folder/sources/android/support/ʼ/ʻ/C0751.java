package android.support.ʼ.ʻ;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ʻ.C0108;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ˈ.C0331;
import android.util.AttributeSet;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: android.support.ʼ.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: AnimatedVectorDrawableCompat */
public class C0751 extends C0759 implements C0750 {

    /* renamed from: ʻ  reason: contains not printable characters */
    final Drawable.Callback f3001;

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0752 f3002;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Context f3003;

    /* renamed from: ʿ  reason: contains not printable characters */
    private ArgbEvaluator f3004;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Animator.AnimatorListener f3005;

    /* renamed from: ˈ  reason: contains not printable characters */
    private ArrayList<Object> f3006;

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i) {
        super.setChangingConfigurations(i);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i, PorterDuff.Mode mode) {
        super.setColorFilter(i, mode);
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f, float f2) {
        super.setHotspot(f, f2);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    C0751() {
        this(null, null, null);
    }

    private C0751(Context context) {
        this(context, null, null);
    }

    private C0751(Context context, C0752 r4, Resources resources) {
        this.f3004 = null;
        this.f3005 = null;
        this.f3006 = null;
        this.f3001 = new Drawable.Callback() {
            public void invalidateDrawable(Drawable drawable) {
                C0751.this.invalidateSelf();
            }

            public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
                C0751.this.scheduleSelf(runnable, j);
            }

            public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
                C0751.this.unscheduleSelf(runnable);
            }
        };
        this.f3003 = context;
        if (r4 != null) {
            this.f3002 = r4;
        } else {
            this.f3002 = new C0752(context, r4, this.f3001, resources);
        }
    }

    public Drawable mutate() {
        if (this.f3018 != null) {
            this.f3018.mutate();
        }
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static C0751 m4899(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        C0751 r0 = new C0751(context);
        r0.inflate(resources, xmlPullParser, attributeSet, theme);
        return r0;
    }

    public Drawable.ConstantState getConstantState() {
        if (this.f3018 == null || Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new C0753(this.f3018.getConstantState());
    }

    public int getChangingConfigurations() {
        if (this.f3018 != null) {
            return this.f3018.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f3002.f3008;
    }

    public void draw(Canvas canvas) {
        if (this.f3018 != null) {
            this.f3018.draw(canvas);
            return;
        }
        this.f3002.f3009.draw(canvas);
        if (this.f3002.f3010.isStarted()) {
            invalidateSelf();
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f3018 != null) {
            this.f3018.setBounds(rect);
        } else {
            this.f3002.f3009.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        if (this.f3018 != null) {
            return this.f3018.setState(iArr);
        }
        return this.f3002.f3009.setState(iArr);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        if (this.f3018 != null) {
            return this.f3018.setLevel(i);
        }
        return this.f3002.f3009.setLevel(i);
    }

    public int getAlpha() {
        if (this.f3018 != null) {
            return C0288.m1710(this.f3018);
        }
        return this.f3002.f3009.getAlpha();
    }

    public void setAlpha(int i) {
        if (this.f3018 != null) {
            this.f3018.setAlpha(i);
        } else {
            this.f3002.f3009.setAlpha(i);
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f3018 != null) {
            this.f3018.setColorFilter(colorFilter);
        } else {
            this.f3002.f3009.setColorFilter(colorFilter);
        }
    }

    public void setTint(int i) {
        if (this.f3018 != null) {
            C0288.m1701(this.f3018, i);
        } else {
            this.f3002.f3009.setTint(i);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.f3018 != null) {
            C0288.m1703(this.f3018, colorStateList);
        } else {
            this.f3002.f3009.setTintList(colorStateList);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (this.f3018 != null) {
            C0288.m1706(this.f3018, mode);
        } else {
            this.f3002.f3009.setTintMode(mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.f3018 != null) {
            return this.f3018.setVisible(z, z2);
        }
        this.f3002.f3009.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    public boolean isStateful() {
        if (this.f3018 != null) {
            return this.f3018.isStateful();
        }
        return this.f3002.f3009.isStateful();
    }

    public int getOpacity() {
        if (this.f3018 != null) {
            return this.f3018.getOpacity();
        }
        return this.f3002.f3009.getOpacity();
    }

    public int getIntrinsicWidth() {
        if (this.f3018 != null) {
            return this.f3018.getIntrinsicWidth();
        }
        return this.f3002.f3009.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        if (this.f3018 != null) {
            return this.f3018.getIntrinsicHeight();
        }
        return this.f3002.f3009.getIntrinsicHeight();
    }

    public boolean isAutoMirrored() {
        if (this.f3018 != null) {
            return C0288.m1708(this.f3018);
        }
        return this.f3002.f3009.isAutoMirrored();
    }

    public void setAutoMirrored(boolean z) {
        if (this.f3018 != null) {
            C0288.m1707(this.f3018, z);
        } else {
            this.f3002.f3009.setAutoMirrored(z);
        }
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        if (this.f3018 != null) {
            C0288.m1705(this.f3018, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    TypedArray r0 = C0108.m566(resources, theme, attributeSet, C0749.f2993);
                    int resourceId = r0.getResourceId(0, 0);
                    if (resourceId != 0) {
                        C0760 r3 = C0760.m4934(resources, resourceId, theme);
                        r3.m4941(false);
                        r3.setCallback(this.f3001);
                        if (this.f3002.f3009 != null) {
                            this.f3002.f3009.setCallback(null);
                        }
                        this.f3002.f3009 = r3;
                    }
                    r0.recycle();
                } else if ("target".equals(name)) {
                    TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, C0749.f2994);
                    String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        Context context = this.f3003;
                        if (context != null) {
                            m4901(string, C0755.m4909(context, resourceId2));
                        } else {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.f3002.m4904();
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        inflate(resources, xmlPullParser, attributeSet, null);
    }

    public void applyTheme(Resources.Theme theme) {
        if (this.f3018 != null) {
            C0288.m1704(this.f3018, theme);
        }
    }

    public boolean canApplyTheme() {
        if (this.f3018 != null) {
            return C0288.m1711(this.f3018);
        }
        return false;
    }

    /* renamed from: android.support.ʼ.ʻ.ʽ$ʼ  reason: contains not printable characters */
    /* compiled from: AnimatedVectorDrawableCompat */
    private static class C0753 extends Drawable.ConstantState {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Drawable.ConstantState f3013;

        public C0753(Drawable.ConstantState constantState) {
            this.f3013 = constantState;
        }

        public Drawable newDrawable() {
            C0751 r0 = new C0751();
            r0.f3018 = this.f3013.newDrawable();
            r0.f3018.setCallback(r0.f3001);
            return r0;
        }

        public Drawable newDrawable(Resources resources) {
            C0751 r0 = new C0751();
            r0.f3018 = this.f3013.newDrawable(resources);
            r0.f3018.setCallback(r0.f3001);
            return r0;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            C0751 r0 = new C0751();
            r0.f3018 = this.f3013.newDrawable(resources, theme);
            r0.f3018.setCallback(r0.f3001);
            return r0;
        }

        public boolean canApplyTheme() {
            return this.f3013.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f3013.getChangingConfigurations();
        }
    }

    /* renamed from: android.support.ʼ.ʻ.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: AnimatedVectorDrawableCompat */
    private static class C0752 extends Drawable.ConstantState {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f3008;

        /* renamed from: ʼ  reason: contains not printable characters */
        C0760 f3009;

        /* renamed from: ʽ  reason: contains not printable characters */
        AnimatorSet f3010;

        /* renamed from: ʾ  reason: contains not printable characters */
        C0331<Animator, String> f3011;
        /* access modifiers changed from: private */

        /* renamed from: ʿ  reason: contains not printable characters */
        public ArrayList<Animator> f3012;

        public C0752(Context context, C0752 r4, Drawable.Callback callback, Resources resources) {
            if (r4 != null) {
                this.f3008 = r4.f3008;
                C0760 r3 = r4.f3009;
                if (r3 != null) {
                    Drawable.ConstantState constantState = r3.getConstantState();
                    if (resources != null) {
                        this.f3009 = (C0760) constantState.newDrawable(resources);
                    } else {
                        this.f3009 = (C0760) constantState.newDrawable();
                    }
                    this.f3009 = (C0760) this.f3009.mutate();
                    this.f3009.setCallback(callback);
                    this.f3009.setBounds(r4.f3009.getBounds());
                    this.f3009.m4941(false);
                }
                ArrayList<Animator> arrayList = r4.f3012;
                if (arrayList != null) {
                    int size = arrayList.size();
                    this.f3012 = new ArrayList<>(size);
                    this.f3011 = new C0331<>(size);
                    for (int i = 0; i < size; i++) {
                        Animator animator = r4.f3012.get(i);
                        Animator clone = animator.clone();
                        String str = r4.f3011.get(animator);
                        clone.setTarget(this.f3009.m4940(str));
                        this.f3012.add(clone);
                        this.f3011.put(clone, str);
                    }
                    m4904();
                }
            }
        }

        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public int getChangingConfigurations() {
            return this.f3008;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m4904() {
            if (this.f3010 == null) {
                this.f3010 = new AnimatorSet();
            }
            this.f3010.playTogether(this.f3012);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4900(Animator animator) {
        ArrayList<Animator> childAnimations;
        if ((animator instanceof AnimatorSet) && (childAnimations = ((AnimatorSet) animator).getChildAnimations()) != null) {
            for (int i = 0; i < childAnimations.size(); i++) {
                m4900(childAnimations.get(i));
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.f3004 == null) {
                    this.f3004 = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.f3004);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m4901(String str, Animator animator) {
        animator.setTarget(this.f3002.f3009.m4940(str));
        if (Build.VERSION.SDK_INT < 21) {
            m4900(animator);
        }
        if (this.f3002.f3012 == null) {
            ArrayList unused = this.f3002.f3012 = new ArrayList();
            this.f3002.f3011 = new C0331<>();
        }
        this.f3002.f3012.add(animator);
        this.f3002.f3011.put(animator, str);
    }

    public boolean isRunning() {
        if (this.f3018 != null) {
            return ((AnimatedVectorDrawable) this.f3018).isRunning();
        }
        return this.f3002.f3010.isRunning();
    }

    public void start() {
        if (this.f3018 != null) {
            ((AnimatedVectorDrawable) this.f3018).start();
        } else if (!this.f3002.f3010.isStarted()) {
            this.f3002.f3010.start();
            invalidateSelf();
        }
    }

    public void stop() {
        if (this.f3018 != null) {
            ((AnimatedVectorDrawable) this.f3018).stop();
        } else {
            this.f3002.f3010.end();
        }
    }
}
