package android.support.design.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.design.C0089;
import android.support.v4.ˉ.C0412;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0439;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

/* renamed from: android.support.design.internal.ˆ  reason: contains not printable characters */
/* compiled from: ScrimInsetsFrameLayout */
public class C0035 extends FrameLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    Drawable f86;

    /* renamed from: ʼ  reason: contains not printable characters */
    Rect f87;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Rect f88;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m105(C0439 r1) {
    }

    public C0035(Context context) {
        this(context, null);
    }

    public C0035(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public C0035(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f88 = new Rect();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.ScrimInsetsFrameLayout, i, C0089.C0097.Widget_Design_ScrimInsetsFrameLayout);
        this.f86 = obtainStyledAttributes.getDrawable(C0089.C0098.ScrimInsetsFrameLayout_insetForeground);
        obtainStyledAttributes.recycle();
        setWillNotDraw(true);
        C0414.m2225(this, new C0412() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0439 m106(View view, C0439 r6) {
                if (C0035.this.f87 == null) {
                    C0035.this.f87 = new Rect();
                }
                C0035.this.f87.set(r6.m2397(), r6.m2399(), r6.m2400(), r6.m2401());
                C0035.this.m105(r6);
                C0035.this.setWillNotDraw(!r6.m2402() || C0035.this.f86 == null);
                C0414.m2233(C0035.this);
                return r6.m2404();
            }
        });
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        int width = getWidth();
        int height = getHeight();
        if (this.f87 != null && this.f86 != null) {
            int save = canvas.save();
            canvas.translate((float) getScrollX(), (float) getScrollY());
            this.f88.set(0, 0, width, this.f87.top);
            this.f86.setBounds(this.f88);
            this.f86.draw(canvas);
            this.f88.set(0, height - this.f87.bottom, width, height);
            this.f86.setBounds(this.f88);
            this.f86.draw(canvas);
            this.f88.set(0, this.f87.top, this.f87.left, height - this.f87.bottom);
            this.f86.setBounds(this.f88);
            this.f86.draw(canvas);
            this.f88.set(width - this.f87.right, this.f87.top, width, height - this.f87.bottom);
            this.f86.setBounds(this.f88);
            this.f86.draw(canvas);
            canvas.restoreToCount(save);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Drawable drawable = this.f86;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Drawable drawable = this.f86;
        if (drawable != null) {
            drawable.setCallback(null);
        }
    }
}
