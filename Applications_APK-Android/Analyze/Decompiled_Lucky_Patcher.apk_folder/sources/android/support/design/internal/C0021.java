package android.support.design.internal;

import android.content.Context;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0508;
import android.view.SubMenu;

/* renamed from: android.support.design.internal.ʼ  reason: contains not printable characters */
/* compiled from: NavigationMenu */
public class C0021 extends C0504 {
    public C0021(Context context) {
        super(context);
    }

    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        C0508 r1 = (C0508) m2890(i, i2, i3, charSequence);
        C0033 r2 = new C0033(m2918(), this, r1);
        r1.m2947(r2);
        return r2;
    }
}
