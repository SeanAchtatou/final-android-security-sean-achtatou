package android.support.design.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.design.C0089;
import android.support.v7.widget.C0652;
import android.util.AttributeSet;
import android.view.Gravity;

/* renamed from: android.support.design.internal.ʻ  reason: contains not printable characters */
/* compiled from: ForegroundLinearLayout */
public class C0020 extends C0652 {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected boolean f56;

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean f57;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Drawable f58;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final Rect f59;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Rect f60;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f61;

    public C0020(Context context) {
        this(context, null);
    }

    public C0020(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public C0020(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f59 = new Rect();
        this.f60 = new Rect();
        this.f61 = 119;
        this.f56 = true;
        this.f57 = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.ForegroundLinearLayout, i, 0);
        this.f61 = obtainStyledAttributes.getInt(C0089.C0098.ForegroundLinearLayout_android_foregroundGravity, this.f61);
        Drawable drawable = obtainStyledAttributes.getDrawable(C0089.C0098.ForegroundLinearLayout_android_foreground);
        if (drawable != null) {
            setForeground(drawable);
        }
        this.f56 = obtainStyledAttributes.getBoolean(C0089.C0098.ForegroundLinearLayout_foregroundInsidePadding, true);
        obtainStyledAttributes.recycle();
    }

    public int getForegroundGravity() {
        return this.f61;
    }

    public void setForegroundGravity(int i) {
        if (this.f61 != i) {
            if ((8388615 & i) == 0) {
                i |= 8388611;
            }
            if ((i & 112) == 0) {
                i |= 48;
            }
            this.f61 = i;
            if (this.f61 == 119 && this.f58 != null) {
                this.f58.getPadding(new Rect());
            }
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f58;
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.f58;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.f58;
        if (drawable != null && drawable.isStateful()) {
            this.f58.setState(getDrawableState());
        }
    }

    public void setForeground(Drawable drawable) {
        Drawable drawable2 = this.f58;
        if (drawable2 != drawable) {
            if (drawable2 != null) {
                drawable2.setCallback(null);
                unscheduleDrawable(this.f58);
            }
            this.f58 = drawable;
            if (drawable != null) {
                setWillNotDraw(false);
                drawable.setCallback(this);
                if (drawable.isStateful()) {
                    drawable.setState(getDrawableState());
                }
                if (this.f61 == 119) {
                    drawable.getPadding(new Rect());
                }
            } else {
                setWillNotDraw(true);
            }
            requestLayout();
            invalidate();
        }
    }

    public Drawable getForeground() {
        return this.f58;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.f57 = z | this.f57;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f57 = true;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Drawable drawable = this.f58;
        if (drawable != null) {
            if (this.f57) {
                this.f57 = false;
                Rect rect = this.f59;
                Rect rect2 = this.f60;
                int right = getRight() - getLeft();
                int bottom = getBottom() - getTop();
                if (this.f56) {
                    rect.set(0, 0, right, bottom);
                } else {
                    rect.set(getPaddingLeft(), getPaddingTop(), right - getPaddingRight(), bottom - getPaddingBottom());
                }
                Gravity.apply(this.f61, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), rect, rect2);
                drawable.setBounds(rect2);
            }
            drawable.draw(canvas);
        }
    }

    public void drawableHotspotChanged(float f, float f2) {
        super.drawableHotspotChanged(f, f2);
        Drawable drawable = this.f58;
        if (drawable != null) {
            drawable.setHotspot(f, f2);
        }
    }
}
