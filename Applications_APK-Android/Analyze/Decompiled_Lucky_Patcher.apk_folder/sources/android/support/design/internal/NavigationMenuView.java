package android.support.design.internal;

import android.content.Context;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0520;
import android.support.v7.widget.C0690;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

public class NavigationMenuView extends C0690 implements C0520 {
    public int getWindowAnimations() {
        return 0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m51(C0504 r1) {
    }

    public NavigationMenuView(Context context) {
        this(context, null);
    }

    public NavigationMenuView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NavigationMenuView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setLayoutManager(new LinearLayoutManager(context, 1, false));
    }
}
