package android.support.design.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

/* renamed from: android.support.design.internal.ʿ  reason: contains not printable characters */
/* compiled from: ParcelableSparseArray */
public class C0034 extends SparseArray<Parcelable> implements Parcelable {
    public static final Parcelable.Creator<C0034> CREATOR = new Parcelable.ClassLoaderCreator<C0034>() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0034 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new C0034(parcel, classLoader);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0034 createFromParcel(Parcel parcel) {
            return new C0034(parcel, null);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0034[] newArray(int i) {
            return new C0034[i];
        }
    };

    public int describeContents() {
        return 0;
    }

    public C0034() {
    }

    public C0034(Parcel parcel, ClassLoader classLoader) {
        int readInt = parcel.readInt();
        int[] iArr = new int[readInt];
        parcel.readIntArray(iArr);
        Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
        for (int i = 0; i < readInt; i++) {
            put(iArr[i], readParcelableArray[i]);
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        int size = size();
        int[] iArr = new int[size];
        Parcelable[] parcelableArr = new Parcelable[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr[i2] = keyAt(i2);
            parcelableArr[i2] = (Parcelable) valueAt(i2);
        }
        parcel.writeInt(size);
        parcel.writeIntArray(iArr);
        parcel.writeParcelableArray(parcelableArr, i);
    }
}
