package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.C0089;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0439;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0508;
import android.support.v7.view.menu.C0518;
import android.support.v7.view.menu.C0520;
import android.support.v7.view.menu.C0526;
import android.support.v7.widget.C0690;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

/* renamed from: android.support.design.internal.ʽ  reason: contains not printable characters */
/* compiled from: NavigationMenuPresenter */
public class C0022 implements C0518 {

    /* renamed from: ʻ  reason: contains not printable characters */
    LinearLayout f62;

    /* renamed from: ʼ  reason: contains not printable characters */
    C0504 f63;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0024 f64;

    /* renamed from: ʾ  reason: contains not printable characters */
    LayoutInflater f65;

    /* renamed from: ʿ  reason: contains not printable characters */
    int f66;

    /* renamed from: ˆ  reason: contains not printable characters */
    boolean f67;

    /* renamed from: ˈ  reason: contains not printable characters */
    ColorStateList f68;

    /* renamed from: ˉ  reason: contains not printable characters */
    ColorStateList f69;

    /* renamed from: ˊ  reason: contains not printable characters */
    Drawable f70;

    /* renamed from: ˋ  reason: contains not printable characters */
    int f71;

    /* renamed from: ˎ  reason: contains not printable characters */
    final View.OnClickListener f72 = new View.OnClickListener() {
        public void onClick(View view) {
            C0022.this.m74(true);
            C0508 itemData = ((NavigationMenuItemView) view).getItemData();
            boolean r0 = C0022.this.f63.m2902(itemData, C0022.this, 0);
            if (itemData != null && itemData.isCheckable() && r0) {
                C0022.this.f64.m90(itemData);
            }
            C0022.this.m74(false);
            C0022.this.m67(false);
        }
    };

    /* renamed from: ˏ  reason: contains not printable characters */
    private NavigationMenuView f73;

    /* renamed from: ˑ  reason: contains not printable characters */
    private C0518.C0519 f74;

    /* renamed from: י  reason: contains not printable characters */
    private int f75;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f76;

    /* renamed from: android.support.design.internal.ʽ$ʾ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private interface C0026 {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m68() {
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m69(C0504 r1, C0508 r2) {
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m70(C0526 r1) {
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m75(C0504 r1, C0508 r2) {
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m58(Context context, C0504 r3) {
        this.f65 = LayoutInflater.from(context);
        this.f63 = r3;
        this.f71 = context.getResources().getDimensionPixelOffset(C0089.C0092.design_navigation_separator_vertical_padding);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.design.internal.NavigationMenuView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0520 m56(ViewGroup viewGroup) {
        if (this.f73 == null) {
            this.f73 = (NavigationMenuView) this.f65.inflate(C0089.C0096.design_navigation_menu, viewGroup, false);
            if (this.f64 == null) {
                this.f64 = new C0024();
            }
            this.f62 = (LinearLayout) this.f65.inflate(C0089.C0096.design_navigation_item_header, (ViewGroup) this.f73, false);
            this.f73.setAdapter(this.f64);
        }
        return this.f73;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m67(boolean z) {
        C0024 r1 = this.f64;
        if (r1 != null) {
            r1.m96();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m65(C0518.C0519 r1) {
        this.f74 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m63(C0504 r2, boolean z) {
        C0518.C0519 r0 = this.f74;
        if (r0 != null) {
            r0.m3027(r2, z);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m71() {
        return this.f75;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m57(int i) {
        this.f75 = i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Parcelable m76() {
        if (Build.VERSION.SDK_INT < 11) {
            return null;
        }
        Bundle bundle = new Bundle();
        if (this.f73 != null) {
            SparseArray sparseArray = new SparseArray();
            this.f73.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        C0024 r1 = this.f64;
        if (r1 != null) {
            bundle.putBundle("android:menu:adapter", r1.m97());
        }
        if (this.f62 != null) {
            SparseArray sparseArray2 = new SparseArray();
            this.f62.saveHierarchyState(sparseArray2);
            bundle.putSparseParcelableArray("android:menu:header", sparseArray2);
        }
        return bundle;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m61(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
            if (sparseParcelableArray != null) {
                this.f73.restoreHierarchyState(sparseParcelableArray);
            }
            Bundle bundle2 = bundle.getBundle("android:menu:adapter");
            if (bundle2 != null) {
                this.f64.m87(bundle2);
            }
            SparseArray sparseParcelableArray2 = bundle.getSparseParcelableArray("android:menu:header");
            if (sparseParcelableArray2 != null) {
                this.f62.restoreHierarchyState(sparseParcelableArray2);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m64(C0508 r2) {
        this.f64.m90(r2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: ʼ  reason: contains not printable characters */
    public View m72(int i) {
        View inflate = this.f65.inflate(i, (ViewGroup) this.f62, false);
        m66(inflate);
        return inflate;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m66(View view) {
        this.f62.addView(view);
        NavigationMenuView navigationMenuView = this.f73;
        navigationMenuView.setPadding(0, 0, 0, navigationMenuView.getPaddingBottom());
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m78() {
        return this.f62.getChildCount();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public ColorStateList m79() {
        return this.f69;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m59(ColorStateList colorStateList) {
        this.f69 = colorStateList;
        m67(false);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public ColorStateList m80() {
        return this.f68;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m73(ColorStateList colorStateList) {
        this.f68 = colorStateList;
        m67(false);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m77(int i) {
        this.f66 = i;
        this.f67 = true;
        m67(false);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public Drawable m81() {
        return this.f70;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m60(Drawable drawable) {
        this.f70 = drawable;
        m67(false);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m74(boolean z) {
        C0024 r0 = this.f64;
        if (r0 != null) {
            r0.m93(z);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m62(C0439 r5) {
        int r0 = r5.m2399();
        if (this.f76 != r0) {
            this.f76 = r0;
            if (this.f62.getChildCount() == 0) {
                NavigationMenuView navigationMenuView = this.f73;
                navigationMenuView.setPadding(0, this.f76, 0, navigationMenuView.getPaddingBottom());
            }
        }
        C0414.m2230(this.f62, r5);
    }

    /* renamed from: android.support.design.internal.ʽ$ˋ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private static abstract class C0032 extends C0690.C0720 {
        public C0032(View view) {
            super(view);
        }
    }

    /* renamed from: android.support.design.internal.ʽ$ˈ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private static class C0029 extends C0032 {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public C0029(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(C0089.C0096.design_navigation_item, viewGroup, false));
            this.f2914.setOnClickListener(onClickListener);
        }
    }

    /* renamed from: android.support.design.internal.ʽ$ˊ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private static class C0031 extends C0032 {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public C0031(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(C0089.C0096.design_navigation_item_subheader, viewGroup, false));
        }
    }

    /* renamed from: android.support.design.internal.ʽ$ˉ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private static class C0030 extends C0032 {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public C0030(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(C0089.C0096.design_navigation_item_separator, viewGroup, false));
        }
    }

    /* renamed from: android.support.design.internal.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private static class C0023 extends C0032 {
        public C0023(View view) {
            super(view);
        }
    }

    /* renamed from: android.support.design.internal.ʽ$ʼ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private class C0024 extends C0690.C0691<C0032> {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final ArrayList<C0026> f79 = new ArrayList<>();

        /* renamed from: ʽ  reason: contains not printable characters */
        private C0508 f80;

        /* renamed from: ʾ  reason: contains not printable characters */
        private boolean f81;

        /* renamed from: ʻ  reason: contains not printable characters */
        public long m85(int i) {
            return (long) i;
        }

        C0024() {
            m83();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m84() {
            return this.f79.size();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m94(int i) {
            C0026 r2 = this.f79.get(i);
            if (r2 instanceof C0027) {
                return 2;
            }
            if (r2 instanceof C0025) {
                return 3;
            }
            if (r2 instanceof C0028) {
                return ((C0028) r2).m100().hasSubMenu() ? 1 : 0;
            }
            throw new RuntimeException("Unknown item type.");
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0032 m95(ViewGroup viewGroup, int i) {
            if (i == 0) {
                return new C0029(C0022.this.f65, viewGroup, C0022.this.f72);
            }
            if (i == 1) {
                return new C0031(C0022.this.f65, viewGroup);
            }
            if (i == 2) {
                return new C0030(C0022.this.f65, viewGroup);
            }
            if (i != 3) {
                return null;
            }
            return new C0023(C0022.this.f62);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m92(C0032 r4, int i) {
            int r0 = m94(i);
            if (r0 == 0) {
                NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) r4.f2914;
                navigationMenuItemView.setIconTintList(C0022.this.f69);
                if (C0022.this.f67) {
                    navigationMenuItemView.setTextAppearance(C0022.this.f66);
                }
                if (C0022.this.f68 != null) {
                    navigationMenuItemView.setTextColor(C0022.this.f68);
                }
                C0414.m2223(navigationMenuItemView, C0022.this.f70 != null ? C0022.this.f70.getConstantState().newDrawable() : null);
                C0028 r5 = (C0028) this.f79.get(i);
                navigationMenuItemView.setNeedsEmptyIcon(r5.f84);
                navigationMenuItemView.m48(r5.m100(), 0);
            } else if (r0 == 1) {
                ((TextView) r4.f2914).setText(((C0028) this.f79.get(i)).m100().getTitle());
            } else if (r0 == 2) {
                C0027 r52 = (C0027) this.f79.get(i);
                r4.f2914.setPadding(0, r52.m98(), 0, r52.m99());
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m91(C0032 r2) {
            if (r2 instanceof C0029) {
                ((NavigationMenuItemView) r2.f2914).m47();
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m96() {
            m83();
            m4497();
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        private void m83() {
            if (!this.f81) {
                this.f81 = true;
                this.f79.clear();
                this.f79.add(new C0025());
                int size = C0022.this.f63.m2923().size();
                int i = -1;
                boolean z = false;
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    C0508 r8 = C0022.this.f63.m2923().get(i3);
                    if (r8.isChecked()) {
                        m90(r8);
                    }
                    if (r8.isCheckable()) {
                        r8.m2949(false);
                    }
                    if (r8.hasSubMenu()) {
                        SubMenu subMenu = r8.getSubMenu();
                        if (subMenu.hasVisibleItems()) {
                            if (i3 != 0) {
                                this.f79.add(new C0027(C0022.this.f71, 0));
                            }
                            this.f79.add(new C0028(r8));
                            int size2 = this.f79.size();
                            int size3 = subMenu.size();
                            boolean z2 = false;
                            for (int i4 = 0; i4 < size3; i4++) {
                                C0508 r14 = (C0508) subMenu.getItem(i4);
                                if (r14.isVisible()) {
                                    if (!z2 && r14.getIcon() != null) {
                                        z2 = true;
                                    }
                                    if (r14.isCheckable()) {
                                        r14.m2949(false);
                                    }
                                    if (r8.isChecked()) {
                                        m90(r8);
                                    }
                                    this.f79.add(new C0028(r14));
                                }
                            }
                            if (z2) {
                                m82(size2, this.f79.size());
                            }
                        }
                    } else {
                        int groupId = r8.getGroupId();
                        if (groupId != i) {
                            i2 = this.f79.size();
                            boolean z3 = r8.getIcon() != null;
                            if (i3 != 0) {
                                i2++;
                                this.f79.add(new C0027(C0022.this.f71, C0022.this.f71));
                            }
                            z = z3;
                        } else if (!z && r8.getIcon() != null) {
                            m82(i2, this.f79.size());
                            z = true;
                        }
                        C0028 r5 = new C0028(r8);
                        r5.f84 = z;
                        this.f79.add(r5);
                        i = groupId;
                    }
                }
                this.f81 = false;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m82(int i, int i2) {
            while (i < i2) {
                ((C0028) this.f79.get(i)).f84 = true;
                i++;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m90(C0508 r3) {
            if (this.f80 != r3 && r3.isCheckable()) {
                C0508 r0 = this.f80;
                if (r0 != null) {
                    r0.setChecked(false);
                }
                this.f80 = r3;
                r3.setChecked(true);
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Bundle m97() {
            Bundle bundle = new Bundle();
            C0508 r1 = this.f80;
            if (r1 != null) {
                bundle.putInt("android:menu:checked", r1.getItemId());
            }
            SparseArray sparseArray = new SparseArray();
            int size = this.f79.size();
            for (int i = 0; i < size; i++) {
                C0026 r4 = this.f79.get(i);
                if (r4 instanceof C0028) {
                    C0508 r42 = ((C0028) r4).m100();
                    View actionView = r42 != null ? r42.getActionView() : null;
                    if (actionView != null) {
                        C0034 r6 = new C0034();
                        actionView.saveHierarchyState(r6);
                        sparseArray.put(r42.getItemId(), r6);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:action_views", sparseArray);
            return bundle;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m87(Bundle bundle) {
            C0508 r2;
            View actionView;
            C0034 r22;
            C0508 r4;
            int i = bundle.getInt("android:menu:checked", 0);
            if (i != 0) {
                this.f81 = true;
                int size = this.f79.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    C0026 r42 = this.f79.get(i2);
                    if ((r42 instanceof C0028) && (r4 = ((C0028) r42).m100()) != null && r4.getItemId() == i) {
                        m90(r4);
                        break;
                    }
                    i2++;
                }
                this.f81 = false;
                m83();
            }
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:action_views");
            if (sparseParcelableArray != null) {
                int size2 = this.f79.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    C0026 r23 = this.f79.get(i3);
                    if (!(!(r23 instanceof C0028) || (r2 = ((C0028) r23).m100()) == null || (actionView = r2.getActionView()) == null || (r22 = (C0034) sparseParcelableArray.get(r2.getItemId())) == null)) {
                        actionView.restoreHierarchyState(r22);
                    }
                }
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m93(boolean z) {
            this.f81 = z;
        }
    }

    /* renamed from: android.support.design.internal.ʽ$ˆ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private static class C0028 implements C0026 {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f84;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final C0508 f85;

        C0028(C0508 r1) {
            this.f85 = r1;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public C0508 m100() {
            return this.f85;
        }
    }

    /* renamed from: android.support.design.internal.ʽ$ʿ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private static class C0027 implements C0026 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final int f82;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final int f83;

        public C0027(int i, int i2) {
            this.f82 = i;
            this.f83 = i2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m98() {
            return this.f82;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m99() {
            return this.f83;
        }
    }

    /* renamed from: android.support.design.internal.ʽ$ʽ  reason: contains not printable characters */
    /* compiled from: NavigationMenuPresenter */
    private static class C0025 implements C0026 {
        C0025() {
        }
    }
}
