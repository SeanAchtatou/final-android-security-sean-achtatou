package android.support.design.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.C0089;
import android.support.design.widget.C0057;
import android.support.v4.ˉ.C0414;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SnackbarContentLayout extends LinearLayout implements C0057.C0060 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private TextView f52;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Button f53;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f54;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f55;

    public SnackbarContentLayout(Context context) {
        this(context, null);
    }

    public SnackbarContentLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.SnackbarLayout);
        this.f54 = obtainStyledAttributes.getDimensionPixelSize(C0089.C0098.SnackbarLayout_android_maxWidth, -1);
        this.f55 = obtainStyledAttributes.getDimensionPixelSize(C0089.C0098.SnackbarLayout_maxActionInlineWidth, -1);
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.f52 = (TextView) findViewById(C0089.C0094.snackbar_text);
        this.f53 = (Button) findViewById(C0089.C0094.snackbar_action);
    }

    public TextView getMessageView() {
        return this.f52;
    }

    public Button getActionView() {
        return this.f53;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if (m53(1, r0, r0 - r1) != false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005e, code lost:
        if (m53(0, r0, r0) != false) goto L_0x0062;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r8, int r9) {
        /*
            r7 = this;
            super.onMeasure(r8, r9)
            int r0 = r7.f54
            if (r0 <= 0) goto L_0x0018
            int r0 = r7.getMeasuredWidth()
            int r1 = r7.f54
            if (r0 <= r1) goto L_0x0018
            r8 = 1073741824(0x40000000, float:2.0)
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r8)
            super.onMeasure(r8, r9)
        L_0x0018:
            android.content.res.Resources r0 = r7.getResources()
            int r1 = android.support.design.C0089.C0092.design_snackbar_padding_vertical_2lines
            int r0 = r0.getDimensionPixelSize(r1)
            android.content.res.Resources r1 = r7.getResources()
            int r2 = android.support.design.C0089.C0092.design_snackbar_padding_vertical
            int r1 = r1.getDimensionPixelSize(r2)
            android.widget.TextView r2 = r7.f52
            android.text.Layout r2 = r2.getLayout()
            int r2 = r2.getLineCount()
            r3 = 0
            r4 = 1
            if (r2 <= r4) goto L_0x003c
            r2 = 1
            goto L_0x003d
        L_0x003c:
            r2 = 0
        L_0x003d:
            if (r2 == 0) goto L_0x0056
            int r5 = r7.f55
            if (r5 <= 0) goto L_0x0056
            android.widget.Button r5 = r7.f53
            int r5 = r5.getMeasuredWidth()
            int r6 = r7.f55
            if (r5 <= r6) goto L_0x0056
            int r1 = r0 - r1
            boolean r0 = r7.m53(r4, r0, r1)
            if (r0 == 0) goto L_0x0061
            goto L_0x0062
        L_0x0056:
            if (r2 == 0) goto L_0x0059
            goto L_0x005a
        L_0x0059:
            r0 = r1
        L_0x005a:
            boolean r0 = r7.m53(r3, r0, r0)
            if (r0 == 0) goto L_0x0061
            goto L_0x0062
        L_0x0061:
            r4 = 0
        L_0x0062:
            if (r4 == 0) goto L_0x0067
            super.onMeasure(r8, r9)
        L_0x0067:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.internal.SnackbarContentLayout.onMeasure(int, int):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m53(int i, int i2, int i3) {
        boolean z;
        if (i != getOrientation()) {
            setOrientation(i);
            z = true;
        } else {
            z = false;
        }
        if (this.f52.getPaddingTop() == i2 && this.f52.getPaddingBottom() == i3) {
            return z;
        }
        m52(this.f52, i2, i3);
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m52(View view, int i, int i2) {
        if (C0414.m2250(view)) {
            C0414.m2220(view, C0414.m2238(view), i, C0414.m2239(view), i2);
        } else {
            view.setPadding(view.getPaddingLeft(), i, view.getPaddingRight(), i2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m54(int i, int i2) {
        this.f52.setAlpha(0.0f);
        long j = (long) i2;
        long j2 = (long) i;
        this.f52.animate().alpha(1.0f).setDuration(j).setStartDelay(j2).start();
        if (this.f53.getVisibility() == 0) {
            this.f53.setAlpha(0.0f);
            this.f53.animate().alpha(1.0f).setDuration(j).setStartDelay(j2).start();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m55(int i, int i2) {
        this.f52.setAlpha(1.0f);
        long j = (long) i2;
        long j2 = (long) i;
        this.f52.animate().alpha(0.0f).setDuration(j).setStartDelay(j2).start();
        if (this.f53.getVisibility() == 0) {
            this.f53.setAlpha(1.0f);
            this.f53.animate().alpha(0.0f).setDuration(j).setStartDelay(j2).start();
        }
    }
}
