package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.design.C0089;
import android.support.v4.content.ʻ.C0107;
import android.support.v4.widget.C0179;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ˉ.C0386;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.ʻ.C0360;
import android.support.v7.view.menu.C0508;
import android.support.v7.view.menu.C0520;
import android.support.v7.widget.C0594;
import android.support.v7.widget.C0652;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import net.lingala.zip4j.util.InternalZipConstants;

public class NavigationMenuItemView extends C0020 implements C0520.C0521 {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final int[] f40 = {16842912};

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f41;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f42;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f43;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final CheckedTextView f44;

    /* renamed from: ˉ  reason: contains not printable characters */
    private FrameLayout f45;

    /* renamed from: ˊ  reason: contains not printable characters */
    private C0508 f46;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ColorStateList f47;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f48;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Drawable f49;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final C0386 f50;

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m49() {
        return false;
    }

    public NavigationMenuItemView(Context context) {
        this(context, null);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.design.internal.NavigationMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public NavigationMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f50 = new C0386() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public void m50(View view, C0360 r2) {
                super.m2129(view, r2);
                r2.m2014(NavigationMenuItemView.this.f41);
            }
        };
        setOrientation(0);
        LayoutInflater.from(context).inflate(C0089.C0096.design_navigation_menu_item, (ViewGroup) this, true);
        this.f42 = context.getResources().getDimensionPixelSize(C0089.C0092.design_navigation_icon_size);
        this.f44 = (CheckedTextView) findViewById(C0089.C0094.design_menu_item_text);
        this.f44.setDuplicateParentStateEnabled(true);
        C0414.m2224(this.f44, this.f50);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m48(C0508 r1, int i) {
        this.f46 = r1;
        setVisibility(r1.isVisible() ? 0 : 8);
        if (getBackground() == null) {
            C0414.m2223(this, m46());
        }
        setCheckable(r1.isCheckable());
        setChecked(r1.isChecked());
        setEnabled(r1.isEnabled());
        setTitle(r1.getTitle());
        setIcon(r1.getIcon());
        setActionView(r1.getActionView());
        setContentDescription(r1.getContentDescription());
        C0594.m3805(this, r1.getTooltipText());
        m45();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean m44() {
        return this.f46.getTitle() == null && this.f46.getIcon() == null && this.f46.getActionView() != null;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m45() {
        if (m44()) {
            this.f44.setVisibility(8);
            FrameLayout frameLayout = this.f45;
            if (frameLayout != null) {
                C0652.C0653 r0 = (C0652.C0653) frameLayout.getLayoutParams();
                r0.width = -1;
                this.f45.setLayoutParams(r0);
                return;
            }
            return;
        }
        this.f44.setVisibility(0);
        FrameLayout frameLayout2 = this.f45;
        if (frameLayout2 != null) {
            C0652.C0653 r02 = (C0652.C0653) frameLayout2.getLayoutParams();
            r02.width = -2;
            this.f45.setLayoutParams(r02);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m47() {
        FrameLayout frameLayout = this.f45;
        if (frameLayout != null) {
            frameLayout.removeAllViews();
        }
        this.f44.setCompoundDrawables(null, null, null, null);
    }

    private void setActionView(View view) {
        if (view != null) {
            if (this.f45 == null) {
                this.f45 = (FrameLayout) ((ViewStub) findViewById(C0089.C0094.design_menu_item_action_area_stub)).inflate();
            }
            this.f45.removeAllViews();
            this.f45.addView(view);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private StateListDrawable m46() {
        TypedValue typedValue = new TypedValue();
        if (!getContext().getTheme().resolveAttribute(C0727.C0728.colorControlHighlight, typedValue, true)) {
            return null;
        }
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(f40, new ColorDrawable(typedValue.data));
        stateListDrawable.addState(EMPTY_STATE_SET, new ColorDrawable(0));
        return stateListDrawable;
    }

    public C0508 getItemData() {
        return this.f46;
    }

    public void setTitle(CharSequence charSequence) {
        this.f44.setText(charSequence);
    }

    public void setCheckable(boolean z) {
        refreshDrawableState();
        if (this.f41 != z) {
            this.f41 = z;
            this.f50.m2128(this.f44, (int) InternalZipConstants.UFT8_NAMES_FLAG);
        }
    }

    public void setChecked(boolean z) {
        refreshDrawableState();
        this.f44.setChecked(z);
    }

    public void setIcon(Drawable drawable) {
        if (drawable != null) {
            if (this.f48) {
                Drawable.ConstantState constantState = drawable.getConstantState();
                if (constantState != null) {
                    drawable = constantState.newDrawable();
                }
                drawable = C0288.m1713(drawable).mutate();
                C0288.m1703(drawable, this.f47);
            }
            int i = this.f42;
            drawable.setBounds(0, 0, i, i);
        } else if (this.f43) {
            if (this.f49 == null) {
                this.f49 = C0107.m562(getResources(), C0089.C0093.navigation_empty_icon, getContext().getTheme());
                Drawable drawable2 = this.f49;
                if (drawable2 != null) {
                    int i2 = this.f42;
                    drawable2.setBounds(0, 0, i2, i2);
                }
            }
            drawable = this.f49;
        }
        C0179.m1057(this.f44, drawable, null, null, null);
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        C0508 r0 = this.f46;
        if (r0 != null && r0.isCheckable() && this.f46.isChecked()) {
            mergeDrawableStates(onCreateDrawableState, f40);
        }
        return onCreateDrawableState;
    }

    /* access modifiers changed from: package-private */
    public void setIconTintList(ColorStateList colorStateList) {
        this.f47 = colorStateList;
        this.f48 = this.f47 != null;
        C0508 r1 = this.f46;
        if (r1 != null) {
            setIcon(r1.getIcon());
        }
    }

    public void setTextAppearance(int i) {
        C0179.m1056(this.f44, i);
    }

    public void setTextColor(ColorStateList colorStateList) {
        this.f44.setTextColor(colorStateList);
    }

    public void setNeedsEmptyIcon(boolean z) {
        this.f43 = z;
    }
}
