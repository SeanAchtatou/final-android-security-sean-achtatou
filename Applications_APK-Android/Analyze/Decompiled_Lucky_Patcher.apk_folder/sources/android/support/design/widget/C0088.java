package android.support.design.widget;

import android.widget.ImageButton;

/* renamed from: android.support.design.widget.ᴵ  reason: contains not printable characters */
/* compiled from: VisibilityAwareImageButton */
class C0088 extends ImageButton {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f374;

    public void setVisibility(int i) {
        m526(i, true);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m526(int i, boolean z) {
        super.setVisibility(i);
        if (z) {
            this.f374 = i;
        }
    }

    /* access modifiers changed from: package-private */
    public final int getUserSetVisibility() {
        return this.f374;
    }
}
