package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.C0089;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.C0188;
import android.support.v4.ʾ.C0317;
import android.support.v4.ˉ.C0355;
import android.support.v4.ˉ.C0414;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;

public class BottomSheetBehavior<V extends View> extends CoordinatorLayout.C0043<V> {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f115;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f116;

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean f117;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f118 = 4;

    /* renamed from: ʿ  reason: contains not printable characters */
    C0188 f119;

    /* renamed from: ˆ  reason: contains not printable characters */
    int f120;

    /* renamed from: ˈ  reason: contains not printable characters */
    WeakReference<V> f121;

    /* renamed from: ˉ  reason: contains not printable characters */
    WeakReference<View> f122;

    /* renamed from: ˊ  reason: contains not printable characters */
    int f123;

    /* renamed from: ˋ  reason: contains not printable characters */
    boolean f124;

    /* renamed from: ˎ  reason: contains not printable characters */
    private float f125;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f126;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f127;

    /* renamed from: י  reason: contains not printable characters */
    private int f128;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f129;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f130;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f131;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f132;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private C0040 f133;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private VelocityTracker f134;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int f135;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private final C0188.C0189 f136 = new C0188.C0189() {
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m203(View view, int i) {
            View view2;
            if (BottomSheetBehavior.this.f118 == 1 || BottomSheetBehavior.this.f124) {
                return false;
            }
            if ((BottomSheetBehavior.this.f118 != 3 || BottomSheetBehavior.this.f123 != i || (view2 = BottomSheetBehavior.this.f122.get()) == null || !view2.canScrollVertically(-1)) && BottomSheetBehavior.this.f121 != null && BottomSheetBehavior.this.f121.get() == view) {
                return true;
            }
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m202(View view, int i, int i2, int i3, int i4) {
            BottomSheetBehavior.this.m196(i2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m200(int i) {
            if (i == 1) {
                BottomSheetBehavior.this.m193(1);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m201(View view, float f, float f2) {
            int i;
            int i2;
            int i3 = 3;
            if (f2 < 0.0f) {
                i = BottomSheetBehavior.this.f115;
            } else if (!BottomSheetBehavior.this.f117 || !BottomSheetBehavior.this.m191(view, f2)) {
                if (f2 == 0.0f) {
                    int top = view.getTop();
                    if (Math.abs(top - BottomSheetBehavior.this.f115) < Math.abs(top - BottomSheetBehavior.this.f116)) {
                        i = BottomSheetBehavior.this.f115;
                    } else {
                        i2 = BottomSheetBehavior.this.f116;
                    }
                } else {
                    i2 = BottomSheetBehavior.this.f116;
                }
                i = i2;
                i3 = 4;
            } else {
                i = BottomSheetBehavior.this.f120;
                i3 = 5;
            }
            if (BottomSheetBehavior.this.f119.m1088(view.getLeft(), i)) {
                BottomSheetBehavior.this.m193(2);
                C0414.m2226(view, new C0042(view, i3));
                return;
            }
            BottomSheetBehavior.this.m193(i3);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m199(View view, int i, int i2) {
            return C0317.m1833(i, BottomSheetBehavior.this.f115, BottomSheetBehavior.this.f117 ? BottomSheetBehavior.this.f120 : BottomSheetBehavior.this.f116);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m204(View view, int i, int i2) {
            return view.getLeft();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m198(View view) {
            int i;
            int i2;
            if (BottomSheetBehavior.this.f117) {
                i = BottomSheetBehavior.this.f120;
                i2 = BottomSheetBehavior.this.f115;
            } else {
                i = BottomSheetBehavior.this.f116;
                i2 = BottomSheetBehavior.this.f115;
            }
            return i - i2;
        }
    };

    /* renamed from: android.support.design.widget.BottomSheetBehavior$ʻ  reason: contains not printable characters */
    public static abstract class C0040 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract void m205(View view, float f);

        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract void m206(View view, int i);
    }

    public BottomSheetBehavior() {
    }

    public BottomSheetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.BottomSheetBehavior_Layout);
        TypedValue peekValue = obtainStyledAttributes.peekValue(C0089.C0098.BottomSheetBehavior_Layout_behavior_peekHeight);
        if (peekValue == null || peekValue.data != -1) {
            m183(obtainStyledAttributes.getDimensionPixelSize(C0089.C0098.BottomSheetBehavior_Layout_behavior_peekHeight, -1));
        } else {
            m183(peekValue.data);
        }
        m186(obtainStyledAttributes.getBoolean(C0089.C0098.BottomSheetBehavior_Layout_behavior_hideable, false));
        m194(obtainStyledAttributes.getBoolean(C0089.C0098.BottomSheetBehavior_Layout_behavior_skipCollapsed, false));
        obtainStyledAttributes.recycle();
        this.f125 = (float) ViewConfiguration.get(context).getScaledMaximumFlingVelocity();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Parcelable m192(CoordinatorLayout coordinatorLayout, V v) {
        return new C0041(super.m279(coordinatorLayout, v), this.f118);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m184(CoordinatorLayout coordinatorLayout, V v, Parcelable parcelable) {
        C0041 r4 = (C0041) parcelable;
        super.m263(coordinatorLayout, v, r4.m1995());
        if (r4.f138 == 1 || r4.f138 == 2) {
            this.f118 = 4;
        } else {
            this.f118 = r4.f138;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ˉ.ᐧ.ʻ(android.view.View, boolean):void
     arg types: [V, int]
     candidates:
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ﾞ):android.support.v4.ˉ.ﾞ
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, float):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, int):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ʼ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ـ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, java.lang.Runnable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, int):void
     arg types: [V, int]
     candidates:
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.MotionEvent, int):boolean
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, android.graphics.Rect):void
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, int):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m187(CoordinatorLayout coordinatorLayout, V v, int i) {
        int i2;
        if (C0414.m2247(coordinatorLayout) && !C0414.m2247(v)) {
            C0414.m2228((View) v, true);
        }
        int top = v.getTop();
        coordinatorLayout.m240((View) v, i);
        this.f120 = coordinatorLayout.getHeight();
        if (this.f127) {
            if (this.f128 == 0) {
                this.f128 = coordinatorLayout.getResources().getDimensionPixelSize(C0089.C0092.design_bottom_sheet_peek_height_min);
            }
            i2 = Math.max(this.f128, this.f120 - ((coordinatorLayout.getWidth() * 9) / 16));
        } else {
            i2 = this.f126;
        }
        this.f115 = Math.max(0, this.f120 - v.getHeight());
        this.f116 = Math.max(this.f120 - i2, this.f115);
        int i3 = this.f118;
        if (i3 == 3) {
            C0414.m2231(v, this.f115);
        } else if (!this.f117 || i3 != 5) {
            int i4 = this.f118;
            if (i4 == 4) {
                C0414.m2231(v, this.f116);
            } else if (i4 == 1 || i4 == 2) {
                C0414.m2231(v, top - v.getTop());
            }
        } else {
            C0414.m2231(v, this.f120);
        }
        if (this.f119 == null) {
            this.f119 = C0188.m1067(coordinatorLayout, this.f136);
        }
        this.f121 = new WeakReference<>(v);
        this.f122 = new WeakReference<>(m182((View) v));
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m188(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (!v.isShown()) {
            this.f130 = true;
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            m180();
        }
        if (this.f134 == null) {
            this.f134 = VelocityTracker.obtain();
        }
        this.f134.addMovement(motionEvent);
        if (actionMasked == 0) {
            int x = (int) motionEvent.getX();
            this.f135 = (int) motionEvent.getY();
            WeakReference<View> weakReference = this.f122;
            View view = weakReference != null ? weakReference.get() : null;
            if (view != null && coordinatorLayout.m247(view, x, this.f135)) {
                this.f123 = motionEvent.getPointerId(motionEvent.getActionIndex());
                this.f124 = true;
            }
            this.f130 = this.f123 == -1 && !coordinatorLayout.m247(v, x, this.f135);
        } else if (actionMasked == 1 || actionMasked == 3) {
            this.f124 = false;
            this.f123 = -1;
            if (this.f130) {
                this.f130 = false;
                return false;
            }
        }
        if (!this.f130 && this.f119.m1089(motionEvent)) {
            return true;
        }
        View view2 = this.f122.get();
        if (actionMasked != 2 || view2 == null || this.f130 || this.f118 == 1 || coordinatorLayout.m247(view2, (int) motionEvent.getX(), (int) motionEvent.getY()) || Math.abs(((float) this.f135) - motionEvent.getY()) <= ((float) this.f119.m1101())) {
            return false;
        }
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m195(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        if (!v.isShown()) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (this.f118 == 1 && actionMasked == 0) {
            return true;
        }
        this.f119.m1093(motionEvent);
        if (actionMasked == 0) {
            m180();
        }
        if (this.f134 == null) {
            this.f134 = VelocityTracker.obtain();
        }
        this.f134.addMovement(motionEvent);
        if (actionMasked == 2 && !this.f130 && Math.abs(((float) this.f135) - motionEvent.getY()) > ((float) this.f119.m1101())) {
            this.f119.m1087(v, motionEvent.getPointerId(motionEvent.getActionIndex()));
        }
        return !this.f130;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m190(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i) {
        this.f131 = 0;
        this.f132 = false;
        if ((i & 2) != 0) {
            return true;
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m185(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int[] iArr) {
        if (view == this.f122.get()) {
            int top = v.getTop();
            int i3 = top - i2;
            if (i2 > 0) {
                int i4 = this.f115;
                if (i3 < i4) {
                    iArr[1] = top - i4;
                    C0414.m2231(v, -iArr[1]);
                    m193(3);
                } else {
                    iArr[1] = i2;
                    C0414.m2231(v, -i2);
                    m193(1);
                }
            } else if (i2 < 0 && !view.canScrollVertically(-1)) {
                int i5 = this.f116;
                if (i3 <= i5 || this.f117) {
                    iArr[1] = i2;
                    C0414.m2231(v, -i2);
                    m193(1);
                } else {
                    iArr[1] = top - i5;
                    C0414.m2231(v, -iArr[1]);
                    m193(4);
                }
            }
            m196(v.getTop());
            this.f131 = i2;
            this.f132 = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.י.ʻ(android.view.View, int, int):boolean
     arg types: [V, int, int]
     candidates:
      android.support.v4.widget.י.ʻ(float, float, float):float
      android.support.v4.widget.י.ʻ(int, int, int):int
      android.support.v4.widget.י.ʻ(android.view.ViewGroup, float, android.support.v4.widget.י$ʻ):android.support.v4.widget.י
      android.support.v4.widget.י.ʻ(float, float, int):void
      android.support.v4.widget.י.ʻ(android.view.View, float, float):boolean
      android.support.v4.widget.י.ʻ(android.view.View, int, int):boolean */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m197(CoordinatorLayout coordinatorLayout, V v, View view) {
        int i;
        int i2 = 3;
        if (v.getTop() == this.f115) {
            m193(3);
            return;
        }
        WeakReference<View> weakReference = this.f122;
        if (weakReference != null && view == weakReference.get() && this.f132) {
            if (this.f131 > 0) {
                i = this.f115;
            } else if (!this.f117 || !m191(v, m181())) {
                if (this.f131 == 0) {
                    int top = v.getTop();
                    if (Math.abs(top - this.f115) < Math.abs(top - this.f116)) {
                        i = this.f115;
                    } else {
                        i = this.f116;
                    }
                } else {
                    i = this.f116;
                }
                i2 = 4;
            } else {
                i = this.f120;
                i2 = 5;
            }
            if (this.f119.m1090((View) v, v.getLeft(), i)) {
                m193(2);
                C0414.m2226(v, new C0042(v, i2));
            } else {
                m193(i2);
            }
            this.f132 = false;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m189(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2) {
        return view == this.f122.get() && (this.f118 != 3 || super.m275(coordinatorLayout, v, view, f, f2));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m183(int r4) {
        /*
            r3 = this;
            r0 = 1
            r1 = 0
            r2 = -1
            if (r4 != r2) goto L_0x000c
            boolean r4 = r3.f127
            if (r4 != 0) goto L_0x0015
            r3.f127 = r0
            goto L_0x0024
        L_0x000c:
            boolean r2 = r3.f127
            if (r2 != 0) goto L_0x0017
            int r2 = r3.f126
            if (r2 == r4) goto L_0x0015
            goto L_0x0017
        L_0x0015:
            r0 = 0
            goto L_0x0024
        L_0x0017:
            r3.f127 = r1
            int r1 = java.lang.Math.max(r1, r4)
            r3.f126 = r1
            int r1 = r3.f120
            int r1 = r1 - r4
            r3.f116 = r1
        L_0x0024:
            if (r0 == 0) goto L_0x003a
            int r4 = r3.f118
            r0 = 4
            if (r4 != r0) goto L_0x003a
            java.lang.ref.WeakReference<V> r4 = r3.f121
            if (r4 == 0) goto L_0x003a
            java.lang.Object r4 = r4.get()
            android.view.View r4 = (android.view.View) r4
            if (r4 == 0) goto L_0x003a
            r4.requestLayout()
        L_0x003a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.BottomSheetBehavior.m183(int):void");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m186(boolean z) {
        this.f117 = z;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m194(boolean z) {
        this.f129 = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m193(int i) {
        C0040 r1;
        if (this.f118 != i) {
            this.f118 = i;
            View view = (View) this.f121.get();
            if (view != null && (r1 = this.f133) != null) {
                r1.m206(view, i);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m180() {
        this.f123 = -1;
        VelocityTracker velocityTracker = this.f134;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.f134 = null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m191(View view, float f) {
        if (this.f129) {
            return true;
        }
        if (view.getTop() >= this.f116 && Math.abs((((float) view.getTop()) + (f * 0.1f)) - ((float) this.f116)) / ((float) this.f126) > 0.5f) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public View m182(View view) {
        if (C0414.m2253(view)) {
            return view;
        }
        if (!(view instanceof ViewGroup)) {
            return null;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View r2 = m182(viewGroup.getChildAt(i));
            if (r2 != null) {
                return r2;
            }
        }
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private float m181() {
        this.f134.computeCurrentVelocity(1000, this.f125);
        return this.f134.getYVelocity(this.f123);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m196(int i) {
        C0040 r1;
        View view = (View) this.f121.get();
        if (view != null && (r1 = this.f133) != null) {
            int i2 = this.f116;
            if (i > i2) {
                r1.m205(view, ((float) (i2 - i)) / ((float) (this.f120 - i2)));
            } else {
                r1.m205(view, ((float) (i2 - i)) / ((float) (i2 - this.f115)));
            }
        }
    }

    /* renamed from: android.support.design.widget.BottomSheetBehavior$ʽ  reason: contains not printable characters */
    private class C0042 implements Runnable {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final View f140;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final int f141;

        C0042(View view, int i) {
            this.f140 = view;
            this.f141 = i;
        }

        public void run() {
            if (BottomSheetBehavior.this.f119 == null || !BottomSheetBehavior.this.f119.m1091(true)) {
                BottomSheetBehavior.this.m193(this.f141);
            } else {
                C0414.m2226(this.f140, this);
            }
        }
    }

    /* renamed from: android.support.design.widget.BottomSheetBehavior$ʼ  reason: contains not printable characters */
    protected static class C0041 extends C0355 {
        public static final Parcelable.Creator<C0041> CREATOR = new Parcelable.ClassLoaderCreator<C0041>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0041 createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new C0041(parcel, classLoader);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0041 createFromParcel(Parcel parcel) {
                return new C0041(parcel, (ClassLoader) null);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0041[] newArray(int i) {
                return new C0041[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        final int f138;

        public C0041(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f138 = parcel.readInt();
        }

        public C0041(Parcelable parcelable, int i) {
            super(parcelable);
            this.f138 = i;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f138);
        }
    }
}
