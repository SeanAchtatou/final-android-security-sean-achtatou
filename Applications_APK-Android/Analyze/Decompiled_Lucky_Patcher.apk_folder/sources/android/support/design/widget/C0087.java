package android.support.design.widget;

import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.support.design.C0089;
import android.view.View;

/* renamed from: android.support.design.widget.ᐧ  reason: contains not printable characters */
/* compiled from: ViewUtilsLollipop */
class C0087 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f373 = {16843848};

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m525(View view, float f) {
        int integer = view.getResources().getInteger(C0089.C0095.app_bar_elevation_anim_duration);
        StateListAnimator stateListAnimator = new StateListAnimator();
        long j = (long) integer;
        stateListAnimator.addState(new int[]{16842766, C0089.C0091.state_collapsible, -C0089.C0091.state_collapsed}, ObjectAnimator.ofFloat(view, "elevation", 0.0f).setDuration(j));
        stateListAnimator.addState(new int[]{16842766}, ObjectAnimator.ofFloat(view, "elevation", f).setDuration(j));
        stateListAnimator.addState(new int[0], ObjectAnimator.ofFloat(view, "elevation", 0.0f).setDuration(0L));
        view.setStateListAnimator(stateListAnimator);
    }
}
