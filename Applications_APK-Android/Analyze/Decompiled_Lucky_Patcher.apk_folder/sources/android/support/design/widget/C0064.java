package android.support.design.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.v4.ʼ.C0287;

/* renamed from: android.support.design.widget.ʽ  reason: contains not printable characters */
/* compiled from: CircularBorderDrawable */
class C0064 extends Drawable {

    /* renamed from: ʻ  reason: contains not printable characters */
    final Paint f264;

    /* renamed from: ʼ  reason: contains not printable characters */
    final Rect f265;

    /* renamed from: ʽ  reason: contains not printable characters */
    final RectF f266;

    /* renamed from: ʾ  reason: contains not printable characters */
    float f267;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f268;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f269;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f270;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f271;

    /* renamed from: ˊ  reason: contains not printable characters */
    private ColorStateList f272;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f273;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f274;

    /* renamed from: ˏ  reason: contains not printable characters */
    private float f275;

    public void draw(Canvas canvas) {
        if (this.f274) {
            this.f264.setShader(m401());
            this.f274 = false;
        }
        float strokeWidth = this.f264.getStrokeWidth() / 2.0f;
        RectF rectF = this.f266;
        copyBounds(this.f265);
        rectF.set(this.f265);
        rectF.left += strokeWidth;
        rectF.top += strokeWidth;
        rectF.right -= strokeWidth;
        rectF.bottom -= strokeWidth;
        canvas.save();
        canvas.rotate(this.f275, rectF.centerX(), rectF.centerY());
        canvas.drawOval(rectF, this.f264);
        canvas.restore();
    }

    public boolean getPadding(Rect rect) {
        int round = Math.round(this.f267);
        rect.set(round, round, round, round);
        return true;
    }

    public void setAlpha(int i) {
        this.f264.setAlpha(i);
        invalidateSelf();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m403(ColorStateList colorStateList) {
        if (colorStateList != null) {
            this.f273 = colorStateList.getColorForState(getState(), this.f273);
        }
        this.f272 = colorStateList;
        this.f274 = true;
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f264.setColorFilter(colorFilter);
        invalidateSelf();
    }

    public int getOpacity() {
        return this.f267 > 0.0f ? -3 : -2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m402(float f) {
        if (f != this.f275) {
            this.f275 = f;
            invalidateSelf();
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.f274 = true;
    }

    public boolean isStateful() {
        ColorStateList colorStateList = this.f272;
        return (colorStateList != null && colorStateList.isStateful()) || super.isStateful();
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        int colorForState;
        ColorStateList colorStateList = this.f272;
        if (!(colorStateList == null || (colorForState = colorStateList.getColorForState(iArr, this.f273)) == this.f273)) {
            this.f274 = true;
            this.f273 = colorForState;
        }
        if (this.f274) {
            invalidateSelf();
        }
        return this.f274;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    /* renamed from: ʻ  reason: contains not printable characters */
    private Shader m401() {
        Rect rect = this.f265;
        copyBounds(rect);
        float height = this.f267 / ((float) rect.height());
        return new LinearGradient(0.0f, (float) rect.top, 0.0f, (float) rect.bottom, new int[]{C0287.m1695(this.f268, this.f273), C0287.m1695(this.f269, this.f273), C0287.m1695(C0287.m1697(this.f269, 0), this.f273), C0287.m1695(C0287.m1697(this.f271, 0), this.f273), C0287.m1695(this.f271, this.f273), C0287.m1695(this.f270, this.f273)}, new float[]{0.0f, height, 0.5f, 0.5f, 1.0f - height, 1.0f}, Shader.TileMode.CLAMP);
    }
}
