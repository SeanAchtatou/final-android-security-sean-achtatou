package android.support.design.widget;

import android.content.Context;
import android.support.v4.ʾ.C0317;
import android.support.v4.ˉ.C0414;
import android.util.AttributeSet;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.OverScroller;

/* renamed from: android.support.design.widget.ˈ  reason: contains not printable characters */
/* compiled from: HeaderBehavior */
abstract class C0073<V extends View> extends C0085<V> {

    /* renamed from: ʻ  reason: contains not printable characters */
    OverScroller f315;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Runnable f316;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f317;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f318 = -1;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f319;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f320 = -1;

    /* renamed from: ˈ  reason: contains not printable characters */
    private VelocityTracker f321;

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m460(CoordinatorLayout coordinatorLayout, View view) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m466(View view) {
        return false;
    }

    public C0073() {
    }

    public C0073(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        if (r0 != 3) goto L_0x0083;
     */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m462(android.support.design.widget.CoordinatorLayout r5, android.view.View r6, android.view.MotionEvent r7) {
        /*
            r4 = this;
            int r0 = r4.f320
            if (r0 >= 0) goto L_0x0012
            android.content.Context r0 = r5.getContext()
            android.view.ViewConfiguration r0 = android.view.ViewConfiguration.get(r0)
            int r0 = r0.getScaledTouchSlop()
            r4.f320 = r0
        L_0x0012:
            int r0 = r7.getAction()
            r1 = 2
            r2 = 1
            if (r0 != r1) goto L_0x001f
            boolean r0 = r4.f317
            if (r0 == 0) goto L_0x001f
            return r2
        L_0x001f:
            int r0 = r7.getActionMasked()
            r3 = 0
            if (r0 == 0) goto L_0x0060
            r5 = -1
            if (r0 == r2) goto L_0x0051
            if (r0 == r1) goto L_0x002f
            r6 = 3
            if (r0 == r6) goto L_0x0051
            goto L_0x0083
        L_0x002f:
            int r6 = r4.f318
            if (r6 != r5) goto L_0x0034
            goto L_0x0083
        L_0x0034:
            int r6 = r7.findPointerIndex(r6)
            if (r6 != r5) goto L_0x003b
            goto L_0x0083
        L_0x003b:
            float r5 = r7.getY(r6)
            int r5 = (int) r5
            int r6 = r4.f319
            int r6 = r5 - r6
            int r6 = java.lang.Math.abs(r6)
            int r0 = r4.f320
            if (r6 <= r0) goto L_0x0083
            r4.f317 = r2
            r4.f319 = r5
            goto L_0x0083
        L_0x0051:
            r4.f317 = r3
            r4.f318 = r5
            android.view.VelocityTracker r5 = r4.f321
            if (r5 == 0) goto L_0x0083
            r5.recycle()
            r5 = 0
            r4.f321 = r5
            goto L_0x0083
        L_0x0060:
            r4.f317 = r3
            float r0 = r7.getX()
            int r0 = (int) r0
            float r1 = r7.getY()
            int r1 = (int) r1
            boolean r2 = r4.m466(r6)
            if (r2 == 0) goto L_0x0083
            boolean r5 = r5.m247(r6, r0, r1)
            if (r5 == 0) goto L_0x0083
            r4.f319 = r1
            int r5 = r7.getPointerId(r3)
            r4.f318 = r5
            r4.m456()
        L_0x0083:
            android.view.VelocityTracker r5 = r4.f321
            if (r5 == 0) goto L_0x008a
            r5.addMovement(r7)
        L_0x008a:
            boolean r5 = r4.f317
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.C0073.m462(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0021, code lost:
        if (r0 != 3) goto L_0x00ae;
     */
    /* renamed from: ʼ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m465(android.support.design.widget.CoordinatorLayout r12, V r13, android.view.MotionEvent r14) {
        /*
            r11 = this;
            int r0 = r11.f320
            if (r0 >= 0) goto L_0x0012
            android.content.Context r0 = r12.getContext()
            android.view.ViewConfiguration r0 = android.view.ViewConfiguration.get(r0)
            int r0 = r0.getScaledTouchSlop()
            r11.f320 = r0
        L_0x0012:
            int r0 = r14.getActionMasked()
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x008d
            r3 = -1
            if (r0 == r1) goto L_0x005c
            r4 = 2
            if (r0 == r4) goto L_0x0025
            r12 = 3
            if (r0 == r12) goto L_0x007e
            goto L_0x00ae
        L_0x0025:
            int r0 = r11.f318
            int r0 = r14.findPointerIndex(r0)
            if (r0 != r3) goto L_0x002e
            return r2
        L_0x002e:
            float r0 = r14.getY(r0)
            int r0 = (int) r0
            int r2 = r11.f319
            int r2 = r2 - r0
            boolean r3 = r11.f317
            if (r3 != 0) goto L_0x0049
            int r3 = java.lang.Math.abs(r2)
            int r4 = r11.f320
            if (r3 <= r4) goto L_0x0049
            r11.f317 = r1
            if (r2 <= 0) goto L_0x0048
            int r2 = r2 - r4
            goto L_0x0049
        L_0x0048:
            int r2 = r2 + r4
        L_0x0049:
            r6 = r2
            boolean r2 = r11.f317
            if (r2 == 0) goto L_0x00ae
            r11.f319 = r0
            int r7 = r11.m464(r13)
            r8 = 0
            r3 = r11
            r4 = r12
            r5 = r13
            r3.m463(r4, r5, r6, r7, r8)
            goto L_0x00ae
        L_0x005c:
            android.view.VelocityTracker r0 = r11.f321
            if (r0 == 0) goto L_0x007e
            r0.addMovement(r14)
            android.view.VelocityTracker r0 = r11.f321
            r4 = 1000(0x3e8, float:1.401E-42)
            r0.computeCurrentVelocity(r4)
            android.view.VelocityTracker r0 = r11.f321
            int r4 = r11.f318
            float r10 = r0.getYVelocity(r4)
            int r0 = r11.m459(r13)
            int r8 = -r0
            r9 = 0
            r5 = r11
            r6 = r12
            r7 = r13
            r5.m461(r6, r7, r8, r9, r10)
        L_0x007e:
            r11.f317 = r2
            r11.f318 = r3
            android.view.VelocityTracker r12 = r11.f321
            if (r12 == 0) goto L_0x00ae
            r12.recycle()
            r12 = 0
            r11.f321 = r12
            goto L_0x00ae
        L_0x008d:
            float r0 = r14.getX()
            int r0 = (int) r0
            float r3 = r14.getY()
            int r3 = (int) r3
            boolean r12 = r12.m247(r13, r0, r3)
            if (r12 == 0) goto L_0x00b6
            boolean r12 = r11.m466(r13)
            if (r12 == 0) goto L_0x00b6
            r11.f319 = r3
            int r12 = r14.getPointerId(r2)
            r11.f318 = r12
            r11.m456()
        L_0x00ae:
            android.view.VelocityTracker r12 = r11.f321
            if (r12 == 0) goto L_0x00b5
            r12.addMovement(r14)
        L_0x00b5:
            return r1
        L_0x00b6:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.C0073.m465(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.ˈ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, int):int
     arg types: [android.support.design.widget.CoordinatorLayout, V, int, int, int]
     candidates:
      android.support.design.widget.ˈ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, float):boolean
      android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, float, float):boolean
      android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, android.view.View, int):boolean
      android.support.design.widget.ˈ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, int):int */
    /* access modifiers changed from: package-private */
    public int a_(CoordinatorLayout coordinatorLayout, V v, int i) {
        return m458(coordinatorLayout, (View) v, i, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m458(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3) {
        int r2;
        int r1 = m518();
        if (i2 == 0 || r1 < i2 || r1 > i3 || r1 == (r2 = C0317.m1833(i, i2, i3))) {
            return 0;
        }
        m516(r2);
        return r1 - r2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m457() {
        return m518();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final int m463(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3) {
        return m458(coordinatorLayout, view, m457() - i, i2, i3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final boolean m461(CoordinatorLayout coordinatorLayout, View view, int i, int i2, float f) {
        Runnable runnable = this.f316;
        if (runnable != null) {
            view.removeCallbacks(runnable);
            this.f316 = null;
        }
        if (this.f315 == null) {
            this.f315 = new OverScroller(view.getContext());
        }
        this.f315.fling(0, m518(), 0, Math.round(f), 0, 0, i, i2);
        if (this.f315.computeScrollOffset()) {
            this.f316 = new C0074(coordinatorLayout, view);
            C0414.m2226(view, this.f316);
            return true;
        }
        m460(coordinatorLayout, view);
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m464(View view) {
        return -view.getHeight();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m459(View view) {
        return view.getHeight();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m456() {
        if (this.f321 == null) {
            this.f321 = VelocityTracker.obtain();
        }
    }

    /* renamed from: android.support.design.widget.ˈ$ʻ  reason: contains not printable characters */
    /* compiled from: HeaderBehavior */
    private class C0074 implements Runnable {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final CoordinatorLayout f323;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final V f324;

        C0074(CoordinatorLayout coordinatorLayout, V v) {
            this.f323 = coordinatorLayout;
            this.f324 = v;
        }

        public void run() {
            if (this.f324 != null && C0073.this.f315 != null) {
                if (C0073.this.f315.computeScrollOffset()) {
                    C0073 r0 = C0073.this;
                    r0.a_(this.f323, this.f324, r0.f315.getCurrY());
                    C0414.m2226(this.f324, this);
                    return;
                }
                C0073.this.m460(this.f323, this.f324);
            }
        }
    }
}
