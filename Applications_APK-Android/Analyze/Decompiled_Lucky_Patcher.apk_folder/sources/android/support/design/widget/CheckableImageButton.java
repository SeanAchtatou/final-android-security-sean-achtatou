package android.support.design.widget;

import android.content.Context;
import android.support.v4.ˉ.C0386;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.ʻ.C0360;
import android.support.v7.widget.C0669;
import android.support.v7.ʻ.C0727;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Checkable;
import net.lingala.zip4j.util.InternalZipConstants;

public class CheckableImageButton extends C0669 implements Checkable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f142 = {16842912};

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f143;

    public CheckableImageButton(Context context) {
        this(context, null);
    }

    public CheckableImageButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, C0727.C0728.imageButtonStyle);
    }

    public CheckableImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        C0414.m2224(this, new C0386() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public void m211(View view, AccessibilityEvent accessibilityEvent) {
                super.m2130(view, accessibilityEvent);
                accessibilityEvent.setChecked(CheckableImageButton.this.isChecked());
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public void m210(View view, C0360 r2) {
                super.m2129(view, r2);
                r2.m2014(true);
                r2.m2021(CheckableImageButton.this.isChecked());
            }
        });
    }

    public void setChecked(boolean z) {
        if (this.f143 != z) {
            this.f143 = z;
            refreshDrawableState();
            sendAccessibilityEvent(InternalZipConstants.UFT8_NAMES_FLAG);
        }
    }

    public boolean isChecked() {
        return this.f143;
    }

    public void toggle() {
        setChecked(!this.f143);
    }

    public int[] onCreateDrawableState(int i) {
        if (this.f143) {
            return mergeDrawableStates(super.onCreateDrawableState(i + f142.length), f142);
        }
        return super.onCreateDrawableState(i);
    }
}
