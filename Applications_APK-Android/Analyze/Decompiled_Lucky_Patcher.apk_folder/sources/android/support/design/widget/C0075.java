package android.support.design.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.ʾ.C0317;
import android.support.v4.ˉ.C0396;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0439;
import android.util.AttributeSet;
import android.view.View;
import java.util.List;

/* renamed from: android.support.design.widget.ˉ  reason: contains not printable characters */
/* compiled from: HeaderScrollingViewBehavior */
abstract class C0075 extends C0085<View> {

    /* renamed from: ʻ  reason: contains not printable characters */
    final Rect f325 = new Rect();

    /* renamed from: ʼ  reason: contains not printable characters */
    final Rect f326 = new Rect();

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f327 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f328;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static int m467(int i) {
        if (i == 0) {
            return 8388659;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public float m468(View view) {
        return 1.0f;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract View m472(List<View> list);

    public C0075() {
    }

    public C0075(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ˉ.ᐧ.ʻ(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ﾞ):android.support.v4.ˉ.ﾞ
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, float):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, int):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ʼ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ـ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, java.lang.Runnable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m470(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
        View r3;
        int i5 = view.getLayoutParams().height;
        if ((i5 != -1 && i5 != -2) || (r3 = m472(coordinatorLayout.m254(view))) == null) {
            return false;
        }
        if (C0414.m2247(r3) && !C0414.m2247(view)) {
            C0414.m2228(view, true);
            if (C0414.m2247(view)) {
                view.requestLayout();
                return true;
            }
        }
        int size = View.MeasureSpec.getSize(i3);
        if (size == 0) {
            size = coordinatorLayout.getHeight();
        }
        coordinatorLayout.m241(view, i, i2, View.MeasureSpec.makeMeasureSpec((size - r3.getMeasuredHeight()) + m471(r3), i5 == -1 ? 1073741824 : Integer.MIN_VALUE), i4);
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m474(CoordinatorLayout coordinatorLayout, View view, int i) {
        View r0 = m472(coordinatorLayout.m254(view));
        if (r0 != null) {
            CoordinatorLayout.C0046 r1 = (CoordinatorLayout.C0046) view.getLayoutParams();
            Rect rect = this.f325;
            rect.set(coordinatorLayout.getPaddingLeft() + r1.leftMargin, r0.getBottom() + r1.topMargin, (coordinatorLayout.getWidth() - coordinatorLayout.getPaddingRight()) - r1.rightMargin, ((coordinatorLayout.getHeight() + r0.getBottom()) - coordinatorLayout.getPaddingBottom()) - r1.bottomMargin);
            C0439 lastWindowInsets = coordinatorLayout.getLastWindowInsets();
            if (lastWindowInsets != null && C0414.m2247(coordinatorLayout) && !C0414.m2247(view)) {
                rect.left += lastWindowInsets.m2397();
                rect.right -= lastWindowInsets.m2400();
            }
            Rect rect2 = this.f326;
            C0396.m2156(m467(r1.f173), view.getMeasuredWidth(), view.getMeasuredHeight(), rect, rect2, i);
            int r11 = m475(r0);
            view.layout(rect2.left, rect2.top - r11, rect2.right, rect2.bottom - r11);
            this.f327 = rect2.top - r0.getBottom();
            return;
        }
        super.m519(coordinatorLayout, view, i);
        this.f327 = 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final int m475(View view) {
        if (this.f328 == 0) {
            return 0;
        }
        float r4 = m468(view);
        int i = this.f328;
        return C0317.m1833((int) (r4 * ((float) i)), 0, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m471(View view) {
        return view.getMeasuredHeight();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final int m469() {
        return this.f327;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m473(int i) {
        this.f328 = i;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public final int m476() {
        return this.f328;
    }
}
