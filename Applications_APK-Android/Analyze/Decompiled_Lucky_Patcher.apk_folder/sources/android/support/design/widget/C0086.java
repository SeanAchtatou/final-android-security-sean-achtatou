package android.support.design.widget;

import android.support.v4.ˉ.C0414;
import android.view.View;

/* renamed from: android.support.design.widget.ٴ  reason: contains not printable characters */
/* compiled from: ViewOffsetHelper */
class C0086 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final View f368;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f369;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f370;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f371;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f372;

    public C0086(View view) {
        this.f368 = view;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m521() {
        this.f369 = this.f368.getTop();
        this.f370 = this.f368.getLeft();
        m520();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m520() {
        View view = this.f368;
        C0414.m2231(view, this.f371 - (view.getTop() - this.f369));
        View view2 = this.f368;
        C0414.m2234(view2, this.f372 - (view2.getLeft() - this.f370));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m522(int i) {
        if (this.f371 == i) {
            return false;
        }
        this.f371 = i;
        m520();
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m524(int i) {
        if (this.f372 == i) {
            return false;
        }
        this.f372 = i;
        m520();
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m523() {
        return this.f371;
    }
}
