package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.ʻ.C0727;

/* renamed from: android.support.design.widget.ˑ  reason: contains not printable characters */
/* compiled from: ThemeUtils */
class C0083 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int[] f362 = {C0727.C0728.colorPrimary};

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m512(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(f362);
        boolean z = !obtainStyledAttributes.hasValue(0);
        obtainStyledAttributes.recycle();
        if (z) {
            throw new IllegalArgumentException("You need to use a Theme.AppCompat theme (or descendant) with the design library.");
        }
    }
}
