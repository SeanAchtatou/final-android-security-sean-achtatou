package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.design.C0089;
import android.support.design.widget.C0057;
import android.support.design.widget.C0078;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v4.ˉ.C0414;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import java.util.List;

/* renamed from: android.support.design.widget.ʼ  reason: contains not printable characters */
/* compiled from: BaseTransientBottomBar */
public abstract class C0057<B extends C0057<B>> {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final Handler f238 = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                ((C0057) message.obj).m380();
                return true;
            } else if (i != 1) {
                return false;
            } else {
                ((C0057) message.obj).m381(message.arg1);
                return true;
            }
        }
    });
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final boolean f239 = (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT <= 19);

    /* renamed from: ʼ  reason: contains not printable characters */
    final C0063 f240;

    /* renamed from: ʽ  reason: contains not printable characters */
    final C0078.C0079 f241;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final ViewGroup f242;
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public final C0060 f243;

    /* renamed from: ˈ  reason: contains not printable characters */
    private List<C0058<B>> f244;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final AccessibilityManager f245;

    /* renamed from: android.support.design.widget.ʼ$ʻ  reason: contains not printable characters */
    /* compiled from: BaseTransientBottomBar */
    public static abstract class C0058<B> {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m391(B b) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m392(B b, int i) {
        }
    }

    /* renamed from: android.support.design.widget.ʼ$ʽ  reason: contains not printable characters */
    /* compiled from: BaseTransientBottomBar */
    public interface C0060 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m396(int i, int i2);

        /* renamed from: ʼ  reason: contains not printable characters */
        void m397(int i, int i2);
    }

    /* renamed from: android.support.design.widget.ʼ$ʾ  reason: contains not printable characters */
    /* compiled from: BaseTransientBottomBar */
    interface C0061 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m398(View view);

        /* renamed from: ʼ  reason: contains not printable characters */
        void m399(View view);
    }

    /* renamed from: android.support.design.widget.ʼ$ʿ  reason: contains not printable characters */
    /* compiled from: BaseTransientBottomBar */
    interface C0062 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m400(View view, int i, int i2, int i3, int i4);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m378(int i) {
        C0078.m491().m498(this.f241, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m379() {
        return C0078.m491().m503(this.f241);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m380() {
        if (this.f240.getParent() == null) {
            ViewGroup.LayoutParams layoutParams = this.f240.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.C0046) {
                CoordinatorLayout.C0046 r0 = (CoordinatorLayout.C0046) layoutParams;
                C0059 r1 = new C0059();
                r1.m357(0.1f);
                r1.m362(0.6f);
                r1.m358(0);
                r1.m359(new SwipeDismissBehavior.C0054() {
                    /* renamed from: ʻ  reason: contains not printable characters */
                    public void m387(View view) {
                        view.setVisibility(8);
                        C0057.this.m378(0);
                    }

                    /* renamed from: ʻ  reason: contains not printable characters */
                    public void m386(int i) {
                        if (i == 0) {
                            C0078.m491().m502(C0057.this.f241);
                        } else if (i == 1 || i == 2) {
                            C0078.m491().m501(C0057.this.f241);
                        }
                    }
                });
                r0.m298(r1);
                r0.f177 = 80;
            }
            this.f242.addView(this.f240);
        }
        this.f240.setOnAttachStateChangeListener(new C0061() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public void m388(View view) {
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public void m389(View view) {
                if (C0057.this.m379()) {
                    C0057.f238.post(new Runnable() {
                        public void run() {
                            C0057.this.m383(3);
                        }
                    });
                }
            }
        });
        if (!C0414.m2255(this.f240)) {
            this.f240.setOnLayoutChangeListener(new C0062() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public void m390(View view, int i, int i2, int i3, int i4) {
                    C0057.this.f240.setOnLayoutChangeListener(null);
                    if (C0057.this.m385()) {
                        C0057.this.m382();
                    } else {
                        C0057.this.m384();
                    }
                }
            });
        } else if (m385()) {
            m382();
        } else {
            m384();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m382() {
        if (Build.VERSION.SDK_INT >= 12) {
            final int height = this.f240.getHeight();
            if (f239) {
                C0414.m2231(this.f240, height);
            } else {
                this.f240.setTranslationY((float) height);
            }
            ValueAnimator valueAnimator = new ValueAnimator();
            valueAnimator.setIntValues(height, 0);
            valueAnimator.setInterpolator(C0056.f234);
            valueAnimator.setDuration(250L);
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animator) {
                    C0057.this.f243.m396(70, 180);
                }

                public void onAnimationEnd(Animator animator) {
                    C0057.this.m384();
                }
            });
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                /* renamed from: ʽ  reason: contains not printable characters */
                private int f260 = height;

                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                    if (C0057.f239) {
                        C0414.m2231(C0057.this.f240, intValue - this.f260);
                    } else {
                        C0057.this.f240.setTranslationY((float) intValue);
                    }
                    this.f260 = intValue;
                }
            });
            valueAnimator.start();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.f240.getContext(), C0089.C0090.design_snackbar_in);
        loadAnimation.setInterpolator(C0056.f234);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                C0057.this.m384();
            }
        });
        this.f240.startAnimation(loadAnimation);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m376(final int i) {
        if (Build.VERSION.SDK_INT >= 12) {
            ValueAnimator valueAnimator = new ValueAnimator();
            valueAnimator.setIntValues(0, this.f240.getHeight());
            valueAnimator.setInterpolator(C0056.f234);
            valueAnimator.setDuration(250L);
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animator) {
                    C0057.this.f243.m397(0, 180);
                }

                public void onAnimationEnd(Animator animator) {
                    C0057.this.m383(i);
                }
            });
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                /* renamed from: ʼ  reason: contains not printable characters */
                private int f250 = 0;

                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                    if (C0057.f239) {
                        C0414.m2231(C0057.this.f240, intValue - this.f250);
                    } else {
                        C0057.this.f240.setTranslationY((float) intValue);
                    }
                    this.f250 = intValue;
                }
            });
            valueAnimator.start();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.f240.getContext(), C0089.C0090.design_snackbar_out);
        loadAnimation.setInterpolator(C0056.f234);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                C0057.this.m383(i);
            }
        });
        this.f240.startAnimation(loadAnimation);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final void m381(int i) {
        if (!m385() || this.f240.getVisibility() != 0) {
            m383(i);
        } else {
            m376(i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m384() {
        C0078.m491().m500(this.f241);
        List<C0058<B>> list = this.f244;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.f244.get(size).m391(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m383(int i) {
        C0078.m491().m497(this.f241);
        List<C0058<B>> list = this.f244;
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                this.f244.get(size).m392(this, i);
            }
        }
        if (Build.VERSION.SDK_INT < 11) {
            this.f240.setVisibility(8);
        }
        ViewParent parent = this.f240.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.f240);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m385() {
        return !this.f245.isEnabled();
    }

    /* renamed from: android.support.design.widget.ʼ$ˆ  reason: contains not printable characters */
    /* compiled from: BaseTransientBottomBar */
    static class C0063 extends FrameLayout {

        /* renamed from: ʻ  reason: contains not printable characters */
        private C0062 f262;

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0061 f263;

        C0063(Context context) {
            this(context, null);
        }

        C0063(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.SnackbarLayout);
            if (obtainStyledAttributes.hasValue(C0089.C0098.SnackbarLayout_elevation)) {
                C0414.m2217(this, (float) obtainStyledAttributes.getDimensionPixelSize(C0089.C0098.SnackbarLayout_elevation, 0));
            }
            obtainStyledAttributes.recycle();
            setClickable(true);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            C0062 r0 = this.f262;
            if (r0 != null) {
                r0.m400(this, i, i2, i3, i4);
            }
        }

        /* access modifiers changed from: protected */
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            C0061 r0 = this.f263;
            if (r0 != null) {
                r0.m398(this);
            }
            C0414.m2246(this);
        }

        /* access modifiers changed from: protected */
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            C0061 r0 = this.f263;
            if (r0 != null) {
                r0.m399(this);
            }
        }

        /* access modifiers changed from: package-private */
        public void setOnLayoutChangeListener(C0062 r1) {
            this.f262 = r1;
        }

        /* access modifiers changed from: package-private */
        public void setOnAttachStateChangeListener(C0061 r1) {
            this.f263 = r1;
        }
    }

    /* renamed from: android.support.design.widget.ʼ$ʼ  reason: contains not printable characters */
    /* compiled from: BaseTransientBottomBar */
    final class C0059 extends SwipeDismissBehavior<C0063> {
        C0059() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m395(View view) {
            return view instanceof C0063;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.SwipeDismissBehavior.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.ʼ$ˆ, android.view.MotionEvent]
         candidates:
          android.support.design.widget.ʼ.ʼ.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.ʼ$ˆ, android.view.MotionEvent):boolean
          android.support.design.widget.SwipeDismissBehavior.ʻ(float, float, float):float
          android.support.design.widget.SwipeDismissBehavior.ʻ(int, int, int):int
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.support.v4.ˉ.ﾞ):android.support.v4.ˉ.ﾞ
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.graphics.Rect):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View):boolean
          android.support.design.widget.SwipeDismissBehavior.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m394(CoordinatorLayout coordinatorLayout, C0063 r4, MotionEvent motionEvent) {
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked != 0) {
                if (actionMasked == 1 || actionMasked == 3) {
                    C0078.m491().m502(C0057.this.f241);
                }
            } else if (coordinatorLayout.m247(r4, (int) motionEvent.getX(), (int) motionEvent.getY())) {
                C0078.m491().m501(C0057.this.f241);
            }
            return super.m360(coordinatorLayout, (View) r4, motionEvent);
        }
    }
}
