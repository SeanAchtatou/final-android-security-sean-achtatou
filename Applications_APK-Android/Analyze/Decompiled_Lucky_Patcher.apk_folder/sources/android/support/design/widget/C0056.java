package android.support.design.widget;

import android.support.v4.ˉ.ʼ.C0388;
import android.support.v4.ˉ.ʼ.C0390;
import android.support.v4.ˉ.ʼ.C0391;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

/* renamed from: android.support.design.widget.ʻ  reason: contains not printable characters */
/* compiled from: AnimationUtils */
class C0056 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final Interpolator f233 = new LinearInterpolator();

    /* renamed from: ʼ  reason: contains not printable characters */
    static final Interpolator f234 = new C0390();

    /* renamed from: ʽ  reason: contains not printable characters */
    static final Interpolator f235 = new C0388();

    /* renamed from: ʾ  reason: contains not printable characters */
    static final Interpolator f236 = new C0391();

    /* renamed from: ʿ  reason: contains not printable characters */
    static final Interpolator f237 = new DecelerateInterpolator();
}
