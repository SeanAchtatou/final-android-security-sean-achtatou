package android.support.design.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.design.C0089;
import android.support.v4.content.C0101;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ʾ.C0317;
import android.support.v4.ˈ.C0344;
import android.support.v4.ˈ.C0348;
import android.support.v4.ˉ.C0355;
import android.support.v4.ˉ.C0396;
import android.support.v4.ˉ.C0410;
import android.support.v4.ˉ.C0411;
import android.support.v4.ˉ.C0412;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0439;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoordinatorLayout extends ViewGroup implements C0410 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final String f145;

    /* renamed from: ʼ  reason: contains not printable characters */
    static final Class<?>[] f146 = {Context.class, AttributeSet.class};

    /* renamed from: ʽ  reason: contains not printable characters */
    static final ThreadLocal<Map<String, Constructor<C0043>>> f147 = new ThreadLocal<>();

    /* renamed from: ʾ  reason: contains not printable characters */
    static final Comparator<View> f148;

    /* renamed from: ˆ  reason: contains not printable characters */
    private static final C0348.C0349<Rect> f149 = new C0348.C0351(12);

    /* renamed from: ʿ  reason: contains not printable characters */
    ViewGroup.OnHierarchyChangeListener f150;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final List<View> f151;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final C0065<View> f152;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final List<View> f153;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final List<View> f154;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final int[] f155;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Paint f156;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f157;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f158;

    /* renamed from: ـ  reason: contains not printable characters */
    private int[] f159;

    /* renamed from: ٴ  reason: contains not printable characters */
    private View f160;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private View f161;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private C0047 f162;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f163;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private C0439 f164;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f165;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private Drawable f166;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private C0412 f167;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final C0411 f168;

    @Retention(RetentionPolicy.RUNTIME)
    /* renamed from: android.support.design.widget.CoordinatorLayout$ʼ  reason: contains not printable characters */
    public @interface C0044 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Class<? extends C0043> m290();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static int m223(int i) {
        if ((i & 7) == 0) {
            i |= 8388611;
        }
        return (i & 112) == 0 ? i | 48 : i;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private static int m224(int i) {
        if (i == 0) {
            return 8388661;
        }
        return i;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private static int m226(int i) {
        if (i == 0) {
            return 17;
        }
        return i;
    }

    static {
        Package packageR = CoordinatorLayout.class.getPackage();
        f145 = packageR != null ? packageR.getName() : null;
        if (Build.VERSION.SDK_INT >= 21) {
            f148 = new C0049();
        } else {
            f148 = null;
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private static Rect m227() {
        Rect r0 = f149.m1962();
        return r0 == null ? new Rect() : r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m213(Rect rect) {
        rect.setEmpty();
        f149.m1963(rect);
    }

    public CoordinatorLayout(Context context) {
        this(context, null);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f151 = new ArrayList();
        this.f152 = new C0065<>();
        this.f153 = new ArrayList();
        this.f154 = new ArrayList();
        this.f155 = new int[2];
        this.f168 = new C0411(this);
        C0083.m512(context);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.CoordinatorLayout, i, C0089.C0097.Widget_Design_CoordinatorLayout);
        int resourceId = obtainStyledAttributes.getResourceId(C0089.C0098.CoordinatorLayout_keylines, 0);
        if (resourceId != 0) {
            Resources resources = context.getResources();
            this.f159 = resources.getIntArray(resourceId);
            float f = resources.getDisplayMetrics().density;
            int length = this.f159.length;
            for (int i2 = 0; i2 < length; i2++) {
                int[] iArr = this.f159;
                iArr[i2] = (int) (((float) iArr[i2]) * f);
            }
        }
        this.f166 = obtainStyledAttributes.getDrawable(C0089.C0098.CoordinatorLayout_statusBarBackground);
        obtainStyledAttributes.recycle();
        m233();
        super.setOnHierarchyChangeListener(new C0045());
    }

    public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener) {
        this.f150 = onHierarchyChangeListener;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        m230();
        if (this.f163) {
            if (this.f162 == null) {
                this.f162 = new C0047();
            }
            getViewTreeObserver().addOnPreDrawListener(this.f162);
        }
        if (this.f164 == null && C0414.m2247(this)) {
            C0414.m2246(this);
        }
        this.f158 = true;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        m230();
        if (this.f163 && this.f162 != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.f162);
        }
        View view = this.f161;
        if (view != null) {
            onStopNestedScroll(view);
        }
        this.f158 = false;
    }

    public void setStatusBarBackground(Drawable drawable) {
        Drawable drawable2 = this.f166;
        if (drawable2 != drawable) {
            Drawable drawable3 = null;
            if (drawable2 != null) {
                drawable2.setCallback(null);
            }
            if (drawable != null) {
                drawable3 = drawable.mutate();
            }
            this.f166 = drawable3;
            Drawable drawable4 = this.f166;
            if (drawable4 != null) {
                if (drawable4.isStateful()) {
                    this.f166.setState(getDrawableState());
                }
                C0288.m1709(this.f166, C0414.m2236(this));
                this.f166.setVisible(getVisibility() == 0, false);
                this.f166.setCallback(this);
            }
            C0414.m2233(this);
        }
    }

    public Drawable getStatusBarBackground() {
        return this.f166;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        Drawable drawable = this.f166;
        boolean z = false;
        if (drawable != null && drawable.isStateful()) {
            z = false | drawable.setState(drawableState);
        }
        if (z) {
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f166;
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        boolean z = i == 0;
        Drawable drawable = this.f166;
        if (drawable != null && drawable.isVisible() != z) {
            this.f166.setVisible(z, false);
        }
    }

    public void setStatusBarBackgroundResource(int i) {
        setStatusBarBackground(i != 0 ? C0101.m539(getContext(), i) : null);
    }

    public void setStatusBarBackgroundColor(int i) {
        setStatusBarBackground(new ColorDrawable(i));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final C0439 m237(C0439 r4) {
        if (C0344.m1958(this.f164, r4)) {
            return r4;
        }
        this.f164 = r4;
        boolean z = true;
        this.f165 = r4 != null && r4.m2399() > 0;
        if (this.f165 || getBackground() != null) {
            z = false;
        }
        setWillNotDraw(z);
        C0439 r42 = m221(r4);
        requestLayout();
        return r42;
    }

    /* access modifiers changed from: package-private */
    public final C0439 getLastWindowInsets() {
        return this.f164;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m230() {
        View view = this.f160;
        if (view != null) {
            C0043 r0 = ((C0046) view.getLayoutParams()).m302();
            if (r0 != null) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                r0.m282(this, this.f160, obtain);
                obtain.recycle();
            }
            this.f160 = null;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            ((C0046) getChildAt(i).getLayoutParams()).m308();
        }
        this.f157 = false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m218(List<View> list) {
        list.clear();
        boolean isChildrenDrawingOrderEnabled = isChildrenDrawingOrderEnabled();
        int childCount = getChildCount();
        for (int i = childCount - 1; i >= 0; i--) {
            list.add(getChildAt(isChildrenDrawingOrderEnabled ? getChildDrawingOrder(childCount, i) : i));
        }
        Comparator<View> comparator = f148;
        if (comparator != null) {
            Collections.sort(list, comparator);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m219(MotionEvent motionEvent, int i) {
        MotionEvent motionEvent2 = motionEvent;
        int i2 = i;
        int actionMasked = motionEvent.getActionMasked();
        List<View> list = this.f153;
        m218(list);
        int size = list.size();
        MotionEvent motionEvent3 = null;
        boolean z = false;
        boolean z2 = false;
        for (int i3 = 0; i3 < size; i3++) {
            View view = list.get(i3);
            C0046 r12 = (C0046) view.getLayoutParams();
            C0043 r13 = r12.m302();
            boolean z3 = true;
            if ((!z && !z2) || actionMasked == 0) {
                if (!z && r13 != null) {
                    if (i2 == 0) {
                        z = r13.m273(this, view, motionEvent2);
                    } else if (i2 == 1) {
                        z = r13.m282(this, view, motionEvent2);
                    }
                    if (z) {
                        this.f160 = view;
                    }
                }
                boolean r9 = r12.m307();
                boolean r11 = r12.m300(this, view);
                if (!r11 || r9) {
                    z3 = false;
                }
                if (r11 && !z3) {
                    break;
                }
                z2 = z3;
            } else if (r13 != null) {
                if (motionEvent3 == null) {
                    long uptimeMillis = SystemClock.uptimeMillis();
                    motionEvent3 = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                }
                if (i2 == 0) {
                    r13.m273(this, view, motionEvent3);
                } else if (i2 == 1) {
                    r13.m282(this, view, motionEvent3);
                }
            }
        }
        list.clear();
        return z;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            m230();
        }
        boolean r3 = m219(motionEvent, 0);
        if (actionMasked == 1 || actionMasked == 3) {
            m230();
        }
        return r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000e, code lost:
        if (r1 != false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r15) {
        /*
            r14 = this;
            int r0 = r15.getActionMasked()
            android.view.View r1 = r14.f160
            r2 = 1
            r3 = 0
            if (r1 != 0) goto L_0x0011
            boolean r1 = r14.m219(r15, r2)
            if (r1 == 0) goto L_0x0026
            goto L_0x0012
        L_0x0011:
            r1 = 0
        L_0x0012:
            android.view.View r4 = r14.f160
            android.view.ViewGroup$LayoutParams r4 = r4.getLayoutParams()
            android.support.design.widget.CoordinatorLayout$ʾ r4 = (android.support.design.widget.CoordinatorLayout.C0046) r4
            android.support.design.widget.CoordinatorLayout$ʻ r4 = r4.m302()
            if (r4 == 0) goto L_0x0026
            android.view.View r3 = r14.f160
            boolean r3 = r4.m282(r14, r3, r15)
        L_0x0026:
            android.view.View r4 = r14.f160
            r5 = 0
            if (r4 != 0) goto L_0x0031
            boolean r15 = super.onTouchEvent(r15)
            r3 = r3 | r15
            goto L_0x0043
        L_0x0031:
            if (r1 == 0) goto L_0x0043
            long r8 = android.os.SystemClock.uptimeMillis()
            r10 = 3
            r11 = 0
            r12 = 0
            r13 = 0
            r6 = r8
            android.view.MotionEvent r5 = android.view.MotionEvent.obtain(r6, r8, r10, r11, r12, r13)
            super.onTouchEvent(r5)
        L_0x0043:
            if (r5 == 0) goto L_0x0048
            r5.recycle()
        L_0x0048:
            if (r0 == r2) goto L_0x004d
            r15 = 3
            if (r0 != r15) goto L_0x0050
        L_0x004d:
            r14.m230()
        L_0x0050:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (z && !this.f157) {
            m230();
            this.f157 = true;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m220(int i) {
        int[] iArr = this.f159;
        if (iArr == null) {
            Log.e("CoordinatorLayout", "No keylines defined for " + this + " - attempted index lookup " + i);
            return 0;
        } else if (i >= 0 && i < iArr.length) {
            return iArr[i];
        } else {
            Log.e("CoordinatorLayout", "Keyline index " + i + " out of range for " + this);
            return 0;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static C0043 m212(Context context, AttributeSet attributeSet, String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith(".")) {
            str = context.getPackageName() + str;
        } else if (str.indexOf(46) < 0 && !TextUtils.isEmpty(f145)) {
            str = f145 + '.' + str;
        }
        try {
            Map map = f147.get();
            if (map == null) {
                map = new HashMap();
                f147.set(map);
            }
            Constructor<?> constructor = (Constructor) map.get(str);
            if (constructor == null) {
                constructor = Class.forName(str, true, context.getClassLoader()).getConstructor(f146);
                constructor.setAccessible(true);
                map.put(str, constructor);
            }
            return (C0043) constructor.newInstance(context, attributeSet);
        } catch (Exception e) {
            throw new RuntimeException("Could not inflate Behavior subclass " + str, e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0046 m235(View view) {
        C0046 r0 = (C0046) view.getLayoutParams();
        if (!r0.f172) {
            C0044 r1 = null;
            for (Class<?> cls = view.getClass(); cls != null; cls = cls.getSuperclass()) {
                r1 = (C0044) cls.getAnnotation(C0044.class);
                if (r1 != null) {
                    break;
                }
            }
            if (r1 != null) {
                try {
                    r0.m298((C0043) r1.m290().getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                } catch (Exception e) {
                    Log.e("CoordinatorLayout", "Default behavior class " + r1.m290().getName() + " could not be instantiated. Did you forget a default constructor?", e);
                }
            }
            r0.f172 = true;
        }
        return r0;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m232() {
        this.f151.clear();
        this.f152.m407();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            C0046 r4 = m235(childAt);
            r4.m303(this, childAt);
            this.f152.m408(childAt);
            for (int i2 = 0; i2 < childCount; i2++) {
                if (i2 != i) {
                    View childAt2 = getChildAt(i2);
                    if (r4.m301(this, childAt, childAt2)) {
                        if (!this.f152.m411(childAt2)) {
                            this.f152.m408(childAt2);
                        }
                        this.f152.m409(childAt2, childAt);
                    }
                }
            }
        }
        this.f151.addAll(this.f152.m410());
        Collections.reverse(this.f151);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m245(View view, Rect rect) {
        C0084.m515(this, view, rect);
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumWidth() {
        return Math.max(super.getSuggestedMinimumWidth(), getPaddingLeft() + getPaddingRight());
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumHeight() {
        return Math.max(super.getSuggestedMinimumHeight(), getPaddingTop() + getPaddingBottom());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m241(View view, int i, int i2, int i3, int i4) {
        measureChildWithMargins(view, i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0119, code lost:
        if (r0.m270(r1, r20, r11, r21, r23, 0) == false) goto L_0x0128;
     */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x011c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r31, int r32) {
        /*
            r30 = this;
            r7 = r30
            r30.m232()
            r30.m238()
            int r8 = r30.getPaddingLeft()
            int r0 = r30.getPaddingTop()
            int r9 = r30.getPaddingRight()
            int r1 = r30.getPaddingBottom()
            int r10 = android.support.v4.ˉ.C0414.m2236(r30)
            r2 = 1
            if (r10 != r2) goto L_0x0021
            r12 = 1
            goto L_0x0022
        L_0x0021:
            r12 = 0
        L_0x0022:
            int r13 = android.view.View.MeasureSpec.getMode(r31)
            int r14 = android.view.View.MeasureSpec.getSize(r31)
            int r15 = android.view.View.MeasureSpec.getMode(r32)
            int r16 = android.view.View.MeasureSpec.getSize(r32)
            int r17 = r8 + r9
            int r18 = r0 + r1
            int r0 = r30.getSuggestedMinimumWidth()
            int r1 = r30.getSuggestedMinimumHeight()
            android.support.v4.ˉ.ﾞ r3 = r7.f164
            if (r3 == 0) goto L_0x004b
            boolean r3 = android.support.v4.ˉ.C0414.m2247(r30)
            if (r3 == 0) goto L_0x004b
            r19 = 1
            goto L_0x004d
        L_0x004b:
            r19 = 0
        L_0x004d:
            java.util.List<android.view.View> r2 = r7.f151
            int r6 = r2.size()
            r4 = r0
            r2 = r1
            r3 = 0
            r5 = 0
        L_0x0057:
            if (r5 >= r6) goto L_0x016e
            java.util.List<android.view.View> r0 = r7.f151
            java.lang.Object r0 = r0.get(r5)
            r20 = r0
            android.view.View r20 = (android.view.View) r20
            int r0 = r20.getVisibility()
            r1 = 8
            if (r0 != r1) goto L_0x0071
            r22 = r5
            r29 = r6
            goto L_0x0168
        L_0x0071:
            android.view.ViewGroup$LayoutParams r0 = r20.getLayoutParams()
            r1 = r0
            android.support.design.widget.CoordinatorLayout$ʾ r1 = (android.support.design.widget.CoordinatorLayout.C0046) r1
            int r0 = r1.f175
            if (r0 < 0) goto L_0x00ba
            if (r13 == 0) goto L_0x00ba
            int r0 = r1.f175
            int r0 = r7.m220(r0)
            int r11 = r1.f173
            int r11 = m224(r11)
            int r11 = android.support.v4.ˉ.C0396.m2155(r11, r10)
            r11 = r11 & 7
            r22 = r2
            r2 = 3
            if (r11 != r2) goto L_0x0097
            if (r12 == 0) goto L_0x009c
        L_0x0097:
            r2 = 5
            if (r11 != r2) goto L_0x00a8
            if (r12 == 0) goto L_0x00a8
        L_0x009c:
            int r2 = r14 - r9
            int r2 = r2 - r0
            r0 = 0
            int r2 = java.lang.Math.max(r0, r2)
            r21 = r2
            r11 = 0
            goto L_0x00bf
        L_0x00a8:
            if (r11 != r2) goto L_0x00ac
            if (r12 == 0) goto L_0x00b1
        L_0x00ac:
            r2 = 3
            if (r11 != r2) goto L_0x00bc
            if (r12 == 0) goto L_0x00bc
        L_0x00b1:
            int r0 = r0 - r8
            r11 = 0
            int r0 = java.lang.Math.max(r11, r0)
            r21 = r0
            goto L_0x00bf
        L_0x00ba:
            r22 = r2
        L_0x00bc:
            r11 = 0
            r21 = 0
        L_0x00bf:
            if (r19 == 0) goto L_0x00f1
            boolean r0 = android.support.v4.ˉ.C0414.m2247(r20)
            if (r0 != 0) goto L_0x00f1
            android.support.v4.ˉ.ﾞ r0 = r7.f164
            int r0 = r0.m2397()
            android.support.v4.ˉ.ﾞ r2 = r7.f164
            int r2 = r2.m2400()
            int r0 = r0 + r2
            android.support.v4.ˉ.ﾞ r2 = r7.f164
            int r2 = r2.m2399()
            android.support.v4.ˉ.ﾞ r11 = r7.f164
            int r11 = r11.m2401()
            int r2 = r2 + r11
            int r0 = r14 - r0
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r13)
            int r2 = r16 - r2
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r15)
            r11 = r0
            r23 = r2
            goto L_0x00f5
        L_0x00f1:
            r11 = r31
            r23 = r32
        L_0x00f5:
            android.support.design.widget.CoordinatorLayout$ʻ r0 = r1.m302()
            if (r0 == 0) goto L_0x011c
            r24 = 0
            r2 = r1
            r1 = r30
            r26 = r2
            r25 = r22
            r2 = r20
            r27 = r3
            r3 = r11
            r28 = r4
            r4 = r21
            r22 = r5
            r5 = r23
            r29 = r6
            r6 = r24
            boolean r0 = r0.m270(r1, r2, r3, r4, r5, r6)
            if (r0 != 0) goto L_0x0135
            goto L_0x0128
        L_0x011c:
            r26 = r1
            r27 = r3
            r28 = r4
            r29 = r6
            r25 = r22
            r22 = r5
        L_0x0128:
            r5 = 0
            r0 = r30
            r1 = r20
            r2 = r11
            r3 = r21
            r4 = r23
            r0.m241(r1, r2, r3, r4, r5)
        L_0x0135:
            int r0 = r20.getMeasuredWidth()
            int r0 = r17 + r0
            r1 = r26
            int r2 = r1.leftMargin
            int r0 = r0 + r2
            int r2 = r1.rightMargin
            int r0 = r0 + r2
            r2 = r28
            int r0 = java.lang.Math.max(r2, r0)
            int r2 = r20.getMeasuredHeight()
            int r2 = r18 + r2
            int r3 = r1.topMargin
            int r2 = r2 + r3
            int r1 = r1.bottomMargin
            int r2 = r2 + r1
            r1 = r25
            int r1 = java.lang.Math.max(r1, r2)
            int r2 = r20.getMeasuredState()
            r11 = r27
            int r2 = android.view.View.combineMeasuredStates(r11, r2)
            r4 = r0
            r3 = r2
            r2 = r1
        L_0x0168:
            int r5 = r22 + 1
            r6 = r29
            goto L_0x0057
        L_0x016e:
            r1 = r2
            r11 = r3
            r2 = r4
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r0 & r11
            r3 = r31
            int r0 = android.view.View.resolveSizeAndState(r2, r3, r0)
            int r2 = r11 << 16
            r3 = r32
            int r1 = android.view.View.resolveSizeAndState(r1, r3, r2)
            r7.setMeasuredDimension(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.onMeasure(int, int):void");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0439 m221(C0439 r5) {
        C0043 r3;
        if (r5.m2403()) {
            return r5;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (C0414.m2247(childAt) && (r3 = ((C0046) childAt.getLayoutParams()).m302()) != null) {
                r5 = r3.m261(this, childAt, r5);
                if (r5.m2403()) {
                    break;
                }
            }
        }
        return r5;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m240(View view, int i) {
        C0046 r0 = (C0046) view.getLayoutParams();
        if (r0.m306()) {
            throw new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
        } else if (r0.f181 != null) {
            m217(view, r0.f181, i);
        } else if (r0.f175 >= 0) {
            m222(view, r0.f175, i);
        } else {
            m225(view, i);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        C0043 r6;
        int r2 = C0414.m2236(this);
        int size = this.f151.size();
        for (int i5 = 0; i5 < size; i5++) {
            View view = this.f151.get(i5);
            if (view.getVisibility() != 8 && ((r6 = ((C0046) view.getLayoutParams()).m302()) == null || !r6.m269(this, view, r2))) {
                m240(view, r2);
            }
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f165 && this.f166 != null) {
            C0439 r0 = this.f164;
            int r02 = r0 != null ? r0.m2399() : 0;
            if (r02 > 0) {
                this.f166.setBounds(0, 0, getWidth(), r02);
                this.f166.draw(canvas);
            }
        }
    }

    public void setFitsSystemWindows(boolean z) {
        super.setFitsSystemWindows(z);
        m233();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m252(View view, Rect rect) {
        ((C0046) view.getLayoutParams()).m297(rect);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m257(View view, Rect rect) {
        rect.set(((C0046) view.getLayoutParams()).m305());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m246(View view, boolean z, Rect rect) {
        if (view.isLayoutRequested() || view.getVisibility() == 8) {
            rect.setEmpty();
        } else if (z) {
            m245(view, rect);
        } else {
            rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m215(View view, int i, Rect rect, Rect rect2, C0046 r10, int i2, int i3) {
        int i4;
        int i5;
        int r6 = C0396.m2155(m226(r10.f173), i);
        int r7 = C0396.m2155(m223(r10.f174), i);
        int i6 = r6 & 7;
        int i7 = r6 & 112;
        int i8 = r7 & 7;
        int i9 = r7 & 112;
        if (i8 == 1) {
            i4 = rect.left + (rect.width() / 2);
        } else if (i8 != 5) {
            i4 = rect.left;
        } else {
            i4 = rect.right;
        }
        if (i9 == 16) {
            i5 = rect.top + (rect.height() / 2);
        } else if (i9 != 80) {
            i5 = rect.top;
        } else {
            i5 = rect.bottom;
        }
        if (i6 == 1) {
            i4 -= i2 / 2;
        } else if (i6 != 5) {
            i4 -= i2;
        }
        if (i7 == 16) {
            i5 -= i3 / 2;
        } else if (i7 != 80) {
            i5 -= i3;
        }
        rect2.set(i4, i5, i2 + i4, i3 + i5);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m214(C0046 r6, Rect rect, int i, int i2) {
        int width = getWidth();
        int height = getHeight();
        int max = Math.max(getPaddingLeft() + r6.leftMargin, Math.min(rect.left, ((width - getPaddingRight()) - i) - r6.rightMargin));
        int max2 = Math.max(getPaddingTop() + r6.topMargin, Math.min(rect.top, ((height - getPaddingBottom()) - i2) - r6.bottomMargin));
        rect.set(max, max2, i + max, i2 + max2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m244(View view, int i, Rect rect, Rect rect2) {
        C0046 r0 = (C0046) view.getLayoutParams();
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        m215(view, i, rect, rect2, r0, measuredWidth, measuredHeight);
        m214(r0, rect2, measuredWidth, measuredHeight);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m217(View view, View view2, int i) {
        C0046 r0 = (C0046) view.getLayoutParams();
        Rect r02 = m227();
        Rect r1 = m227();
        try {
            m245(view2, r02);
            m244(view, i, r02, r1);
            view.layout(r1.left, r1.top, r1.right, r1.bottom);
        } finally {
            m213(r02);
            m213(r1);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m222(View view, int i, int i2) {
        C0046 r0 = (C0046) view.getLayoutParams();
        int r1 = C0396.m2155(m224(r0.f173), i2);
        int i3 = r1 & 7;
        int i4 = r1 & 112;
        int width = getWidth();
        int height = getHeight();
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        if (i2 == 1) {
            i = width - i;
        }
        int r10 = m220(i) - measuredWidth;
        int i5 = 0;
        if (i3 == 1) {
            r10 += measuredWidth / 2;
        } else if (i3 == 5) {
            r10 += measuredWidth;
        }
        if (i4 == 16) {
            i5 = 0 + (measuredHeight / 2);
        } else if (i4 == 80) {
            i5 = measuredHeight + 0;
        }
        int max = Math.max(getPaddingLeft() + r0.leftMargin, Math.min(r10, ((width - getPaddingRight()) - measuredWidth) - r0.rightMargin));
        int max2 = Math.max(getPaddingTop() + r0.topMargin, Math.min(i5, ((height - getPaddingBottom()) - measuredHeight) - r0.bottomMargin));
        view.layout(max, max2, measuredWidth + max, measuredHeight + max2);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m225(View view, int i) {
        C0046 r0 = (C0046) view.getLayoutParams();
        Rect r7 = m227();
        r7.set(getPaddingLeft() + r0.leftMargin, getPaddingTop() + r0.topMargin, (getWidth() - getPaddingRight()) - r0.rightMargin, (getHeight() - getPaddingBottom()) - r0.bottomMargin);
        if (this.f164 != null && C0414.m2247(this) && !C0414.m2247(view)) {
            r7.left += this.f164.m2397();
            r7.top += this.f164.m2399();
            r7.right -= this.f164.m2400();
            r7.bottom -= this.f164.m2401();
        }
        Rect r8 = m227();
        C0396.m2156(m223(r0.f173), view.getMeasuredWidth(), view.getMeasuredHeight(), r7, r8, i);
        view.layout(r8.left, r8.top, r8.right, r8.bottom);
        m213(r7);
        m213(r8);
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        C0046 r0 = (C0046) view.getLayoutParams();
        if (r0.f171 != null) {
            float r1 = r0.f171.m287(this, view);
            if (r1 > 0.0f) {
                if (this.f156 == null) {
                    this.f156 = new Paint();
                }
                this.f156.setColor(r0.f171.m284(this, view));
                this.f156.setAlpha(C0317.m1833(Math.round(r1 * 255.0f), 0, 255));
                int save = canvas.save();
                if (view.isOpaque()) {
                    canvas.clipRect((float) view.getLeft(), (float) view.getTop(), (float) view.getRight(), (float) view.getBottom(), Region.Op.DIFFERENCE);
                }
                canvas.drawRect((float) getPaddingLeft(), (float) getPaddingTop(), (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()), this.f156);
                canvas.restoreToCount(save);
            }
        }
        return super.drawChild(canvas, view, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, boolean, android.graphics.Rect):void
     arg types: [android.view.View, int, android.graphics.Rect]
     candidates:
      android.support.design.widget.CoordinatorLayout.ʻ(android.content.Context, android.util.AttributeSet, java.lang.String):android.support.design.widget.CoordinatorLayout$ʻ
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, android.graphics.Rect, int):void
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, android.view.View, int):void
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, int, int):boolean
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, boolean, android.graphics.Rect):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m239(int i) {
        boolean z;
        int i2 = i;
        int r2 = C0414.m2236(this);
        int size = this.f151.size();
        Rect r4 = m227();
        Rect r5 = m227();
        Rect r6 = m227();
        for (int i3 = 0; i3 < size; i3++) {
            View view = this.f151.get(i3);
            C0046 r10 = (C0046) view.getLayoutParams();
            if (i2 != 0 || view.getVisibility() != 8) {
                for (int i4 = 0; i4 < i3; i4++) {
                    if (r10.f182 == this.f151.get(i4)) {
                        m251(view, r2);
                    }
                }
                m246(view, true, r5);
                if (r10.f177 != 0 && !r5.isEmpty()) {
                    int r12 = C0396.m2155(r10.f177, r2);
                    int i5 = r12 & 112;
                    if (i5 == 48) {
                        r4.top = Math.max(r4.top, r5.bottom);
                    } else if (i5 == 80) {
                        r4.bottom = Math.max(r4.bottom, getHeight() - r5.top);
                    }
                    int i6 = r12 & 7;
                    if (i6 == 3) {
                        r4.left = Math.max(r4.left, r5.right);
                    } else if (i6 == 5) {
                        r4.right = Math.max(r4.right, getWidth() - r5.left);
                    }
                }
                if (r10.f178 != 0 && view.getVisibility() == 0) {
                    m216(view, r4, r2);
                }
                if (i2 != 2) {
                    m257(view, r6);
                    if (!r6.equals(r5)) {
                        m252(view, r5);
                    }
                }
                for (int i7 = i3 + 1; i7 < size; i7++) {
                    View view2 = this.f151.get(i7);
                    C0046 r14 = (C0046) view2.getLayoutParams();
                    C0043 r15 = r14.m302();
                    if (r15 != null && r15.m274(this, view2, view)) {
                        if (i2 != 0 || !r14.m309()) {
                            if (i2 != 2) {
                                z = r15.m283(this, view2, view);
                            } else {
                                r15.m288(this, view2, view);
                                z = true;
                            }
                            if (i2 == 1) {
                                r14.m299(z);
                            }
                        } else {
                            r14.m310();
                        }
                    }
                }
            }
        }
        m213(r4);
        m213(r5);
        m213(r6);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m216(View view, Rect rect, int i) {
        boolean z;
        boolean z2;
        int width;
        int i2;
        int height;
        int i3;
        if (C0414.m2255(view) && view.getWidth() > 0 && view.getHeight() > 0) {
            C0046 r0 = (C0046) view.getLayoutParams();
            C0043 r1 = r0.m302();
            Rect r2 = m227();
            Rect r3 = m227();
            r3.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            if (r1 == null || !r1.m271(this, view, r2)) {
                r2.set(r3);
            } else if (!r3.contains(r2)) {
                throw new IllegalArgumentException("Rect should be within the child's bounds. Rect:" + r2.toShortString() + " | Bounds:" + r3.toShortString());
            }
            m213(r3);
            if (r2.isEmpty()) {
                m213(r2);
                return;
            }
            int r11 = C0396.m2155(r0.f178, i);
            if ((r11 & 48) != 48 || (i3 = (r2.top - r0.topMargin) - r0.f180) >= rect.top) {
                z = false;
            } else {
                m231(view, rect.top - i3);
                z = true;
            }
            if ((r11 & 80) == 80 && (height = ((getHeight() - r2.bottom) - r0.bottomMargin) + r0.f180) < rect.bottom) {
                m231(view, height - rect.bottom);
                z = true;
            }
            if (!z) {
                m231(view, 0);
            }
            if ((r11 & 3) != 3 || (i2 = (r2.left - r0.leftMargin) - r0.f179) >= rect.left) {
                z2 = false;
            } else {
                m228(view, rect.left - i2);
                z2 = true;
            }
            if ((r11 & 5) == 5 && (width = ((getWidth() - r2.right) - r0.rightMargin) + r0.f179) < rect.right) {
                m228(view, width - rect.right);
                z2 = true;
            }
            if (!z2) {
                m228(view, 0);
            }
            m213(r2);
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m228(View view, int i) {
        C0046 r0 = (C0046) view.getLayoutParams();
        if (r0.f179 != i) {
            C0414.m2234(view, i - r0.f179);
            r0.f179 = i;
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m231(View view, int i) {
        C0046 r0 = (C0046) view.getLayoutParams();
        if (r0.f180 != i) {
            C0414.m2231(view, i - r0.f180);
            r0.f180 = i;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m250(View view) {
        List r0 = this.f152.m412(view);
        if (r0 != null && !r0.isEmpty()) {
            for (int i = 0; i < r0.size(); i++) {
                View view2 = (View) r0.get(i);
                C0043 r3 = ((C0046) view2.getLayoutParams()).m302();
                if (r3 != null) {
                    r3.m283(this, view2, view);
                }
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public List<View> m254(View view) {
        List<View> r2 = this.f152.m413(view);
        this.f154.clear();
        if (r2 != null) {
            this.f154.addAll(r2);
        }
        return this.f154;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public List<View> m259(View view) {
        List r2 = this.f152.m412(view);
        this.f154.clear();
        if (r2 != null) {
            this.f154.addAll(r2);
        }
        return this.f154;
    }

    /* access modifiers changed from: package-private */
    public final List<View> getDependencySortedChildren() {
        m232();
        return Collections.unmodifiableList(this.f151);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m238() {
        int childCount = getChildCount();
        boolean z = false;
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            } else if (m229(getChildAt(i))) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z == this.f163) {
            return;
        }
        if (z) {
            m249();
        } else {
            m255();
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean m229(View view) {
        return this.f152.m414(view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m249() {
        if (this.f158) {
            if (this.f162 == null) {
                this.f162 = new C0047();
            }
            getViewTreeObserver().addOnPreDrawListener(this.f162);
        }
        this.f163 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m255() {
        if (this.f158 && this.f162 != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.f162);
        }
        this.f163 = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, boolean, android.graphics.Rect):void
     arg types: [android.view.View, int, android.graphics.Rect]
     candidates:
      android.support.design.widget.CoordinatorLayout.ʻ(android.content.Context, android.util.AttributeSet, java.lang.String):android.support.design.widget.CoordinatorLayout$ʻ
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, android.graphics.Rect, int):void
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, android.view.View, int):void
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, int, int):boolean
      android.support.design.widget.CoordinatorLayout.ʻ(android.view.View, boolean, android.graphics.Rect):void */
    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m251(View view, int i) {
        C0043 r0;
        View view2 = view;
        C0046 r10 = (C0046) view.getLayoutParams();
        if (r10.f181 != null) {
            Rect r11 = m227();
            Rect r12 = m227();
            Rect r13 = m227();
            m245(r10.f181, r11);
            boolean z = false;
            m246(view2, false, r12);
            int measuredWidth = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();
            int i2 = measuredHeight;
            m215(view, i, r11, r13, r10, measuredWidth, measuredHeight);
            if (!(r13.left == r12.left && r13.top == r12.top)) {
                z = true;
            }
            m214(r10, r13, measuredWidth, i2);
            int i3 = r13.left - r12.left;
            int i4 = r13.top - r12.top;
            if (i3 != 0) {
                C0414.m2234(view2, i3);
            }
            if (i4 != 0) {
                C0414.m2231(view2, i4);
            }
            if (z && (r0 = r10.m302()) != null) {
                r0.m283(this, view2, r10.f181);
            }
            m213(r11);
            m213(r12);
            m213(r13);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m247(View view, int i, int i2) {
        Rect r0 = m227();
        m245(view, r0);
        try {
            return r0.contains(i, i2);
        } finally {
            m213(r0);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0046 generateLayoutParams(AttributeSet attributeSet) {
        return new C0046(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0046 generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof C0046) {
            return new C0046((C0046) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new C0046((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new C0046(layoutParams);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public C0046 generateDefaultLayoutParams() {
        return new C0046(-2, -2);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof C0046) && super.checkLayoutParams(layoutParams);
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        return m248(view, view2, i, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.CoordinatorLayout.ʾ.ʻ(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.design.widget.CoordinatorLayout.ʾ.ʻ(android.view.View, android.support.design.widget.CoordinatorLayout):void
      android.support.design.widget.CoordinatorLayout.ʾ.ʻ(android.view.View, int):boolean
      android.support.design.widget.CoordinatorLayout.ʾ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View):boolean
      android.support.design.widget.CoordinatorLayout.ʾ.ʻ(int, boolean):void */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m248(View view, View view2, int i, int i2) {
        int i3 = i2;
        int childCount = getChildCount();
        boolean z = false;
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt = getChildAt(i4);
            if (childAt.getVisibility() != 8) {
                C0046 r13 = (C0046) childAt.getLayoutParams();
                C0043 r0 = r13.m302();
                if (r0 != null) {
                    boolean r02 = r0.m278(this, childAt, view, view2, i, i2);
                    r13.m296(i3, r02);
                    z |= r02;
                } else {
                    r13.m296(i3, false);
                }
            }
        }
        return z;
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        m253(view, view2, i, 0);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m253(View view, View view2, int i, int i2) {
        C0043 r2;
        this.f168.m2214(view, view2, i, i2);
        this.f161 = view2;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            C0046 r22 = (C0046) childAt.getLayoutParams();
            if (r22.m304(i2) && (r2 = r22.m302()) != null) {
                r2.m281(this, childAt, view, view2, i, i2);
            }
        }
    }

    public void onStopNestedScroll(View view) {
        m256(view, 0);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m256(View view, int i) {
        this.f168.m2212(view, i);
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            C0046 r3 = (C0046) childAt.getLayoutParams();
            if (r3.m304(i)) {
                C0043 r4 = r3.m302();
                if (r4 != null) {
                    r4.m264(this, childAt, view, i);
                }
                r3.m295(i);
                r3.m310();
            }
        }
        this.f161 = null;
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        m242(view, i, i2, i3, i4, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m242(View view, int i, int i2, int i3, int i4, int i5) {
        C0043 r1;
        int childCount = getChildCount();
        boolean z = false;
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                C0046 r12 = (C0046) childAt.getLayoutParams();
                if (r12.m304(i5) && (r1 = r12.m302()) != null) {
                    r1.m266(this, childAt, view, i, i2, i3, i4, i5);
                    z = true;
                }
            }
        }
        if (z) {
            m239(1);
        }
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        m243(view, i, i2, iArr, 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m243(View view, int i, int i2, int[] iArr, int i3) {
        C0043 r1;
        int i4;
        int i5;
        int childCount = getChildCount();
        boolean z = false;
        int i6 = 0;
        int i7 = 0;
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                C0046 r12 = (C0046) childAt.getLayoutParams();
                if (r12.m304(i3) && (r1 = r12.m302()) != null) {
                    int[] iArr2 = this.f155;
                    iArr2[1] = 0;
                    iArr2[0] = 0;
                    r1.m268(this, childAt, view, i, i2, iArr2, i3);
                    if (i > 0) {
                        i4 = Math.max(i6, this.f155[0]);
                    } else {
                        i4 = Math.min(i6, this.f155[0]);
                    }
                    if (i2 > 0) {
                        i5 = Math.max(i7, this.f155[1]);
                    } else {
                        i5 = Math.min(i7, this.f155[1]);
                    }
                    i6 = i4;
                    i7 = i5;
                    z = true;
                }
            }
        }
        iArr[0] = i6;
        iArr[1] = i7;
        if (z) {
            m239(1);
        }
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        C0043 r4;
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() != 8) {
                C0046 r42 = (C0046) childAt.getLayoutParams();
                if (r42.m304(0) && (r4 = r42.m302()) != null) {
                    z2 |= r4.m276(this, childAt, view, f, f2, z);
                }
            }
        }
        if (z2) {
            m239(1);
        }
        return z2;
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        C0043 r4;
        int childCount = getChildCount();
        boolean z = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() != 8) {
                C0046 r42 = (C0046) childAt.getLayoutParams();
                if (r42.m304(0) && (r4 = r42.m302()) != null) {
                    z |= r4.m275(this, childAt, view, f, f2);
                }
            }
        }
        return z;
    }

    public int getNestedScrollAxes() {
        return this.f168.m2210();
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$ʿ  reason: contains not printable characters */
    class C0047 implements ViewTreeObserver.OnPreDrawListener {
        C0047() {
        }

        public boolean onPreDraw() {
            CoordinatorLayout.this.m239(0);
            return true;
        }
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$ˈ  reason: contains not printable characters */
    static class C0049 implements Comparator<View> {
        C0049() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int compare(View view, View view2) {
            float r2 = C0414.m2256(view);
            float r3 = C0414.m2256(view2);
            if (r2 > r3) {
                return -1;
            }
            return r2 < r3 ? 1 : 0;
        }
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$ʻ  reason: contains not printable characters */
    public static abstract class C0043<V extends View> {
        /* renamed from: ʻ  reason: contains not printable characters */
        public C0439 m261(CoordinatorLayout coordinatorLayout, View view, C0439 r3) {
            return r3;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m262(C0046 r1) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m263(CoordinatorLayout coordinatorLayout, View view, Parcelable parcelable) {
        }

        @Deprecated
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m265(CoordinatorLayout coordinatorLayout, View view, View view2, int i, int i2, int i3, int i4) {
        }

        @Deprecated
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m267(CoordinatorLayout coordinatorLayout, View view, View view2, int i, int i2, int[] iArr) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m269(CoordinatorLayout coordinatorLayout, View view, int i) {
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m270(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m271(CoordinatorLayout coordinatorLayout, View view, Rect rect) {
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m272(CoordinatorLayout coordinatorLayout, View view, Rect rect, boolean z) {
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m273(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m274(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m275(CoordinatorLayout coordinatorLayout, View view, View view2, float f, float f2) {
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m276(CoordinatorLayout coordinatorLayout, View view, View view2, float f, float f2, boolean z) {
            return false;
        }

        @Deprecated
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m277(CoordinatorLayout coordinatorLayout, View view, View view2, View view3, int i) {
            return false;
        }

        @Deprecated
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m280(CoordinatorLayout coordinatorLayout, View view, View view2, View view3, int i) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m282(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
            return false;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m283(CoordinatorLayout coordinatorLayout, V v, View view) {
            return false;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public int m284(CoordinatorLayout coordinatorLayout, View view) {
            return -16777216;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public void m285() {
        }

        @Deprecated
        /* renamed from: ʽ  reason: contains not printable characters */
        public void m286(CoordinatorLayout coordinatorLayout, V v, View view) {
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public float m287(CoordinatorLayout coordinatorLayout, View view) {
            return 0.0f;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public void m288(CoordinatorLayout coordinatorLayout, V v, View view) {
        }

        public C0043() {
        }

        public C0043(Context context, AttributeSet attributeSet) {
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m289(CoordinatorLayout coordinatorLayout, V v) {
            return m287(coordinatorLayout, v) > 0.0f;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m278(CoordinatorLayout coordinatorLayout, View view, View view2, View view3, int i, int i2) {
            if (i2 == 0) {
                return m277(coordinatorLayout, view, view2, view3, i);
            }
            return false;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m281(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i, int i2) {
            if (i2 == 0) {
                m280(coordinatorLayout, v, view, view2, i);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m264(CoordinatorLayout coordinatorLayout, View view, View view2, int i) {
            if (i == 0) {
                m286(coordinatorLayout, view, view2);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m266(CoordinatorLayout coordinatorLayout, View view, View view2, int i, int i2, int i3, int i4, int i5) {
            if (i5 == 0) {
                m265(coordinatorLayout, view, view2, i, i2, i3, i4);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m268(CoordinatorLayout coordinatorLayout, View view, View view2, int i, int i2, int[] iArr, int i3) {
            if (i3 == 0) {
                m267(coordinatorLayout, view, view2, i, i2, iArr);
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Parcelable m279(CoordinatorLayout coordinatorLayout, View view) {
            return View.BaseSavedState.EMPTY_STATE;
        }
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$ʾ  reason: contains not printable characters */
    public static class C0046 extends ViewGroup.MarginLayoutParams {

        /* renamed from: ʻ  reason: contains not printable characters */
        C0043 f171;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f172 = false;

        /* renamed from: ʽ  reason: contains not printable characters */
        public int f173 = 0;

        /* renamed from: ʾ  reason: contains not printable characters */
        public int f174 = 0;

        /* renamed from: ʿ  reason: contains not printable characters */
        public int f175 = -1;

        /* renamed from: ˆ  reason: contains not printable characters */
        int f176 = -1;

        /* renamed from: ˈ  reason: contains not printable characters */
        public int f177 = 0;

        /* renamed from: ˉ  reason: contains not printable characters */
        public int f178 = 0;

        /* renamed from: ˊ  reason: contains not printable characters */
        int f179;

        /* renamed from: ˋ  reason: contains not printable characters */
        int f180;

        /* renamed from: ˎ  reason: contains not printable characters */
        View f181;

        /* renamed from: ˏ  reason: contains not printable characters */
        View f182;

        /* renamed from: ˑ  reason: contains not printable characters */
        final Rect f183 = new Rect();

        /* renamed from: י  reason: contains not printable characters */
        Object f184;

        /* renamed from: ـ  reason: contains not printable characters */
        private boolean f185;

        /* renamed from: ٴ  reason: contains not printable characters */
        private boolean f186;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private boolean f187;

        /* renamed from: ᴵ  reason: contains not printable characters */
        private boolean f188;

        public C0046(int i, int i2) {
            super(i, i2);
        }

        C0046(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.CoordinatorLayout_Layout);
            this.f173 = obtainStyledAttributes.getInteger(C0089.C0098.CoordinatorLayout_Layout_android_layout_gravity, 0);
            this.f176 = obtainStyledAttributes.getResourceId(C0089.C0098.CoordinatorLayout_Layout_layout_anchor, -1);
            this.f174 = obtainStyledAttributes.getInteger(C0089.C0098.CoordinatorLayout_Layout_layout_anchorGravity, 0);
            this.f175 = obtainStyledAttributes.getInteger(C0089.C0098.CoordinatorLayout_Layout_layout_keyline, -1);
            this.f177 = obtainStyledAttributes.getInt(C0089.C0098.CoordinatorLayout_Layout_layout_insetEdge, 0);
            this.f178 = obtainStyledAttributes.getInt(C0089.C0098.CoordinatorLayout_Layout_layout_dodgeInsetEdges, 0);
            this.f172 = obtainStyledAttributes.hasValue(C0089.C0098.CoordinatorLayout_Layout_layout_behavior);
            if (this.f172) {
                this.f171 = CoordinatorLayout.m212(context, attributeSet, obtainStyledAttributes.getString(C0089.C0098.CoordinatorLayout_Layout_layout_behavior));
            }
            obtainStyledAttributes.recycle();
            C0043 r5 = this.f171;
            if (r5 != null) {
                r5.m262(this);
            }
        }

        public C0046(C0046 r2) {
            super((ViewGroup.MarginLayoutParams) r2);
        }

        public C0046(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public C0046(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m294() {
            return this.f176;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public C0043 m302() {
            return this.f171;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m298(C0043 r2) {
            C0043 r0 = this.f171;
            if (r0 != r2) {
                if (r0 != null) {
                    r0.m285();
                }
                this.f171 = r2;
                this.f184 = null;
                this.f172 = true;
                if (r2 != null) {
                    r2.m262(this);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m297(Rect rect) {
            this.f183.set(rect);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public Rect m305() {
            return this.f183;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʾ  reason: contains not printable characters */
        public boolean m306() {
            return this.f181 == null && this.f176 != -1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m307() {
            if (this.f171 == null) {
                this.f185 = false;
            }
            return this.f185;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m300(CoordinatorLayout coordinatorLayout, View view) {
            boolean z = this.f185;
            if (z) {
                return true;
            }
            C0043 r1 = this.f171;
            boolean r3 = (r1 != null ? r1.m289(coordinatorLayout, view) : false) | z;
            this.f185 = r3;
            return r3;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public void m308() {
            this.f185 = false;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.CoordinatorLayout.ʾ.ʻ(int, boolean):void
         arg types: [int, int]
         candidates:
          android.support.design.widget.CoordinatorLayout.ʾ.ʻ(android.view.View, android.support.design.widget.CoordinatorLayout):void
          android.support.design.widget.CoordinatorLayout.ʾ.ʻ(android.view.View, int):boolean
          android.support.design.widget.CoordinatorLayout.ʾ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View):boolean
          android.support.design.widget.CoordinatorLayout.ʾ.ʻ(int, boolean):void */
        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m295(int i) {
            m296(i, false);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m296(int i, boolean z) {
            if (i == 0) {
                this.f186 = z;
            } else if (i == 1) {
                this.f187 = z;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m304(int i) {
            if (i == 0) {
                return this.f186;
            }
            if (i != 1) {
                return false;
            }
            return this.f187;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˈ  reason: contains not printable characters */
        public boolean m309() {
            return this.f188;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m299(boolean z) {
            this.f188 = z;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˉ  reason: contains not printable characters */
        public void m310() {
            this.f188 = false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m301(CoordinatorLayout coordinatorLayout, View view, View view2) {
            C0043 r0;
            return view2 == this.f182 || m292(view2, C0414.m2236(coordinatorLayout)) || ((r0 = this.f171) != null && r0.m274(coordinatorLayout, view, view2));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public View m303(CoordinatorLayout coordinatorLayout, View view) {
            if (this.f176 == -1) {
                this.f182 = null;
                this.f181 = null;
                return null;
            }
            if (this.f181 == null || !m293(view, coordinatorLayout)) {
                m291(view, coordinatorLayout);
            }
            return this.f181;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m291(View view, CoordinatorLayout coordinatorLayout) {
            this.f181 = coordinatorLayout.findViewById(this.f176);
            View view2 = this.f181;
            if (view2 != null) {
                if (view2 != coordinatorLayout) {
                    ViewParent parent = view2.getParent();
                    while (parent != coordinatorLayout && parent != null) {
                        if (parent != view) {
                            if (parent instanceof View) {
                                view2 = (View) parent;
                            }
                            parent = parent.getParent();
                        } else if (coordinatorLayout.isInEditMode()) {
                            this.f182 = null;
                            this.f181 = null;
                            return;
                        } else {
                            throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                        }
                    }
                    this.f182 = view2;
                } else if (coordinatorLayout.isInEditMode()) {
                    this.f182 = null;
                    this.f181 = null;
                } else {
                    throw new IllegalStateException("View can not be anchored to the the parent CoordinatorLayout");
                }
            } else if (coordinatorLayout.isInEditMode()) {
                this.f182 = null;
                this.f181 = null;
            } else {
                throw new IllegalStateException("Could not find CoordinatorLayout descendant view with id " + coordinatorLayout.getResources().getResourceName(this.f176) + " to anchor view " + view);
            }
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean m293(View view, CoordinatorLayout coordinatorLayout) {
            if (this.f181.getId() != this.f176) {
                return false;
            }
            View view2 = this.f181;
            for (ViewParent parent = view2.getParent(); parent != coordinatorLayout; parent = parent.getParent()) {
                if (parent == null || parent == view) {
                    this.f182 = null;
                    this.f181 = null;
                    return false;
                }
                if (parent instanceof View) {
                    view2 = (View) parent;
                }
            }
            this.f182 = view2;
            return true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean m292(View view, int i) {
            int r2 = C0396.m2155(((C0046) view.getLayoutParams()).f177, i);
            return r2 != 0 && (C0396.m2155(this.f178, i) & r2) == r2;
        }
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$ʽ  reason: contains not printable characters */
    private class C0045 implements ViewGroup.OnHierarchyChangeListener {
        C0045() {
        }

        public void onChildViewAdded(View view, View view2) {
            if (CoordinatorLayout.this.f150 != null) {
                CoordinatorLayout.this.f150.onChildViewAdded(view, view2);
            }
        }

        public void onChildViewRemoved(View view, View view2) {
            CoordinatorLayout.this.m239(2);
            if (CoordinatorLayout.this.f150 != null) {
                CoordinatorLayout.this.f150.onChildViewRemoved(view, view2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        if (!(parcelable instanceof C0048)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        C0048 r7 = (C0048) parcelable;
        super.onRestoreInstanceState(r7.m1995());
        SparseArray<Parcelable> sparseArray = r7.f190;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            int id = childAt.getId();
            C0043 r4 = m235(childAt).m302();
            if (!(id == -1 || r4 == null || (parcelable2 = sparseArray.get(id)) == null)) {
                r4.m263(this, childAt, parcelable2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable r4;
        C0048 r0 = new C0048(super.onSaveInstanceState());
        SparseArray<Parcelable> sparseArray = new SparseArray<>();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            int id = childAt.getId();
            C0043 r6 = ((C0046) childAt.getLayoutParams()).m302();
            if (!(id == -1 || r6 == null || (r4 = r6.m279(this, childAt)) == null)) {
                sparseArray.append(id, r4);
            }
        }
        r0.f190 = sparseArray;
        return r0;
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        C0043 r0 = ((C0046) view.getLayoutParams()).m302();
        if (r0 == null || !r0.m272(this, view, rect, z)) {
            return super.requestChildRectangleOnScreen(view, rect, z);
        }
        return true;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private void m233() {
        if (Build.VERSION.SDK_INT >= 21) {
            if (C0414.m2247(this)) {
                if (this.f167 == null) {
                    this.f167 = new C0412() {
                        /* renamed from: ʻ  reason: contains not printable characters */
                        public C0439 m260(View view, C0439 r2) {
                            return CoordinatorLayout.this.m237(r2);
                        }
                    };
                }
                C0414.m2225(this, this.f167);
                setSystemUiVisibility(1280);
                return;
            }
            C0414.m2225(this, (C0412) null);
        }
    }

    /* renamed from: android.support.design.widget.CoordinatorLayout$ˆ  reason: contains not printable characters */
    protected static class C0048 extends C0355 {
        public static final Parcelable.Creator<C0048> CREATOR = new Parcelable.ClassLoaderCreator<C0048>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0048 createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new C0048(parcel, classLoader);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0048 createFromParcel(Parcel parcel) {
                return new C0048(parcel, null);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0048[] newArray(int i) {
                return new C0048[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        SparseArray<Parcelable> f190;

        public C0048(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            int readInt = parcel.readInt();
            int[] iArr = new int[readInt];
            parcel.readIntArray(iArr);
            Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
            this.f190 = new SparseArray<>(readInt);
            for (int i = 0; i < readInt; i++) {
                this.f190.append(iArr[i], readParcelableArray[i]);
            }
        }

        public C0048(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            SparseArray<Parcelable> sparseArray = this.f190;
            int size = sparseArray != null ? sparseArray.size() : 0;
            parcel.writeInt(size);
            int[] iArr = new int[size];
            Parcelable[] parcelableArr = new Parcelable[size];
            for (int i2 = 0; i2 < size; i2++) {
                iArr[i2] = this.f190.keyAt(i2);
                parcelableArr[i2] = this.f190.valueAt(i2);
            }
            parcel.writeIntArray(iArr);
            parcel.writeParcelableArray(parcelableArr, i);
        }
    }
}
