package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.util.StateSet;
import java.util.ArrayList;

/* renamed from: android.support.design.widget.ˏ  reason: contains not printable characters */
/* compiled from: StateListAnimator */
final class C0081 {

    /* renamed from: ʻ  reason: contains not printable characters */
    ValueAnimator f355 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ArrayList<C0082> f356 = new ArrayList<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    private C0082 f357 = null;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final Animator.AnimatorListener f358 = new AnimatorListenerAdapter() {
        public void onAnimationEnd(Animator animator) {
            if (C0081.this.f355 == animator) {
                C0081.this.f355 = null;
            }
        }
    };

    C0081() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m511(int[] iArr, ValueAnimator valueAnimator) {
        C0082 r0 = new C0082(iArr, valueAnimator);
        valueAnimator.addListener(this.f358);
        this.f356.add(r0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m510(int[] iArr) {
        C0082 r2;
        int size = this.f356.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                r2 = null;
                break;
            }
            r2 = this.f356.get(i);
            if (StateSet.stateSetMatches(r2.f360, iArr)) {
                break;
            }
            i++;
        }
        C0082 r5 = this.f357;
        if (r2 != r5) {
            if (r5 != null) {
                m508();
            }
            this.f357 = r2;
            if (r2 != null) {
                m507(r2);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m507(C0082 r1) {
        this.f355 = r1.f361;
        this.f355.start();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m508() {
        ValueAnimator valueAnimator = this.f355;
        if (valueAnimator != null) {
            valueAnimator.cancel();
            this.f355 = null;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m509() {
        ValueAnimator valueAnimator = this.f355;
        if (valueAnimator != null) {
            valueAnimator.end();
            this.f355 = null;
        }
    }

    /* renamed from: android.support.design.widget.ˏ$ʻ  reason: contains not printable characters */
    /* compiled from: StateListAnimator */
    static class C0082 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final int[] f360;

        /* renamed from: ʼ  reason: contains not printable characters */
        final ValueAnimator f361;

        C0082(int[] iArr, ValueAnimator valueAnimator) {
            this.f360 = iArr;
            this.f361 = valueAnimator;
        }
    }
}
