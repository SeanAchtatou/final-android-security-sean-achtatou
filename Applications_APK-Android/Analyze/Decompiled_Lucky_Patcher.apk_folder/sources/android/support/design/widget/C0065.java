package android.support.design.widget;

import android.support.v4.ˈ.C0348;
import android.support.v4.ˈ.C0353;
import android.view.View;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/* renamed from: android.support.design.widget.ʾ  reason: contains not printable characters */
/* compiled from: DirectedAcyclicGraph */
final class C0065<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final C0348.C0349<ArrayList<T>> f276 = new C0348.C0350(10);

    /* renamed from: ʼ  reason: contains not printable characters */
    private final C0353<T, ArrayList<T>> f277 = new C0353<>();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ArrayList<T> f278 = new ArrayList<>();

    /* renamed from: ʾ  reason: contains not printable characters */
    private final HashSet<T> f279 = new HashSet<>();

    C0065() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m408(View view) {
        if (!this.f277.containsKey(view)) {
            this.f277.put(view, null);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m411(T t) {
        return this.f277.containsKey(t);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m409(T t, T t2) {
        if (!this.f277.containsKey(t) || !this.f277.containsKey(t2)) {
            throw new IllegalArgumentException("All nodes must be present in the graph before being added as an edge");
        }
        ArrayList arrayList = this.f277.get(t);
        if (arrayList == null) {
            arrayList = m406();
            this.f277.put(t, arrayList);
        }
        arrayList.add(t2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public List m412(T t) {
        return this.f277.get(t);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public List<T> m413(T t) {
        int size = this.f277.size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            ArrayList r3 = this.f277.m1980(i);
            if (r3 != null && r3.contains(t)) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(this.f277.m1979(i));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m414(T t) {
        int size = this.f277.size();
        for (int i = 0; i < size; i++) {
            ArrayList r3 = this.f277.m1980(i);
            if (r3 != null && r3.contains(t)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m407() {
        int size = this.f277.size();
        for (int i = 0; i < size; i++) {
            ArrayList r2 = this.f277.m1980(i);
            if (r2 != null) {
                m405(r2);
            }
        }
        this.f277.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public ArrayList<T> m410() {
        this.f278.clear();
        this.f279.clear();
        int size = this.f277.size();
        for (int i = 0; i < size; i++) {
            m404(this.f277.m1979(i), this.f278, this.f279);
        }
        return this.f278;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m404(T t, ArrayList<T> arrayList, HashSet<T> hashSet) {
        if (!arrayList.contains(t)) {
            if (!hashSet.contains(t)) {
                hashSet.add(t);
                ArrayList arrayList2 = this.f277.get(t);
                if (arrayList2 != null) {
                    int size = arrayList2.size();
                    for (int i = 0; i < size; i++) {
                        m404(arrayList2.get(i), arrayList, hashSet);
                    }
                }
                hashSet.remove(t);
                arrayList.add(t);
                return;
            }
            throw new RuntimeException("This graph contains cyclic dependencies");
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private ArrayList<T> m406() {
        ArrayList<T> r0 = this.f276.m1962();
        return r0 == null ? new ArrayList<>() : r0;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m405(ArrayList<View> arrayList) {
        arrayList.clear();
        this.f276.m1963(arrayList);
    }
}
