package android.support.design.widget;

import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.C0188;
import android.support.v4.ˉ.C0414;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public class SwipeDismissBehavior<V extends View> extends CoordinatorLayout.C0043<V> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f217;

    /* renamed from: ʼ  reason: contains not printable characters */
    C0188 f218;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0054 f219;

    /* renamed from: ʾ  reason: contains not printable characters */
    int f220 = 2;

    /* renamed from: ʿ  reason: contains not printable characters */
    float f221 = 0.5f;

    /* renamed from: ˆ  reason: contains not printable characters */
    float f222 = 0.0f;

    /* renamed from: ˈ  reason: contains not printable characters */
    float f223 = 0.5f;

    /* renamed from: ˉ  reason: contains not printable characters */
    private float f224 = 0.0f;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f225;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final C0188.C0189 f226 = new C0188.C0189() {

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f228;

        /* renamed from: ʽ  reason: contains not printable characters */
        private int f229 = -1;

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m369(View view, int i) {
            return this.f229 == -1 && SwipeDismissBehavior.this.m361(view);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m372(View view, int i) {
            this.f229 = i;
            this.f228 = view.getLeft();
            ViewParent parent = view.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m366(int i) {
            if (SwipeDismissBehavior.this.f219 != null) {
                SwipeDismissBehavior.this.f219.m373(i);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m367(View view, float f, float f2) {
            boolean z;
            int i;
            this.f229 = -1;
            int width = view.getWidth();
            if (m364(view, f)) {
                int left = view.getLeft();
                int i2 = this.f228;
                i = left < i2 ? i2 - width : i2 + width;
                z = true;
            } else {
                i = this.f228;
                z = false;
            }
            if (SwipeDismissBehavior.this.f218.m1088(i, view.getTop())) {
                C0414.m2226(view, new C0055(view, z));
            } else if (z && SwipeDismissBehavior.this.f219 != null) {
                SwipeDismissBehavior.this.f219.m374(view);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0029 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x003c A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean m364(android.view.View r6, float r7) {
            /*
                r5 = this;
                r0 = 0
                r1 = 0
                r2 = 1
                int r3 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
                if (r3 == 0) goto L_0x003e
                int r6 = android.support.v4.ˉ.C0414.m2236(r6)
                if (r6 != r2) goto L_0x000f
                r6 = 1
                goto L_0x0010
            L_0x000f:
                r6 = 0
            L_0x0010:
                android.support.design.widget.SwipeDismissBehavior r3 = android.support.design.widget.SwipeDismissBehavior.this
                int r3 = r3.f220
                r4 = 2
                if (r3 != r4) goto L_0x0018
                return r2
            L_0x0018:
                android.support.design.widget.SwipeDismissBehavior r3 = android.support.design.widget.SwipeDismissBehavior.this
                int r3 = r3.f220
                if (r3 != 0) goto L_0x002b
                if (r6 == 0) goto L_0x0025
                int r6 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
                if (r6 >= 0) goto L_0x002a
                goto L_0x0029
            L_0x0025:
                int r6 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
                if (r6 <= 0) goto L_0x002a
            L_0x0029:
                r1 = 1
            L_0x002a:
                return r1
            L_0x002b:
                android.support.design.widget.SwipeDismissBehavior r3 = android.support.design.widget.SwipeDismissBehavior.this
                int r3 = r3.f220
                if (r3 != r2) goto L_0x003d
                if (r6 == 0) goto L_0x0038
                int r6 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
                if (r6 <= 0) goto L_0x003d
                goto L_0x003c
            L_0x0038:
                int r6 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
                if (r6 >= 0) goto L_0x003d
            L_0x003c:
                r1 = 1
            L_0x003d:
                return r1
            L_0x003e:
                int r7 = r6.getLeft()
                int r0 = r5.f228
                int r7 = r7 - r0
                int r6 = r6.getWidth()
                float r6 = (float) r6
                android.support.design.widget.SwipeDismissBehavior r0 = android.support.design.widget.SwipeDismissBehavior.this
                float r0 = r0.f221
                float r6 = r6 * r0
                int r6 = java.lang.Math.round(r6)
                int r7 = java.lang.Math.abs(r7)
                if (r7 < r6) goto L_0x005b
                r1 = 1
            L_0x005b:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.SwipeDismissBehavior.AnonymousClass1.m364(android.view.View, float):boolean");
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m370(View view) {
            return view.getWidth();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m371(View view, int i, int i2) {
            int i3;
            int i4;
            int width;
            boolean z = C0414.m2236(view) == 1;
            if (SwipeDismissBehavior.this.f220 != 0) {
                if (SwipeDismissBehavior.this.f220 != 1) {
                    i3 = this.f228 - view.getWidth();
                    i4 = view.getWidth() + this.f228;
                } else if (z) {
                    i3 = this.f228;
                    width = view.getWidth();
                } else {
                    i3 = this.f228 - view.getWidth();
                    i4 = this.f228;
                }
                return SwipeDismissBehavior.m354(i3, i, i4);
            } else if (z) {
                i3 = this.f228 - view.getWidth();
                i4 = this.f228;
                return SwipeDismissBehavior.m354(i3, i, i4);
            } else {
                i3 = this.f228;
                width = view.getWidth();
            }
            i4 = width + i3;
            return SwipeDismissBehavior.m354(i3, i, i4);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m365(View view, int i, int i2) {
            return view.getTop();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m368(View view, int i, int i2, int i3, int i4) {
            float width = ((float) this.f228) + (((float) view.getWidth()) * SwipeDismissBehavior.this.f222);
            float width2 = ((float) this.f228) + (((float) view.getWidth()) * SwipeDismissBehavior.this.f223);
            float f = (float) i;
            if (f <= width) {
                view.setAlpha(1.0f);
            } else if (f >= width2) {
                view.setAlpha(0.0f);
            } else {
                view.setAlpha(SwipeDismissBehavior.m353(0.0f, 1.0f - SwipeDismissBehavior.m356(width, width2, f), 1.0f));
            }
        }
    };

    /* renamed from: android.support.design.widget.SwipeDismissBehavior$ʻ  reason: contains not printable characters */
    public interface C0054 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m373(int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        void m374(View view);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static float m356(float f, float f2, float f3) {
        return (f3 - f) / (f2 - f);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m361(View view) {
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m359(C0054 r1) {
        this.f219 = r1;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m358(int i) {
        this.f220 = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m357(float f) {
        this.f222 = m353(0.0f, f, 1.0f);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m362(float f) {
        this.f223 = m353(0.0f, f, 1.0f);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m360(CoordinatorLayout coordinatorLayout, View view, MotionEvent motionEvent) {
        boolean z = this.f217;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.f217 = coordinatorLayout.m247(view, (int) motionEvent.getX(), (int) motionEvent.getY());
            z = this.f217;
        } else if (actionMasked == 1 || actionMasked == 3) {
            this.f217 = false;
        }
        if (!z) {
            return false;
        }
        m355((ViewGroup) coordinatorLayout);
        return this.f218.m1089(motionEvent);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m363(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
        C0188 r1 = this.f218;
        if (r1 == null) {
            return false;
        }
        r1.m1093(motionEvent);
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m355(ViewGroup viewGroup) {
        C0188 r3;
        if (this.f218 == null) {
            if (this.f225) {
                r3 = C0188.m1066(viewGroup, this.f224, this.f226);
            } else {
                r3 = C0188.m1067(viewGroup, this.f226);
            }
            this.f218 = r3;
        }
    }

    /* renamed from: android.support.design.widget.SwipeDismissBehavior$ʼ  reason: contains not printable characters */
    private class C0055 implements Runnable {

        /* renamed from: ʼ  reason: contains not printable characters */
        private final View f231;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final boolean f232;

        C0055(View view, boolean z) {
            this.f231 = view;
            this.f232 = z;
        }

        public void run() {
            if (SwipeDismissBehavior.this.f218 != null && SwipeDismissBehavior.this.f218.m1091(true)) {
                C0414.m2226(this.f231, this);
            } else if (this.f232 && SwipeDismissBehavior.this.f219 != null) {
                SwipeDismissBehavior.this.f219.m374(this.f231);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static float m353(float f, float f2, float f3) {
        return Math.min(Math.max(f, f2), f3);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static int m354(int i, int i2, int i3) {
        return Math.min(Math.max(i, i2), i3);
    }
}
