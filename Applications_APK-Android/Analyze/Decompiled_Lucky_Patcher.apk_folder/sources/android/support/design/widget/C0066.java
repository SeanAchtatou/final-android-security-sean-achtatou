package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.ʼ.ʻ.C0288;
import android.support.v4.ˉ.C0414;
import android.view.ViewTreeObserver;
import android.view.animation.Interpolator;

/* renamed from: android.support.design.widget.ʿ  reason: contains not printable characters */
/* compiled from: FloatingActionButtonImpl */
class C0066 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final Interpolator f280 = C0056.f235;

    /* renamed from: ˋ  reason: contains not printable characters */
    static final int[] f281 = {16842919, 16842910};

    /* renamed from: ˎ  reason: contains not printable characters */
    static final int[] f282 = {16842908, 16842910};

    /* renamed from: ˏ  reason: contains not printable characters */
    static final int[] f283 = {16842910};

    /* renamed from: ˑ  reason: contains not printable characters */
    static final int[] f284 = new int[0];

    /* renamed from: ʼ  reason: contains not printable characters */
    int f285 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    C0076 f286;

    /* renamed from: ʾ  reason: contains not printable characters */
    Drawable f287;

    /* renamed from: ʿ  reason: contains not printable characters */
    Drawable f288;

    /* renamed from: ˆ  reason: contains not printable characters */
    C0064 f289;

    /* renamed from: ˈ  reason: contains not printable characters */
    Drawable f290;

    /* renamed from: ˉ  reason: contains not printable characters */
    float f291;

    /* renamed from: ˊ  reason: contains not printable characters */
    float f292;

    /* renamed from: י  reason: contains not printable characters */
    final C0088 f293;

    /* renamed from: ـ  reason: contains not printable characters */
    final C0077 f294;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final C0081 f295;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f296;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final Rect f297 = new Rect();

    /* renamed from: ᵎ  reason: contains not printable characters */
    private ViewTreeObserver.OnPreDrawListener f298;

    /* renamed from: android.support.design.widget.ʿ$ʽ  reason: contains not printable characters */
    /* compiled from: FloatingActionButtonImpl */
    interface C0069 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m443();

        /* renamed from: ʼ  reason: contains not printable characters */
        void m444();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m430(Rect rect) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m433() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m437() {
        return true;
    }

    C0066(C0088 r2, C0077 r3) {
        this.f293 = r2;
        this.f294 = r3;
        this.f295 = new C0081();
        this.f295.m511(f281, m415(new C0068()));
        this.f295.m511(f282, m415(new C0068()));
        this.f295.m511(f283, m415(new C0070()));
        this.f295.m511(f284, m415(new C0067()));
        this.f296 = this.f293.getRotation();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m424(ColorStateList colorStateList) {
        Drawable drawable = this.f287;
        if (drawable != null) {
            C0288.m1703(drawable, colorStateList);
        }
        C0064 r0 = this.f289;
        if (r0 != null) {
            r0.m403(colorStateList);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m425(PorterDuff.Mode mode) {
        Drawable drawable = this.f287;
        if (drawable != null) {
            C0288.m1706(drawable, mode);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m423(int i) {
        Drawable drawable = this.f288;
        if (drawable != null) {
            C0288.m1703(drawable, m416(i));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m421(float f) {
        if (this.f291 != f) {
            this.f291 = f;
            m422(f, this.f292);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public float m420() {
        return this.f291;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m422(float f, float f2) {
        C0076 r3 = this.f286;
        if (r3 != null) {
            r3.m485(f, this.f292 + f);
            m434();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m428(int[] iArr) {
        this.f295.m510(iArr);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m429() {
        this.f295.m509();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m427(final C0069 r4, final boolean z) {
        if (!m440()) {
            this.f293.animate().cancel();
            if (m418()) {
                this.f285 = 1;
                this.f293.animate().scaleX(0.0f).scaleY(0.0f).alpha(0.0f).setDuration(200).setInterpolator(C0056.f235).setListener(new AnimatorListenerAdapter() {

                    /* renamed from: ʾ  reason: contains not printable characters */
                    private boolean f302;

                    public void onAnimationStart(Animator animator) {
                        C0066.this.f293.m526(0, z);
                        this.f302 = false;
                    }

                    public void onAnimationCancel(Animator animator) {
                        this.f302 = true;
                    }

                    public void onAnimationEnd(Animator animator) {
                        C0066 r3 = C0066.this;
                        r3.f285 = 0;
                        if (!this.f302) {
                            r3.f293.m526(z ? 8 : 4, z);
                            C0069 r32 = r4;
                            if (r32 != null) {
                                r32.m444();
                            }
                        }
                    }
                });
                return;
            }
            this.f293.m526(z ? 8 : 4, z);
            if (r4 != null) {
                r4.m444();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m431(final C0069 r4, final boolean z) {
        if (!m439()) {
            this.f293.animate().cancel();
            if (m418()) {
                this.f285 = 2;
                if (this.f293.getVisibility() != 0) {
                    this.f293.setAlpha(0.0f);
                    this.f293.setScaleY(0.0f);
                    this.f293.setScaleX(0.0f);
                }
                this.f293.animate().scaleX(1.0f).scaleY(1.0f).alpha(1.0f).setDuration(200).setInterpolator(C0056.f236).setListener(new AnimatorListenerAdapter() {
                    public void onAnimationStart(Animator animator) {
                        C0066.this.f293.m526(0, z);
                    }

                    public void onAnimationEnd(Animator animator) {
                        C0066.this.f285 = 0;
                        C0069 r2 = r4;
                        if (r2 != null) {
                            r2.m443();
                        }
                    }
                });
                return;
            }
            this.f293.m526(0, z);
            this.f293.setAlpha(1.0f);
            this.f293.setScaleY(1.0f);
            this.f293.setScaleX(1.0f);
            if (r4 != null) {
                r4.m443();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final Drawable m432() {
        return this.f290;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public final void m434() {
        Rect rect = this.f297;
        m426(rect);
        m430(rect);
        this.f294.m488(rect.left, rect.top, rect.right, rect.bottom);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m426(Rect rect) {
        this.f286.getPadding(rect);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m435() {
        if (m437()) {
            m417();
            this.f293.getViewTreeObserver().addOnPreDrawListener(this.f298);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m436() {
        if (this.f298 != null) {
            this.f293.getViewTreeObserver().removeOnPreDrawListener(this.f298);
            this.f298 = null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public void m438() {
        float rotation = this.f293.getRotation();
        if (this.f296 != rotation) {
            this.f296 = rotation;
            m419();
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m417() {
        if (this.f298 == null) {
            this.f298 = new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    C0066.this.m438();
                    return true;
                }
            };
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m439() {
        if (this.f293.getVisibility() != 0) {
            if (this.f285 == 2) {
                return true;
            }
            return false;
        } else if (this.f285 != 1) {
            return true;
        } else {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m440() {
        if (this.f293.getVisibility() == 0) {
            if (this.f285 == 1) {
                return true;
            }
            return false;
        } else if (this.f285 != 2) {
            return true;
        } else {
            return false;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private ValueAnimator m415(C0071 r4) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(f280);
        valueAnimator.setDuration(100L);
        valueAnimator.addListener(r4);
        valueAnimator.addUpdateListener(r4);
        valueAnimator.setFloatValues(0.0f, 1.0f);
        return valueAnimator;
    }

    /* renamed from: android.support.design.widget.ʿ$ʿ  reason: contains not printable characters */
    /* compiled from: FloatingActionButtonImpl */
    private abstract class C0071 extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f310;

        /* renamed from: ʽ  reason: contains not printable characters */
        private float f312;

        /* renamed from: ʾ  reason: contains not printable characters */
        private float f313;

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public abstract float m446();

        private C0071() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (!this.f310) {
                this.f312 = C0066.this.f286.m483();
                this.f313 = m446();
                this.f310 = true;
            }
            C0076 r0 = C0066.this.f286;
            float f = this.f312;
            r0.m486(f + ((this.f313 - f) * valueAnimator.getAnimatedFraction()));
        }

        public void onAnimationEnd(Animator animator) {
            C0066.this.f286.m486(this.f313);
            this.f310 = false;
        }
    }

    /* renamed from: android.support.design.widget.ʿ$ʾ  reason: contains not printable characters */
    /* compiled from: FloatingActionButtonImpl */
    private class C0070 extends C0071 {
        C0070() {
            super();
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public float m445() {
            return C0066.this.f291;
        }
    }

    /* renamed from: android.support.design.widget.ʿ$ʼ  reason: contains not printable characters */
    /* compiled from: FloatingActionButtonImpl */
    private class C0068 extends C0071 {
        C0068() {
            super();
        }

        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public float m442() {
            return C0066.this.f291 + C0066.this.f292;
        }
    }

    /* renamed from: android.support.design.widget.ʿ$ʻ  reason: contains not printable characters */
    /* compiled from: FloatingActionButtonImpl */
    private class C0067 extends C0071 {
        /* access modifiers changed from: protected */
        /* renamed from: ʻ  reason: contains not printable characters */
        public float m441() {
            return 0.0f;
        }

        C0067() {
            super();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static ColorStateList m416(int i) {
        return new ColorStateList(new int[][]{f282, f281, new int[0]}, new int[]{i, i, 0});
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean m418() {
        return C0414.m2255(this.f293) && !this.f293.isInEditMode();
    }

    /* renamed from: י  reason: contains not printable characters */
    private void m419() {
        if (Build.VERSION.SDK_INT == 19) {
            if (this.f296 % 90.0f != 0.0f) {
                if (this.f293.getLayerType() != 1) {
                    this.f293.setLayerType(1, null);
                }
            } else if (this.f293.getLayerType() != 0) {
                this.f293.setLayerType(0, null);
            }
        }
        C0076 r0 = this.f286;
        if (r0 != null) {
            r0.m484(-this.f296);
        }
        C0064 r02 = this.f289;
        if (r02 != null) {
            r02.m402(-this.f296);
        }
    }
}
