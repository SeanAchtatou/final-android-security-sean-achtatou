package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.C0089;
import android.support.design.widget.C0066;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.ˉ.C0414;
import android.support.v7.widget.C0671;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

@CoordinatorLayout.C0044(m290 = Behavior.class)
public class FloatingActionButton extends C0088 {

    /* renamed from: ʻ  reason: contains not printable characters */
    int f191;

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean f192;

    /* renamed from: ʽ  reason: contains not printable characters */
    final Rect f193;

    /* renamed from: ʾ  reason: contains not printable characters */
    private ColorStateList f194;

    /* renamed from: ʿ  reason: contains not printable characters */
    private PorterDuff.Mode f195;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f196;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f197;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f198;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final Rect f199;

    /* renamed from: ˋ  reason: contains not printable characters */
    private C0671 f200;

    /* renamed from: ˎ  reason: contains not printable characters */
    private C0066 f201;

    /* renamed from: android.support.design.widget.FloatingActionButton$ʻ  reason: contains not printable characters */
    public static abstract class C0050 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m337(FloatingActionButton floatingActionButton) {
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m338(FloatingActionButton floatingActionButton) {
        }
    }

    public /* bridge */ /* synthetic */ void setVisibility(int i) {
        super.setVisibility(i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int sizeDimension = getSizeDimension();
        this.f191 = (sizeDimension - this.f198) / 2;
        getImpl().m434();
        int min = Math.min(m316(sizeDimension, i), m316(sizeDimension, i2));
        setMeasuredDimension(this.f193.left + min + this.f193.right, min + this.f193.top + this.f193.bottom);
    }

    public int getRippleColor() {
        return this.f196;
    }

    public void setRippleColor(int i) {
        if (this.f196 != i) {
            this.f196 = i;
            getImpl().m423(i);
        }
    }

    public ColorStateList getBackgroundTintList() {
        return this.f194;
    }

    public void setBackgroundTintList(ColorStateList colorStateList) {
        if (this.f194 != colorStateList) {
            this.f194 = colorStateList;
            getImpl().m424(colorStateList);
        }
    }

    public PorterDuff.Mode getBackgroundTintMode() {
        return this.f195;
    }

    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f195 != mode) {
            this.f195 = mode;
            getImpl().m425(mode);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    public void setBackgroundResource(int i) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    public void setBackgroundColor(int i) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    public void setImageResource(int i) {
        this.f200.m4236(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m320(C0050 r2, boolean z) {
        getImpl().m431(m317(r2), z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m322(C0050 r2, boolean z) {
        getImpl().m427(m317(r2), z);
    }

    public void setUseCompatPadding(boolean z) {
        if (this.f192 != z) {
            this.f192 = z;
            getImpl().m433();
        }
    }

    public boolean getUseCompatPadding() {
        return this.f192;
    }

    public void setSize(int i) {
        if (i != this.f197) {
            this.f197 = i;
            requestLayout();
        }
    }

    public int getSize() {
        return this.f197;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0066.C0069 m317(final C0050 r2) {
        if (r2 == null) {
            return null;
        }
        return new C0066.C0069() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public void m323() {
                r2.m337(FloatingActionButton.this);
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public void m324() {
                r2.m338(FloatingActionButton.this);
            }
        };
    }

    /* access modifiers changed from: package-private */
    public int getSizeDimension() {
        return m315(this.f197);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m315(int i) {
        Resources resources = getResources();
        if (i != -1) {
            if (i != 1) {
                return resources.getDimensionPixelSize(C0089.C0092.design_fab_size_normal);
            }
            return resources.getDimensionPixelSize(C0089.C0092.design_fab_size_mini);
        } else if (Math.max(resources.getConfiguration().screenWidthDp, resources.getConfiguration().screenHeightDp) < 470) {
            return m315(1);
        } else {
            return m315(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getImpl().m435();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getImpl().m436();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        getImpl().m428(getDrawableState());
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        getImpl().m429();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m321(Rect rect) {
        if (!C0414.m2255(this)) {
            return false;
        }
        rect.set(0, 0, getWidth(), getHeight());
        rect.left += this.f193.left;
        rect.top += this.f193.top;
        rect.right -= this.f193.right;
        rect.bottom -= this.f193.bottom;
        return true;
    }

    public Drawable getContentBackground() {
        return getImpl().m432();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int m316(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode != Integer.MIN_VALUE) {
            return (mode == 0 || mode != 1073741824) ? i : size;
        }
        return Math.min(i, size);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && m321(this.f199) && !this.f199.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public static class Behavior extends CoordinatorLayout.C0043<FloatingActionButton> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Rect f204;

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0050 f205;

        /* renamed from: ʽ  reason: contains not printable characters */
        private boolean f206;

        public Behavior() {
            this.f206 = true;
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.FloatingActionButton_Behavior_Layout);
            this.f206 = obtainStyledAttributes.getBoolean(C0089.C0098.FloatingActionButton_Behavior_Layout_behavior_autoHide, true);
            obtainStyledAttributes.recycle();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m330(CoordinatorLayout.C0046 r2) {
            if (r2.f178 == 0) {
                r2.f178 = 80;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m336(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, View view) {
            if (view instanceof AppBarLayout) {
                m326(coordinatorLayout, (AppBarLayout) view, floatingActionButton);
                return false;
            } else if (!m327(view)) {
                return false;
            } else {
                m329(view, floatingActionButton);
                return false;
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private static boolean m327(View view) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.C0046) {
                return ((CoordinatorLayout.C0046) layoutParams).m302() instanceof BottomSheetBehavior;
            }
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean m328(View view, FloatingActionButton floatingActionButton) {
            CoordinatorLayout.C0046 r0 = (CoordinatorLayout.C0046) floatingActionButton.getLayoutParams();
            if (this.f206 && r0.m294() == view.getId() && floatingActionButton.getUserSetVisibility() == 0) {
                return true;
            }
            return false;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.FloatingActionButton.ʻ(android.support.design.widget.FloatingActionButton$ʻ, boolean):void
         arg types: [android.support.design.widget.FloatingActionButton$ʻ, int]
         candidates:
          android.support.design.widget.FloatingActionButton.ʻ(int, int):int
          android.support.design.widget.FloatingActionButton.ʻ(android.support.design.widget.FloatingActionButton, android.graphics.drawable.Drawable):void
          android.support.design.widget.ᴵ.ʻ(int, boolean):void
          android.support.design.widget.FloatingActionButton.ʻ(android.support.design.widget.FloatingActionButton$ʻ, boolean):void */
        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean m326(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, FloatingActionButton floatingActionButton) {
            if (!m328(appBarLayout, floatingActionButton)) {
                return false;
            }
            if (this.f204 == null) {
                this.f204 = new Rect();
            }
            Rect rect = this.f204;
            C0084.m515(coordinatorLayout, appBarLayout, rect);
            if (rect.bottom <= appBarLayout.getMinimumHeightForVisibleOverlappingContent()) {
                floatingActionButton.m322(this.f205, false);
                return true;
            }
            floatingActionButton.m320(this.f205, false);
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.FloatingActionButton.ʻ(android.support.design.widget.FloatingActionButton$ʻ, boolean):void
         arg types: [android.support.design.widget.FloatingActionButton$ʻ, int]
         candidates:
          android.support.design.widget.FloatingActionButton.ʻ(int, int):int
          android.support.design.widget.FloatingActionButton.ʻ(android.support.design.widget.FloatingActionButton, android.graphics.drawable.Drawable):void
          android.support.design.widget.ᴵ.ʻ(int, boolean):void
          android.support.design.widget.FloatingActionButton.ʻ(android.support.design.widget.FloatingActionButton$ʻ, boolean):void */
        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean m329(View view, FloatingActionButton floatingActionButton) {
            if (!m328(view, floatingActionButton)) {
                return false;
            }
            if (view.getTop() < (floatingActionButton.getHeight() / 2) + ((CoordinatorLayout.C0046) floatingActionButton.getLayoutParams()).topMargin) {
                floatingActionButton.m322(this.f205, false);
                return true;
            }
            floatingActionButton.m320(this.f205, false);
            return true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m334(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, int i) {
            List<View> r0 = coordinatorLayout.m254(floatingActionButton);
            int size = r0.size();
            for (int i2 = 0; i2 < size; i2++) {
                View view = r0.get(i2);
                if (!(view instanceof AppBarLayout)) {
                    if (m327(view) && m329(view, floatingActionButton)) {
                        break;
                    }
                } else if (m326(coordinatorLayout, (AppBarLayout) view, floatingActionButton)) {
                    break;
                }
            }
            coordinatorLayout.m240(floatingActionButton, i);
            m325(coordinatorLayout, floatingActionButton);
            return true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m335(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, Rect rect) {
            Rect rect2 = floatingActionButton.f193;
            rect.set(floatingActionButton.getLeft() + rect2.left, floatingActionButton.getTop() + rect2.top, floatingActionButton.getRight() - rect2.right, floatingActionButton.getBottom() - rect2.bottom);
            return true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m325(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton) {
            int i;
            Rect rect = floatingActionButton.f193;
            if (rect != null && rect.centerX() > 0 && rect.centerY() > 0) {
                CoordinatorLayout.C0046 r1 = (CoordinatorLayout.C0046) floatingActionButton.getLayoutParams();
                int i2 = 0;
                if (floatingActionButton.getRight() >= coordinatorLayout.getWidth() - r1.rightMargin) {
                    i = rect.right;
                } else {
                    i = floatingActionButton.getLeft() <= r1.leftMargin ? -rect.left : 0;
                }
                if (floatingActionButton.getBottom() >= coordinatorLayout.getHeight() - r1.bottomMargin) {
                    i2 = rect.bottom;
                } else if (floatingActionButton.getTop() <= r1.topMargin) {
                    i2 = -rect.top;
                }
                if (i2 != 0) {
                    C0414.m2231(floatingActionButton, i2);
                }
                if (i != 0) {
                    C0414.m2234(floatingActionButton, i);
                }
            }
        }
    }

    public float getCompatElevation() {
        return getImpl().m420();
    }

    public void setCompatElevation(float f) {
        getImpl().m421(f);
    }

    private C0066 getImpl() {
        if (this.f201 == null) {
            this.f201 = m318();
        }
        return this.f201;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0066 m318() {
        if (Build.VERSION.SDK_INT >= 21) {
            return new C0072(this, new C0051());
        }
        return new C0066(this, new C0051());
    }

    /* renamed from: android.support.design.widget.FloatingActionButton$ʼ  reason: contains not printable characters */
    private class C0051 implements C0077 {
        C0051() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public float m339() {
            return ((float) FloatingActionButton.this.getSizeDimension()) / 2.0f;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m340(int i, int i2, int i3, int i4) {
            FloatingActionButton.this.f193.set(i, i2, i3, i4);
            FloatingActionButton floatingActionButton = FloatingActionButton.this;
            floatingActionButton.setPadding(i + floatingActionButton.f191, i2 + FloatingActionButton.this.f191, i3 + FloatingActionButton.this.f191, i4 + FloatingActionButton.this.f191);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m341(Drawable drawable) {
            FloatingActionButton.super.setBackgroundDrawable(drawable);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m342() {
            return FloatingActionButton.this.f192;
        }
    }
}
