package android.support.design.widget;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

/* renamed from: android.support.design.widget.ˎ  reason: contains not printable characters */
/* compiled from: SnackbarManager */
class C0078 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static C0078 f346;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Object f347 = new Object();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Handler f348 = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            if (message.what != 0) {
                return false;
            }
            C0078.this.m499((C0080) message.obj);
            return true;
        }
    });

    /* renamed from: ʾ  reason: contains not printable characters */
    private C0080 f349;

    /* renamed from: ʿ  reason: contains not printable characters */
    private C0080 f350;

    /* renamed from: android.support.design.widget.ˎ$ʻ  reason: contains not printable characters */
    /* compiled from: SnackbarManager */
    interface C0079 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m504();

        /* renamed from: ʻ  reason: contains not printable characters */
        void m505(int i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static C0078 m491() {
        if (f346 == null) {
            f346 = new C0078();
        }
        return f346;
    }

    private C0078() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m498(C0079 r3, int i) {
        synchronized (this.f347) {
            if (m495(r3)) {
                m492(this.f349, i);
            } else if (m496(r3)) {
                m492(this.f350, i);
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m497(C0079 r2) {
        synchronized (this.f347) {
            if (m495(r2)) {
                this.f349 = null;
                if (this.f350 != null) {
                    m493();
                }
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m500(C0079 r2) {
        synchronized (this.f347) {
            if (m495(r2)) {
                m494(this.f349);
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m501(C0079 r3) {
        synchronized (this.f347) {
            if (m495(r3) && !this.f349.f354) {
                this.f349.f354 = true;
                this.f348.removeCallbacksAndMessages(this.f349);
            }
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public void m502(C0079 r3) {
        synchronized (this.f347) {
            if (m495(r3) && this.f349.f354) {
                this.f349.f354 = false;
                m494(this.f349);
            }
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public boolean m503(C0079 r3) {
        boolean z;
        synchronized (this.f347) {
            if (!m495(r3)) {
                if (!m496(r3)) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    /* renamed from: android.support.design.widget.ˎ$ʼ  reason: contains not printable characters */
    /* compiled from: SnackbarManager */
    private static class C0080 {

        /* renamed from: ʻ  reason: contains not printable characters */
        final WeakReference<C0079> f352;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f353;

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f354;

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m506(C0079 r2) {
            return r2 != null && this.f352.get() == r2;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m493() {
        C0080 r0 = this.f350;
        if (r0 != null) {
            this.f349 = r0;
            this.f350 = null;
            C0079 r1 = this.f349.f352.get();
            if (r1 != null) {
                r1.m504();
            } else {
                this.f349 = null;
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m492(C0080 r3, int i) {
        C0079 r0 = r3.f352.get();
        if (r0 == null) {
            return false;
        }
        this.f348.removeCallbacksAndMessages(r3);
        r0.m505(i);
        return true;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean m495(C0079 r2) {
        C0080 r0 = this.f349;
        return r0 != null && r0.m506(r2);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean m496(C0079 r2) {
        C0080 r0 = this.f350;
        return r0 != null && r0.m506(r2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m494(C0080 r5) {
        if (r5.f353 != -2) {
            int i = 2750;
            if (r5.f353 > 0) {
                i = r5.f353;
            } else if (r5.f353 == -1) {
                i = 1500;
            }
            this.f348.removeCallbacksAndMessages(r5);
            Handler handler = this.f348;
            handler.sendMessageDelayed(Message.obtain(handler, 0, r5), (long) i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m499(C0080 r3) {
        synchronized (this.f347) {
            if (this.f349 == r3 || this.f350 == r3) {
                m492(r3, 2);
            }
        }
    }
}
