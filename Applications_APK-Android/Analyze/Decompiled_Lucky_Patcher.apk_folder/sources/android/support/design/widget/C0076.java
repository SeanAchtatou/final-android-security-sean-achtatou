package android.support.design.widget;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.v7.ʽ.ʻ.C0742;

/* renamed from: android.support.design.widget.ˊ  reason: contains not printable characters */
/* compiled from: ShadowDrawableWrapper */
class C0076 extends C0742 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final double f329 = Math.cos(Math.toRadians(45.0d));

    /* renamed from: ʼ  reason: contains not printable characters */
    final Paint f330;

    /* renamed from: ʽ  reason: contains not printable characters */
    final Paint f331;

    /* renamed from: ʾ  reason: contains not printable characters */
    final RectF f332;

    /* renamed from: ʿ  reason: contains not printable characters */
    float f333;

    /* renamed from: ˆ  reason: contains not printable characters */
    Path f334;

    /* renamed from: ˈ  reason: contains not printable characters */
    float f335;

    /* renamed from: ˉ  reason: contains not printable characters */
    float f336;

    /* renamed from: ˊ  reason: contains not printable characters */
    float f337;

    /* renamed from: ˋ  reason: contains not printable characters */
    float f338;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f339;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final int f340;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final int f341;

    /* renamed from: י  reason: contains not printable characters */
    private final int f342;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f343;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f344;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f345;

    public int getOpacity() {
        return -3;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static int m481(float f) {
        int round = Math.round(f);
        return round % 2 == 1 ? round - 1 : round;
    }

    public void setAlpha(int i) {
        super.setAlpha(i);
        this.f330.setAlpha(i);
        this.f331.setAlpha(i);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.f339 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m485(float f, float f2) {
        if (f < 0.0f || f2 < 0.0f) {
            throw new IllegalArgumentException("invalid shadow size");
        }
        float r3 = (float) m481(f);
        float r4 = (float) m481(f2);
        if (r3 > r4) {
            if (!this.f345) {
                this.f345 = true;
            }
            r3 = r4;
        }
        if (this.f338 != r3 || this.f336 != r4) {
            this.f338 = r3;
            this.f336 = r4;
            this.f337 = (float) Math.round(r3 * 1.5f);
            this.f335 = r4;
            this.f339 = true;
            invalidateSelf();
        }
    }

    public boolean getPadding(Rect rect) {
        int ceil = (int) Math.ceil((double) m477(this.f336, this.f333, this.f343));
        int ceil2 = (int) Math.ceil((double) m480(this.f336, this.f333, this.f343));
        rect.set(ceil2, ceil, ceil2, ceil);
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static float m477(float f, float f2, boolean z) {
        if (!z) {
            return f * 1.5f;
        }
        double d = (double) (f * 1.5f);
        double d2 = (double) f2;
        Double.isNaN(d2);
        Double.isNaN(d);
        return (float) (d + ((1.0d - f329) * d2));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static float m480(float f, float f2, boolean z) {
        if (!z) {
            return f;
        }
        double d = (double) f;
        double d2 = (double) f2;
        Double.isNaN(d2);
        Double.isNaN(d);
        return (float) (d + ((1.0d - f329) * d2));
    }

    public void draw(Canvas canvas) {
        if (this.f339) {
            m479(getBounds());
            this.f339 = false;
        }
        m478(canvas);
        super.draw(canvas);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m484(float f) {
        if (this.f344 != f) {
            this.f344 = f;
            invalidateSelf();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m478(Canvas canvas) {
        float f;
        int i;
        int i2;
        float f2;
        float f3;
        float f4;
        Canvas canvas2 = canvas;
        int save = canvas.save();
        canvas2.rotate(this.f344, this.f332.centerX(), this.f332.centerY());
        float f5 = this.f333;
        float f6 = (-f5) - this.f337;
        float f7 = f5 * 2.0f;
        boolean z = this.f332.width() - f7 > 0.0f;
        boolean z2 = this.f332.height() - f7 > 0.0f;
        float f8 = this.f338;
        float f9 = f5 / ((f8 - (0.5f * f8)) + f5);
        float f10 = f5 / ((f8 - (0.25f * f8)) + f5);
        float f11 = f5 / ((f8 - (f8 * 1.0f)) + f5);
        int save2 = canvas.save();
        canvas2.translate(this.f332.left + f5, this.f332.top + f5);
        canvas2.scale(f9, f10);
        canvas2.drawPath(this.f334, this.f330);
        if (z) {
            canvas2.scale(1.0f / f9, 1.0f);
            i2 = save2;
            f = f11;
            i = save;
            f2 = f10;
            canvas.drawRect(0.0f, f6, this.f332.width() - f7, -this.f333, this.f331);
        } else {
            i2 = save2;
            f = f11;
            i = save;
            f2 = f10;
        }
        canvas2.restoreToCount(i2);
        int save3 = canvas.save();
        canvas2.translate(this.f332.right - f5, this.f332.bottom - f5);
        float f12 = f;
        canvas2.scale(f9, f12);
        canvas2.rotate(180.0f);
        canvas2.drawPath(this.f334, this.f330);
        if (z) {
            canvas2.scale(1.0f / f9, 1.0f);
            f3 = f2;
            f4 = f12;
            canvas.drawRect(0.0f, f6, this.f332.width() - f7, (-this.f333) + this.f337, this.f331);
        } else {
            f3 = f2;
            f4 = f12;
        }
        canvas2.restoreToCount(save3);
        int save4 = canvas.save();
        canvas2.translate(this.f332.left + f5, this.f332.bottom - f5);
        canvas2.scale(f9, f4);
        canvas2.rotate(270.0f);
        canvas2.drawPath(this.f334, this.f330);
        if (z2) {
            canvas2.scale(1.0f / f4, 1.0f);
            canvas.drawRect(0.0f, f6, this.f332.height() - f7, -this.f333, this.f331);
        }
        canvas2.restoreToCount(save4);
        int save5 = canvas.save();
        canvas2.translate(this.f332.right - f5, this.f332.top + f5);
        float f13 = f3;
        canvas2.scale(f9, f13);
        canvas2.rotate(90.0f);
        canvas2.drawPath(this.f334, this.f330);
        if (z2) {
            canvas2.scale(1.0f / f13, 1.0f);
            canvas.drawRect(0.0f, f6, this.f332.height() - f7, -this.f333, this.f331);
        }
        canvas2.restoreToCount(save5);
        canvas2.restoreToCount(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    /* renamed from: ʽ  reason: contains not printable characters */
    private void m482() {
        float f = this.f333;
        RectF rectF = new RectF(-f, -f, f, f);
        RectF rectF2 = new RectF(rectF);
        float f2 = this.f337;
        rectF2.inset(-f2, -f2);
        Path path = this.f334;
        if (path == null) {
            this.f334 = new Path();
        } else {
            path.reset();
        }
        this.f334.setFillType(Path.FillType.EVEN_ODD);
        this.f334.moveTo(-this.f333, 0.0f);
        this.f334.rLineTo(-this.f337, 0.0f);
        this.f334.arcTo(rectF2, 180.0f, 90.0f, false);
        this.f334.arcTo(rectF, 270.0f, -90.0f, false);
        this.f334.close();
        float f3 = -rectF2.top;
        if (f3 > 0.0f) {
            float f4 = this.f333 / f3;
            Paint paint = this.f330;
            RadialGradient radialGradient = r8;
            RadialGradient radialGradient2 = new RadialGradient(0.0f, 0.0f, f3, new int[]{0, this.f340, this.f341, this.f342}, new float[]{0.0f, f4, ((1.0f - f4) / 2.0f) + f4, 1.0f}, Shader.TileMode.CLAMP);
            paint.setShader(radialGradient);
        }
        this.f331.setShader(new LinearGradient(0.0f, rectF.top, 0.0f, rectF2.top, new int[]{this.f340, this.f341, this.f342}, new float[]{0.0f, 0.5f, 1.0f}, Shader.TileMode.CLAMP));
        this.f331.setAntiAlias(false);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m479(Rect rect) {
        float f = this.f336 * 1.5f;
        this.f332.set(((float) rect.left) + this.f336, ((float) rect.top) + f, ((float) rect.right) - this.f336, ((float) rect.bottom) - f);
        m4891().setBounds((int) this.f332.left, (int) this.f332.top, (int) this.f332.right, (int) this.f332.bottom);
        m482();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m486(float f) {
        m485(f, this.f336);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public float m483() {
        return this.f338;
    }
}
