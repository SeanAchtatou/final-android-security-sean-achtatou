package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.C0089;
import android.support.design.internal.C0021;
import android.support.design.internal.C0022;
import android.support.design.internal.C0035;
import android.support.v4.content.C0101;
import android.support.v4.ˉ.C0355;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0439;
import android.support.v7.view.C0536;
import android.support.v7.view.menu.C0504;
import android.support.v7.view.menu.C0508;
import android.support.v7.widget.C0592;
import android.support.v7.ʻ.C0727;
import android.support.v7.ʼ.ʻ.C0739;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class NavigationView extends C0035 {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final int[] f208 = {16842912};

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final int[] f209 = {-16842910};

    /* renamed from: ʽ  reason: contains not printable characters */
    C0052 f210;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final C0021 f211;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final C0022 f212;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f213;

    /* renamed from: ˊ  reason: contains not printable characters */
    private MenuInflater f214;

    /* renamed from: android.support.design.widget.NavigationView$ʻ  reason: contains not printable characters */
    public interface C0052 {
        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m349(MenuItem menuItem);
    }

    public NavigationView(Context context) {
        this(context, null);
    }

    public NavigationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.ˉ.ᐧ.ʻ(android.view.View, boolean):void
     arg types: [android.support.design.widget.NavigationView, boolean]
     candidates:
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ﾞ):android.support.v4.ˉ.ﾞ
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, float):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, int):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.content.res.ColorStateList):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.PorterDuff$Mode):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.graphics.drawable.Drawable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ʼ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, android.support.v4.ˉ.ـ):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, java.lang.Runnable):void
      android.support.v4.ˉ.ᐧ.ʻ(android.view.View, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ʻˏ.ʻ(int, float):float
      android.support.v7.widget.ʻˏ.ʻ(int, int):int
      android.support.v7.widget.ʻˏ.ʻ(int, boolean):boolean */
    public NavigationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ColorStateList colorStateList;
        int i2;
        boolean z;
        this.f212 = new C0022();
        C0083.m512(context);
        this.f211 = new C0021(context);
        C0592 r10 = C0592.m3740(context, attributeSet, C0089.C0098.NavigationView, i, C0089.C0097.Widget_Design_NavigationView);
        C0414.m2223(this, r10.m3744(C0089.C0098.NavigationView_android_background));
        if (r10.m3758(C0089.C0098.NavigationView_elevation)) {
            C0414.m2217(this, (float) r10.m3753(C0089.C0098.NavigationView_elevation, 0));
        }
        C0414.m2228((View) this, r10.m3746(C0089.C0098.NavigationView_android_fitsSystemWindows, false));
        this.f213 = r10.m3753(C0089.C0098.NavigationView_android_maxWidth, 0);
        if (r10.m3758(C0089.C0098.NavigationView_itemIconTint)) {
            colorStateList = r10.m3754(C0089.C0098.NavigationView_itemIconTint);
        } else {
            colorStateList = m343(16842808);
        }
        if (r10.m3758(C0089.C0098.NavigationView_itemTextAppearance)) {
            i2 = r10.m3757(C0089.C0098.NavigationView_itemTextAppearance, 0);
            z = true;
        } else {
            z = false;
            i2 = 0;
        }
        ColorStateList r4 = r10.m3758(C0089.C0098.NavigationView_itemTextColor) ? r10.m3754(C0089.C0098.NavigationView_itemTextColor) : null;
        if (!z && r4 == null) {
            r4 = m343(16842806);
        }
        Drawable r5 = r10.m3744(C0089.C0098.NavigationView_itemBackground);
        this.f211.m2893(new C0504.C0505() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public void m347(C0504 r1) {
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public boolean m348(C0504 r1, MenuItem menuItem) {
                return NavigationView.this.f210 != null && NavigationView.this.f210.m349(menuItem);
            }
        });
        this.f212.m57(1);
        this.f212.m58(context, this.f211);
        this.f212.m59(colorStateList);
        if (z) {
            this.f212.m77(i2);
        }
        this.f212.m73(r4);
        this.f212.m60(r5);
        this.f211.m2895(this.f212);
        addView((View) this.f212.m56((ViewGroup) this));
        if (r10.m3758(C0089.C0098.NavigationView_menu)) {
            m344(r10.m3757(C0089.C0098.NavigationView_menu, 0));
        }
        if (r10.m3758(C0089.C0098.NavigationView_headerLayout)) {
            m346(r10.m3757(C0089.C0098.NavigationView_headerLayout, 0));
        }
        r10.m3745();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        C0053 r1 = new C0053(super.onSaveInstanceState());
        r1.f216 = new Bundle();
        this.f211.m2892(r1.f216);
        return r1;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof C0053)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        C0053 r2 = (C0053) parcelable;
        super.onRestoreInstanceState(r2.m1995());
        this.f211.m2904(r2.f216);
    }

    public void setNavigationItemSelectedListener(C0052 r1) {
        this.f210 = r1;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        if (mode == Integer.MIN_VALUE) {
            i = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(i), this.f213), 1073741824);
        } else if (mode == 0) {
            i = View.MeasureSpec.makeMeasureSpec(this.f213, 1073741824);
        }
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m345(C0439 r2) {
        this.f212.m62(r2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m344(int i) {
        this.f212.m74(true);
        getMenuInflater().inflate(i, this.f211);
        this.f212.m74(false);
        this.f212.m67(false);
    }

    public Menu getMenu() {
        return this.f211;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public View m346(int i) {
        return this.f212.m72(i);
    }

    public int getHeaderCount() {
        return this.f212.m78();
    }

    public ColorStateList getItemIconTintList() {
        return this.f212.m79();
    }

    public void setItemIconTintList(ColorStateList colorStateList) {
        this.f212.m59(colorStateList);
    }

    public ColorStateList getItemTextColor() {
        return this.f212.m80();
    }

    public void setItemTextColor(ColorStateList colorStateList) {
        this.f212.m73(colorStateList);
    }

    public Drawable getItemBackground() {
        return this.f212.m81();
    }

    public void setItemBackgroundResource(int i) {
        setItemBackground(C0101.m539(getContext(), i));
    }

    public void setItemBackground(Drawable drawable) {
        this.f212.m60(drawable);
    }

    public void setCheckedItem(int i) {
        MenuItem findItem = this.f211.findItem(i);
        if (findItem != null) {
            this.f212.m64((C0508) findItem);
        }
    }

    public void setItemTextAppearance(int i) {
        this.f212.m77(i);
    }

    private MenuInflater getMenuInflater() {
        if (this.f214 == null) {
            this.f214 = new C0536(getContext());
        }
        return this.f214;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private ColorStateList m343(int i) {
        TypedValue typedValue = new TypedValue();
        if (!getContext().getTheme().resolveAttribute(i, typedValue, true)) {
            return null;
        }
        ColorStateList r10 = C0739.m4880(getContext(), typedValue.resourceId);
        if (!getContext().getTheme().resolveAttribute(C0727.C0728.colorPrimary, typedValue, true)) {
            return null;
        }
        int i2 = typedValue.data;
        int defaultColor = r10.getDefaultColor();
        return new ColorStateList(new int[][]{f209, f208, EMPTY_STATE_SET}, new int[]{r10.getColorForState(f209, defaultColor), i2, defaultColor});
    }

    /* renamed from: android.support.design.widget.NavigationView$ʼ  reason: contains not printable characters */
    public static class C0053 extends C0355 {
        public static final Parcelable.Creator<C0053> CREATOR = new Parcelable.ClassLoaderCreator<C0053>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public C0053 createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new C0053(parcel, classLoader);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0053 createFromParcel(Parcel parcel) {
                return new C0053(parcel, null);
            }

            /* renamed from: ʻ  reason: contains not printable characters */
            public C0053[] newArray(int i) {
                return new C0053[i];
            }
        };

        /* renamed from: ʻ  reason: contains not printable characters */
        public Bundle f216;

        public C0053(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f216 = parcel.readBundle(classLoader);
        }

        public C0053(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeBundle(this.f216);
        }
    }
}
