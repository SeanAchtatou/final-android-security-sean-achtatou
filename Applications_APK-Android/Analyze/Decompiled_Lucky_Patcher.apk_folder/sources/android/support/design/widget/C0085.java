package android.support.design.widget;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

/* renamed from: android.support.design.widget.ـ  reason: contains not printable characters */
/* compiled from: ViewOffsetBehavior */
class C0085<V extends View> extends CoordinatorLayout.C0043<V> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0086 f365;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f366 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f367 = 0;

    public C0085() {
    }

    public C0085(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m517(CoordinatorLayout coordinatorLayout, View view, int i) {
        m519(coordinatorLayout, view, i);
        if (this.f365 == null) {
            this.f365 = new C0086(view);
        }
        this.f365.m521();
        int i2 = this.f366;
        if (i2 != 0) {
            this.f365.m522(i2);
            this.f366 = 0;
        }
        int i3 = this.f367;
        if (i3 == 0) {
            return true;
        }
        this.f365.m524(i3);
        this.f367 = 0;
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m519(CoordinatorLayout coordinatorLayout, View view, int i) {
        coordinatorLayout.m240(view, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m516(int i) {
        C0086 r0 = this.f365;
        if (r0 != null) {
            return r0.m522(i);
        }
        this.f366 = i;
        return false;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m518() {
        C0086 r0 = this.f365;
        if (r0 != null) {
            return r0.m523();
        }
        return 0;
    }
}
