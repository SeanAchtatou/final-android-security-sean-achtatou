package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import java.util.ArrayList;

/* renamed from: android.support.design.widget.ˆ  reason: contains not printable characters */
/* compiled from: FloatingActionButtonLollipop */
class C0072 extends C0066 {

    /* renamed from: ٴ  reason: contains not printable characters */
    private InsetDrawable f314;

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m451(int[] iArr) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m452() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m455() {
        return false;
    }

    C0072(C0088 r1, C0077 r2) {
        super(r1, r2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m449(int i) {
        if (this.f288 instanceof RippleDrawable) {
            ((RippleDrawable) this.f288).setColor(ColorStateList.valueOf(i));
        } else {
            super.m423(i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m448(float f, float f2) {
        float f3 = f;
        float f4 = f2;
        if (Build.VERSION.SDK_INT != 21) {
            StateListAnimator stateListAnimator = new StateListAnimator();
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.play(ObjectAnimator.ofFloat(this.f293, "elevation", f3).setDuration(0L)).with(ObjectAnimator.ofFloat(this.f293, View.TRANSLATION_Z, f4).setDuration(100L));
            animatorSet.setInterpolator(f280);
            stateListAnimator.addState(f281, animatorSet);
            AnimatorSet animatorSet2 = new AnimatorSet();
            animatorSet2.play(ObjectAnimator.ofFloat(this.f293, "elevation", f3).setDuration(0L)).with(ObjectAnimator.ofFloat(this.f293, View.TRANSLATION_Z, f4).setDuration(100L));
            animatorSet2.setInterpolator(f280);
            stateListAnimator.addState(f282, animatorSet2);
            AnimatorSet animatorSet3 = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            arrayList.add(ObjectAnimator.ofFloat(this.f293, "elevation", f3).setDuration(0L));
            if (Build.VERSION.SDK_INT >= 22 && Build.VERSION.SDK_INT <= 24) {
                arrayList.add(ObjectAnimator.ofFloat(this.f293, View.TRANSLATION_Z, this.f293.getTranslationZ()).setDuration(100L));
            }
            arrayList.add(ObjectAnimator.ofFloat(this.f293, View.TRANSLATION_Z, 0.0f).setDuration(100L));
            animatorSet3.playSequentially((Animator[]) arrayList.toArray(new ObjectAnimator[0]));
            animatorSet3.setInterpolator(f280);
            stateListAnimator.addState(f283, animatorSet3);
            AnimatorSet animatorSet4 = new AnimatorSet();
            animatorSet4.play(ObjectAnimator.ofFloat(this.f293, "elevation", 0.0f).setDuration(0L)).with(ObjectAnimator.ofFloat(this.f293, View.TRANSLATION_Z, 0.0f).setDuration(0L));
            animatorSet4.setInterpolator(f280);
            stateListAnimator.addState(f284, animatorSet4);
            this.f293.setStateListAnimator(stateListAnimator);
        } else if (this.f293.isEnabled()) {
            this.f293.setElevation(f3);
            if (this.f293.isFocused() || this.f293.isPressed()) {
                this.f293.setTranslationZ(f4);
            } else {
                this.f293.setTranslationZ(0.0f);
            }
        } else {
            this.f293.setElevation(0.0f);
            this.f293.setTranslationZ(0.0f);
        }
        if (this.f294.m490()) {
            m434();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public float m447() {
        return this.f293.getElevation();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m454() {
        m434();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m453(Rect rect) {
        if (this.f294.m490()) {
            this.f314 = new InsetDrawable(this.f288, rect.left, rect.top, rect.right, rect.bottom);
            this.f294.m489(this.f314);
            return;
        }
        this.f294.m489(this.f288);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m450(Rect rect) {
        if (this.f294.m490()) {
            float r0 = this.f294.m487();
            float r2 = m447() + this.f292;
            int ceil = (int) Math.ceil((double) C0076.m480(r2, r0, false));
            int ceil2 = (int) Math.ceil((double) C0076.m477(r2, r0, false));
            rect.set(ceil, ceil2, ceil, ceil2);
            return;
        }
        rect.set(0, 0, 0, 0);
    }
}
