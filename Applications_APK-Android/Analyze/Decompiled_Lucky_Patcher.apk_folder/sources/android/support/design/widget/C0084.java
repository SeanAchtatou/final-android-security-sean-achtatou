package android.support.design.widget;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

/* renamed from: android.support.design.widget.י  reason: contains not printable characters */
/* compiled from: ViewGroupUtils */
class C0084 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final ThreadLocal<Matrix> f363 = new ThreadLocal<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final ThreadLocal<RectF> f364 = new ThreadLocal<>();

    /* renamed from: ʻ  reason: contains not printable characters */
    static void m513(ViewGroup viewGroup, View view, Rect rect) {
        Matrix matrix = f363.get();
        if (matrix == null) {
            matrix = new Matrix();
            f363.set(matrix);
        } else {
            matrix.reset();
        }
        m514(viewGroup, view, matrix);
        RectF rectF = f364.get();
        if (rectF == null) {
            rectF = new RectF();
            f364.set(rectF);
        }
        rectF.set(rect);
        matrix.mapRect(rectF);
        rect.set((int) (rectF.left + 0.5f), (int) (rectF.top + 0.5f), (int) (rectF.right + 0.5f), (int) (rectF.bottom + 0.5f));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    static void m515(ViewGroup viewGroup, View view, Rect rect) {
        rect.set(0, 0, view.getWidth(), view.getHeight());
        m513(viewGroup, view, rect);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m514(ViewParent viewParent, View view, Matrix matrix) {
        ViewParent parent = view.getParent();
        if ((parent instanceof View) && parent != viewParent) {
            View view2 = (View) parent;
            m514(viewParent, view2, matrix);
            matrix.preTranslate((float) (-view2.getScrollX()), (float) (-view2.getScrollY()));
        }
        matrix.preTranslate((float) view.getLeft(), (float) view.getTop());
        if (!view.getMatrix().isIdentity()) {
            matrix.preConcat(view.getMatrix());
        }
    }
}
