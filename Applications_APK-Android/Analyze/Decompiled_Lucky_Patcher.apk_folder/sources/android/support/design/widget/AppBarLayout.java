package android.support.design.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.C0089;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.ʾ.C0317;
import android.support.v4.ˉ.C0355;
import android.support.v4.ˉ.C0414;
import android.support.v4.ˉ.C0439;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import java.lang.ref.WeakReference;
import java.util.List;

@CoordinatorLayout.C0044(m290 = Behavior.class)
public class AppBarLayout extends LinearLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f90;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f91;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f92;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f93;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f94;

    /* renamed from: ˆ  reason: contains not printable characters */
    private C0439 f95;

    /* renamed from: ˈ  reason: contains not printable characters */
    private List<C0039> f96;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f97;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f98;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int[] f99;

    /* renamed from: android.support.design.widget.AppBarLayout$ʼ  reason: contains not printable characters */
    public interface C0039 {
        /* renamed from: ʻ  reason: contains not printable characters */
        void m179(AppBarLayout appBarLayout, int i);
    }

    @Deprecated
    public float getTargetElevation() {
        return 0.0f;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        m110();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        m110();
        int i5 = 0;
        this.f93 = false;
        int childCount = getChildCount();
        while (true) {
            if (i5 >= childCount) {
                break;
            } else if (((C0038) getChildAt(i5).getLayoutParams()).m177() != null) {
                this.f93 = true;
                break;
            } else {
                i5++;
            }
        }
        m109();
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m109() {
        int childCount = getChildCount();
        boolean z = false;
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            } else if (((C0038) getChildAt(i).getLayoutParams()).m178()) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        m108(z);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m110() {
        this.f90 = -1;
        this.f91 = -1;
        this.f92 = -1;
    }

    public void setOrientation(int i) {
        if (i == 1) {
            super.setOrientation(i);
            return;
        }
        throw new IllegalArgumentException("AppBarLayout is always vertical and does not support horizontal orientation");
    }

    public void setExpanded(boolean z) {
        m115(z, C0414.m2255(this));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m115(boolean z, boolean z2) {
        m107(z, z2, true);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m107(boolean z, boolean z2, boolean z3) {
        int i = 0;
        int i2 = (z ? 1 : 2) | (z2 ? 4 : 0);
        if (z3) {
            i = 8;
        }
        this.f94 = i2 | i;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof C0038;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0038 generateDefaultLayoutParams() {
        return new C0038(-1, -2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0038 generateLayoutParams(AttributeSet attributeSet) {
        return new C0038(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0038 generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (Build.VERSION.SDK_INT >= 19 && (layoutParams instanceof LinearLayout.LayoutParams)) {
            return new C0038((LinearLayout.LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new C0038((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new C0038(layoutParams);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m117() {
        return this.f93;
    }

    public final int getTotalScrollRange() {
        int i = this.f90;
        if (i != -1) {
            return i;
        }
        int childCount = getChildCount();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            C0038 r5 = (C0038) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i4 = r5.f113;
            if ((i4 & 1) == 0) {
                break;
            }
            i3 += measuredHeight + r5.topMargin + r5.bottomMargin;
            if ((i4 & 2) != 0) {
                i3 -= C0414.m2241(childAt);
                break;
            }
            i2++;
        }
        int max = Math.max(0, i3 - getTopInset());
        this.f90 = max;
        return max;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m118() {
        return getTotalScrollRange() != 0;
    }

    /* access modifiers changed from: package-private */
    public int getUpNestedPreScrollRange() {
        return getTotalScrollRange();
    }

    /* access modifiers changed from: package-private */
    public int getDownNestedPreScrollRange() {
        int i;
        int i2 = this.f91;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            C0038 r4 = (C0038) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i4 = r4.f113;
            if ((i4 & 5) == 5) {
                int i5 = i3 + r4.topMargin + r4.bottomMargin;
                if ((i4 & 8) != 0) {
                    i3 = i5 + C0414.m2241(childAt);
                } else {
                    if ((i4 & 2) != 0) {
                        i = C0414.m2241(childAt);
                    } else {
                        i = getTopInset();
                    }
                    i3 = i5 + (measuredHeight - i);
                }
            } else if (i3 > 0) {
                break;
            }
        }
        int max = Math.max(0, i3);
        this.f91 = max;
        return max;
    }

    /* access modifiers changed from: package-private */
    public int getDownNestedScrollRange() {
        int i = this.f92;
        if (i != -1) {
            return i;
        }
        int childCount = getChildCount();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            C0038 r5 = (C0038) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight() + r5.topMargin + r5.bottomMargin;
            int i4 = r5.f113;
            if ((i4 & 1) == 0) {
                break;
            }
            i3 += measuredHeight;
            if ((i4 & 2) != 0) {
                i3 -= C0414.m2241(childAt) + getTopInset();
                break;
            }
            i2++;
        }
        int max = Math.max(0, i3);
        this.f92 = max;
        return max;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m114(int i) {
        List<C0039> list = this.f96;
        if (list != null) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                C0039 r2 = this.f96.get(i2);
                if (r2 != null) {
                    r2.m179(this, i);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final int getMinimumHeightForVisibleOverlappingContent() {
        int topInset = getTopInset();
        int r1 = C0414.m2241(this);
        if (r1 == 0) {
            int childCount = getChildCount();
            r1 = childCount >= 1 ? C0414.m2241(getChildAt(childCount - 1)) : 0;
            if (r1 == 0) {
                return getHeight() / 3;
            }
        }
        return (r1 * 2) + topInset;
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        if (this.f99 == null) {
            this.f99 = new int[2];
        }
        int[] iArr = this.f99;
        int[] onCreateDrawableState = super.onCreateDrawableState(i + iArr.length);
        iArr[0] = this.f97 ? C0089.C0091.state_collapsible : -C0089.C0091.state_collapsible;
        iArr[1] = (!this.f97 || !this.f98) ? -C0089.C0091.state_collapsed : C0089.C0091.state_collapsed;
        return mergeDrawableStates(onCreateDrawableState, iArr);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m108(boolean z) {
        if (this.f97 == z) {
            return false;
        }
        this.f97 = z;
        refreshDrawableState();
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m116(boolean z) {
        if (this.f98 == z) {
            return false;
        }
        this.f98 = z;
        refreshDrawableState();
        return true;
    }

    @Deprecated
    public void setTargetElevation(float f) {
        if (Build.VERSION.SDK_INT >= 21) {
            C0087.m525(this, f);
        }
    }

    /* access modifiers changed from: package-private */
    public int getPendingAction() {
        return this.f94;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m119() {
        this.f94 = 0;
    }

    /* access modifiers changed from: package-private */
    public final int getTopInset() {
        C0439 r0 = this.f95;
        if (r0 != null) {
            return r0.m2399();
        }
        return 0;
    }

    /* renamed from: android.support.design.widget.AppBarLayout$ʻ  reason: contains not printable characters */
    public static class C0038 extends LinearLayout.LayoutParams {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f113 = 1;

        /* renamed from: ʼ  reason: contains not printable characters */
        Interpolator f114;

        public C0038(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.AppBarLayout_Layout);
            this.f113 = obtainStyledAttributes.getInt(C0089.C0098.AppBarLayout_Layout_layout_scrollFlags, 0);
            if (obtainStyledAttributes.hasValue(C0089.C0098.AppBarLayout_Layout_layout_scrollInterpolator)) {
                this.f114 = AnimationUtils.loadInterpolator(context, obtainStyledAttributes.getResourceId(C0089.C0098.AppBarLayout_Layout_layout_scrollInterpolator, 0));
            }
            obtainStyledAttributes.recycle();
        }

        public C0038(int i, int i2) {
            super(i, i2);
        }

        public C0038(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public C0038(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public C0038(LinearLayout.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m176() {
            return this.f113;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public Interpolator m177() {
            return this.f114;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public boolean m178() {
            int i = this.f113;
            return (i & 1) == 1 && (i & 10) != 0;
        }
    }

    public static class Behavior extends C0073<AppBarLayout> {
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public int f100;

        /* renamed from: ʽ  reason: contains not printable characters */
        private ValueAnimator f101;

        /* renamed from: ʾ  reason: contains not printable characters */
        private int f102 = -1;

        /* renamed from: ʿ  reason: contains not printable characters */
        private boolean f103;

        /* renamed from: ˆ  reason: contains not printable characters */
        private float f104;

        /* renamed from: ˈ  reason: contains not printable characters */
        private WeakReference<View> f105;

        /* renamed from: ˉ  reason: contains not printable characters */
        private C0036 f106;

        /* renamed from: android.support.design.widget.AppBarLayout$Behavior$ʻ  reason: contains not printable characters */
        public static abstract class C0036 {
            /* renamed from: ʻ  reason: contains not printable characters */
            public abstract boolean m159(AppBarLayout appBarLayout);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private static boolean m125(int i, int i2) {
            return (i & i2) == i2;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public /* bridge */ /* synthetic */ boolean m144(int i) {
            return super.m516(i);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public /* bridge */ /* synthetic */ int m152() {
            return super.m518();
        }

        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m151(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, View view2, int i, int i2) {
            ValueAnimator valueAnimator;
            boolean z = (i & 2) != 0 && appBarLayout.m118() && coordinatorLayout.getHeight() - view.getHeight() <= appBarLayout.getHeight();
            if (z && (valueAnimator = this.f101) != null) {
                valueAnimator.cancel();
            }
            this.f105 = null;
            return z;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m143(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int[] iArr, int i3) {
            int i4;
            int i5;
            if (i2 != 0) {
                if (i2 < 0) {
                    int i6 = -appBarLayout.getTotalScrollRange();
                    i5 = i6;
                    i4 = appBarLayout.getDownNestedPreScrollRange() + i6;
                } else {
                    i5 = -appBarLayout.getUpNestedPreScrollRange();
                    i4 = 0;
                }
                if (i5 != i4) {
                    iArr[1] = m463(coordinatorLayout, appBarLayout, i2, i5, i4);
                }
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m142(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int i3, int i4, int i5) {
            if (i4 < 0) {
                m463(coordinatorLayout, appBarLayout, i4, -appBarLayout.getDownNestedScrollRange(), 0);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m141(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i) {
            if (i == 0) {
                m128(coordinatorLayout, appBarLayout);
            }
            this.f105 = new WeakReference<>(view);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m122(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, float f) {
            int i2;
            int abs = Math.abs(m130() - i);
            float abs2 = Math.abs(f);
            if (abs2 > 0.0f) {
                i2 = Math.round((((float) abs) / abs2) * 1000.0f) * 3;
            } else {
                i2 = (int) (((((float) abs) / ((float) appBarLayout.getHeight())) + 1.0f) * 150.0f);
            }
            m123(coordinatorLayout, appBarLayout, i, i2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m123(final CoordinatorLayout coordinatorLayout, final AppBarLayout appBarLayout, int i, int i2) {
            int r0 = m130();
            if (r0 == i) {
                ValueAnimator valueAnimator = this.f101;
                if (valueAnimator != null && valueAnimator.isRunning()) {
                    this.f101.cancel();
                    return;
                }
                return;
            }
            ValueAnimator valueAnimator2 = this.f101;
            if (valueAnimator2 == null) {
                this.f101 = new ValueAnimator();
                this.f101.setInterpolator(C0056.f237);
                this.f101.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        Behavior.this.a_(coordinatorLayout, appBarLayout, ((Integer) valueAnimator.getAnimatedValue()).intValue());
                    }
                });
            } else {
                valueAnimator2.cancel();
            }
            this.f101.setDuration((long) Math.min(i2, 600));
            this.f101.setIntValues(r0, i);
            this.f101.start();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private int m121(AppBarLayout appBarLayout, int i) {
            int childCount = appBarLayout.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = appBarLayout.getChildAt(i2);
                int i3 = -i;
                if (childAt.getTop() <= i3 && childAt.getBottom() >= i3) {
                    return i2;
                }
            }
            return -1;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, float):void
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int):void
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, android.view.View, int):void
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.graphics.Rect, boolean):boolean
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, float):void */
        /* renamed from: ʽ  reason: contains not printable characters */
        private void m128(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            int r0 = m130();
            int r1 = m121(appBarLayout, r0);
            if (r1 >= 0) {
                View childAt = appBarLayout.getChildAt(r1);
                int r3 = ((C0038) childAt.getLayoutParams()).m176();
                if ((r3 & 17) == 17) {
                    int i = -childAt.getTop();
                    int i2 = -childAt.getBottom();
                    if (r1 == appBarLayout.getChildCount() - 1) {
                        i2 += appBarLayout.getTopInset();
                    }
                    if (m125(r3, 2)) {
                        i2 += C0414.m2241(childAt);
                    } else if (m125(r3, 5)) {
                        int r2 = C0414.m2241(childAt) + i2;
                        if (r0 < r2) {
                            i = r2;
                        } else {
                            i2 = r2;
                        }
                    }
                    if (r0 < (i2 + i) / 2) {
                        i = i2;
                    }
                    m122(coordinatorLayout, appBarLayout, C0317.m1833(i, -appBarLayout.getTotalScrollRange(), 0), 0.0f);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, int, int):boolean
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, int, int]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, int, int):boolean
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, android.view.View, android.view.View, int, int):boolean
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, android.view.View, int, int):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, int, int, int[]):void
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, float, float, boolean):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, android.view.View, int, int):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, int, int):boolean */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m150(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3, int i4) {
            if (((CoordinatorLayout.C0046) appBarLayout.getLayoutParams()).height != -2) {
                return super.m270(coordinatorLayout, (View) appBarLayout, i, i2, i3, i4);
            }
            coordinatorLayout.m241(appBarLayout, i, i2, View.MeasureSpec.makeMeasureSpec(0, 0), i4);
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.ـ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, android.os.Parcelable):void
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int):boolean
          android.support.design.widget.ˈ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.support.v4.ˉ.ﾞ):android.support.v4.ˉ.ﾞ
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.graphics.Rect):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View):boolean
          android.support.design.widget.ـ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, float):void
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int):void
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, android.view.View, int):void
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.graphics.Rect, boolean):boolean
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, float):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, boolean):void
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, int]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, int):int
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, int):int
          android.support.design.widget.ˈ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, int):int
          android.support.design.widget.ˈ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, float):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, float, float):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, android.view.View, int):boolean
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, boolean):void */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m149(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i) {
            int i2;
            boolean r10 = super.m517(coordinatorLayout, (View) appBarLayout, i);
            int pendingAction = appBarLayout.getPendingAction();
            int i3 = this.f102;
            if (i3 >= 0 && (pendingAction & 8) == 0) {
                View childAt = appBarLayout.getChildAt(i3);
                int i4 = -childAt.getBottom();
                if (this.f103) {
                    i2 = C0414.m2241(childAt) + appBarLayout.getTopInset();
                } else {
                    i2 = Math.round(((float) childAt.getHeight()) * this.f104);
                }
                a_(coordinatorLayout, appBarLayout, i4 + i2);
            } else if (pendingAction != 0) {
                boolean z = (pendingAction & 4) != 0;
                if ((pendingAction & 2) != 0) {
                    int i5 = -appBarLayout.getUpNestedPreScrollRange();
                    if (z) {
                        m122(coordinatorLayout, appBarLayout, i5, 0.0f);
                    } else {
                        a_(coordinatorLayout, appBarLayout, i5);
                    }
                } else if ((pendingAction & 1) != 0) {
                    if (z) {
                        m122(coordinatorLayout, appBarLayout, 0, 0.0f);
                    } else {
                        a_(coordinatorLayout, appBarLayout, 0);
                    }
                }
            }
            appBarLayout.m119();
            this.f102 = -1;
            m144(C0317.m1833(m152(), -appBarLayout.getTotalScrollRange(), 0));
            m124(coordinatorLayout, appBarLayout, m152(), 0, true);
            appBarLayout.m114(m152());
            return r10;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m158(AppBarLayout appBarLayout) {
            C0036 r0 = this.f106;
            if (r0 != null) {
                return r0.m159(appBarLayout);
            }
            WeakReference<View> weakReference = this.f105;
            if (weakReference == null) {
                return true;
            }
            View view = weakReference.get();
            if (view == null || !view.isShown() || view.canScrollVertically(-1)) {
                return false;
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m139(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            m128(coordinatorLayout, appBarLayout);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public int m154(AppBarLayout appBarLayout) {
            return -appBarLayout.getDownNestedScrollRange();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʽ  reason: contains not printable characters */
        public int m133(AppBarLayout appBarLayout) {
            return appBarLayout.getTotalScrollRange();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, boolean):void
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, int]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, int):int
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, int):int
          android.support.design.widget.ˈ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, int):int
          android.support.design.widget.ˈ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, float):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, float, float):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, android.view.View, int):boolean
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int, int, boolean):void */
        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m132(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3) {
            int r0 = m130();
            int i4 = 0;
            if (i2 == 0 || r0 < i2 || r0 > i3) {
                this.f100 = 0;
            } else {
                int r5 = C0317.m1833(i, i2, i3);
                if (r0 != r5) {
                    int r11 = appBarLayout.m117() ? m126(appBarLayout, r5) : r5;
                    boolean r12 = m144(r11);
                    i4 = r0 - r5;
                    this.f100 = r5 - r11;
                    if (!r12 && appBarLayout.m117()) {
                        coordinatorLayout.m250(appBarLayout);
                    }
                    appBarLayout.m114(m152());
                    m124(coordinatorLayout, appBarLayout, r5, r5 < r0 ? -1 : 1, false);
                }
            }
            return i4;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        private int m126(AppBarLayout appBarLayout, int i) {
            int abs = Math.abs(i);
            int childCount = appBarLayout.getChildCount();
            int i2 = 0;
            int i3 = 0;
            while (true) {
                if (i3 >= childCount) {
                    break;
                }
                View childAt = appBarLayout.getChildAt(i3);
                C0038 r5 = (C0038) childAt.getLayoutParams();
                Interpolator r6 = r5.m177();
                if (abs < childAt.getTop() || abs > childAt.getBottom()) {
                    i3++;
                } else if (r6 != null) {
                    int r1 = r5.m176();
                    if ((r1 & 1) != 0) {
                        i2 = 0 + childAt.getHeight() + r5.topMargin + r5.bottomMargin;
                        if ((r1 & 2) != 0) {
                            i2 -= C0414.m2241(childAt);
                        }
                    }
                    if (C0414.m2247(childAt)) {
                        i2 -= appBarLayout.getTopInset();
                    }
                    if (i2 > 0) {
                        float f = (float) i2;
                        return Integer.signum(i) * (childAt.getTop() + Math.round(f * r6.getInterpolation(((float) (abs - childAt.getTop())) / f)));
                    }
                }
            }
            return i;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m124(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, boolean z) {
            View r0 = m127(appBarLayout, i);
            if (r0 != null) {
                int r1 = ((C0038) r0.getLayoutParams()).m176();
                boolean z2 = false;
                if ((r1 & 1) != 0) {
                    int r2 = C0414.m2241(r0);
                    if (i2 <= 0 || (r1 & 12) == 0 ? !((r1 & 2) == 0 || (-i) < (r0.getBottom() - r2) - appBarLayout.getTopInset()) : (-i) >= (r0.getBottom() - r2) - appBarLayout.getTopInset()) {
                        z2 = true;
                    }
                }
                boolean r8 = appBarLayout.m116(z2);
                if (Build.VERSION.SDK_INT < 11) {
                    return;
                }
                if (z || (r8 && m129(coordinatorLayout, appBarLayout))) {
                    appBarLayout.jumpDrawablesToCurrentState();
                }
            }
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        private boolean m129(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            List<View> r5 = coordinatorLayout.m259(appBarLayout);
            int size = r5.size();
            int i = 0;
            while (i < size) {
                CoordinatorLayout.C0043 r2 = ((CoordinatorLayout.C0046) r5.get(i).getLayoutParams()).m302();
                if (!(r2 instanceof ScrollingViewBehavior)) {
                    i++;
                } else if (((ScrollingViewBehavior) r2).m476() != 0) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        private static View m127(AppBarLayout appBarLayout, int i) {
            int abs = Math.abs(i);
            int childCount = appBarLayout.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = appBarLayout.getChildAt(i2);
                if (abs >= childAt.getTop() && abs <= childAt.getBottom()) {
                    return childAt;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m130() {
            return m152() + this.f100;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.CoordinatorLayout.ʻ.ʼ(android.support.design.widget.CoordinatorLayout, android.view.View):android.os.Parcelable
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.ʼ(android.support.design.widget.AppBarLayout, int):int
          android.support.design.widget.AppBarLayout.Behavior.ʼ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout):android.os.Parcelable
          android.support.design.widget.CoordinatorLayout.ʻ.ʼ(android.support.design.widget.CoordinatorLayout, android.view.View):android.os.Parcelable */
        /* renamed from: ʼ  reason: contains not printable characters */
        public Parcelable m156(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            Parcelable r8 = super.m279(coordinatorLayout, (View) appBarLayout);
            int r0 = m152();
            int childCount = appBarLayout.getChildCount();
            boolean z = false;
            int i = 0;
            while (i < childCount) {
                View childAt = appBarLayout.getChildAt(i);
                int bottom = childAt.getBottom() + r0;
                if (childAt.getTop() + r0 > 0 || bottom < 0) {
                    i++;
                } else {
                    C0037 r02 = new C0037(r8);
                    r02.f110 = i;
                    if (bottom == C0414.m2241(childAt) + appBarLayout.getTopInset()) {
                        z = true;
                    }
                    r02.f112 = z;
                    r02.f111 = ((float) bottom) / ((float) childAt.getHeight());
                    return r02;
                }
            }
            return r8;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, android.os.Parcelable]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, android.os.Parcelable):void
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int):boolean
          android.support.design.widget.AppBarLayout.Behavior.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean
          android.support.design.widget.ˈ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
          android.support.design.widget.ـ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.support.v4.ˉ.ﾞ):android.support.v4.ˉ.ﾞ
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.graphics.Rect):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View):boolean
          android.support.design.widget.CoordinatorLayout.ʻ.ʻ(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void */
        /* renamed from: ʻ  reason: contains not printable characters */
        public void m140(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, Parcelable parcelable) {
            if (parcelable instanceof C0037) {
                C0037 r4 = (C0037) parcelable;
                super.m263(coordinatorLayout, (View) appBarLayout, r4.m1995());
                this.f102 = r4.f110;
                this.f104 = r4.f111;
                this.f103 = r4.f112;
                return;
            }
            super.m263(coordinatorLayout, (View) appBarLayout, parcelable);
            this.f102 = -1;
        }

        /* renamed from: android.support.design.widget.AppBarLayout$Behavior$ʼ  reason: contains not printable characters */
        protected static class C0037 extends C0355 {
            public static final Parcelable.Creator<C0037> CREATOR = new Parcelable.ClassLoaderCreator<C0037>() {
                /* renamed from: ʻ  reason: contains not printable characters */
                public C0037 createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return new C0037(parcel, classLoader);
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                public C0037 createFromParcel(Parcel parcel) {
                    return new C0037(parcel, null);
                }

                /* renamed from: ʻ  reason: contains not printable characters */
                public C0037[] newArray(int i) {
                    return new C0037[i];
                }
            };

            /* renamed from: ʻ  reason: contains not printable characters */
            int f110;

            /* renamed from: ʼ  reason: contains not printable characters */
            float f111;

            /* renamed from: ʽ  reason: contains not printable characters */
            boolean f112;

            public C0037(Parcel parcel, ClassLoader classLoader) {
                super(parcel, classLoader);
                this.f110 = parcel.readInt();
                this.f111 = parcel.readFloat();
                this.f112 = parcel.readByte() != 0;
            }

            public C0037(Parcelable parcelable) {
                super(parcelable);
            }

            public void writeToParcel(Parcel parcel, int i) {
                super.writeToParcel(parcel, i);
                parcel.writeInt(this.f110);
                parcel.writeFloat(this.f111);
                parcel.writeByte(this.f112 ? (byte) 1 : 0);
            }
        }
    }

    public static class ScrollingViewBehavior extends C0075 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public /* bridge */ /* synthetic */ boolean m167(int i) {
            return super.m516(i);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public /* bridge */ /* synthetic */ boolean m168(CoordinatorLayout coordinatorLayout, View view, int i) {
            return super.m517(coordinatorLayout, view, i);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public /* bridge */ /* synthetic */ boolean m169(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
            return super.m470(coordinatorLayout, view, i, i2, i3, i4);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public /* bridge */ /* synthetic */ int m172() {
            return super.m518();
        }

        public ScrollingViewBehavior() {
        }

        public ScrollingViewBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C0089.C0098.ScrollingViewBehavior_Layout);
            m473(obtainStyledAttributes.getDimensionPixelSize(C0089.C0098.ScrollingViewBehavior_Layout_behavior_overlapTop, 0));
            obtainStyledAttributes.recycle();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m171(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2 instanceof AppBarLayout;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m175(CoordinatorLayout coordinatorLayout, View view, View view2) {
            m164(coordinatorLayout, view, view2);
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m170(CoordinatorLayout coordinatorLayout, View view, Rect rect, boolean z) {
            AppBarLayout r0 = m174(coordinatorLayout.m254(view));
            if (r0 != null) {
                rect.offset(view.getLeft(), view.getTop());
                Rect rect2 = this.f325;
                rect2.set(0, 0, coordinatorLayout.getWidth(), coordinatorLayout.getHeight());
                if (!rect2.contains(rect)) {
                    r0.m115(false, !z);
                    return true;
                }
            }
            return false;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        private void m164(CoordinatorLayout coordinatorLayout, View view, View view2) {
            CoordinatorLayout.C0043 r3 = ((CoordinatorLayout.C0046) view2.getLayoutParams()).m302();
            if (r3 instanceof Behavior) {
                C0414.m2231(view, (((view2.getBottom() - view.getTop()) + ((Behavior) r3).f100) + m469()) - m475(view2));
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public float m165(View view) {
            int i;
            if (view instanceof AppBarLayout) {
                AppBarLayout appBarLayout = (AppBarLayout) view;
                int totalScrollRange = appBarLayout.getTotalScrollRange();
                int downNestedPreScrollRange = appBarLayout.getDownNestedPreScrollRange();
                int r5 = m163(appBarLayout);
                if ((downNestedPreScrollRange == 0 || totalScrollRange + r5 > downNestedPreScrollRange) && (i = totalScrollRange - downNestedPreScrollRange) != 0) {
                    return (((float) r5) / ((float) i)) + 1.0f;
                }
            }
            return 0.0f;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private static int m163(AppBarLayout appBarLayout) {
            CoordinatorLayout.C0043 r1 = ((CoordinatorLayout.C0046) appBarLayout.getLayoutParams()).m302();
            if (r1 instanceof Behavior) {
                return ((Behavior) r1).m130();
            }
            return 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public AppBarLayout m174(List<View> list) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                View view = list.get(i);
                if (view instanceof AppBarLayout) {
                    return (AppBarLayout) view;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public int m173(View view) {
            if (view instanceof AppBarLayout) {
                return ((AppBarLayout) view).getTotalScrollRange();
            }
            return super.m471(view);
        }
    }
}
