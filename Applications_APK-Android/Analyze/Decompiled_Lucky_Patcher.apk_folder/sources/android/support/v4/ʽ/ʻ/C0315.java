package android.support.v4.ʽ.ʻ;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.support.v4.ˉ.C0393;
import android.view.MenuItem;
import android.view.View;

/* renamed from: android.support.v4.ʽ.ʻ.ʼ  reason: contains not printable characters */
/* compiled from: SupportMenuItem */
public interface C0315 extends MenuItem {
    boolean collapseActionView();

    boolean expandActionView();

    View getActionView();

    int getAlphabeticModifiers();

    CharSequence getContentDescription();

    ColorStateList getIconTintList();

    PorterDuff.Mode getIconTintMode();

    int getNumericModifiers();

    CharSequence getTooltipText();

    boolean isActionViewExpanded();

    MenuItem setActionView(int i);

    MenuItem setActionView(View view);

    MenuItem setAlphabeticShortcut(char c, int i);

    MenuItem setIconTintList(ColorStateList colorStateList);

    MenuItem setIconTintMode(PorterDuff.Mode mode);

    MenuItem setNumericShortcut(char c, int i);

    MenuItem setShortcut(char c, char c2, int i, int i2);

    void setShowAsAction(int i);

    MenuItem setShowAsActionFlags(int i);

    /* renamed from: ʻ  reason: contains not printable characters */
    C0315 m1829(C0393 r1);

    /* renamed from: ʻ  reason: contains not printable characters */
    C0315 m1830(CharSequence charSequence);

    /* renamed from: ʻ  reason: contains not printable characters */
    C0393 m1831();

    /* renamed from: ʼ  reason: contains not printable characters */
    C0315 m1832(CharSequence charSequence);
}
