package android.support.v4.ʼ;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.v4.content.ʻ.C0102;
import android.support.v4.ˆ.C0326;
import android.support.v4.ˈ.C0337;
import android.widget.TextView;

/* renamed from: android.support.v4.ʼ.ʽ  reason: contains not printable characters */
/* compiled from: TypefaceCompat */
public class C0306 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0307 f1044;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final C0337<String, Typeface> f1045 = new C0337<>(16);

    /* renamed from: android.support.v4.ʼ.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: TypefaceCompat */
    interface C0307 {
        /* renamed from: ʻ  reason: contains not printable characters */
        Typeface m1784(Context context, Resources resources, int i, String str, int i2);

        /* renamed from: ʻ  reason: contains not printable characters */
        Typeface m1785(Context context, CancellationSignal cancellationSignal, C0326.C0328[] r3, int i);

        /* renamed from: ʻ  reason: contains not printable characters */
        Typeface m1786(Context context, C0102.C0104 r2, Resources resources, int i);
    }

    static {
        if (Build.VERSION.SDK_INT >= 26) {
            f1044 = new C0310();
        } else if (Build.VERSION.SDK_INT >= 24 && C0309.m1790()) {
            f1044 = new C0309();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f1044 = new C0308();
        } else {
            f1044 = new C0311();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Typeface m1782(Resources resources, int i, int i2) {
        return f1045.m1932(m1783(resources, i, i2));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static String m1783(Resources resources, int i, int i2) {
        return resources.getResourcePackageName(i) + "-" + i + "-" + i2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Typeface m1781(Context context, C0102.C0103 r7, Resources resources, int i, int i2, TextView textView) {
        Typeface typeface;
        if (r7 instanceof C0102.C0106) {
            C0102.C0106 r72 = (C0102.C0106) r7;
            typeface = C0326.m1851(context, r72.m557(), textView, r72.m558(), r72.m559(), i2);
        } else {
            typeface = f1044.m1786(context, (C0102.C0104) r7, resources, i2);
        }
        if (typeface != null) {
            f1045.m1933(m1783(resources, i, i2), typeface);
        }
        return typeface;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Typeface m1779(Context context, Resources resources, int i, String str, int i2) {
        Typeface r6 = f1044.m1784(context, resources, i, str, i2);
        if (r6 != null) {
            f1045.m1933(m1783(resources, i, i2), r6);
        }
        return r6;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static Typeface m1780(Context context, CancellationSignal cancellationSignal, C0326.C0328[] r3, int i) {
        return f1044.m1785(context, cancellationSignal, r3, i);
    }
}
