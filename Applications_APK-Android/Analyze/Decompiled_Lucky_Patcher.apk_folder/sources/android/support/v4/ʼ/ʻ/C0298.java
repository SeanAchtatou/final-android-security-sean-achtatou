package android.support.v4.ʼ.ʻ;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.ʼ.ʻ.C0295;

/* renamed from: android.support.v4.ʼ.ʻ.ʾ  reason: contains not printable characters */
/* compiled from: DrawableWrapperApi19 */
class C0298 extends C0295 {
    C0298(Drawable drawable) {
        super(drawable);
    }

    C0298(C0295.C0296 r1, Resources resources) {
        super(r1, resources);
    }

    public void setAutoMirrored(boolean z) {
        this.f1030.setAutoMirrored(z);
    }

    public boolean isAutoMirrored() {
        return this.f1030.isAutoMirrored();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0295.C0296 m1760() {
        return new C0299(this.f1029, null);
    }

    /* renamed from: android.support.v4.ʼ.ʻ.ʾ$ʻ  reason: contains not printable characters */
    /* compiled from: DrawableWrapperApi19 */
    private static class C0299 extends C0295.C0296 {
        C0299(C0295.C0296 r1, Resources resources) {
            super(r1, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new C0298(this, resources);
        }
    }
}
