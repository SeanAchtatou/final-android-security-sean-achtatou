package android.support.v4.ʼ;

import android.graphics.Color;

/* renamed from: android.support.v4.ʼ.ʻ  reason: contains not printable characters */
/* compiled from: ColorUtils */
public final class C0287 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final ThreadLocal<double[]> f1022 = new ThreadLocal<>();

    /* renamed from: ʻ  reason: contains not printable characters */
    public static int m1695(int i, int i2) {
        int alpha = Color.alpha(i2);
        int alpha2 = Color.alpha(i);
        int r2 = m1698(alpha2, alpha);
        return Color.argb(r2, m1696(Color.red(i), alpha2, Color.red(i2), alpha, r2), m1696(Color.green(i), alpha2, Color.green(i2), alpha, r2), m1696(Color.blue(i), alpha2, Color.blue(i2), alpha, r2));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static int m1698(int i, int i2) {
        return 255 - (((255 - i2) * (255 - i)) / 255);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int m1696(int i, int i2, int i3, int i4, int i5) {
        if (i5 == 0) {
            return 0;
        }
        return (((i * 255) * i2) + ((i3 * i4) * (255 - i2))) / (i5 * 255);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static int m1697(int i, int i2) {
        if (i2 >= 0 && i2 <= 255) {
            return (i & 16777215) | (i2 << 24);
        }
        throw new IllegalArgumentException("alpha must be between 0 and 255.");
    }
}
