package android.support.v4.ʼ.ʻ;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* renamed from: android.support.v4.ʼ.ʻ.ˆ  reason: contains not printable characters */
/* compiled from: TintAwareDrawable */
public interface C0302 {
    void setTint(int i);

    void setTintList(ColorStateList colorStateList);

    void setTintMode(PorterDuff.Mode mode);
}
