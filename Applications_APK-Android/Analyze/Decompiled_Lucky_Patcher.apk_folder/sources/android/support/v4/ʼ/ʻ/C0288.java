package android.support.v4.ʼ.ʻ;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: android.support.v4.ʼ.ʻ.ʻ  reason: contains not printable characters */
/* compiled from: DrawableCompat */
public final class C0288 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final C0293 f1023;

    /* renamed from: android.support.v4.ʼ.ʻ.ʻ$ʿ  reason: contains not printable characters */
    /* compiled from: DrawableCompat */
    static class C0293 {
        /* renamed from: ʻ  reason: contains not printable characters */
        public int m1734(Drawable drawable) {
            return 0;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1735(Drawable drawable, float f, float f2) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1736(Drawable drawable, int i, int i2, int i3, int i4) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1738(Drawable drawable, Resources.Theme theme) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1741(Drawable drawable, boolean z) {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1742(Drawable drawable, int i) {
            return false;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m1744(Drawable drawable) {
            return false;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m1746(Drawable drawable) {
            return 0;
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m1747(Drawable drawable) {
            return false;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public ColorFilter m1748(Drawable drawable) {
            return null;
        }

        C0293() {
        }

        /* renamed from: ˈ  reason: contains not printable characters */
        public void m1749(Drawable drawable) {
            drawable.jumpToCurrentState();
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1743(Drawable drawable, int i) {
            if (drawable instanceof C0302) {
                ((C0302) drawable).setTint(i);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1737(Drawable drawable, ColorStateList colorStateList) {
            if (drawable instanceof C0302) {
                ((C0302) drawable).setTintList(colorStateList);
            }
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1740(Drawable drawable, PorterDuff.Mode mode) {
            if (drawable instanceof C0302) {
                ((C0302) drawable).setTintMode(mode);
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Drawable m1745(Drawable drawable) {
            return !(drawable instanceof C0302) ? new C0295(drawable) : drawable;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1739(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        }
    }

    /* renamed from: android.support.v4.ʼ.ʻ.ʻ$ʻ  reason: contains not printable characters */
    /* compiled from: DrawableCompat */
    static class C0289 extends C0293 {

        /* renamed from: ʻ  reason: contains not printable characters */
        private static Method f1024;

        /* renamed from: ʼ  reason: contains not printable characters */
        private static boolean f1025;

        /* renamed from: ʽ  reason: contains not printable characters */
        private static Method f1026;

        /* renamed from: ʾ  reason: contains not printable characters */
        private static boolean f1027;

        C0289() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1716(Drawable drawable, int i) {
            if (!f1025) {
                Class<Drawable> cls = Drawable.class;
                try {
                    f1024 = cls.getDeclaredMethod("setLayoutDirection", Integer.TYPE);
                    f1024.setAccessible(true);
                } catch (NoSuchMethodException e) {
                    Log.i("DrawableCompatApi17", "Failed to retrieve setLayoutDirection(int) method", e);
                }
                f1025 = true;
            }
            Method method = f1024;
            if (method != null) {
                try {
                    method.invoke(drawable, Integer.valueOf(i));
                    return true;
                } catch (Exception e2) {
                    Log.i("DrawableCompatApi17", "Failed to invoke setLayoutDirection(int) via reflection", e2);
                    f1024 = null;
                }
            }
            return false;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m1715(Drawable drawable) {
            if (!f1027) {
                try {
                    f1026 = Drawable.class.getDeclaredMethod("getLayoutDirection", new Class[0]);
                    f1026.setAccessible(true);
                } catch (NoSuchMethodException e) {
                    Log.i("DrawableCompatApi17", "Failed to retrieve getLayoutDirection() method", e);
                }
                f1027 = true;
            }
            Method method = f1026;
            if (method != null) {
                try {
                    return ((Integer) method.invoke(drawable, new Object[0])).intValue();
                } catch (Exception e2) {
                    Log.i("DrawableCompatApi17", "Failed to invoke getLayoutDirection() via reflection", e2);
                    f1026 = null;
                }
            }
            return 0;
        }
    }

    /* renamed from: android.support.v4.ʼ.ʻ.ʻ$ʼ  reason: contains not printable characters */
    /* compiled from: DrawableCompat */
    static class C0290 extends C0289 {
        C0290() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1717(Drawable drawable, boolean z) {
            drawable.setAutoMirrored(z);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m1718(Drawable drawable) {
            return drawable.isAutoMirrored();
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Drawable m1719(Drawable drawable) {
            return !(drawable instanceof C0302) ? new C0298(drawable) : drawable;
        }

        /* renamed from: ʾ  reason: contains not printable characters */
        public int m1720(Drawable drawable) {
            return drawable.getAlpha();
        }
    }

    /* renamed from: android.support.v4.ʼ.ʻ.ʻ$ʽ  reason: contains not printable characters */
    /* compiled from: DrawableCompat */
    static class C0291 extends C0290 {
        C0291() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1721(Drawable drawable, float f, float f2) {
            drawable.setHotspot(f, f2);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1722(Drawable drawable, int i, int i2, int i3, int i4) {
            drawable.setHotspotBounds(i, i2, i3, i4);
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public void m1727(Drawable drawable, int i) {
            drawable.setTint(i);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1723(Drawable drawable, ColorStateList colorStateList) {
            drawable.setTintList(colorStateList);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1726(Drawable drawable, PorterDuff.Mode mode) {
            drawable.setTintMode(mode);
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public Drawable m1728(Drawable drawable) {
            return !(drawable instanceof C0302) ? new C0300(drawable) : drawable;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1724(Drawable drawable, Resources.Theme theme) {
            drawable.applyTheme(theme);
        }

        /* renamed from: ʿ  reason: contains not printable characters */
        public boolean m1729(Drawable drawable) {
            return drawable.canApplyTheme();
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        public ColorFilter m1730(Drawable drawable) {
            return drawable.getColorFilter();
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1725(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            drawable.inflate(resources, xmlPullParser, attributeSet, theme);
        }
    }

    /* renamed from: android.support.v4.ʼ.ʻ.ʻ$ʾ  reason: contains not printable characters */
    /* compiled from: DrawableCompat */
    static class C0292 extends C0291 {
        /* renamed from: ʽ  reason: contains not printable characters */
        public Drawable m1733(Drawable drawable) {
            return drawable;
        }

        C0292() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1732(Drawable drawable, int i) {
            return drawable.setLayoutDirection(i);
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m1731(Drawable drawable) {
            return drawable.getLayoutDirection();
        }
    }

    static {
        if (Build.VERSION.SDK_INT >= 23) {
            f1023 = new C0292();
        } else if (Build.VERSION.SDK_INT >= 21) {
            f1023 = new C0291();
        } else if (Build.VERSION.SDK_INT >= 19) {
            f1023 = new C0290();
        } else if (Build.VERSION.SDK_INT >= 17) {
            f1023 = new C0289();
        } else {
            f1023 = new C0293();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1699(Drawable drawable) {
        f1023.m1749(drawable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1707(Drawable drawable, boolean z) {
        f1023.m1741(drawable, z);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m1708(Drawable drawable) {
        return f1023.m1744(drawable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1700(Drawable drawable, float f, float f2) {
        f1023.m1735(drawable, f, f2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1702(Drawable drawable, int i, int i2, int i3, int i4) {
        f1023.m1736(drawable, i, i2, i3, i4);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1701(Drawable drawable, int i) {
        f1023.m1743(drawable, i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1703(Drawable drawable, ColorStateList colorStateList) {
        f1023.m1737(drawable, colorStateList);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1706(Drawable drawable, PorterDuff.Mode mode) {
        f1023.m1740(drawable, mode);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static int m1710(Drawable drawable) {
        return f1023.m1746(drawable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1704(Drawable drawable, Resources.Theme theme) {
        f1023.m1738(drawable, theme);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static boolean m1711(Drawable drawable) {
        return f1023.m1747(drawable);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static ColorFilter m1712(Drawable drawable) {
        return f1023.m1748(drawable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1705(Drawable drawable, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        f1023.m1739(drawable, resources, xmlPullParser, attributeSet, theme);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public static Drawable m1713(Drawable drawable) {
        return f1023.m1745(drawable);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static boolean m1709(Drawable drawable, int i) {
        return f1023.m1742(drawable, i);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static int m1714(Drawable drawable) {
        return f1023.m1734(drawable);
    }
}
