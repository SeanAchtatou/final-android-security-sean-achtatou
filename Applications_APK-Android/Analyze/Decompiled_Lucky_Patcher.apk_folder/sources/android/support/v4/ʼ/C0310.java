package android.support.v4.ʼ;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.fonts.FontVariationAxis;
import android.support.v4.content.ʻ.C0102;
import android.util.Log;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

/* renamed from: android.support.v4.ʼ.ˆ  reason: contains not printable characters */
/* compiled from: TypefaceCompatApi26Impl */
public class C0310 extends C0308 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Class f1050;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final Constructor f1051;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final Method f1052;

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final Method f1053;

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final Method f1054;

    /* renamed from: ˆ  reason: contains not printable characters */
    private static final Method f1055;

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final Method f1056;

    static {
        Method method;
        Method method2;
        Method method3;
        Method method4;
        Method method5;
        Class<?> cls;
        Constructor<?> constructor = null;
        try {
            cls = Class.forName("android.graphics.FontFamily");
            Constructor<?> constructor2 = cls.getConstructor(new Class[0]);
            method4 = cls.getMethod("addFontFromAssetManager", AssetManager.class, String.class, Integer.TYPE, Boolean.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, FontVariationAxis[].class);
            method3 = cls.getMethod("addFontFromBuffer", ByteBuffer.class, Integer.TYPE, FontVariationAxis[].class, Integer.TYPE, Integer.TYPE);
            method2 = cls.getMethod("freeze", new Class[0]);
            method = cls.getMethod("abortCreation", new Class[0]);
            method5 = Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", Array.newInstance(cls, 1).getClass(), Integer.TYPE, Integer.TYPE);
            method5.setAccessible(true);
            constructor = constructor2;
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            Log.e("TypefaceCompatApi26Impl", "Unable to collect necessary methods for class " + e.getClass().getName(), e);
            cls = null;
            method5 = null;
            method4 = null;
            method3 = null;
            method2 = null;
            method = null;
        }
        f1051 = constructor;
        f1050 = cls;
        f1052 = method4;
        f1053 = method3;
        f1054 = method2;
        f1055 = method;
        f1056 = method5;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m1796() {
        if (f1052 == null) {
            Log.w("TypefaceCompatApi26Impl", "Unable to collect necessary private methods.Fallback to legacy implementation.");
        }
        return f1052 != null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Object m1799() {
        try {
            return f1051.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m1797(Context context, Object obj, String str, int i, int i2, int i3) {
        try {
            return ((Boolean) f1052.invoke(obj, context.getAssets(), str, 0, false, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), null)).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m1798(Object obj, ByteBuffer byteBuffer, int i, int i2, int i3) {
        try {
            return ((Boolean) f1053.invoke(obj, byteBuffer, Integer.valueOf(i), null, Integer.valueOf(i2), Integer.valueOf(i3))).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Typeface m1795(Object obj) {
        try {
            Object newInstance = Array.newInstance(f1050, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) f1056.invoke(null, newInstance, -1, -1);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static boolean m1800(Object obj) {
        try {
            return ((Boolean) f1054.invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static boolean m1801(Object obj) {
        try {
            return ((Boolean) f1055.invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Typeface m1804(Context context, C0102.C0104 r10, Resources resources, int i) {
        if (!m1796()) {
            return super.m1809(context, r10, resources, i);
        }
        Object r11 = m1799();
        for (C0102.C0105 r0 : r10.m552()) {
            if (!m1797(context, r11, r0.m553(), 0, r0.m554(), r0.m555() ? 1 : 0)) {
                m1801(r11);
                return null;
            }
        }
        if (!m1800(r11)) {
            return null;
        }
        return m1795(r11);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0041, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0042, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0046, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0047, code lost:
        r7 = r11;
        r11 = r10;
        r10 = r7;
     */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Typeface m1803(android.content.Context r9, android.os.CancellationSignal r10, android.support.v4.ˆ.C0326.C0328[] r11, int r12) {
        /*
            r8 = this;
            int r0 = r11.length
            r1 = 1
            r2 = 0
            if (r0 >= r1) goto L_0x0006
            return r2
        L_0x0006:
            boolean r0 = m1796()
            if (r0 != 0) goto L_0x0057
            android.support.v4.ˆ.ʼ$ʼ r11 = r8.m1811(r11, r12)
            android.content.ContentResolver r9 = r9.getContentResolver()
            android.net.Uri r12 = r11.m1870()     // Catch:{ IOException -> 0x0056 }
            java.lang.String r0 = "r"
            android.os.ParcelFileDescriptor r9 = r9.openFileDescriptor(r12, r0, r10)     // Catch:{ IOException -> 0x0056 }
            android.graphics.Typeface$Builder r10 = new android.graphics.Typeface$Builder     // Catch:{ Throwable -> 0x0044, all -> 0x0041 }
            java.io.FileDescriptor r12 = r9.getFileDescriptor()     // Catch:{ Throwable -> 0x0044, all -> 0x0041 }
            r10.<init>(r12)     // Catch:{ Throwable -> 0x0044, all -> 0x0041 }
            int r12 = r11.m1872()     // Catch:{ Throwable -> 0x0044, all -> 0x0041 }
            android.graphics.Typeface$Builder r10 = r10.setWeight(r12)     // Catch:{ Throwable -> 0x0044, all -> 0x0041 }
            boolean r11 = r11.m1873()     // Catch:{ Throwable -> 0x0044, all -> 0x0041 }
            android.graphics.Typeface$Builder r10 = r10.setItalic(r11)     // Catch:{ Throwable -> 0x0044, all -> 0x0041 }
            android.graphics.Typeface r10 = r10.build()     // Catch:{ Throwable -> 0x0044, all -> 0x0041 }
            if (r9 == 0) goto L_0x0040
            r9.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0040:
            return r10
        L_0x0041:
            r10 = move-exception
            r11 = r2
            goto L_0x004a
        L_0x0044:
            r10 = move-exception
            throw r10     // Catch:{ all -> 0x0046 }
        L_0x0046:
            r11 = move-exception
            r7 = r11
            r11 = r10
            r10 = r7
        L_0x004a:
            if (r9 == 0) goto L_0x0055
            if (r11 == 0) goto L_0x0052
            r9.close()     // Catch:{ Throwable -> 0x0055 }
            goto L_0x0055
        L_0x0052:
            r9.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0055:
            throw r10     // Catch:{ IOException -> 0x0056 }
        L_0x0056:
            return r2
        L_0x0057:
            java.util.Map r9 = android.support.v4.ˆ.C0326.m1856(r9, r11, r10)
            java.lang.Object r10 = m1799()
            int r12 = r11.length
            r0 = 0
            r3 = 0
        L_0x0062:
            if (r0 >= r12) goto L_0x008d
            r4 = r11[r0]
            android.net.Uri r5 = r4.m1870()
            java.lang.Object r5 = r9.get(r5)
            java.nio.ByteBuffer r5 = (java.nio.ByteBuffer) r5
            if (r5 != 0) goto L_0x0073
            goto L_0x008a
        L_0x0073:
            int r3 = r4.m1871()
            int r6 = r4.m1872()
            boolean r4 = r4.m1873()
            boolean r3 = m1798(r10, r5, r3, r6, r4)
            if (r3 != 0) goto L_0x0089
            m1801(r10)
            return r2
        L_0x0089:
            r3 = 1
        L_0x008a:
            int r0 = r0 + 1
            goto L_0x0062
        L_0x008d:
            if (r3 != 0) goto L_0x0093
            m1801(r10)
            return r2
        L_0x0093:
            boolean r9 = m1800(r10)
            if (r9 != 0) goto L_0x009a
            return r2
        L_0x009a:
            android.graphics.Typeface r9 = m1795(r10)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ʼ.C0310.m1803(android.content.Context, android.os.CancellationSignal, android.support.v4.ˆ.ʼ$ʼ[], int):android.graphics.Typeface");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Typeface m1802(Context context, Resources resources, int i, String str, int i2) {
        if (!m1796()) {
            return super.m1807(context, resources, i, str, i2);
        }
        Object r8 = m1799();
        if (!m1797(context, r8, str, 0, -1, -1)) {
            m1801(r8);
            return null;
        } else if (!m1800(r8)) {
            return null;
        } else {
            return m1795(r8);
        }
    }
}
