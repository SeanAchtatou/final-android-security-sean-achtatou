package android.support.v4.ʼ.ʻ;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.v4.ʼ.ʻ.C0295;
import android.util.Log;
import java.lang.reflect.Method;

/* renamed from: android.support.v4.ʼ.ʻ.ʿ  reason: contains not printable characters */
/* compiled from: DrawableWrapperApi21 */
class C0300 extends C0298 {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static Method f1039;

    C0300(Drawable drawable) {
        super(drawable);
        m1761();
    }

    C0300(C0295.C0296 r1, Resources resources) {
        super(r1, resources);
        m1761();
    }

    public void setHotspot(float f, float f2) {
        this.f1030.setHotspot(f, f2);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        this.f1030.setHotspotBounds(i, i2, i3, i4);
    }

    public void getOutline(Outline outline) {
        this.f1030.getOutline(outline);
    }

    public Rect getDirtyBounds() {
        return this.f1030.getDirtyBounds();
    }

    public void setTintList(ColorStateList colorStateList) {
        if (m1763()) {
            super.setTintList(colorStateList);
        } else {
            this.f1030.setTintList(colorStateList);
        }
    }

    public void setTint(int i) {
        if (m1763()) {
            super.setTint(i);
        } else {
            this.f1030.setTint(i);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (m1763()) {
            super.setTintMode(mode);
        } else {
            this.f1030.setTintMode(mode);
        }
    }

    public boolean setState(int[] iArr) {
        if (!super.setState(iArr)) {
            return false;
        }
        invalidateSelf();
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1763() {
        if (Build.VERSION.SDK_INT != 21) {
            return false;
        }
        Drawable drawable = this.f1030;
        if ((drawable instanceof GradientDrawable) || (drawable instanceof DrawableContainer) || (drawable instanceof InsetDrawable) || (drawable instanceof RippleDrawable)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0295.C0296 m1762() {
        return new C0301(this.f1029, null);
    }

    /* renamed from: android.support.v4.ʼ.ʻ.ʿ$ʻ  reason: contains not printable characters */
    /* compiled from: DrawableWrapperApi21 */
    private static class C0301 extends C0295.C0296 {
        C0301(C0295.C0296 r1, Resources resources) {
            super(r1, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new C0300(this, resources);
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m1761() {
        if (f1039 == null) {
            try {
                f1039 = Drawable.class.getDeclaredMethod("isProjected", new Class[0]);
            } catch (Exception e) {
                Log.w("DrawableWrapperApi21", "Failed to retrieve Drawable#isProjected() method", e);
            }
        }
    }
}
