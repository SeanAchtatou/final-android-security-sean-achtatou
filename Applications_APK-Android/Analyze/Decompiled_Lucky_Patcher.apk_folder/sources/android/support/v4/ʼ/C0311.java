package android.support.v4.ʼ;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.support.v4.content.ʻ.C0102;
import android.support.v4.ʼ.C0306;
import android.support.v4.ˆ.C0326;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: android.support.v4.ʼ.ˈ  reason: contains not printable characters */
/* compiled from: TypefaceCompatBaseImpl */
class C0311 implements C0306.C0307 {

    /* renamed from: android.support.v4.ʼ.ˈ$ʻ  reason: contains not printable characters */
    /* compiled from: TypefaceCompatBaseImpl */
    private interface C0312<T> {
        /* renamed from: ʻ  reason: contains not printable characters */
        boolean m1820(T t);

        /* renamed from: ʼ  reason: contains not printable characters */
        int m1821(T t);
    }

    C0311() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static <T> T m1806(T[] tArr, int i, C0312<T> r12) {
        int i2 = (i & 1) == 0 ? 400 : 700;
        boolean z = (i & 2) != 0;
        T t = null;
        int i3 = Integer.MAX_VALUE;
        for (T t2 : tArr) {
            int abs = (Math.abs(r12.m1821(t2) - i2) * 2) + (r12.m1820(t2) == z ? 0 : 1);
            if (t == null || i3 > abs) {
                t = t2;
                i3 = abs;
            }
        }
        return t;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public C0326.C0328 m1811(C0326.C0328[] r2, int i) {
        return (C0326.C0328) m1806(r2, i, new C0312<C0326.C0328>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public int m1814(C0326.C0328 r1) {
                return r1.m1872();
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public boolean m1813(C0326.C0328 r1) {
                return r1.m1873();
            }
        });
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Typeface m1810(Context context, InputStream inputStream) {
        File r2 = C0313.m1822(context);
        if (r2 == null) {
            return null;
        }
        try {
            if (!C0313.m1828(r2, inputStream)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(r2.getPath());
            r2.delete();
            return createFromFile;
        } catch (RuntimeException unused) {
            return null;
        } finally {
            r2.delete();
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Typeface m1808(Context context, CancellationSignal cancellationSignal, C0326.C0328[] r5, int i) {
        InputStream inputStream;
        InputStream inputStream2 = null;
        if (r5.length < 1) {
            return null;
        }
        try {
            inputStream = context.getContentResolver().openInputStream(m1811(r5, i).m1870());
            try {
                Typeface r3 = m1810(context, inputStream);
                C0313.m1826(inputStream);
                return r3;
            } catch (IOException unused) {
                C0313.m1826(inputStream);
                return null;
            } catch (Throwable th) {
                th = th;
                inputStream2 = inputStream;
                C0313.m1826(inputStream2);
                throw th;
            }
        } catch (IOException unused2) {
            inputStream = null;
            C0313.m1826(inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            C0313.m1826(inputStream2);
            throw th;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0102.C0105 m1805(C0102.C0104 r2, int i) {
        return (C0102.C0105) m1806(r2.m552(), i, new C0312<C0102.C0105>() {
            /* renamed from: ʻ  reason: contains not printable characters */
            public int m1818(C0102.C0105 r1) {
                return r1.m554();
            }

            /* renamed from: ʼ  reason: contains not printable characters */
            public boolean m1817(C0102.C0105 r1) {
                return r1.m555();
            }
        });
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Typeface m1809(Context context, C0102.C0104 r3, Resources resources, int i) {
        C0102.C0105 r32 = m1805(r3, i);
        if (r32 == null) {
            return null;
        }
        return C0306.m1779(context, resources, r32.m556(), r32.m553(), i);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Typeface m1807(Context context, Resources resources, int i, String str, int i2) {
        File r1 = C0313.m1822(context);
        if (r1 == null) {
            return null;
        }
        try {
            if (!C0313.m1827(r1, resources, i)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(r1.getPath());
            r1.delete();
            return createFromFile;
        } catch (RuntimeException unused) {
            return null;
        } finally {
            r1.delete();
        }
    }
}
