package android.support.v4.ʼ;

import android.os.ParcelFileDescriptor;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import java.io.File;

/* renamed from: android.support.v4.ʼ.ʾ  reason: contains not printable characters */
/* compiled from: TypefaceCompatApi21Impl */
class C0308 extends C0311 {
    C0308() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private File m1787(ParcelFileDescriptor parcelFileDescriptor) {
        try {
            String readlink = Os.readlink("/proc/self/fd/" + parcelFileDescriptor.getFd());
            if (OsConstants.S_ISREG(Os.stat(readlink).st_mode)) {
                return new File(readlink);
            }
        } catch (ErrnoException unused) {
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0045, code lost:
        r5 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0046, code lost:
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004a, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x004b, code lost:
        r3 = r8;
        r8 = r5;
        r5 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0058, code lost:
        r5 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0059, code lost:
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x005d, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x005e, code lost:
        r3 = r7;
        r7 = r5;
        r5 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0063, code lost:
        if (r7 != null) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0058 A[ExcHandler: all (th java.lang.Throwable)] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0063  */
    /* renamed from: ʻ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Typeface m1788(android.content.Context r5, android.os.CancellationSignal r6, android.support.v4.ˆ.C0326.C0328[] r7, int r8) {
        /*
            r4 = this;
            int r0 = r7.length
            r1 = 0
            r2 = 1
            if (r0 >= r2) goto L_0x0006
            return r1
        L_0x0006:
            android.support.v4.ˆ.ʼ$ʼ r7 = r4.m1811(r7, r8)
            android.content.ContentResolver r8 = r5.getContentResolver()
            android.net.Uri r7 = r7.m1870()     // Catch:{ IOException -> 0x006d }
            java.lang.String r0 = "r"
            android.os.ParcelFileDescriptor r6 = r8.openFileDescriptor(r7, r0, r6)     // Catch:{ IOException -> 0x006d }
            java.io.File r7 = r4.m1787(r6)     // Catch:{ Throwable -> 0x005b, all -> 0x0058 }
            if (r7 == 0) goto L_0x002f
            boolean r8 = r7.canRead()     // Catch:{ Throwable -> 0x005b, all -> 0x0058 }
            if (r8 != 0) goto L_0x0025
            goto L_0x002f
        L_0x0025:
            android.graphics.Typeface r5 = android.graphics.Typeface.createFromFile(r7)     // Catch:{ Throwable -> 0x005b, all -> 0x0058 }
            if (r6 == 0) goto L_0x002e
            r6.close()     // Catch:{ IOException -> 0x006d }
        L_0x002e:
            return r5
        L_0x002f:
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x005b, all -> 0x0058 }
            java.io.FileDescriptor r8 = r6.getFileDescriptor()     // Catch:{ Throwable -> 0x005b, all -> 0x0058 }
            r7.<init>(r8)     // Catch:{ Throwable -> 0x005b, all -> 0x0058 }
            android.graphics.Typeface r5 = super.m1810(r5, r7)     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            r7.close()     // Catch:{ Throwable -> 0x005b, all -> 0x0058 }
            if (r6 == 0) goto L_0x0044
            r6.close()     // Catch:{ IOException -> 0x006d }
        L_0x0044:
            return r5
        L_0x0045:
            r5 = move-exception
            r8 = r1
            goto L_0x004e
        L_0x0048:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x004a }
        L_0x004a:
            r8 = move-exception
            r3 = r8
            r8 = r5
            r5 = r3
        L_0x004e:
            if (r8 == 0) goto L_0x0054
            r7.close()     // Catch:{ Throwable -> 0x0057, all -> 0x0058 }
            goto L_0x0057
        L_0x0054:
            r7.close()     // Catch:{ Throwable -> 0x005b, all -> 0x0058 }
        L_0x0057:
            throw r5     // Catch:{ Throwable -> 0x005b, all -> 0x0058 }
        L_0x0058:
            r5 = move-exception
            r7 = r1
            goto L_0x0061
        L_0x005b:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x005d }
        L_0x005d:
            r7 = move-exception
            r3 = r7
            r7 = r5
            r5 = r3
        L_0x0061:
            if (r6 == 0) goto L_0x006c
            if (r7 == 0) goto L_0x0069
            r6.close()     // Catch:{ Throwable -> 0x006c }
            goto L_0x006c
        L_0x0069:
            r6.close()     // Catch:{ IOException -> 0x006d }
        L_0x006c:
            throw r5     // Catch:{ IOException -> 0x006d }
        L_0x006d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.ʼ.C0308.m1788(android.content.Context, android.os.CancellationSignal, android.support.v4.ˆ.ʼ$ʼ[], int):android.graphics.Typeface");
    }
}
