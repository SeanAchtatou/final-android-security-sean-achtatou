package android.support.v4.ʼ.ʻ;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;

/* renamed from: android.support.v4.ʼ.ʻ.ʽ  reason: contains not printable characters */
/* compiled from: DrawableWrapperApi14 */
class C0295 extends Drawable implements Drawable.Callback, C0294, C0302 {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final PorterDuff.Mode f1028 = PorterDuff.Mode.SRC_IN;

    /* renamed from: ʼ  reason: contains not printable characters */
    C0296 f1029;

    /* renamed from: ʽ  reason: contains not printable characters */
    Drawable f1030;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f1031;

    /* renamed from: ʿ  reason: contains not printable characters */
    private PorterDuff.Mode f1032;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f1033;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f1034;

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m1758() {
        return true;
    }

    C0295(C0296 r1, Resources resources) {
        this.f1029 = r1;
        m1752(resources);
    }

    C0295(Drawable drawable) {
        this.f1029 = m1757();
        m1756(drawable);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m1752(Resources resources) {
        C0296 r0 = this.f1029;
        if (r0 != null && r0.f1036 != null) {
            m1756(m1755(this.f1029.f1036, resources));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Drawable m1755(Drawable.ConstantState constantState, Resources resources) {
        return constantState.newDrawable(resources);
    }

    public void jumpToCurrentState() {
        this.f1030.jumpToCurrentState();
    }

    public void draw(Canvas canvas) {
        this.f1030.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.f1030;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    public void setChangingConfigurations(int i) {
        this.f1030.setChangingConfigurations(i);
    }

    public int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        C0296 r1 = this.f1029;
        return changingConfigurations | (r1 != null ? r1.getChangingConfigurations() : 0) | this.f1030.getChangingConfigurations();
    }

    public void setDither(boolean z) {
        this.f1030.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f1030.setFilterBitmap(z);
    }

    public void setAlpha(int i) {
        this.f1030.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f1030.setColorFilter(colorFilter);
    }

    public boolean isStateful() {
        C0296 r0;
        ColorStateList colorStateList = (!m1758() || (r0 = this.f1029) == null) ? null : r0.f1037;
        return (colorStateList != null && colorStateList.isStateful()) || this.f1030.isStateful();
    }

    public boolean setState(int[] iArr) {
        return m1753(iArr) || this.f1030.setState(iArr);
    }

    public int[] getState() {
        return this.f1030.getState();
    }

    public Drawable getCurrent() {
        return this.f1030.getCurrent();
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f1030.setVisible(z, z2);
    }

    public int getOpacity() {
        return this.f1030.getOpacity();
    }

    public Region getTransparentRegion() {
        return this.f1030.getTransparentRegion();
    }

    public int getIntrinsicWidth() {
        return this.f1030.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        return this.f1030.getIntrinsicHeight();
    }

    public int getMinimumWidth() {
        return this.f1030.getMinimumWidth();
    }

    public int getMinimumHeight() {
        return this.f1030.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        return this.f1030.getPadding(rect);
    }

    public Drawable.ConstantState getConstantState() {
        C0296 r0 = this.f1029;
        if (r0 == null || !r0.m1759()) {
            return null;
        }
        this.f1029.f1035 = getChangingConfigurations();
        return this.f1029;
    }

    public Drawable mutate() {
        if (!this.f1034 && super.mutate() == this) {
            this.f1029 = m1757();
            Drawable drawable = this.f1030;
            if (drawable != null) {
                drawable.mutate();
            }
            C0296 r0 = this.f1029;
            if (r0 != null) {
                Drawable drawable2 = this.f1030;
                r0.f1036 = drawable2 != null ? drawable2.getConstantState() : null;
            }
            this.f1034 = true;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public C0296 m1757() {
        return new C0297(this.f1029, null);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f1030.setLevel(i);
    }

    public void setTint(int i) {
        setTintList(ColorStateList.valueOf(i));
    }

    public void setTintList(ColorStateList colorStateList) {
        this.f1029.f1037 = colorStateList;
        m1753(getState());
    }

    public void setTintMode(PorterDuff.Mode mode) {
        this.f1029.f1038 = mode;
        m1753(getState());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m1753(int[] iArr) {
        if (!m1758()) {
            return false;
        }
        ColorStateList colorStateList = this.f1029.f1037;
        PorterDuff.Mode mode = this.f1029.f1038;
        if (colorStateList == null || mode == null) {
            this.f1033 = false;
            clearColorFilter();
        } else {
            int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
            if (!(this.f1033 && colorForState == this.f1031 && mode == this.f1032)) {
                setColorFilter(colorForState, mode);
                this.f1031 = colorForState;
                this.f1032 = mode;
                this.f1033 = true;
                return true;
            }
        }
        return false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Drawable m1754() {
        return this.f1030;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final void m1756(Drawable drawable) {
        Drawable drawable2 = this.f1030;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.f1030 = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            setVisible(drawable.isVisible(), true);
            setState(drawable.getState());
            setLevel(drawable.getLevel());
            setBounds(drawable.getBounds());
            C0296 r0 = this.f1029;
            if (r0 != null) {
                r0.f1036 = drawable.getConstantState();
            }
        }
        invalidateSelf();
    }

    /* renamed from: android.support.v4.ʼ.ʻ.ʽ$ʻ  reason: contains not printable characters */
    /* compiled from: DrawableWrapperApi14 */
    protected static abstract class C0296 extends Drawable.ConstantState {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f1035;

        /* renamed from: ʼ  reason: contains not printable characters */
        Drawable.ConstantState f1036;

        /* renamed from: ʽ  reason: contains not printable characters */
        ColorStateList f1037 = null;

        /* renamed from: ʾ  reason: contains not printable characters */
        PorterDuff.Mode f1038 = C0295.f1028;

        public abstract Drawable newDrawable(Resources resources);

        C0296(C0296 r1, Resources resources) {
            if (r1 != null) {
                this.f1035 = r1.f1035;
                this.f1036 = r1.f1036;
                this.f1037 = r1.f1037;
                this.f1038 = r1.f1038;
            }
        }

        public Drawable newDrawable() {
            return newDrawable(null);
        }

        public int getChangingConfigurations() {
            int i = this.f1035;
            Drawable.ConstantState constantState = this.f1036;
            return i | (constantState != null ? constantState.getChangingConfigurations() : 0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m1759() {
            return this.f1036 != null;
        }
    }

    /* renamed from: android.support.v4.ʼ.ʻ.ʽ$ʼ  reason: contains not printable characters */
    /* compiled from: DrawableWrapperApi14 */
    private static class C0297 extends C0296 {
        C0297(C0296 r1, Resources resources) {
            super(r1, resources);
        }

        public Drawable newDrawable(Resources resources) {
            return new C0295(this, resources);
        }
    }
}
