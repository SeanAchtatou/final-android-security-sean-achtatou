package android.support.v4.ʼ;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CancellationSignal;
import android.support.v4.content.ʻ.C0102;
import android.support.v4.ˆ.C0326;
import android.support.v4.ˈ.C0353;
import android.util.Log;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.List;

/* renamed from: android.support.v4.ʼ.ʿ  reason: contains not printable characters */
/* compiled from: TypefaceCompatApi24Impl */
class C0309 extends C0311 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Class f1046;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final Constructor f1047;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final Method f1048;

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final Method f1049;

    C0309() {
    }

    static {
        Method method;
        Method method2;
        Class<?> cls;
        Constructor<?> constructor = null;
        try {
            cls = Class.forName("android.graphics.FontFamily");
            Constructor<?> constructor2 = cls.getConstructor(new Class[0]);
            method = cls.getMethod("addFontWeightStyle", ByteBuffer.class, Integer.TYPE, List.class, Integer.TYPE, Boolean.TYPE);
            method2 = Typeface.class.getMethod("createFromFamiliesWithDefault", Array.newInstance(cls, 1).getClass());
            constructor = constructor2;
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            Log.e("TypefaceCompatApi24Impl", e.getClass().getName(), e);
            cls = null;
            method2 = null;
            method = null;
        }
        f1047 = constructor;
        f1046 = cls;
        f1048 = method;
        f1049 = method2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static boolean m1790() {
        if (f1048 == null) {
            Log.w("TypefaceCompatApi24Impl", "Unable to collect necessary private methods.Fallback to legacy implementation.");
        }
        return f1048 != null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Object m1792() {
        try {
            return f1047.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m1791(Object obj, ByteBuffer byteBuffer, int i, int i2, boolean z) {
        try {
            return ((Boolean) f1048.invoke(obj, byteBuffer, Integer.valueOf(i), null, Integer.valueOf(i2), Boolean.valueOf(z))).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static Typeface m1789(Object obj) {
        try {
            Object newInstance = Array.newInstance(f1046, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) f1049.invoke(null, newInstance);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Typeface m1793(Context context, CancellationSignal cancellationSignal, C0326.C0328[] r10, int i) {
        Object r11 = m1792();
        C0353 r0 = new C0353();
        for (C0326.C0328 r3 : r10) {
            Uri r4 = r3.m1870();
            ByteBuffer byteBuffer = (ByteBuffer) r0.get(r4);
            if (byteBuffer == null) {
                byteBuffer = C0313.m1824(context, cancellationSignal, r4);
                r0.put(r4, byteBuffer);
            }
            if (!m1791(r11, byteBuffer, r3.m1871(), r3.m1872(), r3.m1873())) {
                return null;
            }
        }
        return m1789(r11);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Typeface m1794(Context context, C0102.C0104 r8, Resources resources, int i) {
        Object r10 = m1792();
        for (C0102.C0105 r3 : r8.m552()) {
            if (!m1791(r10, C0313.m1823(context, resources, r3.m556()), 0, r3.m554(), r3.m555())) {
                return null;
            }
        }
        return m1789(r10);
    }
}
