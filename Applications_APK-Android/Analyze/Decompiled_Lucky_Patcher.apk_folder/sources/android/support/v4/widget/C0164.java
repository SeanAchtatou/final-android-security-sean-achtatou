package android.support.v4.widget;

import android.os.Build;
import android.widget.EdgeEffect;

/* renamed from: android.support.v4.widget.ʿ  reason: contains not printable characters */
/* compiled from: EdgeEffectCompat */
public final class C0164 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final C0166 f612;

    static {
        if (Build.VERSION.SDK_INT >= 21) {
            f612 = new C0165();
        } else {
            f612 = new C0166();
        }
    }

    /* renamed from: android.support.v4.widget.ʿ$ʼ  reason: contains not printable characters */
    /* compiled from: EdgeEffectCompat */
    static class C0166 {
        C0166() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1023(EdgeEffect edgeEffect, float f, float f2) {
            edgeEffect.onPull(f);
        }
    }

    /* renamed from: android.support.v4.widget.ʿ$ʻ  reason: contains not printable characters */
    /* compiled from: EdgeEffectCompat */
    static class C0165 extends C0166 {
        C0165() {
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public void m1022(EdgeEffect edgeEffect, float f, float f2) {
            edgeEffect.onPull(f, f2);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m1021(EdgeEffect edgeEffect, float f, float f2) {
        f612.m1023(edgeEffect, f, f2);
    }
}
